﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SIBFAPIEn.DTO;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using Contracts;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Org.BouncyCastle.Crypto.Signers;

namespace SIBFAPIEn
{
    public interface IUserService
    {
        LoggedinUserInfo Authenticate(string username, string password, long langid);
       
        // void setTokensInsideCookie(TokenDTO tokenDTO);
    }

    public class UserService : IUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ILoggerManager _logger;
        private List<LoggedinUserInfo> _users = new List<LoggedinUserInfo>();
        private readonly AppCustomSettings _appSettings;
        private readonly IConfiguration _configuration;

        public UserService(IOptions<AppCustomSettings> appSettings, IConfiguration configuration, ILoggerManager logger, IHttpContextAccessor httpContextAccessor)
        {
            _appSettings = appSettings.Value;
            _configuration = configuration;
            _logger = logger;
            this._httpContextAccessor = httpContextAccessor;
        }

        public LoggedinUserInfo Authenticate(string username, string password, long langid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                LoggedinUserInfo userInfo = new LoggedinUserInfo();
                userInfo.IsValidLoggedInMember = false;
                //var entity = _context.XsiExhibitionMember.SingleOrDefault(x => x.UserName == username && x.Password == password && x.Status == "A");
                var entity = _context.XsiExhibitionMember.SingleOrDefault(x => x.UserName == username && x.Status == "A");
                // return null if user not found
                if (entity == null)
                {
                    //return BadRequest();
                    userInfo.StatusMessage = langid == 1 ? "Bad Request or Invalid username/password" : "Bad Request or Invalid username/password";
                    userInfo.PopUpId = "2";
                    //_logger.LogWarn($"Bad Request: Failed login attempt for user {username} from IP {HttpContext.Connection.RemoteIpAddress}");
                    return userInfo;
                }

                //var ismatch = string.Equals(password, entity.Password, StringComparison.OrdinalIgnoreCase);
                var ismatch = PasswordHasher.VerifyPasswordHash(password, entity.PasswordHash, entity.PasswordSalt);
                if (!ismatch)
                {
                    userInfo.StatusMessage = langid == 1 ? "Invalid username/password." : "Invalid username/password."; //"2";
                    userInfo.PopUpId = "2";
                    // _logger.LogWarn($"Invalid username/password: Failed login attempt for user {username} from IP {HttpContext.Connection.RemoteIpAddress}");
                    return userInfo;
                }
            
                userInfo.SIBFMemberId = entity.MemberId;
                userInfo.SIBFMemberPublicName = entity.UserName;
                userInfo.IsLibraryMember = string.Equals(entity.IsLibraryMember, EnumConversion.ToString(EnumBool.Yes), StringComparison.OrdinalIgnoreCase);
                //(entity.IsLibraryMember == EnumConversion.ToString(EnumBool.Yes)) ? true : false;
                userInfo.IsNewPatternChanged = string.IsNullOrEmpty(entity.IsNewPasswordChange) ? "N" : entity.IsNewPasswordChange;
                userInfo.IsValidLoggedInMember = true;
                userInfo.StatusMessage = "/en/MemberDashboard";
                userInfo.PopUpId = "3";
                LogUser(entity.MemberId);

                var claim = new[]
                {
                new Claim(JwtRegisteredClaimNames.FamilyName, userInfo.SIBFMemberPublicName),
                new Claim("ID", Convert.ToString(userInfo.SIBFMemberId))
                };

                var signinKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(_configuration["Jwt:SigningKey"]));

                int expiryInMinutes = Convert.ToInt32(_configuration["Jwt:ExpiryInMinutes"]);

                var token = new JwtSecurityToken(
                    claims: claim,
                    issuer: _configuration["Jwt:Site"],
                    audience: _configuration["Jwt:Site"],
                    expires: DateTime.UtcNow.AddMinutes(expiryInMinutes),
                    signingCredentials: new SigningCredentials(signinKey, SecurityAlgorithms.HmacSha256)
                );

                userInfo.Token = new JwtSecurityTokenHandler().WriteToken(token);
                userInfo.expiration = token.ValidTo;

                return userInfo;
            }
        }
      
        void LogUser(long memberId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibitionMemberLog memberLog = new XsiExhibitionMemberLog();
                memberLog.ExhibitionMemberId = memberId;
              
                memberLog.CreatedOn = MethodFactory.ArabianTimeNow();
                _context.XsiExhibitionMemberLog.Add(memberLog);
                _context.SaveChanges();
            }
        }
        string JWTTokenGenerate(LoggedinUserInfo userInfo)
        {
            var claim = new[]
              {
                new Claim(JwtRegisteredClaimNames.FamilyName, userInfo.SIBFMemberPublicName),
                new Claim("ID", Convert.ToString(userInfo.SIBFMemberId))
                };

            var signinKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_configuration["Jwt:SigningKey"]));

            int expiryInMinutes = Convert.ToInt32(_configuration["Jwt:ExpiryInMinutes"]);

            var token = new JwtSecurityToken(
                claims: claim,
                issuer: _configuration["Jwt:Site"],
                audience: _configuration["Jwt:Site"],
                expires: DateTime.UtcNow.AddMinutes(expiryInMinutes),
                signingCredentials: new SigningCredentials(signinKey, SecurityAlgorithms.HmacSha256)
            );
            userInfo.expiration = token.ValidTo;
           return new JwtSecurityTokenHandler().WriteToken(token);
        }

        string JWTGeneratorQ(LoggedinUserInfo userInfo)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var claim = new[]
                    {
                         //new Claim(JwtRegisteredClaimNames.FamilyName, userInfo.SIBFMemberPublicName),
                         new Claim(ClaimTypes.NameIdentifier,userInfo.SIBFMemberPublicName),
                         new Claim(ClaimTypes.Name,userInfo.SIBFMemberPublicName),
                         //new Claim(ClaimTypes.Role, "USER"),
                            new Claim("ID", Convert.ToString(userInfo.SIBFMemberId))
                    };

            var signinKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:SigningKey"]));

            int expiryInMinutes = Convert.ToInt32(_configuration["Jwt:ExpiryInMinutes"]);

            var token = new JwtSecurityToken(
                claims: claim,
                issuer: _configuration["Jwt:Site"],
                audience: _configuration["Jwt:Site"],
                expires: DateTime.UtcNow.AddMinutes(expiryInMinutes),
                signingCredentials: new SigningCredentials(signinKey, SecurityAlgorithms.HmacSha256)
            );

            var encryptedToken = tokenHandler.WriteToken(token);

            _httpContextAccessor.HttpContext.Response.Cookies.Append("token", encryptedToken,
                new Microsoft.AspNetCore.Http.CookieOptions
                {
                    HttpOnly = true,
                    SameSite = SameSiteMode.None,
                    Secure = true,
                    IsEssential = true,
                    Expires = MethodFactory.ArabianTimeNow().AddDays(1)
                });

            #region From rzr
            var identityUser = new ClaimsIdentity(claim, CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(identityUser);
            _httpContextAccessor.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
            #endregion

            return encryptedToken;
        }


        async void JWTGeneratorRzr(LoggedinUserInfo userInfo)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier,userInfo.SIBFMemberPublicName),
                new Claim(ClaimTypes.Name,userInfo.SIBFMemberPublicName),
                //new Claim(ClaimTypes.Role, "USER"),
                new Claim("ID",Convert.ToString(userInfo.SIBFMemberId))
            };
            var identityUser = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(identityUser);
            await _httpContextAccessor.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
        }
    }
}
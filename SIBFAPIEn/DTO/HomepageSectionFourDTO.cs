﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class HomepageSectionFourDTO
    {
        public HomepageSectionFourDTO()
        {
        }
        public long ItemId { get; set; }
        public string Title { get; set; }
        public string ViewAllUrl { get; set; }
        public string SubSectionOneTitle { get; set; }
        public string SubSectionOneOverview { get; set; }
        public string SubSectionOneImage { get; set; }
        public string SubSectionOneUrl { get; set; }
        public string SubSectionTwoTitle { get; set; }
        public string SubSectionTwoOverview { get; set; }
        public string SubSectionTwoImage { get; set; }
        public string SubSectionTwoUrl { get; set; }
        public string SubSectionThreeTitle { get; set; }
        public string SubSectionThreeOverview { get; set; }
        public string SubSectionThreeImage { get; set; }
        public string SubSectionThreeUrl { get; set; }
    }
}

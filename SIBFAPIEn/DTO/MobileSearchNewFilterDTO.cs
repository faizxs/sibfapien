﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class ListDTO //ItemsDTO to send
    {
        public long? WebsiteId { get; set; }
        public long? TypeId { get; set; }
        //public string FilterBy { get; set; }
        public string SearchBy { get; set; }
        public List<FilterPayLoadDTO> FilterByPayLoad { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
    }
    public class SearchAllDTO //SeachListDTO to send
    {
        public long? WebsiteId { get; set; }
        public string SearchBy { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
    }
    public class FilterPayLoadDTO
    {
        public long? PropertyId { get; set; }
        public string Value { get; set; }
    }

    public class DashboardMobileDTO
    {
        public MetaDataDTO MetaData { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public int Id { get; set; }
        public int Position { get; set; }
        public List<ItemsDTO> Items { get; set; }
    }

    public class ItemsDTO
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Caption { get; set; }
        public string Image { get; set; }
        public string Color { get; set; }
        public string Author { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "type")]
        public string Type { get; set; }
        public string FeatureId { get; set; }
    }
    public class PageCountAndDataDTO
    {
        public int pageCount { get; set; }
        public List<SearchListDTO> data { get; set; }
    }
    public class SearchListDTO
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public long TypeId { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
    }

    public class MetaDataDTO
    {
        public string Layout { get; set; }
        public string View { get; set; }
    }


    #region TMapDTO
    public class TMapDTO
    {
        public long Id { get; set; }
        public string FeatureId { get; set; }
        public string SubTitle { get; set; }
        public string Title { get; set; }
        public string BoothId { get; set; }
        public string Icon { get; set; }  
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "type")]
        public string Type { get; set; }
    }
    #endregion TMapDTO
}


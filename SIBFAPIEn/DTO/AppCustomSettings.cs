﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class AppCustomSettings
    {
        public string ServerAddressNew { get; set; }
        public string ServerAddressSCRF { get; set; }
        public string ServerAddressCMS { get; set; }
        public string HtmltopdfPath { get; set; }
        public string UploadsPath { get; set; }
        public string UploadsCMSPath { get; set; }
        public string ApplicationPhysicalPath { get; set; }
        public string UploadsPhysicalPath { get; set; }
        public string AdminName { get; set; }
        public string AdminNameSCRF { get; set; }
        public string AdminEmail { get; set; }
        public string AccountsEmail { get; set; }
        public string SIBFEmail { get; set; }
        public string RevenueSystemBillingAPI { get; set; }
    }
}

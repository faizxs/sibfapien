﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class ExhibitionCurrencyDTO
    {
        public ExhibitionCurrencyDTO()
        {
            //this.CategoryDTO = new HashSet<CategoryDTO>();
        }
        public long ItemId { get; set; }
        public string Title { get; set; }
        public string CurrencyCode { get; set; }
    }
}

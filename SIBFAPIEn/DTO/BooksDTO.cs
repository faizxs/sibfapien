﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class BooksDTO
    {
        public long MemberExhibitionYearlyId { get; set; }
        public long? MemberId { get; set; }
        public int isexhibitor { get; set; }
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public string TotalNumberOfTitles { get; set; }
        public string TotalNumberOfNewTitles { get; set; }
        public Nullable<long> CountryId { get; set; }
        public Nullable<long> ExhibitionCategoryId { get; set; }
        public Nullable<long> BoothSectionId { get; set; }
        public Nullable<long> BoothSubSectionId { get; set; }
    }

    public class GetBooksToExport_Result
    {
        [Description("TitleEn")]
        public string TitleEn { get; set; }
        [Description("TitleAr")]
        public string TitleAr { get; set; }
        [Description("Author English")]
        public string AuthorEn { get; set; }
        [Description("Author Arabic")]
        public string AuthorAr { get; set; }
        [Description("BookDetails")]
        public string BookDetails { get; set; }
        [Description("BookDetailsAr")]
        public string BookDetailsAr{get;set;}
        [Description("Boothsection")]
        public string Boothsection { get; set; }
        [Description("Boothsubsection")]
        public string Boothsubsection { get; set; }
        [Description("ExhibitionCurrency")]
        public string ExhibitionCurrency { get; set; }
        [Description("ExhibitionLanguage")]
        public string ExhibitionLanguage { get; set; }
        [Description("ExhibitorAr")]
        public string ExhibitorAr { get; set; }
        [Description("Exhibitor English")]
        public string ExhibitorEn { get; set; }
        [Description("Hall Number")]
        public string hallnumber { get; set; }
        [Description("ISBN")]
        public string ISBN { get; set; }
        [Description("IssueYear")]
        public string IssueYear { get; set; }
        [Description("Price")]
        public string Price { get; set; }
        [Description("Publisher Arabic")]
        public string PublisherAr { get; set; }
        [Description("Publisher English")]
        public string PublisherEn { get; set; }
        [Description("Standnumberend")]
        public string Standnumberend { get; set; }
        [Description("Standnumberstart")]
        public string Standnumberstart { get; set; }
        [Description("Subject")]
        public string Subject { get; set; }
        [Description("Subsubject")]
        public string Subsubject { get; set; }
    }
}


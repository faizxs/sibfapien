﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class ContentDTO
    {
        public ContentDTO()
        {

        }
        public long ItemId { get; set; }
        public long? HierarchicalGroupId { get; set; }
        public string ExplorerTitle { get; set; }
        public string PageMetaKeywords { get; set; }
        public string PageMetaDescription { get; set; }
        public string HeaderImage { get; set; }
        public string TitleGray { get; set; }
        public string InnerBanner { get; set; }
        public string PageTitle { get; set; }
        public string PageSubTitle { get; set; }
        public string PageContent { get; set; }
        public string PageSubContent { get; set; }
        public string PageSubSubContent { get; set; }
        public string StrDir { get; set; }
        public long MenuParentId { get; set; }
        public bool isProgram { get; set; }
        public string CanonicalURL { get; set; }
    }
}

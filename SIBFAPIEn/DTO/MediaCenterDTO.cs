﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class MediaCenterDTO
    {
        public long ItemId { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }
    }
}

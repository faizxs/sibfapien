﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class ExhibitorDTO
    {
        public long MemberExhibitionYearlyId { get; set; }
        public long? MemberId { get; set; }
        public int isexhibitor { get; set; }
        public string PublisherName { get; set; }
        public string TotalNumberOfTitles { get; set; }
        public string TotalNumberOfNewTitles { get; set; }
        public string Country { get; set; }
        public string HallNumber { get; set; }
        public string StandNumber { get; set; }
    }
    public partial class XsiExhibitorRegistrationEntityComplete
    {
        public long ExhibitorId { get; set; }
        public Nullable<long> ExhibitionId { get; set; }
        public Nullable<long> MemberId { get; set; }
        public string PublisherName { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonTitle { get; set; }
        public string ExhibitionCountry { get; set; }
        public string ExhibitionCity { get; set; }
        public string ExhibitionCategory { get; set; }
        public string ExhbitionActivity { get; set; }
        public string BoothSection { get; set; }
        public string BoothSubSection { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string TotalNumberOfTitles { get; set; }
        public string TotalNumberOfNewTitles { get; set; }
        public string TotalNumberOfBookTitles { get; set; }
        public string IsFirstTime { get; set; }
        public string Status { get; set; }
        public string AllocatedSpace { get; set; }
        public string AgencyFees { get; set; }
        public string DueFromLastYearCredit { get; set; }
        public string DueFromLastYearDebit { get; set; }
        public string Discount { get; set; }
        public string UploadInvoice { get; set; }
        public string UploadPaymentReceipt { get; set; }
        public Nullable<long> PaymentTypeId { get; set; }
        public string Area { get; set; }
        public string HallNumber { get; set; }
        public string StandNumberStart { get; set; }
        public string StandNumberEnd { get; set; }
        public string UploadLocationMap { get; set; }
        public string FileName { get; set; }
        public string Brief { get; set; }
    }
}


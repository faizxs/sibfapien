﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class PCMemberEventsBookingDTO
    {
        public long? EventId { get; set; }
        public long? MemberId { get; set; }
        public long TicketCount { get; set; }
    }
}


﻿using Microsoft.AspNetCore.Http;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{/// <summary>
 /// DTO for Representative Form
 /// </summary>
    public class SaveRepresentativeDTO
    {
        public long RepresentativeId { get; set; }

        // public long ExhibitionId { get; set; }
        public long WebsiteId { get; set; }
        public long MemberId { get; set; }
        public string IsUAEResident { get; set; }
        public long MemberExhibitionYearlyId { get; set; }
        public bool IsVisaNeed { get; set; }
        public string IsAddHealthInsurance { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string Profession { get; set; }
        public long NationalityId { get; set; }
        public string IsVisitedUae { get; set; }
        public string DateofBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Phone { get; set; }
        public string PhoneISD { get; set; }
        public string PhoneSTD { get; set; }
        public string Mobile { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }

        public string PassportNumber { get; set; }
        public string PlaceOfIssue { get; set; }
        public string PassportDateOfIssue { get; set; }
        public string PassportExpiryDate { get; set; }
        public string FileBase64Passport { get; set; }
        public string FileBase64Passport2 { get; set; }
        public string FileBase64Visa { get; set; }
        public string FileBase64PersonalPhoto { get; set; }
        public string FileBase64CountryUID { get; set; }
        public string FileExtensionofFlightTickets { get; set; }

        public string FileExtensionofPassport { get; set; }
        public string FileExtensionofPassport2 { get; set; }
        public string FileExtensionVisa { get; set; }
        public string FileExtensionofPersonalPhoto { get; set; }
        public string FileExtensionofCountryUID { get; set; }
        public string FileBase64FlightTickets { get; set; }
        public bool IsEditClicked { get; set; }
        public string PassportHRef { get; set; }
        public string PassportCopy2Href { get; set; }
        public string PhotoHref { get; set; }
        public string VisaHref { get; set; }
        public string CountryUidnoHref { get; set; }
        public string Status { get; set; }
        public bool IsRemove { get; set; }
    }

    #region Load
    public class LoadRepresentativeDTO
    {
        public long RepresentativeId { get; set; }

        // public long ExhibitionId { get; set; }
        public long WebsiteId { get; set; }
        public long MemberId { get; set; }
        public long MemberExhibitionYearlyId { get; set; }
        public bool IsVisaNeed { get; set; }
        public string IsUAEResident { get; set; }
        public string IsAddHealthInsurance { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string Profession { get; set; }
        public long NationalityId { get; set; }
        public string IsVisitedUae { get; set; }
        public string DateofBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Phone { get; set; }
        public string PhoneISD { get; set; }
        public string PhoneSTD { get; set; }
        public string Mobile { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }

        public string PassportNumber { get; set; }
        public string PlaceOfIssue { get; set; }
        public string PassportDateOfIssue { get; set; }
        public string PassportExpiryDate { get; set; }
        public string FileBase64Passport { get; set; }
        public string FileBase64Passport2 { get; set; }
        public string FileBase64Visa { get; set; }
        public string FileBase64PersonalPhoto { get; set; }
        public string FileBase64CountryUID { get; set; }
        public string FileExtensionofFlightTickets { get; set; }

        public string FileExtensionofPassport { get; set; }
        public string FileExtensionofPassport2 { get; set; }
        public string FileExtensionVisa { get; set; }
        public string FileExtensionofPersonalPhoto { get; set; }
        public string FileExtensionofCountryUID { get; set; }
        public string FileBase64FlightTickets { get; set; }
        public bool IsEditClicked { get; set; }
        public string PassportHRef { get; set; }
        public string PassportCopy2Href { get; set; }
        public string PhotoHref { get; set; }
        public string VisaHref { get; set; }
        public string CountryUidnoHref { get; set; }
        public string Status { get; set; }
        public bool IsRemove { get; set; }
    }
    #endregion
    /// <summary>
    /// DTO for Tabs
    /// </summary>
    #region Tabs
    public class RepresentativeTabDTO
    {
        public long RepresentativeId { get; set; }
        public long MemberExhibitionYearlyId { get; set; }
        public string Name { get; set; }
        public string Profession { get; set; }
        public string Passport { get; set; }
        public string Nationality { get; set; }
        public string Status { get; set; }
        public string StatusFlag { get; set; }
        public string Date { get; set; }
        public string IsTravelDetails { get; set; }
        public string IsVisaNeed { get; set; }
        public long CountryId { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public bool IsParticipated { get; set; }
        public bool IsRemove { get; set; }
    }
    #endregion

    #region RemoveCommand
    public class RemoveCommandDTO
    {
        public long RepresentativeId { get; set; }
        public long MemberExhibitionYearlyId { get; set; }
        public long WebsiteId { get; set; }
        public string Commandname { get; set; }
    }
    #endregion

    #region MessageDTO
    public class MessageDTO
    {
        public string Message { get; set; }
        public string TravelMessage { get; set; }
        public string MessageTypeResponse { get; set; }
        public string ReturnURL { get; set; }
    }
    public class MessageGxyDTO
    {
        public string Message { get; set; }
        public string MessageTypeResponse { get; set; }
    }
    public class UploadFileMessageDTO
    {
        public string FileName { get; set; }
        public string Message { get; set; }
        public string MessageTypeResponse { get; set; }
        public string ReturnURL { get; set; }
        public string PhysicalPath { get; set; }
    }
    class InviteeDetailsMessageDTO
    {
        public string Message { get; set; }
        public string MessageTypeResponse { get; set; }
        public XsiInvitee Invitee { get; set; }
    }
    public class TGMessageDTO
    {
        public long TranslationGrantMemberId { get; set; }
        public string ActionType { get; set; }
        public string PDFUrl { get; set; }
        public string Message { get; set; }
        public string MessageStepOne { get; set; }
        public string MessageTypeResponse { get; set; }
    }
    #endregion

    public class TravelDTO
    {
        public long RepresentativeId { get; set; }
        public long MemberExhibitionYearlyId { get; set; }
        public long MemberId { get; set; }
        public long WebsiteId { get; set; }
        public long ArrivalAirportId { get; set; }
        public long ArrivalTerminalId { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
    }

    /// <summary>
    /// Upload Agreement
    /// </summary>
    public class UploadAgreementDTO
    {
        public long MemberId { get; set; }
        public long WebsiteId { get; set; }
        public string FileBase64UploadSignedagreement { get; set; }
        public string FileExtensionOfSignedagreement { get; set; }
        public string HiddenAgreement { get; set; }
        public string SignedAgreementHRef { get; set; }
        public bool AgreementThankyouVisibility { get; set; }
        public bool HrefAgreementVisibility { get; set; }
        public List<XsiExhibitionRepresentativeParticipating> RepresentativePDFList { get; set; }
        public string FilePathForDownload { get; set; }
    }

    public class XsiRepresentativeNew
    {
        public long RepresentativeId { get; set; }
        public long MemberExhibitionYearlyId { get; set; }
        public string IsUAEResident { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string Profession { get; set; }
        public string Nationality { get; set; }
        public string IsAddHealthInsurance { get; set; }
        public string DOB { get; set; }
        public string POB { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string PassportNumber { get; set; }
        public string POI { get; set; }
        public string DOI { get; set; }
        public string ExpiryDate { get; set; }
        public string PassportCopy { get; set; }
        public string PassportCopyTwo { get; set; }
        public string HealthInsurance { get; set; }
        public string HealthInsuranceTwo { get; set; }
        public string VisaType { get; set; }
        public string PersonalPhoto { get; set; }
        public string VisaApplicationForm { get; set; }
        public string FlightTickets { get; set; }
        public string VisaCopy { get; set; }
        public string Status { get; set; }
        public string ArrivalDate { get; set; }
        public string DepartureDate { get; set; }
        public string ArrivalAirportId { get; set; }
        public string IsTravelDetails { get; set; }
        public string ExhibitorLastDate { get; set; }
        public string RemainingDays { get; set; }
        public bool IsRemove { get; set; }
        public bool IsVisaNeed { get; set; }
    }

    public class Representative1DTO
    {
        public String ReturnURL { get; set; }
        public String Message { get; set; }
    }
}

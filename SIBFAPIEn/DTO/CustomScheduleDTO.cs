﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class SlotInfo
    {
        public long SlotDay { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string SlotDayText { get; set; }
        public string SlotDayClass { get; set; }
        public long RequestId { get; set; }
        public long SlotId { get; set; }
        public long TransactionId { get; set; }
        public long RegisteredItemId { get; set; }
        public string DisplayNameAndTableNo { get; set; }
        public bool SentInvites { get; set; }
        public bool RegisteredEntityTwo { get; set; }
        public int Count { get; set; }

        public string Status { get; set; }
    }
    public class CustomScheduleDTO
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        #region Day One
        public long SlotDayOne { get; set; }
        public string SlotDayOneClass { get; set; }
        public string SlotDayOneText { get; set; }
        public string DayOneStatus { get; set; }
        public bool DayOneSentInvites { get; set; }
        public long DayOneRequestId { get; set; }
        public long DayOneSlotId { get; set; }
        public long DayOneTransactionId { get; set; }
        public bool DayOneRegisteredEntityTwo { get; set; }
        public int DayOneCount { get; set; }
        #endregion
        #region Day Two
        public long SlotDayTwo { get; set; }
        public string SlotDayTwoClass { get; set; }
        public string SlotDayTwoText { get; set; }
        public string DayTwoStatus { get; set; }
        public bool DayTwoSentInvites { get; set; }
        public long DayTwoRequestId { get; set; }
        public long DayTwoSlotId { get; set; }
        public long DayTwoTransactionId { get; set; }
        public bool DayTwoRegisteredEntityTwo { get; set; }
        public int DayTwoCount { get; set; }
        #endregion
        #region Day Three
        public long SlotDayThree { get; set; }
        public string SlotDayThreeClass { get; set; }
        public string SlotDayThreeText { get; set; }
        public string DayThreeStatus { get; set; }
        public bool DayThreeSentInvites { get; set; }
        public long DayThreeRequestId { get; set; }
        public long DayThreeSlotId { get; set; }
        public long DayThreeTransactionId { get; set; }
        public bool DayThreeRegisteredEntityTwo { get; set; }
        public int DayThreeCount { get; set; }
        #endregion
    }
}

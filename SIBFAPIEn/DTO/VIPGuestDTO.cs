﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class VIPGuestDTO
    {
        public VIPGuestDTO()
        {
            //this.CategoryDTO = new HashSet<CategoryDTO>();
        }
        public long ItemId { get; set; }
        public string GuestPhoto { get; set; }
        public string GuestTitle { get; set; }
        public string GuestName { get; set; }
       // public GuestsEvents_Result Events { get; set; }
       // public string Description { get; set; }
        //public string URL { get; set; }
    }
    public class VIPGuestsEvents_Result
    {
        public long ItemId { get; set; }
        public long EventItemId { get; set; }
        public Nullable<long> LanguageId { get; set; }
        public Nullable<long> GroupId { get; set; }
        public Nullable<long> CategoryId { get; set; }
        public Nullable<long> PhotoAlbumId { get; set; }
        public Nullable<long> VideoAlbumId { get; set; }
        public string Type { get; set; }
        public string IsActive { get; set; }
        public string IsFeatured { get; set; }
        public string Title { get; set; }
        public string Overview { get; set; }
        public string Details { get; set; }
        public string CategoryTitle { get; set; }
        public string Color { get; set; }
        public string SubCategoryTitle { get; set; }
        public Nullable<System.DateTime> StartDate1 { get; set; }
        public Nullable<System.DateTime> EndDate1 { get; set; }
        public Nullable<System.DateTime> StartDateNew1 { get; set; }
        public Nullable<System.DateTime> EndDateNew1 { get; set; }
        public string Guest { get; set; }
        public string Location { get; set; }
        public string FileName { get; set; }
        public string EventThumbnailNew { get; set; }
        public string InnerTopImageOne { get; set; }
        public string InnerTopImageTwo { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class TranslationGrantLoadDTO
    {
        public long TranslationGrantId { get; set; }
        public long TranslationGrantMemberId { get; set; }
        public Nullable<long> MemberId { get; set; }
        public string TGStatus { get; set; }
        public int StepId { get; set; }
        public string Message { get; set; }
        public string MessageStepOne { get; set; }
        public bool IsStartNew { get; set; }
        public bool IsPhaseTwoDateExpired { get; set; }
        public bool IsPhaseThreeDateExpired { get; set; }
        public List<TranslationGrant> TranslationList { get; set; }
        public List<DropdownDataDTO> OldSellersList { get; set; }
        public string TranslationType { get; set; }
        public StepTwoSectionOneDTO StepTwoSectionOneDTO { get; set; }
        public StepTwoSectionTwoDTO StepTwoSectionTwoDTO { get; set; }
        public StepThreeTGSDTO StepThreeTGSDTO { get; set; }
        public StepFourTGSDTO StepFourTGSDTO { get; set; }
        public StepFiveTGSDTO StepFiveTGSDTO { get; set; }
        public StepSixTGSDTO StepSixTGSDTO { get; set; }
    }
    #region Step Two DTO
    public class StepTwoSectionOneDTO
    {
        public List<DropdownDataDTO> OldSellersList { get; set; }
        public string TranslationType { get; set; }
        public string SellerFirstName { get; set; }
        public string SellerMiddleName { get; set; }
        public string SellerLastName { get; set; }
        public string SellerJobTitle { get; set; }
        public string SellerCompanyName { get; set; }
        public string SellerAddress { get; set; }
        public string SellerPhoneISD { get; set; }
        public string SellerPhoneSTD { get; set; }
        public string SellerPhone { get; set; }
        public string SellerFaxISD { get; set; }
        public string SellerFaxSTD { get; set; }
        public string SellerFax { get; set; }
        public string SellerEmail { get; set; }
        public string BuyerFirstName { get; set; }
        public string BuyerMiddleName { get; set; }
        public string BuyerLastName { get; set; }
        public string BuyerJobTitle { get; set; }
        public string BuyerCompanyName { get; set; }
        public string BuyerAddress { get; set; }
        public string BuyerPhoneISD { get; set; }
        public string BuyerPhoneSTD { get; set; }
        public string BuyerPhone { get; set; }
        public string BuyerFaxISD { get; set; }
        public string BuyerFaxSTD { get; set; }
        public string BuyerFax { get; set; }
        public string BuyerEmail { get; set; }
        public string BookTitleOriginal { get; set; }
        public string BookTitleEnglish { get; set; }
        public string TitleInProposedLanguage { get; set; } //TitleInProposedLanguage
        public string Author { get; set; }
        public string Isbn { get; set; }
        public long GenreId { get; set; }
        public string OtherGenre { get; set; }
        public string NoOfPagesOriginal { get; set; }
        public string NoOfWords { get; set; }
        public long OriginalLanguageId { get; set; }
        public long ProposedLanguageId { get; set; }
    }

    public class StepTwoSectionTwoDTO
    {
        public string SignedApplicationFormBase64 { get; set; }
        public string SignedApplicationFormExtension { get; set; }
        public string OriginalBookStatus { get; set; }
        public string SoftCopyofTheBookBase64 { get; set; }
        public string SoftCopyofTheBookExtension { get; set; }
        public string SignedApplicationFormFileName { get; set; }
        public string SoftCopyofTheBookFileName { get; set; }
    }
    #endregion
    #region Step Three to Six DTO
    public class StepThreeTGSDTO
    {
        public string ContractUploadedByAdmin { get; set; }
        public string FundTransferFormByAdmin { get; set; }
        public string SignedContractFormFileName { get; set; }
        public string BuyersAndSellersRightsContractsFileName { get; set; }
        public string FundsTransferFormFileName { get; set; }
        //public string BankName { get; set; }
        //public string BranchNameAddress { get; set; }
        //public string AccountNumber { get; set; }
        //public string IsIBANorBoth { get; set; }
        //public string IBANNo { get; set; }
        //public string SwiftCode { get; set; }

    }
    public class StepFourTGSDTO
    {
        public string AwardGrantValue { get; set; }
        public string FirstPaymentAmount { get; set; }
        public string FirstPaymentDate { get; set; }
        public string PhaseTwoEndDate { get; set; }
        public bool IsHardCopy { get; set; }
        public string DateOfShipment { get; set; }
        public string SoftCopyOfTranslatedBookFileName { get; set; }
        public string FirstPaymentReceipt { get; set; }
    }
    public class StepFiveTGSDTO
    {
        public string SecondPaymentAmount { get; set; }
        public string SecondPaymentDate { get; set; }
        public string Phase3EndDate { get; set; }
        public string SecondPaymentReceipt { get; set; }
    }
    public class StepSixTGSDTO
    {
        public string ThirdPaymentAmount { get; set; }
        public string ThirdPaymentReceipt { get; set; }
        public string ThirdPaymentDate { get; set; }
    }
    #endregion
    public class TranslationGrantHomeDto
    {
        public Nullable<long> Id { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Url { get; set; }
    }
    public class TranslationGrant
    {
        public long ItemId { get; set; }
        public Nullable<long> TranslationGrantId { get; set; }
        public Nullable<long> MemberId { get; set; }
        public string BookTitleOriginal { get; set; }
        public string Status { get; set; }
        public string StatusDate { get; set; }
        public string FinalContract { get; set; }
        public bool IsCancelRegistrationShow { get; set; }
    }
    public class TranslationGrantPost
    {
        public long TranslationGrantMemberId { get; set; }
        public long? MemberId { get; set; }
        public long? TranslationGrantId { get; set; }
        public string IsActive { get; set; }
        public string Status { get; set; }
        public string TranslationType { get; set; }
        public string SellerFirstName { get; set; }
        public string SellerMiddleName { get; set; }
        public string SellerLastName { get; set; }
        public string SellerJobTitle { get; set; }
        public string SellerCompanyName { get; set; }
        public string SellerAddress { get; set; }
        public string SellerPhoneISD { get; set; }
        public string SellerPhoneSTD { get; set; }
        public string SellerPhone { get; set; }
        public string SellerFaxISD { get; set; }
        public string SellerFaxSTD { get; set; }
        public string SellerFax { get; set; }
        public string SellerEmail { get; set; }
        public string BuyerFirstName { get; set; }
        public string BuyerMiddleName { get; set; }
        public string BuyerLastName { get; set; }
        public string BuyerJobTitle { get; set; }
        public string BuyerCompanyName { get; set; }
        public string BuyerAddress { get; set; }
        public string BuyerPhoneISD { get; set; }
        public string BuyerPhoneSTD { get; set; }
        public string BuyerFaxISD { get; set; }
        public string BuyerFaxSTD { get; set; }
        public string BuyerPhone { get; set; }
        public string BuyerFax { get; set; }
        public string BuyerEmail { get; set; }
        public string BookTitleOriginal { get; set; }
        public string BookTitleEnglish { get; set; }
        public string TitleInProposedLanguage { get; set; }
        public string Author { get; set; }
        public string Isbn { get; set; }
        public long GenreId { get; set; }
        public string OtherGenre { get; set; }
        public string NoOfPagesOriginal { get; set; }
        public string NoOfWords { get; set; }
        public long? OriginalLanguageId { get; set; }
        public long? ProposedLanguageId { get; set; }
        public string SignedApplicationFormBase64 { get; set; }
        public string SignedApplicationFormExtension { get; set; }
        public string SoftCopyofTheBookBase64 { get; set; }
        public string SoftCopyofTheBookExtension { get; set; }
        public string OriginalBookStatus { get; set; }
        public string SignedContractFormBase64 { get; set; }
        public string SignedContractFormExtension { get; set; }
        public string BuyersAndSellersRightsContractsBase64 { get; set; }
        public string BuyersAndSellersRightsContractsExtension { get; set; }
        public string FundsTransferFormBase64 { get; set; }
        public string FundsTransferFormExtension { get; set; }

        public string SignedApplicationFormFileName { get; set; }
        public string SoftCopyofTheBookFileName { get; set; }

        //   public string GrantLetter { get; set; }
        //  public string Contract { get; set; }

        //public string BankName { get; set; }
        //public string BranchNameAddress { get; set; }
        //public string AccountNumber { get; set; }
        //public string IsIBANorBoth { get; set; }
        //public string IBAN { get; set; }
        //public string SwiftCode { get; set; }
        //public string OriginalSoftCopy { get; set; }
        public string SoftCopyOfTranslatedBookBase64 { get; set; } //Draft1
        public string SoftCopyOfTranslatedBookExtension { get; set; } //Draft1
        public bool IsHardCopy { get; set; }
        public string DateOfShipment { get; set; }
        //public string Draft1 { get; set; }
        public string SecondPayment { get; set; }
        public DateTime? SecondPaymentDate { get; set; }
        public string SecondPaymentReceipt { get; set; }
        public string FinalPayment { get; set; }
        public DateTime? FinalPaymentDate { get; set; }
        public string FinalPaymentReceipt { get; set; }
        //   public string OriginalBookStatus { get; set; }
        // public string IsIbanorBoth { get; set; }
        //  public long? GenreId { get; set; }
        public string AwardGrantValue { get; set; }
        //  public string IsHardCopy { get; set; }
        //public string BuyerorSellerRights { get; set; }
        //public string FinalContract { get; set; }
        //public string TransferForm { get; set; }
        //public string PhoneISD { get; set; }
        //public string PhoneSTD { get; set; }
        //public string Phone { get; set; }
        //public string FaxISD { get; set; }
        //public string FaxSTD { get; set; }
        //public string Fax { get; set; }
        //public string PhoneISDBuy { get; set; }
        //public string PhoneSTDBuy { get; set; }
        //public string PhoneBuy { get; set; }
        //public string FaxISDBuy { get; set; }
        //public string FaxSTDBuy { get; set; }
        //public string FaxBuy { get; set; }
        public bool ShowSaveAndPrintInfo { get; set; }
        public string ActionType { get; set; }
        public string PDFUrl { get; set; }
        public string TGStatus { get; set; }
    }
    public class ExhibitingDetailsStep2
    {
        public long TranslationGrantMemberId { get; set; }
        public string OriginalBookStatus { get; set; }
        // public string NoOfWords { get; set; }
        public string ApplicationFormFileBase64 { get; set; }
        public string OriginalSoftCopyFileBase64 { get; set; }
        public string ApplicationFormFileExtension { get; set; }
        public string OriginalSoftCopyFileExtension { get; set; }
    }
    public class BuyersOrSellersContractPost
    {
        public long TranslationGrantMemberId { get; set; }
        //public string BankName { get; set; }
        //public string BranchNameAddress { get; set; }
        //public string AccountNumber { get; set; }
        //public string IsIBANorBoth { get; set; }
        //public string IBANNo { get; set; }
        //public string SwiftCode { get; set; }
        public string TransferFormFileBase64 { get; set; }
        public string BuyerorSellerRightsFileBase64 { get; set; }
        public string SignedContractFileBase64 { get; set; }
        public string TransferFormFileExtension { get; set; }
        public string BuyerorSellerRightsFileExtension { get; set; }
        public string SignedContractFileExtension { get; set; }
        //public string ContractUploadedByAdmin { get; set; }
        //public string FundTransferFormByAdmin { get; set; }
    }
    public class BuyersOrSellersContractGet
    {
        public long TranslationGrantMemberId { get; set; }
        //public string BankName { get; set; }
        //public string BranchNameAddress { get; set; }
        //public string AccountNumber { get; set; }
        //public string IsIBANorBoth { get; set; }
        //public string IBANNo { get; set; }
        //public string SwiftCode { get; set; }
        public string SignedContract { get; set; }
        public string BuyerorSellerRights { get; set; }
        public string TransferForm { get; set; }
        public string ContractUploadedByAdmin { get; set; }
        public string FundTransferFormByAdmin { get; set; }
        public string TGStatus { get; set; }
    }
    public class Step4DTO
    {
        public long TranslationGrantMemberId { get; set; }
        public bool IsHardCopy { get; set; }
        public DateTime? DateofShipment { get; set; }
        public string FirstDraftFileBase64 { get; set; }
        public string FirstDraftFileExtension { get; set; }
    }
    public class LoadReceiptDTO
    {
        public long TranslationGrantMemberId { get; set; }
        public string AwardGrantValue { get; set; }
        public string FirstPayment { get; set; }
        public string FirstPaymentDate { get; set; }
        public string Phase2EndDate { get; set; }
        public string Phase3EndDate { get; set; }
        public string FirstPaymentReceipt { get; set; }
        public string FirstPaymentReceiptStatus { get; set; }
        public string SecondPayment { get; set; }
        public string SecondPaymentDate { get; set; }
        public string IsHardCopy { get; set; }
        public DateTime? DateofShipment { get; set; }
        public string SecondPaymentReceipt { get; set; }
        public string FinalPayment { get; set; }
        public string FinalPaymentDate { get; set; }
        public string FinalPaymentReceipt { get; set; }
        public string AwardGrantValueOnStep5 { get; set; }
        public string AwardGrantValueOnStep6 { get; set; }

        public string Message { get; set; }
        public int Step { get; set; }
    }
    #region Winners
    public class YearModel
    {
        public int Year { get; set; }
    }
    public class EditionModel
    {
        public int Edition { get; set; }
    }
    public class TGWinnersDTO
    {
        public string Name { get; set; }
        public string BookTitle { get; set; }
        public string CategoryTitle { get; set; }
        public string Year { get; set; }
        public string BookLanguageTitle { get; set; }
        public string TranslatedToTitle { get; set; }
        public string Edition { get; set; }
        public string BookThumbnail { get; set; }
    }
    public class TGWinnersSearchDTO
    {
        public long? TranslatedTo { get; set; }
        public long? CategoryId { get; set; }
        public string Year { get; set; }
        public string BookTitle { get; set; }
        public string WinnerName { get; set; }
        public long? WebsiteId { get; set; }
        public int? pageNumber { get; set; } = 1;
        public int? pageSize { get; set; } = 12;
    } 
    #endregion
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class HomepageSectionTwoDTO
    {
        public HomepageSectionTwoDTO()
        {
        }
        public long ItemId { get; set; }
        public long? CategoryId { get; set; }
        public string CategoryTitle { get; set; }
        public string Title { get; set; }
        public string Thumbnail { get; set; }
        public string Color { get; set; }
        public string Url { get; set; }
        public long ActivityCount { get; set; }
    }
}

﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class AgencyRegistrationDTO
    {
        //public long MemberExhibitionYearlyId { get; set; }
        public long AgencyId { get; set; }
        public long? MemberId { get; set; }
        public long? ExhibitionId { get; set; }
        public long? WebsiteId { get; set; }
        public long? StepId { get; set; }
        public bool IsEditRequest { get; set; }
        public bool IsBooksRequired { get; set; }
        public long? ExhibitorId { get; set; }
        public string BookMessageResponse { get; set; }
        public StepTwoAgencyDTO StepTwoDTO { get; set; }
        public StepThreeAgencyDTO StepThreeDTO { get; set; }
        public StepFourAgencyDTO StepFourDTO { get; set; }
        //public DateTime AgencyStartDate { get; set; }
        //public DateTime AgencyEndDate { get; set; }
        //public string ThankYouNStatusMsg { get; set; }
        //public string MessageStepThree { get; set; }
        //public string ErrorMsgStepThree { get; set; }
        //public string MessageThree { get; set; }
        //public int TypeofMessage { get; set; }
        //public bool IsPanelVisible { get; set; }
        //public string CSSAttributes { get; set; }
        //public string StatusMessage { get; set; }
        //public bool IsStatusMsgVisible { get; set; }
        public string Message { get; set; }
    }
    public class StepTwoAgencyDTO
    {
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public long? CountryId { get; set; }
        public long? CityId { get; set; }
        public string OtherCity { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonNameAr { get; set; }
        public string ContactPersonTitle { get; set; }
        public string ContactPersonTitleAr { get; set; }
        public string PhoneISD { get; set; }
        public string PhoneSTD { get; set; }
        public string Phone { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }
        public string Mobile { get; set; }
        public string FaxISD { get; set; }
        public string FaxSTD { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public long? ExhibitionCategoryId { get; set; }
    }
    public class StepThreeAgencyDTO
    {
        public string TotalNumberOfTitles { get; set; }
        public string TotalNumberOfNewTitles { get; set; }
        public string PublisherAutoCompleteAr { get; set; }
        public string PublisherAutoCompleteEn { get; set; }
        public long ExhibitorId { get; set; }
        public string AuhtorizationLetterFileName { get; set; }
        public string IsBooksUpload { get; set; }
        public string BooksExcelFileName { get; set; }

    }
    public class StepFourAgencyDTO
    {
        public string Section { get; set; }
        public long BoothSectionId { get; set; }
        public string SubSection { get; set; }
        public string AllocatedSpace { get; set; }
        public string Area { get; set; }
        public string HallNumber { get; set; }
        public string Stand { get; set; }
        public string UploadLocationMap { get; set; }
    }

    public class SaveAgencyDataForStepTwoAndThreeDTO
    {
        public long AgencyId { get; set; }
        public long WebsiteId { get; set; }
        public long ExhibitionId { get; set; }
        public long MemberId { get; set; }
        public long ExhibitorId { get; set; }
        public long CountryId { get; set; }
        public long CityId { get; set; }
        public string OtherCity { get; set; }
        public long ExhibitionCategoryId { get; set; }
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonNameAr { get; set; }
        public string ContactPersonTitle { get; set; }
        public string ContactPersonTitleAr { get; set; }
        public string IsFirstTime { get; set; }
        public string PhoneISD { get; set; }
        public string PhoneSTD { get; set; }
        public string Phone { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }
        public string Mobile { get; set; }
        public string FaxISD { get; set; }
        public string FaxSTD { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string TotalNumberOfTitles { get; set; }
        public string TotalNumberOfNewTitles { get; set; }
        public string ExhibitorNameEn { get; set; }
        public string ExhibitorNameAr { get; set; }
        public string AuhtorizationLetterBase64 { get; set; }
        public string AuhtorizationLetterExtension { get; set; }
        public string AuhtorizationLetterFileName { get; set; }
        public string IsBooksUpload { get; set; }
        public string excelFileBase64 { get; set; }
        public string BookMessageResponse { get; set; }
        //public long BoothSectionId { get; set; }
        //public long BoothSubSectionId { get; set; }
        // public List<long> ActivityIds { get; set; }
        public bool IsEditRequest { get; set; }
    }
}


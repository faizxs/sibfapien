﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class HomepageBannerDTO
    {
        public long ItemId { get; set; }
        public string Cmstitle { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string SubTitleTwo { get; set; }
        public string SubTitleThree { get; set; }
        public string HomeBanner { get; set; }
        public string Url { get; set; }
        public string IsActive { get; set; }
        public string IsFeatured { get; set; }
        public string Color { get; set; }
        public long? SortOrder { get; set; }
    }
    public class HomepageSubBannerNewDTO
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
        public string Color { get; set; }
    }
}

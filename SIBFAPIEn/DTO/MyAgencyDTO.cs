﻿using Microsoft.AspNetCore.Http;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class MyAgencyDTO
    {
        /*public long MemberExhibitionYearlyId { get; set; }
        
        public long? MemberId { get; set; }
        public long? MemberRoleId { get; set; }

        public string AgencyName { get; set; }
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public long? CountryId { get; set; }
        public long? CityId { get; set; }
        public string TotalNumberOfTitles { get; set; }
        public string TotalNumberOfNewTitles { get; set; }
        public long? WebSiteId { get; set; }*/
        public long ExhibitorId { get; set; }
        public long? ExhibitionId { get; set; }
        public int CurrentAgencyCount { get; set; }
        public int CurrentInvitationCount { get; set; }
        public int ShowAgencyCount { get; set; }
        public string InvitationMessage { get; set; }
        public string AgencyMessage { get; set; }
        public string FormMessage { get; set; }
        public bool HideForm { get; set; } = false;
        public string ReturnURL { get; set; }

        public List<AgencyDTO> AgencyList { get; set; }
        public List<AgencyDTO> InvitationList { get; set; }
    }

    public class AgencyDTO
    {
        public long AgencyId { get; set; }
        public long ExhibitorId { get; set; }
        public string AgencyName { get; set; }
        public string ContactPerson { get; set; }
        public string TotalTitles { get; set; }
        public string NewTitles { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public bool IsPending { get; set; }
        public bool IsAddedByExhibitor { get; set; }
    }
    public class AgencyAcceptOrRejectDTO
    {
        public long ItemId { get; set; }
        public long Websiteid { get; set; }
    }
    public class AcceptOrRejectStatusMessage
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
    public class AgencyPostDTO
    {
        public long ExhibitorId { get; set; }
        public long MemberId { get; set; }
        public long WebSiteId { get; set; }
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public long CountryId { get; set; }
        public long CityId { get; set; }
        public string TotalNumberOfTitles { get; set; }
        public string TotalNumberOfNewTitles { get; set; }
        public string AuhtorizationLetterBase64 { get; set; }
        public string AuhtorizationLetterExtension { get; set; }
        public string AuhtorizationLetterFileName { get; set; }
    }

    public partial class AgencyRegistrationEntityCompleteDTO
    {
        public long ItemId { get; set; }
        public Nullable<long> MemberId { get; set; }
        public Nullable<long> ExhibitorId { get; set; }
        public Nullable<long> CountryId { get; set; }
        public Nullable<long> CityId { get; set; }
        public Nullable<long> LanguageId { get; set; }
        public Nullable<long> ExhibitionCategoryId { get; set; }
        public string IsActive { get; set; }
        public string Status { get; set; }
        public string ExhibitorName { get; set; }
        //public string ExhibitorNameAr { get; set; }
        // public string AgencyNameAr { get; set; }
        public string AgencyName { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonTitle { get; set; }
        // public string ContactPersonNameAr { get; set; }
        // public string ContactPersonTitleAr { get; set; }
        public string ExhibitionCountry { get; set; }
        public string ExhibitionCity { get; set; }
        public string ExhibitionCategory { get; set; }
        public string ExhbitionActivity { get; set; }
        public string BoothSection { get; set; }
        public string BoothSubSection { get; set; }
        public string HallNumber { get; set; }
        public string StandNumberStart { get; set; }
        public string StandNumberEnd { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string TotalNumberTitles { get; set; }
        public string TotalNumberNewTitles { get; set; }
        public string FileNumber { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
        public string ExactNameBoard { get; set; }
        public string UploadLocationMap { get; set; }
        public string Address { get; set; }
        public string AuhtorizationLetterFileName { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
    }
}

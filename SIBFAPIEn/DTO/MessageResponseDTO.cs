﻿namespace SIBFAPIEn.DTO
{
    public class MessageResponseDTO
    {
        public string Message { get; set; }
        public string MessageTypeResponse { get; set; }
        public dynamic ResponseDto { get; set; }
    }
}

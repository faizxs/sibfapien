﻿namespace SIBFAPIEn.DTO
{
    public class StaffGuestFlightBookingEditDTO
    {
        public long BookingId { get; set; }
        public long StaffGuestId { get; set; }
        public long WebsiteId { get; set; }
        public long ExhibitionId { get; set; }
        public long MemberId { get; set; }

        public string GuestCategory { get; set; }
        public string GuestPhoneNumber { get; set; }
        public string GuestPhoneISD { get; set; }
        public string GuestPhoneSTD { get; set; }
        public string StaffInChargePhoneNumber { get; set; }
        public string StaffInChargePhoneISD { get; set; }
        public string StaffInChargePhoneSTD { get; set; }
        public string ServiceType { get; set; }
        public string MarhabaReferenceNumber { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string PreferredTime { get; set; }
        public string PreferredAirline { get; set; }
        public string ClassSeat { get; set; }
        public string FlightBookingType { get; set; }
        public string Terminal { get; set; }
        public string FlightDetails { get; set; }
        public string FlightNumber { get; set; }
        //public string FlightCheckInTime { get; set; }
        //public string FlightCheckOutTime { get; set; }
        public string ArrivalTime { get; set; }
        public string ReturnTime { get; set; }
        public string FlightTicket { get; set; }
        public long? DestinationFrom { get; set; }
        public long? DestinationTo { get; set; }
        public long? OriginAirport { get; set; }
        public long? DestinationAirport { get; set; }
        public string ReturnClassSeat { get; set; }
        public string ReturnTerminal { get; set; }
        public string ReturnClassSeatAllotted { get; set; }
        public string ReturnFlightBookingType { get; set; }
        public string ReturnFlightStatus { get; set; }
        public string ReturnFlight { get; set; }
        public string ReturnFlightTicket { get; set; }
        public string ReturnFlightNumber { get; set; }
        public string IsDirectOrTransitFlight { get; set; }
        public string SuggestedFlightFileName { get; set; }

        // public StaffGuestFlightBookingEditLoadDTO LoadFLightDTO;
    }
    public class StaffGuestHoteltBookingEditDTO
    {
        public long BookingId { get; set; }
        public long StaffGuestId { get; set; }
        public long WebsiteId { get; set; }
        public long ExhibitionId { get; set; }
        public long MemberId { get; set; }

        public string HotelBookingStartDate { get; set; }
        public string HotelBookingEndDate { get; set; }
        public string RoomType { get; set; }
        public string RemarksOrSpecialRequest { get; set; }
        public string ReservationReferenceNumber { get; set; }
        public string HotelName { get; set; }
        public string HotelAddress { get; set; }
        public string HotelPhoneNumber { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public string ReservationCopy { get; set; }

        // public StaffGuestHotelBookingEditLoadDTO LoadHotelDTO;
    }
    public class StaffGuestTransportBookingEditDTO
    {
        public long BookingId { get; set; }
        public long StaffGuestId { get; set; }
        public long WebsiteId { get; set; }
        public long ExhibitionId { get; set; }
        public long MemberId { get; set; }
        public string TransportStartDate { get; set; }
        public string TransportEndDate { get; set; }
        public string TransportContactPerson { get; set; }
        public string TransportContactNumber { get; set; }
        public string TransportContactISD { get; set; }
        public string TransportContactSTD { get; set; }
        public string TransportationType { get; set; }
        public string IsIndividualOrGroup { get; set; }
        public int? NoofPeople { get; set; }
        public string PickupLocation { get; set; }

        // public StaffGuestTransportBookingEditLoadDTO LoadTransportDTO;
    }
    public class StaffGuestPassportBookingEditDTO
    {
        public long BookingId { get; set; }
        public long StaffGuestId { get; set; }
        public long WebsiteId { get; set; }
        public long ExhibitionId { get; set; }
        public long MemberId { get; set; }
        public string PassportCopyOne { get; set; }
        public string PassportCopyTwo { get; set; }

        // public UploadsEditDTO UploadsDTO;
    }
   
    #region Post
    public class FlightUpdateStaffGuestBookingDTO
    {
        public long BookingId { get; set; }
        public long StaffGuestId { get; set; }
        public long WebsiteId { get; set; }
        public long ExhibitionId { get; set; }
        public long MemberId { get; set; }

        public StaffGuestFlightBookingDTO StaffGuestFlightBookingDTO;
    }
    public class HotelUpdateStaffGuestBookingDTO
    {
        public long BookingId { get; set; }
        public long StaffGuestId { get; set; }
        public long WebsiteId { get; set; }
        public long ExhibitionId { get; set; }
        public long MemberId { get; set; }
        public StaffGuestHotelBookingDTO StaffGuestHotelBookingDTO;
    }
    public class TransportUpdateStaffGuestBookingDTO
    {
        public long BookingId { get; set; }
        public long StaffGuestId { get; set; }
        public long WebsiteId { get; set; }
        public long ExhibitionId { get; set; }
        public long MemberId { get; set; }
        public StaffGuestTransportBookingDTO StaffGuestTransportBookingDTO;
    }
    public class PassportUpdateStaffGuestBookingDTO
    {
        public long BookingId { get; set; }
        public long StaffGuestId { get; set; }
        public long WebsiteId { get; set; }
        public long ExhibitionId { get; set; }
        public long MemberId { get; set; }
        public bool IsUploadingNewPassport { get; set; }
        public UploadsDTO UploadsDTO;
    }
    #endregion
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class EmailContentDTO
    {
        public string strEmailContentBody { get; set; }
        public string strEmailContentEmail { get; set; }
        public string strEmailContentSubject { get; set; }
        public string strEmailContentAdmin { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class EmailSettings
    {
        public string MailServer { get; set; }
        public string MailServerEmail { get; set; }
        public string MailServerPassword { get; set; }
        public string MailServerSSL { get; set; }
        public int MailServerPort { get; set; }
    }
}

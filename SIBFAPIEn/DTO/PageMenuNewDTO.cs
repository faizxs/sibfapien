﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class PageMenuNewDTO
    {
        public PageMenuNewDTO()
        {
        }
        public long ItemId { get; set; }
        public Nullable<long> ParentId { get; set; }
        public string Title { get; set; }
        public string TitleImage { get; set; }
        public string IsTitleImagePrimary { get; set; }
        public string PageUrl { get; set; }
        public Nullable<long> PageContentId { get; set; }
        public Nullable<long> SortOrder { get; set; }
        public string IsExternal { get; set; }
        public List<PageMenuNewDTO> Children { get; set; }
    }
}

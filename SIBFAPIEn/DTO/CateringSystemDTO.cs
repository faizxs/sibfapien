﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{

    public class CateringSystemLoadDTO
    {
        public long ItemId { get; set; }
        public long? MemberId { get; set; }
        public long? ExhibitionId { get; set; }
        public string Status { get; set; }
        public long? WebsiteId { get; set; }
        public long? StaffguestId { get; set; }
        public string StaffguestName { get; set; }
        public string EventName { get; set; }
        public string EventType { get; set; }
        public string CateringType { get; set; }
        public string StartDate { get; set; }
        //public long? Duration { get; set; }
        public long? NoofDays { get; set; }
        public string StartTime { get; set; }
        public string StartTimeMeridiain { get; set; }
        public string EndTime { get; set; }
        public string EndTimeMeridiain { get; set; }
        public long? NoofTables { get; set; }
        public long? NoofChairs { get; set; }
        public long? NoofGuests { get; set; }
        public string ContactPerson { get; set; }
        public string ContactEmail { get; set; }
        public string ContactNumberISD { get; set; }
        public string ContactNumberSTD { get; set; }
        public string ContactNumber { get; set; }
        public string Notes { get; set; }
        public string CreatedOn { get; set; }
    }
    public class CateringSystemSaveDataDTO
    {
        public long ItemId { get; set; }
        public long? MemberId { get; set; }
        public string Status { get; set; }
        public long? WebsiteId { get; set; }
        public long? StaffguestId { get; set; }
        public string StaffguestName { get; set; }
        public string EventName { get; set; }
        public string EventType { get; set; }
        public string CateringType { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public long? Duration { get; set; }
        public long? NoofDays { get; set; }
        public string StartTime { get; set; }
        public string StartTimeMeridiain { get; set; }
        public string EndTime { get; set; }
        public string EndTimeMeridiain { get; set; }
        public long? NoofTables { get; set; }
        public long? NoofChairs { get; set; }
        public long? NoofGuests { get; set; }
        public string ContactPerson { get; set; }
        public string ContactEmail { get; set; }
        public string ContactNumberISD { get; set; }
        public string ContactNumberSTD { get; set; }
        public string ContactNumber { get; set; }
        public string Notes { get; set; }
        public string CreatedOn { get; set; }
    }
}

﻿using Entities.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    /// <summary>
    /// DTO for StaffGuest Form
    /// </summary>
    public class SaveStaffGuestDTO
    {
        public long StaffGuestId { get; set; }
        public long WebsiteId { get; set; }
        public long ExhibitionId { get; set; }
        public long MemberId { get; set; }
        public string IsUAEResident { get; set; }
        public string IsVisaNeed { get; set; }
        public long? MainGuestId { get; set; }
        public string MainGuest { get; set; }
        public string IsCompanion { get; set; }

        //public string ReturnURL { get; set; }
        //public string Message { get; set; }
        //public string MessageType { get; set; }
        public string NameEn { get; set; }
        public string LastName { get; set; }
        public string NameAr { get; set; }
        public string Profession { get; set; }
        public long NationalityId { get; set; }
        public string IsVisitedUae { get; set; }
        public string DateofBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Phone { get; set; }
        public string PhoneISD { get; set; }
        public string PhoneSTD { get; set; }
        public string Mobile { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }

        public string VisaNoOfMonths { get; set; }
        public long? GuestType { get; set; }
        public long? GuestCategory { get; set; }
        public string ActivityDate { get; set; }
        public string IsPaidVisa { get; set; }
        public string CompanionDescription { get; set; }

        public string PassportNumber { get; set; }
        public string PlaceOfIssue { get; set; }
        public string PassportDateOfIssue { get; set; }
        public string PassportExpiryDate { get; set; }
        public string FileBase64Passport { get; set; }
        public string FileBase64Passport2 { get; set; }
        public string FileBase64Visa { get; set; }
        public string FileBase64PersonalPhoto { get; set; }
        public string FileBase64CountryUID { get; set; }
        public string FileExtensionofFlightTickets { get; set; }
        public string FileExtensionofPassport { get; set; }
        public string FileExtensionofPassport2 { get; set; }
        public string FileExtensionVisa { get; set; }
        public string FileExtensionofPersonalPhoto { get; set; }
        public string FileExtensionofCountryUID { get; set; }
        public string FileBase64FlightTickets { get; set; }
        public bool IsEditClicked { get; set; }
        public string PassportHRef { get; set; }
        public string PassportCopy2Href { get; set; }
        public string PhotoHref { get; set; }
        public string VisaHref { get; set; }
        public string CountryUidnoHref { get; set; }
        public string IsFromPCRegistration { get; set; }
    }

    /// <summary>
    /// DTO for Tabs
    /// </summary>
    #region Tabs
    public class StaffGuestTabDTO
    {
        public long StaffGuestId { get; set; }
        public long ExhibitionId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Profession { get; set; }
        public string Passport { get; set; }
        public string Nationality { get; set; }
        public string Status { get; set; }
        public String Date { get; set; }
        public string IsTravelDetails { get; set; }
        public string IsUAEResident { get; set; }
        public string IsVisaNeed { get; set; }
        public long CountryId { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public bool IsParticipated { get; set; }
        public bool IsEditable { get; set; }
        public bool IsShowBooking { get; set; }
        public long? MainGuestId { get; set; }
        public string MainGuest { get; set; }
    }
    public class PCSAutoFillTabDTO
    {
        public long StaffGuestId { get; set; }
        public long MemberId { get; set; }
        public string NameEn { get; set; }
        public string LastName { get; set; }
        public string NameAr { get; set; }
        public long CountryId { get; set; }
        public bool IsShowForm { get; set; }
    }
    #endregion

    #region RemoveCommand
    public class RemoveCommandStaffGuestDTO
    {
        public long StaffGuestId { get; set; }
        public long ExhibitionId { get; set; }
        public long WebsiteId { get; set; }

        public string Commandname { get; set; }
    }
    #endregion
     
    public class TravelStaffGuestDTO
    {
        public long StaffGuestId { get; set; }
        public long ExhibitionId { get; set; }
        public long MemberId { get; set; }
        public long WebsiteId { get; set; }
        public long ArrivalAirportId { get; set; }
        public long ArrivalTerminalId { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
    }

    /// <summary>
    /// Upload Agreement
    /// </summary>
    public class UploadAgreementStaffGuestDTO
    {
        public long MemberId { get; set; }
        public long WebsiteId { get; set; }
        public string FileBase64UploadSignedagreement { get; set; }
        public string FileExtensionOfSignedagreement { get; set; }
        public string HiddenAgreement { get; set; }
        public string SignedAgreementHRef { get; set; }
        public bool AgreementThankyouVisibility { get; set; }
        public bool HrefAgreementVisibility { get; set; }
        public List<XsiExhibitionStaffGuestParticipating> StaffGuestPDFList { get; set; }
        public string FilePathForDownload { get; set; }
    }

    public class XsiStaffGuestNew
    {
        public long StaffGuestId { get; set; }
        public long ExhibitionId { get; set; }
        public string IsUAEResident { get; set; }
        public string NameEn { get; set; }
        public string LastName { get; set; }
        public string NameAr { get; set; }
        public string Profession { get; set; }
        public string Nationality { get; set; }
        public string DOB { get; set; }
        public string POB { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string PassportNumber { get; set; }
        public string POI { get; set; }
        public string DOI { get; set; }
        public string ExpiryDate { get; set; }
        public string PassportCopy { get; set; }
        public string PassportCopyTwo { get; set; }
        public string HealthInsurance { get; set; }
        public string HealthInsuranceTwo { get; set; }
        public string VisaNoOfMonths { get; set; }
        public long? GuestType { get; set; }
        public long? GuestCategory { get; set; }
        public string VisaType { get; set; }
        public string PersonalPhoto { get; set; }
        public string VisaApplicationForm { get; set; }
        public string FlightTicket { get; set; }
        public string ReturnFlightTicket { get; set; }
        public string VisaCopy { get; set; }
        public string Status { get; set; }
        public string ArrivalDate { get; set; }
        public string DepartureDate { get; set; }
        public string ArrivalAirportGroupId { get; set; }
        public string IsTravelDetails { get; set; }
        public string ExhibitorLastDate { get; set; }
        public string RemainingDays { get; set; }
        public string NotesForUser { get; set; }
        public bool IsVisaNeed { get; set; }
        public long? MainGuestId { get; set; }
        public string MainGuest { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class BooksParticipatingDTO
    {
        public long MemberId { get; set; }
        public List<LeftRightListDTO> LeftList { get; set; }
        public List<long> RightList { get; set; }
        public string ParticipateCount { get; set; }
        public string ParticipateMessage { get; set; }
        public string InventoryMessage { get; set; }
    }
    public class BooksToParticipateTabDTO
    {
        public List<LeftRightListDTO> list { get; set; }
        public long count { get; set; }
    }
    public class LeftRightListDTO
    {
        public long key { get; set; }
        public string title { get; set; }
    }
    public class RightListDTO
    {
        public long key { get; set; }
    }
    public class BooksParticipating
    {
        public long ItemId { get; set; }
        public string TitleEn { get; set; }
        public string Price { get; set; }
        public long AuthorId { get; set; }
        public long ExhibitionSubjectId { get; set; }
        public string Authors { get; set; }
        public string Subject { get; set; }
    }
    public class BooksInventoryList
    {
        public List<BooksInventoryDTO> List { get; set; }
        public long Count { get; set; }
        public string Downloadurl { get; set; }
    }
    public class BooksInventoryDTO
    {
        public long BookId { get; set; }
        public string Title { get; set; }
        public string BestSeller { get; set; }
        //public string BookType { get; set; }
        public string Price { get; set; }
        public string Thumbnail { get; set; }
        public string Author { get; set; }
    }
    public class BooksDetailsDTO
    {
        public long BookId { get; set; }
        public string Title { get; set; }
        public string AuthorName { get; set; }
        public string PublisherName { get; set; }
        public string Price { get; set; }
        public string Subject { get; set; }
        public string SubSubject { get; set; }
        public string Country { get; set; }
        public string HallNumber { get; set; }
        public string StandNumber { get; set; }
        public string BoothSection { get; set; }
        public string BoothSubSection { get; set; }
        public string Thumbnail { get; set; }
        public string Image { get; set; }
    }
}


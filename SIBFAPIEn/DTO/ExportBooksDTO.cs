﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class ExportBooksDTO
    {
        [Description("Title *")]
        public string Title { get; set; }
        [Description("Publisher *")]
        public string Publisher { get; set; }   
        [Description("Author *")]
        public string Author { get; set; }       
        [Description("MainSubject and Sub Subject *")]
        public string SubjectSubSubject { get; set; }
        [Description("Language *")]
        public string Language { get; set; }    
        [Description("IssueYear *")]
        public string IssueYear { get; set; }
        [Description("Price *")]
        public string Price { get; set; }
        [Description("Price Before Discount *")]
        public string PriceBeforeDiscount { get; set; }
        [Description("ISBN")]
        public string ISBN { get; set; }
        [Description("BookDetails")]
        public string BookDetails { get; set; }
        [Description("BestSeller")]
        public string BestSeller { get; set; }
    }

    public class ExportBooksArDTO
    {
        [Description("عنوان الكتاب * ")]
        public string Title { get; set; }

        [Description("دار نشر * ")]
        public string Publisher { get; set; }

        [Description("دار المؤلف *")]
        public string Author { get; set; }

        [Description("الفئة الرئيسية_الفئة الفرعية * ")]
        public string SubjectSubSubject { get; set; }

        [Description("اللغة * ")]
        public string Language { get; set; }

        [Description("سنة الإصدار")]
        public string IssueYear { get; set; }

        [Description("السعر *")]
        public string Price { get; set; }

        [Description("السعر قبل التخفيض *")]
        public string PriceBeforeDiscount { get; set; }

        [Description("الترقيم الدولي")]
        public string ISBN { get; set; }

        [Description("تفاصيل الكتاب")]
        public string BookDetails { get; set; }

        [Description("أفضل مبيعاً")]
        public string BestSeller { get; set; }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class AwardsListDTO
    {
        public long ItemId { get; set; }
        public string Thumbnail { get; set; }
        public string Name { get; set; }
    }

    public class AwardsDetailsDTO
    {
        public long ItemId { get; set; }
        public string Thumbnail { get; set; }
        public string Name { get; set; }
        public List<AwardMenuTabDto> AwardMenuList { get; set; }
    }
    public class AwardsItemDetailsDTO
    {
        public long ItemId { get; set; }
        public string Title { get; set; }
        public string Thumbnail { get; set; }
        public string Introduction { get; set; }
        public string Category { get; set; }
        public string Objectives { get; set; }
        public string Conditions { get; set; }
        public string Requirements { get; set; }
        public string URLTitle { get; set; }
        public string Url { get; set; }
    }
    public class AwardMenuTabDto
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public bool IsURL { get; set; }
    }
    public class ExhibitionYearDTO
    {
        public long ExhibitionId { get; set; }
        public string ExhibitionYear { get; set; }
    }
    public class AwardWinnerDto
    {
        public long ItemId { get; set; }
        public long AwardId { get; set; }
        public long SubAwardId { get; set; }
        public long ExhibitionId { get; set; }
        public string AwardTitle { get; set; }
        public string PublisherName { get; set; }
        public string Overview { get; set; }
        public string PublisherThumbnail { get; set; }
        public string BookTitle { get; set; }
        public string BookThumbnail { get; set; }
        public string BookOverview { get; set; }
    }
    public class TranslationAwardWinnerDto
    {
        public long ItemId { get; set; }
        public long AwardId { get; set; }
        public long ExhibitionId { get; set; }
        public string AwardTitle { get; set; }
        public string TranslatorName { get; set; }
        public string BookTitle { get; set; }
        public string BookThumbnail { get; set; }
    }
    public class PCAwardWinnerDto
    {
        public long ItemId { get; set; }
        public long AwardId { get; set; }
        public long AwardDetailId { get; set; }
        public string AwardTitle { get; set; }
        public string ParticipantName { get; set; }
        public string Overview { get; set; }
        public string Testimonial { get; set; }
        public string Thumbnail { get; set; }
    }
}

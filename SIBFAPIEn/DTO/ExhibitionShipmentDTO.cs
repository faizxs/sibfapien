﻿using System;
using System.Collections.Generic;

namespace SIBFAPIEn.DTO
{
    public class ExhibitionShipmentListDTO
    {
        public List<ExhibitionShipmentItemDTO> ExhibitionShipmentList { get; set; }
        public long ExhibitorId { get; set; }
        public long? MemberId { get; set; }
    }
    public class UserInfoItemDTO
    {
        public long ExhibitorId { get; set; }
        public string ExhibitorName { get; set; }
        public long AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string IsAgency { get; set; }
        public string FileNumber { get; set; }
        public string CountryName { get; set; }
        public List<DropdownDataDTO> AgencyList { get; set; }
    }
    public class ExhibitionShipmentItemDTO
    {
        public long ItemId { get; set; }
        public string ExhibitorName { get; set; }
        public string FileNumber { get; set; }
        public string CountryName { get; set; }
        public string IsAgency { get; set; }
        public List<DropdownDataDTO> AgencyList { get; set; }
        public Nullable<long> ShipmentCount { get; set; }
        public string ShipmentTypeName { get; set; }
        public string ShipmentCompanyName { get; set; }
        public string BillofLading { get; set; }
        public string ShipmentNumber { get; set; }
        public string ShipmentCountryNameFrom { get; set; }
        public string ShipmentCountryNameTo { get; set; }
        public string ReceivingCompanyName { get; set; }
        public string ReceivedShipmentLocationTypeName { get; set; }
        public string QrfileName { get; set; }
        public Nullable<long> NoofBooks { get; set; }
        public Nullable<long> CategoryId { get; set; }
        public string BookCategoryName { get; set; }
        public string BookFileName { get; set; }
        public string Status { get; set; }
        public string IsLocalWarehouse { get; set; }
        public long? WShipmentCount { get; set; }
        public string WShipmentCompanyName { get; set; }
        public string WShipmentNumber { get; set; }
        public Nullable<long> WNoofBooks { get; set; }
        public string WBookFileName { get; set; }
    }

    public class ShipmentSaveDataDTO
    {
        public long? ExhibitorId { get; set; }
        public long? WebsiteId { get; set; }
        public string IsAgency { get; set; }
        public List<long> AgencyIds { get; set; }
        public long? ShipmentCount { get; set; }
        public long? ShipmentType { get; set; }
        public string ShipmentCompanyName { get; set; }
        public string BillofLading { get; set; }
        public string ShipmentNumber { get; set; }
        public long? ShipmentCountryFrom { get; set; }
        public long? ShipmentCountryTo { get; set; }
        public string ReceivingCompanyName { get; set; }
        public long? ReceivedShipmentLocationType { get; set; }
        public Nullable<long> NoofBooks { get; set; }
        public string IsLocalWarehouse { get; set; }

        public long? CategoryId { get; set; }

        public string FileBase64BookFile { get; set; }
        public string FileExtensionofBookFile { get; set; }

        public long? WShipmentCount { get; set; }
        public string WShipmentCompanyName { get; set; }
        public string WShipmentNumber { get; set; }
        public Nullable<long> WNoofBooks { get; set; }
        public string WFileBase64BookFile { get; set; }
        public string WFileExtensionofBookFile { get; set; }
    }
}


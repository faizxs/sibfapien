﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class PPBookInfoDTO
    {
        public long ItemId { get; set; }
        public Nullable<long> MemberId { get; set; }
        public Nullable<long> ProfessionalProgramId { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string FileName1 { get; set; }
        public string BooksURL { get; set; }
        public string Image { get; set; }
        public List<MyBooks> BooksList { get; set; }
    }
}

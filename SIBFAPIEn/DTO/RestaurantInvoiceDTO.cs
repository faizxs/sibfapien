﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class RestaurantInvoiceDTO
    {
        public long ItemId { get; set; }
        public Nullable<long> RestaurantId { get; set; }
        public string InvoiceNumber { get; set; }
        public string FileName { get; set; }
        public string AllocatedSpace { get; set; }
        public string ParticipationFee { get; set; }
        public string StandFee { get; set; }
        public string KnowledgeAndResearchFee { get; set; }
        public string AgencyFee { get; set; }
        public string RepresentativeFee { get; set; }
        public string DueFromLastYearCredit { get; set; }
        public string DueFromLastYearDebit { get; set; }
        public string DueCurrentYear { get; set; }
        public string DebitedCurrentYear { get; set; }
        public string Discount { get; set; }
        public string DiscountNote { get; set; }
        public string Penalty { get; set; }
        public string TotalAED { get; set; }
        public string TotalUSD { get; set; }
        public string Status { get; set; }
        public string IsActive { get; set; }
        public string IsChequeReceived { get; set; }
        public string IsNewPending { get; set; }
        public string UploadePaymentReceipt { get; set; }
        public Nullable<long> PaymentTypeId { get; set; }
        public string IsStatusChanged { get; set; }
        public string Notes { get; set; }
        public string ExemptionType { get; set; }
        public string IsRegistrationInvoice { get; set; }
        public string DiscountPercent { get; set; }
        public string PenaltyPercent { get; set; }
        public string RemainingAgencies { get; set; }
        public string RemainingRepresentatives { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> CreatedById { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<long> ModifiedById { get; set; }
        public string PaidAmount { get; set; }
        public string BalanceAmount { get; set; }
        public string PaymentLastDate { get; set; }
        public string PaymentRemainingDays { get; set; }
    }

    public class RestaurantUploadPayment
    {
        public long RestaurantId { get; set; }
        public long WebsiteId { get; set; }
        public long MemberId { get; set; }
        public long InvoiceId { get; set; }
        public long PaymentTypeId { get; set; }
        public string FileBase64 { get; set; }
        public string FileExtension { get; set; }
        public long LanguageId { get; set; }
        public string BankName { get; set; }
        public DateTime? TransferDate { get; set; }
        public string ReceiptNumber { get; set; }
        public string TRNNumber { get; set; }
        public string ContractUserFileBase64 { get; set; }
        public string ContractUserFileExtension { get; set; }
    }

    public class RestaurantCancelRegistrationDTO
    {
        public long RestaurantId { get; set; }
        public long WebsiteId { get; set; }
        public string CancellationReason { get; set; }
    }
}

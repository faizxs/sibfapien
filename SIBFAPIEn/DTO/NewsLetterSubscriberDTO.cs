﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class NewsLetterSubscriberDTO
    {
        public Nullable<long> CountryId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string IsSubscribe { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class ProfessionalProfileDTO
    {
        public long ItemId { get; set; }
        public Nullable<long> MemberId { get; set; }
        public Nullable<long> ProfessionalProgramId { get; set; }
        public Nullable<long> TableId { get; set; }
        public long PPSlotItemID { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string CompanyNameAr { get; set; }
        public string JobTitle { get; set; }
        public string JobTitleAr { get; set; }
        public string WorkProfile { get; set; }
        public string WorkProfileAr { get; set; }
        public string Website { get; set; }
        public string TradeType { get; set; }
        public string TradeTypeName { get; set; }
        public string IsEbooks { get; set; }
        public string IsInterested { get; set; }
        public string ExamplesOfTranslatedBooks { get; set; }
        public string UnSoldRightsBooks { get; set; }
        public string IsAlreadyBoughtRights { get; set; }
        public string KeyPublisherTitlesOrBusinessRegion { get; set; }
        public string MainTerritories { get; set; }
        public string FileName1 { get; set; }
        public string BooksURL { get; set; }
        public string GenreTags { get; set; }
        public string Image { get; set; }
    }
    public class MessageOtherProfileDTO
    {
        public string Message { get; set; }
        public ProfessionalProfileDTO ProfessionalProfileDTO { get; set; }
        public SlotsDTO SlotsList { get; set; }
        public string MessageTypeResponse { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class NewsDetailsPGDTO
    {
        public string src { get; set; }
        public string linkUrl { get; set; }
        public long? SortIndex { get; set; }
    }
  
}

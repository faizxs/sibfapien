﻿using Microsoft.AspNetCore.Http;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class PaymentDTO
    {
        public List<PendingPaymentDTO> PendingList { get; set; }
        public List<PaidPaymentDTO> PaidList { get; set; }
        public int PendingCount { get; set; }
        public int PaidCount { get; set; }
        public long? CountryId { get; set; }
        // OverView Content
        public long TotalAllocatedSpace { get; set; }
        public string TotalParticipationFees { get; set; }
        public string TotalStandFee { get; set; }
        public string DueFromLastYearDebit { get; set; }
        public string DueFromLastYearCredit { get; set; }
        public string DueCurrentYear { get; set; }
        public string DebitedCurrentYear { get; set; }
        public string TotalAgencyFee { get; set; }
        public string TotalRepresentativeFee { get; set; }
        public double TotalDiscount { get; set; }
        public double TotalPenalty { get; set; }
        public double TotalAED { get; set; }
        public double TotalUSD { get; set; }
        public double TotalPaid { get; set; }
        public double TotalBalance { get; set; }
        public long Lang { get; set; }

        public string ReturnURL { get; set; }
        public string Message { get; set; }
        public string Message1 { get; set; }
    }
    public class PendingPaymentDTO
    {
        public long ItemId { get; set; }
        public string InvoiceNumber { get; set; }
        public string IsActive { get; set; }
        public long? ExhibitorId { get; set; }
        public string TotalAed { get; set; }
        public string CreatedOn { get; set; }
        public string IsNewPending { get; set; }
        public string Status { get; set; }
        public string IsRegistrationInvoice { get; set; }
    }
    public class PaidPaymentDTO
    {
        public long ItemId { get; set; }
        public string InvoiceNumber { get; set; }
        public string IsActive { get; set; }
        public long? ExhibitorId { get; set; }
        public string TotalAed { get; set; }
        public string CreatedOn { get; set; }
        public string IsNewPending { get; set; }
        public string Status { get; set; }
        public string PaymentType { get; set; }
        public string IsRegistrationInvoice { get; set; }
    }

    public class PaymentAcknowledgementDTO
    {
        public long ExhibitorId { get; set; }
        public string ContentTitle { get; set; }
        public string ContentSubtitle { get; set; }
        public string Content { get; set; }
        public string DocumentURL1 { get; set; }
        public string DocumentOneName { get; set; }
        public string DocumentURL2 { get; set; }
        public string DocumentTwoName { get; set; }
    }
    public class PaymentAcknowledgementFormDTO
    {
        public long MemberId { get; set; }
        public long ExhibitorId { get; set; }
        public string DocumentURL1Base64 { get; set; }
        public string DocumentURL1Ext { get; set; }
        public string DocumentURL2Base64 { get; set; }
        public string DocumentURL2Ext { get; set; }
    }
    public class InvoiceRootDto
    {
        public List<InvoiceInquiryDto> Table;
    }

    public class InvoiceInquiryDto
    {
        public string ExhibitionName;
        public int PublisherID;
        public string PublisherName;
        public string GovernmentEntity;
        public string InvoiceNo;
        public DateTime InvoiceDate;
        public double InvoiceAmount;
        // public string InvoiceStatus;
        public string Status;
    }
    public class InquiryItem
    {
        public string InvoiceNo;
        public string InvoiceDate;
        public double InvoiceAmount;
        public string InvoiceStatus;
        public string Status;
    }
}

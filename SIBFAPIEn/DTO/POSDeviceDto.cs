﻿using Entities.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace SIBFAPIEn.DTO
{
    public class POSDeviceDto
    {
        public long RegistrationId { get; set; }
        public long? LanguageUrl { get; set; }
        public string IsLocal { get; set; }
        public long? PosdeviceQauntity { get; set; }
        public long? ExhibitorId { get; set; }
        public string IdentificationNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FirstNameAr { get; set; }
        public string LastNameAr { get; set; }
        public long? RecipientNationalityId { get; set; }
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public long? PublisherOriginCountryId { get; set; }
        public string EmiratedIdnumber { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }
        public string MobileNumber { get; set; }
        public string PublishingHouseEmail { get; set; }
        public string BankNameInsideCountry { get; set; }
        public string AccountHolderName { get; set; }
        public string Ibannumber { get; set; }
        public string AdressPublishingHouseBankAccount { get; set; }
        public string PassportNumber { get; set; }
        public string PassportCopy { get; set; }
    }
    public class AddorUpdatePOSDeviceDto
    {
        public long RegistrationId { get; set; }
        public long? LanguageUrl { get; set; }
        public string IsLocal { get; set; }
        [Required]
        public long? PosdeviceQauntity { get; set; }
        [Required]
        public long? ExhibitorId { get; set; }
        //public string IdentificationNumber { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FirstNameAr { get; set; }
        public string LastNameAr { get; set; }
        public long? RecipientNationalityId { get; set; }
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public long? PublisherOriginCountryId { get; set; }
        public string EmiratedIdnumber { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }
        public string MobileNumber { get; set; }
        [EmailAddress]
        public string PublishingHouseEmail { get; set; }
        public string BankNameInsideCountry { get; set; }
        public string AccountHolderName { get; set; }
        public string Ibannumber { get; set; }
        public string AdressPublishingHouseBankAccount { get; set; }
        public string PassportNumber { get; set; }
        public string FileBase64Passport { get; set; }
        public string FileExtensionofPassport { get; set; }
    }
}

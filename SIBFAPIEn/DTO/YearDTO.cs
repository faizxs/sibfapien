﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class YearDTO
    {
        public YearDTO()
        {
        }
        public Int32 Id { get; set; }
        public Int32 Year { get; set; }
    }
}

﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class ExhibitorRegistrationDTO
    {
        public long ExhibitorId { get; set; }
        public long ExhibitionId { get; set; }
        public long? MemberId { get; set; }
        public long? WebsiteId { get; set; }
        public long? LanguageUrl { get; set; }
        public long? StepId { get; set; }
        public bool IsEditRequest { get; set; }
        public bool IsBooksRequired { get; set; }
        public string Status { get; set; }
        public string IsNotesRead { get; set; }
        public string Message { get; set; }
        public string DueMessage { get; set; }
        public StepTwoDTO StepTwoDTO { get; set; }
        public StepThreeDTO StepThreeDTO { get; set; }
        public StepFourDTO StepFourDTO { get; set; }
        public StepFiveDTO StepFiveDTO { get; set; }
        public StepSixDTO StepSixDTO { get; set; }
    }
    public class StepTwoDTO
    {
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public long CountryId { get; set; }
        public long CityId { get; set; }
        public string OtherCity { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonNameAr { get; set; }
        public string ContactPersonTitle { get; set; }
        public string ContactPersonTitleAr { get; set; }
        public string PhoneISD { get; set; }
        public string PhoneSTD { get; set; }
        public string Phone { get; set; }
        public string FaxISD { get; set; }
        public string FaxSTD { get; set; }
        public string Fax { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public long ExhibitionCategoryId { get; set; }
        public List<long> ActivityIds { get; set; }
        // public long ActivityIds { get; set; }
        public string TradeLicenceName { get; set; }
        public string LogoName { get; set; }
        public string BooksExcelFileName { get; set; }
        public string APU { get; set; }
        public string Brief { get; set; }
        public string BriefAr { get; set; }
        public string IsBooksUpload { get; set; }
    }
    public class StepThreeDTO
    {
        public string CurrentEditRequest { get; set; }
        public string TotalNumberOfNewTitles { get; set; }
        public string TotalNumberOfTitles { get; set; }
        public long BoothSectionId { get; set; }
        public long BoothSubSectionId { get; set; }
        public string BoothSubSectionTitle { get; set; }
        public string RequiredAreaShapeType { get; set; }
        public long RequiredAreaId { get; set; }

        //For Restaurant
        public string RequiredArea { get; set; }
        public string CalculatedArea { get; set; }
        public string RequiredAreaMeter { get; set; }
        public string RequiredAreaValue { get; set; }
        public string BoothDetail { get; set; }

    }

    public class PostStepFourDTO
    {
        //public long WebsiteId { get; set; }
        //public long MemberId { get; set; }
        //public long ExhibitorId { get; set; }
        //public long InvoiceId { get; set; }
        //public string TRNNumber { get; set; }
        //public string ReceiptFile { get; set; }
        //public string ReceiptExt { get; set; }
        //public string BankName { get; set; }
        //public string ReceiptNumber { get; set; }
        //public DateTime? TransferDate { get; set; }
        //public long PaymentTypeId { get; set; }
    }
    public class StepFiveDTO
    {
        public long WebsiteId { get; set; }
        public long MemberId { get; set; }
        public long ExhibitorId { get; set; }
        public long InvoiceId { get; set; }
        public string TRNNumber { get; set; }
        public string ReceiptFile { get; set; }
        public string ReceiptExt { get; set; }
        public string BankName { get; set; }
        public string ReceiptNumber { get; set; }
        public DateTime? TransferDate { get; set; }
        public long PaymentTypeId { get; set; }
    }
    public class StepSixDTO
    {
        public string Section { get; set; }
        public long BoothSectionId { get; set; }
        public string SubSection { get; set; }
        // public string AllocatedSpace { get; set; }
        public string Area { get; set; }
        public string HallNumber { get; set; }
        public string Stand { get; set; }
        public string UploadLocationMap { get; set; }
    }
    public class StepFourDTO
    {
        public long InvoiceId { get; set; }
        public string AllocatedSpace { get; set; }
        public string InvoiceNumber { get; set; }
        public string ParticipationFee { get; set; }
        public string StandFee { get; set; }
        public string KnowledgeAndResearchFee { get; set; }
        public string AgencyFee { get; set; }
        public string RepresentativeFee { get; set; }
        public string DueFromLastYearCredit { get; set; }
        public string DueFromLastYearDebit { get; set; }
        public string DueCurrentYear { get; set; }
        public string DebitedCurrentYear { get; set; }
        public string Discount { get; set; }
        public string Penalty { get; set; }
        public string VAT { get; set; }
        public string TotalAED { get; set; }
        public string TotalUSD { get; set; }
        public string FileName { get; set; }
        public string ExhibitorLastDate { get; set; }
        public string RemainingDays { get; set; }
    }
    //public class ExhibitorBoothInfo
    //{

    //}

    //public class StepTwoDTO
    //{
    //    public long WebsiteId { get; set; }
    //    public long MemberId { get; set; }
    //    public long ExhibitorId { get; set; }
    //    public long ExhibitionCategoryId { get; set; }
    //    public string TradeLicence { get; set; }
    //    public string TradeLicenceExt { get; set; }
    //    public string Logo { get; set; }
    //    public string LogoExt { get; set; }
    //    public bool IsStep2EditClicked { get; set; }
    //}
    public class SaveDataForStepTwoAndThreeDTO
    {
        public long WebsiteId { get; set; }
        public long ExhibitionId { get; set; }
        public long MemberId { get; set; }
        public long ExhibitorId { get; set; }
        public long CountryId { get; set; }
        public long CityId { get; set; }
        public string OtherCity { get; set; }
        public long ExhibitionCategoryId { get; set; }
        [Required]
        public string PublisherNameEn { get; set; }

        public string PublisherNameAr { get; set; }
        [Required]
        public string ContactPersonName { get; set; }

        public string ContactPersonNameAr { get; set; }
        public string ContactPersonTitle { get; set; }
        public string ContactPersonTitleAr { get; set; }
        public string IsFirstTime { get; set; }
        [Required]
        public string PhoneISD { get; set; }
        [Required]
        public string PhoneSTD { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string MobileISD { get; set; }
        [Required]
        public string MobileSTD { get; set; }
        [Required]
        public string Mobile { get; set; }
        public string FaxISD { get; set; }
        public string FaxSTD { get; set; }
        public string Fax { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string TotalNumberOfTitles { get; set; }
        [Required]
        public string TotalNumberOfNewTitles { get; set; }
        [Required]
        public long BoothSectionId { get; set; }
        [Required]
        public long BoothSubSectionId { get; set; }
        public string APU { get; set; }
        [Required]
        public string RequiredAreaShapeType { get; set; }
        public string CalculatedArea { get; set; }
        public long RequiredAreaId { get; set; }
        public long RequiredAreaValue { get; set; }
        public string RequiredArea { get; set; }
        public string RequiredAreaMeter { get; set; }
        public string TradeLicenceName { get; set; }
        public string TradeLicenceBase64 { get; set; }
        public string TradeLicenceExt { get; set; }
        public string LogoName { get; set; }
        public string LogoBase64 { get; set; }
        public string LogoExt { get; set; }
        [Required]
        public List<long> ActivityIds { get; set; }
        [Required]
        public string Brief { get; set; }
        public string BriefAr { get; set; }
        public bool IsEditRequest { get; set; }
        public string BoothDetail { get; set; }
        public string IsBooksUpload { get; set; }
        public string excelFileBase64 { get; set; }
        public string BookMessageResponse { get; set; }
    }

    public class ExhibitorReturnDTO
    {
        public long ExhibitorId { get; set; }
        public long? MemberId { get; set; }
        public long? WebsiteId { get; set; }
        public long? StepId { get; set; }
        public string Message { get; set; }
        public string DueMessage { get; set; }
        public string MessageTypeResponse { get; set; }
        public ExhibitorRegistrationDTO ExhibitorResponseDTO { get; set; }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class AwardNominationFormNewDTO
    {
        public string hdnTempData { get; set; }
        //public long? ExhibitionId { get; set; }
        public long? AwardId { get; set; }
        public long? SubAwardId { get; set; }
        public string Title { get; set; }
        public string PublishedYear { get; set; }
        public string ISBN { get; set; }
       // public string NominationType { get; set; }
        public string AuthorName { get; set; }
        public string AuthorISD { get; set; }
        public string AuthorSTD { get; set; }
        public string AuthorPhone { get; set; }
        public string AuthorEmail { get; set; }
        public long? AuthorNationalityId { get; set; }
        public string Publisher { get; set; }
        public long? PublisherCountryId { get; set; }
        public string PublisherEmail { get; set; }
        public string PublisherISD { get; set; }
        public string PublisherSTD { get; set; }
        public string PublisherPhone { get; set; }
        // public string PublishersSpecialization { get; set; }
        //public string Comments { get; set; }
        public string BookCoverFileBase64 { get; set; }
        public string BookCoverDocExtension { get; set; }
        public string FileBase64 { get; set; }
        public string DocExtension { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{

    public class CareerDetailsDTO
    {
        public long ItemId { get; set; }
        public long GroupId { get; set; }
        public string JobTitle;
        public string DepartmentTitle;
        public string PostingDate;
        public string JobCode;
        public string Description;
        public string Responsibilities;
        public string Experience;
        public string Degree;
        public string Overview;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class VolunteerRegistrationDTO
    {
        public Nullable<long> VolunteerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FirstNameAr { get; set; }
        public string LastNameAr { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string Mobile { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }
        public string Email { get; set; }
        public string Organization { get; set; }
        public string Other { get; set; }
        public string IsUID { get; set; }
        public string UIDorOtherUniversity { get; set; }
        public List<long> Period { get; set; }
        public List<long> ExhibitionDaysMorningShift { get; set; }
        public List<long> ExhibitionDaysEveningShift { get; set; }
        public string MorningShiftNoofDays { get; set; }
        public string EveningShiftNoofDays { get; set; }
        public string FullShiftNoofDays { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ClaimedNoofHours { get; set; }
        public string PreviousVolunteerExperience { get; set; }
        public string OtherNotes { get; set; }
        public string FileBase64Passport { get; set; }
        public string FileExtensionofPassport { get; set; }
        public string EmiratesIDBase64 { get; set; }
        public string EmiratesIDExt { get; set; }
        public string EmiratesIDTwoBase64 { get; set; }
        public string EmiratesIDTwoExt { get; set; }
        public string CVBase64 { get; set; }
        public string CVBaseExt { get; set; }
        public string SecurityDocumentBase64 { get; set; }
        public string SecurityDocumentBaseExt { get; set; }
    }
  
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class BooksAddDTO
    {
        public long BookId { get; set; }
        public long? MemberId { get; set; }
        public long? ExhibitorId { get; set; }
        public long? AgencyId { get; set; }
        public string TitleEn { get; set; }
        public string TitleAr { get; set; }
        public string BookPublisher { get; set; }
       // public long BookPublisherId { get; set; }
        public string BookAuthor { get; set; }
       // public long BookAuthorId { get; set; }
        public string IsNew { get; set; }
        public long? ExhibitionSubjectId { get; set; }
        public long? ExhibitionSubsubjectId { get; set; }
        public long? BookLanguageId { get; set; }
        public long? BookTypeId { get; set; }
        public string IssueYear { get; set; }
        public string Price { get; set; }
        public string PriceBeforeDiscount { get; set; }
        public string Isbn { get; set; }
        public long? ExhibitionCurrencyId { get; set; }
        public string IsAvailableOnline { get; set; }
        public string BookDetails { get; set; }
        public string BookDetailsAr { get; set; }
        public string IsBestSeller { get; set; }
        public string ThumbnailFileBase64 { get; set; }
        public string ThumbnailFileExtension { get; set; }
        public long WebsiteId { get; set; }
        public bool IsBookSave { get; set; }
    }

    public class PublisherDTO
    {
        public long MemberId { get; set; }
        public string PublisherEn { get; set; }
        public string PublisherAr { get; set; }
    }

    public class ImportExcelDTO
    {
        public long MemberId { get; set; }
        public long WebsiteId { get; set; }
        public string excelFileBase64 { get; set; }
    }
    public class AuthorPostDTO
    {
        public long MemberId { get; set; }
        public string AuthorEn { get; set; }
        public string AuthorAr { get; set; }
    }

    public partial class XsiBookComplete
    {
        public long ItemId { get; set; }
        public Nullable<long> AuthorId { get; set; }
        public Nullable<long> PublisherId { get; set; }
        public Nullable<long> ExhibitionId { get; set; }
        public Nullable<long> BookLanguageId { get; set; }
        public Nullable<long> ExhibitionSubjectId { get; set; }
        public Nullable<long> ExhibitionSubsubjectId { get; set; }
        public Nullable<long> ExhibitionCurrencyId { get; set; }
        public Nullable<long> BookTypeId { get; set; }
        public string Thumbnail { get; set; }
        public string IsAvailableOnline { get; set; }
        public string IsNew { get; set; }
        public string IsBestSeller { get; set; }
        public string TitleEn { get; set; }
        public string TitleAr { get; set; }
        public string ISBN { get; set; }
        public string IssueYear { get; set; }
        public string Price { get; set; }
        public string PriceBeforeDiscount { get; set; }
        public string BookDetails { get; set; }
        public string BookDetailsAr { get; set; }
        public string PublisherName { get; set; }
        public string AuthorName { get; set; }
        public string IsParticipating { get; set; }
        public Nullable<long> ExhibitorId { get; set; }
        public Nullable<long> AgencyId { get; set; }
    }
}


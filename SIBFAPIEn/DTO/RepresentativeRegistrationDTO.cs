﻿using Entities.Models;
using System;
using System.Collections.Generic;

namespace SIBFAPIEn.DTO
{
    /// <summary>
    /// Tab1 Add Represntative
    /// </summary>
    public class RepresentativeRegistrationDTO
    {
        public long RepresentativeId { get; set; }
        public long MemberExhibitionYearlyId { get; set; }
        public long? MemberId { get; set; }
        public long? ExhibitionId { get; set; }
        public long? WebsiteId { get; set; }


        public int ExpiryMonths { get; set; }
        public DateTime AgencyStartDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime PenaltyDate { get; set; }

        public DateTime ExhibitorEndDate { get; set; }

        public string TotalParticipating { get; set; }
        public string TotalCancelled { get; set; }
        public string VisaNoteHref { get; set; }

        public string StatusMessage { get; set; }

        public string PariticipateCounter { get; set; }
        //public string PariticipateCounter { get; set; }
        //public string PariticipateCounter { get; set; }
        //public string PariticipateCounter { get; set; }
        //public string PariticipateCounter { get; set; }

        public string RemainingDays { get; set; }
        public string ExhibitorLastDate { get; set; }
        public string ClearHref { get; set; }
        public string RedirectUrl { get; set; }
        public int TypeofMessage { get; set; }

        public bool IsRegisterClicked { get; set; }
        public bool IsVisaRequired { get; set; }
        public bool VisaPanelVisibility { get; set; }
        public bool Tab1ContentVisibility { get; set; }
        public bool VisaNoteVisibility { get; set; }
        public bool ListingArchiveVisibility { get; set; }
        public bool PanelErrMsg2Visibility { get; set; }
        public bool PariticipateCounterSpanVisibility { get; set; }
        public bool ParticipatingGridVisibility { get; set; }
        public bool ParticipatingMsgPanelVisibility { get; set; }
        public bool IsEditClicked { get; set; }
        public bool IsReregisterClicked { get; set; }
        public bool VisaFormHrefVisibility { get; set; }
        public bool PersonalPhotoRepHRefVisibility { get; set; }
        public bool PassportCopy2RepHRefVisibility { get; set; }

        public bool IsPanelVisible { get; set; }
        public string CSSAttributes { get; set; }
        public string HiddenAgreementValue { get; set; }
        public PendingDTO PendingDTO { get; set; }
        public ParticipatingDTO ParticipatingDTO { get; set; }
        public ArchiveDTO ArchiveDTO { get; set; }

        public string FileExtensionVisa { get; set; }
        public string FileExtensionofPassport { get; set; }
        public string FileExtensionofPassport2 { get; set; }
        public string FileExtensionofPersonalPhoto { get; set; }
        public string FileExtensionofCountryUID { get; set; }
        public string FileExtensionofFlightTickets { get; set; }

        public string FileBase64Passport { get; set; }
        public string FileBase64Passport2 { get; set; }
        public string FileBase64PersonalPhoto { get; set; }
        public string FileBase64Visa { get; set; }
        public string FileBase64CountryUID { get; set; }
        public string FileBase64FlightTickets { get; set; }
        public string Profession { get; set; }
        public long CountryId { get; set; }
        public string CountryUidno { get; set; }
        public string CountryUidnoHref { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string PlaceofBirth { get; set; }
        public string DOB { get; set; }
        public bool IsVisaNeed { get; set; }
        public string IsVisitedUae { get; set; }
        public string PassportNo { get; set; }
        public string PlaceOfIssue { get; set; }
        public string DateOfIssue { get; set; }

        public string Passport { get; set; }
        public string ExpiryDate { get; set; }
        public string PassportRepHRef { get; set; }
        public bool PassportRepHRefVisibility { get; set; }
        public string PassportCopy2Href { get; set; }
        public bool PassportCopy2HrefVisibility { get; set; }
        public bool PnlMsgVisibility { get; set; }

        public string VisaFormHref { get; set; }
        public string PhotoHref { get; set; }
        public bool PhotoHrefVisibility { get; set; }
        public string VisaHref { get; set; }
        public bool VisaHrefVisibility { get; set; }
        public string ThankYouNStatusMsg { get; set; }
        public string FlightTicketsHref { get; set; }
        public bool FlightTicketsHrefVisibility { get; set; }
        public bool CountryUidDivVisibility { get; set; }
        public bool CountryUIDNoHrefVisibility { get; set; }

        public string Phone { get; set; }
        public string PhoneISD { get; set; }
        public string PhoneSTD { get; set; }

        public string Mobile { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }

        public string Fax { get; set; }
        public string FaxISD { get; set; }
        public string FaxSTD { get; set; }
        public List<XsiExhibitionRepresentativeParticipating> RepresentativeListOverview { get; set; }
        public List<XsiExhibitionRepresentativeParticipating> RepresentativeListParticipating { get; set; }

        /// <summary>
        /// store it in session for later use UI side
        /// </summary>
        public List<XsiExhibitionRepresentativeParticipating> RepresentativePDFList { get; set; }
        public string DownloadableFilename { get; set; }

    }
    /// <summary>
    /// Form Representative DTO
    /// </summary>
    public class FormRepresentativeDTO
    {
        public string PlaceofBirth { get; set; }
        public string DOB { get; set; }

    }
    /// <summary>
    /// Form RepresentativeTravel
    /// </summary>
    public class RepresentativeTravelDTO
    {
        public long RepresentativeId { get; set; }
        public long MemberId { get; set; }
        public long WebsiteId { get; set; }
        public long ArrivalAirportId { get; set; }
        public long ArrivalTerminalId { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
    }
   

    /// <summary>
    /// Tab2
    /// </summary>
    public class PendingDTO
    {
        public string Message { get; set; }
        public string SpanCounter { get; set; }
        public string SignedAgreementHRef { get; set; }
        public bool PanelErrMsg1Visibility { get; set; }
        public bool SpanCounterVisibility { get; set; }
        public bool GridVisibility { get; set; }
        public bool AgreementDivVisibility { get; set; }
        public bool SignedAgreementHRefVisibility { get; set; }
        public List<XsiExhibitionRepresentativeParticipating> RepresentativeListPending { get; set; }

    }

    /// <summary>
    /// Tab4
    /// </summary>
    public class ParticipatingDTO
    {

    }
    /// <summary>
    /// Tab3
    /// </summary>
    public class ArchiveDTO
    {
        public long? MemberId { get; set; }
        public string IsActive { get; set; }
    }

}


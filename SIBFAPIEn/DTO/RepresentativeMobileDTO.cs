﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class RepresentativeMobileDTO
    {
        [JsonProperty(PropertyName = "id")]
        public long RepresentativeId { get; set; }
        public long ExhibitorId { get; set; } 
        public long Status { get; set; }
        //public long StatusType { get; set; }
        public string Date { get; set; } 

        [JsonProperty(PropertyName = "data")]
        public dynamic Data { get; set; }


        //(string first, string middle, string last) LookupName { get; set; }
    }
   public class MyTuple : Tuple<string, string, string>
    {
        public MyTuple(string Name, string Status, string Passport)
            : base(Name, Status, Passport)
        {

        }

        public string Name { get { return this.Item1; } }
        public string Status { get { return this.Item2; } }

        [JsonProperty(PropertyName = "Passport #")]
        public string Passport { get { return this.Item3; } }

    }
    public class RepresentativeDataMobileDTO
    {
        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "Status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "Passport #")]
        public string Passport { get; set; }
    }
    public class RepresentativeDataMobileArDTO
    {
        [JsonProperty(PropertyName = "اسم")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "حالة")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "رقم جواز السفر")]
        public string Passport { get; set; }
    }
    public class RepresentativeDetailsMobileDTO
    {
        public long ItemId { get; set; }
        [JsonProperty(PropertyName = "exhibitorid")]
        public long MemberExhibitionYearlyId { get; set; }
        public long MemberId { get; set; }
        public string NameEnglish { get; set; }
        public string NameArabic { get; set; }
        public string Profession { get; set; }
        public string Nationality { get; set; }
        public string DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string VisaCopy { get; set; }
    }

}


﻿using Entities.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class FurnitureItemDTO
    {
        public long FurnitureId { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public string Thumbnail { get; set; }
        public decimal? Price { get; set; }
    }
    public class FurnitureItemOrderDTO
    {
        public long MemberId { get; set; }
        public long ExhbitorId { get; set; }
        public decimal? Subtotal { get; set; }
        public List<FurnitureItemPostDTO> OrderItems { get; set; }
    }
    public class FurnitureItemPostDTO
    {
        public long FurnitureId { get; set; }
        public decimal Price { get; set; }
        public long Quantity { get; set; }
        public decimal ItemTotal { get; set; }
    }
}

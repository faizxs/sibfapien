﻿using Microsoft.AspNetCore.Http;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class FindProfessionalDTO
    {
        public List<FindProfessional> SuggestionList { get; set; }
        public List<FindProfessional> MainList { get; set; }
        public string Message { get; set; }
        public string MessageTypeResponse { get; set; }
        public long Count { get; set; }
    }

    public class FindProfessional
    {
        public long ItemId { get; set; }
        public string Name { get; set; }
        public string DayOne { get; set; }
        public string DayTwo { get; set; }
        public string DayThree { get; set; }
        public string CompanyName { get; set; }
        public bool IsScheduleMeetup { get; set; }
        public string Image { get; set; }
    }
    public class MyScheduleDTO
    {
        public long MyRegistrationId { get; set; }
        public long PPRegId { get; set; }
        public long MemberId { get; set; }
        public List<CustomScheduleDTO> CustomScheduleList { get; set; }
        public bool IsShowPDF { get; set; }
        public string IsHopitalityPackage { get; set; }
        public string Message { get; set; }
        public string MessageTypeResponse { get; set; }
        public string DayOne { get; set; }
        public string DayTwo { get; set; }
        public string DayThree { get; set; }
        public string ReturnURL { get; set; }
    }

    public class FindProfessionalMessageDTO
    {
        public MyProfileDTO MyProfileDTO { get; set; }
        public string Message { get; set; }
        public string MessageTypeResponse { get; set; }
    }
    public class PPMessageDTO
    {
        public long RegistrationId { get; set; }
        public long MemberId { get; set; }
        public int StepId { get; set; }
        public string Message { get; set; }
        public string MessageTypeResponse { get; set; }
    }
    public class MyProfileMessageDTO
    {
        public long MyRegistrationId { get; set; }
        public ExhibitionProfessionalProgramRegistrationDTO MyProfileDTO { get; set; }
        public string Message { get; set; }
        public string MessageTypeResponse { get; set; }
    }
    public class MyProfileBooksMessageDTO
    {
        public long MyRegistrationId { get; set; }
        public ProfileBooksDTO MyProfileDTO { get; set; }
        public List<ProfessionalProgramBook> ProfessionalProgramBookList { get; set; }
        public string Message { get; set; }
        public string MessageTypeResponse { get; set; }
    }
    public class ProfileBooksDTO
    {
        public long ItemId { get; set; }
        public long ProfessionalProgramId { get; set; }
        public string BooksUrl { get; set; }
        public string FileName1 { get; set; }
        public string CatalogFileName { get; set; }
    }
    public class MyProfileDTO
    {
        public long ItemId { get; set; }
        public long TableId { get; set; }
        public long WebsiteId { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string FirstNameAr { get; set; }
        public string LastName { get; set; }
        public string LastNameAr { get; set; }
        public string Email { get; set; }
        public long CountryId { get; set; }
        public long CityId { get; set; }
        public string OtherCity { get; set; }
        public string PhoneISD { get; set; }
        public string PhoneSTD { get; set; }
        public string Phone { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }
        public string Mobile { get; set; }
        public string CompanyName { get; set; }
        public string CompanyNameAr { get; set; }
        public string JobTitle { get; set; }
        public string JobTitleAr { get; set; }
        public string WorkProfile { get; set; }
        public string WorkProfileAr { get; set; }
        public string Website { get; set; }
        public string SocialLinkURL { get; set; }
        public string IsAttended { get; set; }
        public string HowManyTimes { get; set; }
        public string ReasonToAttend { get; set; }
        public string FileName { get; set; }
        public string FileNameExt { get; set; }
        public string OtherGenre { get; set; }
        public string TradeType { get; set; }
        public string IsEbooks { get; set; }
        public string ExamplesOfTranslatedBooks { get; set; }
        public string IsInterested { get; set; }
        public string InterestedInfo { get; set; }
        public string CatalogURL { get; set; }
        public string FileName1 { get; set; }
        public string BooksURL { get; set; }
        public string ZipCode { get; set; }
        public string ProgramOther { get; set; }
        public string ReasonToAttendAgain { get; set; }
        public string IsAttendedTG { get; set; }
        public string NoofPendingTG { get; set; }
        public string NoofPublishedTG { get; set; }
        public string UnSoldRightsBooks { get; set; }
        public string AcquireRightsToPublished { get; set; }
        public string IsAlreadyBoughtRights { get; set; }
        public string KeyPublisherTitlesOrBusinessRegion { get; set; }
        public string CatalogueMainLanguageOfOriginalTitle { get; set; }
        public string MainTerritories { get; set; }
        public string SuggestionToSupportApp { get; set; }
        public string RoundTableTopics { get; set; }
        public string SpeakOnRoundTable { get; set; }
        public string RecommendPublishers { get; set; }
        public List<long?> PPProgramIds { get; set; }
        public List<long> GenreIds { get; set; }
    }

    public class MyBooksMessageDTO
    {
        public MyBooksDTO MyBooksDTO { get; set; }
        public string Message { get; set; }
        public string MessageTypeResponse { get; set; }
    }
    public class MyBooks
    {
        public long ItemId { get; set; }
        public string AuthorName { get; set; }
        public string Title { get; set; }
        public long? GenreId { get; set; }
        public string Genre { get; set; }
        public string OtherGenre { get; set; }
        public List<long> LanguageId { get; set; }
        public string Language { get; set; }
        public string IsRightsAvailable { get; set; }
        public string IsOwnRights { get; set; }
        public string RightsOwner { get; set; }
        public string Synopsis { get; set; }
        public string FileName { get; set; }
        public string FileNameExt { get; set; }
    }
    public class MyBooksDTO
    {
        public long RegisteredId { get; set; }
        public List<MyBooks> BooksList { get; set; }
        public int Count { get; set; }
        public string Message { get; set; }
    }
    public class UpdateCatalogueDTO
    {
        //public long RegisteredId { get; set; }
        //public long MemberId { get; set; }
        //public string BooksURL { get; set; }
        //public string CatalogueBase64 { get; set; }
        //public string FileName { get; set; }
        //public string FileNameExt { get; set; }

        public long ItemId { get; set; }
        public long? MemberId { get; set; }
        public string BooksUrl { get; set; }
        public string FileBase64 { get; set; }
        public string FileExtension { get; set; }
    }
    public class SlotResult
    {
        public SlotResult()
        {
            SlotsList = new List<XsiSlots>();
        }
        public List<XsiSlots> SlotsList { get; set; }
    }
    public partial class XsiSlots
    {
        public long ItemId { get; set; }
        public Nullable<long> DayOneId { get; set; }
        public Nullable<long> DayTwoId { get; set; }
        public Nullable<long> DayThreeId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string IsBreakDayOne { get; set; }
        public string IsAvailableDayOne { get; set; }
        public string IsBreakDayTwo { get; set; }
        public string IsAvailableDayTwo { get; set; }
        public string IsBreakDayThree { get; set; }
        public string IsAvailableDayThree { get; set; }
        public string IsApprovedStatusDayOne { get; set; }
        public string IsApprovedStatusDayTwo { get; set; }
        public string IsApprovedStatusDayThree { get; set; }
        public string Break1Title { get; set; }
        public string Break2Title { get; set; }
        public string Break3Title { get; set; }

        public string DayOne { get; set; }
        public string DayTwo { get; set; }
        public string DayThree { get; set; }
    }
    public partial class XsiInvitee
    {
        public long ItemId { get; set; }
        public string FileName { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string Genre { get; set; }
        public List<string> PPGenreTextList { get; set; }
        public string Message { get; set; }
        public string Meetup { get; set; }
        public long MemberId { get; set; }
        public string SDate { get; set; }
        public string EDate { get; set; }
    }
    public class SendMeetupRequest
    {
        public long RequestId { get; set; }
        public long ResponseId { get; set; }
        public long SlotId { get; set; }
        public long PPSlotItemID { get; set; }
        public string Message { get; set; }
        public string InviteURL { get; set; }
    }
    public class SlotsDTO
    {
        public List<CustomScheduleDTO> SlotsList { get; set; }
        public string Message { get; set; }
        public string MessageTypeResponse { get; set; }
        public string DayOne { get; set; }
        public string DayTwo { get; set; }
        public string DayThree { get; set; }
    }

    public class CancelAfterApprovalDTO
    {
        public long TransactionId { get; set; }
        public string Reason { get; set; }
    }
    public class CancelRequestSentDTO
    {
        public long TransactionId { get; set; }
    }
    public class SubmitInvitationDTO
    {

        public long TransactionId { get; set; }
        public bool IsAccept { get; set; }
    }
    public class UpdateViewStatusDTO
    {
        public long TransactionId { get; set; }
    }
}

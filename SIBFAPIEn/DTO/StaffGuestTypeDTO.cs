﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class StaffGuestTypeDTO
    {
        public StaffGuestTypeDTO()
        {
            //this.StaffGuestTypeDTO = new HashSet<StaffGuestTypeDTO>();
        }
        public long ItemId { get; set; }
        public string Title { get; set; }
    }
}

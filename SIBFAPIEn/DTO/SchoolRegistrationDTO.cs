﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class SchoolRegistrationDTO
    {
        public SchoolRegistrationDTO()
        {

        }
        public string SchoolName { get; set; }
        public long? SchoolTypeId { get; set; }
        public string PersonInCharge { get; set; }
        // public string MobileNo { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public long? EmirateId { get; set; }
        public string NumberofStudents { get; set; }
        public string AgeRange { get; set; }
        public string DateofVisit { get; set; }
        public long? CategoryId { get; set; }
        public string ActivityOfInterest { get; set; }
        public long? MemberId { get; set; }
    }
    public class Dates
    {
        public string Date { get; set; }
        public string DateId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class ExhibitionMemberDTO
    {
        public long LanguageURL { get; set; }

        public long WebsiteId { get; set; } = 2;

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }

        public string FirstNameAr { get; set; }
        public string LastNameAr { get; set; }

        public string CompanyName { get; set; }
        public string CompanyNameAr { get; set; }

        public string ContactTitle { get; set; }
        public string ContactTitleAr { get; set; }

        public string CompanyFieldofWork { get; set; }
        public string CompanyFieldofWorkAr { get; set; }

        public Nullable<long> CountryId { get; set; }
        public Nullable<long> CityId { get; set; }
        public string OtherCity { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Website { get; set; }

        public string Phone { get; set; }
        public string ISD { get; set; }
        public string STD { get; set; }

        public string Mobile { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }

        public string POBox { get; set; }
        public string Address { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }

        public List<long> ReasonIds { get; set; }
        public string OtherReason { get; set; }
    }
    public class EditExhibitionMemberDTO
    {
        public long MemberId { get; set; }
        public long LanguageURL { get; set; }
        public long WebsiteId { get; set; }

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }

        public string FirstNameAr { get; set; }
        public string LastNameAr { get; set; }

        public string CompanyName { get; set; }
        public string CompanyNameAr { get; set; }

        public string ContactTitle { get; set; }
        public string ContactTitleAr { get; set; }
        public string CompanyFieldofWork { get; set; }
        public string CompanyFieldofWorkAr { get; set; }

        public Nullable<long> CountryId { get; set; }
        public Nullable<long> CityId { get; set; }
        public string OtherCity { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Website { get; set; }

        public string Phone { get; set; }
        public string ISD { get; set; }
        public string STD { get; set; }

        public string Mobile { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }

        public string Fax { get; set; }
        public string FaxISD { get; set; }
        public string FaxSTD { get; set; }

        public string POBox { get; set; }
        public string Address { get; set; }
        [Required]
        public string UserName { get; set; }
        public string Password { get; set; }

        //public List<Reasonddl> ReasondIds { get; set; }
        //public List<long> ReasonIdsSelected { get; set; }
        //public string OtherReason { get; set; }
    }
    public class LoginDto
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
   
    public class VerifyDTO
    {
        [Required]
        public string id { get; set; }
    }
    public class VerifyForgotPasswordDTO
    {
        [Required]
        public string id { get; set; }
        [Required]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Password and Confirmation Password must match.")]
        public string confirm_password { get; set; }
    }
    public class VerifyChangeEmailDTO
    {
        [Required]
        public string id { get; set; }
        public string Email { get; set; }
        public long? websiteid { get; set; }
    }

    public class LoggedinUserInfo
    {

        public long SIBFMemberId { get; set; }
        public long AdminUserId { get; set; }
        public string SIBFMemberPublicName { get; set; }
        public bool IsLibraryMember { get; set; }
        public bool IsValidLoggedInMember { get; set; }
        public string IsNewPatternChanged { get; set; }
        public string StatusMessage { get; set; }
        public string PopUpId { get; set; }
        public string Token { get; set; }
        public DateTime expiration { get; set; }
        public string RefreshToken { get; set; }
        public DateTime RefreshTokenExpiryTime { get; set; }
    }
    public class AdminLoginDto
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string AdminUserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
    public class AdminLoginOTPDto
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string AdminUserName { get; set; }
        [Required]
        public string OTP { get; set; }
    }
    public class AdminLoggedinUserInfo
    {

        public long SIBFMemberId { get; set; }
        public long AdminUserId { get; set; }
        public string SIBFMemberPublicName { get; set; }
        public bool IsLibraryMember { get; set; }
        public bool IsValidLoggedInMember { get; set; }
        public string IsNewPatternChanged { get; set; }
        public string StatusMessage { get; set; }
        public string PopUpId { get; set; }
        public string Token { get; set; }
        public DateTime expiration { get; set; }
        public string RefreshToken { get; set; }
        public DateTime RefreshTokenExpiryTime { get; set; }
        public string UserName { get; set; }
        public string AdminUserName { get; set; }
    }

    public class ForgotPasswordDto
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}


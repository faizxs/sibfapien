﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class LoginResposeMobileDTO
    {
        public long MemberId { get; set; }
        public string Name { get; set; }
        //public string SubTitle { get; set; }        

    }    
    
    public class PaymentMobileDTO
    {
        public long Id{ get; set; }
        public string Date{ get; set; }
        public string Status { get; set; }

        [JsonProperty(PropertyName = "data")]
        public dynamic PaymentDetailsMobileDTO { get; set; }
    }
    public class PaymentDetailsMobileDTO
    {
        [JsonProperty(PropertyName = "Invoice #")]
        public string Invoice { get; set; }

        [JsonProperty(PropertyName = "Status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "Payment Type")]
        public string PaymentType { get; set; }

        [JsonProperty(PropertyName = "Total Amount")]
        public string TotalAmount { get; set; }

    }
    public class PaymentDetailsMobileArDTO
    {
        [JsonProperty(PropertyName = "رقم الفاتورة #")]
        public string Invoice { get; set; }

        [JsonProperty(PropertyName = "الحالة")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "طريقة الدفع")]
        public string PaymentType { get; set; }

        [JsonProperty(PropertyName = "المبلغ الإجمالي")]
        public string TotalAmount { get; set; }

    }

    public class InvoiceDetailsMobileDTO
    {
        [JsonProperty(PropertyName = "Allocated space assigned by SIBF")]
        public string AllocatedSpace { get; set; }

        [JsonProperty(PropertyName = "Participation Fees")]
        public string ParticipationFees { get; set; }

        [JsonProperty(PropertyName = "Allocated space fees")]
        public string AllocatedSpaceFee { get; set; }

        [JsonProperty(PropertyName = "Due from last year (Debit)")]
        public string DueFromLastYearDebit { get; set; }

        [JsonProperty(PropertyName = "Due from last year (Credit)")]
        public string DueFromLastYearCredit { get; set; }

        [JsonProperty(PropertyName = "Discount")]
        public string Discount { get; set; }

        [JsonProperty(PropertyName = "Penalties")]
        public string Penalties { get; set; }

        [JsonProperty(PropertyName = "Total in AED")]
        public string TotalInAED { get; set; }

        //[JsonProperty(PropertyName = "Total in USD")]
        //public string TotalInUSD { get; set; }

        [JsonProperty(PropertyName = "Total Paid")]
        public string TotalPaid{ get; set; }

        [JsonProperty(PropertyName = "Total Balance")]
        public string TotalBalance { get; set; } 
    }
    public class InvoiceDetailsMobileArDTO
    {
        [JsonProperty(PropertyName = "المساحة المعتمدة")]
        public string AllocatedSpace { get; set; }

        [JsonProperty(PropertyName = "رسم الاشتراك")]
        public string ParticipationFees { get; set; }

        [JsonProperty(PropertyName = "رسم الجناح")]
        public string AllocatedSpaceFee { get; set; }

        [JsonProperty(PropertyName = "المستحق عليه من السنوات السابقة")]
        public string DueFromLastYearDebit { get; set; }

        [JsonProperty(PropertyName = "المستحق له")]
        public string DueFromLastYearCredit { get; set; }

        [JsonProperty(PropertyName = "الخصم")]
        public string Discount { get; set; }

        [JsonProperty(PropertyName = "الغرامات")]
        public string Penalties { get; set; }

        [JsonProperty(PropertyName = "المبلغ الإجمالي بالدرهم الإماراتي")]
        public string TotalInAED { get; set; }

        //[JsonProperty(PropertyName = "Total in USD")]
        //public string TotalInUSD { get; set; }

        [JsonProperty(PropertyName = "المبلغ المدفوع")]
        public string TotalPaid { get; set; }

        [JsonProperty(PropertyName = "المتبقي")]
        public string TotalBalance { get; set; }
    }
}


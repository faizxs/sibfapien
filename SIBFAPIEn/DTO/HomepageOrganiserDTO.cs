﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class HomepageOrganiserDTO
    {
        public HomepageOrganiserDTO()
        {
        }
        public long ItemId { get; set; }
        public string Title { get; set; }
        public string IsActive { get; set; }
    }
    public class HomepageOrganiserItemDTO
    {
        public string HomeBannerSectionIsActive { get; set; }
        public string HomeSubBannerSectionIsActive { get; set; }
        public string ActivitiesSectionIsActive { get; set; }
        public string CounterSectionIsActive { get; set; }
        public string ExhibitorsSectionIsActive { get; set; }
        public string LatestNewsSectionIsActive { get; set; }
        public string BooksSearchIsActive { get; set; }
        public string GuestSectionIsActive { get; set; }
        public string SponsorSectionIsActive { get; set; }
        public string TabSectionIsActive { get; set; }
        public string VipGuestSectionIsActive { get; set; }

    }
}

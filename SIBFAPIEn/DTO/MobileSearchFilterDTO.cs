﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class MobileSearchFilterDTO
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public List<PropertiesDTO> Properties { get; set; }
    }

    public class PropertiesDTO
    {
        /// <summary>
        /// Order of the Filter
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        //[Display(Name = "City", ResourceType = typeof(EmployeeResx))] 

        public long OrderId { get; set; }
        public string Title { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "subtitle")]
        public string SubTitle { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "color")]
        public string Color { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "items")]
        public List<FilterItemsDTO> FilterItemsDTO { get; set; }
    }

    public class FilterItemsDTO
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "parentid")]
        public long ParentId { get; set; }
        public long Id { get; set; }
        public string Title { get; set; }
    }


    public class BooksRequestDTO
    {
        public long WebsiteId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public long Subject { get; set; }
        public long Type { get; set; }
        public string Exhibitor { get; set; }
        public string Year { get; set; }
    }

    public class ExhibitorsRequestDTO
    {
        public long WebsiteId { get; set; }
        public long CategoryId { get; set; }
        public long CountryId { get; set; }
        public long StandSectionId { get; set; }
        public long StandSubSectionId { get; set; }
    }

    public class MainRequestDTO
    {
        public long? PropertyId { get; set; }
        public long? ItemId { get; set; }
        public string Value { get; set; }
    }
}


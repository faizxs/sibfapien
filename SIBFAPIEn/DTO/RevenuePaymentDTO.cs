﻿using Microsoft.AspNetCore.Http;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class RevRoot
    {
        public List<RevenueInvoice> Table { get; } = new List<RevenueInvoice>();
    }

    public class RevenueInvoice
    {
        public string ExhibitionName_en { get; set; }
        public string ExhibitionName_ar { get; set; }
        public int PublisherID { get; set; }
        public string CustomerName_Ar { get; set; }
        public string CustomerName_En { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public double InvoiceAmount { get; set; }
        public double TransferAmount { get; set; }
        public string Status { get; set; }
        public string StatArDesc { get; set; }
        public int StatIDNo { get; set; }
        public int Remaining { get; set; }
    }
  
    public class RevenueInvoiceDetails
    {
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string CustomerName { get; set; }
        public double TotalInvoiceAmount { get; set; }
        public double DiscountAmount { get; set; }
        public double VatAmount { get; set; }
        public double TotalPenaltyAmount { get; set; }
        public long TransRefNumber { get; set; }
        public long ApplicationID { get; set; }
        public List<InvoiceFieldInfo> InvoiceDetails { get; } = new List<InvoiceFieldInfo>();
    }
    public class InvoiceFieldInfo
    {
        public string FeeNameAr { get; set; }
        public string FeeNameEn { get; set; }
        public double FeeAmount { get; set; }
        public double FeeQty { get; set; }
        public double TotalAmount { get; set; }
    }
}

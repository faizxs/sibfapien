﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class PhotoCategoryDTO
    {
        public long ItemId { get; set; }
        public string Title { get; set; }
    }
    public class PhotoSubCategoryDTO
    {
        public long ItemId { get; set; }
        public long? CategoryId { get; set; }
        public string CategoryTitle { get; set; }
        public string Title { get; set; }
        public string Dated { get; set; }
        public string FileName { get; set; }
    }
    #region Album
    public class AlbumPageDTO
    {
        public AlbumDTO TopAlbumItemDTO { get; set; }
        public List<AlbumDTO> AlbumDTO { get; set; }
    }
    public class AlbumDTO
    {
        public long ItemId { get; set; }
        public Nullable<long> PhotoCategoryId { get; set; }
        public string CategoryTitle { get; set; }
        public Nullable<long> SubCategoryId { get; set; }
        public string SubCategoryTitle { get; set; }
        public string Title { get; set; }
        public string StrDated { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Dated { get; set; }
        public string Year { get; set; }
        public Nullable<long> SortIndex { get; set; }
        public long Count { get; set; }
        public List<PhotoDTO> Photos { get; set; }
        public string Thumbnail { get; set; }
        public string MiniThumbnail { get; set; }
        public string LargeThumbnail { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
    public class PhotoDTO
    {
        public long ItemId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Nullable<long> SortIndex { get; set; }
        public string Thumbnail { get; set; }
        public string MiniThumbnail { get; set; }
        public string LargeThumbnail { get; set; }
        public DateTime? CreatedOn { get; set; }
    } 
    #endregion
    #region Details
    public class PhotosGalleryDetailsDTO
    {
        public List<PhotoDTO> Photos { get; set; }
        public List<PhotoDTO> LatestPhotos { get; set; }
        public AlbumDTO AlbumDetails;
    }
    #endregion
}
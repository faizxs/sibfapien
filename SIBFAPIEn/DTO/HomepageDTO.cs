﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class HomepageAllDTO
    {
        public HomepageOrganiserDTO HomepageOrganiserDTO { get; set; }
        public HomepageOrganiserItemDTO HomepageOrganiserItemDTO { get; set; }

        public HomepageBannerDTO HomepageBannerDTO { get; set; }
        public HomepageSubBannerNewDTO HomepageSubBannerNewDTO { get; set; }
        
        //Activities
        public HomepageSectionTwoDTO HomepageSectionTwoDTO { get; set; }

        //EXHIBITORS
        public HomepageSectionFourDTO HomepageSectionFourDTO { get; set; }
        public List<HomepageNewsDto> HomepageNewsList { get; set; }
        public List<HomeEventDTO> HomepageEventsList { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    
    //public class DashboardMobileDTO
    //{
    //    public MetaDataDTO MetaData { get; set; }
    //    public string Title { get; set; }
    //    public string SubTitle { get; set; }
    //    public int Id { get; set; }
    //    public int Position { get; set; }
    //    public List<ItemsDTO> Items { get; set; }
    //}

    //public class ItemsDTO
    //{
    //    public long Id { get; set; }
    //    public string Title { get; set; }
    //    public string SubTitle { get; set; }
    //    public string Caption { get; set; }
    //    public string Image { get; set; }
    //    public string Color { get; set; }
    //    public string Author { get; set; }
    //    [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "type")]
    //    public string Type { get; set; }
    //}

    //public class MetaDataDTO
    //{
    //    public string Layout { get; set; }
    //    public string View { get; set; }
    //}

    public class ActivityItemDetailsDTO
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Category { get; set; }
        public string Image { get; set; }
        public string Color { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "data")]
        public dynamic ActivityDetailsDTO { get; set; }
    }

    public class BookItemDetailsDTO
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Category { get; set; }
        public string Image { get; set; }
        public string Color { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "data")]
        public dynamic BookDetailsDTO { get; set; }
        public long exhibitorId { get; set; }
    }

    public class ExhibitorItemDetailsDTO
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Category { get; set; }
        public string Image { get; set; }
        public string Color { get; set; }
        public string Overview { get; set; }
        public string FeatureId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "data")]
        public dynamic ExhibitorDetailsDTO { get; set; }
    }

    public class LoggedInExhibitorDetailsDTO
    {
        [JsonProperty(PropertyName = "Hall #")]
        public string Hall { get; set; }
        [JsonProperty(PropertyName = "Allocated Area")]
        public string AllocatedArea { get; set; }
        [JsonProperty(PropertyName = "Stand Section")]
        public string StandSection { get; set; }

        [JsonProperty(PropertyName = "Stand Subsection")]
        public string StandSubSection { get; set; }
        public string Overview { get; set; }
    }
    public class LoggedInExhibitorDetailsArDTO
    {
        
        [JsonProperty(PropertyName = "Hall #")]
        public string Hall { get; set; }
        [JsonProperty(PropertyName = "Allocated Area")]
        public string AllocatedArea { get; set; }
        [JsonProperty(PropertyName = "قسم الجناح")]
        public string StandSection { get; set; }

        [JsonProperty(PropertyName = "القسم الفرعي")]
        public string StandSubSection { get; set; }
        public string Overview { get; set; }
    }

    public class LoggedInExhibitorItemDetailsDTO
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Category { get; set; }
        public string Image { get; set; }
        public string Color { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "data")]
        public dynamic LoggedInExhibitorDetailsDTO { get; set; }
        public string FeatureId { get; set; }
    }
    public class ExhibitorDetailsDTO
    {
        [JsonProperty(PropertyName = "Country")]
        public string Country { get; set; }

        [JsonProperty(PropertyName = "Stand Section")]
        public string StandSection { get; set; }

        [JsonProperty(PropertyName = "Category")]
        public string Category { get; set; }
        [JsonProperty(PropertyName = "Stand Subsection")]
        public string StandSubSection { get; set; }
        public string Overview { get; set; }

        [JsonProperty(PropertyName = "Stand Number")]
        public string StandNumber { get; set; }
    }
    public class ExhibitorDetailsArDTO
    {
        [JsonProperty(PropertyName = "البلد")]
        public string Country { get; set; }

        [JsonProperty(PropertyName = "قسم الجناح")]
        public string StandSection { get; set; }

        [JsonProperty(PropertyName = "الفئة")]
        public string Category { get; set; }

        [JsonProperty(PropertyName = "القسم الفرعي")]
        public string StandSubSection { get; set; }

        [JsonProperty(PropertyName = "نظرة شاملة")]
        public string Overview { get; set; }

        [JsonProperty(PropertyName = "رقم الجناح")]
        public string StandNumber { get; set; }
    }

    public class GuestItemDetailsDTO
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Category { get; set; }
        public string Image { get; set; }
        public string Color { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "data")]
        public dynamic GuestDetailsDTO { get; set; }
    }

    public class GuestDetailsDTO
    {
        [JsonProperty(PropertyName = "Bio")]
        public string BIO { get; set; }
        [JsonProperty(PropertyName = "Activities")]
        public List<GuestActivityDetailsDTO> GuestActivityDetails { get; set; }
    }
    public class GuestDetailsArDTO
    {
        [JsonProperty(PropertyName = "عن الضيف")]
        public string BIO { get; set; }
        [JsonProperty(PropertyName = "الفعاليات")]
        public List<GuestActivityDetailsDTO> GuestActivityDetails { get; set; }
    }

    public class GuestActivityDetailsDTO
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Caption { get; set; }
        public string Image { get; set; }
        public string Color { get; set; }
    }
    //public class SearchListDTO
    //{
    //    public long Id { get; set; }
    //    public string Type { get; set; }
    //    public long TypeId { get; set; }
    //    public string Image { get; set; }
    //    public string Title { get; set; }
    //    public string SubTitle { get; set; }
    //}
    public class BookDetailsDTO
    {
        [JsonProperty(PropertyName = "Author")]
        public string Author { get; set; }

        [JsonProperty(PropertyName = "Publisher")]
        public string Publisher { get; set; }

        [JsonProperty(PropertyName = "Year")]
        public string Year { get; set; }

        [JsonProperty(PropertyName = "Subject")]
        public string Subject { get; set; }

        [JsonProperty(PropertyName = "Stand Number")]
        public string StandCode { get; set; }

        [JsonProperty(PropertyName = "Details")]
        public string Details { get; set; }

        [JsonProperty(PropertyName = "Price")]
        public string Price { get; set; }

        [JsonProperty(PropertyName = "ISBN")]
        public string ISBN { get; set; }

        [JsonProperty(PropertyName = "Exhibitor")]
        public string ExhibitorName { get; set; }


    }
    public class BookDetailsArDTO
    {
        [JsonProperty(PropertyName = "المؤلف")]
        public string Author { get; set; }

        [JsonProperty(PropertyName = "الناشر")]
        public string Publisher { get; set; }

        [JsonProperty(PropertyName = "السنة")]
        public string Year { get; set; }

        [JsonProperty(PropertyName = "الموضوع")]
        public string Subject { get; set; }

        [JsonProperty(PropertyName = "رقم الجناح")]
        public string StandCode { get; set; }

        [JsonProperty(PropertyName = "التفاصيل")]
        public string Details { get; set; }

        [JsonProperty(PropertyName = "السعر")]
        public string Price { get; set; }

        [JsonProperty(PropertyName = "الترقيم الدولي")]
        public string ISBN { get; set; }

        [JsonProperty(PropertyName = "العارضون")]
        public string ExhibitorName { get; set; }
    }

    public class ActivityDetailsDTO
    {
        [JsonProperty(PropertyName = "Event Start Date / End Date")]
        public string EventDate { get; set; }
        [JsonProperty(PropertyName = "Event Location")]
        public string EventLocation { get; set; }
        [JsonProperty(PropertyName = "Event Details")]
        public string EventDetails { get; set; }
        [JsonProperty(PropertyName = "Guest")]
        public List<ItemsDTO> Guests { get; set; }
    }
    public class ActivityDetailsArDTO
    {
        [JsonProperty(PropertyName = "تاريخ بدء/ انتهاء الفعالية")]
        public string EventDate { get; set; }
        [JsonProperty(PropertyName = "موقع الفعالية")]
        public string EventLocation { get; set; }
        [JsonProperty(PropertyName = "تفاصيل الفعالية")]
        public string EventDetails { get; set; }
        [JsonProperty(PropertyName = "الضيف")]
        public List<ItemsDTO> Guests { get; set; }
    }

    public class ActivityFilterDTO
    {
        public long Id { get; set; }
        public long? CategoryId { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Caption { get; set; }
        public string Image { get; set; }
        public string Color { get; set; }
    }
}


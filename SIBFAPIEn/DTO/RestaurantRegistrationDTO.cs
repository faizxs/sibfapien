﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class RestaurantRegistrationDTO
    {
        public long RestaurantId { get; set; }
        public long ExhibitionId { get; set; }
        public long? MemberId { get; set; }
        public long? WebsiteId { get; set; }
        public long? LanguageUrl { get; set; }
        public long? StepId { get; set; }
        public bool IsEditRequest { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public string IsNotesRead { get; set; }
        public RestaurantStepTwoDTO StepTwoDTO { get; set; }
        public RestaurantStepThreeDTO StepThreeDTO { get; set; }
        public RestaurantStepFourDTO StepFourDTO { get; set; }
        public RestaurantStepFiveDTO StepFiveDTO { get; set; }
        public RestaurantStepSixDTO StepSixDTO { get; set; }
    }
    public class RestaurantStepTwoDTO
    {
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public long CountryId { get; set; }
        public long CityId { get; set; }
        public string OtherCity { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonNameAr { get; set; }
        public string ContactPersonTitle { get; set; }
        public string ContactPersonTitleAr { get; set; }
        public string PhoneISD { get; set; }
        public string PhoneSTD { get; set; }
        public string Phone { get; set; }
        public string FaxISD { get; set; }
        public string FaxSTD { get; set; }
        public string Fax { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        //  public long ExhibitionCategoryId { get; set; }
        // public List<long> ActivityIds { get; set; }
        // public string APU { get; set; }
        //  public string Brief { get; set; }
        //  public string BriefAr { get; set; }
        public string TradeLicenceName { get; set; }
        public string LogoName { get; set; }
        public string StandPhotoName { get; set; }
        public string StandLayoutByMeterName { get; set; }
        public string RequiredPowerCapacity { get; set; }
    }
    public class RestaurantStepThreeDTO
    {
        public string CurrentEditRequest { get; set; }
        //  public string TotalNumberOfNewTitles { get; set; }
        // public string TotalNumberOfTitles { get; set; }
        //   public long BoothSectionId { get; set; }
        //   public long BoothSubSectionId { get; set; }
        //   public string BoothSubSectionTitle { get; set; }
        public string RequiredAreaShapeType { get; set; }
        public long RequiredAreaId { get; set; }

        //For Restaurant
        public string RequiredArea { get; set; } = "3";
        public string CalculatedArea { get; set; }
        public string RequiredAreaMeter { get; set; }
        public string RequiredAreaValue { get; set; }
    }

    public class RestaurantPostStepFourDTO
    {
        //public long WebsiteId { get; set; }
        //public long MemberId { get; set; }
        //public long ExhibitorId { get; set; }
        //public long InvoiceId { get; set; }
        //public string TRNNumber { get; set; }
        //public string ReceiptFile { get; set; }
        //public string ReceiptExt { get; set; }
        //public string BankName { get; set; }
        //public string ReceiptNumber { get; set; }
        //public DateTime? TransferDate { get; set; }
        //public long PaymentTypeId { get; set; }
    }
    public class RestaurantStepFiveDTO
    {
        public long WebsiteId { get; set; }
        public long MemberId { get; set; }
        public long ExhibitorId { get; set; }
        public long InvoiceId { get; set; }
        public string TRNNumber { get; set; }
        public string ReceiptFile { get; set; }
        public string ReceiptExt { get; set; }
        public string BankName { get; set; }
        public string ReceiptNumber { get; set; }
        public DateTime? TransferDate { get; set; }
        public long PaymentTypeId { get; set; }
        public string ContractCMS { get; set; }
        public string ContractUser { get; set; }
    }
    public class RestaurantStepSixDTO
    {
        public string Section { get; set; }
        public long BoothSectionId { get; set; }
        public string SubSection { get; set; }
        // public string AllocatedSpace { get; set; }
        public string Area { get; set; }
        public string HallNumber { get; set; }
        public string Stand { get; set; }
        public string UploadLocationMap { get; set; }
    }
    public class RestaurantStepFourDTO
    {
        public long InvoiceId { get; set; }
        public string AllocatedSpace { get; set; }
        public string InvoiceNumber { get; set; }
        public string ParticipationFee { get; set; }
        public string StandFee { get; set; }
        public string KnowledgeAndResearchFee { get; set; }
        public string AgencyFee { get; set; }
        public string RepresentativeFee { get; set; }
        public string DueFromLastYearCredit { get; set; }
        public string DueFromLastYearDebit { get; set; }
        public string DueCurrentYear { get; set; }
        public string DebitedCurrentYear { get; set; }
        public string Discount { get; set; }
        public string Penalty { get; set; }
        public string VAT { get; set; }
        public string TotalAED { get; set; }
        public string TotalUSD { get; set; }
        public string FileName { get; set; }
        public string RestaurantLastDate { get; set; }
        public string RemainingDays { get; set; }
      
    }
    public class RestaurantSaveDataForStepTwoAndThreeDTO
    {
        public long WebsiteId { get; set; }
        public long ExhibitionId { get; set; }
        public long MemberId { get; set; }
        public long RestaurantId { get; set; }
        public long CountryId { get; set; }
        public long CityId { get; set; }
        public string OtherCity { get; set; }
        public long ExhibitionCategoryId { get; set; }
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonNameAr { get; set; }
        public string ContactPersonTitle { get; set; }
        public string ContactPersonTitleAr { get; set; }
        public string IsFirstTime { get; set; }
        public string PhoneISD { get; set; }
        public string PhoneSTD { get; set; }
        public string Phone { get; set; }
        public string MobileISD { get; set; }
        public string MobileSTD { get; set; }
        public string Mobile { get; set; }
        public string FaxISD { get; set; }
        public string FaxSTD { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        //   public string TotalNumberOfTitles { get; set; }
        // public string TotalNumberOfNewTitles { get; set; }
        //  public long BoothSectionId { get; set; }
        //   public long BoothSubSectionId { get; set; }
        public string APU { get; set; }
        public string RequiredAreaShapeType { get; set; }
        public string CalculatedArea { get; set; }
        public long RequiredAreaId { get; set; }
        public long RequiredAreaValue { get; set; }
        public string RequiredArea { get; set; }
        public string RequiredAreaMeter { get; set; }
        public string TradeLicenceName { get; set; }
        public string TradeLicenceBase64 { get; set; }
        public string TradeLicenceExt { get; set; }
        public string LogoName { get; set; }
        public string LogoBase64 { get; set; }
        public string LogoExt { get; set; }
        // public List<long> ActivityIds { get; set; }
        // public string Brief { get; set; }
        // public string BriefAr { get; set; }
        public bool IsEditRequest { get; set; }
        public string StandPhotoName { get; set; }
        public string StandPhotoBase64 { get; set; }
        public string StandPhotoExt { get; set; }
        public string StandLayoutByMeterName { get; set; }
        public string StandLayoutByMeterBase64 { get; set; }
        public string StandLayoutByMeterExt { get; set; }
        public string RequiredPowerCapacity { get; set; }
    }

    public class RestaurantReturnDTO
    {
        public long RestaurantId { get; set; }
        public long? MemberId { get; set; }
        public long? WebsiteId { get; set; }
        public long? StepId { get; set; }
        public string Message { get; set; }
    }
}


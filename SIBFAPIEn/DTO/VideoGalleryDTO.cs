﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class VideoCategoryDTO
    {
        public long ItemId { get; set; }
        public string Title { get; set; }
    }
    #region Album
    public class VideoAlbumPageDTO
    {
        public VideoAlbumDTO TopAlbumItemDTO { get; set; }
        public List<VideoAlbumDTO> AlbumDTO { get; set; }
    }
    public class VideoAlbumDTO
    {
        public long ItemId { get; set; }
        public Nullable<long> CategoryId { get; set; }
        public string CategoryTitle { get; set; }
        public string Title { get; set; }
        public string StrDated { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Dated { get; set; }
        public string Year { get; set; }
        public Nullable<long> SortIndex { get; set; }
        public long Count { get; set; }
        public List<PhotoDTO> Photos { get; set; }
        public string Thumbnail { get; set; }
        public string MiniThumbnail { get; set; }
        public string LargeThumbnail { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
    public class VideoDTO
    {
        public long ItemId { get; set; }
        public string AlbumTitle { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Nullable<long> SortIndex { get; set; }
        public string Thumbnail { get; set; }
        public string URL { get; set; }
        public string MiniThumbnail { get; set; }
        public string LargeThumbnail { get; set; }
        public DateTime? CreatedOn { get; set; }
    } 
    #endregion
    #region Details
    public class VideoGalleryDetailsDTO
    {
        public List<VideoDTO> Videos { get; set; }
        public VideoAlbumDTO AlbumDetails;
    }
    #endregion
}
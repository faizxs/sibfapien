﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class StaffGuestCategoryDTO
    {
        public StaffGuestCategoryDTO()
        {
            //this.StaffGuestCategoryDTO = new HashSet<StaffGuestCategoryDTO>();
        }
        public long ItemId { get; set; }
        public string Title { get; set; }
    }
}

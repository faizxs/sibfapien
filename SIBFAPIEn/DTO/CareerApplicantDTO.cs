﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class CareerApplicantDTO
    {
        public Nullable<long> CareerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string ISD { get; set; }
        public string STD { get; set; }
        public string YearofBirth { get; set; }
        public string VisaStatus { get; set; }
        public string Gender { get; set; }
        public Nullable<long> Nationality { get; set; }
        public Nullable<long> Residence { get; set; }
        public Nullable<long> Degree { get; set; }
        public Nullable<long> Speciality { get; set; }
        public Nullable<long> SubSpeciality { get; set; }
        //public string VisaStatus { get; set; }
        public string FileBase64 { get; set; }
        public string DocExtension { get; set; }
    }
}

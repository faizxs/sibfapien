﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class ExhibitorBadgeDTO
    {
        public long? ExhibitorId { get; set; }
        public long? MemberId { get; set; }
        public string Name { get; set; }
        public string NameAr { get; set; }
        public string Email { get; set; }
        public string PhoneISD { get; set; }
        public string PhoneSTD { get; set; }
        public string PhoneNumber { get; set; }
        public string pdfFileName { get; set; }
        public string pdfBase64 { get; set; }
    }
    public class ExhibitorDetailsBadgeDTO
    {
        public long? ExhibitorId { get; set; }
        public string PublisherName { get; set; }
        public string PublisherNameAr { get; set; }
        public string PublisherEmail { get; set; }
        public string PublisherPhone { get; set; }
    }
    public class PCTGQRLinkDTO
    {
        public long? ItemId { get; set; }
    }
}

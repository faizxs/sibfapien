﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class AwardNominationFormDTO
    {
        public string hdnTempData { get; set; }
        //public long? ExhibitionId { get; set; }
        public long? AwardId { get; set; }
        public long? SubAwardId { get; set; }
        public string Title { get; set; }
        public string PublishedYear { get; set; }
        public string ISBN { get; set; }
        public string NominationType { get; set; }
        public string AuthorName { get; set; }
        public string AuthorISD { get; set; }
        public string AuthorSTD { get; set; }
        public string AuthorPhone { get; set; }
        public string AuthorEmail { get; set; }
        public long? AuthorNationalityId { get; set; }
        public string Publisher { get; set; }
        public string PublisherISD { get; set; }
        public string PublisherSTD { get; set; }
        public string PublisherPhone { get; set; }
        public string PublisherEmail { get; set; }
        public long? PublisherCountryId { get; set; }
        public string OtherCity { get; set; }
        public long? CityId { get; set; }
        public long? CountryId { get; set; }
        public string PublishersSpecialization { get; set; }
        public string EstablishedYear { get; set; }
        public string MailAddress { get; set; }
        public string POBox { get; set; }
        public string Phone { get; set; }
        public string ISD { get; set; }
        public string STD { get; set; }
        public string Fax { get; set; }
        public string FaxSTD { get; set; }
        public string FaxISD { get; set; }
        public string Email { get; set; }
        public string OwnerOfPubHouse { get; set; }
        public string Date { get; set; }
        public string PostalCode { get; set; }

        public string FirstParticipatedYear { get; set; }
        public long? NoOfPublications { get; set; }
        public long? NoOfPublicationsInCurrentYear { get; set; }
        public string GeneralManagerOfPubHouse { get; set; }

        public string Comments { get; set; }
        public string FileBase64 { get; set; }
        public string DocExtension { get; set; }
    }
    public class PCAwardNominationDTO
    {
        public string hdnTempData { get; set; }
        //public long? ExhibitionId { get; set; }
        public long? AwardId { get; set; }
        public string ParticipantName { get; set; }
        public string CompanyName { get; set; }
        public long? CountryId { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string ReasonToWinAward { get; set; }
        public string Phone { get; set; }
        public string ISD { get; set; }
        public string STD { get; set; }
        public string NameOne { get; set; }
        public string CompanyNameOne { get; set; }
        public string EmailOne { get; set; }
        public string Testimonial { get; set; }
        public string FileBase64 { get; set; }
        public string DocExtension { get; set; }
        public string NameTwo { get; set; }
        public string CompanyNameTwo { get; set; }
        public string EmailTwo { get; set; }
        public string TestimonialTwo { get; set; }
        public string FileTwoBase64 { get; set; }
        public string DocTwoExtension { get; set; }
    }
    public class TurjumanAwardNominationFormDTO
    {
        public string hdnTempData { get; set; }
        public Nullable<long> ExhibitionId { get; set; }
        public Nullable<long> AwardId { get; set; }
        public string TranslatedBookName { get; set; }
        public string PublishedYearOfFirstTranslation { get; set; }
        public string TranslatorName { get; set; }
        public string BookNameArabic { get; set; }
        public string PublishedYearOfFirstArabicTranslation { get; set; }
        public string AuthorName { get; set; }
        public string ForeignPublishName { get; set; }
        public Nullable<long> CountryId { get; set; }
        public Nullable<long> TranslationLanguageId { get; set; }
        public string CorrespondenceAddress { get; set; }
        public string POBox { get; set; }
        public string Email { get; set; }

        public string Phone { get; set; }
        public string ISD { get; set; }
        public string STD { get; set; }


        public string Fax { get; set; }
        public string FaxSTD { get; set; }
        public string FaxISD { get; set; }

        public string ArabPublishHouseName { get; set; }
        public Nullable<long> ArabPublishHouseCountryId { get; set; }

        public string ArabPublishHousePhone { get; set; }
        public string ArabISD { get; set; }
        public string ArabSTD { get; set; }

        public string ArabPublishHouseFax { get; set; }
        public string ArabPublishHouseFaxISD { get; set; }
        public string ArabPublishHouseFaxSTD { get; set; }


        public string ArabPublishHouseEmail { get; set; }
        public string ArabPublishHouseNameAdress { get; set; }
        public string ArabPublishHousePOBox { get; set; }

        public string ArabPublishHouseNominationBody { get; set; }
        public Nullable<System.DateTime> Date { get; set; }

        public string FileBase64 { get; set; }
        public string DocExtension { get; set; }
    }
    public class MemberAndExhibitorFinesDTO
    {
        public string hdnTempData { get; set; }
        public long? ExhibitionId { get; set; }
        public long? FineCategoryId { get; set; }
        public long? MemberId { get; set; }
        public long? ExhibitorId { get; set; }
    }
    public class MemberAndExhibitorFinesListDTO
    {
        public long ItemId { get; set; }
        public long? ExhibitionId { get; set; }
        public long? FineCategoryId { get; set; }
        public long? MemberId { get; set; }
        public long? ExhibitorId { get; set; }
        public string ExhibitionNumber { get; set; }
        public string FinesCategoryName { get; set; }
        public string UserName { get; set; }
        public string MemberName { get; set; }
        public string ExhibitorName { get; set; }
        public string Status { get; set; }
        public string CreatedOn { get; set; }
    }
}

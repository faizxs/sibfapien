﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class CategoryDTO
    {
        public CategoryDTO()
        {
            //this.CategoryDTO = new HashSet<CategoryDTO>();
        }
        public long ItemId { get; set; }
        public string Title { get; set; }
        public string Color { get; set; }
    }
    public class SubCategoryDTO
    {
        public SubCategoryDTO()
        {
            //this.CategoryDTO = new HashSet<CategoryDTO>();
        }
        public long ItemId { get; set; }
        public long CategoryId { get; set; }
        public string CategoryTitle { get; set; }
        public string Title { get; set; }
        public string Color { get; set; }
    }
    public class DropdownDataDTO
    {
        public long ItemId { get; set; }
        public string Title { get; set; }
    }
    public class AwardsDropdownDataDTO
    {
        public long ItemId { get; set; }
        public string Title { get; set; }
        public string RegistrationStartDate { get; set; }
        public string RegistrationEndDate { get; set; }
        public string RegistrationDeadline { get; set; }
    }
    public class ExhbitionAreaDTO
    {
        public string BoothSubSectionTitle { get; set; }
    }
    public class ExhibitionCountryNewDTO
    {
        public ExhibitionCountryNewDTO()
        {
            //this.CategoryDTO = new HashSet<CategoryDTO>();
        }
        public long ItemId { get; set; }
        public string Title { get; set; }
        public string CountryCode { get; set; }
        public string CountryCodeIsotwo { get; set; }
    }

    public class DashboardDTO
    {
        public long ExhibitorId { get; set; }
        public long RestaurantId { get; set; }
        public long MemberRoleId { get; set; }
        public List<ApplicationRoleDTO> ApplicationRoleDTO { get; set; }
    }
    public class ApplicationRoleDTO
    {
        public long ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public long ExhibitorId { get; set; }
        public long RestaurantId { get; set; }
        public string Icon { get; set; }
        public string URL { get; set; }
        public long WebsiteId { get; set; }
    }
    public class MainDashboardDTO
    {
        public long MemberId { get; set; }
        public long ExhibitionId { get; set; }
        public long ExhibitorId { get; set; }
        public long? AgencyId { get; set; }
        public long MemberRoleId { get; set; }
        public List<ApplicationRoleMainDTO> ApplicationRoleDTO { get; set; }
        public List<DashboardStatus> StatusList { get; set; }
    }
    public class ApplicationRoleMainDTO
    {
        public long ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public long ExhibitorId { get; set; }
        public string Icon { get; set; }
        public string URL { get; set; }
        public long WebsiteId { get; set; }
    }
    public class DashboardStatus
    {
        public string Title { get; set; }
        public string Status { get; set; }
        public string UpdatedOn { get; set; }
        public string StatusLabel { get; set; }
        public string UpdatedOnLabel { get; set; }
        public string StandNumber { get; set; }
        public string StandNumberLabel { get; set; }
        public string HallNumber { get; set; }
        public string HallNumberLabel { get; set; }
        public string StandSection { get; set; }
        public string StandSectionLabel { get; set; }
        public string StandSubSection { get; set; }
        public string StandSubSectionLabel { get; set; }
        public string AllocatedArea { get; set; }
        public string AllocatedAreaLabel { get; set; }
        public string BadgeURL { get; set; }
        public List<DashboardStatus> SubList { get; set; }
    }
}

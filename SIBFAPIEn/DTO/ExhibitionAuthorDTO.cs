﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.DTO
{
    public class AuthorDTO
    {
        public AuthorDTO()
        {
            //this.CategoryDTO = new HashSet<CategoryDTO>();
        }
        public long AuthorId { get; set; }
        public string Title { get; set; }
    }
}

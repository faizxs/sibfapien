﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Models;
using Microsoft.Extensions.Caching.Memory;

namespace SIBFAPIEn
{
    public static class CacheKeys
    {
        private const int cacheTime = 3600;
        public static string PagesContentNew { get { return "_PagesContentNew"; } }
        public static string Books { get { return "_Books"; } }
        public static string BookParticipating { get { return "_BookParticipating"; } }
        //public static string BookAuthors { get { return "_BookAuthors"; } }
        //public static string Authors { get { return "_Authors"; } }
        public static string Guest { get { return "_Guest"; } }
        public static string Exhibition { get { return "_Exhibition"; } }

        public static string ExhibitionCountry { get { return "_ExhibitionCountry"; } }
        public static string ExhibitionMemberApplicationYearlyCacheList { get { return "_ExhibitionMemberApplicationYearlyCacheList"; } }
        public static string ExhibitionExhibitorDetailsCacheList { get { return "_ExhibitionExhibitorDetailsCacheList"; } }
        public static string ExhibitionCategory { get { return "_ExhibitionCategory"; } }
        public static string EventCategory { get { return "_EventCategory"; } }
        public static string EventSubCategory { get { return "_EventSubCategory"; } }
        public static string Event { get { return "_Event"; } }
        public static string EventDate { get { return "_EventDate"; } }
        public static string EventGuest { get { return "_EventGuest"; } }
        public static List<XsiPagesContentNew> cachedPagesContentNew { get; set; }
        public static List<XsiExhibition> cachedExhibition { get; set; }
        public static List<XsiExhibitionCountry> cachedExhibitionCountry { get; set; }
        public static List<XsiExhibitionMemberApplicationYearly> cachedExhibitionMemberApplicationYearly { get; set; }

        public static List<XsiExhibitionBooks> cachedBooks { get; set; }
        public static List<XsiExhibitionBookParticipating> cachedBookParticipating { get; set; }
        //public static List<XsiExhibitionBookAuthor> cachedBookAuthors { get; set; }
        //public static List<XsiExhibitionAuthor> cachedAuthors { get; set; }
        public static List<XsiExhibitionCategory> cachedExhibitionCategory { get; set; }
        public static List<XsiExhibitionExhibitorDetails> cachedExhibitionExhibitorDetails { get; set; }

        public static List<XsiEventCategory> cachedEventCategory { get; set; }
        public static List<XsiEventSubCategory> cachedEventSubCategory { get; set; }
        public static List<XsiEvent> cachedEvents { get; set; }
        public static List<XsiEventDate> cachedEventDate { get; set; }

        public static List<XsiGuests> cachedGuest { get; set; }
        public static List<XsiEventGuest> cachedEventGuest { get; set; }

        #region Cache Methods
        public static List<XsiPagesContentNew> GetPagesContentNew(IMemoryCache _cache)
        {
            cachedPagesContentNew = (List<XsiPagesContentNew>)_cache.Get<dynamic>(PagesContentNew);
            if (cachedPagesContentNew == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedPagesContentNew = _cache.GetOrCreate(PagesContentNew, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiPagesContentNew.ToList();
                    });
                }
            }
            return cachedPagesContentNew;
        }
        public static List<XsiEventCategory> GetEventCategory(IMemoryCache _cache)
        {
            cachedEventCategory = (List<XsiEventCategory>)_cache.Get<dynamic>(EventCategory);
            if (cachedEventCategory == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedEventCategory = _cache.GetOrCreate(EventCategory, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiEventCategory.ToList();
                    });
                }
            }
            return cachedEventCategory;
        }
        public static List<XsiEventSubCategory> GetEventSubCategory(IMemoryCache _cache)
        {
            cachedEventSubCategory = (List<XsiEventSubCategory>)_cache.Get<dynamic>(EventSubCategory);
            if (cachedEventSubCategory == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedEventSubCategory = _cache.GetOrCreate(EventSubCategory, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiEventSubCategory.ToList();
                    });
                }
            }
            return cachedEventSubCategory;
        }
        public static List<XsiGuests> GetGuestsList(IMemoryCache _cache)
        {
            cachedGuest = (List<XsiGuests>)_cache.Get<dynamic>(Guest);
            if (cachedGuest == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedGuest = _cache.GetOrCreate(Guest, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiGuests.ToList();
                    });
                }
            }
            return cachedGuest;
        }
        public static List<XsiExhibitionBooks> GetBookList(IMemoryCache _cache)
        {
            cachedBooks = (List<XsiExhibitionBooks>)_cache.Get<dynamic>(Books);
            if (cachedBooks == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedBooks = _cache.GetOrCreate(Books, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiExhibitionBooks.ToList();
                    });
                }
            }
            return cachedBooks;
        }
        public static List<XsiExhibitionBookParticipating> GetBooksParticipatingList(IMemoryCache _cache)
        {
            cachedBookParticipating = (List<XsiExhibitionBookParticipating>)_cache.Get<dynamic>(BookParticipating);
            if (cachedBookParticipating == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedBookParticipating = _cache.GetOrCreate(BookParticipating, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiExhibitionBookParticipating.Where(i => i.ExhibitorId != null).ToList();
                    });
                }
            }
            return cachedBookParticipating;
        }
        //public static List<XsiExhibitionBookAuthor> GetBookAuthorsList(IMemoryCache _cache)
        //{
        //    cachedBookAuthors = (List<XsiExhibitionBookAuthor>)_cache.Get<dynamic>(BookAuthors);
        //    if (cachedBookAuthors == null)
        //    {
        //        using (sibfnewdbContext _context = new sibfnewdbContext())
        //        {
        //            cachedBookAuthors = _cache.GetOrCreate(BookAuthors, entry =>
        //            {
        //                entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
        //                return _context.XsiExhibitionBookAuthor.ToList();
        //            });
        //        }
        //    }
        //    return cachedBookAuthors;
        //}
        //public static List<XsiExhibitionAuthor> GetAuthorsList(IMemoryCache _cache)
        //{
        //    cachedAuthors = (List<XsiExhibitionAuthor>)_cache.Get<dynamic>(Authors);
        //    if (cachedAuthors == null)
        //    {
        //        using (sibfnewdbContext _context = new sibfnewdbContext())
        //        {
        //            cachedAuthors = _cache.GetOrCreate(Authors, entry =>
        //            {
        //                entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
        //                return _context.XsiExhibitionAuthor.ToList();
        //            });
        //        }
        //    }
        //    return cachedAuthors;
        //}
        public static List<XsiExhibition> GetExhibition(this IMemoryCache _cache)
        {
            cachedExhibition = (List<XsiExhibition>)_cache.Get<dynamic>(Exhibition);
            if (cachedExhibition == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedExhibition = _cache.GetOrCreate(Exhibition, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiExhibition.ToList();
                    });
                }
            }
            return cachedExhibition;
        }
        public static List<XsiExhibitionCountry> GetExhibitionCountry(this IMemoryCache _cache)
        {
            cachedExhibitionCountry = (List<XsiExhibitionCountry>)_cache.Get<dynamic>(ExhibitionCountry);
            if (cachedExhibitionCountry == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedExhibitionCountry = _cache.GetOrCreate(ExhibitionCountry, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiExhibitionCountry.ToList();
                    });
                }
            }
            return cachedExhibitionCountry;
        }
        public static List<XsiExhibitionMemberApplicationYearly> GetExhibitionMemberApplicationYearly(IMemoryCache _cache)
        {
            cachedExhibitionMemberApplicationYearly = (List<XsiExhibitionMemberApplicationYearly>)_cache.Get<dynamic>(ExhibitionMemberApplicationYearlyCacheList);
            if (cachedExhibitionMemberApplicationYearly == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedExhibitionMemberApplicationYearly = _cache.GetOrCreate(ExhibitionMemberApplicationYearlyCacheList, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiExhibitionMemberApplicationYearly.ToList();
                    });
                }
            }
            return cachedExhibitionMemberApplicationYearly;
        }
        public static List<XsiExhibitionExhibitorDetails> GetExhibitionExhibitorDetails(IMemoryCache _cache)
        {
            cachedExhibitionExhibitorDetails = (List<XsiExhibitionExhibitorDetails>)_cache.Get<dynamic>(ExhibitionExhibitorDetailsCacheList);
            if (cachedExhibitionExhibitorDetails == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedExhibitionExhibitorDetails = _cache.GetOrCreate(ExhibitionExhibitorDetailsCacheList, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiExhibitionExhibitorDetails.ToList();
                    });
                }
            }
            return cachedExhibitionExhibitorDetails;
        }
        public static List<XsiExhibitionCategory> GetExhibitionCategory(IMemoryCache _cache)
        {
            cachedExhibitionCategory = (List<XsiExhibitionCategory>)_cache.Get<dynamic>(ExhibitionCategory);
            if (cachedExhibitionCategory == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedExhibitionCategory = _cache.GetOrCreate(ExhibitionCategory, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiExhibitionCategory.ToList();
                    });
                }
            }
            return cachedExhibitionCategory;
        }
        public static List<XsiEvent> GetEvents(IMemoryCache _cache)
        {
            cachedEvents = (List<XsiEvent>)_cache.Get<dynamic>(Event);
            if (cachedEvents == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedEvents = _cache.GetOrCreate(Event, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiEvent.ToList();
                    });
                }
            }
            return cachedEvents;
        }
        public static List<XsiEventDate> GetEventDate(IMemoryCache _cache)
        {
            cachedEventDate = (List<XsiEventDate>)_cache.Get<dynamic>(EventDate);
            if (cachedEventDate == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedEventDate = _cache.GetOrCreate(EventDate, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiEventDate.ToList();
                    });
                }
            }
            return cachedEventDate;
        }
        public static List<XsiEventGuest> GetEventGuestFromCache(IMemoryCache _cache)
        {
            cachedEventGuest = (List<XsiEventGuest>)_cache.Get<dynamic>(EventGuest);
            if (cachedEventGuest == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    cachedEventGuest = _cache.GetOrCreate(EventGuest, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(cacheTime);
                        return _context.XsiEventGuest.ToList();
                    });
                }
            }
            return cachedEventGuest;
        }
        public static void RemoveCacheForAll(IMemoryCache _cache)
        {
            _cache.Remove(Books);
            _cache.Remove(BookParticipating);
            //_cache.Remove(BookAuthors);
            //_cache.Remove(Authors);
            _cache.Remove(Guest);
            _cache.Remove(Exhibition);
            _cache.Remove(ExhibitionMemberApplicationYearlyCacheList);
            _cache.Remove(ExhibitionExhibitorDetailsCacheList);
            _cache.Remove(ExhibitionCategory);
            _cache.Remove(EventCategory);
            _cache.Remove(EventSubCategory);
            _cache.Remove(Event);
            _cache.Remove(EventDate);
            _cache.Remove(EventGuest);
        }
        public static void AddCacheForAll(IMemoryCache _cache)
        {
            GetEventCategory(_cache);
            GetEventSubCategory(_cache);
            GetGuestsList(_cache);
            GetBookList(_cache);
            GetBooksParticipatingList(_cache);
            //GetBookAuthorsList(_cache);
            //GetAuthorsList(_cache);
            GetExhibition(_cache);
            GetExhibitionMemberApplicationYearly(_cache);
            GetExhibitionExhibitorDetails(_cache);
            GetExhibitionCategory(_cache);
            GetEvents(_cache);
            GetEventDate(_cache);
            GetEventGuestFromCache(_cache);
        }
        #endregion

    }
}

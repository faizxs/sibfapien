﻿
public enum EnumPageContent : int
{

    Home = 1,
    ErrorPage = 42,
    TermsOfUse = 43,
    LegalPrivacy = 44,
    TermsAndConditions = 46,
    EnContactUs = 3,
    EnNews = 56,
    EnMemberRegistration = 61,
    EnMembersLogin = 66,
    EnPhotoGallery = 66,
    EnVideoGallery = 71,
    EnMemberLogin = 76,
    EnPolls = 81
}
public static partial class EnumConversion
{
    public static string ToString(EnumPageContent enumPageItemId)
    {
        switch (enumPageItemId)
        {
            case EnumPageContent.Home:
                return "1";
            case EnumPageContent.ErrorPage:
                return "2";
            default:
                return null;
        }
    }
}
﻿public enum EnumSortlistBy : int
{
    ByAlphabetAsc,
    ByAlphabetDesc,
    ByDateAsc,
    ByDateDesc,
    ByItemIdAsc,
    ByItemIdDesc,
    ByItemOrder,
    BySortOrderAsc,
    BySortOrderDesc,
    ByCustomSort
}
public enum EnumGender : int
{
    Male,
    Female
}
public enum EnumBool : int
{
    Yes,
    No
}
public enum EnumEmailCategory : int
{
    ContactUs = 1,
    ActivitySuggestion = 2,
    SchoolRegistration = 3
}
public enum EnumOtherText : int
{
    OtherCity,
    OtherCityAr
}
public enum EnumExhibitionMemberStatus : int
{
    New,
    Pending,
    Approved,
    Rejected,
    EmailNotVerified,
    EmailVerified,
    CredentialNotSent,
    Banned
}
public enum EnumSelectAllState : int
{
    Clicked,
    Unclicked,
    Nothing
}
public enum EnumExhibitorEditStatus : int
{
    New,
    Rejected,
    Approved,
    NoEdit
}
public enum EnumExhibitionMemberEditStatus : int
{
    New,
    Rejected,
    Approved,
    NoEdit
}
public enum EnumAwardNominationStatus : int
{
    New,
    Reviewed,
    Accepted,
    Rejected,
    Winner
}
public enum EnumNominationType : int
{
    Personal,
    Publisher
}
public enum EnumExemptionType : int
{
    Exemption,
    PartExemption,
    None
}
public enum EnumDiscountStatus : int
{
    Pending,
    Approved,
    Rejected
}
public enum TradeType : int
{
    Buying,
    Selling,
    Both
}
public enum EnumSCRFMemberStatus : int
{
    New,
    Pending,
    Approved,
    Rejected,
    EmailNotVerified,
    EmailVerfied,
    CredentialNotSent
}
public enum EnumRepresentativeStatus : int
{
    Approved,
    Pending,
    FlightDetailsPending,
    PendingDocumentation,
    VisaProcessing,
    VisaUploaded,
    Rejected,
    DocumentsApproved,
    Cancelled,
    DocumentsUploaded,
    Blocked,
    ReIssueVisa
}
public enum EnumTranslationGrantIBANorSwiftCodeStatus : int
{
    IBAN,
    SwiftCode,
    Both,
}
public enum EnumEventType : int
{
    Event,
    Activity,
    Both
}
public enum EnumTranslationGrantMemberStatus : int
{
    Temporary,
    UnderEvaluation,
    New,
    IncorrectDocuments,
    BankDetailsResubmitted,
    Approved,
    Rejected,
    BankDetailsSubmitted,
    ContractUploaded,
    ContractSubmitted,
    FirstPaymentUploaded,
    DraftSubmitted,
    SecondPaymentUploaded,
    FinalPaymentUploaded,
    Canceled,
    PendingDocumentation,
    DocumentationResubmitted,
    ResubmitDraft,
    DraftResubmitted,
    TemporaryViewed,
    UserCancellationPending,
    CancelledByUser
}
public enum EnumTranslationGrantMemberOriginalBookStatus : int
{
    SoftCopyofOriginalBook,
    TranslatedFirstChapter,
    SentAsHardCopy
}
public enum EnumExhibitorStatus : int
{
    New,
    Pending,
    SampleReceived,
    InitialApproval,
    OnlinePayment,
    Paid,
    PartiallyPaid,
    PartiallyPaidBalanceForwardToNewInvoice,
    Approved,
    Rejected,
    Cancelled,
    Step2,
    ReceiptUploaded,
    PaymentInProcess,
    PendingApproval,
    PendingDocumentation,
    PendingPayment,
    EditRequest
}
public enum EnumExhibitorReceiptStatus : int
{
    Cancelled,
    EReceiptUploaded
}
public enum EnumSCRFIllustrationRegistrationStatus : int
{
    Temporary,
    New,
    ArtworkSubmittedOnline,
    Pending,
    Received,
    Approved,
    Winner,
    Rejected
}
public enum EnumSCRFIllustrationSubmissionStatus : int
{
    Temporary,
    New,
    Reviewed,
    Pending,
    ReSubmitted,
    Received,
    Approved,
    Winner,
    Rejected
}
public enum EnumProfessionalProgramStatus : int
{
    New,
    Pending,
    Approved,
    Rejected
}
public enum EnumProfessionalProgramEditStatus : int
{
    New,
    Rejected,
    Approved,
    NoEdit
}
public enum EnumProfessionalProgramNotificationStatus : int
{
    Pending,
    No,
    Yes,
}
public enum EnumAgencyStatus : int
{
    New,
    ExhibitorApproved,
    SIBFApproved,
    ExhibitorReject,
    SIBFReject,
    Pending
}
public enum EnumPublisherStatus : int
{
    New,
    Approved,
    Merge
}
public enum EnumAuthorStatus : int
{
    New,
    Approved,
    Merge
}
public enum EnumStatus : int
{
    New,
    Active,
    Inactive,
    Approved,
    Rejected,
    Pending,
    Open,
    CLose,
    Error
}
public enum EnumTranslationType : int
{
    ArabicToAnyLanguage,
    AnyLanguageToArabic,
    AnyLanguageToAnyLanguage
}
public enum EnumPaymentType : int
{
    PaidByCash,
    PaidByBankTransfer,
    PaidByChequeTransfer,
    PaidByOnline
}
public enum EnumWebsiteId : int
{
    SIBF = 1,
    SCRF = 2,
    General = 3,
}
public enum EnumWebsiteTypeId : int
{
    Both = 0,
    SIBF = 1,
    SCRF = 2
}
public enum EnumMessageType : int
{
    Error,
    Success,
    Info
}
public enum EnumRequiredAreaType : int
{
    Default,
    Open,
    NoShelves
}
public enum EnumViewType : int
{
    Default,
    Add,
    Edit,
    View,
    Merge,
    ViewMore,
    Sort,
    Create,
    Email,
    AdminPermissions,
    EditPassword,
    EditExhibitor,
    EditAgency,
    ExportExcel,
    DisplayBooks
}
public enum EnumRegistrationViewType : int
{
    Default,
    Step1,
    Step2,
    Step3,
    Step4,
    Step5,
    Step6,
    ThankYou,
    StepReview,
    ExhibitionUnavailableView,
    RegistrationExpired,
    RegisteredAsAgent,
    RegisteredAsExhibitor,
    Reject
}
public enum EnumCareerApplicantStatus : int
{
    New,
    Shortlist,
    Reject
}

public enum EnumFlag : int
{
    A,
    B,
    C,
    D,
    E,
    Default
}
public enum EnumFlagableModule : int
{
    Contactus,
    ChallengeSolution,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    Default
}
public enum EnumFileType : int
{
    Document,
    Photo,
    Video,
    Audio,
    Others
}

public enum EnumHomePageSection : int
{
    SectionOne = 1,
    SectionTwo = 3,
    SectionThree = 5,
    SectionFour = 7,
    SectionFive = 9,
    SectionSix = 11,
    SectionSeven = 13,
    SectionEight = 15,
    SectionNine = 17
}

public static partial class EnumConversion
{
    public static string ToString(EnumGender enumGender)
    {
        switch (enumGender)
        {
            case EnumGender.Male:
                return "M";
            case EnumGender.Female:
                return "F";
            default:
                return null;
        }
    }

    public static string ToString(EnumBool enumBool)
    {
        switch (enumBool)
        {
            case EnumBool.Yes:
                return "Y";
            case EnumBool.No:
                return "N";
            default:
                return null;
        }
    }
    public static string ToString(EnumFlag enumFlag)
    {
        switch (enumFlag)
        {
            case EnumFlag.A:
                return "A";
            case EnumFlag.B:
                return "B";
            case EnumFlag.C:
                return "C";
            case EnumFlag.D:
                return "D";
            case EnumFlag.E:
                return "E";
            case EnumFlag.Default:
                return "A";
            default:
                return "A";
        }
    }
    public static string ToString(EnumFlagableModule enumModule)
    {
        switch (enumModule)
        {
            case EnumFlagableModule.Contactus:
                return "A";
            case EnumFlagableModule.ChallengeSolution:
                return "B";
            case EnumFlagableModule.C:
                return "C";
            case EnumFlagableModule.D:
                return "D";
            case EnumFlagableModule.E:
                return "E";
            case EnumFlagableModule.F:
                return "F";
            case EnumFlagableModule.G:
                return "G";
            case EnumFlagableModule.H:
                return "H";
            case EnumFlagableModule.I:
                return "I";
            case EnumFlagableModule.J:
                return "J";

            case EnumFlagableModule.Default:
                return "Z";
            default:
                return "Z";
        }
    }
    public static string ToString(EnumStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumStatus.Active:
                return "A";
            case EnumStatus.Approved:
                return "B";
            case EnumStatus.CLose:
                return "C";
            case EnumStatus.Rejected:
                return "D";
            case EnumStatus.Inactive:
                return "I";
            case EnumStatus.New:
                return "N";
            case EnumStatus.Open:
                return "O";
            case EnumStatus.Pending:
                return "P";
            case EnumStatus.Error:
                return "Z";
            default:
                return null;
        }
    }
    public static string ToString(EnumTranslationType enumStatus)
    {
        switch (enumStatus)
        {
            case EnumTranslationType.ArabicToAnyLanguage:
                return "A";
            case EnumTranslationType.AnyLanguageToArabic:
                return "B";
            case EnumTranslationType.AnyLanguageToAnyLanguage:
                return "C";
            default:
                return null;
        }
    }
    public static string ToString(EnumTranslationGrantMemberOriginalBookStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumTranslationGrantMemberOriginalBookStatus.SoftCopyofOriginalBook:
                return "S";
            case EnumTranslationGrantMemberOriginalBookStatus.TranslatedFirstChapter:
                return "T";
            case EnumTranslationGrantMemberOriginalBookStatus.SentAsHardCopy:
                return "H";
            default:
                return null;
        }
    }
    public static string ToString(EnumSCRFIllustrationRegistrationStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumSCRFIllustrationRegistrationStatus.Temporary:
                return "T";
            case EnumSCRFIllustrationRegistrationStatus.New:
                return "N";
            case EnumSCRFIllustrationRegistrationStatus.ArtworkSubmittedOnline:
                return "V";
            case EnumSCRFIllustrationRegistrationStatus.Pending:
                return "P";
            case EnumSCRFIllustrationRegistrationStatus.Received:
                return "S";
            case EnumSCRFIllustrationRegistrationStatus.Approved:
                return "A";
            case EnumSCRFIllustrationRegistrationStatus.Winner:
                return "W";
            case EnumSCRFIllustrationRegistrationStatus.Rejected:
                return "R";
            default:
                return null;
        }
    }
    public static string ToString(EnumSCRFIllustrationSubmissionStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumSCRFIllustrationSubmissionStatus.Temporary:
                return "T";
            case EnumSCRFIllustrationSubmissionStatus.New:
                return "N";
            case EnumSCRFIllustrationSubmissionStatus.Reviewed:
                return "V";
            case EnumSCRFIllustrationSubmissionStatus.Pending:
                return "P";
            case EnumSCRFIllustrationSubmissionStatus.ReSubmitted:
                return "Q";
            case EnumSCRFIllustrationSubmissionStatus.Received:
                return "S";
            case EnumSCRFIllustrationSubmissionStatus.Approved:
                return "A";
            case EnumSCRFIllustrationSubmissionStatus.Winner:
                return "W";
            case EnumSCRFIllustrationSubmissionStatus.Rejected:
                return "R";
            default:
                return null;
        }
    }
    public static long ToLong(EnumPaymentType enumStatus)
    {
        switch (enumStatus)
        {
            case EnumPaymentType.PaidByCash:
                return 0;
            case EnumPaymentType.PaidByBankTransfer:
                return 1;
            case EnumPaymentType.PaidByChequeTransfer:
                return 2;
            case EnumPaymentType.PaidByOnline:
                return 3;
            default:
                return -1;
        }
    }
    public static string ToString(EnumPaymentType enumStatus)
    {
        switch (enumStatus)
        {
            case EnumPaymentType.PaidByCash:
                return "0";
            case EnumPaymentType.PaidByBankTransfer:
                return "1";
            case EnumPaymentType.PaidByChequeTransfer:
                return "2";
            case EnumPaymentType.PaidByOnline:
                return "3";
            default:
                return "-1";
        }
    }
    public static long ToString(EnumEmailCategory enumEmail)
    {
        switch (enumEmail)
        {
            case EnumEmailCategory.ContactUs:
                return 1;
            case EnumEmailCategory.ActivitySuggestion:
                return 2;
            case EnumEmailCategory.SchoolRegistration:
                return 3;
            default:
                return -1;
        }
    }
    public static string ToString(EnumOtherText enumOtherText)
    {
        switch (enumOtherText)
        {
            case EnumOtherText.OtherCity:
                return "Other";
            case EnumOtherText.OtherCityAr:
                return "أخرى";

            default:
                return null;
        }
    }
    public static string ToString(EnumExhibitionMemberStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumExhibitionMemberStatus.New:
                return "N";
            case EnumExhibitionMemberStatus.Pending:
                return "P";
            case EnumExhibitionMemberStatus.Approved:
                return "A";
            case EnumExhibitionMemberStatus.Rejected:
                return "R";
            case EnumExhibitionMemberStatus.EmailNotVerified:
                return "E";
            case EnumExhibitionMemberStatus.EmailVerified:
                return "V";
            case EnumExhibitionMemberStatus.CredentialNotSent:
                return "C";
            case EnumExhibitionMemberStatus.Banned:
                return "B";
            default:
                return null;
        }
    }
    public static string ToString(EnumSelectAllState enumStatus)
    {
        switch (enumStatus)
        {
            case EnumSelectAllState.Clicked:
                return "C";
            case EnumSelectAllState.Unclicked:
                return "U";
            case EnumSelectAllState.Nothing:
                return "N";
            default:
                return null;
        }
    }
    public static string ToString(EnumExhibitorEditStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumExhibitorEditStatus.New:
                return "N";
            case EnumExhibitorEditStatus.Rejected:
                return "R";
            case EnumExhibitorEditStatus.Approved:
                return "A";
            case EnumExhibitorEditStatus.NoEdit:
                return "O";
            default:
                return null;
        }
    }
    public static string ToString(EnumAwardNominationStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumAwardNominationStatus.New:
                return "N";
            case EnumAwardNominationStatus.Reviewed:
                return "O";
            case EnumAwardNominationStatus.Accepted:
                return "A";
            case EnumAwardNominationStatus.Rejected:
                return "R";
            case EnumAwardNominationStatus.Winner:
                return "W";
            default:
                return null;
        }
    }
    public static string ToString(EnumExhibitionMemberEditStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumExhibitionMemberEditStatus.New:
                return "N";
            case EnumExhibitionMemberEditStatus.Rejected:
                return "R";
            case EnumExhibitionMemberEditStatus.Approved:
                return "A";
            case EnumExhibitionMemberEditStatus.NoEdit:
                return "O";
            default:
                return null;
        }
    }
    public static string ToString(EnumNominationType enumStatus)
    {
        switch (enumStatus)
        {
            case EnumNominationType.Personal:
                return "A";
            case EnumNominationType.Publisher:
                return "B";
            default:
                return null;
        }
    }
    public static string ToString(EnumExemptionType enumStatus)
    {
        switch (enumStatus)
        {
            case EnumExemptionType.Exemption:
                return "W";
            case EnumExemptionType.PartExemption:
                return "P";
            case EnumExemptionType.None:
                return "N";
            default:
                return null;
        }
    }
    public static string ToString(EnumDiscountStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumDiscountStatus.Pending:
                return "P";
            case EnumDiscountStatus.Approved:
                return "A";
            case EnumDiscountStatus.Rejected:
                return "R";
            default:
                return null;
        }
    }
    public static string ToString(TradeType enumStatus)
    {
        switch (enumStatus)
        {
            case TradeType.Buying:
                return "B";
            case TradeType.Selling:
                return "S";
            case TradeType.Both:
                return "A";
            default:
                return null;
        }
    }
    public static string ToString(EnumSCRFMemberStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumSCRFMemberStatus.New:
                return "N";
            case EnumSCRFMemberStatus.Pending:
                return "P";
            case EnumSCRFMemberStatus.Approved:
                return "A";
            case EnumSCRFMemberStatus.Rejected:
                return "R";
            case EnumSCRFMemberStatus.EmailNotVerified:
                return "E";
            case EnumSCRFMemberStatus.EmailVerfied:
                return "V";
            case EnumSCRFMemberStatus.CredentialNotSent:
                return "C";
            default:
                return null;
        }
    }
    public static string ToString(EnumRepresentativeStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumRepresentativeStatus.Approved:
                return "A";
            case EnumRepresentativeStatus.Pending:
                return "P";
            case EnumRepresentativeStatus.FlightDetailsPending:
                return "F";
            case EnumRepresentativeStatus.PendingDocumentation:
                return "Y";
            case EnumRepresentativeStatus.VisaProcessing:
                return "V";
            case EnumRepresentativeStatus.VisaUploaded:
                return "U";
            case EnumRepresentativeStatus.Rejected:
                return "R";
            case EnumRepresentativeStatus.DocumentsApproved:
                return "O";
            case EnumRepresentativeStatus.Cancelled:
                return "D";
            case EnumRepresentativeStatus.DocumentsUploaded:
                return "L";
            case EnumRepresentativeStatus.Blocked:
                return "B";
            case EnumRepresentativeStatus.ReIssueVisa:
                return "W";
            default:
                return null;
        }
    }


    public static string ToString(EnumTranslationGrantIBANorSwiftCodeStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumTranslationGrantIBANorSwiftCodeStatus.IBAN:
                return "I";
            case EnumTranslationGrantIBANorSwiftCodeStatus.SwiftCode:
                return "S";
            case EnumTranslationGrantIBANorSwiftCodeStatus.Both:
                return "B";
            default:
                return null;
        }
    }

    public static string ToString(EnumEventType enumStatus)
    {
        switch (enumStatus)
        {
            case EnumEventType.Event:
                return "E";
            case EnumEventType.Activity:
                return "A";
            case EnumEventType.Both:
                return "B";
            default:
                return null;
        }
    }
    public static string ToString(EnumTranslationGrantMemberStatus enumStatus)
    {
        switch (enumStatus)
        {

            case EnumTranslationGrantMemberStatus.Temporary:
                return "T";
            case EnumTranslationGrantMemberStatus.UnderEvaluation:
                return "V";
            case EnumTranslationGrantMemberStatus.New:
                return "N";
            case EnumTranslationGrantMemberStatus.IncorrectDocuments:
                return "I";
            case EnumTranslationGrantMemberStatus.BankDetailsResubmitted:
                return "S";
            case EnumTranslationGrantMemberStatus.Approved:
                return "A";
            case EnumTranslationGrantMemberStatus.Rejected:
                return "R";
            case EnumTranslationGrantMemberStatus.BankDetailsSubmitted:
                return "B";
            case EnumTranslationGrantMemberStatus.ContractUploaded:
                return "U";
            case EnumTranslationGrantMemberStatus.ContractSubmitted:
                return "C";
            case EnumTranslationGrantMemberStatus.FirstPaymentUploaded:
                return "F";
            case EnumTranslationGrantMemberStatus.DraftSubmitted:
                return "D";
            case EnumTranslationGrantMemberStatus.SecondPaymentUploaded:
                return "P";
            case EnumTranslationGrantMemberStatus.FinalPaymentUploaded:
                return "Z";
            case EnumTranslationGrantMemberStatus.Canceled:
                return "X";
            case EnumTranslationGrantMemberStatus.PendingDocumentation:
                return "M";
            case EnumTranslationGrantMemberStatus.DocumentationResubmitted:
                return "O";
            case EnumTranslationGrantMemberStatus.ResubmitDraft:
                return "Q";
            case EnumTranslationGrantMemberStatus.DraftResubmitted:
                return "E";
            case EnumTranslationGrantMemberStatus.TemporaryViewed:
                return "Y";
            case EnumTranslationGrantMemberStatus.UserCancellationPending:
                return "G";
            case EnumTranslationGrantMemberStatus.CancelledByUser:
                return "W";
            default:
                return null;
        }
    }
    public static string ToString(EnumExhibitorStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumExhibitorStatus.New:
                return "N";
            case EnumExhibitorStatus.Pending:
                return "P";
            case EnumExhibitorStatus.SampleReceived:
                return "H";
            case EnumExhibitorStatus.InitialApproval:
                return "I";
            case EnumExhibitorStatus.OnlinePayment:
                return "O";
            case EnumExhibitorStatus.Paid:
                return "D";
            case EnumExhibitorStatus.PartiallyPaid:
                return "F";
            case EnumExhibitorStatus.PartiallyPaidBalanceForwardToNewInvoice:
                return "B";
            case EnumExhibitorStatus.Approved:
                return "A";
            case EnumExhibitorStatus.Rejected:
                return "R";
            case EnumExhibitorStatus.Cancelled:
                return "C";
            case EnumExhibitorStatus.Step2:
                return "S";
            case EnumExhibitorStatus.ReceiptUploaded:
                return "U";
            case EnumExhibitorStatus.PaymentInProcess:
                return "V";
            case EnumExhibitorStatus.PendingApproval:
                return "X";
            case EnumExhibitorStatus.PendingDocumentation:
                return "Y";
            case EnumExhibitorStatus.PendingPayment:
                return "Z";
            case EnumExhibitorStatus.EditRequest:
                return "E";
            default:
                return null;
        }
    }
    public static string ToString(EnumExhibitorReceiptStatus enumStatus)
    {
        switch (enumStatus)
        {

            case EnumExhibitorReceiptStatus.Cancelled:
                return "C";
            case EnumExhibitorReceiptStatus.EReceiptUploaded:
                return "U";

            default:
                return null;
        }
    }

    public static string ToString(EnumProfessionalProgramStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumProfessionalProgramStatus.New:
                return "N";
            case EnumProfessionalProgramStatus.Pending:
                return "P";
            case EnumProfessionalProgramStatus.Approved:
                return "A";
            case EnumProfessionalProgramStatus.Rejected:
                return "R";
            default:
                return null;
        }
    }
    public static string ToString(EnumProfessionalProgramEditStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumProfessionalProgramEditStatus.New:
                return "N";
            case EnumProfessionalProgramEditStatus.Rejected:
                return "R";
            case EnumProfessionalProgramEditStatus.Approved:
                return "A";
            case EnumProfessionalProgramEditStatus.NoEdit:
                return "O";
            default:
                return null;
        }
    }

    public static string ToString(EnumProfessionalProgramNotificationStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumProfessionalProgramNotificationStatus.Pending:
                return "P";
            case EnumProfessionalProgramNotificationStatus.No:
                return "N";
            case EnumProfessionalProgramNotificationStatus.Yes:
                return "Y";
            default:
                return null;
        }
    }

    public static string ToString(EnumAgencyStatus enumStatus)
    {
        switch (enumStatus)
        {
            case EnumAgencyStatus.New:
                return "N";
            case EnumAgencyStatus.ExhibitorApproved:
                return "E";
            case EnumAgencyStatus.SIBFApproved:
                return "A";
            case EnumAgencyStatus.ExhibitorReject:
                return "R";
            case EnumAgencyStatus.SIBFReject:
                return "C";
            case EnumAgencyStatus.Pending:
                return "P";
            default:
                return null;
        }
    }
    public static string ToString(EnumFileType enumFileType)
    {
        switch (enumFileType)
        {
            case EnumFileType.Audio:
                return "A";
            case EnumFileType.Document:
                return "D";
            case EnumFileType.Photo:
                return "P";
            case EnumFileType.Video:
                return "V";
            case EnumFileType.Others:
                return "Z";
            default:
                return null;
        }
    }
    public static string ToString(EnumRequiredAreaType enumRequiredAreaType)
    {
        switch (enumRequiredAreaType)
        {
            case EnumRequiredAreaType.Default:
                return "D";
            case EnumRequiredAreaType.Open:
                return "O";
            case EnumRequiredAreaType.NoShelves:
                return "N";
            default:
                return null;
        }
    }

    public static string ToString(EnumPublisherStatus enumPublisherStatus)
    {
        switch (enumPublisherStatus)
        {
            case EnumPublisherStatus.New:
                return "N";
            case EnumPublisherStatus.Approved:
                return "A";
            case EnumPublisherStatus.Merge:
                return "M";
            default:
                return null;
        }
    }

    public static string ToString(EnumAuthorStatus enumPublisherStatus)
    {
        switch (enumPublisherStatus)
        {
            case EnumAuthorStatus.New:
                return "N";
            case EnumAuthorStatus.Approved:
                return "A";
            case EnumAuthorStatus.Merge:
                return "M";
            default:
                return null;
        }
    }
    public static string ToString(EnumCareerApplicantStatus enumstatus)
    {
        switch (enumstatus)
        {
            case EnumCareerApplicantStatus.New:
                return "N";
            case EnumCareerApplicantStatus.Shortlist:
                return "S";
            case EnumCareerApplicantStatus.Reject:
                return "R";
            default:
                return "N";
        }
    }


}
﻿using Contracts;
//using MailKit.Net.Smtp;
//using MailKit.Security;
using Microsoft.Extensions.Options;
//using MimeKit;
//using System.Net.Mail;
using SIBFAPIEn.DTO;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace SIBFAPIEn.Utility
{
    public interface IEmailSender
    {
        EnumResultType SendEmail(string from, string fromemail, string email, string subject, string htmlMessage, List<string> MailAttachments = null);
    }

    public class SmtpEmail : IEmailSender
    {
        private readonly EmailSettings _emailSettings;
        private readonly SCRFEmailSettings _scrfemailSettings;

        private static ILoggerManager _logger;
        public SmtpEmail(IOptions<EmailSettings> emailConfig, IOptions<SCRFEmailSettings> scrfemailConfig)
        {
            this._emailSettings = emailConfig.Value;
            this._scrfemailSettings = scrfemailConfig.Value;
        }
        public EnumResultType SendEmail(string From, string FromEmail, string ToEmail, string Subject, string Body, List<string> MailAttachments)
        {
            EnumResultType XsiResult = EnumResultType.Failed;
            try
            {
                if (!string.IsNullOrEmpty(ToEmail))
                {
                    #region Code without Mandrill

                    string MailServer = _emailSettings.MailServer;
                    string MailServerEmail = _emailSettings.MailServerEmail;
                    string MailServerPassword = _emailSettings.MailServerPassword;
                    string MailServerSSL = _emailSettings.MailServerSSL;
                    string MailServerPort = _emailSettings.MailServerPort.ToString();

                    if (_scrfemailSettings != null && _scrfemailSettings != default(SCRFEmailSettings))
                    {
                        if (Body.IndexOf("Sharjah Children’s Reading Festival") > -1 || Body.IndexOf("OTP for SCRF Member login") > -1)
                        {
                            MailServer = _scrfemailSettings.MailServer;
                            MailServerEmail = _scrfemailSettings.MailServerEmail;
                            MailServerPassword = _scrfemailSettings.MailServerPassword;
                            MailServerSSL = _scrfemailSettings.MailServerSSL;
                            MailServerPort = _scrfemailSettings.MailServerPort.ToString();
                            FromEmail = MailServerEmail;
                        }
                    }

                    string[] emailAddresses = ToEmail.Split(',');

                    System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    message.From = new MailAddress(FromEmail, From, System.Text.UnicodeEncoding.UTF8);
                    foreach (string emailAddress in emailAddresses)
                        message.To.Add(new MailAddress(emailAddress));

                    if (MailAttachments != null && MailAttachments.Count > 0)
                    {
                        foreach (string attachment in MailAttachments)
                        {
                            if (System.IO.File.Exists(attachment))
                                message.Attachments.Add(new Attachment(attachment));
                        }
                    }

                    message.SubjectEncoding = System.Text.UnicodeEncoding.UTF8;
                    message.Subject = Subject;
                    message.Body = Body;
                    message.IsBodyHtml = true;

                    SmtpClient client = new SmtpClient();
                    client.UseDefaultCredentials = false;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.Timeout = 100000;
                    client.Host = MailServer;

                    int portnumnumber = 25;
                    if (Int32.TryParse(MailServerPort, out portnumnumber))
                        client.Port = portnumnumber;

                    if (MailServerSSL == "true")
                    {
                        client.Port = Convert.ToInt32(MailServerPort);
                        client.EnableSsl = true;
                    }
                    else
                        client.EnableSsl = false;
                    client.Credentials =
                    new System.Net.NetworkCredential(MailServerEmail, MailServerPassword);

                    client.Send(message);

                    XsiResult = EnumResultType.Success;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                //using (System.IO.StreamWriter file = new System.IO.StreamWriter("D:\\vhosts\\xsi.sibf.com\\Content\\sibfapiemail.txt", true))
                //{
                //    file.WriteLine("------------------------------------");
                //    file.WriteLine(DateTime.Now);
                //    file.WriteLine("InnerException:" + ex.Message);
                //    file.WriteLine("StackTrace:" + ex.StackTrace);
                //    file.WriteLine("------------------------------------");
                //}
                _logger.LogError($"Something went wrong inside SendEmail : {ex.InnerException}");
                return XsiResult;
            }
            return XsiResult;
        }
    }
}

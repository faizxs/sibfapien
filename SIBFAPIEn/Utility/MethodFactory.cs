﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Razor.Language.Intermediate;
using Microsoft.EntityFrameworkCore;
using NLog;
using SIBFAPIEn.DTO;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using Xsi.ServicesLayer;
using Entities.Models;
using OfficeOpenXml;
using ImageMagick;
using Microsoft.Extensions.Caching.Memory;
using GIGTools;
using Newtonsoft.Json;
using System.Net;
using Newtonsoft.Json.Linq;
using Org.BouncyCastle.Asn1.Ocsp;
using Xsi.BusinessLogicLayer;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;

namespace SIBFAPIEn
{
    public static class MethodFactory
    {
        public static ILoggerManager _logger;
        // public static static readonly AppCustomSettings _appCustomSettings;
        public static readonly string RevenueSystemBillingAPI = "https://billing.sibf.com/";
        #region Utilities
        public static DateTime ArabianTimeNow()
        {
            TimeZoneInfo AEZone = TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time");
            DateTime datetimeNow = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, AEZone);
            return datetimeNow;
        }
        public static string GenerateOTP(int uppercase, int numerics)
        {
            // string lowers = "abcdefghijklmnopqrstuvwxyz";
            string uppers = "0123456789";
            string number = "0123456789";

            Random random = new Random();

            string generated = "!";
            //for (int i = 1; i <= lowercase; i++)
            //    generated = generated.Insert(
            //        random.Next(generated.Length),
            //        lowers[random.Next(lowers.Length - 1)].ToString()
            //    );

            for (int i = 1; i <= uppercase; i++)
                generated = generated.Insert(
                    random.Next(generated.Length),
                    uppers[random.Next(uppers.Length - 1)].ToString()
                );

            for (int i = 1; i <= numerics; i++)
                generated = generated.Insert(
                    random.Next(generated.Length),
                    number[random.Next(number.Length - 1)].ToString()
                );

            return generated.Replace("!", string.Empty);
        }
        public static decimal ChangeToDecimal(this string val)
        {
            if (string.IsNullOrWhiteSpace(val))
                return 0;
            else
                return Convert.ToDecimal(val);
        }
        public static string Encrypt(string value, bool isUrlValue)
        {
            string passPhrase = "Pas5pr@se";        // can be any string
            string saltValue = "s@1tValue";        // can be any string
            string hashAlgorithm = "SHA1";             // can be "MD5"
            int passwordIterations = 2;                  // can be any number
            string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
            int keySize = 256;                // can be 192 or 128

            string strEncrypted = Encryption.Encrypt(value,
                                                    passPhrase,
                                                    saltValue,
                                                    hashAlgorithm,
                                                    passwordIterations,
                                                    initVector,
                                                    keySize);

            if (isUrlValue)
                return HttpUtility.UrlEncode(strEncrypted);
            else
                return strEncrypted;
        }
        public static string Decrypt(string value)
        {
            try
            {
                string passPhrase = "Pas5pr@se";        // can be any string
                string saltValue = "s@1tValue";        // can be any string
                string hashAlgorithm = "SHA1";             // can be "MD5"
                int passwordIterations = 2;                  // can be any number
                string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
                int keySize = 256;                // can be 192 or 128

                string strDecrypted = value;

                strDecrypted = Encryption.Decrypt(strDecrypted,
                                                      passPhrase,
                                                      saltValue,
                                                      hashAlgorithm,
                                                      passwordIterations,
                                                      initVector,
                                                      keySize);
                return strDecrypted;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong in Decrypt");
                return null;
            }
        }
        public static string GetRandomNumber()
        {
            return Guid.NewGuid().ToString("N").Replace("-", new Random().Next(99).ToString()).Substring(0, 30);
        }
        public static bool IsValidImage(string fileextension)
        {
            fileextension = fileextension.ToLower();
            return (fileextension == ".gif" || fileextension == ".jpg" || fileextension == ".jpeg" || fileextension == ".png") ? true : false;
        }
        public static bool IsValidAgencyAuthorizationLetter(string fileextension)
        {
            fileextension = fileextension.ToLower();
            return (fileextension == ".gif" || fileextension == ".jpg" || fileextension == ".jpeg" || fileextension == ".png" || fileextension == ".png") ? true : false;
        }
        public static bool IsValidLogo_TradeLicense(string fileextension)
        {
            fileextension = fileextension.ToLower();
            return (fileextension == ".gif" || fileextension == ".jpg" || fileextension == ".jpeg" || fileextension == ".png") ? true : false;
        }
        public static string GetAreaShape(string strAreaShape, long langId)
        {
            switch (strAreaShape)
            {
                case "D":
                    return langId == 1 ? "Equipped Booth" : "الجناح المجهز";
                case "N":
                    return langId == 1 ? "Equipped Booth (no shelves)" : "الجناح المجهز (بدون أرفف)";
                case "O":
                    return langId == 1 ? "Open Space" : "مساحة مفتوحة";

                default:
                    return string.Empty;
            }
        }
        public static void ResizeImage(int widthimg, int heightimg, string sourcepath, string destinationpath, string filename)
        {
            // Image.Load(string path) is a shortcut for our default type. 
            // Other pixel formats use Image.Load<TPixel>(string path))
            using (Image<Rgba32> image = SixLabors.ImageSharp.Image.Load(sourcepath + filename))
            {
                if (heightimg > 0)
                    image.Mutate(x => x.Resize(widthimg, heightimg));
                else
                    image.Mutate(x => x.Resize(widthimg, image.Height / 2));

                image.Save(destinationpath + filename); // Automatic encoder selected based on extension.
            }
        }
        #endregion
        #region EF Methods
        public static bool iSHomepageSectionAllowed(long id)
        {
            return true;
            //using (_context = new sibfnewdbContext())
            //{
            //    if (_context.XsiHomepageOrganiser.Where(e => e.ItemId == id && e.IsActive == "Y").Count() > 0)
            //        return true;
            //    else
            //        return false;
            //    //return _context.XsiHomepageOrganiser.Any(e => e.ItemId == id && e.IsActive == "Y");
            //}
        }

        public static XsiExhibitionMemberApplicationYearly GetExhibitor(long memberid, long websiteId, sibfnewdbContext _context, long memberroleid = 1)
        {
            var predicate = PredicateBuilder.New<XsiExhibition>();
            //predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
            predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicate.And(i => i.WebsiteId == websiteId);

            XsiExhibition exhibition = _context.XsiExhibition.Where(predicate).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
            if (exhibition != null)
            {
                string strRejectedStatus = EnumConversion.ToString(EnumExhibitorStatus.Rejected);
                string strCancelledStatus = EnumConversion.ToString(EnumExhibitorStatus.Cancelled);
                var predicate1 = PredicateBuilder.New<XsiExhibitionMemberApplicationYearly>();
                predicate1.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate1.And(i => i.MemberId == memberid);
                predicate1.And(i => i.MemberRoleId == memberroleid);
                var list = _context.XsiExhibitionMemberApplicationYearly.Where(predicate1).ToList();
                XsiExhibitionMemberApplicationYearly ExhibitorEntity = list.Where(p => (p.Status != strCancelledStatus && p.Status != strRejectedStatus) && p.ExhibitionId == exhibition.ExhibitionId).OrderByDescending(x => x.MemberExhibitionYearlyId).FirstOrDefault();
                if (ExhibitorEntity != null)
                    return ExhibitorEntity;
            }
            return null;
        }
        public static long GetExhibitorByMember(long SIBFMemberId, long websiteId, sibfnewdbContext _context)
        {
            var predicate = PredicateBuilder.New<XsiExhibition>();

            predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
            predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicate.And(i => i.WebsiteId == websiteId);
            List<long> ExhibitionIdList = _context.XsiExhibition.Where(predicate).Select(s => s.ExhibitionId).ToList();
            if (ExhibitionIdList.Count() > 0)
            {
                string strNew = EnumConversion.ToString(EnumExhibitorStatus.New);
                string strInitialApprove = EnumConversion.ToString(EnumExhibitorStatus.InitialApproval);
                string strApprove = EnumConversion.ToString(EnumExhibitorStatus.Approved);
                string strActive = EnumConversion.ToString(EnumBool.Yes);

                var isActive = EnumConversion.ToString(EnumBool.Yes);
                List<XsiExhibitionMemberApplicationYearly> ExhibitorList = _context.XsiExhibitionMemberApplicationYearly.Where(x => x.MemberId == SIBFMemberId && x.IsActive == isActive).ToList();
                XsiExhibitionMemberApplicationYearly entity = ExhibitorList.Where(p => (p.Status == strNew || p.Status == strInitialApprove || p.Status == strApprove) && ExhibitionIdList.Contains(p.ExhibitionId.Value)).FirstOrDefault();
                if (entity != null)
                    return entity.MemberExhibitionYearlyId;
            }
            return -1;
        }
        public static XsiExhibitionMemberApplicationYearly GetAgencyRegistered(long SIBFMemberId, long websiteId, sibfnewdbContext _context)
        {
            string strExhibitorReject = EnumConversion.ToString(EnumAgencyStatus.ExhibitorReject);
            string strSIBFReject = EnumConversion.ToString(EnumAgencyStatus.SIBFReject);

            XsiExhibition ExhibitionEntity = MethodFactory.GetActiveExhibition(websiteId, _context);
            if (ExhibitionEntity != null)
            {
                var AgencyEntity = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.IsRegisteredByExhibitor == "Y" && i.MemberId == SIBFMemberId && i.IsActive == "Y" && i.ExhibitionId == ExhibitionEntity.ExhibitionId
                                          && i.ParentId != null).Select(i => i).FirstOrDefault();

                if (AgencyEntity != null)
                    return AgencyEntity;
            }
            return null;
        }
        public static dynamic IsValidAwardDates(long websiteId, long langid, IMemoryCache _cache)
        {
            string strmessage = string.Empty;
            websiteId = 1;
            //var predicateExhibition = PredicateBuilder.True<XsiExhibition>();
            //predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            ////predicateExhibition = predicateExhibition.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
            //predicateExhibition = predicateExhibition.And(i => i.WebsiteId == websiteId);

            XsiExhibition entity = GetActiveExhibitionNew(websiteId, _cache);

            if (entity != null)
            {
                if (entity.AwardStartDate.Value.CompareTo(ArabianTimeNow()) <= 0 && entity.AwardEndDate.Value.CompareTo(ArabianTimeNow()) >= 0)
                    return true;
            }
            return false;
            // var dto = new { Isvalid, strmessage };
            //return dto;
        }

        public static bool IsValidTurjumanAwardDates(long websiteId, long langid, IMemoryCache _cache)
        {
            string strmessage = string.Empty;
            websiteId = 1;
            //var predicateExhibition = PredicateBuilder.True<XsiExhibition>();

            //predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            //predicateExhibition = predicateExhibition.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
            //predicateExhibition = predicateExhibition.And(i => i.WebsiteId == websiteId);

            //XsiExhibition entity = _context.XsiExhibition.Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
            XsiExhibition entity = GetActiveExhibitionNew(websiteId, _cache);

            if (entity != null)
            {
                if (entity.TurjumanStartDate.Value.CompareTo(MethodFactory.ArabianTimeNow()) <= 0 && entity.TurjumanEndDate.Value.CompareTo(MethodFactory.ArabianTimeNow()) >= 0)
                    return true;
            }

            return false;
        }
        public static string GetExhibitorFileNumber(long exhibitorId)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                var entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                if (entity != null)
                    return entity.FileNumber;
                return string.Empty;
            }
        }
        public static string GetCountryNameByExhibitorIdOrAgencyId(long exhibitorId, long? agencyId, long langId)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService();

                if (agencyId != null && agencyId > 0)
                {
                    var entity = AgencyRegistrationService.GetAgencyRegistrationByItemId(agencyId.Value);
                    if (entity != null)
                        return GetCountryName(entity.CountryId ?? -1, langId);
                    return string.Empty;
                }
                else
                {
                    var entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                    if (entity != null)
                        return GetCountryName(entity.CountryId ?? -1, langId);
                    return string.Empty;
                }
            }
        }
        public static string GetExhibitorName(long exhibitorId, long langId)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                var entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                if (entity != null)
                {
                    if (langId == 1)
                    {
                        if (entity.PublisherNameEn != null)
                            return entity.PublisherNameEn;
                    }
                    else
                    {
                        if (entity.PublisherNameAr != null)
                            return entity.PublisherNameAr;
                    }
                }
            }
            return string.Empty;
        }
        public static string GetExhibitorName(long itemId, sibfnewdbContext _context)
        {
            long EnglishId = 1;
            long ArabicId = 2;
            var entity = _context.XsiExhibitionMemberApplicationYearly.Where(x => x.MemberExhibitionYearlyId == itemId).FirstOrDefault();
            // XsiExhibitorRegistration entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(itemId);
            if (entity != null)
                if (entity.LanguageUrl == EnglishId)
                {
                    if (entity.PublisherNameEn != null)
                        return entity.PublisherNameEn;
                }
                else if (entity.LanguageUrl == ArabicId)
                    if (entity.PublisherNameAr != null)
                        return entity.PublisherNameAr;
            return string.Empty;
        }
        public static string GetExhibitionDetails(long websiteId, long langId, sibfnewdbContext _context)
        {
            long? exhibitionNumber = -1;
            string strdate = string.Empty;

            XsiExhibition entity = _context.XsiExhibition.Where(i => i.WebsiteId == websiteId && i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.IsArchive == EnumConversion.ToString(EnumBool.No)).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
            string startdaysuffix = string.Empty;
            string enddaysuffix = string.Empty;
            string stryear = MethodFactory.ArabianTimeNow().Year.ToString();

            if (entity != default(XsiExhibition))
            {
                if (entity.ExhibitionNumber != null)
                    exhibitionNumber = entity.ExhibitionNumber;
                long dayno = entity.StartDate.Value.Date.Day;
                if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                    startdaysuffix = "th";
                else
                    startdaysuffix = GetSuffix(entity.StartDate.Value.Date.Day % 10, entity.StartDate.Value);
                dayno = entity.EndDate.Value.Date.Day;
                if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                    enddaysuffix = "th";
                else
                    enddaysuffix = GetSuffix(entity.EndDate.Value.Date.Day % 10, entity.EndDate.Value);

                if (entity.StartDate.Value.Year == entity.EndDate.Value.Year)
                {
                    if (entity.StartDate.Value.Month == entity.EndDate.Value.Month)
                        strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.StartDate.Value.ToString("MMM") + " " + entity.StartDate.Value.ToString("yyyy");
                    else
                        strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " + entity.StartDate.Value.ToString("MMM")
                             + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.EndDate.Value.ToString("MMM")
                            + " " + entity.StartDate.Value.ToString("yyyy");
                }
                else
                {
                    strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " + entity.StartDate.Value.ToString("MMM") + " " + entity.StartDate.Value.ToString("yyyy")
                         + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.EndDate.Value.ToString("MMM")
                        + " " + entity.EndDate.Value.ToString("yyyy");
                }
                if (entity.ExhibitionYear != null)
                    stryear = entity.ExhibitionYear.ToString();
            }
            return exhibitionNumber.ToString() + "," + strdate + "," + stryear;
        }
        public static string GetSuffix(int startday, DateTime dt)
        {
            string daysuffix;
            if (startday != 11 && dt.Date.Day != 11)
            {
                if (startday == 1)
                    daysuffix = "st";
                else if (startday == 2)
                    daysuffix = "nd";
                else if (startday == 3)
                    daysuffix = "rd";
                else
                    daysuffix = "th";
            }
            else
                daysuffix = "th";
            return daysuffix;
        }
        public static string GetSuffix(int startday)
        {
            string daysuffix;
            if (startday == 1)
                daysuffix = "st";
            else if (startday == 2)
                daysuffix = "nd";
            else if (startday == 3)
                daysuffix = "rd";
            else
                daysuffix = "th";
            return daysuffix;
        }
        public static string AddCommasToCurrency(string currency, bool isdecimalrequired = false)
        {
            decimal currencyamt = 0;
            if (decimal.TryParse(currency, out currencyamt))
            {
                if (!string.IsNullOrEmpty(currency))
                {
                    var strcurrency = string.Empty;
                    if (isdecimalrequired)
                        strcurrency = Convert.ToDouble(currency).ToString("#.##", CultureInfo.InvariantCulture);
                    else
                        strcurrency = Convert.ToDouble(currency).ToString("#,#", CultureInfo.InvariantCulture);

                    if (string.IsNullOrWhiteSpace(strcurrency))
                        return "0";
                    return strcurrency;
                }
            }
            return "0";
        }
        public static string GetExhibitionCurrency(long itemId, sibfnewdbContext _context)
        {
            var entity = _context.XsiExhibitionCurrency.Where(x => x.ItemId == itemId).FirstOrDefault();
            if (entity != default(XsiExhibitionCurrency))
                return entity.CurrencyCode ?? string.Empty;
            return string.Empty;
        }
        #endregion

        #region TranslationGrant
        public static string GetTranslationGranteGenrebyId(long groupId, long langId)
        {
            switch (groupId)
            {
                case 1: return (langId == 1) ? "Fiction" : "خيال";
                case 2: return (langId == 1) ? "Non Fiction" : "واقعي";
                case 3: return (langId == 1) ? "Memoir" : "السيرة الذاتية";
                case 4: return (langId == 1) ? "History" : "كتب التاريخ";
                case 5: return (langId == 1) ? "Cookery" : "كتب الطبخ";
                case 6: return (langId == 1) ? "Children's" : "كتب الأطفال";
                case 7: return (langId == 1) ? "Young Adult" : "كتب اليافعين";
                case 8: return (langId == 1) ? "Poetry" : "شعر";

                default: return string.Empty;
            }
        }
        public static string GetExhibitionLanuageTitle(long itemId, long langId)
        {
            using (ExhibitionLanguageService BookService = new ExhibitionLanguageService())
            {
                Entities.Models.XsiExhibitionLanguage entity = new Entities.Models.XsiExhibitionLanguage();
                entity = BookService.GetExhibitionLanguageByItemId(itemId);
                if (entity != default(Entities.Models.XsiExhibitionLanguage))
                    return entity.Title;
                return string.Empty;
            }
        }
        public static void LogTranslationGrantMemberModifiedStatus(string strStatus, long itemId)
        {
            using (TranslationGrantMemberStatusLogService TranslationGrantMemberStatusLogService = new TranslationGrantMemberStatusLogService())
            {
                Entities.Models.XsiExhibitionTranslationGrantMemberStatusLog entity = new Entities.Models.XsiExhibitionTranslationGrantMemberStatusLog();
                entity.TranslationGrantMemberId = itemId;
                entity.Status = strStatus;
                entity.CreatedOn = MethodFactory.ArabianTimeNow();
                TranslationGrantMemberStatusLogService.InsertTranslationGrantMemberStatusLog(entity);
            }
        }
        public static string GetExhibitionDetails(long wid, long langId = 1, long exhibitionId = -1)
        {
            long strExhibitionNo = 0;
            string strdate = string.Empty;
            string stryear = MethodFactory.ArabianTimeNow().Year.ToString();
            using (ExhibitionService service = new ExhibitionService())
            {
                XsiExhibition entity;
                if (exhibitionId == -1)
                    entity = service.GetExhibition(new Entities.Models.XsiExhibition { WebsiteId = wid, IsActive = EnumConversion.ToString(EnumBool.Yes), IsArchive = EnumConversion.ToString(EnumBool.No) }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                else
                    entity = service.GetExhibition(new Entities.Models.XsiExhibition { ExhibitionId = exhibitionId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

                if (entity != default(Entities.Models.XsiExhibition))
                {
                    string startdaysuffix;
                    string enddaysuffix;
                    if (entity.ExhibitionNumber != null && entity.ExhibitionNumber.HasValue)
                        strExhibitionNo = entity.ExhibitionNumber.Value;
                    long dayno = entity.StartDate.Value.Date.Day;
                    if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                        startdaysuffix = "th";
                    else
                        startdaysuffix = GetSuffix(entity.StartDate.Value.Date.Day % 10, entity.StartDate.Value);
                    dayno = entity.EndDate.Value.Date.Day;
                    if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                        enddaysuffix = "th";
                    else
                        enddaysuffix = GetSuffix(entity.EndDate.Value.Date.Day % 10, entity.EndDate.Value);

                    if (langId == 1)
                    {
                        #region English

                        if (entity.StartDate.Value.Year == entity.EndDate.Value.Year)
                        {
                            if (entity.StartDate.Value.Month == entity.EndDate.Value.Month)
                                strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " - " +
                                          entity.EndDate.Value.Date.Day + enddaysuffix + " " +
                                          entity.StartDate.Value.ToString("MMM") + " " +
                                          entity.StartDate.Value.ToString("yyyy");
                            else
                                strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " +
                                          entity.StartDate.Value.ToString("MMM")
                                          + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " +
                                          entity.EndDate.Value.ToString("MMM")
                                          + " " + entity.StartDate.Value.ToString("yyyy");
                        }
                        else
                        {
                            strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " +
                                      entity.StartDate.Value.ToString("MMM") + " " +
                                      entity.StartDate.Value.ToString("yyyy")
                                      + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " +
                                      entity.EndDate.Value.ToString("MMM")
                                      + " " + entity.EndDate.Value.ToString("yyyy");
                        }

                        #endregion
                    }
                    else
                    {
                        #region Arabic
                        if (entity.StartDate.Value.Year == entity.EndDate.Value.Year)
                        {
                            if (entity.StartDate.Value.Month == entity.EndDate.Value.Month)
                                strdate = entity.StartDate.Value.Date.Day + " - " +
                                          entity.EndDate.Value.Date.Day + " " +
                                          entity.StartDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")) + " " +
                                          entity.StartDate.Value.ToString("yyyy");
                            else
                                strdate = entity.StartDate.Value.Date.Day + " " +
                                          entity.StartDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"))
                                          + " - " + entity.EndDate.Value.Date.Day + " " +
                                          entity.EndDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"))
                                          + " " + entity.StartDate.Value.ToString("yyyy");
                        }
                        else
                        {
                            strdate = entity.StartDate.Value.Date.Day + " " +
                                      entity.StartDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")) + " " +
                                      entity.StartDate.Value.ToString("yyyy")
                                      + " - " + entity.EndDate.Value.Date.Day + " " +
                                      entity.EndDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"))
                                      + " " + entity.EndDate.Value.ToString("yyyy");
                        }
                        #endregion
                    }

                    if (entity.ExhibitionYear != null)
                        stryear = entity.ExhibitionYear.ToString();
                }
            }
            return strExhibitionNo + "," + strdate + "," + stryear;
        }
        public static StringBuilder BindEmailContentForTGMember(long languageId, string bodyContent, Entities.Models.XsiExhibitionMember entity, string ServerAddress, long websiteId, AppCustomSettings _appCustomSettings = null)
        {
            if (_appCustomSettings == null)
                _appCustomSettings = new AppCustomSettings();
            string scrfURL = _appCustomSettings.ServerAddressSCRF;
            string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
            string EmailContent = string.Empty;
            string stryear = MethodFactory.ArabianTimeNow().Year.ToString();
            if (languageId == 1)
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRF.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
            }
            else
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRF.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
            }
            StringBuilder body = new StringBuilder();

            if (File.Exists(EmailContent))
            {
                body.Append(File.ReadAllText(EmailContent));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);
            string str = GetExhibitionDetails(websiteId, languageId);
            string[] strArray = str.Split(',');
            //body.Replace("$$exhibitionno$$", strArray[0].ToString()); //strArray[0].ToString()
            //body.Replace("$$exhibitiondate$$", strArray[1].ToString());
            //body.Replace("$$exhyear$$", strArray[2]);

            body.Replace("$$exhibitionno$$", string.Empty); //strArray[0].ToString()
            body.Replace("$$exhibitiondate$$", string.Empty);
            body.Replace("$$exhyear$$", string.Empty);

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            body.Replace(swidth, sreplacewidth);

            body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
            body.Replace("$$ServerAddress$$", ServerAddress);
            if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                body.Replace("$$ServerAddressSCRF$$", scrfURL);

            if (entity != null)
            {
                string exhibitionmembername = string.Empty;
                if (languageId == 1)
                {
                    if (entity.Firstname != null && entity.LastName != null)
                        exhibitionmembername = entity.Firstname + " " + entity.LastName;
                    else if (entity.Firstname != null)
                        exhibitionmembername = entity.Firstname;
                    else if (entity.LastName != null)
                        exhibitionmembername = entity.LastName;
                }
                else
                {
                    if (entity.FirstNameAr != null && entity.LastNameAr != null)
                        exhibitionmembername = entity.FirstNameAr + " " + entity.LastNameAr;
                    else if (entity.Firstname != null)
                        exhibitionmembername = entity.FirstNameAr;
                    else if (entity.LastName != null)
                        exhibitionmembername = entity.LastNameAr;

                    if (string.IsNullOrEmpty(exhibitionmembername))
                    {
                        if (entity.Firstname != null && entity.LastName != null)
                            exhibitionmembername = entity.Firstname + " " + entity.LastName;
                        else if (entity.Firstname != null)
                            exhibitionmembername = entity.Firstname;
                        else if (entity.LastName != null)
                            exhibitionmembername = entity.LastName;
                    }
                }
                body.Replace("$$Name$$", exhibitionmembername);
            }
            return body;
        }
        public static StringBuilder BindEmailContentTG(long languageId, string bodyContent, string ServerAddress, AppCustomSettings _appCustomSettings = null, long exhibitionid = -1, long websiteId = 1)
        {
            if (_appCustomSettings == null)
                _appCustomSettings = new AppCustomSettings();
            string scrfURL = _appCustomSettings.ServerAddressSCRF;
            string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
            string EmailContent = string.Empty;
            if (languageId == 1)
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRF.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
            }
            else
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRF.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
            }
            StringBuilder body = new StringBuilder();

            if (File.Exists(EmailContent))
            {
                body.Append(File.ReadAllText(EmailContent));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);
            string str = GetExhibitionDetails(websiteId, languageId, exhibitionid);
            string[] strArray = str.Split(',');
            //body.Replace("$$exhibitionno$$", strArray[0].ToString()); 
            //body.Replace("$$exhibitiondate$$", strArray[1].ToString());
            //body.Replace("$$exhyear$$", strArray[2]);

            body.Replace("$$exhibitionno$$", string.Empty);
            body.Replace("$$exhibitiondate$$", string.Empty);
            body.Replace("$$exhyear$$", string.Empty);

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            body.Replace(swidth, sreplacewidth);
            body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
            body.Replace("$$ServerAddress$$", ServerAddress);
            if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                body.Replace("$$ServerAddressSCRF$$", scrfURL);
            return body;
        }
        public static string ShortDate(string dateTime)
        {
            return Convert.ToDateTime(dateTime).ToString("dd/MM/yyyy");
        }
        internal static string ShortDateTG(this string dtStr)
        {
            DateTime date = Convert.ToDateTime(dtStr);
            string ordinal;

            switch (date.Day)
            {
                case 1:
                case 21:
                case 31:
                    ordinal = "st";
                    break;
                case 2:
                case 22:
                    ordinal = "nd";
                    break;
                case 3:
                case 23:
                    ordinal = "rd";
                    break;
                default:
                    ordinal = "th";
                    break;
            }

            return string.Format("{0:MMMM dd}{1} {0: yyyy}", date, ordinal);
        }
        #endregion

        #region Books
        public static string MainSubjectById(long id, long langId)
        {
            string strTitle = string.Empty;
            using (ExhibitionBookSubjectService ExhibitionBookSubjectService = new ExhibitionBookSubjectService())
            {
                Entities.Models.XsiExhibitionBookSubject entity = ExhibitionBookSubjectService.GetExhibitionBookSubjectByItemId(id);
                if (entity != null)
                {
                    if (langId == 1)
                    {
                        if (entity.Title != null && !string.IsNullOrEmpty(entity.Title))
                            strTitle = entity.Title;
                    }
                    else
                    {
                        if (entity.TitleAr != null && !string.IsNullOrEmpty(entity.TitleAr))
                            strTitle = entity.TitleAr;

                    }
                }
            }
            return strTitle;
        }
        public static string SubSubjectById(long id, long mainsujectgid, long langid)
        {
            string strTitle = string.Empty;
            long catid = -1;
            using (ExhibitionBookSubsubjectService ExhibitionBookSubsubjectService = new ExhibitionBookSubsubjectService())
            {
                ExhibitionBookSubjectService ExhibitionBookSubjectService = new ExhibitionBookSubjectService();
                catid = ExhibitionBookSubjectService.GetExhibitionBookSubjectByItemId(mainsujectgid).ItemId;
                Entities.Models.XsiExhibitionBookSubsubject entity = ExhibitionBookSubsubjectService.GetExhibitionBookSubsubject(new Entities.Models.XsiExhibitionBookSubsubject { ItemId = id, CategoryId = catid }).FirstOrDefault();
                if (entity != null)
                {
                    if (langid == 1)
                    {
                        if (entity.Title != null && !string.IsNullOrEmpty(entity.Title))
                            strTitle = entity.Title;
                    }
                    else
                    {
                        if (entity.TitleAr != null && !string.IsNullOrEmpty(entity.TitleAr))
                            strTitle = entity.TitleAr;

                    }
                }
            }
            return strTitle;
        }
        public static string GetBookTypeTitle(long itemId, long langId)
        {
            using (ExhibitionBookTypeService ExhibitionBookTypeService = new ExhibitionBookTypeService())
            {
                Entities.Models.XsiExhibitionBookType entity = new Entities.Models.XsiExhibitionBookType();
                entity = ExhibitionBookTypeService.GetExhibitionBookTypeByItemId(itemId);
                if (entity != default(Entities.Models.XsiExhibitionBookType))
                {
                    if (langId == 1)
                        return entity.Title;
                    else
                        return entity.TitleAr;
                }
                return string.Empty;
            }
        }
        public static string GetCurrencyTitle(long itemId, long langId)
        {
            using (ExhibitionCurrencyService ExhibitionCurrencyService = new ExhibitionCurrencyService())
            {
                Entities.Models.XsiExhibitionCurrency entity = new Entities.Models.XsiExhibitionCurrency();
                entity = ExhibitionCurrencyService.GetExhibitionCurrencyByItemId(itemId);
                if (entity != default(Entities.Models.XsiExhibitionCurrency))
                    if (langId == 1)
                        return entity.Title;
                    else
                        return entity.TitleAr;
                return string.Empty;
            }
        }
        #endregion
        #region Exhibition


        public static XsiExhibition GetActiveExhibition(long websiteId, sibfnewdbContext _context)
        {
            var predicate = PredicateBuilder.New<XsiExhibition>();
            //predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
            predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicate.And(i => i.WebsiteId == websiteId);

            return _context.XsiExhibition.Where(predicate).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
        }
        public static XsiExhibition GetActiveExhibitionNew(long websiteId, IMemoryCache _cache)
        {
            var cachedData = (List<XsiExhibition>)_cache.Get<dynamic>(CacheKeys.Exhibition);

            if (cachedData == null)
                cachedData = CacheKeys.GetExhibition(_cache);

            var predicate = PredicateBuilder.New<XsiExhibition>();
            //predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
            predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicate.And(i => i.WebsiteId == websiteId);

            return cachedData.AsQueryable().Where(predicate).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
        }

        public static XsiExhibition GetExhibition(long websiteId)
        {
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {
                var where = new XsiExhibition();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                // where.IsArchive = EnumConversion.ToString(EnumBool.No);
                where.WebsiteId = websiteId;
                XsiExhibition exhibition = ExhibitionService.GetExhibition(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                return exhibition;
            }
        }
        public static XsiExhibitionOtherEvents GetExhibitionOtherEvents(long exhibitionid)
        {
            using (ExhibitionOtherEventsService ExhibitionOtherEventsService = new ExhibitionOtherEventsService())
            {
                var where = new XsiExhibitionOtherEvents();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                // where.IsArchive = EnumConversion.ToString(EnumBool.No);
                where.ExhibitionId = exhibitionid;
                XsiExhibitionOtherEvents exhibition = ExhibitionOtherEventsService.GetExhibitionOtherEvents(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                return exhibition;
            }
        }
        public static XsiExhibitionOtherEvents GetActiveExhibitionOtherEvents(long exhibitionid, sibfnewdbContext _context)
        {
            var predicate = PredicateBuilder.New<XsiExhibitionOtherEvents>();
            //predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
            predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicate.And(i => i.ExhibitionId == exhibitionid);

            return _context.XsiExhibitionOtherEvents.Where(predicate).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
        }
        #endregion
        #region Agency

        public static bool AgencyInRegistrationProcess(long memberId, long websiteId)
        {
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {
                var whereQuery = new Entities.Models.XsiExhibition();
                whereQuery.IsActive = EnumConversion.ToString(EnumBool.Yes);
                whereQuery.IsArchive = EnumConversion.ToString(EnumBool.No);
                whereQuery.WebsiteId = websiteId;
                var exhibitionEntity = ExhibitionService.GetExhibition(whereQuery).FirstOrDefault();
                if (exhibitionEntity != null)
                {
                    var AgencyRegistrationService = new AgencyRegistrationService();
                    var where = new Entities.Models.XsiExhibitionMemberApplicationYearly();
                    where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    where.MemberId = memberId;
                    where.ExhibitionId = exhibitionEntity.ExhibitionId;
                    var entity = AgencyRegistrationService.GetAgencyRegistration(where).FirstOrDefault();
                    if (entity != null)
                        return true;
                }
            }
            return false;
        }
        #endregion Agency

        #region Representative
        public static string GetRepresentativeAgreementToDownload(long exhibitorId)
        {
            using (RepresentativeAgreementService RepresentativeAgreementService = new RepresentativeAgreementService())
            {
                XsiRepresentativeAgreement entity = RepresentativeAgreementService.GetRepresentativeAgreement(new XsiRepresentativeAgreement() { ExhibitorId = exhibitorId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                if (entity != default(XsiRepresentativeAgreement))
                    return entity.Agreement;
            }
            return string.Empty;
        }

        public static bool IsDateExpired(string strExpiryDate, DateTime? startDate, int expiryMonths)
        {
            if (!string.IsNullOrEmpty(strExpiryDate) && startDate != null)
            {
                DateTime dt = startDate.Value.AddMonths(expiryMonths);
                if (dt < Convert.ToDateTime(strExpiryDate))
                    return false;
            }
            return true;
        }

        public static bool IsValidFile1(string filetype)
        {
            return (filetype == ".pdf" || filetype == ".doc" || filetype == ".docx" || filetype == ".xls" || filetype == ".xlsx" || filetype == ".gif" || filetype == ".jpg" || filetype == ".jpeg" || filetype == ".png") ? true : false;
        }

        public static bool IsValidImgAndPDF(string filetype)
        {
            return (filetype == ".pdf" || filetype == ".gif" || filetype == ".jpg" || filetype == ".jpeg" || filetype == ".png" || filetype == ".tif" || filetype == ".tiff") ? true : false;
        }

        public static void InactivateBooksParticipating(long exhibitorId)
        {
            using (BooksParticipatingService BooksParticipatingService = new BooksParticipatingService())
            {
                XsiExhibitionBookParticipating where = new XsiExhibitionBookParticipating();
                where.ExhibitorId = exhibitorId;
                List<XsiExhibitionBookParticipating> BooksParticipatingList = BooksParticipatingService.GetBooksParticipating(where).ToList();
                if (BooksParticipatingList.Count() > 0)
                {
                    foreach (XsiExhibitionBookParticipating entity in BooksParticipatingList)
                    {
                        entity.IsActive = EnumConversion.ToString(EnumBool.No);
                        BooksParticipatingService.UpdateBooksParticipating(entity);
                    }
                }
            }
        }
        #endregion

        #region Statuses
        public static string GetExhibitorRegistrationStatus(string strstatus, long langId)
        {
            if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.New))
                return (langId == 1) ? "New" : "New";
            if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.PreApproved))
                return (langId == 1) ? "Pre Approved" : "Pre Approved";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.Paid))
                return (langId == 1) ? "Paid" : "Paid";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.PartiallyPaid))
                return (langId == 1) ? "Partially Paid" : "Partially Paid";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.Pending))
                return (langId == 1) ? "Pending" : "Pending";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.Rejected))
                return (langId == 1) ? "Rejected" : "Rejected";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval))
                return (langId == 1) ? "Initial Approval" : "Initial Approval";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.Cancelled))
                return (langId == 1) ? "Cancelled" : "Cancelled";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.Approved))
                return (langId == 1) ? "Approved" : "Approved";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.Step2))
                return (langId == 1) ? "Step2" : "Step2";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.ReceiptUploaded))
                return (langId == 1) ? "Receipt Uploaded" : "Receipt Uploaded";
            //else if (strstatus== EnumConversion.ToString(EnumExhibitorStatus.PaymentInProcess))
            //    return  (langId == 1) ?"Payment in Process":"Payment in Process";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.PendingApproval))
                return (langId == 1) ? "Pending Approval" : "Pending Approval";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.PendingDocumentation))
                return (langId == 1) ? "Pending Documentation" : "Pending Documentation";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.PendingPayment))
                return (langId == 1) ? "Pending Payment" : "Pending Payment";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.EditRequest))
                return (langId == 1) ? "Edit Request" : "Edit Request";
            else
                return "-";
        }
        public static string GetRestaurantRegistrationStatus(string strstatus, long langId)
        {
            if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.New))
                return (langId == 1) ? "New" : "New";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.Paid))
                return (langId == 1) ? "Paid" : "Paid";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.PartiallyPaid))
                return (langId == 1) ? "Partially Paid" : "Partially Paid";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.Pending))
                return (langId == 1) ? "Pending" : "Pending";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.Rejected))
                return (langId == 1) ? "Rejected" : "Rejected";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval))
                return (langId == 1) ? "Initial Approval" : "Initial Approval";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.Cancelled))
                return (langId == 1) ? "Cancelled" : "Cancelled";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.Approved))
                return (langId == 1) ? "Approved" : "Approved";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.Step2))
                return (langId == 1) ? "Step2" : "Step2";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.ReceiptUploaded))
                return (langId == 1) ? "Receipt Uploaded" : "Receipt Uploaded";
            //else if (strstatus== EnumConversion.ToString(EnumExhibitorStatus.PaymentInProcess))
            //    return  (langId == 1) ?"Payment in Process":"Payment in Process";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.PendingApproval))
                return (langId == 1) ? "Pending Approval" : "Pending Approval";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.PendingDocumentation))
                return (langId == 1) ? "Pending Documentation" : "Pending Documentation";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.PendingPayment))
                return (langId == 1) ? "Pending Payment" : "Pending Payment";
            else if (strstatus == EnumConversion.ToString(EnumExhibitorStatus.EditRequest))
                return (langId == 1) ? "Edit Request" : "Edit Request";
            else
                return "-";
        }
        public static string GetAgencyRegistrationStatus(string strstatus, long langId)
        {
            if (strstatus ==
                EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved))
                return (langId == 1) ? "Approved by Exhibitor" : "Approved by Exhibitor";
            else if (strstatus ==
                     EnumConversion.ToString(EnumAgencyStatus.ExhibitorReject))
                return (langId == 1) ? "Rejected by Exhibitor" : "Rejected by Exhibitor";
            else if (strstatus ==
                     EnumConversion.ToString(EnumAgencyStatus.New))
                return (langId == 1) ? "New" : "New";
            else if (strstatus ==
                     EnumConversion.ToString(EnumAgencyStatus.Pending))
                return (langId == 1) ? "Pending" : "Pending";
            else if (strstatus ==
                     EnumConversion.ToString(EnumAgencyStatus.SIBFApproved))
                return (langId == 1) ? "Approved by SIBF" : "Approved by SIBF";
            else if (strstatus ==
                     EnumConversion.ToString(EnumAgencyStatus.SIBFReject))
                return (langId == 1) ? "Rejected by SIBF" : "Rejected by SIBF";
            return "-";
        }
        public static string GetInvoiceStatus(string status, long langId)
        {
            if (status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.New))
                return (langId == 1) ? "New" : "New";
            else if (status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.PendingPayment))
                return (langId == 1) ? "Pending Payment" : "Pending Payment";
            else if (status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.PaidOnline))
                return (langId == 1) ? "Paid Online" : "Paid Online";
            else if (status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.Paid))
                return (langId == 1) ? "Paid" : "Paid";
            else if (status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.PartiallyPaid))
                return (langId == 1) ? "Partially Paid" : "Partially Paid";
            else if (status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.PartiallyPaidBalanceForwardToNewInvoice))
                return (langId == 1) ? "Partially Paid Balance Forward To New nvoice" : "Partially Paid Balance Forward To NewInvoice";
            else if (status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.ReceiptUploaded))
                return (langId == 1) ? "Receipt Uploaded" : "Receipt Uploaded";
            else if (status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.ReceiptReviewed))
                return (langId == 1) ? "Receipt Reviewed" : "Receipt Reviewed";
            else if (status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.Cancelled))
                return (langId == 1) ? "Cancelled" : "Cancelled";
            return "-";
        }
        public static string GetReprepresentativeStatus(string strstatus, long langId)
        {
            switch (strstatus)
            {
                case "A": return (langId == 1) ? "Approved" : "مشارك";
                case "P": return (langId == 1) ? "Pending" : "معلق";
                case "F": return (langId == 1) ? "Flight Details Pending" : "Flight Details Pending";
                case "Y": return (langId == 1) ? "Pending Documentation" : "وثائق ناقصة";
                case "V": return (langId == 1) ? "Visa Processing" : "التأشيرة قيد الإجراء";
                case "U": return (langId == 1) ? "Visa Uploaded" : "تم تحميل التأشيرة";
                case "R": return (langId == 1) ? "Rejected" : "مرفوض";
                case "O": return (langId == 1) ? "Documents Approved" : "الوثائق المعتمدة";
                case "D": return (langId == 1) ? "Cancelled" : "تم الإلغاء";
                case "L": return (langId == 1) ? "Documents Uploaded" : "تم تحميل الوثائق";
                case "B": return (langId == 1) ? "Rejected" : "مرفوض";
                case "W": return (langId == 1) ? "Re Issue Visa" : "إعادة إصدار التأشيرة";
                case "X": return (langId == 1) ? "Book (No Visa)" : "Book (No Visa)";
                case "1": return (langId == 1) ? "Cancelled" : "Cancelled";
                case "2": return (langId == 1) ? "Cancelled: Cancellation Charges and Visa" : "Cancelled: Cancellation Charges and Visa";
                case "3": return (langId == 1) ? "Cancelled: Cancellation Charges" : "Cancelled: Cancellation Charges";
                default: return "-";
            }
        }
        public static string GetTranslationGrantMemberStatus(string TGStatus, long langId)
        {
            switch (TGStatus)
            {
                case "T": return (langId == 1) ? "Temporary" : "مؤقت";
                case "Y": return (langId == 1) ? "Temporary View" : "مؤقت";
                case "V": return (langId == 1) ? "Under Evaluation" : "جاري التقييم";
                case "N": return (langId == 1) ? "New" : "جديد";
                case "A": return (langId == 1) ? "Approved" : "تمت الموافقة";
                case "R": return (langId == 1) ? "Rejected" : "تم الرفض";
                case "I": return (langId == 1) ? "Phase2 Incorrect Details" : "Phase2 Incorrect Details";
                case "S": return (langId == 1) ? "Phase2 Details Resubmitted" : "Phase2 Details Resubmitted";
                case "B": return (langId == 1) ? "Bank Details Submitted" : "تم إرسال تفاصيل البنك";
                case "U": return (langId == 1) ? "Contract Uploaded" : "تم تحميل العقد";
                case "C": return (langId == 1) ? "Contract Submitted" : "تم تقديم العقد";
                case "F": return (langId == 1) ? "First Payment is Being Processed" : "تم تحميل الدفعة الأولى";
                case "D": return (langId == 1) ? "Translation Draft Has Been Submitted" : "تم إرسال الكتاب المترجم";
                case "P": return (langId == 1) ? "Second Payment Proof of Transfer Uploaded" : "تحميل إيصال الدفعة الثانية";
                case "Z": return (langId == 1) ? "Final Payment Proof of Transfer Uploaded" : "تحميل إيصال الدفعة الأخيرة";
                case "X": return (langId == 1) ? "Cancelled" : "تم الإلغاء";
                case "M": return (langId == 1) ? "Pending Documentation" : "الملف يحتاج للتعديل";
                case "O": return (langId == 1) ? "Documentation Resubmitted" : "الملف بعد التعديل";
                case "Q": return (langId == 1) ? "Resubmit Draft" : "اعادة تحميل النسخة المترجمة";//"النسخة المترجمة بعد التعديل";
                case "E": return (langId == 1) ? "Draft submitted" : "تم ارسال النسخة المترجمة بعد التعديل";
                case "G": return (langId == 1) ? "Money to Refund by Registrant" : "إلغاء من قبل المستخدم مع استرجاع الدفعات: الإلغاء معلق";
                case "W": return (langId == 1) ? "Cancelled By Registrant" : "إلغاء من قبل المشارك";
                case "H": return (langId == 1) ? "Sample Received" : "إلغاء من قبل المشارك";
                default: return "-";
            }
        }
        public static bool ShowRemoveRepresentative(string status)
        {
            switch (status)
            {
                case "P": return true;  //Pending
                case "F": return true;  //FlightDetailsPending
                case "Y": return true; //PendingDocumentation
                case "O": return true;  //DocumentsApproved
                case "L": return true;  //DocumentsUploaded
                case "A": return true;  //Approved
                case "V": return true;  //VisaProcessing
                case "U": return true;  //VisaUploaded
                case "W": return true;  //ReIssueVisa

                case "1": return false;  //CancelNoCharge
                case "2": return false;  //CancelChargeandVisa
                case "3": return false;  //CancelCharge
                case "D": return false;  //Cancelled
                case "R": return false;  //Rejected
                case "B": return false;  //Blocked

                //case "D": return true;  //Cancelled
                //case "R": return true;  //Rejected
                //case "B": return true;  //Blocked
                //case "A": return false;  //Approved
                //case "V": return false;  //VisaProcessing
                //case "U": return false;  //VisaUploaded
                //case "W": return false;  //ReIssueVisa
                default: return true;
            }
        }
        public static string GetProfessionalProgramStatus(string strstatus, long langId)
        {
            if (strstatus ==
                EnumConversion.ToString(EnumProfessionalProgramStatus.New))
                return (langId == 1) ? "New" : "New";
            else if (strstatus ==
                    EnumConversion.ToString(EnumProfessionalProgramStatus.Pending))
                return (langId == 1) ? "Pending" : "Pending";
            else if (strstatus ==
                     EnumConversion.ToString(EnumProfessionalProgramStatus.Rejected))
                return (langId == 1) ? "Rejected" : "Rejected";
            else if (strstatus ==
                     EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                return (langId == 1) ? "Approved" : "Approved";

            return (langId == 1) ? "-" : "-";
        }
        #endregion


        #region Exhibitor
        public static bool IsPreApproved(string publisherNameEn, string publisherNameAr, string fileNumber)
        {
            using (ExhibitionMemberApplicationYearlyPreApprovedService ExhibitionMemberApplicationYearlyPreApprovedService = new ExhibitionMemberApplicationYearlyPreApprovedService())
            {
                XsiExhibitionMemberApplicationYearlyPreApproved where = new XsiExhibitionMemberApplicationYearlyPreApproved();

                where.IsActive = EnumConversion.ToString(EnumBool.Yes);

                if (!string.IsNullOrEmpty(publisherNameEn))
                    where.PublisherNameEn = publisherNameEn;

                if (!string.IsNullOrEmpty(publisherNameAr))
                    where.PublisherNameAr = publisherNameAr;

                if (!string.IsNullOrEmpty(fileNumber))
                    where.FileNumber = fileNumber;

                var preApprovedEntity = ExhibitionMemberApplicationYearlyPreApprovedService.GetExhibitionMemberApplicationYearlyPreApproved(where).FirstOrDefault();
                if (preApprovedEntity != null)
                    return true;
            }
            return false;
        }
        public static bool ExhibitorInRegistrationProcess(long memberId, long websiteId)
        {
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {
                XsiExhibition whereQuery = new XsiExhibition();
                whereQuery.IsActive = EnumConversion.ToString(EnumBool.Yes);
                whereQuery.IsArchive = EnumConversion.ToString(EnumBool.No);
                whereQuery.WebsiteId = websiteId;
                List<long> exhibitionidList = ExhibitionService.GetExhibition(whereQuery).Select(s => s.ExhibitionId).ToList();
                if (exhibitionidList.Count() > 0)
                {
                    ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                    XsiExhibitionMemberApplicationYearly where = new XsiExhibitionMemberApplicationYearly();
                    where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    where.MemberId = memberId;
                    XsiExhibitionMemberApplicationYearly entity = ExhibitorRegistrationService.GetExhibitorRegistration(where).Where(p => exhibitionidList.Contains(p.ExhibitionId.Value)).FirstOrDefault();
                    if (entity != null)
                        return true;
                }
            }
            return false;
        }

        public static bool IsValidForRegistrationFromPreviousPayments(long exhibitionId, long memberId)
        {
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {
                ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService();
                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                XsiExhibition exhibitionentity = ExhibitionService.GetExhibition(new XsiExhibition() { ExhibitionId = exhibitionId }).FirstOrDefault();
                if (exhibitionentity != default(XsiExhibition))
                {
                    #region Temporary Allow User
                    var ExhibitionMember = ExhibitionMemberService.GetExhibitionMemberByItemId(memberId);
                    if (ExhibitionMember != null && !string.IsNullOrEmpty(ExhibitionMember.FileNumber))
                    {
                        using (sibfnewdbContext _context = new sibfnewdbContext())
                        {
                            var entity = _context.FinDueExcel.Where(i => i.FileNumber.ToString().Equals(ExhibitionMember.FileNumber)).FirstOrDefault();
                            if (entity != null)
                            {
                                if (entity.CustomerBalance > 100)
                                {
                                    if (entity.IsAllow == "Y")
                                    {
                                        return true;
                                    }

                                    return false;
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            return true;
        }
        public static decimal GetCustomerBalanceFromPreviousPayments(long memberId)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                #region Temporary Allow User
                var ExhibitionMember = ExhibitionMemberService.GetExhibitionMemberByItemId(memberId);
                if (ExhibitionMember != null && !string.IsNullOrEmpty(ExhibitionMember.FileNumber))
                {
                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        var entity = _context.FinDueExcel.Where(i => i.FileNumber.ToString().Equals(ExhibitionMember.FileNumber)).FirstOrDefault();
                        if (entity != null)
                        {
                            return entity.CustomerBalance.Value;
                        }
                    }
                }
                #endregion
            }
            return 0;
        }
        public static bool IsValidForRegistrationFromPreviousPaymentsOld(long exhibitionId, long memberId)
        {
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {
                ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService();
                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                XsiExhibition exhibitionentity = ExhibitionService.GetExhibition(new XsiExhibition() { ExhibitionId = exhibitionId }).FirstOrDefault();
                if (exhibitionentity != default(XsiExhibition))
                {
                    #region Temporary Allow User
                    var ExhibitionMember = ExhibitionMemberService.GetExhibitionMemberByItemId(memberId);
                    if (ExhibitionMember != null)
                    {
                        if (ExhibitionMember.FileNumber == "4419")
                            return true;
                        if (ExhibitionMember.FileNumber == "16")
                            return true;
                        if (ExhibitionMember.FileNumber == "7357")
                            return true;
                        if (ExhibitionMember.FileNumber == "2002")
                            return true;
                        if (ExhibitionMember.FileNumber == "4601")
                            return true;
                        if (ExhibitionMember.FileNumber == "3108")
                            return true;
                        if (ExhibitionMember.FileNumber == "4140")
                            return true;
                        if (ExhibitionMember.FileNumber == "4216")
                            return true;
                        if (ExhibitionMember.FileNumber == "9890")
                            return true;
                        if (ExhibitionMember.FileNumber == "9992")
                            return true;
                    }
                    #endregion

                    // List<long> PrevExhibitionIds = ExhibitionService.GetExhibition(new XsiExhibition() { WebsiteId = exhibitionentity.WebsiteId, IsActive = EnumConversion.ToString(EnumBool.Yes), IsArchive = EnumConversion.ToString(EnumBool.Yes) }, EnumSortlistBy.ByItemIdDesc).Select(i => i.ItemId).ToList();
                    List<long> PrevExhibitionIds = ExhibitionService.GetExhibition(new XsiExhibition() { IsActive = EnumConversion.ToString(EnumBool.Yes), IsArchive = EnumConversion.ToString(EnumBool.Yes) }, EnumSortlistBy.ByItemIdDesc).Select(i => i.ExhibitionId).ToList();
                    XsiExhibitionMemberApplicationYearly exhibitorEntity = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly() { MemberId = memberId }).Where(i => (i.MemberRoleId == 1 || i.MemberRoleId == 8) && PrevExhibitionIds.Contains(i.ExhibitionId.Value)).OrderByDescending(i => i.MemberExhibitionYearlyId).FirstOrDefault();
                    if (exhibitorEntity != default(XsiExhibitionMemberApplicationYearly))
                    {
                        InvoiceService InvoiceService = new InvoiceService();
                        List<long> PrevYearInoiceIds = InvoiceService.GetInvoice(new XsiInvoice() { ExhibitorId = exhibitorEntity.MemberExhibitionYearlyId }).Where(i => i.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingPayment) || i.Status == EnumConversion.ToString(EnumExhibitorStatus.PartiallyPaid)).Select(i => i.ItemId).ToList();
                        if (PrevYearInoiceIds.Count() > 0)
                        {
                            EReceiptService EReceiptService = new EReceiptService();
                            var EReceiptPrevList = EReceiptService.GetEReceipt(new XsiEreceipt() { IsPayLater = EnumConversion.ToString(EnumBool.Yes), ExhibitorId = exhibitorEntity.MemberExhibitionYearlyId }).Where(i => PrevYearInoiceIds.Contains(i.InvoiceId.Value)).ToList();
                            if (EReceiptPrevList.Count() > 0)
                                return true;
                            else
                            {
                                /* Check Allow upto 100 AED due amount */
                                var invoiceSumTotalAEDFromPreviousYears = InvoiceService
                                    .GetInvoice(new XsiInvoice() { ExhibitorId = exhibitorEntity.MemberExhibitionYearlyId }).Where(i =>
                                        (i.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingPayment) || i.Status == EnumConversion.ToString(EnumExhibitorStatus.PartiallyPaid)))
                                    .Select(i => i.TotalAed.ChangeToDecimal()).Sum();

                                var invoiceSumPaidAmountFromPreviousYears = InvoiceService
                                    .GetInvoice(new XsiInvoice() { ExhibitorId = exhibitorEntity.MemberExhibitionYearlyId }).Where(i =>
                                    (i.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingPayment) || i.Status == EnumConversion.ToString(EnumExhibitorStatus.PartiallyPaid)))
                                    .Select(i => i.PaidAmount.ChangeToDecimal()).Sum();

                                invoiceSumTotalAEDFromPreviousYears =
                                    invoiceSumTotalAEDFromPreviousYears - invoiceSumPaidAmountFromPreviousYears;
                                if (invoiceSumTotalAEDFromPreviousYears <= 100)
                                    return true;

                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }
        public static bool RejectedExhibitor(long exhibitionId, long memberId, long memberroleId)
        {
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {
                XsiExhibition whereQuery = new XsiExhibition();
                whereQuery.ExhibitionId = exhibitionId;
                List<XsiExhibition> ExhibitionGenericList = ExhibitionService.GetExhibition(whereQuery).ToList();
                if (ExhibitionGenericList.Count() > 0)
                {
                    ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                    foreach (XsiExhibition entity in ExhibitionGenericList)
                    {
                        XsiExhibitionMemberApplicationYearly ExhibitorRegistrationEntity = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly { MemberId = memberId, MemberRoleId = memberroleId, ExhibitionId = entity.ExhibitionId, IsActive = EnumConversion.ToString(EnumBool.Yes), Status = EnumConversion.ToString(EnumExhibitorStatus.Rejected) }).FirstOrDefault();
                        if (ExhibitorRegistrationEntity != null)
                            return true;
                    }
                }
            }
            return false;
        }
        public static bool CancelledExhibitor(long ExhibitionId, long SIBFMemberId, long memberroleId)
        {
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {
                XsiExhibition whereQuery = new XsiExhibition();
                whereQuery.ExhibitionId = ExhibitionId;
                List<XsiExhibition> ExhibitionGenericList = ExhibitionService.GetExhibition(whereQuery).ToList();
                if (ExhibitionGenericList.Count() > 0)
                {
                    ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                    foreach (XsiExhibition entity in ExhibitionGenericList)
                    {
                        XsiExhibitionMemberApplicationYearly ExhibitorRegistrationEntity = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly { MemberId = SIBFMemberId, MemberRoleId = memberroleId, ExhibitionId = entity.ExhibitionId, IsActive = EnumConversion.ToString(EnumBool.Yes), Status = EnumConversion.ToString(EnumExhibitorStatus.Cancelled) }).FirstOrDefault();
                        if (ExhibitorRegistrationEntity != null)
                            return true;
                    }
                }
            }
            return false;
        }
        public static string BindExhibitionArea(long itemId, long langId)
        {
            using (ExhibitionAreaService ExhibitionAreaService = new ExhibitionAreaService())
            {
                XsiExhibitionArea entity = ExhibitionAreaService.GetExhibitionAreaByItemId(itemId);
                if (entity != null)
                {
                    if (langId == 1)
                        return entity.Title;
                    else
                        return entity.TitleAr;
                }
                return "";
            }
        }
        public static string BindAreaShape(string itemId, long langId)
        {
            switch (itemId)
            {
                case "D":
                    return "Equipped Booth";
                case "N":
                    return "Equipped Booth (no shelves)";
                case "O":
                    return "Open Space";
                default:
                    return "";
            }
        }
        public static string GetAwardOrSubAwardTitle(long subawardid, long languageId, long awardid)
        {
            using (AwardsService AwardsService = new AwardsService())
            {

                SubAwardsService SubAwardsService = new SubAwardsService();
                XsiAwards where = new XsiAwards();
                XsiSubAwards subawardwhere = new XsiSubAwards();
                long websiteId = Convert.ToInt32(EnumWebsiteTypeId.SIBF);

                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                where.ItemId = awardid;

                XsiAwards awardentity = AwardsService.GetAwards(where).FirstOrDefault();
                if (awardentity != null)
                {
                    if (subawardid != -1)
                    {
                        var predicateSubAward = PredicateBuilder.True<XsiSubAwards>();

                        subawardwhere.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        subawardwhere.ItemId = subawardid;
                        subawardwhere.AwardId = awardid;

                        XsiSubAwards subawardentity = SubAwardsService.GetSubAwards(subawardwhere).FirstOrDefault();
                        if (subawardentity != null && subawardentity.Name != null && !string.IsNullOrEmpty(subawardentity.Name))
                            return languageId == 1 ? awardentity.Name : awardentity.NameAr;
                    }

                    return languageId == 1 ? awardentity.Name : awardentity.NameAr;
                }
                return string.Empty;
            }
        }
        public static string GetCountryName(long countryId, long langId)
        {
            using (ExhibitionCountryService ExhibitionCountryService = new ExhibitionCountryService())
            {
                var entity = ExhibitionCountryService.GetExhibitionCountryByItemId(countryId);
                if (entity != null)
                {
                    if (langId == 1)
                        return entity.CountryName;
                    else
                        return entity.CountryNameAr;
                }
                return string.Empty;
            }
        }
        public static string GetCountryNameNew(long countryId, long langId, IMemoryCache _cache)
        {
            var cachedCountryData = (List<XsiExhibitionCountry>)_cache.Get<dynamic>(CacheKeys.ExhibitionCountry);

            if (cachedCountryData == null)
                cachedCountryData = CacheKeys.GetExhibitionCountry(_cache);

            var predicate = PredicateBuilder.New<XsiExhibitionCountry>();
            // predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
            // predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicate.And(i => i.CountryId == countryId);

            var entity = cachedCountryData.AsQueryable().Where(predicate).FirstOrDefault();
            if (entity != null)
            {
                if (langId == 1)
                    return entity.CountryName;
                else
                    return entity.CountryNameAr;
            }
            return string.Empty;
        }
        public static string GetCityName(long itemId, long langId)
        {
            using (ExhibitionCityService ExhibitionCityService = new ExhibitionCityService())
            {
                var entity = ExhibitionCityService.GetExhibitionCityByItemId(itemId);
                if (entity != null)
                {
                    if (langId == 1)
                        return entity.CityName;
                    else
                        return entity.CityNameAr;
                }
                return "";
            }
        }
        public static string GetExhibitionCategory(long itemId, long langId)
        {
            using (ExhibitionCategoryService ExhibitionCategoryService = new ExhibitionCategoryService())
            {
                var entity = ExhibitionCategoryService.GetExhibitionCategoryByItemId(itemId);
                if (entity != null)
                {
                    if (langId == 1)
                        return entity.Title;
                    else
                        return entity.TitleAr;
                }
                return "";
            }
        }
        public static DateTime? GetExhibitionDateOfDue(long websiteId)
        {
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {
                XsiExhibition whereQuery = new XsiExhibition();
                whereQuery.IsActive = EnumConversion.ToString(EnumBool.Yes);
                whereQuery.IsArchive = EnumConversion.ToString(EnumBool.No);
                whereQuery.WebsiteId = websiteId;
                XsiExhibition entity = ExhibitionService.GetExhibition(whereQuery).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                if (entity != null)
                {
                    if (entity.DateOfDue != null)
                        return entity.DateOfDue.Value;
                }
            }
            return null;
        }
        public static string GetDateFormatted(DateTime date, long langId)
        {
            if (langId == 2)
                return date.ToString("MMM dd, yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
            else
                return date.ToString("MMM dd, yyyy");
        }
        public static long GetMemberLanguageId(long memId)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                var member = ExhibitionMemberService.GetExhibitionMemberByItemId(memId);
                if (member != default(XsiExhibitionMember) && member.LanguageUrl != null)
                    return member.LanguageUrl.Value;
                return 1;
            }
        }
        public static string GetBoothSubSection(long subsectionId, long sectionId, long langid = 1)
        {
            ExhibitionBoothService ExhibitionBoothService = new ExhibitionBoothService();
            XsiExhibitionBooth whereBoothSection = new XsiExhibitionBooth();
            whereBoothSection.ItemId = sectionId;
            XsiExhibitionBooth ExhibitionBoothEntity = ExhibitionBoothService.GetExhibitionBooth(whereBoothSection, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            if (ExhibitionBoothEntity != null)
            {
                ExhibitionBoothSubsectionService ExhibitionBoothSubsectionService = new ExhibitionBoothSubsectionService();
                XsiExhibitionBoothSubsection whereBoothSubSection = new XsiExhibitionBoothSubsection();
                whereBoothSubSection.ItemId = subsectionId;
                whereBoothSubSection.CategoryId = ExhibitionBoothEntity.ItemId;
                XsiExhibitionBoothSubsection ExhibitionBoothSubSectionEntity = ExhibitionBoothSubsectionService.GetExhibitionBoothSubsection(whereBoothSubSection, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                if (ExhibitionBoothSubSectionEntity != null)
                    return langid == 1 ? ExhibitionBoothSubSectionEntity.Title : ExhibitionBoothSubSectionEntity.TitleAr;
            }
            return "-";
        }
        public static string GetBoothSection(long itemId, long langid = 1)
        {
            ExhibitionBoothService ExhibitionBoothService = new ExhibitionBoothService();
            XsiExhibitionBooth whereBoothSection = new XsiExhibitionBooth();
            whereBoothSection.ItemId = itemId;
            XsiExhibitionBooth ExhibitionBoothEntity = ExhibitionBoothService.GetExhibitionBooth(whereBoothSection, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            if (ExhibitionBoothEntity != null)
                return langid == 1 ? ExhibitionBoothEntity.Title : ExhibitionBoothEntity.TitleAr;
            return "-";
        }
        #endregion

        #region Professional Program
        public static XsiExhibitionProfessionalProgramRegistration VerifyProfessionalProgramRegistration(long memberId, long ppid = -1)
        {
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                ProfessionalProgramService ProfessionalProgramService = new ProfessionalProgramService();

                XsiExhibitionProfessionalProgram where1 = new XsiExhibitionProfessionalProgram();
                where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                where1.IsArchive = EnumConversion.ToString(EnumBool.No);
                XsiExhibitionProfessionalProgram entity = ProfessionalProgramService.GetProfessionalProgram(where1).FirstOrDefault();
                XsiExhibitionProfessionalProgramRegistration model = null;
                if (entity != null)
                {
                    XsiExhibitionProfessionalProgramRegistration where = new XsiExhibitionProfessionalProgramRegistration();
                    where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    where.MemberId = memberId;
                    if (ppid != -1)
                        where.ProfessionalProgramId = ppid;
                    else
                        where.ProfessionalProgramId = entity.ProfessionalProgramId;
                    model = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistration(where).FirstOrDefault();
                }
                return model;
            }
        }
        public static bool IsFirstimeUserForPP(long memberId)
        {
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                XsiExhibitionProfessionalProgramRegistration where = new XsiExhibitionProfessionalProgramRegistration();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                where.MemberId = memberId;
                if (ProfessionalProgramRegistrationService.GetProfessionalProgramRegistration(where).FirstOrDefault() == null)
                    return true;
                return false;
            }
        }
        public static long GetRegisteredId(long memberId)
        {
            using (ProfessionalProgramService ProfessionalProgramService = new ProfessionalProgramService())
            {
                XsiExhibitionProfessionalProgram where = new XsiExhibitionProfessionalProgram();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                where.IsArchive = EnumConversion.ToString(EnumBool.No);
                XsiExhibitionProfessionalProgram entity = ProfessionalProgramService.GetProfessionalProgram(where).FirstOrDefault();
                if (entity != null)
                {
                    using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
                    {
                        #region Get RegisteredId For Logged In Member
                        XsiExhibitionProfessionalProgramRegistration registeredwhere2 = new XsiExhibitionProfessionalProgramRegistration();
                        registeredwhere2.MemberId = memberId;
                        registeredwhere2.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        registeredwhere2.ProfessionalProgramId = entity.ProfessionalProgramId;
                        registeredwhere2.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Approved);
                        XsiExhibitionProfessionalProgramRegistration entityPPRegistration = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistration(registeredwhere2, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                        if (entityPPRegistration != null)
                            return entityPPRegistration.ItemId;
                        #endregion
                    }
                }
            }
            return -1;
        }
        public static XsiExhibitionProfessionalProgram GetProfessionalProgram(long ppid = -1)
        {
            using (ProfessionalProgramService ProfessionalProgramService = new ProfessionalProgramService())
            {
                XsiExhibitionProfessionalProgram where = new XsiExhibitionProfessionalProgram();

                if (ppid != -1)
                    where.ProfessionalProgramId = ppid;
                else
                {
                    where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    where.IsArchive = EnumConversion.ToString(EnumBool.No);
                }
                XsiExhibitionProfessionalProgram entity = ProfessionalProgramService.GetProfessionalProgram(where).FirstOrDefault();
                if (entity != null)
                {
                    return entity;
                }
            }
            return null;
        }
        public static List<ProfessionalProgramBook> GetProfessionalProgramBookList(long ppregid)
        {
            List<ProfessionalProgramBook> PPBooksList = new List<ProfessionalProgramBook>();
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                List<XsiExhibitionProfessionalProgramBook> ProfessionalProgramBookList = new List<XsiExhibitionProfessionalProgramBook>();
                ProfessionalProgramBookList = _context.XsiExhibitionProfessionalProgramBook.Where(i => i.ProfessionalProgramRegistrationId == ppregid).ToList();

                if (ProfessionalProgramBookList.Count() > 0)
                {
                    ProfessionalProgramBook ProfessionalProgramBookDTO;
                    foreach (var ProfessionalProgramBookEntity in ProfessionalProgramBookList)
                    {
                        ProfessionalProgramBookDTO = new ProfessionalProgramBook();
                        ProfessionalProgramBookDTO.Title = ProfessionalProgramBookEntity.Title;
                        ProfessionalProgramBookDTO.AuthorName = ProfessionalProgramBookEntity.AuthorName;

                        if (ProfessionalProgramBookEntity.LanguageId != null)
                            ProfessionalProgramBookDTO.OriginalLangugeId = ProfessionalProgramBookEntity.LanguageId.Value;

                        ProfessionalProgramBookDTO.TranslatedLanguageId = _context.XsiExhibitionPpbooksLanguage.Where(i => i.PpbookId == ProfessionalProgramBookEntity.ItemId).Select(i => i.LanguageId.Value).FirstOrDefault();
                        ProfessionalProgramBookDTO.TranslatedLanguageList = _context.XsiExhibitionPpbooksLanguage.Where(i => i.PpbookId == ProfessionalProgramBookEntity.ItemId).Select(i => i.LanguageId.Value).ToList();

                        PPBooksList.Add(ProfessionalProgramBookDTO);
                    }
                }
            }
            return PPBooksList;
        }
        public static bool IsAppointmentDateAvailable(long PPItemId)
        {
            using (ProfessionalProgramService ProfessionalProgramService = new ProfessionalProgramService())
            {
                XsiExhibitionProfessionalProgram entity = ProfessionalProgramService.GetProfessionalProgramByItemId(PPItemId);
                if (entity != default(XsiExhibitionProfessionalProgram) && entity.AppointmentStartDate != null && entity.AppointmentEndDate != null)
                    if (MethodFactory.ArabianTimeNow() >= entity.AppointmentStartDate && MethodFactory.ArabianTimeNow() <= entity.AppointmentEndDate)
                        return true;
            }
            return false;
        }

        public static string GetPPType(string type)
        {
            if (type == "U")
                return "Arabic World";
            else if (type == "I")
                return "International";
            else
                return "";
        }
        public static string GetPPTitle(string type)
        {
            if (type == "M")
                return "Mr";
            else if (type == "F")
                return "Ms";
            else
                return "";
        }
        public static string GetGenreName(long? itemId, long langId)
        {
            using (GenreService GenreService = new GenreService())
            {
                if (itemId.HasValue)
                {
                    XsiGenre genreEntity = GenreService.GetGenre(new XsiGenre { ItemId = itemId.Value }).FirstOrDefault();
                    if (genreEntity != null)
                    {
                        if (genreEntity.Title != null && langId == 1)
                        {
                            return genreEntity.Title;
                        }
                        else
                        {
                            return genreEntity.TitleAr;
                        }
                    }
                }
            }
            return "";
        }
        public static string GetLanguage(long bookId, long langId)
        {
            using (PPBooksLanguageService PPBooksLanguageService = new PPBooksLanguageService())
            {
                HashSet<long> PPBooksLanguageIds = new HashSet<long>(PPBooksLanguageService.GetPPBooksLanguage(new XsiExhibitionPpbooksLanguage() { PpbookId = bookId }).Select(p => p.LanguageId.Value).ToList());
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    if (langId == 1)
                    {
                        var language = _context.XsiExhibitionLanguage.Where(x => PPBooksLanguageIds.Contains(x.ItemId)).Select(x => x.Title).ToList();
                        return string.Join(',', language);
                    }
                    else
                    {
                        var language = _context.XsiExhibitionLanguage.Where(x => PPBooksLanguageIds.Contains(x.ItemId)).Select(x => x.TitleAr).ToList();
                        return string.Join(',', language);
                    }
                }
            }
        }
        public static long GetHostTableNumber(long itemID)
        {
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                XsiExhibitionProfessionalProgramRegistration entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(itemID);
                if (entity != default(XsiExhibitionProfessionalProgramRegistration))
                {
                    if (entity.TableId != null)
                        return entity.TableId.Value;
                }
            }
            return -1;
        }
        public static string GetTimeSlot(long langID, XsiExhibitionProfessionalProgramSlots SlotEntity)
        {
            if (langID == 1)
                return SlotEntity.StartTime.Value.ToString("dd/MM/yyyy") + " from: " + SlotEntity.StartTime.Value.ToString("HH:mm") + " to: " + SlotEntity.EndTime.Value.ToString("HH:mm");
            else if (langID == 2)
                return SlotEntity.StartTime.Value.ToString("dd/MM/yyyy") + " من: " + SlotEntity.StartTime.Value.ToString("HH:mm") + " إلى: " + SlotEntity.EndTime.Value.ToString("HH:mm");
            return string.Empty;
        }
        #endregion

        #region StaffGuest
        public static bool IsSIBFSTaff(long memId)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                XsiExhibitionMember entity = ExhibitionMemberService.GetExhibitionMemberByItemId(memId);
                if (entity != default(XsiExhibitionMember))
                    if (entity.IsSibfdepartment == EnumConversion.ToString(EnumBool.Yes))
                        return true;
            }
            return false;
        }
        public static bool IsHospitalityPackage(long pcregid)
        {
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                XsiExhibitionProfessionalProgramRegistration entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(pcregid);
                if (entity != default(XsiExhibitionProfessionalProgramRegistration) && entity.IsHopitalityPackage == EnumConversion.ToString(EnumBool.Yes))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion StaffGuest

        #region Image Resize
        public static void ResizeImage(string imagePath, int imgWidth, int imgHeight, string outputImage)
        {
            using (var image = new MagickImage(imagePath))
            {
                image.Resize(imgWidth, imgHeight);
                // image.Crop();
                //  image.Quality = 90;
                image.Write(outputImage);
            }
        }


        #endregion

        #region Get Info: Dashboad Section Status and Updated On
        public static DashboardStatus GetStatusUpdateInfo(long applicationId, MainDashboardDTO dashboard, DashboardStatus dto, long langid)
        {
            if (langid == 1)
            {
                dto.StatusLabel = "Status ";
                dto.UpdatedOnLabel = "Last updated on ";
            }
            else
            {
                dto.StatusLabel = "الحالة ";
                dto.UpdatedOnLabel = " آخر تحديث في ";
            }

            var TGListYears = GetLatestTranslationGrants();
            switch (applicationId)
            {
                case 1: return ExhibitorRegistration(dashboard, dto, langid);
                case 2: return AgencyRegistration(dashboard, dto, langid);
                case 3: return Representative(dashboard, dto, langid);
                case 4: return StaffGuest(dashboard, dto, langid);
                case 5: return PublisherConference(dashboard, dto, langid);
                case 7: return dto;
                case 8: return RestaurantRegistration(dashboard, dto, langid);
                case 9: return Payment(dashboard, dto, langid);
                case 10: return BooksParticipating(dashboard, dto, langid);
                case 11: return MyAgencyStatuses(dashboard, dto, langid);
                case 12: return AddBooks(dashboard, dto, langid);
                case 13: return StaffGuestBadge(dashboard, dto, langid);
                case 14: return Badge(dashboard, dto, langid);
                case 15: return InvoiceEnquiry(dashboard, dto, langid);
                case 16: return MemberDashboard(dashboard, dto, langid);

                case 6: return TranslationGrantOnStatusPage(dashboard, dto, TGListYears, langid, 6);
                case 17: return TranslationGrantOnStatusPage(dashboard, dto, TGListYears, langid, 17);
                case 18: return TranslationGrantOnStatusPage(dashboard, dto, TGListYears, langid, 18);
                default:
                    return dto;
            }
        }
        public static DashboardStatus ExhibitorRegistration(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                ExhibitionService ExhibitionService = new ExhibitionService();
                var exhibitorEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(dashboard.ExhibitorId);
                if (exhibitorEntity != null)
                {
                    var exhibtionentity = ExhibitionService.GetExhibitionByItemId(exhibitorEntity.ExhibitionId.Value);
                    dto.Title = (langid == 1) ? "Exhibitor Registration" : "التسجيل كعارض";
                    dto.Status = MethodFactory.GetExhibitorRegistrationStatus(exhibitorEntity.Status, langid);
                    dto.UpdatedOn = exhibitorEntity.ModifiedOn.Value.ToString("dd/MMM/yyyy HH:mm");

                    dto.StandNumberLabel = (langid == 1) ? "Stand Number" : "رقم الجناح";
                    dto.HallNumberLabel = (langid == 1) ? "Hall Number" : "رقم القاعة";
                    dto.StandSectionLabel = (langid == 1) ? "Stand Section" : "الجناح";
                    dto.StandSubSectionLabel = (langid == 1) ? "Stand Sub Section" : "القسم الفرعي للجناح";
                    dto.AllocatedAreaLabel = (langid == 1) ? "Allocated Area" : "المساحة المخصصة";

                    dto.StandNumber = "-";
                    dto.HallNumber = "-";
                    dto.StandSection = "-";
                    dto.StandSubSection = "-";
                    dto.AllocatedArea = "-";

                    var details = ExhibitorRegistrationService.GetDetailsById(dashboard.ExhibitorId);
                    if (details != null)
                    {
                        if (exhibitorEntity.Status == "I" || exhibitorEntity.Status == "A")
                        {
                            if (!string.IsNullOrWhiteSpace(details.StandNumberEnd) && !string.IsNullOrWhiteSpace(details.StandNumberStart))
                                dto.StandNumber = details.StandNumberEnd + "-" + details.StandNumberStart;
                            else if (!string.IsNullOrWhiteSpace(details.StandNumberEnd))
                                dto.StandNumber = details.StandNumberEnd;
                            else if (!string.IsNullOrWhiteSpace(details.StandNumberStart))
                                dto.StandNumber = details.StandNumberStart;

                            if (!string.IsNullOrEmpty(details.HallNumber))
                                dto.HallNumber = details.HallNumber;
                            else
                                dto.HallNumber = "-";
                            dto.StandSection = GetBoothSection(details.BoothSectionId.Value, langid);
                            dto.StandSubSection = GetBoothSubSection(details.BoothSubSectionId.Value, details.BoothSectionId.Value, langid);

                            if (!string.IsNullOrEmpty(details.AllocatedSpace))
                            {
                                dto.AllocatedArea = details.AllocatedSpace;

                                if (!string.IsNullOrEmpty(details.Area))
                                {
                                    dto.AllocatedArea = dto.AllocatedArea + " " + details.Area;
                                }
                            }
                        }
                    }

                    //if ((entity.Status == "I" || entity.Status == "A") && !string.IsNullOrEmpty(entity.WifiVoucherCode))
                    //{
                    //    if (exhibtionentity != null && exhibtionentity.WebsiteId != null)
                    //        dto.BadgeURL = GetExhibitorBadgeLinkToSend(entity.MemberExhibitionYearlyId, exhibtionentity.WebsiteId.Value);
                    //}
                }
                else
                {
                    dto.Title = (langid == 1) ? "Exhibitor Registration" : "التسجيل كعارض";
                    dto.Status = MethodFactory.GetExhibitorRegistrationStatus(exhibitorEntity.Status, langid);
                    dto.UpdatedOn = exhibitorEntity.ModifiedOn.Value.ToString("dd/MMM/yyyy HH:mm");

                    dto.StandNumberLabel = (langid == 1) ? "Stand Number" : "رقم الجناح";
                    dto.HallNumberLabel = (langid == 1) ? "Hall Number" : "رقم القاعة";
                    dto.StandSectionLabel = (langid == 1) ? "Stand Section" : "الجناح";
                    dto.StandSubSectionLabel = (langid == 1) ? "Stand Sub Section" : "القسم الفرعي للجناح";
                    dto.AllocatedAreaLabel = (langid == 1) ? "Allocated Area" : "المساحة المخصصة";

                    dto.StandNumber = "-";
                    dto.HallNumber = "-";
                    dto.StandSection = "-";
                    dto.StandSubSection = "-";
                    dto.AllocatedArea = "-";
                }
            }
            return dto;
        }
        public static DashboardStatus AgencyRegistration(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                ExhibitionExhibitorDetailService ExhibitionExhibitorDetailService = new ExhibitionExhibitorDetailService();
                dto.Title = langid == 1 ? "Agency Registration" : "التسجيل كتوكيل";
                if (dashboard.AgencyId != null)
                {
                    var agencyentity = AgencyRegistrationService.GetAgencyRegistrationByItemId(dashboard.AgencyId.Value);
                    if (agencyentity != null)
                    {
                        dto.Status = MethodFactory.GetAgencyRegistrationStatus(agencyentity.Status, langid);
                        dto.UpdatedOn = agencyentity.ModifiedOn.Value.ToString("dd/MMM/yyyy HH:mm");
                        dto.StandNumber = "-";
                        if (agencyentity.ParentId != null)
                        {
                            var details = ExhibitionExhibitorDetailService.GetExhibitorRegistrationDetails(new XsiExhibitionExhibitorDetails() { MemberExhibitionYearlyId = agencyentity.ParentId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                            if (details != null)
                            {
                                if (agencyentity.Status == "E" || agencyentity.Status == "A")
                                {
                                    if (!string.IsNullOrWhiteSpace(details.StandNumberEnd) && !string.IsNullOrWhiteSpace(details.StandNumberStart))
                                        dto.StandNumber = details.StandNumberEnd + "-" + details.StandNumberStart;
                                    else if (!string.IsNullOrWhiteSpace(details.StandNumberEnd))
                                        dto.StandNumber = details.StandNumberEnd;
                                    else if (!string.IsNullOrWhiteSpace(details.StandNumberStart))
                                        dto.StandNumber = details.StandNumberStart;
                                }
                            }
                        }

                        if (langid == 1)
                            dto.StandNumberLabel = "Stand Number ";
                        else
                            dto.StandNumberLabel = "الجناح ";
                    }
                }
            }
            return dto;
        }
        public static DashboardStatus RestaurantRegistration(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            using (RestaurantRegistrationService RestaurantRegistrationService = new RestaurantRegistrationService())
            {
                var entity = RestaurantRegistrationService.GetRestaurantRegistrationByItemId(dashboard.ExhibitorId);
                if (entity != null)
                {
                    dto.Title = langid == 1 ? "Restaurant Registration" : "تسجيل المطاعم";
                    dto.Status = MethodFactory.GetRestaurantRegistrationStatus(entity.Status, langid);
                    dto.UpdatedOn = entity.ModifiedOn.Value.ToString("dd/MMM/yyyy");
                }
            }
            return dto;
        }
        public static DashboardStatus Payment(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            List<DashboardStatus> sublist = new List<DashboardStatus>();
            dto.Title = langid == 1 ? "Payment" : "السداد";
            dto.Status = string.Empty;
            dto.UpdatedOn = string.Empty;

            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                DashboardStatus obj;
                var predicate = PredicateBuilder.New<XsiInvoice>();
                predicate = predicate.And(i => i.ExhibitorId == dashboard.ExhibitorId);
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                List<XsiInvoice> invoiceList = _context.XsiInvoice.Where(predicate).ToList();

                foreach (var item in invoiceList)
                {
                    obj = new DashboardStatus();
                    obj.Title = item.InvoiceNumber;
                    obj.Status = MethodFactory.GetInvoiceStatus(item.Status, langid);
                    if (item.ModifiedOn != null)
                        obj.UpdatedOn = item.ModifiedOn.Value.ToString("dd/MMM/yyyy HH:mm");
                    else
                        obj.UpdatedOn = "-";
                    sublist.Add(obj);
                }
            }
            if (sublist.Count > 0)
                dto.SubList = sublist;
            return dto;
        }
        public static DashboardStatus Representative(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            dto.Title = langid == 1 ? "Representative Registration" : "المندوبين";
            dto.Status = string.Empty;
            dto.UpdatedOn = string.Empty;
            List<DashboardStatus> sublist = new List<DashboardStatus>();
            DashboardStatus obj;

            using (ExhibitionRepresentativeParticipatingService ExhibitionRepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService())
            {
                if (dashboard.ExhibitorId > 0)
                {
                    ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService();
                    var RepresentativeParticipatingList = ExhibitionRepresentativeParticipatingService.GetExhibitionRepresentativeParticipating(new XsiExhibitionRepresentativeParticipating() { MemberExhibitionYearlyId = dashboard.ExhibitorId }, EnumSortlistBy.ByItemIdDesc);
                    foreach (var item in RepresentativeParticipatingList)
                    {
                        obj = new DashboardStatus();
                        var representativeentity = ExhibitionRepresentativeService.GetRepresentativeByItemId(item.RepresentativeId);
                        if (representativeentity != null)
                        {
                            obj.Title = ExhibitionRepresentativeService.GetRepresentativeByItemId(item.RepresentativeId).NameEn;
                        }
                        else
                        {
                            obj.Title = "-";
                        }

                        obj.Status = MethodFactory.GetReprepresentativeStatus(item.Status, 1);
                        obj.UpdatedOn = item.ModifiedOn == null ? "-" : item.ModifiedOn.Value.ToString("dd/MMM/yyyy HH:mm");
                        sublist.Add(obj);
                    }
                    dto.SubList = sublist;
                }
            }

            using (ExhibitionRepresentativeParticipatingNoVisaService ExhibitionRepresentativeParticipatingNoVisaService = new ExhibitionRepresentativeParticipatingNoVisaService())
            {
                if (dashboard.ExhibitorId > 0)
                {
                    ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService();
                    var RepresentativeParticipatingList = ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa(new XsiExhibitionRepresentativeParticipatingNoVisa() { MemberExhibitionYearlyId = dashboard.ExhibitorId }, EnumSortlistBy.ByItemIdDesc);
                    foreach (var item in RepresentativeParticipatingList)
                    {
                        obj = new DashboardStatus();
                        var representativeentity = ExhibitionRepresentativeService.GetRepresentativeByItemId(item.RepresentativeId);
                        if (representativeentity != null)
                        {
                            obj.Title = ExhibitionRepresentativeService.GetRepresentativeByItemId(item.RepresentativeId).NameEn;
                        }
                        else
                        {
                            obj.Title = "-";
                        }

                        obj.Status = MethodFactory.GetReprepresentativeStatus(item.Status, 1);
                        if (item.ModifiedOn != null)
                            obj.UpdatedOn = item.ModifiedOn.Value.ToString("dd/MMM/yyyy HH:mm");
                        else
                            obj.UpdatedOn = "-";
                        sublist.Add(obj);
                    }
                    dto.SubList = sublist;
                }
            }

            if (sublist.Count > 0)
                dto.SubList = sublist;
            return dto;
        }
        public static DashboardStatus StaffGuest(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            dto.Title = langid == 1 ? "Staff Guest Registration" : "إضافة ضيف";
            dto.Status = string.Empty;
            dto.UpdatedOn = string.Empty;
            List<DashboardStatus> sublist = new List<DashboardStatus>();
            DashboardStatus obj;

            using (ExhibitionStaffGuestParticipatingService ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService())
            {
                ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService();
                var staffguestIDs = ExhibitionStaffGuestService.GetExhibitionStaffGuest(new XsiExhibitionStaffGuest() { MemberId = dashboard.MemberId }).Select(i => i.StaffGuestId).ToList();

                var ParticipatingList = ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(new XsiExhibitionStaffGuestParticipating() { ExhibitionId = dashboard.ExhibitionId }).Where(i => staffguestIDs.Contains(i.StaffGuestId)).ToList();
                foreach (var item in ParticipatingList)
                {
                    obj = new DashboardStatus();
                    var entity = ExhibitionStaffGuestService.GetStaffGuestByItemId(item.StaffGuestId);
                    if (entity != null)
                    {
                        obj.Title = entity.NameEn;
                    }
                    else
                    {
                        obj.Title = "-";
                    }

                    obj.Status = MethodFactory.GetReprepresentativeStatus(item.Status, 1);
                    if (item.ModifiedOn != null)
                        obj.UpdatedOn = item.ModifiedOn.Value.ToString("dd/MMM/yyyy HH:mm");
                    else
                        obj.UpdatedOn = "-";
                    sublist.Add(obj);
                }
                dto.SubList = sublist;
            }

            if (sublist.Count > 0)
                dto.SubList = sublist;
            return dto;
        }
        public static DashboardStatus PublisherConference(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                var entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistration(new XsiExhibitionProfessionalProgramRegistration() { MemberId = dashboard.MemberId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                if (entity != null)
                {
                    dto.Title = langid == 1 ? "Publisher Conference Registration" : "مؤتمر الناشرين";
                    dto.Status = MethodFactory.GetProfessionalProgramStatus(entity.Status, langid);
                    dto.UpdatedOn = entity.ModifiedOn.Value.ToString("dd/MMM/yyyy HH:mm");
                    //if (entity.Status == "A")
                    //    dto.BadgeURL = "https://tgform.sibf.com/pcbadgeprint?item=" + entity.ItemId;
                }
            }
            return dto;
        }
        public static List<XsiExhibitionTranslationGrant> GetLatestTranslationGrants()
        {

            using (TranslationGrantService TranslationGrantService = new TranslationGrantService())
            {
                XsiExhibitionTranslationGrant where = new XsiExhibitionTranslationGrant();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                where.IsArchive = EnumConversion.ToString(EnumBool.No);
                var list = TranslationGrantService.GetTranslationGrant(where, EnumSortlistBy.ByItemIdDesc).Take(3).ToList();
                return list;
            }
        }
        public static string TranslationGrant(List<XsiExhibitionTranslationGrant> tglist, long langid, long id = 6)
        {
            if (tglist.Count == 3)
            {
                if (id == 17)
                    return langid == 1 ? tglist[1].Title : tglist[1].TitleAr;
                else if (id == 18)
                    return langid == 1 ? tglist[2].Title : tglist[2].TitleAr;
                else
                    return langid == 1 ? tglist[0].Title : tglist[0].TitleAr;
            }
            else if (tglist.Count == 2)
            {
                if (id == 17)
                    return langid == 1 ? tglist[1].Title : tglist[1].TitleAr;
                else
                    return langid == 1 ? tglist[0].Title : tglist[0].TitleAr;
            }
            else if (tglist.Count == 1)
            {
                var TGListYears = GetLatestTranslationGrants();
                return langid == 1 ? tglist[0].Title : tglist[0].TitleAr;
            }
            else
                return "-";
        }
        public static DashboardStatus TranslationGrantOnStatusPage(MainDashboardDTO dashboard, DashboardStatus dto, List<XsiExhibitionTranslationGrant> tglist, long langid, long id = 6)
        {
            using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
            {
                List<DashboardStatus> sublist = new List<DashboardStatus>();

                if (tglist.Count == 3)
                {
                    if (id == 6)
                    {
                        dto.Title = langid == 1 ? tglist[0].Title : tglist[0].TitleAr;
                        dto.SubList = GetTGSubList(dashboard, tglist[0].ItemId, TranslationGrantMemberService, sublist);
                    }
                    else if (id == 17)
                    {
                        dto.Title = langid == 1 ? tglist[1].Title : tglist[1].TitleAr;
                        dto.SubList = GetTGSubList(dashboard, tglist[1].ItemId, TranslationGrantMemberService, sublist);
                    }
                    else if (id == 18)
                    {
                        dto.Title = langid == 1 ? tglist[2].Title : tglist[2].TitleAr;
                        dto.SubList = GetTGSubList(dashboard, tglist[2].ItemId, TranslationGrantMemberService, sublist);
                    }
                }
                else if (tglist.Count == 2)
                {
                    if (id == 6)
                    {
                        dto.Title = langid == 1 ? tglist[0].Title : tglist[0].TitleAr;
                        dto.SubList = GetTGSubList(dashboard, tglist[0].ItemId, TranslationGrantMemberService, sublist);
                    }
                    else if (id == 17)
                    {
                        dto.Title = langid == 1 ? tglist[1].Title : tglist[1].TitleAr;
                        dto.SubList = GetTGSubList(dashboard, tglist[1].ItemId, TranslationGrantMemberService, sublist);
                    }
                }
                else if (tglist.Count == 1)
                {
                    if (id == 6)
                    {
                        dto.Title = langid == 1 ? tglist[0].Title : tglist[0].TitleAr;
                        dto.SubList = GetTGSubList(dashboard, tglist[0].ItemId, TranslationGrantMemberService, sublist);
                    }
                }
            }
            return dto;
        }
        public static List<DashboardStatus> GetTGSubList(MainDashboardDTO dashboard, long translationgrantid, TranslationGrantMemberService TranslationGrantMemberService, List<DashboardStatus> sublist)
        {
            DashboardStatus dto1;
            var TGRegistrationList = TranslationGrantMemberService.GetTranslationGrantMember(new XsiExhibitionTranslationGrantMembers() { TranslationGrantId = translationgrantid, MemberId = dashboard.MemberId }).Where(i => i.MemberId != null).OrderByDescending(i => i.ItemId).ToList();
            foreach (var item in TGRegistrationList)
            {
                dto1 = new DashboardStatus();
                dto1.Title = item.BookTitleEnglish;
                dto1.Status = MethodFactory.GetTranslationGrantMemberStatus(item.Status, 1);
                dto1.UpdatedOn = item.ModifiedOn == null ? "-" : item.ModifiedOn.Value.ToString("dd/MMM/yyyy HH:mm");
                sublist.Add(dto1);
            }

            return sublist;
        }
        public static DashboardStatus BooksParticipating(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            dto.Title = langid == 1 ? "Books Participating" : "الكتب المشاركة";
            return dto;
        }
        public static DashboardStatus MyAgencyStatuses(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                dto.Title = langid == 1 ? "My Agency" : "التوكيلات";
                var agencylist = AgencyRegistrationService.GetAgencyRegistration(new XsiExhibitionMemberApplicationYearly() { ParentId = dashboard.ExhibitorId }, EnumSortlistBy.ByItemIdDesc).ToList();

                List<DashboardStatus> sublist = new List<DashboardStatus>();
                DashboardStatus dto1;
                foreach (var item in agencylist)
                {
                    dto1 = new DashboardStatus();
                    dto1.Title = item.PublisherNameEn;
                    dto1.UpdatedOn = item.ModifiedOn == null ? "-" : item.ModifiedOn.Value.ToString("dd/MMM/yyyy HH:mm");
                    dto1.Status = MethodFactory.GetAgencyRegistrationStatus(item.Status, 1);
                    sublist.Add(dto1);
                }
                dto.SubList = sublist;
                return dto;
            }
        }
        public static DashboardStatus AddBooks(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            dto.Title = langid == 1 ? "Add Books" : "إضافة كتب";
            return dto;
        }
        public static DashboardStatus StaffGuestBadge(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            dto.Title = langid == 1 ? "Staff Guest Registration" : "بطاقات الدخول";
            return dto;
        }
        public static DashboardStatus Badge(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            dto.Title = langid == 1 ? "Badge Registration" : "التسجيل لبطاقات دخول العارضين";
            return dto;
        }
        public static DashboardStatus InvoiceEnquiry(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            dto.Title = langid == 1 ? "Invoice Enquiry" : "Invoice Enquiry";
            return dto;
        }
        public static DashboardStatus MemberDashboard(MainDashboardDTO dashboard, DashboardStatus dto, long langid = 1)
        {
            return dto;
        }
        public static string GetExhibitorBadgeLinkToSend(long exhibitorid, long websiteid)
        {
            try
            {
                string url = "https://localapi.scrf.ae/api/Exhibitors/badgeLink/" + exhibitorid;

                if (ServicePointManager.SecurityProtocol != SecurityProtocolType.SystemDefault)
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                }
                WebClient client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                client.Headers[HttpRequestHeader.ContentType] = "application/json; charset=utf-8";
                Stream data = client.OpenRead(url);
                StreamReader reader = new StreamReader(data);
                var responseFromServer = reader.ReadToEnd();
                dynamic myObject = JsonConvert.DeserializeObject<dynamic>(responseFromServer);
                data.Close();
                reader.Close();
                return myObject.ToString();
            }
            catch (Exception ex)
            {
                //using (System.IO.StreamWriter file = new System.IO.StreamWriter("D:\\vhosts\\xsi.sibf.com\\Content\\Uploads\\log.txt", true))
                //{
                //    file.WriteLine("------------------------------------");
                //    file.WriteLine(DateTime.Now);
                //    file.WriteLine("InnerException:" + ex.Message);
                //    file.WriteLine("StackTrace:" + ex.StackTrace);
                //    file.WriteLine("------------------------------------");
                //}
                return string.Empty;
            }
        }
        public static string GetURLforPP(long memberid)
        {
            XsiExhibitionProfessionalProgramRegistration entity = MethodFactory.VerifyProfessionalProgramRegistration(memberid);
            if (entity != default(XsiExhibitionProfessionalProgramRegistration) && entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                return "findprofessional";

            return "publisherconference"; //publisherconference //ProfessionalProgram
        }
        public static string GetApplicationIcon(long applicationId)
        {
            switch (applicationId)
            {
                case 1: return "ex-exhibitor";
                case 2: return "ex-agencies";
                case 3: return "ex-representative";
                case 4: return "ex-representative";
                case 5: return "ex-publisherconference";
                case 6: return "ex-tg";
                case 7: return "ex-others";
                case 8: return "ex-restaurant";
                case 9: return "ex-payments";
                case 10: return "ex-books";
                case 11: return "ex-myagency";
                case 12: return "ex-books";
                case 13: return "ex-badge";
                case 14: return "ex-badge";
                case 15: return "ex-invoiceenquiry";
                case 16: return "ex-member-status";
                case 17: return "ex-tg";
                case 18: return "ex-tg";
                default:
                    return "ex-others";
            }
        }
        public static string GetApplicationURL(long applicationId, long tgid = -1)
        {
            switch (applicationId)
            {
                case 1: return "ExhibitorRegistration";
                case 2: return "Agency";  //AgencyRegistration
                case 3: return "Representative";
                case 4: return "staffguest";
                case 5: return "publisherconference"; //publisherconference  //ProfessionalProgram
                case 6: return "Translationgrant?websiteId=1&id=" + tgid + "&";
                case 7: return "#";
                case 8: return "RestaurantRegistration";
                case 9: return "Payment";
                case 10: return "BooksParticipating";
                case 11: return "MyAgency";
                case 12: return "addbooks";
                case 13: return "staffguestbadge";
                case 14: return "badge";
                case 15: return "invoiceenquiry";
                case 16: return "memberdashboard";
                case 17: return "Translationgrant?websiteId=1&id=" + tgid + "&";
                case 18: return "Translationgrant?websiteId=1&id=" + tgid + "&";
                default:
                    return "#";
            }
        }
        public static string GetVisaType(string visaType)
        {
            if (visaType == "M")
                return "Manual";
            else if (visaType == "E")
                return "Electronic";
            else
                return "-";
        }

        public static string GetGuestCategory(long guestCategoryId)
        {
            using (StaffGuestCategoryService StaffGuestCategoryService = new StaffGuestCategoryService())
            {
                var category = StaffGuestCategoryService.GetStaffGuestCategoryByItemId(guestCategoryId);

                if (category != null)
                {
                    return category.Title;
                }
            }
            return "-";
        }

        public static string GetGuestType(long guestTypeId)
        {
            using (StaffGuestTypeService StaffGuestTypeService = new StaffGuestTypeService())
            {
                var category = StaffGuestTypeService.GetStaffGuestTypeByItemId(guestTypeId);

                if (category != null)
                {
                    return category.Title;
                }
            }
            return "-";
        }
        public static string GetBooleanValue(string paidVisa)
        {
            if (paidVisa == EnumConversion.ToString(EnumBool.Yes))
                return "Yes";
            else if (paidVisa == EnumConversion.ToString(EnumBool.No))
                return "No";
            return "-";
        }

        internal static string GetExhibitionStaffGuestStatus(string strstatus, long langId = 1)
        {
            switch (strstatus)
            {
                case "A": return (langId == 1) ? "Approved" : "مشارك";
                case "P": return (langId == 1) ? "Pending" : "قيد الإجراء";
                case "F": return (langId == 1) ? "Flight Details Pending" : "Flight Details Pending";
                case "Y": return (langId == 1) ? "Pending Documentation" : "وثائق انتظار";
                case "V": return (langId == 1) ? "Visa Processing" : "التأشيرة قيد الإجراء";
                case "U": return (langId == 1) ? "Visa Uploaded" : "تم تحميل التأشيرة";
                case "X": return (langId == 1) ? "Book" : "Book";
                case "Q": return (langId == 1) ? "No Visa Insurance Uploaded" : "No Visa Insurance Uploaded";
                case "R": return (langId == 1) ? "Rejected" : "مرفوض";
                case "O": return (langId == 1) ? "Documents Approved" : "الوثائق المعتمدة";
                case "D": return (langId == 1) ? "Cancelled" : "تم الإلغاء";
                case "L": return (langId == 1) ? "Documents Uploaded" : "وثيقة حملت";
                case "B": return (langId == 1) ? "Blocked" : "Blocked";
                case "C": return (langId == 1) ? "Duplicate" : "Duplicate";
                case "J": return (langId == 1) ? "Reject With Reason" : "Reject With Reason";
                case "W": return (langId == 1) ? "Re Issue Visa" : "Re Issue Visa";
                case "1": return (langId == 1) ? "Cancelled (Cancellation No Charge)" : "Cancelled (Cancellation No Charge)";
                case "2": return (langId == 1) ? "Cancelled (Cancellation Charges and Visa Cost)" : "Cancelled (Cancellation Charges and Visa Cost)";
                case "3": return (langId == 1) ? "Cancelled (Cancellation Charges)" : "Cancelled  (Cancellation Charges)";
                default: return string.Empty;
            }
        }
        #endregion

        public static string GetShipmentTypeName(long shipmenttype, long langid)
        {
            switch (shipmenttype)
            {
                case 1: return langid == 1 ? "Air" : "جوي";
                case 2: return langid == 1 ? "Sea" : "بحري";
                case 3: return langid == 1 ? "Land" : "بري";
                default: return "-";
            }
        }
        public static string GetShipmentLocationTypeName(long locationtype, long langid)
        {
            switch (locationtype)
            {
                case 1: return langid == 1 ? "Sharjah International Airport (Air)" : "مطار الشارقة الدولي (جوي)";
                case 2: return langid == 1 ? "Dubai International Airport (Air)" : "مطار دبي الدولي (جوي)";
                case 3: return langid == 1 ? "Khalid Port  (Sea)" : "ميناء خالد الشارقة (بحري)";
                case 4: return langid == 1 ? "Port Rashid  (Sea)" : "ميناء راشد دبي (بحري)";
                case 5: return langid == 1 ? "Al Ghuwaifat Port Station (Land)" : "المنافذ البرية – الغويفات الحدودي (بري)";
                case 6: return langid == 1 ? "Al Wajajah Port (Land)" : "المنافذ البرية – الوجاجة الحدودي (بري)";
                default: return "-";
            }
        }
        #region Galaxy API
        public static RevRoot GetRevenueInvoiceNumberUsingSIBFFileNumber(string strfilenumber)
        {
            try
            {
                GIGEncryption gIGEncryption = new GIGEncryption();
                GIGEncryptionMobile gIGEncryptionMobile = new GIGEncryptionMobile();
                string strtimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                string pwd = "G@123456";
                string tokenbeforeencryption = "Myahia&" + pwd + "&" + strtimestamp;

                var tokenwithencryption = gIGEncryptionMobile.encrypt(tokenbeforeencryption, "BE1A8B181E6ABDE36EBD0276C18AB995", "getinvoicesstatus");
                string strmodel = "getInvoicesStatus|" + strfilenumber + "|" + strtimestamp;
                var strmodelencrypt = gIGEncryption.Encrypt(strmodel, "BE1A8B181E6ABDE36EBD0276C18AB995");

                //string url = "https://sba-bffinap-01.sharjahba.ae/api/GetInvoicesStatus/" + tokenwithencryption + "?model=" + strmodelencrypt;
                string url = RevenueSystemBillingAPI + "api/GetInvoicesStatus/" + tokenwithencryption + "?model=" + strmodelencrypt;
                WebClient client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                Stream data = client.OpenRead(url);
                StreamReader reader = new StreamReader(data);
                var responseFromServer = reader.ReadToEnd();
                dynamic myObject = JsonConvert.DeserializeObject<dynamic>(responseFromServer);
                var strdecrypt = gIGEncryption.Decrypt(myObject, "BE1A8B181E6ABDE36EBD0276C18AB995");
                dynamic revdata = JObject.Parse(strdecrypt);
                RevRoot InvoiceInfo = JsonConvert.DeserializeObject<RevRoot>(strdecrypt);
                // string strinvoicenumber = revdata.Table[0].InvoiceNo.Value;

                data.Close();
                reader.Close();
                return InvoiceInfo;
            }
            catch (Exception ex)
            {
                return new RevRoot();
                //LogException(ex);
            }
        }
        public static RevenueInvoiceDetails GetInvoiceDetailsGalaxyServiceByRevenueInvoiceNumber(string invoiceno)
        {
            try
            {
                GIGEncryption gIGEncryption = new GIGEncryption();
                GIGEncryptionMobile gIGEncryptionMobile = new GIGEncryptionMobile();
                string strtimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                string pwd = "G@123456";
                string tokenbeforeencryption = "Myahia&" + pwd + "&" + strtimestamp;

                var tokenwithencryption = gIGEncryptionMobile.encrypt(tokenbeforeencryption, "BE1A8B181E6ABDE36EBD0276C18AB995", "getinvoicedetails");
                string strmodel = "getInvoicesStatus|" + invoiceno + "|" + strtimestamp;
                var strmodelencrypt = gIGEncryption.Encrypt(strmodel, "BE1A8B181E6ABDE36EBD0276C18AB995");

                string url = RevenueSystemBillingAPI + "api/GetInvoiceDetails/" + tokenwithencryption + "?model=" + strmodelencrypt;

                WebClient client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                Stream data = client.OpenRead(url);
                StreamReader reader = new StreamReader(data);
                var responseFromServer = reader.ReadToEnd();
                dynamic myObject = JsonConvert.DeserializeObject<dynamic>(responseFromServer);
                var strdecrypt = gIGEncryption.Decrypt(myObject, "BE1A8B181E6ABDE36EBD0276C18AB995");
                dynamic revdata = JObject.Parse(strdecrypt);
                RevenueInvoiceDetails InvoiceInfo = JsonConvert.DeserializeObject<RevenueInvoiceDetails>(strdecrypt);

                data.Close();
                reader.Close();
                return InvoiceInfo;
            }
            catch (Exception ex)
            {
                return new RevenueInvoiceDetails();
                //LogException(ex);
            }
        }
        public static long GetTransRefNumberByRevenueInvoiceNumber(string invoiceno)
        {
            try
            {
                GIGEncryption gIGEncryption = new GIGEncryption();
                GIGEncryptionMobile gIGEncryptionMobile = new GIGEncryptionMobile();
                string strtimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                string pwd = "G@123456";
                string tokenbeforeencryption = "Myahia&" + pwd + "&" + strtimestamp;

                var tokenwithencryption = gIGEncryptionMobile.encrypt(tokenbeforeencryption, "BE1A8B181E6ABDE36EBD0276C18AB995", "getinvoicedetails");
                string strmodel = "getInvoicesStatus|" + invoiceno + "|" + strtimestamp;
                var strmodelencrypt = gIGEncryption.Encrypt(strmodel, "BE1A8B181E6ABDE36EBD0276C18AB995");

                string url = RevenueSystemBillingAPI + "api/GetInvoiceDetails/" + tokenwithencryption + "?model=" + strmodelencrypt;

                WebClient client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                Stream data = client.OpenRead(url);
                StreamReader reader = new StreamReader(data);
                var responseFromServer = reader.ReadToEnd();
                dynamic myObject = JsonConvert.DeserializeObject<dynamic>(responseFromServer);
                var strdecrypt = gIGEncryption.Decrypt(myObject, "BE1A8B181E6ABDE36EBD0276C18AB995");
                dynamic revdata = JObject.Parse(strdecrypt);
                RevenueInvoiceDetails InvoiceInfo = JsonConvert.DeserializeObject<RevenueInvoiceDetails>(strdecrypt);

                data.Close();
                reader.Close();
                return InvoiceInfo.TransRefNumber;
            }
            catch (Exception ex)
            {
                return -1;
                //LogException(ex);
            }
        }
        public static void PushMemberExhibitionMemberApplicationYearlyUpdateDataToGalaxyService(XsiExhibitionMemberApplicationYearly exhibitorentity, string serveraddresscms, long adminuserid, string uploadcmsphysicalpath)
        {
            /*
            try
            {
                GIGEncryption gIGEncryption = new GIGEncryption();
                GIGEncryptionMobile gIGEncryptionMobile = new GIGEncryptionMobile();
                string strtimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                string pwd = "G@123456";
                string tokenbeforeencryption = "Myahia&" + pwd + "&" + strtimestamp;
                string strmodel = String.Empty;

                #region Fields
                string CustomerId = "0";
                string CustomerName_Ar = "-";
                string CustomerName_En = "-";
                string CustomerTypeID = "-";
                long Country = 0;
                string Location = "-";
                string FileNumber = "-";
                string ExID = "110";
                string CustomerEmail = "-";
                string PhoneNo = "-";
                string TRN = "-";
                string Notes = "-";

                CustomerId = exhibitorentity.MemberExhibitionYearlyId.ToString();

                if (exhibitorentity.PublisherNameAr != null && !string.IsNullOrEmpty(exhibitorentity.PublisherNameAr))
                    CustomerName_Ar = exhibitorentity.PublisherNameAr;

                if (exhibitorentity.PublisherNameEn != null && !string.IsNullOrEmpty(exhibitorentity.PublisherNameEn))
                    CustomerName_En = exhibitorentity.PublisherNameEn;

                if (exhibitorentity.MemberRoleId != null)
                    CustomerTypeID = exhibitorentity.MemberRoleId.Value.ToString();

                if (exhibitorentity.CountryId != null)
                    Country = exhibitorentity.CountryId.Value;// MethodFactory.GetCountryTitle(exhibitorentity.CountryId.Value, 1);

                if (exhibitorentity.UploadLocationMap != null && !string.IsNullOrEmpty(exhibitorentity.UploadLocationMap))
                    Location = serveraddresscms + "Content/Uploads/ExhibitorRegistration/LocationMap/" + exhibitorentity.UploadLocationMap;

                if (exhibitorentity.FileNumber != null && !string.IsNullOrEmpty(exhibitorentity.FileNumber))
                    FileNumber = exhibitorentity.FileNumber;

                //if (exhibitorentity.ExhibitionId != null)
                //    ExID = exhibitorentity.ExhibitionId.Value.ToString();

                if (exhibitorentity.Email != null && !string.IsNullOrEmpty(exhibitorentity.Email))
                    CustomerEmail = exhibitorentity.Email;

                if (exhibitorentity.Phone != null && !string.IsNullOrEmpty(exhibitorentity.Phone))
                    PhoneNo = exhibitorentity.Phone.Replace('$', '-');

                if (exhibitorentity.Trn != null && !string.IsNullOrEmpty(exhibitorentity.Trn))
                    TRN = exhibitorentity.Trn;

                if (exhibitorentity.Notes != null && !string.IsNullOrEmpty(exhibitorentity.Notes))
                    Notes = exhibitorentity.Notes;
                #endregion

                var tokenwithencryption = gIGEncryptionMobile.encrypt(tokenbeforeencryption, "BE1A8B181E6ABDE36EBD0276C18AB995", "customersregestration");
                strmodel = "Update|" + CustomerId + "|" + CustomerName_Ar + "|" + CustomerName_En + "|" + CustomerTypeID + "|" + Country + "|" + Location + "|" + FileNumber + "|" + ExID + "|" + CustomerEmail + "|" + PhoneNo + "|" + TRN + "|" + Notes;

                var strmodelencrypt = gIGEncryption.Encrypt(strmodel, "BE1A8B181E6ABDE36EBD0276C18AB995");

                string url = RevenueSystemBillingAPI + "api/CustomersRegestration/" + tokenwithencryption + "?model=" + strmodelencrypt;

                WebClient client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                Stream data = client.OpenRead(url);
                StreamReader reader = new StreamReader(data);
                var responseFromServer = reader.ReadToEnd();
                dynamic myObject = JsonConvert.DeserializeObject<dynamic>(responseFromServer);
                var strdecrypt = gIGEncryption.Decrypt(myObject, "BE1A8B181E6ABDE36EBD0276C18AB995");
                data.Close();
                reader.Close();
                // string[] arr = strdecrypt.Split('|');
                var arr = strdecrypt.Split('|');

                if (CustomerTypeID == "1")
                    GalaxyLogInfo("exhibitorupdate", CustomerId + "_" + FileNumber, arr[1], strmodel, url, uploadcmsphysicalpath);
                else if (CustomerTypeID == "8")
                    GalaxyLogInfo("restuarantupdate", CustomerId + "_" + FileNumber, arr[1], strmodel, url, uploadcmsphysicalpath);
                else
                    GalaxyLogInfo("agencyupdate", CustomerId + "_" + FileNumber, arr[1], strmodel, url, uploadcmsphysicalpath);

                if (arr[1] == "000" || arr[1] == "101")
                {

                }
            }
            catch (Exception ex)
            {
                //LogException(ex);
            }
            */
        }
        public static void PushBankTransferDataToGalaxyService(XsiInvoice invoiceentity, XsiExhibitionMemberApplicationYearly exhibitorentity, string uploadscmspath, long adminuserid, string uploadcmsphysicalpath)
        {
            try
            {
                using (InvoiceStatusLogService InvoiceStatusLogService = new InvoiceStatusLogService())
                {
                    GIGEncryption gIGEncryption = new GIGEncryption();
                    GIGEncryptionMobile gIGEncryptionMobile = new GIGEncryptionMobile();
                    //string strtimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");

                    DateTime strtimestamp = DateTime.Now;
                    string pwd = "G@123456";
                    string tokenbeforeencryption = "Myahia&" + pwd + "&" + strtimestamp;
                    var tokenwithencryption = gIGEncryptionMobile.encrypt(tokenbeforeencryption, "BE1A8B181E6ABDE36EBD0276C18AB995", "postbanktransferer");  //PostBankTransferer
                                                                                                                                                             //string strmodel = "PostBankTransfer|" + strtimestamp + "|119602" + "|9999991"+ "|SIBF/21ACW/99999/J1"+"|100"+"|1"+"|abc bank"+ "|01-01-2021"+ "|RCPT12345";
                                                                                                                                                             //PostBankTransferer|01-01-2021|123|1|123|1256|1|testBank|01-01-2021 | AE00001232 | testFile
                                                                                                                                                             //SIBF/21ACW/999999/E1

                    #region Fields
                    string strreceiptnumber = "-";

                    string strbankname = "-";
                    string strinvoicenumber = "-";
                    string strtransferdate = "-";
                    string strpaidamount = "0";
                    string strpaymenttype = "-";
                    string struploadpaymentreceipt = "-";
                    //3CDBBC4DE6AB3ACCD53BDA60E8E055D2

                    dynamic myObject1 = JsonConvert.DeserializeObject<dynamic>("\"3CDBBC4DE6AB3ACCD53BDA60E8E055D2\"");
                    var strdecrypt1 = gIGEncryption.Decrypt(myObject1, "BE1A8B181E6ABDE36EBD0276C18AB995");

                    if (invoiceentity != null)
                    {
                        if (invoiceentity.PaidAmount != null && !string.IsNullOrEmpty(invoiceentity.PaidAmount))
                            strpaidamount = invoiceentity.PaidAmount;

                        if (invoiceentity.InvoiceNumber != null && !string.IsNullOrEmpty(invoiceentity.InvoiceNumber))
                            strinvoicenumber = invoiceentity.InvoiceNumberFromUser; //invoiceentity.InvoiceNumber;

                        if (invoiceentity.PaymentTypeId != null)
                            strpaymenttype = invoiceentity.PaymentTypeId.ToString();

                        if (invoiceentity.BankName != null && !string.IsNullOrEmpty(invoiceentity.BankName))
                            strbankname = invoiceentity.BankName;

                        if (invoiceentity.TransferDate != null)
                            strtransferdate = invoiceentity.TransferDate.Value.ToString("dd-MM-yyyy");

                        if (invoiceentity.ReceiptNumber != null && !string.IsNullOrEmpty(invoiceentity.ReceiptNumber))
                            strreceiptnumber = invoiceentity.ReceiptNumber;

                        if (invoiceentity.UploadePaymentReceipt != null && !string.IsNullOrEmpty(invoiceentity.UploadePaymentReceipt))
                            struploadpaymentreceipt = uploadscmspath + "/ExhibitorRegistration/PaymentReceipt/" + invoiceentity.UploadePaymentReceipt;
                    }
                    #endregion
                    // string strmodel = "PostBankTransfer|" + strtimestamp + "|120851" + "|99999" + "|SIBF21ACW99999E1" + "|100" + "|1" + "|xyz bank" + "|01-01-2021" + "|RCPT12345" + "|abcRCPT12345.jpg";
                    //PostBankTransfer|9/8/2022 12:08:46 AM|0|9999997|-|0|1|xyz bank|08/09/2022|-|1_84fba5a68355440e97d15ec3bf0865.pdf
                    string strmodel = "PostBankTransfer|" + strtimestamp + "|" + invoiceentity.ItemId + "|" + exhibitorentity.FileNumber + "|" + strinvoicenumber + "|" + strpaidamount + "|" + strpaymenttype + "|" + strbankname + "|" + strtransferdate + "|" + strreceiptnumber + "|" + struploadpaymentreceipt;

                    var strmodelencrypt = gIGEncryption.Encrypt(strmodel, "BE1A8B181E6ABDE36EBD0276C18AB995");

                    string url = RevenueSystemBillingAPI + "api/PostBankTransfer/" + tokenwithencryption + "?model=" + strmodelencrypt;

                    WebClient client = new WebClient();
                    client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                    Stream data = client.OpenRead(url);
                    StreamReader reader = new StreamReader(data);
                    var responseFromServer = reader.ReadToEnd();
                    dynamic myObject = JsonConvert.DeserializeObject<dynamic>(responseFromServer);
                    var strdecrypt = gIGEncryption.Decrypt(myObject, "BE1A8B181E6ABDE36EBD0276C18AB995");
                    data.Close();
                    reader.Close();
                    var arr = strdecrypt.Split('|');
                    if (arr[1] == "000" || arr[1] == "101")
                    {
                        XsiInvoiceStatusLog log = new XsiInvoiceStatusLog();

                        log.InvoiceId = invoiceentity.ItemId;
                        log.ExhibitorId = invoiceentity.ExhibitorId;
                        log.AdminId = adminuserid;
                        log.GalaxyTeamId = Convert.ToInt64(arr[0]);
                        log.Status = invoiceentity.Status;
                        log.CreatedOn = MethodFactory.ArabianTimeNow();
                        InvoiceStatusLogService.InsertInvoiceStatusLog(log);
                    }
                    GalaxyLogInfo("exhibitorbanktransfer", invoiceentity.ExhibitorId + "_" + exhibitorentity.FileNumber, arr[1], strmodel, url, uploadcmsphysicalpath);

                    //return strdecrypt;
                }
            }
            catch (Exception ex)
            {
                // LogException(ex);
            }
        }
        public static void GalaxyLogInfo(string name, string id, string strcode, string strmodel, string url, string uploadscmspath)
        {
            var dtnow = MethodFactory.ArabianTimeNow().ToString("yyyyMMddHHmmss");

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(uploadscmspath + dtnow + name + id + "galaxyinfolog.txt", true))
            {
                file.WriteLine("------------------------------------");
                file.WriteLine(MethodFactory.ArabianTimeNow());
                file.WriteLine(name + " " + id + " " + strcode);
                file.WriteLine(strmodel);
                file.WriteLine(url);
                file.WriteLine("------------------------------------");
            }
        }
        public static string GenerateTokenCodeForRevenueSystem(int length = 13)
        {
            // Create a string of characters, numbers, special characters that allowed in the password
            string validChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-"; //!@#$%^&*?_
            Random random = new Random();

            // Select one random character at a time from the string
            // and create an array of chars
            char[] chars = new char[length];
            for (int i = 0; i < length; i++)
            {
                chars[i] = validChars[random.Next(0, validChars.Length)];
            }
            return new string(chars);
        }

        #endregion
    }
}

﻿using Entities.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using Xsi.ServicesLayer;
using drawingFont = System.Drawing.Font;
namespace SIBFAPIEn.Utility
{
    public class PDForImageBookMark
    {
        //readonly string PassportPath = "StaffGuest\\Passport\\";
        //readonly string PassportPathTemp = "StaffGuest\\Passport\\temp\\";

        public PDForImageBookMark()
        {

        }

        public string WriteTextOnImage(string fileBase64, string fileName, string name, string filenumber, long exhibitorId, string ext, string PassportPath, string PassportPathTemp, AppCustomSettings _appCustomSettings)
        {
            fileName = SaveImage(fileBase64, fileName, name, PassportPathTemp, ext, _appCustomSettings);
            Bitmap b = new Bitmap((Bitmap)System.Drawing.Image.FromFile(_appCustomSettings.UploadsPhysicalPath + PassportPathTemp + fileName));
            Graphics g = Graphics.FromImage(b);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;

            name = name.Trim();
            //string FileNumber = string.Empty;
            //using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            //{
            //    var entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
            //    if (entity != default(XsiExhibitionMemberApplicationYearly))
            //    {
            //        if (entity.FileNumber != null)
            //            if (!string.IsNullOrEmpty(entity.FileNumber))
            //                FileNumber = entity.FileNumber;
            //    }
            //}
            drawingFont f = new drawingFont("calibri", 14, FontStyle.Bold);
            g.DrawString("File Number: " + filenumber, f, SystemBrushes.WindowText, new Point(20, b.Height - 30));
            g.DrawString("Name: " + name, f, SystemBrushes.WindowText, new Point(20, b.Height - 50));
            ext = "." + ext.ToLower();
            if (ext == ".png")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Png);
            else if (ext == ".jpg" || ext == ".jpeg")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Jpeg);
            else if (ext == ".gif")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Gif);
            else if (ext == ".bmp")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Bmp);
            else if (ext == ".tiff" || ext == ".tif")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Tiff);
            b.Dispose();
            return fileName;
        }
        public string WriteTextOnPDF(string fileBase64, string fileName, string name, string filenumber, long memberid, string ext, string PassportPath, string PassportPathTemp, AppCustomSettings _appCustomSettings)
        {
            PdfContentByte cb;
            fileName = SaveImage(fileBase64, fileName, name, PassportPathTemp, ext, _appCustomSettings);
            //PPUpload.SaveAs(string.Format("{0}{1}", Server.MapPath(PassportPathTemp), fileName));
            PdfReader reader = new PdfReader(ReadImage(fileBase64, fileName, name, PassportPathTemp, _appCustomSettings));
            using (var fileStream = new FileStream(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, FileMode.Create, FileAccess.Write))
            {
                var document = new Document(reader.GetPageSizeWithRotation(1));
                var writer = PdfWriter.GetInstance(document, fileStream);
                document.Open();
                for (var i = 1; i <= reader.NumberOfPages; i++)
                {
                    document.NewPage();
                    var baseFont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    var importedPage = writer.GetImportedPage(reader, i);
                    cb = writer.DirectContent;
                    cb.AddTemplate(importedPage, 0, 0);
                    string StaffGuestName = string.Empty;
                    if (name != null)
                        StaffGuestName = name.Trim();

                    //string FileNumber = string.Empty;
                    //using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
                    //{
                    //    XsiExhibitionMember entity = ExhibitionMemberService.GetExhibitionMemberByItemId(memberid);
                    //    if (entity != default(XsiExhibitionMember))
                    //    {
                    //        if (entity.FileNumber != null)
                    //            if (!string.IsNullOrEmpty(entity.FileNumber))
                    //                FileNumber = entity.FileNumber;
                    //    }
                    //}
                    string text = "Name: " + StaffGuestName;
                    {
                        cb.BeginText();
                        cb.SetFontAndSize(baseFont, 12);
                        cb.SetTextMatrix(document.PageSize.GetLeft(26), document.PageSize.GetBottom(20));
                        cb.ShowText(text);
                        cb.EndText();
                    }
                    text = "File Number: " + filenumber;
                    {
                        cb.BeginText();
                        cb.SetFontAndSize(baseFont, 12);
                        cb.SetTextMatrix(document.PageSize.GetLeft(26), document.PageSize.GetBottom(30));
                        cb.ShowText(text);
                        cb.EndText();
                    }
                }
                document.Close();
                writer.Close();
            }
            return fileName;
        }

        byte[] ReadImage(string fileBase64, string fileName, string name, string path, AppCustomSettings _appCustomSettings)
        {
            byte[] responseBytes = null;
            if (fileBase64 != null && fileBase64.Length > 0)
            {
                //string fileName = string.Empty;
                //fileName = string.Format("{0}_{1}_{2}{3}", name, 1, MethodFactory.GetRandomNumber(),
                //    "." + fileExtension);
                responseBytes = System.IO.File.ReadAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                 path) + fileName);
            }
            return responseBytes;
        }
        string SaveImage(string fileBase64, string fileName, string name, string path, string fileExtension, AppCustomSettings _appCustomSettings)
        {
            if (fileBase64 != null && fileBase64.Length > 0)
            {
                //string fileName = string.Empty;
                byte[] imageBytes;
                if (fileBase64.Contains("data:"))
                {
                    var strInfo = fileBase64.Split(",")[0];
                    imageBytes = Convert.FromBase64String(fileBase64.Split(',')[1]);
                }
                else
                {
                    imageBytes = Convert.FromBase64String(fileBase64);
                }

                fileName = string.Format("{0}_{1}_{2}{3}", name, 1, MethodFactory.GetRandomNumber(),
                    "." + fileExtension);
                System.IO.File.WriteAllBytes(
                    Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                 path) + fileName, imageBytes);

                return fileName;
            }
            return string.Empty;
        }
    }
}

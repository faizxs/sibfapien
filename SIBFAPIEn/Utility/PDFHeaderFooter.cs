﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.Utility
{
    public class HeaderFooter : PdfPageEventHelper
    {
        PdfContentByte cb;
        PdfTemplate template;
        public long XsiCurrentLanguage = 1;

        // public string PhysicalApplicationPath = "D:\\xsstudios\\sibf\\sibfnewcms\\Xsi.WebUI\\"; //localhost
        public string PhysicalApplicationPath = "D:\\vhosts\\xsi.sibf.com\\"; // sibf server -  D:\vhosts\xsi.sibf.com\Content\Assets\images
        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                cb = writer.DirectContent;
                template = cb.CreateTemplate(100, 100);
            }
            catch (DocumentException de)
            {
                //handle exception here
            }
            catch (System.IO.IOException ioe)
            {
                //handle exception here
            }
        }
        public override void OnEndPage(PdfWriter writer, Document document)
        {

            string url = string.Empty, email = string.Empty;
            int rColor = -1, gColor = -1, bColor = -1;
            string path = PhysicalApplicationPath;//.Server.MapPath(".");
            string imagePathHeader = string.Empty;
            imagePathHeader = "content\\assets\\images\\invoice-header.jpg";
            rColor = 241;
            gColor = 88;
            bColor = 34;
            url = "sibf.com";
            email = "info@sibf.com";

            iTextSharp.text.Image top = iTextSharp.text.Image.GetInstance(path + imagePathHeader);
            // iTextSharp.text.Image bottom = iTextSharp.text.Image.GetInstance(path + "content\\assets\\images\\invoice-footernew.jpg");
            //top.SetAbsolutePosition(0, 680);
            top.ScaleAbsolute(596f, 108f);
            top.SetAbsolutePosition(0, 734);
            //    bottom.ScaleAbsolute(153f, 109f);
            //   bottom.SetAbsolutePosition(430, 0);
            document.Add(top);
            //  document.Add(bottom);
            #region Font declaration
            BaseFont bfTheSerif = BaseFont.CreateFont(PhysicalApplicationPath + "Xsi\\Content\\fonts\\theserif_light_plain-webfont.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont bfTheSerifBold = BaseFont.CreateFont(PhysicalApplicationPath + "Xsi\\Content\\fonts\\theserif_BOLD_plain-webfont.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            BaseFont arialuni = BaseFont.CreateFont(PhysicalApplicationPath + "Xsi\\Content\\fonts\\tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font fontBoldInvoiceAr = new Font(arialuni, 23, Font.BOLD, new BaseColor(100, 100, 100));
            Font fontBoldInvoiceEn = new Font(bfTheSerifBold, 26, Font.NORMAL, new BaseColor(100, 100, 100));
            #endregion
            //cb = writer.DirectContent;
            //template = cb.CreateTemplate(50, 50);
            base.OnEndPage(writer, document);
            Rectangle pageSize = document.PageSize;
            #region Footer Content
            String text = "P.O. Box ";
            cb.SetRGBColorFill(rColor, gColor, bColor);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerifBold, 10);
            cb.SetTextMatrix(pageSize.GetLeft(40), pageSize.GetBottom(30));
            cb.ShowText(text);
            cb.EndText();

            text = "73111, Sharjah";
            cb.SetRGBColorFill(100, 100, 100);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerif, 10);
            cb.SetTextMatrix(pageSize.GetLeft(85), pageSize.GetBottom(30));
            cb.ShowText(text);
            cb.EndText();

            text = "T ";
            cb.SetRGBColorFill(rColor, gColor, bColor);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerifBold, 10);
            cb.SetTextMatrix(pageSize.GetLeft(158), pageSize.GetBottom(30));
            cb.ShowText(text);
            cb.EndText();

            text = "+97165123157";//"+97165123333";
            cb.SetRGBColorFill(100, 100, 100);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerif, 10);
            cb.SetTextMatrix(pageSize.GetLeft(168), pageSize.GetBottom(30));
            cb.ShowText(text);
            cb.EndText();

            text = "E ";
            cb.SetRGBColorFill(rColor, gColor, bColor);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerifBold, 10);
            cb.SetTextMatrix(pageSize.GetLeft(240), pageSize.GetBottom(30));
            cb.ShowText(text);
            cb.EndText();

            text = email;
            cb.SetRGBColorFill(100, 100, 100);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerif, 10);
            cb.SetTextMatrix(pageSize.GetLeft(251), pageSize.GetBottom(30));
            cb.ShowText(text);
            cb.EndText();

            text = "Arabic ";
            cb.SetRGBColorFill(rColor, gColor, bColor);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerifBold, 10);
            cb.SetTextMatrix(pageSize.GetLeft(330), pageSize.GetBottom(30));
            cb.ShowText(text);
            cb.EndText();

            text = "+97165123225";
            cb.SetRGBColorFill(100, 100, 100);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerif, 10);
            cb.SetTextMatrix(pageSize.GetLeft(365), pageSize.GetBottom(30));
            cb.ShowText(text);
            cb.EndText();

            text = "United Arab Emirates";
            cb.SetRGBColorFill(100, 100, 100);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerif, 10);
            cb.SetTextMatrix(pageSize.GetLeft(40), pageSize.GetBottom(15));
            cb.ShowText(text);
            cb.EndText();

            text = "F";
            cb.SetRGBColorFill(rColor, gColor, bColor);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerifBold, 10);
            cb.SetTextMatrix(pageSize.GetLeft(158), pageSize.GetBottom(15));
            cb.ShowText(text);
            cb.EndText();

            text = "+97165123337";
            cb.SetRGBColorFill(100, 100, 100);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerif, 10);
            cb.SetTextMatrix(pageSize.GetLeft(168), pageSize.GetBottom(15));
            cb.ShowText(text);
            cb.EndText();

            text = "W";
            cb.SetRGBColorFill(rColor, gColor, bColor);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerifBold, 10);
            cb.SetTextMatrix(pageSize.GetLeft(240), pageSize.GetBottom(15));
            cb.ShowText(text);
            cb.EndText();

            text = url;
            cb.SetRGBColorFill(100, 100, 100);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerif, 10);
            cb.SetTextMatrix(pageSize.GetLeft(253), pageSize.GetBottom(15));
            cb.ShowText(text);
            cb.EndText();


            text = "International ";
            cb.SetRGBColorFill(rColor, gColor, bColor);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerifBold, 10);
            cb.SetTextMatrix(pageSize.GetLeft(330), pageSize.GetBottom(15));
            cb.ShowText(text);
            cb.EndText();

            text = "+97165123219";
            cb.SetRGBColorFill(100, 100, 100);
            cb.BeginText();
            cb.SetFontAndSize(bfTheSerif, 10);
            cb.SetTextMatrix(pageSize.GetLeft(401), pageSize.GetBottom(15));
            cb.ShowText(text);
            cb.EndText();


            text = writer.PageNumber + " / ";
            //Add paging to header
            {
                cb.BeginText();
                cb.SetFontAndSize(bfTheSerifBold, 12);
                cb.SetTextMatrix(document.PageSize.GetLeft(26), document.PageSize.GetTop(37));
                cb.ShowText(text);
                cb.EndText();
                float len = bfTheSerifBold.GetWidthPoint(text, 12);
                //Adds "12" in Page 1 of 12
                cb.AddTemplate(template, document.PageSize.GetLeft(26) + len, document.PageSize.GetTop(37));
            }


            //cb.EndText();
            #endregion
        }
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
            #region Font declaration
            BaseFont bfTheSerifBold = BaseFont.CreateFont(PhysicalApplicationPath + "Xsi\\Content\\fonts\\theserif_BOLD_plain-webfont.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            #endregion

            template.BeginText();
            template.SetFontAndSize(bfTheSerifBold, 12);
            template.SetTextMatrix(0, 0);
            template.ShowText((writer.PageNumber - 1).ToString());
            template.EndText();
        }
    }
}

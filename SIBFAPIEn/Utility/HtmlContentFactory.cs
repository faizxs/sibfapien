﻿using Contracts;
using Entities.Models;
using SIBFAPIEn.DTO;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace SIBFAPIEn
{
    public class HtmlContentFactory
    {
        private ILoggerManager _logger;
        public HtmlContentFactory(ILoggerManager logger)
        {
            _logger = logger;
        }
        public StringBuilder BindExhibitionOtherEventsEmailContent(long languageId, string bodyContent, XsiExhibitionMember exhibitionmember, long exhibitionid, string ServerAddress, AppCustomSettings _appCustomSettings)
        {
            string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
            string EmailContent = string.Empty;
            if (languageId == 1)
            {
                EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\OtherExhibitionSGEmailContent.html";
            }
            else
            {
                EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\OtherExhibitionSGEmailContentArabic.html";
            }
            StringBuilder body = new StringBuilder();

            if (File.Exists(EmailContent))
            {
                body.Append(File.ReadAllText(EmailContent));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);
            string str = GetExhibitionOtherEventsDetails(exhibitionid, languageId);
            string[] strArray = str.Split(',');

            body.Replace("$$exhibitionno$$", strArray[0]);
            body.Replace("$$exhibitiondate$$", strArray[1]);
            body.Replace("$$exhyear$$", strArray[2]);

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            body.Replace(swidth, sreplacewidth);

            body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
            body.Replace("$$ServerAddress$$", ServerAddress);

            if (exhibitionmember != null)
            {
                string exhibitionmembername = string.Empty;
                if (languageId == 1)
                {
                    if (!string.IsNullOrEmpty(exhibitionmember.Firstname) && !string.IsNullOrEmpty(exhibitionmember.LastName))
                        exhibitionmembername = exhibitionmember.Firstname + " " + exhibitionmember.LastName;
                    else if (!string.IsNullOrEmpty(exhibitionmember.Firstname))
                        exhibitionmembername = exhibitionmember.Firstname;
                    else if (!string.IsNullOrEmpty(exhibitionmember.LastName))
                        exhibitionmembername = exhibitionmember.LastName;
                }
                else
                {
                    if (!string.IsNullOrEmpty(exhibitionmember.FirstNameAr) && !string.IsNullOrEmpty(exhibitionmember.LastNameAr))
                        exhibitionmembername = exhibitionmember.FirstNameAr + " " + exhibitionmember.LastNameAr;
                    else if (!string.IsNullOrEmpty(exhibitionmember.FirstNameAr))
                        exhibitionmembername = exhibitionmember.FirstNameAr;
                    else if (!string.IsNullOrEmpty(exhibitionmember.LastNameAr))
                        exhibitionmembername = exhibitionmember.LastNameAr;

                    if (string.IsNullOrEmpty(exhibitionmembername))
                    {
                        if (!string.IsNullOrEmpty(exhibitionmember.Firstname) && !string.IsNullOrEmpty(exhibitionmember.LastName))
                            exhibitionmembername = exhibitionmember.Firstname + " " + exhibitionmember.LastName;
                        else if (!string.IsNullOrEmpty(exhibitionmember.Firstname))
                            exhibitionmembername = exhibitionmember.Firstname;
                        else if (!string.IsNullOrEmpty(exhibitionmember.LastName))
                            exhibitionmembername = exhibitionmember.LastName;
                    }
                }
                body.Replace("$$Name$$", exhibitionmembername);
            }
            return body;
        }

        public string GetExhibitionOtherEventsDetails(long exhibitionid, long langid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long? exhibitionNumber = -1;
                string strdate = string.Empty;

                XsiExhibitionOtherEvents exhibitionentity = _context.XsiExhibitionOtherEvents.Where(i => i.ExhibitionId == exhibitionid && i.IsActive == EnumConversion.ToString(EnumBool.Yes)).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                string startdaysuffix = string.Empty;
                string enddaysuffix = string.Empty;
                string stryear = MethodFactory.ArabianTimeNow().Year.ToString();
                if (exhibitionentity != default(XsiExhibitionOtherEvents))
                {
                    if (exhibitionentity.ExhibitionNumber != null)
                        exhibitionNumber = exhibitionentity.ExhibitionNumber;
                    long dayno = exhibitionentity.StartDate.Value.Date.Day;
                    if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                        startdaysuffix = "th";
                    else
                        startdaysuffix = GetSuffix(exhibitionentity.StartDate.Value.Date.Day % 10, exhibitionentity.StartDate.Value);
                    dayno = exhibitionentity.EndDate.Value.Date.Day;
                    if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                        enddaysuffix = "th";
                    else
                        enddaysuffix = GetSuffix(exhibitionentity.EndDate.Value.Date.Day % 10, exhibitionentity.EndDate.Value);

                    //if (exhibitionentity.StartDate.Value.Year == exhibitionentity.EndDate.Value.Year)
                    //{
                    //    if (exhibitionentity.StartDate.Value.Month == exhibitionentity.EndDate.Value.Month)
                    //        strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " - " + exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " + exhibitionentity.StartDate.Value.ToString("MMM") + " " + exhibitionentity.StartDate.Value.ToString("yyyy");
                    //    else
                    //        strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " " + exhibitionentity.StartDate.Value.ToString("MMM")
                    //             + " - " + exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " + exhibitionentity.EndDate.Value.ToString("MMM")
                    //            + " " + exhibitionentity.StartDate.Value.ToString("yyyy");
                    //}
                    //else
                    //{
                    //    strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " " + exhibitionentity.StartDate.Value.ToString("MMM") + " " + exhibitionentity.StartDate.Value.ToString("yyyy")
                    //         + " - " + exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " + exhibitionentity.EndDate.Value.ToString("MMM")
                    //        + " " + exhibitionentity.EndDate.Value.ToString("yyyy");
                    //}

                    if (langid == 1)
                    {
                        #region English
                        if (exhibitionentity.StartDate.Value.Year == exhibitionentity.EndDate.Value.Year)
                        {
                            if (exhibitionentity.StartDate.Value.Month == exhibitionentity.EndDate.Value.Month)
                            {
                                strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " - " +
                                          exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " +
                                          exhibitionentity.StartDate.Value.ToString("MMM") + " " +
                                          exhibitionentity.StartDate.Value.ToString("yyyy");
                            }
                            else
                            {
                                strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " " +
                                         exhibitionentity.StartDate.Value.ToString("MMM")
                                         + " - " + exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " +
                                         exhibitionentity.EndDate.Value.ToString("MMM")
                                         + " " + exhibitionentity.StartDate.Value.ToString("yyyy");
                            }
                        }
                        else
                        {
                            strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " " +
                                      exhibitionentity.StartDate.Value.ToString("MMM") + " " +
                                      exhibitionentity.StartDate.Value.ToString("yyyy")
                                      + " - " + exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " +
                                      exhibitionentity.EndDate.Value.ToString("MMM")
                                      + " " + exhibitionentity.EndDate.Value.ToString("yyyy");
                        }

                        #endregion
                    }
                    else
                    {
                        #region Arabic
                        if (exhibitionentity.StartDate.Value.Year == exhibitionentity.EndDate.Value.Year)
                        {
                            if (exhibitionentity.StartDate.Value.Month == exhibitionentity.EndDate.Value.Month)
                            {
                                strdate = exhibitionentity.StartDate.Value.Date.Day + " - " +
                                         exhibitionentity.EndDate.Value.Date.Day + " " +
                                         exhibitionentity.StartDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")) + " " +
                                         exhibitionentity.StartDate.Value.ToString("yyyy");
                            }
                            else
                            {
                                strdate = exhibitionentity.StartDate.Value.Date.Day + " " +
                                        exhibitionentity.StartDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"))
                                        + " - " + exhibitionentity.EndDate.Value.Date.Day + " " +
                                        exhibitionentity.EndDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"))
                                        + " " + exhibitionentity.StartDate.Value.ToString("yyyy");
                            }
                        }
                        else
                        {
                            strdate = exhibitionentity.StartDate.Value.Date.Day + " " +
                                      exhibitionentity.StartDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")) + " " +
                                      exhibitionentity.StartDate.Value.ToString("yyyy")
                                      + " - " + exhibitionentity.EndDate.Value.Date.Day + " " +
                                      exhibitionentity.EndDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"))
                                      + " " + exhibitionentity.EndDate.Value.ToString("yyyy");
                        }
                        #endregion
                    }

                    if (exhibitionentity.ExhibitionYear != null)
                        stryear = exhibitionentity.ExhibitionYear.ToString();
                }
                return exhibitionNumber.ToString() + "," + strdate + "," + stryear;
            }
        }
        public StringBuilder BindEmailContent(long languageId, string bodyContent, XsiExhibitionMember exhibitionmember, string ServerAddress, long websiteId, AppCustomSettings _appCustomSettings)
        {
            string scrfURL = _appCustomSettings.ServerAddressSCRF;
            string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
            string EmailContent = string.Empty;
            if (languageId == 1)
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRF.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
            }
            else
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRF.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
            }
            StringBuilder body = new StringBuilder();

            if (File.Exists(EmailContent))
            {
                body.Append(File.ReadAllText(EmailContent));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);
            string str = GetExhibitionDetails(websiteId, languageId);
            string[] strArray = str.Split(',');

            body.Replace("$$exhibitionno$$", strArray[0]);
            body.Replace("$$exhibitiondate$$", strArray[1]);
            body.Replace("$$exhyear$$", strArray[2]);

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            body.Replace(swidth, sreplacewidth);

            body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
            body.Replace("$$ServerAddress$$", ServerAddress);
            if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                body.Replace("$$ServerAddressSCRF$$", scrfURL);

            if (exhibitionmember != null)
            {
                string exhibitionmembername = string.Empty;
                if (languageId == 1)
                {
                    if (!string.IsNullOrEmpty(exhibitionmember.Firstname) && !string.IsNullOrEmpty(exhibitionmember.LastName))
                        exhibitionmembername = exhibitionmember.Firstname + " " + exhibitionmember.LastName;
                    else if (!string.IsNullOrEmpty(exhibitionmember.Firstname))
                        exhibitionmembername = exhibitionmember.Firstname;
                    else if (!string.IsNullOrEmpty(exhibitionmember.LastName))
                        exhibitionmembername = exhibitionmember.LastName;
                }
                else
                {
                    if (!string.IsNullOrEmpty(exhibitionmember.FirstNameAr) && !string.IsNullOrEmpty(exhibitionmember.LastNameAr))
                        exhibitionmembername = exhibitionmember.FirstNameAr + " " + exhibitionmember.LastNameAr;
                    else if (!string.IsNullOrEmpty(exhibitionmember.FirstNameAr))
                        exhibitionmembername = exhibitionmember.FirstNameAr;
                    else if (!string.IsNullOrEmpty(exhibitionmember.LastNameAr))
                        exhibitionmembername = exhibitionmember.LastNameAr;

                    if (string.IsNullOrEmpty(exhibitionmembername))
                    {
                        if (!string.IsNullOrEmpty(exhibitionmember.Firstname) && !string.IsNullOrEmpty(exhibitionmember.LastName))
                            exhibitionmembername = exhibitionmember.Firstname + " " + exhibitionmember.LastName;
                        else if (!string.IsNullOrEmpty(exhibitionmember.Firstname))
                            exhibitionmembername = exhibitionmember.Firstname;
                        else if (!string.IsNullOrEmpty(exhibitionmember.LastName))
                            exhibitionmembername = exhibitionmember.LastName;
                    }
                }
                body.Replace("$$Name$$", exhibitionmembername);
            }
            return body;
        }

        public StringBuilder BindEmailContentPP(long languageId, string bodyContent, XsiExhibitionProfessionalProgramRegistration entity, string ServerAddress, long websiteId, AppCustomSettings _appCustomSettings)
        {
            //languageId = EnglishId;
            string scrfURL = _appCustomSettings.ServerAddressSCRF;
            string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
            string EmailContent = string.Empty;
            if (languageId == 1)
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRF.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
            }
            else
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRF.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
            }
            StringBuilder body = new StringBuilder();

            if (File.Exists(EmailContent))
            {
                body.Append(File.ReadAllText(EmailContent));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);

            body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
            body.Replace("$$ServerAddress$$", ServerAddress);
            if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                body.Replace("$$ServerAddressSCRF$$", scrfURL);
            string str = "" + "," + "";
            if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                str = GetExhibitionDetails(websiteId, languageId);
            else
                str = GetExhibitionDetails(Convert.ToInt64(EnumWebsiteId.SIBF), languageId);
            string[] strArray = str.Split(',');
            body.Replace("$$exhibitionno$$", strArray[0]);
            body.Replace("$$exhibitiondate$$", strArray[1]);
            body.Replace("$$exhyear$$", strArray[2]);

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            body.Replace(swidth, sreplacewidth);

            if (entity != null)
            {
                if (languageId == 1)
                {
                    if (entity.FirstName != null && entity.LastName != null)
                        body.Replace("$$Name$$", entity.FirstName + " " + entity.LastName);
                    else if (entity.FirstName != null)
                        body.Replace("$$Name$$", entity.FirstName);
                    else if (entity.LastName != null)
                        body.Replace("$$Name$$", entity.LastName);
                }
                else if (languageId == 2)
                {

                    if (entity.FirstName != null && entity.LastName != null)
                        body.Replace("$$Name$$", entity.FirstName + " " + entity.LastName);
                    else if (entity.FirstName != null)
                        body.Replace("$$Name$$", entity.FirstName);
                    else if (entity.LastName != null)
                        body.Replace("$$Name$$", entity.LastName);

                    //if (entity.FirstNameAr != null && entity.LastNameAr != null)
                    //    body.Replace("$$Name$$", entity.FirstNameAr + " " + entity.LastNameAr);
                    //else if (entity.FirstNameAr != null)
                    //    body.Replace("$$Name$$", entity.FirstNameAr);
                    //else if (entity.LastNameAr != null)
                    //    body.Replace("$$Name$$", entity.LastNameAr);
                }
            }
            return body;
        }

        public StringBuilder BindEmailContentPCAward(long languageId, string bodyContent, XsiPcawardNominationForms entity, string ServerAddress, long websiteId, AppCustomSettings _appCustomSettings)
        {
            string scrfURL = _appCustomSettings.ServerAddressSCRF;
            string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
            string EmailContent = string.Empty;
            if (languageId == 1)
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRFAward.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentAward.html";
            }
            else
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRFAward.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicAward.html";
            }
            StringBuilder body = new StringBuilder();

            if (File.Exists(EmailContent))
            {
                body.Append(File.ReadAllText(EmailContent));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);
            string str = GetExhibitionDetails(websiteId, languageId);
            string[] strArray = str.Split(',');

            body.Replace("$$exhibitionno$$", strArray[0]);
            body.Replace("$$exhibitiondate$$", strArray[1]);
            body.Replace("$$exhyear$$", strArray[2]);

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            body.Replace(swidth, sreplacewidth);

            body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
            body.Replace("$$ServerAddress$$", ServerAddress);
            if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                body.Replace("$$ServerAddressSCRF$$", scrfURL);


            return body;
        }

        public StringBuilder BindEmailContentSIBFAwards(long languageId, string bodyContent, XsiAwardsNominationForms entity, string ServerAddress, long websiteId, AppCustomSettings _appCustomSettings)
        {
            string scrfURL = _appCustomSettings.ServerAddressSCRF;
            string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
            string EmailContent = string.Empty;
            if (languageId == 1)
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRFAward.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentAward.html";
            }
            else
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRFAward.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicAward.html";
            }
            StringBuilder body = new StringBuilder();

            if (File.Exists(EmailContent))
            {
                body.Append(File.ReadAllText(EmailContent));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);
            string str = GetExhibitionDetails(websiteId, languageId);
            string[] strArray = str.Split(',');

            body.Replace("$$exhibitionno$$", strArray[0]);
            body.Replace("$$exhibitiondate$$", strArray[1]);
            body.Replace("$$exhyear$$", strArray[2]);

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            body.Replace(swidth, sreplacewidth);

            body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
            body.Replace("$$ServerAddress$$", ServerAddress);
            if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                body.Replace("$$ServerAddressSCRF$$", scrfURL);

            return body;

        }

        public StringBuilder BindEmailContentForPublisherRecognitionAward(long languageId, string bodyContent, XsiAwardsNominationForms entity, string ServerAddress, AppCustomSettings _appCustomSettings)
        {
            long websiteId = 1;
            string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
            string EmailContent = string.Empty;
            if (languageId == 1)
            {
                EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
            }
            else
            {
                EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
            }
            StringBuilder body = new StringBuilder();

            if (File.Exists(EmailContent))
            {
                body.Append(File.ReadAllText(EmailContent));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);
            string str = GetExhibitionDetails(websiteId, languageId);
            string[] strArray = str.Split(',');

            body.Replace("$$exhibitionno$$", strArray[0]);
            body.Replace("$$exhibitiondate$$", strArray[1]);
            body.Replace("$$exhyear$$", strArray[2]);

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            body.Replace(swidth, sreplacewidth);

            body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
            body.Replace("$$ServerAddress$$", ServerAddress);

            if (entity != null)
            {
                string exhibitionmembername = string.Empty;
                if (languageId == 1)
                {
                    if (entity.OwnerOfPubHouse != null)
                        exhibitionmembername = entity.OwnerOfPubHouse;
                }
                else
                {
                    if (entity.OwnerOfPubHouse != null)
                        exhibitionmembername = entity.OwnerOfPubHouse;
                }
                body.Replace("$$Name$$", exhibitionmembername);
            }
            return body;
        }

        public StringBuilder BindEmailContentForAwardsSIBF(long languageId, string bodyContent, XsiAwardNominationRegistrations entity, string ServerAddress, AppCustomSettings _appCustomSettings)
        {
            long websiteId = 1;
            string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
            string EmailContent = string.Empty;
            if (languageId == 1)
            {
                EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
            }
            else
            {
                EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
            }
            StringBuilder body = new StringBuilder();

            if (File.Exists(EmailContent))
            {
                body.Append(File.ReadAllText(EmailContent));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);
            string str = GetExhibitionDetails(websiteId, languageId);
            string[] strArray = str.Split(',');

            body.Replace("$$exhibitionno$$", strArray[0]);
            body.Replace("$$exhibitiondate$$", strArray[1]);
            body.Replace("$$exhyear$$", strArray[2]);

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            body.Replace(swidth, sreplacewidth);

            body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
            body.Replace("$$ServerAddress$$", ServerAddress);

            if (entity != null)
            {
                string exhibitionmembername = string.Empty;
                if (languageId == 1)
                {
                    if (entity.AuthorName != null)
                        exhibitionmembername = entity.AuthorName;
                }
                else
                {
                    if (entity.AuthorName != null)
                        exhibitionmembername = entity.AuthorName;
                }
                body.Replace("$$Name$$", exhibitionmembername);
            }
            return body;
        }
        public StringBuilder BindEmailContentForAwardsSIBFTurjuman(long languageId, string bodyContent, XsiSharjahAwardForTranslation entity, string ServerAddress, AppCustomSettings _appCustomSettings)
        {
            long websiteId = 1;
            string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
            string EmailContent = string.Empty;
            if (languageId == 1)
            {
                EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
            }
            else
            {
                EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
            }
            StringBuilder body = new StringBuilder();

            if (File.Exists(EmailContent))
            {
                body.Append(File.ReadAllText(EmailContent));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);
            string str = GetExhibitionDetails(websiteId, languageId);
            string[] strArray = str.Split(',');

            body.Replace("$$exhibitionno$$", strArray[0]);
            body.Replace("$$exhibitiondate$$", strArray[1]);
            body.Replace("$$exhyear$$", strArray[2]);

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            body.Replace(swidth, sreplacewidth);

            body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
            body.Replace("$$ServerAddress$$", ServerAddress);

            if (entity != null)
            {
                string exhibitionmembername = string.Empty;
                if (languageId == 1)
                {
                    if (entity.AuthorName != null)
                        exhibitionmembername = entity.AuthorName;
                }
                else
                {
                    if (entity.AuthorName != null)
                        exhibitionmembername = entity.AuthorName;
                }
                body.Replace("$$Name$$", exhibitionmembername);
            }
            return body;
        }

        public string GetSuffix(int startday, DateTime dt)
        {
            string daysuffix;
            if (startday != 11 && dt.Date.Day != 11)
            {
                if (startday == 1)
                    daysuffix = "st";
                else if (startday == 2)
                    daysuffix = "nd";
                else if (startday == 3)
                    daysuffix = "rd";
                else
                    daysuffix = "th";
            }
            else
                daysuffix = "th";
            return daysuffix;
        }
        public string GetExhibitionDetails(long websiteId, long langid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long? exhibitionNumber = -1;
                string strdate = string.Empty;

                XsiExhibition exhibitionentity = _context.XsiExhibition.Where(i => i.WebsiteId == websiteId && i.IsActive == EnumConversion.ToString(EnumBool.Yes)).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                string startdaysuffix = string.Empty;
                string enddaysuffix = string.Empty;
                string stryear = MethodFactory.ArabianTimeNow().Year.ToString();
                if (exhibitionentity != default(XsiExhibition))
                {
                    if (exhibitionentity.ExhibitionNumber != null)
                        exhibitionNumber = exhibitionentity.ExhibitionNumber;
                    long dayno = exhibitionentity.StartDate.Value.Date.Day;
                    if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                        startdaysuffix = "th";
                    else
                        startdaysuffix = GetSuffix(exhibitionentity.StartDate.Value.Date.Day % 10, exhibitionentity.StartDate.Value);
                    dayno = exhibitionentity.EndDate.Value.Date.Day;
                    if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                        enddaysuffix = "th";
                    else
                        enddaysuffix = GetSuffix(exhibitionentity.EndDate.Value.Date.Day % 10, exhibitionentity.EndDate.Value);

                    //if (exhibitionentity.StartDate.Value.Year == exhibitionentity.EndDate.Value.Year)
                    //{
                    //    if (exhibitionentity.StartDate.Value.Month == exhibitionentity.EndDate.Value.Month)
                    //        strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " - " + exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " + exhibitionentity.StartDate.Value.ToString("MMM") + " " + exhibitionentity.StartDate.Value.ToString("yyyy");
                    //    else
                    //        strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " " + exhibitionentity.StartDate.Value.ToString("MMM")
                    //             + " - " + exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " + exhibitionentity.EndDate.Value.ToString("MMM")
                    //            + " " + exhibitionentity.StartDate.Value.ToString("yyyy");
                    //}
                    //else
                    //{
                    //    strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " " + exhibitionentity.StartDate.Value.ToString("MMM") + " " + exhibitionentity.StartDate.Value.ToString("yyyy")
                    //         + " - " + exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " + exhibitionentity.EndDate.Value.ToString("MMM")
                    //        + " " + exhibitionentity.EndDate.Value.ToString("yyyy");
                    //}

                    if (langid == 1)
                    {
                        #region English
                        if (exhibitionentity.StartDate.Value.Year == exhibitionentity.EndDate.Value.Year)
                        {
                            if (exhibitionentity.StartDate.Value.Month == exhibitionentity.EndDate.Value.Month)
                            {
                                strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " - " +
                                          exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " +
                                          exhibitionentity.StartDate.Value.ToString("MMM") + " " +
                                          exhibitionentity.StartDate.Value.ToString("yyyy");
                            }
                            else
                            {
                                strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " " +
                                         exhibitionentity.StartDate.Value.ToString("MMM")
                                         + " - " + exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " +
                                         exhibitionentity.EndDate.Value.ToString("MMM")
                                         + " " + exhibitionentity.StartDate.Value.ToString("yyyy");
                            }
                        }
                        else
                        {
                            strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " " +
                                      exhibitionentity.StartDate.Value.ToString("MMM") + " " +
                                      exhibitionentity.StartDate.Value.ToString("yyyy")
                                      + " - " + exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " +
                                      exhibitionentity.EndDate.Value.ToString("MMM")
                                      + " " + exhibitionentity.EndDate.Value.ToString("yyyy");
                        }

                        #endregion
                    }
                    else
                    {
                        #region Arabic
                        if (exhibitionentity.StartDate.Value.Year == exhibitionentity.EndDate.Value.Year)
                        {
                            if (exhibitionentity.StartDate.Value.Month == exhibitionentity.EndDate.Value.Month)
                            {
                                strdate = exhibitionentity.StartDate.Value.Date.Day + " - " +
                                         exhibitionentity.EndDate.Value.Date.Day + " " +
                                         exhibitionentity.StartDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")) + " " +
                                         exhibitionentity.StartDate.Value.ToString("yyyy");
                            }
                            else
                            {
                                strdate = exhibitionentity.StartDate.Value.Date.Day + " " +
                                        exhibitionentity.StartDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"))
                                        + " - " + exhibitionentity.EndDate.Value.Date.Day + " " +
                                        exhibitionentity.EndDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"))
                                        + " " + exhibitionentity.StartDate.Value.ToString("yyyy");
                            }
                        }
                        else
                        {
                            strdate = exhibitionentity.StartDate.Value.Date.Day + " " +
                                      exhibitionentity.StartDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")) + " " +
                                      exhibitionentity.StartDate.Value.ToString("yyyy")
                                      + " - " + exhibitionentity.EndDate.Value.Date.Day + " " +
                                      exhibitionentity.EndDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"))
                                      + " " + exhibitionentity.EndDate.Value.ToString("yyyy");
                        }
                        #endregion
                    }

                    if (exhibitionentity.ExhibitionYear != null)
                        stryear = exhibitionentity.ExhibitionYear.ToString();
                }
                return exhibitionNumber.ToString() + "," + strdate + "," + stryear;
            }
        }
    }
}

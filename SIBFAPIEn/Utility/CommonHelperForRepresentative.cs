﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using SIBFAPIEn.DTO;
using Xsi.ServicesLayer;
namespace SIBFAPIEn
{
    public class CommonHelperForRepresentative
    {
        #region Representative Validation Methods
        //public bool IsduplicateRepresentativeForAdd(SaveRepresentativeDTO representativeDTO, long langId, long exhibitionId)
        //{
        //    using (ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService())
        //    {
        //        ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
        //        ExhibitionRepresentativeParticipatingService ExhibitionRepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService();
        //        List<long> parentrepIDs = new List<long>();
        //        List<long> participatingrepIDs = new List<long>();

        //        if (langId == 1)
        //            parentrepIDs = ExhibitionRepresentativeService.GetExhibitionRepresentative(new XsiExhibitionRepresentative() { NameEn = representativeDTO.NameEn }).Select(i => i.RepresentativeId).ToList();
        //        else
        //            parentrepIDs = ExhibitionRepresentativeService.GetExhibitionRepresentative(new XsiExhibitionRepresentative() { NameEn = representativeDTO.NameEn, NameAr = representativeDTO.NameAr }).Select(i => i.RepresentativeId).ToList();

        //        if (parentrepIDs.Count == 0)
        //            return true;
        //        else if (parentrepIDs.Count() > 0)
        //        {
        //            var exhibitorIds = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly() { ExhibitionId = exhibitionId }).Select(i => i.MemberExhibitionYearlyId).ToList();
        //            var passport = ExhibitionRepresentativeParticipatingService.GetExhibitionRepresentativeParticipating().Where(i => i.Passport == representativeDTO.PassportNumber).FirstOrDefault();
        //            if (passport!=null)
        //            {
        //                return false;
        //            }
        //            else
        //            {
        //                return true;
        //            }
        //            //if (ExhibitionRepresentativeParticipatingService.GetExhibitionRepresentativeParticipating().Where(i => i.Passport == representativeDTO.PassportNumber).ToList().Count==0)
        //            //    return true;
        //        }
        //        return false;
        //    }
        //}

        public bool IsUniqueRepresentativeForAdd(SaveRepresentativeDTO representativeDTO, long langId, long exhibitionId)
        {
            using (ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService())
            {
                string strCancelled = EnumConversion.ToString(EnumRepresentativeStatus.Cancelled);
                string strRejected = EnumConversion.ToString(EnumRepresentativeStatus.Rejected);
                string strBlocked = EnumConversion.ToString(EnumRepresentativeStatus.Blocked );
                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                ExhibitionRepresentativeParticipatingService ExhibitionRepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService();
                List<long> parentrepIDs = new List<long>();
                List<long> participatingrepIDs = new List<long>();

                if (langId == 1)
                    parentrepIDs = ExhibitionRepresentativeService.GetExhibitionRepresentative(new XsiExhibitionRepresentative() { NameEn = representativeDTO.NameEn }).Select(i => i.RepresentativeId).ToList();
                else
                    parentrepIDs = ExhibitionRepresentativeService.GetExhibitionRepresentative(new XsiExhibitionRepresentative() { NameEn = representativeDTO.NameEn, NameAr = representativeDTO.NameAr }).Select(i => i.RepresentativeId).ToList();

                if (parentrepIDs.Count == 0)
                    return true;
                else if (parentrepIDs.Count() > 0)
                {
                    var exhibitorIds = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly() { ExhibitionId = exhibitionId }).Select(i => i.MemberExhibitionYearlyId).ToList();
                    if (ExhibitionRepresentativeParticipatingService.GetExhibitionRepresentativeParticipating().Where(i => (i.Status == strBlocked || i.Status == strRejected) && parentrepIDs.Contains(i.RepresentativeId) && exhibitorIds.Contains(i.MemberExhibitionYearlyId) && i.Passport == representativeDTO.PassportNumber).ToList().Count > 0)
                        return false;

                        if (ExhibitionRepresentativeParticipatingService.GetExhibitionRepresentativeParticipating().Where(i =>(i.Status!=strBlocked && i.Status!=strRejected) && parentrepIDs.Contains(i.RepresentativeId) && exhibitorIds.Contains(i.MemberExhibitionYearlyId)&& i.Passport==representativeDTO.PassportNumber).ToList().Count == 0)
                        return true;
                }
                return false;
            }
        }
        public bool IsUniqueRepresentativeForUpdate(SaveRepresentativeDTO representativeDTO, long langId, long exhibitionId)
        {
            string strCancelled = EnumConversion.ToString(EnumRepresentativeStatus.Cancelled);
            string strRejected = EnumConversion.ToString(EnumRepresentativeStatus.Rejected);
            string strBlocked = EnumConversion.ToString(EnumRepresentativeStatus.Blocked);
            using (ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService())
            {
                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                ExhibitionRepresentativeParticipatingService ExhibitionRepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService();
                List<long> parentrepIDs = new List<long>();
                if (langId == 1)
                    parentrepIDs = ExhibitionRepresentativeService.GetExhibitionRepresentative(new XsiExhibitionRepresentative() { NameEn = representativeDTO.NameEn }).Select(i => i.RepresentativeId).ToList();
                else
                    parentrepIDs = ExhibitionRepresentativeService.GetExhibitionRepresentative(new XsiExhibitionRepresentative() { NameEn = representativeDTO.NameEn, NameAr = representativeDTO.NameAr }).Select(i => i.RepresentativeId).ToList();

                if (representativeDTO.RepresentativeId > 0)
                {
                    if (parentrepIDs.Count == 0)
                        return true;
                    else if (parentrepIDs.Count() > 0)
                    {
                        var exhibitorIds = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly() { ExhibitionId = exhibitionId }).Select(i => i.MemberExhibitionYearlyId).ToList();
                        if (ExhibitionRepresentativeParticipatingService.GetExhibitionRepresentativeParticipating().Where(i => (i.Status == strBlocked || i.Status == strRejected) && i.RepresentativeId != representativeDTO.RepresentativeId && parentrepIDs.Contains(i.RepresentativeId) && exhibitorIds.Contains(i.MemberExhibitionYearlyId) && i.Passport == representativeDTO.PassportNumber).ToList().Count > 0)
                            return false;
                        if (ExhibitionRepresentativeParticipatingService.GetExhibitionRepresentativeParticipating().Where(i =>(i.Status != strBlocked && i.Status != strRejected) && i.RepresentativeId != representativeDTO.RepresentativeId && parentrepIDs.Contains(i.RepresentativeId) && exhibitorIds.Contains(i.MemberExhibitionYearlyId)&& i.Passport==representativeDTO.PassportNumber).ToList().Count == 0)
                            return true;
                    }
                }

                return false;
            }
        }
        public bool IsVisaApplicationFormValid(string fileExtenstion, bool hrefVisibility)
        {
            if (!string.IsNullOrEmpty(fileExtenstion))
            {
                if (MethodFactory.IsValidFile1(fileExtenstion))
                    return true;
            }
            else if (hrefVisibility)
                return true;

            return false;
        }
        public bool IsPasportCopyValid(string fileExtenstion, bool hrefVisibility)
        {
            if (!string.IsNullOrEmpty(fileExtenstion))
            {
                if (MethodFactory.IsValidImgAndPDF(fileExtenstion))
                    return true;
            }
            else if (hrefVisibility)
                return true;

            return false;
        }
        public bool IsPersonalPhotoValid(string fileExtenstion, bool hrefVisibility)
        {
            if (!string.IsNullOrEmpty(fileExtenstion))
            {
                if (MethodFactory.IsValidImage(fileExtenstion))
                    return true;
            }
            else if (hrefVisibility)
                return true;

            return false;
        }
        public bool IsExhibitionActive()
        {
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {

                XsiExhibition whereQuery = new XsiExhibition();
                whereQuery.IsActive = EnumConversion.ToString(EnumBool.Yes);
                whereQuery.IsArchive = EnumConversion.ToString(EnumBool.No);

                // whereQuery.WebsiteId = RepresentativeDTO.WebsiteId;
                //List<XsiExhibition> ExhibitionList = ExhibitionService.GetExhibition(whereQuery).OrderByDescending(i => i.ItemId).ToList();
                XsiExhibition entity = ExhibitionService.GetExhibition(whereQuery).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                if (entity != null)
                {
                    //if (entity.ExhibitionId > 0)
                    //    RepresentativeDTO.ExhibitionId = entity.ExhibitionId;
                    //if (entity.ExhibitorEndDate != null)
                    //    RepresentativeDTO.ExhibitorEndDate = entity.ExhibitorEndDate.Value;
                    //if (entity.StartDate != null)
                    //    RepresentativeDTO.StartDate = entity.StartDate.Value;
                    //if (entity.DateOfDue != null)
                    //    RepresentativeDTO.PenaltyDate = entity.DateOfDue.Value;
                    //if (entity.ExpiryMonths != null)
                    //    RepresentativeDTO.ExpiryMonths = Convert.ToInt32(entity.ExpiryMonths);
                    return true;
                }
            }
            return false;
        }
        public bool CheckExhibition(long memberExhibitionYearlyId, long exhibitionId)
        {
            ExhibitorRegistrationService exhibitorRegistrationService = new ExhibitorRegistrationService();
            var exhibitor = exhibitorRegistrationService.GetExhibitorRegistrationByItemId(memberExhibitionYearlyId);
            if (exhibitor != null)
            {
                if (exhibitor.ExhibitionId == exhibitionId)
                    return true;
                else
                    return false;
            }
            return true;
        }
        public bool CheckParticipated(long itemId, long exhibitorId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var list = _context.XsiExhibitionRepresentativeParticipating.Where(x => x.RepresentativeId == itemId && x.MemberExhibitionYearlyId == exhibitorId).ToList();
                if (list.Count > 0)
                    return true;
                else
                    return false;
            }
        }
        public bool CheckParticipatedNoVisa(long itemId, long exhibitorId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var list = _context.XsiExhibitionRepresentativeParticipatingNoVisa.Where(x => x.RepresentativeId == itemId && x.MemberExhibitionYearlyId == exhibitorId).ToList();
                if (list.Count > 0)
                    return true;
                else
                    return false;
            }
        }
        #endregion

        #region No Visa Validation
        public bool IsUniqueRepresentativeForAddNoVIsa(SaveRepresentativeDTO representativeDTO, long langId, long exhibitionId)
        {
            string strCancelled = EnumConversion.ToString(EnumRepresentativeStatus.Cancelled);
            string strRejected = EnumConversion.ToString(EnumRepresentativeStatus.Rejected);
            string strBlocked = EnumConversion.ToString(EnumRepresentativeStatus.Blocked);
            using (ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService())
            {
                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                ExhibitionRepresentativeParticipatingNoVisaService ExhibitionRepresentativeParticipatingNoVisaService = new ExhibitionRepresentativeParticipatingNoVisaService();
                List<long> parentrepIDs = new List<long>();
                List<long> participatingrepIDs = new List<long>();

                if (langId == 1)
                    parentrepIDs = ExhibitionRepresentativeService.GetExhibitionRepresentative(new XsiExhibitionRepresentative() { NameEn = representativeDTO.NameEn }).Select(i => i.RepresentativeId).ToList();
                else
                    parentrepIDs = ExhibitionRepresentativeService.GetExhibitionRepresentative(new XsiExhibitionRepresentative() { NameEn = representativeDTO.NameEn, NameAr = representativeDTO.NameAr }).Select(i => i.RepresentativeId).ToList();

                if (parentrepIDs.Count == 0)
                    return true;
                else if (parentrepIDs.Count() > 0)
                {
                    var exhibitorIds = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly() { ExhibitionId = exhibitionId }).Select(i => i.MemberExhibitionYearlyId).ToList();
                    if (ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa().Where(i => (i.Status == strBlocked || i.Status == strRejected) && parentrepIDs.Contains(i.RepresentativeId) && exhibitorIds.Contains(i.MemberExhibitionYearlyId) && i.Passport == representativeDTO.PassportNumber).ToList().Count > 0)
                        return false;

                    if (ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa().Where(i => (i.Status != strBlocked && i.Status != strRejected) && parentrepIDs.Contains(i.RepresentativeId) && exhibitorIds.Contains(i.MemberExhibitionYearlyId) && i.Passport==representativeDTO.PassportNumber).ToList().Count == 0)
                        return true;
                }
                return false;
            }
        }
        public bool IsUniqueRepresentativeForUpdateNoVIsa(SaveRepresentativeDTO representativeDTO, long langId, long exhibitionId)
        {
            string strCancelled = EnumConversion.ToString(EnumRepresentativeStatus.Cancelled);
            string strRejected = EnumConversion.ToString(EnumRepresentativeStatus.Rejected);
            string strBlocked = EnumConversion.ToString(EnumRepresentativeStatus.Blocked);
            using (ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService())
            {
                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                ExhibitionRepresentativeParticipatingNoVisaService ExhibitionRepresentativeParticipatingNoVisaService = new ExhibitionRepresentativeParticipatingNoVisaService();
                List<long> parentrepIDs = new List<long>();
                if (langId == 1)
                    parentrepIDs = ExhibitionRepresentativeService.GetExhibitionRepresentative(new XsiExhibitionRepresentative() { NameEn = representativeDTO.NameEn }).Select(i => i.RepresentativeId).ToList();
                else
                    parentrepIDs = ExhibitionRepresentativeService.GetExhibitionRepresentative(new XsiExhibitionRepresentative() { NameEn = representativeDTO.NameEn, NameAr = representativeDTO.NameAr }).Select(i => i.RepresentativeId).ToList();

                if (representativeDTO.RepresentativeId > 0)
                {
                    if (parentrepIDs.Count == 0)
                        return true;
                    else if (parentrepIDs.Count() > 0)
                    {
                        var exhibitorIds = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly() { ExhibitionId = exhibitionId }).Select(i => i.MemberExhibitionYearlyId).ToList();
                        if (ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa().Where(i => (i.Status == strBlocked || i.Status == strRejected) && i.RepresentativeId != representativeDTO.RepresentativeId && parentrepIDs.Contains(i.RepresentativeId) && exhibitorIds.Contains(i.MemberExhibitionYearlyId) && i.Passport == representativeDTO.PassportNumber).ToList().Count > 0)
                            return false;
                        if (ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa().Where(i => (i.Status != strBlocked && i.Status != strRejected) && i.RepresentativeId != representativeDTO.RepresentativeId && parentrepIDs.Contains(i.RepresentativeId) && exhibitorIds.Contains(i.MemberExhibitionYearlyId)&& i.Passport== representativeDTO.PassportNumber).ToList().Count == 0)
                            return true;
                    }
                }
                return false;
            }
        }
        public bool IsUniqueStaffguestForAdd(SaveStaffGuestDTO StaffGuestDTO, long langId, long exhibitionId)
        {
            string strCancelled = EnumConversion.ToString(EnumRepresentativeStatus.Cancelled);
            string strRejected = EnumConversion.ToString(EnumRepresentativeStatus.Rejected);
            string strBlocked = EnumConversion.ToString(EnumRepresentativeStatus.Blocked);
            using (ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService())
            {
                ExhibitionStaffGuestParticipatingService ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService();
                List<long> parentrepIDs = new List<long>();
                if (langId == 1)
                    parentrepIDs = ExhibitionStaffGuestService.GetExhibitionStaffGuest(new XsiExhibitionStaffGuest() { NameEn = StaffGuestDTO.NameEn }).Select(i => i.StaffGuestId).ToList();
                else
                    parentrepIDs = ExhibitionStaffGuestService.GetExhibitionStaffGuest(new XsiExhibitionStaffGuest() { NameEn = StaffGuestDTO.NameEn, NameAr = StaffGuestDTO.NameAr }).Select(i => i.StaffGuestId).ToList();

                if (parentrepIDs.Count == 0)
                    return true;
                else if (parentrepIDs.Count() > 0)
                {
                    if (ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating().Where(i => (i.Status == strBlocked || i.Status == strRejected) && i.ExhibitionId == exhibitionId && i.Passport == StaffGuestDTO.PassportNumber).ToList().Count > 0)
                        return false;
                    if (ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating().Where(i => (i.Status != strBlocked && i.Status != strRejected) && i.ExhibitionId == exhibitionId && i.Passport == StaffGuestDTO.PassportNumber).ToList().Count == 0)
                        return true;
                }
                return false;
            }
        }
        public bool IsUniqueStaffguestForUpdate(SaveStaffGuestDTO StaffGuestDTO, long langId, long exhibitionId)
        {
            string strCancelled = EnumConversion.ToString(EnumRepresentativeStatus.Cancelled);
            string strRejected = EnumConversion.ToString(EnumRepresentativeStatus.Rejected);
            string strBlocked = EnumConversion.ToString(EnumRepresentativeStatus.Blocked);
            using (ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService())
            {
                ExhibitionStaffGuestParticipatingService ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService();
                List<long> parentrepIDs = new List<long>();
                if (langId == 1)
                    parentrepIDs = ExhibitionStaffGuestService.GetExhibitionStaffGuest(new XsiExhibitionStaffGuest() { NameEn = StaffGuestDTO.NameEn }).Select(i => i.StaffGuestId).ToList();
                else
                    parentrepIDs = ExhibitionStaffGuestService.GetExhibitionStaffGuest(new XsiExhibitionStaffGuest() { NameEn = StaffGuestDTO.NameEn, NameAr = StaffGuestDTO.NameAr }).Select(i => i.StaffGuestId).ToList();

                if (StaffGuestDTO.StaffGuestId > 0)
                {
                    if (parentrepIDs.Count == 0)
                        return true;
                    else if (parentrepIDs.Count() > 0)
                    {
                        if (ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating().Where(i => (i.Status == strBlocked || i.Status == strRejected) && i.StaffGuestId != StaffGuestDTO.StaffGuestId &&  i.ExhibitionId== exhibitionId && i.Passport == StaffGuestDTO.PassportNumber).ToList().Count > 0)
                            return false;
                        if (ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating().Where(i => (i.Status != strBlocked && i.Status != strRejected) && i.StaffGuestId != StaffGuestDTO.StaffGuestId && i.ExhibitionId == exhibitionId && i.Passport == StaffGuestDTO.PassportNumber).ToList().Count == 0)
                            return true;
                    }
                }
                return false;
            }
        }
        public bool IsUniquePCStaffguestForAdd(PCSaveStaffGuestDTO StaffGuestDTO, long langId, long exhibitionId)
        {
            string strCancelled = EnumConversion.ToString(EnumRepresentativeStatus.Cancelled);
            string strRejected = EnumConversion.ToString(EnumRepresentativeStatus.Rejected);
            string strBlocked = EnumConversion.ToString(EnumRepresentativeStatus.Blocked);
            using (ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService())
            {
                ExhibitionStaffGuestParticipatingService ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService();
                List<long> parentrepIDs = new List<long>();
                if (langId == 1)
                    parentrepIDs = ExhibitionStaffGuestService.GetExhibitionStaffGuest(new XsiExhibitionStaffGuest() { NameEn = StaffGuestDTO.NameEn }).Select(i => i.StaffGuestId).ToList();
                else
                    parentrepIDs = ExhibitionStaffGuestService.GetExhibitionStaffGuest(new XsiExhibitionStaffGuest() { NameEn = StaffGuestDTO.NameEn, NameAr = StaffGuestDTO.NameAr }).Select(i => i.StaffGuestId).ToList();

                if (parentrepIDs.Count == 0)
                    return true;
                else if (parentrepIDs.Count() > 0)
                {
                    if (ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating().Where(i => (i.Status == strBlocked || i.Status == strRejected) && i.ExhibitionId == exhibitionId && i.Passport == StaffGuestDTO.PassportNumber).ToList().Count > 0)
                        return false;
                    if (ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating().Where(i => (i.Status != strBlocked && i.Status != strRejected) && i.ExhibitionId == exhibitionId && i.Passport == StaffGuestDTO.PassportNumber).ToList().Count == 0)
                        return true;
                }
                return false;
            }
        }
        public bool IsUniquePCStaffguestForUpdate(PCSaveStaffGuestDTO StaffGuestDTO, long langId, long exhibitionId)
        {
            string strCancelled = EnumConversion.ToString(EnumRepresentativeStatus.Cancelled);
            string strRejected = EnumConversion.ToString(EnumRepresentativeStatus.Rejected);
            string strBlocked = EnumConversion.ToString(EnumRepresentativeStatus.Blocked);
            using (ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService())
            {
                ExhibitionStaffGuestParticipatingService ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService();
                List<long> parentrepIDs = new List<long>();
                if (langId == 1)
                    parentrepIDs = ExhibitionStaffGuestService.GetExhibitionStaffGuest(new XsiExhibitionStaffGuest() { NameEn = StaffGuestDTO.NameEn }).Select(i => i.StaffGuestId).ToList();
                else
                    parentrepIDs = ExhibitionStaffGuestService.GetExhibitionStaffGuest(new XsiExhibitionStaffGuest() { NameEn = StaffGuestDTO.NameEn, NameAr = StaffGuestDTO.NameAr }).Select(i => i.StaffGuestId).ToList();

                if (StaffGuestDTO.StaffGuestId > 0)
                {
                    if (parentrepIDs.Count == 0)
                        return true;
                    else if (parentrepIDs.Count() > 0)
                    {
                        if (ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating().Where(i => (i.Status == strBlocked || i.Status == strRejected) && i.StaffGuestId != StaffGuestDTO.StaffGuestId && i.ExhibitionId == exhibitionId && i.Passport == StaffGuestDTO.PassportNumber).ToList().Count > 0)
                            return false;
                        if (ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating().Where(i => (i.Status != strBlocked && i.Status != strRejected) && i.StaffGuestId != StaffGuestDTO.StaffGuestId && i.ExhibitionId == exhibitionId && i.Passport == StaffGuestDTO.PassportNumber).ToList().Count == 0)
                            return true;
                    }
                }
                return false;
            }
        }
      
        #endregion
    }
}

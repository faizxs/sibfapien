﻿using Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using Xsi.BusinessLogicLayer;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using OfficeOpenXml.ConditionalFormatting;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ExhibitionMemberLoginController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;
        EnumResultType XsiResult { get; set; }

        private readonly IEmailSender _emailSender;
        private IUserService _userService;
        private readonly IConfiguration _configuration;
        public ExhibitionMemberLoginController(IEmailSender emailSender, IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IUserService userService, sibfnewdbContext context, IConfiguration configuration)
        {
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _userService = userService;
            _context = context;
            _configuration = configuration;
        }


        // GET: api/ExhibitionMemberLogin/IsValidMemberLoggedIn/1
        [HttpGet]
        [Route("IsValidMemberLoggedIn/{memberid}")]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> IsValidMemberLoggedIn(long memberid)
        {
            dynamic dto = new ExpandoObject();
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                dto.isvalid = false;
                if (User.Identity.GetID() == memberid)
                {
                    dto.isvalid = true;

                }
                return Ok(dto);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
            return Ok(dto);
        }

        // POST: api/ExhibitionMemberLogin
        [AllowAnonymous]
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> ExhibitionMemberLogin(LoginDto data)
        {
            LoggedinUserInfo userInfo = new LoggedinUserInfo();
            userInfo.IsValidLoggedInMember = false;

            try
            {
                long langid = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langid);
                userInfo = _userService.Authenticate(data.UserName, data.Password, langid);
                return Ok(userInfo);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
            return Ok(userInfo);
        }


        [AllowAnonymous]
        [HttpPost("logout")]
        [EnableCors("AllowOrigin")]
        public async Task<IActionResult> ExhibitionMemberLogout()
        {
           // await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Ok();
        }

        // POST: api/ExhibitionMemberLogin/ForgotPassword
        [AllowAnonymous]
        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("ForgotPassword")]
        public ActionResult<dynamic> ForgotPassword([FromBody] ForgotPasswordDto data)
        {
            string strPopid = string.Empty;
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long websiteid = 1;
                long.TryParse(Request.Headers["websiteId"], out websiteid);
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    var entity = _context.XsiExhibitionMember.Where(i => i.Email == data.Email).FirstOrDefault();

                    if (entity != null)
                    {
                        if (entity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.Approved))
                        {
                            entity.ForgotPasswordExpiresAt = MethodFactory.ArabianTimeNow().AddMinutes(45);
                            _context.SaveChanges();

                            #region Email Content

                            if (websiteid == 2)
                            {
                                XsiScrfemailContent emailContent;
                                emailContent = _context.XsiScrfemailContent.Where(i => i.ItemId == 20141).FirstOrDefault(); //EmailContentService.GetEmailContent(4, entity.LanguageId.Value);
                                if (emailContent != null)
                                {
                                    StringBuilder body = new StringBuilder();
                                    body.Append(GetEmailContentAndTemplate(1, emailContent.Body, entity, _appCustomSettings.ServerAddressNew, websiteid));
                                    body.Replace("$$UserName$$", entity.UserName);
                                    body.Replace("$$Url$$", string.Format("{0}en/memberchangepassword?id={1}", _appCustomSettings.ServerAddressSCRF, entity.MemberId.ToString()));

                                    XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminNameSCRF, _appCustomSettings.AdminEmail, entity.Email, emailContent.Subject, body.ToString());

                                    if (XsiResult == EnumResultType.Success || XsiResult == EnumResultType.EmailSent)
                                    {

                                        if (LangId == 1)
                                            return Ok("Email Sent! Please check your email after 5 to 10 minutes");
                                        else
                                            return Ok("تم ارسال بريد الكتروني لاستعادة كلمة المرور المرور الخاصة بكم");
                                    }
                                }
                            }
                            else
                            {
                                XsiEmailContent emailContent;

                                emailContent = _context.XsiEmailContent.Where(i => i.ItemId == 7).FirstOrDefault(); //EmailContentService.GetEmailContent(4, entity.LanguageId.Value);
                                if (emailContent != null)
                                {
                                    StringBuilder body = new StringBuilder();

                                    body.Append(GetEmailContentAndTemplate(1, emailContent.Body, entity, _appCustomSettings.ServerAddressNew, websiteid));
                                    body.Replace("$$UserName$$", entity.UserName);
                                    body.Replace("$$Url$$", string.Format("{0}en/ChangePassword?id={1}", _appCustomSettings.ServerAddressNew, entity.MemberId.ToString()));

                                    XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, entity.Email, emailContent.Subject, body.ToString());

                                    if (XsiResult == EnumResultType.Success || XsiResult == EnumResultType.EmailSent)

                                        if (LangId == 1)
                                            return Ok("Email Sent! Please check your email after 5 to 10 minutes");
                                        else
                                            return Ok("تم ارسال بريد الكتروني لاستعادة كلمة المرور المرور الخاصة بكم");
                                }
                            }

                            return Ok("Email not Sent! Please try after some time.");
                            #endregion
                        }
                        return Ok("Inactive User State");
                    }
                }
                return Ok("EmailId Not Registered");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return Ok("Exception: Something went wrong. Please retry again later. Email Not sent");
            }
        }

        //[AllowAnonymous]
        //[HttpGet]
        //public async Task<IActionResult> ExhibitionMemberLogin()
        //{
        //    // await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        //    return Ok();
        //}



        //// POST: api/ExhibitionMemberLogin
        //[AllowAnonymous]
        //[HttpPost("refresh")]
        //[EnableCors("AllowOrigin")]
        //[ApiExplorerSettings(IgnoreApi = true)]
        //public ActionResult<dynamic> RefreshToken()
        //{
        //    long langid = 1;
        //    long.TryParse(Request.Headers["LanguageURL"], out langid);

        //    HttpContext.Request.Cookies.TryGetValue("accessToken", out var accessToken);
        //    HttpContext.Request.Cookies.TryGetValue("refreshToken", out var refreshToken);
        //    TokenDTO tokenDto = new TokenDTO();
        //    tokenDto.accessToken = accessToken;
        //    tokenDto.refreshToken = refreshToken;

        //    LoggedinUserInfo userInfo = new LoggedinUserInfo();
        //    userInfo = _userService.UpdateRefreshToken(tokenDto, "", langid);
        //    _userService.setTokensInsideCookie(tokenDto);

        //    return Ok();
        //}

        //// POST: api/ExhibitionMemberLogin
        //[AllowAnonymous]
        //[HttpGet]
        //[Route("SaveInfoFz")]
        //[EnableCors("AllowOrigin")]
        //public ActionResult<dynamic> ExhibitionMemberHashpassword()
        //{
        //    try
        //    {
        //        long langid = 1;
        //        long.TryParse(Request.Headers["LanguageURL"], out langid);

        //        var memberList = _context.XsiExhibitionMember.Where(i => i.PasswordHash == null).ToList();
        //        foreach (var member in memberList)
        //        {
        //            byte[] passwordHash, passwordSalt;
        //            PasswordHasher.CreatePasswordHash(member.Password, out passwordHash, out passwordSalt);
        //            member.PasswordSalt = passwordSalt;
        //            member.PasswordHash = passwordHash;
        //            member.ModifiedOn = MethodFactory.ArabianTimeNow();
        //            _context.XsiExhibitionMember.Update(member);
        //            _context.SaveChanges();
        //        }

        //        return Ok(true);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex.ToString());
        //    }
        //    return Ok(false);
        //}

        #region Email Utility
        StringBuilder GetEmailContentAndTemplate(long languageId, string bodyContent, XsiExhibitionMember entity, string serverAddress, long Wid)
        {
            string scrfURL = _appCustomSettings.ServerAddressSCRF;
            string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
            string emailContent = string.Empty;
            if (languageId == 1)
            {
                if (Wid == Convert.ToInt64(EnumWebsiteId.SCRF))
                    emailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRF.html";
                else
                    emailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
            }
            else
            {
                if (Wid == Convert.ToInt64(EnumWebsiteId.SCRF))
                    emailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRF.html";
                else
                    emailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
            }
            StringBuilder body = new StringBuilder();

            if (System.IO.File.Exists(emailContent))
            {
                body.Append(System.IO.File.ReadAllText(emailContent));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);
            string str = GetExhibitionDetails(Wid);
            string[] strArray = str.Split(',');
            body.Replace("$$exhibitionno$$", strArray[0].ToString());
            body.Replace("$$exhibitiondate$$", strArray[1].ToString());
            body.Replace("$$exhyear$$", strArray[2]);

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            body.Replace(swidth, sreplacewidth);

            body.Replace("$$ServerAddress$$", serverAddress);
            body.Replace("$$ServerAddress$$", ServerAddressCMS);
            if (Wid == Convert.ToInt64(EnumWebsiteId.SCRF))
                body.Replace("$$ServerAddressSCRF$$", scrfURL);

            if (entity != null)
            {
                string name = string.Empty;
                if (languageId == 1)
                {
                    if (entity.Firstname != null && entity.LastName != null)
                        name = entity.Firstname + " " + entity.LastName;
                    else if (entity.Firstname != null)
                        name = entity.Firstname;
                    else if (entity.LastName != null)
                        name = entity.LastName;
                }
                else
                {
                    if (entity.FirstNameAr != null && entity.LastNameAr != null)
                        name = entity.FirstNameAr + " " + entity.LastNameAr;
                    else if (entity.Firstname != null)
                        name = entity.FirstNameAr;
                    else if (entity.LastName != null)
                        name = entity.LastNameAr;

                    if (string.IsNullOrEmpty(name))
                    {
                        if (entity.Firstname != null && entity.LastName != null)
                            name = entity.Firstname + " " + entity.LastName;
                        else if (entity.Firstname != null)
                            name = entity.Firstname;
                        else if (entity.LastName != null)
                            name = entity.LastName;
                    }
                }
                body.Replace("$$Name$$", name);
            }
            return body;
        }
        string GetExhibitionDetails(long websiteId)
        {
            long? exhibitionNumber = -1;
            string strdate = string.Empty;

            XsiExhibition entity = _context.XsiExhibition.Where(i => i.WebsiteId == websiteId && i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.IsArchive == EnumConversion.ToString(EnumBool.No)).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
            string startdaysuffix = string.Empty;
            string enddaysuffix = string.Empty;
            string stryear = MethodFactory.ArabianTimeNow().Year.ToString();

            if (entity != default(XsiExhibition))
            {
                if (entity.ExhibitionNumber != null)
                    exhibitionNumber = entity.ExhibitionNumber;
                long dayno = entity.StartDate.Value.Date.Day;
                if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                    startdaysuffix = "th";
                else
                    startdaysuffix = GetSuffix(entity.StartDate.Value.Date.Day % 10, entity.StartDate.Value);
                dayno = entity.EndDate.Value.Date.Day;
                if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                    enddaysuffix = "th";
                else
                    enddaysuffix = GetSuffix(entity.EndDate.Value.Date.Day % 10, entity.EndDate.Value);

                if (entity.StartDate.Value.Year == entity.EndDate.Value.Year)
                {
                    if (entity.StartDate.Value.Month == entity.EndDate.Value.Month)
                        strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.StartDate.Value.ToString("MMM") + " " + entity.StartDate.Value.ToString("yyyy");
                    else
                        strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " + entity.StartDate.Value.ToString("MMM")
                             + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.EndDate.Value.ToString("MMM")
                            + " " + entity.StartDate.Value.ToString("yyyy");
                }
                else
                {
                    strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " + entity.StartDate.Value.ToString("MMM") + " " + entity.StartDate.Value.ToString("yyyy")
                         + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.EndDate.Value.ToString("MMM")
                        + " " + entity.EndDate.Value.ToString("yyyy");
                }
                if (entity.ExhibitionYear != null)
                    stryear = entity.ExhibitionYear.ToString();
            }
            return exhibitionNumber.ToString() + "," + strdate + "," + stryear;
        }
        string GetSuffix(int startday, DateTime dt)
        {
            string daysuffix;
            if (startday != 11 && dt.Date.Day != 11)
            {
                if (startday == 1)
                    daysuffix = "st";
                else if (startday == 2)
                    daysuffix = "nd";
                else if (startday == 3)
                    daysuffix = "rd";
                else
                    daysuffix = "th";
            }
            else
                daysuffix = "th";
            return daysuffix;
        }
        #endregion
    }
    public static class IdentityExtensions
    {
        public static Int64 GetID(this IIdentity identity)
        {
            if (identity == null)
                return 0;

            var claims = identity as ClaimsIdentity;

            if (claims.FindFirst("ID") == null)
                return 0;

            return Convert.ToInt64(claims.FindFirst("ID").Value);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PhotoSubCategoryController : ControllerBase
    {
        public PhotoSubCategoryController()
        {
        }

        // GET: api/PhotoSubCategory
        [HttpGet]
        [Route("PhotoSubCategoryByCategoryId/{categoryid}")]
        public async Task<ActionResult<IEnumerable<DropdownDataDTO>>> PhotoSubCategoryByCategoryId(long categoryid = -1)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiPhotoSubCategory>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.CategoryId == categoryid);
                    var List = _context.XsiPhotoSubCategory.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(x => new DropdownDataDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.Title,
                    }).ToListAsync();

                    return await List;
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiPhotoSubCategory>();
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.CategoryId == categoryid);
                    var List = _context.XsiPhotoSubCategory.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr).Select(x => new DropdownDataDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.TitleAr,
                    }).ToListAsync();

                    return await List;
                }
            }
        }

        // GET: api/PhotoSubCategory/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DropdownDataDTO>> GetXsiPhotoSubCategory(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var xsiItem = await _context.XsiPhotoSubCategory.FindAsync(id);

                if (xsiItem == null)
                {
                    return NotFound();
                }

                DropdownDataDTO itemDTO = new DropdownDataDTO()
                {
                    ItemId = xsiItem.ItemId,
                    Title = LangId == 1 ? xsiItem.Title : xsiItem.TitleAr,
                };
                return itemDTO;
            }
        }
    }
}

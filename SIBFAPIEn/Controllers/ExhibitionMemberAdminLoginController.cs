﻿using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ExhibitionMemberAdminLoginController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly sibfnewdbContext _context;
        EnumResultType XsiResult { get; set; }

        private readonly IEmailSender _emailSender;
        private IAdminMemberUserServiceService _userService;
        private readonly IConfiguration _configuration;
        private readonly AppCustomSettings _appCustomSettings;

        public ExhibitionMemberAdminLoginController(IEmailSender emailSender, ILoggerManager logger, IAdminMemberUserServiceService userService, sibfnewdbContext context, IOptions<AppCustomSettings> appCustomSettings, IConfiguration configuration)
        {
            _logger = logger;
            _userService = userService;
            _context = context;
            _configuration = configuration;
            this._appCustomSettings = appCustomSettings.Value;
            _emailSender = emailSender;
        }

        // POST: api/ExhibitionMemberLogin
        [AllowAnonymous]
        [HttpGet("{adminid}/{memberid}")]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> ExhibitionMemberAdminLogin(long adminid, long memberid)
        {
            AdminLoggedinUserInfo userInfo = new AdminLoggedinUserInfo();
            userInfo.IsValidLoggedInMember = false;
            try
            {
                long langid = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langid);

                long webid = 1;
                long.TryParse(Request.Headers["websiteId"], out webid);
                //1,22,23,10059,10063
                List<long> adminuserIDs = new List<long>(){
                    1,8,10086,10088,10089,10090,10091,10115,10111
                };

                #region Validations
                if (!adminuserIDs.Contains(adminid))
                {
                    userInfo.StatusMessage = langid == 1 ? "Bad Request or Unauthorised user" : "Bad Request or Unauthorised user";
                    userInfo.PopUpId = "2";
                    return userInfo;
                }

                string memberusername = string.Empty;
                string adminusername = string.Empty;

                var adminentity = _context.XsiAdminUsers.Where(i => i.ItemId == adminid && i.IsActive == "Y").FirstOrDefault();
                var memberentity = _context.XsiExhibitionMember.Where(i => i.MemberId == memberid && i.Status == "A").FirstOrDefault();
                if (adminentity != null && adminentity != default(XsiAdminUsers))
                {
                    adminusername = adminentity.UserName;
                }
                if (memberentity != null && memberentity != default(XsiExhibitionMember))
                {
                    memberusername = memberentity.UserName;
                }

                if (string.IsNullOrEmpty(memberusername) || string.IsNullOrEmpty(adminusername))
                {
                    userInfo.PopUpId = "2";
                    userInfo.StatusMessage = "Invalid login details..";
                    return Ok(userInfo);
                }
                #endregion
                userInfo.PopUpId = "-2";
                userInfo.UserName = memberusername;
                userInfo.AdminUserName = adminusername;
                userInfo.SIBFMemberId = memberentity.MemberId;
                userInfo.AdminUserId = adminentity.ItemId;

                bool issendmail = SendOTPToAdmin(userInfo, webid);
                if (issendmail)
                {
                    userInfo.StatusMessage = langid == 1 ? "OTP sent to email. Please validate." : "OTP sent to email. Please validate.";
                    return Ok(userInfo);
                }
                else
                {
                    userInfo.StatusMessage = langid == 1 ? "OTP could not be sent. Please try again later" : "OTP could not be sent. Please try again later.";
                    userInfo.PopUpId = "2";
                    return Ok(userInfo);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                userInfo.StatusMessage = ex.ToString();
                return Ok(userInfo);
            }
        }

        // POST: api/ExhibitionMemberLogin
        [AllowAnonymous]
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> ExhibitionMemberAdminLogin(AdminLoginDto data)
        {
            AdminLoggedinUserInfo userInfo = new AdminLoggedinUserInfo();
            userInfo.IsValidLoggedInMember = false;
            try
            {
                long langid = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langid);

                long webid = 1;
                long.TryParse(Request.Headers["websiteId"], out webid);

                userInfo = _userService.AuthenticateAdminLoginForMember(data.UserName, data.AdminUserName, data.Password, langid);
                if (userInfo.PopUpId == "-2")
                {
                    bool issendmail = SendOTPToAdmin(userInfo, webid);
                    if (issendmail)
                    {
                        userInfo.StatusMessage = langid == 1 ? "OTP sent to email. Please validate." : "OTP sent to email. Please validate.";
                        return Ok(userInfo);
                    }
                    else
                    {
                        userInfo.StatusMessage = langid == 1 ? "OTP could not be sent. Please try again later" : "OTP could not be sent. Please try again later.";
                        userInfo.PopUpId = "2";
                        return Ok(userInfo);
                    }
                }
                userInfo.StatusMessage = "Invalid login details..";
                return Ok(userInfo);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                userInfo.StatusMessage = ex.ToString();
                return Ok(userInfo);
            }
        }

        [AllowAnonymous]
        [HttpPost("ValidateEmailOTP")]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> ExhibitionMemberAdminValidateOTPLogin(AdminLoginOTPDto data)
        {
            AdminLoggedinUserInfo userInfo = new AdminLoggedinUserInfo();
            userInfo.IsValidLoggedInMember = false;

            try
            {
                long langid = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langid);
                userInfo = _userService.AuthenticateAdminLoginForMemberUsingOTP(data.UserName, data.AdminUserName, data.OTP, langid);
                if (userInfo.PopUpId == "3")
                    ResetOTPAndExpiry(userInfo.SIBFMemberId);
                return Ok(userInfo);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
            return Ok(userInfo);
        }

        private void ResetOTPAndExpiry(long memberid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var member = _context.XsiExhibitionMember.Where(i => i.MemberId == memberid).FirstOrDefault();
                member.AdminEmailOtp = null;
                member.AdminEmailOtpexpiresAt = null;
                _context.SaveChanges();
            }
        }

        private bool SendOTPToAdmin(AdminLoggedinUserInfo userInfo, long webid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var otp = MethodFactory.GenerateOTP(3, 3);

                var member = _context.XsiExhibitionMember.Where(i => i.MemberId == userInfo.SIBFMemberId).FirstOrDefault();
                if (member != null)
                {
                    member.AdminEmailOtp = otp;
                    member.AdminEmailOtpexpiresAt = MethodFactory.ArabianTimeNow().AddMinutes(20);
                    _context.SaveChanges();

                    var adminuser = _context.XsiAdminUsers.Where(i => i.ItemId == userInfo.AdminUserId).FirstOrDefault();
                    if (adminuser != default(XsiAdminUsers))
                    {
                        StringBuilder body = new StringBuilder();
                        if (webid == 2)
                        {
                            body.Append("Dear Admin User,<br><br>OTP for SCRF Member login (" + member.UserName + " is " + otp + ")<br><br> Regards<br>Admin");
                            if (userInfo.AdminUserId == 1)
                            {
                                XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminNameSCRF, _appCustomSettings.AdminEmail, adminuser.Email, "Admin: Member login otp", body.ToString(), null);
                            }
                            else
                            {
                                XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminNameSCRF, _appCustomSettings.AdminEmail, adminuser.Email, "Admin: Member login otp", body.ToString(), null);
                            }
                        }
                        else
                        {
                            body.Append("Dear Admin User,<br><br>OTP for SIBF Member login (" + member.UserName + " is " + otp + ")<br><br> Regards<br>Admin");

                            if (userInfo.AdminUserId == 1)
                            {
                                XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, adminuser.Email, "Admin: Member login otp", body.ToString(), null);
                            }
                            else
                            {
                                XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, adminuser.Email, "Admin: Member login otp", body.ToString(), null);
                            }
                        }
                        if (XsiResult == EnumResultType.Success)
                            return true;
                    }
                }
                return false;
            }
        }
    }
}

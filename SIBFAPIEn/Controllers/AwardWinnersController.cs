﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AwardWinnersController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public AwardWinnersController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }
        // GET: api/Awards
        [HttpGet]
        [Route("{pageNumber}/{pageSize}/{exhibitionid}")]
        public async Task<ActionResult<dynamic>> GetXsiAwardWinners(int? pageNumber = 1, int? pageSize = 15, long exhibitionid = -1)
        {
            try
            {
                long WebsiteId = Convert.ToInt32(EnumWebsiteTypeId.SIBF);
                //    MethodFactory.LoadPageContentsNew(92, langID);
                var thumbnailpath = _appCustomSettings.UploadsPath + "/Winner/PublisherThumbnail/";
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiAwards>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.WebsiteId == WebsiteId);
                    var SIBFAwardIds = _context.XsiAwards.Where(predicate).Select(i => i.ItemId).ToList();


                    XsiAwardWinners where = new XsiAwardWinners();
                    var predicateW = PredicateBuilder.True<XsiAwardWinners>();

                    if (exhibitionid != -1)
                        predicateW = predicateW.And(i => i.ExhibitionId == exhibitionid);

                    var WinnerList = _context.XsiAwardWinners.Where(predicateW).OrderByDescending(i => i.ItemId).ToList();

                    var List = WinnerList.Select(x => new AwardWinnerDto()
                    {
                        ItemId = x.ItemId,
                        AwardId = x.AwardId ?? -1,
                        SubAwardId = x.SubAwardId ?? -1,
                        ExhibitionId = x.ExhibitionId ?? -1,
                        AwardTitle = GetAwardOrSubAwardTitle(x.SubAwardId ?? -1, x.AwardId ?? -1, LangId),
                        PublisherName = x.PublisherEn,
                        PublisherThumbnail = thumbnailpath + (x.PublisherThumbnail ?? "award-tile.jpg"),
                        Overview = x.OverviewEn
                    }).ToList();

                    var totalCount = List.Count;
                    List = List.Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToList();
                    var dto = new { List, TotalCount = totalCount };

                    return Ok(dto);
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiAwards>();
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.WebsiteId == WebsiteId);
                    var SIBFAwardIds = _context.XsiAwards.Where(predicate).Select(i => i.ItemId).ToList();


                    XsiAwardWinners where = new XsiAwardWinners();
                    var predicateW = PredicateBuilder.True<XsiAwardWinners>();

                    if (exhibitionid != -1)
                        predicateW = predicateW.And(i => i.ExhibitionId == exhibitionid);

                    var WinnerList = _context.XsiAwardWinners.Where(predicateW).OrderByDescending(i => i.ItemId).ToList();

                    var List = WinnerList.Select(x => new AwardWinnerDto()
                    {
                        ItemId = x.ItemId,
                        AwardId = x.AwardId ?? -1,
                        SubAwardId = x.SubAwardId ?? -1,
                        ExhibitionId = x.ExhibitionId ?? -1,
                        AwardTitle = GetAwardOrSubAwardTitle(x.SubAwardId ?? -1, x.AwardId ?? -1, LangId),
                        PublisherName = x.PublisherAr,
                        PublisherThumbnail = thumbnailpath + (x.PublisherThumbnail ?? "award-tile.jpg"),
                        Overview = x.OverviewEn
                    }).ToList();

                    var totalCount = List.Count;
                    List = List.Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToList();
                    var dto = new { List, TotalCount = totalCount };

                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwardWinners action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        // GET: api/AwardWinners/5
        [HttpGet("{id}")]
        public ActionResult<dynamic> GetXsiAwardWinnerDetails(long id)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiAwardWinners>();
                    predicate = predicate.And(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    var thumbnailpath = _appCustomSettings.UploadsPath;
                    var dto = _context.XsiAwardWinners.AsQueryable().Where(predicate)
                        .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(x => new AwardWinnerDto()
                        {
                            ItemId = x.ItemId,
                            AwardId = x.AwardId ?? -1,
                            SubAwardId = x.SubAwardId ?? -1,
                            ExhibitionId = x.ExhibitionId ?? -1,
                            // AwardTitle = GetAwardOrSubAwardTitle(x.SubAwardId ?? -1, x.AwardId ?? -1),
                            PublisherName = x.PublisherEn,
                            PublisherThumbnail = (x.PublisherThumbnail == null) ? thumbnailpath + "/Winner/PublisherThumbnail/" + "award-tile.jpg" : thumbnailpath + "/Winner/PublisherThumbnail/" + x.PublisherThumbnail,
                            Overview = x.OverviewEn,
                            BookTitle = x.BookTitleEn,
                            BookThumbnail = (x.BookThumbnail == null) ? thumbnailpath + "/Winner/BookThumbnail/" + "award-tile.jpg" : thumbnailpath + "/Winner/BookThumbnail/" + x.BookThumbnail,
                            BookOverview = x.BookOverviewEn
                        }).FirstOrDefault();

                    if (dto == null)
                    {
                        return NotFound();
                    }

                    dto.AwardTitle = GetAwardOrSubAwardTitle(dto.SubAwardId, dto.AwardId, LangId);
                    return Ok(dto);
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiAwardWinners>();
                    predicate = predicate.And(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    var thumbnailpath = _appCustomSettings.UploadsPath;
                    var dto = _context.XsiAwardWinners.AsQueryable().Where(predicate)
                        .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(x => new AwardWinnerDto()
                        {
                            ItemId = x.ItemId,
                            AwardId = x.AwardId ?? -1,
                            SubAwardId = x.SubAwardId ?? -1,
                            ExhibitionId = x.ExhibitionId ?? -1,
                            // AwardTitle = GetAwardOrSubAwardTitle(x.SubAwardId ?? -1, x.AwardId ?? -1),
                            PublisherName = x.PublisherAr,
                            PublisherThumbnail = (x.PublisherThumbnail == null) ? thumbnailpath + "/Winner/PublisherThumbnail/" + "award-tile.jpg" : thumbnailpath + "/Winner/PublisherThumbnail/" + x.PublisherThumbnail,
                            Overview = x.OverviewAr,
                            BookTitle = x.BookTitleAr,
                            BookThumbnail = (x.BookThumbnail == null) ? thumbnailpath + "/Winner/BookThumbnail/" + "award-tile.jpg" : thumbnailpath + "/Winner/BookThumbnail/" + x.BookThumbnail,
                            BookOverview = x.BookOverviewAr
                        }).FirstOrDefault();

                    if (dto == null)
                    {
                        return NotFound();
                    }

                    dto.AwardTitle = GetAwardOrSubAwardTitle(dto.SubAwardId, dto.AwardId, LangId);
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwardWinners action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
        string GetAwardOrSubAwardTitle(long subawardid, long awardid, long langId)
        {
            if (langId == 1)
            {
                var predicate = PredicateBuilder.True<XsiAwards>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.ItemId == awardid);

                XsiAwards entity = _context.XsiAwards.Where(predicate).FirstOrDefault();
                if (entity != null)
                {
                    var predicateSubAward = PredicateBuilder.True<XsiSubAwards>();

                    predicateSubAward = predicateSubAward.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateSubAward = predicateSubAward.And(i => i.ItemId == subawardid);
                    predicateSubAward = predicateSubAward.And(i => i.AwardId == awardid);

                    XsiSubAwards entity1 = _context.XsiSubAwards.Where(predicateSubAward).FirstOrDefault();
                    if (entity1 != null && entity1.Name != null && !string.IsNullOrEmpty(entity1.Name))
                        return entity1.Name;

                    return entity.Name;
                }
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiAwards>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.ItemId == awardid);

                XsiAwards entity = _context.XsiAwards.Where(predicate).FirstOrDefault();
                if (entity != null)
                {
                    var predicateSubAward = PredicateBuilder.True<XsiSubAwards>();

                    predicateSubAward = predicateSubAward.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicateSubAward = predicateSubAward.And(i => i.ItemId == subawardid);
                    predicateSubAward = predicateSubAward.And(i => i.AwardId == awardid);

                    XsiSubAwards entity1 = _context.XsiSubAwards.Where(predicateSubAward).FirstOrDefault();
                    if (entity1 != null && entity1.NameAr != null && !string.IsNullOrEmpty(entity1.NameAr))
                        return entity1.NameAr;

                    return entity.NameAr;
                }
            }
            return string.Empty;
        }
    }
}

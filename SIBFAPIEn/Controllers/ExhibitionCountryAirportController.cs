﻿using LinqKit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionCountryAirportController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionCountryAirportController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/ExhibitionCountryAirport/5
        [HttpGet("{countryid}")]
        public async Task<ActionResult<IEnumerable<SubCategoryDTO>>> GetXsiExhibitionCountryAirport(int countryid)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionCountryAirport>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.CountryId == countryid);

                var List = _context.XsiExhibitionCountryAirport.AsQueryable().Where(predicate).OrderBy(x => x.AirportName).Select(i => new SubCategoryDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.AirportName,
                    //CategoryTitle = i.Country.CountryName,
                    //CategoryId = i.Country.CountryId
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionCountryAirport>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.CountryId == countryid);

                var List = _context.XsiExhibitionCountryAirport.AsQueryable().Where(predicate).OrderBy(x => x.AirportNameAr).Select(i => new SubCategoryDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.AirportNameAr,
                    //CategoryTitle = i.Country.CountryName,
                    //CategoryId = i.Country.CountryId
                }).ToListAsync();

                return await List;
            }
        }

        // GET: api/ExhibitionCountryAirport/5/1
        [HttpGet("{countryid}/{id}")]
        public async Task<ActionResult<SubCategoryDTO>> GetXsiExhibitionCountryAirport(int countryid, long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var subCategory = await _context.XsiExhibitionCountryAirport.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes))
                  .Select(i => new SubCategoryDTO()
                  {
                      ItemId = i.ItemId,
                      Title = i.AirportName,
                      //CategoryTitle = i.Country.CountryName,
                      //CategoryId = i.Country.CountryId
                  }).FirstOrDefaultAsync();

                if (subCategory == null)
                {
                    return NotFound();
                }

                return subCategory;
            }
            else
            {
                var subCategory = await _context.XsiExhibitionCountryAirport.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes))
                   .Select(i => new SubCategoryDTO()
                   {
                       ItemId = i.ItemId,
                       Title = i.AirportNameAr,
                       //CategoryTitle = i.Country.CountryName,
                       //CategoryId = i.Country.CountryId
                   }).FirstOrDefaultAsync();

                if (subCategory == null)
                {
                    return NotFound();
                }

                return subCategory;
            }
        }

        private bool XsiExhibitionCountryAirportExists(long id)
        {
            return _context.XsiExhibitionCountryAirport.Any(e => e.ItemId == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Xsi.ServicesLayer;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionCountry1Controller : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionCountry1Controller(sibfnewdbContext context)
        {
            _context = context;
        }
        // GET: api/ExhibitionCountry
        [HttpGet]
        public async Task<ActionResult<dynamic>> GetXsiExhibitionCountry()
        {
            using (ExhibitionCountryService ExhibitionCountryService = new ExhibitionCountryService())
            {
                try
                {
                    List<CategoryDTO> List = new List<CategoryDTO>();
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    if (LangId == 1)
                        List = ExhibitionCountryService.GetExhibitionCountry().Select(i => new CategoryDTO() { ItemId = i.CountryId, Title = i.CountryName }).ToList();
                    else
                        List = ExhibitionCountryService.GetExhibitionCountry().Select(i => new CategoryDTO() { ItemId = i.CountryId, Title = i.CountryNameAr }).ToList();
                    return Ok(List);
                }
                catch (Exception ex)
                {
                    return Ok("Exception: Something went wrong. Please retry again later.");
                }
            }
        }
    }
}

﻿using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xsi.ServicesLayer;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PPBooksInfoController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly IEmailSender _emailSender;

        public PPBooksInfoController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IEmailSender emailSender)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _emailSender = emailSender;
        }
        #region Get Methods
        [HttpGet]
        [Route("{registeredid}")]
        public async Task<ActionResult<dynamic>> GetPPBooksInfo(long registeredid, long? ppsid)
        {
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    var ppUrl = CheckProfessionalProgram(memberId, LangId);
                    if (string.IsNullOrEmpty(ppUrl))
                    {
                        var model = LoadContent(memberId, LangId, registeredid);
                        return Ok(model);
                    }
                    else
                        return Ok(ppUrl);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiCareers action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
        #endregion
        #region Post Methods

        #endregion
        #region Helper Methods
        string CheckProfessionalProgram(long memberId, long langId)
        {
            string ReturnUrl = "";
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                XsiExhibitionProfessionalProgramRegistration entity = MethodFactory.VerifyProfessionalProgramRegistration(memberId);
                if (entity != default(XsiExhibitionProfessionalProgramRegistration))
                {
                    if (entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.New))
                        ReturnUrl = _appCustomSettings.ServerAddressNew + "en/ProfessionalProgram/" + entity.StepId.ToString() + "/" + Convert.ToInt64(EnumWebsiteId.SIBF);
                    else if (entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Pending))
                        ReturnUrl = _appCustomSettings.ServerAddressNew + "en/ProfessionalProgram/" + entity.StepId.ToString() + "/" + Convert.ToInt64(EnumWebsiteId.SIBF);
                    else if (entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Rejected))
                        ReturnUrl = _appCustomSettings.ServerAddressNew + "en/ProfessionalProgram/" + entity.StepId.ToString() + "/" + Convert.ToInt64(EnumWebsiteId.SIBF);
                }
                else if (MethodFactory.IsFirstimeUserForPP(memberId))
                    ReturnUrl = _appCustomSettings.ServerAddressNew + "en/ProfessionalProgram/-2" + "/" + Convert.ToInt64(EnumWebsiteId.SIBF);
                else
                    ReturnUrl = _appCustomSettings.ServerAddressNew + "en/ProfessionalProgram/2" + "/" + Convert.ToInt64(EnumWebsiteId.SIBF);
            }
            return ReturnUrl;
        }
        PPBookInfoDTO LoadContent(long memberId, long langId, long itemId)
        {
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                PPBookInfoDTO model = new PPBookInfoDTO();
                #region GET SIBF Publishers Conference REGISTRATION ID FOR LOGGED IN USER
                XsiExhibitionProfessionalProgram ppEntity = MethodFactory.GetProfessionalProgram();
                XsiExhibitionProfessionalProgramRegistration where2 = new XsiExhibitionProfessionalProgramRegistration();
                where2.MemberId = memberId;
                where2.IsActive = EnumConversion.ToString(EnumBool.Yes);
                if (ppEntity != null)
                    where2.ProfessionalProgramId = ppEntity.ProfessionalProgramId;
                where2.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Approved);
                XsiExhibitionProfessionalProgramRegistration entityPPRegistration = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistration(where2).FirstOrDefault();
                //if (entityPPRegistration != null)
                //{
                //    Notification1.UCRegisteredId = entityPPRegistration.ItemId;
                //    hfRequestId.Value = entityPPRegistration.ItemId.ToString();
                //}
                #endregion

                XsiExhibitionProfessionalProgramRegistration entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(itemId);
                if (entity != null)
                {
                    //ahref3.Attributes.Add("data-val", itemId.ToString());
                    //ahrefProfile.HRef = "/en/ProfessionalProfile/" + itemId;
                    //ahrefBooks.HRef = "/en/PPBooksInfo/" + itemId;
                    //ahref3.HRef = "/en/PPBooksInfo/" + itemId + "#ScheduleMeetup";
                    //ahref3.Visible = MethodFactory.IsAppointmentDateAvailable(entity.ProfessionalProgramId.Value);
                    model.Image = AssignAvatar(entity.FileName, entity.Title);
                    if (entity.FileName1 != null && !string.IsNullOrEmpty(entity.FileName1))
                        model.FileName1 = "/content/uploads/PPRegistration/Document/" + entity.FileName1;

                    if (entity.BooksUrl != null && !string.IsNullOrEmpty(entity.BooksUrl))
                    {
                        if (entity.BooksUrl.IndexOf("http") > -1)
                            model.BooksURL = entity.BooksUrl;
                        else
                            model.BooksURL = "http://" + entity.BooksUrl;
                    }

                    string strLastName = string.Empty;
                    if (entity.FirstName != null && entity.FirstName != string.Empty)
                        model.Name = entity.FirstName;
                    else if (entity.FirstNameAr != null && entity.FirstNameAr != string.Empty)
                        model.Name = entity.FirstNameAr;
                    if (entity.LastName != null && entity.LastName != string.Empty)
                        strLastName = " " + entity.LastName;
                    else if (entity.LastNameAr != null && entity.LastNameAr != string.Empty)
                        strLastName = " " + entity.LastNameAr;
                    model.Name += strLastName;
                    if (entity.CompanyName != null && entity.CompanyName != string.Empty)
                        model.CompanyName = entity.CompanyName;
                    else if (entity.CompanyNameAr != null && entity.CompanyNameAr != string.Empty)
                        model.CompanyName = entity.CompanyNameAr;
                    ProfessionalProgramBookService ProfessionalProgramBookService = new ProfessionalProgramBookService();
                    #region Book Info
                    XsiExhibitionProfessionalProgramBook where = new XsiExhibitionProfessionalProgramBook();
                    where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    where.ProfessionalProgramRegistrationId = itemId;
                    List<XsiExhibitionProfessionalProgramBook> ProfessionalProgramBookList = ProfessionalProgramBookService.GetProfessionalProgramBook(where);
                    if (ProfessionalProgramBookList.Count() > 0)
                    {
                        var booksList = ProfessionalProgramBookList.Select(x => new MyBooks
                        {
                            ItemId = x.ItemId,
                            AuthorName = x.AuthorName,
                            Title = x.Title,
                            GenreId = x.GenreId,
                            Genre = MethodFactory.GetGenreName(x.GenreId, langId),
                            OtherGenre = x.OtherGenre,
                            Language = MethodFactory.GetLanguage(x.ItemId, langId),
                            IsRightsAvailable = x.IsRightsAvailable,
                            IsOwnRights = x.IsOwnRights,
                            RightsOwner = x.RightsOwner,
                            Synopsis = x.Synopsis,
                            FileName = _appCustomSettings.UploadsCMSPath + "/PPRegistration/Cover/" + x.FileName
                        }).ToList();
                        model.BooksList = booksList;
                    }
                    #endregion
                }
                return model;
            }
        }
        string AssignAvatar(string FileName, string Title)
        {
            string LocationMapPath = _appCustomSettings.UploadsCMSPath + "/PPRegistration/";
            string imagePath = "";
            if (!string.IsNullOrEmpty(FileName))
            {
                if (System.IO.File.Exists(LocationMapPath + "Avatar/" + FileName))
                    imagePath = LocationMapPath + "Avatar/" + FileName;
                else
                {
                    imagePath = "/Content/assets/images/avatarmale.png";
                    if (!string.IsNullOrEmpty(Title))
                        if (Title == "F")
                            imagePath = "/Content/assets/images/avatarfemale.png";
                }
            }
            else
            {
                imagePath = "/Content/assets/images/avatarmale.png";
                if (!string.IsNullOrEmpty(Title))
                    if (Title == "F")
                        imagePath = "/Content/assets/images/avatarfemale.png";
            }
            return imagePath;
        }
        #endregion
    }
}

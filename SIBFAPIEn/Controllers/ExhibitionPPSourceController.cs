﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionPPSourceController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionPPSourceController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/PPSource
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DropdownDataDTO>>> GetPPSource()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgramSource>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionProfessionalProgramSource.AsQueryable().Where(predicate).OrderBy(x => x.Title.Trim()).Select(x => new DropdownDataDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title,
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgramSource>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionProfessionalProgramSource.AsQueryable().Where(predicate).OrderBy(x => x.Title.Trim()).Select(x => new DropdownDataDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title,
                }).ToListAsync();

                return await List;
            }
        }

        [HttpGet]
        [Route("MultiSelect")]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetMultiPPSource()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgramSource>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionProfessionalProgramSource.AsQueryable().Where(predicate).OrderBy(x => x.Title.Trim()).Select(x => new
                {
                    value = x.ItemId,
                    label = x.Title,
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgramSource>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionProfessionalProgramSource.AsQueryable().Where(predicate).OrderBy(x => x.Title.Trim()).Select(x => new
                {
                    value = x.ItemId,
                    label = x.TitleAr,
                }).ToListAsync();

                return await List;
            }
        }

        // GET: api/StaffGuestType/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DropdownDataDTO>> GetPPSource(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            var xsiItem = await _context.XsiExhibitionProfessionalProgramSource.FindAsync(id);

            if (xsiItem == null)
            {
                return NotFound();
            }

            DropdownDataDTO itemDTO = new DropdownDataDTO()
            {
                ItemId = xsiItem.ItemId,
                Title = LangId == 1 ? xsiItem.Title : xsiItem.TitleAr,
            };
            return itemDTO;
        }

        private bool PPSourceExists(long id)
        {
            return _context.XsiExhibitionProfessionalProgramSource.Any(e => e.ItemId == id);
        }
    }
}

﻿using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Linq;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitorAgencySIBFController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;
        public ExhibitorAgencySIBFController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings, IEmailSender emailSender, sibfnewdbContext context)
        {
            _logger = logger;
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        #region Get
        [HttpGet]
        public ActionResult<dynamic> GetExhibitorAgency()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                string Message = "";

                var List = _context.vwExhibitorAgencySIBF.ToList();
                return Ok(List);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetExhibitorAgency action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
        #endregion
    }
}

﻿using Contracts;
using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class TMapMobileController : Controller
    {
        private IMemoryCache _cache;
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private string UploadsCMSPath = "https://xsi.sibf.com/content/uploads"; //replace with UploadsCMSPath
        #region Cache Properties
        public List<XsiExhibition> cachedExhibition { get; set; }
        public List<XsiExhibitionMemberApplicationYearly> cachedExhibitionMemberApplicationYearly { get; set; }
        public List<XsiGuests> cachedGuest { get; set; }
        public List<XsiExhibitionBooks> cachedBooks { get; set; }
        public List<XsiExhibitionBookParticipating> cachedBookParticipating { get; set; }
        public List<XsiExhibitionBookAuthor> cachedBookAuthors { get; set; }
        public List<XsiExhibitionAuthor> cachedAuthors { get; set; }
        public List<XsiExhibitionCategory> cachedExhibitionCategory { get; set; }
        public List<XsiEvent> cachedEvents { get; set; }
        public List<XsiEventDate> cachedEventDate { get; set; }
        public List<XsiExhibitionExhibitorDetails> cachedExhibitionExhibitorDetails { get; set; }
        public List<XsiEventGuest> cachedEventGuest { get; set; }
        public static List<XsiEventCategory> cachedEventCategory { get; set; }
        public static List<XsiEventSubCategory> cachedEventSubCategory { get; set; }

        #endregion
        public TMapMobileController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IMemoryCache memoryCache)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _cache = memoryCache;
        }

        // GET api/<controller>/5
        [HttpGet("{webid}")]
        public ActionResult<dynamic> FetchExhibitors(long webId)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                var exhibitors = GetExhibitors(LangId, webId);

                return exhibitors;

            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside FetchDashboardItems action: {ex.Message}");
                return Ok("Something went wrong. Please try again later.");

            }
        }

        #region Exhibitors 

        private List<TMapDTO> GetExhibitors(long langId, long webId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (cachedExhibitionMemberApplicationYearly == null)
                    cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

                if (cachedExhibitionExhibitorDetails == null)
                    cachedExhibitionExhibitorDetails = CacheKeys.GetExhibitionExhibitorDetails(_cache);

                List<TMapDTO> result = new List<TMapDTO>();
                #region Current Exhibition
                var exhibition = MethodFactory.GetActiveExhibitionNew(webId, _cache);
                #endregion
                if (exhibition != null)
                {
                    var predicate = PredicateBuilder.New<XsiExhibitionMemberApplicationYearly>();
                    predicate = predicate.And(i => (i.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved) || i.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval)));
                    predicate = predicate.And(i => i.MemberRoleId == 1);
                    predicate = predicate.And(i => i.ExhibitionId == exhibition.ExhibitionId);
                    // predicate = predicate.And(i => i.FileName != null && i.FileName.Length > 0);

                    //SibTitle: ContactPerson 
                    if (langId == 1)
                    {
                        result = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).Select(i => new TMapDTO
                        {
                            Id = i.MemberExhibitionYearlyId,
                            Title = i.PublisherNameEn,
                            SubTitle = i.ContactPersonName,
                            FeatureId = GetFeatureIdOrBoothId(1, i.MemberExhibitionYearlyId),
                            BoothId = GetFeatureIdOrBoothId(2, i.MemberExhibitionYearlyId),
                            //BoothId= i.XsiExhibitionExhibitorDetails.FirstOrDefault() != null ? i.XsiExhibitionExhibitorDetails.FirstOrDefault().BoothSectionId : "-",
                            // Caption = cachedExhibitionExhibitorDetails.AsQueryable().Where(p => p.MemberExhibitionYearlyId == p.MemberExhibitionYearlyId).FirstOrDefault() != null ? cachedExhibitionExhibitorDetails.AsQueryable().Where(p => p.MemberExhibitionYearlyId == p.MemberExhibitionYearlyId).FirstOrDefault().StandCode : "CCC",
                            Icon = _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/" + i.FileName,
                        }).OrderByDescending(i => i.Id).ToList();
                        //}).OrderBy(m => m.Title.StartsWith("TAMER") ? 0 : 1).ThenByDescending(i => i.Id).Take(10).ToList();

                        return result;
                    }
                    else
                    {
                        result = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).Select(i => new TMapDTO
                        {
                            Id = i.MemberExhibitionYearlyId,
                            Title = i.PublisherNameAr ?? i.PublisherNameEn,
                            SubTitle = i.ContactPersonNameAr ?? i.ContactPersonName,
                            FeatureId = GetFeatureIdOrBoothId(1, i.MemberExhibitionYearlyId),
                            BoothId = GetFeatureIdOrBoothId(2, i.MemberExhibitionYearlyId),
                            // Caption = cachedExhibitionExhibitorDetails.AsQueryable().Where(p => p.MemberExhibitionYearlyId == p.MemberExhibitionYearlyId).FirstOrDefault() != null ? cachedExhibitionExhibitorDetails.AsQueryable().Where(p => p.MemberExhibitionYearlyId == p.MemberExhibitionYearlyId).FirstOrDefault().StandCode : "CCC",
                            Icon = _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/" + i.FileName,
                        }).OrderByDescending(i => i.Id).ToList();
                        //}).OrderBy(m => m.Title.StartsWith("TAMER") ? 0 : 1).ThenByDescending(i => i.Id).Take(10).ToList();
                    }

                    return result;
                }
                return new List<TMapDTO>();
            }
        }
        string GetFeatureIdOrBoothId(int type, long exhibitorid)
        {
            if (cachedExhibitionExhibitorDetails == null)
                cachedExhibitionExhibitorDetails = CacheKeys.GetExhibitionExhibitorDetails(_cache);

            var details = cachedExhibitionExhibitorDetails.Where(p => p.MemberExhibitionYearlyId == exhibitorid).OrderByDescending(i => i.ExhibitorDetailsId).FirstOrDefault();
            if (details != null)
            {
                if (type == 1)
                    return details.FeatureId ?? "-";
                else
                    return details.StandCode ?? "-";
            }
            return "-";
        }
        #endregion
    }
}

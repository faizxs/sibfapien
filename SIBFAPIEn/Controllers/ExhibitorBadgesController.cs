﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using SIBFAPIEn.Utility;
using SIBFAPIEn.DTO;
using Microsoft.AspNetCore.Cors;
using LinqKit;
using Microsoft.Extensions.Options;
using Xsi.ServicesLayer;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;
namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ExhibitorBadgesController : ControllerBase
    {
        EnumResultType XsiResult { get; set; }
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public ExhibitorBadgesController(IOptions<AppCustomSettings> appCustomSettings, IEmailSender emailSender, sibfnewdbContext context)
        {
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }


        [HttpGet("{websiteid}/{memberId}")]
        // [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> GetExhibitor(long websiteid, long memberId)
        {
            MessageDTO dto = new MessageDTO();
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            long memberid = User.Identity.GetID();

            if (memberid == memberId)
            {
                var exhibition = MethodFactory.GetActiveExhibition(websiteid, _context);
                if (exhibition != null)
                {
                    string strInitialApprove = EnumConversion.ToString(EnumExhibitorStatus.InitialApproval);
                    string strApprove = EnumConversion.ToString(EnumExhibitorStatus.Approved);
                    var exhibitor = _context.XsiExhibitionMemberApplicationYearly.Where(p => p.MemberId == memberid && p.ExhibitionId == exhibition.ExhibitionId && (p.Status == strInitialApprove || p.Status == strApprove)).FirstOrDefault();
                    if (exhibitor != null)
                    {
                        ExhibitorDetailsBadgeDTO dto1 = new ExhibitorDetailsBadgeDTO();
                        dto1.ExhibitorId = exhibitor.MemberExhibitionYearlyId;
                        dto1.PublisherName = exhibitor.PublisherNameEn;
                        dto1.PublisherNameAr = exhibitor.PublisherNameAr;
                        dto1.PublisherEmail = exhibitor.Email;
                        dto1.PublisherPhone = exhibitor.Phone.Replace("$", "");
                        return Ok(new { MessageTypeResponse = "Success", ExhibitorDetailsBadgeDTO = dto1 });
                    }
                    else
                    {
                        dto.MessageTypeResponse = "Error";
                        if (LangId == 1)
                            dto.Message = "Exhibitor not found";
                        else
                            dto.Message = "Exhibitor not found";
                        return Ok(dto);
                    }
                }
                else
                {
                    dto.MessageTypeResponse = "Error";
                    if (LangId == 1)
                        dto.Message = "No active exhibition available";
                    else
                        dto.Message = "No active exhibition available";
                    return Ok(dto);
                }
            }
            else
            {
                dto.MessageTypeResponse = "Error";
                if (LangId == 1)
                    dto.Message = "Member login expired or token mismatch";
                else
                    dto.Message = "Member login expired or token mismatch";
                return Ok(dto);
            }
        }
        // POST: api/ExhibitorBadges

        [HttpPost]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> PostExhibitorBadge(ExhibitorBadgeDTO exhibitorBadgeDTO)
        {
            MessageDTO dto = new MessageDTO();
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            long memberid = User.Identity.GetID();

            if (memberid == exhibitorBadgeDTO.MemberId)
            {
                var pdfPath = _appCustomSettings.UploadsPhysicalPath + "\\ExhibitorBadge\\" + exhibitorBadgeDTO.pdfFileName;
                XsiResult = SendEmailToUser(exhibitorBadgeDTO, LangId, pdfPath);
                if (XsiResult == EnumResultType.Success || XsiResult == EnumResultType.EmailSent)
                {
                    if (System.IO.File.Exists(pdfPath))
                        System.IO.File.Delete(pdfPath);

                    dto.MessageTypeResponse = "Success";
                    if (LangId == 1)
                        dto.Message = "Form has been submitted successfully.";
                    else
                        dto.Message = "Form has been submitted successfully.";
                    return Ok(dto);
                }
                else
                {
                    dto.MessageTypeResponse = "Error";
                    if (LangId == 1)
                        dto.Message = "Error while submitting form. Please try again later after sometime.";
                    else
                        dto.Message = "Error while submitting form. Please try again later after sometime.";
                    return Ok(dto);
                }
            }
            else
            {
                dto.MessageTypeResponse = "Error";
                if (LangId == 1)
                    dto.Message = "Member login expired or token mismatch";
                else
                    dto.Message = "Member login expired or token mismatch";
                return Ok(dto);
            }
        }

        [HttpPost, DisableRequestSizeLimit]
        [Route("UploadImage")]
        [EnableCors("AllowOrigin")]
        public IActionResult Upload()
        {
            UploadFileMessageDTO dto = new UploadFileMessageDTO();
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long memberid = User.Identity.GetID();


                var file = Request.Form.Files[0];
                var folderName = Path.Combine(_appCustomSettings.UploadsPhysicalPath, "ExhibitorBadge");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                if (file.Length > 0)
                {
                    var fileName = string.Format("{0}_{1}{2}{3}", LangId, memberid, MethodFactory.GetRandomNumber(), ".pdf");
                    //var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    dto.FileName = fileName;
                    dto.MessageTypeResponse = "Success";
                    dto.PhysicalPath = dbPath;
                    dto.ReturnURL = _appCustomSettings.UploadsCMSPath + "/ExhibitorBadge/" + fileName;
                    return Ok(dto);
                }
                else
                {
                    dto.MessageTypeResponse = "Error";
                    dto.Message = "File length is empty.";
                    return Ok(dto);
                    //return BadRequest();
                }
            }
            catch (Exception ex)
            {
                dto.MessageTypeResponse = "Error";
                dto.Message = "Something went wrong please try again later.";
                return Ok(dto);
                //return StatusCode(500, $"Internal server error: {ex}");
            }
        }

        private EnumResultType SendEmailToUser(ExhibitorBadgeDTO exhibitor, long langid, string pdfPath)
        {
            var exhibitorentity = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == exhibitor.ExhibitorId).FirstOrDefault();
            var exhibition = _context.XsiExhibition.Where(i => i.ExhibitionId == exhibitorentity.ExhibitionId).First();

            #region Send  Email to User
            StringBuilder body = new StringBuilder();
            string strEmailContentBody = "";
            string strEmailContentEmail = "";
            string strEmailContentSubject = "";
            string strEmailContentAdmin = "";
            #region Email Content
            if (exhibition.WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
            {
                var scrfEmailContent = _context.XsiScrfemailContent.Where(x => x.ItemId == 20135).FirstOrDefault();//40041
                if (scrfEmailContent != null)
                {
                    if (langid == 1)
                    {
                        if (scrfEmailContent.Body != null)
                            strEmailContentBody = scrfEmailContent.Body;
                        if (scrfEmailContent.Email != null)
                            strEmailContentEmail = scrfEmailContent.Email;
                        if (scrfEmailContent.Subject != null)
                            strEmailContentSubject = scrfEmailContent.Subject;
                    }
                    else
                    {
                        if (scrfEmailContent.BodyAr != null)
                            strEmailContentBody = scrfEmailContent.BodyAr;
                        if (scrfEmailContent.Email != null)
                            strEmailContentEmail = scrfEmailContent.Email;
                        if (scrfEmailContent.SubjectAr != null)
                            strEmailContentSubject = scrfEmailContent.SubjectAr;
                    }

                    strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                }
            }
            else
            {
                var emailContent = _context.XsiEmailContent.Where(x => x.ItemId == 20135).FirstOrDefault();
                if (emailContent != null)
                {
                    if (langid == 1)
                    {
                        if (emailContent.Body != null)
                            strEmailContentBody = emailContent.Body;
                        if (emailContent.Email != null)
                            strEmailContentEmail = emailContent.Email;
                        if (emailContent.Subject != null)
                            strEmailContentSubject = emailContent.Subject;
                    }
                    else
                    {
                        if (emailContent.BodyAr != null)
                            strEmailContentBody = emailContent.BodyAr;
                        if (emailContent.Email != null)
                            strEmailContentEmail = emailContent.Email;
                        if (emailContent.SubjectAr != null)
                            strEmailContentSubject = emailContent.SubjectAr;
                    }
                    strEmailContentAdmin = _appCustomSettings.AdminName;
                }
            }
            #endregion
            body.Append(BindEmailContent(langid, strEmailContentBody, exhibitor, _appCustomSettings.ServerAddressNew, exhibition.WebsiteId ?? 1));
            //body.Replace("$$ExhibitorName$$", exhibitorName);

            List<string> attachment = new List<string>();
            if (!string.IsNullOrEmpty(pdfPath))
                attachment.Add(pdfPath);

            XsiResult = _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, exhibitor.Email, strEmailContentSubject, body.ToString(), attachment);
            #endregion
            return XsiResult;
        }
        private StringBuilder BindEmailContent(long languageId, string bodyContent, ExhibitorBadgeDTO entitydto, string ServerAddress, long websiteId)
        {
            string str = string.Empty;
            string exhibitionmembername = string.Empty;
            string scrfURL = _appCustomSettings.ServerAddressSCRF;
            string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
            string EmailContent = string.Empty;

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            if (languageId == 1)
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRF.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
            }
            else
            {
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRF.html";
                else
                    EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
            }
            StringBuilder body = new StringBuilder();

            if (System.IO.File.Exists(EmailContent))
            {
                body.Append(System.IO.File.ReadAllText(EmailContent));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);
            str = MethodFactory.GetExhibitionDetails(websiteId, languageId, _context);
            string[] strArray = str.Split(',');
            body.Replace("$$exhibitionno$$", strArray[0].ToString());
            body.Replace("$$exhibitiondate$$", strArray[1].ToString());
            body.Replace("$$exhyear$$", strArray[2]);

            body.Replace(swidth, sreplacewidth);

            body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
            body.Replace("$$ServerAddress$$", ServerAddress);
            if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                body.Replace("$$ServerAddressSCRF$$", scrfURL);

            if (entitydto != null)
            {
                if (languageId == 1)
                {
                    exhibitionmembername = entitydto.Name;
                }
                else
                {
                    exhibitionmembername = entitydto.NameAr;

                    if (string.IsNullOrEmpty(exhibitionmembername))
                        exhibitionmembername = entitydto.Name;
                }
                body.Replace("$$Name$$", exhibitionmembername);
            }
            return body;
        }
    }
}

﻿using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using SIBFAPIEn.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PageMenuNewController : ControllerBase
    {
        private IMemoryCache _cache;
        private readonly sibfnewdbContext _context;
        public List<XsiPagesContentNew> cachedPagesContentNew { get; set; }
        public PageMenuNewController(sibfnewdbContext context, IMemoryCache memoryCache)
        {
            _context = context;
            _cache = memoryCache;
        }

        // GET: api/PageMenuNew
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PageMenuNewDTO>>> GetXsiPageMenuNew()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            if (LangId == 1)
            {
                //LanguageId = langId, IsActive = "Y"

                var predicate = PredicateBuilder.True<XsiPageMenuNew>();

                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                List<XsiPageMenuNew> Menu = _context.XsiPageMenuNew.AsQueryable().Where(predicate).OrderBy(o => o.SortOrder).ToList();

                var List = CreateMenu(null, Menu, LangId);

                return await List.ToAsyncEnumerable().ToList();
            }
            else
            {
                //LanguageId = langId, IsActive = "Y"

                var predicate = PredicateBuilder.True<XsiPageMenuNew>();

                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                List<XsiPageMenuNew> Menu = _context.XsiPageMenuNew.AsQueryable().Where(predicate).OrderBy(o => o.SortOrder).ToList();

                var List = CreateMenu(null, Menu, LangId).OrderBy(x => x.SortOrder);

                return await List.ToAsyncEnumerable().ToList();
            }
        }


        // GET: api/PageMenuNew
        [HttpGet]
        [Route("MediaCenterMenu")]
        public async Task<ActionResult<IEnumerable<MediaCenterDTO>>> GetMediaCenterMenu()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            List<MediaCenterDTO> mediaCenter = new List<MediaCenterDTO>();

            var predicate = PredicateBuilder.True<XsiPageMenuNew>();

            if (LangId == 1)
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            else
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
            predicate = predicate.And(i => i.ParentId == 13);

            if (LangId == 1)
                mediaCenter = _context.XsiPageMenuNew.AsQueryable().Where(predicate).OrderBy(o => o.SortOrder).Select(i => new MediaCenterDTO { ItemId = i.ItemId, Title = i.Title }).ToList();
            else
                mediaCenter = _context.XsiPageMenuNew.AsQueryable().Where(predicate).OrderBy(o => o.SortOrder).Select(i => new MediaCenterDTO { ItemId = i.ItemId, Title = i.TitleAr }).ToList();

            predicate = PredicateBuilder.True<XsiPageMenuNew>();
            if (LangId == 1)
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            else
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

            predicate = predicate.And(i => i.ItemId == 20354);//20348

            var publisherNews = _context.XsiPageMenuNew.AsQueryable().Where(predicate).FirstOrDefault();

            if (publisherNews != null)
            {
                if (LangId == 1)
                    mediaCenter.Add(new MediaCenterDTO() { ItemId = 20354, Title = publisherNews.Title }); //20348
                else
                    mediaCenter.Add(new MediaCenterDTO() { ItemId = 20354, Title = publisherNews.TitleAr });//20348
            }

            if (LangId == 1)
            {
                foreach (var item in mediaCenter)
                {
                    if (item.ItemId == 23)
                        item.URL = "en/news";
                    else if (item.ItemId == 25)
                        item.URL = "en/album";
                    else if (item.ItemId == 27)
                        item.URL = "en/videoalbums";
                    else if (item.ItemId == 35)
                        item.URL = "en/PressRelease";
                    else if (item.ItemId == 37)
                        item.URL = "en/NewsLetterSubscribers";
                    else if (item.ItemId == 39)
                        item.URL = "en/events";
                    else if (item.ItemId == 20354 || item.ItemId == 20348) //20343
                        item.URL = "en/publishernews";
                }
            }
            else
            {
                foreach (var item in mediaCenter)
                {
                    if (item.ItemId == 23)
                        item.URL = "ar/news";
                    else if (item.ItemId == 25)
                        item.URL = "ar/album";
                    else if (item.ItemId == 27)
                        item.URL = "ar/videoalbums";
                    else if (item.ItemId == 35)
                        item.URL = "ar/PressRelease";
                    else if (item.ItemId == 37)
                        item.URL = "ar/NewsLetterSubscribers";
                    else if (item.ItemId == 39)
                        item.URL = "ar/events";
                    else if (item.ItemId == 20354 || item.ItemId == 20348) //20343
                        item.URL = "ar/publishernews";
                }
            }
            return await mediaCenter.ToAsyncEnumerable().ToList();
        }

        // GET: api/PageMenuNew/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PageMenuNewDTO>> GetXsiPageMenuNew(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            var xsiPageMenuNew = await _context.XsiPageMenuNew.FindAsync(id);

            if (xsiPageMenuNew == null)
            {
                return NotFound();
            }
            var xsiPageMenuNewList = await _context.XsiPageMenuNew.FindAsync(id).ToAsyncEnumerable().ToList();
            PageMenuNewDTO itemDTO;
            if (LangId == 1)
            {

                itemDTO = new PageMenuNewDTO()
                {
                    ItemId = xsiPageMenuNew.ItemId,
                    Title = xsiPageMenuNew.Title,
                    ParentId = xsiPageMenuNew.ParentId,
                    PageUrl = xsiPageMenuNew.PageUrl,
                    IsExternal = xsiPageMenuNew.IsExternal,
                    PageContentId = xsiPageMenuNew.ParentId ?? -1,
                    // other properties
                    Children = CreateMenu(xsiPageMenuNew.ParentId, xsiPageMenuNewList, LangId).ToList()
                };
            }
            else
            {
                itemDTO = new PageMenuNewDTO()
                {
                    ItemId = xsiPageMenuNew.ItemId,
                    Title = xsiPageMenuNew.TitleAr,
                    ParentId = xsiPageMenuNew.ParentId,
                    PageUrl = xsiPageMenuNew.PageUrlAr,
                    IsExternal = xsiPageMenuNew.IsExternal,
                    PageContentId = xsiPageMenuNew.ParentId ?? -1,
                    // other properties
                    Children = CreateMenu(xsiPageMenuNew.ParentId, xsiPageMenuNewList, LangId).ToList()
                };
            }
            return Ok(itemDTO);
        }
        private IEnumerable<PageMenuNewDTO> CreateMenu(long? parentid, List<XsiPageMenuNew> menuSource, long langId)
        {

            if (langId == 1)
            {
                return menuSource.Where(x => x.ParentId == parentid).Select(i => new PageMenuNewDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.Title,
                    ParentId = i.ParentId,
                    PageUrl = GetURLforMenu(i.PageUrl),
                    IsExternal = i.IsExternal,
                    PageContentId = i.ParentId ?? -1,
                    SortOrder = i.SortOrder,
                    // other properties
                    Children = CreateMenu(i.ItemId, menuSource, langId).ToList()
                });
            }
            else
            {
                return menuSource.Where(x => x.ParentId == parentid).Select(i => new PageMenuNewDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.TitleAr,
                    ParentId = i.ParentId,
                    PageUrl = GetURLforMenu(i.PageUrlAr),
                    IsExternal = i.IsExternal,
                    PageContentId = i.ParentId ?? -1,
                    SortOrder = i.SortOrder,
                    // other properties
                    Children = CreateMenu(i.ItemId, menuSource, langId).ToList()
                });
            }
        }
        private string GetURLforMenu(string pageUrl)
        {
            cachedPagesContentNew = (List<XsiPagesContentNew>)_cache.Get<dynamic>(CacheKeys.PagesContentNew);
            if (cachedPagesContentNew == null)
                cachedPagesContentNew = CacheKeys.GetPagesContentNew(_cache);

            if (pageUrl == null || pageUrl == "#")
                return string.Empty;
            if (pageUrl.ToLower().IndexOf("content?id") > -1)
            {
                long contentId = 0;
                long.TryParse(pageUrl.ToLower().Split("content?id=")[1], out contentId);
                if (contentId > 0)
                {
                    var entity = cachedPagesContentNew.Where(i => i.ItemId == contentId).FirstOrDefault();
                    if (entity != null)
                    {
                        pageUrl = entity.PageTitle
                            .Replace(" ", "-")
                            .Replace("&-", "")
                            .Replace(":-", "-")
                            .Replace("---", "-");
                        pageUrl = pageUrl.ToLower();
                        return pageUrl;
                    }
                }
            }
            return pageUrl;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionRegionController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionRegionController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/ExhibitionRegion
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryDTO>>> GetXsiExhibitionRegion()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionRegion>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionRegion.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new CategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title,
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionRegion>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionRegion.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new CategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.TitleAr,
                }).ToListAsync();

                return await List;
            }
        }

        // GET: api/ExhibitionRegion/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDTO>> GetXsiExhibitionRegion(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            var xsiItem = await _context.XsiExhibitionRegion.FindAsync(id);

            if (xsiItem == null)
            {
                return NotFound();
            }

            CategoryDTO itemDTO = new CategoryDTO()
            {
                ItemId = xsiItem.ItemId,
                Title = LangId == 1 ? xsiItem.Title : xsiItem.TitleAr,
            };
            return itemDTO;
        }

        private bool XsiExhibitionRegionExists(long id)
        {
            return _context.XsiExhibitionRegion.Any(e => e.ItemId == id);
        }
    }
}

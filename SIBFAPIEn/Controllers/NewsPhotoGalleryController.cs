﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using SIBFAPIEn.DTO;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class NewsPhotosGalleryController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;
        public NewsPhotosGalleryController(IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }
        // GET: api/NewsPhotosGallery/5
        [HttpGet("{newsid}")]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetXsiNewsPhotosGallery(long newsid)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicateNewsPhotosGallery = PredicateBuilder.True<XsiNewsPhotoGallery>();

                predicateNewsPhotosGallery = predicateNewsPhotosGallery.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicateNewsPhotosGallery = predicateNewsPhotosGallery.And(i => i.NewsId == newsid);

                //  List<dynamic> PhotoList = new List<dynamic>();
                var PhotoList = _context.XsiNewsPhotoGallery.AsQueryable().Where(predicateNewsPhotosGallery).OrderBy(i => i.SortIndex)
                      .Select(i => new
                      {
                          ItemId = i.ItemId,
                          Title = i.Title,
                          SortIndex = i.SortIndex,
                          ImageName = _appCustomSettings.UploadsPath + "/NewsPhotoGallery/" + (i.ImageName ?? "book-image.jpg"),
                      }).ToList();

                return await PhotoList.ToAsyncEnumerable().ToList();
            }
            else
            {
                var predicateNewsPhotosGallery = PredicateBuilder.True<XsiNewsPhotoGallery>();

                predicateNewsPhotosGallery = predicateNewsPhotosGallery.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                predicateNewsPhotosGallery = predicateNewsPhotosGallery.And(i => i.NewsId == newsid);

                //  List<dynamic> PhotoList = new List<dynamic>();
                var PhotoList = _context.XsiNewsPhotoGallery.AsQueryable().Where(predicateNewsPhotosGallery).OrderBy(i => i.SortIndex)
                      .Select(i => new
                      {
                          ItemId = i.ItemId,
                          Title = i.TitleAr,
                          SortIndex = i.SortIndex,
                          ImageName = _appCustomSettings.UploadsPath + "/NewsPhotoGallery/" + (i.ImageName ?? "book-image.jpg"),
                      }).ToList();

                return await PhotoList.ToAsyncEnumerable().ToList();
            }
        }

        // GET: api/NewsPhotosGallery/5/1
        [HttpGet("{newsid}/{id}")]
        public async Task<ActionResult<dynamic>> GetXsiPhoto(long newsid, long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var newsphoto = await _context.XsiNewsPhotoGallery.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                if (newsphoto == null)
                {
                    return NotFound();
                }
                var itemDTO = new
                {
                    ItemId = newsphoto.ItemId,
                    Title = newsphoto.Title,
                    SortIndex = newsphoto.SortIndex,
                    ImageName = _appCustomSettings.UploadsPath + "/NewsPhotoGallery/" + (newsphoto.ImageName ?? "book-image.jpg"),
                };
                return itemDTO;
            }
            else
            {
                var newsphoto = await _context.XsiNewsPhotoGallery.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                if (newsphoto == null)
                {
                    return NotFound();
                }
                var itemDTO = new
                {
                    ItemId = newsphoto.ItemId,
                    Title = newsphoto.TitleAr,
                    SortIndex = newsphoto.SortIndex,
                    ImageName = _appCustomSettings.UploadsPath + "/NewsPhotoGallery/" + (newsphoto.ImageName ?? "book-image.jpg"),
                };
                return itemDTO;
            }
        }

        private bool XsiNewsPhotoExists(long id)
        {
            return _context.XsiNewsPhotoGallery.Any(e => e.ItemId == id);
        }
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitorsController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public ExhibitorsController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _context = context;
        }

        // GET: api/Exhibitors
        [HttpGet]
        [Route("{pageNumber}/{pageSize}")]
        public async Task<ActionResult<dynamic>> GetXsiExhibitor(int? pageNumber = 1, int? pageSize = 15)
        {
            try
            {
                if (MethodFactory.iSHomepageSectionAllowed(Convert.ToInt64(EnumHomePageSection.SectionThree)))
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    if (LangId == 1)
                    {
                        var List = await _context.XsiExhibitor.Where(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.ItemId).Select(i => new
                        {
                            ItemId = i.ItemId,
                            Title = i.Title,
                            Overview = i.Overview,
                            ImageName = _appCustomSettings.UploadsPath + "/ExhibitorNew/" + i.ImageName,
                            Url = i.Url
                        }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                        var dto = new { List, TotalCount = List.Count };
                        return dto;
                    }
                    else
                    {
                        var List = await _context.XsiExhibitor.Where(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.ItemId).Select(i => new
                        {
                            ItemId = i.ItemId,
                            Title = i.TitleAr,
                            Overview = i.OverviewAr,
                            ImageName = _appCustomSettings.UploadsPath + "/ExhibitorNew/" + i.ImageNameAr,
                            Url = i.Urlar
                        }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                        var dto = new { List, TotalCount = List.Count };
                        return dto;
                    }

                }
                return null;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiExhibitorss action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");

            }
        }

        // GET: api/Exhibitors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<dynamic>> GetXsiExhibitor(long id)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var xsiItem = await _context.XsiExhibitor.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                    if (xsiItem == null)
                    {
                        return NotFound();
                    }

                    dynamic itemDTO = new
                    {
                        ItemId = xsiItem.ItemId,
                        Title = xsiItem.Title,
                        Overview = xsiItem.Overview,
                        ImageName = _appCustomSettings.UploadsPath + "/ExhibitorNew/" + xsiItem.ImageName,
                        Url = xsiItem.Url
                    };
                    return itemDTO;
                }
                else
                {
                    var xsiItem = await _context.XsiExhibitor.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                    if (xsiItem == null)
                    {
                        return NotFound();
                    }

                    dynamic itemDTO = new
                    {
                        ItemId = xsiItem.ItemId,
                        Title = xsiItem.TitleAr,
                        Overview = xsiItem.OverviewAr,
                        ImageName = _appCustomSettings.UploadsPath + "/ExhibitorNew/" + xsiItem.ImageNameAr,
                        Url = xsiItem.Urlar
                    };
                    return itemDTO;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiExhibitorss action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");

            }
        }

        private bool XsiExhibitorExists(long id)
        {
            return _context.XsiExhibitor.Any(e => e.ItemId == id);
        }
    }
}

﻿using Contracts;
using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xsi.ServicesLayer;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class ListingMobileController : Controller
    {
        private IMemoryCache _cache;
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;

        private string UploadsCMSPath = "https://xsi.sibf.com/content/uploads"; //replace with UploadsCMSPath
        #region Cache Properties
        public List<XsiExhibition> cachedExhibition { get; set; }
        public List<XsiExhibitionMemberApplicationYearly> cachedExhibitionMemberApplicationYearly { get; set; }
        public List<XsiGuests> cachedGuest { get; set; }
        public List<XsiExhibitionBooks> cachedBooks { get; set; }
        public List<XsiExhibitionBookParticipating> cachedBookParticipating { get; set; }
        public List<XsiExhibitionBookAuthor> cachedBookAuthors { get; set; }
        public List<XsiExhibitionAuthor> cachedAuthors { get; set; }
        public List<XsiExhibitionCategory> cachedExhibitionCategory { get; set; }
        public List<XsiExhibitionExhibitorDetails> cachedExhibitionExhibitorDetails { get; set; }
        public List<XsiEvent> cachedEvents { get; set; }
        public List<XsiEventDate> cachedEventDate { get; set; }
        public List<XsiEventGuest> cachedEventGuest { get; set; }
        public static List<XsiEventCategory> cachedEventCategory { get; set; }
        public static List<XsiEventSubCategory> cachedEventSubCategory { get; set; }
        #endregion

        public ListingMobileController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IMemoryCache memoryCache)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _cache = memoryCache;
        }

        // GET api/<controller>/5
        [HttpGet("{webid}/{sectionid}/{pageNumber}/{pageSize}")]
        //[HttpGet("{webid}/{sectionid}")]
        public ActionResult<dynamic> GetList(long webId, int sectionId, int pageNumber = 1, int pageSize = 1000)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var itemsList = new List<ItemsDTO>();
                switch (sectionId)
                {
                    case 1:
                        itemsList = GetBooks(webId, LangId, pageNumber, pageSize);
                        break;
                    case 2:
                        itemsList = GetActivities(pageNumber, pageSize);
                        break;
                    case 3:
                        itemsList = GetXsiGuest(pageNumber, pageSize);
                        break;
                    case 4:
                        itemsList = GetExhibitors(webId, LangId, pageNumber, pageSize);
                        break;
                }
                return itemsList;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok("Something went wrong. Please try again later.");

            }
        }

        //For See All
        [HttpPost]
        public ActionResult<dynamic> GetList(ListDTO dto)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);


                //var itemsList = new dynamic;
                switch (dto.TypeId)
                {
                    case 1:
                        return GetBooks(dto, LangId);
                        break;
                    case 2:
                        return GetActivities(dto, LangId);
                        break;
                    case 3:
                        return GetGuest(dto, LangId);
                        break;
                    case 4:
                        return GetExhibitors(dto, LangId);
                        break;
                }
                return new { pageCount = 0, data = new List<ItemsDTO>() };
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside ListMobile  Post action: {ex.Message}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet("GetSearchMobile/{webId}/{type}/{term}/{pageNumber}/{pageSize}")]
        public ActionResult<dynamic> GetSearchMobile(long webId, long? type, string term, int pageNumber = 1, int pageSize = 1000)
        {
            try
            {

                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var itemsList = new List<SearchListDTO>();
                //SearchListNewDTO itemsListObj = new SearchListNewDTO();
                // List<PageCountAndDataDTO> itemsList = new List<PageCountAndDataDTO>();
                PageCountAndDataDTO itemsObj = new PageCountAndDataDTO();

                switch (type)
                {
                    case 1:
                        itemsList = GetBooksByTerm(webId, LangId, term, pageNumber, pageSize);
                        break;
                    case 2:
                        itemsList = GetActivitiesByTerm(webId, LangId, term, pageNumber, pageSize);
                        break;
                    case 3:
                        itemsList = GetGuestsByTerm(webId, LangId, term, pageNumber, pageSize);
                        break;
                    case 4:
                        itemsList = GetExhibitorsByTerm(webId, LangId, term, pageNumber, pageSize);
                        break;
                    default:
                        //itemsList= SearchEntireSiteByTerm(webId, LangId, term, pageNumber, pageSize);

                        itemsList = GetBooksByTerm(webId, LangId, term, pageNumber, pageSize);
                        itemsList.AddRange(GetActivitiesByTerm(webId, LangId, term, pageNumber, pageSize));
                        itemsList.AddRange(GetGuestsByTerm(webId, LangId, term, pageNumber, pageSize));
                        itemsList.AddRange(GetExhibitorsByTerm(webId, LangId, term, pageNumber, pageSize));
                        break;
                }
                return itemsList;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok("Something went wrong. Please try again later.");

            }
        }

        [HttpGet("GetFilterMasterData/{webId}/{typeId}")]
        public ActionResult<dynamic> GetFilterMasterData(long webId, long typeId)
        {
            MobileSearchFilterDTO mobileSearchFilterDTO = new MobileSearchFilterDTO();
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            switch (typeId)
            {
                case 1:
                    mobileSearchFilterDTO = FilterDataforBooks(LangId);
                    return mobileSearchFilterDTO;
                case 2:
                    mobileSearchFilterDTO = FilterDataforActivities(LangId);
                    return mobileSearchFilterDTO;
                case 3:
                    mobileSearchFilterDTO = FilterDataforBooks(LangId);//Guests
                    return mobileSearchFilterDTO;
                case 4:
                    mobileSearchFilterDTO = FilterDataforExhibitors(LangId, webId);//Exhibitors
                    return mobileSearchFilterDTO;
            }
            return null;
        }
        [HttpGet]

        [Route("GetItemDetails/{sectionid}/{itemid}")]
        //[HttpGet("{itemid}/{sectionid}")]
        public ActionResult<dynamic> GetItemDetails(long sectionId, long itemId)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);
                //long memberid = User.Identity.GetID();
                //if (memberid > 0)
                //{
                var itemDetails = new ActivityItemDetailsDTO();
                var bookItemDetails = new BookItemDetailsDTO();
                var exhibitorItemDetails = new ExhibitorItemDetailsDTO();
                var guestItemDetails = new GuestItemDetailsDTO();
                if (sectionId == 1)
                {
                    bookItemDetails = GetBookDetails(langId, itemId);
                    return bookItemDetails;
                }
                else if (sectionId == 2)
                {
                    itemDetails = GetXsiActivity(langId, itemId);
                    return itemDetails;
                }
                else if (sectionId == 3)
                {
                    guestItemDetails = GetGuestDetails(langId, itemId);
                    return guestItemDetails;
                }
                else if (sectionId == 4)
                {
                    exhibitorItemDetails = GetExhibitorById(langId, itemId);
                    return exhibitorItemDetails;
                }

                return null;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiCareers action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [Route("GetLoggedInUserItemDetails/{memberid}")]
        //[HttpGet("{itemid}/{sectionid}")]
        public ActionResult<dynamic> GetLoggedInUserItemDetails(long memberId)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);
                long webId = 1;
                long.TryParse(Request.Headers["WebsiteId"], out webId);
                if (webId == 0)
                    webId = 1;
                var exhibitorItemDetails = new LoggedInExhibitorItemDetailsDTO();

                exhibitorItemDetails = GetExhibitorByLoggedUserId(webId, langId, memberId);
                return exhibitorItemDetails;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiCareers action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpPost]
        [Route("GetFilteredBooks")]
        public ActionResult<dynamic> GetFilteredBooks(List<MainRequestDTO> mainRequestDTOList)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);

                long webId = 1;
                long.TryParse(Request.Headers["WebsiteId"], out webId);
                if (webId == 0)
                    webId = 1;

                var booksList = GetBooksByFilterSearch(mainRequestDTOList, webId, langId);
                return booksList;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetFilteredBooks action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [Route("GetFilteredActivities/{categoryId}")]
        public ActionResult<dynamic> GetFilteredActivities(long categoryId)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);

                var booksList = GetActivitiesByFilterSearch(categoryId, langId);
                return booksList;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetFilteredActivities action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpPost]
        [Route("GetFilteredExhibitors")]
        public ActionResult<dynamic> GetFilteredExhibitors(List<MainRequestDTO> mainRequestDTOList)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);

                long webId = 1;
                long.TryParse(Request.Headers["WebsiteId"], out webId);
                if (webId == 0)
                    webId = 1;
                var booksList = GetExhibitorsByFilterSearch(mainRequestDTOList, webId, langId);
                return booksList;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetFilteredBooks action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }


        /*New Begins Here*/

        #region Modules (Type) For See All ,Search, Filters from List
        private dynamic GetBooks(ListDTO dto, long languageId)
        {
            long websiteId = dto.WebsiteId ?? 1;
            string searchby = dto.SearchBy;
            // string filterby = dto.FilterBy;
            List<FilterPayLoadDTO> filterpayloadList = dto.FilterByPayLoad ?? null;
            int pageNumber = dto.PageNumber ?? 1;
            int pageSize = dto.PageSize ?? 10;
            var predicateBooksParticating = PredicateBuilder.New<XsiExhibitionBookParticipating>();
            List<BooksDTO> result = new List<BooksDTO>();
            var predicate = PredicateBuilder.New<XsiExhibitionBooks>();

            if (cachedBooks == null)
                cachedBooks = CacheKeys.GetBookList(_cache);
            if (cachedBookParticipating == null)
                cachedBookParticipating = CacheKeys.GetBooksParticipatingList(_cache);
            //if (cachedBookAuthors == null)
            //    cachedBookAuthors = CacheKeys.GetBookAuthorsList(_cache);
            //if (cachedAuthors == null)
            //    cachedAuthors = CacheKeys.GetAuthorsList(_cache);
            if (cachedExhibitionMemberApplicationYearly == null)
                cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);
            var exhibition = MethodFactory.GetActiveExhibitionNew(websiteId, _cache);
            var cachedAuthorsNew = cachedAuthors;
            List<long> authorids = new List<long>();
            if (exhibition != null)
            {
                HashSet<long> exhibitorIds = new HashSet<long>(cachedExhibitionMemberApplicationYearly.AsQueryable().Where(p => (p.Status == "A" || p.Status == "I") && p.ExhibitionId == exhibition.ExhibitionId).Select(x => x.MemberExhibitionYearlyId));

                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(p => p.IsEnable == "Y");
                predicateBooksParticating = predicateBooksParticating.And(p => p.IsActive == "Y");

                if (!string.IsNullOrEmpty(searchby))
                {
                    if (languageId == 1)
                    {
                        predicate = predicate.And(i => i.TitleEn != null && (i.TitleEn.Contains(searchby, StringComparison.OrdinalIgnoreCase)));
                        predicate = predicate.Or(i => i.BookDetails != null && (i.BookDetails.Contains(searchby, StringComparison.OrdinalIgnoreCase)));
                    }
                    else
                    {
                        predicate = predicate.And(i => i.TitleAr != null && (i.TitleAr.Contains(searchby, StringComparison.OrdinalIgnoreCase)));
                        predicate = predicate.Or(i => i.BookDetailsAr != null && i.BookDetailsAr.Contains(searchby, StringComparison.OrdinalIgnoreCase));
                    }
                }
                else if (filterpayloadList != null && filterpayloadList.Count > 0)
                {
                    var title = filterpayloadList.Where(p => p.PropertyId == 1).Select(s => s.Value).FirstOrDefault();
                    var author = filterpayloadList.Where(p => p.PropertyId == 2).Select(s => s.Value).FirstOrDefault();
                    var subjectId = filterpayloadList.Where(p => p.PropertyId == 3).Select(s => s.Value).FirstOrDefault();
                    var bookTypeId = filterpayloadList.Where(p => p.PropertyId == 4).Select(s => s.Value).FirstOrDefault();
                    var exhibitor = filterpayloadList.Where(p => p.PropertyId == 5).Select(s => s.Value).FirstOrDefault();
                    var year = filterpayloadList.Where(p => p.PropertyId == 6).Select(s => s.Value).FirstOrDefault();

                    if (!string.IsNullOrEmpty(exhibitor))
                    {
                        if (languageId == 1)
                            exhibitorIds = new HashSet<long>(cachedExhibitionMemberApplicationYearly.AsQueryable().Where(p => (p.Status == "A" || p.Status == "I") && exhibitorIds.Contains(p.MemberExhibitionYearlyId) && (p.ContactPersonName == exhibitor || p.PublisherNameEn.Contains(exhibitor))).Select(x => x.MemberExhibitionYearlyId));
                        else
                            exhibitorIds = new HashSet<long>(cachedExhibitionMemberApplicationYearly.AsQueryable().Where(p => (p.Status == "A" || p.Status == "I") && exhibitorIds.Contains(p.MemberExhibitionYearlyId) && (p.ContactPersonNameAr == exhibitor || p.PublisherNameAr.Contains(exhibitor))).Select(x => x.MemberExhibitionYearlyId));
                    }
                    if (exhibitorIds.Count > 0)
                    {
                        if (bookTypeId != null && !string.IsNullOrEmpty(bookTypeId))
                            predicate = predicate.And(i => i.BookTypeId == Convert.ToInt64(bookTypeId));
                        if (!string.IsNullOrEmpty(year))
                            predicate = predicate.And(i => i.IssueYear != null && i.IssueYear.Contains(year));
                        if (subjectId != null && !string.IsNullOrEmpty(subjectId))
                            predicate = predicate.And(i => i.ExhibitionSubjectId == Convert.ToInt64(subjectId));

                        if (!string.IsNullOrEmpty(title))
                        {
                            if (languageId == 1)
                                predicate = predicate.And(i => i.TitleEn != null && i.TitleEn.Contains(title, StringComparison.OrdinalIgnoreCase));
                            else
                                predicate = predicate.And(i => i.TitleAr != null && i.TitleAr.Contains(title, StringComparison.OrdinalIgnoreCase));
                        }

                        if (!string.IsNullOrEmpty(author))
                        {
                            if (languageId == 1)
                                cachedAuthorsNew = cachedAuthors.AsQueryable().Where(i => i.Title != null && i.Title.Contains(author, StringComparison.OrdinalIgnoreCase)).ToList();
                            else
                                cachedAuthorsNew = cachedAuthors.AsQueryable().Where(i => i.TitleAr != null && i.TitleAr.Contains(author, StringComparison.OrdinalIgnoreCase)).ToList();
                        }
                    }
                }
                predicateBooksParticating = predicateBooksParticating.And(p => p.ExhibitorId != null && exhibitorIds.Contains(p.ExhibitorId.Value));
            }

            if (languageId == 1)
            {
                var pagecount = (from bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating)
                                 join b in cachedBooks.AsQueryable().Where(predicate) on bp.BookId equals b.BookId
                                 join ba in cachedBookAuthors.AsQueryable() on b.BookId equals ba.BookId
                                 join a in cachedAuthorsNew.AsQueryable() on ba.AuthorId equals a.AuthorId
                                 select new ItemsDTO
                                 {
                                     Id = bp.BookId
                                 }).Count();

                var list = (from bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating)
                            join b in cachedBooks.AsQueryable().Where(predicate) on bp.BookId equals b.BookId
                            join ba in cachedBookAuthors.AsQueryable() on b.BookId equals ba.BookId
                            join a in cachedAuthorsNew.AsQueryable() on ba.AuthorId equals a.AuthorId
                            select new ItemsDTO
                            {
                                Id = bp.BookId,
                                Image = string.IsNullOrEmpty(b.Thumbnail) ? UploadsCMSPath + "/default-book_en.jpg" : UploadsCMSPath + "/Books/Thumbnails/Small/" + b.Thumbnail,
                                Title = b.TitleEn,
                                SubTitle = "Author(s): " + (a.Title ?? "-") //GetAuthorNameById(ba.AuthorId, languageId)
                            }).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                return new { pageCount = pagecount, data = list };
            }
            else
            {
                var pagecount = (from bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating)
                                 join b in cachedBooks.AsQueryable().Where(predicate) on bp.BookId equals b.BookId
                                 join ba in cachedBookAuthors.AsQueryable() on b.BookId equals ba.BookId
                                 join a in cachedAuthorsNew.AsQueryable() on ba.AuthorId equals a.AuthorId
                                 select new ItemsDTO
                                 {
                                     Id = bp.BookId
                                 }).Count();

                var list = (from bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating)
                            join b in cachedBooks.AsQueryable().Where(predicate) on bp.BookId equals b.BookId
                            join ba in cachedBookAuthors.AsQueryable() on b.BookId equals ba.BookId
                            join a in cachedAuthorsNew.AsQueryable() on ba.AuthorId equals a.AuthorId
                            select new ItemsDTO
                            {
                                Id = bp.BookId,
                                Image = string.IsNullOrEmpty(b.Thumbnail) ? UploadsCMSPath + "/default-book_ar.jpg" : UploadsCMSPath + "/Books/Thumbnails/Small/" + b.Thumbnail,
                                Title = b.TitleAr,
                                SubTitle = "المؤلف: " + (a.TitleAr ?? "-") // GetAuthorNameById(ba.AuthorId, languageId)
                            }).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                return new { pageCount = pagecount, data = list };
            }
        }

        private dynamic GetActivities(ListDTO dto, long languageId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long websiteId = dto.WebsiteId ?? 1;
                string searchby = dto.SearchBy;
                // string filterby = dto.FilterBy;
                List<FilterPayLoadDTO> filterpayloadList = dto.FilterByPayLoad ?? null;
                int pageNumber = dto.PageNumber ?? 1;
                int pageSize = dto.PageSize ?? 10;

                if (cachedExhibition == null)
                    cachedExhibition = CacheKeys.GetExhibition(_cache);
                if (cachedEventGuest == null)
                    cachedEventGuest = CacheKeys.GetEventGuestFromCache(_cache);

                if (cachedEventCategory == null)
                    cachedEventCategory = CacheKeys.GetEventCategory(_cache);

                if (cachedEventSubCategory == null)
                    cachedEventSubCategory = CacheKeys.GetEventSubCategory(_cache);

                if (cachedEvents == null)
                    cachedEvents = CacheKeys.GetEvents(_cache);
                if (cachedEventDate == null)
                    cachedEventDate = CacheKeys.GetEventDate(_cache);

                List<long> categoryIDs = new List<long>();
                List<long> subCategoryIDs = new List<long>();

                var predicateCategory = PredicateBuilder.New<XsiEventCategory>();
                var predicateSubCategory = PredicateBuilder.New<XsiEventSubCategory>();
                var predicateEvent = PredicateBuilder.New<XsiEvent>();
                var predicateEventDate = PredicateBuilder.New<XsiEventDate>();
                predicateCategory = predicateCategory.And(i => i.WebsiteId == 1 || i.WebsiteId == 0);
                predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");
                DateTime? dateStartsWith = null;
                // DateTime dtNow = MethodFactory.ArabianTimeNow();
                DateTime dtNow = new DateTime(2019, 10, 30, 0, 0, 0);
                XsiExhibition exhibition = MethodFactory.GetActiveExhibitionNew(websiteId, _cache);
                if (languageId == 1)
                {
                    predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    predicateSubCategory = predicateSubCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    categoryIDs = cachedEventCategory.AsQueryable().Where(predicateCategory).Select(i => i.ItemId).ToList();

                    if (categoryIDs.Count > 0)
                    {
                        predicateSubCategory = predicateSubCategory.And(i => categoryIDs.Contains(i.EventCategoryId.Value));
                        subCategoryIDs = cachedEventSubCategory.AsQueryable().Where(predicateSubCategory).Select(i => i.ItemId).ToList();
                    }

                    if (subCategoryIDs.Count > 0)
                        predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));

                    if (!string.IsNullOrEmpty(searchby))
                    {
                        if (languageId == 1)
                            predicateEvent = predicateEvent.And(i => i.Title != null && i.Title.Contains(searchby, StringComparison.OrdinalIgnoreCase));
                        else
                            predicateEvent = predicateEvent.And(i => i.TitleAr != null && i.TitleAr.Contains(searchby, StringComparison.OrdinalIgnoreCase));
                    }
                    else if (filterpayloadList != null && filterpayloadList.Count > 0)
                    {
                        var eventdatebegin = filterpayloadList.Where(p => p.PropertyId == -1).Select(s => s.Value).FirstOrDefault();
                        var category = filterpayloadList.Where(p => p.PropertyId == -2).Select(s => s.Value).FirstOrDefault();
                        if (category != null && !string.IsNullOrEmpty(category) && Convert.ToInt64(category) > 0)
                        {
                            long categoryId = Convert.ToInt64(category);
                            predicateSubCategory = predicateSubCategory.And(i => i.EventCategoryId == categoryId);
                            subCategoryIDs = cachedEventSubCategory.AsQueryable().Where(predicateSubCategory).Select(i => i.ItemId).ToList();
                            predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));
                        }
                        if (!string.IsNullOrEmpty(eventdatebegin))
                        {
                            DateTime dt;
                            if (DateTime.TryParse(eventdatebegin, out dt))
                                dateStartsWith = dt;
                        }
                    }


                    //if (exhibition != null)
                    //{
                    //    predicateEventDate = predicateEventDate.And(i =>
                    //        i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate);
                    //}

                    //var predicatePrevExhibition = PredicateBuilder.New<XsiExhibition>();
                    //predicatePrevExhibition = predicatePrevExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 2);
                    //XsiExhibition prevexhibition = cachedExhibition.AsQueryable().Where(predicatePrevExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        if (dateStartsWith != null)
                            predicateEventDate = predicateEventDate.And(i => (i.StartDate >= dateStartsWith && i.EndDate <= exhibition.EndDate.Value.AddDays(1)));
                        else
                            predicateEventDate = predicateEventDate.And(i => (i.StartDate >= dtNow && i.EndDate <= exhibition.EndDate.Value.AddDays(1)));
                    }

                    //prevexhibition.EndDate

                    //var eventitemIDs = cachedEventGuest.AsQueryable().Select(i => i.EventId.Value).ToList();
                    //if (eventitemIDs.Count > 0)
                    //    predicateEvent = predicateEvent.And(i => eventitemIDs.Contains(i.ItemId));

                    var pagecount = _context.XsiEventDate.Where(predicateEventDate)
                           .Join(_context.XsiEvent.Where(predicateEvent)
                               , ed => ed.EventId
                               , ev => ev.ItemId
                               , (ed, ev) => new
                               {
                                   Id = ed.ItemId
                               }).Count();
                    var listMain = _context.XsiEventDate.Where(predicateEventDate)
                           .Join(_context.XsiEvent.Where(predicateEvent)
                               , ed => ed.EventId
                               , ev => ev.ItemId
                               , (ed, ev) => new
                               {
                                   Id = ed.ItemId,
                                   Title = ev.Title,
                                   Caption = ev.EventSubCategory.EventCategory.Title,
                                   Color = ev.EventSubCategory.EventCategory.Color,
                                   StartDate = ed.StartDate,
                                   SubTitle = ed.StartDate != null ? (GetDateFormat(ed.StartDate, "dd MMM", languageId)) : string.Empty,
                                   EndDate = ed.EndDate
                               }).OrderBy(o => o.StartDate).ThenBy(o => o.EndDate)
                           .Select(s => s).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                    var list = new List<ItemsDTO>();
                    foreach (var item in listMain)
                    {
                        var itemDto = new ItemsDTO();
                        itemDto.Title = item.Title;
                        itemDto.SubTitle = item.SubTitle;
                        itemDto.Caption = item.Caption;
                        itemDto.Color = item.Color;
                        itemDto.Id = item.Id;
                        list.Add(itemDto);
                    }

                    //if (pageNumber > 0 && pageSize > 0)
                    //    return list.Skip(pageNumber).Take(pageSize).ToList();
                    //else



                    //return list;
                    return new { pageCount = pagecount, data = list };
                }
                else
                {
                    predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    predicateSubCategory = predicateSubCategory.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicateEvent = predicateEvent.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                    categoryIDs = cachedEventCategory.AsQueryable().Where(predicateCategory).Select(i => i.ItemId).ToList();

                    if (categoryIDs.Count > 0)
                    {
                        predicateSubCategory = predicateSubCategory.And(i => categoryIDs.Contains(i.EventCategoryId.Value));
                        subCategoryIDs = cachedEventSubCategory.AsQueryable().Where(predicateSubCategory).Select(i => i.ItemId).ToList();
                    }

                    if (subCategoryIDs.Count > 0)
                        predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));

                    if (!string.IsNullOrEmpty(searchby))
                    {
                        if (languageId == 1)
                            predicateEvent = predicateEvent.And(i => i.Title != null && i.Title.Contains(searchby, StringComparison.OrdinalIgnoreCase));
                        else
                            predicateEvent = predicateEvent.And(i => i.TitleAr != null && i.TitleAr.Contains(searchby, StringComparison.OrdinalIgnoreCase));
                    }
                    else if (filterpayloadList != null && filterpayloadList.Count > 0)
                    {
                        var eventdatebegin = filterpayloadList.Where(p => p.PropertyId == -1).Select(s => s.Value).FirstOrDefault();
                        var category = filterpayloadList.Where(p => p.PropertyId == -2).Select(s => s.Value).FirstOrDefault();
                        if (category != null && !string.IsNullOrEmpty(category) && Convert.ToInt64(category) > 0)
                        {
                            long categoryId = Convert.ToInt64(category);
                            predicateSubCategory = predicateSubCategory.And(i => i.EventCategoryId == categoryId);
                            subCategoryIDs = cachedEventSubCategory.AsQueryable().Where(predicateSubCategory).Select(i => i.ItemId).ToList();
                            predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));
                        }
                        if (!string.IsNullOrEmpty(eventdatebegin))
                        {
                            DateTime dt;
                            if (DateTime.TryParse(eventdatebegin, out dt))
                                dateStartsWith = dt;
                        }
                    }
                    // XsiExhibition exhibition = MethodFactory.GetActiveExhibitionNew(websiteId, _cache);

                    //if (exhibition != null)
                    //{
                    //    predicateEventDate = predicateEventDate.And(i =>
                    //        i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate);
                    //}

                    //var predicatePrevExhibition = PredicateBuilder.New<XsiExhibition>();
                    //predicatePrevExhibition = predicatePrevExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 2);
                    //XsiExhibition prevexhibition = cachedExhibition.AsQueryable().Where(predicatePrevExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        if (dateStartsWith != null)
                            predicateEventDate = predicateEventDate.And(i => (i.StartDate >= dateStartsWith && i.EndDate <= exhibition.EndDate.Value.AddDays(1)));
                        else
                            predicateEventDate = predicateEventDate.And(i => (i.StartDate >= dtNow && i.EndDate <= exhibition.EndDate.Value.AddDays(1)));
                    }

                    //prevexhibition.EndDate

                    //var eventitemIDs = cachedEventGuest.AsQueryable().Select(i => i.EventId.Value).ToList();
                    //if (eventitemIDs.Count > 0)
                    //    predicateEvent = predicateEvent.And(i => eventitemIDs.Contains(i.ItemId));
                    var pagecount = _context.XsiEventDate.Where(predicateEventDate)
                       .Join(_context.XsiEvent.Where(predicateEvent)
                           , ed => ed.EventId
                           , ev => ev.ItemId
                           , (ed, ev) => new
                           {
                               Id = ed.ItemId
                           }).Count();
                    var listMain = _context.XsiEventDate.Where(predicateEventDate)
                           .Join(_context.XsiEvent.Where(predicateEvent)
                               , ed => ed.EventId
                               , ev => ev.ItemId
                               , (ed, ev) => new
                               {
                                   Id = ed.ItemId,
                                   Title = ev.TitleAr,
                                   Caption = ev.EventSubCategory.EventCategory.TitleAr,
                                   Color = ev.EventSubCategory.EventCategory.ColorAr,
                                   StartDate = ed.StartDate,
                                   SubTitle = ed.StartDate != null ? (GetDateFormat(ed.StartDate, "dd MMM", languageId)) : string.Empty,
                                   EndDate = ed.EndDate
                               }).OrderBy(o => o.StartDate).ThenBy(o => o.EndDate)
            .Select(s => s).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                    var list = new List<ItemsDTO>();
                    foreach (var item in listMain)
                    {
                        var itemDto = new ItemsDTO();
                        itemDto.Title = item.Title;
                        itemDto.SubTitle = item.SubTitle;
                        itemDto.Caption = item.Caption;
                        itemDto.Color = item.Color;
                        itemDto.Id = item.Id;
                        list.Add(itemDto);
                    }

                    //if (pageNumber > 0 && pageSize > 0)
                    //    return list.Skip(pageNumber).Take(pageSize).ToList();
                    //else


                    //return list;
                    return new { pageCount = pagecount, data = list };
                }
            }
        }
        private dynamic GetGuest(ListDTO dto, long languageId)
        {
            long websiteId = dto.WebsiteId ?? 1;
            string searchby = dto.SearchBy;
            // string filterby = dto.FilterBy;
            List<FilterPayLoadDTO> filterpayloadList = dto.FilterByPayLoad ?? null;
            int pageNumber = dto.PageNumber ?? 1;
            int pageSize = dto.PageSize ?? 10;
            if (cachedGuest == null)
                cachedGuest = CacheKeys.GetGuestsList(_cache);
            if (cachedExhibition == null)
                cachedExhibition = CacheKeys.GetExhibition(_cache);
            if (cachedEventGuest == null)
                cachedEventGuest = CacheKeys.GetEventGuestFromCache(_cache);
            if (cachedEvents == null)
                cachedEvents = CacheKeys.GetEvents(_cache);
            if (cachedEventDate == null)
                cachedEventDate = CacheKeys.GetEventDate(_cache);
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = languageId;

                List<long> eventIDs = new List<long>();
                List<long> guestIDs = new List<long>();

                var predicateGuest = PredicateBuilder.New<XsiGuests>();
                var predicateEvent = PredicateBuilder.New<XsiEvent>();
                var predicateEventDate = PredicateBuilder.New<XsiEventDate>();

                var predicateExhibition = PredicateBuilder.New<XsiExhibition>();

                if (LangId == 1)
                {
                    predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 1);
                    XsiExhibition exhibition = MethodFactory.GetActiveExhibitionNew(websiteId, _cache); // cachedExhibition.AsQueryable().Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        if (!string.IsNullOrEmpty(searchby))
                        {
                            if (languageId == 1)
                                predicateGuest = predicateGuest.And(i => i.GuestName != null && i.GuestName.Contains(searchby, StringComparison.OrdinalIgnoreCase));
                            else
                                predicateGuest = predicateGuest.And(i => i.GuestNameAr != null && i.GuestNameAr.Contains(searchby, StringComparison.OrdinalIgnoreCase));
                        }
                        else if (filterpayloadList != null && filterpayloadList.Count > 0)
                        {
                            var startswithterm = filterpayloadList.Where(p => p.PropertyId == -1).Select(s => s.Value).FirstOrDefault();
                            if (!string.IsNullOrEmpty(startswithterm))
                            {
                                if (languageId == 1)
                                    predicateGuest = predicateGuest.And(i => i.GuestName != null && i.GuestName.StartsWith(startswithterm, StringComparison.OrdinalIgnoreCase));
                                else
                                    predicateGuest = predicateGuest.And(i => i.GuestNameAr != null && i.GuestNameAr.StartsWith(startswithterm, StringComparison.OrdinalIgnoreCase));
                            }
                        }

                        predicateEventDate = predicateEventDate.And(i =>
                        i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate.Value.AddDays(1));

                        predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");
                        //predicateEvent = predicateEvent.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);

                        eventIDs = cachedEventDate.AsQueryable().Where(predicateEventDate)
                            .Join(cachedEvents.AsQueryable().Where(predicateEvent)
                                , ed => ed.EventId
                                , ev => ev.ItemId
                                , (ed, ev) => new
                                {
                                    ev.ItemId
                                }).Select(i => i.ItemId)
                            .ToList();

                        guestIDs = cachedEventGuest.AsQueryable().Where(i => eventIDs.Contains(i.EventId.Value)).Select(i => i.GuestId.Value).ToList();

                        if (guestIDs.Count > 0)
                            predicateGuest = predicateGuest.And(i => guestIDs.Contains(i.ItemId));
                    }

                    predicateGuest = predicateGuest.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    //predicateGuest = predicateGuest.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);
                    var pagecount = cachedGuest.AsQueryable().Where(predicateGuest)
                                     .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new ItemsDTO()
                                     {
                                         Id = i.ItemId
                                     }).Count();
                    var list = cachedGuest.AsQueryable().Where(predicateGuest)
                                         .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new ItemsDTO()
                                         {
                                             Id = i.ItemId,
                                             Title = i.GuestName,
                                             SubTitle = i.GuestTitle,
                                             Image = !string.IsNullOrEmpty(i.GuestPhoto) ? _appCustomSettings.UploadsPath + "/guest/ListThumbnailNew/" + (i.GuestPhoto ?? "guest.jpg") : "",

                                             //SubTitle = Regex.Replace(WithMaxLength(i.Description, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty),
                                             //  Events = "",
                                             //Caption = i.GuestName
                                         }).OrderBy(i => i.Title).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                    //if (pageNumber > 0 && pageSize > 0)
                    //    return list.Skip(pageNumber).Take(pageSize).ToList();
                    //else


                    //return list;
                    return new { pageCount = pagecount, data = list };
                }
                else
                {
                    predicateExhibition = predicateExhibition.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 1);
                    XsiExhibition exhibition = cachedExhibition.AsQueryable().Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        if (!string.IsNullOrEmpty(searchby))
                        {
                            if (languageId == 1)
                                predicateGuest = predicateGuest.And(i => i.GuestName != null && i.GuestName.Contains(searchby, StringComparison.OrdinalIgnoreCase));
                            else
                                predicateGuest = predicateGuest.And(i => i.GuestNameAr != null && i.GuestNameAr.Contains(searchby, StringComparison.OrdinalIgnoreCase));
                        }
                        else if (filterpayloadList != null && filterpayloadList.Count > 0)
                        {
                            var startswithterm = filterpayloadList.Where(p => p.PropertyId == -1).Select(s => s.Value).FirstOrDefault();
                            if (!string.IsNullOrEmpty(startswithterm))
                            {
                                if (languageId == 1)
                                    predicateGuest = predicateGuest.And(i => i.GuestName != null && i.GuestName.StartsWith(startswithterm, StringComparison.OrdinalIgnoreCase));
                                else
                                    predicateGuest = predicateGuest.And(i => i.GuestNameAr != null && i.GuestNameAr.StartsWith(startswithterm, StringComparison.OrdinalIgnoreCase));
                            }
                        }

                        predicateEventDate = predicateEventDate.And(i =>
                            i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate.Value.AddDays(1));

                        predicateEvent = predicateEvent.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                        predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");
                        //predicateEvent = predicateEvent.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);

                        eventIDs = cachedEventDate.AsQueryable().Where(predicateEventDate)
                             .Join(cachedEvents.AsQueryable().Where(predicateEvent)
                                 , ed => ed.EventId
                                , ev => ev.ItemId
                                , (ed, ev) => new
                                {
                                    ev.ItemId
                                }).Select(i => i.ItemId)
                            .ToList();

                        guestIDs = cachedEventGuest.AsQueryable().Where(i => eventIDs.Contains(i.EventId.Value)).Select(i => i.GuestId.Value).ToList();

                        if (guestIDs.Count > 0)
                            predicateGuest = predicateGuest.And(i => guestIDs.Contains(i.ItemId));
                    }

                    predicateGuest = predicateGuest.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    //predicateGuest = predicateGuest.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);

                    var pagecount = cachedGuest.AsQueryable().Where(predicateGuest)
                                         .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new ItemsDTO()
                                         {
                                             Id = i.ItemId
                                         }).Count();
                    var list = cachedGuest.AsQueryable().Where(predicateGuest)
                                         .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new ItemsDTO()
                                         {
                                             Id = i.ItemId,
                                             Title = i.GuestNameAr,
                                             SubTitle = i.GuestTitleAr,
                                             Image = _appCustomSettings.UploadsPath + "/guest/ListThumbnailNew/" + (i.GuestPhotoAr ?? "guest.jpg"),
                                             //SubTitle = Regex.Replace(WithMaxLength(i.DescriptionAr, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty),
                                         }).OrderBy(i => i.Title).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                    //if (pageNumber > 0 && pageSize > 0)
                    //    return list.Skip(pageNumber).Take(pageSize).ToList();
                    //else



                    //return list;
                    return new { pageCount = pagecount, data = list };
                }
            }
        }

        private dynamic GetExhibitors(ListDTO dto, long languageId)
        {
            var predicate = PredicateBuilder.New<XsiExhibitionMemberApplicationYearly>();

            long websiteId = dto.WebsiteId ?? 1;
            string searchby = dto.SearchBy;
            //  string filterby = dto.FilterBy;
            List<FilterPayLoadDTO> filterpayloadList = dto.FilterByPayLoad ?? null;
            int pageNumber = dto.PageNumber ?? 1;
            int pageSize = dto.PageSize ?? 10;
            if (cachedExhibitionMemberApplicationYearly == null)
                cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

            if (cachedExhibitionExhibitorDetails == null)
                cachedExhibitionExhibitorDetails = CacheKeys.GetExhibitionExhibitorDetails(_cache);
            var exhibitorIds = new List<long?>();
            List<ItemsDTO> result = new List<ItemsDTO>();
            #region Current Exhibition
            var exhibition = MethodFactory.GetActiveExhibitionNew(websiteId, _cache);
            #endregion
            if (exhibition != null)
            {
                if (!string.IsNullOrEmpty(searchby))
                {
                    if (languageId == 1)
                        predicate = predicate.And(i => i.PublisherNameEn != null && i.PublisherNameEn.Contains(searchby, StringComparison.OrdinalIgnoreCase));
                    else
                        predicate = predicate.And(i => i.PublisherNameAr != null && i.PublisherNameAr.Contains(searchby, StringComparison.OrdinalIgnoreCase));
                }
                else if (filterpayloadList != null && filterpayloadList.Count > 0)
                {
                    var category = filterpayloadList.Where(p => p.PropertyId == 1).Select(s => s.Value).FirstOrDefault();
                    var country = filterpayloadList.Where(p => p.PropertyId == 2).Select(s => s.Value).FirstOrDefault();
                    var standSection = filterpayloadList.Where(p => p.PropertyId == 3).Select(s => s.Value).FirstOrDefault();
                    var standSubSection = filterpayloadList.Where(p => p.PropertyId == 4).Select(s => s.Value).FirstOrDefault();

                    long categoryId = -1;
                    long countryId = -1;
                    long standSectionId = -1;
                    long standSubSectionId = -1;

                    if (category != null && !string.IsNullOrEmpty(category))
                    {
                        categoryId = Convert.ToInt64(category);
                        predicate = predicate.And(i => i.ExhibitionCategoryId == categoryId);
                    }
                    if (country != null && !string.IsNullOrEmpty(country))
                    {
                        countryId = Convert.ToInt64(country);
                        predicate = predicate.And(i => i.CountryId == countryId);
                    }
                    //predicate = predicate.And(i => i.XsiExhibitionExhibitorDetails == exhibitorsRequestDTO.StandSectionId);

                    //var exhibitors = cachedExhibitionExhibitorDetails.AsQueryable().Where(p => p.MemberExhibitionYearly.MemberRoleId == 1).ToList();

                    if (standSection != null && standSubSection != null && !string.IsNullOrEmpty(standSection) && !string.IsNullOrEmpty(standSubSection))
                    {
                        standSectionId = Convert.ToInt64(standSection);
                        standSubSectionId = Convert.ToInt64(standSubSection);
                        exhibitorIds = cachedExhibitionExhibitorDetails.AsQueryable().Where(p => p.BoothSectionId == standSectionId && p.BoothSubSectionId == standSubSectionId).Select(s => s.MemberExhibitionYearlyId).ToList();
                    }
                    else if (standSection != null && !string.IsNullOrEmpty(standSection))
                    {
                        standSectionId = Convert.ToInt64(standSection);
                        exhibitorIds = cachedExhibitionExhibitorDetails.AsQueryable().Where(p => p.BoothSectionId == standSectionId).Select(s => s.MemberExhibitionYearlyId).ToList();
                    }
                }

                predicate = predicate.And(i => (i.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved) || i.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval)));
                predicate = predicate.And(i => i.MemberRoleId == 1 || i.MemberRoleId == 2);
                predicate = predicate.And(i => i.ExhibitionId == exhibition.ExhibitionId);

                string exhibitorlogo = _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/";
                string agencylogo = _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/agency-logo.png";
                //  var items = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).ToList();
                //if (exhibitorIds.Count > 0)
                // items = items.Where(p => exhibitorIds.Contains(p.MemberExhibitionYearlyId)).ToList();

                if (exhibitorIds.Count > 0)
                    predicate = predicate.And(i => exhibitorIds.Contains(i.MemberExhibitionYearlyId));
                if (languageId == 1)
                {
                    var pagecount = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).Select(i => new
                    {
                        Id = i.MemberExhibitionYearlyId
                    }).Count();
                    var memberResult = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).Select(i => new
                    {
                        Id = i.MemberExhibitionYearlyId,
                        Title = i.PublisherNameEn,
                        Caption = GetCategoryTitle(i.ExhibitionCategoryId, languageId), // Regex.Replace(WithMaxLength(i.ExhibitionCategory.Title, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty),
                        Image = i.MemberRoleId == 1 ? exhibitorlogo + i.FileName : agencylogo,
                        SubTitle = "-", // later needs to change to - details.StandCode;
                                        //Type = (i.MemberRoleId == 1) ? "Exhibitors" : (i.MemberRoleId == 2) ? "Agents" : string.Empty
                        Type = (i.MemberRoleId == 1) ? "EXBH" : "AGNT",
                        i.ParentId,
                        i.MemberRoleId
                    }).OrderBy(i => i.Title).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                    //if (pageNumber > 0 && pageSize > 0)
                    //    memberResult = memberResult.Skip(pageNumber).Take(pageSize).ToList();

                    foreach (var item in memberResult)
                    {
                        var itemDto = new ItemsDTO();
                        itemDto.Id = item.Id;
                        itemDto.Title = item.Title;
                        itemDto.Caption = item.Caption;
                        itemDto.Image = item.Image;
                        itemDto.Type = item.Type;
                        var details = item.MemberRoleId == 1 ? cachedExhibitionExhibitorDetails.AsQueryable().Where(i => i.MemberExhibitionYearlyId == item.Id).FirstOrDefault() : cachedExhibitionExhibitorDetails.AsQueryable().Where(i => i.MemberExhibitionYearlyId == (item.ParentId ?? -1)).FirstOrDefault();
                        if (details != null)
                        {
                            itemDto.FeatureId = details.FeatureId ?? "-1";
                            if (languageId == 1)
                                itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "Location: " + details.StandCode : "Location: -";
                            else
                                itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "رقم الجناح: " + details.StandCode : "رقم الجناح: -";
                        }
                        //itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "Location: " + details.StandCode : "Location: -";
                        result.Add(itemDto);
                    }

                    result = result.OrderBy(i => i.Title).ToList();


                    return new { pageCount = pagecount, data = result };
                }
                else
                {
                    var pagecount = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).Select(i => new
                    {
                        Id = i.MemberExhibitionYearlyId
                    }).Count();
                    var memberResult = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).Select(i => new
                    {
                        Id = i.MemberExhibitionYearlyId,
                        Title = i.PublisherNameAr ?? i.PublisherNameEn,
                        Caption = GetCategoryTitle(i.ExhibitionCategoryId, languageId), // Regex.Replace(WithMaxLength(i.ExhibitionCategory.TitleAr, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty),
                        Image = _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/" + i.FileName,
                        // later needs to change to - details.StandCode;
                        Type = (i.MemberRoleId == 1) ? "العارضين" : "التوكيلات",
                        i.ParentId,
                        i.MemberRoleId
                    }).OrderBy(i => i.Title).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                    //if (pageNumber > 0 && pageSize > 0)
                    //    memberResult = memberResult.Skip(pageNumber).Take(pageSize).ToList();

                    foreach (var item in memberResult)
                    {
                        var itemDto = new ItemsDTO();
                        itemDto.Id = item.Id;
                        itemDto.Title = item.Title;
                        itemDto.Caption = item.Caption;
                        itemDto.Image = item.Image;
                        itemDto.Type = item.Type;
                        var details = item.MemberRoleId == 1 ? cachedExhibitionExhibitorDetails.AsQueryable().Where(i => i.MemberExhibitionYearlyId == item.Id).FirstOrDefault() : cachedExhibitionExhibitorDetails.AsQueryable().Where(i => i.MemberExhibitionYearlyId == (item.ParentId ?? -1)).FirstOrDefault();
                        if (details != null)
                        {
                            itemDto.FeatureId = details.FeatureId ?? "-1";
                            if (languageId == 1)
                                itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "Location: " + details.StandCode : "Location: -";
                            else
                                itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "رقم الجناح: " + details.StandCode : "رقم الجناح: -";
                        }
                        // itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "Location: " + details.StandCode : "Location: -";
                        result.Add(itemDto);
                    }
                    result = result.OrderBy(i => i.Title).ToList();

                    return new { pageCount = pagecount, data = result };
                }
            }
            return null;
        }
        #endregion

        /***Old Starts Here ***/
        #region Dashboard
        private List<ItemsDTO> GetBooks(long websiteId, long languageId, int pageNumber = 1, int pageSize = 10)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (cachedBooks == null)
                    cachedBooks = CacheKeys.GetBookList(_cache);
                if (cachedBookParticipating == null)
                    cachedBookParticipating = CacheKeys.GetBooksParticipatingList(_cache);
                //if (cachedBookAuthors == null)
                //    cachedBookAuthors = CacheKeys.GetBookAuthorsList(_cache);
                //if (cachedAuthors == null)
                //    cachedAuthors = CacheKeys.GetAuthorsList(_cache);
                if (cachedExhibitionMemberApplicationYearly == null)
                    cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

                var exhibition = MethodFactory.GetActiveExhibitionNew(websiteId, _cache);
                if (exhibition != null)
                {
                    HashSet<long> exhibitorIds = new HashSet<long>(cachedExhibitionMemberApplicationYearly.AsQueryable().Where(p => (p.Status == "A" || p.Status == "I") && p.ExhibitionId == exhibition.ExhibitionId).Select(x => x.MemberExhibitionYearlyId));

                    List<BooksDTO> result = new List<BooksDTO>();
                    // var predicateBooks = PredicateBuilder.New<XsiExhibitionBooks>();
                    var predicateBooksParticating = PredicateBuilder.New<XsiExhibitionBookParticipating>();
                    var predicate = PredicateBuilder.New<XsiExhibitionBooks>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(p => p.IsEnable == "Y");
                    if (exhibitorIds.Count > 0)
                    {
                        predicateBooksParticating = predicateBooksParticating.And(p => p.IsActive == "Y");
                        predicateBooksParticating = predicateBooksParticating.And(p => p.ExhibitorId != null && exhibitorIds.Contains(p.ExhibitorId.Value));

                        // var bookParticipating = _context.XsiExhibitionBookParticipating.Where(p => exhibitorIds.Contains(p.ExhibitorId.Value) &&
                        //p.IsActive == EnumConversion.ToString(EnumBool.Yes)).Take(10);
                        // var books = _context.XsiExhibitionBooks.Where(predicate).ToList();
                        // var bookIds = books.Select(s => s.BookId).ToList();
                        //var authorIds=  _context.XsiExhibitionBookAuthor.Where(p => bookIds.Contains(p.BookId)).Select(s => s.AuthorId).ToList();
                        //  var bookAuthors = _context.XsiExhibitionBookAuthor.ToList();

                        if (languageId == 1)
                        {
                            var list = (from bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating)
                                        join b in cachedBooks.AsQueryable().Where(predicate) on bp.BookId equals b.BookId
                                        join ba in cachedBookAuthors.AsQueryable() on b.BookId equals ba.BookId
                                        join a in cachedAuthors.AsQueryable() on ba.AuthorId equals a.AuthorId
                                        select new ItemsDTO
                                        {
                                            Id = bp.BookId,
                                            Image = string.IsNullOrEmpty(b.Thumbnail) ? UploadsCMSPath + "/default-book_en.jpg" : UploadsCMSPath + "/Books/Thumbnails/Small/" + b.Thumbnail,
                                            Title = b.TitleEn,
                                            SubTitle = "Author(s): " + (a.Title ?? "-") //GetAuthorNameById(ba.AuthorId, languageId)
                                        }).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                            // string.Join(",", _context.XsiExhibitionAuthor.Where(p => p.AuthorId == ba.AuthorId).Select(s => s.Title).ToList()

                            //if (pageNumber > 0 && pageSize > 0)
                            //    return list.Skip(pageNumber).Take(200).ToList();
                            //else

                            return list;
                        }
                        else
                        {
                            var list = (from bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating)
                                        join b in cachedBooks.AsQueryable().Where(predicate) on bp.BookId equals b.BookId
                                        join ba in cachedBookAuthors.AsQueryable() on b.BookId equals ba.BookId
                                        join a in cachedAuthors.AsQueryable() on ba.AuthorId equals a.AuthorId
                                        select new ItemsDTO
                                        {
                                            Id = bp.BookId,
                                            Image = string.IsNullOrEmpty(b.Thumbnail) ? UploadsCMSPath + "/default-book_ar.jpg" : UploadsCMSPath + "/Books/Thumbnails/Small/" + b.Thumbnail,
                                            Title = b.TitleAr,
                                            SubTitle = "المؤلف: " + (a.TitleAr ?? "-") // GetAuthorNameById(ba.AuthorId, languageId)
                                        }).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                            //if (pageNumber > 0 && pageSize > 0)
                            //    return list.Skip(pageNumber).Take(200).ToList();
                            //else
                            return list;
                        }
                    }
                }
            }
            return null;
        }
        private List<ItemsDTO> GetActivities(int pageNumber = 1, int pageSize = 10)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (cachedExhibition == null)
                    cachedExhibition = CacheKeys.GetExhibition(_cache);
                if (cachedEventGuest == null)
                    cachedEventGuest = CacheKeys.GetEventGuestFromCache(_cache);
                DateTime dtNow = new DateTime(2019, 10, 30, 0, 0, 0);
                //    DateTime dtNow = MethodFactory.ArabianTimeNow();

                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    List<long> categoryIDs = new List<long>();
                    List<long> subCategoryIDs = new List<long>();

                    var predicateCategory = PredicateBuilder.New<XsiEventCategory>();
                    var predicateSubCategory = PredicateBuilder.New<XsiEventSubCategory>();
                    var predicateEvent = PredicateBuilder.New<XsiEvent>();
                    var predicateEventDate = PredicateBuilder.New<XsiEventDate>();

                    predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateCategory = predicateCategory.And(i => i.WebsiteId == 1 || i.WebsiteId == 0);

                    predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");

                    if (subCategoryIDs.Count > 0)
                        predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));


                    var predicateExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 1);
                    XsiExhibition exhibition = cachedExhibition.AsQueryable().Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    //if (exhibition != null)
                    //{
                    //    predicateEventDate = predicateEventDate.And(i =>
                    //        i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate);
                    //}

                    var predicatePrevExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicatePrevExhibition = predicatePrevExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 2);
                    XsiExhibition prevexhibition = cachedExhibition.AsQueryable().Where(predicatePrevExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        //predicateEventDate = predicateEventDate.And(i => (i.StartDate > prevexhibition.EndDate && i.EndDate <= exhibition.EndDate.Value.AddDays(1)));
                        predicateEventDate = predicateEventDate.And(i => (i.StartDate >= dtNow && i.EndDate <= exhibition.EndDate.Value.AddDays(1)));
                    }
                    //var eventitemIDs = _context.XsiEventGuest.Where(i => i.GuestId == guestid).Select(i => i.EventId.Value).ToList();

                    var eventitemIDs = cachedEventGuest.AsQueryable().Select(i => i.EventId.Value).ToList();
                    if (eventitemIDs.Count > 0)
                        predicateEvent = predicateEvent.And(i => eventitemIDs.Contains(i.ItemId));

                    var listMain = _context.XsiEventDate.Where(predicateEventDate)
                         .Join(_context.XsiEvent.Where(predicateEvent)
                             , ed => ed.EventId
                             , ev => ev.ItemId
                             , (ed, ev) => new
                             {
                                 Id = ed.ItemId,
                                 Title = ev.Title,
                                 Caption = ev.EventSubCategory.EventCategory.Title,
                                 Color = ev.EventSubCategory.EventCategory.Color,
                                 StartDate = ed.StartDate,
                                 SubTitle = ed.StartDate != null ? (GetDateFormat(ed.StartDate, "dd MMM", LangId)) : string.Empty,
                                 EndDate = ed.EndDate
                             }).OrderBy(o => o.StartDate).ThenBy(o => o.EndDate)
                         .Select(s => s).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                    var list = new List<ItemsDTO>();
                    foreach (var item in listMain)
                    {
                        var itemDto = new ItemsDTO();
                        itemDto.Title = item.Title;
                        itemDto.SubTitle = item.SubTitle;
                        itemDto.Caption = item.Caption;
                        itemDto.Color = item.Color;
                        itemDto.Id = item.Id;
                        list.Add(itemDto);
                    }

                    //if (pageNumber > 0 && pageSize > 0)
                    //    return list.Skip(pageNumber).Take(pageSize).ToList();
                    //else
                    return list;
                }
                else
                {
                    List<long> categoryIDs = new List<long>();
                    List<long> subCategoryIDs = new List<long>();

                    var predicateCategory = PredicateBuilder.New<XsiEventCategory>();
                    var predicateSubCategory = PredicateBuilder.New<XsiEventSubCategory>();
                    var predicateEvent = PredicateBuilder.New<XsiEvent>();
                    var predicateEventDate = PredicateBuilder.New<XsiEventDate>();

                    predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateCategory = predicateCategory.And(i => i.WebsiteId == 1 || i.WebsiteId == 0);

                    predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");

                    if (subCategoryIDs.Count > 0)
                        predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));


                    var predicateExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicateExhibition = predicateExhibition.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 1);
                    XsiExhibition exhibition = cachedExhibition.AsQueryable().Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).Where(predicateExhibition).FirstOrDefault();

                    //if (exhibition != null)
                    //{
                    //    predicateEventDate = predicateEventDate.And(i =>
                    //        i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate);
                    //}

                    var predicatePrevExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicatePrevExhibition = predicatePrevExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 2);
                    XsiExhibition prevexhibition = cachedExhibition.AsQueryable().Where(predicatePrevExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        //predicateEventDate = predicateEventDate.And(i => (i.StartDate > prevexhibition.EndDate && i.EndDate <= exhibition.EndDate.Value.AddDays(1)));
                        predicateEventDate = predicateEventDate.And(i => (i.StartDate >= dtNow && i.EndDate <= exhibition.EndDate.Value.AddDays(1)));
                    }
                    //var eventitemIDs = _context.XsiEventGuest.Where(i => i.GuestId == guestid).Select(i => i.EventId.Value).ToList();

                    var eventitemIDs = cachedEventGuest.AsQueryable().Select(i => i.EventId.Value).ToList();
                    if (eventitemIDs.Count > 0)
                        predicateEvent = predicateEvent.And(i => eventitemIDs.Contains(i.ItemId));

                    var listMain = _context.XsiEventDate.Where(predicateEventDate)
                         .Join(_context.XsiEvent.Where(predicateEvent)
                             , ed => ed.EventId
                             , ev => ev.ItemId
                             , (ed, ev) => new
                             {
                                 Id = ed.ItemId,
                                 Title = ev.TitleAr,
                                 Caption = ev.EventSubCategory.EventCategory.TitleAr,
                                 Color = ev.EventSubCategory.EventCategory.ColorAr,
                                 StartDate = ed.StartDate,
                                 SubTitle = ed.StartDate != null ? (GetDateFormat(ed.StartDate, "dd MMM", LangId)) : string.Empty,
                                 EndDate = ed.EndDate
                             }).OrderBy(o => o.StartDate).ThenBy(o => o.EndDate)
            .Select(s => s).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                    var list = new List<ItemsDTO>();
                    foreach (var item in listMain)
                    {
                        var itemDto = new ItemsDTO();
                        itemDto.Title = item.Title;
                        itemDto.SubTitle = item.SubTitle;
                        itemDto.Caption = item.Caption;
                        itemDto.Color = item.Color;
                        itemDto.Id = item.Id;
                        list.Add(itemDto);
                    }
                    //if (pageNumber > 0 && pageSize > 0)
                    //    return list.Skip(pageNumber).Take(pageSize).ToList();
                    //else
                    return list;
                }
            }
        }

        private string GetDateFormat(DateTime? dt, string dtformat, long langId)
        {
            if (dt != null)
                return dt.Value.ToString(dtformat);
            // return langId == 2 ? dt.Value.ToString(dtformat, System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")) :
            return string.Empty;
        }

        private List<ItemsDTO> GetXsiGuest(int pageNumber = 1, int pageSize = 10)
        {
            if (cachedGuest == null)
                cachedGuest = CacheKeys.GetGuestsList(_cache);
            if (cachedExhibition == null)
                cachedExhibition = CacheKeys.GetExhibition(_cache);
            if (cachedEventGuest == null)
                cachedEventGuest = CacheKeys.GetEventGuestFromCache(_cache);
            if (cachedEvents == null)
                cachedEvents = CacheKeys.GetEvents(_cache);
            if (cachedEventDate == null)
                cachedEventDate = CacheKeys.GetEventDate(_cache);
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    List<long> eventIDs = new List<long>();
                    List<long> guestIDs = new List<long>();

                    var predicateGuest = PredicateBuilder.New<XsiGuests>();
                    var predicateEvent = PredicateBuilder.New<XsiEvent>();
                    var predicateEventDate = PredicateBuilder.New<XsiEventDate>();

                    var predicateExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 1);
                    XsiExhibition exhibition = cachedExhibition.AsQueryable().Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        predicateEventDate = predicateEventDate.And(i =>
                            i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate.Value.AddDays(1));

                        predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");
                        //predicateEvent = predicateEvent.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);

                        eventIDs = cachedEventDate.AsQueryable().Where(predicateEventDate)
                            .Join(cachedEvents.AsQueryable().Where(predicateEvent)
                                , ed => ed.EventId
                                , ev => ev.ItemId
                                , (ed, ev) => new
                                {
                                    ev.ItemId
                                }).Select(i => i.ItemId)
                            .ToList();

                        guestIDs = cachedEventGuest.AsQueryable().Where(i => eventIDs.Contains(i.EventId.Value)).Select(i => i.GuestId.Value).ToList();

                        if (guestIDs.Count > 0)
                            predicateGuest = predicateGuest.And(i => guestIDs.Contains(i.ItemId));
                    }

                    predicateGuest = predicateGuest.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    //predicateGuest = predicateGuest.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);

                    var list = cachedGuest.AsQueryable().Where(predicateGuest)
                                         .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new ItemsDTO()
                                         {
                                             Id = i.ItemId,
                                             Title = i.GuestName,
                                             SubTitle = i.GuestTitle,
                                             Image = !string.IsNullOrEmpty(i.GuestPhoto) ? _appCustomSettings.UploadsPath + "/guest/ListThumbnailNew/" + (i.GuestPhoto ?? "guest.jpg") : "",

                                             //SubTitle = Regex.Replace(WithMaxLength(i.Description, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty),
                                             //  Events = "",
                                             //Caption = i.GuestName
                                         }).OrderBy(i => i.Title).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                    //if (pageNumber > 0 && pageSize > 0)
                    //    return list.Skip(pageNumber).Take(pageSize).ToList();
                    //else
                    return list;
                }
                else
                {
                    List<long> eventIDs = new List<long>();
                    List<long> guestIDs = new List<long>();

                    var predicateGuest = PredicateBuilder.New<XsiGuests>();
                    var predicateEvent = PredicateBuilder.New<XsiEvent>();
                    var predicateEventDate = PredicateBuilder.New<XsiEventDate>();

                    var predicateExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicateExhibition = predicateExhibition.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 1);
                    XsiExhibition exhibition = cachedExhibition.AsQueryable().Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        predicateEventDate = predicateEventDate.And(i =>
                            i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate.Value.AddDays(1));

                        predicateEvent = predicateEvent.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                        predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");
                        //predicateEvent = predicateEvent.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);

                        eventIDs = cachedEventDate.AsQueryable().Where(predicateEventDate)
                             .Join(cachedEvents.AsQueryable().Where(predicateEvent)
                                 , ed => ed.EventId
                                , ev => ev.ItemId
                                , (ed, ev) => new
                                {
                                    ev.ItemId
                                }).Select(i => i.ItemId)
                            .ToList();

                        guestIDs = cachedEventGuest.AsQueryable().Where(i => eventIDs.Contains(i.EventId.Value)).Select(i => i.GuestId.Value).ToList();

                        if (guestIDs.Count > 0)
                            predicateGuest = predicateGuest.And(i => guestIDs.Contains(i.ItemId));
                    }

                    predicateGuest = predicateGuest.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    //predicateGuest = predicateGuest.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);

                    var list = cachedGuest.AsQueryable().Where(predicateGuest)
                                         .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new ItemsDTO()
                                         {
                                             Id = i.ItemId,
                                             Title = i.GuestNameAr,
                                             SubTitle = i.GuestTitleAr,
                                             Image = _appCustomSettings.UploadsPath + "/guest/ListThumbnailNew/" + (i.GuestPhotoAr ?? "guest.jpg"),
                                             //SubTitle = Regex.Replace(WithMaxLength(i.DescriptionAr, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty),
                                         }).OrderBy(i => i.Title).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                    //if (pageNumber > 0 && pageSize > 0)
                    //    return list.Skip(pageNumber).Take(pageSize).ToList();
                    //else
                    return list;
                }
            }

        }
        private List<ItemsDTO> GetExhibitors(long webId, long langId, int pageNumber = 1, int pageSize = 10)
        {
            if (cachedExhibitionMemberApplicationYearly == null)
                cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

            if (cachedExhibitionExhibitorDetails == null)
                cachedExhibitionExhibitorDetails = CacheKeys.GetExhibitionExhibitorDetails(_cache);

            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                List<ItemsDTO> result = new List<ItemsDTO>();
                #region Current Exhibition
                var exhibition = MethodFactory.GetActiveExhibitionNew(webId, _cache);
                #endregion
                if (exhibition != null)
                {
                    var predicate = PredicateBuilder.New<XsiExhibitionMemberApplicationYearly>();
                    predicate = predicate.And(i => (i.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved) || i.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval)));
                    predicate = predicate.And(i => i.MemberRoleId == 1 || i.MemberRoleId == 2);//exhibitor
                    //predicate = predicate.Or(i => i.MemberRoleId == 2);//Agent
                    //predicate = predicate.And(i => i.MemberRoleId == 1);
                    predicate = predicate.And(i => i.ExhibitionId == exhibition.ExhibitionId);
                    var exhibitorDetailsService = new ExhibitorRegistrationService();

                    string exhibitorlogo = _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/";
                    string agencylogo = _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/agency-logo.png";
                    if (langId == 1)
                    {
                        var memberResult = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).Select(i => new
                        {
                            Id = i.MemberExhibitionYearlyId,
                            Title = i.PublisherNameEn,
                            Caption = GetCategoryTitle(i.ExhibitionCategoryId, langId), // Regex.Replace(WithMaxLength(i.ExhibitionCategory.Title, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty),
                            Image = GetLogoForExhibitor(i.FileName, i.MemberRoleId ?? 1), //exhibitorlogo + i.FileName : agencylogo,
                            SubTitle = "-", // later needs to change to - details.StandCode;
                            //Type = (i.MemberRoleId == 1) ? "Exhibitors" : (i.MemberRoleId == 2) ? "Agents" : string.Empty
                            Type = (i.MemberRoleId == 1) ? "Exhibitors" : "Agents",
                            i.ParentId,
                            i.MemberRoleId
                        }).OrderBy(i => i.Title).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                        //if (pageNumber > 0 && pageSize > 0)
                        //    memberResult = memberResult.Skip(pageNumber).Take(pageSize).ToList();

                        foreach (var item in memberResult)
                        {
                            var itemDto = new ItemsDTO();
                            itemDto.Id = item.Id;
                            itemDto.Title = item.Title;
                            itemDto.Caption = item.Caption;
                            itemDto.Image = item.Image;
                            itemDto.Type = item.Type;
                            var details = item.MemberRoleId == 1 ? cachedExhibitionExhibitorDetails.AsQueryable().Where(i => i.MemberExhibitionYearlyId == item.Id).FirstOrDefault() : cachedExhibitionExhibitorDetails.AsQueryable().Where(i => i.MemberExhibitionYearlyId == (item.ParentId ?? -1)).FirstOrDefault();
                            if (details != null)
                            {
                                if (langId == 1)
                                    itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "Location: " + details.StandCode : "Location: -";
                                else
                                    itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "رقم الجناح: " + details.StandCode : "رقم الجناح: -";
                            }
                            itemDto.FeatureId = GetFeatureIdOrBoothId(1, item.Id);
                            //itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "Location: " + details.StandCode : "Location: -";
                            result.Add(itemDto);
                        }
                        return result;
                    }
                    else
                    {
                        var memberResult = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).Select(i => new
                        {
                            Id = i.MemberExhibitionYearlyId,
                            Title = i.PublisherNameAr ?? i.PublisherNameEn,
                            Caption = GetCategoryTitle(i.ExhibitionCategoryId, langId), // Regex.Replace(WithMaxLength(i.ExhibitionCategory.TitleAr, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty),
                            Image = GetLogoForExhibitor(i.FileName, i.MemberRoleId ?? 1),
                            // Image = _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/" + i.FileName,
                            // later needs to change to - details.StandCode;
                            Type = (i.MemberRoleId == 1) ? "Exhibitors" : "Agents",
                            i.ParentId,
                            i.MemberRoleId
                        }).OrderBy(i => i.Title).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                        //if (pageNumber > 0 && pageSize > 0)
                        //    memberResult = memberResult.Skip(pageNumber).Take(pageSize).ToList();

                        foreach (var item in memberResult)
                        {
                            var itemDto = new ItemsDTO();
                            itemDto.Id = item.Id;
                            itemDto.Title = item.Title;
                            itemDto.Caption = item.Caption;
                            itemDto.Image = item.Image;
                            itemDto.Type = item.Type;
                            var details = item.MemberRoleId == 1 ? cachedExhibitionExhibitorDetails.AsQueryable().Where(i => i.MemberExhibitionYearlyId == item.Id).FirstOrDefault() : cachedExhibitionExhibitorDetails.AsQueryable().Where(i => i.MemberExhibitionYearlyId == (item.ParentId ?? -1)).FirstOrDefault();
                            if (details != null)
                            {
                                if (langId == 1)
                                    itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "Location: " + details.StandCode : "Location: -";
                                else
                                    itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "رقم الجناح: " + details.StandCode : "رقم الجناح: -";
                            }
                            itemDto.FeatureId = GetFeatureIdOrBoothId(1, item.Id);
                            // itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "Location: " + details.StandCode : "Location: -";
                            result.Add(itemDto);
                        }

                        return result.OrderBy(i => i.Title).ToList();
                    }
                }
                return null;
            }
        }

        private string GetLogoForExhibitor(string fileName, long memberroleid)
        {
            string exhibitorlogo = _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/";
            string agencylogo = _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/agency-logo.png";
            if (memberroleid == 1)
            {
                if (!string.IsNullOrEmpty(fileName))
                {
                    if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "ExhibitorRegistration\\Logo\\" + fileName))
                        return exhibitorlogo + fileName;
                }
            }
            else if (memberroleid == 2)
            {
                return agencylogo;
            }
            return exhibitorlogo + "exhibitor-logo.png";
        }
        #endregion
        #region Dashboard Search Mobile
        private List<SearchListDTO> GetBooksByTerm(long websiteId, long languageId, string term, int pageNumber = 1, int pageSize = 1000)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (cachedBooks == null)
                    cachedBooks = CacheKeys.GetBookList(_cache);
                if (cachedBookParticipating == null)
                    cachedBookParticipating = CacheKeys.GetBooksParticipatingList(_cache);
                if (cachedExhibitionMemberApplicationYearly == null)
                    cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

                var exhibition = MethodFactory.GetActiveExhibitionNew(websiteId, _cache);
                if (exhibition != null)
                {
                    HashSet<long> exhibitorIds = new HashSet<long>(cachedExhibitionMemberApplicationYearly.AsQueryable().Where(p => (p.Status == "A" || p.Status == "I") && p.ExhibitionId == exhibition.ExhibitionId).Select(x => x.MemberExhibitionYearlyId));

                    List<BooksDTO> result = new List<BooksDTO>();
                    // var predicateBooks = PredicateBuilder.New<XsiExhibitionBooks>();
                    var predicateBooksParticating = PredicateBuilder.New<XsiExhibitionBookParticipating>();
                    var predicate = PredicateBuilder.New<XsiExhibitionBooks>();
                    predicate = predicate.And(i => i.IsActive == "Y");
                    predicate = predicate.And(p => p.IsEnable == "Y");
                    if (exhibitorIds.Count > 0)
                    {
                        if (term != "-1" && languageId == 1)
                        {
                            predicate = predicate.And(i => i.TitleEn != null && (i.TitleEn.Contains(term, StringComparison.OrdinalIgnoreCase)));
                            predicate = predicate.Or(i => i.BookDetails != null && (i.BookDetails.Contains(term, StringComparison.OrdinalIgnoreCase)));
                        }
                        else if (term != "-1" && languageId == 2)
                        {
                            predicate = predicate.And(i => i.TitleAr != null && (i.TitleAr.Contains(term, StringComparison.OrdinalIgnoreCase)));
                            predicate = predicate.Or(i => i.BookDetailsAr != null && i.BookDetailsAr.Contains(term, StringComparison.OrdinalIgnoreCase));
                        }
                        predicateBooksParticating = predicateBooksParticating.And(i => i.IsActive == "Y");
                        predicateBooksParticating = predicateBooksParticating.And(p => p.ExhibitorId != null && exhibitorIds.Contains(p.ExhibitorId.Value));

                        // var bookParticipating = _context.XsiExhibitionBookParticipating.Where(p => exhibitorIds.Contains(p.ExhibitorId.Value) &&
                        //p.IsActive == EnumConversion.ToString(EnumBool.Yes)).Take(10);
                        //  var books = _context.XsiExhibitionBooks.Where(predicate).ToList();

                        //var test = books.AsQueryable().AsQueryable().Where(predicate).ToList();
                        //var test2 = booksparticipating.AsQueryable().Where(predicateBooksParticating).ToList();

                        if (languageId == 1)
                        {
                            var pagecount = (from b in cachedBooks.AsQueryable().Where(predicate)
                                             join bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating) on b.BookId equals bp.BookId
                                             select new SearchListDTO
                                             {
                                                 Id = b.BookId
                                             }).Count();

                            var list = (from b in cachedBooks.AsQueryable().Where(predicate)
                                        join bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating) on b.BookId equals bp.BookId
                                        select new SearchListDTO
                                        {
                                            Id = b.BookId,
                                            Type = "Book",
                                            TypeId = 1,
                                            Image = string.IsNullOrEmpty(b.Thumbnail) ? UploadsCMSPath + "/default-book_en.jpg" : UploadsCMSPath + "/Books/Thumbnails/Small/" + b.Thumbnail,
                                            Title = b.TitleEn,
                                            //SubTitle = b.
                                        }).ToList();
                            //.Skip((pageNumber - 1) * pageSize).Take(pageSize);

                            if (pageNumber > 0 && pageSize > 0)
                                list = list.Skip(pageNumber).Take(pageSize).ToList();
                            //else
                            //    list = list;

                            // return new PageCountAndDataDTO() { pageCount = pagecount, data = list };
                            return list;
                        }
                        else
                        {
                            var pagecount = (from b in cachedBooks.AsQueryable().Where(predicate)
                                             join bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating) on b.BookId equals bp.BookId
                                             select new SearchListDTO
                                             {
                                                 Id = b.BookId
                                             }).Count();
                            var list = (from b in cachedBooks.AsQueryable().Where(predicate)
                                        join bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating) on b.BookId equals bp.BookId
                                        select new SearchListDTO
                                        {
                                            Id = bp.BookId,
                                            Type = "Book",
                                            TypeId = 1,
                                            Image = string.IsNullOrEmpty(b.Thumbnail) ? UploadsCMSPath + "/default-book_ar.jpg" : UploadsCMSPath + "/Books/Thumbnails/Small/" + b.Thumbnail,
                                            Title = b.TitleAr,
                                            //SubTitle = b.
                                        }).ToList();
                            //.Skip((pageNumber - 1) * pageSize).Take(pageSize);
                            if (pageNumber > 0 && pageSize > 0)
                                list = list.Skip(pageNumber).Take(pageSize).ToList();
                            //else
                            //    lsit= list;

                            // return new PageCountAndDataDTO() { pageCount = pagecount, data = list };
                            return list;
                        }
                    }
                }
            }
            return new List<SearchListDTO>();
        }

        private List<SearchListDTO> GetActivitiesByTerm(long websiteId, long languageId, string term, int pageNumber = 1, int pageSize = 1000)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (cachedExhibition == null)
                    cachedExhibition = CacheKeys.GetExhibition(_cache);

                if (cachedEventGuest == null)
                    cachedEventGuest = CacheKeys.GetEventGuestFromCache(_cache);

                if (cachedEvents == null)
                    cachedEvents = CacheKeys.GetEvents(_cache);

                if (cachedEventDate == null)
                    cachedEventDate = CacheKeys.GetEventDate(_cache);

                List<long> categoryIDs = new List<long>();
                List<long> subCategoryIDs = new List<long>();

                var predicateCategory = PredicateBuilder.New<XsiEventCategory>();
                var predicateSubCategory = PredicateBuilder.New<XsiEventSubCategory>();
                var predicateEvent = PredicateBuilder.New<XsiEvent>();
                var predicateEventDate = PredicateBuilder.New<XsiEventDate>();

                predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicateCategory = predicateCategory.And(i => i.WebsiteId == websiteId || i.WebsiteId == 0);

                predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");

                if (subCategoryIDs.Count > 0)
                    predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));

                var predicateExhibition = PredicateBuilder.New<XsiExhibition>();
                predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == websiteId);
                XsiExhibition exhibition = cachedExhibition.AsQueryable().Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                var predicatePrevExhibition = PredicateBuilder.New<XsiExhibition>();
                predicatePrevExhibition = predicatePrevExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 2);
                XsiExhibition prevexhibition = cachedExhibition.AsQueryable().Where(predicatePrevExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                if (exhibition != null)
                {
                    predicateEventDate = predicateEventDate.And(i => (i.StartDate > prevexhibition.EndDate && i.EndDate <= exhibition.EndDate.Value.AddDays(1)));
                }
                //var eventitemIDs = _context.XsiEventGuest.Where(i => i.GuestId == guestid).Select(i => i.EventId.Value).ToList();
                var eventitemIDs = cachedEventGuest.AsQueryable().Select(i => i.EventId.Value).ToList();
                if (eventitemIDs.Count > 0)
                    predicateEvent = predicateEvent.And(i => eventitemIDs.Contains(i.ItemId));

                if (term != "-1" && languageId == 1)
                {
                    predicateEvent = predicateEvent.And(i => i.Title.Contains(term));
                    //predicateEventDate = predicateEventDate.And(i => i.BookDetails.Contains(term));
                }
                else if (term != "-1" && languageId == 2)
                {
                    predicateEvent = predicateEvent.And(i => i.TitleAr.Contains(term));
                    //predicateEventDate = predicateEventDate.And(i => i.s.Contains(term));
                }

                if (languageId == 1)
                {
                    var pagecount = cachedEventDate.AsQueryable().Where(predicateEventDate)
                            .Join(cachedEvents.AsQueryable().Where(predicateEvent)
                             , ed => ed.EventId
                             , ev => ev.ItemId
                             , (ed, ev) => new
                             {
                                 Id = ed.ItemId
                             }).Count();

                    var list = cachedEventDate.AsQueryable().Where(predicateEventDate)
                            .Join(cachedEvents.AsQueryable().Where(predicateEvent)
                             , ed => ed.EventId
                             , ev => ev.ItemId
                             , (ed, ev) => new
                             {
                                 Id = ed.ItemId,
                                 Title = ev.Title,
                                 Type = "Activities",
                                 TypeId = 2,
                                 ed.StartDate,
                                 ed.EndDate
                                 //SubTitle =
                                 //SubTitle = ed.StartDate != null ? (ed.StartDate.Value.ToString("dd MMM") + " - " + (ed.EndDate != null ? ed.EndDate.Value.ToString("dd MMM") : string.Empty)) : string.Empty,
                             }).OrderBy(o => o.StartDate)
                         .Select(s => new SearchListDTO()
                         {
                             Id = s.Id,
                             Title = s.Title,
                             Type = "Activities",
                             TypeId = 2
                             //SubTitle =
                             //SubTitle = ed.StartDate != null ? (ed.StartDate.Value.ToString("dd MMM") + " - " + (ed.EndDate != null ? ed.EndDate.Value.ToString("dd MMM") : string.Empty)) : string.Empty,
                         }).ToList();
                    //.Skip((pageNumber - 1) * pageSize).Take(pageSize);

                    if (pageNumber > 0 && pageSize > 0)
                        list = list.Skip(pageNumber).Take(pageSize).ToList();
                    //else
                    //    return list;

                    //  return new PageCountAndDataDTO() { pageCount = pagecount, data = list };

                    return list;
                }
                else
                {
                    var pagecount = cachedEventDate.AsQueryable().Where(predicateEventDate)
                           .Join(cachedEvents.AsQueryable().Where(predicateEvent)
                            , ed => ed.EventId
                            , ev => ev.ItemId
                            , (ed, ev) => new
                            {
                                Id = ed.ItemId
                            }).Count();

                    var list = cachedEventDate.AsQueryable().Where(predicateEventDate)
                      .Join(cachedEvents.AsQueryable().Where(predicateEvent)
                       , ed => ed.EventId
                       , ev => ev.ItemId
                       , (ed, ev) => new
                       {
                           Id = ed.ItemId,
                           Title = ev.TitleAr,
                           Type = "Activities",
                           TypeId = 2,
                           ed.StartDate,
                           ed.EndDate
                           //SubTitle =
                           //SubTitle = ed.StartDate != null ? (ed.StartDate.Value.ToString("dd MMM") + " - " + (ed.EndDate != null ? ed.EndDate.Value.ToString("dd MMM") : string.Empty)) : string.Empty,
                       }).OrderBy(o => o.StartDate)
                   .Select(s => new SearchListDTO()
                   {
                       Id = s.Id,
                       Title = s.Title,
                       Type = "Activities",
                       TypeId = 2
                       //SubTitle =
                       //SubTitle = ed.StartDate != null ? (ed.StartDate.Value.ToString("dd MMM") + " - " + (ed.EndDate != null ? ed.EndDate.Value.ToString("dd MMM") : string.Empty)) : string.Empty,
                   }).ToList();
                    //.Skip((pageNumber - 1) * pageSize).Take(pageSize);

                    if (pageNumber > 0 && pageSize > 0)
                        list = list.Skip(pageNumber).Take(pageSize).ToList();
                    //else
                    //    return list;

                    //   return new PageCountAndDataDTO() { pageCount = pagecount, data = list };

                    return list;
                }
            }
        }
        private List<SearchListDTO> GetExhibitorsByTerm(long websiteId, long languageId, string term, int pageNumber = 1, int pageSize = 1000)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (cachedExhibitionMemberApplicationYearly == null)
                    cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

                List<SearchListDTO> list = new List<SearchListDTO>();
                #region Current Exhibition
                var exhibition = MethodFactory.GetActiveExhibitionNew(websiteId, _cache);
                #endregion
                if (exhibition != null)
                {
                    var predicate = PredicateBuilder.New<XsiExhibitionMemberApplicationYearly>();
                    predicate = predicate.And(i => (i.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved) || i.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval)));
                    predicate = predicate.And(i => i.MemberRoleId == 1 || i.MemberRoleId == 2);//exhibitor
                    //predicate = predicate.Or(i => i.MemberRoleId == 2);//Agents
                    predicate = predicate.And(i => i.ExhibitionId == exhibition.ExhibitionId);

                    if (term != "-1" && languageId == 1)
                    {
                        predicate = predicate.And(i => (i.PublisherNameEn != null && i.PublisherNameEn.Contains(term, StringComparison.OrdinalIgnoreCase)) || (i.ContactPersonTitle != null && i.ContactPersonTitle.Contains(term, StringComparison.OrdinalIgnoreCase)));
                        //predicateEventDate = predicateEventDate.And(i => i.BookDetails.Contains(term));
                    }
                    else if (term != "-1" && languageId == 2)
                    {
                        predicate = predicate.And(i => (i.PublisherNameAr != null && i.PublisherNameAr.Contains(term, StringComparison.OrdinalIgnoreCase)) || (i.ContactPersonTitleAr != null && i.ContactPersonTitleAr.Contains(term, StringComparison.OrdinalIgnoreCase)));
                        //predicateEventDate = predicateEventDate.And(i => i.s.Contains(term));
                    }

                    if (languageId == 1)
                    {
                        var pagecount = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).Select(i => new SearchListDTO
                        {
                            Id = i.MemberExhibitionYearlyId
                        }).Count();

                        list = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).Select(i => new SearchListDTO
                        {
                            Id = i.MemberExhibitionYearlyId,
                            Type = i.MemberRoleId == 1 ? "Exhibitors" : i.MemberRoleId == 2 ? "Agents" : string.Empty,
                            Title = i.PublisherNameEn,
                            TypeId = 4,
                            //SubTitle = Regex.Replace(WithMaxLength(i.ExhibitionCategory.Title, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty),
                            Image = _appCustomSettings.UploadsPath + "/ExhibitorNew/" + i.FileName,
                        }).ToList();
                        //.Skip((pageNumber - 1) * pageSize).Take(pageSize);

                        if (pageNumber > 0 && pageSize > 0)
                            list = list.Skip(pageNumber).Take(pageSize).ToList();
                        //else
                        //    return list;

                        //  return new PageCountAndDataDTO() { pageCount = pagecount, data = list };

                        return list;
                    }
                    else
                    {
                        var pagecount = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).Select(i => new SearchListDTO
                        {
                            Id = i.MemberExhibitionYearlyId
                        }).Count();

                        list = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).Select(i => new SearchListDTO
                        {
                            Id = i.MemberExhibitionYearlyId,
                            Type = i.MemberRoleId == 1 ? "Exhibitors" : i.MemberRoleId == 2 ? "Agents" : string.Empty,
                            Title = i.PublisherNameAr,
                            TypeId = 4,
                            //SubTitle = Regex.Replace(WithMaxLength(i.ExhibitionCategory.Title, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty),
                            Image = _appCustomSettings.UploadsPath + "/ExhibitorNew/" + i.FileName,
                        }).ToList();
                        //.Skip((pageNumber - 1) * pageSize).Take(pageSize);

                        if (pageNumber > 0 && pageSize > 0)
                            list = list.Skip(pageNumber).Take(pageSize).ToList();
                        //else
                        //    return list;
                        // return new PageCountAndDataDTO() { pageCount = pagecount, data = list };

                        return list;
                    }
                }

                return new List<SearchListDTO>();
            }
        }

        private List<SearchListDTO> GetGuestsByTerm(long websiteId, long languageId, string term, int pageNumber = 1, int pageSize = 1000)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (cachedExhibition == null)
                    cachedExhibition = CacheKeys.GetExhibition(_cache);

                if (cachedGuest == null)
                    cachedGuest = CacheKeys.GetGuestsList(_cache);

                if (cachedEventGuest == null)
                    cachedEventGuest = CacheKeys.GetEventGuestFromCache(_cache);

                if (cachedEvents == null)
                    cachedEvents = CacheKeys.GetEvents(_cache);

                if (cachedEventDate == null)
                    cachedEventDate = CacheKeys.GetEventDate(_cache);

                long LangId = languageId;
                List<long> eventIDs = new List<long>();
                List<long> guestIDs = new List<long>();

                var predicateGuest = PredicateBuilder.New<XsiGuests>();
                var predicateEvent = PredicateBuilder.New<XsiEvent>();
                var predicateEventDate = PredicateBuilder.New<XsiEventDate>();

                var predicateExhibition = PredicateBuilder.New<XsiExhibition>();
                predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == websiteId);
                XsiExhibition exhibition = cachedExhibition.AsQueryable().Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).Where(predicateExhibition).FirstOrDefault();

                if (exhibition != null)
                {
                    predicateEventDate = predicateEventDate.And(i =>
                        i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate.Value.AddDays(1));

                    predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");
                    //predicateEvent = predicateEvent.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);

                    eventIDs = cachedEventDate.AsQueryable().Where(predicateEventDate)
                        .Join(cachedEvents.AsQueryable().Where(predicateEvent)
                            , ed => ed.EventId
                            , ev => ev.ItemId
                            , (ed, ev) => new
                            {
                                ev.ItemId
                            }).Select(i => i.ItemId)
                        .ToList();

                    guestIDs = cachedEventGuest.AsQueryable().Where(i => eventIDs.Contains(i.EventId.Value)).Select(i => i.GuestId.Value).ToList();

                    if (guestIDs.Count > 0)
                        predicateGuest = predicateGuest.And(i => guestIDs.Contains(i.ItemId));
                }
                if (LangId == 1)
                {
                    predicateGuest = predicateGuest.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    //predicateGuest = predicateGuest.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);
                    if (term != "-1")
                    {
                        predicateGuest = predicateGuest.And(i => i.GuestName.Contains(term, StringComparison.OrdinalIgnoreCase) || i.GuestTitle.Contains(term, StringComparison.OrdinalIgnoreCase));
                    }
                    var pagecount = cachedGuest.AsQueryable().Where(predicateGuest)
                                         .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new SearchListDTO()
                                         {
                                             Id = i.ItemId,
                                             Type = "Guests"
                                         }).Count();
                    var list = cachedGuest.AsQueryable().Where(predicateGuest)
                                         .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new SearchListDTO()
                                         {
                                             Id = i.ItemId,
                                             Type = "Guests",
                                             Title = i.GuestName,
                                             SubTitle = i.GuestTitle,
                                             Image = _appCustomSettings.UploadsPath + "/guest/ListThumbnailNew/" + (i.GuestPhoto ?? "guest.jpg"),
                                             TypeId = 3,
                                             //SubTitle = Regex.Replace(WithMaxLength(i.Description, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty),
                                             //  Events = "",
                                             //Caption = i.GuestName
                                         }).ToList();
                    //.Skip((pageNumber - 1) * pageSize).Take(pageSize);

                    if (pageNumber > 0 && pageSize > 0)
                        list = list.Skip(pageNumber).Take(pageSize).ToList();
                    //else
                    //    return list;

                    //  return new PageCountAndDataDTO() { pageCount = pagecount, data = list };

                    return list;
                }
                else
                {
                    predicateGuest = predicateGuest.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    if (term != "-1")
                    {
                        predicateGuest = predicateGuest.And(i => i.GuestNameAr.Contains(term, StringComparison.OrdinalIgnoreCase) || i.GuestTitleAr.Contains(term, StringComparison.OrdinalIgnoreCase));
                    }
                    var pagecount = cachedGuest.AsQueryable().Where(predicateGuest)
                                      .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new SearchListDTO()
                                      {
                                          Id = i.ItemId,
                                          Type = "Guests"
                                      }).Count();
                    var list = cachedGuest.AsQueryable().Where(predicateGuest)
                                         .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new SearchListDTO()
                                         {
                                             Id = i.ItemId,
                                             Type = "Guests",
                                             Title = i.GuestNameAr,
                                             SubTitle = i.GuestTitleAr,
                                             Image = _appCustomSettings.UploadsPath + "/guest/ListThumbnailNew/" + (i.GuestPhotoAr ?? "guest.jpg"),
                                             //SubTitle = Regex.Replace(WithMaxLength(i.DescriptionAr, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty),
                                             TypeId = 3,
                                         }).ToList();
                    //.Skip((pageNumber - 1) * pageSize).Take(pageSize);

                    if (pageNumber > 0 && pageSize > 0)
                        list = list.Skip(pageNumber).Take(pageSize).ToList();
                    //else
                    //    return list;

                    //return new PageCountAndDataDTO() { pageCount = pagecount, data = list };

                    return list;
                }
            }
        }
        #endregion
        #region Old
        private List<FilterItemsDTO> GetExhibitionCategories(long langId)
        {
            if (cachedExhibitionCategory == null)
                cachedExhibitionCategory = CacheKeys.GetExhibitionCategory(_cache);

            if (langId == 1)
            {
                var predicate = PredicateBuilder.New<XsiExhibitionCategory>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = cachedExhibitionCategory.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(x => new FilterItemsDTO()
                {
                    Id = x.ItemId,
                    Title = x.Title
                }).ToList();

                return List;
            }
            else
            {
                var predicate = PredicateBuilder.New<XsiExhibitionCategory>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                var List = cachedExhibitionCategory.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr).Select(x => new FilterItemsDTO()
                {
                    Id = x.ItemId,
                    Title = x.TitleAr
                }).ToList();

                return List;
            }
        }

        private BookItemDetailsDTO GetBookDetails(long langId, long itemId)
        {
            using (ExhibitionBookService BookService = new ExhibitionBookService())
            {
                if (cachedBooks == null)
                    cachedBooks = CacheKeys.GetBookList(_cache);
                if (cachedBookParticipating == null)
                    cachedBookParticipating = CacheKeys.GetBooksParticipatingList(_cache);
                //if (cachedBookAuthors == null)
                //    cachedBookAuthors = CacheKeys.GetBookAuthorsList(_cache);
                //if (cachedAuthors == null)
                //    cachedAuthors = CacheKeys.GetAuthorsList(_cache);

                string isparticipating = "N";
                XsiExhibitionBooks existingEntity = cachedBooks.AsQueryable().Where(i => i.BookId == itemId).FirstOrDefault(); // BookService.GetBookByItemId(itemId);

                if (existingEntity != null)
                {

                    BooksParticipatingService BooksParticipatingService = new BooksParticipatingService();
                    //   var booksParticipating = BooksParticipatingService.GetBooksParticipating(new XsiExhibitionBookParticipating() { BookId = itemId }).FirstOrDefault();

                    var booksParticipating = cachedBookParticipating.AsQueryable().Where(i => i.BookId == itemId && i.IsActive == "Y").FirstOrDefault();
                    isparticipating = booksParticipating != null ? "Y" : "N";

                    //entity.PublisherId = existingEntity.BookPublisherId;
                    //entity.ExhibitorId = booksParticipating.ExhibitorId;
                    //entity.AgencyId = booksParticipating.AgencyId;
                    //entity.BookLanguageId = existingEntity.BookLanguageId;

                    BookItemDetailsDTO entity = new BookItemDetailsDTO();
                    entity.Id = existingEntity.BookId;
                    if (langId == 1)
                        entity.Image = _appCustomSettings.UploadsCMSPath + "/Books/Thumbnails/" + (string.IsNullOrEmpty(existingEntity.Thumbnail) ? "default-book_en.jpg" : existingEntity.Thumbnail);
                    else
                        entity.Image = _appCustomSettings.UploadsCMSPath + "/Books/Thumbnails/" + (string.IsNullOrEmpty(existingEntity.Thumbnail) ? "default-book_ar.jpg" : existingEntity.Thumbnail);

                    entity.Type = "Book";
                    dynamic bookDto;
                    if (langId == 1)
                        bookDto = new BookDetailsDTO();
                    else
                        bookDto = new BookDetailsArDTO();

                    bookDto.Year = existingEntity.IssueYear;
                    bookDto.Price = existingEntity.Price + " AED";
                    bookDto.ISBN = existingEntity.Isbn;

                    if (langId == 1)
                    {
                        if (!string.IsNullOrEmpty(existingEntity.BookDetails))
                            bookDto.Details = existingEntity.BookDetails;
                        else
                            bookDto.Details = "-";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(existingEntity.BookDetailsAr))
                            bookDto.Details = existingEntity.BookDetailsAr;
                        else
                            bookDto.Details = "-";
                    }

                    if (booksParticipating != null)
                    {
                        entity.exhibitorId = booksParticipating.ExhibitorId ?? -1;
                        bookDto.StandCode = GetFeatureIdOrBoothId(2, booksParticipating.ExhibitorId ?? -1);
                        bookDto.ExhibitorName = GetExhibitorName(booksParticipating.ExhibitorId ?? -1, langId);
                    }
                    else
                    {
                        entity.exhibitorId = -1;
                        bookDto.StandCode = "-";
                        bookDto.ExhibitorName = "-";
                    }
                    ExhibitionBookSubjectService exhibitionBookSubjectService = new ExhibitionBookSubjectService();
                    if (existingEntity.ExhibitionSubjectId != null)
                    {
                        var exhibitionSubject = exhibitionBookSubjectService.GetExhibitionBookSubjectByItemId(existingEntity.ExhibitionSubjectId.Value);

                        if (exhibitionSubject != null)
                        {
                            if (langId == 1)
                            {
                                bookDto.Subject = exhibitionSubject.Title;
                                entity.Title = existingEntity.TitleEn;
                            }
                            else
                            {
                                entity.Title = existingEntity.TitleAr;
                                bookDto.Subject = exhibitionSubject.TitleAr;
                            }
                        }
                    }
                    #region Publisher Name
                    if (existingEntity.BookPublisherId != null)
                        if (existingEntity.BookPublisherId != -1)
                        {
                            ExhibitionPublisherService ExhibitionPublisherService = new ExhibitionPublisherService();
                            XsiExhibitionBookPublisher PublisherEntity = ExhibitionPublisherService.GetExhibitionPublisherByItemId(existingEntity.BookPublisherId.Value);
                            if (langId == 1)
                            {
                                if (PublisherEntity != null)
                                    if (PublisherEntity.Title != null)
                                        bookDto.Publisher = PublisherEntity.Title;
                                    else
                                        bookDto.Publisher = string.Empty;
                                else
                                    bookDto.Publisher = string.Empty;
                            }
                            else
                            {
                                if (PublisherEntity != null)
                                    if (PublisherEntity.TitleAr != null)
                                        bookDto.Publisher = PublisherEntity.TitleAr;
                                    else
                                        bookDto.Publisher = string.Empty;
                                else
                                    bookDto.Publisher = string.Empty;
                            }
                        }
                        else
                            bookDto.Publisher = string.Empty;
                    else
                        bookDto.Publisher = string.Empty;
                    #endregion
                    #region Author Name
                    var authorEntity = cachedBookAuthors.AsQueryable().Where(x => x.BookId == existingEntity.BookId).OrderByDescending(x => x.BookId).FirstOrDefault();
                    if (authorEntity != null && authorEntity.AuthorId != -1)
                    {
                        //entity.AuthorId = authorEntity.AuthorId;
                        XsiExhibitionAuthor AuthorEntity = cachedAuthors.AsQueryable().Where(i => i.AuthorId == authorEntity.AuthorId).FirstOrDefault();
                        if (langId == 1)
                        {
                            if (AuthorEntity != null)
                                if (AuthorEntity.Title != null)
                                    bookDto.Author = AuthorEntity.Title;
                                else
                                    bookDto.Author = string.Empty;
                            else
                                bookDto.Author = string.Empty;
                        }
                        else
                        {
                            if (AuthorEntity != null)
                                if (AuthorEntity.TitleAr != null)
                                    bookDto.Author = AuthorEntity.TitleAr;
                                else
                                    bookDto.Author = string.Empty;
                            else
                                bookDto.Author = string.Empty;
                        }
                    }
                    else
                        bookDto.Author = string.Empty;
                    #endregion
                    entity.BookDetailsDTO = bookDto;
                    return entity;
                }
            }
            return new BookItemDetailsDTO();
        }
        private ActivityItemDetailsDTO GetXsiActivity(long langId, long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (cachedGuest == null)
                    cachedGuest = CacheKeys.GetGuestsList(_cache);
                var predicateEvent = PredicateBuilder.New<XsiEvent>();
                if (langId == 1)
                {
                    predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    var event1 = _context.XsiEventDate.Where(i => i.ItemId == id)
                    .Join(_context.XsiEvent.Where(predicateEvent)
                        , ed => ed.EventId
                        , ev => ev.ItemId
                        , (ed, ev) => new
                        {
                            Id = ed.ItemId,
                            EventId = ev.ItemId,
                            Title = ev.Title,
                            TitleAr = ev.TitleAr,
                            CategoryTitle = ev.EventSubCategory.EventCategory.Title,
                            CategoryTitleAr = ev.EventSubCategory.EventCategory.TitleAr,
                            Color = ev.EventSubCategory.EventCategory.Color,
                            SubCategoryTitle = ev.EventSubCategory.Title,
                            ev.Details,
                            StartDate1 = GetDateForActivityUI(ed.StartDate, langId),
                            EndDate1 = GetDateForActivityUI(ed.EndDate, langId),
                            StartDateSubTitle = ed.StartDate,
                            ev.Guest,
                            ev.Location,
                            ev.FileName,
                            EventThumbnailNew = UploadsCMSPath + "/eventnew/" + ev.EventThumbnailNew ?? "activity1.png",
                            ev.InnerTopImageOne,
                            ev.InnerTopImageTwo
                        }).FirstOrDefault();

                    if (event1 != null)
                    {
                        EventGuestService eventGuestService = new EventGuestService();
                        var eventGuest = new XsiEventGuest();
                        eventGuest.EventId = event1.EventId;
                        var eventGuestIdList = eventGuestService.GetEventGuest(eventGuest).Select(s => s.GuestId).ToList();
                        GuestService guestService = new GuestService();
                        var guestsList = cachedGuest.AsQueryable().Where(i => eventGuestIdList.Contains(i.ItemId)).ToList();

                        ActivityItemDetailsDTO itemDetailsDTO = new ActivityItemDetailsDTO();
                        itemDetailsDTO.Id = event1.Id;
                        itemDetailsDTO.Type = "Activity";
                        itemDetailsDTO.Image = event1.EventThumbnailNew;
                        itemDetailsDTO.Color = event1.Color;

                        itemDetailsDTO.Title = event1.Title;
                        itemDetailsDTO.SubTitle = event1.StartDateSubTitle != null ? event1.StartDateSubTitle.Value.ToString("dd MMM") : string.Empty;
                        itemDetailsDTO.Category = event1.CategoryTitle;
                        dynamic activityDetailsDTO;
                        if (langId == 1)
                            activityDetailsDTO = new ActivityDetailsDTO();
                        else
                            activityDetailsDTO = new ActivityDetailsArDTO();
                        activityDetailsDTO.EventDate = event1.StartDate1 + " - " + event1.EndDate1;
                        activityDetailsDTO.EventLocation = event1.Location;
                        activityDetailsDTO.EventDetails = event1.Details;

                        List<ItemsDTO> itemsDTOList = new List<ItemsDTO>();
                        ItemsDTO guestItemsDTO = new ItemsDTO();
                        foreach (var item in guestsList)
                        {
                            guestItemsDTO.Id = item.ItemId;
                            guestItemsDTO.Title = item.GuestName;
                            guestItemsDTO.SubTitle = item.GuestTitle;
                            guestItemsDTO.Image = _appCustomSettings.UploadsPath + "/guest/ListThumbnailNew/" + (item.GuestPhoto ?? "guest.jpg");
                            itemsDTOList.Add(guestItemsDTO);
                        }
                        if (langId == 1)
                            itemDetailsDTO.ActivityDetailsDTO = new ActivityDetailsDTO();
                        else
                            itemDetailsDTO.ActivityDetailsDTO = new ActivityDetailsArDTO();
                        activityDetailsDTO.Guests = new List<ItemsDTO>();
                        activityDetailsDTO.Guests = itemsDTOList;
                        itemDetailsDTO.ActivityDetailsDTO = activityDetailsDTO;

                        return itemDetailsDTO;
                    }
                    return null;
                }
                else
                {
                    predicateEvent = predicateEvent.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    var event1 = _context.XsiEventDate.Where(i => i.ItemId == id)
                    .Join(_context.XsiEvent.Where(predicateEvent)
                        , ed => ed.EventId
                        , ev => ev.ItemId
                        , (ed, ev) => new
                        {
                            Id = ed.ItemId,
                            EventId = ev.ItemId,
                            EventItemId = ed.ItemId,

                            ev.EventSubCategoryId,
                            ev.PhotoAlbumId,
                            ev.VideoAlbumId,
                            ev.Type,
                            ev.IsActiveAr,
                            ev.IsFeatured,
                            Title = ev.TitleAr,
                            Overview = ev.OverviewAr,
                            Details = ev.DetailsAr,
                            CategoryTitle = ev.EventSubCategory.EventCategory.TitleAr,
                            Color = ev.EventSubCategory.EventCategory.ColorAr,
                            SubCategoryTitle = ev.EventSubCategory.TitleAr,
                            StartDate1 = GetDateForActivityUI(ed.StartDate, langId),
                            EndDate1 = GetDateForActivityUI(ed.EndDate, langId),
                            StartDateSubTitle = ed.StartDate,
                            Guest = ev.GuestAr,
                            Location = ev.LocationAr,
                            FileName = ev.FileNameAr,
                            EventThumbnailNew = UploadsCMSPath + "/eventnew/" + ev.EventThumbnailNewAr ?? "activity1.png",
                            InnerTopImageOne = ev.InnerTopImageOneAr,
                            InnerTopImageTw = ev.InnerTopImageTwoAr
                        }).FirstOrDefault();
                    if (event1 != null)
                    {
                        EventGuestService eventGuestService = new EventGuestService();
                        var eventGuest = new XsiEventGuest();
                        eventGuest.EventId = event1.EventId;
                        var eventGuestIdList = eventGuestService.GetEventGuest(eventGuest).Select(s => s.GuestId).ToList();
                        GuestService guestService = new GuestService();
                        var guestsList = cachedGuest.AsQueryable().Where(i => eventGuestIdList.Contains(i.ItemId)).ToList();


                        ActivityItemDetailsDTO itemDetailsDTO = new ActivityItemDetailsDTO();
                        itemDetailsDTO.Id = event1.Id;
                        itemDetailsDTO.Type = "Activity";
                        itemDetailsDTO.Image = event1.EventThumbnailNew;
                        itemDetailsDTO.Color = event1.Color;

                        itemDetailsDTO.Title = event1.Title;
                        itemDetailsDTO.SubTitle = event1.StartDateSubTitle != null ? event1.StartDateSubTitle.Value.ToString("dd MMM") : string.Empty;
                        itemDetailsDTO.Category = event1.CategoryTitle;
                        dynamic activityDetailsDTO;
                        if (langId == 1)
                            activityDetailsDTO = new ActivityDetailsDTO();
                        else
                            activityDetailsDTO = new ActivityDetailsArDTO();
                        activityDetailsDTO.EventDate = event1.StartDate1 + " - " + event1.EndDate1;
                        activityDetailsDTO.EventLocation = event1.Location;
                        activityDetailsDTO.EventDetails = event1.Details;

                        List<ItemsDTO> itemsDTOList = new List<ItemsDTO>();
                        ItemsDTO guestItemsDTO = new ItemsDTO();
                        foreach (var item in guestsList)
                        {
                            guestItemsDTO.Id = item.ItemId;
                            guestItemsDTO.Title = item.GuestNameAr;
                            guestItemsDTO.SubTitle = item.GuestTitleAr;
                            guestItemsDTO.Image = _appCustomSettings.UploadsPath + "/guest/ListThumbnailNew/" + (item.GuestPhotoAr ?? "guest.jpg");
                            itemsDTOList.Add(guestItemsDTO);
                        }
                        if (langId == 1)
                            itemDetailsDTO.ActivityDetailsDTO = new ActivityDetailsDTO();
                        else
                            itemDetailsDTO.ActivityDetailsDTO = new ActivityDetailsArDTO();
                        activityDetailsDTO.Guests = new List<ItemsDTO>();
                        activityDetailsDTO.Guests = itemsDTOList;
                        itemDetailsDTO.ActivityDetailsDTO = activityDetailsDTO;

                        return itemDetailsDTO;
                    }

                    return null;
                }
            }
        }
        private ExhibitorItemDetailsDTO GetExhibitorById(long langId, long exhibitorid)
        {
            ExhibitorRegistrationService ExhibitorRegistrationService;
            string strActivities = string.Empty;
            #region exhibitor
            XsiExhibitionMemberApplicationYearly ExhibitorRegistrationEntity;
            //XsiExhibitorRegistrationEntityComplete ExhibitorRegistrationEntityComplete = new XsiExhibitorRegistrationEntityComplete();
            using (ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly where = new XsiExhibitionMemberApplicationYearly();
                where.MemberExhibitionYearlyId = exhibitorid;
                ExhibitorRegistrationEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorid);
                ExhibitorItemDetailsDTO itemDetailsDTO = new ExhibitorItemDetailsDTO();
                if (ExhibitorRegistrationEntity != null)
                {
                    var memberExhibitionYearlyId = exhibitorid;
                    itemDetailsDTO.Type = "Exhibitor";

                    if (ExhibitorRegistrationEntity.MemberRoleId == 2)
                    {
                        itemDetailsDTO.Type = "Agent";
                        AgencyRegistrationService agency = new AgencyRegistrationService();
                        var agentDetails = agency.GetAgencyRegistrationByItemId(exhibitorid);//parentid=exhibitors
                        if (agentDetails != null)
                        {
                            if (agentDetails.ParentId != null)
                                memberExhibitionYearlyId = agentDetails.ParentId.Value;
                        }
                    }
                    var details = ExhibitorRegistrationService.GetDetailsById(memberExhibitionYearlyId);

                    //if (ExhibitorRegistrationEntity.MemberRoleId == 1)
                    //details = ExhibitorRegistrationService.GetDetailsById(memberExhibitionYearlyId);
                    //XsiExhibitionExhibitorDetails whereDetails = new XsiExhibitionExhibitorDetails();
                    //whereDetails.MemberExhibitionYearlyId = memberExhibitionYearlyId;

                    #region ExhibtiorRegistration Entity
                    #region Exhibitor Table
                    itemDetailsDTO.Id = ExhibitorRegistrationEntity.MemberExhibitionYearlyId;
                    if (langId == 1)
                        itemDetailsDTO.Title = ExhibitorRegistrationEntity.PublisherNameEn;
                    else
                        itemDetailsDTO.Title = ExhibitorRegistrationEntity.PublisherNameAr;

                    #endregion
                    #region Exhibitor details table      
                    if (langId == 1)
                        itemDetailsDTO.ExhibitorDetailsDTO = new ExhibitorDetailsDTO();
                    else
                        itemDetailsDTO.ExhibitorDetailsDTO = new ExhibitorDetailsArDTO();

                    if (details != null)
                    {
                        itemDetailsDTO.Image = GetLogoForExhibitor(details.FileName, ExhibitorRegistrationEntity.MemberRoleId ?? 1); //!string.IsNullOrEmpty(details.FileName) ? UploadsCMSPath + "/ExhibitorRegistration/Logo/" + details.FileName : UploadsCMSPath + "/ExhibitorRegistration/Logo/exhibitor-logo.png";
                        itemDetailsDTO.FeatureId = details.FeatureId;
                        if (ExhibitorRegistrationEntity.CountryId != null)
                        {
                            ExhibitionCountryService exhibitionCountryService = new ExhibitionCountryService();
                            var countryDetails = exhibitionCountryService.GetExhibitionCountryByItemId(ExhibitorRegistrationEntity.CountryId.Value);
                            //itemDetailsDTO.ExhibitorDetailsDTO.Country = countryDetails.CountryName + " (" + countryDetails.CountryCode + ")";
                            if (langId == 1)
                                itemDetailsDTO.ExhibitorDetailsDTO.Country = countryDetails.CountryName;
                            else
                                itemDetailsDTO.ExhibitorDetailsDTO.Country = countryDetails.CountryNameAr;
                        }
                        if (langId == 1)
                            itemDetailsDTO.ExhibitorDetailsDTO.Overview = details.Brief;
                        else
                            itemDetailsDTO.ExhibitorDetailsDTO.Overview = details.BriefAr;
                        //itemDetailsDTO.ExhibitorDetailsDTO.Hall = details.HallNumber;
                        //itemDetailsDTO.ExhibitorDetailsDTO.AllocatedArea = details.Area;
                        //itemDetailsDTO.Color=details.
                        #region Section and Subsection
                        if (details.BoothSectionId != null)
                        {
                            itemDetailsDTO.ExhibitorDetailsDTO.StandNumber = details.StandCode;
                            ExhibitionBoothService ExhibitionBoothService = new ExhibitionBoothService();
                            XsiExhibitionBooth where4 = new XsiExhibitionBooth();
                            where4.ItemId = details.BoothSectionId.Value;
                            XsiExhibitionBooth ExhibitionBoothEntity = ExhibitionBoothService.GetCompleteExhibitionBooth(where4, EnumSortlistBy.ByAlphabetAsc).FirstOrDefault();
                            if (ExhibitionBoothEntity != null)
                            {
                                if (ExhibitionBoothEntity.Title != null && langId == 1)
                                    itemDetailsDTO.ExhibitorDetailsDTO.StandSection = ExhibitionBoothEntity.Title;
                                else if (ExhibitionBoothEntity.TitleAr != null && langId == 2)
                                    itemDetailsDTO.ExhibitorDetailsDTO.StandSection = ExhibitionBoothEntity.TitleAr;
                                else
                                    itemDetailsDTO.ExhibitorDetailsDTO.StandSection = string.Empty;
                                if (details.BoothSubSectionId != null)
                                {
                                    ExhibitionBoothSubsectionService ExhibitionBoothSubsectionService = new ExhibitionBoothSubsectionService();
                                    XsiExhibitionBoothSubsection where5 = new XsiExhibitionBoothSubsection();
                                    where5.CategoryId = ExhibitionBoothEntity.ItemId;
                                    where5.ItemId = details.BoothSubSectionId.Value;
                                    XsiExhibitionBoothSubsection ExhibitionBoothSubEntity = ExhibitionBoothSubsectionService.GetExhibitionBoothSubsection(where5).FirstOrDefault();
                                    if (ExhibitionBoothSubEntity != null)
                                    {
                                        if (ExhibitionBoothSubEntity.Title != null && langId == 1)
                                            itemDetailsDTO.ExhibitorDetailsDTO.StandSubSection = ExhibitionBoothSubEntity.Title;
                                        else if (ExhibitionBoothSubEntity.TitleAr != null && langId == 2)
                                            itemDetailsDTO.ExhibitorDetailsDTO.StandSubSection = ExhibitionBoothSubEntity.TitleAr;
                                        else
                                            itemDetailsDTO.ExhibitorDetailsDTO.StandSubSection = string.Empty;

                                        if (ExhibitionBoothSubEntity.Color != null)
                                            itemDetailsDTO.Color = ExhibitionBoothSubEntity.Color;
                                    }
                                }
                                else
                                    itemDetailsDTO.ExhibitorDetailsDTO.StandSubSection = string.Empty;
                            }
                            else
                            {
                                itemDetailsDTO.ExhibitorDetailsDTO.StandSection = string.Empty;
                                itemDetailsDTO.ExhibitorDetailsDTO.StandSubSection = string.Empty;
                            }
                        }
                        #endregion
                    }
                    #endregion
                    #endregion 
                    if (ExhibitorRegistrationEntity.ExhibitionCategoryId.HasValue)
                    {
                        ExhibitionCategoryService ExhibitionCategoryService = new ExhibitionCategoryService();
                        XsiExhibitionCategory where2 = new XsiExhibitionCategory();
                        where2.ItemId = ExhibitorRegistrationEntity.ExhibitionCategoryId.Value;
                        List<XsiExhibitionCategory> ExhibitionCategoryList = ExhibitionCategoryService.GetExhibitionCategory(where2).ToList();
                        if (ExhibitionCategoryList.Count() > 0)
                        {
                            if (langId == 1)
                                itemDetailsDTO.ExhibitorDetailsDTO.Category = ExhibitionCategoryList[0].Title;
                            else
                                itemDetailsDTO.ExhibitorDetailsDTO.Category = ExhibitionCategoryList[0].TitleAr;
                        }
                    }
                    return itemDetailsDTO;
                }
                return null;
            }
            #endregion

        }
        private LoggedInExhibitorItemDetailsDTO GetExhibitorByLoggedUserId(long webId, long langId, long memberid)
        {
            if (cachedExhibitionMemberApplicationYearly == null)
                cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

            if (cachedExhibitionCategory == null)
                cachedExhibitionCategory = CacheKeys.GetExhibitionCategory(_cache);

            if (cachedExhibitionExhibitorDetails == null)
                cachedExhibitionExhibitorDetails = CacheKeys.GetExhibitionExhibitorDetails(_cache);

            ExhibitorRegistrationService ExhibitorRegistrationService;
            string strActivities = string.Empty;
            #region exhibitor
            XsiExhibitionMemberApplicationYearly ExhibitorRegistrationEntity;
            //XsiExhibitorRegistrationEntityComplete ExhibitorRegistrationEntityComplete = new XsiExhibitorRegistrationEntityComplete();
            long exhibitorId = 0;

            var exhibition = MethodFactory.GetActiveExhibitionNew(webId, _cache);
            var exhibitor = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(i => i.MemberRoleId == 1 && i.ParentId == null && i.MemberId == memberid && i.ExhibitionId == exhibition.ExhibitionId).FirstOrDefault();
            if (exhibitor != null)
                exhibitorId = exhibitor.MemberExhibitionYearlyId;

            using (ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly where = new XsiExhibitionMemberApplicationYearly();
                where.MemberExhibitionYearlyId = exhibitorId;
                ExhibitorRegistrationEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                LoggedInExhibitorItemDetailsDTO itemDetailsDTO = new LoggedInExhibitorItemDetailsDTO();
                if (ExhibitorRegistrationEntity != null)
                {
                    var details = cachedExhibitionExhibitorDetails.AsQueryable().Where(i => i.MemberExhibitionYearlyId == exhibitorId).FirstOrDefault();  // ExhibitorRegistrationService.GetDetailsById(exhibitorId);

                    #region ExhibtiorRegistration Entity
                    #region Exhibitor Table
                    itemDetailsDTO.Id = ExhibitorRegistrationEntity.MemberExhibitionYearlyId;
                    if (langId == 1)
                        itemDetailsDTO.Title = ExhibitorRegistrationEntity.PublisherNameEn;
                    else
                        itemDetailsDTO.Title = string.IsNullOrEmpty(ExhibitorRegistrationEntity.PublisherNameAr) ? ExhibitorRegistrationEntity.PublisherNameEn : ExhibitorRegistrationEntity.PublisherNameAr;
                    itemDetailsDTO.Type = "Exhibitor";
                    #endregion
                    #region Exhibitor details table
                    if (langId == 1)
                        itemDetailsDTO.LoggedInExhibitorDetailsDTO = new LoggedInExhibitorDetailsDTO();
                    else
                        itemDetailsDTO.LoggedInExhibitorDetailsDTO = new LoggedInExhibitorDetailsArDTO();
                    if (details != null)
                    {
                        //itemDetailsDTO.Image = UploadsCMSPath + "/ExhibitorRegistration/Logo/" + details.FileName;
                        itemDetailsDTO.Image = !string.IsNullOrEmpty(details.FileName) ? UploadsCMSPath + "/ExhibitorRegistration/Logo/" + details.FileName : UploadsCMSPath + "/ExhibitorRegistration/Logo/exhibitor-logo.png";
                        itemDetailsDTO.FeatureId = details.FeatureId;
                        itemDetailsDTO.LoggedInExhibitorDetailsDTO.Hall = details.HallNumber;
                        itemDetailsDTO.LoggedInExhibitorDetailsDTO.AllocatedArea = details.Area;
                        //itemDetailsDTO.Color=details.

                        #region Section and Subsection
                        if (details.BoothSectionId != null)
                        {
                            ExhibitionBoothService ExhibitionBoothService = new ExhibitionBoothService();
                            XsiExhibitionBooth where4 = new XsiExhibitionBooth();
                            where4.ItemId = details.BoothSectionId.Value;
                            XsiExhibitionBooth ExhibitionBoothEntity = ExhibitionBoothService.GetCompleteExhibitionBooth(where4, EnumSortlistBy.ByAlphabetAsc).FirstOrDefault();
                            if (ExhibitionBoothEntity != null)
                            {
                                if (ExhibitionBoothEntity.Title != null && langId == 1)
                                    itemDetailsDTO.LoggedInExhibitorDetailsDTO.StandSection = ExhibitionBoothEntity.Title;
                                else if (ExhibitionBoothEntity.TitleAr != null && langId == 2)
                                    itemDetailsDTO.LoggedInExhibitorDetailsDTO.StandSection = ExhibitionBoothEntity.TitleAr;
                                else
                                    itemDetailsDTO.LoggedInExhibitorDetailsDTO.StandSection = string.Empty;
                                if (details.BoothSubSectionId != null)
                                {
                                    ExhibitionBoothSubsectionService ExhibitionBoothSubsectionService = new ExhibitionBoothSubsectionService();
                                    XsiExhibitionBoothSubsection where5 = new XsiExhibitionBoothSubsection();
                                    where5.CategoryId = ExhibitionBoothEntity.ItemId;
                                    where5.ItemId = details.BoothSubSectionId.Value;
                                    XsiExhibitionBoothSubsection ExhibitionBoothSubEntity = ExhibitionBoothSubsectionService.GetExhibitionBoothSubsection(where5).FirstOrDefault();
                                    if (ExhibitionBoothSubEntity != null)
                                    {
                                        if (ExhibitionBoothSubEntity.Title != null && langId == 1)
                                            itemDetailsDTO.LoggedInExhibitorDetailsDTO.StandSubSection = ExhibitionBoothSubEntity.Title;
                                        else if (ExhibitionBoothSubEntity.TitleAr != null && langId == 2)
                                            itemDetailsDTO.LoggedInExhibitorDetailsDTO.StandSubSection = ExhibitionBoothSubEntity.TitleAr;
                                        else
                                            itemDetailsDTO.LoggedInExhibitorDetailsDTO.StandSubSection = string.Empty;

                                        if (ExhibitionBoothSubEntity.Color != null)
                                            itemDetailsDTO.Color = ExhibitionBoothSubEntity.Color;
                                    }
                                }
                                else
                                    itemDetailsDTO.LoggedInExhibitorDetailsDTO.StandSubSection = string.Empty;
                            }
                            else
                            {
                                itemDetailsDTO.LoggedInExhibitorDetailsDTO.StandSection = string.Empty;
                                itemDetailsDTO.LoggedInExhibitorDetailsDTO.StandSubSection = string.Empty;
                            }
                        }
                        #endregion
                    }
                    #endregion
                    #endregion 
                    if (ExhibitorRegistrationEntity.ExhibitionCategoryId.HasValue)
                    {
                        var exhibitionCategory = cachedExhibitionCategory.AsQueryable().Where(i => i.ItemId == ExhibitorRegistrationEntity.ExhibitionCategoryId).FirstOrDefault();    // ExhibitionCategoryService.GetExhibitionCategory(where2).ToList();
                        if (exhibitionCategory != null)
                        {
                            if (langId == 1)
                                itemDetailsDTO.Category = exhibitionCategory.Title ?? string.Empty;
                            else
                                itemDetailsDTO.Category = exhibitionCategory.TitleAr ?? string.Empty;
                        }
                    }
                    return itemDetailsDTO;
                }
                return null;
            }
            #endregion

        }

        private List<XsiEventCategory> GetCategoriesForMasterData(long langId)
        {
            if (cachedEventCategory == null)
                cachedEventCategory = CacheKeys.GetEventCategory(_cache);

            var predicateCategory = PredicateBuilder.New<XsiEventCategory>();
            predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicateCategory = predicateCategory.And(i => i.WebsiteId == 1 || i.WebsiteId == 0);
            return cachedEventCategory.AsQueryable().Where(predicateCategory).ToList();
        }
        private List<ActivityFilterDTO> GetActivitiesForMasterData(long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (cachedExhibition == null)
                    cachedExhibition = CacheKeys.GetExhibition(_cache);

                if (cachedEventCategory == null)
                    cachedEventCategory = CacheKeys.GetEventCategory(_cache);

                if (cachedEventSubCategory == null)
                    cachedEventSubCategory = CacheKeys.GetEventSubCategory(_cache);

                List<long> categoryIDs = new List<long>();
                List<long> subCategoryIDs = new List<long>();

                var predicateCategory = PredicateBuilder.New<XsiEventCategory>();
                var predicateSubCategory = PredicateBuilder.New<XsiEventSubCategory>();
                var predicateEvent = PredicateBuilder.New<XsiEvent>();
                var predicateEventDate = PredicateBuilder.New<XsiEventDate>();

                predicateCategory = predicateCategory.And(i => i.WebsiteId == 1 || i.WebsiteId == 0);
                // predicateSubCategory = predicateSubCategory.And(i => i.WebsiteId == 1 || i.WebsiteId == 0);
                predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");

                // DateTime dtNow = MethodFactory.ArabianTimeNow();
                DateTime dtNow = new DateTime(2019, 10, 30, 0, 0, 0);
                if (langId == 1)
                {
                    predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    predicateSubCategory = predicateSubCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    categoryIDs = cachedEventCategory.AsQueryable().Where(predicateCategory).Select(i => i.ItemId).ToList();

                    if (categoryIDs.Count > 0)
                    {
                        predicateSubCategory = predicateSubCategory.And(i => categoryIDs.Contains(i.EventCategoryId.Value));
                        subCategoryIDs = cachedEventSubCategory.AsQueryable().Where(predicateSubCategory).Select(i => i.ItemId).ToList();
                    }

                    if (subCategoryIDs.Count > 0)
                        predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));

                    var predicateExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 1);
                    XsiExhibition exhibition = cachedExhibition.AsQueryable().Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    //if (exhibition != null)
                    //{
                    //    predicateEventDate = predicateEventDate.And(i =>
                    //        i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate);
                    //}

                    var predicatePrevExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicatePrevExhibition = predicatePrevExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 2);
                    XsiExhibition prevexhibition = cachedExhibition.AsQueryable().Where(predicatePrevExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        predicateEventDate = predicateEventDate.And(i => (i.StartDate >= dtNow && i.EndDate <= exhibition.EndDate.Value.AddDays(1)));
                    }

                    //prevexhibition.EndDate

                    //var eventitemIDs = _context.XsiEventGuest.Where(i => i.GuestId == guestid).Select(i => i.EventId.Value).ToList();
                    //var eventitemIDs = _context.XsiEventGuest.Select(i => i.EventId.Value).ToList();
                    //if (eventitemIDs.Count > 0)
                    //    predicateEvent = predicateEvent.And(i => eventitemIDs.Contains(i.ItemId));

                    var list = _context.XsiEventDate.Where(predicateEventDate)
                         .Join(_context.XsiEvent.Where(predicateEvent)
                             , ed => ed.EventId
                             , ev => ev.ItemId
                             , (ed, ev) => new ActivityFilterDTO
                             {
                                 Id = ed.ItemId,
                                 Title = ev.Title,
                                 CategoryId = ev.EventSubCategory.EventCategoryId,
                                 Caption = ev.EventSubCategory.EventCategory.Title,
                                 Color = ev.EventSubCategory.EventCategory.Color,

                                 SubTitle = ed.StartDate != null ? (ed.StartDate.Value.ToString("dd MMM") + " - " + (ed.EndDate != null ? ed.EndDate.Value.ToString("dd MMM") : string.Empty)) : string.Empty,

                             }).OrderByDescending(o => o.Id)
                         .Select(s => s).ToList();

                    return list;
                }
                else
                {
                    predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateCategory = predicateCategory.And(i => i.WebsiteId == 1 || i.WebsiteId == 0);

                    predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");

                    categoryIDs = cachedEventCategory.AsQueryable().Where(predicateCategory).Select(i => i.ItemId).ToList();
                    if (categoryIDs.Count > 0)
                    {
                        subCategoryIDs = cachedEventSubCategory.AsQueryable().Where(i => categoryIDs.Contains(i.EventCategoryId.Value)).Select(i => i.ItemId).ToList();
                    }

                    if (subCategoryIDs.Count > 0)
                        predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));

                    var predicateExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicateExhibition = predicateExhibition.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 1);
                    XsiExhibition exhibition = cachedExhibition.AsQueryable().Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    var predicatePrevExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicatePrevExhibition = predicatePrevExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 2);
                    XsiExhibition prevexhibition = cachedExhibition.AsQueryable().Where(predicatePrevExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        predicateEventDate = predicateEventDate.And(i => (i.StartDate >= dtNow && i.EndDate <= exhibition.EndDate.Value.AddDays(1)));
                    }
                    //var eventitemIDs = _context.XsiEventGuest.Where(i => i.GuestId == guestid).Select(i => i.EventId.Value).ToList();
                    //var eventitemIDs = _context.XsiEventGuest.Select(i => i.EventId.Value).ToList();
                    //if (eventitemIDs.Count > 0)
                    //    predicateEvent = predicateEvent.And(i => eventitemIDs.Contains(i.ItemId));

                    var list = _context.XsiEventDate.Where(predicateEventDate)
                         .Join(_context.XsiEvent.Where(predicateEvent)
                             , ed => ed.EventId
                             , ev => ev.ItemId
                             , (ed, ev) => new ActivityFilterDTO
                             {
                                 Id = ed.ItemId,
                                 Title = ev.TitleAr,
                                 CategoryId = ev.EventSubCategory.EventCategoryId,
                                 Caption = ev.EventSubCategory.EventCategory.TitleAr,
                                 Color = ev.EventSubCategory.EventCategory.ColorAr,
                                 SubTitle = ed.StartDate != null ? (ed.StartDate.Value.ToString("dd MMM") + " - " + (ed.EndDate != null ? ed.EndDate.Value.ToString("dd MMM") : string.Empty)) : string.Empty,
                             }).OrderByDescending(o => o.Id)
                         .Select(s => s).ToList();

                    return list;
                }
            }
        }

        #region Take 10
        private GuestItemDetailsDTO GetGuestDetails(long langId, long guestId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var predicateEvent = PredicateBuilder.New<XsiEvent>();
                var predicateEventDate = PredicateBuilder.New<XsiEventDate>();
                var predicateExhibition = PredicateBuilder.New<XsiExhibition>();
                predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 1);

                if (cachedGuest == null)
                    cachedGuest = CacheKeys.GetGuestsList(_cache);
                if (cachedEventGuest == null)
                    cachedEventGuest = CacheKeys.GetEventGuestFromCache(_cache);
                if (cachedExhibition == null)
                    cachedExhibition = CacheKeys.GetExhibition(_cache);
                XsiExhibition exhibition = cachedExhibition.AsQueryable().Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                if (exhibition != null)
                {
                    predicateEventDate = predicateEventDate.And(i =>
                        i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate.Value.AddDays(1));


                    var predicate = PredicateBuilder.New<XsiGuests>();
                    predicate = predicate.And(i => i.ItemId == guestId && i.IsActive == EnumConversion.ToString(EnumBool.Yes));



                    var dto = cachedGuest.AsQueryable().Where(predicate).Select(i => new
                    {
                        Id = i.ItemId,
                        Title = langId == 1 ? i.GuestName : i.GuestNameAr,
                        Photo = langId == 1 ? i.GuestPhoto : i.GuestPhotoAr,
                        Bio = langId == 1 ? i.Description : i.DescriptionAr,

                        //Image = UploadsCMSPath + "/guest/ListThumbnailNew/" + (Photo ?? "guest.jpg"),
                        SubTitle = langId == 1 ? i.GuestTitle : i.GuestTitleAr,
                    }).FirstOrDefault();
                    var guestItemDetails = new GuestItemDetailsDTO();
                    if (dto != null)
                    {
                        guestItemDetails.Id = dto.Id;
                        guestItemDetails.Title = dto.Title;
                        guestItemDetails.SubTitle = dto.SubTitle;
                        guestItemDetails.Image = UploadsCMSPath + "/guest/ListThumbnailNew/" + (dto.Photo ?? "guest.jpg");
                        if (langId == 1)
                            guestItemDetails.GuestDetailsDTO = new GuestDetailsDTO();
                        else
                            guestItemDetails.GuestDetailsDTO = new GuestDetailsArDTO();
                        guestItemDetails.GuestDetailsDTO.BIO = Regex.Replace(WithMaxLength(dto.Bio, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty);
                    }
                    var eventIDs = cachedEventGuest.AsQueryable().Where(i => i.GuestId == guestId).Select(i => i.EventId)
                               .ToList();
                    predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");

                    if (eventIDs.Count > 0)
                    {
                        predicateEventDate = predicateEventDate.And(i => eventIDs.Contains(i.EventId));
                        predicateEvent = predicateEvent.And(i => eventIDs.Contains(i.ItemId));
                    }
                    // var Listnew = _context.XsiEvent.Where(predicateEvent).ToList();

                    //var test=Listnew.Where(p=>p.ItemId==)
                    var List = _context.XsiEventDate.Where(predicateEventDate)
                            .Join(_context.XsiEvent.Where(predicateEvent)
                                , ed => ed.EventId
                                , ev => ev.ItemId
                                , (ed, ev) => new GuestActivityDetailsDTO
                                {
                                    Id = ed.ItemId,

                                    Title = langId == 1 ? ev.Title : ev.TitleAr,
                                    Caption = langId == 1 ? ev.EventSubCategory.EventCategory.Title : ev.EventSubCategory.EventCategory.TitleAr,
                                    Color = langId == 1 ? ev.EventSubCategory.EventCategory.Color : ev.EventSubCategory.EventCategory.ColorAr,
                                    SubTitle = GetDateForUI(ed.StartDate, langId) + " - " + GetDateForUI(ed.EndDate, langId),
                                    //Image = ev.FileName,
                                })
                            .OrderBy(i => i.SubTitle)
                            .Select(s => s).Take(10).ToList();

                    if (guestItemDetails != null && guestItemDetails.GuestDetailsDTO != null)
                    {
                        guestItemDetails.GuestDetailsDTO.GuestActivityDetails = new List<GuestActivityDetailsDTO>();
                        guestItemDetails.GuestDetailsDTO.GuestActivityDetails = List;
                    }
                    return guestItemDetails;
                }
            }
            return null;
        }
        #endregion

        #region Search Old
        private dynamic GetBooksByFilterSearch(List<MainRequestDTO> mainRequestDTOList, long websiteId, long langId, int pageNumber = 1, int pageSize = 1000)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (cachedBooks == null)
                    cachedBooks = CacheKeys.GetBookList(_cache);
                if (cachedBookParticipating == null)
                    cachedBookParticipating = CacheKeys.GetBooksParticipatingList(_cache);
                if (cachedExhibitionMemberApplicationYearly == null)
                    cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

                var exhibition = MethodFactory.GetActiveExhibitionNew(websiteId, _cache);
                if (exhibition != null)
                {
                    var title = mainRequestDTOList.Where(p => p.PropertyId == 1).Select(s => s.Value).FirstOrDefault();
                    var author = mainRequestDTOList.Where(p => p.PropertyId == 2).Select(s => s.Value).FirstOrDefault();
                    var subject = mainRequestDTOList.Where(p => p.PropertyId == 3).Select(s => s.ItemId).FirstOrDefault();
                    var bookType = mainRequestDTOList.Where(p => p.PropertyId == 4).Select(s => s.ItemId).FirstOrDefault();
                    var exhibitor = mainRequestDTOList.Where(p => p.PropertyId == 5).Select(s => s.Value).FirstOrDefault();
                    var year = mainRequestDTOList.Where(p => p.PropertyId == 6).Select(s => s.Value).FirstOrDefault();

                    HashSet<long> exhibitorIds = new HashSet<long>(cachedExhibitionMemberApplicationYearly.AsQueryable().Where(p => (p.Status == "A" || p.Status == "I") && p.ExhibitionId == exhibition.ExhibitionId).Select(x => x.MemberExhibitionYearlyId));

                    List<BooksDTO> result = new List<BooksDTO>();
                    // var predicateBooks = PredicateBuilder.New<XsiExhibitionBooks>();
                    var predicateBooksParticating = PredicateBuilder.New<XsiExhibitionBookParticipating>();
                    var predicate = PredicateBuilder.New<XsiExhibitionBooks>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(p => p.IsEnable == "Y");
                    if (!string.IsNullOrEmpty(exhibitor))
                    {
                        if (langId == 1)
                            exhibitorIds = new HashSet<long>(cachedExhibitionMemberApplicationYearly.AsQueryable().Where(p => (p.Status == "A" || p.Status == "I") && (p.ContactPersonName == exhibitor || p.PublisherNameEn.Contains(exhibitor))).Select(x => x.MemberExhibitionYearlyId));
                        else
                            exhibitorIds = new HashSet<long>(cachedExhibitionMemberApplicationYearly.AsQueryable().Where(p => (p.Status == "A" || p.Status == "I") && (p.ContactPersonNameAr == exhibitor || p.PublisherNameAr.Contains(exhibitor))).Select(x => x.MemberExhibitionYearlyId));
                    }
                    if (exhibitorIds.Count > 0)
                    {
                        if (bookType != null)
                            predicate = predicate.And(i => i.BookTypeId == bookType);
                        if (!string.IsNullOrEmpty(year))
                            predicate = predicate.And(i => i.IssueYear != null && i.IssueYear.Contains(year));
                        if (subject != null)
                            predicate = predicate.And(i => i.ExhibitionSubjectId == subject);
                        if (langId == 1)
                        {
                            if (!string.IsNullOrEmpty(title))
                                predicate = predicate.And(i => i.TitleEn != null && i.TitleEn.Contains(title, StringComparison.OrdinalIgnoreCase));
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(title))
                                predicate = predicate.And(i => i.TitleAr != null && i.TitleAr.Contains(title, StringComparison.OrdinalIgnoreCase));
                        }
                        predicateBooksParticating = predicateBooksParticating.And(p => p.ExhibitorId != null && exhibitorIds.Contains(p.ExhibitorId.Value));

                        // var bookParticipating = _context.XsiExhibitionBookParticipating.Where(p => exhibitorIds.Contains(p.ExhibitorId.Value) &&
                        //p.IsActive == EnumConversion.ToString(EnumBool.Yes)).Take(10);
                        // var books = _context.XsiExhibitionBooks.Where(predicate).ToList();

                        if (langId == 1)
                        {
                            var pagecount = (from bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating)
                                             join b in cachedBooks.AsQueryable().Where(predicate) on bp.BookId equals b.BookId
                                             select new SearchListDTO
                                             {
                                                 Id = bp.BookId,
                                                 Type = "Book",
                                                 //Image = string.IsNullOrEmpty(b.Thumbnail) ? UploadsCMSPath + "/default-book_en.jpg" : UploadsCMSPath + "/Books/Thumbnails/Small/" + b.Thumbnail,
                                                 //Title = b.TitleAr,
                                                 //SubTitle = b.
                                             }).Count();

                            var list = (from bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating)
                                        join b in cachedBooks.AsQueryable().Where(predicate) on bp.BookId equals b.BookId
                                        select new SearchListDTO
                                        {
                                            Id = bp.BookId,
                                            Type = "Book",
                                            Image = string.IsNullOrEmpty(b.Thumbnail) ? UploadsCMSPath + "/default-book_en.jpg" : UploadsCMSPath + "/Books/Thumbnails/Small/" + b.Thumbnail,
                                            Title = b.TitleEn,
                                            //SubTitle = b.
                                        }).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                            //if (pageNumber > 0 && pageSize > 0)
                            //    return list.Skip(pageNumber).Take(pageSize).ToList();
                            //else


                            //return list;
                            return new { pageCount = pagecount, data = list };
                        }
                        else
                        {
                            var pagecount = (from bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating)
                                             join b in cachedBooks.AsQueryable().Where(predicate) on bp.BookId equals b.BookId
                                             select new SearchListDTO
                                             {
                                                 Id = bp.BookId,
                                                 Type = "Book",
                                                 //Image = string.IsNullOrEmpty(b.Thumbnail) ? UploadsCMSPath + "/default-book_en.jpg" : UploadsCMSPath + "/Books/Thumbnails/Small/" + b.Thumbnail,
                                                 //Title = b.TitleAr,
                                                 //SubTitle = b.
                                             }).Count();
                            var list = (from bp in cachedBookParticipating.AsQueryable().Where(predicateBooksParticating)
                                        join b in cachedBooks.AsQueryable().Where(predicate) on bp.BookId equals b.BookId
                                        select new SearchListDTO
                                        {
                                            Id = bp.BookId,
                                            Type = "Book",
                                            Image = string.IsNullOrEmpty(b.Thumbnail) ? UploadsCMSPath + "/default-book_ar.jpg" : UploadsCMSPath + "/Books/Thumbnails/Small/" + b.Thumbnail,
                                            Title = b.TitleAr,
                                            //SubTitle = b.
                                        }).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                            //if (pageNumber > 0 && pageSize > 0)
                            //    return list.Skip(pageNumber).Take(pageSize).ToList();
                            //else


                            //return list;
                            return new { pageCount = pagecount, data = list };
                        }
                    }
                }
            }
            return null;
        }

        private dynamic GetActivitiesByFilterSearch(long categoryId, long langId, int pageNumber = 1, int pageSize = 1000)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (cachedExhibition == null)
                    cachedExhibition = CacheKeys.GetExhibition(_cache);

                if (langId == 1)
                {
                    List<long> categoryIDs = new List<long>();
                    List<long> subCategoryIDs = new List<long>();

                    var predicateCategory = PredicateBuilder.New<XsiEventCategory>();
                    var predicateSubCategory = PredicateBuilder.New<XsiEventSubCategory>();
                    var predicateEvent = PredicateBuilder.New<XsiEvent>();
                    var predicateEventDate = PredicateBuilder.New<XsiEventDate>();

                    predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateCategory = predicateCategory.And(i => i.WebsiteId == 1 || i.WebsiteId == 0);


                    predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");

                    //if (subCategoryIDs.Count > 0)
                    //    predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));


                    var predicateExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 1);
                    XsiExhibition exhibition = cachedExhibition.AsQueryable().Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();


                    var predicatePrevExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicatePrevExhibition = predicatePrevExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 2);
                    XsiExhibition prevexhibition = cachedExhibition.AsQueryable().Where(predicatePrevExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        predicateEventDate = predicateEventDate.And(i => (i.StartDate > prevexhibition.EndDate && i.EndDate <= exhibition.EndDate.Value.AddDays(1)));
                    }
                    //var eventitemIDs = _context.XsiEventGuest.Where(i => i.GuestId == guestid).Select(i => i.EventId.Value).ToList();
                    //var eventitemIDs = _context.XsiEventGuest.Select(i => i.EventId.Value).ToList();
                    //if (eventitemIDs.Count > 0)
                    //    predicateEvent = predicateEvent.And(i => eventitemIDs.Contains(i.ItemId));
                    predicateEvent = predicateEvent.And(i => i.EventSubCategory.EventCategoryId == categoryId);

                    var pagecount = _context.XsiEventDate.Where(predicateEventDate)
                         .Join(_context.XsiEvent.Where(predicateEvent)
                             , ed => ed.EventId
                             , ev => ev.ItemId
                             , (ed, ev) => new
                             {
                                 Id = ed.ItemId
                             }).Count();

                    var list = _context.XsiEventDate.Where(predicateEventDate)
                         .Join(_context.XsiEvent.Where(predicateEvent)
                             , ed => ed.EventId
                             , ev => ev.ItemId
                             , (ed, ev) => new
                             {
                                 Id = ed.ItemId,
                                 Title = ev.Title,
                                 Caption = ev.EventSubCategory.EventCategory.Title,
                                 Color = ev.EventSubCategory.EventCategory.Color,
                                 SubTitle = ed.StartDate != null ? (ed.StartDate.Value.ToString("dd MMM") + " - " + (ed.EndDate != null ? ed.EndDate.Value.ToString("dd MMM") : string.Empty)) : string.Empty,
                                 ed.StartDate,
                                 ed.EndDate
                             }).OrderBy(i => i.StartDate).ThenBy(i => i.EndDate)
                         .Select(s => new ItemsDTO
                         {
                             Id = s.Id,
                             Title = s.Title,
                             Caption = s.Caption,
                             Color = s.Color,
                             SubTitle = s.SubTitle
                         }).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                    //if (pageNumber > 0 && pageSize > 0)
                    //    return list.Skip(pageNumber).Take(pageSize).ToList();
                    //else

                    //return list;
                    return new { pageCount = pagecount, data = list };
                }
                else
                {
                    List<long> categoryIDs = new List<long>();
                    List<long> subCategoryIDs = new List<long>();

                    var predicateCategory = PredicateBuilder.New<XsiEventCategory>();
                    var predicateSubCategory = PredicateBuilder.New<XsiEventSubCategory>();
                    var predicateEvent = PredicateBuilder.New<XsiEvent>();
                    var predicateEventDate = PredicateBuilder.New<XsiEventDate>();

                    predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateCategory = predicateCategory.And(i => i.WebsiteId == 1 || i.WebsiteId == 0);

                    predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");

                    //if (subCategoryIDs.Count > 0)
                    //    predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));


                    var predicateExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicateExhibition = predicateExhibition.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 1);
                    XsiExhibition exhibition = cachedExhibition.AsQueryable().Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    var predicatePrevExhibition = PredicateBuilder.New<XsiExhibition>();
                    predicatePrevExhibition = predicatePrevExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == 2);
                    XsiExhibition prevexhibition = cachedExhibition.AsQueryable().Where(predicatePrevExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        predicateEventDate = predicateEventDate.And(i => (i.StartDate > prevexhibition.EndDate && i.EndDate <= exhibition.EndDate.Value.AddDays(1)));
                    }
                    //var eventitemIDs = _context.XsiEventGuest.Where(i => i.GuestId == guestid).Select(i => i.EventId.Value).ToList();
                    //var eventitemIDs = _context.XsiEventGuest.Select(i => i.EventId.Value).ToList();
                    //if (eventitemIDs.Count > 0)
                    //    predicateEvent = predicateEvent.And(i => eventitemIDs.Contains(i.ItemId));

                    predicateEvent = predicateEvent.And(i => i.EventSubCategory.EventCategoryId == categoryId);

                    var pagecount = _context.XsiEventDate.Where(predicateEventDate)
                         .Join(_context.XsiEvent.Where(predicateEvent)
                             , ed => ed.EventId
                             , ev => ev.ItemId
                             , (ed, ev) => new
                             {
                                 Id = ed.ItemId,
                                 //  Title = ev.TitleAr,
                                 //Caption = ev.EventSubCategory.EventCategory.TitleAr,
                                 //Color = ev.EventSubCategory.EventCategory.Color,
                                 //SubTitle = ed.StartDate != null ? (ed.StartDate.Value.ToString("dd MMM") + " - " + (ed.EndDate != null ? ed.EndDate.Value.ToString("dd MMM") : string.Empty)) : string.Empty,
                                 //ed.StartDate,
                                 //ed.EndDate
                             }).Count();

                    var list = _context.XsiEventDate.Where(predicateEventDate)
                         .Join(_context.XsiEvent.Where(predicateEvent)
                             , ed => ed.EventId
                             , ev => ev.ItemId
                             , (ed, ev) => new
                             {
                                 Id = ed.ItemId,
                                 Title = ev.TitleAr,
                                 Caption = ev.EventSubCategory.EventCategory.TitleAr,
                                 Color = ev.EventSubCategory.EventCategory.Color,
                                 SubTitle = ed.StartDate != null ? (ed.StartDate.Value.ToString("dd MMM") + " - " + (ed.EndDate != null ? ed.EndDate.Value.ToString("dd MMM") : string.Empty)) : string.Empty,
                                 ed.StartDate,
                                 ed.EndDate
                             }).OrderBy(i => i.StartDate).ThenBy(i => i.EndDate)
                         .Select(s => new ItemsDTO
                         {
                             Id = s.Id,
                             Title = s.Title,
                             Caption = s.Caption,
                             Color = s.Color,
                             SubTitle = s.SubTitle
                         }).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

                    //if (pageNumber > 0 && pageSize > 0)
                    //    return list.Skip(pageNumber).Take(pageSize).ToList();
                    //else


                    return new { pageCount = pagecount, data = list };
                }
            }
        }

        private dynamic GetExhibitorsByFilterSearch(List<MainRequestDTO> mainRequestDTOList, long websiteId, long langId, int pageNumber = 1, int pageSize = 1000)
        {
            if (cachedExhibitionMemberApplicationYearly == null)
                cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

            if (cachedExhibitionExhibitorDetails == null)
                cachedExhibitionExhibitorDetails = CacheKeys.GetExhibitionExhibitorDetails(_cache);
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                List<ItemsDTO> result = new List<ItemsDTO>();
                #region Current Exhibition
                var exhibition = MethodFactory.GetActiveExhibitionNew(websiteId, _cache);
                #endregion
                if (exhibition != null)
                {
                    var categoryId = mainRequestDTOList.Where(p => p.PropertyId == 1).Select(s => s.ItemId).FirstOrDefault();
                    var countryId = mainRequestDTOList.Where(p => p.PropertyId == 2).Select(s => s.ItemId).FirstOrDefault();
                    var standSectionId = mainRequestDTOList.Where(p => p.PropertyId == 3).Select(s => s.ItemId).FirstOrDefault();
                    var standSubSectionId = mainRequestDTOList.Where(p => p.PropertyId == 4).Select(s => s.ItemId).FirstOrDefault();

                    var predicate = PredicateBuilder.New<XsiExhibitionMemberApplicationYearly>();
                    predicate = predicate.And(i => (i.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved) || i.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval)));
                    predicate = predicate.And(i => i.MemberRoleId == 1 || i.MemberRoleId == 2);//exhibitor
                    //predicate = predicate.Or(i => i.MemberRoleId == 2);//Agents
                    predicate = predicate.And(i => i.ExhibitionId == exhibition.ExhibitionId);
                    if (categoryId != null)
                        predicate = predicate.And(i => i.ExhibitionCategoryId == categoryId);
                    if (countryId != null)
                        predicate = predicate.And(i => i.CountryId == countryId);
                    //predicate = predicate.And(i => i.XsiExhibitionExhibitorDetails == exhibitorsRequestDTO.StandSectionId);
                    var exhibitorIds = new List<long?>();
                    var exhibitors = cachedExhibitionExhibitorDetails.AsQueryable().ToList();

                    if (standSectionId != null && standSectionId > 0 && standSubSectionId != null && standSubSectionId > 0)
                        exhibitorIds = exhibitors.Where(p => p.BoothSectionId == standSectionId && p.BoothSubSectionId == standSubSectionId).Select(s => s.MemberExhibitionYearlyId).ToList();
                    else if (standSectionId != null && standSectionId > 0)
                        exhibitorIds = exhibitors.Where(p => p.BoothSectionId == standSectionId).Select(s => s.MemberExhibitionYearlyId).ToList();
                    var exhibitorDetailsService = new ExhibitorRegistrationService();

                    if (langId == 1)
                    {
                        //result = _context.XsiExhibitionMemberApplicationYearly.Where(predicate)
                        var items = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).ToList();
                        if (exhibitorIds.Count > 0)
                            items = items.Where(p => exhibitorIds.Contains(p.MemberExhibitionYearlyId)).ToList();

                        var pagecount = items
                           .Select(i => new
                           {
                               Id = i.MemberExhibitionYearlyId,
                               //Title = i.PublisherNameEn,
                               //Caption = i.ExhibitionCategory != null ? Regex.Replace(WithMaxLength(i.ExhibitionCategory.Title, 250).Replace("\r\n\t", ""), "<.*?>", string.Empty) : string.Empty,
                               //Image = i.FileName != null ? _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/" + i.FileName : string.Empty,
                               //SubTitle = "-", // later needs to change to - details.StandCode;
                               //Type = i.MemberRoleId == 1 ? "Exhibitors" : i.MemberRoleId == 2 ? "Agents" : string.Empty
                           }).Count();
                        var memberResult = items
                           .Select(i => new
                           {
                               Id = i.MemberExhibitionYearlyId,
                               Title = i.PublisherNameEn,
                               Caption = i.ExhibitionCategory != null ? Regex.Replace(WithMaxLength(i.ExhibitionCategory.Title, 250).Replace("\r\n\t", ""), "<.*?>", string.Empty) : string.Empty,
                               Image = i.FileName != null ? _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/" + i.FileName : string.Empty,
                               SubTitle = "-", // later needs to change to - details.StandCode;
                               Type = i.MemberRoleId == 1 ? "Exhibitors" : i.MemberRoleId == 2 ? "Agents" : string.Empty
                           }).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();


                        foreach (var item in memberResult)
                        {
                            var itemDto = new ItemsDTO();
                            itemDto.Id = item.Id;
                            itemDto.Title = item.Title;
                            itemDto.Caption = item.Caption;
                            itemDto.Image = item.Image;
                            itemDto.Type = item.Type;
                            var details = cachedExhibitionExhibitorDetails.AsQueryable().Where(i => i.MemberExhibitionYearlyId == item.Id).FirstOrDefault(); //exhibitorDetailsService.GetDetailsById(item.Id);
                            if (details != null)
                            {
                                if (langId == 1)
                                    itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "Location: " + details.StandCode : "Location: -";
                                else
                                    itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "رقم الجناح: " + details.StandCode : "رقم الجناح: -";

                            }
                            // itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "Location: " + details.StandCode : "Location: -";
                            result.Add(itemDto);
                        }

                        //if (pageNumber > 0 && pageSize > 0)
                        //    return result.Skip(pageNumber).Take(pageSize).ToList();
                        //else


                        //return result;

                        return new { pageCount = pagecount, data = result };
                    }
                    else
                    {
                        //result = _context.XsiExhibitionMemberApplicationYearly.Where(predicate)
                        var items = cachedExhibitionMemberApplicationYearly.AsQueryable().Where(predicate).ToList();
                        if (exhibitorIds.Count > 0)
                            items = items.Where(p => exhibitorIds.Contains(p.MemberExhibitionYearlyId)).ToList();

                        var pagecount = items
                          .Select(i => new
                          {
                              Id = i.MemberExhibitionYearlyId,
                              //Title = i.PublisherNameEn,
                              //Caption = i.ExhibitionCategory != null ? Regex.Replace(WithMaxLength(i.ExhibitionCategory.Title, 250).Replace("\r\n\t", ""), "<.*?>", string.Empty) : string.Empty,
                              //Image = i.FileName != null ? _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/" + i.FileName : string.Empty,
                              //SubTitle = "-", // later needs to change to - details.StandCode;
                              //Type = i.MemberRoleId == 1 ? "Exhibitors" : i.MemberRoleId == 2 ? "Agents" : string.Empty
                          }).Count();
                        var memberResult = items
                            .Select(i => new
                            {
                                Id = i.MemberExhibitionYearlyId,
                                Title = i.PublisherNameAr ?? i.PublisherNameEn,
                                Caption = Regex.Replace(WithMaxLength(i.ExhibitionCategory.TitleAr, 250).Replace("\r\n\t", ""), "<.*?>", String.Empty),
                                Image = _appCustomSettings.UploadsPath + "/ExhibitorRegistration/Logo/" + i.FileName,
                                SubTitle = "ابحث اسم العارض: C13", // later needs to change to - details.StandCode;
                                Type = i.MemberRoleId == 1 ? "Exhibitors" : i.MemberRoleId == 2 ? "Agents" : string.Empty
                            }).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();


                        foreach (var item in memberResult)
                        {
                            var itemDto = new ItemsDTO();
                            itemDto.Id = item.Id;
                            itemDto.Title = item.Title;
                            itemDto.Caption = item.Caption;
                            itemDto.Image = item.Image;
                            itemDto.Type = item.Type;
                            var details = cachedExhibitionExhibitorDetails.AsQueryable().Where(i => i.MemberExhibitionYearlyId == item.Id).FirstOrDefault(); //exhibitorDetailsService.GetDetailsById(item.Id);
                            if (details != null)
                            {
                                if (langId == 1)
                                    itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "Location: " + details.StandCode : "Location: -";
                                else
                                    itemDto.SubTitle = !string.IsNullOrEmpty(details.StandCode) ? "رقم الجناح: " + details.StandCode : "رقم الجناح: -";
                            }
                            result.Add(itemDto);
                        }
                        //if (pageNumber > 0 && pageSize > 0)
                        //    return result.Skip(pageNumber).Take(pageSize).ToList();
                        //else


                        //return result;
                        return new { pageCount = pagecount, data = result };
                    }
                }
                return null;
            }
        }

        private MobileSearchFilterDTO FilterDataforBooks(long langId)
        {
            MobileSearchFilterDTO mobileSearchFilterDTO = new MobileSearchFilterDTO();
            if (langId == 1)
            {
                mobileSearchFilterDTO.Properties = new List<PropertiesDTO>();
                mobileSearchFilterDTO.Title = "Search Book Title";
                //mobileSearchFilterDTO.SubTitle = "Lorem ipsum...";
                var propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 1;
                propertiesDTO.Title = "Book Title";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 2;
                propertiesDTO.Title = "Author";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 3;
                propertiesDTO.Title = "Main Subject";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO.FilterItemsDTO = new List<FilterItemsDTO>();
                propertiesDTO.FilterItemsDTO = GetBookSubjects(langId);
                //propertiesDTO = new PropertiesDTO();
                //propertiesDTO.OrderId = 4;
                //propertiesDTO.Title = "Book Type";
                //mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                //propertiesDTO.FilterItemsDTO = new List<FilterItemsDTO>();
                //propertiesDTO.FilterItemsDTO = GetBooksType(langId);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 5;
                propertiesDTO.Title = "Exhibitor";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 6;
                propertiesDTO.Title = "Issue Year";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
            }
            else
            {
                mobileSearchFilterDTO.Properties = new List<PropertiesDTO>();
                mobileSearchFilterDTO.Title = "البحث بناء على";
                //mobileSearchFilterDTO.SubTitle = "Lorem ipsum...";
                var propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 1;
                propertiesDTO.Title = "عنوان الكتاب";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 2;
                propertiesDTO.Title = "المؤلف";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 3;
                propertiesDTO.Title = "الفئة الرئيسية";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO.FilterItemsDTO = new List<FilterItemsDTO>();
                propertiesDTO.FilterItemsDTO = GetBookSubjects(langId);
                //propertiesDTO = new PropertiesDTO();
                //propertiesDTO.OrderId = 4;
                //propertiesDTO.Title = "Book Type";
                //mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                //propertiesDTO.FilterItemsDTO = new List<FilterItemsDTO>();
                //propertiesDTO.FilterItemsDTO = GetBooksType(langId);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 5;
                propertiesDTO.Title = "العارض";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 6;
                propertiesDTO.Title = "سنة النشر";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
            }
            return mobileSearchFilterDTO;
        }

        private MobileSearchFilterDTO FilterDataforActivities(long langId)
        {
            List<XsiEventCategory> categoryList = GetCategoriesForMasterData(langId);
            MobileSearchFilterDTO mobileSearchFilterDTO = new MobileSearchFilterDTO();
            if (langId == 1)
            {
                mobileSearchFilterDTO.Title = "Search Activity Name";
                //mobileSearchFilterDTO.SubTitle = "Lorem ipsum...";
                var activities = GetActivitiesForMasterData(langId);

                if (activities.Count > 0)
                {
                    long count = 0;
                    var groupedActivities = activities.GroupBy(g => new { g.CategoryId, g.Caption, g.Color }).ToList();
                    //List<long> missingCatIDs = new List<long>();
                    //if (categoryIDs.Count != groupedActivities.Count)
                    //{
                    //    var catids = groupedActivities.Select(i => i.Key.CategoryId).ToList();
                    //    missingCatIDs = categoryIDs.Where(i => !catids.Contains(i.ItemId)).Select(i => i.ItemId).ToList();

                    //    categoryIDs = categoryIDs.Where(i => missingCatIDs.Contains(i.ItemId)).ToList();
                    //}

                    mobileSearchFilterDTO.Properties = new List<PropertiesDTO>();
                    foreach (var item in groupedActivities)
                    {
                        var propertiesDto = new PropertiesDTO();
                        propertiesDto.OrderId = item.Key.CategoryId.Value;
                        propertiesDto.Color = item.Key.Color;
                        propertiesDto.Title = item.Key.Caption + " (" + item.Count().ToString() + ")";
                        count = count + item.Count();
                        mobileSearchFilterDTO.Properties.Add(propertiesDto);
                    }
                    //foreach (var item in categoryIDs)
                    //{
                    //    var propertiesDto = new PropertiesDTO();
                    //    propertiesDto.OrderId = item.ItemId;
                    //    propertiesDto.Color = item.Color;
                    //    propertiesDto.Title = item.Title + " (0)";
                    //    mobileSearchFilterDTO.Properties.Add(propertiesDto);
                    //}
                    var propertiesDto1 = new PropertiesDTO();
                    propertiesDto1.OrderId = 0;
                    propertiesDto1.Title = "All (" + count.ToString() + ")";
                    mobileSearchFilterDTO.Properties.Add(propertiesDto1);
                }
            }
            else
            {
                mobileSearchFilterDTO.Title = "اختر الفئة";
                //mobileSearchFilterDTO.SubTitle = "Lorem ipsum...";
                var activities = GetActivitiesForMasterData(langId);
                if (activities.Count > 0)
                {
                    long count = 0;
                    var groupedActivities = activities.GroupBy(g => new { g.CategoryId, g.Caption, g.Color }).ToList();
                    //List<long> missingCatIDs = new List<long>();

                    //if (categoryIDs.Count != groupedActivities.Count)
                    //{
                    //    var catids = groupedActivities.Select(i => i.Key.CategoryId).ToList();
                    //    missingCatIDs = categoryIDs.Where(i => !catids.Contains(i.ItemId)).Select(i => i.ItemId).ToList();

                    //    categoryIDs = categoryIDs.Where(i => missingCatIDs.Contains(i.ItemId)).ToList();
                    //}

                    mobileSearchFilterDTO.Properties = new List<PropertiesDTO>();
                    foreach (var item in groupedActivities)
                    {
                        var propertiesDto = new PropertiesDTO();
                        propertiesDto.OrderId = item.Key.CategoryId.Value;
                        propertiesDto.Color = item.Key.Color;
                        propertiesDto.Title = item.Key.Caption + " (" + item.Count().ToString() + ")";
                        count = count + item.Count();
                        mobileSearchFilterDTO.Properties.Add(propertiesDto);
                    }
                    //foreach (var item in categoryIDs)
                    //{
                    //    var propertiesDto = new PropertiesDTO();
                    //    propertiesDto.OrderId = item.ItemId;
                    //    propertiesDto.Color = item.ColorAr;
                    //    propertiesDto.Title = item.TitleAr + " (0)";
                    //    mobileSearchFilterDTO.Properties.Add(propertiesDto);
                    //}
                    var propertiesDto1 = new PropertiesDTO();
                    propertiesDto1.OrderId = 0;
                    propertiesDto1.Title = "الكل (" + count.ToString() + ")";
                    mobileSearchFilterDTO.Properties.Add(propertiesDto1);
                }
            }

            return mobileSearchFilterDTO;
        }

        private MobileSearchFilterDTO FilterDataforExhibitors(long langId, long webId)
        {
            MobileSearchFilterDTO mobileSearchFilterDTO = new MobileSearchFilterDTO();
            if (langId == 1)
            {
                mobileSearchFilterDTO.Title = "Search base on";
                //mobileSearchFilterDTO.SubTitle = "Lorem ipsum dolor sit amet,...";
                mobileSearchFilterDTO.Properties = new List<PropertiesDTO>();
                var propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 1;
                propertiesDTO.Title = "Category";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO.FilterItemsDTO = new List<FilterItemsDTO>();
                propertiesDTO.FilterItemsDTO = GetExhibitionCategories(langId);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 2;
                propertiesDTO.Title = "Country";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO.FilterItemsDTO = new List<FilterItemsDTO>();
                propertiesDTO.FilterItemsDTO = GetCountries(langId);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 3;
                propertiesDTO.Title = "Stand Section";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO.FilterItemsDTO = new List<FilterItemsDTO>();
                propertiesDTO.FilterItemsDTO = GetStandSection(langId, webId);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 4;
                propertiesDTO.Title = "Stand Subsection";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO.FilterItemsDTO = new List<FilterItemsDTO>();
                propertiesDTO.FilterItemsDTO = GetStandSubSection(langId);  //need to change webId to StandSectionId

            }
            else
            {
                mobileSearchFilterDTO.Title = "البحث بناء على";
                //mobileSearchFilterDTO.SubTitle = "Lorem ipsum dolor sit amet,...";
                mobileSearchFilterDTO.Properties = new List<PropertiesDTO>();
                var propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 1;
                propertiesDTO.Title = "الفئة";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO.FilterItemsDTO = new List<FilterItemsDTO>();
                propertiesDTO.FilterItemsDTO = GetExhibitionCategories(langId);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 2;
                propertiesDTO.Title = "البلد";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO.FilterItemsDTO = new List<FilterItemsDTO>();
                propertiesDTO.FilterItemsDTO = GetCountries(langId);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 3;
                propertiesDTO.Title = "قسم الجناح";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO.FilterItemsDTO = new List<FilterItemsDTO>();
                propertiesDTO.FilterItemsDTO = GetStandSection(langId, webId);
                propertiesDTO = new PropertiesDTO();
                propertiesDTO.OrderId = 4;
                propertiesDTO.Title = "القسم الفرعي";
                mobileSearchFilterDTO.Properties.Add(propertiesDTO);
                propertiesDTO.FilterItemsDTO = new List<FilterItemsDTO>();
                propertiesDTO.FilterItemsDTO = GetStandSubSection(langId);  //need to change webId to StandSectionId
            }

            return mobileSearchFilterDTO;
        }

        private MobileSearchFilterDTO FilterDataforGuests(long langId, long webId)
        {
            MobileSearchFilterDTO mobileSearchFilterDTO = new MobileSearchFilterDTO();
            if (langId == 1)
            {
                mobileSearchFilterDTO.Title = "Search base on";
                //mobileSearchFilterDTO.SubTitle = "Lorem ipsum dolor sit amet,...";
                //mobileSearchFilterDTO.Properties = new PropertiesDTO();
                //mobileSearchFilterDTO.Properties.OrderId = 1;
                //mobileSearchFilterDTO.Properties.Title = "Category";
            }
            else
            {
                mobileSearchFilterDTO.Title = "البحث بناء على";
                //mobileSearchFilterDTO.SubTitle = "Lorem ipsum dolor sit amet,...";
                //mobileSearchFilterDTO.Properties = new PropertiesDTO();
                //mobileSearchFilterDTO.Properties.OrderId = 1;
                //mobileSearchFilterDTO.Properties.Title = "Category";
            }
            return null;
        }

        private List<FilterItemsDTO> GetBookSubjects(long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (langId == 1)
                {
                    var predicate = PredicateBuilder.New<XsiExhibitionBookSubject>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    var List = _context.XsiExhibitionBookSubject.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new FilterItemsDTO()
                    {
                        Id = x.ItemId,
                        Title = x.Title,
                    }).ToList();

                    return List;
                }
                else
                {
                    var predicate = PredicateBuilder.New<XsiExhibitionBookSubject>();
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                    var List = _context.XsiExhibitionBookSubject.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new FilterItemsDTO()
                    {
                        Id = x.ItemId,
                        Title = x.TitleAr,
                    }).ToList();

                    return List;
                }
            }
        }

        private List<FilterItemsDTO> GetBooksType(long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (langId == 1)
                {
                    var predicate = PredicateBuilder.New<XsiExhibitionBookType>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    var List = _context.XsiExhibitionBookType.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new FilterItemsDTO()
                    {
                        Id = x.ItemId,
                        Title = x.Title,
                    }).ToList();

                    return List;
                }
                else
                {
                    var predicate = PredicateBuilder.New<XsiExhibitionBookType>();
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                    var List = _context.XsiExhibitionBookType.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new FilterItemsDTO()
                    {
                        Id = x.ItemId,
                        Title = x.TitleAr,
                    }).ToList();

                    return List;
                }
            }
        }

        private List<FilterItemsDTO> GetCountries(long langId)
        {
            using (sibfnewdbContext _context1 = new sibfnewdbContext())
            {

                var predicate = PredicateBuilder.New<XsiExhibitionCountry>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                if (langId == 1)
                {
                    var List = _context1.XsiExhibitionCountry.AsQueryable().Where(predicate).OrderBy(x => x.CountryName).Select(x => new FilterItemsDTO()
                    {
                        Id = x.CountryId,
                        Title = x.CountryName
                    }).ToList();
                    return List;
                }
                else
                {
                    var List = _context1.XsiExhibitionCountry.AsQueryable().Where(predicate).OrderBy(x => x.CountryNameAr).Select(x => new FilterItemsDTO()
                    {
                        Id = x.CountryId,
                        Title = x.CountryNameAr
                    }).ToList();
                    return List;
                }
            }
        }

        private List<FilterItemsDTO> GetStandSection(long langId, long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (langId == 1)
                {
                    var predicate = PredicateBuilder.New<XsiExhibitionBooth>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.WebsiteType == websiteId);

                    var List = _context.XsiExhibitionBooth.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(x => new FilterItemsDTO()
                    {
                        Id = x.ItemId,
                        Title = x.Title
                    }).ToList();

                    return List;
                }
                else
                {
                    var predicate = PredicateBuilder.New<XsiExhibitionBooth>();
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.WebsiteType == websiteId);

                    var List = _context.XsiExhibitionBooth.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr).Select(x => new FilterItemsDTO()
                    {
                        Id = x.ItemId,
                        Title = x.TitleAr
                    }).ToList();

                    return List;
                }
            }
        }

        private List<FilterItemsDTO> GetStandSubSection(long langId, long categoryId = 0)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (langId == 1)
                {
                    var predicate = PredicateBuilder.New<XsiExhibitionBoothSubsection>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    if (categoryId > 0)
                        predicate = predicate.And(i => i.CategoryId == categoryId);

                    var List = _context.XsiExhibitionBoothSubsection.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(i => new FilterItemsDTO()
                    {
                        ParentId = i.CategoryId.Value,
                        Id = i.ItemId,
                        Title = i.Title,
                        //Color = i.Color,
                        //CategoryTitle = i.Category.Title,
                        //CategoryId = i.Category.ItemId
                    }).ToList();

                    return List;
                }
                else
                {
                    var predicate = PredicateBuilder.New<XsiExhibitionBoothSubsection>();
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.CategoryId == categoryId);

                    var List = _context.XsiExhibitionBoothSubsection.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr).Select(i => new FilterItemsDTO()
                    {
                        ParentId = i.CategoryId.Value,
                        Id = i.ItemId,
                        Title = i.TitleAr,
                    }).ToList();

                    return List;
                }
            }
        }
        #endregion EndSearch

        #endregion
        #region Other Methods
        string GetExhibitorName(long exhibitorid, long langid)
        {
            if (cachedExhibitionMemberApplicationYearly == null)
                cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

            var entity = cachedExhibitionMemberApplicationYearly.Where(p => p.MemberExhibitionYearlyId == exhibitorid).OrderByDescending(i => i.MemberExhibitionYearlyId).FirstOrDefault();
            if (entity != null)
            {
                if (langid == 1)
                    return entity.PublisherNameEn;
                else
                    return entity.PublisherNameAr ?? entity.PublisherNameEn;
            }
            return "-";
        }
        string GetFeatureIdOrBoothId(int type, long exhibitorid)
        {
            if (cachedExhibitionExhibitorDetails == null)
                cachedExhibitionExhibitorDetails = CacheKeys.GetExhibitionExhibitorDetails(_cache);

            var details = cachedExhibitionExhibitorDetails.Where(p => p.MemberExhibitionYearlyId == exhibitorid).OrderByDescending(i => i.ExhibitorDetailsId).FirstOrDefault();
            if (details != null)
            {
                if (type == 1)
                    return details.FeatureId ?? "-";
                else
                    return details.StandCode ?? "-";
            }
            return "-";
        }
        private string GetCategoryTitle(long? exhibitionCategoryId, long langId)
        {
            if (cachedExhibitionCategory == null)
                cachedExhibitionCategory = CacheKeys.GetExhibitionCategory(_cache);

            var predicate = PredicateBuilder.New<XsiExhibitionCategory>();
            predicate = predicate.And(i => i.ItemId == exhibitionCategoryId);
            var category = cachedExhibitionCategory.AsQueryable().Where(predicate).FirstOrDefault();
            if (category != null)
            {
                if (langId == 1)
                    return category.Title ?? string.Empty;
                else
                    return category.TitleAr ?? string.Empty;
            }
            return string.Empty;
        }
        private string GetAuthorNameById(long authorId, long langId)
        {
            //if (cachedAuthors == null)
            //    cachedAuthors = CacheKeys.GetAuthorsList(_cache);

            //var author = cachedAuthors.AsQueryable().Where(x => x.AuthorId == authorId && x.IsActive == "Y" && x.Status == "A").FirstOrDefault();
            //if (author != null)
            //{
            //    if (langId == 1)
            //        return author.Title ?? string.Empty;
            //    else
            //        return author.TitleAr ?? string.Empty;
            //}
            return string.Empty;
        }
        private string GetDateForUI(DateTime? startDate, long langId)
        {
            if (startDate != null)
                return langId == 1 ? startDate.Value.ToString("yyyy-MM-dd") : startDate.Value.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
            return string.Empty;
        }
        private string GetDateForActivityUI(DateTime? startDate, long langId)
        {
            if (startDate != null)
            {
                return langId == 1 ? startDate.Value.ToString("dddd,d MMMM HH:mm") : startDate.Value.ToString("dddd,d MMMM HH:mm", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
            }
            return string.Empty;
        }
        private string GetDaySuffix(int day)
        {
            switch (day)
            {
                case 1:
                case 21:
                case 31:
                    return "st";
                case 2:
                case 22:
                    return "nd";
                case 3:
                case 23:
                    return "rd";
                default:
                    return "th";
            }
        }
        private string WithMaxLength(string value, int maxLength)
        {
            if (!string.IsNullOrEmpty(value))
                return value?.Substring(0, Math.Min(value.Length, maxLength));
            return string.Empty;
        }
        private string GetAuthorName(long bookId, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {

                HashSet<long> authorIds = new HashSet<long>(_context.XsiExhibitionBookAuthor.Where(x => x.BookId == bookId).Select(x => x.AuthorId));
                List<string> authorNames = new List<string>();
                if (langId == 1)
                    authorNames = _context.XsiExhibitionAuthor.Where(x => authorIds.Contains(x.AuthorId) && x.IsActive == "Y" && x.Status == "A").Select(x => x.Title).ToList();
                else
                    authorNames = _context.XsiExhibitionAuthor.Where(x => authorIds.Contains(x.AuthorId) && x.IsActive == "Y" && x.Status == "A").Select(x => x.TitleAr).ToList();

                return String.Join(",", authorNames.ToArray());
            }
        }
        private string GetSubject(long itemid, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var entity = _context.XsiExhibitionBookSubject.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
                if (entity != null)
                    return langId == 1 ? entity.Title : entity.TitleAr;

                return string.Empty;
            }
        }
        private string GetSubSubject(long itemid, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var entity = _context.XsiExhibitionBookSubsubject.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
                if (entity != null)
                    return langId == 1 ? entity.Title : entity.TitleAr;

                return string.Empty;
            }
        }
        private string GetBoothSubSection(long itemid, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var entity = _context.XsiExhibitionBoothSubsection.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
                if (entity != null)
                    return langId == 1 ? entity.Title : entity.TitleAr;

                return string.Empty;
            }
        }
        private string GetBookType(long itemid, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var entity = _context.XsiExhibitionBookType.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
                if (entity != null)
                    return langId == 1 ? entity.Title : entity.TitleAr;

                return string.Empty;
            }
        }
        private string GetBoothSection(long itemid, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var entity = _context.XsiExhibitionBooth.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
                if (entity != null)
                    return langId == 1 ? entity.Title : entity.TitleAr;

                return string.Empty;
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using SIBFAPIEn.Utility;
using SIBFAPIEn.DTO;
using Microsoft.AspNetCore.Cors;
using LinqKit;
using Microsoft.Extensions.Options;
using Contracts;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class VolunteerRegistrationController : ControllerBase
    {
        readonly string PassportPath = "Volunteer\\Passport\\";
        readonly string EmiratesIDDocumentPath = "Volunteer\\EmiratesID\\";
        readonly string EmiratesIDTwoDocumentPath = "Volunteer\\EmiratesIDTwo\\";
        readonly string CVPath = "Volunteer\\CV\\";
        readonly string SecurityDocumentPath = "Volunteer\\SecurityDocument\\";

        private ILoggerManager _logger;
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public VolunteerRegistrationController(IOptions<AppCustomSettings> appCustomSettings, IEmailSender emailSender, ILoggerManager logger, sibfnewdbContext context)
        {
            _emailSender = emailSender;
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        // POST: api/VolunteerRegistration
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public async Task<ActionResult<dynamic>> PostXsiVolunteerRegistration(VolunteerRegistrationDTO dto)
        {
            MessageDTO MessageDTO = new MessageDTO();
            long LangId = 1;
            MessageDTO.MessageTypeResponse = "Error";
            string filename = string.Empty;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            try
            {
                if (IsUniqueEmail(dto.Email))
                {
                    if (IsValidAge(dto.DateOfBirth))
                    {
                        XsiVolunteer entity = new XsiVolunteer();
                        entity.LanguageUrl = LangId;
                        entity.Firstname = dto.FirstName;
                        entity.LastName = dto.LastName;
                        if (LangId == 2)
                        {
                            entity.FirstNameAr = dto.FirstNameAr;
                            entity.LastNameAr = dto.LastNameAr;
                        }
                        entity.Gender = dto.Gender;

                        if (!string.IsNullOrEmpty(dto.DateOfBirth))
                        {
                            DateTime dob = new DateTime();
                            DateTime.TryParse(dto.DateOfBirth, out dob);

                            if (!string.IsNullOrEmpty(dto.DateOfBirth))
                                entity.Dob = dob;
                        }
                        entity.Mobile = dto.MobileISD + "$" + dto.MobileSTD + "$" + dto.Mobile;
                        entity.Email = dto.Email;
                        entity.Organization = dto.Organization;

                        if (dto.Organization == "O" && !string.IsNullOrEmpty(dto.Other))
                            entity.Other = dto.Other;

                        entity.IsUid = dto.IsUID;
                        entity.UniversityNameOrUid = dto.UIDorOtherUniversity;
                        if (!string.IsNullOrEmpty(dto.StartDate))
                            entity.StartDate = Convert.ToDateTime(dto.StartDate);
                        if (!string.IsNullOrEmpty(dto.EndDate))
                            entity.EndDate = Convert.ToDateTime(dto.EndDate);

                        entity.ClaimedNoofHrs = dto.ClaimedNoofHours;
                        entity.PreviousExperience = dto.PreviousVolunteerExperience;
                        entity.OtherNotes = dto.OtherNotes;

                        #region Browse Fields
                        if (!string.IsNullOrEmpty(dto.FileBase64Passport))
                        {
                            filename = SaveImage(dto.FileBase64Passport, dto.FileExtensionofPassport, dto.FirstName, PassportPath, dto.FileExtensionofPassport);
                            entity.Passport = filename;
                        }

                        if (!string.IsNullOrEmpty(dto.EmiratesIDBase64))
                        {
                            filename = SaveImage(dto.EmiratesIDBase64, dto.EmiratesIDExt, dto.FirstName, EmiratesIDDocumentPath, dto.EmiratesIDExt);
                            entity.EmiratesIddocument = filename;
                        }

                        if (!string.IsNullOrEmpty(dto.EmiratesIDTwoBase64))
                        {
                            filename = SaveImage(dto.EmiratesIDTwoBase64, dto.EmiratesIDTwoExt, dto.FirstName, EmiratesIDTwoDocumentPath, dto.EmiratesIDTwoExt);
                            entity.EmiratesIddocumentTwo = filename;
                        }

                        if (!string.IsNullOrEmpty(dto.CVBase64))
                        {
                            filename = SaveImage(dto.CVBase64, dto.CVBaseExt, dto.FirstName, CVPath, dto.CVBaseExt);
                            entity.Cv = filename;
                        }
                        if (!string.IsNullOrEmpty(dto.SecurityDocumentBase64))
                        {
                            filename = SaveImage(dto.SecurityDocumentBase64, dto.SecurityDocumentBaseExt, dto.FirstName, SecurityDocumentPath, dto.SecurityDocumentBaseExt);
                            entity.SecurityApprovalDocument = filename;
                        }
                        entity.Status = EnumConversion.ToString(EnumVolunteerStatus.New);
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        entity.CreatedOn = MethodFactory.ArabianTimeNow();
                        entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                        #endregion
                        _context.XsiVolunteer.Add(entity);
                        await _context.SaveChangesAsync();
                        long itemId = entity.ItemId;
                        if (itemId > 0)
                        {
                            dto.VolunteerId = itemId;
                            XsiVolunteerPeriod period;
                            foreach (var item in dto.Period)
                            {
                                period = new XsiVolunteerPeriod();
                                period.VolunteerId = itemId;
                                period.Shift = item;

                                if (item == 1)
                                {
                                    period.NoofDays = dto.ExhibitionDaysMorningShift.Count.ToString();
                                }
                                else if (item == 2)
                                {
                                    period.NoofDays = dto.ExhibitionDaysEveningShift.Count.ToString();
                                }
                                else if (item == 3)
                                {
                                    period.NoofDays = dto.FullShiftNoofDays;
                                }

                                _context.XsiVolunteerPeriod.Add(period);

                                await _context.SaveChangesAsync();
                                long itemid = period.ItemId;
                                if (item == 1)
                                {
                                    AddVolunteerExhibitionDays(itemid, dto.ExhibitionDaysMorningShift);
                                }
                                else if (item == 2)
                                {
                                    AddVolunteerExhibitionDays(itemid, dto.ExhibitionDaysEveningShift);
                                }
                            }
                            if (LangId == 1)
                                MessageDTO.Message = "Your Request has been submitted. We'll get back to you soon.";
                            else
                                MessageDTO.Message = "Your Request has been submitted. We'll get back to you soon.";

                            MessageDTO.MessageTypeResponse = "Success";
                            return Ok(MessageDTO);
                        }
                        if (LangId == 1)
                            MessageDTO.Message = "Some thing went wrong. Please try again.";
                        else
                            MessageDTO.Message = "Some thing went wrong. Please try again.";

                        MessageDTO.MessageTypeResponse = "Error";
                        return Ok(MessageDTO);
                    }
                    else
                    {
                        if (LangId == 1)
                            MessageDTO.Message = "Age must be 18 and above.";
                        else
                            MessageDTO.Message = "Age must be 18 and above.";

                        MessageDTO.MessageTypeResponse = "Error";
                        return Ok(MessageDTO);
                    }
                }
                else
                {
                    if (LangId == 1)
                        MessageDTO.Message = "Already registered with this email";
                    else
                        MessageDTO.Message = "Already registered with this email";

                    MessageDTO.MessageTypeResponse = "Error";
                    return Ok(MessageDTO);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                if (LangId == 1)
                    MessageDTO.Message = "Some thing went wrong. Please try again.";
                else
                    MessageDTO.Message = "Some thing went wrong. Please try again.";

                MessageDTO.MessageTypeResponse = "Error";
                return Ok(MessageDTO);
            }
        }

        private void AddVolunteerExhibitionDays(long periodid, List<long> ExhibitionShiftDays)
        {
            foreach (var item in ExhibitionShiftDays)
            {
                XsiVolunteerExhibitionDays obj = new XsiVolunteerExhibitionDays();
                obj.VolunteerPerioIdId = periodid;
                obj.ExhibitionDaysId = item;
                _context.XsiVolunteerExhibitionDays.Add(obj);
                _context.SaveChanges();
            }
        }

        [HttpGet]
        [Route("ExhibitionDays")]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetXsiExhibitionDays()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            var predicateExhibition = PredicateBuilder.New<XsiExhibition>();
            predicateExhibition.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
            predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicateExhibition.And(i => i.ExhibitionId == 30); //temporary added
            var exhibition = _context.XsiExhibition.Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

            var predicate = PredicateBuilder.True<XsiExhibitionDays>();
            predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            if (exhibition != null)
                predicate = predicate.And(i => i.ExhibitionId == exhibition.ExhibitionId);

            if (LangId == 1)
            {
                var List = await _context.XsiExhibitionDays.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new
                {
                    value = x.ItemId,
                    label = x.ExhibitionDate.Value.ToString("dd MMM yyyy")
                }).ToListAsync();
                return Ok(List);
            }
            else
            {
                var List = await _context.XsiExhibitionDays.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new
                {
                    value = x.ItemId,
                    label = x.ExhibitionDate.Value.ToString("dd MMM yyyy")
                }).ToListAsync();
                return Ok(List);
            }
        }
        private bool IsUniqueEmail(string email)
        {
            return _context.XsiVolunteer.Where(i => i.Email == email).ToList().Count == 0 ? true : false;
        }

        private bool IsValidAge(string dateOfBirth)
        {
            if (!string.IsNullOrEmpty(dateOfBirth))
            {
                DateTime dob = new DateTime();
                DateTime.TryParse(dateOfBirth, out dob);

                TimeSpan difference = MethodFactory.ArabianTimeNow().Subtract(dob);

                // This is to convert the timespan to datetime object
                DateTime age = DateTime.MinValue + difference;
                int ageInYears = age.Year - 1;
                int ageInMonths = age.Month - 1;
                int ageInDays = age.Day - 1;
                if (ageInYears >= 18)
                    return true;
            }
            return false;
        }

        string SaveImage(string fileBase64, string fileName, string name, string path, string fileExtension)
        {
            if (fileBase64 != null && fileBase64.Length > 0)
            {
                //string fileName = string.Empty;
                byte[] imageBytes;
                if (fileBase64.Contains("data:"))
                {
                    var strInfo = fileBase64.Split(",")[0];
                    imageBytes = Convert.FromBase64String(fileBase64.Split(',')[1]);
                }
                else
                    imageBytes = Convert.FromBase64String(fileBase64);

                fileName = string.Format("{0}_{1}_{2}{3}", name, 1, MethodFactory.GetRandomNumber(),
                    "." + fileExtension);
                System.IO.File.WriteAllBytes(
                    Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                 path) + fileName, imageBytes);

                return fileName;
            }
            return string.Empty;
        }
    }
}

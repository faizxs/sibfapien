﻿using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SIBFAPIEn.DTO;
using Entities.Models;
using Xsi.ServicesLayer;
using Microsoft.Extensions.Localization;
using System.Text;
using SIBFAPIEn.Utility;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using drawingFont = System.Drawing.Font;
using System.Drawing.Imaging;
using Microsoft.AspNetCore.Cors;
using System.Diagnostics;
namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RepresentativeController : ControllerBase
    {
        ExhibitionRepresentativeParticipatingService ExhibitionRepresentativeParticipatingService;
        ExhibitionRepresentativeService ExhibitionRepresentativeService;
        #region Variables
        private ILoggerManager _logger;
        private readonly IEmailSender _emailSender;
        private readonly IStringLocalizer<RepresentativeController> _stringLocalizer;
        private readonly AppCustomSettings _appCustomSettings;
        #endregion

        #region Status
        string strPending = EnumConversion.ToString(EnumRepresentativeStatus.Pending);

        string strDocumentsApproved = EnumConversion.ToString(EnumRepresentativeStatus.DocumentsApproved);
        string strDocumentsUploaded = EnumConversion.ToString(EnumRepresentativeStatus.DocumentsUploaded);

        string strApproved = EnumConversion.ToString(EnumRepresentativeStatus.Approved);
        string strVisaProcessing = EnumConversion.ToString(EnumRepresentativeStatus.VisaProcessing);
        string strVisaUploaded = EnumConversion.ToString(EnumRepresentativeStatus.VisaUploaded);
        string strReIssueVisa = EnumConversion.ToString(EnumRepresentativeStatus.ReIssueVisa);
        string strNoVisaInsuranceUploaded = EnumConversion.ToString(EnumStaffGuestStatus.NoVisaInsuranceUploaded);

        string strFlightDetailsPending = EnumConversion.ToString(EnumRepresentativeStatus.FlightDetailsPending);
        string strPendingDocumentation = EnumConversion.ToString(EnumRepresentativeStatus.PendingDocumentation);

        string strRejected = EnumConversion.ToString(EnumRepresentativeStatus.Rejected);
        string strBlocked = EnumConversion.ToString(EnumRepresentativeStatus.Blocked);

        string strCancelled = EnumConversion.ToString(EnumRepresentativeStatus.Cancelled);
        string strCancelNoCharge = EnumConversion.ToString(EnumRepresentativeStatus.CancelNoCharge);
        string strCancelCharge = EnumConversion.ToString(EnumRepresentativeStatus.CancelCharge);
        string strCancelChargeandVisa = EnumConversion.ToString(EnumRepresentativeStatus.CancelChargeandVisa);

        string strYes = EnumConversion.ToString(EnumBool.Yes);
        #endregion

        #region FilePaths
        readonly string PassportPath = "Representative\\Passport\\";
        readonly string PhotoPath = "Representative\\Photo\\";
        readonly string VisaFormPath = "Representative\\VisaForm\\";
        readonly string FlightTicketsPath = "Representative\\FlightTickets\\";
        readonly string CountryUIDPath = "Representative\\CountryUID\\";
        readonly string AgreementPDFPath = "Representative\\Agreement\\";
        readonly string SignedAgreementPhotoPath = "Representative\\SignedAgreement\\";
        readonly string PassportPathTemp = "Representative\\Passport\\temp\\";
        readonly string VisaPath = "Representative\\Visa\\";
        readonly string HealthInsurancePath = "Representative\\HealthInsurance\\";
        readonly string HealthInsurancePathTwo = "Representative\\HealthInsuranceTwo\\";
        public string ext = "PNG";
        #endregion FilePaths

        #region Email Related
        private string StrEmailContentBody { get; set; }
        private string StrEmailContentEmail { get; set; }
        private string StrEmailContentSubject { get; set; }
        private string StrEmailContentAdmin { get; set; }
        XsiEmailContent EmailContent;
        XsiScrfemailContent ScrfEmailContent;
        #endregion 

        private MessageDTO MessageDTO { get; set; }
        public long SIBFMemberId { get; set; }
        public RepresentativeController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings,
           IStringLocalizer<RepresentativeController> stringLocalizer, IEmailSender emailSender)
        {
            _logger = logger;
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
            _stringLocalizer = stringLocalizer;
        }

        #region Front End Using APIs Get
        [HttpGet("Representative/{representativeId}/{exhibitorId}")]
        public ActionResult<dynamic> GetRepresentative(long representativeId, long exhibitorId)
        {
            try
            {
                LoadRepresentativeDTO LoadRepresentativeDTO = new LoadRepresentativeDTO();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                LoadRepresentativeDTO = LoadContent(representativeId, exhibitorId, LangId, LoadRepresentativeDTO);

                return Ok(LoadRepresentativeDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetRepresentative action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [Route("RepresentativePending/{websiteid}/{memberid}/{exhibitorid}")]
        public ActionResult<dynamic> GetRepresentativePending(int websiteid, long memberid, long exhibitorid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                List<RepresentativeTabDTO> List = new List<RepresentativeTabDTO>();
                List<RepresentativeTabDTO> NoVisaList = new List<RepresentativeTabDTO>();
                XsiExhibition exhibtion = new XsiExhibition();
                SIBFMemberId = memberid;
                long memberId = User.Identity.GetID();
                if (memberId == memberid)
                {
                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        exhibtion = MethodFactory.GetActiveExhibition(websiteid, _context);
                    }

                    using (ExhibitionRepresentativeParticipatingService RepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService())
                    {
                        ExhibitionRepresentativeParticipatingNoVisaService ExhibitionRepresentativeParticipatingNoVisaService = new ExhibitionRepresentativeParticipatingNoVisaService();
                        ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService();

                        var whereRep = new XsiExhibitionRepresentative();
                        whereRep.MemberId = SIBFMemberId;
                        whereRep.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        var representativeIdList = ExhibitionRepresentativeService.GetExhibitionRepresentative(whereRep).Select(s => s.RepresentativeId).ToList();

                        #region Participating
                        var participatingwhere = new XsiExhibitionRepresentativeParticipating();
                        participatingwhere.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        participatingwhere.MemberExhibitionYearlyId = exhibitorid;

                        //representativeIdList.Contains(p.RepresentativeId) && (p.Status == strApproved || p.Status == strVisaProcessing || p.Status == strReIssueVisa || p.Status == strPending || p.Status == strFlightDetailsPending || p.Status == strPendingDocumentation || p.Status == strDocumentsApproved || p.Status == strDocumentsUploaded)).OrderByDescending(o => o.MemberExhibitionYearlyId)

                        List = RepresentativeParticipatingService.GetExhibitionRepresentativeParticipating(participatingwhere).Where(p =>
                        representativeIdList.Contains(p.RepresentativeId) && (p.Status != strVisaUploaded)).OrderByDescending(o => o.MemberExhibitionYearlyId)
                            .Select(i => new RepresentativeTabDTO()
                            {
                                RepresentativeId = i.RepresentativeId,
                                MemberExhibitionYearlyId = i.MemberExhibitionYearlyId,
                                Name = GetRepresentativeName(i.RepresentativeId, i.MemberExhibitionYearlyId, LangId),
                                Passport = i.Passport,
                                Status = MethodFactory.GetReprepresentativeStatus(i.Status, LangId),
                                StatusFlag = i.Status,
                                Date = i.CreatedOn.HasValue ? MethodFactory.GetDateFormatted(i.CreatedOn.Value, LangId) : null,
                                IsTravelDetails = i.IsTravelDetails,
                                IsRemove = MethodFactory.ShowRemoveRepresentative(i.Status)
                            }).ToList();
                        #endregion

                        #region Participating No Visa
                        var participatingnovisawhere = new XsiExhibitionRepresentativeParticipatingNoVisa();
                        participatingnovisawhere.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        participatingnovisawhere.MemberExhibitionYearlyId = exhibitorid;

                        NoVisaList = ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa(participatingnovisawhere).Where(p =>
                        representativeIdList.Contains(p.RepresentativeId) && (p.Status != strApproved)).OrderByDescending(o => o.MemberExhibitionYearlyId)
                            .Select(i => new RepresentativeTabDTO()
                            {
                                RepresentativeId = i.RepresentativeId,
                                MemberExhibitionYearlyId = i.MemberExhibitionYearlyId,
                                Name = GetRepresentativeNameFromParticipatingNoVisa(i.RepresentativeId, i.MemberExhibitionYearlyId, LangId),
                                Passport = i.Passport,
                                Status = MethodFactory.GetReprepresentativeStatus(i.Status, LangId),
                                StatusFlag = i.Status,
                                Date = i.CreatedOn.HasValue ? MethodFactory.GetDateFormatted(i.CreatedOn.Value, LangId) : null,
                                IsTravelDetails = i.IsTravelDetails,
                                IsRemove = MethodFactory.ShowRemoveRepresentative(i.Status)
                            }).ToList();
                        #endregion

                        if (NoVisaList.Count > 0)
                            List.AddRange(NoVisaList);
                    }
                    return Ok(List);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside RepresentativePending action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [Route("RepresentativeParticipating/{websiteid}/{memberid}/{exhibitorid}")]
        public ActionResult<dynamic> GetRepresentativeParticipating(int websiteid, long memberid, long exhibitorid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                List<RepresentativeTabDTO> List = new List<RepresentativeTabDTO>();
                List<RepresentativeTabDTO> NoVisaList = new List<RepresentativeTabDTO>();
                XsiExhibition exhibtion = new XsiExhibition();
                SIBFMemberId = memberid;
                long memberId = User.Identity.GetID();
                if (memberId == memberid)
                {
                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        exhibtion = MethodFactory.GetActiveExhibition(websiteid, _context);
                    }
                    using (ExhibitionRepresentativeParticipatingService RepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService())
                    {
                        ExhibitionRepresentativeParticipatingNoVisaService ExhibitionRepresentativeParticipatingNoVisaService = new ExhibitionRepresentativeParticipatingNoVisaService();
                        ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService();

                        var whereRep = new XsiExhibitionRepresentative();
                        whereRep.MemberId = SIBFMemberId;
                        whereRep.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        var representativeIds = ExhibitionRepresentativeService.GetExhibitionRepresentative(whereRep).Select(s => s.RepresentativeId).ToList();

                        #region Participating
                        var participatingwhere = new XsiExhibitionRepresentativeParticipating();
                        participatingwhere.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        participatingwhere.MemberExhibitionYearlyId = exhibitorid;

                        List = RepresentativeParticipatingService.GetExhibitionRepresentativeParticipating(participatingwhere).Where(p =>
                          representativeIds.Contains(p.RepresentativeId) && (p.Status == strVisaUploaded)).OrderBy(p => p.RepresentativeId)
                            .Select(i => new RepresentativeTabDTO()
                            {
                                RepresentativeId = i.RepresentativeId,
                                MemberExhibitionYearlyId = i.MemberExhibitionYearlyId,
                                Name = GetRepresentativeName(i.RepresentativeId, i.MemberExhibitionYearlyId, LangId),
                                Nationality = MethodFactory.GetCountryName(i.CountryId ?? -1, LangId),
                                Profession = i.Profession,
                                Status = MethodFactory.GetReprepresentativeStatus(i.Status, LangId),
                                StatusFlag = i.Status,
                                Date = i.DateOfIssue.HasValue ? MethodFactory.GetDateFormatted(i.DateOfIssue.Value, LangId) : null,
                                IsTravelDetails = i.IsTravelDetails,
                                IsRemove = MethodFactory.ShowRemoveRepresentative(i.Status)
                            }).ToList();
                        #endregion

                        #region Participating No Visa
                        var participatingnovisawhere = new XsiExhibitionRepresentativeParticipatingNoVisa();
                        participatingnovisawhere.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        participatingnovisawhere.MemberExhibitionYearlyId = exhibitorid;

                        NoVisaList = ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa(participatingnovisawhere).Where(p =>
                          representativeIds.Contains(p.RepresentativeId) && (p.Status == strApproved)).OrderBy(p => p.RepresentativeId)
                            .Select(i => new RepresentativeTabDTO()
                            {
                                RepresentativeId = i.RepresentativeId,
                                MemberExhibitionYearlyId = i.MemberExhibitionYearlyId,
                                Name = GetRepresentativeNameFromParticipatingNoVisa(i.RepresentativeId, i.MemberExhibitionYearlyId, LangId),
                                Nationality = MethodFactory.GetCountryName(i.CountryId ?? -1, LangId),
                                Profession = i.Profession,
                                Status = MethodFactory.GetReprepresentativeStatus(i.Status, LangId),
                                StatusFlag = i.Status,
                                Date = i.DateOfIssue.HasValue ? MethodFactory.GetDateFormatted(i.DateOfIssue.Value, LangId) : null,
                                IsTravelDetails = i.IsTravelDetails,
                                IsRemove = MethodFactory.ShowRemoveRepresentative(i.Status)
                            }).ToList();
                        #endregion

                        if (List.Count > 0 && NoVisaList.Count > 0)
                            List.AddRange(NoVisaList);
                    }
                    return Ok(List);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside RepresentativeParticipating action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [Route("RepresentativeArchive/{websiteid}/{memberid}/{exhibitorid}")]
        public ActionResult<dynamic> GetRepresentativeArchive(int websiteid, long memberid, long exhibitorid)
        {
            try
            {
                CommonHelperForRepresentative commonHelper = new CommonHelperForRepresentative();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                List<RepresentativeTabDTO> List = new List<RepresentativeTabDTO>();
                List<RepresentativeTabDTO> NoVisaList = new List<RepresentativeTabDTO>();
                XsiExhibition exhibtion = new XsiExhibition();
                SIBFMemberId = memberid;
                long memberId = User.Identity.GetID();
                if (memberId == memberid)
                {
                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        exhibtion = MethodFactory.GetActiveExhibition(websiteid, _context);
                    }
                    using (ExhibitionRepresentativeParticipatingService RepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService())
                    {
                        ExhibitionRepresentativeParticipatingNoVisaService ExhibitionRepresentativeParticipatingNoVisaService = new ExhibitionRepresentativeParticipatingNoVisaService();
                        ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService();

                        var whereRep = new XsiExhibitionRepresentative();
                        whereRep.MemberId = SIBFMemberId;
                        whereRep.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        var representativeIdList = ExhibitionRepresentativeService.GetExhibitionRepresentative(whereRep).Select(s => s.RepresentativeId).ToList();

                        #region Participating
                        var participatingwhere = new XsiExhibitionRepresentativeParticipating();
                        participatingwhere.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        participatingwhere.MemberExhibitionYearlyId = exhibitorid;

                        List = RepresentativeParticipatingService.GetExhibitionRepresentativeParticipating(participatingwhere).Where(p =>
                          representativeIdList.Contains(p.RepresentativeId) &&
                        (p.Status == strApproved || p.Status == strNoVisaInsuranceUploaded || p.Status == strPending || p.Status == strFlightDetailsPending || p.Status == strPendingDocumentation
                     || p.Status == strRejected || p.Status == strBlocked)).OrderBy(o => o.NameEn)
                            .Select(i => new RepresentativeTabDTO()
                            {
                                RepresentativeId = i.RepresentativeId,
                                MemberExhibitionYearlyId = i.MemberExhibitionYearlyId,
                                Name = GetRepresentativeName(i.RepresentativeId, i.MemberExhibitionYearlyId, LangId),
                                Nationality = MethodFactory.GetCountryName(i.CountryId ?? -1, LangId),
                                Profession = i.Profession,
                                Status = commonHelper.CheckExhibition(i.MemberExhibitionYearlyId, exhibtion.ExhibitionId) ? MethodFactory.GetReprepresentativeStatus(i.Status, LangId) : (LangId == 1 ? "Former Participant" : "مشارك سابق"),
                                StatusFlag = i.Status,
                                Date = i.DateOfIssue.HasValue ? MethodFactory.GetDateFormatted(i.DateOfIssue.Value, LangId) : null,
                                IsTravelDetails = i.IsTravelDetails,
                                IsParticipated = commonHelper.CheckParticipated(i.RepresentativeId, exhibitorid),
                                IsRemove = MethodFactory.ShowRemoveRepresentative(i.Status)
                            }).ToList();
                        #endregion

                        #region Participating No Visa
                        var participatingnovisawhere = new XsiExhibitionRepresentativeParticipatingNoVisa();
                        participatingnovisawhere.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        participatingnovisawhere.MemberExhibitionYearlyId = exhibitorid;

                        NoVisaList = ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa(participatingnovisawhere).Where(p =>
                          representativeIdList.Contains(p.RepresentativeId) &&
                        (p.Status == strApproved || p.Status == strNoVisaInsuranceUploaded || p.Status == strPending || p.Status == strFlightDetailsPending || p.Status == strPendingDocumentation
                     || p.Status == strRejected || p.Status == strBlocked)).OrderBy(o => o.NameEn)
                            .Select(i => new RepresentativeTabDTO()
                            {
                                RepresentativeId = i.RepresentativeId,
                                MemberExhibitionYearlyId = i.MemberExhibitionYearlyId,
                                Name = GetRepresentativeNameFromParticipatingNoVisa(i.RepresentativeId, i.MemberExhibitionYearlyId, LangId),
                                Nationality = MethodFactory.GetCountryName(i.CountryId ?? -1, LangId),
                                Profession = i.Profession,
                                Status = commonHelper.CheckExhibition(i.MemberExhibitionYearlyId, exhibtion.ExhibitionId) ? MethodFactory.GetReprepresentativeStatus(i.Status, LangId) : (LangId == 1 ? "Former Participant" : "مشارك سابق"),
                                StatusFlag = i.Status,
                                Date = i.DateOfIssue.HasValue ? MethodFactory.GetDateFormatted(i.DateOfIssue.Value, LangId) : null,
                                IsTravelDetails = i.IsTravelDetails,
                                IsParticipated = commonHelper.CheckParticipatedNoVisa(i.RepresentativeId, exhibitorid),
                                IsRemove = MethodFactory.ShowRemoveRepresentative(i.Status)
                            }).ToList();
                        #endregion

                        if (NoVisaList.Count > 0)
                            List.AddRange(NoVisaList);
                    }
                    return Ok(List);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside RepresentativeArchive action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [Route("RepresentativeDetails/{itemid}/{exhibitorId}/{websiteid}")]
        public ActionResult<dynamic> GetRepresentativeWebMethod(long itemid, long exhibitorId, long websiteid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long memberId = User.Identity.GetID();
                XsiRepresentativeNew dto = new XsiRepresentativeNew();
                //long EnglishId = 1;// Convert.ToInt64(System.Web.Configuration.WebConfigurationManager.AppSettings["EnglishId"]);
                dto = GetRepresentativeNewEntity(dto, memberId, itemid, exhibitorId, websiteid, LangId);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetRepresentativeWebMethod action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
        #endregion

        #region Common API to separate in new controller
        [HttpGet]
        [EnableCors("AllowOrigin")]
        [Route("GetArrivalAirport")]
        public ActionResult<dynamic> GetArrivalAirport()
        {
            try
            {
                using (ExhibitionArrivalAirportService ExhibitionArrivalAirportService = new ExhibitionArrivalAirportService())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    XsiExhibitionArrivalAirport where = new XsiExhibitionArrivalAirport();
                    //where.LanguageId = 1;
                    where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    return ExhibitionArrivalAirportService.GetExhibitionArrivalAirport(where).Select(x => new { x.ItemId, x.Title }).OrderBy(o => o.Title.Trim()).ToArray();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetArrivalAirport action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [EnableCors("AllowOrigin")]
        [Route("GetArrivalTerminal")]
        public ActionResult<dynamic> GetArrivalTerminal(long categoryId)
        {
            try
            {
                using (ArrivalTerminalService ArrivalTerminalService = new ArrivalTerminalService())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    XsiArrivalTerminal where = new XsiArrivalTerminal();
                    where.CategoryId = categoryId;
                    where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    return ArrivalTerminalService.GetArrivalTerminal(where).Select(x => new { x.ItemId, x.Title }).OrderBy(o => o.Title.Trim()).ToArray();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetArrivalTerminal action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
        #endregion

        #region APIs to be checked if its in use at front end
        [HttpGet]
        [Route("{websiteid}/{memberid}")]
        public ActionResult<dynamic> RepresentativePageLoad(int websiteid, long memberid)
        {
            try
            {
                CommonHelperForRepresentative commonHelper = new CommonHelperForRepresentative();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long memberId = User.Identity.GetID();
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    if (memberId == memberid)
                    {
                        if (commonHelper.IsExhibitionActive())
                        {
                            var ExhibitorEntity = MethodFactory.GetExhibitor(memberid, websiteid, _context);
                            if (ExhibitorEntity != null)
                            {
                                //if (ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval) || ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved))
                                //{
                                //    var data = MethodFactory.AssignLastDate(string.Empty, string.Empty, RepresentativeDTO.PenaltyDate);
                                //    if (data != null)
                                //    {
                                //        RepresentativeDTO.ExhibitorLastDate = data.Item1;
                                //        RepresentativeDTO.RemainingDays = data.Item2;
                                //    }
                                //    //BindRepresentative();
                                //}
                                //else
                                //    RepresentativeDTO.RedirectUrl = "/en/MemberDashboard";
                            }
                            else
                                return Ok("/en/MemberDashboard");
                        }
                        else
                        {
                            if (LangId == 1)
                                return Ok("Exhibition Unavailable.");
                            else
                                return Ok("المعرض غير متوفر");
                        }
                    }
                    return Ok("Unauthorized access");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside RepresentativePageLoad action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("UploadAgreement")]
        public ActionResult<dynamic> UploadAgreement_Click(UploadAgreementDTO uploadAgreementDTO)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var ExhibitorEntity = new XsiExhibitionMemberApplicationYearly();
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    ExhibitorEntity = MethodFactory.GetExhibitor(uploadAgreementDTO.MemberId, uploadAgreementDTO.WebsiteId, _context);
                }
                using (RepresentativeAgreementService RepresentativeAgreementService = new RepresentativeAgreementService())
                {
                    var xhibitorRegistrationService = new ExhibitorRegistrationService();
                    if (!string.IsNullOrEmpty(uploadAgreementDTO.FileBase64UploadSignedagreement))
                    {
                        XsiRepresentativeAgreement entity = new XsiRepresentativeAgreement();

                        long ExhibitorId = -1;
                        if (ExhibitorEntity != null)
                            ExhibitorId = ExhibitorEntity.MemberExhibitionYearlyId;
                        if (ExhibitorId != -1)
                        {
                            entity = RepresentativeAgreementService.GetRepresentativeAgreement(new XsiRepresentativeAgreement() { ExhibitorId = ExhibitorId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                            if (entity != default(XsiRepresentativeAgreement))
                            {
                                var filename = SaveImage(uploadAgreementDTO.FileBase64UploadSignedagreement, uploadAgreementDTO.FileExtensionOfSignedagreement, string.Empty, SignedAgreementPhotoPath, uploadAgreementDTO.FileExtensionOfSignedagreement);
                                entity.SignedAgreement = filename;
                                RepresentativeAgreementService.UpdateRepresentativeAgreement(entity);
                                uploadAgreementDTO.HiddenAgreement = entity.Agreement;
                                uploadAgreementDTO.HrefAgreementVisibility = true;
                                uploadAgreementDTO.SignedAgreementHRef = SignedAgreementPhotoPath + entity.SignedAgreement;
                                uploadAgreementDTO.AgreementThankyouVisibility = true;
                            }
                        }
                    }
                }
                return Ok(uploadAgreementDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UploadAgreement action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("DownloadAgreement")]
        public ActionResult<dynamic> DownloadAgreement_Click(UploadAgreementDTO uploadAgreementDTO)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                //RepresentativeRegistrationDTO = new RepresentativeRegistrationDTO();
                //BindRepresentative(); 
                var filename = GenerateAndDownloadAgreement(uploadAgreementDTO);
                uploadAgreementDTO.FilePathForDownload = filename;
                return Ok(uploadAgreementDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DownloadAgreement action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
            //SelectTab2();
        }
        #endregion

        #region Post Methods
        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("SaveData")]
        public ActionResult<dynamic> SaveData(SaveRepresentativeDTO representativeDTO)
        {
            long langid = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langid);
            MessageDTO = new MessageDTO();
            try
            {
                if (representativeDTO.IsUAEResident == "Y")
                {
                    MessageDTO = AddorUpdateRepresentativeDataNoVisa(representativeDTO, langid);
                }
                else
                {
                    MessageDTO = IsVisaAndNoVisaValidationPassed(representativeDTO.NationalityId, representativeDTO.IsVisaNeed);
                    if (MessageDTO.MessageTypeResponse == "Error")
                    {
                        return Ok(MessageDTO);
                    }
                    if (representativeDTO.IsVisaNeed)
                    {

                        MessageDTO = AddorUpdateRepresentativeData(representativeDTO, langid);
                    }
                    else
                        MessageDTO = AddorUpdateRepresentativeDataNoVisa(representativeDTO, langid);
                }
                return Ok(MessageDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside SaveData action: {ex.InnerException}");
                MessageDTO.MessageTypeResponse = "Error";
                MessageDTO.Message = "Something went wrong. Please try again later.";
                return Ok(MessageDTO);
            }
        }

        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("UpdateAndReregister")]
        public ActionResult<dynamic> UpdateAndReregisterParticipateInCurrentyYear(SaveRepresentativeDTO representativeDTO)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                MessageDTO = new MessageDTO();
                SIBFMemberId = representativeDTO.MemberId;
                if (UpdateNReregister(representativeDTO))
                {
                    MessageDTO.MessageTypeResponse = "Success";
                    MessageDTO.Message = "Updated successfully.";
                    return Ok(MessageDTO);
                }
                else
                {
                    MessageDTO.MessageTypeResponse = "Error";
                    MessageDTO.Message = "Exhibitor not found";
                    return Ok(MessageDTO);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateAndReregister action: {ex.InnerException}");
                MessageDTO.MessageTypeResponse = "Error";
                MessageDTO.Message = "Something went wrong. Please try again later.";
                return Ok(MessageDTO);
            }
        }

        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("RemoveCommand")]
        public ActionResult<dynamic> Remove_Command(RemoveCommandDTO removeCommandDTO)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (RemoveRepresentative(removeCommandDTO, LangId))
                    return Ok("Removed Successfully");
                return Ok("Error removing a record");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside RemoveCommand representative action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("SaveTravelDetails")]
        public ActionResult<dynamic> SubmitRepTravel_Click(TravelDTO travelDTO)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                MessageDTO = new MessageDTO();
                SIBFMemberId = travelDTO.MemberId;
                UpdateTravelDetails(travelDTO);
                return Ok(MessageDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside SaveTravelDetails action: {ex.InnerException}");
                MessageDTO.MessageTypeResponse = "Error";
                MessageDTO.Message = "Something went wrong. Please try again later.";
                return Ok(MessageDTO);
            }
        }
        #endregion

        #region CRUD
        private MessageDTO AddorUpdateRepresentativeData(SaveRepresentativeDTO representativeDTO, long langid)
        {
            CommonHelperForRepresentative commonHelper = new CommonHelperForRepresentative();
            MessageDTO = new MessageDTO();
            SIBFMemberId = representativeDTO.MemberId;
            long memberId = User.Identity.GetID();
            if (memberId == SIBFMemberId)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    ExhibitionRepresentativeParticipatingService ExhibitionRepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService();

                    var currentExhibition = MethodFactory.GetActiveExhibition(representativeDTO.WebsiteId, _context);
                    bool isunique = true;
                    if (representativeDTO.IsEditClicked)
                    {
                        MessageDTO = UpdateRepresentativeData(representativeDTO, langid);
                    }
                    else
                    {
                        #region Add to check
                        isunique = commonHelper.IsUniqueRepresentativeForAdd(representativeDTO, langid, currentExhibition.ExhibitionId);
                        //isduplicate = commonHelper.IsduplicateRepresentativeForAdd(representativeDTO, langid, currentExhibition.ExhibitionId);

                        if (isunique)
                        {
                            var isvalidupload = false;
                            var isupload = false;
                            if (string.IsNullOrEmpty(representativeDTO.VisaHref))
                                isvalidupload = false;
                            else
                                isvalidupload = true;

                            if (commonHelper.IsVisaApplicationFormValid("." + representativeDTO.FileExtensionVisa, isvalidupload) || isvalidupload)
                            {
                                if (string.IsNullOrEmpty(representativeDTO.PassportHRef))
                                    isvalidupload = false;
                                else
                                    isvalidupload = true;
                                if (commonHelper.IsPasportCopyValid("." + representativeDTO.FileExtensionofPassport, isvalidupload) || isvalidupload)
                                {
                                    isvalidupload = false;
                                    if (string.IsNullOrEmpty(representativeDTO.PhotoHref))
                                        isvalidupload = false;
                                    else
                                        isvalidupload = true;

                                    if (commonHelper.IsPersonalPhotoValid("." + representativeDTO.FileExtensionofPersonalPhoto, isvalidupload))
                                    {
                                        #region Passport 
                                        if (!MethodFactory.IsDateExpired(representativeDTO.PassportExpiryDate.Trim(), currentExhibition.StartDate, Convert.ToInt32(currentExhibition.ExpiryMonths)))
                                        {
                                            if (!string.IsNullOrEmpty(representativeDTO.FileBase64Passport) && !string.IsNullOrEmpty(representativeDTO.FileBase64PersonalPhoto) && !string.IsNullOrEmpty(representativeDTO.FileBase64Visa))
                                            {
                                                MessageDTO = SaveContentForVisa(representativeDTO, langid);
                                            }
                                            else
                                            {
                                                if (langid == 1)
                                                    MessageDTO.Message = "Please upload passport copy, personal photo,visa application form then try submitting again.";
                                                else
                                                    MessageDTO.Message = "Please upload passport copy, personal photo,visa application form then try submitting again.";
                                                MessageDTO.MessageTypeResponse = "Error";
                                            }

                                        }
                                        else if (currentExhibition.StartDate != null && representativeDTO.PassportExpiryDate != null && !string.IsNullOrEmpty(representativeDTO.PassportExpiryDate.Trim()))
                                        {
                                            if (representativeDTO.PassportExpiryDate != null && Convert.ToDateTime(representativeDTO.PassportExpiryDate.Trim()) <= currentExhibition.StartDate)
                                            {
                                                //if (langid == 1)
                                                //    MessageDTO.Message = "Passport expires before or on the same day of the start of exhibition. Please provide a valid passport expiry date."; // _stringLocalizer["PassportExpiry"];
                                                //else
                                                //    MessageDTO.Message = "Passport expires before or on the same day of the start of exhibition. Please provide a valid passport expiry date."; // _stringLocalizer["PassportExpiry"];
                                                if (langid == 1)
                                                    MessageDTO.Message = "Passport should have at least 6 months validity.";
                                                else
                                                    MessageDTO.Message = "يجب ان تكون صلاحية الجواز 6 شهور أو اكثر.";
                                                MessageDTO.MessageTypeResponse = "Error";
                                            }
                                            else
                                            {
                                                //if (langid == 1)
                                                //    MessageDTO.Message = "The validity of passport should be more than 6 months from the exhibition starting date";// _stringLocalizer["InvalidExhibitionStartorExpiryDate"];
                                                //else
                                                //    MessageDTO.Message = "يجب أن تكون صلاحية جواز السفر أكثر من 6 أشهر من تاريخ بدء المعرض";

                                                if (langid == 1)
                                                    MessageDTO.Message = "Passport should have at least 6 months validity.";
                                                else
                                                    MessageDTO.Message = "يجب ان تكون صلاحية الجواز 6 شهور أو اكثر.";
                                                MessageDTO.MessageTypeResponse = "Error";
                                            }
                                        }
                                        else
                                        {
                                            //if (langid == 1)
                                            //    MessageDTO.Message = "The validity of passport should be more than 6 months from the exhibition starting date";// _stringLocalizer["InvalidExhibitionStartorExpiryDate"];
                                            //else
                                            //    MessageDTO.Message = "يجب أن تكون صلاحية جواز السفر أكثر من 6 أشهر من تاريخ بدء المعرض";

                                            if (langid == 1)
                                                MessageDTO.Message = "Passport should have at least 6 months validity.";
                                            else
                                                MessageDTO.Message = "يجب ان تكون صلاحية الجواز 6 شهور أو اكثر.";
                                            MessageDTO.MessageTypeResponse = "Error";
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        if (langid == 1)
                                            MessageDTO.Message = "Invalid file format for Personal Photo. File type should be either gif/jpg/jpeg/png format";// _stringLocalizer["InvalidImgPDF"];
                                        else
                                            MessageDTO.Message = "Invalid file format for Personal Photo. File type should be either gif/jpg/jpeg/png format";// _stringLocalizer["InvalidImgPDF"];
                                        MessageDTO.MessageTypeResponse = "Error";
                                    }
                                }
                                else
                                {
                                    if (langid == 1)
                                        MessageDTO.Message = "Invalid file format for Passport Copy. File type should be either pdf/gif/jpg/jpeg/png/tif/tiff format";// _stringLocalizer["InvalidImgPDF"];
                                    else
                                        MessageDTO.Message = "Invalid file format for Passport Copy. File type should be either pdf/gif/jpg/jpeg/png/tif/tiff format";// _stringLocalizer["InvalidImgPDF"];
                                    MessageDTO.MessageTypeResponse = "Error";
                                }
                            }
                            else
                            {
                                if (langid == 1)
                                    MessageDTO.Message = "Invalid file format. File type should be either pdf/xls/xlsx/doc/docx/gif/jpg/jpeg/png format"; //_stringLocalizer["InvalidFileFormat1"];
                                else
                                    MessageDTO.Message = "Invalid file format. File type should be either pdf/xls/xlsx/doc/docx/gif/jpg/jpeg/png format"; //_stringLocalizer["InvalidFileFormat1"];
                                MessageDTO.MessageTypeResponse = "Error";
                            }
                        }
                        else
                        {
                            if (langid == 1)
                                MessageDTO.Message = "Name already exists for this exhibition";
                            else
                                MessageDTO.Message = "Name already exists for this exhibition";
                            MessageDTO.MessageTypeResponse = "Error";
                        }


                        #endregion
                    }
                }
                return MessageDTO;
            }

            MessageDTO.Message = "Not Found";
            MessageDTO.MessageTypeResponse = "Error";
            return MessageDTO;
        }
        private MessageDTO SaveContentForVisa(SaveRepresentativeDTO representativeDTO, long langid)
        {
            //  long.TryParse(Request.Headers["LanguageURL"], out LangId);
            EnumResultType result = EnumResultType.Failed;
            string filename = string.Empty;
            long? exhibitorId = null;
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                exhibitorId = MethodFactory.GetExhibitorByMember(SIBFMemberId, representativeDTO.WebsiteId, _context);
            }
            if (exhibitorId > 0)
            {
                DateTime dob = new DateTime();
                DateTime.TryParse(representativeDTO.DateofBirth, out dob);
                using (ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService())
                {

                    result = AddParentRepresentative(representativeDTO, dob, ExhibitionRepresentativeService);
                    if (result == EnumResultType.Success)
                    {
                        MessageDTO = AddExhibitionRepresentativeParticipating(representativeDTO, langid, filename, exhibitorId, ExhibitionRepresentativeService, out result);
                    }
                    return MessageDTO;
                }
            }
            else
            {
                if (langid == 1)
                    MessageDTO.Message = "Exhibitor Id not found or not yet processed.";// _stringLocalizer["RepresentativeFailedToAdd"];
                else
                    MessageDTO.Message = "Exhibitor Id not found or not yet processed.";// _stringLocalizer["RepresentativeFailedToAdd"];
                MessageDTO.MessageTypeResponse = "Error";
                return MessageDTO;
            }
        }
        private MessageDTO AddorUpdateRepresentativeDataNoVisa(SaveRepresentativeDTO representativeDTO, long langid)
        {
            CommonHelperForRepresentative commonHelper = new CommonHelperForRepresentative();
            MessageDTO = new MessageDTO();
            SIBFMemberId = representativeDTO.MemberId;
            long memberId = User.Identity.GetID();
            bool isunique = true;
            if (memberId == SIBFMemberId)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    var currentExhibition = MethodFactory.GetActiveExhibition(representativeDTO.WebsiteId, _context);
                    if (representativeDTO.IsEditClicked)
                    {
                        isunique = commonHelper.IsUniqueRepresentativeForUpdateNoVIsa(representativeDTO, langid, currentExhibition.ExhibitionId);
                        if (isunique)
                        {
                            MessageDTO = UpdateContentNoVisa(representativeDTO, langid);
                            return MessageDTO;
                        }
                        else
                        {
                            MessageDTO.MessageTypeResponse = "Error";
                            if (langid == 1)
                                MessageDTO.Message = "Can't update. Name already exists for this exhibition";
                            else
                                MessageDTO.Message = "Can't update. Name already exists for this exhibition";
                            return MessageDTO;
                        }
                    }
                    else
                    {
                        isunique = commonHelper.IsUniqueRepresentativeForAddNoVIsa(representativeDTO, langid, currentExhibition.ExhibitionId);
                        if (isunique)
                        {
                            MessageDTO = SaveContentNoVisa(representativeDTO, langid);
                            return MessageDTO;
                        }
                        else
                        {
                            MessageDTO.MessageTypeResponse = "Error";
                            if (langid == 1)
                                MessageDTO.Message = "Name already exists for this exhibition";
                            else
                                MessageDTO.Message = "Name already exists for this exhibition";
                            return MessageDTO;
                        }
                    }
                }
            }

            MessageDTO.Message = "Not Found";
            MessageDTO.MessageTypeResponse = "Error";
            return MessageDTO;
        }
        private MessageDTO SaveContentNoVisa(SaveRepresentativeDTO representativeDTO, long langid)
        {
            //  long.TryParse(Request.Headers["LanguageURL"], out LangId);
            EnumResultType result = EnumResultType.Failed;
            string filename = string.Empty;
            long? exhibitorId = null;
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                exhibitorId = MethodFactory.GetExhibitorByMember(SIBFMemberId, representativeDTO.WebsiteId, _context);
            }
            if (exhibitorId > 0)
            {
                DateTime dob = new DateTime();
                DateTime.TryParse(representativeDTO.DateofBirth, out dob);
                using (ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService())
                {
                    result = AddParentRepresentative(representativeDTO, dob, ExhibitionRepresentativeService);
                    if (result == EnumResultType.Success)
                    {
                        MessageDTO = AddExhibitionRepresentativeParticipatingNoVisa(representativeDTO, langid, filename, exhibitorId, ExhibitionRepresentativeService, out result);
                    }
                    return MessageDTO;
                }
            }
            else
            {
                if (langid == 1)
                    MessageDTO.Message = "Exhibitor Id not found or not yet processed.";
                else
                    MessageDTO.Message = "Exhibitor Id not found or not yet processed.";
                MessageDTO.MessageTypeResponse = "Error";
                return MessageDTO;
            }
        }
        private EnumResultType AddParentRepresentative(SaveRepresentativeDTO representativeDTO, DateTime dob, ExhibitionRepresentativeService ExhibitionRepresentativeService)
        {
            EnumResultType result;
            XsiExhibitionRepresentative representativeParentEntity = new XsiExhibitionRepresentative();
            representativeParentEntity.NameEn = representativeDTO.NameEn;
            representativeParentEntity.NameAr = representativeDTO.NameAr;
            representativeParentEntity.MemberId = representativeDTO.MemberId;

            if (!string.IsNullOrEmpty(representativeDTO.DateofBirth))
                representativeParentEntity.Dob = dob;
            representativeParentEntity.PlaceOfBirth = representativeDTO.PlaceOfBirth;
            representativeParentEntity.IsActive = EnumConversion.ToString(EnumBool.Yes);
            //rentity.RepresentativeId = representativeDTO.RepresentativeId;
            //entity.ExhibitionId = ExhibitionGroupId;
            representativeParentEntity.CreatedOn = MethodFactory.ArabianTimeNow();
            representativeParentEntity.CreatedBy = representativeDTO.MemberId;
            representativeParentEntity.ModifiedOn = MethodFactory.ArabianTimeNow();
            representativeParentEntity.ModifiedBy = representativeDTO.MemberId;
            result = ExhibitionRepresentativeService.InsertExhibitionRepresentative(representativeParentEntity);
            return result;
        }
        private MessageDTO AddExhibitionRepresentativeParticipating(SaveRepresentativeDTO representativeDTO, long langid, string filename, long? exhibitorId, ExhibitionRepresentativeService ExhibitionRepresentativeService, out EnumResultType result)
        {
            MessageDTO MessageDTO = new MessageDTO();
            XsiExhibitionRepresentativeParticipating representativeParticipatingEntity = new XsiExhibitionRepresentativeParticipating();
            #region Insert Representative Participating
            using (ExhibitionRepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService())
            {
                representativeParticipatingEntity.RepresentativeId = ExhibitionRepresentativeService.XsiItemdId;
                representativeParticipatingEntity.MemberExhibitionYearlyId = exhibitorId != null ? exhibitorId.Value : 0;
                representativeDTO.MemberExhibitionYearlyId = exhibitorId ?? 0;
                if (representativeDTO.NationalityId > 0)
                    representativeParticipatingEntity.CountryId = representativeDTO.NationalityId;
                representativeParticipatingEntity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                representativeParticipatingEntity.IsEmailSent = EnumConversion.ToString(EnumBool.No);
                representativeParticipatingEntity.Status = EnumConversion.ToString(EnumRepresentativeStatus.Pending);
                representativeParticipatingEntity.LanguageId = langid;

                if (representativeDTO.FileExtensionofCountryUID != null && !string.IsNullOrEmpty(representativeDTO.FileExtensionofCountryUID))
                {
                    filename = SaveImage(representativeDTO.FileBase64CountryUID, representativeDTO.FileExtensionofCountryUID, representativeDTO.NameEn, CountryUIDPath, representativeDTO.FileExtensionofCountryUID);
                    representativeParticipatingEntity.CountryUidno = filename;
                }
                if (representativeDTO.NameEn != null && !string.IsNullOrEmpty(representativeDTO.NameEn))
                    representativeParticipatingEntity.NameEn = representativeDTO.NameEn.Trim();

                if (representativeDTO.NameAr != null && !string.IsNullOrEmpty(representativeDTO.NameAr))
                    representativeParticipatingEntity.NameAr = representativeDTO.NameAr.Trim();

                if (representativeDTO.Profession != null && !string.IsNullOrEmpty(representativeDTO.Profession))
                    representativeParticipatingEntity.Profession = representativeDTO.Profession.Trim();

                if (representativeDTO.IsVisitedUae != null && !string.IsNullOrEmpty(representativeDTO.IsVisitedUae))
                    representativeParticipatingEntity.IsVisitedUae = representativeDTO.IsVisitedUae;

                representativeParticipatingEntity.Telephone = representativeDTO.PhoneISD + "$" + representativeDTO.PhoneSTD + "$" + representativeDTO.Phone;
                representativeParticipatingEntity.Mobile = representativeDTO.MobileISD + "$" + representativeDTO.MobileSTD + "$" + representativeDTO.Mobile;
                representativeParticipatingEntity.NeedVisa = representativeDTO.IsVisaNeed ? EnumConversion.ToString(EnumBool.Yes) : EnumConversion.ToString(EnumBool.No);

                if (representativeDTO.IsUAEResident != null && !string.IsNullOrEmpty(representativeDTO.IsUAEResident))
                    representativeParticipatingEntity.IsUaeresident = representativeDTO.IsUAEResident.Trim();

                if (representativeDTO.PassportNumber != null && !string.IsNullOrEmpty(representativeDTO.PassportNumber))
                    representativeParticipatingEntity.Passport = representativeDTO.PassportNumber.Trim();
                if (representativeDTO.PlaceOfIssue != null && !string.IsNullOrEmpty(representativeDTO.PlaceOfIssue))
                    representativeParticipatingEntity.PlaceOfIssue = representativeDTO.PlaceOfIssue.Trim();
                if (representativeDTO.PassportDateOfIssue != null && !string.IsNullOrEmpty(representativeDTO.PassportDateOfIssue))
                    representativeParticipatingEntity.DateOfIssue = Convert.ToDateTime(representativeDTO.PassportDateOfIssue.Trim());
                if (representativeDTO.PassportExpiryDate != null && !string.IsNullOrEmpty(representativeDTO.PassportExpiryDate))
                    representativeParticipatingEntity.ExpiryDate = Convert.ToDateTime(representativeDTO.PassportExpiryDate.Trim());

                if (!string.IsNullOrEmpty(representativeDTO.IsAddHealthInsurance))
                {
                    if (representativeDTO.IsAddHealthInsurance == EnumConversion.ToString(EnumBool.Yes))
                        representativeParticipatingEntity.IsAddHealthInsurance = representativeDTO.IsAddHealthInsurance;
                    else
                        representativeParticipatingEntity.IsAddHealthInsurance = EnumConversion.ToString(EnumBool.No);
                }

                if (!string.IsNullOrEmpty(representativeDTO.FileBase64Passport))
                {
                    ext = representativeDTO.FileExtensionofPassport;
                    filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                    if ("." + ext.ToLower() == ".pdf" || ext.ToLower() == "pdf")
                        filename = WriteTextOnPDF(representativeDTO.FileBase64Passport, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                    else
                        filename = WriteTextOnImage(representativeDTO.FileBase64Passport, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                    representativeParticipatingEntity.PassportCopy = filename;
                }
                if (!string.IsNullOrEmpty(representativeDTO.FileBase64Passport2))
                {
                    ext = representativeDTO.FileExtensionofPassport2;
                    filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                    if ("." + ext.ToLower() == ".pdf" || ext.ToLower() == "pdf")
                        filename = WriteTextOnPDF(representativeDTO.FileBase64Passport2, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                    else
                        filename = WriteTextOnImage(representativeDTO.FileBase64Passport2, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                    representativeParticipatingEntity.PassportCopyTwo = filename;
                }
                if (!string.IsNullOrEmpty(representativeDTO.FileBase64PersonalPhoto))
                {
                    filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), representativeDTO.FileExtensionofPersonalPhoto);
                    filename = SaveImage(representativeDTO.FileBase64PersonalPhoto, representativeDTO.FileExtensionofPersonalPhoto, representativeDTO.NameEn, PhotoPath, representativeDTO.FileExtensionofPersonalPhoto);
                    representativeParticipatingEntity.Photo = filename;
                }
                if (!string.IsNullOrEmpty(representativeDTO.FileBase64Visa))
                {
                    filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), representativeDTO.FileExtensionVisa);
                    //FileUpload3.SaveAs(string.Format("{0}{1}", Server.MapPath(VisaFormPath), filename));
                    filename = SaveImage(representativeDTO.FileBase64Visa, representativeDTO.FileExtensionVisa, representativeDTO.NameEn, VisaFormPath, representativeDTO.FileExtensionVisa);
                    representativeParticipatingEntity.VisaApplicationForm = filename;
                }
                if (!string.IsNullOrEmpty(representativeDTO.FileBase64FlightTickets))
                {
                    filename = SaveImage(representativeDTO.FileBase64FlightTickets, representativeDTO.FileExtensionofFlightTickets, representativeDTO.NameEn, FlightTicketsPath, representativeDTO.FileExtensionofFlightTickets);
                    representativeParticipatingEntity.FlightTickets = filename;
                }

                representativeParticipatingEntity.CreatedOn = MethodFactory.ArabianTimeNow();
                representativeParticipatingEntity.CreatedBy = SIBFMemberId;
                representativeParticipatingEntity.ModifiedOn = MethodFactory.ArabianTimeNow();
                representativeParticipatingEntity.ModifiedBy = SIBFMemberId;

                result = ExhibitionRepresentativeParticipatingService.InsertExhibitionRepresentativeParticipating(representativeParticipatingEntity);

                //  filename = AddExhibitionRepresentativeParticipating(representativeDTO, langid, out result, filename, exhibitorId, ExhibitionRepresentativeService, out representativeParticipatingEntity);
                if (result == EnumResultType.Success)
                {
                    if (langid == 1)
                        MessageDTO.Message = "Representative details have been sent for Admin’s approval. We will get back to you at our earliest. "; //_stringLocalizer["RepresentativeInsertSucessfull"];
                    else
                        MessageDTO.Message = "تفاصيل المندوب الذي قمت بإضافته سيتم عرضه على الإدارة للموافقة. سيتم الرد عليك في أقرب وقت";//_stringLocalizer["RepresentativeInsertSucessfull"];
                    MessageDTO.MessageTypeResponse = "Success";

                }
                else
                {
                    if (langid == 1)
                        MessageDTO.Message = "Failed to add representative, Please try adding again.";// _stringLocalizer["RepresentativeFailedToAdd"];
                    else
                        MessageDTO.Message = "لم يتم إضافة المندوب، الرجاء إعادة المحاولة";// _stringLocalizer["RepresentativeFailedToAdd"];

                    MessageDTO.MessageTypeResponse = "Error";
                }
            }
            #endregion
            return MessageDTO;
        }
        private MessageDTO AddExhibitionRepresentativeParticipatingNoVisa(SaveRepresentativeDTO representativeDTO, long langid, string filename, long? exhibitorId, ExhibitionRepresentativeService ExhibitionRepresentativeService, out EnumResultType result)
        {
            MessageDTO MessageDTO = new MessageDTO();
            XsiExhibitionRepresentativeParticipatingNoVisa representativeParticipatingNoVisaEntity = new XsiExhibitionRepresentativeParticipatingNoVisa();
            #region Insert Representative Participating
            using (ExhibitionRepresentativeParticipatingNoVisaService ExhibitionRepresentativeParticipatingNoVisaService = new ExhibitionRepresentativeParticipatingNoVisaService())
            {
                representativeParticipatingNoVisaEntity.RepresentativeId = ExhibitionRepresentativeService.XsiItemdId;
                representativeParticipatingNoVisaEntity.MemberExhibitionYearlyId = exhibitorId != null ? exhibitorId.Value : 0;
                representativeDTO.MemberExhibitionYearlyId = exhibitorId ?? 0;
                if (representativeDTO.NationalityId > 0)
                    representativeParticipatingNoVisaEntity.CountryId = representativeDTO.NationalityId;
                representativeParticipatingNoVisaEntity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                representativeParticipatingNoVisaEntity.IsEmailSent = EnumConversion.ToString(EnumBool.No);
                representativeParticipatingNoVisaEntity.Status = EnumConversion.ToString(EnumRepresentativeStatus.Pending);
                representativeParticipatingNoVisaEntity.LanguageId = langid;

                if (!string.IsNullOrEmpty(representativeDTO.FileExtensionofCountryUID))
                {
                    filename = SaveImage(representativeDTO.FileBase64CountryUID, representativeDTO.FileExtensionofCountryUID, representativeDTO.NameEn, CountryUIDPath, representativeDTO.FileExtensionofCountryUID);
                    representativeParticipatingNoVisaEntity.CountryUidno = filename;
                }
                if (representativeDTO.NameEn != null && !string.IsNullOrEmpty(representativeDTO.NameEn))
                    representativeParticipatingNoVisaEntity.NameEn = representativeDTO.NameEn.Trim();

                if (representativeDTO.NameAr != null && !string.IsNullOrEmpty(representativeDTO.NameAr))
                    representativeParticipatingNoVisaEntity.NameAr = representativeDTO.NameAr.Trim();

                if (representativeDTO.Profession != null && !string.IsNullOrEmpty(representativeDTO.Profession))
                    representativeParticipatingNoVisaEntity.Profession = representativeDTO.Profession.Trim();

                if (representativeDTO.IsVisitedUae != null && !string.IsNullOrEmpty(representativeDTO.IsVisitedUae))
                    representativeParticipatingNoVisaEntity.IsVisitedUae = representativeDTO.IsVisitedUae;

                representativeParticipatingNoVisaEntity.Telephone = representativeDTO.PhoneISD + "$" + representativeDTO.PhoneSTD + "$" + representativeDTO.Phone;
                representativeParticipatingNoVisaEntity.Mobile = representativeDTO.MobileISD + "$" + representativeDTO.MobileSTD + "$" + representativeDTO.Mobile;
                representativeParticipatingNoVisaEntity.NeedVisa = representativeDTO.IsVisaNeed ? EnumConversion.ToString(EnumBool.Yes) : EnumConversion.ToString(EnumBool.No);

                if (representativeDTO.IsUAEResident != null && !string.IsNullOrEmpty(representativeDTO.IsUAEResident))
                    representativeParticipatingNoVisaEntity.IsUaeresident = representativeDTO.IsUAEResident.Trim();

                if (representativeDTO.PassportNumber != null && !string.IsNullOrEmpty(representativeDTO.PassportNumber))
                    representativeParticipatingNoVisaEntity.Passport = representativeDTO.PassportNumber.Trim();
                if (representativeDTO.PlaceOfIssue != null && !string.IsNullOrEmpty(representativeDTO.PlaceOfIssue))
                    representativeParticipatingNoVisaEntity.PlaceOfIssue = representativeDTO.PlaceOfIssue.Trim();
                if (representativeDTO.PassportDateOfIssue != null && !string.IsNullOrEmpty(representativeDTO.PassportDateOfIssue))
                    representativeParticipatingNoVisaEntity.DateOfIssue = Convert.ToDateTime(representativeDTO.PassportDateOfIssue.Trim());
                if (representativeDTO.PassportExpiryDate != null && !string.IsNullOrEmpty(representativeDTO.PassportExpiryDate))
                    representativeParticipatingNoVisaEntity.ExpiryDate = Convert.ToDateTime(representativeDTO.PassportExpiryDate.Trim());

                if (!string.IsNullOrEmpty(representativeDTO.IsAddHealthInsurance))
                {
                    if (representativeDTO.IsAddHealthInsurance == EnumConversion.ToString(EnumBool.Yes))
                        representativeParticipatingNoVisaEntity.IsAddHealthInsurance = representativeDTO.IsAddHealthInsurance;
                    else
                        representativeParticipatingNoVisaEntity.IsAddHealthInsurance = EnumConversion.ToString(EnumBool.No);
                }

                if (!string.IsNullOrEmpty(representativeDTO.FileBase64Passport))
                {
                    ext = representativeDTO.FileExtensionofPassport;
                    filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                    if ("." + ext.ToLower() == ".pdf" || ext.ToLower() == "pdf")
                        filename = WriteTextOnPDF(representativeDTO.FileBase64Passport, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                    else
                        filename = WriteTextOnImage(representativeDTO.FileBase64Passport, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                    representativeParticipatingNoVisaEntity.PassportCopy = filename;
                }
                if (!string.IsNullOrEmpty(representativeDTO.FileBase64Passport2))
                {
                    ext = representativeDTO.FileExtensionofPassport2;
                    filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                    if ("." + ext.ToLower() == ".pdf" || ext.ToLower() == "pdf")
                        filename = WriteTextOnPDF(representativeDTO.FileBase64Passport2, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                    else
                        filename = WriteTextOnImage(representativeDTO.FileBase64Passport2, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                    representativeParticipatingNoVisaEntity.PassportCopyTwo = filename;
                }
                if (!string.IsNullOrEmpty(representativeDTO.FileBase64PersonalPhoto))
                {
                    filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), representativeDTO.FileExtensionofPersonalPhoto);
                    filename = SaveImage(representativeDTO.FileBase64PersonalPhoto, representativeDTO.FileExtensionofPersonalPhoto, representativeDTO.NameEn, PhotoPath, representativeDTO.FileExtensionofPersonalPhoto);
                    representativeParticipatingNoVisaEntity.Photo = filename;
                }
                if (!string.IsNullOrEmpty(representativeDTO.FileBase64Visa))
                {
                    filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), representativeDTO.FileExtensionVisa);
                    //FileUpload3.SaveAs(string.Format("{0}{1}", Server.MapPath(VisaFormPath), filename));
                    filename = SaveImage(representativeDTO.FileBase64Visa, representativeDTO.FileExtensionVisa, representativeDTO.NameEn, VisaFormPath, representativeDTO.FileExtensionVisa);
                    representativeParticipatingNoVisaEntity.VisaApplicationForm = filename;
                }
                if (!string.IsNullOrEmpty(representativeDTO.FileBase64FlightTickets))
                {
                    filename = SaveImage(representativeDTO.FileBase64FlightTickets, representativeDTO.FileExtensionofFlightTickets, representativeDTO.NameEn, FlightTicketsPath, representativeDTO.FileExtensionofFlightTickets);
                    representativeParticipatingNoVisaEntity.FlightTickets = filename;
                }

                representativeParticipatingNoVisaEntity.CreatedOn = MethodFactory.ArabianTimeNow();
                representativeParticipatingNoVisaEntity.CreatedBy = SIBFMemberId;
                representativeParticipatingNoVisaEntity.ModifiedOn = MethodFactory.ArabianTimeNow();
                representativeParticipatingNoVisaEntity.ModifiedBy = SIBFMemberId;

                result = ExhibitionRepresentativeParticipatingNoVisaService.InsertExhibitionRepresentativeParticipatingNoVisa(representativeParticipatingNoVisaEntity);

                //  filename = AddExhibitionRepresentativeParticipating(representativeDTO, langid, out result, filename, exhibitorId, ExhibitionRepresentativeService, out representativeParticipatingEntity);
                if (result == EnumResultType.Success)
                {
                    if (langid == 1)
                        MessageDTO.Message = "Representative details have been sent for Admin’s approval. We will get back to you at our earliest. "; //_stringLocalizer["RepresentativeInsertSucessfull"];
                    else
                        MessageDTO.Message = "تفاصيل المندوب الذي قمت بإضافته سيتم عرضه على الإدارة للموافقة. سيتم الرد عليك في أقرب وقت";//_stringLocalizer["RepresentativeInsertSucessfull"];
                    MessageDTO.MessageTypeResponse = "Success";
                }
                else
                {
                    if (langid == 1)
                        MessageDTO.Message = "Failed to add representative, Please try adding again.";// _stringLocalizer["RepresentativeFailedToAdd"];
                    else
                        MessageDTO.Message = "لم يتم إضافة المندوب، الرجاء إعادة المحاولة";// _stringLocalizer["RepresentativeFailedToAdd"];

                    MessageDTO.MessageTypeResponse = "Error";
                }
            }
            #endregion
            return MessageDTO;
        }
        private MessageDTO UpdateRepresentativeData(SaveRepresentativeDTO representativeDTO, long langid)
        {
            CommonHelperForRepresentative commonHelper = new CommonHelperForRepresentative();
            MessageDTO = new MessageDTO();
            SIBFMemberId = representativeDTO.MemberId;
            long memberId = User.Identity.GetID();
            if (memberId == SIBFMemberId)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    var currentExhibition = MethodFactory.GetActiveExhibition(representativeDTO.WebsiteId, _context);
                    var isunique = commonHelper.IsUniqueRepresentativeForUpdate(representativeDTO, langid, currentExhibition.ExhibitionId);
                    if (isunique)
                    {
                        if (representativeDTO.IsVisaNeed)
                        {
                            #region Visa Required
                            if (!MethodFactory.IsDateExpired(representativeDTO.PassportExpiryDate.Trim(), currentExhibition.StartDate, Convert.ToInt32(currentExhibition.ExpiryMonths)))
                            {
                                MessageDTO = UpdateContent(representativeDTO, langid);
                                //BindRepresentative();
                            }
                            else
                            if (currentExhibition.StartDate != null && representativeDTO.PassportExpiryDate != null && !string.IsNullOrEmpty(representativeDTO.PassportExpiryDate.Trim()))
                            {
                                if (representativeDTO.PassportExpiryDate != null && Convert.ToDateTime(representativeDTO.PassportExpiryDate.Trim()) <= currentExhibition.StartDate)
                                {
                                    //if (langid == 1)
                                    //    MessageDTO.Message = "Passport expires before or on the same day of the start of exhibition. Please provide a valid passport expiry date."; // _stringLocalizer["PassportExpiry"];
                                    //else
                                    //    MessageDTO.Message = "Passport expires before or on the same day of the start of exhibition. Please provide a valid passport expiry date."; // _stringLocalizer["PassportExpiry"];
                                    if (langid == 1)
                                        MessageDTO.Message = "Passport should have at least 6 months validity.";
                                    else
                                        MessageDTO.Message = "يجب ان تكون صلاحية الجواز 6 شهور أو اكثر.";
                                    MessageDTO.MessageTypeResponse = "Error";
                                }
                                else
                                {
                                    //if (langid == 1)
                                    //    MessageDTO.Message = "The validity of passport should be more than 6 months from the exhibition starting date";// _stringLocalizer["InvalidExhibitionStartorExpiryDate"];
                                    //else
                                    //    MessageDTO.Message = "يجب أن تكون صلاحية جواز السفر أكثر من 6 أشهر من تاريخ بدء المعرض";
                                    if (langid == 1)
                                        MessageDTO.Message = "Passport should have at least 6 months validity.";
                                    else
                                        MessageDTO.Message = "يجب ان تكون صلاحية الجواز 6 شهور أو اكثر.";
                                    MessageDTO.MessageTypeResponse = "Error";
                                }
                            }
                            else
                            {
                                //if (langid == 1)
                                //    MessageDTO.Message = "The validity of passport should be more than 6 months from the exhibition starting date";// _stringLocalizer["InvalidExhibitionStartorExpiryDate"];
                                //else
                                //    MessageDTO.Message = "يجب أن تكون صلاحية جواز السفر أكثر من 6 أشهر من تاريخ بدء المعرض";
                                if (langid == 1)
                                    MessageDTO.Message = "Passport should have at least 6 months validity.";
                                else
                                    MessageDTO.Message = "يجب ان تكون صلاحية الجواز 6 شهور أو اكثر.";
                                MessageDTO.MessageTypeResponse = "Error";
                            }
                            #endregion
                        }
                        else
                        {
                            #region Visa Not Required
                            MessageDTO = UpdateContent(representativeDTO, langid);
                            //BindRepresentative();
                            #endregion
                        }
                        //imgSpinnerRep.Style["display"] = "none";
                        //SelectTab1();
                    }
                    else
                    {
                        if (langid == 1)
                            MessageDTO.Message = "Name already exists for this exhibition";
                        else
                            MessageDTO.Message = "Name already exists for this exhibition";
                        MessageDTO.MessageTypeResponse = "Error";
                    }
                }
                return MessageDTO;
            }

            MessageDTO.Message = "Not Found";
            MessageDTO.MessageTypeResponse = "Error";
            return MessageDTO;
        }
        #endregion

        #region Update
        private MessageDTO IsVisaAndNoVisaValidationPassed(long countryid, bool isvisaneed)
        {
            MessageDTO MessageDTO = new MessageDTO();
            MessageDTO.MessageTypeResponse = "Success";
            if (countryid > 0)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    var country = _context.XsiExhibitionCountry.Where(i => i.CountryId == countryid).FirstOrDefault();
                    if (country != null)
                    {
                        if (isvisaneed)
                        {
                            if (country.IsVisaRequired == "N")
                            {
                                MessageDTO.MessageTypeResponse = "Error";
                                MessageDTO.Message = "Visa not required for the selected country. Please choose No-Visa tab and start the registration process.";
                            }
                        }
                        else
                        {
                            if (country.IsVisaRequired == "Y")
                            {
                                MessageDTO.MessageTypeResponse = "Error";
                                MessageDTO.Message = "Visa required for selected country. Try adding from Visa tab.";
                            }
                        }
                    }
                }
            }
            return MessageDTO;

        }
        private MessageDTO UpdateContent(SaveRepresentativeDTO representativeDTO, long langid)
        {
            bool isdocumentuploaded = false;
            bool ispassportuploaded = false;
            string filename = string.Empty;
            using (ExhibitionRepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService())
            {
                ExhibitionRepresentativeService = new ExhibitionRepresentativeService();
                var parententityRepresentative = ExhibitionRepresentativeService.GetRepresentativeByItemId(representativeDTO.RepresentativeId);

                var participatingentity = ExhibitionRepresentativeParticipatingService.GetExhibitionRepresentativeParticipating(new XsiExhibitionRepresentativeParticipating() { RepresentativeId = representativeDTO.RepresentativeId, MemberExhibitionYearlyId = representativeDTO.MemberExhibitionYearlyId }).FirstOrDefault(); ;
                if (participatingentity != null)
                {
                    if (representativeDTO.NationalityId > 0)
                        participatingentity.CountryId = representativeDTO.NationalityId;

                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64CountryUID))
                    {
                        filename = SaveImage(representativeDTO.FileBase64CountryUID, representativeDTO.FileExtensionofCountryUID, representativeDTO.NameEn, CountryUIDPath, representativeDTO.FileExtensionofCountryUID);
                        //fuUploadCountryUID.SaveAs(string.Format("{0}{1}", Server.MapPath(CountryUIDPath), filename));
                        participatingentity.CountryUidno = filename;
                    }

                    if (representativeDTO.NameEn != null)
                        participatingentity.NameEn = representativeDTO.NameEn.Trim();
                    if (representativeDTO.NameAr != null)
                        participatingentity.NameAr = representativeDTO.NameAr.Trim();
                    if (representativeDTO.Profession != null)
                        participatingentity.Profession = representativeDTO.Profession.Trim();
                    participatingentity.IsVisitedUae = representativeDTO.IsVisitedUae;

                    participatingentity.Telephone = representativeDTO.PhoneISD + "$" + representativeDTO.PhoneSTD + "$" + representativeDTO.Phone;
                    participatingentity.Mobile = representativeDTO.MobileISD + "$" + representativeDTO.MobileSTD + "$" + representativeDTO.Mobile;
                 
                    if (representativeDTO.IsUAEResident != null)
                        participatingentity.IsUaeresident = representativeDTO.IsUAEResident.Trim();

                    #region Visa
                    if (representativeDTO.IsVisaNeed)
                    {
                        participatingentity.NeedVisa = EnumConversion.ToString(EnumBool.Yes);
                    }
                    else
                    {
                        participatingentity.NeedVisa = EnumConversion.ToString(EnumBool.No);
                    }
                    if (representativeDTO.PassportNumber != null)
                        participatingentity.Passport = representativeDTO.PassportNumber.Trim();
                    if (representativeDTO.PlaceOfIssue != null)
                        participatingentity.PlaceOfIssue = representativeDTO.PlaceOfIssue.Trim();
                    if (representativeDTO.PassportDateOfIssue != null)
                        participatingentity.DateOfIssue = Convert.ToDateTime(representativeDTO.PassportDateOfIssue.Trim());
                    if (representativeDTO.PassportExpiryDate != null)
                        participatingentity.ExpiryDate = Convert.ToDateTime(representativeDTO.PassportExpiryDate.Trim());

                    if (!string.IsNullOrEmpty(representativeDTO.IsAddHealthInsurance))
                    {
                        if (representativeDTO.IsAddHealthInsurance == EnumConversion.ToString(EnumBool.Yes))
                            participatingentity.IsAddHealthInsurance = representativeDTO.IsAddHealthInsurance;
                        else
                            participatingentity.IsAddHealthInsurance = EnumConversion.ToString(EnumBool.No);
                    }

                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64Passport))
                    {
                        ext = representativeDTO.FileExtensionofPassport.ToLower();
                        filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                        if (ext == "pdf")
                            filename = WriteTextOnPDF(representativeDTO.FileBase64Passport, filename, representativeDTO.NameEn, participatingentity.MemberExhibitionYearlyId, ext);
                        else
                            filename = WriteTextOnImage(representativeDTO.FileBase64Passport, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                        participatingentity.PassportCopy = filename;
                        ispassportuploaded = true;
                    }
                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64Passport2))
                    {
                        ext = representativeDTO.FileExtensionofPassport2.ToLower();
                        filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                        if (ext == "pdf")
                            filename = WriteTextOnPDF(representativeDTO.FileBase64Passport2, filename, representativeDTO.NameEn, participatingentity.MemberExhibitionYearlyId, ext);
                        else
                            filename = WriteTextOnImage(representativeDTO.FileBase64Passport2, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                        participatingentity.PassportCopyTwo = filename;
                        ispassportuploaded = true;
                    }
                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64PersonalPhoto))
                    {
                        filename = SaveImage(representativeDTO.FileBase64PersonalPhoto, representativeDTO.FileExtensionofPersonalPhoto, representativeDTO.NameEn, PhotoPath, representativeDTO.FileExtensionofPersonalPhoto);
                        participatingentity.Photo = filename;
                    }
                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64Visa))
                    {
                        filename = SaveImage(representativeDTO.FileBase64Visa, representativeDTO.FileExtensionVisa, representativeDTO.NameEn, VisaFormPath, representativeDTO.FileExtensionVisa);
                        participatingentity.VisaApplicationForm = filename;
                    }
                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64FlightTickets))
                    {
                        filename = SaveImage(representativeDTO.FileBase64FlightTickets, representativeDTO.FileExtensionofFlightTickets, representativeDTO.NameEn, FlightTicketsPath, representativeDTO.FileExtensionofFlightTickets);
                        participatingentity.FlightTickets = filename;
                    }
                    #endregion
                    participatingentity.ModifiedOn = MethodFactory.ArabianTimeNow();
                    participatingentity.ModifiedBy = representativeDTO.MemberId;


                    if (participatingentity.Status != null)
                    {
                        if (participatingentity.Status == EnumConversion.ToString(EnumRepresentativeStatus.PendingDocumentation))
                        {
                            participatingentity.Status = EnumConversion.ToString(EnumRepresentativeStatus.DocumentsUploaded);
                            isdocumentuploaded = true;
                        }
                    }

                    if (ispassportuploaded && (participatingentity.Status == EnumConversion.ToString(EnumRepresentativeStatus.VisaProcessing) || participatingentity.Status == EnumConversion.ToString(EnumRepresentativeStatus.VisaUploaded)))
                    {
                        participatingentity.Status = EnumConversion.ToString(EnumRepresentativeStatus.ReIssueVisa);
                        participatingentity.ReIssueVisaCount = (participatingentity.ReIssueVisaCount ?? 0) + 1;
                    }
                    else if (ispassportuploaded && (participatingentity.Status == EnumConversion.ToString(EnumRepresentativeStatus.Approved)))
                    {
                        participatingentity.PassportModifiedCountApproved = (participatingentity.PassportModifiedCountApproved ?? 0) + 1;
                    }

                    if (ExhibitionRepresentativeParticipatingService.UpdateExhibitionRepresentativeParticipating(participatingentity) == EnumResultType.Success)
                    {
                        if (ispassportuploaded && participatingentity.Status == EnumConversion.ToString(EnumRepresentativeStatus.ReIssueVisa))
                        {
                            NotifyAdminAboutDocumentUploaded(parententityRepresentative, participatingentity, representativeDTO);
                        }
                        else if (isdocumentuploaded)
                        {
                            NotifyAdminAboutDocumentUploaded(parententityRepresentative, participatingentity, representativeDTO);
                        }
                        UpdateExhibitionRepresentativeFromTabModify(participatingentity, representativeDTO);
                        representativeDTO.IsEditClicked = false;
                        //ResetControls();
                        if (langid == 1)
                            MessageDTO.Message = "Representative details updated successfully.";// _stringLocalizer["RepresentativeUpdatedSucessfully"];
                        else
                            MessageDTO.Message = "تم تعديل تفاصيل المندوب بنجاح";// _stringLocalizer["RepresentativeUpdatedSucessfully"];
                        MessageDTO.MessageTypeResponse = "Success";
                        //mvRepresentativeForm.SetActiveView(vwThankYou);
                        //pnlMessage.Visible = false;
                    }
                    else
                    {
                        if (langid == 1)
                            MessageDTO.Message = "Failed to update the representative details.";
                        else
                            MessageDTO.Message = "لم يتم تعديل تفاصيل المندوب";// _stringLocalizer["RepresentativeFailedToUpdate"];
                        MessageDTO.MessageTypeResponse = "Error";
                    }
                }
            }
            return MessageDTO;
        }
        private MessageDTO UpdateContentNoVisa(SaveRepresentativeDTO representativeDTO, long langid)
        {
            bool isdocumentuploaded = false;
            bool ispassportuploaded = false;
            string filename = string.Empty;
            using (ExhibitionRepresentativeParticipatingNoVisaService ExhibitionRepresentativeParticipatingNoVisaService = new ExhibitionRepresentativeParticipatingNoVisaService())
            {
                ExhibitionRepresentativeService = new ExhibitionRepresentativeService();
                var parententityRepresentative = ExhibitionRepresentativeService.GetRepresentativeByItemId(representativeDTO.RepresentativeId);

                var participatingNoVisaEntity = ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa(new XsiExhibitionRepresentativeParticipatingNoVisa() { RepresentativeId = representativeDTO.RepresentativeId, MemberExhibitionYearlyId = representativeDTO.MemberExhibitionYearlyId }).FirstOrDefault();
                if (participatingNoVisaEntity != null)
                {
                    if (representativeDTO.NationalityId > 0)
                        participatingNoVisaEntity.CountryId = representativeDTO.NationalityId;

                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64CountryUID))
                    {
                        filename = SaveImage(representativeDTO.FileBase64CountryUID, representativeDTO.FileExtensionofCountryUID, representativeDTO.NameEn, CountryUIDPath, representativeDTO.FileExtensionofCountryUID);
                        //fuUploadCountryUID.SaveAs(string.Format("{0}{1}", Server.MapPath(CountryUIDPath), filename));
                        participatingNoVisaEntity.CountryUidno = filename;
                    }

                    if (representativeDTO.NameEn != null)
                        participatingNoVisaEntity.NameEn = representativeDTO.NameEn.Trim();
                    if (representativeDTO.NameAr != null)
                        participatingNoVisaEntity.NameAr = representativeDTO.NameAr.Trim();
                    if (representativeDTO.Profession != null)
                        participatingNoVisaEntity.Profession = representativeDTO.Profession.Trim();
                    participatingNoVisaEntity.IsVisitedUae = representativeDTO.IsVisitedUae;

                    participatingNoVisaEntity.Telephone = representativeDTO.PhoneISD + "$" + representativeDTO.PhoneSTD + "$" + representativeDTO.Phone;
                    participatingNoVisaEntity.Mobile = representativeDTO.MobileISD + "$" + representativeDTO.MobileSTD + "$" + representativeDTO.Mobile;

                    if (representativeDTO.IsUAEResident != null)
                        participatingNoVisaEntity.IsUaeresident = representativeDTO.IsUAEResident.Trim();

                    #region Visa
                    if (representativeDTO.IsVisaNeed)
                    {
                        participatingNoVisaEntity.NeedVisa = EnumConversion.ToString(EnumBool.Yes);
                    }
                    else
                    {
                        participatingNoVisaEntity.NeedVisa = EnumConversion.ToString(EnumBool.No);
                    }

                    if (!string.IsNullOrEmpty(representativeDTO.IsAddHealthInsurance))
                    {
                        if (representativeDTO.IsAddHealthInsurance == EnumConversion.ToString(EnumBool.Yes))
                            participatingNoVisaEntity.IsAddHealthInsurance = representativeDTO.IsAddHealthInsurance;
                        else
                            participatingNoVisaEntity.IsAddHealthInsurance = EnumConversion.ToString(EnumBool.No);
                    }
                    if (representativeDTO.PassportNumber != null)
                        participatingNoVisaEntity.Passport = representativeDTO.PassportNumber.Trim();
                    if (representativeDTO.PlaceOfIssue != null)
                        participatingNoVisaEntity.PlaceOfIssue = representativeDTO.PlaceOfIssue.Trim();
                    if (representativeDTO.PassportDateOfIssue != null)
                        participatingNoVisaEntity.DateOfIssue = Convert.ToDateTime(representativeDTO.PassportDateOfIssue.Trim());
                    if (representativeDTO.PassportExpiryDate != null)
                        participatingNoVisaEntity.ExpiryDate = Convert.ToDateTime(representativeDTO.PassportExpiryDate.Trim());



                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64Passport))
                    {
                        ext = representativeDTO.FileExtensionofPassport.ToLower();
                        filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                        if (ext == "pdf")
                            filename = WriteTextOnPDF(representativeDTO.FileBase64Passport, filename, representativeDTO.NameEn, participatingNoVisaEntity.MemberExhibitionYearlyId, ext);
                        else
                            filename = WriteTextOnImage(representativeDTO.FileBase64Passport, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                        participatingNoVisaEntity.PassportCopy = filename;
                        ispassportuploaded = true;
                    }
                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64Passport2))
                    {
                        ext = representativeDTO.FileExtensionofPassport2.ToLower();
                        filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                        if (ext == "pdf")
                            filename = WriteTextOnPDF(representativeDTO.FileBase64Passport2, filename, representativeDTO.NameEn, participatingNoVisaEntity.MemberExhibitionYearlyId, ext);
                        else
                            filename = WriteTextOnImage(representativeDTO.FileBase64Passport2, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                        participatingNoVisaEntity.PassportCopyTwo = filename;
                        ispassportuploaded = true;
                    }
                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64PersonalPhoto))
                    {
                        filename = SaveImage(representativeDTO.FileBase64PersonalPhoto, representativeDTO.FileExtensionofPersonalPhoto, representativeDTO.NameEn, PhotoPath, representativeDTO.FileExtensionofPersonalPhoto);
                        participatingNoVisaEntity.Photo = filename;
                    }
                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64Visa))
                    {
                        filename = SaveImage(representativeDTO.FileBase64Visa, representativeDTO.FileExtensionVisa, representativeDTO.NameEn, VisaFormPath, representativeDTO.FileExtensionVisa);
                        participatingNoVisaEntity.VisaApplicationForm = filename;
                    }
                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64FlightTickets))
                    {
                        filename = SaveImage(representativeDTO.FileBase64FlightTickets, representativeDTO.FileExtensionofFlightTickets, representativeDTO.NameEn, FlightTicketsPath, representativeDTO.FileExtensionofFlightTickets);
                        participatingNoVisaEntity.FlightTickets = filename;
                    }
                    #endregion
                    participatingNoVisaEntity.ModifiedOn = MethodFactory.ArabianTimeNow();
                    participatingNoVisaEntity.ModifiedBy = representativeDTO.MemberId;


                    if (participatingNoVisaEntity.Status != null)
                    {
                        if (participatingNoVisaEntity.Status == EnumConversion.ToString(EnumRepresentativeStatus.PendingDocumentation))
                        {
                            participatingNoVisaEntity.Status = EnumConversion.ToString(EnumRepresentativeStatus.DocumentsUploaded);
                            isdocumentuploaded = true;
                        }
                    }

                    if (ispassportuploaded && (participatingNoVisaEntity.Status == EnumConversion.ToString(EnumRepresentativeStatus.VisaProcessing) || participatingNoVisaEntity.Status == EnumConversion.ToString(EnumRepresentativeStatus.VisaUploaded)))
                    {
                        participatingNoVisaEntity.Status = EnumConversion.ToString(EnumRepresentativeStatus.ReIssueVisa);
                        participatingNoVisaEntity.ReIssueVisaCount = (participatingNoVisaEntity.ReIssueVisaCount ?? 0) + 1;
                    }
                    else if (ispassportuploaded && (participatingNoVisaEntity.Status == EnumConversion.ToString(EnumRepresentativeStatus.Approved)))
                    {
                        participatingNoVisaEntity.PassportModifiedCountApproved = (participatingNoVisaEntity.PassportModifiedCountApproved ?? 0) + 1;
                    }

                    if (ExhibitionRepresentativeParticipatingNoVisaService.UpdateExhibitionRepresentativeParticipatingNoVisa(participatingNoVisaEntity) == EnumResultType.Success)
                    {
                        if (ispassportuploaded && participatingNoVisaEntity.Status == EnumConversion.ToString(EnumRepresentativeStatus.ReIssueVisa))
                        {
                            NotifyAdminAboutDocumentUploadedNoVisa(parententityRepresentative, participatingNoVisaEntity, representativeDTO);
                        }
                        else if (isdocumentuploaded)
                        {
                            NotifyAdminAboutDocumentUploadedNoVisa(parententityRepresentative, participatingNoVisaEntity, representativeDTO);
                        }
                        UpdateExhibitionRepresentativeFromTabModifyNoVisa(participatingNoVisaEntity, representativeDTO);
                        representativeDTO.IsEditClicked = false;
                        //ResetControls();
                        if (langid == 1)
                            MessageDTO.Message = "Representative details updated successfully.";// _stringLocalizer["RepresentativeUpdatedSucessfully"];
                        else
                            MessageDTO.Message = "تم تعديل تفاصيل المندوب بنجاح";// _stringLocalizer["RepresentativeUpdatedSucessfully"];
                        MessageDTO.MessageTypeResponse = "Success";
                        //mvRepresentativeForm.SetActiveView(vwThankYou);
                        //pnlMessage.Visible = false;
                    }
                    else
                    {
                        if (langid == 1)
                            MessageDTO.Message = "Failed to update the representative details.";
                        else
                            MessageDTO.Message = "لم يتم تعديل تفاصيل المندوب";// _stringLocalizer["RepresentativeFailedToUpdate"];
                        MessageDTO.MessageTypeResponse = "Error";
                    }
                }
            }
            return MessageDTO;
        }
        private bool UpdateNReregister(SaveRepresentativeDTO representativeDTO)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            string filename = string.Empty;
            EnumResultType result = EnumResultType.Failed;
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var exhibitor = MethodFactory.GetExhibitor(representativeDTO.MemberId, representativeDTO.WebsiteId, _context);
                if (exhibitor != null)
                {
                    #region Exhibition Representive
                    using (ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService())
                    {
                        var parentEntity = ExhibitionRepresentativeService.GetRepresentativeByItemId(representativeDTO.RepresentativeId);
                        if (parentEntity != null)
                        {
                            DateTime dob = new DateTime();
                            DateTime.TryParse(representativeDTO.DateofBirth, out dob);
                            parentEntity.MemberId = SIBFMemberId;
                            if (representativeDTO.NameEn != null && !string.IsNullOrEmpty(representativeDTO.NameEn))
                                parentEntity.NameEn = representativeDTO.NameEn.Trim();

                            if (representativeDTO.NameAr != null && !string.IsNullOrEmpty(representativeDTO.NameAr))
                                parentEntity.NameAr = representativeDTO.NameAr.Trim();

                            if (!string.IsNullOrEmpty(representativeDTO.DateofBirth))
                                parentEntity.Dob = dob;
                            parentEntity.PlaceOfBirth = representativeDTO.PlaceOfBirth;
                            parentEntity.ModifiedBy = SIBFMemberId;
                            parentEntity.ModifiedOn = MethodFactory.ArabianTimeNow();
                            result = ExhibitionRepresentativeService.UpdateExhibitionRepresentative(parentEntity);
                        }

                        #endregion
                        if (result == EnumResultType.Success)
                        {
                            using (ExhibitionRepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService())
                            {
                                XsiExhibitionRepresentativeParticipating participatingEntity = new XsiExhibitionRepresentativeParticipating();
                                if (participatingEntity != null)
                                {
                                    //  ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService();

                                    participatingEntity.RepresentativeId = parentEntity.RepresentativeId;
                                    participatingEntity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                    participatingEntity.LanguageId = LangId;
                                    if (exhibitor != null)
                                        participatingEntity.MemberExhibitionYearlyId = exhibitor.MemberExhibitionYearlyId;
                                    //entity.ExhibitionId = representativeFormDTO.ExhibitionId;
                                    participatingEntity.CountryId = representativeDTO.NationalityId;
                                    participatingEntity.Status = EnumConversion.ToString(EnumRepresentativeStatus.Pending);

                                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64CountryUID))
                                    {
                                        filename = SaveImage(representativeDTO.FileBase64CountryUID, representativeDTO.FileExtensionofCountryUID, representativeDTO.NameEn, CountryUIDPath, representativeDTO.FileExtensionofCountryUID);
                                        participatingEntity.CountryUidno = filename;
                                    }


                                    if (representativeDTO.NameEn != null)
                                        participatingEntity.NameEn = representativeDTO.NameEn.Trim();
                                    if (representativeDTO.NameAr != null)
                                        participatingEntity.NameAr = representativeDTO.NameAr.Trim();
                                    if (representativeDTO.Profession != null)
                                        participatingEntity.Profession = representativeDTO.Profession.Trim();
                                    //if (ddlIsVisitedUAE.SelectedIndex > 0)
                                    participatingEntity.IsVisitedUae = representativeDTO.IsVisitedUae;

                                    participatingEntity.ArrivalDate = null;
                                    participatingEntity.DepartureDate = null;
                                    //rpEntity.ArrivalAirportId = -1;
                                    //rpEntity.ArrivalTerminalId = -1;
                                    participatingEntity.VisaCopy = string.Empty;
                                    participatingEntity.FlightTickets = string.Empty;
                                    participatingEntity.Telephone = representativeDTO.PhoneISD + "$" + representativeDTO.PhoneSTD + "$" + representativeDTO.Phone;
                                    participatingEntity.Mobile = representativeDTO.MobileISD + "$" + representativeDTO.MobileSTD + "$" + representativeDTO.Mobile;
                                    
                                    if (representativeDTO.IsUAEResident != null)
                                        participatingEntity.IsUaeresident = representativeDTO.IsUAEResident.Trim();

                                    #region Visa
                                    if (representativeDTO.IsVisaNeed)
                                    {
                                        participatingEntity.NeedVisa = EnumConversion.ToString(EnumBool.Yes);
                                    }
                                    else
                                    {
                                        participatingEntity.NeedVisa = EnumConversion.ToString(EnumBool.No);
                                    }
                                    if (representativeDTO.PassportNumber != null)
                                        participatingEntity.Passport = representativeDTO.PassportNumber.Trim();
                                    if (representativeDTO.PlaceOfIssue != null)
                                        participatingEntity.PlaceOfIssue = representativeDTO.PlaceOfIssue.Trim();
                                    if (representativeDTO.PassportDateOfIssue != null)
                                        participatingEntity.DateOfIssue = Convert.ToDateTime(representativeDTO.PassportDateOfIssue.Trim());

                                    if (representativeDTO.PassportNumber != null)
                                        participatingEntity.ExpiryDate = Convert.ToDateTime(representativeDTO.PassportExpiryDate.Trim());

                                    if (!string.IsNullOrEmpty(representativeDTO.IsAddHealthInsurance))
                                    {
                                        if (representativeDTO.IsAddHealthInsurance == EnumConversion.ToString(EnumBool.Yes))
                                            participatingEntity.IsAddHealthInsurance = representativeDTO.IsAddHealthInsurance;
                                        else
                                            participatingEntity.IsAddHealthInsurance = EnumConversion.ToString(EnumBool.No);
                                    }
                                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64Passport))
                                    {
                                        ext = representativeDTO.FileExtensionofPassport;
                                        filename = string.Format("{0}_{1}.{2}", LangId, MethodFactory.GetRandomNumber(), ext);
                                        if (representativeDTO.FileExtensionofPassport.ToLower() == ".pdf" || representativeDTO.FileExtensionofPassport.ToLower() == "pdf") //1=EnglishId
                                            filename = WriteTextOnPDF(representativeDTO.FileBase64Passport, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                                        else
                                            filename = WriteTextOnImage(representativeDTO.FileBase64Passport, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                                        participatingEntity.PassportCopy = filename;
                                    }
                                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64Passport2))
                                    {
                                        ext = representativeDTO.FileExtensionofPassport2;
                                        filename = string.Format("{0}_{1}.{2}", LangId, MethodFactory.GetRandomNumber(), ext);
                                        if (ext.ToLower() == ".pdf" || representativeDTO.FileExtensionofPassport.ToLower() == "pdf")
                                            filename = WriteTextOnPDF(representativeDTO.FileBase64Passport2, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                                        else
                                            filename = WriteTextOnImage(representativeDTO.FileBase64Passport2, filename, representativeDTO.NameEn, representativeDTO.MemberExhibitionYearlyId, ext);
                                        participatingEntity.PassportCopyTwo = filename;
                                    }
                                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64PersonalPhoto))
                                    {
                                        //  filename = string.Format("{0}_{1}.{2}", LangId, MethodFactory.GetRandomNumber(), representativeDTO.FileExtensionofPersonalPhoto);
                                        filename = SaveImage(representativeDTO.FileBase64PersonalPhoto, representativeDTO.FileExtensionofPersonalPhoto, representativeDTO.NameEn, PhotoPath, representativeDTO.FileExtensionofPersonalPhoto);
                                        participatingEntity.Photo = filename;
                                    }
                                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64Visa))
                                    {
                                        filename = SaveImage(representativeDTO.FileBase64Visa, representativeDTO.FileExtensionVisa, representativeDTO.NameEn, VisaFormPath, representativeDTO.FileExtensionVisa);
                                        participatingEntity.VisaApplicationForm = filename;
                                    }
                                    if (!string.IsNullOrEmpty(representativeDTO.FileBase64FlightTickets))
                                    {
                                        filename = SaveImage(representativeDTO.FileBase64FlightTickets, representativeDTO.FileExtensionofFlightTickets, representativeDTO.NameEn, FlightTicketsPath, representativeDTO.FileExtensionofFlightTickets);
                                        participatingEntity.FlightTickets = filename;
                                    }
                                    #endregion
                                    participatingEntity.ModifiedOn = MethodFactory.ArabianTimeNow();
                                    participatingEntity.ModifiedBy = representativeDTO.MemberId;
                                    if (ExhibitionRepresentativeParticipatingService.InsertExhibitionRepresentativeParticipating(participatingEntity) == EnumResultType.Success)
                                    {
                                        representativeDTO.IsEditClicked = false;
                                        //ResetControls();
                                        if (LangId == 1)
                                            MessageDTO.Message = "Representative details have been sent for Admin’s approval. We will get back to you at our earliest.";// _stringLocalizer["RepresentativeInsertSucessfull"];
                                        else
                                            MessageDTO.Message = "تفاصيل المندوب الذي قمت بإضافته سيتم عرضه على الإدارة للموافقة. سيتم الرد عليك في أقرب وقت"; // _stringLocalizer["RepresentativeInsertSucessfull"];
                                        MessageDTO.MessageTypeResponse = "Success";
                                    }
                                    else
                                    {
                                        if (LangId == 1)
                                            MessageDTO.Message = "Failed to register the representative."; // _stringLocalizer["RepresentativeFailedToReRegister"];
                                        else
                                            MessageDTO.Message = "لم يتم تسجيل المندوب";// _stringLocalizer["RepresentativeFailedToReRegister"];


                                        MessageDTO.MessageTypeResponse = "Error";
                                    }
                                }
                            }
                        }
                    }
                    return true;
                }
                return false;
            }
        }
        private void UpdateTravelDetails(TravelDTO travelDto)
        {
            //WebsiteId = travelDto.WebsiteId;
            using (ExhibitionRepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService())
            {
                var participatingentity = ExhibitionRepresentativeParticipatingService.GetExhibitionRepresentativeParticipating(new XsiExhibitionRepresentativeParticipating() { RepresentativeId = travelDto.RepresentativeId, MemberExhibitionYearlyId = travelDto.MemberExhibitionYearlyId }).FirstOrDefault();
                if (participatingentity != null)
                {
                    participatingentity.ArrivalDate = travelDto.ArrivalDate;
                    participatingentity.DepartureDate = travelDto.DepartureDate;
                    participatingentity.ArrivalAirportId = travelDto.ArrivalAirportId;
                    participatingentity.ArrivalTerminalId = travelDto.ArrivalTerminalId;
                    participatingentity.Status = EnumConversion.ToString(EnumRepresentativeStatus.Approved);
                    participatingentity.ModifiedOn = MethodFactory.ArabianTimeNow();
                    participatingentity.ModifiedBy = travelDto.MemberId;
                    if (ExhibitionRepresentativeParticipatingService.UpdateExhibitionRepresentativeParticipating(participatingentity) == EnumResultType.Success)
                    {
                        #region ExhibitionRepresentative Status Update
                        ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService();
                        XsiExhibitionRepresentativeParticipating where = new XsiExhibitionRepresentativeParticipating();
                        where.RepresentativeId = travelDto.RepresentativeId;
                        where.MemberExhibitionYearlyId = participatingentity.MemberExhibitionYearlyId;
                        //where.ExhibitionId = ExhibitionId;
                        where.Status = EnumConversion.ToString(EnumRepresentativeStatus.VisaUploaded);
                        var entity1 = ExhibitionRepresentativeParticipatingService.GetExhibitionRepresentativeParticipating(where).OrderByDescending(o => o.RepresentativeId).FirstOrDefault();
                        if (entity1 != null)
                        {
                            entity1.Status = EnumConversion.ToString(EnumRepresentativeStatus.Approved);
                            ExhibitionRepresentativeParticipatingService.UpdateExhibitionRepresentativeParticipating(entity1);
                            SendEmail(participatingentity, travelDto.WebsiteId);
                        }
                        #endregion
                        //HidePaymentDetails(); 
                    }
                    else
                    {
                        //ShowPaymentDetails();
                        MessageDTO.Message = "Failed to add representative details, Please try adding again.";
                        MessageDTO.MessageTypeResponse = "Error";
                    }
                }
                else
                {
                    ExhibitionRepresentativeParticipatingNoVisaService ExhibitionRepresentativeParticipatingNoVisaService = new ExhibitionRepresentativeParticipatingNoVisaService();
                    var participatingnovisaentity = ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa(new XsiExhibitionRepresentativeParticipatingNoVisa() { RepresentativeId = travelDto.RepresentativeId, MemberExhibitionYearlyId = travelDto.MemberExhibitionYearlyId }).FirstOrDefault();
                    if (participatingnovisaentity != null)
                    {
                        participatingnovisaentity.ArrivalDate = travelDto.ArrivalDate;
                        participatingnovisaentity.DepartureDate = travelDto.DepartureDate;
                        participatingnovisaentity.ArrivalAirportId = travelDto.ArrivalAirportId;
                        participatingnovisaentity.ArrivalTerminalId = travelDto.ArrivalTerminalId;
                        participatingnovisaentity.Status = EnumConversion.ToString(EnumRepresentativeStatus.Approved);
                        participatingnovisaentity.ModifiedOn = MethodFactory.ArabianTimeNow();
                        participatingnovisaentity.ModifiedBy = travelDto.MemberId;
                        if (ExhibitionRepresentativeParticipatingNoVisaService.UpdateExhibitionRepresentativeParticipatingNoVisa(participatingnovisaentity) == EnumResultType.Success)
                        {
                            #region ExhibitionRepresentative Status Update
                            ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService();
                            XsiExhibitionRepresentativeParticipating where = new XsiExhibitionRepresentativeParticipating();
                            where.RepresentativeId = travelDto.RepresentativeId;
                            where.MemberExhibitionYearlyId = participatingnovisaentity.MemberExhibitionYearlyId;
                            //where.ExhibitionId = ExhibitionId;
                            where.Status = EnumConversion.ToString(EnumRepresentativeStatus.VisaUploaded);
                            var entity1 = ExhibitionRepresentativeParticipatingService.GetExhibitionRepresentativeParticipating(where).OrderByDescending(o => o.RepresentativeId).FirstOrDefault();
                            if (entity1 != null)
                            {
                                entity1.Status = EnumConversion.ToString(EnumRepresentativeStatus.Approved);
                                ExhibitionRepresentativeParticipatingService.UpdateExhibitionRepresentativeParticipating(entity1);
                                SendEmailParticipatingNovisa(participatingnovisaentity, travelDto.WebsiteId);
                            }
                            #endregion
                            //HidePaymentDetails(); 
                        }
                        else
                        {
                            //ShowPaymentDetails();
                            MessageDTO.Message = "Failed to add representative details, Please try adding again.";
                            MessageDTO.MessageTypeResponse = "Error";
                        }
                    }
                }
            }
        }
        EnumResultType UpdateExhibitionRepresentativeFromTabModify(XsiExhibitionRepresentativeParticipating entity, SaveRepresentativeDTO representativeDTO)
        {
            using (ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService())
            {
                var entity1 = ExhibitionRepresentativeService.GetRepresentativeByItemId(entity.RepresentativeId);
                if (entity1 != null)
                {
                    DateTime dob = new DateTime();
                    DateTime.TryParse(representativeDTO.DateofBirth, out dob);

                    entity1.NameEn = entity.NameEn;
                    entity1.NameAr = entity.NameAr;

                    if (!string.IsNullOrEmpty(representativeDTO.DateofBirth))
                        entity1.Dob = dob;
                    entity1.PlaceOfBirth = representativeDTO.PlaceOfBirth;

                    entity1.ModifiedOn = MethodFactory.ArabianTimeNow();
                    entity1.ModifiedBy = representativeDTO.MemberId;
                    return ExhibitionRepresentativeService.UpdateExhibitionRepresentative(entity1);
                }
            }
            return EnumResultType.Failed;
        }
        EnumResultType UpdateExhibitionRepresentativeFromTabModifyNoVisa(XsiExhibitionRepresentativeParticipatingNoVisa entity, SaveRepresentativeDTO representativeDTO)
        {
            using (ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService())
            {
                var entity1 = ExhibitionRepresentativeService.GetRepresentativeByItemId(entity.RepresentativeId);
                if (entity1 != null)
                {
                    DateTime dob = new DateTime();
                    DateTime.TryParse(representativeDTO.DateofBirth, out dob);

                    entity1.NameEn = entity.NameEn;
                    entity1.NameAr = entity.NameAr;

                    if (!string.IsNullOrEmpty(representativeDTO.DateofBirth))
                        entity1.Dob = dob;
                    entity1.PlaceOfBirth = representativeDTO.PlaceOfBirth;

                    entity1.ModifiedOn = MethodFactory.ArabianTimeNow();
                    entity1.ModifiedBy = representativeDTO.MemberId;
                    return ExhibitionRepresentativeService.UpdateExhibitionRepresentative(entity1);
                }
            }
            return EnumResultType.Failed;
        }
        #endregion

        #region Read Representative
        #region Load
        LoadRepresentativeDTO LoadContent(long representativeId, long exhibitorid, long langid, LoadRepresentativeDTO LoadRepresentativeDTO)
        {
            using (ExhibitionRepresentativeParticipatingService representativeParticipatingService = new ExhibitionRepresentativeParticipatingService())
            {
                ExhibitionRepresentativeService representativeService = new ExhibitionRepresentativeService();
                XsiExhibitionRepresentative parententityRepresentative = representativeService.GetRepresentativeByItemId(representativeId);

                XsiExhibitionRepresentativeParticipating participatingentity = representativeParticipatingService.GetExhibitionRepresentativeParticipating(new XsiExhibitionRepresentativeParticipating() { RepresentativeId = representativeId, MemberExhibitionYearlyId = exhibitorid }).FirstOrDefault();

                if (participatingentity == null)
                {
                    ExhibitionRepresentativeParticipatingNoVisaService ExhibitionRepresentativeParticipatingNoVisaService = new ExhibitionRepresentativeParticipatingNoVisaService();
                    XsiExhibitionRepresentativeParticipatingNoVisa participatingnovisaentity = ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa(new XsiExhibitionRepresentativeParticipatingNoVisa() { RepresentativeId = representativeId, MemberExhibitionYearlyId = exhibitorid }).FirstOrDefault();
                    if (participatingnovisaentity != null)
                    {
                        LoadRepresentativeDTO = LoadEntityFromParticipatingNoVisa(LoadRepresentativeDTO, parententityRepresentative, participatingnovisaentity);
                    }
                }
                else if (participatingentity != null)
                {
                    LoadRepresentativeDTO = LoadEntityFromParticipating(LoadRepresentativeDTO, parententityRepresentative, participatingentity);
                }
                return LoadRepresentativeDTO;
            }
        }
        private LoadRepresentativeDTO LoadEntityFromParticipating(LoadRepresentativeDTO representativeDTO, XsiExhibitionRepresentative parententityRepresentative, XsiExhibitionRepresentativeParticipating participatingentity)
        {
            #region No Visa Entity
            representativeDTO.RepresentativeId = participatingentity.RepresentativeId;
            representativeDTO.MemberExhibitionYearlyId = participatingentity.MemberExhibitionYearlyId;
            representativeDTO.MemberId = parententityRepresentative.MemberId.Value;

            if (participatingentity.NameEn != null)
                representativeDTO.NameEn = participatingentity.NameEn;
            if (participatingentity.NameAr != null)
                representativeDTO.NameAr = participatingentity.NameAr;
            if (participatingentity.Profession != null)
                representativeDTO.Profession = participatingentity.Profession;

            if (participatingentity.CountryId != null)
                representativeDTO.NationalityId = participatingentity.CountryId.Value;

            if (participatingentity.IsVisitedUae != null)
                representativeDTO.IsVisitedUae = participatingentity.IsVisitedUae;

            if (participatingentity.IsUaeresident != null)
                representativeDTO.IsUAEResident = participatingentity.IsUaeresident;

            if (parententityRepresentative != null)
            {
                if (parententityRepresentative.Dob != null)
                    representativeDTO.DateofBirth = parententityRepresentative.Dob.Value.ToString("MM/dd/yyyy");
                if (parententityRepresentative.PlaceOfBirth != null)
                    representativeDTO.PlaceOfBirth = parententityRepresentative.PlaceOfBirth;
            }

            if (participatingentity.Status != null && !string.IsNullOrEmpty(participatingentity.Status))
                representativeDTO.Status = participatingentity.Status;
            else
                representativeDTO.Status = EnumConversion.ToString(EnumRepresentativeStatus.Pending);

            if (participatingentity.IsAddHealthInsurance != null && !string.IsNullOrEmpty(participatingentity.IsAddHealthInsurance))
                representativeDTO.IsAddHealthInsurance = participatingentity.IsAddHealthInsurance;

            #region Phone,Mobile
            if (participatingentity.Telephone != null)
            {
                string[] str = participatingentity.Telephone.Split('$');
                if (str.Count() == 3)
                {
                    representativeDTO.PhoneISD = str[0].Trim();
                    representativeDTO.PhoneSTD = str[1].Trim();
                    representativeDTO.Phone = str[2].Trim();
                }
                else if (str.Count() == 2)
                {
                    representativeDTO.PhoneSTD = str[0].Trim();
                    representativeDTO.Phone = str[1].Trim();
                }
                else
                    representativeDTO.Phone = str[0].Trim();
            }
            if (participatingentity.Mobile != null)
            {
                string[] str = participatingentity.Mobile.Split('$');
                if (str.Count() == 3)
                {
                    representativeDTO.MobileISD = str[0].Trim();
                    representativeDTO.MobileSTD = str[1].Trim();
                    representativeDTO.Mobile = str[2].Trim();
                }
                else if (str.Count() == 2)
                {
                    representativeDTO.MobileSTD = str[0].Trim();
                    representativeDTO.Mobile = str[1].Trim();
                }
                else
                    representativeDTO.Mobile = str[0].Trim();
            }
            #endregion
            #region Visa
            if (participatingentity.NeedVisa != null)
            {
                //ShowVisaControls();
                if (participatingentity.NeedVisa == EnumConversion.ToString(EnumBool.Yes))
                {
                    representativeDTO.IsVisaNeed = true;

                }
                if (participatingentity.VisaApplicationForm != null)
                    if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + VisaFormPath + participatingentity.VisaApplicationForm))
                        representativeDTO.VisaHref = _appCustomSettings.UploadsCMSPath + "/Representative/VisaForm/" + participatingentity.VisaApplicationForm;
            }
            #endregion
            //rbtnYesRep.Checked = true;
            //rbtnNoRep.Checked = false;
            if (participatingentity.Passport != null)
            {
                // RepresentativeDTO.Passport = entity.Passport;
                representativeDTO.PassportNumber = participatingentity.Passport;
            }
            if (participatingentity.PlaceOfIssue != null)
                representativeDTO.PlaceOfIssue = participatingentity.PlaceOfIssue;
            if (participatingentity.DateOfIssue != null)
                representativeDTO.PassportDateOfIssue = participatingentity.DateOfIssue.Value.ToString("MM/dd/yyyy");

            if (participatingentity.ExpiryDate != null)
                representativeDTO.PassportExpiryDate = participatingentity.ExpiryDate.Value.ToString("MM/dd/yyyy");

            //  string downloadUrl = _appCustomSettings.UploadsCMSPath + "/Books/ExportBooks/" + excelName;
            if (participatingentity.PassportCopy != null)
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PassportPath + participatingentity.PassportCopy))
                    representativeDTO.PassportHRef = _appCustomSettings.UploadsCMSPath + "/Representative/Passport/" + participatingentity.PassportCopy;

            if (participatingentity.PassportCopyTwo != null)
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PassportPath + participatingentity.PassportCopyTwo))
                    representativeDTO.PassportCopy2Href = _appCustomSettings.UploadsCMSPath + "/Representative/Passport/" + participatingentity.PassportCopyTwo;


            if (participatingentity.Photo != null)
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PhotoPath + participatingentity.Photo))
                    representativeDTO.PhotoHref = _appCustomSettings.UploadsCMSPath + "/Representative/Photo/" + participatingentity.Photo;

            if (participatingentity.CountryUidno != null)
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + CountryUIDPath + participatingentity.CountryUidno))
                    representativeDTO.CountryUidnoHref = _appCustomSettings.UploadsCMSPath + "/Representative/CountryUID/" + participatingentity.CountryUidno;

            representativeDTO.IsRemove = MethodFactory.ShowRemoveRepresentative(participatingentity.Status);
            #endregion
            return representativeDTO;
        }
        private LoadRepresentativeDTO LoadEntityFromParticipatingNoVisa(LoadRepresentativeDTO representativeDTO, XsiExhibitionRepresentative parententityRepresentative, XsiExhibitionRepresentativeParticipatingNoVisa participatingnovisaentity)
        {
            #region No Visa Entity
            representativeDTO.RepresentativeId = participatingnovisaentity.RepresentativeId;
            representativeDTO.MemberExhibitionYearlyId = participatingnovisaentity.MemberExhibitionYearlyId;
            representativeDTO.MemberId = parententityRepresentative.MemberId.Value;

            if (participatingnovisaentity.NameEn != null)
                representativeDTO.NameEn = participatingnovisaentity.NameEn;
            if (participatingnovisaentity.NameAr != null)
                representativeDTO.NameAr = participatingnovisaentity.NameAr;
            if (participatingnovisaentity.Profession != null)
                representativeDTO.Profession = participatingnovisaentity.Profession;

            if (participatingnovisaentity.CountryId != null)
                representativeDTO.NationalityId = participatingnovisaentity.CountryId.Value;

            if (participatingnovisaentity.IsVisitedUae != null)
                representativeDTO.IsVisitedUae = participatingnovisaentity.IsVisitedUae;

            if (participatingnovisaentity.IsUaeresident != null)
                representativeDTO.IsUAEResident = participatingnovisaentity.IsUaeresident;

            if (parententityRepresentative != null)
            {
                if (parententityRepresentative.Dob != null)
                    representativeDTO.DateofBirth = parententityRepresentative.Dob.Value.ToString("MM/dd/yyyy");
                if (parententityRepresentative.PlaceOfBirth != null)
                    representativeDTO.PlaceOfBirth = parententityRepresentative.PlaceOfBirth;
            }

            if (participatingnovisaentity.Status != null && !string.IsNullOrEmpty(participatingnovisaentity.Status))
                representativeDTO.Status = participatingnovisaentity.Status;
            else
                representativeDTO.Status = EnumConversion.ToString(EnumRepresentativeStatus.Pending);

            if (participatingnovisaentity.IsAddHealthInsurance != null && !string.IsNullOrEmpty(participatingnovisaentity.IsAddHealthInsurance))
                representativeDTO.IsAddHealthInsurance = participatingnovisaentity.IsAddHealthInsurance;

            #region Phone,Mobile
            if (participatingnovisaentity.Telephone != null)
            {
                string[] str = participatingnovisaentity.Telephone.Split('$');
                if (str.Count() == 3)
                {
                    representativeDTO.PhoneISD = str[0].Trim();
                    representativeDTO.PhoneSTD = str[1].Trim();
                    representativeDTO.Phone = str[2].Trim();
                }
                else if (str.Count() == 2)
                {
                    representativeDTO.PhoneSTD = str[0].Trim();
                    representativeDTO.Phone = str[1].Trim();
                }
                else
                    representativeDTO.Phone = str[0].Trim();
            }
            if (participatingnovisaentity.Mobile != null)
            {
                string[] str = participatingnovisaentity.Mobile.Split('$');
                if (str.Count() == 3)
                {
                    representativeDTO.MobileISD = str[0].Trim();
                    representativeDTO.MobileSTD = str[1].Trim();
                    representativeDTO.Mobile = str[2].Trim();
                }
                else if (str.Count() == 2)
                {
                    representativeDTO.MobileSTD = str[0].Trim();
                    representativeDTO.Mobile = str[1].Trim();
                }
                else
                    representativeDTO.Mobile = str[0].Trim();
            }
            #endregion
            #region Visa
            if (participatingnovisaentity.NeedVisa != null)
            {
                //ShowVisaControls();
                if (participatingnovisaentity.NeedVisa == EnumConversion.ToString(EnumBool.Yes))
                {
                    representativeDTO.IsVisaNeed = true;
                }
                if (participatingnovisaentity.VisaApplicationForm != null)
                    if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + VisaFormPath + participatingnovisaentity.VisaApplicationForm))
                        representativeDTO.VisaHref = _appCustomSettings.UploadsCMSPath + "/Representative/VisaForm/" + participatingnovisaentity.VisaApplicationForm;
            }
            #endregion
            //rbtnYesRep.Checked = true;
            //rbtnNoRep.Checked = false;
            if (participatingnovisaentity.Passport != null)
            {
                // RepresentativeDTO.Passport = entity.Passport;
                representativeDTO.PassportNumber = participatingnovisaentity.Passport;
            }
            if (participatingnovisaentity.PlaceOfIssue != null)
                representativeDTO.PlaceOfIssue = participatingnovisaentity.PlaceOfIssue;
            if (participatingnovisaentity.DateOfIssue != null)
                representativeDTO.PassportDateOfIssue = participatingnovisaentity.DateOfIssue.Value.ToString("MM/dd/yyyy");

            if (participatingnovisaentity.ExpiryDate != null)
                representativeDTO.PassportExpiryDate = participatingnovisaentity.ExpiryDate.Value.ToString("MM/dd/yyyy");

            //  string downloadUrl = _appCustomSettings.UploadsCMSPath + "/Books/ExportBooks/" + excelName;
            if (participatingnovisaentity.PassportCopy != null)
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PassportPath + participatingnovisaentity.PassportCopy))
                    representativeDTO.PassportHRef = _appCustomSettings.UploadsCMSPath + "/Representative/Passport/" + participatingnovisaentity.PassportCopy;

            if (participatingnovisaentity.PassportCopyTwo != null)
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PassportPath + participatingnovisaentity.PassportCopyTwo))
                    representativeDTO.PassportCopy2Href = _appCustomSettings.UploadsCMSPath + "/Representative/Passport/" + participatingnovisaentity.PassportCopyTwo;


            if (participatingnovisaentity.Photo != null)
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PhotoPath + participatingnovisaentity.Photo))
                    representativeDTO.PhotoHref = _appCustomSettings.UploadsCMSPath + "/Representative/Photo/" + participatingnovisaentity.Photo;

            if (participatingnovisaentity.CountryUidno != null)
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + CountryUIDPath + participatingnovisaentity.CountryUidno))
                    representativeDTO.CountryUidnoHref = _appCustomSettings.UploadsCMSPath + "/Representative/CountryUID/" + participatingnovisaentity.CountryUidno;

            representativeDTO.IsRemove = MethodFactory.ShowRemoveRepresentative(participatingnovisaentity.Status);
            #endregion
            return representativeDTO;
        }
        #endregion

        #region Load Representative New Entity
        private XsiRepresentativeNew GetRepresentativeNewEntity(XsiRepresentativeNew dto, long memberId, long representativeid, long exhibitorId, long websiteid, long langid)
        {
            #region Participating Entity
            using (ExhibitionRepresentativeParticipatingService RepresentativeService = new ExhibitionRepresentativeParticipatingService())
            {
                ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService();
                var representative = ExhibitionRepresentativeService.GetRepresentativeByItemId(representativeid);

                var representativeParticipating = RepresentativeService.GetExhibitionRepresentativeParticipating(new XsiExhibitionRepresentativeParticipating() { RepresentativeId = representativeid, MemberExhibitionYearlyId = exhibitorId }).FirstOrDefault();

                if (representativeParticipating == null)
                {
                    dto.IsVisaNeed = false;
                    ExhibitionRepresentativeParticipatingNoVisaService ExhibitionRepresentativeParticipatingNoVisaService = new ExhibitionRepresentativeParticipatingNoVisaService();
                    var representativeParticipatingnovisa = ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa(new XsiExhibitionRepresentativeParticipatingNoVisa() { RepresentativeId = representativeid, MemberExhibitionYearlyId = exhibitorId }).FirstOrDefault();
                    if (representativeParticipatingnovisa != null)
                    {
                        dto = GetPariticipatingReresentativeNewNoVisa(dto, websiteid, langid, representative, representativeParticipatingnovisa);
                    }
                }
                else if (representativeParticipating != null)
                {
                    dto.IsVisaNeed = true;
                    dto = GetPariticipatingReresentativeNew(dto, websiteid, langid, representative, representativeParticipating);
                }
            }
            #endregion
            return dto;
        }
        private XsiRepresentativeNew GetPariticipatingReresentativeNew(XsiRepresentativeNew dto, long websiteid, long langid, XsiExhibitionRepresentative representative, XsiExhibitionRepresentativeParticipating representativeParticipating)
        {
            #region Participating
            dto.RepresentativeId = representativeParticipating.RepresentativeId;
            dto.MemberExhibitionYearlyId = representativeParticipating.MemberExhibitionYearlyId;
            dto.NameEn = representativeParticipating.NameEn;
            dto.NameAr = representativeParticipating.NameAr;
            dto.Profession = representativeParticipating.Profession;

            if (representativeParticipating.CountryId != null)
            {
                ExhibitionCountryService ExhibitionCountryService = new ExhibitionCountryService();
                XsiExhibitionCountry entityCountry = ExhibitionCountryService.GetExhibitionCountryByItemId(representativeParticipating.CountryId.Value);
                if (entityCountry != null)
                    dto.Nationality = langid == 1 ? entityCountry.CountryName : entityCountry.CountryNameAr;
            }

            if (representative != null)
            {
                //if (memberId != representative.MemberId)
                //    return Unauthorized();

                if (representative.Dob != null)
                    dto.DOB = representative.Dob.Value.ToString("MM/dd/yyyy");
                if (representative.PlaceOfBirth != null)
                    dto.POB = representative.PlaceOfBirth;
            }

            if (representativeParticipating.Telephone != null)
                if (representativeParticipating.Telephone.IndexOf('$') > -1)
                    dto.Telephone = representativeParticipating.Telephone.Replace('$', '-');
            if (representativeParticipating.Mobile != null)
                if (representativeParticipating.Mobile.IndexOf('$') > -1)
                    dto.Mobile = representativeParticipating.Mobile.Replace('$', '-');
            dto.PassportNumber = representativeParticipating.Passport;
            dto.POI = representativeParticipating.PlaceOfIssue;
            if (representativeParticipating.DateOfIssue != null)
                dto.DOI = representativeParticipating.DateOfIssue.Value.ToString("MM/dd/yyyy");
            if (representativeParticipating.ExpiryDate != null)
                dto.ExpiryDate = representativeParticipating.ExpiryDate.Value.ToString("MM/dd/yyyy");

            if (representativeParticipating.IsUaeresident != null)
                dto.IsUAEResident = representativeParticipating.IsUaeresident;

            dto.VisaType = representativeParticipating.VisaType;

            if (!string.IsNullOrEmpty(representativeParticipating.PassportCopy))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PassportPath + representativeParticipating.PassportCopy))
                    dto.PassportCopy = _appCustomSettings.UploadsCMSPath + "/Representative/Passport/" + representativeParticipating.PassportCopy;
            if (!string.IsNullOrEmpty(representativeParticipating.PassportCopyTwo))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PassportPath + representativeParticipating.PassportCopyTwo))
                    dto.PassportCopyTwo = _appCustomSettings.UploadsCMSPath + "/Representative/Passport/" + representativeParticipating.PassportCopyTwo;

            if (!string.IsNullOrEmpty(representativeParticipating.HealthInsurance))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + HealthInsurancePath + representativeParticipating.HealthInsurance))
                    dto.HealthInsurance = _appCustomSettings.UploadsCMSPath + "/Representative/HealthInsurance/" + representativeParticipating.HealthInsurance;
            if (!string.IsNullOrEmpty(representativeParticipating.HealthInsuranceTwo))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + HealthInsurancePathTwo + representativeParticipating.HealthInsuranceTwo))
                    dto.HealthInsuranceTwo = _appCustomSettings.UploadsCMSPath + "/Representative/HealthInsurance/" + representativeParticipating.HealthInsuranceTwo;
            if (!string.IsNullOrEmpty(representativeParticipating.Photo))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PhotoPath + representativeParticipating.Photo))
                    dto.PersonalPhoto = _appCustomSettings.UploadsCMSPath + "/Representative/Photo/" + representativeParticipating.Photo;
            if (!string.IsNullOrEmpty(representativeParticipating.VisaApplicationForm))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + VisaFormPath + representativeParticipating.VisaApplicationForm))
                    dto.VisaApplicationForm = _appCustomSettings.UploadsCMSPath + "/Representative/VisaForm/" + representativeParticipating.VisaApplicationForm;
            if (!string.IsNullOrEmpty(representativeParticipating.FlightTickets))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + FlightTicketsPath + representativeParticipating.FlightTickets))
                    dto.FlightTickets = _appCustomSettings.UploadsCMSPath + "/Representative/FlightTickets/" + representativeParticipating.FlightTickets;

            if (!string.IsNullOrEmpty(representativeParticipating.VisaCopy))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + VisaPath + representativeParticipating.VisaCopy))
                    dto.VisaCopy = _appCustomSettings.UploadsCMSPath + "/Representative/Visa/" + representativeParticipating.VisaCopy;

            //if (representativeParticipating.VisaCopy == EnumConversion.ToString(EnumBool.Yes))
            //{
            //    if (!string.IsNullOrEmpty(representativeParticipating.VisaCopy))
            //        if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + VisaPath + representativeParticipating.VisaCopy))
            //            dto.VisaCopy = _appCustomSettings.UploadsCMSPath + "/Representative/Visa/" + representativeParticipating.VisaCopy;
            //}
            
            if (representativeParticipating.IsAddHealthInsurance != null && !string.IsNullOrEmpty(representativeParticipating.IsAddHealthInsurance))
                dto.IsAddHealthInsurance = representativeParticipating.IsAddHealthInsurance;

            dto.Status = representativeParticipating.Status;
            if (representativeParticipating.ArrivalDate != null)
                dto.ArrivalDate = representativeParticipating.ArrivalDate.Value.ToString("MM/dd/yyyy");
            if (representativeParticipating.DepartureDate != null)
                dto.DepartureDate = representativeParticipating.DepartureDate.Value.ToString("MM/dd/yyyy");
            if (representativeParticipating.ArrivalAirportId != null)
                dto.ArrivalAirportId = representativeParticipating.ArrivalAirportId.ToString();
            if (representativeParticipating.IsTravelDetails != null)
                dto.IsTravelDetails = representativeParticipating.IsTravelDetails;
            else
                dto.IsTravelDetails = EnumConversion.ToString(EnumBool.No);

            var PenaltyDate = MethodFactory.GetExhibitionDateOfDue(websiteid);
            if (PenaltyDate != null)
            {
                dto.ExhibitorLastDate = PenaltyDate.Value.ToString("dddd d MMMM yyyy");
                if (PenaltyDate > MethodFactory.ArabianTimeNow())
                    dto.RemainingDays = Math.Ceiling(PenaltyDate.Value.Subtract(MethodFactory.ArabianTimeNow()).TotalDays).ToString();
                else
                    dto.RemainingDays = "0 ";
            }
            dto.IsRemove = MethodFactory.ShowRemoveRepresentative(representativeParticipating.Status);
            #endregion

            return dto;
        }
        private XsiRepresentativeNew GetPariticipatingReresentativeNewNoVisa(XsiRepresentativeNew dto, long websiteid, long langid, XsiExhibitionRepresentative representative, XsiExhibitionRepresentativeParticipatingNoVisa representativeParticipating)
        {
            #region Participating
            dto.RepresentativeId = representativeParticipating.RepresentativeId;
            dto.MemberExhibitionYearlyId = representativeParticipating.MemberExhibitionYearlyId;
            dto.NameEn = representativeParticipating.NameEn;
            dto.NameAr = representativeParticipating.NameAr;
            dto.Profession = representativeParticipating.Profession;

            if (representativeParticipating.CountryId != null)
            {
                ExhibitionCountryService ExhibitionCountryService = new ExhibitionCountryService();
                XsiExhibitionCountry entityCountry = ExhibitionCountryService.GetExhibitionCountryByItemId(representativeParticipating.CountryId.Value);
                if (entityCountry != null)
                    dto.Nationality = langid == 1 ? entityCountry.CountryName : entityCountry.CountryNameAr;
            }

            if (representative != null)
            {
                //if (memberId != representative.MemberId)
                //    return Unauthorized();

                if (representative.Dob != null)
                    dto.DOB = representative.Dob.Value.ToString("MM/dd/yyyy");
                if (representative.PlaceOfBirth != null)
                    dto.POB = representative.PlaceOfBirth;
            }

            if (representativeParticipating.Telephone != null)
                if (representativeParticipating.Telephone.IndexOf('$') > -1)
                    dto.Telephone = representativeParticipating.Telephone.Replace('$', '-');
            if (representativeParticipating.Mobile != null)
                if (representativeParticipating.Mobile.IndexOf('$') > -1)
                    dto.Mobile = representativeParticipating.Mobile.Replace('$', '-');
            dto.PassportNumber = representativeParticipating.Passport;
            dto.POI = representativeParticipating.PlaceOfIssue;
            if (representativeParticipating.DateOfIssue != null)
                dto.DOI = representativeParticipating.DateOfIssue.Value.ToString("MM/dd/yyyy");
            if (representativeParticipating.ExpiryDate != null)
                dto.ExpiryDate = representativeParticipating.ExpiryDate.Value.ToString("MM/dd/yyyy");

            if (representativeParticipating.IsUaeresident != null)
                dto.IsUAEResident = representativeParticipating.IsUaeresident;

            dto.VisaType = representativeParticipating.VisaType;

            if (!string.IsNullOrEmpty(representativeParticipating.PassportCopy))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PassportPath + representativeParticipating.PassportCopy))
                    dto.PassportCopy = _appCustomSettings.UploadsCMSPath + "/Representative/Passport/" + representativeParticipating.PassportCopy;
            if (!string.IsNullOrEmpty(representativeParticipating.PassportCopyTwo))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PassportPath + representativeParticipating.PassportCopyTwo))
                    dto.PassportCopyTwo = _appCustomSettings.UploadsCMSPath + "/Representative/Passport/" + representativeParticipating.PassportCopyTwo;

            if (!string.IsNullOrEmpty(representativeParticipating.HealthInsurance))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + HealthInsurancePath + representativeParticipating.HealthInsurance))
                    dto.HealthInsurance = _appCustomSettings.UploadsCMSPath + "/Representative/HealthInsurance/" + representativeParticipating.HealthInsurance;
            if (!string.IsNullOrEmpty(representativeParticipating.HealthInsuranceTwo))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + HealthInsurancePathTwo + representativeParticipating.HealthInsuranceTwo))
                    dto.HealthInsuranceTwo = _appCustomSettings.UploadsCMSPath + "/Representative/HealthInsurance/" + representativeParticipating.HealthInsuranceTwo;
            if (!string.IsNullOrEmpty(representativeParticipating.Photo))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PhotoPath + representativeParticipating.Photo))
                    dto.PersonalPhoto = _appCustomSettings.UploadsCMSPath + "/Representative/Photo/" + representativeParticipating.Photo;

            if (!string.IsNullOrEmpty(representativeParticipating.FlightTickets))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + FlightTicketsPath + representativeParticipating.FlightTickets))
                    dto.FlightTickets = _appCustomSettings.UploadsCMSPath + "/Representative/FlightTickets/" + representativeParticipating.FlightTickets;

            //if (!string.IsNullOrEmpty(representativeParticipating.VisaApplicationForm))
            //    if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + VisaFormPath + representativeParticipating.VisaApplicationForm))
            //        dto.VisaApplicationForm = _appCustomSettings.UploadsCMSPath + "/Representative/VisaForm/" + representativeParticipating.VisaApplicationForm;

            if (!string.IsNullOrEmpty(representativeParticipating.VisaCopy))
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + VisaPath + representativeParticipating.VisaCopy))
                    dto.VisaCopy = _appCustomSettings.UploadsCMSPath + "/Representative/Visa/" + representativeParticipating.VisaCopy;

            if (representativeParticipating.IsAddHealthInsurance != null && !string.IsNullOrEmpty(representativeParticipating.IsAddHealthInsurance))
                dto.IsAddHealthInsurance = representativeParticipating.IsAddHealthInsurance;

            dto.Status = representativeParticipating.Status;
            if (representativeParticipating.ArrivalDate != null)
                dto.ArrivalDate = representativeParticipating.ArrivalDate.Value.ToString("MM/dd/yyyy");
            if (representativeParticipating.DepartureDate != null)
                dto.DepartureDate = representativeParticipating.DepartureDate.Value.ToString("MM/dd/yyyy");
            if (representativeParticipating.ArrivalAirportId != null)
                dto.ArrivalAirportId = representativeParticipating.ArrivalAirportId.ToString();

            if (representativeParticipating.IsTravelDetails != null)
                dto.IsTravelDetails = representativeParticipating.IsTravelDetails;
            else
                dto.IsTravelDetails = EnumConversion.ToString(EnumBool.No);

            var PenaltyDate = MethodFactory.GetExhibitionDateOfDue(websiteid);
            if (PenaltyDate != null)
            {
                dto.ExhibitorLastDate = PenaltyDate.Value.ToString("dddd d MMMM yyyy");
                if (PenaltyDate > MethodFactory.ArabianTimeNow())
                    dto.RemainingDays = Math.Ceiling(PenaltyDate.Value.Subtract(MethodFactory.ArabianTimeNow()).TotalDays).ToString();
                else
                    dto.RemainingDays = "0 ";
            }
            dto.IsRemove = MethodFactory.ShowRemoveRepresentative(representativeParticipating.Status);
            #endregion
            return dto;
        }
        #endregion
        #endregion

        #region Remove or Delete
        bool RemoveRepresentative(RemoveCommandDTO removeCommandDTO, long langid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                using (ExhibitionRepresentativeParticipatingService representativeParticipatingService = new ExhibitionRepresentativeParticipatingService())
                {
                    ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService();

                    var existingParticipatingEntity = representativeParticipatingService.GetExhibitionRepresentativeParticipating(new XsiExhibitionRepresentativeParticipating() { RepresentativeId = removeCommandDTO.RepresentativeId, MemberExhibitionYearlyId = removeCommandDTO.MemberExhibitionYearlyId }).FirstOrDefault();
                    if (existingParticipatingEntity == null)
                    {
                        ExhibitionRepresentativeParticipatingNoVisaService ExhibitionRepresentativeParticipatingNoVisaService = new ExhibitionRepresentativeParticipatingNoVisaService();
                        XsiExhibitionRepresentativeParticipatingNoVisa existingParticipatingNoVisaEntity = ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa(new XsiExhibitionRepresentativeParticipatingNoVisa() { RepresentativeId = removeCommandDTO.RepresentativeId, MemberExhibitionYearlyId = removeCommandDTO.MemberExhibitionYearlyId }).FirstOrDefault();

                        if (existingParticipatingNoVisaEntity != null)
                        {
                            #region Remove
                            var strExistingStatus = existingParticipatingEntity.Status;
                            if (strExistingStatus == strCancelled || strExistingStatus == strCancelNoCharge || strExistingStatus == strCancelCharge || strExistingStatus == strCancelChargeandVisa)
                            {
                                return false;
                            }
                            else
                            {
                                XsiExhibitionRepresentativeParticipatingNoVisa participatingNoVisaEntity = new XsiExhibitionRepresentativeParticipatingNoVisa();
                                participatingNoVisaEntity = ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa(new XsiExhibitionRepresentativeParticipatingNoVisa() { RepresentativeId = removeCommandDTO.RepresentativeId, MemberExhibitionYearlyId = removeCommandDTO.MemberExhibitionYearlyId }).FirstOrDefault();
                                if (participatingNoVisaEntity != default(XsiExhibitionRepresentativeParticipatingNoVisa))
                                {
                                    if (strExistingStatus == EnumConversion.ToString(EnumRepresentativeStatus.VisaProcessing) || strExistingStatus == EnumConversion.ToString(EnumRepresentativeStatus.VisaUploaded))
                                        participatingNoVisaEntity.Status = EnumConversion.ToString(EnumRepresentativeStatus.CancelChargeandVisa); //380+ 150 +380
                                    else if (strExistingStatus == EnumConversion.ToString(EnumRepresentativeStatus.Approved))
                                        participatingNoVisaEntity.Status = EnumConversion.ToString(EnumRepresentativeStatus.CancelCharge); //150
                                    else
                                        participatingNoVisaEntity.Status = EnumConversion.ToString(EnumRepresentativeStatus.Cancelled);

                                    ExhibitionRepresentativeParticipatingNoVisaService.UpdateExhibitionRepresentativeParticipatingNoVisa(participatingNoVisaEntity);

                                    if (strExistingStatus == EnumConversion.ToString(EnumRepresentativeStatus.VisaProcessing) || strExistingStatus == EnumConversion.ToString(EnumRepresentativeStatus.VisaUploaded) || strExistingStatus == EnumConversion.ToString(EnumRepresentativeStatus.Approved))
                                    {
                                        SendEmailToSIBFNoVisa(removeCommandDTO, participatingNoVisaEntity);
                                    }
                                    return true;
                                }
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        #region Remove
                        var strExistingStatus = existingParticipatingEntity.Status;
                        if (strExistingStatus == strCancelled || strExistingStatus == strCancelNoCharge || strExistingStatus == strCancelCharge || strExistingStatus == strCancelChargeandVisa)
                        {
                            return false;
                        }
                        else
                        {
                            XsiExhibitionRepresentativeParticipating participatingEntity = new XsiExhibitionRepresentativeParticipating();
                            participatingEntity = representativeParticipatingService.GetExhibitionRepresentativeParticipating(new XsiExhibitionRepresentativeParticipating() { RepresentativeId = removeCommandDTO.RepresentativeId, MemberExhibitionYearlyId = removeCommandDTO.MemberExhibitionYearlyId }).FirstOrDefault();
                            if (participatingEntity != default(XsiExhibitionRepresentativeParticipating))
                            {
                                if (strExistingStatus == EnumConversion.ToString(EnumRepresentativeStatus.VisaProcessing) || strExistingStatus == EnumConversion.ToString(EnumRepresentativeStatus.VisaUploaded))
                                    participatingEntity.Status = EnumConversion.ToString(EnumRepresentativeStatus.CancelChargeandVisa); //380+ 150 +380
                                else if (strExistingStatus == EnumConversion.ToString(EnumRepresentativeStatus.Approved))
                                    participatingEntity.Status = EnumConversion.ToString(EnumRepresentativeStatus.CancelCharge); //150
                                else
                                    participatingEntity.Status = EnumConversion.ToString(EnumRepresentativeStatus.Cancelled);

                                representativeParticipatingService.UpdateExhibitionRepresentativeParticipating(participatingEntity);

                                if (strExistingStatus == EnumConversion.ToString(EnumRepresentativeStatus.VisaProcessing) || strExistingStatus == EnumConversion.ToString(EnumRepresentativeStatus.VisaUploaded) || strExistingStatus == EnumConversion.ToString(EnumRepresentativeStatus.Approved))
                                {
                                    SendEmailToSIBF(removeCommandDTO, participatingEntity);
                                }
                                return true;
                            }
                        }
                        #endregion
                    }
                }
            }
            return false;
        }
        #endregion

        #region Other Methods
        private string GetRepresentativeName(long representativeId, long exhibitorid, long langId)
        {
            using (ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService())
            {
                ExhibitionRepresentativeParticipatingService ExhibitionRepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService();
                var entity = ExhibitionRepresentativeService.GetRepresentativeByItemId(representativeId);
                if (entity != null)
                {
                    var representativeParticipating = ExhibitionRepresentativeParticipatingService.GetExhibitionRepresentativeParticipating(new XsiExhibitionRepresentativeParticipating() { MemberExhibitionYearlyId = exhibitorid, RepresentativeId = entity.RepresentativeId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                    if (representativeParticipating != null)
                        return langId == 1 ? representativeParticipating.NameEn : representativeParticipating.NameAr;
                }
                return string.Empty;
            }
        }
        private string GetRepresentativeNameFromParticipatingNoVisa(long representativeId, long exhibitorid, long langId)
        {
            using (ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService())
            {
                ExhibitionRepresentativeParticipatingNoVisaService ExhibitionRepresentativeParticipatingNoVisaService = new ExhibitionRepresentativeParticipatingNoVisaService();
                var entity = ExhibitionRepresentativeService.GetRepresentativeByItemId(representativeId);
                if (entity != null)
                {
                    var representativeParticipating = ExhibitionRepresentativeParticipatingNoVisaService.GetExhibitionRepresentativeParticipatingNoVisa(new XsiExhibitionRepresentativeParticipatingNoVisa() { MemberExhibitionYearlyId = exhibitorid, RepresentativeId = entity.RepresentativeId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                    if (representativeParticipating != null)
                        return langId == 1 ? representativeParticipating.NameEn : representativeParticipating.NameAr;
                }
                return string.Empty;
            }
        }
        string SaveImage(string fileBase64, string fileName, string name, string path, string fileExtension)
        {
            if (fileBase64 != null && fileBase64.Length > 0)
            {
                //string fileName = string.Empty;
                byte[] imageBytes;
                if (fileBase64.Contains("data:"))
                {
                    var strInfo = fileBase64.Split(",")[0];
                    imageBytes = Convert.FromBase64String(fileBase64.Split(',')[1]);
                }
                else
                {
                    imageBytes = Convert.FromBase64String(fileBase64);
                }

                fileName = string.Format("{0}_{1}_{2}{3}", name, 1, MethodFactory.GetRandomNumber(),
                    "." + fileExtension);
                System.IO.File.WriteAllBytes(
                    Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                 path) + fileName, imageBytes);

                return fileName;
            }
            return string.Empty;
        }
        protected string WriteTextOnImage(string fileBase64, string fileName, string name, long exhibitorId, string ext)
        {
            fileName = SaveImage(fileBase64, fileName, name, PassportPathTemp, ext);
            Bitmap b = new Bitmap((Bitmap)System.Drawing.Image.FromFile(_appCustomSettings.UploadsPhysicalPath + PassportPathTemp + fileName));
            Graphics g = Graphics.FromImage(b);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            string RepresentativeName = string.Empty;
            if (name != null)
                RepresentativeName = name.Trim();
            string FileNumber = string.Empty;
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                var entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                if (entity != default(XsiExhibitionMemberApplicationYearly))
                {
                    if (entity.FileNumber != null)
                        if (!string.IsNullOrEmpty(entity.FileNumber))
                            FileNumber = entity.FileNumber;
                }
            }
            drawingFont f = new drawingFont("calibri", 14, FontStyle.Bold);
            g.DrawString("File Number: " + FileNumber, f, SystemBrushes.WindowText, new Point(20, b.Height - 30));
            g.DrawString("Representative Name: " + RepresentativeName, f, SystemBrushes.WindowText, new Point(20, b.Height - 50));
            ext = "." + ext.ToLower();
            if (ext == ".png")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Png);
            else if (ext == ".jpg" || ext == ".jpeg")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Jpeg);
            else if (ext == ".gif")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Gif);
            else if (ext == ".bmp")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Bmp);
            else if (ext == ".tiff" || ext == ".tif")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Tiff);
            b.Dispose();
            return fileName;
        }
        string WriteTextOnPDF(string fileBase64, string fileName, string name, long exhibitorId, string ext)
        {
            PdfContentByte cb;
            fileName = SaveImage(fileBase64, fileName, name, PassportPathTemp, ext);
            //PPUpload.SaveAs(string.Format("{0}{1}", Server.MapPath(PassportPathTemp), fileName));
            PdfReader reader = new PdfReader(ReadImage(fileBase64, fileName, name, PassportPathTemp));
            using (var fileStream = new FileStream(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, FileMode.Create, FileAccess.Write))
            {
                var document = new Document(reader.GetPageSizeWithRotation(1));
                var writer = PdfWriter.GetInstance(document, fileStream);
                document.Open();
                for (var i = 1; i <= reader.NumberOfPages; i++)
                {
                    document.NewPage();
                    var baseFont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    var importedPage = writer.GetImportedPage(reader, i);
                    cb = writer.DirectContent;
                    cb.AddTemplate(importedPage, 0, 0);
                    string RepresentativeName = string.Empty;
                    if (name != null)
                        RepresentativeName = name;
                    string FileNumber = string.Empty;
                    using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
                    {
                        var entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                        if (entity != default(XsiExhibitionMemberApplicationYearly))
                        {
                            if (entity.FileNumber != null)
                                if (!string.IsNullOrEmpty(entity.FileNumber))
                                    FileNumber = entity.FileNumber;
                        }
                    }
                    string text = "Representative Name: " + RepresentativeName;
                    {
                        cb.BeginText();
                        cb.SetFontAndSize(baseFont, 12);
                        cb.SetTextMatrix(document.PageSize.GetLeft(26), document.PageSize.GetBottom(20));
                        cb.ShowText(text);
                        cb.EndText();
                    }
                    text = "File Number: " + FileNumber;
                    {
                        cb.BeginText();
                        cb.SetFontAndSize(baseFont, 12);
                        cb.SetTextMatrix(document.PageSize.GetLeft(26), document.PageSize.GetBottom(30));
                        cb.ShowText(text);
                        cb.EndText();
                    }
                }
                document.Close();
                writer.Close();
            }
            return fileName;
        }
        byte[] ReadImage(string fileBase64, string fileName, string name, string path)
        {
            byte[] responseBytes = null;
            if (fileBase64 != null && fileBase64.Length > 0)
            {
                //string fileName = string.Empty;
                //fileName = string.Format("{0}_{1}_{2}{3}", name, 1, MethodFactory.GetRandomNumber(),
                //    "." + fileExtension);
                responseBytes = System.IO.File.ReadAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                 path) + fileName);
            }
            return responseBytes;
        }
        string GetArrivalAirport(long itemId)
        {
            var ExhibitionArrivalAirportService = new ExhibitionArrivalAirportService();
            var entity = ExhibitionArrivalAirportService.GetExhibitionArrivalAirportByItemId(itemId);
            if (entity != null)
                if (entity.Title != null)
                    return entity.Title;
            return string.Empty;
        }
        string GenerateAndDownloadAgreement(UploadAgreementDTO uploadAgreementDTO)
        {
            //var List = Session["RepresentativePDFList"] as List<XsiRepresentative>;
            //var repPdfList = uploadAgreementDTO.RepresentativePDFList;
            HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
            StringBuilder representativeinfohtml;
            StringBuilder agreementhtml = new StringBuilder();
            //Response.Clear();
            //string photopath = HttpContext.Current.Server.MapPath("/Content/Uploads/Representative/Photo/");
            string photopath = _appCustomSettings.UploadsPhysicalPath + PhotoPath;
            #region Replace Placeholders
            string strinfohtml = string.Empty;
            int index = 0;
            long memberid = -1;
            long exhibitorid = -1;
            long langID = -1;
            var ExhibitorEntity = new XsiExhibitionMemberApplicationYearly();
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                ExhibitorEntity = MethodFactory.GetExhibitor(uploadAgreementDTO.MemberId, uploadAgreementDTO.WebsiteId, _context);
            }
            memberid = uploadAgreementDTO.MemberId;
            if (ExhibitorEntity != null)
                exhibitorid = ExhibitorEntity.MemberExhibitionYearlyId;
            langID = MethodFactory.GetMemberLanguageId(uploadAgreementDTO.MemberId);

            representativeinfohtml = new StringBuilder();
            StringBuilder strrepresentativeinfohtml = new StringBuilder();
            if (langID == 1)
            {

                if (uploadAgreementDTO.WebsiteId == 2)
                    strrepresentativeinfohtml.Append(System.IO.File.ReadAllText(_appCustomSettings.UploadsPhysicalPath + "/Content/EmailMedia/representativeSCRF.html"));
                else
                    strrepresentativeinfohtml.Append(System.IO.File.ReadAllText(_appCustomSettings.UploadsPhysicalPath + "/Content/EmailMedia/representative.html"));
            }
            else
            {
                if (uploadAgreementDTO.WebsiteId == 2)
                    strrepresentativeinfohtml.Append(System.IO.File.ReadAllText(_appCustomSettings.UploadsPhysicalPath + "/Content/EmailMedia/representativeSCRFArabic.html"));
                else
                    strrepresentativeinfohtml.Append(System.IO.File.ReadAllText(_appCustomSettings.UploadsPhysicalPath + "/Content/EmailMedia/representativeArabic.html"));
            }
            var List1 = uploadAgreementDTO.RepresentativePDFList.Where(i => i.NeedVisa == "Y").ToList();
            foreach (var rows in List1)
            {
                representativeinfohtml = new StringBuilder();
                representativeinfohtml.Append(strrepresentativeinfohtml.ToString());
                if (langID == 2)
                {
                    if (rows.NameAr != null && !string.IsNullOrEmpty(rows.NameAr))
                        representativeinfohtml.Replace("$$name$$", rows.NameAr);
                    else
                        representativeinfohtml.Replace("$$name$$", rows.NameEn);
                }
                else
                    representativeinfohtml.Replace("$$name$$", rows.NameEn);


                if (rows.Passport != null && !string.IsNullOrEmpty(rows.Passport))
                    representativeinfohtml.Replace("$$passport$$", rows.Passport);
                else
                    representativeinfohtml.Replace("$$passport$$", "");
                representativeinfohtml.Replace("$$nationality$$", MethodFactory.GetCountryName(rows.CountryId.Value, 1));

                if (rows.Status != null && !string.IsNullOrEmpty(rows.Status))
                {
                    if (langID == 1)
                        representativeinfohtml.Replace("$$status$$", MethodFactory.GetReprepresentativeStatus(rows.Status, 1));
                    else
                        representativeinfohtml.Replace("$$status$$", MethodFactory.GetReprepresentativeStatus(rows.Status, 2));
                }
                else
                    representativeinfohtml.Replace("$$status$$", "");


                if (rows.CreatedOn != null)
                {
                    if (langID == 1)
                        representativeinfohtml.Replace("$$dateadded$$", rows.CreatedOn.Value.ToString("dd MMM yyy"));
                    else
                        representativeinfohtml.Replace("$$dateadded$$", rows.CreatedOn.Value.ToString("dd MMM yyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")));
                }
                else
                    representativeinfohtml.Replace("$$dateadded$$", "");

                if (rows.Photo != null && !string.IsNullOrEmpty(rows.Photo))
                    representativeinfohtml.Replace("$$photo$$", photopath + rows.Photo);
                else
                    representativeinfohtml.Replace("$$photo$$", "");

                strinfohtml += representativeinfohtml.ToString();
                index++;
            }
            #endregion
            if (langID == 1)
            {
                if (uploadAgreementDTO.WebsiteId == 2)
                    agreementhtml.Append(System.IO.File.ReadAllText(_appCustomSettings.UploadsPhysicalPath + "/Content/EmailMedia/RepresentativeAgreementSCRF.html"));
                else
                    agreementhtml.Append(System.IO.File.ReadAllText(_appCustomSettings.UploadsPhysicalPath + "/Content/EmailMedia/RepresentativeAgreement.html"));
            }
            else
            {
                if (uploadAgreementDTO.WebsiteId == 2)
                    agreementhtml.Append(System.IO.File.ReadAllText(_appCustomSettings.UploadsPhysicalPath + "/Content/EmailMedia/RepresentativeAgreementSCRFArabic.html"));
                else
                    agreementhtml.Append(System.IO.File.ReadAllText(_appCustomSettings.UploadsPhysicalPath + "/Content/EmailMedia/RepresentativeAgreementArabic.html"));
            }
            string strhtml = string.Empty;
            agreementhtml.Replace("$$representativehtml$$", strinfohtml);
            agreementhtml.Replace("$$ServerAddressSCRF$$", _appCustomSettings.ServerAddressSCRF);
            agreementhtml.Replace("$$ServerAddress$$", _appCustomSettings.ServerAddressNew);

            string str = htmlContentFactory.GetExhibitionDetails(uploadAgreementDTO.WebsiteId, langID);
            string[] strArray = str.Split(',');
            agreementhtml.Replace("$$exhibitionno$$", strArray[0].ToString());
            agreementhtml.Replace("$$exhibitiondate$$", strArray[1].ToString());
            agreementhtml.Replace("$$exhyear$$", strArray[2]);

            agreementhtml.Replace("$$totalparticipating$$", uploadAgreementDTO.RepresentativePDFList.Where(p => p.Status != "D" && p.Status != "R").Count().ToString());
            agreementhtml.Replace("$$totalcancelled$$", uploadAgreementDTO.RepresentativePDFList.Where(p => p.Status == "D").Count().ToString());

            XsiEmailContent PDFAgreement = GetPDFAgreementcontent(76, 1);
            if (PDFAgreement != default(XsiEmailContent))
            {
                agreementhtml.Replace("$$agreementtitle$$", PDFAgreement.Subject);
                agreementhtml.Replace("$$agreement$$", PDFAgreement.Body);
            }
            strhtml = agreementhtml.ToString();
            if (!string.IsNullOrEmpty(strhtml))
            {
                string FileName = string.Format("{0}_{1}", 1, MethodFactory.GetRandomNumber());
                Process PDFProcess = GetPDFForRepresentativeAgreement(strhtml, FileName);
                if (memberid != -1)
                {

                    string pdfPath = string.Empty;
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {
                        #region Add Generated Representative Agreement
                        RepresentativeAgreementService RepresentativeAgreementService = new RepresentativeAgreementService();
                        #region Delete Previous Agreement
                        List<XsiRepresentativeAgreement> PrevAgreementList = RepresentativeAgreementService.GetRepresentativeAgreement(new XsiRepresentativeAgreement() { ExhibitorId = exhibitorid });
                        foreach (var rows in PrevAgreementList)
                        {
                            if (rows.Agreement != null && !string.IsNullOrEmpty(rows.Agreement))
                            {
                                pdfPath = _appCustomSettings.UploadsPhysicalPath + "~/Content/uploads/Representative/Agreement/" + rows.Agreement;
                                string htmlfilename = System.IO.Path.GetFileNameWithoutExtension(pdfPath) + ".html";
                                if (System.IO.File.Exists(pdfPath))
                                    System.IO.File.Delete(pdfPath);
                                pdfPath = _appCustomSettings.UploadsPhysicalPath + "~/Content/uploads/Representative/Agreement/htmlfilename";
                                if (System.IO.File.Exists(pdfPath))
                                    System.IO.File.Delete(pdfPath);
                            }
                            if (rows.SignedAgreement != null && !string.IsNullOrEmpty(rows.SignedAgreement))
                            {
                                pdfPath = _appCustomSettings.UploadsPhysicalPath + "~/Content/uploads/Representative/SignedAgreement/" + rows.SignedAgreement;
                                if (System.IO.File.Exists(pdfPath))
                                    System.IO.File.Delete(pdfPath);
                            }
                            RepresentativeAgreementService.DeleteRepresentativeAgreement(new XsiRepresentativeAgreement() { ItemId = rows.ItemId });
                        }
                        //ahrefSignedAgreement.Visible = false;
                        #endregion  
                        XsiRepresentativeAgreement entity = new XsiRepresentativeAgreement();
                        entity.ExhibitorId = exhibitorid;
                        entity.Agreement = FileName + ".pdf";
                        entity.CreatedOn = MethodFactory.ArabianTimeNow();
                        entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                        RepresentativeAgreementService.InsertRepresentativeAgreement(entity);
                        #endregion
                    }
                    while (true)
                    {
                        if (PDFProcess.HasExited)
                        {
                            // pdfPath = _appCustomSettings.UploadsPhysicalPath + "/Representative/Agreement/" + FileName + ".pdf";
                            FileName = Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                                "\\Representative\\Agreement\\") + FileName + ".pdf";
                            //if (System.IO.File.Exists(pdfPath))
                            //{
                            //    Response.ContentType = "Application/pdf";
                            //    Response.AppendHeader("Content-Disposition", "attachment; filename=RepresentativeAgreement.pdf");
                            //    Response.TransmitFile(pdfPath);
                            //    Response.End();
                            //}
                            break;
                        }
                    }
                    return FileName;
                }
                return string.Empty;
            }
            return string.Empty;
        }
        Process GetPDFForRepresentativeAgreement(string html, string strFileName)
        {
            string pdfPath = _appCustomSettings.UploadsPhysicalPath + "~/Content/uploads/Representative/Agreement/";
            html = @"<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'><head><title>Application Form</title></head>
                <meta http-equiv='content-Type' content='text/html; charset=utf-8' /><body>" + html + "</body></html>";

            string pdfPath1 = _appCustomSettings.UploadsPhysicalPath + "~/Content/uploads/Representative/Agreement/html/" + strFileName + ".html";
            System.IO.File.WriteAllText(pdfPath1, html);
            Process ProcessStarted;
            while (true)
            {
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "~/Content/uploads/Representative/Agreement/html/" + strFileName + ".html"))
                {
                    ProcessStarted = Process.Start(new ProcessStartInfo
                    {
                        FileName = _appCustomSettings.HtmltopdfPath,
                        Arguments = String.Format("{0} {1}", pdfPath1, pdfPath + "/" + strFileName + ".pdf"),
                        UseShellExecute = false
                    });
                    break;
                }
            }
            return ProcessStarted;
        }
        XsiEmailContent GetPDFAgreementcontent(long emailcontentGid, long langId)
        {
            using (EmailContentService EmailContentService = new EmailContentService())
            {
                return EmailContentService.GetEmailContentByItemId(emailcontentGid);
            }
        }
        #endregion

        #region Email Methods
        private void SendEmailToSIBF(RemoveCommandDTO removeCommandDTO, XsiExhibitionRepresentativeParticipating entity)
        {
            #region Send Email to SIBF
            StringBuilder body1 = new StringBuilder();
            #region Email Content
            EmailContentService EmailContentService = new EmailContentService();
            SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
            if (removeCommandDTO.WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
            {
                ScrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(10011);
                if (ScrfEmailContent != null)
                {
                    if (ScrfEmailContent.Body != null)
                        StrEmailContentBody = ScrfEmailContent.Body;
                    if (ScrfEmailContent.Email != null)
                        StrEmailContentEmail = ScrfEmailContent.Email;
                    if (ScrfEmailContent.Subject != null)
                        StrEmailContentSubject = ScrfEmailContent.Subject;
                    StrEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                }
            }
            else
            {
                EmailContent = EmailContentService.GetEmailContentByItemId(10011);
                if (EmailContent != null)
                {
                    if (EmailContent.Body != null)
                        StrEmailContentBody = EmailContent.Body;
                    if (EmailContent.Email != null)
                        StrEmailContentEmail = EmailContent.Email;
                    if (EmailContent.Subject != null)
                        StrEmailContentSubject = EmailContent.Subject;
                    StrEmailContentAdmin = _appCustomSettings.AdminName;
                }
            }
            #endregion
            HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
            body1.Append(htmlContentFactory.BindEmailContent(1, StrEmailContentBody, null, _appCustomSettings.ServerAddressNew, removeCommandDTO.WebsiteId, _appCustomSettings));
            body1.Replace("$$RepresentativeName$$", entity.NameEn);
            if (StrEmailContentEmail != string.Empty)
                _emailSender.SendEmail(StrEmailContentAdmin, _appCustomSettings.AdminEmail, StrEmailContentEmail, StrEmailContentSubject, body1.ToString());
            #endregion
        }
        private void NotifyAdminAboutDocumentUploaded(XsiExhibitionRepresentative entityRep1, XsiExhibitionRepresentativeParticipating entityRep2, SaveRepresentativeDTO representativeDTO)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                XsiExhibitionMember memberentity = new XsiExhibitionMember();

                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                XsiExhibitionMemberApplicationYearly ExhibitorEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(entityRep2.MemberExhibitionYearlyId);
                memberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(ExhibitorEntity.MemberId.Value);
                if (memberentity != default(XsiExhibitionMember) && ExhibitorEntity != default(XsiExhibitionMemberApplicationYearly))
                {
                    string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {
                        #region Send Admin Email Notifying about Email Change
                        StringBuilder body = new StringBuilder();
                        #region Email Content
                        SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                        if (representativeDTO.WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                        {
                            var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(20103);
                            if (scrfEmailContent != null)
                            {
                                if (scrfEmailContent.Body != null)
                                    strEmailContentBody = scrfEmailContent.Body;
                                if (scrfEmailContent.Email != null && !string.IsNullOrEmpty(scrfEmailContent.Email))
                                    strEmailContentEmail = scrfEmailContent.Email;
                                else
                                    strEmailContentEmail = _appCustomSettings.AdminEmail;
                                if (scrfEmailContent.Subject != null)
                                    strEmailContentSubject = scrfEmailContent.Subject;
                                strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                            }
                        }
                        else
                        {
                            var emailContent = EmailContentService.GetEmailContentByItemId(20118);
                            if (emailContent != null)
                            {
                                if (emailContent.Body != null)
                                    strEmailContentBody = emailContent.Body;
                                if (emailContent.Email != null && !string.IsNullOrEmpty(emailContent.Email))
                                    strEmailContentEmail = emailContent.Email;
                                else
                                    strEmailContentEmail = _appCustomSettings.AdminEmail;
                                if (emailContent.Subject != null)
                                    strEmailContentSubject = emailContent.Subject;
                                strEmailContentAdmin = _appCustomSettings.AdminName;
                            }
                        }
                        #endregion
                        strEmailContentBody = strEmailContentBody.Replace("$$RepresentativeName$$", representativeDTO.NameEn);
                        if (ExhibitorEntity.PublisherNameEn != null)
                            strEmailContentBody = strEmailContentBody.Replace("$$ExhibitorName$$", ExhibitorEntity.PublisherNameEn);
                        if (ExhibitorEntity.FileNumber != null)
                            strEmailContentBody = strEmailContentBody.Replace("$$FileNumber$$", ExhibitorEntity.FileNumber);

                        HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                        body.Append(htmlContentFactory.BindEmailContent(memberentity.LanguageUrl.Value, strEmailContentBody, memberentity, _appCustomSettings.ServerAddressNew, representativeDTO.WebsiteId, _appCustomSettings));
                        _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString());
                        #endregion
                    }
                }
            }
        }
        void SendEmail(XsiExhibitionRepresentativeParticipating entity, long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                using (EmailContentService EmailContentService = new EmailContentService())
                {
                    #region Send Email To Visa Admin
                    StringBuilder body = new StringBuilder();
                    #region Email Content
                    SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        ScrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(10017);
                        if (ScrfEmailContent != null)
                        {
                            if (ScrfEmailContent.Body != null)
                                StrEmailContentBody = ScrfEmailContent.Body;
                            if (ScrfEmailContent.Email != null)
                                StrEmailContentEmail = ScrfEmailContent.Email;
                            if (ScrfEmailContent.Subject != null)
                                StrEmailContentSubject = ScrfEmailContent.Subject;
                            StrEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                        }
                    }
                    else
                    {
                        EmailContent = EmailContentService.GetEmailContentByItemId(10017);
                        if (EmailContent != null)
                        {
                            if (EmailContent.Body != null)
                                StrEmailContentBody = EmailContent.Body;
                            if (EmailContent.Email != null)
                                StrEmailContentEmail = EmailContent.Email;
                            if (EmailContent.Subject != null)
                                StrEmailContentSubject = EmailContent.Subject;
                            StrEmailContentAdmin = _appCustomSettings.AdminName;
                        }
                    }
                    #endregion

                    body.Append(htmlContentFactory.BindEmailContent(1, StrEmailContentBody, null, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                    if (entity != null)
                    {
                        if (entity.NameEn != null)
                            body.Replace("$$RepresentativeName$$", entity.NameEn);
                        if (entity.ArrivalDate != null)
                            body.Replace("$$ArrivalDate$$", entity.ArrivalDate.Value.ToString("dd MMM yyyy"));
                        if (entity.ArrivalAirportId != null)
                            body.Replace("$$ArrivalAirport$$", GetArrivalAirport(entity.ArrivalAirportId.Value));
                    }
                    if (entity.MemberExhibitionYearlyId > 0)
                    {
                        ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                        var ExhibitorEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(entity.MemberExhibitionYearlyId);
                        if (ExhibitorEntity != null)
                        {
                            if (ExhibitorEntity.PublisherNameEn != null)
                                body.Replace("$$ExhibitorNameEn$$", ExhibitorEntity.PublisherNameEn);
                            if (ExhibitorEntity.FileNumber != null)
                                body.Replace("$$FileNumber$$", ExhibitorEntity.FileNumber);
                        }
                    }
                    if (StrEmailContentEmail != string.Empty)
                        _emailSender.SendEmail(StrEmailContentAdmin, _appCustomSettings.AdminEmail, StrEmailContentEmail, StrEmailContentSubject, body.ToString());
                    //SmtpEmail.SendNetEmail(AdminName, AdminEmailAddress, VisaAdminEmail, emailContent.Subject, body.ToString());
                    #endregion
                }
            }
        }
        #endregion

        #region Email Methods No Visa
        private void SendEmailToSIBFNoVisa(RemoveCommandDTO removeCommandDTO, XsiExhibitionRepresentativeParticipatingNoVisa entity)
        {
            #region Send Email to SIBF
            StringBuilder body1 = new StringBuilder();
            #region Email Content
            EmailContentService EmailContentService = new EmailContentService();
            SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
            if (removeCommandDTO.WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
            {
                ScrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(10011);
                if (ScrfEmailContent != null)
                {
                    if (ScrfEmailContent.Body != null)
                        StrEmailContentBody = ScrfEmailContent.Body;
                    if (ScrfEmailContent.Email != null)
                        StrEmailContentEmail = ScrfEmailContent.Email;
                    if (ScrfEmailContent.Subject != null)
                        StrEmailContentSubject = ScrfEmailContent.Subject;
                    StrEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                }
            }
            else
            {
                EmailContent = EmailContentService.GetEmailContentByItemId(10011);
                if (EmailContent != null)
                {
                    if (EmailContent.Body != null)
                        StrEmailContentBody = EmailContent.Body;
                    if (EmailContent.Email != null)
                        StrEmailContentEmail = EmailContent.Email;
                    if (EmailContent.Subject != null)
                        StrEmailContentSubject = EmailContent.Subject;
                    StrEmailContentAdmin = _appCustomSettings.AdminName;
                }
            }
            #endregion
            HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
            body1.Append(htmlContentFactory.BindEmailContent(1, StrEmailContentBody, null, _appCustomSettings.ServerAddressNew, removeCommandDTO.WebsiteId, _appCustomSettings));
            body1.Replace("$$RepresentativeName$$", entity.NameEn);
            if (StrEmailContentEmail != string.Empty)
                _emailSender.SendEmail(StrEmailContentAdmin, _appCustomSettings.AdminEmail, StrEmailContentEmail, StrEmailContentSubject, body1.ToString());
            #endregion
        }
        private void NotifyAdminAboutDocumentUploadedNoVisa(XsiExhibitionRepresentative entityRep1, XsiExhibitionRepresentativeParticipatingNoVisa entityRep2, SaveRepresentativeDTO representativeDTO)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                XsiExhibitionMember memberentity = new XsiExhibitionMember();

                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                XsiExhibitionMemberApplicationYearly ExhibitorEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(entityRep2.MemberExhibitionYearlyId);
                memberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(ExhibitorEntity.MemberId.Value);
                if (memberentity != default(XsiExhibitionMember) && ExhibitorEntity != default(XsiExhibitionMemberApplicationYearly))
                {
                    string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {
                        #region Send Admin Email Notifying about Email Change
                        StringBuilder body = new StringBuilder();
                        #region Email Content
                        SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                        if (representativeDTO.WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                        {
                            var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(20103);
                            if (scrfEmailContent != null)
                            {
                                if (scrfEmailContent.Body != null)
                                    strEmailContentBody = scrfEmailContent.Body;
                                if (scrfEmailContent.Email != null && !string.IsNullOrEmpty(scrfEmailContent.Email))
                                    strEmailContentEmail = scrfEmailContent.Email;
                                else
                                    strEmailContentEmail = _appCustomSettings.AdminEmail;
                                if (scrfEmailContent.Subject != null)
                                    strEmailContentSubject = scrfEmailContent.Subject;
                                strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                            }
                        }
                        else
                        {
                            var emailContent = EmailContentService.GetEmailContentByItemId(20118);
                            if (emailContent != null)
                            {
                                if (emailContent.Body != null)
                                    strEmailContentBody = emailContent.Body;
                                if (emailContent.Email != null && !string.IsNullOrEmpty(emailContent.Email))
                                    strEmailContentEmail = emailContent.Email;
                                else
                                    strEmailContentEmail = _appCustomSettings.AdminEmail;
                                if (emailContent.Subject != null)
                                    strEmailContentSubject = emailContent.Subject;
                                strEmailContentAdmin = _appCustomSettings.AdminName;
                            }
                        }
                        #endregion
                        strEmailContentBody = strEmailContentBody.Replace("$$RepresentativeName$$", representativeDTO.NameEn);
                        if (ExhibitorEntity.PublisherNameEn != null)
                            strEmailContentBody = strEmailContentBody.Replace("$$ExhibitorName$$", ExhibitorEntity.PublisherNameEn);
                        if (ExhibitorEntity.FileNumber != null)
                            strEmailContentBody = strEmailContentBody.Replace("$$FileNumber$$", ExhibitorEntity.FileNumber);

                        HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                        body.Append(htmlContentFactory.BindEmailContent(memberentity.LanguageUrl.Value, strEmailContentBody, memberentity, _appCustomSettings.ServerAddressNew, representativeDTO.WebsiteId, _appCustomSettings));
                        _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString());
                        #endregion
                    }
                }
            }
        }
        void SendEmailParticipatingNovisa(XsiExhibitionRepresentativeParticipatingNoVisa entity, long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                using (EmailContentService EmailContentService = new EmailContentService())
                {
                    #region Send Email To Visa Admin
                    StringBuilder body = new StringBuilder();
                    #region Email Content
                    SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        ScrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(10017);
                        if (ScrfEmailContent != null)
                        {
                            if (ScrfEmailContent.Body != null)
                                StrEmailContentBody = ScrfEmailContent.Body;
                            if (ScrfEmailContent.Email != null)
                                StrEmailContentEmail = ScrfEmailContent.Email;
                            if (ScrfEmailContent.Subject != null)
                                StrEmailContentSubject = ScrfEmailContent.Subject;
                            StrEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                        }
                    }
                    else
                    {
                        EmailContent = EmailContentService.GetEmailContentByItemId(10017);
                        if (EmailContent != null)
                        {
                            if (EmailContent.Body != null)
                                StrEmailContentBody = EmailContent.Body;
                            if (EmailContent.Email != null)
                                StrEmailContentEmail = EmailContent.Email;
                            if (EmailContent.Subject != null)
                                StrEmailContentSubject = EmailContent.Subject;
                            StrEmailContentAdmin = _appCustomSettings.AdminName;
                        }
                    }
                    #endregion

                    body.Append(htmlContentFactory.BindEmailContent(1, StrEmailContentBody, null, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                    if (entity != null)
                    {
                        if (entity.NameEn != null)
                            body.Replace("$$RepresentativeName$$", entity.NameEn);
                        if (entity.ArrivalDate != null)
                            body.Replace("$$ArrivalDate$$", entity.ArrivalDate.Value.ToString("dd MMM yyyy"));
                        if (entity.ArrivalAirportId != null)
                            body.Replace("$$ArrivalAirport$$", GetArrivalAirport(entity.ArrivalAirportId.Value));
                    }
                    if (entity.MemberExhibitionYearlyId > 0)
                    {
                        ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                        var ExhibitorEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(entity.MemberExhibitionYearlyId);
                        if (ExhibitorEntity != null)
                        {
                            if (ExhibitorEntity.PublisherNameEn != null)
                                body.Replace("$$ExhibitorNameEn$$", ExhibitorEntity.PublisherNameEn);
                            if (ExhibitorEntity.FileNumber != null)
                                body.Replace("$$FileNumber$$", ExhibitorEntity.FileNumber);
                        }
                    }
                    if (StrEmailContentEmail != string.Empty)
                        _emailSender.SendEmail(StrEmailContentAdmin, _appCustomSettings.AdminEmail, StrEmailContentEmail, StrEmailContentSubject, body.ToString());
                    //SmtpEmail.SendNetEmail(AdminName, AdminEmailAddress, VisaAdminEmail, emailContent.Subject, body.ToString());
                    #endregion
                }
            }
        }
        #endregion
    }
}

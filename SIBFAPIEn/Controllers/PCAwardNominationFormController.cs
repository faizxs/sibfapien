﻿using Contracts;
using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xsi.ServicesLayer;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PCAwardNominationFormController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private ILoggerManager _logger;
        EnumResultType XsiResult { get; set; }

        private readonly IEmailSender _emailSender;
        public PCAwardNominationFormController(IEmailSender emailSender, IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger)
        {
            _emailSender = emailSender;
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
        }

        // POST: api/PCAwardNominationFormBook
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> PostPCAwardNominationFormBook(PCAwardNominationDTO data)
        {
            try
            {
                bool isvalid = true;
                string strmessage = string.Empty;
                if (!string.IsNullOrEmpty(data.ParticipantName))
                {
                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        var awardentity = _context.XsiPcawards.Where(i => i.ItemId == data.AwardId && i.IsActive == "Y").FirstOrDefault();
                        if (awardentity != null)
                        {
                            long LangId = 1;
                            long.TryParse(Request.Headers["LanguageURL"], out LangId);

                            long.TryParse(Request.Headers["LanguageURL"], out LangId);
                            long nominationid = -1;
                            string pdf = string.Empty;
                            string fileName = string.Empty;
                            XsiPcawardNominationForms entity = new XsiPcawardNominationForms();
                            if (data.hdnTempData != null && data.hdnTempData != "0" && data.hdnTempData != "" &&
                                data.hdnTempData != "-1")
                            {
                                entity = _context.XsiPcawardNominationForms.Where(i => i.ItemId == Convert.ToInt64(data.hdnTempData))
                                    .FirstOrDefault();
                                if (entity != default(XsiPcawardNominationForms))
                                {
                                    entity.ItemId = Convert.ToInt64(data.hdnTempData);
                                }
                            }

                            var awarddetail = _context.XsiPcawardDetails.Where(i => i.AwardId == data.AwardId && i.IsActive == "Y").OrderByDescending(i => i.ItemId).FirstOrDefault();
                            if (awarddetail != null)
                            {
                                entity.AwardDetailId = awarddetail.ItemId;
                            }

                            //entity.ExhibitionId = MethodFactory.GetActiveExhibition(Convert.ToInt64(EnumWebsiteId.SIBF));

                            entity.ParticipantName = data.ParticipantName;
                            entity.CompanyName = data.CompanyName;
                            if (data.CountryId != null && data.CountryId != -1 && data.CountryId != 0)
                                entity.CountryId = data.CountryId;

                            entity.Email = data.Email;
                            entity.Website = data.Website;
                            entity.ReasonToWinAward = data.ReasonToWinAward;

                            entity.NameOne = data.NameOne;
                            entity.CompanyNameOne = data.CompanyNameOne;
                            entity.EmailOne = data.EmailOne;
                            entity.Phone = data.ISD + "$" + data.STD + "$" + data.Phone;
                            entity.Testimonial = data.Testimonial;

                            if (data.FileBase64 != null && data.FileBase64.Length > 0)
                            {
                                fileName = string.Empty;
                                byte[] imageBytes;
                                if (data.FileBase64.Contains("data:"))
                                {
                                    var strInfo = data.FileBase64.Split(",")[0];
                                    imageBytes = Convert.FromBase64String(data.FileBase64.Split(',')[1]);
                                }
                                else
                                    imageBytes = Convert.FromBase64String(data.FileBase64);

                                fileName = string.Format("{0}_{1}_{2}{3}", data.ParticipantName, LangId, MethodFactory.GetRandomNumber(), "." + data.DocExtension);
                                System.IO.File.WriteAllBytes(
                                    Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                                 "\\PCAwards\\TestimonialOne\\") + fileName, imageBytes);

                                entity.FileName = fileName;
                            }

                            entity.NameTwo = data.NameTwo;
                            entity.CompanyNameTwo = data.CompanyNameTwo;
                            entity.EmailTwo = data.EmailTwo;
                            entity.TestimonialTwo = data.TestimonialTwo;

                            if (data.FileTwoBase64 != null && data.FileTwoBase64.Length > 0)
                            {
                                fileName = string.Empty;
                                byte[] imageBytes;
                                if (data.FileTwoBase64.Contains("data:"))
                                {
                                    var strInfo = data.FileTwoBase64.Split(",")[0];
                                    imageBytes = Convert.FromBase64String(data.FileTwoBase64.Split(',')[1]);
                                }
                                else
                                    imageBytes = Convert.FromBase64String(data.FileTwoBase64);

                                fileName = string.Format("{0}_{1}_{2}{3}", data.ParticipantName, LangId, MethodFactory.GetRandomNumber(), "." + data.DocTwoExtension);
                                System.IO.File.WriteAllBytes(
                                    Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                                 "\\PCAwards\\TestimonialTwo\\") + fileName, imageBytes);

                                entity.FileNameTwo = fileName;
                            }

                            entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                            entity.Status = EnumConversion.ToString(EnumAwardNominationStatus.New);

                            entity.CreatedOn = MethodFactory.ArabianTimeNow();
                            entity.ModifiedOn = MethodFactory.ArabianTimeNow();

                            if (Convert.ToInt64(data.hdnTempData) <= 0)
                                _context.XsiPcawardNominationForms.Add(entity);

                            if (_context.SaveChanges() > 0)
                            {
                                nominationid = entity.ItemId;
                                data.hdnTempData = nominationid.ToString();

                                XsiResult = EnumResultType.Success;
                                // pdf = DownloadAwardsRegistrationPDF(entity, LangId);

                                if (LangId == 1)
                                {
                                    strmessage = @"Thank you! Your application has been submitted successfully";
                                }
                                else
                                {
                                    strmessage = @"Thank you! Your application has been submitted successfully";
                                }

                                if (!string.IsNullOrEmpty(pdf))
                                {
                                    NotifyToAdmin(nominationid, 1, LangId);
                                    pdf = Path.GetFileName(pdf);
                                    isvalid = true;

                                    var dto1 = new { Isvalid = isvalid, PDFFileName = _appCustomSettings.UploadsPath + "PCAwards/AwardFormPassportCopy/downloadpdf/" + pdf, strmessage, data.hdnTempData };
                                    return Ok(dto1);
                                }
                                else
                                {
                                    NotifyToAdmin(nominationid, 1, LangId);
                                    isvalid = true;
                                    //  strmessage = "Form submitted. Pdf Couldn't be generated. Try again later.";
                                    var dto = new { Isvalid = isvalid, strmessage, data.hdnTempData };
                                    return Ok(dto);
                                }
                            }

                            isvalid = false;
                            strmessage = "Error while submitting the form";
                            var dto3 = new { Isvalid = isvalid, strmessage, data.hdnTempData };
                            return Ok(dto3);
                        }
                        else
                        {
                            strmessage = "Award Name field is missing";
                            var dto3 = new { Isvalid = isvalid, strmessage, data.hdnTempData };
                            return Ok(dto3);
                        }
                    }
                }
                else
                {
                    strmessage = "Participant Name field is missing";
                    var dto3 = new { Isvalid = isvalid, strmessage, data.hdnTempData };
                    return Ok(dto3);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside PC Award Nomination action: {ex.InnerException}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpGet]
        [Route("GetPCAwardList/{websiteId}")]
        public ActionResult<dynamic> GetPCAwardList(long websiteId)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            List<DropdownDataDTO> dtolist = new List<DropdownDataDTO>();
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (LangId == 1)
                    dtolist = _context.XsiPcawards.Where(i => i.IsActive == "Y" && i.WebsiteId == websiteId).Select(i => new DropdownDataDTO() { ItemId = i.ItemId, Title = i.Name }).ToList();
                else
                    dtolist = _context.XsiPcawards.Where(i => i.IsActiveAr == "Y" && i.WebsiteId == websiteId).Select(i => new DropdownDataDTO() { ItemId = i.ItemId, Title = i.NameAr }).ToList();
                return Ok(dtolist);
            }
        }
        [HttpGet]
        [Route("IsValidAwardDates/{websiteId}/{awardid}")]
        public ActionResult<dynamic> IsValidAwardDates(long websiteId, long awardid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string strmessage = string.Empty;
                websiteId = 1;
                bool Isvalid = false;
                var predicate = PredicateBuilder.True<XsiPcawardDetails>();

                predicate = predicate.And(i => i.AwardId == awardid);
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                XsiPcawardDetails entity = _context.XsiPcawardDetails.Where(predicate).OrderByDescending(i => i.ItemId).FirstOrDefault();
                if (entity != null)
                {
                    if (entity.RegistrationStartDate.Value.CompareTo(MethodFactory.ArabianTimeNow()) <= 0 && entity.RegistrationEndDate.Value.CompareTo(MethodFactory.ArabianTimeNow()) >= 0)
                        Isvalid = true;
                }
                else
                {
                    strmessage = "Registration is closed.";
                }

                var dto = new { Isvalid, strmessage };
                return Ok(dto);
            }
        }

        void NotifyToAdmin(long pcawardnominationid, long websiteId, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var pcawardnomination = _context.XsiPcawardNominationForms.Where(i => i.ItemId == pcawardnominationid).FirstOrDefault();
                if (pcawardnomination != null)
                {
                    HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {
                        string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";

                        #region Send Thankyou Email
                        StringBuilder body = new StringBuilder();
                        #region Email Content
                        var emailContent = EmailContentService.GetEmailContentByItemId(20172);
                        if (emailContent != null)
                        {
                            if (emailContent.Body != null)
                                strEmailContentBody = emailContent.Body;
                            if (emailContent.Email != null)
                                strEmailContentEmail = emailContent.Email;
                            if (emailContent.Subject != null)
                                strEmailContentSubject = emailContent.Subject;
                            strEmailContentAdmin = _appCustomSettings.AdminName;
                            #endregion
                            body.Append(htmlContentFactory.BindEmailContentPCAward(langId, strEmailContentBody, null, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                            var detailentity = _context.XsiPcawardDetails.Where(i => i.ItemId == pcawardnomination.AwardDetailId).FirstOrDefault();
                            if (detailentity != null)
                            {
                                var award = _context.XsiPcawards.Where(i => i.ItemId == detailentity.AwardId).FirstOrDefault();

                                if (award != null)
                                    body.Replace("$$awardname$$", detailentity.Award.Name);
                                else
                                    body.Replace("$$awardname$$", "-");
                            }

                            body.Replace("$$nominee$$", pcawardnomination.ParticipantName);
                            _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString());
                        }
                        #endregion
                    }
                }
            }
        }
    }
}


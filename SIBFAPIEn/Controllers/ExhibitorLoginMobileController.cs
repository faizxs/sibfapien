﻿using Contracts;
using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http.Cors;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class ExhibitorLoginMobileController : Controller
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private IUserService _userService;
        public ExhibitorLoginMobileController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IUserService userService)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost]
        //[EnableCors("AllowOrigin")]
        public ActionResult<dynamic> ExhibitionMemberLogin(LoginDto data)
        {
            LoggedinUserInfo userInfo = new LoggedinUserInfo();
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                var member = _userService.Authenticate(data.UserName, data.Password, LangId);
                LoginResposeMobileDTO response = new LoginResposeMobileDTO();
                if (member.SIBFMemberId > 0)
                {
                    response.MemberId = member.SIBFMemberId;
                    response.Name = member.SIBFMemberPublicName;
                    return Ok(response);
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
            return Ok(userInfo);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionActivityYearlyController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionActivityYearlyController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/ExhibitionActivityYearly
        [HttpGet("{exhibitionid}")]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetXsiExhibitionActivityYearly(long? exhibitionid = -1)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionActivityYearly>();

                if (exhibitionid <= 0)
                {
                    var exhibitionwhere = PredicateBuilder.True<XsiExhibition>();
                    exhibitionwhere = exhibitionwhere.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    exhibitionwhere = exhibitionwhere.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
                    exhibitionwhere = exhibitionwhere.And(i => i.WebsiteId == 1);

                    var exhibitionEntity = _context.XsiExhibition.AsQueryable().Where(exhibitionwhere).OrderByDescending(x => x.ExhibitionId).FirstOrDefault();

                    if (exhibitionEntity != null)
                        predicate = predicate.And(i => i.ExhibitionId == exhibitionEntity.ExhibitionId);
                    else
                        predicate = predicate.And(i => i.ExhibitionId == exhibitionid);
                }
                else
                    predicate = predicate.And(i => i.ExhibitionId == exhibitionid);

                var activityIDs = _context.XsiExhibitionActivityYearly.AsQueryable().Where(predicate).OrderBy(x => x.ExhibitionId).Select(x => x.ExhibitionActivityId).ToList();
                return await _context.XsiExhibitionActivity.Where(i => activityIDs.Contains(i.ItemId) && i.IsActive == "Y")
                    .Select(i => new { value = i.ItemId, label = i.Title }).ToListAsync();
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionActivityYearly>();

                if (exhibitionid <= 0)
                {
                    var exhibitionwhere = PredicateBuilder.True<XsiExhibition>();
                    exhibitionwhere = exhibitionwhere.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    exhibitionwhere = exhibitionwhere.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
                    exhibitionwhere = exhibitionwhere.And(i => i.WebsiteId == 1);

                    var exhibitionEntity = _context.XsiExhibition.AsQueryable().Where(exhibitionwhere).OrderByDescending(x => x.ExhibitionId).FirstOrDefault();

                    if (exhibitionEntity != null)
                        predicate = predicate.And(i => i.ExhibitionId == exhibitionEntity.ExhibitionId);
                    else
                        predicate = predicate.And(i => i.ExhibitionId == exhibitionid);
                }
                else
                    predicate = predicate.And(i => i.ExhibitionId == exhibitionid);

                var activityIDs = _context.XsiExhibitionActivityYearly.AsQueryable().Where(predicate).OrderBy(x => x.ExhibitionId).Select(x => x.ExhibitionActivityId).ToList();
                return await _context.XsiExhibitionActivity.Where(i => activityIDs.Contains(i.ItemId) && i.IsActiveAr == "Y")
                    .Select(i => new { value = i.ItemId, label = i.TitleAr }).ToListAsync();
            }
        }

        // GET: api/ExhibitionActivity/5
        [HttpGet]
        [Route("ExhibitionActivity/{id}")]
        public async Task<ActionResult<CategoryDTO>> GetXsiExhibitionActivity(long id)
        {
            var xsiItem = await _context.XsiExhibitionActivity.FindAsync(id);

            if (xsiItem == null)
            {
                return NotFound();
            }
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                CategoryDTO itemDTO = new CategoryDTO()
                {
                    ItemId = xsiItem.ItemId,
                    Title = xsiItem.Title,
                };
                return itemDTO;
            }
            else
            {
                CategoryDTO itemDTO = new CategoryDTO()
                {
                    ItemId = xsiItem.ItemId,
                    Title = xsiItem.TitleAr,
                };
                return itemDTO;
            }
        }

        private bool XsiExhibitionActivityYearlyExists(long id)
        {
            return _context.XsiExhibitionActivityYearly.Any(e => e.ExhibitionId == id);
        }
    }
}

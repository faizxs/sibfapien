﻿using Contracts;
using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PCAwardWinnersController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;

        public PCAwardWinnersController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings)
        {
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
        }
        // GET: api/Awards
        [HttpGet]
        [Route("{pageNumber}/{pageSize}/{yr}")]
        public async Task<ActionResult<dynamic>> GetXsiPCAwardWinners(int? pageNumber = 1, int? pageSize = 15, long yr = -1)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long WebsiteId = Convert.ToInt32(EnumWebsiteTypeId.SIBF);
                    //    MethodFactory.LoadPageContentsNew(92, langID);
                    var thumbnailpath = _appCustomSettings.UploadsPath + "/PCAwards/Thumbnail/";
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    if (LangId == 1)
                    {
                        var predicate = PredicateBuilder.True<XsiPcawards>();
                        predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicate = predicate.And(i => i.WebsiteId == WebsiteId);
                        var SIBFPCAwardIds = _context.XsiPcawards.Where(predicate).Select(i => i.ItemId).ToList();

                        var SIBFPCAwardDetailIds = _context.XsiPcawardDetails.Where(i => i.RegistrationStartDate.Value.Year == yr && i.IsActive == "Y" && SIBFPCAwardIds.Contains(i.AwardId.Value)).Select(i => i.ItemId).ToList();

                        XsiPcawardNominationForms where = new XsiPcawardNominationForms();
                        var predicateW = PredicateBuilder.True<XsiPcawardNominationForms>();

                        //if (exhibitionid != -1)
                        //    predicateW = predicateW.And(i => i.ExhibitionId == exhibitionid);
                        predicateW = predicateW.And(i => i.Status == "W" && i.IsActive=="Y" && SIBFPCAwardDetailIds.Contains(i.AwardDetailId.Value));
                        var List = _context.XsiPcawardNominationForms.Where(predicateW).OrderByDescending(i => i.ItemId)
                            .Select(x => new PCAwardWinnerDto()
                            {
                                ItemId = x.ItemId,
                                AwardId = x.AwardDetail.AwardId ?? -1,
                                AwardDetailId = x.AwardDetailId?? - 1,
                                AwardTitle = GetAwardOrSubAwardTitle(x.AwardDetail.AwardId ?? -1, LangId),
                                ParticipantName = x.ParticipantName,
                                Thumbnail = thumbnailpath + (x.Thumbnail ?? "award-tile.jpg"),
                                Overview = x.Overview,
                                Testimonial = x.Testimonial
                            }).ToList();

                        var totalCount = List.Count;
                        List = List.Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToList();
                        var dto = new { List, TotalCount = totalCount };

                        return Ok(dto);
                    }
                    else
                    {
                        var predicate = PredicateBuilder.True<XsiAwards>();
                        predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                        predicate = predicate.And(i => i.WebsiteId == WebsiteId);
                        var SIBFAwardIds = _context.XsiAwards.Where(predicate).Select(i => i.ItemId).ToList();


                        XsiPcawardNominationForms where = new XsiPcawardNominationForms();
                        var predicateW = PredicateBuilder.True<XsiPcawardNominationForms>();

                        //if (exhibitionid != -1)
                        //    predicateW = predicateW.And(i => i.ExhibitionId == exhibitionid);
                        predicateW = predicateW.And(i => i.Status == "W" && i.IsActive == "Y");

                        var List = _context.XsiPcawardNominationForms.Where(predicateW).OrderByDescending(i => i.ItemId).Select(x => new PCAwardWinnerDto()
                        {
                            ItemId = x.ItemId,
                            AwardId = x.AwardDetail.AwardId ?? -1,
                            AwardDetailId = x.AwardDetailId??-1,
                            AwardTitle = GetAwardOrSubAwardTitle(x.AwardDetail.AwardId ?? -1, LangId),
                            ParticipantName = x.ParticipantName,
                            Thumbnail = thumbnailpath + (x.Thumbnail ?? "award-tile.jpg"),
                            Overview = x.Overview,
                            Testimonial = x.Testimonial
                        }).ToList();

                        var totalCount = List.Count;
                        List = List.Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToList();
                        var dto = new { List, TotalCount = totalCount };

                        return Ok(dto);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwardWinners action: {ex.InnerException}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        // GET: api/AwardWinners/5
        [HttpGet("{id}")]
        public ActionResult<dynamic> GetXsiAwardWinnerDetails(long id)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    if (LangId == 1)
                    {
                        var predicate = PredicateBuilder.True<XsiPcawardNominationForms>();
                        predicate = predicate.And(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                        var thumbnailpath = _appCustomSettings.UploadsPath;
                        var dto = _context.XsiPcawardNominationForms.AsQueryable().Where(predicate)
                            .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(x => new AwardWinnerDto()
                            {
                                ItemId = x.ItemId,
                                AwardId = x.AwardDetail.AwardId ?? -1,
                                SubAwardId =  -1,
                                ExhibitionId =  -1,
                                AwardTitle = GetAwardOrSubAwardTitle(x.AwardDetail.AwardId??-1,LangId),
                                 PublisherName = x.ParticipantName,
                                PublisherThumbnail = (x.Thumbnail == null) ? thumbnailpath + "/Winner/Thumbnail/" + "award-tile.jpg" : thumbnailpath + "/Winner/Thumbnail/" + x.Thumbnail,
                                Overview = x.Overview
                            }).FirstOrDefault();

                        if (dto == null)
                        {
                            return NotFound();
                        }

                        dto.AwardTitle = GetAwardOrSubAwardTitle(dto.AwardId, LangId);
                        return Ok(dto);
                    }
                    else
                    {
                        var predicate = PredicateBuilder.True<XsiPcawardNominationForms>();
                        predicate = predicate.And(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                        var thumbnailpath = _appCustomSettings.UploadsPath;
                        var dto = _context.XsiPcawardNominationForms.AsQueryable().Where(predicate)
                            .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(x => new AwardWinnerDto()
                            {
                                ItemId = x.ItemId,
                                AwardId = x.AwardDetail.AwardId ?? -1,
                                SubAwardId = -1,
                                ExhibitionId = -1,
                                AwardTitle = GetAwardOrSubAwardTitle(x.AwardDetail.AwardId ?? -1, LangId),
                                PublisherName = x.ParticipantName,
                                PublisherThumbnail = (x.Thumbnail == null) ? thumbnailpath + "/Winner/Thumbnail/" + "award-tile.jpg" : thumbnailpath + "/Winner/Thumbnail/" + x.Thumbnail,
                                Overview = x.Overview
                            }).FirstOrDefault();

                        if (dto == null)
                        {
                            return NotFound();
                        }

                        dto.AwardTitle = GetAwardOrSubAwardTitle(dto.AwardId, LangId);
                        return Ok(dto);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwardWinners action: {ex.InnerException}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpGet("GetYears")]
        public ActionResult<IEnumerable<YearDTO>> GetYears()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiPcawardDetails>();

                    predicate = predicate.And(i => i.RegistrationStartDate != null);
                    predicate = predicate.And(i => i.IsActive == "Y");
                    var Year = _context.XsiPcawardDetails.AsQueryable().Where(predicate)
                        .GroupBy(g => g.RegistrationStartDate.Value.Year)
                         .OrderByDescending(o => o.Key).Select(i => new YearDTO() { Id = i.Key, Year = i.Key })
                        .ToList();

                    return Year;
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiPcawardDetails>();

                    predicate = predicate.And(i => i.RegistrationStartDate != null);
                    predicate = predicate.And(i => i.IsActiveAr == "Y");
                    var Year = _context.XsiPcawardDetails.AsQueryable().Where(predicate)
                        .GroupBy(g => g.RegistrationStartDate.Value.Year)
                         .OrderByDescending(o => o.Key).Select(i => new YearDTO() { Id = i.Key, Year = i.Key })
                        .ToList();
                    return  Year;
                }
            }
        }
        string GetAwardOrSubAwardTitle(long awardid, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (langId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiPcawards>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.ItemId == awardid);

                    XsiPcawards entity = _context.XsiPcawards.Where(predicate).FirstOrDefault();
                    if (entity != null)
                    {
                        return entity.Name;
                    }
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiPcawards>();
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.ItemId == awardid);

                    XsiPcawards entity = _context.XsiPcawards.Where(predicate).FirstOrDefault();
                    if (entity != null)
                    {
                        return entity.Name;
                    }
                }
                return string.Empty;
            }
        }
    }
}

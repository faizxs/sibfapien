﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionBookSubSubjectController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionBookSubSubjectController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/ExhibitionBookSubSubject/5
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<SubCategoryDTO>>> GetXsiExhibitionBookSubsubject(int id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionBookSubsubject>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.CategoryId == id);

                var List = _context.XsiExhibitionBookSubsubject.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(i => new SubCategoryDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.Title,
                    CategoryTitle = i.Category.Title,
                    CategoryId = i.Category.ItemId
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionBookSubsubject>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.CategoryId == id);

                var List = _context.XsiExhibitionBookSubsubject.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(i => new SubCategoryDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.TitleAr,
                    CategoryTitle = i.Category.TitleAr,
                    CategoryId = i.Category.ItemId
                }).ToListAsync();

                return await List;
            }
        }

        // GET: api/ExhibitionBookSubSubject/5/1
        [HttpGet("{subjectid}/{id}")]
        public async Task<ActionResult<SubCategoryDTO>> GetXsiExhibitionBookSubsubject(int subjectid, long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var subCategory = await _context.XsiExhibitionBookSubsubject.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes))
              .Select(i => new SubCategoryDTO()
              {
                  ItemId = i.ItemId,
                  Title = i.Title,
                  CategoryTitle = i.Category.Title,
                  CategoryId = i.Category.ItemId
              }).FirstOrDefaultAsync();

                if (subCategory == null)
                {
                    return NotFound();
                }

                return subCategory;
            }
            else
            {
                var subCategory = await _context.XsiExhibitionBookSubsubject.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes))
              .Select(i => new SubCategoryDTO()
              {
                  ItemId = i.ItemId,
                  Title = i.TitleAr,
                  CategoryTitle = i.Category.TitleAr,
                  CategoryId = i.Category.ItemId
              }).FirstOrDefaultAsync();

                if (subCategory == null)
                {
                    return NotFound();
                }

                return subCategory;
            }
        }

        private bool XsiExhibitionSubSubjectExists(long id)
        {
            return _context.XsiExhibitionBookSubsubject.Any(e => e.ItemId == id);
        }
    }
}

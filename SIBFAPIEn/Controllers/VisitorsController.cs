﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class VisitorsController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public VisitorsController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _context = context;
        }

        // GET: api/Visitors
        [HttpGet]
        [Route("{pageNumber}/{pageSize}")]
        public async Task<ActionResult<dynamic>> GetXsiVisitor(int? pageNumber = 1, int? pageSize = 15)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var List = await _context.XsiVisitor.Where(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.ItemId).Select(i => new
                    {
                        ItemId = i.ItemId,
                        Title = i.Title,
                        Overview = i.Overview,
                        ImageName = _appCustomSettings.UploadsCMSPath + "/VisitorNew/" + i.ImageName,
                        Url = i.Url
                    }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();
                    var count = _context.XsiVisitor.Where(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes)).ToList()
                        .Count;

                    var dto = new { List, TotalCount = count };
                    return dto;
                }
                else
                {
                    var List = await _context.XsiVisitor.Where(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.ItemId).Select(i => new
                    {
                        ItemId = i.ItemId,
                        Title = i.TitleAr,
                        Overview = i.OverviewAr,
                        ImageName = _appCustomSettings.UploadsCMSPath + "/VisitorNew/" + i.ImageNameAr,
                        Url = i.Urlar
                    }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();
                    var count = _context.XsiVisitor.Where(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).ToList()
                        .Count;

                    var dto = new { List, TotalCount = count };
                    return dto;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiVisitors action: {ex.Message}");
                return Ok("Something went wrong. Please try again later.");

            }
        }

        // GET: api/Visitors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<dynamic>> GetXsiVisitor(long id)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var xsiItem = await _context.XsiVisitor.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                    if (xsiItem == null)
                    {
                        return NotFound();
                    }
                    var itemDTO = new
                    {
                        ItemId = xsiItem.ItemId,
                        Title = xsiItem.Title,
                        Overview = xsiItem.Overview,
                        ImageName = _appCustomSettings.UploadsPath + "/VisitorNew/" + xsiItem.ImageName,
                        Url = xsiItem.Url
                    };
                    return itemDTO;
                }
                else
                {
                    var xsiItem = await _context.XsiVisitor.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                    if (xsiItem == null)
                    {
                        return NotFound();
                    }
                    var itemDTO = new
                    {
                        ItemId = xsiItem.ItemId,
                        Title = xsiItem.TitleAr,
                        Overview = xsiItem.OverviewAr,
                        ImageName = _appCustomSettings.UploadsPath + "/VisitorNew/" + xsiItem.ImageNameAr,
                        Url = xsiItem.Urlar
                    };
                    return itemDTO;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiVisitors action: {ex.Message}");
                return Ok("Something went wrong. Please try again later.");

            }
        }

        private bool XsiVisitorExists(long id)
        {
            return _context.XsiVisitor.Any(e => e.ItemId == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class VideosController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public VideosController(IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }
        // GET: api/Video
        [HttpGet]
        [Route("{albumid}/{pageNumber}/{pageSize}/{strSearch}")]
        public async Task<ActionResult<dynamic>> GetXsiVideo(long? albumid = -1, int? pageNumber = 1, int? pageSize = 15, string strSearch = "")
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiVideoAlbum>();

                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                if (albumid != -1)
                    predicate = predicate = predicate.And(i => i.ItemId == albumid);

                if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                    predicate = predicate.And(i => i.Dated != null && i.Dated.Value.Year.ToString() == strSearch);

                XsiVideoAlbum entity = new XsiVideoAlbum();
                entity = _context.XsiVideoAlbum.AsQueryable().Where(predicate).FirstOrDefault();

                dynamic album = new ExpandoObject();
                if (entity != default(XsiVideoAlbum))
                {
                    album.ItemId = entity.ItemId;
                    album.Title = entity.Title;
                    album.StrDated = entity.Dated == null ? string.Empty : entity.Dated.Value.ToString("dd MMM yyyy");
                }
                #region Video List
                var predicateVideo = PredicateBuilder.True<XsiVideo>();

                predicateVideo = predicateVideo.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                if (albumid != -1)
                {
                    predicateVideo = predicateVideo.And(i => i.VideoAlbumId == albumid);
                }
                var kount = _context.XsiVideo.Where(predicateVideo).Count();
                List<VideoDTO> VideoList = new List<VideoDTO>();
                VideoList = await _context.XsiVideo.AsQueryable().Where(predicateVideo)
                    .Select(i => new VideoDTO()
                    {
                        ItemId = i.ItemId,
                        Title = i.Title,
                        AlbumTitle = i.VideoAlbum.Title,
                        Description = i.Description,
                        SortIndex = i.SortIndex,
                        URL = GetVideoURL(i.Url),
                        Thumbnail = _appCustomSettings.UploadsPath + "/VideoGallery/Thumbnailnew/" + (i.Thumbnail ?? "book-image.jpg"),
                        CreatedOn = i.CreatedOn
                    }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                #endregion
                var dto = new { Album = album, VideoList, TotalCount = kount };
                return dto;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiVideoAlbum>();

                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                if (albumid != -1)
                    predicate = predicate = predicate.And(i => i.ItemId == albumid);

                if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                    predicate = predicate.And(i => i.DatedAr != null && i.DatedAr.Value.Year.ToString() == strSearch);

                XsiVideoAlbum entity = new XsiVideoAlbum();
                entity = _context.XsiVideoAlbum.AsQueryable().Where(predicate).FirstOrDefault();

                dynamic album = new ExpandoObject();
                if (entity != default(XsiVideoAlbum))
                {
                    album.ItemId = entity.ItemId;
                    album.Title = entity.TitleAr;
                    album.StrDated = entity.DatedAr == null ? string.Empty : entity.DatedAr.Value.ToString("dd MMM yyyy");
                }
                #region Video List
                var predicateVideo = PredicateBuilder.True<XsiVideo>();

                predicateVideo = predicateVideo.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                if (albumid != -1)
                {
                    predicateVideo = predicateVideo.And(i => i.VideoAlbumId == albumid);
                }
                var kount = _context.XsiVideo.Where(predicateVideo).Count();
                List<VideoDTO> VideoList = new List<VideoDTO>();
                VideoList = await _context.XsiVideo.AsQueryable().Where(predicateVideo)
                    .Select(i => new VideoDTO()
                    {
                        ItemId = i.ItemId,
                        Title = i.TitleAr,
                        AlbumTitle = i.VideoAlbum.TitleAr,
                        Description = i.DescriptionAr,
                        SortIndex = i.SortIndex,
                        URL = GetVideoURL(i.Url),
                        Thumbnail = _appCustomSettings.UploadsPath + "/VideoGallery/Thumbnailnew/" + (i.ThumbnailAr ?? "book-image.jpg"),
                        CreatedOn = i.CreatedOn
                    }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                #endregion
                var dto = new { Album = album, VideoList, TotalCount = kount };
                return dto;
            }
        }

        // GET: api/VideoAlbum/5
        [HttpGet("{id}")]
        public async Task<ActionResult<VideoDTO>> GetXsiVideo(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var Video = await _context.XsiVideo.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes))
                .Select(i => new VideoDTO()
                {
                    ItemId = i.ItemId,
                    AlbumTitle = i.VideoAlbum.Title,
                    Title = i.Title,
                    Description = i.Description,
                    SortIndex = i.SortIndex,
                    URL = GetVideoURL(i.Url),
                    Thumbnail = _appCustomSettings.UploadsPath + "/VideoGallery/Thumbnailnew/" + (i.Thumbnail ?? "book-image.jpg"),
                    CreatedOn = i.CreatedOn
                }).FirstOrDefaultAsync();

                if (Video == null)
                {
                    return NotFound();
                }

                return Video;
            }
            else
            {
                var Video = await _context.XsiVideo.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes))
                .Select(i => new VideoDTO()
                {
                    ItemId = i.ItemId,
                    AlbumTitle = i.VideoAlbum.TitleAr,
                    Title = i.TitleAr,
                    Description = i.DescriptionAr,
                    SortIndex = i.SortIndex,
                    URL = GetVideoURL(i.Urlar),
                    Thumbnail = _appCustomSettings.UploadsPath + "/VideoGallery/Thumbnailnew/" + (i.ThumbnailAr ?? "book-image.jpg"),
                    CreatedOn = i.CreatedOn
                }).FirstOrDefaultAsync();

                if (Video == null)
                {
                    return NotFound();
                }

                return Video;
            }
        }

        private string GetVideoURL(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                //if (url.Contains("embed"))
                //    return url.Replace("embed/", "watch?v=");
                //else
                if (url.Contains("watch?v="))
                    return url.Replace("watch?v=", "embed/");
                else
                    return url;
            }
            return string.Empty;
        }

        private bool XsiVideo(long id)
        {
            return _context.XsiVideo.Any(e => e.ItemId == id);
        }
        internal string GetThumbnail(long albumId)
        {
            string strimage = "book-image.jpg";
            var thumbnail = _context.XsiVideo.Where(p => p.Thumbnail != null && p.IsAlbumCover == "Y" && p.IsActive == "Y" && p.VideoAlbumId == albumId).OrderByDescending(i => i.CreatedOn).FirstOrDefault();
            if (thumbnail != null)
                strimage = thumbnail.Thumbnail;
            else
            {
                thumbnail = _context.XsiVideo.Where(p => p.Thumbnail != null && p.IsActive == "Y" && p.VideoAlbumId == albumId).OrderByDescending(i => i.CreatedOn).FirstOrDefault();
                if (thumbnail != null)
                    strimage = thumbnail.Thumbnail;
            }
            return strimage;
        }
    }
}

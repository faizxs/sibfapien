﻿using Contracts;
using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xsi.ServicesLayer;
namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AwardNominationRegistrationNewController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private ILoggerManager _logger;
        private IMemoryCache _cache;
        private readonly sibfnewdbContext _context;
        EnumResultType XsiResult { get; set; }

        private readonly IEmailSender _emailSender;
        public AwardNominationRegistrationNewController(IEmailSender emailSender, IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IMemoryCache memoryCache, sibfnewdbContext context)
        {
            _emailSender = emailSender;
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _cache = memoryCache;
            _context = context;
        }

        [HttpGet]
        [Route("IsValidAwardDates/{websiteId}")]
        public ActionResult<dynamic> IsValidAwardDates(long websiteId)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            string strmessage = string.Empty;
            var dto = MethodFactory.IsValidAwardDates(websiteId, LangId, _cache);
            if (LangId == 1)
                strmessage = "Registration is closed.";
            else
                strmessage = "Registration is closed.";
            var dto1 = new { Isvalid = dto, strmessage };
            return Ok(dto1);
        }

        // GET: api/AwardListById
        [HttpGet]
        [Route("AwardList")]
        public async Task<ActionResult<IEnumerable<DropdownDataDTO>>> GetXsiAwardNewList()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiAwardNew>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => (i.WebsiteId == 0 || i.WebsiteId == 1));

                    var List = await _context.XsiAwardNew.AsQueryable().Where(predicate).OrderBy(x => x.Name.Trim()).Select(x => new DropdownDataDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.Name
                    }).ToListAsync();

                    return Ok(List);
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiAwardNew>();
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => (i.WebsiteId == 0 || i.WebsiteId == 1));

                    var List = await _context.XsiAwardNew.AsQueryable().Where(predicate).OrderBy(x => x.NameAr.Trim()).Select(x => new DropdownDataDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.NameAr
                    }).ToListAsync();

                    return Ok(List);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwardNewsListById action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        // GET: api/AwardListById
        [HttpGet]
        [Route("SubAwardList/{awardid}")]
        public async Task<ActionResult<IEnumerable<DropdownDataDTO>>> GetXsiSubAwardNeweListById(long awardid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                var predicate = PredicateBuilder.True<XsiSubAwardNew>();
                predicate = predicate.And(i => i.AwardId == awardid);

                if (LangId == 1)
                {
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    var List = await _context.XsiSubAwardNew.AsQueryable().Where(predicate).OrderBy(x => x.Name.Trim()).Select(x => new DropdownDataDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.Name
                    }).ToListAsync();

                    return Ok(List);
                }
                else
                {
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    var List = await _context.XsiSubAwardNew.AsQueryable().Where(predicate).OrderBy(x => x.NameAr.Trim()).Select(x => new DropdownDataDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.NameAr
                    }).ToListAsync();

                    return Ok(List);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiSubAwardNewsListById action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        // POST: api/AwardNominationRegistrationNew
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> PostAwardNominationFormBook(AwardNominationFormNewDTO data)
        {
            try
            {
                string strmessage = string.Empty;
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var isvalidawaraddate = MethodFactory.IsValidAwardDates(1, LangId, _cache);
                if (isvalidawaraddate)
                {
                    #region Award Submission
                    bool isvalid = true;
                    string pdf = string.Empty;

                    XsiAwardNominationRegistrations entity = new XsiAwardNominationRegistrations();
                    if (data.hdnTempData != null && data.hdnTempData != "0" && data.hdnTempData != "" && data.hdnTempData != "-1")
                    {
                        entity = _context.XsiAwardNominationRegistrations.Where(i => i.ItemId == Convert.ToInt64(data.hdnTempData))
                            .FirstOrDefault();
                        if (entity != default(XsiAwardNominationRegistrations))
                        {
                            entity.ItemId = Convert.ToInt64(data.hdnTempData);
                        }
                    }

                    //entity.ExhibitionId = MethodFactory.GetActiveExhibition(Convert.ToInt64(EnumWebsiteId.SIBF));

                    var exhibition = MethodFactory.GetActiveExhibitionNew(Convert.ToInt64(EnumWebsiteId.SIBF), _cache);
                    entity.ExhibitionId = exhibition.ExhibitionId;

                    #region Form Fields
                    entity.AwardId = data.AwardId;
                    if (data.SubAwardId != null && data.SubAwardId != 0 && data.SubAwardId != -1)
                        entity.SubAwardId = data.SubAwardId;

                    entity.Title = data.Title;
                    if (data.BookCoverFileBase64 != null && data.BookCoverFileBase64.Length > 0)
                    {
                        string fileName = string.Empty;
                        byte[] imageBytes;
                        if (data.BookCoverFileBase64.Contains("data:"))
                        {
                            var strInfo = data.BookCoverFileBase64.Split(",")[0];
                            imageBytes = Convert.FromBase64String(data.BookCoverFileBase64.Split(',')[1]);
                        }
                        else
                            imageBytes = Convert.FromBase64String(data.BookCoverFileBase64);

                        fileName = string.Format("{0}_{1}_{2}{3}", data.AuthorName, LangId, MethodFactory.GetRandomNumber(), "." + data.DocExtension);
                        System.IO.File.WriteAllBytes(
                            Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                         "\\AwardsNominationRegistrationNew\\BookCover\\") + fileName, imageBytes);

                        entity.BookCover = fileName;
                    }
                    entity.PublishedYear = data.PublishedYear;
                    entity.Isbn = data.ISBN;
                    #endregion

                    #region Author
                    entity.AuthorName = data.AuthorName;
                    if (data.AuthorNationalityId != null && data.AuthorNationalityId != -1 && data.AuthorNationalityId != 0)
                        entity.AuthorNationalityId = data.AuthorNationalityId;
                    if (data.FileBase64 != null && data.FileBase64.Length > 0)
                    {
                        string fileName = string.Empty;
                        byte[] imageBytes;
                        if (data.FileBase64.Contains("data:"))
                        {
                            var strInfo = data.FileBase64.Split(",")[0];
                            imageBytes = Convert.FromBase64String(data.FileBase64.Split(',')[1]);
                        }
                        else
                            imageBytes = Convert.FromBase64String(data.FileBase64);

                        fileName = string.Format("{0}_{1}_{2}{3}", data.AuthorName, LangId, MethodFactory.GetRandomNumber(), "." + data.DocExtension);
                        System.IO.File.WriteAllBytes(
                            Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                         "\\AwardsNominationRegistrationNew\\PassportCopy\\") + fileName, imageBytes);

                        entity.PassportCopy = fileName;
                    }

                    entity.AuthorEmail = data.AuthorEmail;
                    entity.AuthorNumber = data.AuthorISD + "$" + data.AuthorSTD + "$" + data.AuthorPhone;

                    #endregion

                    #region Publisher
                    entity.Publisher = data.Publisher;
                    if (data.PublisherCountryId != null && data.PublisherCountryId != -1 && data.PublisherCountryId != 0)
                        entity.PublisherCountryId = data.PublisherCountryId;
                    entity.PublisherEmail = data.PublisherEmail;
                    entity.PublisherNumber = data.PublisherISD + "$" + data.PublisherSTD + "$" + data.PublisherPhone;
                    #endregion

                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    entity.Status = EnumConversion.ToString(EnumAwardNominationStatus.New);
                    entity.CreatedOn = MethodFactory.ArabianTimeNow();
                    entity.ModifiedOn = MethodFactory.ArabianTimeNow();

                    if (Convert.ToInt64(data.hdnTempData) <= 0)
                        _context.XsiAwardNominationRegistrations.Add(entity);

                    if (_context.SaveChanges() > 0)
                    {
                        long participantid = entity.ItemId;
                        data.hdnTempData = participantid.ToString();

                        XsiResult = EnumResultType.Success;
                        pdf = DownloadAwardsRegistrationPDF(entity, LangId);

                        if (LangId == 1)
                        {
                            strmessage = @" <p>We would like to thank you for participating in Sharjah International Book Fair Awards 2024. In order to complete your participation, it is required to submit the following:</p>
                                <p>1- Three hard copies of the participating book (note that these copies will not be sent back)<br />
                                   2- A document of the participation form that has been submitted on the website.<br />
                                </p>
                                <p>Kindly send the requirements to the following address:</p>
                                <p>
                                   <b>Sharjah Book Authority (Cultural Awards Department)</b> <br /> 
                                    Awards Adminstration<br />
                                    Sheikh Mohammed bin Zayed Road - Al Zahia - Sharjah,<br />
                                    P.O. Box:73111<br />

                                    Tel: +971 56 603 6623,<br />
                                    E-mail: <a href='mailto: logistics@sharjahbookfair.com' target='_blank'>logistics@sharjahbookfair.com</a></p>";
                        }
                        else
                        {
                            strmessage = @"<p>نشكركم على مشاركتكم في جوائز معرض الشارقة الدولي للكتاب 2024.</p>
                              <p>لاستكمال المشاركة، يرجى تقديم ما يلي :</p>
<p>1-	ثلاث نسخ ورقية من الكتاب المشارك (يرجى ملاحظة أنه لن يتم إرجاع هذه النسخ)	<br>2-	وثيقة نموذج المشاركة المقدم على الموقع الإلكتروني</p>
<p>الرجاء إرسال الوثائق المطلوبة إلى العنوان التالي:</p>

<p><b>هيئة الشارقة للكتاب) قسم الجوائز الثقافية)</b>
<br>طريق الشيخ محمد بن زايد - الزاهية - الشارقة 

<br>  ص.ب : 73111 
<br> <span dir='ltr'>+971 56 603 6623</span>
<br> بريد الكتروني:<a href='logistics@sharjahbookfair.com' target='_blank'>logistics@sharjahbookfair.com</a></p>";
                        }

                        if (!string.IsNullOrEmpty(pdf))
                        {
                            pdf = Path.GetFileName(pdf);
                            isvalid = true;

                            var dto1 = new { Isvalid = isvalid, PDFFileName = _appCustomSettings.UploadsPath + "/AwardsNominationRegistrationNew/downloadpdf/" + pdf, strmessage, data.hdnTempData };
                            SendEmailToAuthor(participantid, LangId);
                            return Ok(dto1);
                        }
                        else
                        {
                            isvalid = true;
                            //  strmessage = "Form submitted. Pdf Couldn't be generated. Try again later.";
                            var dto = new { Isvalid = isvalid, strmessage, data.hdnTempData };
                            SendEmailToAuthor(participantid, LangId);
                            return Ok(dto);
                        }
                    }

                    isvalid = false;
                    strmessage = "Error while submitting the form";
                    var dto3 = new { Isvalid = isvalid, strmessage, data.hdnTempData };
                    return Ok(dto3);
                    #endregion
                }
                else
                {
                    if (LangId == 1)
                        strmessage = "Registration is closed.";
                    else
                        strmessage = "Registration is closed.";
                    var dto4 = new { Isvalid = isvalidawaraddate, strmessage, data.hdnTempData };
                    return Ok(dto4);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwardNominationForm action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
        
        void SendEmailToAuthor(long itemid, long langid)
        {
            var entity = _context.XsiAwardNominationRegistrations.Where(i => i.ItemId == itemid).FirstOrDefault();
            if (entity != default(XsiAwardNominationRegistrations))
            {
                using (EmailContentService EmailContentService = new EmailContentService())
                {
                    HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                    #region Email to Author
                    StringBuilder body = new StringBuilder();
                    string StrEmailContentBody = string.Empty;
                    string StrEmailContentSubject = string.Empty;
                    #region Email Content
                    var EmailContent = EmailContentService.GetEmailContentByItemId(20198);
                    if (EmailContent != null)
                    {
                        if (langid == 1)
                        {
                            if (EmailContent.Body != null)
                                StrEmailContentBody = EmailContent.Body;

                            if (EmailContent.Subject != null)
                                StrEmailContentSubject = EmailContent.Subject;
                        }
                        else
                        {
                            if (EmailContent.BodyAr != null)
                                StrEmailContentBody = EmailContent.BodyAr;
                            if (EmailContent.SubjectAr != null)
                                StrEmailContentSubject = EmailContent.SubjectAr;
                        }
                    }
                    #endregion
                    #endregion

                    body.Append(htmlContentFactory.BindEmailContentForAwardsSIBF(langid, StrEmailContentBody, entity, _appCustomSettings.ServerAddressNew, _appCustomSettings));
                    _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, entity.AuthorEmail, StrEmailContentSubject, body.ToString());
                }
            }
        }
        internal string DownloadAwardsRegistrationPDF(XsiAwardNominationRegistrations entity, long langId)
        {
            string fileName = string.Empty;
            #region PDF
            StringBuilder applicationform = new StringBuilder();
            if (entity != default(XsiAwardNominationRegistrations))
            {
                var XsiCurrentLanguageId = 1;

                applicationform.Append(System.IO.File.ReadAllText(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\AwardsNominationRegistrationNew\\HtmlTemplate\\AwardNominationRegistrationForm.html")));

                #region Replace Placeholders
                applicationform.Replace("$$ServerAddress$$", _appCustomSettings.ServerAddressNew);
                applicationform.Replace("$$ServerAddressCMS$$", _appCustomSettings.ServerAddressCMS);
                string str = MethodFactory.GetExhibitionDetails(Convert.ToInt64(EnumWebsiteId.SIBF));
                string[] strArray = str.Split(',');

                if (strArray[0] == "10" || strArray[0] == "11" || strArray[0] == "12" || strArray[0] == "13")
                    applicationform.Replace("$$sf$$", "th");
                else
                    applicationform.Replace("$$sf$$", MethodFactory.GetSuffix(Convert.ToInt32(strArray[0]) % 10));

                //applicationform.Replace("$$sf$$", GetSuffix(Convert.ToInt32(strArray[0])));
                //applicationform.Replace("$$exhibitiondate$$", strArray[1]);

                applicationform.Replace("$$exhibitionno$$", strArray[0].ToString());
                applicationform.Replace("$$exhibitiondate$$", strArray[1].ToString());
                applicationform.Replace("$$exhyear$$", strArray[2]);

                applicationform.Replace("$$award$$", MethodFactory.GetAwardOrSubAwardTitle(-1, XsiCurrentLanguageId, entity.AwardId.Value));// MethodFactory.GetAwardTitle(entity.AwardGroupId.Value, XsiCurrentLanguageId));
                applicationform.Replace("$$subaward$$", MethodFactory.GetAwardOrSubAwardTitle(entity.SubAwardId ?? -1, XsiCurrentLanguageId, entity.AwardId.Value));
                applicationform.Replace("$$titleofthenominatedbook$$", entity.Title);
                applicationform.Replace("$$yearpublished$$", entity.PublishedYear);
                applicationform.Replace("$$isbn$$", entity.Isbn);

                if (entity.NominationType == "A")
                    applicationform.Replace("$$nominationtype$$", "Personal");
                else if (entity.NominationType == "B")
                    applicationform.Replace("$$nominationtype$$", "Publisher");
                else
                    applicationform.Replace("$$nominationtype$$", string.Empty);

                #region Author
                applicationform.Replace("$$authorname$$", entity.AuthorName);

                if (entity.AuthorNationalityId != null && entity.AuthorNationalityId != -1 && entity.AuthorNationalityId != 0)
                    applicationform.Replace("$$authornationality$$", MethodFactory.GetCountryName(entity.AuthorNationalityId ?? -1, langId));
                else
                    applicationform.Replace("$$authornationality$$", string.Empty);

                applicationform.Replace("$$authoremail$$", entity.AuthorEmail);

                if (!string.IsNullOrEmpty(entity.AuthorNumber))
                    applicationform.Replace("$$authornumber$$", entity.AuthorNumber.Replace("$", ""));
                else
                    applicationform.Replace("$$authornumber$$", string.Empty);
                applicationform.Replace("$$passportcopy$$", entity.PassportCopy);
                #endregion

                #region Publisher
                applicationform.Replace("$$publisher$$", entity.Publisher);

                if (entity.PublisherCountryId != null && entity.PublisherCountryId != -1 && entity.PublisherCountryId != 0)
                    applicationform.Replace("$$publishercountry$$", MethodFactory.GetCountryName(entity.PublisherCountryId ?? -1, langId));
                else
                    applicationform.Replace("$$publishercountry$$", string.Empty);

                applicationform.Replace("$$publisheremail$$", entity.PublisherEmail);

                if (!string.IsNullOrEmpty(entity.PublisherNumber))
                    applicationform.Replace("$$publishernumber$$", entity.PublisherNumber.Replace("$", ""));
                else
                    applicationform.Replace("$$publishernumber$$", string.Empty);
                #endregion

                #region Not in use fields
                applicationform.Replace("$$country$$", string.Empty);
                applicationform.Replace("$$city$$", string.Empty);
                applicationform.Replace("$$publisherspecialization$$", string.Empty);
                applicationform.Replace("$$yearestablished$$", string.Empty);
                applicationform.Replace("$$yearoffirstparticipationinsibf$$", string.Empty);
                applicationform.Replace("$$noofpublications$$", string.Empty);
                applicationform.Replace("$$noofpublicationsincurrentyear$$", string.Empty);
                applicationform.Replace("$$pobox$$", string.Empty);
                applicationform.Replace("$$address$$", string.Empty);
                applicationform.Replace("$$phone$$", string.Empty);
                applicationform.Replace("$$fax$$", string.Empty);
                applicationform.Replace("$$email$$", string.Empty);
                applicationform.Replace("$$publisherhouse$$", string.Empty);
                applicationform.Replace("$$generalmanagerofpublisherhouse$$", string.Empty);
                applicationform.Replace("$$date$$", string.Empty);
                #endregion
                #endregion

                fileName = string.Format("{0}_{1}", XsiCurrentLanguageId, "AwardsNominationRegistrationNew_" + MethodFactory.GetRandomNumber());
                Process PDFProcess = GetPDFForAwardRegistrationApplication(applicationform.ToString(), fileName);
                while (true)
                {
                    if (PDFProcess.HasExited)
                    {
                        fileName = Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                                "\\AwardsNominationRegistrationNew\\") + "downloadpdf/" + fileName + ".pdf";
                        break;
                    }
                }
            }
            return fileName;
            #endregion
        }
        Process GetPDFForAwardRegistrationApplication(string html, string strFileName)
        {
            string HtmltopdfPath = _appCustomSettings.HtmltopdfPath; // WebConfigurationManager.AppSettings["HtmltopdfPath"];
            string pdfPath = Path.Combine(_appCustomSettings.ApplicationPhysicalPath + "Content\\uploads\\AwardsNominationRegistrationNew/");
            html = @"<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'><head><title>Application Form</title></head>
                <meta http-equiv='content-Type' content='text/html; charset=utf-8' /><body>" + html + "</body></html>";
            Process PDFProcess = new Process();
            string pdfPath1 = Path.Combine(_appCustomSettings.ApplicationPhysicalPath +
                                           "\\Content\\uploads\\AwardsNominationRegistrationNew\\downloadpdf\\" + strFileName +
                                           ".html");
            System.IO.File.WriteAllText(pdfPath1, html);
            Thread.Sleep(4000);
            if (System.IO.File.Exists(pdfPath1))
            {
                PDFProcess = Process.Start(new ProcessStartInfo
                {
                    FileName = @HtmltopdfPath,
                    Arguments = String.Format("{0} {1}", pdfPath1, pdfPath + "/downloadpdf/" + strFileName + ".pdf"),
                    UseShellExecute = false
                });

                while (true)
                {
                    if (PDFProcess.HasExited)
                    {
                        pdfPath = Path.Combine(_appCustomSettings.ApplicationPhysicalPath +
                                               "\\Content\\uploads\\AwardsNominationRegistrationNew\\downloadpdf\\" + strFileName +
                                               ".pdf");
                        if (System.IO.File.Exists(pdfPath))
                        {
                            if (System.IO.File.Exists(pdfPath1))
                                System.IO.File.Delete(pdfPath1);
                        }
                        break;
                    }
                }
            }
            return PDFProcess;
        }
    }
}

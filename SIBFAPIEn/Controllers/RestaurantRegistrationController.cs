﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using SIBFAPIEn.Utility;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Account.Manage.Internal;
using Contracts;
using LinqKit;
using Microsoft.Extensions.Localization;
using System.IO;
using Xsi.ServicesLayer;
using System.Net;
using Newtonsoft.Json;
using GIGTools;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RestaurantRegistrationController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly IStringLocalizer<RestaurantRegistrationController> _stringLocalizer;
        #region Properties
        string strEmailContentBody { get; set; }
        string strEmailContentEmail { get; set; }
        string strEmailContentSubject { get; set; }
        string strEmailContentAdmin { get; set; }
        #endregion

        public RestaurantRegistrationController(IEmailSender emailSender,
            IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger,
            IStringLocalizer<RestaurantRegistrationController> stringLocalizer)
        {
            _emailSender = emailSender;
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _stringLocalizer = stringLocalizer;
        }
        #region Get Methods
        [HttpGet]
        [Route("Restaurant/{websiteid}/{memberid}")]
        public ActionResult<dynamic> GetExhibitor(long websiteid, long memberid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                string Message = "";
                long MemberId = User.Identity.GetID();
                if (MemberId == memberid)
                {
                    RestaurantRegistrationDTO model = new RestaurantRegistrationDTO();
                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        var exhibition = MethodFactory.GetActiveExhibition(websiteid, _context);
                        if (exhibition != null)
                        {
                            model.ExhibitionId = exhibition.ExhibitionId;
                            if (_context.XsiExhibitionMemberApplicationYearly.Any(i => i.MemberRoleId == 8 && i.MemberId == memberid && i.ExhibitionId == exhibition.ExhibitionId))
                            {
                                DateTime dtnow = MethodFactory.ArabianTimeNow().Date;
                                dtnow = new DateTime(dtnow.Year, dtnow.Month, dtnow.Day, 0, 0, 0);
                                if ((exhibition.RestaurantStartDate <= dtnow && dtnow <= exhibition.RestaurantEndDate) || MethodFactory.ExhibitorInRegistrationProcess(MemberId, websiteid))
                                {
                                    if (MethodFactory.IsValidForRegistrationFromPreviousPayments(exhibition.ExhibitionId, MemberId))
                                    {
                                        if (!MethodFactory.RejectedExhibitor(exhibition.ExhibitionId, MemberId, 8) && !MethodFactory.CancelledExhibitor(exhibition.ExhibitionId, MemberId, 8))
                                        {
                                            model = GetRestaurantView(MemberId, websiteid, exhibition, LangId);
                                            if (model.RestaurantId > 0)
                                                UpdateStatusChange(model.RestaurantId);
                                        }
                                        else
                                        {
                                            model.StepId = 1;
                                            if (LangId == 1)
                                                model.Message = "We regret that we could not confirm your participation for this year. Better luck next time.";
                                            else
                                                model.Message = "نعتذر عن عدم قبول مشاركتكم لهذا العام على أمل أن نلقاكم في الأعوام القادمة.";
                                        }
                                    }
                                    else
                                    {
                                        model.StepId = 1;
                                        if (LangId == 1)
                                            model.Message = "Registration cannot be processed due to pending payments from previous year.";
                                        else
                                            model.Message = "لا يمكنكم استكمال عملية التسجيل نظراً لوجود مستحقات مالية معلقة من الدورة السابقة.";
                                    }
                                }
                                else
                                {
                                    model.StepId = 1;
                                    if (LangId == 1)
                                        model.Message = "Restaurant Registration Date Expired";
                                    else
                                        model.Message = "Restaurant Registration Date Expired";
                                }

                                //CreateInterface(EnumRegistrationViewType.RegistrationExpired);
                            }
                            else if (_context.XsiExhibitionMemberApplicationYearly.Any(i => i.MemberRoleId == 1 && i.MemberId == memberid && i.ExhibitionId == exhibition.ExhibitionId))
                            {
                                model.StepId = 1;
                                if (LangId == 1)
                                    model.Message = "User already registered for this exhibition as Exhibitor.";
                                else
                                    model.Message = "User already registered for this exhibition as Exhibitor.";
                                var dto1 = new { data = model != null ? model : null, message = Message };

                                return Ok(dto1);
                            }
                            else if (_context.XsiExhibitionMemberApplicationYearly.Any(i => i.MemberRoleId == 2 && i.MemberId == memberid && i.ExhibitionId == exhibition.ExhibitionId))
                            {
                                model.StepId = 1;
                                if (LangId == 1)
                                    model.Message = "User already registered for this exhibition as Agency.";
                                else
                                    model.Message = "User already registered for this exhibition as Agency.";
                                var dto1 = new { data = model != null ? model : null, message = Message };

                                return Ok(dto1);
                            }
                            else
                            {
                                DateTime dtnow = MethodFactory.ArabianTimeNow().Date;
                                dtnow = new DateTime(dtnow.Year, dtnow.Month, dtnow.Day, 0, 0, 0);
                                if ((exhibition.RestaurantStartDate <= dtnow && dtnow <= exhibition.RestaurantEndDate) || MethodFactory.ExhibitorInRegistrationProcess(MemberId, websiteid))
                                {
                                    if (MethodFactory.IsValidForRegistrationFromPreviousPayments(exhibition.ExhibitionId, MemberId))
                                    {
                                        model = GetRestaurantView(MemberId, websiteid, exhibition, LangId);
                                        var dto1 = new { data = model != null ? model : null, message = Message };
                                        return Ok(dto1);
                                    }
                                    else
                                    {
                                        model.StepId = 1;
                                        if (LangId == 1)
                                            model.Message = "Registration cannot be processed due to pending payments from previous year.";
                                        else
                                            model.Message = "لا يمكنكم استكمال عملية التسجيل نظراً لوجود مستحقات مالية معلقة من الدورة السابقة.";
                                    }
                                }
                                else
                                {
                                    model.StepId = 1;
                                    if (LangId == 1)
                                        model.Message = "Restaurant Registration Date Expired";
                                    else
                                        model.Message = "Restaurant Registration Date Expired";
                                }
                            }
                        }
                        else
                        {
                            model.StepId = 1;
                            if (LangId == 1)
                                model.Message = "No exhibition available";
                            else
                                model.Message = "No exhibition available";
                        }
                    }
                    var dto = new { data = model != null ? model : null, message = Message };

                    return Ok(dto);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }
        //[HttpGet]
        //[Route("StartRegistration/{websiteid}/{memberid}")]
        //public ActionResult<dynamic> GetStartRegistration(long websiteid, long memberid)
        //{
        //    long memberId = User.Identity.GetID();
        //    if (memberId == memberid)
        //    {
        //        // CreateInterface(EnumRegistrationViewType.Step2);
        //        var loadExibitor = LoadStepTwoDTOByMemberId(websiteid, memberId);
        //        var dto = new { loadexibitor = loadExibitor, stepid = 2 };
        //        return Ok(dto);
        //    }
        //    return Unauthorized();
        //}

        [HttpGet]
        [Route("StepTwoEdit")]
        public ActionResult<dynamic> GetStepTwoEdit()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    RestaurantReturnDTO model = new RestaurantReturnDTO();
                    model.StepId = 2;
                    if (LangId == 1)
                        model.Message = "EnableStep2Controls";
                    else
                        model.Message = "EnableStep2Controls";
                    return Ok(model);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }
        [HttpGet]
        [Route("StepThreeNext/{Restaurantid}")]
        public ActionResult<dynamic> GetStepThreeNext(long Restaurantid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    RestaurantReturnDTO model = new RestaurantReturnDTO();
                    model.StepId = 4;
                    if (LangId == 1)
                        model.Message = "DisableStep3Controls";
                    else
                        model.Message = "DisableStep3Controls";
                    return Ok(model);
                    //CreateInterface(EnumRegistrationViewType.Step4);
                    //DisableStep3Controls();
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpGet]
        [Route("StepThreeEdit/{Restaurantid}")]
        public ActionResult<dynamic> GetStepThreeEdit(long Restaurantid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    var currentRestaurant = GetCurrentRestaurantDetails(Restaurantid);
                    var dto = new { currentRestaurant = currentRestaurant, stepid = 3, message = "ShowStep3Submit" };
                    return Ok(dto);
                    //EnableStep3Controls();
                    // ShowStep3Submit();
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }
        [HttpGet]
        [Route("StepThreeBack")]
        public ActionResult<dynamic> GetStepThreeBack()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    RestaurantReturnDTO model = new RestaurantReturnDTO();
                    model.StepId = 2;
                    if (LangId == 1)
                        model.Message = "DisableStep2Controls";
                    else
                        model.Message = "DisableStep2Controls";
                    return Ok(model);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }
        [HttpGet]
        [Route("StepThreeThankyouBack/{Restaurantid}")]
        public ActionResult<dynamic> GetStepThreeThankyouBack(long Restaurantid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    //dvRequiredAreaContainer.Visible = true;
                    // CreateInterface(EnumRegistrationViewType.Step3);
                    //BindDropDowns();
                    bool ShowStep3UnderProcess = false;
                    bool EnableStep3Submit = false;
                    var step3Dta = LoadStepThreeContent(Restaurantid, LangId);
                    var currentRestaurant = GetCurrentRestaurantDetails(Restaurantid);

                    if (step3Dta.CurrentEditRequest == EnumConversion.ToString(EnumExhibitorEditStatus.New))
                        ShowStep3UnderProcess = true;
                    else
                        EnableStep3Submit = true;
                    var dto = new { step3data = step3Dta, currentRestaurant = currentRestaurant, showstep3underprocess = ShowStep3UnderProcess, enablestep3submit = EnableStep3Submit };
                    return Ok(dto);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }

        }

        [HttpGet]
        [Route("StepFourBack/{Restaurantid}")]
        public ActionResult<dynamic> GetStepFourBack(long Restaurantid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    bool ShowStep3UnderProcess = false;
                    bool EnableStep3Submit = false;
                    var step3Dta = LoadStepThreeContent(Restaurantid, LangId);
                    var currentRestaurant = GetCurrentRestaurantDetails(Restaurantid);

                    if (step3Dta.CurrentEditRequest == EnumConversion.ToString(EnumExhibitorEditStatus.New))
                        ShowStep3UnderProcess = true;
                    else
                        EnableStep3Submit = true;
                    var dto = new { step3data = step3Dta, currentRestaurant = currentRestaurant, showstep3underprocess = ShowStep3UnderProcess, enablestep3submit = EnableStep3Submit, message = "DisableStep3Controls" };
                    return Ok(dto);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }
        [HttpGet]
        [Route("StepFiveThankyouBack/{Restaurantid}")]
        public ActionResult<dynamic> GetStepFiveThankyouBack(long Restaurantid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    return Ok("Step5");
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
            // CreateInterface(EnumRegistrationViewType.Step5);
        }
        [HttpGet]
        [Route("StepFiveBack/{Restaurantid}")]
        public ActionResult<dynamic> GetStepFiveBack(long Restaurantid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    //CreateInterface(EnumRegistrationViewType.Step4);
                    RestaurantReturnDTO model = new RestaurantReturnDTO();
                    model.StepId = 4;
                    if (LangId == 1)
                        model.Message = "Step4";
                    else
                        model.Message = "Step4";
                    return Ok(model);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }

        }

        [HttpGet]
        [Route("NotesRead/{Restaurantid}")]
        public ActionResult<dynamic> GetNotesRead(long Restaurantid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    using (RestaurantRegistrationService RestaurantRegistrationService = new RestaurantRegistrationService())
                    {
                        var restaurant = RestaurantRegistrationService.GetRestaurantRegistrationByItemId(Restaurantid);
                        if (restaurant != null)
                        {
                            restaurant.IsNotesRead = EnumConversion.ToString(EnumBool.Yes);
                            RestaurantRegistrationService.UpdateRestaurantRegistration(restaurant);
                            return Ok(true);
                        }
                    }
                    return Ok(false);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok(false);
                //return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }
        #endregion
        #region Post Method
        [HttpPost]
        [Route("StepThree")]
        public ActionResult<dynamic> PostStepThree(RestaurantSaveDataForStepTwoAndThreeDTO model)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId == model.MemberId)
                {
                    RestaurantRegistrationDTO returnModel = new RestaurantRegistrationDTO();
                    bool validate = true;

                    if (validate)
                    {
                        long langId = 1;
                        long.TryParse(Request.Headers["LanguageURL"], out langId);
                        if (model.RestaurantId <= 0)
                        {
                            model.RestaurantId = SaveRestaurantRegistration(model, langId);
                            model.MemberId = memberId;
                            model.WebsiteId = model.WebsiteId;
                        }
                        else
                        {
                            var CurrentRestaurantEntity = GetCurrentRestaurantDetails(model.RestaurantId);
                            if (CurrentRestaurantEntity != null)
                            {
                                if (CurrentRestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.New))
                                {
                                    UpdateRestaurantRegistration(string.Empty, model);
                                    if (CurrentRestaurantEntity.Email != model.Email)
                                        NotifyAdminandUserAboutEmailChange(CurrentRestaurantEntity.MemberExhibitionYearlyId, CurrentRestaurantEntity.Email, model.Email, model.MemberId, model.WebsiteId);
                                }
                                else
                                {
                                    if (IsSpaceDependentFieldChanged(CurrentRestaurantEntity, model))
                                    {
                                        //if (CurrentRestaurantEntity.RequiredAreaShapeType != ddlAreaShape.SelectedValue || CurrentRestaurantEntity.RequiredAreaId.ToString() != ddlRequiredArea.SelectedValue)
                                        //    UpdateRegistrationInvoice ();
                                        UpdateRestaurantRegistration(EnumConversion.ToString(EnumExhibitorEditStatus.New), model);
                                        //EnableEditReqMsgOnStep3thankyou();
                                    }
                                    else
                                        UpdateRestaurantRegistration(string.Empty, model);
                                    if (IsAnyFieldChanged(CurrentRestaurantEntity, model))
                                        SendEmailBasedOnEdit(CurrentRestaurantEntity, model.MemberId, model.WebsiteId, model.RestaurantId, langId);
                                }
                            }
                        }
                        //var meesage = @"<p>Your Exhibition Registration has been submitted. </p><p>We'll get back to you soon.</p>";
                        if (model.RestaurantId > 0)
                        {

                            var message = string.Empty;
                            if (LangId == 1)
                                message = @"Your Exhibition Registration has been submitted. We'll get back to you soon.";
                            else
                                message = @"قد تم إستلام طلبك للتسجيل في المعرض. وسوف يتم الرد عليكم قريباً";
                            return Ok(ShowMessage(message, 3, model.RestaurantId, model.MemberId, model.WebsiteId));
                        }
                        else
                        {
                            var message = string.Empty;
                            if (LangId == 1)
                                message = @"Something went wrong. Please retry again after some time.";
                            else
                                message = @"Something went wrong. Please retry again after some time.";

                            return Ok(ShowMessage(message, 3, model.RestaurantId, model.MemberId, model.WebsiteId));
                        }
                    }
                    else
                    {
                        var message = string.Empty;
                        //if (LangId == 1)
                        //    message = _stringLocalizer["NewTitleShouldBeLessThanTotalOfTitles"];
                        //else
                        //    message = "عدد العناوين الجديدة يجب أن يكون أقل من عدد العناوين الكلية";
                        if (LangId == 1)
                            message = "Total number of new titles should be less than total number of titles";//_stringLocalizer["NewTitleShouldBeLessThanTotalOfTitles"];
                        else
                            message = "يجب ان يكون عدد العناوين الجديدة اقل من العدد الاجمالي للعناوين";//"عدد العناوين الجديدة يجب أن يكون أقل من عدد العناوين الكلية";
                        return Ok(ShowMessage(message, 3, model.RestaurantId, model.MemberId, model.WebsiteId));
                    }

                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Restaurant Registration StepThree action: {ex.InnerException}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        //[HttpPost]
        //[Route("ExhibitingDetailsStepTwo")]
        //public ActionResult<dynamic> PostExhibitingDetailsStepTwo(StepTwoDTO model, long memberID)
        //{
        //    long memberId = User.Identity.GetID();
        //    if (memberId == memberID)
        //    {
        //        long langId = 1;
        //        long.TryParse(Request.Headers["LanguageURL"], out langId);
        //        if (IsTradeLicenceValid(model, langId))
        //        {
        //            if (IsLogoValid(model, langId))
        //                MoveToStep3(model, langId);
        //        }
        //    }
        //    return Unauthorized();
        //}

        [HttpPost]
        [Route("StepFour")]
        public ActionResult<dynamic> PostStepFour()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    RestaurantReturnDTO model = new RestaurantReturnDTO();
                    model.StepId = 5;
                    if (LangId == 1)
                        model.Message = "HideError";
                    else
                        model.Message = "HideError";
                    return Ok(model);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }

            //HideError(lblMessageStep5, pnlErrorMsgStep5);
            //CreateInterface(EnumRegistrationViewType.Step5);

        }
        [HttpPost]
        [Route("StepFive")]
        public ActionResult<dynamic> PostStepFive(RestaurantUploadPayment model)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);
                long memberId = User.Identity.GetID();
                if (memberId == model.MemberId)
                {
                    if (!string.IsNullOrWhiteSpace(model.TRNNumber))
                    {
                        using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
                        {
                            var Restaurant = new XsiExhibitionMemberApplicationYearly();
                            Restaurant = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(model.RestaurantId);
                            Restaurant.Trn = model.TRNNumber;
                            Restaurant.ModifiedOn = MethodFactory.ArabianTimeNow();
                            ExhibitorRegistrationService.UpdateExhibitorRegistration(Restaurant);
                        }
                    }
                    if (model.FileBase64 != null && model.FileBase64.Length > 0)
                    {
                        var message = UploadReceipt(model, langId);// TODO
                                                                   //else
                                                                   //    ShowMessage(Resources.BackendError.InvalidFileFormat, EnumMessageType.Error, lblMessageStep5,
                                                                   //        pnlErrorMsgStep5, liMessage5);

                        if (langId == 1)
                            return Ok(ShowMessage(message, 5, model.RestaurantId, model.MemberId, model.WebsiteId));
                        else
                            return Ok(ShowMessage(message, 5, model.RestaurantId, model.MemberId, model.WebsiteId));
                    }
                    else
                    {
                        if (langId == 1)
                            return Ok(ShowMessage("UploadFieldMissing", 5, model.RestaurantId, model.MemberId, model.WebsiteId));
                        else
                            return Ok(ShowMessage("تحميل الحقول المفقودة، الرجاء تحميل الملف", 5, model.RestaurantId, model.MemberId, model.WebsiteId));
                    }
                }
                return Unauthorized();
                //ShowMessage(Resources.BackendError.UploadFieldMissing, EnumMessageType.Error, lblMessageStep5, pnlErrorMsgStep5, liMessage5);
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpPost]
        [Route("Proceed")]
        public ActionResult<dynamic> PostProceed(RestaurantCancelRegistrationDTO dto)
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);

            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long memberId = User.Identity.GetID();
                var exhibition = MethodFactory.GetActiveExhibition(dto.WebsiteId, _context);
                if (exhibition != null)
                {
                    if (MethodFactory.ArabianTimeNow() < exhibition.DateOfDue)
                    {
                        using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
                        {
                            XsiExhibitionMemberApplicationYearly entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(dto.RestaurantId);
                            if (entity != null)
                            {
                                entity.Status = EnumConversion.ToString(EnumExhibitorStatus.Cancelled);
                                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                                if (ExhibitorRegistrationService.UpdateExhibitorRegistration(entity) == EnumResultType.Success)
                                {
                                    var details = ExhibitorRegistrationService.GetDetailsById(dto.RestaurantId);
                                    if (details != null && dto.CancellationReason != null)
                                    {
                                        details.CancellationReason = dto.CancellationReason.Trim();
                                        details.ModifiedOn = MethodFactory.ArabianTimeNow();
                                        ExhibitorRegistrationService.UpdateExhibitorRegistrationDetails(details);
                                    }
                                    //send email to user notifying abt the cancellation
                                    SendEmail(entity, memberId, dto.WebsiteId, langId);

                                    LogRestaurantStatus(EnumConversion.ToString(EnumExhibitorStatus.Cancelled), entity.MemberExhibitionYearlyId);
                                    MethodFactory.InactivateBooksParticipating(entity.MemberExhibitionYearlyId);
                                    // RestaurantId = -1;
                                    //redirect to dashboard
                                    //string pageReferrer = "/en/MemberDashboard";
                                    //Response.Redirect(pageReferrer);

                                    return Ok(ShowMessage("true", 2, entity.MemberExhibitionYearlyId, memberId, dto.WebsiteId));
                                }

                                if (langId == 1)
                                    return Ok(ShowMessage("Cancellation Failure, Retry again or contact admin.", 2, entity.MemberExhibitionYearlyId, memberId, dto.WebsiteId));
                                else
                                    return Ok(ShowMessage("Cancellation Failure, Retry again or contact admin.", 2, entity.MemberExhibitionYearlyId, memberId, dto.WebsiteId));
                            }
                        }
                    }
                    //else
                    //{
                    //    if (MVRegistration.ActiveViewIndex == 2)
                    //        ShowMessage(Resources.BackendError.DateExpired, EnumMessageType.Info, lblMessageStep3, pnlErrorMsgStep3, liMessage3);
                    //    else if (MVRegistration.ActiveViewIndex == 3)
                    //        ShowMessage(Resources.BackendError.DateExpired, EnumMessageType.Info, lblMessageStep4, pnlErrorMsgStep4, liMessage4);
                    //    else if (MVRegistration.ActiveViewIndex == 4)
                    //        ShowMessage(Resources.BackendError.DateExpired, EnumMessageType.Info, lblMessageStep5, pnlErrorMsgStep5, liMessage5);
                    //    else if (MVRegistration.ActiveViewIndex == 5)
                    //        ShowMessage(Resources.BackendError.DateExpired, EnumMessageType.Info, lblMessageStep6, pnlErrorMsgStep6, liMessage6);
                    //}
                }

                if (langId == 1)
                    return Ok(ShowMessage(_stringLocalizer["DateExpired"], 3, dto.RestaurantId, memberId, dto.WebsiteId));
                else
                    return Ok(ShowMessage(_stringLocalizer["DateExpired"], 3, dto.RestaurantId, memberId, dto.WebsiteId));
            }
        }

        #endregion
        #region Helper Methods
        private RestaurantRegistrationDTO GetRestaurantView(long memberId, long websiteId, XsiExhibition exhibition, long LangId = 1)
        {
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {
                RestaurantRegistrationDTO model = new RestaurantRegistrationDTO();
                long RestaurantId = 0;
                bool IsFirstTime = false;

                model.MemberId = memberId;
                model.WebsiteId = websiteId;
                model.ExhibitionId = exhibition.ExhibitionId;
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    if (exhibition != null)
                    {
                        model.IsNotesRead = EnumConversion.ToString(EnumBool.No);
                        model.ExhibitionId = exhibition.ExhibitionId;
                        ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                        XsiExhibitionMemberApplicationYearly RestaurantEntity = null;
                        XsiInvoice InvoiceEntity = new XsiInvoice();
                        string strExhbitorCancelled = EnumConversion.ToString(EnumExhibitorStatus.Cancelled);
                        string strExhbitorReject = EnumConversion.ToString(EnumExhibitorStatus.Rejected);
                        model.StepId = 1;

                        RestaurantEntity = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly { MemberRoleId = 8, MemberId = memberId, ExhibitionId = exhibition.ExhibitionId, IsActive = EnumConversion.ToString(EnumBool.Yes) }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                        if (RestaurantEntity == null)
                        {
                            model.StepId = 1;
                            IsFirstTime = true;
                            model.Message = "";
                            model.Status = EnumConversion.ToString(EnumExhibitorStatus.New);
                            //.Where(p => p.Status == strExhbitorReject && p.Status == strExhbitorCancelled)
                            // RestaurantEntity = ExhibitorRegistrationService.GetRestaurantRegistration(new XsiExhibitionMemberApplicationYearly { MemberId = memberId, ExhibitionId = exhibition.ExhibitionId, IsActive = EnumConversion.ToString(EnumBool.Yes) }, EnumSortlistBy.ByItemIdDesc).Where(p => p.Status != strExhbitorReject && p.Status != strExhbitorCancelled).FirstOrDefault();
                        }
                        else if (RestaurantEntity != null)
                        {
                            if (!string.IsNullOrEmpty(RestaurantEntity.IsNotesRead))
                                model.IsNotesRead = RestaurantEntity.IsNotesRead;

                            model.Status = RestaurantEntity.Status;

                            if (RestaurantEntity.Status != strExhbitorReject && RestaurantEntity.Status != strExhbitorCancelled)
                            {

                                RestaurantId = RestaurantEntity.MemberExhibitionYearlyId;
                                model.RestaurantId = RestaurantId;
                                IsFirstTime = false;
                            }
                            else
                            {
                                model.MemberId = memberId;
                                model.WebsiteId = exhibition.WebsiteId.Value;

                                if (LangId == 1)
                                    model.Message = "User already participated for this exhibition.";
                                else
                                    model.Message = "User already participated for this exhibition.";
                                return model;
                            }
                        }
                        else
                        {
                            model.MemberId = memberId;
                            model.WebsiteId = exhibition.WebsiteId.Value;

                            if (LangId == 1)
                                model.Message = "User already participated for this exhibition.";
                            else
                                model.Message = "User already participated for this exhibition.";
                            return model;
                        }

                        if (IsFirstTime)
                        {
                            model.StepId = 1;
                            if (MethodFactory.GetAgencyRegistered(memberId, websiteId, _context) == null)
                            {
                                var loadExibitor = LoadStepTwoDTOByMemberId(websiteId, memberId);
                                if (LangId == 1)
                                    model.Message = "";
                                else
                                    model.Message = "";
                                model.StepTwoDTO = loadExibitor;
                            }
                            else
                            {
                                if (LangId == 1)
                                    model.Message = "Registered As Agent";
                                else
                                    model.Message = "Registered As Agent";
                            }
                        }
                        else
                        {
                            if (RestaurantEntity != null)
                            {
                                model.Status = RestaurantEntity.Status;
                                RestaurantId = RestaurantEntity.MemberExhibitionYearlyId;
                                model.RestaurantId = RestaurantId;

                                if (RestaurantEntity.IsEditRequest == EnumConversion.ToString(EnumExhibitorEditStatus.New))
                                {
                                    model.StepId = 3;
                                    if (LangId == 1)
                                        model.Message = "Your modification request has been sent and your modified data will be reviewed later.";
                                    else
                                        model.Message = "تم إرسال طلب التعديل وسيتم مراجعة بياناتكم والرد عليكم لاحقا";
                                }
                                else
                                {
                                    #region Status Based View
                                    if (RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.New) || RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.SampleReceived) || RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval) || RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingApproval) || RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingDocumentation))
                                    {
                                        model.StepId = 3;
                                        if (RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval))
                                        {
                                            model.StepId = 3;
                                            model.StepTwoDTO = LoadStepTwoContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                            model.StepThreeDTO = LoadStepThreeContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                            model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                            model.StepFiveDTO = LoadStepFiveContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                            model.StepSixDTO = LoadStepSixContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);

                                            if (LangId == 1)
                                                model.Message = @"<p>Your Exhibition Registration has been approved.</p><p>You will be notified shortly regarding payment. Only after receiving the payment as per the payment rules, your participation will be confirmed.</p>";
                                            else
                                            {
                                                if (websiteId == 1)
                                                    model.Message = @"<p>تمت الموافقة على تسجيلكم في المعرض.</p><p>سيتم إخطاركم قريبًا بشأن الدفع. بعد استلام المبلغ وفقًا لقوانين الدفع، سيتم تأكيد مشاركتكم</p>";
                                                else
                                                    model.Message = @"<p>تمت الموافقة على تسجيلكم في المهرجان.</p><p>سيتم إخطاركم قريبًا بشأن الدفع. بعد استلام المبلغ وفقًا لقوانين الدفع، سيتم تأكيد مشاركتكم</p>";
                                            }

                                            /*
                                            #region To work Later 
                                            string strCancelled = EnumConversion.ToString(EnumExhibitorStatus.Cancelled);
                                            if (LangId == 1)
                                                model.Message = "Step3Thankyou";
                                            else
                                                model.Message = "Step3Thankyou";
                                            #region Check Invoice Table
                                            InvoiceService InvoiceService = new InvoiceService();
                                            XsiInvoice where = new XsiInvoice();
                                            where.ExhibitorId = RestaurantId;
                                            where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                            where.IsRegistrationInvoice = EnumConversion.ToString(EnumBool.Yes);
                                            InvoiceEntity = InvoiceService.GetInvoice(where, EnumSortlistBy.ByItemIdDesc).Where(p => p.Status != strCancelled).OrderBy(o => o.ItemId).FirstOrDefault();
                                           // InvoiceEntity = null;
                                            if (InvoiceEntity != null)
                                            {
                                                model.StepId = 4;
                                                if (InvoiceEntity.Status != null)
                                                {
                                                    if (InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.PendingPayment))
                                                    {
                                                        var InvoiceId = InvoiceEntity.ItemId;
                                                        //EnableUpload(InvoiceEntity);
                                                        if (LangId == 1)
                                                            model.Message = "EnableUpload";
                                                        else
                                                            model.Message = "EnableUpload";


                                                        //var payment = LoadStepFourContent(InvoiceEntity, exhibition);
                                                        //payment.InvoiceId = InvoiceId;

                                                        model.StepTwoDTO = LoadStepTwoContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                        model.StepThreeDTO = LoadStepThreeContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                        model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                        model.StepFiveDTO = LoadStepFiveContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                        model.StepSixDTO = LoadStepSixContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                    }
                                                    else if (InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.ReceiptUploaded) || InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.ReceiptReviewed) || InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.Paid) || InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.PartiallyPaid))
                                                    {
                                                        model.StepId = 5;
                                                        // || InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.PaymentInProcess) -- ReceiptReviewed
                                                        var InvoiceId = InvoiceEntity.ItemId;
                                                        //EnableUpload(InvoiceEntity);
                                                        string strtypemessage = string.Empty;
                                                        if (InvoiceEntity.PaymentTypeId == EnumConversion.ToLong(EnumPaymentType.PaidByCash))
                                                        {

                                                        }
                                                        else if (InvoiceEntity.PaymentTypeId == EnumConversion.ToLong(EnumPaymentType.PaidByBankTransfer))
                                                        {
                                                        }
                                                        else if (InvoiceEntity.PaymentTypeId == EnumConversion.ToLong(EnumPaymentType.PaidByChequeTransfer))
                                                        {
                                                            if (LangId == 1)
                                                                strtypemessage = "Note : Your cheque has been received by SIBF.You will be notified via email once payment has been approved.";
                                                            else
                                                                strtypemessage = "ملاحظة: تم إستلام الشيك الخاص بك من قبل إدارة المعرض. سيتم إعلامك بعد صرف الشيك";
                                                        }
                                                        else if (InvoiceEntity.PaymentTypeId == EnumConversion.ToLong(EnumPaymentType.PaidByOnline))
                                                        {
                                                        }


                                                        if (InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.ReceiptUploaded))
                                                        {
                                                            if (LangId == 1)
                                                                model.Message = "Note : You will be notified via email after the payments are approved by accounting department.";
                                                            else
                                                                model.Message = "ملاحظة: سيتم إعلامك بعد أن يتم إعتماد المبلغ من قبل قسم الحسابات";
                                                        }
                                                        else if (InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.ReceiptReviewed))
                                                        {
                                                            if (LangId == 1)
                                                                model.Message = "Receipt correct and waiting to be in our account.";
                                                            else
                                                                model.Message = "Receipt correct and waiting to be in our account.";

                                                            if (InvoiceEntity.IsChequeReceived == EnumConversion.ToString(EnumBool.Yes))
                                                            {
                                                                if (LangId == 1)
                                                                    model.Message = strtypemessage;
                                                                else
                                                                    model.Message = strtypemessage;
                                                            }
                                                        }
                                                        else if (InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.Paid))
                                                        {
                                                            if (LangId == 1)
                                                                model.Message = "Note: Your payment is approved by the accounting department. Your stand details will be sent shortly.";
                                                            else
                                                                model.Message = "ملاحظة: تم إعتماد المبلغ من قبل قسم الحسابات. سيتم إرسال تفاصيل الجناح الخاص بكم قريبا";
                                                        }
                                                        else
                                                        {
                                                            if (LangId == 1)
                                                                model.Message = "Note: You will be notified via email after the payments are approved by accounting department.";
                                                            else
                                                                model.Message = "ملاحظة: سيتم إعلامك بعد أن يتم إعتماد المبلغ من قبل قسم الحسابات";
                                                        }

                                                        model.StepTwoDTO = LoadStepTwoContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                        model.StepThreeDTO = LoadStepThreeContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                        model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                        model.StepFiveDTO = LoadStepFiveContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                        model.StepSixDTO = LoadStepSixContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                        //  MVRegistration.SetActiveView(Step5Thankyou);
                                                    }
                                                    else
                                                    {
                                                        model.StepId = 3;
                                                        model.Message = @"<p>Your Exhibition Registration has been approved.</p><p>You will be notified shortly regarding payment. Only after receiving the payment as per the payment rules, your participation will be confirmed.</p>";
                                                        model.StepTwoDTO = LoadStepTwoContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                        model.StepThreeDTO = LoadStepThreeContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                        model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                        model.StepFiveDTO = LoadStepFiveContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                        model.StepSixDTO = LoadStepSixContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                        //  MVRegistration.SetActiveView(Step3Thankyou);
                                                        //  NewStatusMsg.Visible = true;
                                                        // EnableInitialApprovalStatusMessage();
                                                    }
                                                }
                                                else
                                                {
                                                    model.StepId = 3;
                                                    model.StepTwoDTO = LoadStepTwoContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                    model.StepThreeDTO = LoadStepThreeContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                    model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                    model.StepFiveDTO = LoadStepFiveContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                    model.StepSixDTO = LoadStepSixContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);

                                                    if (LangId == 1)
                                                        model.Message = @"<p>Your Exhibition Registration has been submitted.</p><p>We'll get back to you soon.</p>";
                                                    else
                                                        model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المعرض.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                                }
                                            }
                                            else
                                            {
                                                model.StepId = 3;
                                                model.StepTwoDTO = LoadStepTwoContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepThreeDTO = LoadStepThreeContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                model.StepFiveDTO = LoadStepFiveContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepSixDTO = LoadStepSixContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                if (LangId == 1)
                                                    model.Message = @"<p>Your Exhibition Registration has been submitted.</p><p>We'll get back to you soon.</p>";
                                                else
                                                    model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المعرض.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                            }
                                            #endregion
                                            #endregion
                                            */
                                        }
                                        else
                                        {
                                            if (LangId == 1)
                                                model.Message = @"<p>Your Exhibition Registration has been submitted.</p><p>We'll get back to you soon.</p>";
                                            else
                                                model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المعرض.</p><p>وسوف يتم الرد عليكم قريباً</p>";


                                            model.StepId = 3;
                                            if (RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.New) || RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.SampleReceived) || RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingApproval))
                                            {
                                                if (LangId == 1)
                                                    model.Message = @"<p>Your Exhibition Registration has been submitted.</p><p>We'll get back to you soon.</p>";
                                                else
                                                    model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المعرض.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                            }
                                            else
                                            {
                                                if (LangId == 1)
                                                    model.Message = @"<p>Your Exhibition Registration has been submitted. </p><p>Please contact us by email or phone to know more about the pending formalities & documentations.</p>";
                                                else
                                                    model.Message = @"<p>تم تقديم طلبك للتسجيل في المعرض للإدارة.</p><p>الرجاء مراجعة بريدك الإلكتروني أو الإتصال بإدارة المعرض بخصوص الوثائق المطلوب تقديمها.</p>";
                                            }
                                            model.StepTwoDTO = LoadStepTwoContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                            model.StepThreeDTO = LoadStepThreeContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                            model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                            model.StepFiveDTO = LoadStepFiveContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                            model.StepSixDTO = LoadStepSixContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                        }
                                    }
                                    else if (RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved))
                                    {
                                        model.StepId = 6;
                                        model.StepTwoDTO = LoadStepTwoContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                        model.StepThreeDTO = LoadStepThreeContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                        model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                        model.StepFiveDTO = LoadStepFiveContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                        model.StepSixDTO = LoadStepSixContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                    }
                                    else
                                    {
                                        if (RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.New) || RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingDocumentation))
                                        {
                                            model.StepId = 2;
                                            if (model.RestaurantId > 0)
                                            {
                                                model.StepTwoDTO = LoadStepTwoContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepThreeDTO = LoadStepThreeContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                model.StepFiveDTO = LoadStepFiveContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepSixDTO = LoadStepSixContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                            }
                                            else
                                                model.StepTwoDTO = LoadStepTwoDTOByMemberId(websiteId, memberId);
                                        }
                                        else
                                        {
                                            model.StepId = 3;

                                            if (LangId == 1)
                                                model.Message = @"<p>Your Exhibition Registration has been submitted.</p><p>We'll get back to you soon.</p>";
                                            else
                                                model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المعرض.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                            if (model.RestaurantId > 0)
                                            {
                                                model.StepTwoDTO = LoadStepTwoContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepThreeDTO = LoadStepThreeContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                model.StepFiveDTO = LoadStepFiveContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepSixDTO = LoadStepSixContent(RestaurantEntity.MemberExhibitionYearlyId, LangId);
                                            }
                                            else
                                                model.StepTwoDTO = LoadStepTwoDTOByMemberId(websiteId, memberId);
                                        }

                                    }
                                    #endregion
                                }
                            }

                            model.MemberId = memberId;
                            model.WebsiteId = exhibition.WebsiteId.Value;

                            return model;
                        }
                    }
                }
                return model;
            }
        }
        private RestaurantStepTwoDTO LoadStepTwoContent(long RestaurantId, long langid = 1)
        {
            RestaurantStepTwoDTO dto = new RestaurantStepTwoDTO();
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(RestaurantId);
                XsiExhibitionExhibitorDetails detail = ExhibitorRegistrationService.GetDetailsById(RestaurantId);
                if (entity != null)
                {
                    //if (entity.IsEditRequest != null)
                    //    dto.CurrentEditRequest = entity.IsEditRequest;

                    if (entity.PublisherNameEn != null)
                        dto.PublisherNameEn = entity.PublisherNameEn;
                    if (entity.PublisherNameAr != null)
                        dto.PublisherNameAr = entity.PublisherNameAr;

                    #region Country and city
                    if (entity.CountryId != null)
                    {
                        dto.CountryId = entity.CountryId.Value;
                    }
                    if (entity.CityId != null)
                    {
                        dto.CityId = entity.CityId.Value;
                    }
                    if (entity.OtherCity != null)
                    {
                        dto.OtherCity = entity.OtherCity;
                    }
                    #endregion

                    if (entity.ContactPersonName != null)
                        dto.ContactPersonName = entity.ContactPersonName;
                    if (entity.ContactPersonNameAr != null)
                        dto.ContactPersonNameAr = entity.ContactPersonNameAr;
                    if (entity.ContactPersonTitle != null)
                        dto.ContactPersonTitle = entity.ContactPersonTitle;
                    if (entity.ContactPersonTitleAr != null)
                        dto.ContactPersonTitleAr = entity.ContactPersonTitleAr;

                    #region Phone,Mobile,Fax
                    if (entity.Phone != null)
                    {
                        string[] str = entity.Phone.Split('$');
                        if (str.Count() == 3)
                        {
                            dto.PhoneISD = str[0].Trim();
                            dto.PhoneSTD = str[1].Trim();
                            dto.Phone = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            dto.PhoneSTD = str[0].Trim();
                            dto.Phone = str[1].Trim();
                        }
                        else
                            dto.Phone = str[0].Trim();
                    }
                    if (entity.Mobile != null)
                    {
                        string[] str = entity.Mobile.Split('$');
                        if (str.Count() == 3)
                        {
                            dto.MobileISD = str[0].Trim();
                            dto.MobileSTD = str[1].Trim();
                            dto.Mobile = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            dto.MobileSTD = str[0].Trim();
                            dto.Mobile = str[1].Trim();
                        }
                        else
                            dto.Mobile = str[0].Trim();
                    }
                    if (entity.Fax != null)
                    {
                        string[] str = entity.Fax.Split('$');
                        if (str.Count() == 3)
                        {
                            dto.FaxISD = str[0].Trim();
                            dto.FaxSTD = str[1].Trim();
                            dto.Fax = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            dto.FaxSTD = str[0].Trim();
                            dto.Fax = str[1].Trim();
                        }
                        else
                            dto.Fax = str[0].Trim();
                    }
                    #endregion
                    if (entity.Email != null)
                        dto.Email = entity.Email;

                    if (detail != default(XsiExhibitionExhibitorDetails))
                    {
                        if (detail.FileName != null)
                            dto.LogoName = detail.FileName;

                        if (detail != null && detail.TradeLicence != null)
                            dto.TradeLicenceName = detail.TradeLicence;

                        if (detail.StandPhoto != null)
                            dto.StandPhotoName = detail.StandPhoto;

                        if (detail.StandLayoutByMeter != null)
                            dto.StandLayoutByMeterName = detail.StandLayoutByMeter;

                        if (detail.RequiredPowerCapacity != null)
                            dto.RequiredPowerCapacity = detail.RequiredPowerCapacity;
                    }
                }
            }
            return dto;
        }
        private RestaurantStepTwoDTO LoadStepTwoDTOByMemberId(long websiteid, long memberId)
        {
            RestaurantStepTwoDTO dto = new RestaurantStepTwoDTO();
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                XsiExhibitionMember entity = ExhibitionMemberService.GetExhibitionMemberByItemId(memberId);
                if (entity != null)
                {
                    ExhibitionService exhibitionService = new ExhibitionService();
                    var exhibition = exhibitionService.GetExhibition(new XsiExhibition() { IsActive = "Y", IsArchive = "N", WebsiteId = websiteid }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                    //dto.ExhibitionId = exhibition.ExhibitionId;
                    #region Contact Fields
                    if (entity.CompanyName != null)
                        dto.PublisherNameEn = entity.CompanyName;
                    if (entity.CompanyNameAr != null)
                        dto.PublisherNameAr = entity.CompanyNameAr;
                    #region Country and city
                    if (entity.CityId.HasValue)
                    {
                        dto.CityId = entity.CityId.Value;
                        ExhibitionCityService ExhibitionCityService = new ExhibitionCityService();
                        dto.CountryId = ExhibitionCityService.GetExhibitionCityByItemId(entity.CityId.Value).CountryId;
                    }
                    if (entity.OtherCity != null)
                    {
                        dto.OtherCity = entity.OtherCity;
                    }
                    #endregion
                    if (entity.ContactPerson != null)
                        dto.ContactPersonName = entity.ContactPerson;

                    //if (entity.ContactPerson != null)
                    //    dto.ContactPersonNameAr = entity.ContactPerson;

                    if (entity.JobTitle != null)
                        dto.ContactPersonTitle = entity.JobTitle;
                    if (entity.JobTitleAr != null)
                        dto.ContactPersonTitleAr = entity.JobTitleAr;
                    #endregion

                    #region Phone,Mobile,Fax
                    if (entity.Phone != null)
                    {
                        string[] str = entity.Phone.Split('$');
                        if (str.Count() == 3)
                        {
                            dto.PhoneISD = str[0].Trim();
                            dto.PhoneSTD = str[1].Trim();
                            dto.Phone = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            dto.PhoneSTD = str[0].Trim();
                            dto.Phone = str[1].Trim();
                        }
                        else
                            dto.Phone = str[0].Trim();
                    }
                    if (entity.Mobile != null)
                    {
                        string[] str = entity.Mobile.Split('$');
                        if (str.Count() == 3)
                        {
                            dto.MobileISD = str[0].Trim();
                            dto.MobileSTD = str[1].Trim();
                            dto.Mobile = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            dto.MobileSTD = str[0].Trim();
                            dto.Mobile = str[1].Trim();
                        }
                        else
                            dto.Mobile = str[0].Trim();
                    }
                    if (entity.Fax != null)
                    {
                        string[] str = entity.Fax.Split('$');
                        if (str.Count() == 3)
                        {
                            dto.FaxISD = str[0].Trim();
                            dto.FaxSTD = str[1].Trim();
                            dto.Fax = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            dto.FaxSTD = str[0].Trim();
                            dto.Fax = str[1].Trim();
                        }
                        else
                            dto.Fax = str[0].Trim();
                    }
                    #endregion

                    if (entity.Email != null)
                        dto.Email = entity.Email;
                }
            }
            return dto;
        }
        private RestaurantStepThreeDTO LoadStepThreeContent(long RestaurantId, long langid = 1)
        {
            RestaurantStepThreeDTO dto = new RestaurantStepThreeDTO();
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                ExhibitionAreaService ExhibitionAreaService = new ExhibitionAreaService();
                XsiExhibitionMemberApplicationYearly entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(RestaurantId);
                XsiExhibitionExhibitorDetails detail = ExhibitorRegistrationService.GetDetailsById(RestaurantId);
                if (entity != null)
                {
                    if (entity.IsEditRequest != null)
                        dto.CurrentEditRequest = entity.IsEditRequest;

                    #region Section and Sub Section
                    if (detail != default(XsiExhibitionExhibitorDetails))
                    {
                        dto.RequiredAreaShapeType = detail.RequiredAreaType;
                        dto.CalculatedArea = detail.CalculatedArea;
                        if (detail.RequiredAreaId != null)
                            dto.RequiredAreaId = detail.RequiredAreaId.Value;

                        //if (detail.RequiredAreaId != null)
                        //{
                        //    dto.RequiredAreaId = detail.RequiredAreaId.Value;
                        //    dto.RequiredAreaValue = ExhibitionAreaService.GetExhibitionAreaByItemId(detail.RequiredAreaId.Value).Title;
                        //}
                        //if (detail.RequiredAreaType != null)
                        //{
                        //    dto.RequiredAreaShapeType = detail.RequiredAreaType;
                        //    if (detail.RequiredAreaType == EnumConversion.ToString(EnumRequiredAreaType.Open))
                        //    {

                        //        if (detail.CalculatedArea != null)
                        //        {
                        //            dto.CalculatedArea = detail.CalculatedArea;
                        //            dto.RequiredAreaMeter = dto.RequiredArea = detail.CalculatedArea.Split('x')[1];
                        //        }
                        //    }
                        //}
                    }
                    #endregion
                }
            }
            return dto;
        }
        private RestaurantStepFourDTO LoadStepFourContent(XsiInvoice entity, XsiExhibition exhibition, long langid = 1)
        {
            RestaurantStepFourDTO paymentDTO = new RestaurantStepFourDTO();
            if (entity != null)
            {
                paymentDTO.InvoiceId = entity.ItemId;

                if (entity.AllocatedSpace != null && !string.IsNullOrEmpty(entity.AllocatedSpace))
                    paymentDTO.AllocatedSpace = entity.AllocatedSpace;

                if (entity.InvoiceNumber != null && !string.IsNullOrEmpty(entity.InvoiceNumber))
                    paymentDTO.InvoiceNumber = entity.InvoiceNumber;

                if (entity.ParticipationFee != null && !string.IsNullOrEmpty(entity.ParticipationFee))
                    if (entity.ParticipationFee.Trim() != "0")
                        paymentDTO.ParticipationFee = MethodFactory.AddCommasToCurrency(entity.ParticipationFee);

                if (entity.StandFee != null && !string.IsNullOrEmpty(entity.StandFee))
                    paymentDTO.StandFee = MethodFactory.AddCommasToCurrency(entity.StandFee);

                if (entity.KnowledgeAndResearchFee != null && !string.IsNullOrEmpty(entity.KnowledgeAndResearchFee))
                    paymentDTO.KnowledgeAndResearchFee = MethodFactory.AddCommasToCurrency(entity.KnowledgeAndResearchFee);

                if (entity.AgencyFee != null && !string.IsNullOrEmpty(entity.AgencyFee))
                    paymentDTO.AgencyFee = MethodFactory.AddCommasToCurrency(entity.AgencyFee);

                if (entity.RepresentativeFee != null && !string.IsNullOrEmpty(entity.RepresentativeFee))
                    paymentDTO.RepresentativeFee = MethodFactory.AddCommasToCurrency(entity.RepresentativeFee);

                if (entity.DueFromLastYearCredit != null && !string.IsNullOrEmpty(entity.DueFromLastYearCredit))
                    paymentDTO.DueFromLastYearCredit = MethodFactory.AddCommasToCurrency(entity.DueFromLastYearCredit);

                if (entity.DueFromLastYearDebit != null && !string.IsNullOrEmpty(entity.DueFromLastYearDebit))
                    paymentDTO.DueFromLastYearDebit = MethodFactory.AddCommasToCurrency(entity.DueFromLastYearDebit);

                if (entity.DueCurrentYear != null && !string.IsNullOrEmpty(entity.DueCurrentYear))
                    paymentDTO.DueCurrentYear = MethodFactory.AddCommasToCurrency(entity.DueCurrentYear);

                if (entity.DebitedCurrentYear != null && !string.IsNullOrEmpty(entity.DebitedCurrentYear))
                    paymentDTO.DebitedCurrentYear = MethodFactory.AddCommasToCurrency(entity.DebitedCurrentYear);

                if (entity.Discount != null && !string.IsNullOrEmpty(entity.Discount))
                    paymentDTO.Discount = MethodFactory.AddCommasToCurrency(entity.Discount);

                if (entity.Penalty != null && !string.IsNullOrEmpty(entity.Penalty))
                    paymentDTO.Penalty = MethodFactory.AddCommasToCurrency(entity.Penalty);

                if (entity.Vat != null && !string.IsNullOrEmpty(entity.Vat))
                    paymentDTO.VAT = MethodFactory.AddCommasToCurrency(entity.Vat);

                if (entity.TotalAed != null && !string.IsNullOrEmpty(entity.TotalAed))
                    paymentDTO.TotalAED = MethodFactory.AddCommasToCurrency(entity.TotalAed);

                if (entity.TotalUsd != null && !string.IsNullOrEmpty(entity.TotalUsd))
                    paymentDTO.TotalUSD = MethodFactory.AddCommasToCurrency(entity.TotalUsd);

                if (entity.FileName != null && !string.IsNullOrEmpty(entity.FileName))
                    paymentDTO.FileName = _appCustomSettings.UploadsCMSPath + "/ExhibitorRegistration/Invoice/" + entity.FileName;

                if (exhibition.DateOfDue != null)
                {
                    if (langid == 1)
                        paymentDTO.RestaurantLastDate = exhibition.DateOfDue.Value.ToString("dddd d MMMM yyyy");
                    else
                        paymentDTO.RestaurantLastDate = exhibition.DateOfDue.Value.ToString("dddd d MMMM yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));

                    if (exhibition.DateOfDue > MethodFactory.ArabianTimeNow())
                        paymentDTO.RemainingDays = Math.Ceiling(exhibition.DateOfDue.Value.Subtract(MethodFactory.ArabianTimeNow()).TotalDays).ToString();
                    else
                        paymentDTO.RemainingDays = "0 ";
                }
            }
            return paymentDTO;
        }
        private RestaurantStepFiveDTO LoadStepFiveContent(long RestaurantId, long langid = 1)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly Restaurant = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(RestaurantId);
                RestaurantStepFiveDTO dto = new RestaurantStepFiveDTO();

                if (!string.IsNullOrEmpty(Restaurant.ContractCms))
                    dto.ContractCMS = _appCustomSettings.UploadsCMSPath + "/ExhibitorRegistration/Contract/" + Restaurant.ContractCms;

                if (!string.IsNullOrEmpty(Restaurant.ContractUser))
                    dto.ContractCMS = _appCustomSettings.UploadsCMSPath + "/ExhibitorRegistration/Contract/" + Restaurant.ContractUser;

                return dto;
            }
        }
        private RestaurantStepSixDTO LoadStepSixContent(long RestaurantId, long langid = 1)
        {
            RestaurantStepSixDTO dto = new RestaurantStepSixDTO();
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly Restaurant = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(RestaurantId);
                XsiExhibitionExhibitorDetails Restaurantdetails = ExhibitorRegistrationService.GetDetailsById(RestaurantId);
                if (Restaurantdetails != default(XsiExhibitionExhibitorDetails))
                {
                    #region Section
                    if (Restaurantdetails.BoothSectionId != null)
                    {
                        dto.BoothSectionId = Restaurantdetails.BoothSectionId.Value;
                        dto.Section = GetBoothSection(Restaurantdetails.BoothSectionId.Value);

                        #region SubSection
                        if (Restaurantdetails.BoothSubSectionId != null)
                        {
                            dto.SubSection = GetBoothSubSection(Restaurantdetails.BoothSubSectionId.Value, Restaurantdetails.BoothSectionId.Value);
                        }
                        #endregion
                    }
                    #endregion
                    if (Restaurantdetails.AllocatedSpace != null)
                        if (Restaurantdetails.AllocatedSpace != string.Empty)
                            dto.Area = Restaurantdetails.AllocatedSpace;
                    if (Restaurantdetails.RequiredAreaType != null)
                    {
                        //dto.RequiredAreaShapeType = detail.RequiredAreaType;
                        if (Restaurantdetails.RequiredAreaType == EnumConversion.ToString(EnumRequiredAreaType.Open))
                            dto.Area = Restaurantdetails.AllocatedSpace + " " + Restaurantdetails.Area;
                    }
                    if (Restaurantdetails.HallNumber != null)
                        if (Restaurantdetails.HallNumber != "")
                            dto.HallNumber = Restaurantdetails.HallNumber;
                    if (Restaurantdetails.StandNumberStart != null && Restaurantdetails.StandNumberEnd != null)
                        if (Restaurantdetails.StandNumberStart != string.Empty && Restaurantdetails.StandNumberEnd != string.Empty)
                            dto.Stand = Restaurantdetails.StandNumberEnd + Restaurantdetails.StandNumberStart;
                    if (Restaurant != null)
                    {
                        if (Restaurant.UploadLocationMap != null && !string.IsNullOrEmpty(Restaurant.UploadLocationMap))
                        {
                            dto.UploadLocationMap = _appCustomSettings.UploadsCMSPath + "/ExhibitorRegistration/LocationMap/" + Restaurant.UploadLocationMap;
                        }
                    }
                }
            }
            return dto;
        }
        private void UpdateStatusChange(long RestaurantId)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(RestaurantId);
                if (entity != null)
                {
                    entity.IsStatusChanged = EnumConversion.ToString(EnumBool.No);
                    ExhibitorRegistrationService.UpdateExhibitorRegistration(entity);
                }
            }
        }
        protected bool IsTradeLicenceCategorySelected(long categoryId)
        {
            if (categoryId == 21 || categoryId == 23 || categoryId == 25)
                return true;
            else
                return false;
        }
        string GetBoothSubSection(long itemId, long sectionId, long langid = 1)
        {

            ExhibitionBoothService ExhibitionBoothService = new ExhibitionBoothService();
            XsiExhibitionBooth whereBoothSection = new XsiExhibitionBooth();
            whereBoothSection.ItemId = sectionId;
            XsiExhibitionBooth ExhibitionBoothEntity = ExhibitionBoothService.GetExhibitionBooth(whereBoothSection, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            if (ExhibitionBoothEntity != null)
            {
                ExhibitionBoothSubsectionService ExhibitionBoothSubsectionService = new ExhibitionBoothSubsectionService();
                XsiExhibitionBoothSubsection whereBoothSubSection = new XsiExhibitionBoothSubsection();
                whereBoothSubSection.ItemId = itemId;
                whereBoothSubSection.CategoryId = ExhibitionBoothEntity.ItemId;
                XsiExhibitionBoothSubsection ExhibitionBoothSubSectionEntity = ExhibitionBoothSubsectionService.GetExhibitionBoothSubsection(whereBoothSubSection, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                if (ExhibitionBoothSubSectionEntity != null)
                    return langid == 1 ? ExhibitionBoothSubSectionEntity.Title : ExhibitionBoothSubSectionEntity.TitleAr;
            }
            return string.Empty;
        }
        string GetBoothSection(long itemId, long langid = 1)
        {
            ExhibitionBoothService ExhibitionBoothService = new ExhibitionBoothService();
            XsiExhibitionBooth whereBoothSection = new XsiExhibitionBooth();
            whereBoothSection.ItemId = itemId;
            XsiExhibitionBooth ExhibitionBoothEntity = ExhibitionBoothService.GetExhibitionBooth(whereBoothSection, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            if (ExhibitionBoothEntity != null)
                return langid == 1 ? ExhibitionBoothEntity.Title : ExhibitionBoothEntity.TitleAr;
            return string.Empty;
        }
        private void LogRestaurantStatus(string status, long RestaurantId)
        {
            using (ExhibitorStatusLogService ExhibitorStatusLogService = new ExhibitorStatusLogService())
            {
                XsiExhibitionMemberApplicationYearlyLogs entity = new XsiExhibitionMemberApplicationYearlyLogs();
                entity.MemberExhibitionYearlyId = RestaurantId;
                entity.Status = status;
                entity.CreatedOn = MethodFactory.ArabianTimeNow();
                ExhibitorStatusLogService.InsertExhibitorStatusLog(entity);
            }
        }
        private bool IsTradeLicenceValid(StepTwoDTO entity, long langId)
        {
            if (entity.TradeLicenceName != null && entity.TradeLicenceName.Length > 0)
            {
                string fileName = string.Empty;
                byte[] imageBytes;
                if (entity.TradeLicenceName.Contains("data:"))
                {
                    var strInfo = entity.TradeLicenceName.Split(",")[0];
                    imageBytes = Convert.FromBase64String(entity.TradeLicenceName.Split(',')[1]);
                }
                else
                {
                    imageBytes = Convert.FromBase64String(entity.TradeLicenceName);
                }

                //fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + entity.TradeLicenceExt);
                //if (MethodFactory.IsValidImage(fileName))
                //{
                //    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\TradeLicence\\") + fileName, imageBytes);
                //}
                //else
                //    return false;
                return true;
            }
            else
            {
                //if (IsTradeLicenceCategorySelected(entity.ExhibitionCategoryId) && entity.TradeLicenceName == null)
                if (entity.TradeLicenceName == null)
                {
                    //ShowMessage(Resources.BackendError.UploadTradeLicence, EnumMessageType.Error, lblMessageStep2, pnlErrorMsgStep2, liMessage);
                    return false;
                }
                else
                    return true;
            }
        }
        private bool IsLogoValid(StepTwoDTO entity, long langId)
        {
            if (entity.LogoName != null && entity.LogoName.Length > 0)
            {
                string fileName = string.Empty;
                byte[] imageBytes;
                if (entity.LogoName.Contains("data:"))
                {
                    var strInfo = entity.LogoName.Split(",")[0];
                    imageBytes = Convert.FromBase64String(entity.LogoName.Split(',')[1]);
                }
                else
                {
                    imageBytes = Convert.FromBase64String(entity.LogoName);
                }

                //fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + entity.LogoExt);
                //if (MethodFactory.IsValidImage(fileName))
                //{
                //    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\Logo\\") + fileName, imageBytes);
                //    return true;
                //}
            }
            return false;
        }
        private void MoveToStep3(StepTwoDTO entity, long langId)
        {
            //CreateInterface(EnumRegistrationViewType.Step3);
            //DisableStep2Controls();
            //dvRequiredAreaContainer.Visible = true;

            //if (entity.RestaurantId == -1)
            //{
            //    //  ShowStep3Submit();
            //    //if (CurrentRestaurantEntity != null && CurrentRestaurantEntity.RequiredAreaGroupId != null)
            //    //{
            //    //}
            //    //else
            //    //{

            //    //}
            //    //    dvRequiredAreaContainer.Visible = false;
            //}
            //else
            //{

            //    //if (entity.IsStep2EditClicked)
            //    //{
            //    //    GetCurrentRestaurantDetails(entity.RestaurantId);
            //    //    EnableStep3Controls();
            //    //    ShowStep3Submit();
            //    //}
            //    //else
            //    //{
            //    //    LoadStep3();
            //    //    DisableStep3Controls();
            //    //}
            //}
            //  HideError(lblMessageStep2, pnlErrorMsgStep2);
        }
        XsiExhibitionMemberApplicationYearly GetCurrentRestaurantDetails(long RestaurantId)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                return ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(RestaurantId);
            }
        }
        XsiExhibitionMember GetMemberEntity(long exhibitionmemberId)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                XsiExhibitionMember entity = new XsiExhibitionMember();
                entity = ExhibitionMemberService.GetExhibitionMemberByItemId(Convert.ToInt64(exhibitionmemberId));
                if (entity != default(XsiExhibitionMember))
                    return entity;
            }
            return null;
        }
        private long SaveRestaurantRegistration(RestaurantSaveDataForStepTwoAndThreeDTO model, long langId)
        {
            long Restaurantid = 0;
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly entity = new XsiExhibitionMemberApplicationYearly();
                ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService();
                XsiExhibitionMember entityExhibitionMember = ExhibitionMemberService.GetExhibitionMemberByItemId(model.MemberId);
                if (entityExhibitionMember != null)
                    if (entityExhibitionMember.FileNumber != null && !string.IsNullOrEmpty(entityExhibitionMember.FileNumber) && entityExhibitionMember.FileNumber.ToLower() != "new")
                        entity.FileNumber = entityExhibitionMember.FileNumber;
                entity.ExhibitionId = model.ExhibitionId;
                entity.MemberRoleId = 8;
                entity.LanguageUrl = langId;
                entity.MemberId = model.MemberId;
                entity.CountryId = model.CountryId;
                entity.CityId = model.CityId;
                entity.OtherCity = model.OtherCity;
                entity.ExhibitionCategoryId = 35;
                entity.PublisherNameEn = model.PublisherNameEn;
                entity.PublisherNameAr = model.PublisherNameAr;

                entity.ContactPersonName = model.ContactPersonName;
                entity.ContactPersonNameAr = model.ContactPersonNameAr;

                entity.ContactPersonTitle = model.ContactPersonTitle;
                entity.ContactPersonTitleAr = model.ContactPersonTitleAr;
                entity.IsFirstTime = EnumConversion.ToString(EnumBool.No);

                if (!string.IsNullOrEmpty(model.PhoneISD) && !string.IsNullOrEmpty(model.PhoneSTD) && !string.IsNullOrEmpty(model.Phone))
                    entity.Phone = model.PhoneISD + "$" + model.PhoneSTD + "$" + model.Phone;
                if (!string.IsNullOrEmpty(model.MobileISD) && !string.IsNullOrEmpty(model.MobileSTD) && !string.IsNullOrEmpty(model.Mobile))
                    entity.Mobile = model.MobileISD + "$" + model.MobileSTD + "$" + model.Mobile;
                if (!string.IsNullOrEmpty(model.FaxISD) && !string.IsNullOrEmpty(model.FaxSTD) && !string.IsNullOrEmpty(model.Fax))
                    entity.Fax = model.FaxISD + "$" + model.FaxSTD + "$" + model.Fax;

                entity.Email = model.Email;
                entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                entity.IsStatusChanged = EnumConversion.ToString(EnumBool.No);
                entity.CreatedOn = MethodFactory.ArabianTimeNow();
                entity.CreatedBy = model.MemberId;
                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                entity.ModifiedBy = model.MemberId;
                // entity.FileName = model.FileName;
                entity.Status = EnumConversion.ToString(EnumExhibitorStatus.New);
                if (ExhibitorRegistrationService.InsertExhibitorRegistration(entity) == EnumResultType.Success)
                {
                    Restaurantid = ExhibitorRegistrationService.XsiItemdId;

                    XsiExhibitionExhibitorDetails details = new XsiExhibitionExhibitorDetails();
                    details.MemberExhibitionYearlyId = Restaurantid;
                    details.BoothSectionId = 17;
                    details.RequiredAreaType = EnumConversion.ToString(EnumRequiredAreaType.Open);// model.RequiredAreaShapeType;
                    details.CalculatedArea = model.CalculatedArea;

                    if (model.RequiredAreaId > 0)
                        details.RequiredAreaId = model.RequiredAreaId;

                    //if (model.RequiredAreaId > 0)
                    //{
                    //    details.RequiredAreaId = model.RequiredAreaId;

                    //    if (model.RequiredAreaShapeType == EnumConversion.ToString(EnumRequiredAreaType.Open))
                    //    {
                    //        long CalculatedArea = 0;
                    //        if (model.RequiredArea == "3")
                    //        {
                    //            CalculatedArea = Convert.ToInt64(model.RequiredAreaValue) / Convert.ToInt64(model.RequiredAreaMeter);
                    //            details.CalculatedArea = CalculatedArea.ToString() + "x" + model.RequiredAreaMeter;
                    //        }
                    //        if (model.RequiredArea == "6")
                    //        {
                    //            CalculatedArea = Convert.ToInt64(model.RequiredAreaValue) / Convert.ToInt64(model.RequiredAreaMeter);
                    //            details.CalculatedArea = CalculatedArea.ToString() + "x" + model.RequiredAreaMeter;
                    //        }
                    //    }
                    //}
                    if (model.LogoBase64 != null && model.LogoBase64.Length > 0)
                    {
                        string fileName = string.Empty;
                        byte[] imageBytes;
                        if (model.LogoBase64.Contains("data:"))
                        {
                            var strInfo = model.LogoBase64.Split(",")[0];
                            imageBytes = Convert.FromBase64String(model.LogoBase64.Split(',')[1]);
                        }
                        else
                        {
                            imageBytes = Convert.FromBase64String(model.LogoBase64);
                        }

                        fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + model.LogoExt);
                        if (MethodFactory.IsValidImage("." + model.LogoExt))
                        {
                            System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\Logo\\") + fileName, imageBytes);
                            details.FileName = fileName;
                        }
                    }
                    if (model.StandPhotoBase64 != null && model.StandPhotoBase64.Length > 0)
                    {
                        string fileName = string.Empty;
                        byte[] imageBytes;
                        if (model.StandPhotoBase64.Contains("data:"))
                        {
                            var strInfo = model.StandPhotoBase64.Split(",")[0];
                            imageBytes = Convert.FromBase64String(model.StandPhotoBase64.Split(',')[1]);
                        }
                        else
                            imageBytes = Convert.FromBase64String(model.StandPhotoBase64);

                        fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + model.StandPhotoExt);
                        if (MethodFactory.IsValidImage("." + model.StandPhotoExt))
                        {
                            System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\StandPhoto\\") + fileName, imageBytes);
                            details.StandPhoto = fileName;
                        }
                    }
                    if (model.StandLayoutByMeterBase64 != null && model.StandLayoutByMeterBase64.Length > 0)
                    {
                        string fileName = string.Empty;
                        byte[] imageBytes;
                        if (model.StandLayoutByMeterBase64.Contains("data:"))
                        {
                            var strInfo = model.StandLayoutByMeterBase64.Split(",")[0];
                            imageBytes = Convert.FromBase64String(model.StandLayoutByMeterBase64.Split(',')[1]);
                        }
                        else
                            imageBytes = Convert.FromBase64String(model.StandLayoutByMeterBase64);

                        fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + model.StandLayoutByMeterExt);
                        if (MethodFactory.IsValidImage("." + model.StandLayoutByMeterExt))
                        {
                            System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\StandLayoutByMeter\\") + fileName, imageBytes);
                            details.StandLayoutByMeter = fileName;
                        }
                    }
                    if (model.RequiredPowerCapacity != null)
                        details.RequiredPowerCapacity = model.RequiredPowerCapacity;

                    if (model.TradeLicenceBase64 != null && model.TradeLicenceBase64.Length > 0)
                    {
                        string fileName = string.Empty;
                        byte[] imageBytes;
                        if (model.TradeLicenceBase64.Contains("data:"))
                        {
                            var strInfo = model.TradeLicenceBase64.Split(",")[0];
                            imageBytes = Convert.FromBase64String(model.TradeLicenceBase64.Split(',')[1]);
                        }
                        else
                        {
                            imageBytes = Convert.FromBase64String(model.TradeLicenceBase64);
                        }

                        fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + model.TradeLicenceExt);
                        if (MethodFactory.IsValidImage("." + model.TradeLicenceExt))
                        {
                            System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\TradeLicence\\") + fileName, imageBytes);
                            details.TradeLicence = fileName;
                        }
                    }
                    if (ExhibitorRegistrationService.InsertExhibitorDetailsRegistration(details) == EnumResultType.Success)
                    {
                        var Restaurantdetailsid = ExhibitorRegistrationService.XsiItemdId;
                        // UpdateExhibitionMemberCompanyName(entity);

                        if (entityExhibitionMember.Email != entity.Email)
                            NotifyAdminandUserAboutEmailChange(entity.MemberExhibitionYearlyId, entityExhibitionMember.Email, entity.Email, model.MemberId, model.WebsiteId);
                        SendEmail(entity, model.MemberId, model.WebsiteId, langId);
                        LogRestaurantStatus(EnumConversion.ToString(EnumExhibitorStatus.New), Restaurantid);
                    }
                }
            }
            return Restaurantid;
        }
        void UpdateExhibitionMemberCompanyName(XsiExhibitionMemberApplicationYearly entity)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                XsiExhibitionMember member = ExhibitionMemberService.GetExhibitionMemberByItemId(entity.MemberId.Value);
                if (member != null)
                {
                    if (member.CompanyName != null && entity.PublisherNameEn != null)
                    {
                        if (!member.CompanyName.Equals(entity.PublisherNameEn))
                        {
                            member.CompanyName = entity.PublisherNameEn;
                            ExhibitionMemberService.UpdateExhibitionMember(member);
                        }
                    }
                    else
                    {
                        if (entity.PublisherNameEn != null)
                        {
                            member.CompanyName = entity.PublisherNameEn;
                            ExhibitionMemberService.UpdateExhibitionMember(member);
                        }
                    }


                    if (member.CompanyNameAr != null && entity.PublisherNameAr != null)
                    {
                        if (!member.CompanyNameAr.Equals(entity.PublisherNameAr))
                        {
                            member.CompanyNameAr = entity.PublisherNameAr;
                            ExhibitionMemberService.UpdateExhibitionMember(member);
                        }
                    }
                    else
                    {
                        if (entity.PublisherNameAr != null)
                        {
                            member.CompanyNameAr = entity.PublisherNameAr;
                            ExhibitionMemberService.UpdateExhibitionMember(member);
                        }
                    }
                    if (entity.XsiExhibitionExhibitorDetails != default(XsiExhibitionExhibitorDetails) && entity.XsiExhibitionExhibitorDetails.ToList().Count > 0)
                    {
                        if (entity.XsiExhibitionExhibitorDetails.ToList()[0].Brief != null)
                        {
                            member.Brief = entity.XsiExhibitionExhibitorDetails.ToList()[0].Brief;
                            ExhibitionMemberService.UpdateExhibitionMember(member);
                        }
                        if (entity.XsiExhibitionExhibitorDetails.ToList()[0].BriefAr != null)
                        {
                            member.BriefAr = entity.XsiExhibitionExhibitorDetails.ToList()[0].BriefAr;
                            ExhibitionMemberService.UpdateExhibitionMember(member);
                        }
                    }
                }
            }
        }
        void UpdateRestaurantRegistration(string IsEditRequest, RestaurantSaveDataForStepTwoAndThreeDTO model)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                string memberemail = string.Empty;
                XsiExhibitionMemberApplicationYearly entityOld = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(model.RestaurantId);
                XsiExhibitionMemberApplicationYearly entity = new XsiExhibitionMemberApplicationYearly();
                ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService();
                XsiExhibitionMember exhibitionmemberentity = new XsiExhibitionMember();
                exhibitionmemberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(model.MemberId);
                if (exhibitionmemberentity != default(XsiExhibitionMember) && exhibitionmemberentity.Email != null && !string.IsNullOrEmpty(exhibitionmemberentity.Email))
                    memberemail = exhibitionmemberentity.Email;
                if (entityOld != null)
                {
                    entity.MemberExhibitionYearlyId = model.RestaurantId;
                    if (entityOld.FileNumber != null)
                        entity.FileNumber = entityOld.FileNumber;
                    if (!string.IsNullOrEmpty(IsEditRequest))
                    {
                        entity.IsEditRequest = IsEditRequest;
                        entity.IsEditRequestEmailSent = EnumConversion.ToString(EnumBool.No);
                    }

                    entity.ExhibitionId = entityOld.ExhibitionId;
                    entity.MemberId = model.MemberId;
                    entity.CountryId = model.CountryId;
                    entity.CityId = model.CityId;
                    entity.OtherCity = model.OtherCity;
                    entity.ExhibitionCategoryId = 35;
                    entity.PublisherNameEn = model.PublisherNameEn;
                    entity.PublisherNameAr = model.PublisherNameAr;
                    entity.ContactPersonName = model.ContactPersonName;
                    entity.ContactPersonNameAr = model.ContactPersonNameAr;
                    entity.ContactPersonTitle = model.ContactPersonTitle;
                    entity.ContactPersonTitleAr = model.ContactPersonTitleAr;
                    if (entityOld.IsFirstTime != null)
                        entity.IsFirstTime = entityOld.IsFirstTime;

                    if (!string.IsNullOrEmpty(model.PhoneISD) && !string.IsNullOrEmpty(model.PhoneSTD) && !string.IsNullOrEmpty(model.Phone))
                        entity.Phone = model.PhoneISD + "$" + model.PhoneSTD + "$" + model.Phone;
                    if (!string.IsNullOrEmpty(model.MobileISD) && !string.IsNullOrEmpty(model.MobileSTD) && !string.IsNullOrEmpty(model.Mobile))
                        entity.Mobile = model.MobileISD + "$" + model.MobileSTD + "$" + model.Mobile;
                    if (!string.IsNullOrEmpty(model.FaxISD) && !string.IsNullOrEmpty(model.FaxSTD) && !string.IsNullOrEmpty(model.Fax))
                        entity.Fax = model.FaxISD + "$" + model.FaxSTD + "$" + model.Fax;

                    entity.Email = model.Email;
                    if (entityOld.IsActive != null)
                        entity.IsActive = entityOld.IsActive;
                    if (entityOld.IsStatusChanged != null)
                        entity.IsStatusChanged = entityOld.IsStatusChanged;

                    if (entityOld.Status != null)
                        entity.Status = entityOld.Status;

                    entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                    entity.ModifiedBy = model.MemberId;
                    //  entity.FileName = (!string.IsNullOrEmpty(model.FileName)) ? model.FileName : entityOld.FileName;
                    if (ExhibitorRegistrationService.UpdateExhibitorRegistration(entity) == EnumResultType.Success)
                    {
                        #region Details
                        XsiExhibitionExhibitorDetails entityDetailOld = ExhibitorRegistrationService.GetDetailsById(model.RestaurantId);
                        XsiExhibitionExhibitorDetails entityDetail = new XsiExhibitionExhibitorDetails();
                        if (entityDetailOld != default(XsiExhibitionExhibitorDetails))
                        {
                            entityDetail.ExhibitorDetailsId = entityDetailOld.ExhibitorDetailsId;
                        }
                        entityDetail.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;

                        entityDetail.Apu = model.APU;
                        if (model.LogoBase64 != null && model.LogoBase64.Length > 0)
                        {
                            string fileName = string.Empty;
                            byte[] imageBytes;
                            if (model.LogoBase64.Contains("data:"))
                            {
                                var strInfo = model.LogoBase64.Split(",")[0];
                                imageBytes = Convert.FromBase64String(model.LogoBase64.Split(',')[1]);
                            }
                            else
                            {
                                imageBytes = Convert.FromBase64String(model.LogoBase64);
                            }

                            fileName = string.Format("{0}_{1}{2}", LangId, MethodFactory.GetRandomNumber(), "." + model.LogoExt);
                            if (MethodFactory.IsValidImage("." + model.LogoExt))
                            {
                                System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\Logo\\") + fileName, imageBytes);
                                entityDetail.FileName = fileName;
                            }
                        }
                        if (model.StandPhotoBase64 != null && model.StandPhotoBase64.Length > 0)
                        {
                            string fileName = string.Empty;
                            byte[] imageBytes;
                            if (model.StandPhotoBase64.Contains("data:"))
                            {
                                var strInfo = model.StandPhotoBase64.Split(",")[0];
                                imageBytes = Convert.FromBase64String(model.StandPhotoBase64.Split(',')[1]);
                            }
                            else
                            {
                                imageBytes = Convert.FromBase64String(model.StandPhotoBase64);
                            }

                            fileName = string.Format("{0}_{1}{2}", LangId, MethodFactory.GetRandomNumber(), "." + model.StandPhotoExt);
                            if (MethodFactory.IsValidImage("." + model.StandPhotoExt))
                            {
                                System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\StandPhoto\\") + fileName, imageBytes);
                                entityDetail.StandPhoto = fileName;
                            }
                        }
                        if (model.StandLayoutByMeterBase64 != null && model.StandLayoutByMeterBase64.Length > 0)
                        {
                            string fileName = string.Empty;
                            byte[] imageBytes;
                            if (model.StandLayoutByMeterBase64.Contains("data:"))
                            {
                                var strInfo = model.StandLayoutByMeterBase64.Split(",")[0];
                                imageBytes = Convert.FromBase64String(model.StandLayoutByMeterBase64.Split(',')[1]);
                            }
                            else
                            {
                                imageBytes = Convert.FromBase64String(model.StandLayoutByMeterBase64);
                            }

                            fileName = string.Format("{0}_{1}{2}", LangId, MethodFactory.GetRandomNumber(), "." + model.StandLayoutByMeterExt);
                            if (MethodFactory.IsValidImage("." + model.StandLayoutByMeterExt))
                            {
                                System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\StandLayoutByMeter\\") + fileName, imageBytes);
                                entityDetail.StandLayoutByMeter = fileName;
                            }
                        }

                        if (model.RequiredPowerCapacity != null)
                            entityDetail.RequiredPowerCapacity = model.RequiredPowerCapacity;

                        if (model.TradeLicenceBase64 != null && model.TradeLicenceBase64.Length > 0)
                        {
                            string fileName = string.Empty;
                            byte[] imageBytes;
                            if (model.TradeLicenceBase64.Contains("data:"))
                            {
                                var strInfo = model.TradeLicenceBase64.Split(",")[0];
                                imageBytes = Convert.FromBase64String(model.TradeLicenceBase64.Split(',')[1]);
                            }
                            else
                            {
                                imageBytes = Convert.FromBase64String(model.TradeLicenceBase64);
                            }

                            fileName = string.Format("{0}_{1}{2}", LangId, MethodFactory.GetRandomNumber(), "." + model.TradeLicenceExt);
                            if (MethodFactory.IsValidImage("." + model.TradeLicenceExt))
                            {
                                System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\TradeLicence\\") + fileName, imageBytes);
                                entityDetail.TradeLicence = fileName;
                            }
                        }
                        //entityDetail.TradeLicence = (!string.IsNullOrEmpty(model.TradeLicenceName)) ? model.TradeLicenceName : entityDetailOld.TradeLicence;
                        #region Calculate Area
                        entityDetail.RequiredAreaType = EnumConversion.ToString(EnumRequiredAreaType.Open);// model.RequiredAreaShapeType;
                        entityDetail.CalculatedArea = model.CalculatedArea;

                        if (model.RequiredAreaId > 0)
                            entityDetail.RequiredAreaId = model.RequiredAreaId;

                        // entityDetail.RequiredAreaType = model.RequiredAreaShapeType;
                        //if (model.RequiredAreaId > 0)
                        //{
                        //    entityDetail.RequiredAreaId = model.RequiredAreaId;

                        //    if (model.RequiredAreaShapeType == EnumConversion.ToString(EnumRequiredAreaType.Open))
                        //    {
                        //        long CalculatedArea = 0;
                        //        if (model.RequiredArea == "3")
                        //        {
                        //            CalculatedArea = Convert.ToInt64(model.RequiredAreaValue) / Convert.ToInt64(model.RequiredAreaMeter);
                        //            entityDetail.CalculatedArea = CalculatedArea.ToString() + "x" + model.RequiredAreaMeter;
                        //        }
                        //        if (model.RequiredArea == "6")
                        //        {
                        //            CalculatedArea = Convert.ToInt64(model.RequiredAreaValue) / Convert.ToInt64(model.RequiredAreaMeter);
                        //            entityDetail.CalculatedArea = CalculatedArea.ToString() + "x" + model.RequiredAreaMeter;
                        //        }
                        //    }
                        //}

                        #endregion
                        #region Other Fields
                        if (entityDetailOld != default(XsiExhibitionExhibitorDetails))
                        {
                            if (entityDetail.AllocatedSpace != null)
                                entityDetail.AllocatedSpace = entityDetailOld.AllocatedSpace;
                            if (entityDetail.Area != null)
                                entityDetail.Area = entityDetailOld.Area;
                            if (entityDetail.HallNumber != null)
                                entityDetail.HallNumber = entityDetailOld.HallNumber;
                            if (entityDetail.StandNumberStart != null)
                                entityDetail.StandNumberStart = entityDetailOld.StandNumberStart;
                            if (entityDetail.StandNumberEnd != null)
                                entityDetail.StandNumberEnd = entityDetailOld.StandNumberEnd;
                            if (entityDetail.FirstLocation != null)
                                entityDetail.FirstLocation = entityDetailOld.FirstLocation;
                            if (entityDetail.SecondLocation != null)
                                entityDetail.SecondLocation = entityDetailOld.SecondLocation;
                            if (entityDetail.ThirdLocation != null)
                                entityDetail.ThirdLocation = entityDetailOld.ThirdLocation;
                            if (entityDetail.FourthLocation != null)
                                entityDetail.FourthLocation = entityDetailOld.FourthLocation;
                            if (entityDetail.FifthLocation != null)
                                entityDetail.FifthLocation = entityDetailOld.FifthLocation;
                            if (entityDetail.SixthLocation != null)
                                entityDetail.SixthLocation = entityDetailOld.SixthLocation;
                            if (entityDetail.StandCode != null)
                                entityDetail.StandCode = entityDetailOld.StandCode;
                            if (entityDetail.CancellationReason != null)
                                entityDetail.CancellationReason = entityDetailOld.CancellationReason;
                        }
                        #endregion


                        if (ExhibitorRegistrationService.UpdateExhibitorRegistrationDetails(entityDetail) == EnumResultType.Success)
                        {
                            // UpdateExhibitionMemberCompanyName(entity);

                            //if (IsEditRequest == EnumBool.Yes)
                            //    SendEmailEditRequest(entity);
                            //   NewStatusMsg.Visible = true;

                            //EnableNewStatusMessage();
                            //LogRestaurantStatus(entityOld.Status);
                        }
                        #endregion
                    }
                }
            }
        }
        bool IsSpaceDependentFieldChanged(XsiExhibitionMemberApplicationYearly currentRestaurantEntity, RestaurantSaveDataForStepTwoAndThreeDTO newDto)
        {
            XsiExhibitionExhibitorDetails prevDetails = new XsiExhibitionExhibitorDetails();
            if (currentRestaurantEntity.XsiExhibitionExhibitorDetails != default(XsiExhibitionExhibitorDetails) && currentRestaurantEntity.XsiExhibitionExhibitorDetails.ToList().Count > 0)
            {
                prevDetails = currentRestaurantEntity.XsiExhibitionExhibitorDetails.ToList()[0];

                if (prevDetails.RequiredAreaId != null)
                    if (prevDetails.RequiredAreaId != newDto.RequiredAreaId)
                        return true;
                if (prevDetails.RequiredAreaType != null)
                    if (prevDetails.RequiredAreaType != newDto.RequiredAreaShapeType)
                        return true;
                if (currentRestaurantEntity.PublisherNameAr != null)
                    if (currentRestaurantEntity.PublisherNameAr != newDto.PublisherNameAr)
                        return true;
                if (currentRestaurantEntity.PublisherNameEn != null)
                    if (currentRestaurantEntity.PublisherNameEn != newDto.PublisherNameEn)
                        return true;
            }
            return false;
        }
        bool IsAnyFieldChanged(XsiExhibitionMemberApplicationYearly CurrentRestaurantEntity, RestaurantSaveDataForStepTwoAndThreeDTO newDto)
        {
            XsiExhibitionExhibitorDetails prevDetails = new XsiExhibitionExhibitorDetails();
            if (CurrentRestaurantEntity.XsiExhibitionExhibitorDetails != null && CurrentRestaurantEntity.XsiExhibitionExhibitorDetails.ToList().Count > 0)
            {
                prevDetails = CurrentRestaurantEntity.XsiExhibitionExhibitorDetails.ToList()[0];

                if (CurrentRestaurantEntity.CountryId != null)
                    if (CurrentRestaurantEntity.CountryId != newDto.CountryId)
                        return true;
                if (CurrentRestaurantEntity.CityId != null)
                    if (CurrentRestaurantEntity.CityId != newDto.CityId)
                        return true;
                if (CurrentRestaurantEntity.ExhibitionCategoryId != null)
                    if (CurrentRestaurantEntity.ExhibitionCategoryId != newDto.ExhibitionCategoryId)
                        return true;

                if (prevDetails.RequiredAreaId != null)
                    if (prevDetails.RequiredAreaId != newDto.RequiredAreaId)
                        return true;
                if (prevDetails.RequiredAreaType != null)
                    if (prevDetails.RequiredAreaType != newDto.RequiredAreaShapeType)
                        return true;
                if (CurrentRestaurantEntity.PublisherNameAr != null)
                    if (CurrentRestaurantEntity.PublisherNameAr != newDto.PublisherNameAr.Trim())
                        return true;
                if (CurrentRestaurantEntity.PublisherNameEn != null)
                    if (CurrentRestaurantEntity.PublisherNameEn != newDto.PublisherNameEn.Trim())
                        return true;
                if (CurrentRestaurantEntity.ContactPersonName != null)
                    if (CurrentRestaurantEntity.ContactPersonName != newDto.ContactPersonName.Trim())
                        return true;
                if (CurrentRestaurantEntity.ContactPersonNameAr != null)
                    if (CurrentRestaurantEntity.ContactPersonNameAr != newDto.ContactPersonNameAr.Trim())
                        return true;
                if (CurrentRestaurantEntity.ContactPersonTitle != null)
                    if (CurrentRestaurantEntity.ContactPersonTitle != newDto.ContactPersonTitle.Trim())
                        return true;
                if (CurrentRestaurantEntity.ContactPersonTitleAr != null)
                    if (CurrentRestaurantEntity.ContactPersonTitleAr != newDto.ContactPersonTitleAr.Trim())
                        return true;
                if (CurrentRestaurantEntity.Phone != null)
                    if (CurrentRestaurantEntity.Phone != (newDto.PhoneISD + "$" + newDto.PhoneSTD + "$" + newDto.Phone))
                        return true;
                if (CurrentRestaurantEntity.Fax != null)
                    if (CurrentRestaurantEntity.Fax != (newDto.FaxISD + "$" + newDto.FaxSTD + "$" + newDto.Fax))
                        return true;
                if (CurrentRestaurantEntity.Fax != null)
                    if (CurrentRestaurantEntity.Fax != (newDto.MobileISD + "$" + newDto.MobileSTD + "$" + newDto.Mobile))
                        return true;
                if (CurrentRestaurantEntity.Email != null)
                    if (CurrentRestaurantEntity.Email != newDto.Email.Trim())
                        return true;
            }
            return false;
        }
        private string UploadReceipt(RestaurantUploadPayment model, long langId)
        {
            using (InvoiceService InvoiceService = new InvoiceService())
            {
                XsiInvoice entity = InvoiceService.GetInvoiceByItemId(model.InvoiceId);
                if (entity != null)
                {
                    if (model.FileBase64 != null && !string.IsNullOrEmpty(model.FileBase64))
                    {
                        string fileName = string.Empty;
                        byte[] imageBytes;
                        if (model.FileBase64.Contains("data:"))
                        {
                            var strInfo = model.FileBase64.Split(",")[0];
                            imageBytes = Convert.FromBase64String(model.FileBase64.Split(',')[1]);
                        }
                        else
                        {
                            imageBytes = Convert.FromBase64String(model.FileBase64);
                        }

                        fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + model.FileExtension);
                        System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\PaymentReceipt\\") + fileName, imageBytes);
                        // string filename = string.Format("{0}_{1}{2}", EnglishId, MethodFactory.GetRandomNumber(), MethodFactory.GetFileExtention(fuFile.FileName));
                        entity.UploadePaymentReceipt = fileName;

                        if (model.PaymentTypeId == Convert.ToInt64(EnumPaymentType.PaidByBankTransfer))
                        {
                            if (model.BankName != null)
                                entity.BankName = model.BankName.Trim();
                            entity.ReceiptNumber = string.Empty;
                            if (model.TransferDate != null)
                                entity.TransferDate = model.TransferDate;
                        }
                        else if (model.PaymentTypeId == Convert.ToInt64(EnumPaymentType.PaidByChequeTransfer))
                        {
                            if (model.BankName != null)
                                entity.BankName = model.BankName.Trim();
                            if (model.ReceiptNumber != null)
                                entity.ReceiptNumber = model.ReceiptNumber.Trim();
                        }
                        else if (model.PaymentTypeId == Convert.ToInt64(EnumPaymentType.PaidByCash))
                        {
                            entity.BankName = string.Empty;
                            entity.ReceiptNumber = string.Empty;
                        }
                        entity.Status = EnumConversion.ToString(EnumExhibitorStatus.ReceiptUploaded);
                        entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                        entity.PaymentTypeId = model.PaymentTypeId;
                        if (InvoiceService.UpdateInvoice(entity) == EnumResultType.Success)
                        {
                            var invoiceid = InvoiceService.XsiItemdId;
                            LogInvoiceStatus(EnumConversion.ToString(EnumExhibitorStatus.ReceiptUploaded), model.RestaurantId, invoiceid);
                            //EnableUpload(entity);
                            if (entity.ExhibitorId != null)
                            {
                                RestaurantRegistrationService RestaurantRegistrationService = new RestaurantRegistrationService();
                                XsiExhibitionMemberApplicationYearly restaurantentity = RestaurantRegistrationService.GetRestaurantRegistrationByItemId(entity.ExhibitorId.Value);
                                if (restaurantentity != null)
                                {
                                    if (model.ContractUserFileBase64 != null && !string.IsNullOrEmpty(model.ContractUserFileBase64))
                                    {
                                        string contractuserfileName = string.Empty;
                                        byte[] imageBytescontractuser;
                                        if (model.ContractUserFileBase64.Contains("data:"))
                                        {
                                            var strInfo = model.ContractUserFileBase64.Split(",")[0];
                                            imageBytescontractuser = Convert.FromBase64String(model.ContractUserFileBase64.Split(',')[1]);
                                        }
                                        else
                                        {
                                            imageBytescontractuser = Convert.FromBase64String(model.ContractUserFileBase64);
                                        }

                                        fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + model.ContractUserFileExtension);
                                        System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\Contract\\") + fileName, imageBytescontractuser);
                                        restaurantentity.ContractUser = fileName;
                                        RestaurantRegistrationService.UpdateRestaurantRegistration(restaurantentity);
                                    }

                                    SendEmail(restaurantentity, restaurantentity.MemberId.Value, 1, langId, true);
                                }
                                //MethodFactory.PushBankTransferDataForRestaurantToGalaxyService(entity, restaurantentity, _appCustomSettings.UploadsCMSPath, 2,_appCustomSettings.UploadsPhysicalPath);
                            }
                            if (langId == 1)
                                return "You will be notified via email after the payments are approved by accounting department.";
                            else
                                return "ملاحظة: سيتم إعلامك بعد أن يتم إعتماد المبلغ من قبل قسم الحسابات";
                        }
                        else
                        {
                            return "fail";
                            //EnableUpload(entity);
                            //ShowMessage(Resources.BackendError.InsertFailure, EnumMessageType.Error, lblMessageStep5, pnlErrorMsgStep5, liMessage5);
                        }
                    }
                    else
                    {
                        if (langId == 1)
                            return "Upload receipt missing";
                        else
                            return "Upload receipt missing";
                    }
                }
                return "fail";
            }
        }
        void PushBankTransferDataForRestaurantToGalaxyService(XsiInvoice invoiceentity, XsiExhibitionMemberApplicationYearly restaurantentity, string uploadscmspath, long adminuserid)
        {
            try
            {
                using (InvoiceStatusLogService InvoiceStatusLogService = new InvoiceStatusLogService())
                {
                    GIGEncryption gIGEncryption = new GIGEncryption();
                    GIGEncryptionMobile gIGEncryptionMobile = new GIGEncryptionMobile();
                    //string strtimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");

                    DateTime strtimestamp = DateTime.Now;
                    string pwd = "G@123456";
                    string tokenbeforeencryption = "Myahia&" + pwd + "&" + strtimestamp;
                    var tokenwithencryption = gIGEncryptionMobile.encrypt(tokenbeforeencryption, "BE1A8B181E6ABDE36EBD0276C18AB995", "postbanktransferer");  //PostBankTransferer
                                                                                                                                                             //string strmodel = "PostBankTransfer|" + strtimestamp + "|119602" + "|9999991"+ "|SIBF/21ACW/99999/J1"+"|100"+"|1"+"|abc bank"+ "|01-01-2021"+ "|RCPT12345";
                                                                                                                                                             //PostBankTransferer|01-01-2021|123|1|123|1256|1|testBank|01-01-2021 | AE00001232 | testFile
                                                                                                                                                             //SIBF/21ACW/999999/E1

                    #region Fields
                    string strreceiptnumber = "-";
                    string strpaidamount = "0";
                    string strbankname = "-";
                    string strinvoicenumber = "-";
                    string strtransferdate = "-";
                    string strpaidamt = "0";
                    string strpaymenttype = "-";
                    string struploadpaymentreceipt = "-";
                    //3CDBBC4DE6AB3ACCD53BDA60E8E055D2

                    dynamic myObject1 = JsonConvert.DeserializeObject<dynamic>("\"3CDBBC4DE6AB3ACCD53BDA60E8E055D2\"");
                    var strdecrypt1 = gIGEncryption.Decrypt(myObject1, "BE1A8B181E6ABDE36EBD0276C18AB995");

                    if (invoiceentity != null)
                    {
                        if (invoiceentity.PaidAmount != null && !string.IsNullOrEmpty(invoiceentity.PaidAmount))
                            strpaidamount = invoiceentity.PaidAmount;

                        if (invoiceentity.InvoiceNumber != null && !string.IsNullOrEmpty(invoiceentity.InvoiceNumber))
                            strinvoicenumber = invoiceentity.InvoiceNumber;

                        if (invoiceentity.PaidAmount != null && !string.IsNullOrEmpty(invoiceentity.PaidAmount))
                            strpaidamt = invoiceentity.PaidAmount;

                        if (invoiceentity.PaymentTypeId != null)
                            strpaymenttype = invoiceentity.PaymentTypeId.ToString();

                        if (invoiceentity.BankName != null && !string.IsNullOrEmpty(invoiceentity.BankName))
                            strbankname = invoiceentity.BankName;

                        if (invoiceentity.TransferDate != null)
                            strtransferdate = invoiceentity.TransferDate.Value.ToString("dd/MM/yyyy");

                        if (invoiceentity.ReceiptNumber != null && !string.IsNullOrEmpty(invoiceentity.ReceiptNumber))
                            strreceiptnumber = invoiceentity.ReceiptNumber;

                        if (invoiceentity.UploadePaymentReceipt != null && !string.IsNullOrEmpty(invoiceentity.UploadePaymentReceipt))
                            struploadpaymentreceipt = uploadscmspath + "/ExhibitorRegistration/PaymentReceipt/" + invoiceentity.UploadePaymentReceipt;
                    }
                    #endregion
                    // string strmodel = "PostBankTransfer|" + strtimestamp + "|120851" + "|99999" + "|SIBF21ACW99999E1" + "|100" + "|1" + "|xyz bank" + "|01-01-2021" + "|RCPT12345" + "|abcRCPT12345.jpg";

                    string strmodel = "PostBankTransfer|" + strtimestamp + "|" + invoiceentity.ItemId + "|" + restaurantentity.FileNumber + "|" + strinvoicenumber + "|" + strpaidamt + "|" + strpaymenttype + "|" + strbankname + "|" + strtransferdate + "|" + strreceiptnumber + "|" + struploadpaymentreceipt;

                    var strmodelencrypt = gIGEncryption.Encrypt(strmodel, "BE1A8B181E6ABDE36EBD0276C18AB995");

                    string url = _appCustomSettings.RevenueSystemBillingAPI + "api/PostBankTransfer/" + tokenwithencryption + "?model=" + strmodelencrypt;

                    WebClient client = new WebClient();
                    client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                    Stream data = client.OpenRead(url);
                    StreamReader reader = new StreamReader(data);
                    var responseFromServer = reader.ReadToEnd();
                    dynamic myObject = JsonConvert.DeserializeObject<dynamic>(responseFromServer);
                    var strdecrypt = gIGEncryption.Decrypt(myObject, "BE1A8B181E6ABDE36EBD0276C18AB995");
                    data.Close();
                    reader.Close();
                    var arr = strdecrypt.Split('|');
                    if (arr[1] == "000" || arr[1] == "101")
                    {
                        XsiInvoiceStatusLog log = new XsiInvoiceStatusLog();

                        log.InvoiceId = invoiceentity.ItemId;
                        log.ExhibitorId = invoiceentity.ExhibitorId;
                        log.AdminId = adminuserid;
                        log.GalaxyTeamId = Convert.ToInt64(arr[0]);
                        log.Status = invoiceentity.Status;
                        log.CreatedOn = MethodFactory.ArabianTimeNow();
                        InvoiceStatusLogService.InsertInvoiceStatusLog(log);
                    }
                    GalaxyLogInfo("restaurantbanktransfer", invoiceentity.ExhibitorId + "_" + restaurantentity.FileNumber, arr[1], strmodel, url);

                    //return strdecrypt;
                }
            }
            catch (Exception ex)
            {
                // LogException(ex);
            }
        }
        private void GalaxyLogInfo(string name, string id, string strcode, string strmodel, string url)
        {
            var dtnow = MethodFactory.ArabianTimeNow().ToString("yyyyMMddHHmmss");

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(_appCustomSettings.UploadsPhysicalPath + dtnow + name + id + "galaxyinfolog.txt", true))
            {
                file.WriteLine("------------------------------------");
                file.WriteLine(MethodFactory.ArabianTimeNow());
                file.WriteLine(name + " " + id + " " + strcode);
                file.WriteLine(strmodel);
                file.WriteLine(url);
                file.WriteLine("------------------------------------");
            }
        }
        private void LogInvoiceStatus(string status, long RestaurantId, long InvoiceId)
        {
            using (InvoiceStatusLogService InvoiceStatusLogService = new InvoiceStatusLogService())
            {
                XsiInvoiceStatusLog entity = new XsiInvoiceStatusLog();
                entity.ExhibitorId = RestaurantId;
                entity.InvoiceId = InvoiceId;
                entity.Status = status;
                entity.CreatedOn = MethodFactory.ArabianTimeNow();
                InvoiceStatusLogService.InsertInvoiceStatusLog(entity);
            }
        }
        RestaurantReturnDTO ShowMessage(string error, long stepId, long RestaurantId, long memberId, long websiteId)
        {
            RestaurantReturnDTO model = new RestaurantReturnDTO();
            model.Message = error;
            model.StepId = stepId;
            model.RestaurantId = RestaurantId;
            model.MemberId = memberId;
            model.WebsiteId = websiteId;
            return model;
        }
        #endregion
        #region Email
        void NotifyAdminandUserAboutEmailChange(long RestaurantId, string oldemail, string newemail, long memberId, long websiteId)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                XsiExhibitionMember memberentity = new XsiExhibitionMember();
                memberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(Convert.ToInt64(memberId));
                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                XsiExhibitionMemberApplicationYearly RestaurantEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(RestaurantId);
                if (memberentity != default(XsiExhibitionMember) && RestaurantEntity != default(XsiExhibitionMemberApplicationYearly))
                {
                    string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {
                        #region Send Admin Email Notifying about Email Change
                        StringBuilder body = new StringBuilder();
                        #region Email Content
                        SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                        if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                        {
                            var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(20069);
                            if (scrfEmailContent != null)
                            {
                                if (scrfEmailContent.Body != null)
                                    strEmailContentBody = scrfEmailContent.Body;
                                if (scrfEmailContent.Email != null && !string.IsNullOrEmpty(scrfEmailContent.Email))
                                    strEmailContentEmail = scrfEmailContent.Email;
                                else
                                    strEmailContentEmail = _appCustomSettings.AdminEmail;
                                if (scrfEmailContent.Subject != null)
                                    strEmailContentSubject = scrfEmailContent.Subject;
                                strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                            }
                        }
                        else
                        {
                            var emailContent = EmailContentService.GetEmailContentByItemId(20061);
                            if (emailContent != null)
                            {
                                if (emailContent.Body != null)
                                    strEmailContentBody = emailContent.Body;
                                if (emailContent.Email != null && !string.IsNullOrEmpty(emailContent.Email))
                                    strEmailContentEmail = emailContent.Email;
                                else
                                    strEmailContentEmail = _appCustomSettings.AdminEmail;
                                if (emailContent.Subject != null)
                                    strEmailContentSubject = emailContent.Subject;
                                strEmailContentAdmin = _appCustomSettings.AdminName;
                            }
                        }
                        #endregion
                        strEmailContentBody = strEmailContentBody.Replace("$$NewEmail$$", newemail)
                                                                       .Replace("$$OldEmail$$", oldemail);
                        if (RestaurantEntity.PublisherNameEn != null)
                            strEmailContentBody = strEmailContentBody.Replace("$$ExhibitorNameEn$$", RestaurantEntity.PublisherNameEn);
                        if (RestaurantEntity.FileNumber != null)
                            strEmailContentBody = strEmailContentBody.Replace("$$FileNumber$$", RestaurantEntity.FileNumber);
                        body.Append(htmlContentFactory.BindEmailContent(memberentity.LanguageUrl.Value, strEmailContentBody, memberentity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                        _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString().Replace("$$SpecificName$$", strEmailContentAdmin));
                        if (memberentity != default(XsiExhibitionMember))
                        {
                            string userbody = body.ToString();
                            if (memberentity.FirstNameEmail != null && memberentity.LastNameEmail != null)
                                userbody = userbody.Replace("$$SpecificName$$", memberentity.FirstNameEmail + " " + memberentity.LastNameEmail);
                            else if (memberentity.Firstname != null)
                                userbody = userbody.Replace("$$SpecificName$$", memberentity.FirstNameEmail);
                            else if (memberentity.LastName != null)
                                userbody = userbody.Replace("$$SpecificName$$", memberentity.LastNameEmail);
                            _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, memberentity.Email, strEmailContentSubject, userbody);
                        }
                        #endregion
                    }
                }
            }
        }
        void SendEmail(XsiExhibitionMemberApplicationYearly RestaurantEntity, long memberId, long websiteId, long langId, bool isReceipt = false)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                    XsiExhibitionMember entity = new XsiExhibitionMember();
                    entity = ExhibitionMemberService.GetExhibitionMemberByItemId(Convert.ToInt64(memberId));
                    if (entity != default(XsiExhibitionMember))
                    {
                        using (EmailContentService EmailContentService = new EmailContentService())
                        {
                            string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                            if (RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.New))
                            {
                                #region Send Thankyou Email
                                StringBuilder body = new StringBuilder();
                                #region Email Content
                                SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                                {
                                    var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(3);
                                    if (scrfEmailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (scrfEmailContent.Body != null)
                                                strEmailContentBody = scrfEmailContent.Body;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.Subject != null)
                                                strEmailContentSubject = scrfEmailContent.Subject;
                                        }
                                        else
                                        {
                                            if (scrfEmailContent.BodyAr != null)
                                                strEmailContentBody = scrfEmailContent.BodyAr;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.SubjectAr != null)
                                                strEmailContentSubject = scrfEmailContent.SubjectAr;
                                        }

                                        strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                    }
                                }
                                else
                                {
                                    var emailContent = EmailContentService.GetEmailContentByItemId(3);
                                    if (emailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (emailContent.Body != null)
                                                strEmailContentBody = emailContent.Body;
                                            if (emailContent.Email != null)
                                                strEmailContentEmail = emailContent.Email;
                                            if (emailContent.Subject != null)
                                                strEmailContentSubject = emailContent.Subject;
                                        }
                                        else
                                        {
                                            if (emailContent.BodyAr != null)
                                                strEmailContentBody = emailContent.BodyAr;
                                            if (emailContent.Email != null)
                                                strEmailContentEmail = emailContent.Email;
                                            if (emailContent.SubjectAr != null)
                                                strEmailContentSubject = emailContent.SubjectAr;
                                        }

                                        strEmailContentAdmin = _appCustomSettings.AdminName;
                                    }
                                }
                                #endregion
                                body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, entity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                                _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, entity.Email, strEmailContentSubject, body.ToString());
                                #endregion
                            }
                            else if (RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.Cancelled))
                            {
                                #region Send Cancellation Email
                                StringBuilder body = new StringBuilder();
                                #region Email Content
                                SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                                {
                                    var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(55);
                                    if (scrfEmailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (scrfEmailContent.Body != null)
                                                strEmailContentBody = scrfEmailContent.Body;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.Subject != null)
                                                strEmailContentSubject = scrfEmailContent.Subject;
                                        }
                                        else
                                        {
                                            if (scrfEmailContent.BodyAr != null)
                                                strEmailContentBody = scrfEmailContent.BodyAr;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.SubjectAr != null)
                                                strEmailContentSubject = scrfEmailContent.SubjectAr;
                                        }
                                        strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                    }
                                }
                                else
                                {
                                    var emailContent1 = EmailContentService.GetEmailContentByItemId(55);
                                    if (emailContent1 != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (emailContent1.Body != null)
                                                strEmailContentBody = emailContent1.Body;
                                            if (emailContent1.Email != null)
                                                strEmailContentEmail = emailContent1.Email;
                                            if (emailContent1.Subject != null)
                                                strEmailContentSubject = emailContent1.Subject;
                                        }
                                        else
                                        {
                                            if (emailContent1.BodyAr != null)
                                                strEmailContentBody = emailContent1.BodyAr;
                                            if (emailContent1.Email != null)
                                                strEmailContentEmail = emailContent1.Email;
                                            if (emailContent1.SubjectAr != null)
                                                strEmailContentSubject = emailContent1.SubjectAr;
                                        }

                                        strEmailContentAdmin = _appCustomSettings.AdminName;
                                    }
                                }
                                #endregion
                                body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, entity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                                _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, entity.Email, strEmailContentSubject, body.ToString());

                                #region Notify admin about Restaurant cancellation
                                body = new StringBuilder();
                                var emailContent = EmailContentService.GetEmailContentByItemId(20113);
                                if (emailContent != null)
                                {
                                    //string ToEmail = System.Configuration.ConfigurationManager.AppSettings["AccountsEmail"].ToString();

                                    body.Append(htmlContentFactory.BindEmailContent(langId, emailContent.Body, entity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));

                                    if (RestaurantEntity.PublisherNameEn != null)
                                        body.Replace("$$RestaurantName$$", RestaurantEntity.PublisherNameEn);
                                    else
                                        body.Replace("$$RestaurantName$$", "");

                                    body.Replace("$$FileNumber$$", RestaurantEntity.FileNumber);

                                    if (!string.IsNullOrEmpty(emailContent.Email))
                                        _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, _appCustomSettings.AdminEmail + "," + emailContent.Email, emailContent.Subject, body.ToString());
                                    else
                                        _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, _appCustomSettings.AdminEmail, emailContent.Subject, body.ToString());
                                }
                                #endregion

                                #endregion
                            }
                            else if (RestaurantEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved))
                            {
                                #region Send Approved Email
                                StringBuilder body = new StringBuilder();
                                #region Email Content
                                SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                                {
                                    var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(57);
                                    if (scrfEmailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (scrfEmailContent.Body != null)
                                                strEmailContentBody = scrfEmailContent.Body;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.Subject != null)
                                                strEmailContentSubject = scrfEmailContent.Subject;
                                        }
                                        else
                                        {
                                            if (scrfEmailContent.BodyAr != null)
                                                strEmailContentBody = scrfEmailContent.BodyAr;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.SubjectAr != null)
                                                strEmailContentSubject = scrfEmailContent.SubjectAr;
                                        }

                                        strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                    }
                                }
                                else
                                {
                                    var emailContent = EmailContentService.GetEmailContentByItemId(57);
                                    if (emailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (emailContent.Body != null)
                                                strEmailContentBody = emailContent.Body;
                                            if (emailContent.Email != null)
                                                strEmailContentEmail = emailContent.Email;
                                            if (emailContent.Subject != null)
                                                strEmailContentSubject = emailContent.Subject;
                                        }
                                        else
                                        {
                                            if (emailContent.BodyAr != null)
                                                strEmailContentBody = emailContent.BodyAr;
                                            if (emailContent.Email != null)
                                                strEmailContentEmail = emailContent.Email;
                                            if (emailContent.SubjectAr != null)
                                                strEmailContentSubject = emailContent.SubjectAr;
                                        }

                                        strEmailContentAdmin = _appCustomSettings.AdminName;
                                    }
                                }
                                #endregion
                                List<string> MailAttachments = new List<string>();
                                if (!string.IsNullOrEmpty(RestaurantEntity.UploadLocationMap))
                                    MailAttachments.Add(string.Format("{0}{1}", _appCustomSettings.UploadsPath + "ExhibitorRegistration/LocationMap/", RestaurantEntity.UploadLocationMap));

                                body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, entity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                                if (RestaurantEntity.XsiExhibitionExhibitorDetails != null && RestaurantEntity.XsiExhibitionExhibitorDetails.ToList().Count > 0)
                                {
                                    var details = RestaurantEntity.XsiExhibitionExhibitorDetails.ToList()[0];
                                    if (details.BoothSectionId != null)
                                    {
                                        ExhibitionBoothService ExhibitionBoothService = new ExhibitionBoothService();
                                        var exhibitionboothEntity = ExhibitionBoothService.GetExhibitionBoothByItemId(details.BoothSectionId.Value);
                                        body.Replace("$$BoothSection$$", GetBoothSection(details.BoothSectionId.Value));
                                        if (details.BoothSubSectionId != null)
                                            body.Replace("$$BoothSubSection$$", GetBoothSubSection(exhibitionboothEntity.ItemId, details.BoothSubSectionId.Value));
                                    }
                                    body.Replace("$$Area$$", details.AllocatedSpace);
                                    body.Replace("$$AreaShape$$", MethodFactory.GetAreaShape(details.AllocatedAreaType, langId));
                                    if (details.AllocatedAreaType != "O")
                                        body.Replace("$$display$$", "display:none");
                                    else
                                        body.Replace("$$display$$", string.Empty);
                                    body.Replace("$$HallNumber$$", details.HallNumber);
                                    //  body.Replace("$$BoothNumber$$", details.StandNumberEnd + details.StandNumberStart);
                                    if (!string.IsNullOrWhiteSpace(details.StandNumberEnd) && !string.IsNullOrWhiteSpace(details.StandNumberStart))
                                        body.Replace("$$BoothNumber$$", details.StandNumberEnd + "-" + details.StandNumberStart);
                                    else if (!string.IsNullOrWhiteSpace(details.StandNumberEnd))
                                        body.Replace("$$BoothNumber$$", details.StandNumberEnd);
                                    else if (!string.IsNullOrWhiteSpace(details.StandNumberStart))
                                        body.Replace("$$BoothNumber$$", details.StandNumberStart);

                                    body.Replace("$$CalculatedAllocatedArea$$", details.Area);
                                }


                                if (RestaurantEntity.Email != entity.Email.Trim())
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, RestaurantEntity.Email + "," + entity.Email, strEmailContentSubject, body.ToString(), MailAttachments);
                                else
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, entity.Email, strEmailContentSubject, body.ToString(), MailAttachments);

                                /*if (XsiResult == EnumResultType.Failed)
                                    XsiResult = EnumResultType.Exception;
                                else
                                    XsiResult = EnumResultType.Success;*/
                                #endregion
                            }
                            else if (isReceipt)
                            {
                                #region Send Receipt Uploaded email to accountant
                                string ToEmail = _appCustomSettings.AccountsEmail;
                                StringBuilder body = new StringBuilder();
                                #region Email Content
                                SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                                {
                                    var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(20081);
                                    if (scrfEmailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (scrfEmailContent.Body != null)
                                                strEmailContentBody = scrfEmailContent.Body;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.Subject != null)
                                                strEmailContentSubject = scrfEmailContent.Subject;
                                        }
                                        else
                                        {
                                            if (scrfEmailContent.BodyAr != null)
                                                strEmailContentBody = scrfEmailContent.BodyAr;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.SubjectAr != null)
                                                strEmailContentSubject = scrfEmailContent.SubjectAr;
                                        }

                                        strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                    }
                                }
                                else
                                {
                                    var emailContent = EmailContentService.GetEmailContentByItemId(20095);
                                    if (emailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (emailContent.Body != null)
                                                strEmailContentBody = emailContent.Body;
                                            if (emailContent.Email != null)
                                                strEmailContentEmail = emailContent.Email;
                                            if (emailContent.Subject != null)
                                                strEmailContentSubject = emailContent.Subject;
                                        }
                                        else
                                        {
                                            if (emailContent.BodyAr != null)
                                                strEmailContentBody = emailContent.BodyAr;
                                            if (emailContent.Email != null)
                                                strEmailContentEmail = emailContent.Email;
                                            if (emailContent.SubjectAr != null)
                                                strEmailContentSubject = emailContent.SubjectAr;
                                        }

                                        strEmailContentAdmin = _appCustomSettings.AdminName;
                                    }
                                }
                                #endregion
                                body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, entity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                                if (langId == 1)
                                    if (RestaurantEntity.PublisherNameEn != null)
                                        body.Replace("$$ExhibitorName$$", RestaurantEntity.PublisherNameEn);
                                    else
                                        body.Replace("$$ExhibitorName$$", "");
                                else
                                        if (RestaurantEntity.PublisherNameAr != null)
                                    body.Replace("$$ExhibitorName$$", RestaurantEntity.PublisherNameAr);
                                else
                                    body.Replace("$$ExhibitorName$$", "");
                                if (langId == 1)
                                    if (RestaurantEntity.ContactPersonName != null)
                                        body.Replace("$$ContactPerson$$", RestaurantEntity.ContactPersonName);
                                    else
                                        body.Replace("$$ContactPerson$$", "");
                                else
                                    if (RestaurantEntity.ContactPersonNameAr != null)
                                    body.Replace("$$ContactPerson$$", RestaurantEntity.ContactPersonNameAr);
                                else
                                    body.Replace("$$ContactPerson$$", "");
                                body.Replace("$$FileNumber$$", RestaurantEntity.FileNumber);

                                if (!string.IsNullOrEmpty(strEmailContentEmail))
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString());
                                else
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, ToEmail, strEmailContentSubject, body.ToString());

                                #endregion
                            }
                        }
                    }
                }
            }
        }
        void SendEmailBasedOnEdit(XsiExhibitionMemberApplicationYearly CurrentRestaurantEntity, long memberId, long websiteId, long RestaurantId, long langId)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                XsiExhibitionMember ExhibitionMemberEntity = new XsiExhibitionMember();
                ExhibitionMemberEntity = ExhibitionMemberService.GetExhibitionMemberByItemId(Convert.ToInt64(memberId));
                if (ExhibitionMemberEntity != default(XsiExhibitionMember))
                {
                    HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                    #region Send Edit Request Email
                    StringBuilder body = new StringBuilder();
                    #region Email Content
                    EmailContentService EmailContentService = new EmailContentService();
                    SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(10003);
                        if (scrfEmailContent != null)
                        {
                            if (langId == 1)
                            {
                                if (scrfEmailContent.Body != null)
                                    strEmailContentBody = scrfEmailContent.Body;
                                if (scrfEmailContent.Email != null)
                                    strEmailContentEmail = scrfEmailContent.Email;
                                if (scrfEmailContent.Subject != null)
                                    strEmailContentSubject = scrfEmailContent.Subject;
                            }
                            else
                            {
                                if (scrfEmailContent.BodyAr != null)
                                    strEmailContentBody = scrfEmailContent.BodyAr;
                                if (scrfEmailContent.Email != null)
                                    strEmailContentEmail = scrfEmailContent.Email;
                                if (scrfEmailContent.SubjectAr != null)
                                    strEmailContentSubject = scrfEmailContent.SubjectAr;
                            }

                            strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                        }
                    }
                    else
                    {
                        var emailContent = EmailContentService.GetEmailContentByItemId(10003);
                        if (emailContent != null)
                        {
                            if (langId == 1)
                            {
                                if (emailContent.Body != null)
                                    strEmailContentBody = emailContent.Body;
                                if (emailContent.Email != null)
                                    strEmailContentEmail = emailContent.Email;
                                if (emailContent.Subject != null)
                                    strEmailContentSubject = emailContent.Subject;
                            }
                            else
                            {
                                if (emailContent.BodyAr != null)
                                    strEmailContentBody = emailContent.BodyAr;
                                if (emailContent.Email != null)
                                    strEmailContentEmail = emailContent.Email;
                                if (emailContent.SubjectAr != null)
                                    strEmailContentSubject = emailContent.SubjectAr;
                            }

                            strEmailContentAdmin = _appCustomSettings.AdminName;
                        }
                    }
                    #endregion
                    body.Append(htmlContentFactory.BindEmailContent(ExhibitionMemberEntity.LanguageUrl.Value, strEmailContentBody, ExhibitionMemberEntity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                    string strContent = "<p style=\"font-size: 16px; line-height: 32px; font-family: Arial, Helvetica, sans-serif; color: #555555; margin: 0px;\">$$Label$$ : $$Value$$</p>";
                    using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
                    {
                        XsiExhibitionMemberApplicationYearly entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(RestaurantId);
                        if (entity != null)
                        {
                            #region Body
                            bool IsAnyFieldChanged = false;
                            if (entity.PublisherNameEn != null)
                                body.Replace("$$ExhibitorNameEn$$", entity.PublisherNameEn);
                            if (entity.FileNumber != null)
                                body.Replace("$$FileNumber$$", entity.FileNumber);
                            #region Publisher English
                            if (CurrentRestaurantEntity.PublisherNameEn != entity.PublisherNameEn)
                            {
                                if (entity.PublisherNameEn != null)
                                {
                                    body.Replace("$$NewPublisherNameEn$$", strContent);
                                    body.Replace("$$Label$$", "Publisher Name in English");
                                    body.Replace("$$Value$$", entity.PublisherNameEn);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewPublisherNameEn$$", string.Empty);
                                if (CurrentRestaurantEntity.PublisherNameEn != null)
                                {
                                    body.Replace("$$OldPublisherNameEn$$", strContent);
                                    body.Replace("$$Label$$", "Publisher Name in English");
                                    body.Replace("$$Value$$", CurrentRestaurantEntity.PublisherNameEn);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldPublisherNameEn$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewPublisherNameEn$$", string.Empty);
                                body.Replace("$$OldPublisherNameEn$$", string.Empty);
                            }
                            #endregion
                            #region Publisher Arabic
                            if (CurrentRestaurantEntity.PublisherNameAr != entity.PublisherNameAr)
                            {
                                if (entity.PublisherNameAr != null)
                                {
                                    body.Replace("$$NewPublisherNameAr$$", strContent);
                                    body.Replace("$$Label$$", "Publisher Name in Arabic");
                                    body.Replace("$$Value$$", entity.PublisherNameAr);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewPublisherNameAr$$", string.Empty);
                                if (CurrentRestaurantEntity.PublisherNameAr != null)
                                {
                                    body.Replace("$$OldPublisherNameAr$$", strContent);
                                    body.Replace("$$Label$$", "Publisher Name in Arabic");
                                    body.Replace("$$Value$$", CurrentRestaurantEntity.PublisherNameAr);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldPublisherNameAr$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewPublisherNameAr$$", string.Empty);
                                body.Replace("$$OldPublisherNameAr$$", string.Empty);
                            }
                            #endregion
                            #region Stand Section
                            XsiExhibitionExhibitorDetails prevDetails = new XsiExhibitionExhibitorDetails();
                            if (CurrentRestaurantEntity.XsiExhibitionExhibitorDetails != null && CurrentRestaurantEntity.XsiExhibitionExhibitorDetails.ToList().Count > 0)
                            {
                                prevDetails = CurrentRestaurantEntity.XsiExhibitionExhibitorDetails.ToList()[0];
                            }
                            XsiExhibitionExhibitorDetails currDetails = new XsiExhibitionExhibitorDetails();
                            if (entity.XsiExhibitionExhibitorDetails != null && entity.XsiExhibitionExhibitorDetails.ToList().Count > 0)
                            {
                                currDetails = entity.XsiExhibitionExhibitorDetails.ToList()[0];
                            }
                            if (prevDetails.BoothSectionId != currDetails.BoothSectionId)
                            {
                                if (currDetails.BoothSectionId != null)
                                {
                                    body.Replace("$$NewStandSection$$", strContent);
                                    body.Replace("$$Label$$", "Stand Section");
                                    body.Replace("$$Value$$", GetBoothSection(currDetails.BoothSectionId.Value));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewStandSection$$", string.Empty);
                                if (prevDetails.BoothSectionId != null)
                                {
                                    body.Replace("$$OldStandSection$$", strContent);
                                    body.Replace("$$Label$$", "Stand Section");
                                    body.Replace("$$Value$$", prevDetails.BoothSectionId.ToString());
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldStandSection$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewStandSection$$", string.Empty);
                                body.Replace("$$OldStandSection$$", string.Empty);
                            }
                            #endregion
                            #region Stand Sub Section
                            if (prevDetails.BoothSubSectionId != currDetails.BoothSubSectionId)
                            {
                                if (currDetails.BoothSubSectionId != null)
                                {
                                    body.Replace("$$NewStandSubSection$$", strContent);
                                    body.Replace("$$Label$$", "Stand Sub Section");
                                    body.Replace("$$Value$$", GetBoothSubSection(currDetails.BoothSubSectionId.Value, currDetails.BoothSectionId.Value));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewStandSubSection$$", string.Empty);
                                if (prevDetails.BoothSubSectionId != null)
                                {
                                    body.Replace("$$OldStandSubSection$$", strContent);
                                    body.Replace("$$Label$$", "Stand Sub Section");
                                    body.Replace("$$Value$$", GetBoothSubSection(prevDetails.BoothSubSectionId.Value, prevDetails.BoothSectionId.Value));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldStandSubSection$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewStandSubSection$$", string.Empty);
                                body.Replace("$$OldStandSubSection$$", string.Empty);
                            }
                            #endregion
                            #region Required Area
                            if (prevDetails.RequiredAreaId != currDetails.RequiredAreaId)
                            {
                                if (currDetails.RequiredAreaId != null)
                                {
                                    body.Replace("$$NewRequiredArea$$", strContent);
                                    body.Replace("$$Label$$", "Required Area");
                                    if (MethodFactory.BindExhibitionArea(currDetails.RequiredAreaId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.BindExhibitionArea(currDetails.RequiredAreaId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$NewRequiredArea$$", string.Empty);
                                }
                                else
                                    body.Replace("$$NewRequiredArea$$", string.Empty);
                                if (prevDetails.RequiredAreaId != null)
                                {
                                    body.Replace("$$OldRequiredArea$$", strContent);
                                    body.Replace("$$Label$$", "Required Area");
                                    if (MethodFactory.BindExhibitionArea(currDetails.RequiredAreaId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.BindExhibitionArea(currDetails.RequiredAreaId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$OldRequiredArea$$", string.Empty);
                                }
                                else
                                    body.Replace("$$OldRequiredArea$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewRequiredArea$$", string.Empty);
                                body.Replace("$$OldRequiredArea$$", string.Empty);
                            }
                            #endregion
                            #region Calculated Area
                            if (prevDetails.CalculatedArea != currDetails.CalculatedArea)
                            {
                                if (currDetails.CalculatedArea != null)
                                {
                                    body.Replace("$$NewCalculatedArea$$", strContent);
                                    body.Replace("$$Label$$", "Calculated Area");
                                    body.Replace("$$Value$$", currDetails.CalculatedArea);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewCalculatedArea$$", string.Empty);
                                if (prevDetails.CalculatedArea != null)
                                {
                                    body.Replace("$$OldCalculatedArea$$", strContent);
                                    body.Replace("$$Label$$", "Calculated Area");
                                    body.Replace("$$Value$$", prevDetails.CalculatedArea);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldCalculatedArea$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewCalculatedArea$$", string.Empty);
                                body.Replace("$$OldCalculatedArea$$", string.Empty);
                            }
                            #endregion
                            #region Area Shape
                            if (prevDetails.RequiredAreaType != currDetails.RequiredAreaType)
                            {
                                if (currDetails.RequiredAreaType != null)
                                {
                                    body.Replace("$$NewAreaShape$$", strContent);
                                    body.Replace("$$Label$$", "Area Shape");
                                    if (MethodFactory.BindAreaShape(currDetails.RequiredAreaType, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.BindAreaShape(currDetails.RequiredAreaType, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$NewAreaShape$$", string.Empty);
                                }
                                else
                                    body.Replace("$$NewAreaShape$$", string.Empty);
                                if (prevDetails.RequiredAreaType != null)
                                {
                                    body.Replace("$$OldAreaShape$$", strContent);
                                    body.Replace("$$Label$$", "Area Shape");
                                    if (MethodFactory.BindAreaShape(prevDetails.RequiredAreaType, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.BindAreaShape(prevDetails.RequiredAreaType, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$OldAreaShape$$", string.Empty);
                                }
                                else
                                    body.Replace("$$OldAreaShape$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewAreaShape$$", string.Empty);
                                body.Replace("$$OldAreaShape$$", string.Empty);
                            }
                            #endregion
                            #region Country
                            if (CurrentRestaurantEntity.CountryId != entity.CountryId)
                            {
                                if (entity.CountryId != null)
                                {
                                    body.Replace("$$NewCountry$$", strContent);
                                    body.Replace("$$Label$$", "Country");
                                    if (MethodFactory.GetCountryName(entity.CountryId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.GetCountryName(entity.CountryId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$NewCountry$$", string.Empty);
                                }
                                else
                                    body.Replace("$$NewCountry$$", string.Empty);
                                if (CurrentRestaurantEntity.CountryId != null)
                                {
                                    body.Replace("$$OldCountry$$", strContent);
                                    body.Replace("$$Label$$", "Country");
                                    if (MethodFactory.GetCountryName(CurrentRestaurantEntity.CountryId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.GetCountryName(CurrentRestaurantEntity.CountryId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$OldCountry$$", string.Empty);
                                }
                                else
                                    body.Replace("$$OldCountry$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewCountry$$", string.Empty);
                                body.Replace("$$OldCountry$$", string.Empty);
                            }
                            #endregion
                            #region City
                            if (CurrentRestaurantEntity.CityId != entity.CityId)
                            {
                                if (entity.CityId != null)
                                {
                                    body.Replace("$$NewCity$$", strContent);
                                    body.Replace("$$Label$$", "City");
                                    if (MethodFactory.GetCityName(entity.CityId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.GetCityName(entity.CityId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$NewCity$$", string.Empty);
                                }
                                else
                                    body.Replace("$$NewCity$$", string.Empty);
                                if (CurrentRestaurantEntity.CityId != null)
                                {
                                    body.Replace("$$OldCity$$", strContent);
                                    body.Replace("$$Label$$", "City");
                                    if (MethodFactory.GetCityName(CurrentRestaurantEntity.CityId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.GetCityName(CurrentRestaurantEntity.CityId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$OldCity$$", string.Empty);
                                }
                                else
                                    body.Replace("$$OldCity$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewCity$$", string.Empty);
                                body.Replace("$$OldCity$$", string.Empty);
                            }
                            #endregion
                            #region Exhibition Category
                            if (CurrentRestaurantEntity.ExhibitionCategoryId != entity.ExhibitionCategoryId)
                            {
                                if (entity.ExhibitionCategoryId != null)
                                {
                                    body.Replace("$$NewExhibitionCategory$$", strContent);
                                    body.Replace("$$Label$$", "Exhibition Category");
                                    if (MethodFactory.GetExhibitionCategory(entity.ExhibitionCategoryId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.GetExhibitionCategory(entity.ExhibitionCategoryId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$NewExhibitionCategory$$", string.Empty);
                                }
                                else
                                    body.Replace("$$NewExhibitionCategory$$", string.Empty);
                                if (CurrentRestaurantEntity.ExhibitionCategoryId != null)
                                {
                                    body.Replace("$$OldExhibitionCategory$$", strContent);
                                    body.Replace("$$Label$$", "Exhibition Category");
                                    if (MethodFactory.GetExhibitionCategory(CurrentRestaurantEntity.ExhibitionCategoryId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.GetExhibitionCategory(CurrentRestaurantEntity.ExhibitionCategoryId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$OldExhibitionCategory$$", string.Empty);
                                }
                                else
                                    body.Replace("$$OldExhibitionCategory$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewExhibitionCategory$$", string.Empty);
                                body.Replace("$$OldExhibitionCategory$$", string.Empty);
                            }
                            #endregion
                            #region Contact Person English
                            if (CurrentRestaurantEntity.ContactPersonName != entity.ContactPersonName)
                            {
                                if (entity.ContactPersonName != null)
                                {
                                    body.Replace("$$NewContactPersonNameEn$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Name in English");
                                    body.Replace("$$Value$$", entity.ContactPersonName);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewContactPersonNameEn$$", string.Empty);
                                if (CurrentRestaurantEntity.ContactPersonName != null)
                                {
                                    body.Replace("$$OldContactPersonNameEn$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Name in English");
                                    body.Replace("$$Value$$", CurrentRestaurantEntity.ContactPersonName);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldContactPersonNameEn$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewContactPersonNameEn$$", string.Empty);
                                body.Replace("$$OldContactPersonNameEn$$", string.Empty);
                            }
                            #endregion
                            #region Contact Person Arabic
                            if (CurrentRestaurantEntity.ContactPersonNameAr != entity.ContactPersonNameAr)
                            {
                                if (entity.ContactPersonNameAr != null)
                                {
                                    body.Replace("$$NewContactPersonNameAr$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Name in Arabic");
                                    body.Replace("$$Value$$", entity.ContactPersonNameAr);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewContactPersonNameAr$$", string.Empty);
                                if (CurrentRestaurantEntity.ContactPersonNameAr != null)
                                {
                                    body.Replace("$$OldContactPersonNameAr$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Name in Arabic");
                                    body.Replace("$$Value$$", CurrentRestaurantEntity.ContactPersonNameAr);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldContactPersonNameAr$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewContactPersonNameAr$$", string.Empty);
                                body.Replace("$$OldContactPersonNameAr$$", string.Empty);
                            }
                            #endregion
                            #region Contact Person Title English
                            if (CurrentRestaurantEntity.ContactPersonTitle != entity.ContactPersonTitle)
                            {
                                if (entity.ContactPersonTitle != null)
                                {
                                    body.Replace("$$NewContactPersonTitleEn$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Title in English");
                                    body.Replace("$$Value$$", entity.ContactPersonTitle);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewCContactPersonTitleEn$$", string.Empty);
                                if (CurrentRestaurantEntity.ContactPersonTitle != null)
                                {
                                    body.Replace("$$OldContactPersonTitleEn$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Title in English");
                                    body.Replace("$$Value$$", CurrentRestaurantEntity.ContactPersonTitle);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldContactPersonTitleEn$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewContactPersonTitleEn$$", string.Empty);
                                body.Replace("$$OldContactPersonTitleEn$$", string.Empty);
                            }
                            #endregion
                            #region Contact Person Title Arabic
                            if (CurrentRestaurantEntity.ContactPersonTitleAr != entity.ContactPersonTitleAr)
                            {
                                if (entity.ContactPersonTitleAr != null)
                                {
                                    body.Replace("$$NewContactPersonTitleAr$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Title in Arabic");
                                    body.Replace("$$Value$$", entity.ContactPersonTitleAr);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewCContactPersonTitleAr$$", string.Empty);
                                if (CurrentRestaurantEntity.ContactPersonTitleAr != null)
                                {
                                    body.Replace("$$OldContactPersonTitleAr$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Title in Arabic");
                                    body.Replace("$$Value$$", CurrentRestaurantEntity.ContactPersonTitleAr);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldContactPersonTitleAr$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewContactPersonTitleAr$$", string.Empty);
                                body.Replace("$$OldContactPersonTitleAr$$", string.Empty);
                            }
                            #endregion
                            #region Phone
                            if (CurrentRestaurantEntity.Phone != entity.Phone)
                            {
                                if (entity.Phone != null)
                                {
                                    body.Replace("$$NewPhone$$", strContent);
                                    body.Replace("$$Label$$", "Phone");
                                    body.Replace("$$Value$$", entity.Phone.Replace('$', '-'));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewPhone$$", string.Empty);
                                if (CurrentRestaurantEntity.Phone != null)
                                {
                                    body.Replace("$$OldPhone$$", strContent);
                                    body.Replace("$$Label$$", "Phone");
                                    body.Replace("$$Value$$", CurrentRestaurantEntity.Phone);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldPhone$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewPhone$$", string.Empty);
                                body.Replace("$$OldPhone$$", string.Empty);
                            }
                            #endregion
                            #region Fax
                            if (CurrentRestaurantEntity.Fax != entity.Fax)
                            {
                                if (entity.Fax != null)
                                {
                                    body.Replace("$$NewFax$$", strContent);
                                    body.Replace("$$Label$$", "Fax");
                                    body.Replace("$$Value$$", entity.Fax.Replace('$', '-'));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewFax$$", string.Empty);
                                if (CurrentRestaurantEntity.Fax != null)
                                {
                                    body.Replace("$$OldFax$$", strContent);
                                    body.Replace("$$Label$$", "Fax");
                                    body.Replace("$$Value$$", CurrentRestaurantEntity.Fax);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldFax$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewFax$$", string.Empty);
                                body.Replace("$$OldFax$$", string.Empty);
                            }
                            #endregion
                            #region Mobile
                            if (CurrentRestaurantEntity.Mobile != entity.Mobile)
                            {
                                if (entity.Mobile != null)
                                {
                                    body.Replace("$$NewMobile$$", strContent);
                                    body.Replace("$$Label$$", "Mobile");
                                    body.Replace("$$Value$$", entity.Mobile.Replace('$', '-'));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewMobile$$", string.Empty);
                                if (CurrentRestaurantEntity.Mobile != null)
                                {
                                    body.Replace("$$OldMobile$$", strContent);
                                    body.Replace("$$Label$$", "Mobile");
                                    body.Replace("$$Value$$", CurrentRestaurantEntity.Mobile);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldMobile$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewMobile$$", string.Empty);
                                body.Replace("$$OldMobile$$", string.Empty);
                            }
                            #endregion
                            #region Email
                            if (CurrentRestaurantEntity.Email != entity.Email)
                            {
                                if (entity.Email != null)
                                {
                                    body.Replace("$$NewEmail$$", strContent);
                                    body.Replace("$$Label$$", "Email");
                                    body.Replace("$$Value$$", entity.Email);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewEmail$$", string.Empty);
                                if (CurrentRestaurantEntity.Email != null)
                                {
                                    body.Replace("$$OldEmail$$", strContent);
                                    body.Replace("$$Label$$", "Email");
                                    body.Replace("$$Value$$", CurrentRestaurantEntity.Email);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldEmail$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewEmail$$", string.Empty);
                                body.Replace("$$OldEmail$$", string.Empty);
                            }
                            #endregion
                            #region Total Titles
                            if (CurrentRestaurantEntity.TotalNumberOfTitles != entity.TotalNumberOfTitles)
                            {
                                if (entity.TotalNumberOfTitles != null)
                                {
                                    body.Replace("$$NewTotletitles$$", strContent);
                                    body.Replace("$$Label$$", "Total Number Of Titles");
                                    body.Replace("$$Value$$", entity.TotalNumberOfTitles);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewTotletitles$$", string.Empty);
                                if (CurrentRestaurantEntity.TotalNumberOfTitles != null)
                                {
                                    body.Replace("$$OldTotletitles$$", strContent);
                                    body.Replace("$$Label$$", "Total Number Of Titles");
                                    body.Replace("$$Value$$", CurrentRestaurantEntity.TotalNumberOfTitles);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldTotletitles$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewTotletitles$$", string.Empty);
                                body.Replace("$$OldTotletitles$$", string.Empty);
                            }
                            #endregion
                            #region Total New Titles
                            if (CurrentRestaurantEntity.TotalNumberOfNewTitles != entity.TotalNumberOfNewTitles)
                            {
                                if (entity.TotalNumberOfNewTitles != null)
                                {
                                    body.Replace("$$NewTotletitlesNew$$", strContent);
                                    body.Replace("$$Label$$", "Total Number Of New Titles");
                                    body.Replace("$$Value$$", entity.TotalNumberOfNewTitles);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewTotletitlesNew$$", string.Empty);
                                if (CurrentRestaurantEntity.TotalNumberOfNewTitles != null)
                                {
                                    body.Replace("$$OldTotletitlesNew$$", strContent);
                                    body.Replace("$$Label$$", "Total Number Of new Titles");
                                    body.Replace("$$Value$$", CurrentRestaurantEntity.TotalNumberOfNewTitles);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldTotletitlesNew$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewTotletitlesNew$$", string.Empty);
                                body.Replace("$$OldTotletitlesNew$$", string.Empty);
                            }
                            #endregion
                            #region APU
                            if (prevDetails.Apu != currDetails.Apu)
                            {
                                if (currDetails.Apu != null)
                                {
                                    body.Replace("$$NewAPU$$", strContent);
                                    body.Replace("$$Label$$", "Participation in Publishers Union");
                                    if (currDetails.Apu == "Y")
                                        body.Replace("$$Value$$", "Yes");
                                    else if (currDetails.Apu == "N")
                                        body.Replace("$$Value$$", "No");
                                    else
                                        body.Replace("$$NewAPU$$", string.Empty);
                                }
                                else
                                    body.Replace("$$NewAPU$$", string.Empty);
                                if (prevDetails.Apu != null)
                                {
                                    body.Replace("$$OldAPU$$", strContent);
                                    body.Replace("$$Label$$", "Participation in Publishers Union");
                                    if (prevDetails.Apu == "Y")
                                        body.Replace("$$Value$$", "Yes");
                                    else if (prevDetails.Apu == "N")
                                        body.Replace("$$Value$$", "No");
                                    else
                                        body.Replace("$$OldAPU$$", string.Empty);
                                }
                                else
                                    body.Replace("$$OldAPU$$", string.Empty);
                                IsAnyFieldChanged = true;
                            }
                            else
                            {
                                body.Replace("$$NewAPU$$", string.Empty);
                                body.Replace("$$OldAPU$$", string.Empty);
                            }
                            #endregion
                            #endregion
                            if (IsAnyFieldChanged)
                                if (strEmailContentEmail != string.Empty)
                                {
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString().Replace("$$SpecificName$$", strEmailContentAdmin));

                                    //XsiExhibitionMember memberentity = GetMemberEntity(entity.MemberId.Value);
                                    //if (memberentity != default(XsiExhibitionMember))
                                    //{
                                    //    string userbody = body.ToString();
                                    //    if (memberentity.FirstNameEmail != null && memberentity.LastNameEmail != null)
                                    //        userbody = userbody.Replace("$$SpecificName$$", memberentity.FirstNameEmail + " " + memberentity.LastNameEmail);
                                    //    else if (memberentity.Firstname != null)
                                    //        userbody = userbody.Replace("$$SpecificName$$", memberentity.FirstNameEmail);
                                    //    else if (memberentity.LastName != null)
                                    //        userbody = userbody.Replace("$$SpecificName$$", memberentity.LastNameEmail);
                                    //    SmtpEmail.SendNetEmail(strEmailContentAdmin, AdminEmailAddress, memberentity.Email, strEmailContentSubject, userbody);
                                    //}
                                }
                        }
                    }
                    #endregion
                }
            }
        }
        #endregion
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StaffGuestAddedMemberController : ControllerBase
    {
        public StaffGuestAddedMemberController()
        {
        }

        // GET: api/StaffGuestAddedMember
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DropdownDataDTO>>> GetMembersAddedByStaff()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long memberid = User.Identity.GetID();
                var predicate = PredicateBuilder.True<XsiExhibitionStaffGuest>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.MemberId == memberid);

                var List = _context.XsiExhibitionStaffGuest.AsQueryable().Where(predicate).OrderBy(x => x.NameEn.Trim()).Select(x => new DropdownDataDTO()
                {
                    ItemId = x.StaffGuestId,
                    Title = x.NameEn,
                }).ToListAsync();

                return await List;
            }
        }

        // GET: api/StaffGuestType/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DropdownDataDTO>> GetMembersAddedByStaff(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var xsiItem = await _context.XsiExhibitionStaffGuest.FindAsync(id);

                if (xsiItem == null)
                {
                    return NotFound();
                }

                DropdownDataDTO itemDTO = new DropdownDataDTO()
                {
                    ItemId = xsiItem.StaffGuestId,
                    Title = xsiItem.NameEn,
                };
                return itemDTO;
            }
        }

        private bool XsiStaffGuestTypeExists(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiStaffGuestType.Any(e => e.ItemId == id);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;

using SIBFAPIEn.Utility;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class DownloadCategoryController : ControllerBase
    {
        private readonly IEmailSender _emailSender;
        private readonly sibfnewdbContext _context;
        public DownloadCategoryController(IEmailSender emailSender, sibfnewdbContext context)
        {
            _emailSender = emailSender;
            _context = context;
        }

        // GET: api/DownloadCategory
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryDTO>>> GetXsiDownloadCategory()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {  // await _emailSender.SendEmailAsync("khizar@sharjahbookfair.com", "New Hello World - Sending email using ASP.NET Core 2.2", "Test email trials message");
                var predicate = PredicateBuilder.True<XsiDownloadCategory>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiDownloadCategory.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new CategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title,
                }).ToListAsync();

                return await List;
            }
            else
            {  // await _emailSender.SendEmailAsync("khizar@sharjahbookfair.com", "New Hello World - Sending email using ASP.NET Core 2.2", "Test email trials message");
                var predicate = PredicateBuilder.True<XsiDownloadCategory>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiDownloadCategory.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new CategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.TitleAr,
                }).ToListAsync();

                return await List;
            }
        }

        // GET: api/DownloadCategory/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDTO>> GetXsiDownloadCategory(long id)
        {
            var xsiItem = await _context.XsiDownloadCategory.FindAsync(id);

            if (xsiItem == null)
            {
                return NotFound();
            }
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                CategoryDTO itemDTO = new CategoryDTO()
                {
                    ItemId = xsiItem.ItemId,
                    Title = xsiItem.Title,
                };
                return itemDTO;
            }
            else
            {
                CategoryDTO itemDTO = new CategoryDTO()
                {
                    ItemId = xsiItem.ItemId,
                    Title = xsiItem.TitleAr,
                };
                return itemDTO;
            }

        }

        private bool XsiDownloadCategoryExists(long id)
        {
            return _context.XsiDownloadCategory.Any(e => e.ItemId == id);
        }
    }
}

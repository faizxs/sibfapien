﻿using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using Xsi.ServicesLayer;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class DashboardController : Controller
    {
        private readonly sibfnewdbContext _context;
        public DashboardController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET api/Dashboard/5
        [HttpGet("{webid}")]
        public ActionResult<dynamic> Get(long webid)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    if (webid == 0)
                        webid = 1;
                    long websiteid = webid;
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    long memberid = User.Identity.GetID();

                    List<XsiExhibitionApplication> ExhibitionApplicationList = new List<XsiExhibitionApplication>();
                    XsiExhibition activeExhibition = new XsiExhibition();

                    bool isExhibitor = false;
                    bool isAgency = false;
                    bool isRestaurant = false;
                    var dtNow = MethodFactory.ArabianTimeNow();

                    string strstatus = EnumConversion.ToString(EnumExhibitionMemberStatus.Approved);
                    long exhibitionid = -1;
                    string strprefix = LangId == 1 ? "/en/" : "/ar/";
                    string strYes = EnumConversion.ToString(EnumBool.Yes);
                    string strNo = EnumConversion.ToString(EnumBool.No);

                    ApplicationRoleDTO item;
                    DashboardDTO dashboard = new DashboardDTO();

                    #region Dashboard Application Icons
                    List<ApplicationRoleDTO> memberApplicationIconsFromDb = new List<ApplicationRoleDTO>();
                    List<ApplicationRoleDTO> memberApplicationIcons = new List<ApplicationRoleDTO>();
                    //   List<long> IDs = new List<long>(); // { 1, 2, 4, 5, 6, 8 };

                    var predicate = PredicateBuilder.New<XsiExhibitionApplication>();
                    predicate = predicate.And(i => i.WebsiteId == webid || i.WebsiteId == 3);
                    predicate = predicate.And(i => i.IsActive == strYes);
                    // predicate = predicate.And(i => IDs.Contains(i.ApplicationId));

                    if (webid == 1)
                    {
                        predicate = predicate.And(i => i.IsActiveForSibf == strYes);
                    }
                    else if (webid == 2)
                    {
                        predicate = predicate.And(i => i.IsActiveForScrf == strYes);
                    }

                    if (LangId == 1)
                    {
                        memberApplicationIconsFromDb = _context.XsiExhibitionApplication.Where(predicate).OrderBy(i => i.SortOrder).
                       Select(i => new ApplicationRoleDTO() { ApplicationId = i.ApplicationId, ApplicationName = i.ApplicationName, Icon = i.Icon, URL = i.Url }).ToList();
                    }
                    else
                    {
                        memberApplicationIconsFromDb = _context.XsiExhibitionApplication.Where(predicate).OrderBy(i => i.SortOrder).
                       Select(i => new ApplicationRoleDTO() { ApplicationId = i.ApplicationId, ApplicationName = i.ApplicationNameAr, Icon = i.Icon, URL = i.Url }).ToList();
                    }

                    foreach (var iconitem in memberApplicationIconsFromDb)
                    {
                        if (iconitem.ApplicationId != 8)
                        {
                            iconitem.URL = strprefix + iconitem.URL;
                        }
                    }
                    #endregion

                    //if (LangId == 1)
                    //    activeExhibition = _context.XsiExhibition.Where(i => i.IsActive == strYes && i.IsArchive == strNo && i.WebsiteId == webid).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                    //else
                    //    activeExhibition = _context.XsiExhibition.Where(i => i.IsActiveAr == strYes && i.IsArchive == strNo && i.WebsiteId == webid).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (LangId == 1)
                        activeExhibition = _context.XsiExhibition.Where(i => i.IsActive == strYes && i.WebsiteId == webid).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                    else
                        activeExhibition = _context.XsiExhibition.Where(i => i.IsActiveAr == strYes && i.WebsiteId == webid).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (activeExhibition != null)
                    {
                        if (activeExhibition.WebsiteId != null)
                            websiteid = activeExhibition.WebsiteId.Value;
                        exhibitionid = activeExhibition.ExhibitionId;
                    }

                    var exhibitionmember = _context.XsiExhibitionMember.Where(i => i.MemberId == memberid).FirstOrDefault();
                    if (exhibitionmember != null)
                    {
                        List<XsiExhibitionMemberApplicationYearly> memberApplicationYearlyList = new List<XsiExhibitionMemberApplicationYearly>();
                        memberApplicationYearlyList = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberId == memberid && i.ExhibitionId == exhibitionid).ToList();
                        XsiExhibitionMemberApplicationYearly memberApplicationYearlyEntity = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberId == memberid && i.ExhibitionId == exhibitionid).FirstOrDefault();

                        if (exhibitionmember.IsSibfdepartment == strYes)
                        {
                            dashboard.MemberRoleId = 4;
                            memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId == 4 || i.ApplicationId == 13 || i.ApplicationId == 20).ToList();
                        }
                        else if (exhibitionmember.IsFoodSupplier == strYes)
                        {
                            dashboard.MemberRoleId = 8; //4
                                                        // memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId == 8).ToList();
                            if (memberApplicationYearlyEntity != null)
                            {
                                dashboard.RestaurantId = memberApplicationYearlyEntity.MemberExhibitionYearlyId;
                                dashboard.ExhibitorId = memberApplicationYearlyEntity.MemberExhibitionYearlyId;
                                if (memberApplicationYearlyEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval) || memberApplicationYearlyEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved))
                                {
                                    memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => (i.ApplicationId == 8 || i.ApplicationId == 9 || i.ApplicationId == 15 || i.ApplicationId == 16)).ToList();
                                }
                                else
                                {
                                    memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => (i.ApplicationId == 8 || i.ApplicationId == 16)).ToList();
                                }
                            }
                            else
                            {
                                memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => (i.ApplicationId == 8 || i.ApplicationId == 16)).ToList();
                            }
                        }
                        else if (exhibitionmember.IsLibraryMember == strYes)
                        {
                            dashboard.MemberRoleId = 4;
                            memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId == 7).ToList();
                        }
                        else
                        {
                            isExhibitor = memberApplicationYearlyList.Any(i => i.MemberId == memberid && i.MemberRoleId == 1 && i.ExhibitionId == exhibitionid);
                            isAgency = memberApplicationYearlyList.Any(i => i.MemberId == memberid && i.MemberRoleId == 2 && i.ExhibitionId == exhibitionid);
                            isRestaurant = memberApplicationYearlyList.Any(i => i.MemberId == memberid && i.MemberRoleId == 8 && i.ExhibitionId == exhibitionid);

                            //remove staffguest, staff guestbadge,
                            memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 4 && i.ApplicationId != 13 && i.ApplicationId != 20).ToList();

                            if (activeExhibition != null)
                            {
                                if (isExhibitor)
                                {
                                    memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 2 && i.ApplicationId != 8).ToList();
                                }
                                else
                                {
                                    if (!isExhibitor)
                                        if (dtNow > activeExhibition.ExhibitorEndDate)
                                            memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 1).ToList();
                                }

                                if (isAgency)
                                {
                                    if (!isExhibitor)
                                    {
                                        memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 1 && i.ApplicationId != 8 && i.ApplicationId != 3 && i.ApplicationId != 9).ToList();
                                    }
                                }
                                else
                                {
                                    if (!isAgency)
                                        if (dtNow > activeExhibition.AgencyEndDate)
                                            memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 2).ToList();
                                }

                                if (isRestaurant)
                                {
                                    memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 1 && i.ApplicationId != 2).ToList();
                                }
                                else
                                {
                                    if (!isRestaurant)
                                        if (dtNow > activeExhibition.RestaurantEndDate)
                                            memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 8).ToList();
                                }
                            }
                            else
                            {
                                memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 1 && i.ApplicationId != 2 && i.ApplicationId != 8).ToList();
                            }

                            #region Filter Roles
                            if (memberApplicationYearlyEntity != null && (exhibitionmember.IsSibfdepartment != strYes && exhibitionmember.IsFoodSupplier != strYes && exhibitionmember.IsLibraryMember != strYes))
                            {
                                if (isExhibitor)
                                {
                                    memberApplicationYearlyEntity = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberId == memberid && i.ExhibitionId == exhibitionid && i.MemberRoleId == 1).FirstOrDefault();
                                    dashboard.ExhibitorId = memberApplicationYearlyEntity.MemberExhibitionYearlyId;
                                    dashboard.RestaurantId = memberApplicationYearlyEntity.MemberExhibitionYearlyId;
                                }
                                else if (isAgency)
                                {
                                    memberApplicationYearlyEntity = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberId == memberid && i.ExhibitionId == exhibitionid && i.MemberRoleId == 2).FirstOrDefault();

                                    if (memberApplicationYearlyEntity.ParentId != null)
                                    {
                                        dashboard.ExhibitorId = memberApplicationYearlyEntity.ParentId.Value;
                                        dashboard.RestaurantId = memberApplicationYearlyEntity.ParentId.Value;
                                    }
                                }
                                else if (isRestaurant)
                                {
                                    memberApplicationYearlyEntity = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberId == memberid && i.ExhibitionId == exhibitionid && i.MemberRoleId == 8).FirstOrDefault();
                                    dashboard.ExhibitorId = memberApplicationYearlyEntity.MemberExhibitionYearlyId;
                                    dashboard.RestaurantId = memberApplicationYearlyEntity.MemberExhibitionYearlyId;
                                }

                                if (memberApplicationYearlyEntity.MemberRoleId != null)
                                    dashboard.MemberRoleId = memberApplicationYearlyEntity.MemberRoleId.Value;

                                if (memberApplicationYearlyEntity.MemberRoleId == 1) // exhibitor
                                {
                                    memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 2 && i.ApplicationId != 8).ToList();

                                    if (memberApplicationYearlyEntity.Status != EnumConversion.ToString(EnumExhibitorStatus.InitialApproval) && memberApplicationYearlyEntity.Status != EnumConversion.ToString(EnumExhibitorStatus.Approved))
                                    {
                                        //For Exhibitor: remove icons: representative, payment, books participating, myagency, add books,POS Device
                                        if (memberApplicationYearlyEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.New))
                                        {
                                            memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 3 && i.ApplicationId != 7 && i.ApplicationId != 9 && i.ApplicationId != 11 && i.ApplicationId != 22).ToList();
                                        }
                                        else
                                            memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 3 && i.ApplicationId != 7 && i.ApplicationId != 9 && i.ApplicationId != 10 && i.ApplicationId != 11 && i.ApplicationId != 12 && i.ApplicationId != 19 && i.ApplicationId != 22).ToList();
                                    }
                                }
                                else if (memberApplicationYearlyEntity.MemberRoleId == 2) // agency
                                {
                                    memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 1 && i.ApplicationId != 8 && i.ApplicationId != 22).ToList();
                                    if (memberApplicationYearlyEntity.Status == EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved) || memberApplicationYearlyEntity.Status == EnumConversion.ToString(EnumAgencyStatus.SIBFApproved))
                                    {
                                        //For Agency: agency registration, Books Participating, Add Books icon only, remove my agency //(i.ApplicationId == 2 || i.ApplicationId == 16 || i.ApplicationId == 10 || i.ApplicationId == 12)
                                        memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 7 && i.ApplicationId != 11).ToList();
                                    }
                                    else
                                    {
                                        memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 7 && i.ApplicationId != 11 && i.ApplicationId != 19).ToList();
                                    }
                                }
                                else if (memberApplicationYearlyEntity.MemberRoleId == 8) // restaurant
                                {
                                    if (memberApplicationYearlyEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval) || memberApplicationYearlyEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved))
                                    {
                                        memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => (i.ApplicationId == 8 || i.ApplicationId == 9 || i.ApplicationId == 15 || i.ApplicationId == 16 || i.ApplicationId == 19)).ToList();
                                    }
                                    else
                                    {
                                        memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => (i.ApplicationId == 8 || i.ApplicationId == 16 || i.ApplicationId == 19)).ToList();
                                    }

                                    //memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 1 && i.ApplicationId != 2 && i.ApplicationId != 11).ToList();

                                    //if (memberApplicationYearlyEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval) || memberApplicationYearlyEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved))
                                    //{
                                    //    //For restaurant: restaurant, payments, invoice enquiry icon only, remove my agency
                                    //    memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId == 8 && i.ApplicationId == 9 && i.ApplicationId != 11 && i.ApplicationId == 15).ToList();
                                    //}
                                }
                                else
                                {
                                    memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 1 && i.ApplicationId != 2 && i.ApplicationId != 8 && i.ApplicationId != 19 && i.ApplicationId != 22).ToList();
                                }
                            }
                            else
                            {
                                if (exhibitionmember.IsFoodSupplier == "N")
                                    memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 3 && i.ApplicationId != 8 && i.ApplicationId != 9 && i.ApplicationId != 10 && i.ApplicationId != 11 && i.ApplicationId != 12 && i.ApplicationId != 14 && i.ApplicationId != 15 && i.ApplicationId != 19 && i.ApplicationId != 22).ToList();
                                else
                                    memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 3 && i.ApplicationId != 9 && i.ApplicationId != 10 && i.ApplicationId != 11 && i.ApplicationId != 12 && i.ApplicationId != 14 && i.ApplicationId != 15 && i.ApplicationId != 19 && i.ApplicationId != 22).ToList();
                            }

                            //var memberreasonlist = _context.XsiMemberReason.Where(i => i.MemberId == memberid).ToList();
                            //if (memberreasonlist.Where(i => i.ReasonId == 5).ToList().Count == 0) //IF NOT REGISTERED FOR PC
                            //{
                            //    memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 5).ToList();
                            //}

                            if (dashboard.MemberRoleId != 4)
                            {
                                var TGListYears = MethodFactory.GetLatestTranslationGrants();
                                if (TGListYears.Count == 3)
                                {
                                    foreach (var item1 in memberApplicationIconsFromDb)
                                    {
                                        if (item1.ApplicationId == 6)
                                        {
                                            item1.ApplicationName = MethodFactory.TranslationGrant(TGListYears, LangId, 6);
                                            item1.URL = strprefix + MethodFactory.GetApplicationURL(6, TGListYears[0].ItemId);
                                        }
                                        if (item1.ApplicationId == 17)
                                        {
                                            item1.ApplicationName = MethodFactory.TranslationGrant(TGListYears, LangId, 17);
                                            item1.URL = strprefix + MethodFactory.GetApplicationURL(17, TGListYears[1].ItemId);
                                        }
                                        if (item1.ApplicationId == 18)
                                        {
                                            item1.ApplicationName = MethodFactory.TranslationGrant(TGListYears, LangId, 18);
                                            item1.URL = strprefix + MethodFactory.GetApplicationURL(18, TGListYears[2].ItemId);
                                        }
                                    }
                                }
                                else if (TGListYears.Count == 2)
                                {
                                    memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 18).ToList();
                                    foreach (var item1 in memberApplicationIconsFromDb)
                                    {
                                        if (item1.ApplicationId == 6)
                                        {
                                            item1.ApplicationName = MethodFactory.TranslationGrant(TGListYears, LangId, 6);
                                            item1.URL = strprefix + MethodFactory.GetApplicationURL(6, TGListYears[0].ItemId);
                                        }
                                        if (item1.ApplicationId == 17)
                                        {
                                            item1.ApplicationName = MethodFactory.TranslationGrant(TGListYears, LangId, 17);
                                            item1.URL = strprefix + MethodFactory.GetApplicationURL(17, TGListYears[1].ItemId);
                                        }
                                    }
                                    // memberApplicationIcons.Single(i => i.ApplicationId == 17).ApplicationName = MethodFactory.TranslationGrant(TGListYears, LangId, 17);
                                    //memberApplicationIcons.Single(i => i.ApplicationId == 17).URL = strprefix + MethodFactory.GetApplicationURL(17, TGListYears[1].ItemId);

                                    //item = new ApplicationRoleDTO();
                                    //item.ApplicationId = 17;
                                    //item.URL = strprefix + MethodFactory.GetApplicationURL(17, TGListYears[1].ItemId);
                                    //item.ApplicationName = MethodFactory.TranslationGrant(TGListYears, LangId, 17);
                                    //memberApplicationIcons.Add(item);
                                }
                                else if (TGListYears.Count == 1 && memberApplicationIconsFromDb.Any(i => i.ApplicationId == 6))
                                {
                                    memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => i.ApplicationId != 17 && i.ApplicationId != 18).ToList();
                                    foreach (var item1 in memberApplicationIconsFromDb)
                                    {
                                        if (item1.ApplicationId == 6)
                                        {
                                            item1.ApplicationName = MethodFactory.TranslationGrant(TGListYears, LangId, 6);
                                            item1.URL = strprefix + MethodFactory.GetApplicationURL(6, TGListYears[0].ItemId);
                                        }
                                    }
                                }
                            }
                            #endregion
                        }


                        List<long> itemstoremoveIDs = new List<long>();
                        List<long> itemstoAddIDs = new List<long>();

                        if (exhibitionmember.IsPcorTg == EnumConversion.ToString(EnumBool.Yes))
                        {
                            var ProfessionalProgramEntity = _context.XsiExhibitionProfessionalProgram.Where(i => i.IsActive == "Y" && i.ExhibitionId == exhibitionid).OrderByDescending(i => i.ProfessionalProgramId).FirstOrDefault();
                            if (ProfessionalProgramEntity != null)
                            {
                                var registration = _context.XsiExhibitionProfessionalProgramRegistration.Where(i => i.IsActive == "Y" && i.IsCheckedIn == "Y" && i.MemberId == exhibitionmember.MemberId && i.ProfessionalProgramId == ProfessionalProgramEntity.ProfessionalProgramId).OrderByDescending(i => i.ItemId).FirstOrDefault();
                                if (registration == null)
                                {
                                    itemstoremoveIDs.Add(18);
                                }
                                if (exhibitionmember.IsActivateForTg != EnumConversion.ToString(EnumBool.Yes))
                                {
                                    itemstoremoveIDs.Add(6);
                                    itemstoremoveIDs.Add(17);

                                }
                            }
                        }
                        else
                        {
                            itemstoremoveIDs.Add(5);
                            if (exhibitionmember.IsActivateForTg != EnumConversion.ToString(EnumBool.Yes))
                            {
                                itemstoremoveIDs.Add(6);
                                itemstoremoveIDs.Add(17);
                                itemstoremoveIDs.Add(18);
                            }
                        }

                        foreach (var obj in memberApplicationIconsFromDb)
                        {
                            obj.WebsiteId = websiteid;

                            if (obj.ApplicationId == 5)
                                obj.URL = "/en/" + MethodFactory.GetURLforPP(memberid);
                        }

                        memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => !itemstoremoveIDs.Contains(i.ApplicationId)).ToList();

                        if (MethodFactory.IsValidForRegistrationFromPreviousPayments(activeExhibition.ExhibitionId, memberid))
                        {
                            memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => !itemstoremoveIDs.Contains(i.ApplicationId)).ToList();
                        }
                        else
                        {
                            List<long> paymentdueitemstoremoveIDs = new List<long>();
                            paymentdueitemstoremoveIDs.Add(3); //Representative
                            paymentdueitemstoremoveIDs.Add(9); //Payment
                            paymentdueitemstoremoveIDs.Add(10); //Participating Books
                            paymentdueitemstoremoveIDs.Add(11); //My Agency
                            paymentdueitemstoremoveIDs.Add(12); //Add Books
                            paymentdueitemstoremoveIDs.Add(14); //Exhibitors Badges Registration
                            paymentdueitemstoremoveIDs.Add(15); //Invoice Enquiry
                            paymentdueitemstoremoveIDs.Add(19); //Shipment
                            paymentdueitemstoremoveIDs.Add(22); //POS Device
                            memberApplicationIconsFromDb = memberApplicationIconsFromDb.Where(i => !paymentdueitemstoremoveIDs.Contains(i.ApplicationId)).ToList();
                        }
                    }
                    if (memberApplicationIconsFromDb.Count == 0)
                        dashboard.ApplicationRoleDTO = new List<ApplicationRoleDTO>();
                    else
                    {
                        dashboard.ApplicationRoleDTO = memberApplicationIconsFromDb;

                        if (dashboard.ExhibitorId > 0)
                        {
                            foreach (var itemicon in memberApplicationIconsFromDb)
                            {
                                if (itemicon.ApplicationId == 9)
                                {
                                    itemicon.ExhibitorId = dashboard.ExhibitorId;
                                }
                            }
                        }
                        if (dashboard.RestaurantId > 0)
                        {
                            foreach (var itemicon in memberApplicationIconsFromDb)
                            {
                                if (itemicon.ApplicationId == 9)
                                {
                                    itemicon.RestaurantId = dashboard.RestaurantId;
                                }
                            }
                        }
                    }
                    return Ok(dashboard);
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        // GET api/Dashboard/Main/1
        [HttpGet]
        [Route("Main/{webid}")]
        public ActionResult<dynamic> DashboardMainForDisplayStatus(long webid)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    if (webid == 0)
                        webid = 1;
                    long websiteid = webid;
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    List<XsiExhibitionApplication> ExhibitionApplicationList = new List<XsiExhibitionApplication>();
                    XsiExhibition activeExhibition = new XsiExhibition();

                    string strprefix = LangId == 1 ? "/en/" : "/ar/";
                    string strYes = EnumConversion.ToString(EnumBool.Yes);
                    string strNo = EnumConversion.ToString(EnumBool.No);
                    List<DashboardStatus> List = new List<DashboardStatus>();

                    ApplicationRoleMainDTO item;
                    MainDashboardDTO dashboard = new MainDashboardDTO();
                    long memberid = User.Identity.GetID();
                    dashboard.MemberId = memberid;

                    List<ApplicationRoleMainDTO> memberApplicationRoles = new List<ApplicationRoleMainDTO>();
                    List<long> IDs = new List<long>() { 1, 2, 4, 5, 6, 8 };

                    if (webid == 1)
                    {
                        ExhibitionApplicationList = _context.XsiExhibitionApplication.Where(i => (i.WebsiteId == webid || i.WebsiteId == 3) && i.IsActive == strYes && i.IsActiveForSibf == strYes).ToList();
                    }
                    else if (webid == 2)
                    {
                        ExhibitionApplicationList = _context.XsiExhibitionApplication.Where(i => (i.WebsiteId == webid || i.WebsiteId == 3) && i.IsActive == strYes && i.IsActiveForScrf == strYes).ToList();
                    }
                    IDs = ExhibitionApplicationList.Select(i => i.ApplicationId).ToList();

                    bool isExhibitor = false;
                    bool isAgency = false;
                    bool isRestaurant = false;
                    var dtNow = MethodFactory.ArabianTimeNow();

                    var predicate = PredicateBuilder.New<XsiExhibitionApplication>();
                    predicate = predicate.And(i => IDs.Contains(i.ApplicationId));

                    if (LangId == 1)
                    {
                        memberApplicationRoles = _context.XsiExhibitionApplication.Where(predicate).
                       Select(i => new ApplicationRoleMainDTO() { ApplicationId = i.ApplicationId, ApplicationName = i.ApplicationName })
                       .ToList();
                    }
                    else
                    {
                        memberApplicationRoles = _context.XsiExhibitionApplication.Where(predicate).
                     Select(i => new ApplicationRoleMainDTO() { ApplicationId = i.ApplicationId, ApplicationName = i.ApplicationNameAr }).ToList();
                    }

                    //if (LangId == 1)
                    //    activeExhibition = _context.XsiExhibition.Where(i => i.IsActive == strYes && i.IsArchive == strNo && i.WebsiteId == webid).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                    //else
                    //    activeExhibition = _context.XsiExhibition.Where(i => i.IsActiveAr == strYes && i.IsArchive == strNo && i.WebsiteId == webid).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (LangId == 1)
                        activeExhibition = _context.XsiExhibition.Where(i => i.IsActive == strYes && i.WebsiteId == webid).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                    else
                        activeExhibition = _context.XsiExhibition.Where(i => i.IsActiveAr == strYes && i.WebsiteId == webid).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (activeExhibition != null)
                    {
                        dashboard.ExhibitionId = activeExhibition.ExhibitionId;
                        if (activeExhibition.WebsiteId != null)
                            websiteid = activeExhibition.WebsiteId.Value;
                        string strstatus = EnumConversion.ToString(EnumExhibitionMemberStatus.Approved);
                        long exhibitionid = activeExhibition.ExhibitionId;

                        var exhibitionmember = _context.XsiExhibitionMember.Where(i => i.MemberId == memberid).FirstOrDefault();
                        if (exhibitionmember != null && exhibitionmember.IsSibfdepartment == strYes)
                        {
                            dashboard.MemberRoleId = 4;
                            memberApplicationRoles = memberApplicationRoles.Where(i => i.ApplicationId == 4 || i.ApplicationId == 13 || i.ApplicationId == 20).ToList();
                            //memberApplicationRoles = memberApplicationRoles.Where(i => i.ApplicationId == 4).ToList();

                            //item = new ApplicationRoleDTO();
                            //item.ApplicationId = 13;
                            //item.URL = strprefix + GetApplicationURL(13);
                            //item.ApplicationName = LangId == 1 ? "Badge" : "بطاقات الدخول";
                            //memberApplicationRoles.Add(item);
                        }
                        else if (exhibitionmember != null && exhibitionmember.IsFoodSupplier == strYes)
                        {
                            dashboard.MemberRoleId = 4;
                            memberApplicationRoles = memberApplicationRoles.Where(i => i.ApplicationId == 8).ToList();
                        }
                        else
                        {
                            isExhibitor = _context.XsiExhibitionMemberApplicationYearly.Any(i => i.MemberId == memberid && i.MemberRoleId == 1 && i.ExhibitionId == exhibitionid);
                            isAgency = _context.XsiExhibitionMemberApplicationYearly.Any(i => i.MemberId == memberid && i.MemberRoleId == 2 && i.ExhibitionId == exhibitionid);
                            isRestaurant = _context.XsiExhibitionMemberApplicationYearly.Any(i => i.MemberId == memberid && i.MemberRoleId == 8 && i.ExhibitionId == exhibitionid);

                            memberApplicationRoles = memberApplicationRoles.Where(i => i.ApplicationId != 4 && i.ApplicationId != 13 && i.ApplicationId != 20).ToList();
                            if (isExhibitor)
                            {
                                memberApplicationRoles = memberApplicationRoles.Where(i => i.ApplicationId != 2 && i.ApplicationId != 8).ToList();
                            }
                            else
                            {
                                if (!isExhibitor)
                                    if (dtNow > activeExhibition.ExhibitorEndDate)
                                        memberApplicationRoles = memberApplicationRoles.Where(i => i.ApplicationId != 1).ToList();
                            }

                            if (isAgency)
                            {
                                if (!isExhibitor)
                                {
                                    memberApplicationRoles = memberApplicationRoles.Where(i => i.ApplicationId != 1 && i.ApplicationId != 8).ToList();
                                }
                            }
                            else
                            {
                                if (!isAgency)
                                    if (dtNow > activeExhibition.AgencyEndDate)
                                        memberApplicationRoles = memberApplicationRoles.Where(i => i.ApplicationId != 2).ToList();
                            }

                            if (isRestaurant)
                            {
                                memberApplicationRoles = memberApplicationRoles.Where(i => i.ApplicationId != 1 && i.ApplicationId != 2).ToList();
                            }
                            else
                            {
                                if (!isRestaurant)
                                    if (dtNow > activeExhibition.RestaurantEndDate)
                                        memberApplicationRoles = memberApplicationRoles.Where(i => i.ApplicationId != 8).ToList();
                            }
                        }
                        XsiExhibitionMemberApplicationYearly memberApplicationYearly = new XsiExhibitionMemberApplicationYearly();
                        //XsiExhibitionMemberApplicationYearly memberApplicationYearly = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberId == memberid && i.ExhibitionId == exhibitionid).FirstOrDefault();
                        memberApplicationYearly = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberId == memberid && i.ExhibitionId == exhibitionid).AsNoTracking().OrderByDescending(i => i.MemberExhibitionYearlyId).FirstOrDefault();

                        if (isExhibitor)
                        {
                            memberApplicationYearly = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberId == memberid && i.ExhibitionId == exhibitionid && i.MemberRoleId == 1).FirstOrDefault();
                            dashboard.ExhibitorId = memberApplicationYearly.MemberExhibitionYearlyId;
                        }
                        else if (isAgency)
                        {
                            memberApplicationYearly = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberId == memberid && i.ExhibitionId == exhibitionid && i.MemberRoleId == 2).FirstOrDefault();
                            if (memberApplicationYearly.ParentId != null)
                            {
                                dashboard.ExhibitorId = memberApplicationYearly.ParentId.Value;
                                dashboard.AgencyId = memberApplicationYearly.MemberExhibitionYearlyId;
                            }
                        }
                        else if (isRestaurant)
                        {
                            memberApplicationYearly = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberId == memberid && i.ExhibitionId == exhibitionid && i.MemberRoleId == 8).FirstOrDefault();
                            dashboard.ExhibitorId = memberApplicationYearly.MemberExhibitionYearlyId;
                        }
                        if (memberApplicationYearly != null && memberApplicationYearly != default(XsiExhibitionMemberApplicationYearly))
                        {
                            if (memberApplicationYearly.MemberRoleId != null)
                                dashboard.MemberRoleId = memberApplicationYearly.MemberRoleId.Value;

                            if (memberApplicationYearly.MemberRoleId == 1) // exhibitor
                                memberApplicationRoles = memberApplicationRoles
                                    .Where(i => i.ApplicationId != 2 && i.ApplicationId != 8).ToList();
                            else if (memberApplicationYearly.MemberRoleId == 2) // agency
                                memberApplicationRoles = memberApplicationRoles
                                    .Where(i => i.ApplicationId != 1 && i.ApplicationId != 8).ToList();
                            else if (memberApplicationYearly.MemberRoleId == 8) // restaurant
                                memberApplicationRoles = memberApplicationRoles
                                    .Where(i => i.ApplicationId != 1 && i.ApplicationId != 2).ToList();
                            else
                                memberApplicationRoles = memberApplicationRoles
                                    .Where(i => i.ApplicationId != 1 && i.ApplicationId != 2 && i.ApplicationId != 8).ToList();


                            /*if exhibitor is approved show other icons*/
                            if ((memberApplicationYearly.MemberRoleId == 1) && (memberApplicationYearly.Status == "I" || memberApplicationYearly.Status == "A"))
                            {
                                item = new ApplicationRoleMainDTO();
                                item.ApplicationId = 12;
                                item.URL = strprefix + MethodFactory.GetApplicationURL(12);
                                item.ApplicationName = LangId == 1 ? "Add Books" : "إضافة كتب";
                                memberApplicationRoles.Add(item);

                                item = new ApplicationRoleMainDTO();
                                item.ApplicationId = 10;
                                item.URL = strprefix + MethodFactory.GetApplicationURL(10);
                                item.ApplicationName = LangId == 1 ? "Books Participating" : "الكتب المشاركة";
                                memberApplicationRoles.Add(item);

                                item = new ApplicationRoleMainDTO();
                                item.ApplicationId = 11;
                                item.URL = strprefix + MethodFactory.GetApplicationURL(11);
                                item.ApplicationName = LangId == 1 ? "My Agency" : "التوكيلات";
                                memberApplicationRoles.Add(item);

                                item = new ApplicationRoleMainDTO();
                                item.ApplicationId = 3;
                                item.URL = strprefix + MethodFactory.GetApplicationURL(3);
                                item.ApplicationName = LangId == 1 ? "Representative" : "المندوبين";
                                memberApplicationRoles.Add(item);

                                item = new ApplicationRoleMainDTO();
                                item.ApplicationId = 9;

                                if (dashboard.ExhibitorId > 0)
                                {
                                    item.ExhibitorId = dashboard.ExhibitorId;
                                }

                                item.URL = strprefix + MethodFactory.GetApplicationURL(9);
                                item.ApplicationName = LangId == 1 ? "Payments" : "السداد";
                                memberApplicationRoles.Add(item);

                                //item = new ApplicationRoleDTO();
                                //item.ApplicationId = 14;
                                //item.URL = strprefix + GetApplicationURL(14);
                                //item.ApplicationName = LangId == 1 ? "Exhibitors Badges Registration" : "التسجيل لبطاقات دخول العارضين";
                                //memberApplicationRoles.Add(item);

                                //item = new ApplicationRoleDTO();
                                //item.ApplicationId = 15;
                                //item.URL = strprefix + GetApplicationURL(15);
                                //item.ApplicationName = LangId == 1 ? "Invoice Enquiry" : "Invoice Enquiry";
                                //memberApplicationRoles.Add(item);
                            }
                            else if ((memberApplicationYearly.MemberRoleId == 8) && (memberApplicationYearly.Status == "I" || memberApplicationYearly.Status == "A"))
                            {
                                item = new ApplicationRoleMainDTO();
                                item.ApplicationId = 9;

                                if (dashboard.ExhibitorId > 0)
                                {
                                    item.ExhibitorId = dashboard.ExhibitorId;
                                }

                                item.URL = strprefix + MethodFactory.GetApplicationURL(9);
                                item.ApplicationName = LangId == 1 ? "Payments" : "السداد";
                                memberApplicationRoles.Add(item);

                                //item = new ApplicationRoleDTO();
                                //item.ApplicationId = 15;
                                //item.URL = strprefix + GetApplicationURL(15);
                                //item.ApplicationName = LangId == 1 ? "Invoice Enquiry" : "Invoice Enquiry";
                                //memberApplicationRoles.Add(item);
                            }
                            else if (memberApplicationYearly.MemberRoleId == 2 && (memberApplicationYearly.Status == "E" || memberApplicationYearly.Status == "A"))
                            {
                                item = new ApplicationRoleMainDTO();
                                item.ApplicationId = 12;
                                item.URL = strprefix + MethodFactory.GetApplicationURL(12);
                                item.ApplicationName = LangId == 1 ? "Add Books" : "إضافة كتب";
                                memberApplicationRoles.Add(item);

                                item = new ApplicationRoleMainDTO();
                                item.ApplicationId = 10;
                                item.URL = strprefix + MethodFactory.GetApplicationURL(10);
                                item.ApplicationName = LangId == 1 ? "Books Participating" : "الكتب المشاركة";
                                memberApplicationRoles.Add(item);
                            }
                            else
                            {
                                memberApplicationRoles = memberApplicationRoles.Where(i => i.ApplicationId != 3 && i.ApplicationId != 9 && i.ApplicationId != 10 && i.ApplicationId != 11 && i.ApplicationId != 12 && i.ApplicationId != 14 && i.ApplicationId != 15 && i.ApplicationId != 19 && i.ApplicationId != 22).ToList();
                            }
                        }
                        else
                        {
                            // memberApplicationRoles= memberApplicationRoles.Where(i => i.ApplicationId != 3 && i.ApplicationId != 9 && i.ApplicationId != 10 && i.ApplicationId != 11 && i.ApplicationId != 12).ToList();
                            memberApplicationRoles = memberApplicationRoles.Where(i => i.ApplicationId != 3 && i.ApplicationId != 9 && i.ApplicationId != 10 && i.ApplicationId != 11 && i.ApplicationId != 12 && i.ApplicationId != 14 && i.ApplicationId != 15 && i.ApplicationId != 19 && i.ApplicationId != 22).ToList();
                        }

                        if (websiteid == 1)
                        {
                            //To Hide ICONS temporary for SIBF uncomment the following
                            List<long> applicationIDs = new List<long>();
                            //applicationIDs.Add(1); //Exhibitor
                            //applicationIDs.Add(2); //Agency
                            //applicationIDs.Add(8); //Resaurant
                            // applicationIDs.Add(9);  //payments
                            //  applicationIDs.Add(10); //books participating
                            // applicationIDs.Add(12);
                            // applicationIDs.Add(11); //my agency
                            //  applicationIDs.Add(4);

                            memberApplicationRoles = memberApplicationRoles.Where(i => !applicationIDs.Contains(i.ApplicationId)).ToList();
                        }
                       
                        if (isExhibitor)
                        {
                            if (!memberApplicationRoles.Any(i => i.ApplicationId == 1))
                            {
                                item = new ApplicationRoleMainDTO();
                                item.ApplicationId = 1;
                                item.URL = strprefix + MethodFactory.GetApplicationURL(1);
                                item.ApplicationName = LangId == 1 ? "Exhibitor" : "التسجيل كعارض";
                                memberApplicationRoles.Add(item);
                            }
                        }
                        else if (isAgency)
                        {
                            if (!memberApplicationRoles.Any(i => i.ApplicationId == 2))
                            {
                                item = new ApplicationRoleMainDTO();
                                item.ApplicationId = 2;
                                item.URL = strprefix + MethodFactory.GetApplicationURL(2);
                                item.ApplicationName = LangId == 1 ? "Agency" : "التسجيل كتوكيل";
                                memberApplicationRoles.Add(item);
                            }
                        }

                        DashboardStatus DashboardStatusObj;
                        foreach (var obj in memberApplicationRoles)
                        {
                            obj.Icon = MethodFactory.GetApplicationIcon(obj.ApplicationId);
                            if (obj.ApplicationId == 5)
                                obj.URL = "/en/" + MethodFactory.GetURLforPP(memberid);
                            else
                                obj.URL = strprefix + obj.URL;
                            obj.WebsiteId = websiteid;

                            DashboardStatusObj = new DashboardStatus();
                            DashboardStatusObj = MethodFactory.GetStatusUpdateInfo(obj.ApplicationId, dashboard, DashboardStatusObj, LangId);

                            if (DashboardStatusObj != null && DashboardStatusObj.Status != null && !string.IsNullOrEmpty(DashboardStatusObj.Status))
                                List.Add(DashboardStatusObj);
                        }


                        if (dashboard.MemberRoleId != 4)
                        {
                            if (websiteid == 1)
                            {
                                var tgentityApplicationRole = memberApplicationRoles.Where(i => i.ApplicationId == 6).FirstOrDefault();
                                if (tgentityApplicationRole != null)
                                {
                                    var TGListYears = MethodFactory.GetLatestTranslationGrants();
                                    if (TGListYears.Count == 3)
                                    {
                                        DashboardStatusObj = new DashboardStatus();
                                        DashboardStatusObj = MethodFactory.GetStatusUpdateInfo(17, dashboard, DashboardStatusObj, LangId);
                                        List.Add(DashboardStatusObj);

                                        DashboardStatusObj = new DashboardStatus();
                                        DashboardStatusObj = MethodFactory.GetStatusUpdateInfo(18, dashboard, DashboardStatusObj, LangId);
                                        List.Add(DashboardStatusObj);
                                    }
                                    else if (TGListYears.Count == 2)
                                    {
                                        DashboardStatusObj = new DashboardStatus();
                                        DashboardStatusObj = MethodFactory.GetStatusUpdateInfo(17, dashboard, DashboardStatusObj, LangId);
                                        List.Add(DashboardStatusObj);
                                    }
                                    else if (TGListYears.Count == 1)
                                    {
                                        DashboardStatusObj = new DashboardStatus();
                                        DashboardStatusObj = MethodFactory.GetStatusUpdateInfo(6, dashboard, DashboardStatusObj, LangId);
                                        List.Add(DashboardStatusObj);
                                    }
                                }
                            }
                        }
                    }

                    dashboard.StatusList = List;
                    dashboard.ApplicationRoleDTO = memberApplicationRoles;

                    return Ok(dashboard);
                }
            }
            catch (System.Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using SIBFAPIEn.Utility;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Account.Manage.Internal;
using LinqKit;
using System.IO;
using Contracts;
using Xsi.ServicesLayer;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PaymentMobileController : ControllerBase
    {

        private readonly AppCustomSettings _appCustomSettings;
        EnumResultType XsiResult { get; set; }
        #region Properties
        readonly string VisaPath = "Representative\\Visa\\";
        readonly string VisaFormPath = "Representative\\VisaForm\\";
        private ILoggerManager _logger;
        #endregion

        private readonly IEmailSender _emailSender;
        public PaymentMobileController(IEmailSender emailSender, IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger)
        {
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
        }
        //[Route("{websiteid}/{memberroleid}")]
        //public async Task<ActionResult<dynamic>> GetPayment(long websiteid, long memberroleid)
        // GET: api/GetPayment
        [HttpGet]
        [Route("{websiteid}/{memberId}")]
        public async Task<ActionResult<dynamic>> GetPayment(long websiteid, long memberId)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    //long memberid = User.Identity.GetID();
                    List<PaymentMobileDTO> model = new List<PaymentMobileDTO>();

                    if (IsExhibitionActive(websiteid))
                    {
                        XsiExhibitionMemberApplicationYearly ExhibitorEntity = MethodFactory.GetExhibitor(memberId, websiteid, _context, 1);//memberroleid
                        if (ExhibitorEntity != null && ExhibitorEntity.Status != null)
                        {
                            //if (ExhibitorEntity.CountryId != null)
                            //    model.CountryId = ExhibitorEntity.CountryId;

                            if (ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved) || ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval))
                            {
                                if (AtleastOneInvoice(ExhibitorEntity.MemberExhibitionYearlyId, _context))
                                {
                                    // ExhibitorEntity.MemberExhibitionYearlyId;
                                    BindListing(model, LangId, ExhibitorEntity.MemberExhibitionYearlyId);
                                    //GetOverviewContent(model, LangId, ExhibitorEntity.MemberExhibitionYearlyId);
                                    //UpdateStatusChange(ExhibitorEntity.MemberExhibitionYearlyId);
                                }
                                //else
                                //    model.ReturnURL = "/en/MemberDashboard";
                            }
                            //else
                            //    model.ReturnURL = "/en/MemberDashboard";
                        }
                        //else
                        //    model.ReturnURL = "/en/MemberDashboard";
                    }
                    else
                    {
                        //model.ReturnURL = "/en/MemberDashboard";
                    }
                    return Ok(model);
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }

        }

        [HttpGet]
        [Route("invoice/{invoiceid}")]
        public async Task<ActionResult<dynamic>> GetInvoice(long invoiceid)
        {
            try
            {
                string PaymentLastDate = string.Empty;
                string PaymentRemainingDays = string.Empty;
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                string strcurrency = (LangId == 1) ? " AED" : " درهم إماراتي ";
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    if (invoiceid != 0)
                    {
                        dynamic invoice;
                        if (LangId == 1)
                            invoice = new InvoiceDetailsMobileDTO();
                        else
                            invoice = new InvoiceDetailsMobileArDTO();
                        XsiInvoice entity = _context.XsiInvoice.Where(x => x.ItemId == invoiceid).FirstOrDefault();
                        if (entity != null)
                        {
                            //IsExhibitionActive(1);
                            if (entity.Status != null)
                                if (entity.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingPayment))
                                {
                                    entity.IsNewPending = EnumConversion.ToString(EnumBool.No);
                                    _context.SaveChanges();
                                }

                            XsiExhibitionMemberApplicationYearly exhibitor = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == entity.ExhibitorId).FirstOrDefault();
                            var exhibition = _context.XsiExhibition.Where(i => i.ExhibitionId == exhibitor.ExhibitionId).FirstOrDefault();
                            #region InvoiceEntity

                            invoice.AllocatedSpace = entity.AllocatedSpace;
                            invoice.ParticipationFees = (entity.ParticipationFee ?? "0") + strcurrency;
                            invoice.AllocatedSpaceFee = (entity.StandFee ?? "0") + strcurrency;

                            invoice.DueFromLastYearCredit = (entity.DueFromLastYearCredit ?? "0") + strcurrency;
                            invoice.DueFromLastYearDebit = (entity.DueFromLastYearDebit ?? "0") + strcurrency;
                            invoice.Discount = (entity.Discount ?? "0") + strcurrency;
                            invoice.Penalties = (entity.Penalty ?? "0") + strcurrency;
                            invoice.TotalInAED = (entity.TotalAed ?? "0") + strcurrency;
                            //invoice.TotalInUSD = entity.TotalUsd;
                            invoice.TotalPaid = (entity.PaidAmount ?? "0") + strcurrency;

                            string strBalanceAmount = string.Empty;
                            if (entity.TotalAed != null && entity.PaidAmount != null)
                            {
                                string strTotalAED = entity.TotalAed;
                                string strPaid = entity.PaidAmount;
                                if (!string.IsNullOrEmpty(strTotalAED) && !string.IsNullOrEmpty(strPaid))
                                    strBalanceAmount = (Convert.ToDouble(strTotalAED) - Convert.ToDouble(strPaid)).ToString();
                                else
                                {
                                    if (!string.IsNullOrEmpty(strTotalAED))
                                        strBalanceAmount = strTotalAED;
                                    else if (!string.IsNullOrEmpty(strPaid))
                                        strBalanceAmount = "-" + strPaid;
                                }
                            }
                            else
                            {
                                if (entity.TotalAed != null)
                                {
                                    if (!string.IsNullOrEmpty(entity.TotalAed))
                                        strBalanceAmount = entity.TotalAed;
                                }
                                else if (entity.PaidAmount != null)
                                    if (!string.IsNullOrEmpty(entity.PaidAmount))
                                        strBalanceAmount = "-" + entity.PaidAmount;
                            }
                            invoice.TotalBalance = (strBalanceAmount ?? "0") + strcurrency;
                            XsiExhibition exhibitionnew = MethodFactory.GetActiveExhibition(exhibition.WebsiteId.Value, _context);
                            if (exhibitionnew != null)
                            {
                                if (exhibitionnew.DateOfDue != null)
                                {
                                    PaymentLastDate = exhibitionnew.DateOfDue.Value.ToString("dddd d MMMM yyyy");
                                    if (exhibitionnew.DateOfDue > MethodFactory.ArabianTimeNow())
                                        PaymentRemainingDays = Math.Ceiling(exhibitionnew.DateOfDue.Value.Subtract(MethodFactory.ArabianTimeNow()).TotalDays).ToString();
                                    else
                                        PaymentRemainingDays = "0";
                                }
                            }
                            #endregion
                            return Ok(invoice);
                        }
                        return Ok("Invoice not found.");
                    }
                    else
                    {
                        return Ok("Invoice not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }

        }

        #region Private Methods
        private bool IsExhibitionActive(long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibition entity = MethodFactory.GetActiveExhibition(websiteId, _context);
                if (entity != null)
                {
                    //  ExhibitionId = entity.ExhibitionId;
                    //if (entity.ParticipationFee != null)
                    //    TotalParticipationFee = entity.ParticipationFee;
                    //if (entity.PriceSqM != null)
                    //    PriceSqMeter = Convert.ToDouble(entity.PriceSqM);
                    //if (entity.ArabicAgencyFees != null)
                    //    ArabicAgencyFees = Convert.ToDouble(entity.ArabicAgencyFees);
                    //if (entity.ForeignAgencyFees != null)
                    //    ForeignAgencyFee = Convert.ToDouble(entity.ForeignAgencyFees);
                    //if (entity.VisaProcessingPrice != null)
                    //    RepresentativeFee = Convert.ToDouble(entity.VisaProcessingPrice);
                    #region Endate
                    if (entity.DateOfDue != null)
                    {
                        //  PaymentLastDate = entity.DateOfDue.Value.ToString("dddd d MMMM yyyy");
                        //if (entity.DateOfDue > MethodFactory.ArabianTimeNow())
                        //    PaymentRemainingDays = Math.Ceiling(entity.DateOfDue.Value.Subtract(MethodFactory.ArabianTimeNow()).TotalDays).ToString();
                        //else
                        //    PaymentRemainingDays = "0";
                    }
                    #endregion
                    return true;
                }
                return false;
            }
        }
        private void BindListing(List<PaymentMobileDTO> model, long langId, long exhibitorid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string strcurrency = (langId == 1) ? " AED" : " درهم إماراتي ";
                var predicate = PredicateBuilder.New<XsiInvoice>();
                predicate = predicate.And(i => i.ExhibitorId == exhibitorid);
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                List<XsiInvoice> invoiceList = _context.XsiInvoice.Where(predicate).ToList();

                if (invoiceList.Count() > 0)
                {
                    #region Tab1 Listing
                    string strPendingPayment = EnumConversion.ToString(EnumExhibitorStatus.PendingPayment);
                    string strReceiptUploaded = EnumConversion.ToString(EnumExhibitorStatus.ReceiptUploaded);
                    string strReceiptReviewed = EnumConversion.ToString(EnumExhibitorStatus.PaymentInProcess);

                    #endregion
                    #region Tab2 Listing
                    string strPaid = EnumConversion.ToString(EnumExhibitorStatus.Paid);
                    string strPartiallyPaid = EnumConversion.ToString(EnumExhibitorStatus.PartiallyPaid);
                    var InvoiceList = invoiceList.Where(p => p.Status == strPaid || p.Status == strPartiallyPaid || p.Status == strPendingPayment || p.Status == strReceiptUploaded || p.Status == strReceiptReviewed).ToList();
                    List<PaidPaymentDTO> PaymentList = new List<PaidPaymentDTO>();
                    if (langId == 1)
                    {

                        PaymentList = InvoiceList.Select(s => new PaidPaymentDTO
                        {
                            ItemId = s.ItemId,
                            InvoiceNumber = s.InvoiceNumber,
                            IsActive = s.IsActive,
                            ExhibitorId = s.ExhibitorId,
                            TotalAed = (s.TotalAed ?? "0") + strcurrency,
                            CreatedOn = s.CreatedOn.HasValue ? GetDateFormated(s.CreatedOn.Value, langId) : string.Empty,
                            //CreatedOn = s.CreatedOn.HasValue ? s.CreatedOn.Value.ToString("MMM dd, yyyy") : string.Empty,
                            IsNewPending = s.IsNewPending,
                            Status = (s.Status == strPaid || s.Status == strPartiallyPaid) ? "Paid" : (s.Status == strPendingPayment || s.Status == strReceiptUploaded || s.Status == strReceiptReviewed) ? "In Process" : string.Empty,
                            //Status = langId == 1 ? "Paid" : "مدفوع",//s.Status, Status = langId == 1 ? "In Process" : "قيد الإنجاز",//s.Status,
                            PaymentType = GetPaymentType(s.PaymentTypeId ?? -1),
                            IsRegistrationInvoice = s.IsRegistrationInvoice
                        }).OrderBy(o => o.ItemId).ToList();
                    }
                    else
                    {
                        PaymentList = InvoiceList.Select(s => new PaidPaymentDTO
                        {
                            ItemId = s.ItemId,
                            InvoiceNumber = s.InvoiceNumber,
                            IsActive = s.IsActive,
                            ExhibitorId = s.ExhibitorId,
                            TotalAed = (s.TotalAed ?? "0") + strcurrency,
                            CreatedOn = s.CreatedOn.HasValue ? GetDateFormated(s.CreatedOn.Value, langId) : string.Empty,
                            //CreatedOn = s.CreatedOn.HasValue ? s.CreatedOn.Value.ToString("MMM dd, yyyy") : string.Empty,
                            IsNewPending = s.IsNewPending,
                            Status = (s.Status == strPaid || s.Status == strPartiallyPaid) ? "مدفوع" : (s.Status == strPendingPayment || s.Status == strReceiptUploaded || s.Status == strReceiptReviewed) ? "قيد الإنجاز" : string.Empty,
                            //Status = langId == 1 ? "Paid" : "مدفوع",//s.Status, Status = langId == 1 ? "In Process" : "قيد الإنجاز",//s.Status,
                            PaymentType = GetPaymentType(s.PaymentTypeId ?? -1),
                            IsRegistrationInvoice = s.IsRegistrationInvoice
                        }).OrderBy(o => o.ItemId).ToList();
                    }
                    //model.PaymentDetailsMobileDTO = new PaymentDetailsMobileDTO();

                    foreach (var item in PaymentList)
                    {
                        var mobileDto = new PaymentMobileDTO();
                        mobileDto.Id = item.ItemId;
                        mobileDto.Status = item.Status;
                        mobileDto.Date = item.CreatedOn;
                        if (langId == 1)
                            mobileDto.PaymentDetailsMobileDTO = new PaymentDetailsMobileDTO();
                        else
                            mobileDto.PaymentDetailsMobileDTO = new PaymentDetailsMobileArDTO();
                        mobileDto.PaymentDetailsMobileDTO.Invoice = item.InvoiceNumber;
                        mobileDto.PaymentDetailsMobileDTO.Status = item.Status;
                        mobileDto.PaymentDetailsMobileDTO.PaymentType = item.PaymentType;
                        mobileDto.PaymentDetailsMobileDTO.TotalAmount = item.TotalAed;
                        model.Add(mobileDto);
                    }
                    //model.PaidList = Paid;
                    if (PaymentList.Count() > 0)
                    {
                        //model.PaidCount = Paid.Count;
                    }
                    else
                    {
                        //model.Message1 = "Data not available.";
                    }
                    #endregion
                }
                else
                {
                    //model.Message = "There is no pending payment.";
                    //model.Message1 = "Data not available.";
                }
            }
        }
        private string GetPaymentType(long id)
        {
            if (id == Convert.ToInt32(EnumPaymentType.PaidByBankTransfer))
                return "Bank Transfer";
            else if (id == Convert.ToInt32(EnumPaymentType.PaidByCash))
                return "Cash";
            else if (id == Convert.ToInt32(EnumPaymentType.PaidByChequeTransfer))
                return "Cheque";
            else if (id == Convert.ToInt32(EnumPaymentType.PaidByOnline))
                return "Paid Online";
            return "";
        }

        private bool AtleastOneInvoice(long exhibitorId, sibfnewdbContext _context)
        {
            string strCancelled = EnumConversion.ToString(EnumExhibitorStatus.Cancelled);
            string strRejected = EnumConversion.ToString(EnumExhibitorStatus.Rejected);
            string strNew = EnumConversion.ToString(EnumExhibitorStatus.New);

            var predicate = PredicateBuilder.New<XsiInvoice>();
            predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicate.And(i => i.IsRegistrationInvoice == EnumConversion.ToString(EnumBool.Yes));
            predicate.And(i => i.ExhibitorId == exhibitorId);
            List<XsiInvoice> invoiceList = _context.XsiInvoice.Where(predicate).ToList();
            XsiInvoice entity = invoiceList.Where(p => p.Status != strCancelled && p.Status != strRejected && p.Status != null && p.Status != strNew).FirstOrDefault();
            if (entity != null)
                return true;
            return false;
        }

        #endregion
        #region Email
        private void SendEmail(XsiInvoice entity, long memberId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibitionMember entity1 = new XsiExhibitionMember();
                entity1 = _context.XsiExhibitionMember.Where(x => x.MemberId == memberId).FirstOrDefault();// ExhibitionMemberService.GetExhibitionMemberByItemId(Convert.ToInt64(bp.SIBFMemberId));
                if (entity1 != default(XsiExhibitionMember))
                {
                    var ExhibitorEntity = _context.XsiExhibitionMemberApplicationYearly.Where(x => x.MemberExhibitionYearlyId == entity.ExhibitorId).FirstOrDefault();// ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(entity.ExhibitorId.Value);
                    var websiteId = _context.XsiExhibition.Where(x => x.ExhibitionId == ExhibitorEntity.ExhibitionId.Value).FirstOrDefault().WebsiteId.Value;
                    #region Send Receipt Uploaded email to accountant
                    long LangId = 1;
                    string ToEmail = _appCustomSettings.AccountsEmail;
                    StringBuilder body = new StringBuilder();
                    #region Email Content
                    string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "";
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        //TODO need to work on
                        var scrfEmailContent = _context.XsiScrfemailContent.Where(x => x.ItemId == 82).FirstOrDefault();
                        if (scrfEmailContent != null)
                        {
                            if (scrfEmailContent.Body != null)
                                strEmailContentBody = scrfEmailContent.Body;
                            if (scrfEmailContent.Email != null)
                                strEmailContentEmail = scrfEmailContent.Email;
                            if (scrfEmailContent.Subject != null)
                                strEmailContentSubject = scrfEmailContent.Subject;
                        }
                    }
                    else
                    {
                        var emailContent = _context.XsiEmailContent.Where(x => x.ItemId == 20095).FirstOrDefault();
                        if (emailContent != null)
                        {
                            if (emailContent.Body != null)
                                strEmailContentBody = emailContent.Body;
                            if (emailContent.Email != null)
                                strEmailContentEmail = emailContent.Email;
                            if (emailContent.Subject != null)
                                strEmailContentSubject = emailContent.Subject;
                            // strEmailContentAdmin = AdminName;
                        }
                    }
                    #endregion
                    body.Append(BindEmailContent(LangId, strEmailContentBody, entity1, _appCustomSettings.ServerAddressNew, websiteId));


                    if (ExhibitorEntity.PublisherNameEn != null)
                        body.Replace("$$ExhibitorName$$", ExhibitorEntity.PublisherNameEn);
                    else
                        body.Replace("$$ExhibitorName$$", "");

                    if (ExhibitorEntity.ContactPersonName != null)
                        body.Replace("$$ContactPerson$$", ExhibitorEntity.ContactPersonName);
                    else
                        body.Replace("$$ContactPerson$$", "");

                    body.Replace("$$FileNumber$$", ExhibitorEntity.FileNumber);

                    if (!string.IsNullOrEmpty(strEmailContentEmail))
                        _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString());
                    else
                        _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, ToEmail, strEmailContentSubject, body.ToString());
                    #endregion
                }
            }
        }
        private StringBuilder BindEmailContent(long languageId, string bodyContent, XsiExhibitionMember entity, string ServerAddress, long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string scrfURL = _appCustomSettings.ServerAddressSCRF;
                string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
                string EmailContent = string.Empty;
                if (languageId == 1)
                {
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                        EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRF.html";
                    else
                        EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
                }
                else
                {
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                        EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRF.html";
                    else
                        EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
                }
                StringBuilder body = new StringBuilder();

                if (System.IO.File.Exists(EmailContent))
                {
                    body.Append(System.IO.File.ReadAllText(EmailContent));
                    body.Replace("$$EmailContent$$", bodyContent);
                }
                else
                    body.Append(bodyContent);
                string str = MethodFactory.GetExhibitionDetails(websiteId, languageId, _context);
                string[] strArray = str.Split(',');
                body.Replace("$$exhibitionno$$", strArray[0].ToString());
                body.Replace("$$exhibitiondate$$", strArray[1].ToString());
                body.Replace("$$exhyear$$", strArray[2]);

                string swidth = "width=\"864\"";
                string sreplacewidth = "width=\"100%\"";
                body.Replace(swidth, sreplacewidth);

                body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
                body.Replace("$$ServerAddress$$", ServerAddress);
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    body.Replace("$$ServerAddressSCRF$$", scrfURL);

                if (entity != null)
                {
                    string exhibitionmembername = string.Empty;
                    if (languageId == 1)
                    {
                        if (entity.Firstname != null && entity.LastName != null)
                            exhibitionmembername = entity.Firstname + " " + entity.LastName;
                        else if (entity.Firstname != null)
                            exhibitionmembername = entity.Firstname;
                        else if (entity.LastName != null)
                            exhibitionmembername = entity.LastName;
                    }
                    else
                    {
                        if (entity.FirstNameAr != null && entity.LastNameAr != null)
                            exhibitionmembername = entity.FirstNameAr + " " + entity.LastNameAr;
                        else if (entity.Firstname != null)
                            exhibitionmembername = entity.FirstNameAr;
                        else if (entity.LastName != null)
                            exhibitionmembername = entity.LastNameAr;

                        if (string.IsNullOrEmpty(exhibitionmembername))
                        {
                            if (entity.Firstname != null && entity.LastName != null)
                                exhibitionmembername = entity.Firstname + " " + entity.LastName;
                            else if (entity.Firstname != null)
                                exhibitionmembername = entity.Firstname;
                            else if (entity.LastName != null)
                                exhibitionmembername = entity.LastName;
                        }
                    }
                    body.Replace("$$Name$$", exhibitionmembername);
                }
                return body;
            }
        }
        string GetDateFormated(DateTime date, long langId)
        {
            if (langId == 2)
                return date.ToString("MMM dd, yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
            else
                return date.ToString("MMM dd, yyyy");
        }
        #endregion
    }
}

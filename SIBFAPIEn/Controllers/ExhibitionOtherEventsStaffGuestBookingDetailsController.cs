﻿using Contracts;
using Entities.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using Xsi.ServicesLayer;
using drawingFont = System.Drawing.Font;
namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ExhibitionOtherEventsStaffGuestBookingDetailsController : ControllerBase
    {
        ExhibitionOtherEventsStaffGuestBookingDetailsService ExhibitionOtherEventsStaffGuestBookingDetailsService;

        #region Variables
        private ILoggerManager _logger;
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;
        #endregion

        #region FilePaths
        readonly string PassportPath = "StaffGuestOtherEvents\\Passport\\";
        readonly string SuggestedFlightDocumentPath = "StaffGuestOtherEvents\\SuggestedFlightDocument\\";
        readonly string PassportPathTemp = "StaffGuestOtherEvents\\Passport\\temp\\";
        public string ext = "PNG";
        #endregion FilePaths

        private MessageDTO MessageDTO { get; set; }
        public long SIBFMemberId { get; set; }

        public ExhibitionOtherEventsStaffGuestBookingDetailsController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings,
         IEmailSender emailSender)
        {
            _logger = logger;
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;

        }

        #region Get Methods
        [HttpGet]
        [Route("{memberid}/{ExhibitionId}/{staffguestid}")]
        public ActionResult<dynamic> StaffGuestPageLoad(long memberid, long ExhibitionId, long staffguestid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long memberId = User.Identity.GetID();
                MessageDTO MessageDTO = new MessageDTO();
                MessageDTO.MessageTypeResponse = "Error";
                string passport = string.Empty;
                string passporttwo = string.Empty;
                string submissionexpirydate = string.Empty;

                string flightstatus = string.Empty;
                string returnflightstatus = string.Empty;
                string hotelstatus = string.Empty;
                string transportstatus = string.Empty;
                string uploadstatus = string.Empty;
                bool issubmitvisible = true;
                bool isVIPorVVIP = false;
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    SaveStaffGuestBookingDTO dto = new SaveStaffGuestBookingDTO();
                    StaffGuestFlightBookingGetDTO StaffGuestFlightBookingDTO = new StaffGuestFlightBookingGetDTO();
                    StaffGuestHotelBookingGetDTO StaffGuestHotelBookingDTO = new StaffGuestHotelBookingGetDTO();
                    StaffGuestTransportBookingGetDTO StaffGuestTransportBookingDTO = new StaffGuestTransportBookingGetDTO();
                    bool IsFlightEditable = false;
                    bool IsHotelEditable = false;
                    bool IsTransportEditable = false;
                    bool IsUploadsEditable = false;
                    long bookingitemid = -1;
                    string VisaStatus = string.Empty;
                    if (memberId == memberid)
                    {
                        var ExhibitionOtherEvents = _context.XsiExhibitionOtherEvents.Where(i => i.ExhibitionId == ExhibitionId).FirstOrDefault();
                        if (ExhibitionOtherEvents != null)
                        {

                            if (ExhibitionOtherEvents.StaffGuestBookingExpiryDate != null)
                                submissionexpirydate = ExhibitionOtherEvents.StaffGuestBookingExpiryDate.Value.ToString("dd/MM/yyyy");
                            var staffguest = _context.XsiExhibitionOtherEventsStaffGuestParticipating.Where(i => i.ExhibitionId == ExhibitionId && i.StaffGuestId == staffguestid).FirstOrDefault();
                            #region Load Data
                            if (staffguest != null)
                            {
                                if (MethodFactory.IsSIBFSTaff(memberId) || staffguest.IsFromPcregistration == EnumConversion.ToString(EnumBool.Yes))
                                {
                                    if (staffguest.GuestType != null)
                                        if (staffguest.GuestType == 3 || staffguest.GuestType == 6)
                                            isVIPorVVIP = true;

                                    if (!string.IsNullOrEmpty(staffguest.PassportCopy))
                                    {
                                        if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\StaffGuestOtherEvents\\Passport\\" + staffguest.PassportCopy))
                                            passport = _appCustomSettings.UploadsCMSPath + "/StaffGuestOtherEvents/Passport/" + staffguest.PassportCopy;
                                    }
                                    if (!string.IsNullOrEmpty(staffguest.PassportCopyTwo))
                                    {
                                        if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\StaffGuestOtherEvents\\Passport\\" + staffguest.PassportCopyTwo))
                                            passporttwo = _appCustomSettings.UploadsCMSPath + "/StaffGuestOtherEvents/Passport/" + staffguest.PassportCopyTwo;
                                    }

                                    var booking = _context.XsiExhibitionOtherEventsStaffGuestBookingDetails.Where(i => i.ExhibitionId == ExhibitionId && i.StaffGuestId == staffguestid).OrderByDescending(i => i.ItemId).FirstOrDefault();
                                    if (booking != null)
                                    {
                                        bookingitemid = booking.ItemId;
                                        if (!string.IsNullOrEmpty(booking.FlightStatus))
                                        {
                                            if (booking.FlightStatus == "N" || booking.FlightStatus == "P")
                                            {
                                                IsFlightEditable = true;
                                                IsUploadsEditable = true;
                                            }

                                            flightstatus = GetStatus(booking.FlightStatus, 1);
                                        }
                                        if (!string.IsNullOrEmpty(booking.ReturnFlightStatus))
                                        {
                                            returnflightstatus = GetStatus(booking.ReturnFlightStatus, 1);
                                        }
                                        if (!string.IsNullOrEmpty(booking.IsPassportModified) && booking.IsPassportModified == EnumConversion.ToString(EnumBool.Yes))
                                        {
                                            if (!string.IsNullOrEmpty(booking.Passport))
                                            {
                                                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\StaffGuestOtherEvents\\Passport\\" + booking.Passport))
                                                    passport = _appCustomSettings.UploadsCMSPath + "/StaffGuestOtherEvents/Passport/" + booking.Passport;
                                            }
                                            if (booking.PassportCopyTwo != null && !string.IsNullOrEmpty(booking.PassportCopyTwo))
                                            {
                                                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\StaffGuestOtherEvents\\Passport\\" + booking.PassportCopyTwo))
                                                    passporttwo = _appCustomSettings.UploadsCMSPath + "/StaffGuestOtherEvents/Passport/" + booking.PassportCopyTwo;
                                            }
                                            uploadstatus = "Submitted";
                                        }
                                        StaffGuestFlightBookingDTO = LoadFlightInfo(booking);
                                    }
                                    if (booking != null && !string.IsNullOrEmpty(booking.HotelStatus))
                                    {
                                        if (!string.IsNullOrEmpty(booking.HotelStatus))
                                        {
                                            if (booking.HotelStatus == "N" || booking.HotelStatus == "P")
                                                IsHotelEditable = true;
                                            hotelstatus = GetStatus(booking.HotelStatus, 2);
                                        }
                                        StaffGuestHotelBookingDTO = LoadHotelInfo(booking);
                                    }
                                    if (booking != null && !string.IsNullOrEmpty(booking.TransportationStatus))
                                    {
                                        if (!string.IsNullOrEmpty(booking.TransportationStatus))
                                        {
                                            if (booking.TransportationStatus == "N" || booking.TransportationStatus == "P")
                                                IsTransportEditable = true;
                                            transportstatus = GetStatus(booking.TransportationStatus, 3);
                                        }
                                        StaffGuestTransportBookingDTO = LoadTransportInfo(booking);
                                    }
                                    VisaStatus = MethodFactory.GetExhibitionStaffGuestStatus(staffguest.Status, LangId);


                                    if (!string.IsNullOrEmpty(flightstatus) && !string.IsNullOrEmpty(hotelstatus) && !string.IsNullOrEmpty(transportstatus) && !string.IsNullOrEmpty(uploadstatus))
                                        issubmitvisible = false;

                                    return Ok(new { Message = "", MessageTypeResponse = "Success", SubmissionExpiryDate = submissionexpirydate, IsFlightEditable = IsFlightEditable, IsHotelEditable = IsHotelEditable, IsTransportEditable = IsTransportEditable, IsUploadsEditable = IsUploadsEditable, IsVIPorVVIP = isVIPorVVIP, Passport = passport, PassportCopyTwo = passporttwo, BookingId = bookingitemid, FlightStatus = flightstatus, UploadStatus = uploadstatus, HotelStatus = hotelstatus, TransportStatus = transportstatus, VisaStatus = VisaStatus, IsSubmitVisible = issubmitvisible, StaffGuestFlightBookingDTO = StaffGuestFlightBookingDTO, StaffGuestHotelBookingDTO = StaffGuestHotelBookingDTO, StaffGuestTransportBookingDTO = StaffGuestTransportBookingDTO, });
                                }
                                else
                                {
                                    MessageDTO.Message = "Logged in Member is not a Staff Guest";
                                    return Ok(MessageDTO);
                                }
                            }
                            else
                            {
                                MessageDTO.Message = "Staff Guest record doesn't exist";
                                return Ok(MessageDTO);
                            }
                            #endregion
                            //if (MethodFactory.ArabianTimeNow() <= ExhibitionOtherEvents.StaffGuestBookingExpiryDate)
                            //{

                            //}
                            //else
                            //{
                            //    if (LangId == 1)
                            //        MessageDTO.Message = "Booking Date has been expired or not available";
                            //    else
                            //        MessageDTO.Message = "Booking Date has been expired or not available";
                            //    return Ok(new { Message = "Booking Date has been expired", MessageTypeResponse = "Error", SubmissionExpiryDate = submissionexpirydate });
                            //}

                        }
                        else
                        {
                            MessageDTO.Message = "Exhibition Unavailable.";
                            return Ok(MessageDTO);
                        }
                    }
                }

                MessageDTO.Message = "Unauthorized access";
                return Ok(MessageDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside StaffGuestBookingDetailsPageLoad action: {ex.InnerException}");
                MessageDTO.Message = "Something went wrong. Please try again later." + "---" + ex.InnerException + "---" + ex.StackTrace + ex.ToString();
                return Ok(MessageDTO);
            }
        }

        #endregion
        #region Post Methods
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> PostSaveData(SaveStaffGuestBookingDTO dto)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                MessageDTO = new MessageDTO();
                MessageDTO.MessageTypeResponse = "Error";
                SIBFMemberId = dto.MemberId;
                long memberId = User.Identity.GetID();
                string strtype = string.Empty;
                EnumResultType xsiresult = EnumResultType.Failed;
                if (memberId == SIBFMemberId)
                {
                    using (ExhibitionOtherEventsService ExhibitionOtherEventsService = new ExhibitionOtherEventsService())
                    {
                        XsiExhibitionOtherEventsStaffGuestBookingDetails booking = new XsiExhibitionOtherEventsStaffGuestBookingDetails();
                        var ExhibitionOtherEvents = ExhibitionOtherEventsService.GetExhibitionOtherEventsByItemId(dto.ExhibitionId);
                        bool isoptedatleastone = false;
                        if (dto.StaffGuestFlightBookingDTO != null)
                        {
                            using (ExhibitionOtherEventsStaffGuestBookingDetailsService = new ExhibitionOtherEventsStaffGuestBookingDetailsService())
                            {
                                ExhibitionOtherEventsStaffGuestParticipatingService ExhibitionOtherEventsStaffGuestParticipatingService = new ExhibitionOtherEventsStaffGuestParticipatingService();
                                var SGParticipatingEntity = ExhibitionOtherEventsStaffGuestParticipatingService.GetExhibitionOtherEventsStaffGuestParticipating(new XsiExhibitionOtherEventsStaffGuestParticipating() { StaffGuestId = dto.StaffGuestId, ExhibitionId = dto.ExhibitionId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

                                XsiExhibitionOtherEventsStaffGuestBookingDetails where = new XsiExhibitionOtherEventsStaffGuestBookingDetails();
                                where.StaffGuestId = dto.StaffGuestId;
                                where.ExhibitionId = dto.ExhibitionId;

                                booking = ExhibitionOtherEventsStaffGuestBookingDetailsService.GetExhibitionOtherEventsStaffGuestBookingDetails(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

                                if (booking == null)
                                    booking = new XsiExhibitionOtherEventsStaffGuestBookingDetails();

                                booking.StaffGuestId = dto.StaffGuestId;
                                booking.ExhibitionId = dto.ExhibitionId;
                                if (dto.IsFlightOpted)
                                {
                                    isoptedatleastone = true;
                                    strtype = "Flight,";
                                    #region Flight
                                    //  booking.GuestCategory = dto.StaffGuestFlightBookingDTO.GuestCategory;

                                    if (SGParticipatingEntity != null)
                                    {
                                        if (SGParticipatingEntity.GuestType != null)
                                            booking.GuestCategory = SGParticipatingEntity.GuestType.ToString();
                                    }

                                    booking.GuestPhoneNumber = dto.StaffGuestFlightBookingDTO.GuestPhoneISD + "$" + dto.StaffGuestFlightBookingDTO.GuestPhoneSTD + "$" + dto.StaffGuestFlightBookingDTO.GuestPhoneNumber;
                                    booking.StaffInChargePhoneNumber = dto.StaffGuestFlightBookingDTO.StaffInChargePhoneISD + "$" + dto.StaffGuestFlightBookingDTO.StaffInChargePhoneSTD + "$" + dto.StaffGuestFlightBookingDTO.StaffInChargePhoneNumber;
                                    booking.ServiceType = dto.StaffGuestFlightBookingDTO.ServiceType;

                                    if (!string.IsNullOrEmpty(dto.StaffGuestFlightBookingDTO.FlightBookingType))
                                        booking.FlightBookingType = Convert.ToInt32(dto.StaffGuestFlightBookingDTO.FlightBookingType);

                                    if (!string.IsNullOrEmpty(dto.StaffGuestFlightBookingDTO.StartDate))
                                        booking.StartDate = Convert.ToDateTime(dto.StaffGuestFlightBookingDTO.StartDate.Trim());

                                    if (!string.IsNullOrEmpty(dto.StaffGuestFlightBookingDTO.EndDate))
                                        booking.EndDate = Convert.ToDateTime(dto.StaffGuestFlightBookingDTO.EndDate.Trim());

                                    booking.PreferredTime = dto.StaffGuestFlightBookingDTO.PreferredTime;
                                    booking.PreferredAirline = dto.StaffGuestFlightBookingDTO.PreferredAirline;
                                    booking.ClassSeat = dto.StaffGuestFlightBookingDTO.ClassSeat;
                                    booking.ReturnClassSeat = dto.StaffGuestFlightBookingDTO.ReturnClassSeat;
                                    booking.FlightStatus = "N";

                                    if (!string.IsNullOrEmpty(booking.ReturnClassSeat))
                                        booking.ReturnFlightStatus = "N";
                                    if (dto.StaffGuestFlightBookingDTO.DestinationFrom > 0)
                                        booking.DestinationFrom = dto.StaffGuestFlightBookingDTO.DestinationFrom;
                                    if (dto.StaffGuestFlightBookingDTO.DestinationTo > 0)
                                        booking.DestinationTo = dto.StaffGuestFlightBookingDTO.DestinationTo;

                                    if (dto.StaffGuestFlightBookingDTO.OriginAirport > 0)
                                        booking.OriginAirport = dto.StaffGuestFlightBookingDTO.OriginAirport;
                                    if (dto.StaffGuestFlightBookingDTO.DestinationAirport > 0)
                                        booking.DestinationAirport = dto.StaffGuestFlightBookingDTO.DestinationAirport;

                                    //if (!string.IsNullOrEmpty(dto.StaffGuestFlightBookingDTO.FileBase64SuggestedFlight))
                                    //{
                                    //    ext = dto.StaffGuestFlightBookingDTO.FileExtensionofSuggestedFlight;
                                    //    string filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                                    //    if ("." + ext.ToLower() == ".pdf" || ext.ToLower() == "pdf")
                                    //        filename = WriteTextOnPDF(dto.UploadsDTO.FileBase64Passport2, filename, "StaffGuestOtherEventsDTO.NameEn", SIBFMemberId, ext);
                                    //    else
                                    //        filename = WriteTextOnImage(dto.UploadsDTO.FileBase64Passport2, filename, "StaffGuestOtherEventsDTO.NameEn", SIBFMemberId, ext);
                                    //    booking.SuggestFlightAttachment = filename;
                                    //}

                                    if (dto.StaffGuestFlightBookingDTO.FileBase64SuggestedFlight != null && dto.StaffGuestFlightBookingDTO.FileBase64SuggestedFlight.Length > 0 && !string.IsNullOrEmpty(dto.StaffGuestFlightBookingDTO.FileBase64SuggestedFlight))
                                    {
                                        string fileName = string.Empty;
                                        byte[] imageBytes;
                                        if (dto.StaffGuestFlightBookingDTO.FileBase64SuggestedFlight.Contains("data:"))
                                        {
                                            var strInfo = dto.StaffGuestFlightBookingDTO.FileBase64SuggestedFlight.Split(",")[0];
                                            imageBytes = Convert.FromBase64String(dto.StaffGuestFlightBookingDTO.FileBase64SuggestedFlight.Split(',')[1]);
                                        }
                                        else
                                            imageBytes = Convert.FromBase64String(dto.StaffGuestFlightBookingDTO.FileBase64SuggestedFlight);

                                        booking.IsDirectOrTransitFlight = dto.StaffGuestFlightBookingDTO.IsDirectOrTransitFlight;

                                        fileName = string.Format("{0}_{1}_{2}{3}", memberId, LangId, MethodFactory.GetRandomNumber(), "." + dto.StaffGuestFlightBookingDTO.FileExtensionofSuggestedFlight);
                                        System.IO.File.WriteAllBytes(
                                            Path.Combine(_appCustomSettings.UploadsPhysicalPath + SuggestedFlightDocumentPath) + fileName, imageBytes);

                                        booking.SuggestFlightAttachment = fileName;
                                    }

                                    booking.FlightCreatedOn = MethodFactory.ArabianTimeNow();
                                    #endregion
                                }
                                if (dto.IsUploadingNewPassport)
                                {
                                    isoptedatleastone = true;
                                    if (dto.UploadsDTO != null)
                                    {
                                        booking.IsPassportModified = dto.IsUploadingNewPassport == true ? "Y" : "N";
                                        if (!string.IsNullOrEmpty(dto.UploadsDTO.FileBase64Passport))
                                        {
                                            booking.IsPassportModified = "Y";
                                            ext = dto.UploadsDTO.FileExtensionofPassport.ToLower();
                                            string filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                                            if ("." + ext.ToLower() == ".pdf" || ext.ToLower() == "pdf")
                                                filename = WriteTextOnPDF(dto.UploadsDTO.FileBase64Passport, filename, "StaffGuestDTO.NameEn", SIBFMemberId, ext);
                                            else
                                                filename = WriteTextOnImage(dto.UploadsDTO.FileBase64Passport, filename, "StaffGuestDTO.NameEn", SIBFMemberId, ext);
                                            booking.Passport = filename;
                                        }
                                        if (!string.IsNullOrEmpty(dto.UploadsDTO.FileBase64Passport2))
                                        {
                                            booking.IsPassportModified = "Y";
                                            ext = dto.UploadsDTO.FileExtensionofPassport2;
                                            string filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                                            if ("." + ext.ToLower() == ".pdf" || ext.ToLower() == "pdf")
                                                filename = WriteTextOnPDF(dto.UploadsDTO.FileBase64Passport2, filename, "StaffGuestDTO.NameEn", SIBFMemberId, ext);
                                            else
                                                filename = WriteTextOnImage(dto.UploadsDTO.FileBase64Passport2, filename, "StaffGuestDTO.NameEn", SIBFMemberId, ext);
                                            booking.PassportCopyTwo = filename;
                                        }
                                    }
                                }

                                if (dto.IsHotelOpted)
                                {
                                    isoptedatleastone = true;
                                    if (!string.IsNullOrEmpty(strtype))
                                        strtype = strtype + "Hotel,";
                                    else
                                        strtype = "Hotel,";

                                    #region Hotel
                                    if (!string.IsNullOrEmpty(dto.StaffGuestHotelBookingDTO.HotelBookingStartDate))
                                        booking.HotelBookingStartDate = Convert.ToDateTime(dto.StaffGuestHotelBookingDTO.HotelBookingStartDate.Trim());

                                    if (!string.IsNullOrEmpty(dto.StaffGuestHotelBookingDTO.HotelBookingEndDate))
                                        booking.HotelBookingEndDate = Convert.ToDateTime(dto.StaffGuestHotelBookingDTO.HotelBookingEndDate.Trim());
                                    booking.RoomType = dto.StaffGuestHotelBookingDTO.RoomType;
                                    booking.SpecialRequest = dto.StaffGuestHotelBookingDTO.RemarksOrSpecialRequest;
                                    booking.HotelStatus = "N";
                                    booking.HotelCreatedOn = MethodFactory.ArabianTimeNow();
                                    booking.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                    booking.CreatedOn = MethodFactory.ArabianTimeNow();
                                    booking.CreatedBy = SIBFMemberId;
                                    booking.ModifiedOn = MethodFactory.ArabianTimeNow();
                                    booking.ModifiedBy = SIBFMemberId;
                                    #endregion
                                }

                                if (dto.IsTransportOpted)
                                {
                                    isoptedatleastone = true;
                                    if (!string.IsNullOrEmpty(strtype))
                                        strtype = strtype + "Transport,";
                                    else
                                        strtype = "Transport,";

                                    #region Transport
                                    if (dto.IsFlightOpted)
                                    {
                                        if (booking.ItemId > 0)
                                        {
                                            if (!string.IsNullOrEmpty(booking.FlightStatus) && booking.FlightStatus != "Y")
                                                booking.TransportStartDate = Convert.ToDateTime(dto.StaffGuestFlightBookingDTO.StartDate.Trim());

                                            if (!string.IsNullOrEmpty(booking.FlightStatus) && booking.ReturnFlightStatus != "Y")
                                                booking.TransportEndDate = Convert.ToDateTime(dto.StaffGuestFlightBookingDTO.EndDate.Trim());
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(dto.StaffGuestFlightBookingDTO.StartDate))
                                                booking.TransportStartDate = Convert.ToDateTime(dto.StaffGuestFlightBookingDTO.StartDate.Trim());

                                            if (!string.IsNullOrEmpty(dto.StaffGuestFlightBookingDTO.EndDate))
                                                booking.TransportEndDate = Convert.ToDateTime(dto.StaffGuestFlightBookingDTO.EndDate.Trim());
                                        }
                                    }
                                    else if (dto.IsHotelOpted)
                                    {
                                        if (booking.ItemId > 0)
                                        {
                                            if (!string.IsNullOrEmpty(booking.HotelStatus) && booking.FlightStatus != "Y" && booking.HotelStatus != "Y")
                                                booking.TransportStartDate = Convert.ToDateTime(dto.StaffGuestFlightBookingDTO.StartDate.Trim());

                                            if (!string.IsNullOrEmpty(booking.FlightStatus) && booking.ReturnFlightStatus != "Y")
                                                booking.TransportEndDate = Convert.ToDateTime(dto.StaffGuestFlightBookingDTO.EndDate.Trim());
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(dto.StaffGuestHotelBookingDTO.HotelBookingStartDate))
                                                booking.TransportStartDate = Convert.ToDateTime(dto.StaffGuestHotelBookingDTO.HotelBookingStartDate.Trim());

                                            if (!string.IsNullOrEmpty(dto.StaffGuestHotelBookingDTO.HotelBookingEndDate))
                                                booking.TransportEndDate = Convert.ToDateTime(dto.StaffGuestHotelBookingDTO.HotelBookingEndDate.Trim());
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(dto.StaffGuestTransportBookingDTO.TransportStartDate))
                                            booking.TransportStartDate = Convert.ToDateTime(dto.StaffGuestTransportBookingDTO.TransportStartDate.Trim());

                                        if (!string.IsNullOrEmpty(dto.StaffGuestTransportBookingDTO.TransportEndDate))
                                            booking.TransportEndDate = Convert.ToDateTime(dto.StaffGuestTransportBookingDTO.TransportEndDate.Trim());
                                    }

                                    booking.TransportContactPerson = dto.StaffGuestTransportBookingDTO.TransportContactPerson;
                                    booking.TransportContactNumber = dto.StaffGuestTransportBookingDTO.TransportContactISD + "$" + dto.StaffGuestTransportBookingDTO.TransportContactSTD + "$" + dto.StaffGuestTransportBookingDTO.TransportContactNumber;
                                    booking.TransportationType = dto.StaffGuestTransportBookingDTO.TransportationType;
                                    booking.NoofPeople = dto.StaffGuestTransportBookingDTO.NoofPeople;
                                    booking.PickupLocation = dto.StaffGuestTransportBookingDTO.PickupLocation;
                                    booking.TransportationStatus = "N";
                                    booking.IsIndividualOrGroup = dto.StaffGuestTransportBookingDTO.IsIndividualOrGroup;
                                    booking.TransportCreatedOn = MethodFactory.ArabianTimeNow();
                                    #endregion
                                }
                                booking.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                booking.CreatedOn = MethodFactory.ArabianTimeNow();
                                booking.CreatedBy = SIBFMemberId;
                                booking.ModifiedOn = MethodFactory.ArabianTimeNow();
                                booking.ModifiedBy = SIBFMemberId;


                                if (isoptedatleastone)
                                {
                                    if (booking.ItemId > 0)
                                    {
                                        xsiresult = ExhibitionOtherEventsStaffGuestBookingDetailsService.UpdateExhibitionOtherEventsStaffGuestBookingDetails(booking);
                                    }
                                    else
                                    {
                                        xsiresult = ExhibitionOtherEventsStaffGuestBookingDetailsService.InsertExhibitionOtherEventsStaffGuestBookingDetails(booking);
                                    }

                                    if (xsiresult == EnumResultType.Success)
                                    {
                                        MessageDTO.Message = strtype + " Details Submitted Successfully.";
                                        if (dto.IsFlightOpted)
                                            MessageDTO.TravelMessage = "You will be receiving all travel documents at least one week prior to travel.";
                                        MessageDTO.MessageTypeResponse = "Success";
                                        if (booking.ItemId == 0)
                                            booking.ItemId = ExhibitionOtherEventsStaffGuestBookingDetailsService.XsiItemdId;

                                        if (dto.IsFlightOpted)
                                        {
                                            NotifyAdminAboutBooking("N", memberId, dto.StaffGuestId, ExhibitionOtherEvents.ExhibitionId, 1, 1);
                                            if (booking.IsPassportModified == "Y")
                                            {
                                                NotifyAdminAboutBooking("N", memberId, dto.StaffGuestId, ExhibitionOtherEvents.ExhibitionId, 4, 1);
                                            }
                                        }
                                        if (dto.IsHotelOpted)
                                        {
                                            NotifyAdminAboutBooking("N", memberId, dto.StaffGuestId, ExhibitionOtherEvents.ExhibitionId, 2, 1);
                                        }
                                        if (dto.IsTransportOpted)
                                        {
                                            NotifyAdminAboutBooking("N", memberId, dto.StaffGuestId, ExhibitionOtherEvents.ExhibitionId, 3, 1);
                                        }
                                    }
                                    else
                                    {
                                        MessageDTO.Message = "Error while submitting details.";
                                        MessageDTO.MessageTypeResponse = "Error";
                                    }
                                }
                                else
                                {
                                    MessageDTO.Message = "You need to select atleast one from the sections.";
                                    MessageDTO.MessageTypeResponse = "Error";
                                }
                            }
                        }
                    }
                }

                return Ok(MessageDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside SaveData action: {ex.InnerException}");
                MessageDTO.MessageTypeResponse = "Error";
                MessageDTO.Message = "Something went wrong. Please try again later." + "---" + ex.InnerException + "---" + ex.StackTrace + ex.ToString();
                return Ok(MessageDTO);
            }
        }
        #endregion
        #region BookMark Methods
        [ApiExplorerSettings(IgnoreApi = true)]
        [NonAction]
        string WriteTextOnImage(string fileBase64, string fileName, string name, long exhibitorId, string ext)
        {
            fileName = SaveImage(fileBase64, fileName, name, PassportPathTemp, ext);
            Bitmap b = new Bitmap((Bitmap)System.Drawing.Image.FromFile(_appCustomSettings.UploadsPhysicalPath + PassportPathTemp + fileName));
            Graphics g = Graphics.FromImage(b);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            string StaffGuestName = string.Empty;
            if (name != null)
                StaffGuestName = name.Trim();

            string FileNumber = string.Empty;
            //using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            //{
            //    var entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
            //    if (entity != default(XsiExhibitionMemberApplicationYearly))
            //    {
            //        if (entity.FileNumber != null)
            //            if (!string.IsNullOrEmpty(entity.FileNumber))
            //                FileNumber = entity.FileNumber;
            //    }
            //}
            drawingFont f = new drawingFont("calibri", 14, FontStyle.Bold);
            g.DrawString("File Number: " + FileNumber, f, SystemBrushes.WindowText, new Point(20, b.Height - 30));
            g.DrawString("StaffGuest Name: " + StaffGuestName, f, SystemBrushes.WindowText, new Point(20, b.Height - 50));
            ext = "." + ext.ToLower();
            if (ext == ".png")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Png);
            else if (ext == ".jpg" || ext == ".jpeg")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Jpeg);
            else if (ext == ".gif")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Gif);
            else if (ext == ".bmp")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Bmp);
            else if (ext == ".tiff" || ext == ".tif")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Tiff);
            b.Dispose();
            return fileName;
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        [NonAction]
        string WriteTextOnPDF(string fileBase64, string fileName, string name, long memberid, string ext)
        {
            PdfContentByte cb;
            fileName = SaveImage(fileBase64, fileName, name, PassportPathTemp, ext);
            //PPUpload.SaveAs(string.Format("{0}{1}", Server.MapPath(PassportPathTemp), fileName));
            PdfReader reader = new PdfReader(ReadImage(fileBase64, fileName, name, PassportPathTemp));
            using (var fileStream = new FileStream(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, FileMode.Create, FileAccess.Write))
            {
                var document = new Document(reader.GetPageSizeWithRotation(1));
                var writer = PdfWriter.GetInstance(document, fileStream);
                document.Open();
                for (var i = 1; i <= reader.NumberOfPages; i++)
                {
                    document.NewPage();
                    var baseFont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    var importedPage = writer.GetImportedPage(reader, i);
                    cb = writer.DirectContent;
                    cb.AddTemplate(importedPage, 0, 0);
                    string StaffGuestName = string.Empty;
                    if (name != null)
                        StaffGuestName = name.Trim();
                    string FileNumber = string.Empty;
                    using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
                    {
                        XsiExhibitionMember entity = ExhibitionMemberService.GetExhibitionMemberByItemId(SIBFMemberId);
                        if (entity != default(XsiExhibitionMember))
                        {
                            if (entity.FileNumber != null)
                                if (!string.IsNullOrEmpty(entity.FileNumber))
                                    FileNumber = entity.FileNumber;
                        }
                    }
                    string text = "Guest Name: " + StaffGuestName;
                    {
                        cb.BeginText();
                        cb.SetFontAndSize(baseFont, 12);
                        cb.SetTextMatrix(document.PageSize.GetLeft(26), document.PageSize.GetBottom(20));
                        cb.ShowText(text);
                        cb.EndText();
                    }
                    text = "File Number: " + FileNumber;
                    {
                        cb.BeginText();
                        cb.SetFontAndSize(baseFont, 12);
                        cb.SetTextMatrix(document.PageSize.GetLeft(26), document.PageSize.GetBottom(30));
                        cb.ShowText(text);
                        cb.EndText();
                    }
                }
                document.Close();
                writer.Close();
            }
            return fileName;
        }
        string SaveImage(string fileBase64, string fileName, string name, string path, string fileExtension)
        {
            if (fileBase64 != null && fileBase64.Length > 0)
            {
                //string fileName = string.Empty;
                byte[] imageBytes;
                if (fileBase64.Contains("data:"))
                {
                    var strInfo = fileBase64.Split(",")[0];
                    imageBytes = Convert.FromBase64String(fileBase64.Split(',')[1]);
                }
                else
                {
                    imageBytes = Convert.FromBase64String(fileBase64);
                }

                fileName = string.Format("{0}_{1}_{2}{3}", name, 1, MethodFactory.GetRandomNumber(),
                    "." + fileExtension);
                System.IO.File.WriteAllBytes(
                    Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                 path) + fileName, imageBytes);

                return fileName;
            }
            return string.Empty;
        }
        byte[] ReadImage(string fileBase64, string fileName, string name, string path)
        {
            byte[] responseBytes = null;
            if (fileBase64 != null && fileBase64.Length > 0)
            {
                //string fileName = string.Empty;
                //fileName = string.Format("{0}_{1}_{2}{3}", name, 1, MethodFactory.GetRandomNumber(),
                //    "." + fileExtension);
                responseBytes = System.IO.File.ReadAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                 path) + fileName);
            }
            return responseBytes;
        }
        #endregion

        #region Other Methods
        private StaffGuestFlightBookingGetDTO LoadFlightInfo(XsiExhibitionOtherEventsStaffGuestBookingDetails flight)
        {
            string[] phone = { };
            string[] inchargephone = { };
            StaffGuestFlightBookingGetDTO dto = new StaffGuestFlightBookingGetDTO();

            //dto.GuestCategory = flight.GuestCategory;

            if (!string.IsNullOrEmpty(flight.GuestPhoneNumber))
                phone = flight.GuestPhoneNumber.Split("$");
            if (!string.IsNullOrEmpty(flight.StaffInChargePhoneNumber))
                inchargephone = flight.StaffInChargePhoneNumber.Split("$");
            if (phone.Length > 0)
            {
                dto.GuestPhoneNumber = phone[2];
                dto.GuestPhoneISD = phone[1];
                dto.GuestPhoneSTD = phone[0];
            }
            if (inchargephone.Length > 0)
            {
                dto.StaffInChargePhoneNumber = inchargephone[2];
                dto.StaffInChargePhoneISD = inchargephone[1];
                dto.StaffInChargePhoneSTD = inchargephone[0];
            }
            dto.ServiceType = flight.ServiceType;

            if (flight.PreferredTime != null)
                dto.PreferredTime = flight.PreferredTime.ToString();

            dto.PreferredAirline = flight.PreferredAirline;

            if (flight.DestinationFrom != null)
                dto.DestinationFrom = flight.DestinationFrom;
            if (flight.DestinationTo != null)
                dto.DestinationTo = flight.DestinationTo;

            if (flight.OriginAirport != null)
                dto.OriginAirport = flight.OriginAirport;
            if (flight.DestinationAirport != null)
                dto.DestinationAirport = flight.DestinationAirport;

            if (flight.FlightBookingType != null)
                dto.FlightBookingType = flight.FlightBookingType.ToString();

            if (flight.FlightStatus == "Y" || flight.FlightStatus == "A")
            {
                dto.MarhabaReferenceNumber = flight.MarhabaReferenceNumber;
                dto.FlightDetails = flight.FlightDetails;
                dto.FlightNumber = flight.FlightNumber;
                //dto.FlightCheckInTime = flight.FlightCheckInTime;
                //dto.FlightCheckOutTime = flight.FlightCheckOutTime;

                dto.ArrivalTime = flight.ArrivalDateTime;
                dto.ReturnTime = flight.ReturnDateTime;

                if (flight.ArrivalDate != null)
                    dto.StartDate = flight.ArrivalDate.Value.ToString("MM/dd/yyyy");

                if (flight.ReturnDate != null)
                    dto.EndDate = flight.ReturnDate.Value.ToString("MM/dd/yyyy");

                dto.Terminal = flight.Terminal;

                if (!string.IsNullOrEmpty(flight.IsDirectOrTransitFlight))
                    dto.IsDirectOrTransitFlight = flight.IsDirectOrTransitFlight;

                if (!string.IsNullOrEmpty(flight.FlightTicket))
                    dto.FlightTicket = _appCustomSettings.UploadsCMSPath + "/StaffGuestOtherEvents/FlightTickets/" + flight.FlightTicket;

                dto.ClassSeat = flight.ClassSeatAllotted;
            }
            else
            {
                if (flight.StartDate != null)
                    dto.StartDate = flight.StartDate.Value.ToString("MM/dd/yyyy");

                if (flight.EndDate != null)
                    dto.EndDate = flight.EndDate.Value.ToString("MM/dd/yyyy");

                dto.ClassSeat = flight.ClassSeat;
            }

            if (flight.ReturnFlightStatus == "Y" || flight.ReturnFlightStatus == "A")
            {
                dto.ReturnClassSeat = flight.ReturnClassSeatAllotted;
                dto.ReturnTerminal = flight.ReturnTerminal;
                if (flight.ReturnFlightBookingType != null)
                    dto.ReturnFlightBookingType = flight.ReturnFlightBookingType.ToString();
                dto.ReturnFlight = flight.ReturnFlight;
                if (!string.IsNullOrEmpty(flight.ReturnFlightTicket))
                    dto.ReturnFlightTicket = _appCustomSettings.UploadsCMSPath + "/Content/Uploads/StaffGuestOtherEvents/ReturnFlightTickets/" + flight.ReturnFlightTicket;
                dto.ReturnFlightNumber = flight.ReturnFlightNumber;
            }
            else
            {
                if (!string.IsNullOrEmpty(flight.ReturnClassSeat))
                    dto.ReturnClassSeat = flight.ReturnClassSeat;
            }
            if (!string.IsNullOrEmpty(flight.SuggestFlightAttachment))
                dto.SuggestedFlightFileName = _appCustomSettings.UploadsCMSPath + "/StaffGuestOtherEvents/SuggestedFlightDocument/" + flight.SuggestFlightAttachment;
            return dto;
        }
        private StaffGuestHotelBookingGetDTO LoadHotelInfo(XsiExhibitionOtherEventsStaffGuestBookingDetails hotel)
        {
            StaffGuestHotelBookingGetDTO dto = new StaffGuestHotelBookingGetDTO();


            dto.RemarksOrSpecialRequest = hotel.SpecialRequest;
            // dto.HotelStatus = GetStatus(hotel.HotelStatus, 2, 1);
            if (hotel.HotelStatus == "Y" || hotel.HotelStatus == "A")
            {
                dto.RoomType = hotel.RoomTypeAllotted;
                dto.ReservationReferenceNumber = hotel.ReservationReferenceNumber;
                dto.HotelName = hotel.HotelName;
                dto.HotelAddress = hotel.HotelAddress;
                dto.HotelPhoneNumber = hotel.HotelPhoneNumber;
                dto.CheckInTime = hotel.CheckInTime;
                dto.CheckOutTime = hotel.CheckOutTime;

                if (!string.IsNullOrEmpty(hotel.ReservationCopy))
                    dto.ReservationCopy = _appCustomSettings.UploadsCMSPath + "/Content/Uploads/StaffGuestOtherEvents/ReservationCopy/" + hotel.ReservationCopy;

                if (hotel.HotelStartDate != null)
                    dto.HotelBookingStartDate = hotel.HotelStartDate.Value.ToString("MM/dd/yyyy");
                if (hotel.HotelEndDate != null)
                    dto.HotelBookingEndDate = hotel.HotelEndDate.Value.ToString("MM/dd/yyyy");
            }
            else
            {
                if (hotel.HotelBookingStartDate != null)
                    dto.HotelBookingStartDate = hotel.HotelBookingStartDate.Value.ToString("MM/dd/yyyy");
                if (hotel.HotelBookingEndDate != null)
                    dto.HotelBookingEndDate = hotel.HotelBookingEndDate.Value.ToString("MM/dd/yyyy");

                dto.RoomType = hotel.RoomType;
            }
            return dto;
        }
        private StaffGuestTransportBookingGetDTO LoadTransportInfo(XsiExhibitionOtherEventsStaffGuestBookingDetails transport)
        {
            StaffGuestTransportBookingGetDTO dto = new StaffGuestTransportBookingGetDTO();
            string[] phone = { };

            if (!string.IsNullOrEmpty(transport.TransportContactNumber))
                phone = transport.TransportContactNumber.Split("$");
            if (transport.TransportStartDate != null)
                dto.TransportStartDate = transport.TransportStartDate.Value.ToString("dd/MM/yyyy");
            if (transport.TransportEndDate != null)
                dto.TransportEndDate = transport.TransportEndDate.Value.ToString("dd/MM/yyyy");
            dto.TransportContactPerson = transport.TransportContactPerson;
            //dto.TransportationStatus = GetStatus(transport.TransportationStatus, 3, 1);
            if (transport.IsIndividualOrGroup != null)
                dto.IsIndividualOrGroup = transport.IsIndividualOrGroup.ToString();

            if (transport.NoofPeople != null)
                dto.NoofPeople = transport.NoofPeople.ToString();

            if (transport.PickupLocation != null)
                dto.PickupLocation = transport.PickupLocation.ToString();
            if (phone.Length > 0)
            {
                dto.TransportContactNumber = phone[2];
                dto.TransportContactSTD = phone[1];
                dto.TransportContactISD = phone[0];
            }
            if (transport.TransportationStatus == "Y" || transport.TransportationStatus == "A")
            {
                dto.TransportationType = transport.TransportationTypeAllotted;
            }
            else
            {
                dto.TransportationType = transport.TransportationType;
            }
            return dto;
        }

        private string GetStatus(string strstatus, int section, long langid = 1)
        {
            if (section == 1)
            {
                switch (strstatus)
                {
                    case "N": return langid == 1 ? "Submitted" : "Submitted";  //New
                    case "A": return langid == 1 ? "Approved" : "Approved";
                    case "P": return langid == 1 ? "Pending" : "Pending";
                    case "Y": return langid == 1 ? "Ticket Issued" : "Ticket Issued";
                    case "D": return langid == 1 ? "Cancelled" : "Cancelled";
                    case "R": return langid == 1 ? "Rejected" : "Rejected";
                    default: return string.Empty;
                }
            }
            else if (section == 2)
            {
                switch (strstatus)
                {
                    case "N": return langid == 1 ? "Submitted" : "Submitted";
                    case "A": return langid == 1 ? "Approved" : "Approved";
                    case "P": return langid == 1 ? "Pending" : "Pending";
                    case "Y": return langid == 1 ? "Hotel Reserved" : "Hotel Reserved";
                    case "D": return langid == 1 ? "Cancelled" : "Cancelled";
                    case "R": return langid == 1 ? "Rejected" : "Rejected";
                    default: return string.Empty;
                }
            }
            else if (section == 3)
            {
                switch (strstatus)
                {
                    case "N": return langid == 1 ? "Submitted" : "Submitted";
                    case "A": return langid == 1 ? "Approved" : "Approved";
                    case "P": return langid == 1 ? "Pending" : "Pending";
                    case "Y": return langid == 1 ? "Transportation Reserved" : "Transportation Reserved";
                    case "D": return langid == 1 ? "Cancelled" : "Cancelled";
                    case "R": return langid == 1 ? "Rejected" : "Rejected";
                    default: return string.Empty;
                }
            }
            return string.Empty;
        }
        #endregion
        #region Email

        void NotifyAdminAboutBooking(string strstatus, long memberId, long staffguestid, long exhibitionid, long sectionid, long langid = 1)
        {
            //return;
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                EmailContentDTO contentDTO = new EmailContentDTO();
                ExhibitionOtherEventsStaffGuestService ExhibitionOtherEventsStaffGuestService = new ExhibitionOtherEventsStaffGuestService();
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);

                XsiExhibitionMember memberentity = new XsiExhibitionMember();
                memberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(Convert.ToInt64(memberId));

                if (memberentity != default(XsiExhibitionMember))
                {
                    var sgEntity = ExhibitionOtherEventsStaffGuestService.GetStaffGuestByItemId(staffguestid);
                    using (ExhibitionOtherEventsEmailContentService ExhibitionOtherEventsEmailContentService = new ExhibitionOtherEventsEmailContentService())
                    {
                        if (strstatus == "N")
                        {
                            #region Send Admin Email Notifying about Flight, Hotel or Transport Requested
                            StringBuilder body = new StringBuilder();

                            if (sectionid == 1)
                                contentDTO = GetContent(contentDTO, exhibitionid, 17, -1);
                            else if (sectionid == 2)
                                contentDTO = GetContent(contentDTO, exhibitionid, 18, -1);
                            else if (sectionid == 3)
                                contentDTO = GetContent(contentDTO, exhibitionid, 19, -1);
                            else if (sectionid == 4)
                                contentDTO = GetContent(contentDTO, exhibitionid, 1, -1);

                            contentDTO.strEmailContentBody = contentDTO.strEmailContentBody.Replace("$$Name$$", "Admin");
                            if (langid == 1)
                            {
                                if (sgEntity != null)
                                {
                                    if (!string.IsNullOrEmpty(sgEntity.NameEn) && !string.IsNullOrEmpty(sgEntity.LastName))
                                    {
                                        contentDTO.strEmailContentBody = contentDTO.strEmailContentBody.Replace("$$staffguestname$$", sgEntity.NameEn + " " + sgEntity.LastName);
                                    }
                                    else if (!string.IsNullOrEmpty(sgEntity.NameEn))
                                    {
                                        contentDTO.strEmailContentBody = contentDTO.strEmailContentBody.Replace("$$staffguestname$$", sgEntity.NameEn);
                                    }
                                    else if (!string.IsNullOrEmpty(sgEntity.LastName))
                                    {
                                        contentDTO.strEmailContentBody = contentDTO.strEmailContentBody.Replace("$$staffguestname$$", sgEntity.LastName);
                                    }
                                    else
                                    {
                                        contentDTO.strEmailContentBody = contentDTO.strEmailContentBody.Replace("$$staffguestname$$", string.Empty);
                                    }
                                }
                            }
                            else
                            {
                                if (sgEntity != null)
                                {
                                    if (sgEntity.NameAr != null)
                                        contentDTO.strEmailContentBody = contentDTO.strEmailContentBody.Replace("$$staffguestname$$", sgEntity.NameAr);
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(sgEntity.NameEn) && !string.IsNullOrEmpty(sgEntity.LastName))
                                        {
                                            contentDTO.strEmailContentBody = contentDTO.strEmailContentBody.Replace("$$staffguestname$$", sgEntity.NameEn + " " + sgEntity.LastName);
                                        }
                                        else if (!string.IsNullOrEmpty(sgEntity.NameEn))
                                        {
                                            contentDTO.strEmailContentBody = contentDTO.strEmailContentBody.Replace("$$staffguestname$$", sgEntity.NameEn);
                                        }
                                        else if (!string.IsNullOrEmpty(sgEntity.LastName))
                                        {
                                            contentDTO.strEmailContentBody = contentDTO.strEmailContentBody.Replace("$$staffguestname$$", sgEntity.LastName);
                                        }
                                        else
                                        {
                                            contentDTO.strEmailContentBody = contentDTO.strEmailContentBody.Replace("$$staffguestname$$", string.Empty);
                                        }
                                    }
                                }
                            }

                            body.Append(htmlContentFactory.BindExhibitionOtherEventsEmailContent(langid, contentDTO.strEmailContentBody, memberentity, exhibitionid, _appCustomSettings.ServerAddressNew, _appCustomSettings));
                            _emailSender.SendEmail(contentDTO.strEmailContentAdmin, _appCustomSettings.AdminEmail, contentDTO.strEmailContentEmail, contentDTO.strEmailContentSubject, body.ToString());
                            #endregion
                        }
                    }
                }
            }
        }

        private EmailContentDTO GetContent(EmailContentDTO contentDTO, long exhibitionid, long emailitemid, long scrfcontentbyitemid)
        {
            #region Email Content
            using (ExhibitionOtherEventsEmailContentService ExhibitionOtherEventsEmailContentService = new ExhibitionOtherEventsEmailContentService())
            {
                var emailContent = ExhibitionOtherEventsEmailContentService.GetExhibitionOtherEventsEmailContentByItemId(emailitemid);
                if (emailContent != null)
                {
                    if (emailContent.Body != null)
                        contentDTO.strEmailContentBody = emailContent.Body;
                    if (emailContent.Email != null && !string.IsNullOrEmpty(emailContent.Email))
                        contentDTO.strEmailContentEmail = emailContent.Email;
                    else
                        contentDTO.strEmailContentEmail = _appCustomSettings.AdminEmail;
                    if (emailContent.Subject != null)
                        contentDTO.strEmailContentSubject = emailContent.Subject;
                    contentDTO.strEmailContentAdmin = _appCustomSettings.AdminName;
                }
            }
            #endregion
            return contentDTO;
        }
        #endregion
    }
}

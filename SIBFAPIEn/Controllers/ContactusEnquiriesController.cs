﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using SIBFAPIEn.Utility;
using SIBFAPIEn.DTO;
using Microsoft.AspNetCore.Cors;
using LinqKit;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ContactusEnquiriesController : ControllerBase
    {
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public ContactusEnquiriesController(IOptions<AppCustomSettings> appCustomSettings, IEmailSender emailSender, sibfnewdbContext context)
        {
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        // POST: api/ContactusEnquiries
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public async Task<ActionResult<ContactusEnquiryDTO>> PostXsiContactusEnquiry(ContactusEnquiryDTO contactusEnquiryDTO)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            EnumResultType xsiResult = EnumResultType.Failed;
            long categoryId = contactusEnquiryDTO.CategoryId ?? -1;
            XsiContactusEnquiry entity = new XsiContactusEnquiry();
            entity.LanguageId = 1;
            entity.FirstName = contactusEnquiryDTO.FirstName.Trim();
            entity.LastName = contactusEnquiryDTO.LastName.Trim();
            entity.Email = contactusEnquiryDTO.Email;
            entity.Subject = contactusEnquiryDTO.Subject;
            entity.Detail = contactusEnquiryDTO.Detail;
            entity.Status = EnumConversion.ToString(EnumStatus.Open);
            entity.DateAdded = MethodFactory.ArabianTimeNow();
            entity.IsViewed = EnumConversion.ToString(EnumBool.No);
            entity.FlagType = EnumConversion.ToString(EnumFlag.Default);
            if (categoryId != -1)
                entity.CategoryId = contactusEnquiryDTO.CategoryId;
            _context.XsiContactusEnquiry.Add(entity);
            await _context.SaveChangesAsync();
            long itemId = entity.ItemId;
            if (itemId > 0)
            {
                xsiResult = EnumResultType.Success;
                contactusEnquiryDTO.ItemId = itemId;
                string strEmails = GetEmailByCategoryForContactUs(categoryId, Convert.ToInt64(EnumEmailCategory.ContactUs));

                if (strEmails != null && !string.IsNullOrWhiteSpace(strEmails))
                    if (!string.IsNullOrEmpty(strEmails))
                    {
                        StringBuilder body = new StringBuilder();
                        body.Append(GetEmailMessageContent(entity));

                        xsiResult = _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, strEmails, contactusEnquiryDTO.Subject, body.ToString(), null);
                    }
                string str;
                if (xsiResult == EnumResultType.EmailSent)
                    str = "<p>Your Request has been submitted and email sent to the department.</p><p> We'll get back to you soon.</p>";
                else
                    str = "<p>Your Request has been submitted.</p><p> We'll get back to you soon.</p>";
                return Ok(str);
            }
            return Ok(StatusCodes.Status400BadRequest);
        }

        StringBuilder GetEmailMessageContent(XsiContactusEnquiry xsiContactusEnquiry)
        {
            StringBuilder body = new StringBuilder();
            body.Append(string.Format("<p>Dear admin user,</p><p><strong>{0} {1} </strong> has submitted an inquiry.</p>", xsiContactusEnquiry.FirstName, xsiContactusEnquiry.LastName));
            body.Append("<p><strong>Inquiry details are: </strong></p>");
            body.Append(string.Format("<p>Name : {0}{1}</p>", xsiContactusEnquiry.FirstName, xsiContactusEnquiry.LastName));
            body.Append(string.Format("<p>Email : {0}</p>", xsiContactusEnquiry.Email));
            body.Append(string.Format("<p>Category : {0}</p>", GetEmailCategoryTitleById(xsiContactusEnquiry.CategoryId ?? -1)));
            body.Append(string.Format("<p>Subject : {0}</p>", xsiContactusEnquiry.Subject));
            body.Append(string.Format("<p>Message : {0}</p>", xsiContactusEnquiry.Detail));

            return body;
        }

        string GetEmailCategoryTitleById(long itemId)
        {
            XsiEmailCategory EmailCatEntity = _context.XsiEmailCategory.Where(i => i.ItemId == itemId && i.IsActive == "Y").FirstOrDefault();
            if (EmailCatEntity != null && !string.IsNullOrEmpty(EmailCatEntity.Title))
                return EmailCatEntity.Title;
            return string.Empty;
        }

        string GetEmailByCategoryForContactUs(long itemId, long categoryId)
        {
            var predicate = PredicateBuilder.New<XsiEmailCategory>();
            predicate = predicate.And(i => i.ItemId == itemId);
            predicate = predicate.And(i => i.CategoryId == categoryId);
            predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

            XsiEmailCategory emailCategory = _context.XsiEmailCategory.Where(predicate).FirstOrDefault();
            if (emailCategory != null)
                if (emailCategory.EmailId != null)
                    return emailCategory.EmailId;
            return string.Empty;
        }
    }
}

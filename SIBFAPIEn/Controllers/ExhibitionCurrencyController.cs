﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionCurrencyController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionCurrencyController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/ExhibitionCurrency
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ExhibitionCurrencyDTO>>> GetXsiExhibitionCurrency()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionCurrency>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionCurrency.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new ExhibitionCurrencyDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title,
                    CurrencyCode = x.CurrencyCode,
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionCurrency>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionCurrency.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new ExhibitionCurrencyDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.TitleAr,
                    CurrencyCode = x.CurrencyCode,
                }).ToListAsync();

                return await List;
            }
        }

        // GET: api/ExhibitionCurrency/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ExhibitionCurrencyDTO>> GetXsiExhibitionCurrency(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            var xsiItem = await _context.XsiExhibitionCurrency.FindAsync(id);

            if (xsiItem == null)
            {
                return NotFound();
            }

            ExhibitionCurrencyDTO itemDTO = new ExhibitionCurrencyDTO()
            {
                ItemId = xsiItem.ItemId,
                Title = LangId == 1 ? xsiItem.Title : xsiItem.TitleAr,
                CurrencyCode = xsiItem.CurrencyCode,
            };
            return itemDTO;
        }

        private bool XsiExhibitionCurrencyExists(long id)
        {
            return _context.XsiExhibitionCurrency.Any(e => e.ItemId == id);
        }
    }
}

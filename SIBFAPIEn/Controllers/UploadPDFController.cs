﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using SIBFAPIEn.Utility;
using SIBFAPIEn.DTO;
using Microsoft.AspNetCore.Cors;
using LinqKit;
using Microsoft.Extensions.Options;
using Xsi.ServicesLayer;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Net.Http.Headers;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    public class UploadPDFController : ControllerBase
    {
        EnumResultType XsiResult { get; set; }
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;

        public UploadPDFController(IOptions<AppCustomSettings> appCustomSettings, IEmailSender emailSender)
        {
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
        }

        [HttpPost, DisableRequestSizeLimit]
        [Route("UploadPDF")]
        [EnableCors("AllowOrigin")]
        public IActionResult Upload(string type)
        {
            UploadFileMessageDTO dto = new UploadFileMessageDTO();
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long memberid = User.Identity.GetID();

                var file = Request.Form.Files[0];
                switch (type)
                {
                    case "tgmember":
                        dto = SaveFile(dto, LangId, memberid, file, "translationgrantmember");
                        return Ok(dto);
                }

                dto.Message = "Module type missing";
                dto.MessageTypeResponse = "Error";
                return Ok(dto);
            }
            catch (Exception ex)
            {
                dto.MessageTypeResponse = "Error";
                dto.Message = "Something went wrong please try again later.";
                return Ok(dto);
            }
        }

        private UploadFileMessageDTO SaveFile(UploadFileMessageDTO dto, long LangId, long memberid, IFormFile file, string path)
        {
            #region Save File
            var folderName = Path.Combine(_appCustomSettings.UploadsPhysicalPath, path);
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            if (file.Length > 0)
            {
                var fileName = string.Format("{0}_{1}{2}{3}", LangId, memberid, MethodFactory.GetRandomNumber(), ".pdf");

                var fullPath = Path.Combine(pathToSave, fileName);
                var dbPath = Path.Combine(folderName, fileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                dto.FileName = fileName;
                dto.MessageTypeResponse = "Success";
                dto.PhysicalPath = dbPath;
                dto.ReturnURL = _appCustomSettings.UploadsCMSPath + "/" + path + "/" + fileName;
            }
            else
            {
                dto.MessageTypeResponse = "Error";
                dto.Message = "File length is empty.";
            }
            #endregion
            return dto;
        }
    }
}

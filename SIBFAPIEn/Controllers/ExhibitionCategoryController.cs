﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionCategoryController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionCategoryController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/ExhibitionCategory
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DropdownDataDTO>>> GetXsiExhibitionCategory()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionCategory>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.Title != "Restaurant" && i.Title != "Cafe");


                var List = _context.XsiExhibitionCategory.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(x => new DropdownDataDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionCategory>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.Title != "Restaurant" && i.Title != "Cafe");

                var List = _context.XsiExhibitionCategory.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr).Select(x => new DropdownDataDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.TitleAr
                }).ToListAsync();

                return await List;
            }
        }
        // GET: api/ExhibitionCategory/Restaurant
        [HttpGet]
        [Route("Restaurant")]
        public async Task<ActionResult<IEnumerable<DropdownDataDTO>>> GetXsiExhibitionRestaurantCategory()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionCategory>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionCategory.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(x => new DropdownDataDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionCategory>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionCategory.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr).Select(x => new DropdownDataDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.TitleAr
                }).ToListAsync();

                return await List;
            }
        }

        [HttpGet]
        [Route("MultiSelect")]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetXsiExhibitionCategoryMulti()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.New<XsiExhibitionCategory>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionCategory.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(x => new
                {
                    value = x.ItemId,
                    label = x.Title,
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.New<XsiExhibitionCategory>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionCategory.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr).Select(x => new
                {
                    value = x.ItemId,
                    label = x.TitleAr,
                }).ToListAsync();

                return await List;
            }
        }


        // GET: api/ExhibitionCategory/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDTO>> GetXsiExhibitionCategory(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            var xsiItem = await _context.XsiExhibitionCategory.FindAsync(id);

            if (xsiItem == null)
            {
                return NotFound();
            }

            CategoryDTO itemDTO = new CategoryDTO()
            {
                ItemId = xsiItem.ItemId,
                Title = LangId == 1 ? xsiItem.Title : xsiItem.TitleAr
            };
            return itemDTO;
        }

        private bool XsiExhibitionCountryExists(long id)
        {
            return _context.XsiExhibitionCountry.Any(e => e.CountryId == id);
        }
    }
}

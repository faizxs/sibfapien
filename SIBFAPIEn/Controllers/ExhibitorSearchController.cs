﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Xsi.ServicesLayer;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Caching.Memory;
using Xsi.BusinessLogicLayer;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitorSearchController : ControllerBase
    {
        private IMemoryCache _cache;
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;
        public List<XsiExhibition> cachedExhibition { get; set; }

        public ExhibitorSearchController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IMemoryCache memoryCache, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _cache = memoryCache;
            _context = context;
        }
        public List<XsiExhibitionMemberApplicationYearly> cachedExhibitionMemberApplicationYearly { get; set; }
        public List<XsiExhibitionExhibitorDetails> cachedExhibitionExhibitorDetails { get; set; }

        // GET: api/Exhibitors
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public async Task<ActionResult<dynamic>> GetExhibitor(ExhibitorSearchModel model)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);
                List<ExhibitorDTO> result = new List<ExhibitorDTO>();
                #region Current Exhibition
                // model.WebSiteId = 2;
                var exhibition = MethodFactory.GetActiveExhibitionNew(model.WebSiteId, _cache);
                
                #endregion
                if (exhibition != null)
                {
                    cachedExhibitionMemberApplicationYearly = (List<XsiExhibitionMemberApplicationYearly>)_cache.Get<dynamic>(CacheKeys.ExhibitionMemberApplicationYearlyCacheList);
                    if (cachedExhibitionMemberApplicationYearly == null)
                        cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

                    cachedExhibitionExhibitorDetails = (List<XsiExhibitionExhibitorDetails>)_cache.Get<dynamic>(CacheKeys.ExhibitionExhibitorDetailsCacheList);
                    if (cachedExhibitionExhibitorDetails == null)
                        cachedExhibitionExhibitorDetails = CacheKeys.GetExhibitionExhibitorDetails(_cache);


                    var predicate = PredicateBuilder.New<XsiExhibitionMemberApplicationYearly>();
                    predicate = predicate.And(i => (i.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved) || i.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval)));
                    predicate = predicate.And(i => i.MemberRoleId == 1 || i.MemberRoleId == 2);
                    predicate = predicate.And(i => i.ExhibitionId == exhibition.ExhibitionId);

                    if (langId != 1)
                    {
                        if (!string.IsNullOrEmpty(model.Letter) && model.Letter != "-1")
                        {
                            //var strletter = model.Letter.ToLower();
                            //predicate = predicate.And(i => i.PublisherNameAr != null && i.PublisherNameAr.StartsWith(strletter));

                            var likeExpression = model.Letter + "%";
                            predicate = predicate.And(i => i.PublisherNameAr != null && EF.Functions.Like(i.PublisherNameAr, likeExpression));
                        }
                        if (!string.IsNullOrEmpty(model.PublisherName))
                        {
                            var name = model.PublisherName.ToLower();
                            // predicate = predicate.And(i => i.PublisherNameAr != null && i.PublisherNameAr.ToLower().Contains(name));
                            if (!string.IsNullOrEmpty(name))
                            {
                                string str1 = name;
                                string str2 = name.Replace("أ", "إ");
                                string str3 = name.Replace("أ", "ا");
                                string str4 = name.Replace("إ", "أ");
                                string str5 = name.Replace("إ", "ا");
                                string str6 = name.Replace("ا", "أ");
                                string str7 = name.Replace("ا", "إ");

                                string str8 = name.Replace("ا", "إ");
                                string str9 = name.Replace("ا", "أ");

                                string str10 = name.Replace("إ", "أ");
                                string str11 = name.Replace("إ", "ا");

                                string str12 = name.Replace("إ", "أ");
                                string str13 = name.Replace("إ", "ا");

                                string str14 = name.Replace("ه", "ة");
                                string str15 = name.Replace("ة", "ه");

                                string str16 = name.Replace("ى", "ي");
                                string str17 = name.Replace("ي", "ى");

                                string str18 = name.Replace("ا", "ء");
                                string str19 = name.Replace("ا", "آ");
                                string str20 = name.Replace("ا", "ؤ");
                                string str21 = name.Replace("ا", "ئ");
                                string str22 = name.Replace("ا", "ا");

                                string str23 = name.Replace("ي", "ي");

                                string str24 = name.Replace("ة", "ة");
                                string str25 = name.Replace("ة", "ه");

                                string str26 = name.Replace("و", "و");
                                string str27 = name.Replace("و", "ؤ");

                                predicate = predicate.And(p => (p.PublisherNameEn != null && p.PublisherNameEn.Contains(name, StringComparison.OrdinalIgnoreCase))
                                || (p.PublisherNameAr != null && (p.PublisherNameAr.Contains(str1) || p.PublisherNameAr.Contains(str2) || p.PublisherNameAr.Contains(str3) || p.PublisherNameAr.Contains(str4) || p.PublisherNameAr.Contains(str5) || p.PublisherNameAr.Contains(str6) || p.PublisherNameAr.Contains(str7)
                                                   || p.PublisherNameAr.Contains(str8) || p.PublisherNameAr.Contains(str9) || p.PublisherNameAr.Contains(str10) || p.PublisherNameAr.Contains(str11)
                                                       || p.PublisherNameAr.Contains(str12) || p.PublisherNameAr.Contains(str13) || p.PublisherNameAr.Contains(str14) || p.PublisherNameAr.Contains(str15)
                                                         || p.PublisherNameAr.Contains(str16) || p.PublisherNameAr.Contains(str17)
                                                         || p.PublisherNameAr.Contains(str18) || p.PublisherNameAr.Contains(str19) || p.PublisherNameAr.Contains(str20) || p.PublisherNameAr.Contains(str21)
                                                          || p.PublisherNameAr.Contains(str22) || p.PublisherNameAr.Contains(str23) || p.PublisherNameAr.Contains(str24) || p.PublisherNameAr.Contains(str25)
                                                          || p.PublisherNameAr.Contains(str26) || p.PublisherNameAr.Contains(str27)))
                                                  );

                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(model.Letter) && model.Letter != "-1")
                        {
                            var strletter = model.Letter.ToLower();
                            predicate = predicate.And(i => i.PublisherNameEn != null && i.PublisherNameEn.StartsWith(strletter));
                        }
                        if (!string.IsNullOrEmpty(model.PublisherName))
                        {
                            var name = model.PublisherName.ToLower();
                            predicate = predicate.And(i => i.PublisherNameEn != null && i.PublisherNameEn.ToLower().Contains(name));
                        }
                    }
                    if (model.ExhibitionCategoryId != null && model.ExhibitionCategoryId != 0)
                        predicate = predicate.And(i => i.ExhibitionCategoryId == model.ExhibitionCategoryId);
                    if (model.CountryId != null && model.CountryId != 0)
                        predicate = predicate.And(i => i.CountryId == model.CountryId);
                    if (model.BoothSectionId != null && model.BoothSectionId != 0)
                    {
                        var ids = cachedExhibitionExhibitorDetails.Where(x => x.BoothSectionId == model.BoothSectionId).Select(x => x.MemberExhibitionYearlyId).ToList();
                        if (ids.Count > 0)
                            predicate = predicate.And(i => ids.Contains(i.MemberExhibitionYearlyId));
                    }
                    if (model.BoothSubSectionId != null && model.BoothSubSectionId != 0)
                    {
                        var subids = cachedExhibitionExhibitorDetails.Where(x => x.BoothSubSectionId == model.BoothSubSectionId).Select(x => x.MemberExhibitionYearlyId).ToList();
                        if (subids.Count > 0)
                            predicate = predicate.And(i => subids.Contains(i.MemberExhibitionYearlyId));
                    }

                    //result = _context.XsiExhibitionMemberApplicationYearly.Where(predicate).Select(i => new ExhibitorDTO
                    //{
                    //    MemberExhibitionYearlyId = i.MemberExhibitionYearlyId,
                    //    MemberId = i.MemberId,
                    //    isexhibitor = 1,
                    //    PublisherName = langId == 1 ? i.PublisherNameEn : i.PublisherNameAr,
                    //    TotalNumberOfTitles = i.TotalNumberOfTitles,
                    //    TotalNumberOfNewTitles = i.TotalNumberOfNewTitles,
                    //    Country = MethodFactory.GetCountryName(i.CountryId.HasValue ? i.CountryId.Value : 0, langId),
                    //    HallNumber = GetHallNumber(i.MemberExhibitionYearlyId, i.ParentId ?? 0),
                    //    StandNumber = GetStandNumber(i.MemberExhibitionYearlyId, i.ParentId ?? 0)
                    //}).Skip((model.pageNumber.Value - 1) * model.pageSize.Value).Take(model.pageSize.Value).ToList();

                    result = cachedExhibitionMemberApplicationYearly.Where(predicate).Select(i => new ExhibitorDTO
                    {
                        MemberExhibitionYearlyId = i.MemberExhibitionYearlyId,
                        MemberId = i.MemberId,
                        isexhibitor = 1,
                        PublisherName = langId == 1 ? i.PublisherNameEn : i.PublisherNameAr,
                        TotalNumberOfTitles = i.TotalNumberOfTitles,
                        TotalNumberOfNewTitles = i.TotalNumberOfNewTitles,
                        Country = MethodFactory.GetCountryNameNew(i.CountryId.HasValue ? i.CountryId.Value : 0, langId, _cache),
                        HallNumber = GetHallNumber(i.MemberExhibitionYearlyId, i.ParentId ?? 0),
                        StandNumber = GetStandNumber(i.MemberExhibitionYearlyId, i.ParentId ?? 0)
                    }).Skip((model.pageNumber.Value - 1) * model.pageSize.Value).Take(model.pageSize.Value).ToList();


                    //  int totalCount = _context.XsiExhibitionMemberApplicationYearly.Where(predicate).Select(i => i.MemberExhibitionYearlyId).Count();
                    int totalCount = cachedExhibitionMemberApplicationYearly.Where(predicate).Select(i => i.MemberExhibitionYearlyId).Count();

                    result = result.ToList();
                    var dto = new { result, count = totalCount };
                    return Ok(dto);
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetExhibitors action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpGet]
        [Route("exhibitor/{exhibitorid}")]
        public ActionResult<dynamic> GetExhibitor(long exhibitorid)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);
                ExhibitorRegistrationService ExhibitorRegistrationService;
                string strActivities = string.Empty;
                #region Exhibitor
                XsiExhibitionMemberApplicationYearly ExhibitorRegistrationEntity;
                XsiExhibitionMemberApplicationYearly ParentExhibitorRegistrationEntity;
                XsiExhibitorRegistrationEntityComplete ExhibitorRegistrationEntityComplete = new XsiExhibitorRegistrationEntityComplete();
                using (ExhibitorRegistrationService = new ExhibitorRegistrationService())
                {
                    XsiExhibitionExhibitorDetails details;
                    XsiExhibitionMemberApplicationYearly where = new XsiExhibitionMemberApplicationYearly();
                    where.MemberExhibitionYearlyId = exhibitorid;
                    ExhibitorRegistrationEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorid);

                    if (ExhibitorRegistrationEntity != null)
                    {
                        if (ExhibitorRegistrationEntity.ParentId > 0)
                            details = ExhibitorRegistrationService.GetDetailsById(ExhibitorRegistrationEntity.ParentId ?? 0);
                        else
                            details = ExhibitorRegistrationService.GetDetailsById(exhibitorid);

                        #region ExhibtiorRegistration Entity
                        #region Exhibitor Table
                        ExhibitorRegistrationEntityComplete.ExhibitorId = ExhibitorRegistrationEntity.MemberExhibitionYearlyId;
                        ExhibitorRegistrationEntityComplete.Address = ExhibitorRegistrationEntity.Address;
                        ExhibitorRegistrationEntityComplete.ContactPersonName = ExhibitorRegistrationEntity.ContactPersonNameAr;
                        ExhibitorRegistrationEntityComplete.ContactPersonTitle = ExhibitorRegistrationEntity.ContactPersonTitleAr;
                        ExhibitorRegistrationEntityComplete.ExhbitionActivity = null;
                        ExhibitorRegistrationEntityComplete.ExhibitionCategory = null;
                        ExhibitorRegistrationEntityComplete.ExhibitionCity = null;
                        ExhibitorRegistrationEntityComplete.ExhibitionCountry = null;
                        ExhibitorRegistrationEntityComplete.ExhibitionId = ExhibitorRegistrationEntity.ExhibitionId;
                        if (ExhibitorRegistrationEntity.Fax != null)
                            ExhibitorRegistrationEntityComplete.Fax = ExhibitorRegistrationEntity.Fax.Replace("$", "");
                        ExhibitorRegistrationEntityComplete.Email = ExhibitorRegistrationEntity.Email;
                        ExhibitorRegistrationEntityComplete.IsFirstTime = ExhibitorRegistrationEntity.IsFirstTime;
                        ExhibitorRegistrationEntityComplete.MemberId = ExhibitorRegistrationEntity.MemberId;
                        if (ExhibitorRegistrationEntity.Phone != null)
                            ExhibitorRegistrationEntityComplete.Phone = ExhibitorRegistrationEntity.Phone.Replace("$", "");
                        if (langId == 1)
                            ExhibitorRegistrationEntityComplete.PublisherName = ExhibitorRegistrationEntity.PublisherNameEn;
                        else
                            ExhibitorRegistrationEntityComplete.PublisherName = ExhibitorRegistrationEntity.PublisherNameAr;

                        ExhibitorRegistrationEntityComplete.Status = ExhibitorRegistrationEntity.Status;
                        ExhibitorRegistrationEntityComplete.TotalNumberOfTitles = ExhibitorRegistrationEntity.TotalNumberOfTitles;
                        ExhibitorRegistrationEntityComplete.TotalNumberOfNewTitles = ExhibitorRegistrationEntity.TotalNumberOfNewTitles;
                        ExhibitorRegistrationEntityComplete.UploadLocationMap = ExhibitorRegistrationEntity.UploadLocationMap;
                        ExhibitorRegistrationEntityComplete.FileName = _appCustomSettings.UploadsCMSPath + "/ExhibitorRegistration/Logo/" + details.FileName;
                        #endregion
                        #region Exhibitor details table
                        if (details != null)
                        {
                            ExhibitorRegistrationEntityComplete.AllocatedSpace = details.AllocatedSpace;
                            ExhibitorRegistrationEntityComplete.Area = details.Area;
                            ExhibitorRegistrationEntityComplete.StandNumberStart = details.StandNumberStart;
                            ExhibitorRegistrationEntityComplete.StandNumberEnd = details.StandNumberEnd;
                            ExhibitorRegistrationEntityComplete.HallNumber = details.HallNumber;
                            if (langId == 1)
                                ExhibitorRegistrationEntityComplete.Brief = details.Brief != null ? (details.Brief.Length > 300 ? details.Brief.Substring(0, 299) + "..." : details.Brief) : string.Empty;
                            else
                                ExhibitorRegistrationEntityComplete.Brief = details.BriefAr != null ? (details.BriefAr.Length > 300 ? details.BriefAr.Substring(0, 299) + "..." : details.BriefAr) : string.Empty;
                            #region Section and Subsection
                            if (details.BoothSectionId != null)
                            {
                                ExhibitionBoothService ExhibitionBoothService = new ExhibitionBoothService();
                                XsiExhibitionBooth where4 = new XsiExhibitionBooth();
                                where4.ItemId = details.BoothSectionId.Value;
                                XsiExhibitionBooth ExhibitionBoothEntity = ExhibitionBoothService.GetCompleteExhibitionBooth(where4, EnumSortlistBy.ByAlphabetAsc).FirstOrDefault();
                                if (ExhibitionBoothEntity != null)
                                {
                                    if (ExhibitionBoothEntity.Title != null && langId == 1)
                                        ExhibitorRegistrationEntityComplete.BoothSection = ExhibitionBoothEntity.Title;
                                    else if (ExhibitionBoothEntity.TitleAr != null && langId == 2)
                                        ExhibitorRegistrationEntityComplete.BoothSection = ExhibitionBoothEntity.TitleAr;
                                    else
                                        ExhibitorRegistrationEntityComplete.BoothSection = "";
                                    if (details.BoothSubSectionId != null)
                                    {
                                        ExhibitionBoothSubsectionService ExhibitionBoothSubsectionService = new ExhibitionBoothSubsectionService();
                                        XsiExhibitionBoothSubsection where5 = new XsiExhibitionBoothSubsection();
                                        where5.CategoryId = ExhibitionBoothEntity.ItemId;
                                        where5.ItemId = details.BoothSubSectionId.Value;
                                        XsiExhibitionBoothSubsection ExhibitionBoothSubEntity = ExhibitionBoothSubsectionService.GetExhibitionBoothSubsection(where5).FirstOrDefault();
                                        if (ExhibitionBoothSubEntity != null)
                                        {
                                            if (ExhibitionBoothSubEntity.Title != null && langId == 1)
                                                ExhibitorRegistrationEntityComplete.BoothSubSection = ExhibitionBoothSubEntity.Title;
                                            else if (ExhibitionBoothSubEntity.TitleAr != null && langId == 2)
                                                ExhibitorRegistrationEntityComplete.BoothSubSection = ExhibitionBoothSubEntity.TitleAr;
                                            else
                                                ExhibitorRegistrationEntityComplete.BoothSubSection = "";
                                        }
                                    }
                                    else
                                        ExhibitorRegistrationEntityComplete.BoothSubSection = string.Empty;
                                }
                                else
                                {
                                    ExhibitorRegistrationEntityComplete.BoothSection = string.Empty;
                                    ExhibitorRegistrationEntityComplete.BoothSubSection = string.Empty;
                                }
                            }
                            #endregion
                        }
                        #endregion
                        #endregion
                        #region get activities
                        ExhibitorActivityService ExhibitorActivityService = new ExhibitorActivityService();
                        XsiMemberExhibitionActivityYearly where6 = new XsiMemberExhibitionActivityYearly();
                        where6.MemberExhibitionYearlyId = ExhibitorRegistrationEntity.MemberExhibitionYearlyId;
                        List<long> ActivityIds = ExhibitorActivityService.GetExhibitorActivity(where6).Select(x => x.ActivityId).ToList();
                        if (ActivityIds.Count() > 0)
                        {
                            ExhibitionActivityService ExhibitionActivityService = new ExhibitionActivityService();
                            XsiExhibitionActivity where7 = new XsiExhibitionActivity();
                            where7.IsActive = EnumConversion.ToString(EnumBool.Yes);
                            List<XsiExhibitionActivity> ExhibitionActivityList = ExhibitionActivityService.GetExhibitionActivity(where7).Where(x => ActivityIds.Contains(x.ItemId)).ToList();
                            if (ExhibitionActivityList.Count() > 0)
                            {
                                foreach (XsiExhibitionActivity entity in ExhibitionActivityList)
                                {
                                    if (langId == 1)
                                        strActivities += ", " + entity.Title;
                                    else
                                        strActivities += ", " + entity.TitleAr;
                                }
                                if (strActivities.IndexOf(',') != -1)
                                    ExhibitorRegistrationEntityComplete.ExhbitionActivity = strActivities.Substring(1);
                            }
                        }
                        #endregion
                        if (ExhibitorRegistrationEntity.ExhibitionCategoryId.HasValue)
                        {
                            ExhibitionCategoryService ExhibitionCategoryService = new ExhibitionCategoryService();
                            XsiExhibitionCategory where2 = new XsiExhibitionCategory();
                            where2.ItemId = ExhibitorRegistrationEntity.ExhibitionCategoryId.Value;
                            List<XsiExhibitionCategory> ExhibitionCategoryList = ExhibitionCategoryService.GetExhibitionCategory(where2).ToList();
                            if (ExhibitionCategoryList.Count() > 0)
                            {
                                if (langId == 1)
                                    ExhibitorRegistrationEntityComplete.ExhibitionCategory = ExhibitionCategoryList[0].Title;
                                else
                                    ExhibitorRegistrationEntityComplete.ExhibitionCategory = ExhibitionCategoryList[0].TitleAr;
                            }
                        }
                        #region Country and City
                        if (ExhibitorRegistrationEntity.CountryId.HasValue)
                        {
                            ExhibitorRegistrationEntityComplete.ExhibitionCountry = MethodFactory.GetCountryName(ExhibitorRegistrationEntity.CountryId.Value, langId);
                        }
                        else
                            ExhibitorRegistrationEntityComplete.ExhibitionCountry = string.Empty;
                        if (ExhibitorRegistrationEntity.CityId.HasValue)
                        {
                            ExhibitorRegistrationEntityComplete.ExhibitionCity = MethodFactory.GetCityName(ExhibitorRegistrationEntity.CityId.Value, langId);
                        }
                        else
                            ExhibitorRegistrationEntityComplete.ExhibitionCity = string.Empty;
                        #endregion

                        return ExhibitorRegistrationEntityComplete;
                    }
                    else
                        return Ok("Data not found.");
                }
                #endregion
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetExhibitors action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }


        #region Helper Methods
        private string GetHallNumber(long memberExhibitionYearlyId, long parentid)
        {
            var details = cachedExhibitionExhibitorDetails.Where(x => x.MemberExhibitionYearlyId == memberExhibitionYearlyId).OrderByDescending(x => x.MemberExhibitionYearlyId).FirstOrDefault();
            if (details != null)
            {
                if (parentid > 0)
                {
                    details = cachedExhibitionExhibitorDetails.Where(x => x.MemberExhibitionYearlyId == parentid).OrderByDescending(x => x.MemberExhibitionYearlyId).FirstOrDefault();
                    if (details != null)
                    {
                        return details.HallNumber;
                    }
                }
                return details.HallNumber;
            }
            else if (parentid > 0)
            {
                details = cachedExhibitionExhibitorDetails.Where(x => x.MemberExhibitionYearlyId == parentid).OrderByDescending(x => x.MemberExhibitionYearlyId).FirstOrDefault();
                if (details != null)
                {
                    return details.HallNumber;
                }
            }

            return "";
        }
        private string GetStandNumber(long memberExhibitionYearlyId, long parentid)
        {
            var details = cachedExhibitionExhibitorDetails.Where(x => x.MemberExhibitionYearlyId == memberExhibitionYearlyId).OrderByDescending(x => x.MemberExhibitionYearlyId).FirstOrDefault();
            if (details != null)
            {
                if (parentid > 0)
                {
                    details = cachedExhibitionExhibitorDetails.Where(x => x.MemberExhibitionYearlyId == parentid).OrderByDescending(x => x.MemberExhibitionYearlyId).FirstOrDefault();
                    if (details != null)
                    {
                        return details.StandNumberEnd + details.StandNumberStart;
                    }
                }
                return details.StandNumberEnd + details.StandNumberStart;
            }
            else if (parentid > 0)
            {
                details = cachedExhibitionExhibitorDetails.Where(x => x.MemberExhibitionYearlyId == parentid).OrderByDescending(x => x.MemberExhibitionYearlyId).FirstOrDefault();
                if (details != null)
                {
                    return details.StandNumberEnd + details.StandNumberStart;
                }
            }

            //var details = _context.XsiExhibitionExhibitorDetails.Where(x => x.MemberExhibitionYearlyId == memberExhibitionYearlyId).OrderByDescending(x => x.MemberExhibitionYearlyId).FirstOrDefault();
            //if (details != null)
            //    return details.StandNumberEnd + details.StandNumberStart;
            return "";
        }
        #endregion
    }
    public class ExhibitorSearchModel
    {
        public string PublisherName { get; set; }
        public long? ExhibitionCategoryId { get; set; }
        public long? CountryId { get; set; }
        public long? BoothSectionId { get; set; }
        public long? BoothSubSectionId { get; set; }
        public long WebSiteId { get; set; }
        public int? pageNumber { get; set; } = 1;
        public int? pageSize { get; set; } = 16;
        public string Letter { get; set; }
    }
}

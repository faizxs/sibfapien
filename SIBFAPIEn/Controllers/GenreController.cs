﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class GenreController : ControllerBase
    {
        public GenreController()
        {
        }

        // GET: api/Genre
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryDTO>>> GetXsiGenre()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiGenre>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    var List = _context.XsiGenre.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new CategoryDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.Title,
                    }).ToListAsync();

                    return await List;
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiGenre>();
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                    var List = _context.XsiGenre.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new CategoryDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.TitleAr,
                    }).ToListAsync();

                    return await List;
                }
            }
        }

        // GET: api/Genre/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDTO>> GetXsiGenre(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var xsiItem = await _context.XsiGenre.FindAsync(id);

                if (xsiItem == null)
                {
                    return NotFound();
                }

                CategoryDTO itemDTO = new CategoryDTO()
                {
                    ItemId = xsiItem.ItemId,
                    Title = LangId == 1 ? xsiItem.Title : xsiItem.TitleAr,
                };
                return itemDTO;
            }
        }

        private bool XsiGenreExists(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiGenre.Any(e => e.ItemId == id);
            }
        }
    }
}

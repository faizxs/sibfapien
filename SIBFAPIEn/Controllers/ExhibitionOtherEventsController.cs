﻿using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionOtherEventsController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionOtherEventsController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/Exhibition/1
        [HttpGet]
        public async Task<ActionResult<List<DropdownDataDTO>>> GetXsiExhibition()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionOtherEvents>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                return await _context.XsiExhibitionOtherEvents.AsNoTracking().AsQueryable().Where(predicate).OrderByDescending(x => x.ExhibitionId).Select(i => new DropdownDataDTO() { ItemId = i.ExhibitionId, Title = i.ExhibitionNumber.ToString() }).ToListAsync();
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionOtherEvents>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                return await _context.XsiExhibitionOtherEvents.AsNoTracking().AsQueryable().Where(predicate).OrderByDescending(x => x.ExhibitionId).Select(i => new DropdownDataDTO() { ItemId = i.ExhibitionId, Title = i.ExhibitionNumber.ToString() }).ToListAsync();
            }
        }

        // GET: api/Exhibition/5
        [HttpGet]
        [Route("ExhibitionById/{id}")]
        public async Task<ActionResult<XsiExhibitionOtherEvents>> GetXsiExhibitionById(long id)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionOtherEvents>();
            predicate = predicate.And(i => i.ExhibitionId == id);

            return await _context.XsiExhibitionOtherEvents.AsNoTracking().AsQueryable().Where(predicate).OrderByDescending(x => x.ExhibitionId)
                .FirstOrDefaultAsync();
        }
    }
}

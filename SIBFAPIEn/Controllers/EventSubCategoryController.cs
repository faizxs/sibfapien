﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class EventSubCategoryController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public EventSubCategoryController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/EventSubCategory/5
        [HttpGet("{eventcategoryid}")]
        public async Task<ActionResult<IEnumerable<SubCategoryDTO>>> GetXsiEventSubCategory(int eventcategoryid)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiEventSubCategory>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.EventCategoryId == eventcategoryid);

                var List = _context.XsiEventSubCategory.AsQueryable().Where(predicate).OrderBy(x => x.Title.Trim()).Select(i => new SubCategoryDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.Title,
                    CategoryTitle = i.EventCategory.Title,
                    CategoryId = i.EventCategory.ItemId
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiEventSubCategory>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.EventCategoryId == eventcategoryid);

                var List = _context.XsiEventSubCategory.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr.Trim()).Select(i => new SubCategoryDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.TitleAr,
                    CategoryTitle = i.EventCategory.TitleAr,
                    CategoryId = i.EventCategory.ItemId
                }).ToListAsync();

                return await List;
            }
        }

        // GET: api/EventSubCategory/5/1
        [HttpGet("{eventcategoryid}/{id}")]
        public async Task<ActionResult<SubCategoryDTO>> GetXsiEventSubCategory(int eventcategoryid, long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var subCategory = await _context.XsiEventSubCategory.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes))
                                            .Select(i => new SubCategoryDTO()
                                            {
                                                ItemId = i.ItemId,
                                                Title = i.Title,
                                                CategoryTitle = i.EventCategory.Title,
                                                CategoryId = i.EventCategory.ItemId
                                            }).FirstOrDefaultAsync();

                if (subCategory == null)
                {
                    return NotFound();
                }

                return subCategory;
            }
            else
            {
                var subCategory = await _context.XsiEventSubCategory.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes))
                .Select(i => new SubCategoryDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.TitleAr,
                    CategoryTitle = i.EventCategory.TitleAr,
                    CategoryId = i.EventCategory.ItemId
                }).FirstOrDefaultAsync();

                if (subCategory == null)
                {
                    return NotFound();
                }

                return subCategory;
            }
        }

        private bool XsiEventSubCategoryExists(long id)
        {
            return _context.XsiEventSubCategory.Any(e => e.ItemId == id);
        }
    }
}

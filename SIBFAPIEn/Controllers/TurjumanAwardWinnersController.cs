﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class TurjumanAwardWinnersController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;

        public TurjumanAwardWinnersController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings)
        {
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
        }
        // GET: api/Awards
        [HttpGet]
        [Route("{pageNumber}/{pageSize}/{exhibitionid}")]
        public async Task<ActionResult<dynamic>> GetXsiAwardWinners(int? pageNumber = 1, int? pageSize = 15, long exhibitionid = -1)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long WebsiteId = Convert.ToInt32(EnumWebsiteTypeId.SIBF);
                    //    MethodFactory.LoadPageContentsNew(92, langID);
                    var thumbnailpath = _appCustomSettings.UploadsPath + "/TurjumanWinner/BookCover/";
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    var predicate = PredicateBuilder.True<XsiSharjahAwardForTranslation>();

                    if (exhibitionid > -1)
                        predicate = predicate.And(i => i.ExhibitionId == exhibitionid);

                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.Status == EnumConversion.ToString(EnumAwardNominationStatus.Winner));
                    if (LangId == 1)
                    {
                        var WinnerList = _context.XsiSharjahAwardForTranslation.Where(predicate).OrderBy(i => i.TranslatorNameEn).ToList();

                        var List = WinnerList.Select(x => new TranslationAwardWinnerDto()
                        {
                            ItemId = x.ItemId,
                            AwardId = x.AwardId ?? -1,
                            ExhibitionId = x.ExhibitionId ?? -1,
                            AwardTitle = GetAwardTitle(x.AwardId ?? -1, LangId),
                            TranslatorName = x.TranslatorNameEn,
                            BookTitle = x.BookTitleEn,
                            BookThumbnail = thumbnailpath + (x.BookCover ?? "award-tile.jpg"),
                        }).ToList();

                        var totalCount = List.Count;
                        List = List.Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToList();
                        var dto = new { List, TotalCount = totalCount };

                        return Ok(dto);
                    }
                    else
                    {
                        var WinnerList = _context.XsiSharjahAwardForTranslation.Where(predicate).OrderBy(i => i.TranslatorNameEn).ToList();

                        var List = WinnerList.Select(x => new TranslationAwardWinnerDto()
                        {
                            ItemId = x.ItemId,
                            AwardId = x.AwardId ?? -1,
                            ExhibitionId = x.ExhibitionId ?? -1,
                            AwardTitle = GetAwardTitle(x.AwardId ?? -1, LangId),
                            TranslatorName = x.TranslatorNameAr,
                            BookTitle = GetBookTitle(x.BookTitleEn, x.BookTitleAr),
                            BookThumbnail = thumbnailpath + (x.BookCover ?? "award-tile.jpg"),
                        }).ToList();

                        var totalCount = List.Count;
                        List = List.Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToList();
                        var dto = new { List, TotalCount = totalCount };

                        return Ok(dto);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwardWinners action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        private string GetBookTitle(string bookTitleEn, string bookTitleAr)
        {
            if (!string.IsNullOrEmpty(bookTitleAr))
                return bookTitleAr;

            return bookTitleEn;
        }

        string GetAwardTitle(long awardid, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var predicate = PredicateBuilder.True<XsiAwards>();
                predicate = predicate.And(i => i.ItemId == awardid);
                if (langId == 1)
                {
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    XsiAwards entity = _context.XsiAwards.Where(predicate).FirstOrDefault();
                    if (entity != null)
                    {
                        return entity.Name;
                    }
                }
                else
                {
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                    XsiAwards entity = _context.XsiAwards.Where(predicate).FirstOrDefault();
                    if (entity != null)
                    {
                        return entity.NameAr;
                    }
                }
                return string.Empty;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/Exhibition/1
        [HttpGet("{websiteid}")]
        public async Task<ActionResult<dynamic>> GetXsiExhibition(long websiteid)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibition>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
                predicate = predicate.And(i => i.WebsiteId == websiteid);

                return _context.XsiExhibition.AsQueryable().Where(predicate).OrderByDescending(x => x.ExhibitionId).FirstOrDefault();
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibition>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
                predicate = predicate.And(i => i.WebsiteId == websiteid);

                return _context.XsiExhibition.AsQueryable().Where(predicate).OrderByDescending(x => x.ExhibitionId).FirstOrDefault();
            }
        }

        // GET: api/Exhibition/5
        [HttpGet]
        [Route("ExhibitionById/{id}")]
        public async Task<ActionResult<dynamic>> GetXsiExhibitionById(long id)
        {
            var predicate = PredicateBuilder.True<XsiExhibition>();
            predicate = predicate.And(i => i.ExhibitionId == id);

            return _context.XsiExhibition.AsQueryable().Where(predicate).OrderByDescending(x => x.ExhibitionId)
                .FirstOrDefaultAsync();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Xsi.ServicesLayer;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitorRevController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;

        public ExhibitorRevController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
        }
        public List<XsiExhibitionMemberApplicationYearly> cachedExhibitionMemberApplicationYearly { get; set; }
        public List<XsiExhibitionExhibitorDetails> cachedExhibitionExhibitorDetails { get; set; }

        [HttpGet]
        [Route("{exhibitorid}")]
        public ActionResult<dynamic> GetGenerateTokenForExhibitorRev(long exhibitorid)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);
                
                using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
                {
                    var ExhibitorRegistrationEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorid);
                    if (ExhibitorRegistrationEntity != null)
                    {
                        var strtkn = GenerateTokenForRevenue(exhibitorid);
                        ExhibitorRegistrationEntity.RevenueToken = strtkn;
                        ExhibitorRegistrationEntity.RevenueTokenCreatedOn = MethodFactory.ArabianTimeNow();
                        ExhibitorRegistrationService.UpdateExhibitorRegistration(ExhibitorRegistrationEntity);
                        var dto = new { exhibitorid = exhibitorid, token = strtkn };
                        return (dto);
                    }
                }
                return Ok("0");
            }
            catch (Exception ex)
            {
                return Ok("0");
            }
        }

        [HttpGet]
        [Route("ValidateTokenForRevenue/{token}")]
        public ActionResult<dynamic> GetExhibitor(string token)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);
                #region Exhibitor
                using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
                {
                    XsiExhibitionMemberApplicationYearly ExhibitorRegistrationEntity = new XsiExhibitionMemberApplicationYearly();

                    ExhibitorRegistrationEntity = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly() { RevenueToken = token }).FirstOrDefault();

                    if (ExhibitorRegistrationEntity != null)
                    {
                        var dtnow = MethodFactory.ArabianTimeNow();
                        var diffOfDates = dtnow.Subtract(ExhibitorRegistrationEntity.RevenueTokenCreatedOn.Value);

                        var timeinminute = diffOfDates.Minutes;
                        if (timeinminute <= 1)
                        {
                            if (!string.IsNullOrEmpty(ExhibitorRegistrationEntity.FileNumber))
                            {
                                var dto = new { FileNumber = ExhibitorRegistrationEntity.FileNumber };
                                return (dto);
                            }
                        }
                    }
                    return Ok("0");
                }
                #endregion
            }
            catch (Exception ex)
            {
                return Ok("0");
            }
        }

        private string GenerateTokenForRevenue(long exhibitorid)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                var strtkn = MethodFactory.GenerateTokenCodeForRevenueSystem(13);
                if (ExhibitorRegistrationService.GetExhibitorRegistration().Any(i => i.MemberExhibitionYearlyId == exhibitorid && i.RevenueToken == strtkn))
                {
                    GenerateTokenForRevenue(exhibitorid);
                }
                return strtkn;
            }
        }
    }
}

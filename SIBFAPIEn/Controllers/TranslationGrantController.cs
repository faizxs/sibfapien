﻿using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using Xsi.ServicesLayer;
using System.IO;
using System.Text;
using Contracts;
using Microsoft.Extensions.Localization;
using System.Diagnostics;
using System.Threading;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TranslationGrantController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly IEmailSender _emailSender;
        private readonly sibfnewdbContext _context;
        public TranslationGrantController(IEmailSender emailSender,
           IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, sibfnewdbContext context)
        {
            _emailSender = emailSender;
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        #region Get Methods
        [HttpGet]
        [Route("{websiteid}/{memberid}/{tgid}/{tgregistrationid}")]
        [Route("{websiteid}/{memberid}/{tgid}/{tgregistrationid}/{isfromlist}")]
        public ActionResult<IEnumerable<TranslationGrantLoadDTO>> GetTranslationGrant(long websiteid, long memberid, long tgid, long tgregistrationid = -1, string isfromlist = "N")
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                TGValidationDTO validationDTO = new TGValidationDTO();
                validationDTO.TranslationgrantId = -1;
                TranslationGrantLoadDTO model = new TranslationGrantLoadDTO();
                long MemberId = User.Identity.GetID();
                if (MemberId == memberid)
                {
                    validationDTO.MemberId = MemberId;

                    validationDTO = IsNewRegistrationDatesAvailable(validationDTO);

                    model.MemberId = MemberId;
                    model.IsStartNew = !validationDTO.IsDateExpired;

                    var TGListYears = GetLatestTranslationGrants();
                    var tgentity = GetTranslationGrantById(tgid);
                    if (tgentity != null)
                    {
                        validationDTO.ExhibitionId = tgentity.ExhibitionId.Value;
                        validationDTO.TranslationgrantId = tgentity.ItemId; //Immediate previous year
                    }
                    else
                    {
                        model.TranslationGrantId = validationDTO.TranslationgrantId;
                    }

                    validationDTO.WebsiteId = 1;
                    // validationDTO.IsDateExpired = true;

                    if (validationDTO.IsDateExpired && tgregistrationid == -1)
                    {
                        model.StepId = 1;
                        if (IsRegistrationInProcess(MemberId, validationDTO.TranslationgrantId))
                        {
                            model.StepId = 0;
                            model.TranslationList = BindTranslationGrant(MemberId, validationDTO.TranslationgrantId, LangId);
                        }
                        else
                            model.Message = "Registration Dates Expired";
                    }
                    else if (validationDTO.IsDateExpired)
                    {
                        if (tgregistrationid > 0)
                        {
                            model.StepId = 2;
                            model.Message = "";
                            if (isfromlist == "Y")
                                model = GetStepsData(tgregistrationid, LangId, "Y");
                            else
                                model = GetStepsData(tgregistrationid, LangId);
                        }
                        else
                        {
                            model.StepId = 0;
                            model.TranslationList = BindTranslationGrant(MemberId, validationDTO.TranslationgrantId, LangId);
                        }
                    }
                    else
                    {
                        if (tgregistrationid == -1 && IsFirstFormRegistration(MemberId, validationDTO.TranslationgrantId))
                        {
                            model.StepId = 1;
                            model.Message = "";
                            model.StepTwoSectionOneDTO = GetStepTwoSectionOne(MemberId);
                        }
                        else if (tgregistrationid > 0)
                        {
                            model.StepId = 2;
                            model.Message = "";
                            if (isfromlist == "Y")
                                model = GetStepsData(tgregistrationid, LangId, "Y");
                            else
                                model = GetStepsData(tgregistrationid, LangId);
                        }
                        else
                        {
                            model.TranslationList = BindTranslationGrant(MemberId, validationDTO.TranslationgrantId, LangId);
                            model.StepId = 0;
                            //MVRegistration.SetActiveView(Step0View);
                        }

                        if (tgregistrationid == 0 && validationDTO.IsDateExpired == false)
                        {
                            model.StepId = 2;
                            model.Message = "";
                            model.StepTwoSectionOneDTO = GetStepTwoSectionOne(MemberId);
                        }
                    }
                    return Ok(model);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Translation Grant Registration StepThree action: {ex.InnerException}");
                // return StatusCode(500, "Internal server error " + ex.InnerException);
                return Ok(new TGMessageDTO() { Message = "Something went wrong. Please try again later. ", MessageTypeResponse = "Error" });
            }
        }

        [HttpGet]
        [Route("LoadSellersForm/{translationgrantmemberid}")]
        public ActionResult<IEnumerable<dynamic>> GetLoadSellersForm(long translationgrantmemberid)
        {
            try
            {
                long MemberId = User.Identity.GetID();
                if (MemberId >= 0 && translationgrantmemberid > 0)
                {
                    using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
                    {
                        XsiExhibitionTranslationGrantMembers TranslationGrantMember = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(translationgrantmemberid);
                        if (TranslationGrantMember != null)
                        {
                            StepTwoSectionOneDTO sectionOneDTO = new StepTwoSectionOneDTO();
                            //translationGrantLoadDTO.StepId = 2;
                            if (MemberId == TranslationGrantMember.MemberId)
                            {
                                //  translationGrantLoadDTO.MemberId = MemberId;
                                if (!string.IsNullOrEmpty(TranslationGrantMember.SellerTelephone))
                                {
                                    sectionOneDTO.SellerPhoneISD = TranslationGrantMember.SellerTelephone.Split('$')[0];
                                    sectionOneDTO.SellerPhoneSTD = TranslationGrantMember.SellerTelephone.Split('$')[1];
                                    sectionOneDTO.SellerPhone = TranslationGrantMember.SellerTelephone.Split('$')[2];
                                }
                                if (!string.IsNullOrEmpty(TranslationGrantMember.SellerFax))
                                {
                                    sectionOneDTO.SellerFaxISD = TranslationGrantMember.SellerFax.Split('$')[0];
                                    sectionOneDTO.SellerFaxSTD = TranslationGrantMember.SellerFax.Split('$')[1];
                                    sectionOneDTO.SellerFax = TranslationGrantMember.SellerFax.Split('$')[2];
                                }
                                sectionOneDTO.SellerFirstName = TranslationGrantMember.SellerFirstName;
                                sectionOneDTO.SellerMiddleName = TranslationGrantMember.SellerMiddleName;
                                sectionOneDTO.SellerJobTitle = TranslationGrantMember.SellerLastName;
                                sectionOneDTO.SellerJobTitle = TranslationGrantMember.SellerJobTitle;
                                sectionOneDTO.SellerCompanyName = TranslationGrantMember.SellerCompanyName;
                                sectionOneDTO.SellerAddress = TranslationGrantMember.SellerAddress;
                                sectionOneDTO.SellerEmail = TranslationGrantMember.SellerEmail;
                                return Ok(sectionOneDTO);
                            }
                            return Unauthorized();
                        }
                    }
                    return Ok("No data found.");
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok(new TGMessageDTO() { Message = "Something went wrong. Please try again later. ", MessageTypeResponse = "Error" });
            }
        }

        [HttpGet]
        [Route("SellersList/{memberid}")]
        public ActionResult<IEnumerable<dynamic>> OldSellerList(long memberid)
        {
            try
            {
                long MemberId = User.Identity.GetID();
                if (MemberId == memberid)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    List<DropdownDataDTO> OldSellersList = new List<DropdownDataDTO>();
                    using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
                    {
                        if (LangId == 1)
                        {
                            OldSellersList = TranslationGrantMemberService.GetTranslationGrantMember(new XsiExhibitionTranslationGrantMembers() { MemberId = memberid }).Where(i => i.MemberId != null).Select(x => new DropdownDataDTO()
                            {
                                ItemId = x.ItemId,
                                Title = x.SellerName
                            }).ToList();
                        }
                        else
                        {
                            OldSellersList = TranslationGrantMemberService.GetTranslationGrantMember(new XsiExhibitionTranslationGrantMembers() { MemberId = memberid }).Where(i => i.MemberId != null).Select(x => new DropdownDataDTO()
                            {
                                ItemId = x.ItemId,
                                Title = x.SellerName
                            }).ToList();
                            return Ok(OldSellersList);
                        }
                        return Ok(OldSellersList);
                    }
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok(new TGMessageDTO() { Message = "Something went wrong. Please try again later. ", MessageTypeResponse = "Error" });
            }
        }

        [HttpGet]
        [Route("PrintForm/{translationgrantmemberid}")]
        public ActionResult<string> GetPrintForm(long translationgrantmemberid)
        {
            try
            {
                long MemberId = User.Identity.GetID();
                if (MemberId >= 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
                    {
                        XsiExhibitionTranslationGrantMembers entity = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(translationgrantmemberid);
                        StringBuilder applicationform = new StringBuilder();
                        if (entity != default(XsiExhibitionTranslationGrantMembers))
                        {
                            if (MemberId == entity.MemberId)
                            {
                                //applicationform
                                if (LangId == 1)
                                    applicationform.Append(System.IO.File.ReadAllText(_appCustomSettings.ApplicationPhysicalPath + "Content/EmailMedia/TranslationGrantApplicationForm.html"));
                                else
                                    applicationform.Append(System.IO.File.ReadAllText(_appCustomSettings.ApplicationPhysicalPath + "Content/EmailMedia/TranslationGrantApplicationFormAr.html"));
                                #region Replace Placeholders
                                string strtranslationtpe = entity.TranslationType;
                                if (strtranslationtpe == "A")
                                    strtranslationtpe = (LangId == 1) ? "Arabic To Any Language" : "من اللغة العربية إلى أي لغة أخرى";
                                else if (strtranslationtpe == "B")
                                    strtranslationtpe = (LangId == 1) ? "Any Language To Arabic" : "من أي لغة إلى اللغة العربية";
                                else if (strtranslationtpe == "C")
                                    strtranslationtpe = (LangId == 1) ? "Any Language To Any Language" : "من أي لغة إلى أي لغة";
                                applicationform.Replace("$$translationgranttype$$", strtranslationtpe);
                                applicationform.Replace("$$name$$", entity.SellerFirstName + " " + entity.SellerMiddleName + " " + entity.SellerLastName);
                                applicationform.Replace("$$jobtitle$$", entity.SellerJobTitle);
                                applicationform.Replace("$$companyname$$", entity.SellerCompanyName);
                                applicationform.Replace("$$address$$", entity.SellerAddress);
                                applicationform.Replace("$$telephoneno$$", entity.SellerTelephone.Replace("$", ""));
                                if (!string.IsNullOrEmpty(entity.SellerFax))
                                    applicationform.Replace("$$faxno$$", entity.SellerFax.Replace("$", ""));
                                else
                                    applicationform.Replace("$$faxno$$", "");
                                applicationform.Replace("$$emailaddress$$", entity.SellerEmail);

                                applicationform.Replace("$$buyername$$", entity.BuyerFirstName + " " + entity.BuyerMiddleName + " " + entity.BuyerLastName);
                                applicationform.Replace("$$buyerjobtitle$$", entity.BuyerJobTitle);
                                applicationform.Replace("$$buyercompanyname$$", entity.BuyerCompanyName);
                                applicationform.Replace("$$buyeraddress$$", entity.BuyerAddress);
                                applicationform.Replace("$$buyertelephoneno$$", entity.BuyerTelephone.Replace("$", ""));

                                if (!string.IsNullOrEmpty(entity.BuyerFax))
                                    applicationform.Replace("$$buyerfaxno$$", entity.BuyerFax.Replace("$", ""));
                                else
                                    applicationform.Replace("$$buyerfaxno$$", "");
                                applicationform.Replace("$$buyeremailaddress$$", entity.BuyerEmail);
                                applicationform.Replace("$$detailstitleinoriginallanguage$$", entity.BookTitleOriginal);
                                applicationform.Replace("$$detailstitleinenglish$$", entity.BookTitleEnglish);
                                applicationform.Replace("$$detailstitleinarabic$$", entity.BookTitleArabic);
                                applicationform.Replace("$$detailsauthor$$", entity.Author);
                                applicationform.Replace("$$detailsisbn$$", entity.Isbn);

                                if (entity.GenreId == 10)
                                    applicationform.Replace("$$detailsgengre$$", entity.Genre);
                                else
                                    applicationform.Replace("$$detailsgengre$$", MethodFactory.GetTranslationGranteGenrebyId(entity.GenreId.Value, LangId));

                                applicationform.Replace("$$detailsnumberofpages$$", entity.NoOfPagesOriginal);
                                applicationform.Replace("$$detailsnumberofwords$$", entity.NoOfWords);
                                applicationform.Replace("$$detailsoriginallanguage$$", MethodFactory.GetExhibitionLanuageTitle(entity.OriginalLanguageId.Value, LangId));
                                applicationform.Replace("$$detailsproposedlanguageoftranslation$$", MethodFactory.GetExhibitionLanuageTitle(entity.ProposedLanguageId.Value, LangId));
                                applicationform.Replace("$$ServerAddressCMS$$", _appCustomSettings.ServerAddressCMS);
                                #endregion
                            }
                            return Unauthorized();
                        }
                        return applicationform.ToString();
                    }
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok(new TGMessageDTO() { Message = "Something went wrong. Please try again later. ", MessageTypeResponse = "Error" });
            }
        }

        [HttpGet]
        [Route("CancelRegistration/{memberid}/{translationgrantmemberid}")]
        public ActionResult<dynamic> GetCancelRegistration(long memberid, long translationgrantmemberid)
        {
            try
            {
                long MemberId = User.Identity.GetID();
                if (MemberId == memberid)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
                    {
                        XsiExhibitionTranslationGrantMembers entity = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(translationgrantmemberid);
                        if (entity != default(XsiExhibitionTranslationGrantMembers))
                        {
                            if (entity.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Temporary)
                                || entity.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.UnderEvaluation)
                                || entity.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.New)
                                || entity.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.IncorrectDocuments)
                                || entity.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.BankDetailsResubmitted)
                                )
                            {
                                CancelRegistration(translationgrantmemberid, MemberId, LangId);
                                return Ok(new MessageDTO() { Message = "Registration has been cancelled successfully.", MessageTypeResponse = "Success" });
                            }
                            return Ok(new MessageDTO() { Message = "Cancellation can be done before approval only.", MessageTypeResponse = "Error" });
                        }
                    }
                }
                return Ok(new MessageDTO() { Message = "Unauthorised user access.", MessageTypeResponse = "Error" });
            }
            catch (Exception ex)
            {
                return Ok(new TGMessageDTO() { Message = "Something went wrong. Please try again later. ", MessageTypeResponse = "Error" });
            }
        }
        #endregion

        #region Post Methods
        [HttpPost]
        [Route("SaveAndPrint")]
        public ActionResult<IEnumerable<dynamic>> PostSaveAndPrint(TranslationGrantPost model)
        {
            try
            {
                long MemberId = User.Identity.GetID();
                if (MemberId >= 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    #region Submitted Successfully

                    XsiExhibitionTranslationGrant ActiveTranslationGrant = new XsiExhibitionTranslationGrant();
                    TranslationGrantService TranslationGrantService = new TranslationGrantService();
                    XsiExhibitionTranslationGrant WhereTranslationGrant = new XsiExhibitionTranslationGrant();
                    WhereTranslationGrant.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    WhereTranslationGrant.IsArchive = EnumConversion.ToString(EnumBool.No);
                    if (model.TranslationGrantId > 0)
                    {
                        ActiveTranslationGrant = TranslationGrantService.GetTranslationGrantByItemId(model.TranslationGrantId.Value);
                    }
                    else
                    {
                        ActiveTranslationGrant = TranslationGrantService.GetTranslationGrant(WhereTranslationGrant, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                    }


                    using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
                    {
                        XsiExhibitionTranslationGrantMembers TranslationGrantMember;
                        XsiExhibitionTranslationGrantMembers OldTranslationGrantMember;//= new XsiExhibitionTranslationGrantMembers();
                        if (model.TranslationGrantMemberId <= 0) // nee to set item id
                        {
                            TranslationGrantMember = new XsiExhibitionTranslationGrantMembers();
                            OldTranslationGrantMember = new XsiExhibitionTranslationGrantMembers();
                            TranslationGrantMember.TranslationGrantId = ActiveTranslationGrant.ItemId;
                        }
                        else
                        {
                            TranslationGrantMember = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(model.TranslationGrantMemberId);
                            OldTranslationGrantMember = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(model.TranslationGrantMemberId);

                            WhereTranslationGrant = new XsiExhibitionTranslationGrant();
                            WhereTranslationGrant.ItemId = TranslationGrantMember.TranslationGrantId.Value;
                            ActiveTranslationGrant = TranslationGrantService.GetTranslationGrant(WhereTranslationGrant, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                        }

                        if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Canceled) || TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.CancelledByUser))
                        {
                            return Ok(new TGMessageDTO() { Message = "Registration cannot be processed due to registration status: Cancelled.", MessageTypeResponse = "Error" });
                        }


                        TranslationGrantMember.MemberId = MemberId;
                        OldTranslationGrantMember.MemberId = MemberId;

                        TranslationGrantMember.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        TranslationGrantMember.TranslationType = model.TranslationType;

                        TranslationGrantMember.SellerFirstName = model.SellerFirstName;
                        TranslationGrantMember.SellerMiddleName = model.SellerMiddleName;
                        TranslationGrantMember.SellerLastName = model.SellerLastName;
                        TranslationGrantMember.SellerName = model.SellerFirstName + " " + model.SellerMiddleName + " " + model.SellerLastName;
                        TranslationGrantMember.SellerJobTitle = model.SellerJobTitle;
                        TranslationGrantMember.SellerCompanyName = model.SellerCompanyName;
                        TranslationGrantMember.SellerAddress = model.SellerAddress;
                        if (!string.IsNullOrEmpty(model.SellerPhoneISD) && !string.IsNullOrEmpty(model.SellerPhoneSTD) && !string.IsNullOrEmpty(model.SellerPhone))
                            TranslationGrantMember.SellerTelephone = model.SellerPhoneISD + "$" + model.SellerPhoneSTD + "$" + model.SellerPhone;
                        else
                            TranslationGrantMember.SellerTelephone = string.Empty;
                        if (!string.IsNullOrEmpty(model.SellerFaxISD) && !string.IsNullOrEmpty(model.SellerFaxSTD) && !string.IsNullOrEmpty(model.SellerFax))
                            TranslationGrantMember.SellerFax = model.SellerFaxISD + "$" + model.SellerFaxSTD + "$" + model.SellerFax;
                        else
                            TranslationGrantMember.SellerFax = string.Empty;

                        TranslationGrantMember.SellerEmail = model.SellerEmail;
                        TranslationGrantMember.BuyerFirstName = model.BuyerFirstName;
                        TranslationGrantMember.BuyerMiddleName = model.BuyerMiddleName;
                        TranslationGrantMember.BuyerLastName = model.BuyerLastName;
                        TranslationGrantMember.BuyerName = model.BuyerFirstName + " " + model.BuyerMiddleName + " " + model.BuyerLastName;
                        TranslationGrantMember.BuyerJobTitle = model.BuyerJobTitle;
                        TranslationGrantMember.BuyerCompanyName = model.BuyerCompanyName;
                        TranslationGrantMember.BuyerAddress = model.BuyerAddress;

                        if (!string.IsNullOrEmpty(model.BuyerPhoneISD) && !string.IsNullOrEmpty(model.BuyerPhoneSTD) && !string.IsNullOrEmpty(model.BuyerPhone))
                            TranslationGrantMember.BuyerTelephone = model.BuyerPhoneISD + "$" + model.BuyerPhoneSTD + "$" + model.BuyerPhone;
                        else
                            TranslationGrantMember.BuyerTelephone = string.Empty;

                        if (!string.IsNullOrEmpty(model.BuyerFaxISD) && !string.IsNullOrEmpty(model.BuyerFaxSTD) && !string.IsNullOrEmpty(model.BuyerFax))
                            TranslationGrantMember.BuyerFax = model.BuyerFaxISD + "$" + model.BuyerFaxSTD + "$" + model.BuyerFax;
                        else
                            TranslationGrantMember.BuyerFax = string.Empty;

                        TranslationGrantMember.BuyerEmail = model.BuyerEmail;

                        TranslationGrantMember.BookTitleOriginal = model.BookTitleOriginal;
                        TranslationGrantMember.BookTitleEnglish = model.BookTitleEnglish;
                        TranslationGrantMember.BookTitleArabic = model.TitleInProposedLanguage; //BookTitleArabic to be changed to  TitleInProposedLanguage in db
                        TranslationGrantMember.Author = model.Author;
                        TranslationGrantMember.Isbn = model.Isbn;

                        TranslationGrantMember.NoOfPagesOriginal = model.NoOfPagesOriginal;
                        TranslationGrantMember.NoOfWords = model.NoOfWords;
                        if (model.OriginalLanguageId > 0)
                            TranslationGrantMember.OriginalLanguageId = model.OriginalLanguageId;
                        else
                            TranslationGrantMember.OriginalLanguageId = -1;

                        if (model.ProposedLanguageId > 0)
                            TranslationGrantMember.ProposedLanguageId = model.ProposedLanguageId;
                        else
                            TranslationGrantMember.ProposedLanguageId = -1;

                        if (model.GenreId > 0)
                            TranslationGrantMember.GenreId = model.GenreId;

                        if (model.GenreId == 9)
                            TranslationGrantMember.Genre = model.OtherGenre;
                        else
                            TranslationGrantMember.Genre = string.Empty;

                        TranslationGrantMember.CreatedOn = DateTime.Now;
                        TranslationGrantMember.ModifiedOn = DateTime.Now;

                        if (model.TranslationGrantMemberId <= 0)
                            TranslationGrantMember.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.Temporary);

                        if (model.TranslationGrantMemberId <= 0)
                        {
                            #region If Translation Grant MemberId is -1
                            if (TranslationGrantMemberService.InsertTranslationGrantMember(TranslationGrantMember) == EnumResultType.Success)
                            {
                                model.TranslationGrantMemberId = TranslationGrantMemberService.XsiItemdId;
                                //model.ItemId = TranslationGrantMemberItemId;
                                MethodFactory.LogTranslationGrantMemberModifiedStatus(EnumConversion.ToString(EnumTranslationGrantMemberStatus.Temporary), model.TranslationGrantMemberId);
                                LoadStep2Form(MemberId, LangId, model.TranslationGrantMemberId);
                                model.ShowSaveAndPrintInfo = true;
                                string FileName = string.Format("{0}_{1}", LangId, MethodFactory.GetRandomNumber());
                                if (model.ActionType == "saveonly")
                                {
                                    FileName = GetPDFForApplication(ReplaceApplicationFormPlaceholders(model.TranslationGrantMemberId, LangId), FileName);
                                    if (!string.IsNullOrEmpty(FileName))
                                    {
                                        model.PDFUrl = _appCustomSettings.UploadsCMSPath + "/translationgrantmember/" + FileName + ".pdf";
                                    }
                                }
                                else
                                {
                                    //FileName = GetPDFForApplication(GetPrintFormForStepTwo(model.TranslationGrantMemberId, LangId), FileName);
                                    //if (!string.IsNullOrEmpty(FileName))
                                    //{
                                    //    model.PDFUrl = _appCustomSettings.UploadsCMSPath + "/translationgrantmember/" + FileName + ".pdf";
                                    //}
                                    model.PDFUrl = GetPrintFormForStepTwo(model.TranslationGrantMemberId, LangId);
                                }
                            }
                            else
                            {
                                return Ok(new TGMessageDTO() { Message = "Unable to submit data at this time, please retry.", MessageTypeResponse = "Error" });
                            }
                            #endregion
                        }
                        else
                        {
                            #region Modify and Print
                            XsiExhibitionTranslationGrantMembers Entity = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(model.TranslationGrantMemberId);
                            if (Entity != default(XsiExhibitionTranslationGrantMembers))
                            {
                                if (TranslationGrantMemberService.UpdateTranslationGrantMember(TranslationGrantMember) == EnumResultType.Success)
                                {
                                    MethodFactory.LogTranslationGrantMemberModifiedStatus(TranslationGrantMember.Status, model.TranslationGrantMemberId);
                                    LoadStep2Form(MemberId, LangId, model.TranslationGrantMemberId);
                                    string FileName = string.Format("{0}_{1}", LangId, MethodFactory.GetRandomNumber());
                                    if (model.ActionType == "saveonly")
                                    {

                                        FileName = GetPDFForApplication(ReplaceApplicationFormPlaceholders(model.TranslationGrantMemberId, LangId), FileName);

                                        if (!string.IsNullOrEmpty(FileName))
                                        {
                                            model.PDFUrl = _appCustomSettings.UploadsCMSPath + "/translationgrantmember/" + FileName + ".pdf";
                                            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strUrl + "','_blank')", true);
                                        }
                                    }
                                    else
                                    {
                                        //FileName = GetPDFForApplication(GetPrintFormForStepTwo(model.TranslationGrantMemberId, LangId), FileName);

                                        //if (!string.IsNullOrEmpty(FileName))
                                        //{
                                        //    model.PDFUrl = _appCustomSettings.UploadsCMSPath + "/translationgrantmember/" + FileName + ".pdf";
                                        //    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strUrl + "','_blank')", true);
                                        //}
                                        model.PDFUrl = GetPrintFormForStepTwo(model.TranslationGrantMemberId, LangId);
                                    }
                                    // Sending Email
                                    SendEmailBasedOnEdit(TranslationGrantMember, MemberId, LangId, OldTranslationGrantMember);
                                }
                                else
                                {
                                    return Ok(new TGMessageDTO() { Message = "Unable to submit data at this time, please retry.", MessageTypeResponse = "Error" });
                                }
                            }
                            #endregion
                        }
                        if (LangId == 1)
                        {
                            if (model.ActionType == "saveonly")
                                return Ok(new TGMessageDTO() { ActionType = "saveonly", TranslationGrantMemberId = model.TranslationGrantMemberId, MessageStepOne = "Waiting for submission of signed application and books", PDFUrl = model.PDFUrl, MessageTypeResponse = "Success" });
                            else
                                return Ok(new TGMessageDTO() { ActionType = "print", TranslationGrantMemberId = model.TranslationGrantMemberId, MessageStepOne = "Waiting for submission of signed application and books", PDFUrl = model.PDFUrl, MessageTypeResponse = "Success" });
                        }
                        else
                        {
                            if (model.ActionType == "saveonly")
                                return Ok(new TGMessageDTO() { ActionType = "saveonly", TranslationGrantMemberId = model.TranslationGrantMemberId, MessageStepOne = "في انتظار تسليم الاستمارة الموقعة والكتب", PDFUrl = model.PDFUrl, MessageTypeResponse = "Success" });
                            else
                                return Ok(new TGMessageDTO() { ActionType = "print", TranslationGrantMemberId = model.TranslationGrantMemberId, MessageStepOne = "في انتظار تسليم الاستمارة الموقعة والكتب", PDFUrl = model.PDFUrl, MessageTypeResponse = "Success" });
                        }
                    }
                    #endregion
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok(new TGMessageDTO() { Message = "Something went wrong. Please try again later. ", MessageTypeResponse = "Error" });
            }
        }

        [HttpPost]
        [Route("ExhibitingDetailsStep2")]
        public ActionResult<IEnumerable<dynamic>> PostExhibitingDetailsStep2(ExhibitingDetailsStep2 model)
        {
            try
            {
                long MemberId = User.Identity.GetID();
                if (MemberId >= 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    #region Update Document
                    using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
                    {
                        XsiExhibitionTranslationGrantMembers TranslationGrantMember = new XsiExhibitionTranslationGrantMembers();
                        TranslationGrantMember = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(model.TranslationGrantMemberId);

                        if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Canceled) || TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.CancelledByUser))
                        {
                            return Ok(new TGMessageDTO() { Message = "Registration cannot be processed due to registration status: Cancelled.", MessageTypeResponse = "Error" });
                        }
                        else
                        {
                            var OldTranslationGrantMember = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(model.TranslationGrantMemberId);

                            string strOriginalBookProvidedas = string.Empty;
                            if (model.OriginalBookStatus == "S")
                                strOriginalBookProvidedas = EnumConversion.ToString(EnumTranslationGrantMemberOriginalBookStatus.SoftCopyofOriginalBook);
                            else if (model.OriginalBookStatus == "T")
                                strOriginalBookProvidedas = EnumConversion.ToString(EnumTranslationGrantMemberOriginalBookStatus.TranslatedFirstChapter);
                            else if (model.OriginalBookStatus == "H")
                                strOriginalBookProvidedas = EnumConversion.ToString(EnumTranslationGrantMemberOriginalBookStatus.SentAsHardCopy);
                            TranslationGrantMember.OriginalBookStatus = strOriginalBookProvidedas;

                            if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Temporary))
                                TranslationGrantMember.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.New);
                            else if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.PendingDocumentation))
                                TranslationGrantMember.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.DocumentationResubmitted);
                            // upload file
                            if (model.ApplicationFormFileBase64 != null && model.ApplicationFormFileBase64.Length > 0)
                            {
                                byte[] imageBytes;
                                if (model.ApplicationFormFileBase64.Contains("data:"))
                                {
                                    var strInfo = model.ApplicationFormFileBase64.Split(",")[0];
                                    imageBytes = Convert.FromBase64String(model.ApplicationFormFileBase64.Split(',')[1]);
                                }
                                else
                                {
                                    imageBytes = Convert.FromBase64String(model.ApplicationFormFileBase64);
                                }
                                string FileName = string.Format("{0}_{1}.{2}", LangId, MethodFactory.GetRandomNumber(), model.ApplicationFormFileExtension);
                                System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\TranslationGrantMember\\ApplicationForm\\") + FileName, imageBytes);
                                TranslationGrantMember.ApplicationForm = FileName;
                            }
                            if (model.OriginalSoftCopyFileBase64 != null && model.OriginalSoftCopyFileBase64.Length > 0)
                            {
                                byte[] imageBytes;
                                if (model.OriginalSoftCopyFileBase64.Contains("data:"))
                                {
                                    var strInfo = model.OriginalSoftCopyFileBase64.Split(",")[0];
                                    imageBytes = Convert.FromBase64String(model.OriginalSoftCopyFileBase64.Split(',')[1]);
                                }
                                else
                                {
                                    imageBytes = Convert.FromBase64String(model.OriginalSoftCopyFileBase64);
                                }
                                string FileName = string.Format("{0}_{1}.{2}", LangId, MethodFactory.GetRandomNumber(), model.OriginalSoftCopyFileExtension);
                                System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\TranslationGrantMember\\OriginalSoftCopy\\") + FileName, imageBytes);
                                TranslationGrantMember.OriginalSoftCopy = FileName;
                            }
                            //  TranslationGrantMember.NoOfWords = model.NoOfWords;
                            TranslationGrantMember.ModifiedOn = DateTime.Now;
                            if (TranslationGrantMemberService.UpdateTranslationGrantMember(TranslationGrantMember) == EnumResultType.Success)
                            {
                                if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.New))
                                {
                                    MethodFactory.LogTranslationGrantMemberModifiedStatus(TranslationGrantMember.Status, TranslationGrantMember.ItemId);
                                    //TGMemberStatus = TranslationGrantMember.Status;
                                    #region Send Email to Thank Buyer(Member)
                                    using (EmailContentService EmailContentService = new EmailContentService())
                                    {
                                        XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(31);
                                        if (emailContent != null)
                                        {
                                            StringBuilder body = new StringBuilder();
                                            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
                                            {
                                                XsiExhibitionMember memberentity = new XsiExhibitionMember();
                                                memberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(MemberId);
                                                body.Append(MethodFactory.BindEmailContentForTGMember(LangId, emailContent.Body, memberentity, _appCustomSettings.ServerAddressNew, 1, _appCustomSettings));
                                                var XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, memberentity.Email.Trim(), emailContent.Subject, body.ToString());
                                                if (XsiResult == EnumResultType.Success)
                                                {
                                                    #region Send Email to Seller Informing About New Registered User
                                                    emailContent = EmailContentService.GetEmailContentByItemId(10039);
                                                    if (emailContent != null)
                                                    {
                                                        body = new StringBuilder();
                                                        // body.Append(emailContent.Body);
                                                        body.Append(MethodFactory.BindEmailContentTG(LangId, emailContent.Body, _appCustomSettings.ServerAddressNew, _appCustomSettings));
                                                        body.Replace("$$Name$$", TranslationGrantMember.SellerFirstName + " " + TranslationGrantMember.SellerMiddleName + " " + TranslationGrantMember.SellerLastName);
                                                        body.Replace("$$buyername$$", memberentity.CompanyName);
                                                        _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, TranslationGrantMember.SellerEmail, emailContent.Subject, body.ToString());
                                                    }
                                                    #endregion
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.DocumentationResubmitted))
                                {
                                    MethodFactory.LogTranslationGrantMemberModifiedStatus(TranslationGrantMember.Status, TranslationGrantMember.ItemId);
                                    // TGMemberStatus = TranslationGrantMember.Status;
                                }
                                else if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.IncorrectDocuments))
                                {
                                    MethodFactory.LogTranslationGrantMemberModifiedStatus(TranslationGrantMember.Status, TranslationGrantMember.ItemId);
                                    //TGMemberStatus = TranslationGrantMember.Status;
                                }
                                // Sending Email
                                SendEmailBasedOnEdit(TranslationGrantMember, MemberId, LangId, OldTranslationGrantMember);
                                var message = @"<p>Thank you for participating in the Translation Grant!</p><p>Your submission is now under process.</p>";
                                if (LangId == 2)
                                    message = @"<p>لقد تم إدخال استمارة منحة الترجمة بنجاح</p><p>سنقوم بالرد عليك قريباً.</p>";
                                return Ok(new TGMessageDTO() { Message = message, TranslationGrantMemberId = TranslationGrantMember.ItemId, MessageTypeResponse = "Success" });
                                //  return Ok(message);
                            }
                            else
                            {
                                return Ok(new TGMessageDTO() { Message = "Unable to submit data at this time, please retry.", MessageTypeResponse = "Error" });
                            }
                        }

                    }
                    #endregion
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return Ok(new TGMessageDTO() { Message = "Something went wrong.", MessageTypeResponse = "Error" });
            }
        }

        [HttpPost]
        [Route("Step3")]
        public ActionResult<IEnumerable<dynamic>> PostStep3(BuyersOrSellersContractPost model)
        {
            try
            {
                long MemberId = User.Identity.GetID();
                if (MemberId >= 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);


                    using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
                    {
                        XsiExhibitionTranslationGrantMembers TranslationGrantMember = new XsiExhibitionTranslationGrantMembers();
                        TranslationGrantMember = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(model.TranslationGrantMemberId);

                        if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Canceled) || TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.CancelledByUser))
                        {
                            return Ok(new TGMessageDTO() { Message = "Registration cannot be processed due to registration status: Cancelled.", MessageTypeResponse = "Error" });
                        }

                        var OldTranslationGrantMember = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(model.TranslationGrantMemberId);

                        if (MemberId == TranslationGrantMember.MemberId)
                        {
                            TranslationGrantService TranslationGrantService = new TranslationGrantService();
                            XsiExhibitionTranslationGrant TranslationGrant = new XsiExhibitionTranslationGrant();
                            TranslationGrant.ItemId = TranslationGrantMember.TranslationGrantId.Value;
                            //TranslationGrant.IsActive = EnumConversion.ToString(EnumBool.Yes);
                            //TranslationGrant.IsArchive = EnumConversion.ToString(EnumBool.No);
                            var ActiveTranslationGrant = TranslationGrantService.GetTranslationGrant(TranslationGrant, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

                            long EmailFetchID = -1;
                            string ReturnMessage = "";
                            if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Approved))
                            {
                                EmailFetchID = 37;
                                ReturnMessage = SaveSignedContract(ref TranslationGrantMember, model, LangId);
                                ReturnMessage = SaveBankDetails(ref TranslationGrantMember, model, LangId);
                                ReturnMessage = @"<p>Signed Contract has been submitted.</p><p>Translation Grant Team will review your submission.</p>";
                            }
                            else if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.ContractUploaded))
                            {
                                EmailFetchID = 10023;
                                ReturnMessage = SaveSignedContract(ref TranslationGrantMember, model, LangId);
                                ReturnMessage = SaveBankDetails(ref TranslationGrantMember, model, LangId);
                                ReturnMessage = @"<p>Signed Contract has been submitted.</p><p>Translation Grant Team will review your submission.</p>";
                            }
                            if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.PendingDocumentation))
                            {
                                EmailFetchID = 10033;
                                ReturnMessage = SaveSignedContract(ref TranslationGrantMember, model, LangId);
                                ReturnMessage = SaveBankDetails(ref TranslationGrantMember, model, LangId);
                                ReturnMessage = "Documentation Re-submitted.";
                            }
                            if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.IncorrectDocuments))
                            {
                                EmailFetchID = 20082;
                                ReturnMessage = SaveSignedContract(ref TranslationGrantMember, model, LangId);
                                ReturnMessage = SaveBankDetails(ref TranslationGrantMember, model, LangId);
                                ReturnMessage = "Your Bank Details had been Re submitted successfully.";//"Bank Details Re-submitted.";
                            }
                            if (TranslationGrantMemberService.UpdateTranslationGrantMember(TranslationGrantMember) == EnumResultType.Success)
                            {
                                MethodFactory.LogTranslationGrantMemberModifiedStatus(TranslationGrantMember.Status, model.TranslationGrantMemberId);
                                using (EmailContentService EmailContentService = new EmailContentService())
                                {

                                    XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(EmailFetchID);
                                    if (emailContent != null)
                                    {
                                        StringBuilder body = new StringBuilder();
                                        using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
                                        {
                                            XsiExhibitionMember entity = new XsiExhibitionMember();
                                            entity = ExhibitionMemberService.GetExhibitionMemberByItemId(MemberId);
                                            body.Append(MethodFactory.BindEmailContentForTGMember(LangId, emailContent.Body, entity, _appCustomSettings.ServerAddressNew, 1, _appCustomSettings));
                                            var XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, entity.Email.Trim(), emailContent.Subject, body.ToString());
                                            //MVRegistration.SetActiveView(Step3MsgView);

                                        }
                                    }
                                }
                                // Sending Email
                                SendEmailBasedOnEdit(TranslationGrantMember, MemberId, LangId, OldTranslationGrantMember);
                            }
                            return Ok(new TGMessageDTO() { Message = ReturnMessage, TranslationGrantMemberId = TranslationGrantMember.ItemId, MessageTypeResponse = "Success" });
                        }
                        return Unauthorized();
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return Ok(new TGMessageDTO() { Message = "Exception: Something went wrong. Please retry again later.", MessageTypeResponse = "Error" });
            }
        }

        [HttpPost]
        [Route("Step4")]
        public ActionResult<dynamic> PostStep4(Step4DTO model)
        {
            LoadReceiptDTO ReturnModel = new LoadReceiptDTO();
            try
            {
                long MemberId = User.Identity.GetID();
                if (MemberId >= 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
                    {
                        XsiExhibitionTranslationGrantMembers TranslationGrantMember = new XsiExhibitionTranslationGrantMembers();
                        TranslationGrantMember = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(model.TranslationGrantMemberId);

                        if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Canceled) || TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.CancelledByUser))
                        {
                            return Ok(new TGMessageDTO() { Message = "Registration cannot be processed due to registration status: Cancelled.", MessageTypeResponse = "Error" });
                        }

                        var OldTranslationGrantMember = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(model.TranslationGrantMemberId);

                        if (MemberId == TranslationGrantMember.MemberId)
                        {
                            var TGMemberStatus = GetTGMemberStatus(TranslationGrantMember.ItemId);
                            if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.FinalPaymentUploaded) || TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.SecondPaymentUploaded))
                            {
                                ReturnModel = LoadReceipts(model.TranslationGrantMemberId);
                                return Ok(new TGMessageDTO() { Message = ReturnModel.Message, TranslationGrantMemberId = ReturnModel.TranslationGrantMemberId, MessageTypeResponse = "Success" });
                            }
                            else
                            {
                                long EmailID = 10025;
                                TranslationGrantMember.ModifiedOn = DateTime.Now;

                                if (model.IsHardCopy)
                                {
                                    TranslationGrantMember.IsHardCopy = EnumConversion.ToString(EnumBool.Yes);
                                    if (model.DateofShipment.HasValue)
                                        TranslationGrantMember.DateofShipment = Convert.ToDateTime(model.DateofShipment);

                                    if (TGMemberStatus == EnumConversion.ToString(EnumTranslationGrantMemberStatus.ResubmitDraft))
                                    {
                                        TranslationGrantMember.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.DraftResubmitted);
                                        ReturnModel.Message = "The Drafts of The Translated Book had been Re submitted successfully.";//"Translation Draft has been submitted.";
                                        EmailID = 10035;
                                    }
                                    else
                                    {
                                        TranslationGrantMember.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.DraftSubmitted);
                                        ReturnModel.Message = "The Drafts of The Translated Book had been Re submitted successfully.";//"Translation Draft has been submitted.";
                                    }
                                }
                                else if (model.FirstDraftFileBase64 != null && model.FirstDraftFileBase64.Length > 0)
                                {
                                    TranslationGrantMember.IsHardCopy = EnumConversion.ToString(EnumBool.No);
                                    byte[] imageBytes;
                                    if (model.FirstDraftFileBase64.Contains("data:"))
                                    {
                                        var strInfo = model.FirstDraftFileBase64.Split(",")[0];
                                        imageBytes = Convert.FromBase64String(model.FirstDraftFileBase64.Split(',')[1]);
                                    }
                                    else
                                    {
                                        imageBytes = Convert.FromBase64String(model.FirstDraftFileBase64);
                                    }
                                    string FileName = string.Format("{0}_{1}.{2}", LangId, MethodFactory.GetRandomNumber(), model.FirstDraftFileExtension);
                                    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\TranslationGrantMember\\FirstDraft\\") + FileName, imageBytes);
                                    TranslationGrantMember.Draft1 = FileName;


                                    if (TGMemberStatus == EnumConversion.ToString(EnumTranslationGrantMemberStatus.ResubmitDraft))
                                    {
                                        TranslationGrantMember.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.DraftResubmitted);
                                        ReturnModel.Message = "The Drafts of The Translated Book had been Re submitted successfully.";//"Translation Draft has been submitted.";
                                        EmailID = 10035;
                                    }
                                    else
                                    {
                                        TranslationGrantMember.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.DraftSubmitted);
                                        ReturnModel.Message = "The Drafts of The Translated Book had been Re submitted successfully.";//"Translation Draft has been submitted.";
                                    }
                                }
                                #region Send Email
                                if (TranslationGrantMemberService.UpdateTranslationGrantMember(TranslationGrantMember) == EnumResultType.Success)
                                {
                                    MethodFactory.LogTranslationGrantMemberModifiedStatus(TranslationGrantMember.Status, TranslationGrantMember.ItemId);
                                    using (EmailContentService EmailContentService = new EmailContentService())
                                    {
                                        XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(EmailID);
                                        if (emailContent != null)
                                        {
                                            StringBuilder body = new StringBuilder();
                                            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
                                            {
                                                XsiExhibitionMember entity = new XsiExhibitionMember();
                                                entity = ExhibitionMemberService.GetExhibitionMemberByItemId(MemberId);
                                                body.Append(MethodFactory.BindEmailContentForTGMember(LangId, emailContent.Body, entity, _appCustomSettings.ServerAddressNew, 1, _appCustomSettings));
                                                var XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, entity.Email.Trim(), emailContent.Subject, body.ToString());
                                                //if (XsiResult == EnumResultType.Success)
                                                //{
                                                //    divTranslationSubmitted.Visible = true;
                                                //    divPhase3Expired.Visible = false;
                                                //    MVRegistration.SetActiveView(Step4MsgView);
                                                //}
                                            }
                                        }
                                    }
                                    // Sending Email
                                    SendEmailBasedOnEdit(TranslationGrantMember, MemberId, LangId, OldTranslationGrantMember);
                                }
                                #endregion
                                return Ok(new TGMessageDTO() { Message = ReturnModel.Message, TranslationGrantMemberId = ReturnModel.TranslationGrantMemberId, MessageTypeResponse = "Success" });
                            }
                        }
                        return Unauthorized();
                    }
                }

                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok(new TGMessageDTO() { Message = "Something went wrong. Please try again later. ", TranslationGrantMemberId = ReturnModel.TranslationGrantMemberId, MessageTypeResponse = "Error" });
            }
        }

        #endregion

        #region PDF
        string ReplaceApplicationFormPlaceholders(long itemId, long langId)
        {
            using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
            {

                //AppCustomSettings _appCustomSettings = new AppCustomSettings();
                Entities.Models.XsiExhibitionTranslationGrantMembers entity = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(itemId);
                StringBuilder applicationform = new StringBuilder();
                if (entity != default(Entities.Models.XsiExhibitionTranslationGrantMembers))
                {
                    // XsiExhibition exhibitionentity = new XsiExhibition();
                    TranslationGrantService TranslationGrantService = new TranslationGrantService();
                    ExhibitionService ExhibitionService = new ExhibitionService();
                    XsiExhibitionTranslationGrant tgEntity = TranslationGrantService.GetTranslationGrantByItemId(entity.TranslationGrantId.Value);
                    //if (tgEntity.ExhibitionId != null)
                    //    exhibitionentity = ExhibitionService.GetExhibitionByItemId(tgEntity.ExhibitionId.Value);

                    var pathEn = _appCustomSettings.ApplicationPhysicalPath + "/Content/EmailMedia/TranslationGrantApplicationForm.html";
                    var pathAr = _appCustomSettings.ApplicationPhysicalPath + "/Content/EmailMedia/TranslationGrantApplicationFormAr.html";
                    //applicationform
                    if (langId == 1)
                        applicationform.Append(System.IO.File.ReadAllText(pathEn));
                    else
                        applicationform.Append(System.IO.File.ReadAllText(pathAr));
                    #region Replace Placeholders
                    string strtranslationtpe = entity.TranslationType;
                    if (strtranslationtpe == "A")
                        strtranslationtpe = (langId == 1) ? "Arabic To Any Language" : "من اللغة العربية إلى أي لغة أخرى";
                    else if (strtranslationtpe == "B")
                        strtranslationtpe = (langId == 1) ? "Any Language To Arabic" : "من أي لغة إلى اللغة العربية";
                    else if (strtranslationtpe == "C")
                        strtranslationtpe = (langId == 1) ? "Any Language To Any Language" : "من أي لغة إلى أي لغة";
                    applicationform.Replace("$$translationgranttype$$", strtranslationtpe);
                    applicationform.Replace("$$name$$", entity.SellerFirstName + " " + entity.SellerMiddleName + " " + entity.SellerLastName);
                    applicationform.Replace("$$jobtitle$$", entity.SellerJobTitle);
                    applicationform.Replace("$$companyname$$", entity.SellerCompanyName);
                    applicationform.Replace("$$address$$", entity.SellerAddress);
                    applicationform.Replace("$$telephoneno$$", entity.SellerTelephone.Replace("$", ""));
                    if (!string.IsNullOrEmpty(entity.SellerFax))
                        applicationform.Replace("$$faxno$$", entity.SellerFax.Replace("$", ""));
                    else
                        applicationform.Replace("$$faxno$$", "");
                    applicationform.Replace("$$emailaddress$$", entity.SellerEmail);

                    applicationform.Replace("$$buyername$$", entity.BuyerFirstName + " " + entity.BuyerMiddleName + " " + entity.BuyerLastName);
                    applicationform.Replace("$$buyerjobtitle$$", entity.BuyerJobTitle);
                    applicationform.Replace("$$buyercompanyname$$", entity.BuyerCompanyName);
                    applicationform.Replace("$$buyeraddress$$", entity.BuyerAddress);
                    applicationform.Replace("$$buyertelephoneno$$", entity.BuyerTelephone.Replace("$", ""));

                    if (!string.IsNullOrEmpty(entity.BuyerFax))
                        applicationform.Replace("$$buyerfaxno$$", entity.BuyerFax.Replace("$", ""));
                    else
                        applicationform.Replace("$$buyerfaxno$$", "");
                    applicationform.Replace("$$buyeremailaddress$$", entity.BuyerEmail);
                    applicationform.Replace("$$detailstitleinoriginallanguage$$", entity.BookTitleOriginal);
                    applicationform.Replace("$$detailstitleinenglish$$", entity.BookTitleEnglish);
                    applicationform.Replace("$$detailstitleinarabic$$", entity.BookTitleArabic);
                    applicationform.Replace("$$detailsauthor$$", entity.Author);
                    applicationform.Replace("$$detailsisbn$$", entity.Isbn);

                    if (entity.GenreId == 10)
                        applicationform.Replace("$$detailsgengre$$", entity.Genre);
                    else
                        applicationform.Replace("$$detailsgengre$$", MethodFactory.GetTranslationGranteGenrebyId(entity.GenreId.Value, langId));

                    applicationform.Replace("$$detailsnumberofpages$$", entity.NoOfPagesOriginal);
                    applicationform.Replace("$$detailsnumberofwords$$", entity.NoOfWords);
                    applicationform.Replace("$$detailsoriginallanguage$$", MethodFactory.GetExhibitionLanuageTitle(entity.OriginalLanguageId.Value, langId));
                    applicationform.Replace("$$detailsproposedlanguageoftranslation$$", MethodFactory.GetExhibitionLanuageTitle(entity.ProposedLanguageId.Value, langId));
                    applicationform.Replace("$$ServerAddressCMS$$", _appCustomSettings.ServerAddressCMS);
                    #endregion

                    string str = GetExhibitionDetails(1);
                    applicationform.Replace("$$exhibitionno$$", string.Empty);
                    applicationform.Replace("$$exhibitiondate$$", string.Empty);
                    applicationform.Replace("$$exhyear$$", str);

                    //string[] strArray = str.Split(',');
                    //applicationform.Replace("$$exhibitionno$$", strArray[0].ToString());
                    //applicationform.Replace("$$exhibitiondate$$", strArray[1].ToString());
                    //applicationform.Replace("$$exhyear$$", strArray[2]);

                    string swidth = "width=\"864\"";
                    string sreplacewidth = "width=\"100%\"";
                    applicationform.Replace(swidth, sreplacewidth);
                    applicationform.Replace("$$ServerAddressCMS$$", _appCustomSettings.ServerAddressCMS);
                    applicationform.Replace("$$ServerAddress$$", _appCustomSettings.ServerAddressNew);
                    //if (exhibitionentity.WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    //    applicationform.Replace("$$ServerAddressSCRF$$", _appCustomSettings.ServerAddressSCRF);
                }

                return applicationform.ToString();
            }
        }

        string GetPDFForApplication(string html, string strFileName)
        {
            string HtmltopdfPath = _appCustomSettings.HtmltopdfPath; // WebConfigurationManager.AppSettings["HtmltopdfPath"];
            string pdfPath = Path.Combine(_appCustomSettings.ApplicationPhysicalPath + "Content\\uploads\\translationgrantmember/");
            html = @"<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'><head><title>Application Form</title></head>
                <meta http-equiv='content-Type' content='text/html; charset=utf-8' /><body>" + html + "</body></html>";
            Process PDFProcess = new Process();
            string pdfPath1 = Path.Combine(_appCustomSettings.ApplicationPhysicalPath +
                                           "\\Content\\uploads\\translationgrantmember\\" + strFileName +
                                           ".html");
            System.IO.File.WriteAllText(pdfPath1, html);
            Thread.Sleep(4000);
            if (System.IO.File.Exists(pdfPath1))
            {
                PDFProcess = Process.Start(new ProcessStartInfo
                {
                    FileName = @HtmltopdfPath,
                    Arguments = String.Format("{0} {1}", pdfPath1, pdfPath + strFileName + ".pdf"),
                    UseShellExecute = false
                });

                while (true)
                {
                    if (PDFProcess.HasExited)
                    {
                        pdfPath = Path.Combine(_appCustomSettings.ApplicationPhysicalPath +
                                               "\\Content\\uploads\\translationgrantmember\\" + strFileName +
                                               ".pdf");
                        if (System.IO.File.Exists(pdfPath))
                        {
                            if (System.IO.File.Exists(pdfPath1))
                                System.IO.File.Delete(pdfPath1);
                        }
                        break;
                    }
                }
            }
            return strFileName;
        }
        #endregion

        #region Print
        string GetPrintFormForStepTwo(long translationgrantmemberid, long langid)
        {
            try
            {
                using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
                {
                    XsiExhibitionTranslationGrantMembers entity = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(translationgrantmemberid);
                    if (entity != default(XsiExhibitionTranslationGrantMembers))
                    {
                        //XsiExhibition exhibitionentity = new XsiExhibition();
                        TranslationGrantService TranslationGrantService = new TranslationGrantService();
                        ExhibitionService ExhibitionService = new ExhibitionService();
                        XsiExhibitionTranslationGrant tgEntity = TranslationGrantService.GetTranslationGrantByItemId(entity.TranslationGrantId.Value);

                        //if (tgEntity.ExhibitionId != null)
                        //    exhibitionentity = ExhibitionService.GetExhibitionByItemId(tgEntity.ExhibitionId.Value);

                        StringBuilder applicationform = new StringBuilder();
                        //applicationform
                        if (langid == 1)
                            applicationform.Append(System.IO.File.ReadAllText(_appCustomSettings.ApplicationPhysicalPath + "Content/EmailMedia/TranslationGrantApplicationForm.html"));
                        else
                            applicationform.Append(System.IO.File.ReadAllText(_appCustomSettings.ApplicationPhysicalPath + "Content/EmailMedia/TranslationGrantApplicationFormAr.html"));
                        #region Replace Placeholders
                        string strtranslationtpe = entity.TranslationType;
                        if (strtranslationtpe == "A")
                            strtranslationtpe = (langid == 1) ? "Arabic To Any Language" : "من اللغة العربية إلى أي لغة أخرى";
                        else if (strtranslationtpe == "B")
                            strtranslationtpe = (langid == 1) ? "Any Language To Arabic" : "من أي لغة إلى اللغة العربية";
                        else if (strtranslationtpe == "C")
                            strtranslationtpe = (langid == 1) ? "Any Language To Any Language" : "من أي لغة إلى أي لغة";
                        applicationform.Replace("$$translationgranttype$$", strtranslationtpe);
                        applicationform.Replace("$$name$$", entity.SellerFirstName + " " + entity.SellerMiddleName + " " + entity.SellerLastName);
                        applicationform.Replace("$$jobtitle$$", entity.SellerJobTitle);
                        applicationform.Replace("$$companyname$$", entity.SellerCompanyName);
                        applicationform.Replace("$$address$$", entity.SellerAddress);
                        applicationform.Replace("$$telephoneno$$", entity.SellerTelephone.Replace("$", ""));
                        if (!string.IsNullOrEmpty(entity.SellerFax))
                            applicationform.Replace("$$faxno$$", entity.SellerFax.Replace("$", ""));
                        else
                            applicationform.Replace("$$faxno$$", "");
                        applicationform.Replace("$$emailaddress$$", entity.SellerEmail);

                        applicationform.Replace("$$buyername$$", entity.BuyerFirstName + " " + entity.BuyerMiddleName + " " + entity.BuyerLastName);
                        applicationform.Replace("$$buyerjobtitle$$", entity.BuyerJobTitle);
                        applicationform.Replace("$$buyercompanyname$$", entity.BuyerCompanyName);
                        applicationform.Replace("$$buyeraddress$$", entity.BuyerAddress);
                        applicationform.Replace("$$buyertelephoneno$$", entity.BuyerTelephone.Replace("$", ""));

                        if (!string.IsNullOrEmpty(entity.BuyerFax))
                            applicationform.Replace("$$buyerfaxno$$", entity.BuyerFax.Replace("$", ""));
                        else
                            applicationform.Replace("$$buyerfaxno$$", "");
                        applicationform.Replace("$$buyeremailaddress$$", entity.BuyerEmail);
                        applicationform.Replace("$$detailstitleinoriginallanguage$$", entity.BookTitleOriginal);
                        applicationform.Replace("$$detailstitleinenglish$$", entity.BookTitleEnglish);
                        applicationform.Replace("$$detailstitleinarabic$$", entity.BookTitleArabic);
                        applicationform.Replace("$$detailsauthor$$", entity.Author);
                        applicationform.Replace("$$detailsisbn$$", entity.Isbn);

                        if (entity.GenreId == 10)
                            applicationform.Replace("$$detailsgengre$$", entity.Genre);
                        else
                            applicationform.Replace("$$detailsgengre$$", MethodFactory.GetTranslationGranteGenrebyId(entity.GenreId.Value, langid));

                        applicationform.Replace("$$detailsnumberofpages$$", entity.NoOfPagesOriginal);
                        applicationform.Replace("$$detailsnumberofwords$$", entity.NoOfWords);
                        applicationform.Replace("$$detailsoriginallanguage$$", MethodFactory.GetExhibitionLanuageTitle(entity.OriginalLanguageId.Value, langid));
                        applicationform.Replace("$$detailsproposedlanguageoftranslation$$", MethodFactory.GetExhibitionLanuageTitle(entity.ProposedLanguageId.Value, langid));
                        applicationform.Replace("$$ServerAddressCMS$$", _appCustomSettings.ServerAddressCMS);
                        #endregion

                        string str = GetExhibitionDetails(1);
                        applicationform.Replace("$$exhibitionno$$", string.Empty);
                        applicationform.Replace("$$exhibitiondate$$", string.Empty);
                        applicationform.Replace("$$exhyear$$", str);

                        //string[] strArray = str.Split(',');
                        //applicationform.Replace("$$exhibitionno$$", strArray[0].ToString());
                        //applicationform.Replace("$$exhibitiondate$$", strArray[1].ToString());
                        //applicationform.Replace("$$exhyear$$", strArray[2]);

                        string swidth = "width=\"864\"";
                        string sreplacewidth = "width=\"100%\"";
                        applicationform.Replace(swidth, sreplacewidth);
                        applicationform.Replace("$$ServerAddressCMS$$", _appCustomSettings.ServerAddressCMS);
                        applicationform.Replace("$$ServerAddress$$", _appCustomSettings.ServerAddressNew);

                        //if (exhibitionentity.WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                        //    applicationform.Replace("$$ServerAddressSCRF$$", _appCustomSettings.ServerAddressSCRF);

                        return applicationform.ToString();
                    }

                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                return "Exception: Something went wrong. Please retry again later.";
            }
        }

        #endregion

        #region Helper Methods 
        private XsiExhibitionTranslationGrant GetTranslationGrantById(long tgregistrationid)
        {
            using (TranslationGrantService TranslationGrantService = new TranslationGrantService())
            {
                return TranslationGrantService.GetTranslationGrantByItemId(tgregistrationid);
            }
        }
        private List<XsiExhibitionTranslationGrant> GetLatestTranslationGrants()
        {

            using (TranslationGrantService TranslationGrantService = new TranslationGrantService())
            {
                XsiExhibitionTranslationGrant where = new XsiExhibitionTranslationGrant();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                where.IsArchive = EnumConversion.ToString(EnumBool.No);
                var list = TranslationGrantService.GetTranslationGrant(where, EnumSortlistBy.ByItemIdDesc).Take(3).ToList();
                return list;
            }
        }
        StepTwoSectionOneDTO GetStepTwoSectionOne(long memberid)
        {
            try
            {
                StepTwoSectionOneDTO dto = new StepTwoSectionOneDTO();
                long MemberId = User.Identity.GetID();
                if (MemberId == memberid)
                {
                    using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
                    {
                        var CurrentMember = ExhibitionMemberService.GetExhibitionMemberByItemId(memberid);
                        if (CurrentMember != null)
                        {
                            if (!string.IsNullOrEmpty(CurrentMember.Phone))
                            {
                                dto.BuyerPhoneISD = CurrentMember.Phone.Split('$')[0];
                                dto.BuyerPhoneSTD = CurrentMember.Phone.Split('$')[1];
                                dto.BuyerPhone = CurrentMember.Phone.Split('$')[2];
                            }
                            if (!string.IsNullOrEmpty(CurrentMember.Fax))
                            {
                                dto.BuyerFaxISD = CurrentMember.Fax.Split('$')[0];
                                dto.BuyerFaxSTD = CurrentMember.Fax.Split('$')[1];
                                dto.BuyerFax = CurrentMember.Fax.Split('$')[2];
                            }

                            dto.BuyerFirstName = CurrentMember.Firstname;
                            dto.BuyerLastName = CurrentMember.LastName;
                            dto.BuyerAddress = CurrentMember.Address;
                            dto.BuyerEmail = CurrentMember.Email;
                        }
                    }
                }
                return dto;
            }
            catch (Exception ex)
            {
                return null;
                // return Ok(ex.ToString());
            }
        }
        TranslationGrantLoadDTO GetStepsData(long tgregistrationid, long langId, string isfromlist = "N")
        {
            try
            {
                TranslationGrantLoadDTO translationGrantLoadDTO = new TranslationGrantLoadDTO();
                StepTwoSectionOneDTO sectionOneDTO = new StepTwoSectionOneDTO();
                //StepTwoSectionTwoDTO sectionTwoDTO = new StepTwoSectionTwoDTO();
                //StepTwoSectionThreeDTO sectionThreeDTO = new StepTwoSectionThreeDTO();
                StepTwoSectionTwoDTO steptwosectiontwoDTO = new StepTwoSectionTwoDTO();
                StepThreeTGSDTO stepThree = new StepThreeTGSDTO();
                StepFourTGSDTO stepFour = new StepFourTGSDTO();
                StepFiveTGSDTO stepFive = new StepFiveTGSDTO();
                StepSixTGSDTO stepSix = new StepSixTGSDTO();

                if (tgregistrationid > 0)
                {
                    using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
                    {
                        TranslationGrantService TranslationGrantService = new TranslationGrantService();
                        var TGMember = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(tgregistrationid);

                        #region Get Active TG And Dates
                        XsiExhibitionTranslationGrant WhereTranslationGrant1 = new XsiExhibitionTranslationGrant();
                        WhereTranslationGrant1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        WhereTranslationGrant1.IsArchive = EnumConversion.ToString(EnumBool.No);

                        if (TGMember != null)
                            WhereTranslationGrant1.ItemId = TGMember.TranslationGrantId.Value;

                        var ActiveTranslationGrant = TranslationGrantService.GetTranslationGrant(WhereTranslationGrant1, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                        var Phase1EndDate = ActiveTranslationGrant.Phase1EndDate.Value;
                        var Phase2EndDate = ActiveTranslationGrant.Phase2EndDate.Value;
                        var Phase3EndDate = ActiveTranslationGrant.Phase3EndDate.Value;
                        #endregion

                        if (TGMember != null)
                        {
                            translationGrantLoadDTO.MemberId = TGMember.MemberId.Value;
                            translationGrantLoadDTO.TranslationGrantMemberId = TGMember.ItemId;
                            translationGrantLoadDTO.TranslationGrantId = TGMember.TranslationGrantId.Value;
                            translationGrantLoadDTO.TGStatus = TGMember.Status;
                            var TranslationGrant = TranslationGrantService.GetTranslationGrantByItemId(TGMember.TranslationGrantId.Value);
                            if (TranslationGrant != null)
                            {
                                translationGrantLoadDTO.StepId = GetStepIdByStatus(TGMember.Status);

                                #region Check Date validations
                                if (TGMember.Status != null)
                                {
                                    if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Temporary))
                                    {
                                        #region Phase1 Expired
                                        DateTime expiration_date = Phase1EndDate.Date;
                                        DateTime currentDateTime = MethodFactory.ArabianTimeNow().Date;
                                        int daydiff = (int)((currentDateTime - expiration_date).TotalDays);

                                        if (daydiff > 0)
                                        {
                                            if (langId == 1)
                                                translationGrantLoadDTO.Message = @"<p>Registration for translation grant Phase 1 is completed</p>";
                                            else
                                                translationGrantLoadDTO.Message = @"<p>تم انتهاء التسجيل من المرحلة الأولى لمنحة الترجمة</p>";
                                        }
                                        else
                                        {
                                            if (langId == 1)
                                                translationGrantLoadDTO.MessageStepOne = @"Waiting for submission of signed application and books";
                                            else
                                                translationGrantLoadDTO.MessageStepOne = @"في انتظار تسليم الاستمارة الموقعة والكتب";
                                        }
                                        #endregion
                                    }
                                    else if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.New) || TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.DocumentationResubmitted) || TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.TemporaryViewed) || TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.UnderEvaluation))
                                    {
                                        #region Phase1 Expired
                                        DateTime expiration_date = Phase1EndDate.Date;
                                        DateTime currentDateTime = MethodFactory.ArabianTimeNow().Date;
                                        int daydiff = (int)((currentDateTime - expiration_date).TotalDays);

                                        if (daydiff > 0)
                                        {
                                            if (langId == 1)
                                                translationGrantLoadDTO.Message = @"<p>Registration for translation grant Phase 1 is completed</p>";
                                            else
                                                translationGrantLoadDTO.Message = @"<p>تم انتهاء التسجيل من المرحلة الأولى لمنحة الترجمة</p>";
                                        }
                                        else
                                        {
                                            if (isfromlist == "Y")
                                            {
                                                translationGrantLoadDTO.Message = null;
                                            }
                                            else
                                            {
                                                if (langId == 1)
                                                    translationGrantLoadDTO.Message = @"<p>Thank you for participating in the Translation Grant!</p><p>Your submission is now under process.</p>";
                                                else
                                                    translationGrantLoadDTO.Message = @"<p>لقد تم إدخال استمارة منحة الترجمة بنجاح</p><p>سنقوم بالرد عليك قريباً.</p>";
                                            }
                                        }
                                        #endregion
                                    }
                                    else if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.PendingDocumentation))
                                    {
                                        #region Phase1 Expired
                                        DateTime expiration_date = Phase1EndDate.Date;
                                        DateTime currentDateTime = MethodFactory.ArabianTimeNow().Date;
                                        int daydiff = (int)((currentDateTime - expiration_date).TotalDays);

                                        if (daydiff > 0)
                                        {
                                            if (langId == 1)
                                                translationGrantLoadDTO.Message = @"<p>Registration for translation grant Phase 1 is completed</p>";
                                            else
                                                translationGrantLoadDTO.Message = @"<p>تم انتهاء التسجيل من المرحلة الأولى لمنحة الترجمة</p>";
                                        }
                                        #endregion
                                    }
                                    else if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.DocumentationResubmitted))
                                    {
                                        if (langId == 1)
                                            translationGrantLoadDTO.Message = translationGrantLoadDTO.Message + @"<p>Documentation Re-submitted.</p><p>Translation Grant Team will review your submission.</p>";
                                        else
                                            translationGrantLoadDTO.Message = translationGrantLoadDTO.Message + @"<p>Documentation Re-submitted.</p><p>سنقوم بالرد عليك قريباً.</p>";
                                    }
                                    else if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Approved)) //Approved A
                                    {
                                        if (Phase2EndDate < MethodFactory.ArabianTimeNow())
                                        {
                                            if (langId == 1)
                                                translationGrantLoadDTO.Message = @"<p>Registration for translation grant Phase 2 is completed</p>";
                                            else
                                                translationGrantLoadDTO.Message = @"<p>تم انتهاء التسجيل من المرحلة الأولى لمنحة الترجمة</p>";
                                        }
                                    }
                                    else if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.IncorrectDocuments))
                                    {
                                        if (Phase2EndDate < MethodFactory.ArabianTimeNow())
                                        {
                                            if (langId == 1)
                                                translationGrantLoadDTO.Message = @"<p>Registration for translation grant Phase 2 is completed</p>";
                                            else
                                                translationGrantLoadDTO.Message = @"<p>تم انتهاء التسجيل من المرحلة الأولى لمنحة الترجمة</p>";
                                        }
                                    }
                                    else if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.ContractUploaded))
                                    {
                                        if (Phase2EndDate < MethodFactory.ArabianTimeNow())
                                        {
                                            if (langId == 1)
                                                translationGrantLoadDTO.Message = @"<p>Registration for translation grant Phase 2 is completed</p>";
                                            else
                                                translationGrantLoadDTO.Message = @"<p>تم انتهاء التسجيل من المرحلة الأولى لمنحة الترجمة</p>";
                                        }
                                    }
                                    else if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.FirstPaymentUploaded))
                                    {
                                        if (Phase3EndDate < MethodFactory.ArabianTimeNow())
                                        {
                                            if (langId == 1)
                                                translationGrantLoadDTO.Message = @"<p>Phase 3 End Date Expired</p>";
                                            else
                                                translationGrantLoadDTO.Message = @"<p>انتهى تاريخ المرحلة الثالثة</p>";
                                        }
                                    }
                                    else if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Rejected))
                                    {
                                        if (langId == 1)
                                            translationGrantLoadDTO.Message = translationGrantLoadDTO.Message + @"<p>Unfortunately your book has been rejected. Thank you for your application, and we hope you apply again next year. </p>";
                                        else
                                            translationGrantLoadDTO.Message = translationGrantLoadDTO.Message + @"<p>Unfortunately your book has been rejected. Thank you for your application, and we hope you apply again next year. </p>";
                                    }
                                    else if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.CancelledByUser))
                                    {
                                        if (langId == 1)
                                            translationGrantLoadDTO.Message = translationGrantLoadDTO.Message + @"<p>Cancelled by Registrant. </p>";
                                        else
                                            translationGrantLoadDTO.Message = translationGrantLoadDTO.Message + @"<p>Cancelled by Registrant. </p>";
                                    }
                                    else if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.BankDetailsSubmitted))
                                    {
                                        if (langId == 1)
                                            translationGrantLoadDTO.Message = translationGrantLoadDTO.Message + @"<p>Your Details had been submitted successfully.</p><p>Translation Grant Team will review your submission.</p>";
                                        else
                                            translationGrantLoadDTO.Message = translationGrantLoadDTO.Message + @"<p>Your Details had been submitted successfully.</p><p>سنقوم بالرد عليك قريباً.</p>";
                                    }
                                }
                                #endregion

                                #region Step Two
                                #region Section One Seller
                                sectionOneDTO.TranslationType = TGMember.TranslationType;
                                sectionOneDTO.SellerFirstName = TGMember.SellerFirstName;
                                sectionOneDTO.SellerMiddleName = TGMember.SellerMiddleName;
                                sectionOneDTO.SellerLastName = TGMember.SellerLastName;
                                sectionOneDTO.SellerJobTitle = TGMember.SellerJobTitle;
                                sectionOneDTO.SellerCompanyName = TGMember.SellerCompanyName;
                                sectionOneDTO.SellerAddress = TGMember.SellerAddress;

                                if (!string.IsNullOrEmpty(TGMember.SellerTelephone))
                                {
                                    if (TGMember.SellerTelephone.IndexOf("$") > -1)
                                    {
                                        sectionOneDTO.SellerPhoneISD = TGMember.SellerTelephone.Split('$')[0];
                                        sectionOneDTO.SellerPhoneSTD = TGMember.SellerTelephone.Split('$')[1];
                                        sectionOneDTO.SellerPhone = TGMember.SellerTelephone.Split('$')[2];
                                    }
                                    else
                                    {
                                        sectionOneDTO.SellerPhone = TGMember.SellerTelephone;
                                    }
                                }
                                if (!string.IsNullOrEmpty(TGMember.SellerFax))
                                {
                                    if (TGMember.SellerFax.IndexOf("$") > -1)
                                    {
                                        sectionOneDTO.SellerFaxISD = TGMember.SellerFax.Split('$')[0];
                                        sectionOneDTO.SellerFaxSTD = TGMember.SellerFax.Split('$')[1];
                                        sectionOneDTO.SellerFax = TGMember.SellerFax.Split('$')[2];
                                    }
                                    else
                                    {
                                        sectionOneDTO.SellerFax = TGMember.SellerFax;
                                    }
                                }
                                sectionOneDTO.SellerEmail = TGMember.SellerEmail;
                                #endregion
                                #region Section Two Buyer
                                sectionOneDTO.BuyerFirstName = TGMember.BuyerFirstName;
                                sectionOneDTO.BuyerMiddleName = TGMember.BuyerMiddleName;
                                sectionOneDTO.BuyerLastName = TGMember.BuyerLastName;
                                sectionOneDTO.BuyerJobTitle = TGMember.BuyerJobTitle;
                                sectionOneDTO.BuyerCompanyName = TGMember.BuyerCompanyName;
                                sectionOneDTO.BuyerAddress = TGMember.BuyerAddress;

                                if (!string.IsNullOrEmpty(TGMember.BuyerTelephone))
                                {
                                    if (TGMember.BuyerTelephone.IndexOf("$") > -1)
                                    {
                                        sectionOneDTO.BuyerPhoneISD = TGMember.BuyerTelephone.Split('$')[0];
                                        sectionOneDTO.BuyerPhoneSTD = TGMember.BuyerTelephone.Split('$')[1];
                                        sectionOneDTO.BuyerPhone = TGMember.BuyerTelephone.Split('$')[2];
                                    }
                                    else
                                    {
                                        sectionOneDTO.BuyerPhone = TGMember.BuyerTelephone;
                                    }
                                }
                                if (!string.IsNullOrEmpty(TGMember.BuyerFax))
                                {
                                    if (TGMember.BuyerFax.IndexOf("$") > -1)
                                    {
                                        sectionOneDTO.BuyerFaxISD = TGMember.BuyerFax.Split('$')[0];
                                        sectionOneDTO.BuyerFaxSTD = TGMember.BuyerFax.Split('$')[1];
                                        sectionOneDTO.BuyerFax = TGMember.BuyerFax.Split('$')[2];
                                    }
                                    else
                                    {
                                        sectionOneDTO.BuyerFax = TGMember.BuyerFax;
                                    }
                                }
                                sectionOneDTO.BuyerEmail = TGMember.BuyerEmail;
                                #endregion

                                #region Section Three Book Details
                                sectionOneDTO.BookTitleOriginal = TGMember.BookTitleOriginal;
                                sectionOneDTO.BookTitleEnglish = TGMember.BookTitleEnglish;
                                sectionOneDTO.TitleInProposedLanguage = TGMember.BookTitleArabic;
                                sectionOneDTO.Author = TGMember.Author;
                                sectionOneDTO.Isbn = TGMember.Isbn;
                                if (TGMember.GenreId != null)
                                    sectionOneDTO.GenreId = TGMember.GenreId.Value;
                                sectionOneDTO.NoOfPagesOriginal = TGMember.NoOfPagesOriginal;
                                sectionOneDTO.NoOfWords = TGMember.NoOfWords;
                                if (TGMember.OriginalLanguageId != null)
                                    sectionOneDTO.OriginalLanguageId = TGMember.OriginalLanguageId.Value;
                                if (TGMember.ProposedLanguageId != null)
                                    sectionOneDTO.ProposedLanguageId = TGMember.ProposedLanguageId.Value;
                                #endregion

                                #region Section Four Uploads
                                if (!string.IsNullOrWhiteSpace(TGMember.ApplicationForm))
                                    steptwosectiontwoDTO.SignedApplicationFormFileName = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/ApplicationForm/" + TGMember.ApplicationForm;

                                if (!string.IsNullOrEmpty(TGMember.OriginalBookStatus))
                                    steptwosectiontwoDTO.OriginalBookStatus = TGMember.OriginalBookStatus;

                                if (!string.IsNullOrWhiteSpace(TGMember.OriginalSoftCopy))
                                    steptwosectiontwoDTO.SoftCopyofTheBookFileName = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/OriginalSoftCopy/" + TGMember.OriginalSoftCopy;
                                #endregion

                                translationGrantLoadDTO.StepTwoSectionOneDTO = sectionOneDTO;
                                //translationGrantLoadDTO.StepTwoSectionTwoDTO = sectionTwoDTO;
                                //translationGrantLoadDTO.StepTwoSectionThreeDTO = sectionThreeDTO;


                                translationGrantLoadDTO.StepTwoSectionTwoDTO = steptwosectiontwoDTO;
                                #endregion

                                #region Step Three
                                if (!string.IsNullOrWhiteSpace(TGMember.SignedContract))
                                    stepThree.SignedContractFormFileName = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/SignedContract/" + TGMember.SignedContract;

                                if (!string.IsNullOrWhiteSpace(TGMember.BuyerorSellerRights))
                                    stepThree.BuyersAndSellersRightsContractsFileName = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/BuyersOrSellersRights/" + TGMember.BuyerorSellerRights;

                                if (!string.IsNullOrWhiteSpace(TGMember.TransferForm))
                                    stepThree.FundsTransferFormFileName = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/TransferForm/" + TGMember.TransferForm;

                                if (!string.IsNullOrWhiteSpace(TGMember.TransferFormByAdmin))
                                    stepThree.FundTransferFormByAdmin = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/TransferFormByAdmin/" + TGMember.TransferFormByAdmin;

                                if (!string.IsNullOrWhiteSpace(TGMember.Contract))
                                    stepThree.ContractUploadedByAdmin = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/Contract/" + TGMember.Contract;

                                //stepThree.BankName = TGMember.BankName;
                                //stepThree.BranchNameAddress = TGMember.BranchNameAddress;
                                //stepThree.AccountNumber = TGMember.AccNo;
                                //if (!string.IsNullOrEmpty(TGMember.IsIbanorBoth))
                                //    stepThree.IsIBANorBoth = TGMember.IsIbanorBoth;
                                //stepThree.IBANNo = TGMember.Ibanno;
                                //stepThree.SwiftCode = TGMember.SwiftCode;
                                if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.BankDetailsSubmitted))
                                    translationGrantLoadDTO.Message = @"<p>Your Details had been submitted successfully.</p><p>Translation Grant Team will review your submission.</p>";
                                else if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.ContractUploaded))
                                    translationGrantLoadDTO.Message = @"<p>Signed Contract has been submitted.</p><p>Translation Grant Team will review your submission.</p>";
                                else if (TGMember.Status ==
                                         EnumConversion.ToString(EnumTranslationGrantMemberStatus.BankDetailsResubmitted))
                                    translationGrantLoadDTO.Message = @"Your Details had been Re submitted successfully.";
                                else if (TGMember.Status ==
                                         EnumConversion.ToString(EnumTranslationGrantMemberStatus.ContractSubmitted))
                                    translationGrantLoadDTO.Message = @"Signed Contract has been submitted.";
                                translationGrantLoadDTO.StepThreeTGSDTO = stepThree;
                                #endregion

                                #region Step Four
                                if (TGMember.AwardGrantValue != null)
                                    stepFour.AwardGrantValue = TGMember.AwardGrantValue;

                                if (TGMember.FirstPayment != null)
                                    stepFour.FirstPaymentAmount = TGMember.FirstPayment;

                                if (TGMember.FirstPaymentDate != null)
                                    stepFour.FirstPaymentDate = MethodFactory.ShortDateTG(TGMember.FirstPaymentDate.Value.ToString());

                                if (TranslationGrant.Phase2EndDate != null)
                                    stepFour.PhaseTwoEndDate = "";// MethodFactory.ShortDateTG(TranslationGrant.Phase2EndDate.Value.ToString());

                                if (TGMember.FirstPaymentReceipt != null && !string.IsNullOrEmpty(TGMember.FirstPaymentReceipt))
                                {
                                    stepFour.FirstPaymentReceipt = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/FirstReceipt/" + TGMember.FirstPaymentReceipt;
                                }
                                if (!string.IsNullOrEmpty(TGMember.Draft1))
                                {
                                    stepFour.SoftCopyOfTranslatedBookFileName = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/FirstDraft/" + TGMember.Draft1;
                                }

                                stepFour.IsHardCopy = TGMember.IsHardCopy == EnumConversion.ToString(EnumBool.Yes) ? true : false;

                                if (TGMember.DateofShipment != null)
                                    stepFour.DateOfShipment = MethodFactory.ShortDateTG(TGMember.DateofShipment.Value.ToString());

                                if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.DraftSubmitted))
                                {
                                    if (langId == 1)
                                        translationGrantLoadDTO.Message = @"The Drafts of The Translated Book had been submitted successfully.";
                                    else
                                        translationGrantLoadDTO.Message = @"تم اعادة تحميل النسخة المترجمة للكتاب.";
                                }
                                else if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.DraftResubmitted))
                                {
                                    if (langId == 1)
                                        translationGrantLoadDTO.Message = @"The Drafts of The Translated Book had been submitted successfully.";// Changed based on doc 
                                    //translationGrantLoadDTO.Message = @"The Drafts of The Translated Book had been Re submitted successfully.";
                                    else
                                        translationGrantLoadDTO.Message = @"تم اعادة تسليم النسخة المترجمة للكتاب.";
                                }
                                else if (TGMember.Status ==
                                         EnumConversion.ToString(EnumTranslationGrantMemberStatus.FirstPaymentUploaded))
                                {
                                    if (TGMember.FinalPaymentReceipt != null && !string.IsNullOrEmpty(TGMember.FinalPaymentReceipt))
                                    {
                                        if (langId == 1)
                                            translationGrantLoadDTO.Message = @"<p>The first payment has been processed.</p>";
                                        else
                                            translationGrantLoadDTO.Message = @"<p>لقد تمت عملية الدفعة الأولى بنجاح.</p>";
                                    }
                                }

                                translationGrantLoadDTO.StepFourTGSDTO = stepFour;
                                #endregion

                                #region Step Five 
                                if (TGMember.SecondPayment != null)
                                    stepFive.SecondPaymentAmount = TGMember.SecondPayment;

                                if (TGMember.SecondPaymentDate != null)
                                    stepFive.SecondPaymentDate = MethodFactory.ShortDateTG(TGMember.SecondPaymentDate.Value.ToString());

                                if (TranslationGrant.Phase3EndDate != null)
                                    stepFive.Phase3EndDate = MethodFactory.ShortDateTG(TranslationGrant.Phase3EndDate.Value.ToString());

                                if (TGMember.SecondPaymentReceipt != null && !string.IsNullOrEmpty(TGMember.SecondPaymentReceipt))
                                    stepFive.SecondPaymentReceipt = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/SecondReceipt/" + TGMember.SecondPaymentReceipt;

                                if (TGMember.Status ==
                                    EnumConversion.ToString(EnumTranslationGrantMemberStatus.SecondPaymentUploaded))
                                {
                                    if (TGMember.SecondPaymentReceipt != null && !string.IsNullOrEmpty(TGMember.SecondPaymentReceipt))
                                    {
                                        if (langId == 1)
                                            translationGrantLoadDTO.Message = @"<p>The second payment has been processed.</p>";
                                        else
                                            translationGrantLoadDTO.Message = @"<p>لقد تمت عملية الدفعة الثانية بنجاح.</p>";
                                    }
                                }


                                translationGrantLoadDTO.StepFiveTGSDTO = stepFive;
                                #endregion

                                #region Step Six
                                if (TGMember.FinalPayment != null)
                                    stepSix.ThirdPaymentAmount = TGMember.FinalPayment;

                                if (TGMember.FinalPaymentDate != null)
                                    stepSix.ThirdPaymentDate = MethodFactory.ShortDateTG(TGMember.FinalPaymentDate.Value.ToString());

                                if (TGMember.FinalPaymentReceipt != null && !string.IsNullOrEmpty(TGMember.FinalPaymentReceipt))
                                    stepSix.ThirdPaymentReceipt = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/ThirdReceipt/" + TGMember.FinalPaymentReceipt;

                                if (TGMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.FinalPaymentUploaded))
                                {
                                    if (TGMember.FinalPaymentReceipt != null && !string.IsNullOrEmpty(TGMember.FinalPaymentReceipt))
                                    {
                                        if (langId == 1)
                                            translationGrantLoadDTO.Message = @"<p>The final payment has been processed.</p>"; // @"<p>Your final payment is approved by the SIBF.</p>";
                                        else
                                            translationGrantLoadDTO.Message = @"<p>تمت عملية الدفعة الأخيرة بنجاح.</p>"; // @"<p>Your final payment is approved by the SIBF.</p>";
                                    }
                                }
                                translationGrantLoadDTO.StepSixTGSDTO = stepSix;
                                #endregion
                            }
                        }
                        else
                        {
                            #region Phase1 Expired
                            DateTime expiration_date = Phase1EndDate.Date;
                            DateTime currentDateTime = MethodFactory.ArabianTimeNow().Date;
                            int daydiff = (int)((currentDateTime - expiration_date).TotalDays);

                            if (daydiff > 0)
                            {
                                if (langId == 1)
                                    translationGrantLoadDTO.Message = @"<p>Registration for translation grant Phase 1 is completed</p>";
                                else
                                    translationGrantLoadDTO.Message = @"<p>تم انتهاء التسجيل من المرحلة الأولى لمنحة الترجمة</p>";
                            }
                            #endregion
                        }
                    }
                }
                return translationGrantLoadDTO;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private int GetStepIdByStatus(string status)
        {
            switch (status)
            {
                case "T": return 2;
                case "N": return 2;
                case "Y": return 2;
                case "V": return 2;
                case "M": return 2;
                case "O": return 2;
                case "A": return 3;
                case "I": return 3;
                case "S": return 3;
                case "R": return 3;
                case "B": return 3;
                case "F": return 4;
                case "D": return 4;
                case "Q": return 4;
                case "E": return 4;
                case "P": return 5;
                case "Z": return 6;
                case "X": return 2;
                case "G": return 2;
                case "W": return 2;
                case "U": return 3;
                // case "C": return 0;
                default: return 0;
            }

        }
        TGValidationDTO IsNewRegistrationDatesAvailable(TGValidationDTO dto)
        {
            using (TranslationGrantService TranslationGrantService = new TranslationGrantService())
            {
                dto.IsDateExpired = true;
                ExhibitionService ExhibitionService = new ExhibitionService();
                XsiExhibitionTranslationGrant where = new XsiExhibitionTranslationGrant();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                where.IsArchive = EnumConversion.ToString(EnumBool.No);

                XsiExhibitionTranslationGrant entity = TranslationGrantService.GetTranslationGrant(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                if (entity != null)
                {
                    dto.ExhibitionId = entity.ExhibitionId.Value;
                    dto.TranslationgrantId = entity.ItemId;
                    //XsiExhibition exhibitionentity = ExhibitionService.GetExhibitionByItemId(entity.ExhibitionId.Value);
                    //if (Exhibition != default(XsiExhibition))
                    //if (exhibitionentity != null)
                    //    dto.WebsiteId = exhibitionentity.WebsiteId.Value;
                    //else
                    dto.WebsiteId = 1;
                    if (entity.Phase1StartDate != null && entity.Phase1EndDate != null)
                        if (MethodFactory.ArabianTimeNow() >= entity.Phase1StartDate && MethodFactory.ArabianTimeNow() < entity.Phase1EndDate.Value.AddDays(1))
                            dto.IsDateExpired = false;
                }
                return dto;
            }
        }
        bool IsFirstFormRegistration(long memberId, long translationgrantid)
        {
            using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
            {
                XsiExhibitionTranslationGrantMembers XsiExhibitionTranslationGrantMembers = new XsiExhibitionTranslationGrantMembers();
                XsiExhibitionTranslationGrantMembers.TranslationGrantId = translationgrantid;
                XsiExhibitionTranslationGrantMembers.MemberId = memberId;
                var TranslationGrantMember = TranslationGrantMemberService.GetTranslationGrantMember(XsiExhibitionTranslationGrantMembers).Where(i => i.MemberId != null).ToList();
                if (TranslationGrantMember == null || TranslationGrantMember.Count <= 0)
                    return true;
                return false;
            }
        }
        bool IsRegistrationInProcess(long memberId, long translationgrantid)
        {
            using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
            {
                if (translationgrantid != -1)
                {
                    XsiExhibitionTranslationGrantMembers where = new XsiExhibitionTranslationGrantMembers();
                    where.MemberId = memberId;

                    where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    where.TranslationGrantId = translationgrantid;

                    XsiExhibitionTranslationGrantMembers entity = TranslationGrantMemberService.GetTranslationGrantMember(where).Where(i => i.MemberId != null).FirstOrDefault();
                    if (entity != null)
                        return true;
                }
                return false;
            }
        }
        List<TranslationGrant> BindTranslationGrant(long memberId, long translationgrantid, long langid)
        {
            using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
            {
                XsiExhibitionTranslationGrantMembers XsiExhibitionTranslationGrantMembers = new XsiExhibitionTranslationGrantMembers();
                XsiExhibitionTranslationGrantMembers.TranslationGrantId = translationgrantid;
                XsiExhibitionTranslationGrantMembers.MemberId = memberId;

                if (langid == 1)
                {
                    var TranslationGrantMemberList = TranslationGrantMemberService.GetTranslationGrantMember(XsiExhibitionTranslationGrantMembers).Where(i => i.MemberId != null)
                                          .Select(x => new TranslationGrant
                                          {
                                              ItemId = x.ItemId,
                                              TranslationGrantId = x.TranslationGrantId,
                                              MemberId = x.MemberId,
                                              BookTitleOriginal = x.BookTitleOriginal,
                                              Status = GetStatus(x.Status, langid),
                                              StatusDate = MethodFactory.ShortDateTG(x.ModifiedOn.ToString()),
                                              IsCancelRegistrationShow = CancelRegistration(x.Status),
                                              FinalContract = string.IsNullOrEmpty(x.FinalContract) ? null : _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/FinalContract/" + x.FinalContract
                                          }).OrderByDescending(i => i.ItemId).ToList();
                    return TranslationGrantMemberList;
                }
                else
                {
                    var TranslationGrantMemberList = TranslationGrantMemberService.GetTranslationGrantMember(XsiExhibitionTranslationGrantMembers).Where(i => i.MemberId != null)
                                          .Select(x => new TranslationGrant
                                          {
                                              ItemId = x.ItemId,
                                              TranslationGrantId = x.TranslationGrantId,
                                              MemberId = x.MemberId,
                                              BookTitleOriginal = x.BookTitleOriginal,
                                              Status = GetStatus(x.Status, langid),
                                              StatusDate = x.ModifiedOn != null ? x.ModifiedOn.Value.ToString("dd MMMM yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")) : string.Empty,
                                              IsCancelRegistrationShow = CancelRegistration(x.Status),
                                              FinalContract = string.IsNullOrEmpty(x.FinalContract) ? null : _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/FinalContract/" + x.FinalContract
                                          }).OrderByDescending(i => i.ItemId).ToList();
                    return TranslationGrantMemberList;
                }
            }
        }
        string GetStatus(string status, long langid)
        {
            if (!string.IsNullOrEmpty(status))
            {
                if (status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.TemporaryViewed))
                    return MethodFactory.GetTranslationGrantMemberStatus(EnumConversion.ToString(EnumTranslationGrantMemberStatus.New), langid);
                else
                    return MethodFactory.GetTranslationGrantMemberStatus(status, langid);
            }
            return "";
        }
        bool CancelRegistration(string status)
        {
            if (!string.IsNullOrEmpty(status))
            {
                if (status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Temporary) || status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.New) || status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.TemporaryViewed) || status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.UnderEvaluation)
                       || status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.PendingDocumentation)
                        || status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.DocumentationResubmitted))
                    return true;
                //else if (status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.CancelledByUser) || status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.UserCancellationPending) || status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Rejected) || status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Canceled))
                //     return false;
            }
            return false;
        }
        bool IsRejectedRegistration(long memberId, long translationgrantmemberId)
        {
            using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
            {
                XsiExhibitionTranslationGrantMembers member = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(translationgrantmemberId);
                if (member != default(XsiExhibitionTranslationGrantMembers)) { }
                var TranslationGrantId = member.TranslationGrantId.Value;
                if (TranslationGrantId != -1)
                {
                    XsiExhibitionTranslationGrantMembers where = new XsiExhibitionTranslationGrantMembers();
                    where.ItemId = translationgrantmemberId;
                    where.MemberId = memberId;
                    where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    where.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.Rejected);
                    where.TranslationGrantId = TranslationGrantId;
                    XsiExhibitionTranslationGrantMembers entity = TranslationGrantMemberService.GetTranslationGrantMember(where).Where(i => i.MemberId != null).FirstOrDefault();
                    if (entity != null)
                        return true;
                }
            }
            return false;
        }
        string SaveBankDetails(ref XsiExhibitionTranslationGrantMembers TranslationGrantMember, BuyersOrSellersContractPost model, long langId)
        {
            //TranslationGrantMember.BankName = model.BankName;
            //TranslationGrantMember.BranchNameAddress = model.BranchNameAddress;
            //TranslationGrantMember.AccNo = model.AccountNumber;
            //if (model.IsIBANorBoth != "0")
            //    TranslationGrantMember.IsIbanorBoth = model.IsIBANorBoth;
            //if (model.IsIBANorBoth == EnumConversion.ToString(EnumTranslationGrantIBANorSwiftCodeStatus.Both))
            //{
            //    TranslationGrantMember.Ibanno = model.IBANNo;
            //    TranslationGrantMember.SwiftCode = model.SwiftCode;
            //}
            //else if (model.IsIBANorBoth == EnumConversion.ToString(EnumTranslationGrantIBANorSwiftCodeStatus.IBAN))
            //    TranslationGrantMember.Ibanno = model.IBANNo;
            //else if (model.IsIBANorBoth == EnumConversion.ToString(EnumTranslationGrantIBANorSwiftCodeStatus.SwiftCode))
            //    TranslationGrantMember.SwiftCode = model.SwiftCode;

            if (model.TransferFormFileBase64 != null && model.TransferFormFileBase64.Length > 0)
            {
                byte[] imageBytes;
                if (model.TransferFormFileBase64.Contains("data:"))
                {
                    var strInfo = model.TransferFormFileBase64.Split(",")[0];
                    imageBytes = Convert.FromBase64String(model.TransferFormFileBase64.Split(',')[1]);
                }
                else
                {
                    imageBytes = Convert.FromBase64String(model.TransferFormFileBase64);
                }
                string FileName = string.Format("{0}_{1}.{2}", langId, MethodFactory.GetRandomNumber(), model.TransferFormFileExtension);
                System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\TranslationGrantMember\\TransferForm\\") + FileName, imageBytes);
                TranslationGrantMember.TransferForm = FileName;
            }
            TranslationGrantMember.ModifiedOn = DateTime.Now;
            var TGMemberStatus = GetTGMemberStatus(TranslationGrantMember.ItemId);
            if (TGMemberStatus == EnumConversion.ToString(EnumTranslationGrantMemberStatus.PendingDocumentation))
            {
                TranslationGrantMember.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.DocumentationResubmitted);
                return "Your Details had been Re submitted successfully.";//"Bank Details has been Re-submitted.";
            }
            else if (TGMemberStatus == EnumConversion.ToString(EnumTranslationGrantMemberStatus.IncorrectDocuments))
            {
                TranslationGrantMember.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.BankDetailsResubmitted);
                return "Your Details had been Re submitted successfully.";//"Bank Details has been Re-submitted.";
            }
            else
            {
                TranslationGrantMember.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.BankDetailsSubmitted);
                return "Your Details had been submitted successfully.";
            }
        }
        string SaveSignedContract(ref XsiExhibitionTranslationGrantMembers TranslationGrantMember, BuyersOrSellersContractPost model, long langId)
        {
            if (model.BuyerorSellerRightsFileBase64 != null && model.BuyerorSellerRightsFileBase64.Length > 0)
            {
                byte[] imageBytes;
                if (model.BuyerorSellerRightsFileBase64.Contains("data:"))
                {
                    var strInfo = model.BuyerorSellerRightsFileBase64.Split(",")[0];
                    imageBytes = Convert.FromBase64String(model.BuyerorSellerRightsFileBase64.Split(',')[1]);
                }
                else
                {
                    imageBytes = Convert.FromBase64String(model.BuyerorSellerRightsFileBase64);
                }
                string FileName = string.Format("{0}_{1}.{2}", langId, MethodFactory.GetRandomNumber(), model.BuyerorSellerRightsFileExtension);
                System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\TranslationGrantMember\\BuyersOrSellersRights\\") + FileName, imageBytes);
                TranslationGrantMember.BuyerorSellerRights = FileName;
            }
            if (model.SignedContractFileBase64 != null && model.SignedContractFileBase64.Length > 0)
            {
                byte[] imageBytes;
                if (model.SignedContractFileBase64.Contains("data:"))
                {
                    var strInfo = model.SignedContractFileBase64.Split(",")[0];
                    imageBytes = Convert.FromBase64String(model.SignedContractFileBase64.Split(',')[1]);
                }
                else
                {
                    imageBytes = Convert.FromBase64String(model.SignedContractFileBase64);
                }
                string FileName = string.Format("{0}_{1}.{2}", langId, MethodFactory.GetRandomNumber(), model.SignedContractFileExtension);
                System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\TranslationGrantMember\\SignedContract\\") + FileName, imageBytes);
                TranslationGrantMember.SignedContract = FileName;
                TranslationGrantMember.ModifiedOn = DateTime.Now;
                var TGMemberStatus = GetTGMemberStatus(TranslationGrantMember.ItemId);
                if (TGMemberStatus == EnumConversion.ToString(EnumTranslationGrantMemberStatus.PendingDocumentation))
                {
                    TranslationGrantMember.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.DocumentationResubmitted);
                    return "Signed Contract has been Re-submitted.";
                }
                else if (TGMemberStatus == EnumConversion.ToString(EnumTranslationGrantMemberStatus.IncorrectDocuments))
                {
                    TranslationGrantMember.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.BankDetailsResubmitted);
                    return "Your Details had been Re submitted successfully.";//"Bank Details has been Re-submitted.";
                }
                else
                {
                    TranslationGrantMember.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.ContractSubmitted);
                    return "Signed Contract has been submitted.";
                }
            }
            return "";
        }
        string GetTGMemberStatus(long translationgrantmemberId)
        {
            using (TranslationGrantMemberService translationGrantMemberService = new TranslationGrantMemberService())
            {
                var translationGrantMembers = translationGrantMemberService.GetTranslationGrantMemberByItemId(translationgrantmemberId);
                if (translationGrantMembers != null)
                    return translationGrantMembers.Status;
                return string.Empty;
            }
        }
        string GetExhibitionDetails(long websiteId)
        {
            websiteId = 1;
            long? exhibitionNumber = -1;
            string strdate = string.Empty;
            string stryear = MethodFactory.ArabianTimeNow().Year.ToString();

            //XsiExhibition exhibitionentity = _context.XsiExhibition.Where(i => i.WebsiteId == websiteId && i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.IsArchive == EnumConversion.ToString(EnumBool.No)).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
            string startdaysuffix = string.Empty;
            string enddaysuffix = string.Empty;

            //long dayno = exhibitionentity.StartDate.Value.Date.Day;
            //if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
            //    startdaysuffix = "th";
            //else
            //    startdaysuffix = GetSuffix(exhibitionentity.StartDate.Value.Date.Day % 10, exhibitionentity.StartDate.Value);
            //dayno = exhibitionentity.EndDate.Value.Date.Day;
            //if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
            //    enddaysuffix = "th";
            //else
            //    enddaysuffix = GetSuffix(exhibitionentity.EndDate.Value.Date.Day % 10, exhibitionentity.EndDate.Value);

            //if (exhibitionentity.StartDate.Value.Year == exhibitionentity.EndDate.Value.Year)
            //{
            //    if (exhibitionentity.StartDate.Value.Month == exhibitionentity.EndDate.Value.Month)
            //        strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " - " + exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " + exhibitionentity.StartDate.Value.ToString("MMM") + " " + exhibitionentity.StartDate.Value.ToString("yyyy");
            //    else
            //        strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " " + exhibitionentity.StartDate.Value.ToString("MMM")
            //             + " - " + exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " + exhibitionentity.EndDate.Value.ToString("MMM")
            //            + " " + exhibitionentity.StartDate.Value.ToString("yyyy");
            //}
            //else
            //{
            //    strdate = exhibitionentity.StartDate.Value.Date.Day + startdaysuffix + " " + exhibitionentity.StartDate.Value.ToString("MMM") + " " + exhibitionentity.StartDate.Value.ToString("yyyy")
            //         + " - " + exhibitionentity.EndDate.Value.Date.Day + enddaysuffix + " " + exhibitionentity.EndDate.Value.ToString("MMM")
            //        + " " + exhibitionentity.EndDate.Value.ToString("yyyy");
            //}

            //if (exhibitionentity.ExhibitionYear != null)
            //    stryear = exhibitionentity.ExhibitionYear.ToString();

            //  return exhibitionNumber.ToString() + "," + strdate + "," + stryear;
            return stryear;
        }
        string GetSuffix(int startday, DateTime dt)
        {
            string daysuffix;
            if (startday != 11 && dt.Date.Day != 11)
            {
                if (startday == 1)
                    daysuffix = "st";
                else if (startday == 2)
                    daysuffix = "nd";
                else if (startday == 3)
                    daysuffix = "rd";
                else
                    daysuffix = "th";
            }
            else
                daysuffix = "th";
            return daysuffix;
        }
        #endregion

        #region User Cancellation
        void CancelRegistration(long itemId, long memberId, long langId)
        {
            using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
            {
                XsiExhibitionTranslationGrantMembers entity = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(itemId);
                if (entity != default(XsiExhibitionTranslationGrantMembers))
                {
                    if (IsValidForCancellation(entity.ItemId))
                        entity.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.CancelledByUser);
                    else
                        entity.Status = EnumConversion.ToString(EnumTranslationGrantMemberStatus.UserCancellationPending);

                    entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                    if (TranslationGrantMemberService.UpdateTranslationGrantMember(entity) == EnumResultType.Success)
                    {
                        SendEmailsForUserCancellation(entity, langId, memberId);

                        //BindTranslationGrant(memberId, entity.TranslationGrantId.Value, langId);
                        //MVRegistration.SetActiveView(Step0View);
                    }
                    // MVRegistration.SetActiveView(ViewRejected);
                }
            }
        }
        bool IsValidForCancellation(long itemId)
        {
            using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
            {
                XsiExhibitionTranslationGrantMembers entity = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(itemId);
                if (entity != default(XsiExhibitionTranslationGrantMembers) && entity.Status != null)
                {
                    switch (entity.Status)
                    {
                        case "T": return true;
                        case "V": return true;
                        case "N": return true;
                        case "A": return true;
                        case "B": return true;
                        case "U": return true;
                        case "C": return true;
                        case "X": return true;
                        case "Y": return true;

                        default: return false;
                    }
                }
            }
            return false;
        }
        #endregion

        #region Load Methods
        TranslationGrantPost LoadStep2Form(long memberId, long langId, long translationgrantmemberId)
        {
            TranslationGrantPost model = new TranslationGrantPost();
            //BindDropDowns();
            //MethodFactory.BindOldSellers(memberId, ddlSellersList, EnglishId, divSellersList);
            using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
            {
                XsiExhibitionTranslationGrantMembers XsiExhibitionTranslationGrantMembers = new XsiExhibitionTranslationGrantMembers();
                var TranslationGrantMember = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(translationgrantmemberId);
                if (TranslationGrantMember != null)
                {
                    TranslationGrantService TranslationGrantService = new TranslationGrantService();
                    XsiExhibitionTranslationGrant WhereTranslationGrant = new XsiExhibitionTranslationGrant();
                    WhereTranslationGrant.ItemId = TranslationGrantMember.TranslationGrantId.Value;
                    // WhereTranslationGrant.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    // WhereTranslationGrant.IsArchive = EnumConversion.ToString(EnumBool.No);
                    var ActiveTranslationGrant = TranslationGrantService.GetTranslationGrant(WhereTranslationGrant, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

                    model.TranslationGrantMemberId = TranslationGrantMember.ItemId;
                    model.SellerFirstName = TranslationGrantMember.SellerFirstName;
                    model.SellerMiddleName = TranslationGrantMember.SellerMiddleName;
                    model.SellerLastName = TranslationGrantMember.SellerLastName;
                    model.SellerJobTitle = TranslationGrantMember.SellerJobTitle;
                    model.SellerCompanyName = TranslationGrantMember.SellerCompanyName;
                    model.SellerAddress = TranslationGrantMember.SellerAddress;
                    if (!string.IsNullOrEmpty(TranslationGrantMember.SellerTelephone))
                    {
                        model.SellerPhoneISD = TranslationGrantMember.SellerTelephone.Split('$')[0];
                        model.SellerPhoneSTD = TranslationGrantMember.SellerTelephone.Split('$')[1];
                        model.SellerPhone = TranslationGrantMember.SellerTelephone.Split('$')[2];
                    }
                    if (!string.IsNullOrEmpty(TranslationGrantMember.SellerFax))
                    {
                        model.SellerFaxISD = TranslationGrantMember.SellerFax.Split('$')[0];
                        model.SellerFaxSTD = TranslationGrantMember.SellerFax.Split('$')[1];
                        model.SellerFax = TranslationGrantMember.SellerFax.Split('$')[2];
                    }
                    model.SellerEmail = TranslationGrantMember.SellerEmail;

                    model.BuyerFirstName = TranslationGrantMember.BuyerFirstName;
                    model.BuyerMiddleName = TranslationGrantMember.BuyerMiddleName;
                    model.BuyerLastName = TranslationGrantMember.BuyerLastName;
                    model.BuyerJobTitle = TranslationGrantMember.BuyerJobTitle;
                    model.BuyerCompanyName = TranslationGrantMember.BuyerCompanyName;
                    model.BuyerAddress = TranslationGrantMember.BuyerAddress;
                    if (!string.IsNullOrEmpty(TranslationGrantMember.BuyerTelephone))
                    {
                        model.BuyerPhoneISD = TranslationGrantMember.BuyerTelephone.Split('$')[0];
                        model.BuyerPhoneSTD = TranslationGrantMember.BuyerTelephone.Split('$')[1];
                        model.BuyerPhone = TranslationGrantMember.BuyerTelephone.Split('$')[2];
                    }
                    if (!string.IsNullOrEmpty(TranslationGrantMember.BuyerFax))
                    {
                        model.BuyerFaxISD = TranslationGrantMember.BuyerFax.Split('$')[0];
                        model.BuyerFaxSTD = TranslationGrantMember.BuyerFax.Split('$')[1];
                        model.BuyerFax = TranslationGrantMember.BuyerFax.Split('$')[2];
                    }
                    model.BuyerEmail = TranslationGrantMember.BuyerEmail;

                    model.BookTitleOriginal = TranslationGrantMember.BookTitleOriginal;
                    model.BookTitleEnglish = TranslationGrantMember.BookTitleEnglish;
                    model.TitleInProposedLanguage = TranslationGrantMember.BookTitleArabic;
                    model.Author = TranslationGrantMember.Author;
                    model.Isbn = TranslationGrantMember.Isbn;

                    if (TranslationGrantMember.OriginalLanguageId != null)
                        model.OriginalLanguageId = TranslationGrantMember.OriginalLanguageId;

                    if (TranslationGrantMember.ProposedLanguageId != null)
                        model.ProposedLanguageId = TranslationGrantMember.ProposedLanguageId;

                    model.NoOfPagesOriginal = TranslationGrantMember.NoOfPagesOriginal;
                    model.NoOfWords = TranslationGrantMember.NoOfWords;
                    model.TranslationType = TranslationGrantMember.TranslationType;

                    if (TranslationGrantMember.GenreId != null)
                    {
                        model.GenreId = TranslationGrantMember.GenreId.Value;
                        if (TranslationGrantMember.GenreId == 9)
                            model.OtherGenre = TranslationGrantMember.Genre;
                    }

                    if (!string.IsNullOrEmpty(TranslationGrantMember.OriginalBookStatus))
                        model.OriginalBookStatus = TranslationGrantMember.OriginalBookStatus;
                    #region Anchors
                    if (TranslationGrantMember.ApplicationForm != null && !string.IsNullOrEmpty(TranslationGrantMember.ApplicationForm))
                    {
                        model.SignedApplicationFormFileName = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/ApplicationForm/" + TranslationGrantMember.ApplicationForm;
                    }

                    if (TranslationGrantMember.OriginalSoftCopy != null && !string.IsNullOrEmpty(TranslationGrantMember.OriginalSoftCopy))
                    {
                        model.SoftCopyofTheBookFileName = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/OriginalSoftCopy/" + TranslationGrantMember.OriginalSoftCopy;
                    }

                    #endregion
                    if (TranslationGrantMember.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.Temporary))
                    {
                        model.ShowSaveAndPrintInfo = true;
                    }
                    else
                    {
                        model.ShowSaveAndPrintInfo = false;
                    }
                }
            }
            return model;
        }
        BuyersOrSellersContractGet LoadStep3Form(long memberId, long LangId, long itemId)
        {
            BuyersOrSellersContractGet ReturnModel = new BuyersOrSellersContractGet();

            using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
            {
                XsiExhibitionTranslationGrantMembers XsiExhibitionTranslationGrantMembers = new XsiExhibitionTranslationGrantMembers();
                var TranslationGrantMember = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(itemId);
                if (TranslationGrantMember != null)
                {
                    TranslationGrantService TranslationGrantService = new TranslationGrantService();
                    XsiExhibitionTranslationGrant WhereTranslationGrant = new XsiExhibitionTranslationGrant();
                    WhereTranslationGrant.ItemId = TranslationGrantMember.TranslationGrantId.Value;
                    //  WhereTranslationGrant.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    //  WhereTranslationGrant.IsArchive = EnumConversion.ToString(EnumBool.No);
                    var ActiveTranslationGrant = TranslationGrantService.GetTranslationGrant(WhereTranslationGrant, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

                    if (TranslationGrantMember.SignedContract != null && !string.IsNullOrEmpty(TranslationGrantMember.SignedContract))
                    {
                        ReturnModel.SignedContract = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/SignedContract/" + TranslationGrantMember.SignedContract;
                    }

                    if (TranslationGrantMember.BuyerorSellerRights != null && !string.IsNullOrEmpty(TranslationGrantMember.BuyerorSellerRights))
                    {
                        ReturnModel.BuyerorSellerRights = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/BuyersOrSellersRights/" + TranslationGrantMember.BuyerorSellerRights;
                    }

                    if (TranslationGrantMember.TransferForm != null && !string.IsNullOrEmpty(TranslationGrantMember.TransferForm))
                    {
                        ReturnModel.TransferForm = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/TransferForm/" + TranslationGrantMember.TransferForm;
                    }
                    if (TranslationGrantMember.TransferFormByAdmin != null && !string.IsNullOrEmpty(TranslationGrantMember.TransferFormByAdmin))
                    {
                        ReturnModel.FundTransferFormByAdmin = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/TransferFormByAdmin/" + TranslationGrantMember.TransferFormByAdmin;
                    }
                    if (TranslationGrantMember.Contract != null && !string.IsNullOrEmpty(TranslationGrantMember.Contract))
                    {
                        ReturnModel.ContractUploadedByAdmin = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/contract/" + TranslationGrantMember.Contract;
                    }

                    //ReturnModel.BankName = TranslationGrantMember.BankName;
                    //ReturnModel.BranchNameAddress = TranslationGrantMember.BranchNameAddress;
                    //ReturnModel.AccountNumber = TranslationGrantMember.AccNo;
                    //ReturnModel.IsIBANorBoth = TranslationGrantMember.IsIbanorBoth;
                    //ReturnModel.IBANNo = TranslationGrantMember.Ibanno;
                    //ReturnModel.SwiftCode = TranslationGrantMember.SwiftCode;
                    //LOAD FOR BUYERS OR SELLERS BROWSE BUTTON

                }
                return ReturnModel;
            }
        }
        LoadReceiptDTO LoadReceipts(long translationgrantmemberId)
        {

            LoadReceiptDTO model = new LoadReceiptDTO();
            using (TranslationGrantMemberService TranslationGrantMemberService = new TranslationGrantMemberService())
            {
                XsiExhibitionTranslationGrantMembers XsiExhibitionTranslationGrantMembers = new XsiExhibitionTranslationGrantMembers();
                var TranslationGrantMember = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(translationgrantmemberId);
                if (TranslationGrantMember != null)
                {
                    TranslationGrantService TranslationGrantService = new TranslationGrantService();
                    XsiExhibitionTranslationGrant WhereTranslationGrant = new XsiExhibitionTranslationGrant();
                    WhereTranslationGrant.ItemId = TranslationGrantMember.TranslationGrantId.Value;
                    //WhereTranslationGrant.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    //WhereTranslationGrant.IsArchive = EnumConversion.ToString(EnumBool.No);

                    var ActiveTranslationGrant = TranslationGrantService.GetTranslationGrant(WhereTranslationGrant, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

                    if (TranslationGrantMember.AwardGrantValue != null)
                        model.AwardGrantValue = TranslationGrantMember.AwardGrantValue;
                    if (TranslationGrantMember.FirstPayment != null)
                        model.FirstPayment = TranslationGrantMember.FirstPayment;
                    if (TranslationGrantMember.FirstPaymentDate != null)
                        model.FirstPaymentDate = MethodFactory.ShortDateTG(TranslationGrantMember.FirstPaymentDate.Value.ToString());
                    if (ActiveTranslationGrant.Phase2EndDate != null)
                        model.Phase2EndDate = MethodFactory.ShortDateTG(ActiveTranslationGrant.Phase2EndDate.Value.ToString());

                    if (ActiveTranslationGrant.Phase3EndDate != null)
                        model.Phase3EndDate = MethodFactory.ShortDateTG(ActiveTranslationGrant.Phase3EndDate.Value.ToString());

                    if (TranslationGrantMember.FirstPaymentReceipt != null && !string.IsNullOrEmpty(TranslationGrantMember.FirstPaymentReceipt))
                    {
                        model.FirstPaymentReceiptStatus = "The first payment has been processed.";
                        model.FirstPaymentReceipt = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/FirstReceipt/" + TranslationGrantMember.FirstPaymentReceipt;
                    }
                    if (TranslationGrantMember.AwardGrantValue != null)
                        model.AwardGrantValueOnStep5 = TranslationGrantMember.AwardGrantValue;

                    if (TranslationGrantMember.AwardGrantValue != null)
                        model.AwardGrantValueOnStep6 = TranslationGrantMember.AwardGrantValue;

                    if (TranslationGrantMember.SecondPayment != null)
                        model.SecondPayment = TranslationGrantMember.SecondPayment;
                    if (TranslationGrantMember.SecondPaymentDate != null)
                        model.SecondPaymentDate = MethodFactory.ShortDateTG(TranslationGrantMember.SecondPaymentDate.Value.ToString());
                    model.IsHardCopy = TranslationGrantMember.IsHardCopy;
                    if (TranslationGrantMember.DateofShipment != null)
                        model.DateofShipment = TranslationGrantMember.DateofShipment;

                    if (TranslationGrantMember.SecondPaymentReceipt != null && !string.IsNullOrEmpty(TranslationGrantMember.SecondPaymentReceipt))
                    {
                        model.SecondPaymentReceipt = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/SecondReceipt/" + TranslationGrantMember.SecondPaymentReceipt;
                    }
                    if (TranslationGrantMember.FinalPayment != null)
                        model.FinalPayment = TranslationGrantMember.FinalPayment;
                    if (TranslationGrantMember.FinalPaymentDate != null)
                        model.FinalPaymentDate = MethodFactory.ShortDateTG(TranslationGrantMember.FinalPaymentDate.Value.ToString());
                    if (TranslationGrantMember.FinalPaymentReceipt != null && !string.IsNullOrEmpty(TranslationGrantMember.FinalPaymentReceipt))
                    {
                        model.FinalPaymentReceipt = _appCustomSettings.UploadsCMSPath + "/TranslationGrantMember/ThirdReceipt/" + TranslationGrantMember.FinalPaymentReceipt;
                    }
                }
                return model;
            }
        }
        #endregion

        #region Email Methods
        void SendEmailBasedOnEdit(XsiExhibitionTranslationGrantMembers translationGrantMember, long memberId, long LangId, XsiExhibitionTranslationGrantMembers oldTranslationGrantMember)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                XsiExhibitionMember ExhibitionMemberEntity = new XsiExhibitionMember();
                ExhibitionMemberEntity = ExhibitionMemberService.GetExhibitionMemberByItemId(Convert.ToInt64(memberId));
                if (ExhibitionMemberEntity != default(XsiExhibitionMember))
                {
                    #region Send Edit Request Email
                    string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                    StringBuilder body = new StringBuilder();
                    EmailContentService EmailContentService = new EmailContentService();
                    XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(20065);
                    if (emailContent != null)
                    {
                        if (emailContent.Body != null)
                            strEmailContentBody = emailContent.Body;
                        if (emailContent.Email != null)
                            strEmailContentEmail = emailContent.Email;
                        if (emailContent.Subject != null)
                            strEmailContentSubject = emailContent.Subject;
                        strEmailContentAdmin = _appCustomSettings.AdminName;
                    }
                    body.Append(MethodFactory.BindEmailContentForTGMember(LangId, strEmailContentBody, ExhibitionMemberEntity, _appCustomSettings.ServerAddressNew, 1, _appCustomSettings));
                    string strContent = "<p style=\"font-size: 16px; line-height: 32px; font-family: Arial, Helvetica, sans-serif; color: #555555; margin: 0px;\">$$Label$$ : $$Value$$</p>";
                    #region Body
                    bool IsAnyFieldChanged = false;
                    body.Replace("$$SpecificName$$", strEmailContentAdmin);
                    #region Translation Type
                    if (oldTranslationGrantMember.TranslationType != translationGrantMember.TranslationType)
                    {
                        if (translationGrantMember.TranslationType != null)
                        {
                            body.Replace("$$newtranslationtype$$", strContent);
                            body.Replace("$$Label$$", "Translation Type");
                            if (!string.IsNullOrEmpty(translationGrantMember.TranslationType))
                            {
                                body.Replace("$$Value$$", translationGrantMember.TranslationType);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$newtranslationtype$$", string.Empty);
                        }
                        else
                            body.Replace("$$newtranslationtype$$", string.Empty);
                        if (oldTranslationGrantMember.TranslationType != null)
                        {
                            body.Replace("$$oldtranslationtype$$", strContent);
                            body.Replace("$$Label$$", "Translation Type");
                            if (!string.IsNullOrEmpty(oldTranslationGrantMember.TranslationType))
                            {
                                body.Replace("$$Value$$", oldTranslationGrantMember.TranslationType);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$oldtranslationtype$$", string.Empty);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldtranslationtype$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newtranslationtype$$", string.Empty);
                        body.Replace("$$oldtranslationtype$$", string.Empty);
                    }
                    #endregion
                    #region Seller Information
                    #region First Name
                    if (oldTranslationGrantMember.SellerFirstName != translationGrantMember.SellerFirstName)
                    {
                        if (translationGrantMember.SellerFirstName != null)
                        {
                            body.Replace("$$newfirstname$$", strContent);
                            body.Replace("$$Label$$", "First Name");
                            body.Replace("$$Value$$", translationGrantMember.SellerFirstName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newfirstname$$", string.Empty);
                        if (oldTranslationGrantMember.SellerFirstName != null)
                        {
                            body.Replace("$$oldfirstname$$", strContent);
                            body.Replace("$$Label$$", "First Name");
                            body.Replace("$$Value$$", oldTranslationGrantMember.SellerFirstName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldfirstname$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newfirstname$$", string.Empty);
                        body.Replace("$$oldfirstname$$", string.Empty);
                    }
                    #endregion
                    #region Middle Name
                    if (oldTranslationGrantMember.SellerMiddleName != translationGrantMember.SellerMiddleName)
                    {
                        if (translationGrantMember.SellerMiddleName != null)
                        {
                            body.Replace("$$newmiddlename$$", strContent);
                            body.Replace("$$Label$$", "Middle Name");
                            body.Replace("$$Value$$", translationGrantMember.SellerMiddleName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newmiddlename$$", string.Empty);
                        if (oldTranslationGrantMember.SellerMiddleName != null)
                        {
                            body.Replace("$$oldmiddlename$$", strContent);
                            body.Replace("$$Label$$", "Middle Name");
                            body.Replace("$$Value$$", oldTranslationGrantMember.SellerMiddleName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldmiddlename$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newmiddlename$$", string.Empty);
                        body.Replace("$$oldmiddlename$$", string.Empty);
                    }
                    #endregion
                    #region Last Name
                    if (oldTranslationGrantMember.SellerLastName != translationGrantMember.SellerLastName)
                    {
                        if (translationGrantMember.SellerLastName != null)
                        {
                            body.Replace("$$newlastname$$", strContent);
                            body.Replace("$$Label$$", "Last Name");
                            body.Replace("$$Value$$", translationGrantMember.SellerLastName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newlastname$$", string.Empty);
                        if (oldTranslationGrantMember.SellerLastName != null)
                        {
                            body.Replace("$$oldlastname$$", strContent);
                            body.Replace("$$Label$$", "Last Name");
                            body.Replace("$$Value$$", oldTranslationGrantMember.SellerLastName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldlastname$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newlastname$$", string.Empty);
                        body.Replace("$$oldlastname$$", string.Empty);
                    }
                    #endregion
                    #region Job Title
                    if (oldTranslationGrantMember.SellerJobTitle != translationGrantMember.SellerJobTitle)
                    {
                        if (translationGrantMember.SellerJobTitle != null)
                        {
                            body.Replace("$$newjobtitle$$", strContent);
                            body.Replace("$$Label$$", "Job Title");
                            body.Replace("$$Value$$", translationGrantMember.SellerJobTitle);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newjobtitle$$", string.Empty);
                        if (oldTranslationGrantMember.SellerJobTitle != null)
                        {
                            body.Replace("$$oldjobtitle$$", strContent);
                            body.Replace("$$Label$$", "Job Title");
                            body.Replace("$$Value$$", oldTranslationGrantMember.SellerJobTitle);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldjobtitle$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newjobtitle$$", string.Empty);
                        body.Replace("$$oldjobtitle$$", string.Empty);
                    }
                    #endregion
                    #region Company Name
                    if (oldTranslationGrantMember.SellerCompanyName != translationGrantMember.SellerCompanyName)
                    {
                        if (translationGrantMember.SellerCompanyName != null)
                        {
                            body.Replace("$$newcompanyname$$", strContent);
                            body.Replace("$$Label$$", "Company Name");
                            body.Replace("$$Value$$", translationGrantMember.SellerCompanyName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newcompanyname$$", string.Empty);
                        if (oldTranslationGrantMember.SellerCompanyName != null)
                        {
                            body.Replace("$$oldcompanyname$$", strContent);
                            body.Replace("$$Label$$", "Company Name");
                            body.Replace("$$Value$$", oldTranslationGrantMember.SellerCompanyName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldcompanyname$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newcompanyname$$", string.Empty);
                        body.Replace("$$oldcompanyname$$", string.Empty);
                    }
                    #endregion
                    #region Address
                    if (oldTranslationGrantMember.SellerAddress != translationGrantMember.SellerAddress)
                    {
                        if (translationGrantMember.SellerAddress != null)
                        {
                            body.Replace("$$newaddress$$", strContent);
                            body.Replace("$$Label$$", "Address");
                            body.Replace("$$Value$$", translationGrantMember.SellerAddress);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newaddress$$", string.Empty);
                        if (oldTranslationGrantMember.SellerAddress != null)
                        {
                            body.Replace("$$oldaddress$$", strContent);
                            body.Replace("$$Label$$", "Address");
                            body.Replace("$$Value$$", oldTranslationGrantMember.SellerAddress);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldaddress$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newaddress$$", string.Empty);
                        body.Replace("$$oldaddress$$", string.Empty);
                    }
                    #endregion
                    #region Phone
                    if (oldTranslationGrantMember.SellerTelephone != translationGrantMember.SellerTelephone)
                    {
                        if (translationGrantMember.SellerTelephone != null)
                        {
                            body.Replace("$$newphone$$", strContent);
                            body.Replace("$$Label$$", "Phone");
                            body.Replace("$$Value$$", translationGrantMember.SellerTelephone.Replace('$', '-'));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newphone$$", string.Empty);
                        if (oldTranslationGrantMember.SellerTelephone != null)
                        {
                            body.Replace("$$oldphone$$", strContent);
                            body.Replace("$$Label$$", "Phone");
                            body.Replace("$$Value$$", oldTranslationGrantMember.SellerTelephone);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldphone$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newphone$$", string.Empty);
                        body.Replace("$$oldphone$$", string.Empty);
                    }
                    #endregion
                    #region Fax
                    if (oldTranslationGrantMember.SellerFax != translationGrantMember.SellerFax)
                    {
                        if (translationGrantMember.SellerFax != null)
                        {
                            body.Replace("$$newfax$$", strContent);
                            body.Replace("$$Label$$", "Fax");
                            body.Replace("$$Value$$", translationGrantMember.SellerFax.Replace('$', '-'));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newfax$$", string.Empty);
                        if (oldTranslationGrantMember.SellerFax != null)
                        {
                            body.Replace("$$oldfax$$", strContent);
                            body.Replace("$$Label$$", "Fax");
                            body.Replace("$$Value$$", oldTranslationGrantMember.SellerFax);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldfax$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newfax$$", string.Empty);
                        body.Replace("$$oldfax$$", string.Empty);
                    }
                    #endregion
                    #region Email
                    if (oldTranslationGrantMember.SellerEmail != translationGrantMember.SellerEmail)
                    {
                        if (translationGrantMember.SellerEmail != null)
                        {
                            body.Replace("$$newemailaddress$$", strContent);
                            body.Replace("$$Label$$", "Email");
                            body.Replace("$$Value$$", translationGrantMember.SellerEmail);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newemailaddress$$", string.Empty);
                        if (oldTranslationGrantMember.SellerEmail != null)
                        {
                            body.Replace("$$oldemailaddress$$", strContent);
                            body.Replace("$$Label$$", "Email");
                            body.Replace("$$Value$$", oldTranslationGrantMember.SellerEmail);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldemailaddress$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newemailaddress$$", string.Empty);
                        body.Replace("$$oldemailaddress$$", string.Empty);
                    }
                    #endregion
                    #endregion
                    #region Publisher Information
                    #region First Name
                    if (oldTranslationGrantMember.BuyerFirstName != translationGrantMember.BuyerFirstName)
                    {
                        if (translationGrantMember.BuyerFirstName != null)
                        {
                            body.Replace("$$newbuyerfirstname$$", strContent);
                            body.Replace("$$Label$$", "First Name");
                            body.Replace("$$Value$$", translationGrantMember.BuyerFirstName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newbuyerfirstname$$", string.Empty);
                        if (oldTranslationGrantMember.BuyerFirstName != null)
                        {
                            body.Replace("$$oldbuyerfirstname$$", strContent);
                            body.Replace("$$Label$$", "First Name");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BuyerFirstName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldbuyerfirstname$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newbuyerfirstname$$", string.Empty);
                        body.Replace("$$oldbuyerfirstname$$", string.Empty);
                    }
                    #endregion
                    #region Middle Name
                    if (oldTranslationGrantMember.BuyerMiddleName != translationGrantMember.BuyerMiddleName)
                    {
                        if (translationGrantMember.BuyerMiddleName != null)
                        {
                            body.Replace("$$newbuyermiddlename$$", strContent);
                            body.Replace("$$Label$$", "Middle Name");
                            body.Replace("$$Value$$", translationGrantMember.BuyerMiddleName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newbuyermiddlename$$", string.Empty);
                        if (oldTranslationGrantMember.BuyerMiddleName != null)
                        {
                            body.Replace("$$oldbuyermiddlename$$", strContent);
                            body.Replace("$$Label$$", "Middle Name");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BuyerMiddleName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldbuyermiddlename$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newbuyermiddlename$$", string.Empty);
                        body.Replace("$$oldbuyermiddlename$$", string.Empty);
                    }
                    #endregion
                    #region Last Name
                    if (oldTranslationGrantMember.BuyerLastName != translationGrantMember.BuyerLastName)
                    {
                        if (translationGrantMember.BuyerLastName != null)
                        {
                            body.Replace("$$newbuyerlastname$$", strContent);
                            body.Replace("$$Label$$", "Last Name");
                            body.Replace("$$Value$$", translationGrantMember.BuyerLastName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newbuyerlastname$$", string.Empty);
                        if (oldTranslationGrantMember.BuyerLastName != null)
                        {
                            body.Replace("$$oldbuyerlastname$$", strContent);
                            body.Replace("$$Label$$", "Last Name");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BuyerLastName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldbuyerlastname$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newbuyerlastname$$", string.Empty);
                        body.Replace("$$oldbuyerlastname$$", string.Empty);
                    }
                    #endregion
                    #region Job Title
                    if (oldTranslationGrantMember.BuyerJobTitle != translationGrantMember.BuyerJobTitle)
                    {
                        if (translationGrantMember.BuyerJobTitle != null)
                        {
                            body.Replace("$$newbuyerjobtitle$$", strContent);
                            body.Replace("$$Label$$", "Job Title");
                            body.Replace("$$Value$$", translationGrantMember.BuyerJobTitle);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newbuyerjobtitle$$", string.Empty);
                        if (oldTranslationGrantMember.BuyerJobTitle != null)
                        {
                            body.Replace("$$oldbuyerjobtitle$$", strContent);
                            body.Replace("$$Label$$", "Job Title");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BuyerJobTitle);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldbuyerjobtitle$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newbuyerjobtitle$$", string.Empty);
                        body.Replace("$$oldbuyerjobtitle$$", string.Empty);
                    }
                    #endregion
                    #region Company Name
                    if (oldTranslationGrantMember.BuyerCompanyName != translationGrantMember.BuyerCompanyName)
                    {
                        if (translationGrantMember.BuyerCompanyName != null)
                        {
                            body.Replace("$$newbuyercompanyname$$", strContent);
                            body.Replace("$$Label$$", "Company Name");
                            body.Replace("$$Value$$", translationGrantMember.BuyerCompanyName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newbuyercompanyname$$", string.Empty);
                        if (oldTranslationGrantMember.BuyerCompanyName != null)
                        {
                            body.Replace("$$oldbuyercompanyname$$", strContent);
                            body.Replace("$$Label$$", "Company Name");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BuyerCompanyName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldbuyercompanyname$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newbuyercompanyname$$", string.Empty);
                        body.Replace("$$oldbuyercompanyname$$", string.Empty);
                    }
                    #endregion
                    #region Address
                    if (oldTranslationGrantMember.BuyerAddress != translationGrantMember.BuyerAddress)
                    {
                        if (translationGrantMember.BuyerAddress != null)
                        {
                            body.Replace("$$newbuyeraddress$$", strContent);
                            body.Replace("$$Label$$", "Address");
                            body.Replace("$$Value$$", translationGrantMember.BuyerAddress);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newbuyeraddress$$", string.Empty);
                        if (oldTranslationGrantMember.BuyerAddress != null)
                        {
                            body.Replace("$$oldbuyeraddress$$", strContent);
                            body.Replace("$$Label$$", "Address");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BuyerAddress);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldbuyeraddress$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newbuyeraddress$$", string.Empty);
                        body.Replace("$$oldbuyeraddress$$", string.Empty);
                    }
                    #endregion
                    #region Phone
                    if (oldTranslationGrantMember.BuyerTelephone != translationGrantMember.BuyerTelephone)
                    {
                        if (translationGrantMember.BuyerTelephone != null)
                        {
                            body.Replace("$$newbuyerphone$$", strContent);
                            body.Replace("$$Label$$", "Phone");
                            body.Replace("$$Value$$", translationGrantMember.BuyerTelephone.Replace('$', '-'));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newbuyerphone$$", string.Empty);
                        if (oldTranslationGrantMember.BuyerTelephone != null)
                        {
                            body.Replace("$$oldbuyerphone$$", strContent);
                            body.Replace("$$Label$$", "Phone");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BuyerTelephone);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldbuyerphone$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newbuyerphone$$", string.Empty);
                        body.Replace("$$oldbuyerphone$$", string.Empty);
                    }
                    #endregion
                    #region Fax
                    if (oldTranslationGrantMember.BuyerFax != translationGrantMember.BuyerFax)
                    {
                        if (translationGrantMember.BuyerFax != null)
                        {
                            body.Replace("$$newbuyerfax$$", strContent);
                            body.Replace("$$Label$$", "Fax");
                            body.Replace("$$Value$$", translationGrantMember.BuyerFax.Replace('$', '-'));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newbuyerfax$$", string.Empty);
                        if (oldTranslationGrantMember.BuyerFax != null)
                        {
                            body.Replace("$$oldbuyerfax$$", strContent);
                            body.Replace("$$Label$$", "Fax");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BuyerFax);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldbuyerfax$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newbuyerfax$$", string.Empty);
                        body.Replace("$$oldbuyerfax$$", string.Empty);
                    }
                    #endregion
                    #region Email
                    if (oldTranslationGrantMember.BuyerEmail != translationGrantMember.BuyerEmail)
                    {
                        if (translationGrantMember.BuyerEmail != null)
                        {
                            body.Replace("$$newbuyeremailaddress$$", strContent);
                            body.Replace("$$Label$$", "Email");
                            body.Replace("$$Value$$", translationGrantMember.BuyerEmail);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newbuyeremailaddress$$", string.Empty);
                        if (oldTranslationGrantMember.BuyerEmail != null)
                        {
                            body.Replace("$$oldbuyeremailaddress$$", strContent);
                            body.Replace("$$Label$$", "Email");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BuyerEmail);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldbuyeremailaddress$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newbuyeremailaddress$$", string.Empty);
                        body.Replace("$$oldbuyeremailaddress$$", string.Empty);
                    }
                    #endregion
                    #endregion
                    #region Book Details
                    #region Title In Original Language
                    if (oldTranslationGrantMember.BookTitleOriginal != translationGrantMember.BookTitleOriginal)
                    {
                        if (translationGrantMember.BookTitleOriginal != null)
                        {
                            body.Replace("$$newtitleinoriginallanguage$$", strContent);
                            body.Replace("$$Label$$", "Title In Original Language");
                            body.Replace("$$Value$$", translationGrantMember.BookTitleOriginal);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newtitleinoriginallanguage$$", string.Empty);
                        if (oldTranslationGrantMember.BookTitleOriginal != null)
                        {
                            body.Replace("$$oldtitleinoriginallanguage$$", strContent);
                            body.Replace("$$Label$$", "Title In Original Language");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BookTitleOriginal);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldtitleinoriginallanguage$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newtitleinoriginallanguage$$", string.Empty);
                        body.Replace("$$oldtitleinoriginallanguage$$", string.Empty);
                    }
                    #endregion
                    #region Title In English
                    if (oldTranslationGrantMember.BookTitleEnglish != translationGrantMember.BookTitleEnglish)
                    {
                        if (translationGrantMember.BookTitleEnglish != null)
                        {
                            body.Replace("$$newtitleinenglish$$", strContent);
                            body.Replace("$$Label$$", "Title In English");
                            body.Replace("$$Value$$", translationGrantMember.BookTitleEnglish);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newtitleinenglish$$", string.Empty);
                        if (oldTranslationGrantMember.BookTitleEnglish != null)
                        {
                            body.Replace("$$oldtitleinenglish$$", strContent);
                            body.Replace("$$Label$$", "Title In English");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BookTitleEnglish);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldtitleinenglish$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newtitleinenglish$$", string.Empty);
                        body.Replace("$$oldtitleinenglish$$", string.Empty);
                    }
                    #endregion
                    #region Title In Proposed Language
                    if (oldTranslationGrantMember.BookTitleArabic != translationGrantMember.BookTitleArabic)
                    {
                        if (translationGrantMember.BookTitleArabic != null)
                        {
                            body.Replace("$$newtitleinproposedlanguage$$", strContent);
                            body.Replace("$$Label$$", "Title In Proposed Language");
                            body.Replace("$$Value$$", translationGrantMember.BookTitleArabic);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newtitleinproposedlanguage$$", string.Empty);
                        if (oldTranslationGrantMember.BookTitleArabic != null)
                        {
                            body.Replace("$$oldtitleinproposedlanguage$$", strContent);
                            body.Replace("$$Label$$", "Title In Proposed Language");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BookTitleArabic);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldtitleinproposedlanguage$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newtitleinproposedlanguage$$", string.Empty);
                        body.Replace("$$oldtitleinproposedlanguage$$", string.Empty);
                    }
                    #endregion
                    #region Author
                    if (oldTranslationGrantMember.Author != translationGrantMember.Author)
                    {
                        if (translationGrantMember.Author != null)
                        {
                            body.Replace("$$newauthor$$", strContent);
                            body.Replace("$$Label$$", "Author");
                            body.Replace("$$Value$$", translationGrantMember.Author);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newauthor$$", string.Empty);
                        if (oldTranslationGrantMember.Author != null)
                        {
                            body.Replace("$$oldauthor$$", strContent);
                            body.Replace("$$Label$$", "Author");
                            body.Replace("$$Value$$", oldTranslationGrantMember.Author);
                            IsAnyFieldChanged = true;

                        }
                        else
                            body.Replace("$$oldauthor$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newauthor$$", string.Empty);
                        body.Replace("$$oldauthor$$", string.Empty);
                    }
                    #endregion
                    #region ISBN
                    if (oldTranslationGrantMember.Isbn != translationGrantMember.Isbn)
                    {
                        if (translationGrantMember.Isbn != null)
                        {
                            body.Replace("$$newisbn$$", strContent);
                            body.Replace("$$Label$$", "ISBN");
                            body.Replace("$$Value$$", translationGrantMember.Isbn);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newisbn$$", string.Empty);
                        if (oldTranslationGrantMember.Isbn != null)
                        {
                            body.Replace("$$oldisbn$$", strContent);
                            body.Replace("$$Label$$", "ISBN");
                            body.Replace("$$Value$$", oldTranslationGrantMember.Isbn);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldisbn$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newisbn$$", string.Empty);
                        body.Replace("$$oldisbn$$", string.Empty);
                    }
                    #endregion
                    #region Genre
                    if (oldTranslationGrantMember.Genre != translationGrantMember.Genre)
                    {
                        if (translationGrantMember.Genre != null)
                        {
                            body.Replace("$$newgenre$$", strContent);
                            body.Replace("$$Label$$", "Genre");
                            if (translationGrantMember.GenreId == 10)
                                body.Replace("$$Value$$", translationGrantMember.Genre);
                            else
                                body.Replace("$$Value$$", MethodFactory.GetTranslationGranteGenrebyId(translationGrantMember.GenreId.Value, 1));// TODO LanguageId
                                                                                                                                                //body.Replace("$$Value$$", translationGrantMember.Genre);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newgenre$$", string.Empty);
                        if (oldTranslationGrantMember.Genre != null)
                        {
                            body.Replace("$$oldgenre$$", strContent);
                            body.Replace("$$Label$$", "Genre");
                            if (oldTranslationGrantMember.GenreId == 10)
                                body.Replace("$$Value$$", oldTranslationGrantMember.Genre);
                            else
                                body.Replace("$$Value$$", MethodFactory.GetTranslationGranteGenrebyId(oldTranslationGrantMember.GenreId.Value, 1));// TODO LanguageId
                                                                                                                                                   //body.Replace("$$Value$$", oldTranslationGrantMember.Genre);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldgenre$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newgenre$$", string.Empty);
                        body.Replace("$$oldgenre$$", string.Empty);
                    }
                    #endregion
                    #region Number Of Pages In Original Books
                    if (oldTranslationGrantMember.NoOfPagesOriginal != translationGrantMember.NoOfPagesOriginal)
                    {
                        if (translationGrantMember.NoOfPagesOriginal != null)
                        {
                            body.Replace("$$newnoofpagesinoriginalbooks$$", strContent);
                            body.Replace("$$Label$$", "Number Of Pages In Original Books");
                            body.Replace("$$Value$$", translationGrantMember.NoOfPagesOriginal);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newnoofpagesinoriginalbooks$$", string.Empty);
                        if (oldTranslationGrantMember.NoOfPagesOriginal != null)
                        {
                            body.Replace("$$oldnoofpagesinoriginalbooks$$", strContent);
                            body.Replace("$$Label$$", "Number Of Pages In Original Books");
                            body.Replace("$$Value$$", oldTranslationGrantMember.NoOfPagesOriginal);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldnoofpagesinoriginalbooks$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newnoofpagesinoriginalbooks$$", string.Empty);
                        body.Replace("$$oldnoofpagesinoriginalbooks$$", string.Empty);
                    }
                    #endregion
                    #region Number of words in the book
                    if (oldTranslationGrantMember.NoOfWords != translationGrantMember.NoOfWords)
                    {
                        if (translationGrantMember.NoOfWords != null)
                        {
                            body.Replace("$$newnoofwordsinbook$$ ", strContent);
                            body.Replace("$$Label$$", "Number of words in the book");
                            body.Replace("$$Value$$", translationGrantMember.NoOfWords);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newnoofwordsinbook$$ ", string.Empty);
                        if (oldTranslationGrantMember.NoOfWords != null)
                        {
                            body.Replace("$$oldnoofwordsinbook$$ ", strContent);
                            body.Replace("$$Label$$", "Number of words in the book");
                            body.Replace("$$Value$$", oldTranslationGrantMember.NoOfWords);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldnoofwordsinbook$$ ", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newnoofwordsinbook$$ ", string.Empty);
                        body.Replace("$$oldnoofwordsinbook$$ ", string.Empty);
                    }
                    #endregion
                    #region Original Language
                    if (oldTranslationGrantMember.OriginalLanguageId != translationGrantMember.OriginalLanguageId)
                    {
                        if (translationGrantMember.OriginalLanguageId != null)
                        {
                            body.Replace("$$neworiginallanguage$$", strContent);
                            body.Replace("$$Label$$", "Original Language need to work");
                            body.Replace("$$Value$$", MethodFactory.GetExhibitionLanuageTitle(translationGrantMember.OriginalLanguageId.Value, 1));// TODO LanguageId
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$neworiginallanguage$$", string.Empty);
                        if (oldTranslationGrantMember.OriginalLanguageId != null)
                        {
                            body.Replace("$$oldoriginallanguage$$", strContent);
                            body.Replace("$$Label$$", "Original Language need to work");
                            body.Replace("$$Value$$", MethodFactory.GetExhibitionLanuageTitle(oldTranslationGrantMember.OriginalLanguageId.Value, 1));// TODO LanguageId
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldoriginallanguage$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$neworiginallanguage$$", string.Empty);
                        body.Replace("$$oldoriginallanguage$$", string.Empty);
                    }
                    #endregion
                    #region Proposed Language Translation
                    if (oldTranslationGrantMember.ProposedLanguageId != translationGrantMember.ProposedLanguageId)
                    {
                        if (translationGrantMember.ProposedLanguageId != null)
                        {
                            body.Replace("$$newproposedlanguage$$", strContent);
                            body.Replace("$$Label$$", "Proposed Language Translation");
                            body.Replace("$$Value$$", MethodFactory.GetExhibitionLanuageTitle(translationGrantMember.ProposedLanguageId.Value, 1));// TODO LanguageId
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newproposedlanguage$$", string.Empty);
                        if (oldTranslationGrantMember.ProposedLanguageId != null)
                        {
                            body.Replace("$$oldproposedlanguage$$", strContent);
                            body.Replace("$$Label$$", "Proposed Language Translation");
                            body.Replace("$$Value$$", MethodFactory.GetExhibitionLanuageTitle(oldTranslationGrantMember.ProposedLanguageId.Value, 1));// TODO LanguageId
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldproposedlanguage$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newproposedlanguage$$", string.Empty);
                        body.Replace("$$oldproposedlanguage$$", string.Empty);
                    }
                    #endregion
                    #endregion
                    #region Upload Signed Application Form (Pdf)
                    if (oldTranslationGrantMember.ApplicationForm != translationGrantMember.ApplicationForm)
                    {
                        if (translationGrantMember.ApplicationForm != null)
                        {
                            body.Replace("$$newuploadsignedapplicationform$$", strContent);
                            body.Replace("$$Label$$", "Upload Signed Application Form (Pdf)");
                            body.Replace("$$Value$$", translationGrantMember.ApplicationForm);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newuploadsignedapplicationform$$", string.Empty);
                        if (oldTranslationGrantMember.ApplicationForm != null)
                        {
                            body.Replace("$$olduploadsignedapplicationform$$", strContent);
                            body.Replace("$$Label$$", "Upload Signed Application Form (Pdf)");
                            body.Replace("$$Value$$", oldTranslationGrantMember.ApplicationForm);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$olduploadsignedapplicationform$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newuploadsignedapplicationform$$", string.Empty);
                        body.Replace("$$olduploadsignedapplicationform$$", string.Empty);
                    }
                    #endregion
                    #region Original Book Provided As
                    if (oldTranslationGrantMember.OriginalBookStatus != translationGrantMember.OriginalBookStatus)
                    {
                        if (translationGrantMember.OriginalBookStatus != null)
                        {
                            body.Replace("$$neworiginalbookprovidedas$$", strContent);
                            body.Replace("$$Label$$", "Original Book Provided As");
                            body.Replace("$$Value$$", translationGrantMember.OriginalBookStatus);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$neworiginalbookprovidedas$$", string.Empty);
                        if (oldTranslationGrantMember.OriginalBookStatus != null)
                        {
                            body.Replace("$$oldoriginalbookprovidedas$$", strContent);
                            body.Replace("$$Label$$", "Original Book Provided As");
                            body.Replace("$$Value$$", oldTranslationGrantMember.OriginalBookStatus);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldoriginalbookprovidedas$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$neworiginalbookprovidedas$$", string.Empty);
                        body.Replace("$$oldoriginalbookprovidedas$$", string.Empty);
                    }
                    #endregion
                    #region Upload Soft Copy Of The Book (Pdf,Doc,Docx)
                    if (oldTranslationGrantMember.OriginalSoftCopy != translationGrantMember.OriginalSoftCopy)
                    {
                        if (translationGrantMember.OriginalSoftCopy != null)
                        {
                            body.Replace("$$newuploadsoftcopybook$$", strContent);
                            body.Replace("$$Label$$", "Upload Soft Copy Of The Book");
                            body.Replace("$$Value$$", translationGrantMember.OriginalSoftCopy);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newuploadsoftcopybook$$", string.Empty);
                        if (oldTranslationGrantMember.OriginalSoftCopy != null)
                        {
                            body.Replace("$$olduploadsoftcopybook$$", strContent);
                            body.Replace("$$Label$$", "Upload Soft Copy Of The Book");
                            body.Replace("$$Value$$", oldTranslationGrantMember.OriginalSoftCopy);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$olduploadsoftcopybook$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newuploadsoftcopybook$$", string.Empty);
                        body.Replace("$$olduploadsoftcopybook$$", string.Empty);
                    }
                    #endregion

                    #region Upload Signed Contract(pdf)
                    if (oldTranslationGrantMember.SignedContract != translationGrantMember.SignedContract)
                    {
                        if (translationGrantMember.SignedContract != null)
                        {
                            body.Replace("$$newuploadsignedcontractpdf$$", strContent);
                            body.Replace("$$Label$$", "Upload Signed Contract");
                            body.Replace("$$Value$$", translationGrantMember.SignedContract);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$olduploadsignedcontractpdf$$", string.Empty);
                        if (oldTranslationGrantMember.SignedContract != null)
                        {
                            body.Replace("$$newuploadsignedcontractpdf$$", strContent);
                            body.Replace("$$Label$$", "Upload Signed Contract");
                            body.Replace("$$Value$$", oldTranslationGrantMember.SignedContract);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$olduploadsignedcontractpdf$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newuploadsignedcontractpdf$$", string.Empty);
                        body.Replace("$$olduploadsignedcontractpdf$$", string.Empty);
                    }
                    #endregion
                    #region Buyers and Sellers Rights Contract(pdf,jpg,gif,png,tiff)
                    if (oldTranslationGrantMember.BuyerorSellerRights != translationGrantMember.BuyerorSellerRights)
                    {
                        if (translationGrantMember.BuyerorSellerRights != null)
                        {
                            body.Replace("$$newbuyersandsellersrightcontract$$", strContent);
                            body.Replace("$$Label$$", "Buyeror Seller Rights");
                            body.Replace("$$Value$$", translationGrantMember.BuyerorSellerRights);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newbuyersandsellersrightcontract$$", string.Empty);
                        if (oldTranslationGrantMember.BuyerorSellerRights != null)
                        {
                            body.Replace("$$oldbuyersandsellersrightcontract$$", strContent);
                            body.Replace("$$Label$$", "Buyeror Seller Rights");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BuyerorSellerRights);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldbuyersandsellersrightcontract$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newbuyersandsellersrightcontract$$", string.Empty);
                        body.Replace("$$oldbuyersandsellersrightcontract$$", string.Empty);
                    }
                    #endregion
                    #region Funds Transfer Form (pdf,jpg,gif,png,tiff,xls)
                    if (oldTranslationGrantMember.TransferForm != translationGrantMember.TransferForm)
                    {
                        if (translationGrantMember.TransferForm != null)
                        {
                            body.Replace("$$newfundstransferform$$", strContent);
                            body.Replace("$$Label$$", "Funds Transfer Form");
                            body.Replace("$$Value$$", translationGrantMember.TransferForm);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newfundstransferform$$", string.Empty);
                        if (oldTranslationGrantMember.TransferForm != null)
                        {
                            body.Replace("$$oldfundstransferform$$", strContent);
                            body.Replace("$$Label$$", "Funds Transfer Form");
                            body.Replace("$$Value$$", oldTranslationGrantMember.TransferForm);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldfundstransferform$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newfundstransferform$$", string.Empty);
                        body.Replace("$$oldfundstransferform$$", string.Empty);
                    }
                    #endregion

                    #region Bank Name
                    if (oldTranslationGrantMember.BankName != translationGrantMember.BankName)
                    {
                        if (translationGrantMember.BankName != null)
                        {
                            body.Replace("$$newbankname$$", strContent);
                            body.Replace("$$Label$$", "Bank Name");
                            body.Replace("$$Value$$", translationGrantMember.BankName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newbankname$$", string.Empty);
                        if (oldTranslationGrantMember.BankName != null)
                        {
                            body.Replace("$$oldbankname$$", strContent);
                            body.Replace("$$Label$$", "Bank Name");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BankName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldbankname$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newbankname$$", string.Empty);
                        body.Replace("$$oldbankname$$", string.Empty);
                    }
                    #endregion
                    #region Branch Name &amp; Address
                    if (oldTranslationGrantMember.BranchNameAddress != translationGrantMember.BranchNameAddress)
                    {
                        if (translationGrantMember.BranchNameAddress != null)
                        {
                            body.Replace("$$newbranchnameandaddress$$ ", strContent);
                            body.Replace("$$Label$$", "Branch Name &amp; Address");
                            body.Replace("$$Value$$", translationGrantMember.BranchNameAddress);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newbranchnameandaddress$$ ", string.Empty);
                        if (oldTranslationGrantMember.BranchNameAddress != null)
                        {
                            body.Replace("$$oldbranchnameandaddress$$ ", strContent);
                            body.Replace("$$Label$$", "Branch Name &amp; Address");
                            body.Replace("$$Value$$", oldTranslationGrantMember.BranchNameAddress);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldbranchnameandaddress$$ ", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newbranchnameandaddress$$ ", string.Empty);
                        body.Replace("$$oldbranchnameandaddress$$ ", string.Empty);
                    }
                    #endregion
                    #region Account Number
                    if (oldTranslationGrantMember.AccNo != translationGrantMember.AccNo)
                    {
                        if (translationGrantMember.AccNo != null)
                        {
                            body.Replace("$$newaccountnumber$$", strContent);
                            body.Replace("$$Label$$", "Account Number");
                            body.Replace("$$Value$$", translationGrantMember.AccNo);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newaccountnumber$$", string.Empty);
                        if (oldTranslationGrantMember.AccNo != null)
                        {
                            body.Replace("$$oldaccountnumber$$", strContent);
                            body.Replace("$$Label$$", "Account Number");
                            body.Replace("$$Value$$", oldTranslationGrantMember.AccNo);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldaccountnumber$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newaccountnumber$$", string.Empty);
                        body.Replace("$$oldaccountnumber$$", string.Empty);
                    }
                    #endregion
                    #region IBAN Or Both
                    if (oldTranslationGrantMember.IsIbanorBoth != translationGrantMember.IsIbanorBoth)
                    {
                        if (translationGrantMember.IsIbanorBoth != null)
                        {
                            body.Replace("$$newpleaseselectfromlistibanorboth$$", strContent);
                            body.Replace("$$Label$$", "IBAN Or Both");
                            if (string.IsNullOrEmpty(translationGrantMember.IsIbanorBoth))
                            {
                                body.Replace("$$Value$$", translationGrantMember.IsIbanorBoth);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$newpleaseselectfromlistibanorboth$$", string.Empty);
                        }
                        else
                            body.Replace("$$newpleaseselectfromlistibanorboth$$", string.Empty);
                        if (oldTranslationGrantMember.IsIbanorBoth != null)
                        {
                            body.Replace("$$oldpleaseselectfromlistibanorboth$$", strContent);
                            body.Replace("$$Label$$", "IBAN Or Both");
                            if (!string.IsNullOrEmpty(oldTranslationGrantMember.IsIbanorBoth))
                            {
                                body.Replace("$$Value$$", oldTranslationGrantMember.IsIbanorBoth);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$oldpleaseselectfromlistibanorboth$$", string.Empty);
                        }
                        else
                            body.Replace("$$oldpleaseselectfromlistibanorboth$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newpleaseselectfromlistibanorboth$$", string.Empty);
                        body.Replace("$$oldpleaseselectfromlistibanorboth$$", string.Empty);
                    }
                    #endregion
                    #region IBAN Number
                    if (oldTranslationGrantMember.Ibanno != translationGrantMember.Ibanno)
                    {
                        if (translationGrantMember.Ibanno != null)
                        {
                            body.Replace("$$newibannumber$$", strContent);
                            body.Replace("$$Label$$", "IBAN Number");
                            body.Replace("$$Value$$", translationGrantMember.Ibanno);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newibannumber$$", string.Empty);
                        if (oldTranslationGrantMember.OriginalSoftCopy != null)
                        {
                            body.Replace("$$oldibannumber$$", strContent);
                            body.Replace("$$Label$$", "IBAN Number");
                            body.Replace("$$Value$$", oldTranslationGrantMember.Ibanno);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldibannumber$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newibannumber$$", string.Empty);
                        body.Replace("$$oldibannumber$$", string.Empty);
                    }
                    #endregion
                    #region Swift Code
                    if (oldTranslationGrantMember.SwiftCode != translationGrantMember.SwiftCode)
                    {
                        if (translationGrantMember.SwiftCode != null)
                        {
                            body.Replace("$$newswiftcode$$", strContent);
                            body.Replace("$$Label$$", "Swift Code");
                            body.Replace("$$Value$$", translationGrantMember.SwiftCode);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newswiftcode$$", string.Empty);
                        if (oldTranslationGrantMember.SwiftCode != null)
                        {
                            body.Replace("$$oldswiftcode$$", strContent);
                            body.Replace("$$Label$$", "Swift Code");
                            body.Replace("$$Value$$", oldTranslationGrantMember.SwiftCode);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldswiftcode$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newswiftcode$$", string.Empty);
                        body.Replace("$$oldswiftcode$$", string.Empty);
                    }
                    #endregion
                    #region Upload Soft copy of Translated Book (pdf,doc,docx)
                    if (oldTranslationGrantMember.Draft1 != translationGrantMember.Draft1)
                    {
                        if (translationGrantMember.Draft1 != null)
                        {
                            body.Replace("$$newuploadsoftcopyoftranslatedbook$$", strContent);
                            body.Replace("$$Label$$", "Soft copy of Translated Book");
                            body.Replace("$$Value$$", translationGrantMember.Draft1);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newuploadsoftcopyoftranslatedbook$$", string.Empty);
                        if (oldTranslationGrantMember.Draft1 != null)
                        {
                            body.Replace("$$olduploadsoftcopyoftranslatedbook$$", strContent);
                            body.Replace("$$Label$$", "Soft copy of Translated Book");
                            body.Replace("$$Value$$", oldTranslationGrantMember.Draft1);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$olduploadsoftcopyoftranslatedbook$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newuploadsoftcopyoftranslatedbook$$", string.Empty);
                        body.Replace("$$olduploadsoftcopyoftranslatedbook$$", string.Empty);
                    }
                    #endregion

                    #region Status
                    if (oldTranslationGrantMember.Status != translationGrantMember.Status)
                    {
                        if (translationGrantMember.Status != null)
                        {
                            body.Replace("$$newstatus$$", strContent);
                            body.Replace("$$Label$$", "Status");
                            body.Replace("$$Value$$", MethodFactory.GetTranslationGrantMemberStatus(translationGrantMember.Status, LangId));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$newstatus$$", string.Empty);
                        if (oldTranslationGrantMember.Status != null)
                        {
                            body.Replace("$$oldstatus$$", strContent);
                            body.Replace("$$Label$$", "Soft copy of Translated Book");
                            body.Replace("$$Value$$", MethodFactory.GetTranslationGrantMemberStatus(oldTranslationGrantMember.Status, LangId));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$oldstatus$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$newstatus$$", string.Empty);
                        body.Replace("$$oldstatus$$", string.Empty);
                    }
                    #endregion
                    #endregion
                    if (IsAnyFieldChanged)
                        if (strEmailContentEmail != string.Empty)
                            _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString());
                    //SmtpEmail.SendNetEmail(strEmailContentAdmin, AdminEmailAddress, strEmailContentEmail, strEmailContentSubject, body.ToString());
                    #endregion
                }
            }
        }
        void SendEmailsForUserCancellation(XsiExhibitionTranslationGrantMembers entity, long langId, long memberId)
        {
            string strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
            if (entity.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.CancelledByUser))
            {
                #region Send Cancellation Email
                StringBuilder body = new StringBuilder();
                #region Notify Admin About User Cancellation Email Content
                EmailContentService EmailContentService = new EmailContentService();
                XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(20057);
                if (emailContent != null)
                {
                    if (emailContent.Email != null && !string.IsNullOrEmpty(emailContent.Email))
                        strEmailContentEmail = emailContent.Email;
                    else
                        strEmailContentEmail = _appCustomSettings.SIBFEmail;
                    if (emailContent.Subject != null)
                        strEmailContentSubject = emailContent.Subject;
                    //strEmailContentAdmin = AdminName;
                }
                #endregion
                body.Append(MethodFactory.BindEmailContentTG(langId, emailContent.Body, _appCustomSettings.ServerAddressNew, _appCustomSettings));
                body.Replace("$$BuyerName$$", entity.BuyerName);
                body.Replace("$$Name$$", strEmailContentAdmin);
                body.Replace("$$BookName$$", entity.BookTitleEnglish);
                _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString());
                #endregion
            }
            else if (entity.Status == EnumConversion.ToString(EnumTranslationGrantMemberStatus.UserCancellationPending))
            {
                #region Send Cancellation Email
                StringBuilder body = new StringBuilder();
                #region Notify Admin About User Cancellation Email Content
                EmailContentService EmailContentService = new EmailContentService();
                XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(20057);
                if (emailContent != null)
                {
                    if (emailContent.Email != null && !string.IsNullOrEmpty(emailContent.Email))
                        strEmailContentEmail = emailContent.Email;
                    else
                        strEmailContentEmail = _appCustomSettings.SIBFEmail;
                    if (emailContent.Subject != null)
                        strEmailContentSubject = emailContent.Subject;
                    strEmailContentAdmin = _appCustomSettings.AdminName;
                }
                body.Append(MethodFactory.BindEmailContentTG(langId, emailContent.Body, _appCustomSettings.ServerAddressNew, _appCustomSettings));
                body.Replace("$$BuyerName$$", entity.BuyerName);
                body.Replace("$$BookName$$", entity.BookTitleEnglish);
                body.Replace("$$Name$$", strEmailContentAdmin);
                _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString());
                //SmtpEmail.SendNetEmail(strEmailContentAdmin, AdminEmailAddress, strEmailContentEmail, strEmailContentSubject, body.ToString());
                #endregion
                #region Notify User About Cancellation Pending Due To Money Refund - Email Content
                EmailContentService = new EmailContentService();
                emailContent = EmailContentService.GetEmailContentByItemId(20055);
                body = new StringBuilder();
                if (emailContent != null)
                {
                    ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService();
                    XsiExhibitionMember memeberentity = new XsiExhibitionMember();
                    memeberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(memberId);

                    if (emailContent.Email != null)
                        strEmailContentEmail = memeberentity.Email;
                    if (emailContent.Subject != null)
                        strEmailContentSubject = emailContent.Subject;
                    strEmailContentAdmin = _appCustomSettings.AdminName;
                }
                body.Append(MethodFactory.BindEmailContentTG(langId, emailContent.Body, _appCustomSettings.ServerAddressNew, _appCustomSettings));
                body.Replace("$$BuyerName$$", entity.BuyerName);
                body.Replace("$$BookName$$", entity.BookTitleEnglish);
                _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString());
                //SmtpEmail.SendNetEmail(strEmailContentAdmin, AdminEmailAddress, strEmailContentEmail, strEmailContentSubject, body.ToString());
                #endregion
                #endregion
            }
        }
    }
    #endregion
}
public class TGValidationDTO
{
    public long MemberId { get; set; }
    public long TranslationgrantId { get; set; }
    public long TranslationgrrantMemberId { get; set; }
    public long ExhibitionId { get; set; }
    public long WebsiteId { get; set; }
    public bool IsDateExpired { get; set; } = true;
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Xsi.ServicesLayer;
using System.IO;
using OfficeOpenXml;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BooksParticipatingController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        #region Properties
        protected long ExhibitionId { get; set; }
        public long ExhibitorId { get; set; }
        public long AgencyId { get; set; }
        #endregion

        public BooksParticipatingController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
        }

        #region Get Methods
        [HttpGet]
        [Route("{websiteid}")]
        public async Task<ActionResult<dynamic>> GetBooksParticipating(long websiteid = 1)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long langId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out langId);

                    BooksParticipatingDTO model = new BooksParticipatingDTO();
                    long memberid = User.Identity.GetID();
                    model.MemberId = memberid;
                    #region Get Current Year Active ExhibitionGroupId
                    XsiExhibition entity = MethodFactory.GetActiveExhibition(websiteid, _context);
                    if (entity != null)
                        ExhibitionId = entity.ExhibitionId;
                    #endregion
                    //if (ExhibitionId > 0)
                    //GetAgencyByMember(memberid);
                    //if (AgencyId == -1)
                    ExhibitorId = MethodFactory.GetExhibitorByMember(memberid, websiteid, _context);

                    if (ExhibitorId <= 0)
                        return Ok("/en/MemberDashboard/" + websiteid);
                    GetBooksParticipatingList(model);
                    BindListings(model, langId);
                    return Ok(model);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooksParticipating action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");

            }
        }

        [HttpGet]
        [Route("BookDetails/{bookid}/{exhibitorid}")]
        public ActionResult<dynamic> GetBookDetails(long bookid, long exhibitorid)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    var book = new BooksDetailsDTO();
                    long langId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out langId);
                    var entity = _context.XsiExhibitionBooks.Where(x => x.BookId == bookid).FirstOrDefault();
                    if (langId == 1)
                    {
                        if (entity != null)
                        {
                            book.BookId = entity.BookId;
                            book.Title = entity.TitleEn;

                            //book.AuthorName = GetAutorName(entity.BookId, langId);

                            book.AuthorName = entity.AuthorName;
                            book.PublisherName = entity.BookPublisherName; // GetPublisherName(entity.BookPublisherId.Value, langId);

                            book.Price = entity.Price;
                            if (entity.ExhibitionSubjectId != null)
                                book.Subject = GetSubject(entity.ExhibitionSubjectId.Value, langId);
                            if (entity.ExhibitionSubsubjectId != null)
                                book.SubSubject = GetSubSubject(entity.ExhibitionSubsubjectId.Value, langId);

                            book.Thumbnail = _appCustomSettings.UploadsCMSPath + "/Books/Thumbnails/" + (string.IsNullOrEmpty(entity.Thumbnail) ? "default-book_en.jpg" : entity.Thumbnail);

                            var booksparticpatingEntity = _context.XsiExhibitionBookParticipating.Where(i => i.BookId == bookid && i.ExhibitorId == exhibitorid).FirstOrDefault();
                            if (booksparticpatingEntity != null)
                            {
                                var Exhibitor = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == exhibitorid).FirstOrDefault();
                                if (Exhibitor != null)
                                {
                                    var Country = _context.XsiExhibitionCountry.Where(i => i.CountryId == Exhibitor.CountryId).FirstOrDefault();
                                    if (Country != null)
                                        book.Country = Country.CountryName;

                                    var details = _context.XsiExhibitionExhibitorDetails.Where(i => i.MemberExhibitionYearlyId == exhibitorid).FirstOrDefault();
                                    if (details != null)
                                    {
                                        book.HallNumber = details.HallNumber;

                                        if (details.BoothSectionId != null)
                                            book.BoothSection = GetBoothSection(details.BoothSectionId.Value, langId);

                                        if (details.BoothSubSectionId != null)
                                            book.BoothSubSection = GetBoothSubSection(details.BoothSubSectionId.Value, langId);

                                        book.StandNumber = details.StandNumberStart + details.StandNumberEnd;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (entity != null)
                        {
                            book.BookId = entity.BookId;
                            book.Title = entity.TitleAr ?? entity.TitleEn;

                            //book.AuthorName = GetAutorName(entity.BookId, langId);
                            //if (entity.BookPublisherId != null)
                            //    book.PublisherName = GetPublisherName(entity.BookPublisherId.Value, langId);

                            book.AuthorName = entity.AuthorNameAr;
                            book.PublisherName = entity.BookPublisherNameAr;

                            book.Price = entity.Price;
                            if (entity.ExhibitionSubjectId != null)
                                book.Subject = GetSubject(entity.ExhibitionSubjectId.Value, langId);
                            if (entity.ExhibitionSubsubjectId != null)
                                book.SubSubject = GetSubSubject(entity.ExhibitionSubsubjectId.Value, langId);
                            book.Thumbnail = _appCustomSettings.UploadsCMSPath + "/Books/Thumbnails/" + (string.IsNullOrEmpty(entity.Thumbnail) ? "default-book_ar.jpg" : entity.Thumbnail);

                            var booksparticpatingEntity = _context.XsiExhibitionBookParticipating.Where(i => i.BookId == bookid && i.ExhibitorId == exhibitorid).FirstOrDefault();
                            if (booksparticpatingEntity != null)
                            {
                                var Exhibitor = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == exhibitorid).FirstOrDefault();
                                if (Exhibitor != null)
                                {
                                    var Country = _context.XsiExhibitionCountry.Where(i => i.CountryId == Exhibitor.CountryId).FirstOrDefault();
                                    if (Country != null)
                                        book.Country = Country.CountryNameAr;

                                    var details = _context.XsiExhibitionExhibitorDetails.Where(i => i.MemberExhibitionYearlyId == exhibitorid).FirstOrDefault();
                                    if (details != null)
                                    {
                                        book.HallNumber = details.HallNumber;

                                        if (details.BoothSectionId != null)
                                            book.BoothSection = GetBoothSection(details.BoothSectionId.Value, langId);

                                        if (details.BoothSubSectionId != null)
                                            book.BoothSubSection = GetBoothSubSection(details.BoothSubSectionId.Value, langId);

                                        book.StandNumber = details.StandNumberStart + details.StandNumberEnd;
                                    }
                                }
                            }
                        }
                    }
                    return Ok(book);
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }
        #endregion

        #region Post Methods
        [HttpPost]
        [Route("inventory")]
        public async Task<ActionResult<dynamic>> GetInventory(BookSearch search)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long langId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out langId);

                    long memberid = User.Identity.GetID();

                    #region Get Current Year Active ExhibitionGroupId
                    XsiExhibition entity = MethodFactory.GetActiveExhibition(search.Websiteid, _context);
                    if (entity != null)
                        ExhibitionId = entity.ExhibitionId;
                    #endregion

                    ExhibitorId = MethodFactory.GetExhibitorByMember(memberid, search.Websiteid, _context);

                    if (ExhibitorId <= 0)
                    {
                        var dto = new { list = new List<BooksInventoryDTO>(), TotalCount = 0, MessageTypeResponse = "Error", Message = "Exhibitor Cannot access this page" };
                        return Ok(dto);
                    }
                    else
                    {
                        if (search.Simple && search.Type == "inventory")
                        {
                            #region Inventory
                            BooksToParticipateTabDTO obj = new BooksToParticipateTabDTO();
                            obj = GetSelectBooksToParticipateTab(search, _context, memberid, langId);
                            #endregion
                            var dto = new { list = obj.list, TotalCount = obj.count, MessageTypeResponse = "Success" };
                            return Ok(dto);
                        }
                        else
                        {
                            BooksInventoryList obj = new BooksInventoryList();
                            obj = GetInventoryOrIncompleteBooks(search, _context, langId, memberid);
                            if (search.IsList == false)
                            {
                                return Ok(obj.Downloadurl);
                            }
                            else
                            {
                                var dto = new { list = obj.List, TotalCount = obj.Count, MessageTypeResponse = "Success" };
                                return Ok(dto);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpPost]
        [Route("currentbooks")]
        public async Task<ActionResult<dynamic>> GetCurrentBooks(BookSearch search)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long langid = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out langid);

                    long memberid = User.Identity.GetID();
                    #region Get Current Year Active ExhibitionGroupId
                    XsiExhibition entity = MethodFactory.GetActiveExhibition(search.Websiteid, _context);
                    if (entity != null)
                        ExhibitionId = entity.ExhibitionId;
                    #endregion
                    //if (ExhibitionId > 0)
                    //GetAgencyByMember(memberid);
                    //if (AgencyId == -1)
                    ExhibitorId = MethodFactory.GetExhibitorByMember(memberid, search.Websiteid, _context);

                    if (ExhibitorId <= 0)
                        return Ok("/en/MemberDashboard/" + search.Websiteid);
                    else
                    {
                        if (search.IsList)
                        {
                            if (ExhibitionId != -1)
                            {
                                #region Books Participating
                                BooksInventoryList obj = GetBooksParticipatingListNew(search, _context, langid, memberid);
                                var dto = new { list = obj.List, TotalCount = obj.Count };
                                return Ok(dto);
                                #endregion
                            }
                            else
                            {
                                return Ok("No Books Available");
                            }
                        }
                        else
                        {
                            var downloadUrl = ExportBooksToExcelForBooksParticipating(langid, ExhibitorId);
                            return Ok(downloadUrl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");

            }
        }

        [HttpPost]
        [Route("addbooksparticipating")]
        public async Task<ActionResult<dynamic>> AddBooksParticipating(AddBooksDTO model)
        {
            try
            {
                long langid = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langid);

                long bookcount = 0;
                long memberid = User.Identity.GetID();
                if (memberid > 0)
                {
                    using (BooksParticipatingService BooksParticipatingService = new BooksParticipatingService())
                    {
                        ExhibitionBookService ExhibitionBookService = new ExhibitionBookService();
                        foreach (var item in model.BookIds)
                        {
                            ExhibitionBookService = new ExhibitionBookService();
                            XsiExhibitionBookParticipating whereQuery = new XsiExhibitionBookParticipating();
                            whereQuery.BookId = item;
                            whereQuery.ExhibitorId = model.ExhibitorId;
                            if (model.AgencyId > 0)
                                whereQuery.AgencyId = model.AgencyId;
                            XsiExhibitionBookParticipating entity = BooksParticipatingService.GetBooksParticipating(whereQuery).FirstOrDefault();
                            if (default(XsiExhibitionBookParticipating) != entity)
                            {
                                entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                entity.ModifiedBy = memberid;
                                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                                if (BooksParticipatingService.UpdateBooksParticipating(entity) == EnumResultType.Success)
                                    bookcount = bookcount + 1;
                                //  return Ok("Success");
                            }
                            else
                            {
                                XsiExhibitionBookParticipating newBP = new XsiExhibitionBookParticipating();
                                newBP.ExhibitorId = model.ExhibitorId;
                                newBP.BookId = item;
                                if (model.AgencyId > 0)
                                    newBP.AgencyId = model.AgencyId;
                                var prevBP = BooksParticipatingService.GetBooksParticipating(new XsiExhibitionBookParticipating() { BookId = item }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                                if (prevBP != null)
                                {
                                    newBP.Price = prevBP.Price;
                                    newBP.PriceBeforeDiscount = prevBP.PriceBeforeDiscount;
                                    newBP.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                }
                                else
                                {
                                    var book = ExhibitionBookService.GetBookByItemId(item);
                                    if (book != null)
                                        newBP.Price = book.Price;
                                    newBP.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                }

                                newBP.CreatedBy = memberid;
                                newBP.CreatedOn = MethodFactory.ArabianTimeNow();
                                newBP.ModifiedBy = memberid;
                                newBP.ModifiedOn = MethodFactory.ArabianTimeNow();
                                if (BooksParticipatingService.InsertBooksParticipating(newBP) == EnumResultType.Success)
                                    bookcount = bookcount + 1;

                            }
                            // return Ok("Fail");
                        }
                        if (langid == 1)
                        {
                            if (bookcount == model.BookIds.Count)
                                return Ok(new MessageDTO() { Message = bookcount + " books added to participating.", MessageTypeResponse = "Success" });
                            else
                                return Ok(new MessageDTO() { Message = bookcount + " books added to participating.", MessageTypeResponse = "Error" });
                        }
                        else
                        {
                            if (bookcount == model.BookIds.Count)
                                return Ok(new MessageDTO() { Message = bookcount + " تمت إضافة الكتب للكتب المشاركة", MessageTypeResponse = "Success" });
                            else
                                return Ok(new MessageDTO() { Message = bookcount + " تمت إضافة الكتب للكتب المشاركة", MessageTypeResponse = "Error" });
                        }
                    }
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok(new MessageDTO() { Message = "Something went wrong. Please try again later.", MessageTypeResponse = "Error" });
            }
        }

        [HttpPost]
        [Route("removebooksparticipating")]
        public async Task<ActionResult<dynamic>> RemoveBooksParticipating(AddBooksDTO model)
        {
            try
            {
                long bookcount = 0;
                long memberid = User.Identity.GetID();
                if (memberid > 0)
                {
                    using (BooksParticipatingService BooksParticipatingService = new BooksParticipatingService())
                    {
                        foreach (var item in model.BookIds)
                        {
                            XsiExhibitionBookParticipating whereQuery = new XsiExhibitionBookParticipating();
                            whereQuery.BookId = item;
                            whereQuery.ExhibitorId = model.ExhibitorId;
                            if (model.AgencyId > 0)
                                whereQuery.AgencyId = model.AgencyId;
                            XsiExhibitionBookParticipating entity = BooksParticipatingService.GetBooksParticipating(whereQuery).FirstOrDefault();
                            if (default(XsiExhibitionBookParticipating) != entity)
                            {
                                entity.IsActive = EnumConversion.ToString(EnumBool.No);
                                entity.ModifiedBy = memberid;
                                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                                BooksParticipatingService.UpdateBooksParticipating(entity);
                                bookcount = bookcount + 1;
                            }
                        }
                        if (bookcount == model.BookIds.Count)
                            return Ok(new MessageDTO() { Message = bookcount + " books removed from participating.", MessageTypeResponse = "Success" });
                        else
                            return Ok(new MessageDTO() { Message = bookcount + " books removed from participating.", MessageTypeResponse = "Error" });
                    }
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok(new MessageDTO() { Message = "Something went wrong. Please try again later.", MessageTypeResponse = "Error" });
            }
        }

        [HttpPost]
        [Route("inventory/export")]
        public async Task<ActionResult<dynamic>> GetInventoryExport(BookSearch search)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    BooksParticipatingDTO model = new BooksParticipatingDTO();
                    long memberid = User.Identity.GetID();
                    #region Get Current Year Active ExhibitionId
                    XsiExhibition entity = MethodFactory.GetActiveExhibition(search.Websiteid, _context);
                    if (entity != null)
                        ExhibitionId = entity.ExhibitionId;
                    #endregion
                    long langId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out langId);
                    ExhibitorId = MethodFactory.GetExhibitorByMember(memberid, search.Websiteid, _context);

                    if (ExhibitorId <= 0)
                        return Ok("/en/MemberDashboard/" + search.Websiteid);
                    else
                    {
                        var downloadUrl = ExportBooksToExcel(memberid, search, langId);
                        if (!string.IsNullOrEmpty(downloadUrl))
                        {
                            return Ok(downloadUrl);
                        }
                        else
                        {
                            return Ok("No Books Available");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");

            }
        }

        [HttpPost]
        [Route("currentbooks/export")]
        public async Task<ActionResult<dynamic>> GetCurrentBooksExport(BookSearch search)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long memberid = User.Identity.GetID();
                    #region Get Current Year Active ExhibitionGroupId
                    XsiExhibition entity = MethodFactory.GetActiveExhibition(search.Websiteid, _context);
                    if (entity != null)
                        ExhibitionId = entity.ExhibitionId;
                    #endregion
                    //if (ExhibitionId > 0)
                    //GetAgencyByMember(memberid);
                    //if (AgencyId == -1)
                    ExhibitorId = MethodFactory.GetExhibitorByMember(memberid, search.Websiteid, _context);

                    if (ExhibitorId <= 0)
                        return Ok("/en/MemberDashboard/" + search.Websiteid);
                    else
                    {
                        if (ExhibitionId != -1)
                        {
                            long langId = 1;
                            long.TryParse(Request.Headers["LanguageURL"], out langId);
                            var downloadUrl = ExportBooksToExcel(memberid, search, langId, ExhibitorId);
                            if (!string.IsNullOrEmpty(downloadUrl))
                            {
                                return Ok(downloadUrl);
                            }
                            return Ok("No Books Available");
                        }
                        else
                        {
                            return Ok("No Books Available");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");

            }
        }
        #endregion

        #region Books To Participate (Tab 1)
        private BooksToParticipateTabDTO GetSelectBooksToParticipateTab(BookSearch search, sibfnewdbContext _context, long memberid, long langId)
        {
            BooksToParticipateTabDTO obj = new BooksToParticipateTabDTO();
            var predicate = PredicateBuilder.New<XsiExhibitionBooks>();
            predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicate = predicate.And(i => i.MemberId == memberid);
            predicate = predicate.And(i => i.IsEnable == EnumConversion.ToString(EnumBool.Yes));

            var predicateBP = PredicateBuilder.New<XsiExhibitionBookParticipating>();
            predicateBP = predicateBP.And(p => p.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicateBP = predicateBP.And(p => p.ExhibitorId == ExhibitorId);

            var participatingbooksIds = _context.XsiExhibitionBookParticipating.Where(predicateBP).Select(i => i.BookId).ToList();
            if (participatingbooksIds.Count() > 0)
                predicate = predicate.And(i => !participatingbooksIds.Contains(i.BookId));

            var BookList = _context.XsiExhibitionBooks.Where(predicate).ToList();
            if (!string.IsNullOrEmpty(search.Search))
            {
                BookList = BookList.Where(i => (i.TitleEn != null && i.TitleEn.ToLower().Contains(search.Search.ToLower())) || (i.TitleAr != null && i.TitleAr.ToLower().Contains(search.Search.ToLower()))).ToList();
            }
            BookList = BookList.OrderBy(x => x.TitleEn).ToList();
            var totalCount = BookList.Count;
            if (totalCount > 0)
            {
                List<LeftRightListDTO> list = new List<LeftRightListDTO>();
                if (langId == 1)
                {
                    list = BookList.Select(x => new LeftRightListDTO
                    {
                        key = x.BookId,
                        title = x.TitleEn
                    }).ToList();
                }
                else
                {
                    list = BookList.Select(x => new LeftRightListDTO
                    {
                        key = x.BookId,
                        title = x.TitleAr ?? x.TitleEn
                    }).ToList();
                }
                obj.list = list;
                obj.count = totalCount;
            }
            else
            {
                obj.list = new List<LeftRightListDTO>();
                obj.count = 0;
            }
            return obj;
        }
        #endregion
        #region Participating Books (Tab 3)
        private BooksInventoryList GetBooksParticipatingListNew(BookSearch search, sibfnewdbContext _context, long langid, long memberid)
        {
            BooksInventoryList obj = new BooksInventoryList();
            #region Queries
            var bppredicate = PredicateBuilder.New<XsiExhibitionBookParticipating>();
            bppredicate = bppredicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            bppredicate = bppredicate.And(i => i.ExhibitorId == ExhibitorId);

            var predicate1 = PredicateBuilder.New<XsiExhibitionBooks>();
            predicate1 = predicate1.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicate1 = predicate1.And(i => i.IsEnable == EnumConversion.ToString(EnumBool.Yes));
            predicate1 = predicate1.And(i => i.MemberId == memberid);

            var bookParticipating = _context.XsiExhibitionBookParticipating.Where(bppredicate).ToList();
            var books = _context.XsiExhibitionBooks.Where(predicate1).ToList();
            if (!string.IsNullOrEmpty(search.Search))
            {
                books = books.Where(i =>
                        (i.TitleEn != null && i.TitleEn.ToLower().Contains(search.Search.ToLower())) ||
                        (i.TitleAr != null && i.TitleAr.ToLower().Contains(search.Search.ToLower())))
                    .ToList();
            }

            books = books.OrderByDescending(x => x.BookId).ToList();
            #endregion

            var list = (from bp in bookParticipating
                        join b in books on bp.BookId equals b.BookId
                        // join ba in _context.XsiExhibitionBookAuthor on b.BookId equals ba.BookId
                        select new BooksInventoryDTO
                        {
                            BookId = bp.BookId,
                            Title = b.TitleEn,
                            Price = bp.Price,
                            BestSeller = b.IsBestSeller,
                            Thumbnail = _appCustomSettings.UploadsCMSPath + "/Books/Thumbnails/" +
                                        (string.IsNullOrEmpty(b.Thumbnail)
                                            ? "default-book_en.jpg"
                                            : b.Thumbnail),
                            Author = langid == 1 ? b.AuthorName : b.AuthorNameAr // GetAutorName(b.BookId, LangId)
                        }).Skip((search.PageNumber - 1) * search.PageSize).Take(search.PageSize).ToList();

            obj.Count = (from bp in bookParticipating
                         join b in books on bp.BookId equals b.BookId
                         // join ba in _context.XsiExhibitionBookAuthor on b.BookId equals ba.BookId
                         select new BooksInventoryDTO
                         {
                             BookId = bp.BookId,
                             Title = b.TitleEn,
                             Price = bp.Price,
                             BestSeller = b.IsBestSeller,
                             Thumbnail = _appCustomSettings.UploadsCMSPath + "/Books/Thumbnails/" +
                                         (string.IsNullOrEmpty(b.Thumbnail)
                                             ? "default-book_en.jpg"
                                             : b.Thumbnail),
                             Author = langid == 1 ? b.AuthorName : b.AuthorNameAr // GetAutorName(b.BookId, LangId)
                         }).Count();
            obj.List = list;
            return obj;
        }
        #endregion
        #region Inventory and Incomplete Books (Tab2 and Tab 4)
        private BooksInventoryList GetInventoryOrIncompleteBooks(BookSearch search, sibfnewdbContext _context, long langid, long memberid)
        {
            BooksInventoryList obj = new BooksInventoryList();
            #region Inventory
            var predicate = PredicateBuilder.New<XsiExhibitionBooks>();
            predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicate = predicate.And(i => i.MemberId == memberid);

            if (search.IsInventory)
            {
                // My Inventory Tab
                predicate = predicate.And(i => i.IsEnable == EnumConversion.ToString(EnumBool.Yes));
            }
            else
            {
                //For Incomplete Books Tab
                predicate = predicate.And(i => i.IsEnable == null || string.IsNullOrEmpty(i.IsEnable) || i.IsEnable == EnumConversion.ToString(EnumBool.No));
            }

            var BookList = _context.XsiExhibitionBooks.Where(predicate).ToList();
            if (!string.IsNullOrEmpty(search.Search))
            {
                BookList = BookList.Where(i => (i.TitleEn != null && i.TitleEn.ToLower().Contains(search.Search.ToLower())) || (i.TitleAr != null && i.TitleAr.ToLower().Contains(search.Search.ToLower()))).ToList();
            }
            BookList = BookList.OrderByDescending(x => x.BookId).ToList();
            var totalCount = BookList.Count;
            if (totalCount > 0)
            {
                if (search.IsList == false)
                {
                    var bookIds = BookList.Select(i => i.BookId).ToList();
                    if (bookIds.Count > 0)
                    {
                        obj.Downloadurl = ExportBooksToExcelForTabs(bookIds, langid, _context);
                        return obj;
                    }
                }
                else
                {
                    List<BooksInventoryDTO> list = BookList.Select(x => new BooksInventoryDTO
                    {
                        BookId = x.BookId,
                        Title = x.TitleEn,
                        Price = x.Price,
                        BestSeller = x.IsBestSeller,
                        Thumbnail = _appCustomSettings.UploadsCMSPath + "/Books/Thumbnails/" + (string.IsNullOrEmpty(x.Thumbnail) ? "default-book_en.jpg" : x.Thumbnail),
                        Author = langid == 1 ? x.AuthorName : x.AuthorNameAr // GetAutorName(x.BookId, langId)
                    }).ToList();
                    list = list.Skip((search.PageNumber - 1) * search.PageSize).Take(search.PageSize).ToList();
                    obj.List = list;
                    obj.Count = totalCount;
                    return obj;
                }
            }
            obj.List = new List<BooksInventoryDTO>();
            obj.Count = 0;
            return obj;
            #endregion
        }
        #endregion

        #region Private Methods
        private void BindListings(BooksParticipatingDTO model, long languageid)
        {
            if (ExhibitionId != -1)
            {
                model.LeftList = new List<LeftRightListDTO>();
                var BookList = GetBookList(languageid);
                if (BookList.Count > 0)
                {
                    //HashSet<long> PBListBookIds = new HashSet<long>(model.RightList.Select(p => p.key).Distinct().ToList());
                    BookList = BookList.OrderByDescending(x => x.key).ToList();

                    //if (BookList.Count() > 0)
                    //{
                    //    List<LeftRightListDTO> BooksNoParticipateList = BookList.ToList();
                    //    if (BooksNoParticipateList.Count > 0)
                    //        model.LeftList = BooksNoParticipateList;
                    //    else
                    //        model.InventoryMessage = "No Books Available";
                    //}
                    //else
                    //{
                    //    model.LeftList = BookList;
                    //}
                    model.LeftList = BookList;
                }
                else
                    model.InventoryMessage = "No Books Available";
            }
        }
        private void GetBooksParticipatingList(BooksParticipatingDTO model)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (ExhibitionId != -1)
                {
                    #region Books Participating
                    #region Queries

                    long memberid = model.MemberId;
                    var predicate = PredicateBuilder.New<XsiExhibitionBookParticipating>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.ExhibitorId == ExhibitorId);

                    var predicate1 = PredicateBuilder.New<XsiExhibitionBooks>();
                    predicate1 = predicate1.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate1 = predicate1.And(i => i.IsEnable == EnumConversion.ToString(EnumBool.Yes));
                    predicate1 = predicate1.And(i => i.MemberId == memberid);

                    var bookParticipating = _context.XsiExhibitionBookParticipating.Where(predicate).ToList();
                    var books = _context.XsiExhibitionBooks.Where(predicate1).ToList();
                    #endregion
                    var list = (from bp in bookParticipating
                                join b in books on bp.BookId equals b.BookId
                                select bp).ToList();

                    if (list.Count > 0)
                    {
                        model.ParticipateCount = list.Count + " books added";
                        model.RightList = list.Select(x => x.BookId).ToList();
                    }
                    else
                    {
                        model.ParticipateMessage = "No Books Available";
                        model.RightList = new List<long>();
                    }
                    #endregion
                }
            }
        }
        private List<LeftRightListDTO> GetBookList(long langid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long memberid = User.Identity.GetID();
                var predicate = PredicateBuilder.New<XsiExhibitionBooks>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.IsEnable == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.MemberId == memberid);
                var BookList = _context.XsiExhibitionBooks.Where(predicate).OrderByDescending(i => i.BookId).ToList();

                if (langid == 1)
                {
                    var list = (from b in BookList
                                select new LeftRightListDTO
                                {
                                    key = b.BookId,
                                    title = b.TitleEn ?? b.TitleAr
                                }).ToList();
                    return list;
                }
                else
                {
                    var list = (from b in BookList
                                select new LeftRightListDTO
                                {
                                    key = b.BookId,
                                    title = b.TitleAr ?? b.TitleEn
                                }).ToList();
                    return list;
                }
            }
        }

        private string GetBoothSection(long itemid, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var entity = _context.XsiExhibitionBooth.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
                if (entity != null)
                    return langId == 1 ? entity.Title : entity.TitleAr;

                return string.Empty;
            }
        }
        private string GetSubject(long itemid, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var entity = _context.XsiExhibitionBookSubject.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
                if (entity != null)
                    return langId == 1 ? entity.Title : entity.TitleAr;

                return string.Empty;
            }
        }
        private string GetSubSubject(long itemid, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var entity = _context.XsiExhibitionBookSubsubject.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
                if (entity != null)
                    return langId == 1 ? entity.Title : entity.TitleAr;

                return string.Empty;
            }
        }
        private string GetBoothSubSection(long itemid, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var entity = _context.XsiExhibitionBoothSubsection.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
                if (entity != null)
                    return langId == 1 ? entity.Title : entity.TitleAr;

                return string.Empty;
            }
        }

        private string ExportBooksToExcelForBooksParticipating(long langId, long exhibitorid = -1)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string downloadUrl = string.Empty;
                List<GetBooksToExport_Result> list = new List<GetBooksToExport_Result>();
                var bppredicate = PredicateBuilder.New<XsiExhibitionBookParticipating>();
                if (exhibitorid > 0)
                {
                    bppredicate = bppredicate.And(i => i.ExhibitorId == exhibitorid);
                    bppredicate = bppredicate.And(i => i.IsActive == "Y");
                    list = _context.XsiExhibitionBookParticipating.Where(bppredicate).Select(
                       bp => new GetBooksToExport_Result
                       {
                           TitleAr = bp.Book.TitleAr,
                           TitleEn = bp.Book.TitleEn,
                           AuthorAr = bp.Book.AuthorNameAr, //GetAuthorName(bp.BookId, 2),
                           AuthorEn = bp.Book.AuthorName, //GetAuthorName(bp.BookId, 1),
                           BookDetails = bp.Book.BookDetails,
                           BookDetailsAr = bp.Book.BookDetailsAr,
                           Boothsection = GetExhibitionBoothSection(bp.Exhibitor.MemberExhibitionYearlyId, langId),
                           Boothsubsection = GetExhibitionBoothSubSection(bp.Exhibitor.MemberExhibitionYearlyId, langId),
                           ExhibitionLanguage = bp.Book.BookLanguage.Title,
                           ExhibitorAr = bp.Exhibitor.PublisherNameAr,
                           ExhibitorEn = bp.Exhibitor.PublisherNameEn,
                           hallnumber = bp.Exhibitor.XsiExhibitionExhibitorDetails.ToList()[0].HallNumber,
                           ISBN = bp.Book.Isbn,
                           IssueYear = bp.Book.IssueYear,
                           Price = bp.Book.Price,
                           PublisherAr = bp.Book.BookPublisherNameAr,
                           PublisherEn = bp.Book.BookPublisherName,
                           Standnumberend = bp.Exhibitor.XsiExhibitionExhibitorDetails.ToList()[0].StandNumberEnd,
                           Standnumberstart = bp.Exhibitor.XsiExhibitionExhibitorDetails.ToList()[0].StandNumberStart,
                           Subject = bp.Book.ExhibitionSubject.Title,
                           Subsubject = bp.Book.ExhibitionSubsubject.Title,
                       }).ToList();

                    string folder = _appCustomSettings.UploadsPhysicalPath + "\\BooksExport\\";
                    string excelName = "BooksList" + Guid.NewGuid() + ".xlsx";
                    //string downloadUrl = string.Format("{0}://{1}/{2}/{3}", Request.Scheme, Request.Host, "Content/Uploads/BooksExport", excelName);

                    downloadUrl = _appCustomSettings.UploadsCMSPath + "/BooksExport/" + excelName;
                    FileInfo file = new FileInfo(Path.Combine(folder, excelName));
                    if (!Directory.Exists(folder))
                    {
                        System.IO.Directory.CreateDirectory(folder);
                    }
                    if (file.Exists)
                    {
                        //file.Delete();
                        file = new FileInfo(Path.Combine(folder, excelName));
                    }
                    using (var package = new ExcelPackage(file))
                    {
                        var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                        workSheet.Cells.LoadFromCollection(list, true);
                        package.Save();
                    }
                }
                return downloadUrl;
            }
        }
        private string ExportBooksToExcel(long memberid, BookSearch search, long langId, long exhibitorid = -1)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                List<GetBooksToExport_Result> list = new List<GetBooksToExport_Result>();
                var predicate = PredicateBuilder.New<XsiExhibitionBooks>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.IsEnable == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.MemberId == memberid);
                var BookList = _context.XsiExhibitionBooks.Where(predicate).ToList();
                BookList = BookList.OrderBy(x => x.TitleEn).ToList();
                if (!string.IsNullOrEmpty(search.Search))
                {
                    BookList = BookList.Where(i => i.TitleEn.ToLower().Contains(search.Search.ToLower()) || i.TitleAr.ToLower().Contains(search.Search.ToLower())).ToList();
                }
                BookList = BookList.OrderBy(x => x.TitleEn).ToList();
                List<long> bookIds = BookList.Select(x => x.BookId).ToList();

                predicate = predicate.And(i => bookIds.Contains(i.BookId));

                var bppredicate = PredicateBuilder.New<XsiExhibitionBookParticipating>();

                if (exhibitorid > 0)
                {
                    bppredicate = bppredicate.And(i => i.ExhibitorId == exhibitorid);
                }
                else
                {
                    bppredicate = bppredicate.And(i => i.ExhibitorId > 0);
                }
                list = _context.XsiExhibitionBookParticipating.Where(x => bookIds.Contains(x.BookId)).Select(
                   bp => new GetBooksToExport_Result
                   {
                       TitleAr = bp.Book.TitleAr,
                       TitleEn = bp.Book.TitleEn,
                       AuthorAr = bp.Book.AuthorNameAr, //GetAuthorName(bp.BookId, 2),
                       AuthorEn = bp.Book.AuthorName, //GetAuthorName(bp.BookId, 1),
                       BookDetails = bp.Book.BookDetails,
                       BookDetailsAr = bp.Book.BookDetailsAr,
                       Boothsection = GetExhibitionBoothSection(bp.Exhibitor.MemberExhibitionYearlyId, langId),
                       Boothsubsection = GetExhibitionBoothSubSection(bp.Exhibitor.MemberExhibitionYearlyId, langId),
                       ExhibitionLanguage = bp.Book.BookLanguage.Title,
                       ExhibitorAr = bp.Exhibitor.PublisherNameAr,
                       ExhibitorEn = bp.Exhibitor.PublisherNameEn,
                       hallnumber = bp.Exhibitor.XsiExhibitionExhibitorDetails.ToList()[0].HallNumber,
                       ISBN = bp.Book.Isbn,
                       IssueYear = bp.Book.IssueYear,
                       Price = bp.Book.Price,
                       PublisherAr = bp.Book.BookPublisherNameAr,
                       PublisherEn = bp.Book.BookPublisherName,
                       Standnumberend = bp.Exhibitor.XsiExhibitionExhibitorDetails.ToList()[0].StandNumberEnd,
                       Standnumberstart = bp.Exhibitor.XsiExhibitionExhibitorDetails.ToList()[0].StandNumberStart,
                       Subject = bp.Book.ExhibitionSubject.Title,
                       Subsubject = bp.Book.ExhibitionSubsubject.Title,
                   }).ToList();

                string folder = _appCustomSettings.UploadsPhysicalPath + "\\BooksExport\\";
                string excelName = "BooksList" + Guid.NewGuid() + ".xlsx";
                //string downloadUrl = string.Format("{0}://{1}/{2}/{3}", Request.Scheme, Request.Host, "Content/Uploads/BooksExport", excelName);

                string downloadUrl = _appCustomSettings.UploadsCMSPath + "/BooksExport/" + excelName;
                FileInfo file = new FileInfo(Path.Combine(folder, excelName));
                if (!Directory.Exists(folder))
                {
                    System.IO.Directory.CreateDirectory(folder);
                }
                if (file.Exists)
                {
                    //file.Delete();
                    file = new FileInfo(Path.Combine(folder, excelName));
                }
                using (var package = new ExcelPackage(file))
                {
                    var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                    workSheet.Cells.LoadFromCollection(list, true);
                    package.Save();
                }
                return downloadUrl;
            }
        }

        private string ExportBooksToExcelForTabs(List<long> bookIds, long langId, sibfnewdbContext _context)
        {
            var predicate = PredicateBuilder.New<XsiExhibitionBooks>();
            predicate = predicate.And(i => bookIds.Contains(i.BookId));

            var list = _context.XsiExhibitionBooks.Where(x => bookIds.Contains(x.BookId)).Select(
                       bp => new
                       {
                           TitleAr = bp.TitleAr,
                           TitleEn = bp.TitleEn,
                           AuthorAr = bp.AuthorNameAr,//GetAuthorName(bp.BookId, 2),
                           AuthorEn = bp.AuthorName, //GetAuthorName(bp.BookId, 1),
                           BookDetails = bp.BookDetails,
                           BookDetailsAr = bp.BookDetailsAr,
                           ExhibitionLanguage = bp.BookLanguage.Title,
                           ISBN = bp.Isbn,
                           IssueYear = bp.IssueYear,
                           Price = bp.Price,
                           PublisherAr = bp.BookPublisherNameAr,// bp.BookPublisher.TitleAr,
                           PublisherEn = bp.BookPublisherName, //bp.BookPublisher.Title,
                           //   Boothsection = GetBootSection(bp.Exhibitor.MemberExhibitionYearlyId, langId),
                           //   Boothsubsection = GetBootSubSection(bp.Exhibitor.MemberExhibitionYearlyId, langId),
                           //ExhibitorAr = bp.Exhibitor.PublisherNameAr,
                           //ExhibitorEn = bp.Exhibitor.PublisherNameEn,
                           //hallnumber = bp.Exhibitor.XsiExhibitionExhibitorDetails.ToList()[0].HallNumber,
                           //  Standnumberend = bp.XsiExhibitionExhibitorDetails.ToList()[0].StandNumberEnd,
                           //  Standnumberstart = bp.Exhibitor.XsiExhibitionExhibitorDetails.ToList()[0].StandNumberStart,
                           //  Subject = bp.Book.ExhibitionSubject.Title,
                           //  Subsubject = bp.Book.ExhibitionSubsubject.Title,
                       }).ToList();


            string folder = _appCustomSettings.UploadsPhysicalPath + "\\BooksExport\\";
            string excelName = "BooksList" + Guid.NewGuid() + ".xlsx";
            //string downloadUrl = string.Format("{0}://{1}/{2}/{3}", Request.Scheme, Request.Host, "Content/Uploads/BooksExport", excelName);

            string downloadUrl = _appCustomSettings.UploadsCMSPath + "/BooksExport/" + excelName;
            FileInfo file = new FileInfo(Path.Combine(folder, excelName));
            if (!Directory.Exists(folder))
            {
                System.IO.Directory.CreateDirectory(folder);
            }
            if (file.Exists)
            {
                //file.Delete();
                file = new FileInfo(Path.Combine(folder, excelName));
            }
            using (var package = new ExcelPackage(file))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromCollection(list, true);
                package.Save();
            }
            return downloadUrl;
        }
        private string GetExhibitionBoothSection(long exhibitorId, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string title = string.Empty;
                var boothSection = _context.XsiExhibitionExhibitorDetails
                    .Where(x => x.MemberExhibitionYearlyId == exhibitorId).OrderByDescending(x => x.ExhibitorDetailsId)
                    .FirstOrDefault().BoothSection;
                if (boothSection != null)
                {
                    if (langId == 1)
                        title = boothSection.Title;
                    else
                        title = boothSection.TitleAr;
                }
                return title;
            }
        }
        private string GetExhibitionBoothSubSection(long exhibitorId, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string title = string.Empty;
                var boothSection = _context.XsiExhibitionExhibitorDetails
                    .Where(x => x.MemberExhibitionYearlyId == exhibitorId).OrderByDescending(x => x.ExhibitorDetailsId)
                    .FirstOrDefault().BoothSection;
                if (boothSection != null)
                {
                    if (langId == 1)
                        title = boothSection.Title;
                    else
                        title = boothSection.TitleAr;
                }

                return title;
            }
        }
        private string GetPrice(string price, long? currenyId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (currenyId.HasValue && currenyId != 0)
                {
                    string strcurrency = MethodFactory.GetExhibitionCurrency(currenyId.Value, _context);
                    return strcurrency + " " + MethodFactory.AddCommasToCurrency(price);
                }
                else
                {
                    return MethodFactory.AddCommasToCurrency(price);
                }
            }
        }
        private string GetSubject(long? subjectId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (subjectId.HasValue && subjectId != 0)
                {
                    XsiExhibitionBookSubject BookSubject = _context.XsiExhibitionBookSubject.Where(x => x.ItemId == subjectId).FirstOrDefault();
                    if (BookSubject != null)
                        if (BookSubject.Title != null)
                            return BookSubject.Title;
                }
                return "";
            }
        }
        private string GetAuthors(long bookId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (bookId != 0)
                {
                    HashSet<long> authorIds = new HashSet<long>(_context.XsiExhibitionBookAuthor.Where(x => x.BookId == bookId).Select(x => x.AuthorId));

                    var authors = string.Join(", ", _context.XsiExhibitionAuthor.Where(p => authorIds.Contains(p.AuthorId))
                                  .Select(p => p.Title.ToString()));
                    return authors;
                }
                return "";
            }
        }
        private string GetAuthorName(long bookId, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                HashSet<long> authorIds = new HashSet<long>(_context.XsiExhibitionBookAuthor.Where(x => x.BookId == bookId).Select(x => x.AuthorId));
                List<string> authorNames = new List<string>();
                if (langId == 1)
                    authorNames = _context.XsiExhibitionAuthor.Where(x => authorIds.Contains(x.AuthorId) && x.IsActive == "Y" && x.Status == "A").Select(x => x.Title).ToList();
                else
                    authorNames = _context.XsiExhibitionAuthor.Where(x => authorIds.Contains(x.AuthorId) && x.IsActive == "Y" && x.Status == "A").Select(x => x.TitleAr).ToList();

                return String.Join(",", authorNames.ToArray());
            }
        }
        private string GetPublisherName(long publisherId, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibitionBookPublisher entity = _context.XsiExhibitionBookPublisher.Where(x => x.ItemId == publisherId).FirstOrDefault();
                if (entity != null)
                    return langId == 1 ? entity.Title : entity.TitleAr;
                return string.Empty;
            }
        }
        private string GetBookType(long itemid, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var entity = _context.XsiExhibitionBookType.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
                if (entity != null)
                    return langId == 1 ? entity.Title : entity.TitleAr;

                return string.Empty;
            }
        }
        #endregion
    }

    public class BookSearch
    {
        public long MemberId { get; set; }
        public string Search { get; set; }
        public long Websiteid { get; set; }
        public bool IsInventory { get; set; }
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 15;
        public bool IsList { get; set; }
        public string Type { get; set; }
        public bool Simple { get; set; }
    }
    public class AddBooksDTO
    {
        public List<long> BookIds { get; set; }
        public long ExhibitorId { get; set; }
        public long Websiteid { get; set; }
        public long AgencyId { get; set; }
    }
    public class RemoveBooksDTO
    {
        public List<long> BookIds { get; set; }
        public long ExhibitorId { get; set; }
        public long Websiteid { get; set; }
        public long AgencyId { get; set; }
    }
}

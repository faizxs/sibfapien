﻿using Contracts;
using Entities.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using Xsi.ServicesLayer;
using drawingFont = System.Drawing.Font;
namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StaffGuestController : ControllerBase
    {
        ExhibitionStaffGuestParticipatingService ExhibitionStaffGuestParticipatingService;
        ExhibitionStaffGuestService ExhibitionStaffGuestService;
        #region Variables
        private ILoggerManager _logger;
        private readonly IEmailSender _emailSender;
        // private readonly IStringLocalizer<StaffGuestController> _stringLocalizer;
        private readonly AppCustomSettings _appCustomSettings;
        #endregion

        #region Status
        string strApproved = EnumConversion.ToString(EnumStaffGuestStatus.Approved);
        string strPending = EnumConversion.ToString(EnumStaffGuestStatus.Pending);
        string strFlightDetailsPending = EnumConversion.ToString(EnumStaffGuestStatus.FlightDetailsPending);
        string strPendingDocumentation = EnumConversion.ToString(EnumStaffGuestStatus.PendingDocumentation);
        string strVisaProcessing = EnumConversion.ToString(EnumStaffGuestStatus.VisaProcessing);
        string strVisaUploaded = EnumConversion.ToString(EnumStaffGuestStatus.VisaUploaded);
        string strNoVisaInsuranceUploaded = EnumConversion.ToString(EnumStaffGuestStatus.NoVisaInsuranceUploaded);
        string strReIssueVisa = EnumConversion.ToString(EnumStaffGuestStatus.ReIssueVisa);
        string strDocumentsApproved = EnumConversion.ToString(EnumStaffGuestStatus.DocumentsApproved);
        string strDocumentsUploaded = EnumConversion.ToString(EnumStaffGuestStatus.DocumentsUploaded);
        string strRejected = EnumConversion.ToString(EnumStaffGuestStatus.Rejected);
        string strCancelled = EnumConversion.ToString(EnumStaffGuestStatus.Cancelled);
        string strBlocked = EnumConversion.ToString(EnumStaffGuestStatus.Blocked);
        string strBook = EnumConversion.ToString(EnumStaffGuestStatus.Book);
        string strYes = EnumConversion.ToString(EnumBool.Yes);
        #endregion

        #region FilePaths
        readonly string PassportPath = "StaffGuest\\Passport\\";
        readonly string PhotoPath = "StaffGuest\\Photo\\";
        readonly string VisaFormPath = "StaffGuest\\VisaForm\\";
        readonly string FlightTicketsPath = "StaffGuest\\FlightTickets\\";
        readonly string CountryUIDPath = "StaffGuest\\CountryUID\\";
        readonly string AgreementPDFPath = "StaffGuest\\Agreement\\";
        readonly string SignedAgreementPhotoPath = "StaffGuest\\SignedAgreement\\";
        readonly string PassportPathTemp = "StaffGuest\\Passport\\temp\\";
        readonly string VisaPath = "StaffGuest\\Visa\\";
        readonly string HealthInsurancePath = "StaffGuest\\HealthInsurance\\";
        readonly string HealthInsurancePathTwo = "StaffGuest\\HealthInsuranceTwo\\";
        public string ext = "PNG";
        #endregion FilePaths

        #region EmailRelated
        private string StrEmailContentBody { get; set; }
        private string StrEmailContentEmail { get; set; }
        private string StrEmailContentSubject { get; set; }
        private string StrEmailContentAdmin { get; set; }
        XsiEmailContent EmailContent;
        XsiScrfemailContent ScrfEmailContent;
        #endregion EmailRelated

        private MessageDTO MessageDTO { get; set; }
        public long SIBFMemberId { get; set; }

        public StaffGuestController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings, IEmailSender emailSender)
        {
            _logger = logger;
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
        }

        #region Get Methods

        [HttpGet]
        [Route("{websiteid}/{memberid}")]
        public ActionResult<dynamic> StaffGuestPageLoad(int websiteid, long memberid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long memberId = User.Identity.GetID();

                if (memberId == memberid)
                {
                    if (IsExhibitionActive(websiteid))
                    {
                        if (MethodFactory.IsSIBFSTaff(memberId))
                        {
                            return Ok("");
                        }
                        //else if (MethodFactory.IsHospitalityPackage(memberId))
                        //{
                        //    return Ok("");
                        //}
                        else
                        {
                            return Ok("Member is not Staff Guest");
                            //StaffGuestDTO.MessageType = "Error";
                        }
                    }
                    else
                    {
                        //StaffGuestDTO.Tab1ContentVisibility = false;
                        return Ok("Exhibition Unavailable.");
                        //StaffGuestDTO.MessageType = "Error";
                    }
                }
                return Ok("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside StaffGuestPageLoad action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet("StaffGuest/{staffguestid}/{exhibitionId}")]
        public ActionResult<dynamic> GetStaffGuest(long StaffGuestId, long exhibitionId)
        {
            try
            {
                SaveStaffGuestDTO LoadStaffGuestDTO = new SaveStaffGuestDTO();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                LoadStaffGuestDTO = LoadContent(StaffGuestId, exhibitionId, LangId);

                return Ok(LoadStaffGuestDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetStaffGuest action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [Route("StaffGuestPending/{websiteid}/{memberid}")]
        public ActionResult<dynamic> GetStaffGuestPending(int websiteid, long memberid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                List<StaffGuestTabDTO> List = new List<StaffGuestTabDTO>();
                XsiExhibition exhibtion = new XsiExhibition();
                SIBFMemberId = memberid;
                long memberId = User.Identity.GetID();
                if (memberId == memberid)
                {
                    using (ExhibitionStaffGuestParticipatingService StaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService())
                    {
                        exhibtion = MethodFactory.GetExhibition(websiteid);

                        ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService();
                        var where = new XsiExhibitionStaffGuestParticipating();
                        where.IsActive = EnumConversion.ToString(EnumBool.Yes);

                        //where.IsFromPcregistration = EnumConversion.ToString(EnumBool.Yes);
                        //where.ExhibitionId = exhibitionId;
                        //where.XsiExhibitionMemberApplicationYearly = new XsiExhibitionMemberApplicationYearly();
                        //where.MemberId = SIBFMemberId;
                        var whereRep = new XsiExhibitionStaffGuest();
                        whereRep.MemberId = SIBFMemberId;
                        whereRep.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        HashSet<long> staffGuestIds = new HashSet<long>(ExhibitionStaffGuestService.GetExhibitionStaffGuest(whereRep).Select(s => s.StaffGuestId).ToList());

                        List = StaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(where).Where(p =>
                        staffGuestIds.Contains(p.StaffGuestId) &&
                        p.ExhibitionId == exhibtion.ExhibitionId && (p.Status == strApproved || p.Status == strBook || p.Status == strNoVisaInsuranceUploaded || p.Status == strPending || p.Status == strFlightDetailsPending || p.Status == strPendingDocumentation || p.Status == strVisaProcessing || p.Status == strReIssueVisa || p.Status == strDocumentsApproved || p.Status == strDocumentsUploaded)).OrderByDescending(o => o.ExhibitionId)
                            .Select(i => new StaffGuestTabDTO()
                            {
                                StaffGuestId = i.StaffGuestId,
                                ExhibitionId = i.ExhibitionId,
                                Name = GetStaffGuestName(i.StaffGuestId, i.ExhibitionId, LangId),
                                LastName = GetStaffGuestLastName(i.StaffGuestId, i.ExhibitionId, LangId),
                                Profession = i.Profession,
                                Passport = i.Passport,
                                Nationality = GetCountryTitle(i.CountryId, LangId),
                                Status = MethodFactory.GetReprepresentativeStatus(i.Status, LangId),
                                Date = i.CreatedOn.HasValue ? i.CreatedOn.Value.ToString("MMM dd, yyyy") : null,
                                IsTravelDetails = i.IsTravelDetails
                            }).ToList();
                    }
                    return Ok(List);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside StaffGuestPending action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [Route("StaffGuestParticipating/{websiteid}/{memberid}")]
        public ActionResult<dynamic> GetStaffGuestParticipating(int websiteid, long memberid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                List<StaffGuestTabDTO> List = new List<StaffGuestTabDTO>();
                XsiExhibition exhibition = new XsiExhibition();
                SIBFMemberId = memberid;
                long memberId = User.Identity.GetID();
                long exhibitionId = -1;
                if (memberId == memberid)
                {
                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        exhibition = MethodFactory.GetActiveExhibition(websiteid, _context);
                        if (exhibition != null)
                            exhibitionId = exhibition.ExhibitionId;
                    }
                    using (ExhibitionStaffGuestParticipatingService StaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService())
                    {
                        ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService();
                        var where = new XsiExhibitionStaffGuestParticipating();
                        where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        //where.XsiExhibitionMemberApplicationYearly = new XsiExhibitionMemberApplicationYearly();
                        //where.XsiExhibitionMemberApplicationYearly.MemberId = SIBFMemberId;

                        var whereRep = new XsiExhibitionStaffGuest();
                        whereRep.MemberId = SIBFMemberId;
                        whereRep.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        HashSet<long> staffGuestIds = new HashSet<long>(ExhibitionStaffGuestService.GetExhibitionStaffGuest(whereRep).Select(s => s.StaffGuestId).ToList());

                        List = StaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(where).Where(p =>
                        staffGuestIds.Contains(p.StaffGuestId) &&
                        p.ExhibitionId == exhibition.ExhibitionId && (p.Status == strApproved || p.Status == strBook || p.Status == strNoVisaInsuranceUploaded || p.Status == strVisaUploaded)).OrderBy(o => o.NameEn)
                            .Select(i => new StaffGuestTabDTO()
                            {
                                StaffGuestId = i.StaffGuestId,
                                ExhibitionId = i.ExhibitionId,
                                Name = GetStaffGuestName(i.StaffGuestId, exhibitionId, LangId),
                                LastName = GetStaffGuestLastName(i.StaffGuestId, exhibitionId, LangId),
                                Nationality = GetCountryTitle(i.CountryId, LangId),
                                Passport = i.Passport,
                                Profession = i.Profession,
                                Status = MethodFactory.GetReprepresentativeStatus(i.Status, LangId),
                                Date = i.DateOfIssue.HasValue ? i.DateOfIssue.Value.ToString("MMM dd, yyyy") : null,
                                IsTravelDetails = i.IsTravelDetails,
                                IsShowBooking = IsShowBookingLink(i.ExhibitionId, i.StaffGuestId, i.Status, i.NeedVisa),
                                MainGuest = i.MainGuest,
                                MainGuestId = i.MainGuestId
                            }).ToList();
                    }
                    return Ok(List);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside StaffGuestParticipating action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
        [HttpGet]
        [Route("StaffGuestArchive/{websiteid}/{memberid}")]
        public ActionResult<dynamic> GetStaffGuestArchive(int websiteid, long memberid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                List<StaffGuestTabDTO> List = new List<StaffGuestTabDTO>();
                XsiExhibition exhibtion = new XsiExhibition();
                SIBFMemberId = memberid;
                long memberId = User.Identity.GetID();
                if (memberId == memberid)
                {

                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        exhibtion = MethodFactory.GetActiveExhibition(websiteid, _context);
                    }
                    using (ExhibitionStaffGuestParticipatingService StaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService())
                    {
                        ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService();
                        var where = new XsiExhibitionStaffGuestParticipating();
                        where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        //where.XsiExhibitionMemberApplicationYearly = new XsiExhibitionMemberApplicationYearly();
                        // where.MemberId = SIBFMemberId;

                        var whereRep = new XsiExhibitionStaffGuest();
                        whereRep.MemberId = SIBFMemberId;
                        whereRep.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        HashSet<long> staffGuestIds = new HashSet<long>(ExhibitionStaffGuestService.GetExhibitionStaffGuest(whereRep).Select(s => s.StaffGuestId).ToList());

                        List = StaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(where).Where(p =>
                        staffGuestIds.Contains(p.StaffGuestId) &&
                        (p.Status == strApproved || p.Status == strBook || p.Status == strNoVisaInsuranceUploaded || p.Status == strPending || p.Status == strFlightDetailsPending || p.Status == strPendingDocumentation
                     || p.Status == strRejected || p.Status == strBlocked)).OrderBy(o => o.NameEn)
                            .Select(i => new StaffGuestTabDTO()
                            {
                                StaffGuestId = i.StaffGuestId,
                                ExhibitionId = i.ExhibitionId,
                                Name = GetStaffGuestName(i.StaffGuestId, i.ExhibitionId, LangId),
                                LastName = GetStaffGuestLastName(i.StaffGuestId, i.ExhibitionId, LangId),
                                Nationality = GetCountryTitle(i.CountryId, LangId),
                                Profession = i.Profession,
                                Passport = i.Passport,
                                Status = CheckExhibition(i.StaffGuestId, exhibtion.ExhibitionId) ? MethodFactory.GetReprepresentativeStatus(i.Status, LangId) : (LangId == 1 ? "Former Participant" : "مشارك سابق"),
                                Date = i.DateOfIssue.HasValue ? i.DateOfIssue.Value.ToString("MMM dd, yyyy") : null,
                                IsTravelDetails = i.IsTravelDetails,
                                IsParticipated = CheckParticipated(i.StaffGuestId, i.ExhibitionId),
                                IsEditable = CheckExhibition(i.StaffGuestId, exhibtion.ExhibitionId) ? false : true
                            }).ToList();
                    }
                    return Ok(List);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside StaffGuestArchive action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [EnableCors("AllowOrigin")]
        [Route("StaffGuestDetails/{itemid}/{exhibitionId}/{websiteid}")]
        public ActionResult<dynamic> GetStaffGuestWebMethod(long itemId, long exhibitionId, long websiteid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long memberId = User.Identity.GetID();
                //long EnglishId = 1;// Convert.ToInt64(System.Web.Configuration.WebConfigurationManager.AppSettings["EnglishId"]);
                using (ExhibitionStaffGuestParticipatingService ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService())
                {
                    var staffGuestParticipating = ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(new XsiExhibitionStaffGuestParticipating() { StaffGuestId = itemId, ExhibitionId = exhibitionId }).FirstOrDefault();
                    using (ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService())
                    {

                        var entityDetails = ExhibitionStaffGuestService.GetStaffGuestByItemId(itemId);
                        if (staffGuestParticipating != null)
                        {
                            ExhibitionStaffGuestBookingDetailsService ExhibitionStaffGuestBookingDetailsService = new ExhibitionStaffGuestBookingDetailsService();
                            XsiStaffGuestNew entityNew = new XsiStaffGuestNew();
                            entityNew.StaffGuestId = staffGuestParticipating.StaffGuestId;
                            entityNew.ExhibitionId = staffGuestParticipating.ExhibitionId;
                            entityNew.NameEn = staffGuestParticipating.NameEn;
                            entityNew.LastName = staffGuestParticipating.LastName;
                            entityNew.NameAr = staffGuestParticipating.NameAr;
                            entityNew.Profession = staffGuestParticipating.Profession;
                            if (staffGuestParticipating.CountryId != null)
                            {
                                ExhibitionCountryService ExhibitionCountryService = new ExhibitionCountryService();
                                XsiExhibitionCountry entityCountry = ExhibitionCountryService.GetExhibitionCountryByItemId(staffGuestParticipating.CountryId.Value);
                                if (entityCountry != null)
                                    entityNew.Nationality = entityCountry.CountryName;
                            }
                            if (entityDetails != null)
                            {
                                if (memberId != entityDetails.MemberId)
                                    return Unauthorized();
                                if (entityDetails.Dob != null)
                                    entityNew.DOB = entityDetails.Dob.Value.ToString("MM/dd/yyyy");
                                if (entityDetails.PlaceOfBirth != null)
                                    entityNew.POB = entityDetails.PlaceOfBirth;
                            }
                            entityNew.MainGuestId = staffGuestParticipating.MainGuestId;
                            entityNew.MainGuest = staffGuestParticipating.MainGuest;

                            if (staffGuestParticipating.Telephone != null)
                                if (staffGuestParticipating.Telephone.IndexOf('$') > -1)
                                    entityNew.Telephone = staffGuestParticipating.Telephone.Replace('$', '-');
                            if (staffGuestParticipating.Mobile != null)
                                if (staffGuestParticipating.Mobile.IndexOf('$') > -1)
                                    entityNew.Mobile = staffGuestParticipating.Mobile.Replace('$', '-');
                            entityNew.PassportNumber = staffGuestParticipating.Passport;
                            entityNew.POI = staffGuestParticipating.PlaceOfIssue;
                            if (staffGuestParticipating.DateOfIssue != null)
                                entityNew.DOI = staffGuestParticipating.DateOfIssue.Value.ToString("MM/dd/yyyy");
                            if (staffGuestParticipating.ExpiryDate != null)
                                entityNew.ExpiryDate = staffGuestParticipating.ExpiryDate.Value.ToString("MM/dd/yyyy");

                            if (staffGuestParticipating.NeedVisa == EnumConversion.ToString(EnumBool.Yes))
                                entityNew.IsVisaNeed = true;
                            else
                                entityNew.IsVisaNeed = false;

                            entityNew.VisaType = staffGuestParticipating.VisaType;
                            entityNew.GuestCategory = staffGuestParticipating.GuestCategory;
                            entityNew.GuestType = staffGuestParticipating.GuestType;

                            if (staffGuestParticipating.NoofVisaMonths != null && !string.IsNullOrEmpty(staffGuestParticipating.NoofVisaMonths))
                                entityNew.VisaNoOfMonths = staffGuestParticipating.NoofVisaMonths;

                            if (!string.IsNullOrEmpty(staffGuestParticipating.PassportCopy))
                                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PassportPath + staffGuestParticipating.PassportCopy))
                                    entityNew.PassportCopy = _appCustomSettings.UploadsCMSPath + "/StaffGuest/Passport/" + staffGuestParticipating.PassportCopy;

                            if (!string.IsNullOrEmpty(staffGuestParticipating.PassportCopyTwo))
                                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PassportPath + staffGuestParticipating.PassportCopyTwo))
                                    entityNew.PassportCopyTwo = _appCustomSettings.UploadsCMSPath + "/StaffGuest/Passport/" + staffGuestParticipating.PassportCopyTwo;

                            if (!string.IsNullOrEmpty(staffGuestParticipating.HealthInsurance))
                                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + HealthInsurancePath + staffGuestParticipating.HealthInsurance))
                                    entityNew.HealthInsurance = _appCustomSettings.UploadsCMSPath + "/StaffGuest/HealthInsurance/" + staffGuestParticipating.HealthInsurance;

                            if (!string.IsNullOrEmpty(staffGuestParticipating.HealthInsuranceTwo))
                                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + HealthInsurancePathTwo + staffGuestParticipating.HealthInsuranceTwo))
                                    entityNew.HealthInsuranceTwo = _appCustomSettings.UploadsCMSPath + "/StaffGuest/HealthInsurance/" + staffGuestParticipating.HealthInsuranceTwo;

                            if (!string.IsNullOrEmpty(staffGuestParticipating.Photo))
                                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PhotoPath + staffGuestParticipating.Photo))
                                    entityNew.PersonalPhoto = _appCustomSettings.UploadsCMSPath + "/StaffGuest/Photo/" + staffGuestParticipating.Photo;

                            if (!string.IsNullOrEmpty(staffGuestParticipating.VisaApplicationForm))
                                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + VisaFormPath + staffGuestParticipating.VisaApplicationForm))
                                    entityNew.VisaApplicationForm = _appCustomSettings.UploadsCMSPath + "/StaffGuest/VisaForm/" + staffGuestParticipating.VisaApplicationForm;

                            var booking = ExhibitionStaffGuestBookingDetailsService.GetExhibitionStaffGuestBookingDetails(new XsiExhibitionStaffGuestBookingDetails() { StaffGuestId = staffGuestParticipating.StaffGuestId }).FirstOrDefault();
                            if (booking != null)
                            {
                                if (!string.IsNullOrEmpty(booking.FlightTicket))
                                    if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + FlightTicketsPath + booking.FlightTicket))
                                        entityNew.FlightTicket = _appCustomSettings.UploadsCMSPath + "/StaffGuest/FlightTickets/" + booking.FlightTicket;

                                if (!string.IsNullOrEmpty(booking.ReturnFlightTicket))
                                    if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + FlightTicketsPath + booking.ReturnFlightTicket))
                                        entityNew.ReturnFlightTicket = _appCustomSettings.UploadsCMSPath + "/StaffGuest/FlightTickets/" + booking.ReturnFlightTicket;
                            }

                            if (!string.IsNullOrEmpty(staffGuestParticipating.VisaCopy))
                                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + VisaPath + staffGuestParticipating.VisaCopy))
                                    entityNew.VisaCopy = _appCustomSettings.UploadsCMSPath + "/StaffGuest/Visa/" + staffGuestParticipating.VisaCopy;

                            //if (staffGuestParticipating.NeedVisa == EnumConversion.ToString(EnumBool.Yes))
                            //{
                            //    if (!string.IsNullOrEmpty(staffGuestParticipating.VisaCopy))
                            //        if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + VisaPath + staffGuestParticipating.VisaCopy))
                            //            entityNew.VisaCopy = _appCustomSettings.UploadsCMSPath + "/StaffGuest/Visa/" + staffGuestParticipating.VisaCopy;
                            //}

                            entityNew.Status = staffGuestParticipating.Status;
                            if (staffGuestParticipating.ArrivalDate != null)
                                entityNew.ArrivalDate = staffGuestParticipating.ArrivalDate.Value.ToString("MM/dd/yyyy");
                            if (staffGuestParticipating.DepartureDate != null)
                                entityNew.DepartureDate = staffGuestParticipating.DepartureDate.Value.ToString("MM/dd/yyyy");
                            if (staffGuestParticipating.ArrivalAirportId != null)
                                entityNew.ArrivalAirportGroupId = staffGuestParticipating.ArrivalAirportId.ToString();
                            if (staffGuestParticipating.IsTravelDetails != null)
                                entityNew.IsTravelDetails = staffGuestParticipating.IsTravelDetails;
                            else
                                entityNew.IsTravelDetails = EnumConversion.ToString(EnumBool.No);

                            var PenaltyDate = GetExhibitionDateOfDue(websiteid);
                            if (PenaltyDate != null)
                            {
                                entityNew.ExhibitorLastDate = PenaltyDate.Value.ToString("dddd d MMMM yyyy");
                                if (PenaltyDate > MethodFactory.ArabianTimeNow())
                                    entityNew.RemainingDays = Math.Ceiling(PenaltyDate.Value.Subtract(MethodFactory.ArabianTimeNow()).TotalDays).ToString();
                                else
                                    entityNew.RemainingDays = "0 ";
                            }

                            // entityNew.NotesForUser = staffGuestParticipating.RepresentativeAdminNote;
                            return Ok(entityNew);
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetStaffGuestWebMethod action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [EnableCors("AllowOrigin")]
        [Route("GetArrivalAirport")]
        public ActionResult<dynamic> GetArrivalAirport()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                using (ExhibitionArrivalAirportService ExhibitionArrivalAirportService = new ExhibitionArrivalAirportService())
                {
                    XsiExhibitionArrivalAirport where = new XsiExhibitionArrivalAirport();
                    //where.LanguageId = 1;
                    where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    if (LangId == 1)
                        return ExhibitionArrivalAirportService.GetExhibitionArrivalAirport(where).Select(x => new { x.ItemId, x.Title }).OrderBy(o => o.Title.Trim()).ToArray();
                    else
                        return ExhibitionArrivalAirportService.GetExhibitionArrivalAirport(where).Select(x => new { x.ItemId, x.TitleAr }).OrderBy(o => o.TitleAr.Trim()).ToArray();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetArrivalAirport action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [EnableCors("AllowOrigin")]
        [Route("GetArrivalTerminal")]
        public ActionResult<dynamic> GetArrivalTerminal(long categoryId)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                using (ArrivalTerminalService ArrivalTerminalService = new ArrivalTerminalService())
                {
                    XsiArrivalTerminal where = new XsiArrivalTerminal();
                    where.CategoryId = categoryId;
                    where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    if (LangId == 1)
                        return ArrivalTerminalService.GetArrivalTerminal(where).Select(x => new { x.ItemId, x.Title }).OrderBy(o => o.Title.Trim()).ToArray();
                    else
                        return ArrivalTerminalService.GetArrivalTerminal(where).Select(x => new { x.ItemId, x.TitleAr }).OrderBy(o => o.TitleAr.Trim()).ToArray();

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetArrivalTerminal action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
        #endregion

        #region Post Methods
        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("RemoveCommand")]
        public ActionResult<dynamic> Remove_Command(RemoveCommandStaffGuestDTO removeCommandDTO)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long memberId = User.Identity.GetID();
                if (RemoveStaffGuest(removeCommandDTO, memberId, LangId))
                    return Ok("Removed Successfully");
                return Ok("Error removing a record");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside RemoveCommand staff guest action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }

            //try
            //{
            //    RemoveStaffGuest(removeCommandDTO);
            //    return Ok("Removed Successfully");
            //}
            //catch (Exception ex)
            //{
            //    _logger.LogError($"Something went wrong inside RemoveCommand action: {ex.InnerException}");
            //    return Ok("Something went wrong. Please try again later.");
            //}
        }

        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("SaveData")]
        public ActionResult<dynamic> PostSaveData(SaveStaffGuestDTO StaffGuestDTO)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                bool IsCountryValid = false;
                MessageDTO MessageDTO = new MessageDTO();
                if (StaffGuestDTO.IsEditClicked && StaffGuestDTO.StaffGuestId > 0)
                {
                    if (!string.IsNullOrEmpty(StaffGuestDTO.PassportExpiryDate))
                    {
                        return UpdateStaffGuestFromSaveData(StaffGuestDTO);
                    }
                    else
                    {
                        MessageDTO.MessageTypeResponse = "Error";
                        MessageDTO.Message = "Passport expiry date is missing";
                        return Ok(MessageDTO);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(StaffGuestDTO.PassportExpiryDate))
                    {
                        if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64Visa))
                        {
                            if (StaffGuestDTO.IsVisaNeed == "Y")
                            {
                                IsCountryValid = IsVisaRequired(StaffGuestDTO.NationalityId);
                                if (IsCountryValid)
                                    return AddStaffGuestDataFromSaveData(StaffGuestDTO);
                                else
                                {
                                    MessageDTO.MessageTypeResponse = "Error";
                                    if (LangId == 1)
                                        MessageDTO.Message = "Visa not required for the selected country. Please choose No-Visa tab and start the registration process.";
                                    else
                                        MessageDTO.Message = "Visa not required for the selected country. Please choose No-Visa tab and start the registration process.";
                                    return Ok(MessageDTO);
                                }
                            }
                            else
                            {
                                if (StaffGuestDTO.IsUAEResident == EnumConversion.ToString(EnumBool.Yes))
                                    IsCountryValid = true;
                                else
                                    IsCountryValid = IsVisaNotRequired(StaffGuestDTO.NationalityId);
                                if (IsCountryValid)
                                    return AddStaffGuestDataFromSaveData(StaffGuestDTO);
                                else
                                {
                                    MessageDTO.MessageTypeResponse = "Error";
                                    if (LangId == 1)
                                        MessageDTO.Message = "The selected country requires visa, please add it in the “ADD GUEST (VISA)” Tab.";
                                    else
                                        MessageDTO.Message = "الدولة المختارة تتطلب تأشيرة، الرجاء إضافة الضيف في نافذة \"إضافة ضيف(مع تأشيرة)\"";
                                    return Ok(MessageDTO);
                                }
                            }
                        }
                        else
                        {
                            MessageDTO.MessageTypeResponse = "Error";
                            if (LangId == 1)
                                MessageDTO.Message = "Missing file upload. Please download and fill up the information.";
                            else
                                MessageDTO.Message = "Missing file upload. Please download and fill up the information.";
                            return Ok(MessageDTO);
                        }

                    }
                    else
                    {
                        MessageDTO.MessageTypeResponse = "Error";
                        if (LangId == 1)
                            MessageDTO.Message = "Passport expiry date is missing";
                        else
                            MessageDTO.Message = "Passport expiry date is missing";
                        return Ok(MessageDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Staffguest SaveData action: {ex.InnerException}");
                MessageDTO.MessageTypeResponse = "Error";
                MessageDTO.Message = "Something went wrong. Please try again later.";
                return Ok(MessageDTO);
            }

        }

        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("UpdateAndReregister")]
        public ActionResult<dynamic> UpdateAndReregisterParticipateInCurrentyYear(SaveStaffGuestDTO StaffGuestDTO)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                MessageDTO = new MessageDTO();
                SIBFMemberId = StaffGuestDTO.MemberId;

                if (UpdateNReregister(StaffGuestDTO))
                {
                    MessageDTO.MessageTypeResponse = "Success";
                    MessageDTO.Message = "Updated successfully.";
                    return Ok(MessageDTO);
                }
                else
                {
                    MessageDTO.MessageTypeResponse = "Error";
                    MessageDTO.Message = "Exhibitor not found";
                    return Ok(MessageDTO);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateAndReregister action: {ex.InnerException}");
                MessageDTO.MessageTypeResponse = "Error";
                MessageDTO.Message = "Something went wrong. Please try again later.";
                return Ok(MessageDTO);
            }
        }

        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("SaveTravelDetails")]
        public ActionResult<dynamic> SubmitRepTravel_Click(TravelStaffGuestDTO travelDTO)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                MessageDTO = new MessageDTO();
                SIBFMemberId = travelDTO.MemberId;
                UpdateTravelDetails(travelDTO);
                return Ok(MessageDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside SaveTravelDetails action: {ex.InnerException}");
                MessageDTO.MessageTypeResponse = "Error";
                MessageDTO.Message = "Something went wrong. Please try again later.";
                return Ok(MessageDTO);
            }
        }
        #endregion

        #region Notify Email
        void NotifyAdminAboutBooking(long memberId, long staffguestid, long exhibitionid, long websiteId, bool isnew, StringBuilder sbdetails, long langid = 1)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                EmailContentDTO contentDTO = new EmailContentDTO();
                ExhibitionStaffGuestParticipatingService ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService();
                XsiExhibitionStaffGuestParticipating whereparticitpating = new XsiExhibitionStaffGuestParticipating();
                whereparticitpating.ExhibitionId = exhibitionid;
                whereparticitpating.StaffGuestId = staffguestid;
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);

                XsiExhibitionMember memberentity = new XsiExhibitionMember();
                memberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(Convert.ToInt64(memberId));

                if (memberentity != default(XsiExhibitionMember))
                {
                    var sgParticipatingEntity = ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(whereparticitpating).FirstOrDefault();
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {
                        #region Send Admin Email Notifying about staffguest updates
                        StringBuilder body = new StringBuilder();
                        contentDTO = GetContent(contentDTO, websiteId, 20141, 20119);

                        StringBuilder sb = new StringBuilder();
                        sb = sb.Append(contentDTO.strEmailContentBody);
                        sb = sb.Replace("$$Name$$", "Admin");
                        if (langid == 1)
                        {
                            if (!string.IsNullOrEmpty(sgParticipatingEntity.NameEn) && !string.IsNullOrEmpty(sgParticipatingEntity.LastName))
                                sb = sb.Replace("$$staffguestname$$", sgParticipatingEntity.NameEn);
                            else if (!string.IsNullOrEmpty(sgParticipatingEntity.NameEn))
                                sb = sb.Replace("$$staffguestname$$", sgParticipatingEntity.NameEn);
                            else if (!string.IsNullOrEmpty(sgParticipatingEntity.LastName))
                                sb = sb.Replace("$$staffguestname$$", sgParticipatingEntity.LastName);
                            else
                                sb = sb.Replace("$$staffguestname$$", string.Empty);
                        }
                        else
                        {
                            if (sgParticipatingEntity.NameAr != null)
                                sb = sb.Replace("$$staffguestname$$", sgParticipatingEntity.NameAr);
                            else
                            {
                                if (!string.IsNullOrEmpty(sgParticipatingEntity.NameEn) && !string.IsNullOrEmpty(sgParticipatingEntity.LastName))
                                    sb = sb.Replace("$$staffguestname$$", sgParticipatingEntity.NameEn);
                                else if (!string.IsNullOrEmpty(sgParticipatingEntity.NameEn))
                                    sb = sb.Replace("$$staffguestname$$", sgParticipatingEntity.NameEn);
                                else if (!string.IsNullOrEmpty(sgParticipatingEntity.LastName))
                                    sb = sb.Replace("$$staffguestname$$", sgParticipatingEntity.LastName);
                                else
                                    sb = sb.Replace("$$staffguestname$$", string.Empty);
                            }
                        }

                        if (isnew)
                        {
                            sb = sb.Replace("$$addormodified$$", "added");
                            sb = sb.Replace("$$datetimestamp$$", sgParticipatingEntity.CreatedOn.Value.ToString("dd MMM yyyy hh:mm tt"));
                        }
                        else
                        {
                            sb = sb.Replace("$$addormodified$$", "updated");
                            sb = sb.Replace("$$datetimestamp$$", sgParticipatingEntity.ModifiedOn.Value.ToString("dd MMM yyyy hh:mm tt"));
                        }

                        sb = sb.Replace("$$addedby$$", memberentity.Firstname + " " + memberentity.LastName);

                        sb = sb.Replace("$$details$$", sbdetails.ToString());

                        contentDTO.strEmailContentBody = sb.ToString();

                        body.Append(htmlContentFactory.BindEmailContent(langid, contentDTO.strEmailContentBody, memberentity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                        _emailSender.SendEmail(contentDTO.strEmailContentAdmin, _appCustomSettings.AdminEmail, contentDTO.strEmailContentEmail, contentDTO.strEmailContentSubject, body.ToString());
                        #endregion
                    }
                }
            }
        }
        private EmailContentDTO GetContent(EmailContentDTO contentDTO, long websiteId, long sibfcontentbyitemid, long scrfcontentbyitemid)
        {
            #region Email Content
            using (SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService())
            {
                EmailContentService EmailContentService = new EmailContentService();
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                {
                    var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(scrfcontentbyitemid);
                    if (scrfEmailContent != null)
                    {
                        if (scrfEmailContent.Body != null)
                            contentDTO.strEmailContentBody = scrfEmailContent.Body;
                        if (scrfEmailContent.Email != null && !string.IsNullOrEmpty(scrfEmailContent.Email))
                            contentDTO.strEmailContentEmail = scrfEmailContent.Email;
                        else
                            contentDTO.strEmailContentEmail = _appCustomSettings.AdminEmail;
                        if (scrfEmailContent.Subject != null)
                            contentDTO.strEmailContentSubject = scrfEmailContent.Subject;
                        contentDTO.strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                    }
                }
                else
                {
                    var emailContent = EmailContentService.GetEmailContentByItemId(sibfcontentbyitemid);
                    if (emailContent != null)
                    {
                        if (emailContent.Body != null)
                            contentDTO.strEmailContentBody = emailContent.Body;
                        if (emailContent.Email != null && !string.IsNullOrEmpty(emailContent.Email))
                            contentDTO.strEmailContentEmail = emailContent.Email;
                        else
                            contentDTO.strEmailContentEmail = _appCustomSettings.AdminEmail;
                        if (emailContent.Subject != null)
                            contentDTO.strEmailContentSubject = emailContent.Subject;
                        contentDTO.strEmailContentAdmin = _appCustomSettings.AdminName;
                    }
                }
            }
            #endregion
            return contentDTO;
        }
        #endregion
        #region Other Methods
        private ActionResult<dynamic> AddStaffGuestDataFromSaveData(SaveStaffGuestDTO StaffGuestDTO)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                MessageDTO = new MessageDTO();
                SIBFMemberId = StaffGuestDTO.MemberId;
                long memberId = User.Identity.GetID();
                if (memberId == SIBFMemberId)
                {
                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        var currentExhibition = MethodFactory.GetActiveExhibition(StaffGuestDTO.WebsiteId, _context);
                        if (StaffGuestDTO.IsVisaNeed == "Y")
                        {
                            var isvalidupload = false;
                            var isupload = false;
                            if (string.IsNullOrEmpty(StaffGuestDTO.VisaHref))
                                isvalidupload = false;
                            else
                                isvalidupload = true;

                            if (IsVisaApplicationFormValid("." + StaffGuestDTO.FileExtensionVisa, isvalidupload) || isvalidupload)
                            {
                                if (string.IsNullOrEmpty(StaffGuestDTO.PassportHRef))
                                    isvalidupload = false;
                                else
                                    isvalidupload = true;
                                if (IsPasportCopyValid("." + StaffGuestDTO.FileExtensionofPassport, isvalidupload) || isvalidupload)
                                {
                                    isvalidupload = false;
                                    if (string.IsNullOrEmpty(StaffGuestDTO.PhotoHref))
                                        isvalidupload = false;
                                    else
                                        isvalidupload = true;

                                    if (IsPersonalPhotoValid("." + StaffGuestDTO.FileExtensionofPersonalPhoto, isvalidupload))
                                    {
                                        #region Visa Required
                                        if (!MethodFactory.IsDateExpired(StaffGuestDTO.PassportExpiryDate.Trim(), currentExhibition.StartDate, Convert.ToInt32(currentExhibition.ExpiryMonths)))
                                        {
                                            if (StaffGuestDTO.IsEditClicked && StaffGuestDTO.StaffGuestId > 0)
                                                UpdateContent(StaffGuestDTO, LangId);
                                            else
                                            {
                                                //SelectTab1();
                                                if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64Passport) && !string.IsNullOrEmpty(StaffGuestDTO.FileBase64PersonalPhoto) && !string.IsNullOrEmpty(StaffGuestDTO.FileBase64Visa))
                                                {
                                                    //if (StaffGuestDTO.StaffGuestId == 0)
                                                    SaveContent(StaffGuestDTO, LangId);
                                                }
                                                else
                                                {
                                                    if (LangId == 1)
                                                        MessageDTO.Message = "Please upload passport copy, personal photo,visa application form then try submitting again.";
                                                    else
                                                        MessageDTO.Message = "Please upload passport copy, personal photo,visa application form then try submitting again.";
                                                    MessageDTO.MessageTypeResponse = "Error";
                                                }
                                            }
                                            //BindRepresentative();
                                        }
                                        else if (currentExhibition.StartDate != null && !string.IsNullOrEmpty(StaffGuestDTO.PassportExpiryDate.Trim()))
                                        {
                                            if (StaffGuestDTO.PassportExpiryDate != null && Convert.ToDateTime(StaffGuestDTO.PassportExpiryDate.Trim()) <= currentExhibition.StartDate)
                                            {
                                                //if (LangId == 1)
                                                //    MessageDTO.Message = "Passport expires before or on the same day of the start of exhibition. Please provide a valid passport expiry date."; // _stringLocalizer["PassportExpiry"];
                                                //else
                                                //    MessageDTO.Message = "Passport expires before or on the same day of the start of exhibition. Please provide a valid passport expiry date."; // _stringLocalizer["PassportExpiry"];

                                                if (LangId == 1)
                                                    MessageDTO.Message = "Passport should have at least 6 months validity.";
                                                else
                                                    MessageDTO.Message = "يجب ان تكون صلاحية الجواز 6 شهور أو اكثر.";

                                                MessageDTO.MessageTypeResponse = "Error";
                                            }
                                            else
                                            {
                                                if (LangId == 1)
                                                    MessageDTO.Message = "Passport should have at least 6 months validity.";
                                                else
                                                    MessageDTO.Message = "يجب ان تكون صلاحية الجواز 6 شهور أو اكثر.";

                                                //"The validity of passport should be more than 6 months from the exhibition starting date";// _stringLocalizer["InvalidExhibitionStartorExpiryDate"];
                                                //"يجب أن تكون صلاحية جواز السفر أكثر من 6 أشهر من تاريخ بدء المعرض";
                                                MessageDTO.MessageTypeResponse = "Error";
                                            }

                                        }
                                        else
                                        {
                                            //if (LangId == 1)
                                            //    MessageDTO.Message = "The validity of passport should be more than 6 months from the exhibition starting date";// _stringLocalizer["InvalidExhibitionStartorExpiryDate"];
                                            //else
                                            //    MessageDTO.Message = "يجب أن تكون صلاحية جواز السفر أكثر من 6 أشهر من تاريخ بدء المعرض";

                                            if (LangId == 1)
                                                MessageDTO.Message = "Passport should have at least 6 months validity.";
                                            else
                                                MessageDTO.Message = "يجب ان تكون صلاحية الجواز 6 شهور أو اكثر.";

                                            MessageDTO.MessageTypeResponse = "Error";
                                        }
                                        #endregion
                                        //if (isupload || isvalidupload)
                                        //{
                                        //    #region Visa Required
                                        //    if (!MethodFactory.IsDateExpired(StaffGuestDTO.PassportExpiryDate.Trim(), currentExhibition.StartDate, Convert.ToInt32(currentExhibition.ExpiryMonths)))
                                        //    {
                                        //        if (StaffGuestDTO.IsEditClicked && StaffGuestDTO.RepresentativeId > 0)
                                        //            UpdateContent(StaffGuestDTO, LangId);
                                        //        else
                                        //        {
                                        //            //SelectTab1();
                                        //            if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64Passport) && !string.IsNullOrEmpty(StaffGuestDTO.FileBase64PersonalPhoto) && !string.IsNullOrEmpty(StaffGuestDTO.FileBase64Visa))
                                        //            {
                                        //                if (StaffGuestDTO.RepresentativeId == 0)
                                        //                    SaveContent(StaffGuestDTO, LangId);
                                        //            }
                                        //            else
                                        //            {
                                        //                if (LangId == 1)
                                        //                    MessageDTO.Message = "Please upload passport copy, personal photo,visa application form then try submitting again.";
                                        //                else
                                        //                    MessageDTO.Message = "Please upload passport copy, personal photo,visa application form then try submitting again.";
                                        //                MessageDTO.MeesageTypeResponse = "Error";
                                        //            }
                                        //        }
                                        //        //BindRepresentative();
                                        //    }
                                        //    else
                                        //    if (currentExhibition.StartDate != null && !string.IsNullOrEmpty(StaffGuestDTO.PassportExpiryDate.Trim()))
                                        //    {
                                        //        if (Convert.ToDateTime(StaffGuestDTO.PassportExpiryDate.Trim()) <= currentExhibition.StartDate)
                                        //        {
                                        //            if (LangId == 1)
                                        //                MessageDTO.Message = "Passport expires before or on the same day of the start of exhibition. Please provide a valid passport expiry date."; // _stringLocalizer["PassportExpiry"];
                                        //            else
                                        //                MessageDTO.Message = "Passport expires before or on the same day of the start of exhibition. Please provide a valid passport expiry date."; // _stringLocalizer["PassportExpiry"];
                                        //            MessageDTO.MeesageTypeResponse = "Error";
                                        //        }
                                        //        else
                                        //        {
                                        //            if (LangId == 1)
                                        //                MessageDTO.Message = "The validity of passport should be more than 6 months from the exhibition starting date";// _stringLocalizer["InvalidExhibitionStartorExpiryDate"];
                                        //            else
                                        //                MessageDTO.Message = "يجب أن تكون صلاحية جواز السفر أكثر من 6 أشهر من تاريخ بدء المعرض";
                                        //            MessageDTO.MeesageTypeResponse = "Error";
                                        //        }
                                        //    }
                                        //    else
                                        //    {
                                        //        if (LangId == 1)
                                        //            MessageDTO.Message = "The validity of passport should be more than 6 months from the exhibition starting date";// _stringLocalizer["InvalidExhibitionStartorExpiryDate"];
                                        //        else
                                        //            MessageDTO.Message = "يجب أن تكون صلاحية جواز السفر أكثر من 6 أشهر من تاريخ بدء المعرض";
                                        //        MessageDTO.MeesageTypeResponse = "Error";
                                        //    }
                                        //    #endregion
                                        //}
                                        //else
                                        //{
                                        //    if (LangId == 1)
                                        //        MessageDTO.Message = "Invalid file format for Passport Copy. File type should be either pdf/gif/jpg/jpeg/png/tif/tiff format";// _stringLocalizer["InvalidImgPDF"];
                                        //    else
                                        //        MessageDTO.Message = "Invalid file format for Passport Copy. File type should be either pdf/gif/jpg/jpeg/png/tif/tiff format";// _stringLocalizer["InvalidImgPDF"];
                                        //    MessageDTO.MeesageTypeResponse = "Error";
                                        //}
                                    }
                                    else
                                    {
                                        if (LangId == 1)
                                            MessageDTO.Message = "Invalid file format. File type should be either gif/jpg/jpeg/png format";// _stringLocalizer["InvalidImgPDF"];
                                        else
                                            MessageDTO.Message = "Invalid file format. File type should be either gif/jpg/jpeg/png format";// _stringLocalizer["InvalidImgPDF"];
                                        MessageDTO.MessageTypeResponse = "Error";
                                    }
                                }
                                else
                                {
                                    if (LangId == 1)
                                        MessageDTO.Message = "Invalid file format for Passport Copy. File type should be either pdf/gif/jpg/jpeg/png/tif/tiff format";// _stringLocalizer["InvalidImgPDF"];
                                    else
                                        MessageDTO.Message = "Invalid file format for Passport Copy. File type should be either pdf/gif/jpg/jpeg/png/tif/tiff format";// _stringLocalizer["InvalidImgPDF"];
                                    MessageDTO.MessageTypeResponse = "Error";
                                }
                            }
                            else
                            {
                                if (LangId == 1)
                                    MessageDTO.Message = "Invalid file format. File type should be either pdf/xls/xlsx/doc/docx/gif/jpg/jpeg/png format"; //_stringLocalizer["InvalidFileFormat1"];
                                else
                                    MessageDTO.Message = "Invalid file format. File type should be either pdf/xls/xlsx/doc/docx/gif/jpg/jpeg/png format"; //_stringLocalizer["InvalidFileFormat1"];
                                MessageDTO.MessageTypeResponse = "Error";
                            }
                        }
                        else
                        {
                            #region Visa Not Required
                            if (StaffGuestDTO.IsEditClicked)
                                UpdateContent(StaffGuestDTO, LangId);
                            else
                                SaveContent(StaffGuestDTO, LangId);

                            //BindRepresentative();
                            #endregion
                        }
                        //imgSpinnerRep.Style["display"] = "none";
                        //SelectTab1();
                    }
                    return Ok(MessageDTO);
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside SaveData action: {ex.InnerException}");
                MessageDTO.MessageTypeResponse = "Error";
                MessageDTO.Message = "Something went wrong. Please try again later.";
                return Ok(MessageDTO);
            }
        }

        private bool IsVisaRequired(long nationalityId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var country = _context.XsiExhibitionCountry.Where(i => i.CountryId == nationalityId).FirstOrDefault();
                if (country != null)
                {
                    return country.IsVisaRequired == EnumConversion.ToString(EnumBool.Yes) ? true : false;
                }
            }
            return false;
        }
        private bool IsVisaNotRequired(long nationalityId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var country = _context.XsiExhibitionCountry.Where(i => i.CountryId == nationalityId).FirstOrDefault();
                if (country != null)
                {
                    return country.IsVisaRequired != EnumConversion.ToString(EnumBool.Yes) ? true : false;
                }
            }
            return false;
        }

        private ActionResult<dynamic> UpdateStaffGuestFromSaveData(SaveStaffGuestDTO StaffGuestDTO)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                MessageDTO = new MessageDTO();
                SIBFMemberId = StaffGuestDTO.MemberId;
                long memberId = User.Identity.GetID();
                if (memberId == SIBFMemberId)
                {
                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        var currentExhibition = MethodFactory.GetActiveExhibition(StaffGuestDTO.WebsiteId, _context);
                        if (StaffGuestDTO.IsVisaNeed == "Y")
                        {
                            #region Visa Required
                            if (StaffGuestDTO.PassportExpiryDate != null && !MethodFactory.IsDateExpired(StaffGuestDTO.PassportExpiryDate.Trim(), currentExhibition.StartDate, Convert.ToInt32(currentExhibition.ExpiryMonths)))
                            {
                                UpdateContent(StaffGuestDTO, LangId);
                                //BindRepresentative();
                            }
                            else if (currentExhibition.StartDate != null && StaffGuestDTO.PassportExpiryDate != null && !string.IsNullOrEmpty(StaffGuestDTO.PassportExpiryDate.Trim()))
                            {
                                if (StaffGuestDTO.PassportExpiryDate != null && (Convert.ToDateTime(StaffGuestDTO.PassportExpiryDate.Trim()) <= currentExhibition.StartDate))
                                {
                                    //if (LangId == 1)
                                    //    MessageDTO.Message = "Passport expires before or on the same day of the start of exhibition. Please provide a valid passport expiry date."; // _stringLocalizer["PassportExpiry"];
                                    //else
                                    //    MessageDTO.Message = "Passport expires before or on the same day of the start of exhibition. Please provide a valid passport expiry date."; // _stringLocalizer["PassportExpiry"];
                                    if (LangId == 1)
                                        MessageDTO.Message = "Passport should have at least 6 months validity.";
                                    else
                                        MessageDTO.Message = "يجب ان تكون صلاحية الجواز 6 شهور أو اكثر.";
                                    MessageDTO.MessageTypeResponse = "Error";
                                }
                                else
                                {
                                    //if (LangId == 1)
                                    //    MessageDTO.Message = "The validity of passport should be more than 6 months from the exhibition starting date";// _stringLocalizer["InvalidExhibitionStartorExpiryDate"];
                                    //else
                                    //    MessageDTO.Message = "يجب أن تكون صلاحية جواز السفر أكثر من 6 أشهر من تاريخ بدء المعرض";
                                    if (LangId == 1)
                                        MessageDTO.Message = "Passport should have at least 6 months validity.";
                                    else
                                        MessageDTO.Message = "يجب ان تكون صلاحية الجواز 6 شهور أو اكثر.";
                                    MessageDTO.MessageTypeResponse = "Error";
                                }
                            }
                            else
                            {
                                //if (LangId == 1)
                                //    MessageDTO.Message = "The validity of passport should be more than 6 months from the exhibition starting date";// _stringLocalizer["InvalidExhibitionStartorExpiryDate"];
                                //else
                                //    MessageDTO.Message = "يجب أن تكون صلاحية جواز السفر أكثر من 6 أشهر من تاريخ بدء المعرض";
                                if (LangId == 1)
                                    MessageDTO.Message = "Passport should have at least 6 months validity.";
                                else
                                    MessageDTO.Message = "يجب ان تكون صلاحية الجواز 6 شهور أو اكثر.";
                                MessageDTO.MessageTypeResponse = "Error";
                            }
                            #endregion
                        }
                        else
                        {
                            #region Visa Not Required
                            UpdateContent(StaffGuestDTO, LangId);
                            //BindRepresentative();
                            #endregion
                        }
                        //imgSpinnerRep.Style["display"] = "none";
                        //SelectTab1();
                    }
                    return Ok(MessageDTO);
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Update Data action: {ex.InnerException}");
                MessageDTO.MessageTypeResponse = "Error";
                MessageDTO.Message = "Something went wrong. Please try again later.";
                return Ok(MessageDTO);
            }
        }
        private bool IsShowBookingLink(long exhibitionid, long staffGuestid, string staffgueststatus, string needvisa)
        {
            using (ExhibitionStaffGuestParticipatingService StaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService())
            {
                ExhibitionService ExhibitionService = new ExhibitionService();
                var exhibition = ExhibitionService.GetExhibitionByItemId(exhibitionid);
                if (exhibition != null)
                {
                    if (exhibition.StaffGuestBookingExpiryDate == null)
                    {
                        if (needvisa == EnumConversion.ToString(EnumBool.No))
                        {
                            return true;
                        }
                        else if (staffgueststatus == EnumConversion.ToString(EnumStaffGuestStatus.NoVisaInsuranceUploaded) || staffgueststatus == EnumConversion.ToString(EnumStaffGuestStatus.VisaUploaded))
                        {
                            //staffgueststatus == EnumConversion.ToString(EnumStaffGuestStatus.Approved) || 
                            return true;
                        }
                    }
                    else
                    {
                        var dt = exhibition.StaffGuestBookingExpiryDate.Value;
                        var bookingexpirydate = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 0);
                        if (MethodFactory.ArabianTimeNow() <= bookingexpirydate)
                        {
                            if (needvisa == EnumConversion.ToString(EnumBool.No))
                            {
                                return true;
                            }
                            else if (staffgueststatus == EnumConversion.ToString(EnumStaffGuestStatus.NoVisaInsuranceUploaded) || staffgueststatus == EnumConversion.ToString(EnumStaffGuestStatus.VisaUploaded))
                            {
                                //staffgueststatus == EnumConversion.ToString(EnumStaffGuestStatus.Approved) ||
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        private string GetStaffGuestName(long StaffGuestId, long exhibitionId, long langId)
        {
            using (ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService())
            {
                ExhibitionStaffGuestParticipatingService ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService();
                var entity = ExhibitionStaffGuestService.GetStaffGuestByItemId(StaffGuestId);
                if (entity != null)
                {
                    var staffguestParticipating = ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(new XsiExhibitionStaffGuestParticipating() { ExhibitionId = exhibitionId, StaffGuestId = StaffGuestId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                    if (staffguestParticipating != null)
                        return langId == 1 ? staffguestParticipating.NameEn : staffguestParticipating.NameAr;
                }
                return string.Empty;
            }
        }
        private string GetStaffGuestLastName(long StaffGuestId, long exhibitionId, long langId)
        {
            using (ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService())
            {
                ExhibitionStaffGuestParticipatingService ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService();
                var entity = ExhibitionStaffGuestService.GetStaffGuestByItemId(StaffGuestId);
                if (entity != null)
                {
                    var staffguestParticipating = ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(new XsiExhibitionStaffGuestParticipating() { ExhibitionId = exhibitionId, StaffGuestId = StaffGuestId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                    if (staffguestParticipating != null)
                        return langId == 1 ? staffguestParticipating.LastName : string.Empty;
                }
                return string.Empty;
            }
        }

        SaveStaffGuestDTO LoadContent(long StaffGuestId, long exhibitionId, long langid)
        {
            SaveStaffGuestDTO StaffGuestDTO = new SaveStaffGuestDTO();
            using (ExhibitionStaffGuestParticipatingService StaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService())
            {
                XsiExhibitionStaffGuestParticipating staffGuestParticipating = StaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(new XsiExhibitionStaffGuestParticipating() { StaffGuestId = StaffGuestId, ExhibitionId = exhibitionId }).FirstOrDefault();
                if (staffGuestParticipating != null)
                {
                    ExhibitionStaffGuestService StaffGuestService = new ExhibitionStaffGuestService();
                    XsiExhibitionStaffGuest staffGuest = StaffGuestService.GetStaffGuestByItemId(StaffGuestId);
                    StaffGuestDTO.StaffGuestId = staffGuest.StaffGuestId;
                    if (staffGuest != null)
                    {
                        if (staffGuest.Dob != null)
                            StaffGuestDTO.DateofBirth = staffGuest.Dob.Value.ToString("MM/dd/yyyy");
                        if (staffGuest.PlaceOfBirth != null)
                            StaffGuestDTO.PlaceOfBirth = staffGuest.PlaceOfBirth;
                    }

                    if (!string.IsNullOrEmpty(staffGuestParticipating.IsUaeresident))
                        StaffGuestDTO.IsUAEResident = staffGuestParticipating.IsUaeresident;
                    else
                        StaffGuestDTO.IsUAEResident = EnumConversion.ToString(EnumBool.No);

                    //ShowVisaControls();
                    //StaffGuestDTO.ShowVisaControls = true;
                    // StaffGuestDTO.VisaNoteHref = "/Content/Uploads/StaffGuest/VisaForm/VisaInformationFormEnglish.xlsx";
                    if (staffGuestParticipating.Profession != null)
                        StaffGuestDTO.Profession = staffGuestParticipating.Profession;
                    if (staffGuestParticipating.CountryId != null)
                        StaffGuestDTO.NationalityId = staffGuestParticipating.CountryId.Value;

                    /*if (entity.CountryId == 1 || entity.CountryId == 98 || entity.CountryId == 99 || entity.CountryId == 160 || entity.CountryId == 221)
                    {
                        StaffGuestDTO.CountryUIDNoVisibility = true;
                    }
                    else
                        StaffGuestDTO.CountryUIDNoVisibility = false;

                    if (entity.CountryUidno != null)
                    {
                        StaffGuestDTO.CountryUidnoHrefVisibility = true;
                        StaffGuestDTO.CountryUidnoHref = ".." + CountryUIDPath + entity.CountryUidno;
                    }
                    else
                        StaffGuestDTO.CountryUidnoHrefVisibility = false;*/

                    if (staffGuestParticipating.NameEn != null)
                        StaffGuestDTO.NameEn = staffGuestParticipating.NameEn;
                    if (staffGuestParticipating.LastName != null)
                        StaffGuestDTO.LastName = staffGuestParticipating.LastName;
                    if (staffGuestParticipating.NameAr != null)
                        StaffGuestDTO.NameAr = staffGuestParticipating.NameAr;
                    if (staffGuestParticipating.Profession != null)
                        StaffGuestDTO.Profession = staffGuestParticipating.Profession;
                    if (staffGuestParticipating.IsVisitedUae != null)
                        StaffGuestDTO.IsVisitedUae = staffGuestParticipating.IsVisitedUae;

                    #region Phone,Mobile
                    if (staffGuestParticipating.Telephone != null)
                    {
                        string[] str = staffGuestParticipating.Telephone.Split('$');
                        if (str.Count() == 3)
                        {
                            StaffGuestDTO.PhoneISD = str[0].Trim();
                            StaffGuestDTO.PhoneSTD = str[1].Trim();
                            StaffGuestDTO.Phone = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            StaffGuestDTO.PhoneSTD = str[0].Trim();
                            StaffGuestDTO.Phone = str[1].Trim();
                        }
                        else
                            StaffGuestDTO.Phone = str[0].Trim();
                    }
                    if (staffGuestParticipating.Mobile != null)
                    {
                        string[] str = staffGuestParticipating.Mobile.Split('$');
                        if (str.Count() == 3)
                        {
                            StaffGuestDTO.MobileISD = str[0].Trim();
                            StaffGuestDTO.MobileSTD = str[1].Trim();
                            StaffGuestDTO.Mobile = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            StaffGuestDTO.MobileSTD = str[0].Trim();
                            StaffGuestDTO.Mobile = str[1].Trim();
                        }
                        else
                            StaffGuestDTO.Mobile = str[0].Trim();
                    }
                    #endregion
                    #region Visa
                    if (staffGuestParticipating.NeedVisa != null)
                    {
                        //ShowVisaControls();
                        if (staffGuestParticipating.NeedVisa == EnumConversion.ToString(EnumBool.Yes))
                        {
                            StaffGuestDTO.IsVisaNeed = EnumConversion.ToString(EnumBool.Yes);

                        }
                        else
                        {
                            StaffGuestDTO.IsVisaNeed = EnumConversion.ToString(EnumBool.No);
                        }
                    }
                    else
                    {
                        StaffGuestDTO.IsVisaNeed = EnumConversion.ToString(EnumBool.No);
                    }
                    //rbtnYesRep.Checked = true;
                    //rbtnNoRep.Checked = false;
                    if (staffGuestParticipating.Passport != null)
                        StaffGuestDTO.PassportNumber = staffGuestParticipating.Passport;
                    if (staffGuestParticipating.PlaceOfIssue != null)
                        StaffGuestDTO.PlaceOfIssue = staffGuestParticipating.PlaceOfIssue;
                    if (staffGuestParticipating.DateOfIssue != null)
                        StaffGuestDTO.PassportDateOfIssue = staffGuestParticipating.DateOfIssue.Value.ToString("MM/dd/yyyy");

                    if (staffGuestParticipating.ExpiryDate != null)
                        StaffGuestDTO.PassportExpiryDate = staffGuestParticipating.ExpiryDate.Value.ToString("MM/dd/yyyy");

                    if (staffGuestParticipating.PassportCopy != null)
                        if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PassportPath + staffGuestParticipating.PassportCopy))
                            StaffGuestDTO.PassportHRef = _appCustomSettings.UploadsCMSPath + "/StaffGuest/Passport/" + staffGuestParticipating.PassportCopy;

                    if (staffGuestParticipating.PassportCopyTwo != null)
                        if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PassportPath + staffGuestParticipating.PassportCopyTwo))
                            StaffGuestDTO.PassportCopy2Href = _appCustomSettings.UploadsCMSPath + "/StaffGuest/Passport/" + staffGuestParticipating.PassportCopyTwo;

                    if (staffGuestParticipating.VisaApplicationForm != null)
                        if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + VisaFormPath + staffGuestParticipating.VisaApplicationForm))
                            StaffGuestDTO.VisaHref = _appCustomSettings.UploadsCMSPath + "/StaffGuest/VisaForm/" + staffGuestParticipating.VisaApplicationForm;

                    if (staffGuestParticipating.Photo != null)
                        if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PhotoPath + staffGuestParticipating.Photo))
                            StaffGuestDTO.PhotoHref = _appCustomSettings.UploadsCMSPath + "/StaffGuest/Photo/" + staffGuestParticipating.Photo;

                    if (staffGuestParticipating.CountryUidno != null)
                        if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + CountryUIDPath + staffGuestParticipating.CountryUidno))
                            StaffGuestDTO.CountryUidnoHref = _appCustomSettings.UploadsCMSPath + "/StaffGuest/CountryUID/" + staffGuestParticipating.CountryUidno;

                    //if (entity.FlightTickets != null)
                    //{
                    //    RepresentativeDTO.FlightTicketsHrefVisibility = true;
                    //    RepresentativeDTO.FlightTicketsHref = ".." + FlightTicketsPath + entity.FlightTickets;
                    //}
                    //else
                    //    RepresentativeDTO.FlightTicketsHrefVisibility = false;
                    #endregion

                    StaffGuestDTO.IsCompanion = staffGuestParticipating.IsCompanion;
                    if (staffGuestParticipating.IsCompanion != null && staffGuestParticipating.IsCompanion == EnumConversion.ToString(EnumBool.Yes))
                    {
                        if (staffGuestParticipating.MainGuestId != null)
                            StaffGuestDTO.MainGuestId = staffGuestParticipating.MainGuestId;
                        StaffGuestDTO.MainGuest = staffGuestParticipating.MainGuest;
                        //entity.HealthInsurance
                        //if (ddlMembersAddedByStaff.Items.FindByValue(entity.ItemId.ToString()) != null)
                        //    ddlMembersAddedByStaff.SelectedValue = entity.ItemId.ToString();
                    }
                    else
                    {
                        //divMembersAddedByStaff.Visible = false;
                    }
                    StaffGuestDTO.CompanionDescription = staffGuestParticipating.CompanionDescription;

                    if (staffGuestParticipating.ActivityDate != null)
                        StaffGuestDTO.ActivityDate = staffGuestParticipating.ActivityDate.Value.ToString("MM/dd/yyyy");

                    if (!string.IsNullOrEmpty(staffGuestParticipating.NoofVisaMonths))
                        StaffGuestDTO.VisaNoOfMonths = staffGuestParticipating.NoofVisaMonths;

                    if (!string.IsNullOrEmpty(staffGuestParticipating.PaidVisa))
                        StaffGuestDTO.IsPaidVisa = staffGuestParticipating.PaidVisa;
                    else
                        StaffGuestDTO.IsPaidVisa = EnumConversion.ToString(EnumBool.No);

                    StaffGuestDTO.GuestType = staffGuestParticipating.GuestType;
                    StaffGuestDTO.GuestCategory = staffGuestParticipating.GuestCategory;
                }
                return StaffGuestDTO;
            }
        }

        bool RemoveStaffGuest(RemoveCommandStaffGuestDTO removeCommandDTO, long memberid, long langid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                using (ExhibitionStaffGuestParticipatingService StaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService())
                {
                    StringBuilder sbchanges = new StringBuilder();
                    HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                    var staffGuestParticipatingOld = new XsiExhibitionStaffGuestParticipating();
                    staffGuestParticipatingOld = StaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(new XsiExhibitionStaffGuestParticipating() { StaffGuestId = removeCommandDTO.StaffGuestId, ExhibitionId = removeCommandDTO.ExhibitionId }).FirstOrDefault();

                    var staffGuestParticipating = new XsiExhibitionStaffGuestParticipating();
                    staffGuestParticipating = StaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(new XsiExhibitionStaffGuestParticipating() { StaffGuestId = removeCommandDTO.StaffGuestId, ExhibitionId = removeCommandDTO.ExhibitionId }).FirstOrDefault();
                    if (staffGuestParticipating != default(XsiExhibitionStaffGuestParticipating))
                    {
                        staffGuestParticipating.Status = EnumConversion.ToString(EnumStaffGuestStatus.Cancelled);
                        if (StaffGuestParticipatingService.UpdateExhibitionStaffGuestParticipating(staffGuestParticipating) == EnumResultType.Success)
                        {
                            sbchanges = GetStaffGuestUpdateChanges(staffGuestParticipatingOld, staffGuestParticipating);
                            if (removeCommandDTO.Commandname == "participating")
                            {
                                #region Send Email to SIBF
                                StringBuilder body1 = new StringBuilder();
                                #region Email Content
                                EmailContentService EmailContentService = new EmailContentService();
                                SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                                if (removeCommandDTO.WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                                {
                                    ScrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(10011);
                                    if (ScrfEmailContent != null)
                                    {
                                        if (ScrfEmailContent.Body != null)
                                            StrEmailContentBody = ScrfEmailContent.Body;
                                        if (ScrfEmailContent.Email != null)
                                            StrEmailContentEmail = ScrfEmailContent.Email;
                                        if (ScrfEmailContent.Subject != null)
                                            StrEmailContentSubject = ScrfEmailContent.Subject;
                                        StrEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                    }
                                }
                                else
                                {
                                    EmailContent = EmailContentService.GetEmailContentByItemId(10011);
                                    if (EmailContent != null)
                                    {
                                        if (EmailContent.Body != null)
                                            StrEmailContentBody = EmailContent.Body;
                                        if (EmailContent.Email != null)
                                            StrEmailContentEmail = EmailContent.Email;
                                        if (EmailContent.Subject != null)
                                            StrEmailContentSubject = EmailContent.Subject;
                                        StrEmailContentAdmin = _appCustomSettings.AdminName;
                                    }
                                }
                                #endregion
                                //body1.Append(BindEmailContent(EnglishId, strEmailContentBody, null, ServerAddress));
                                body1.Append(htmlContentFactory.BindEmailContent(1, StrEmailContentBody, null, _appCustomSettings.ServerAddressNew, removeCommandDTO.WebsiteId, _appCustomSettings));

                                if (!string.IsNullOrEmpty(staffGuestParticipating.NameEn) && !string.IsNullOrEmpty(staffGuestParticipating.LastName))
                                    body1.Replace("$$RepresentativeName$$", staffGuestParticipating.NameEn);
                                else if (!string.IsNullOrEmpty(staffGuestParticipating.NameEn))
                                    body1.Replace("$$RepresentativeName$$", staffGuestParticipating.NameEn);
                                else if (!string.IsNullOrEmpty(staffGuestParticipating.LastName))
                                    body1.Replace("$$RepresentativeName$$", staffGuestParticipating.LastName);
                                else
                                    body1.Replace("$$RepresentativeName$$", string.Empty);

                                if (StrEmailContentEmail != string.Empty)
                                    _emailSender.SendEmail(StrEmailContentAdmin, _appCustomSettings.AdminEmail, StrEmailContentEmail, StrEmailContentSubject, body1.ToString());
                                #endregion
                            }
                            NotifyAdminAboutBooking(memberid, staffGuestParticipating.StaffGuestId, staffGuestParticipating.ExhibitionId, removeCommandDTO.WebsiteId, false, sbchanges, 1);
                            return true;
                            //BindRepresentative();
                        }
                    }
                }
            }
            return false;
        }

        EnumResultType UpdateExhibitionStaffGuestFromTabModify(XsiExhibitionStaffGuestParticipating entity, SaveStaffGuestDTO dto)
        {
            using (ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService())
            {
                var entity1 = ExhibitionStaffGuestService.GetStaffGuestByItemId(entity.StaffGuestId);
                if (entity1 != null)
                {
                    DateTime dob = new DateTime();
                    DateTime.TryParse(dto.DateofBirth, out dob);

                    entity1.NameEn = entity.NameEn;
                    entity1.LastName = entity.LastName;
                    entity1.NameAr = entity.NameAr;

                    if (!string.IsNullOrEmpty(dto.DateofBirth))
                        entity1.Dob = dob;
                    entity1.PlaceOfBirth = dto.PlaceOfBirth;

                    entity1.ModifiedOn = MethodFactory.ArabianTimeNow();
                    entity1.ModifiedBy = dto.MemberId;
                    return ExhibitionStaffGuestService.UpdateExhibitionStaffGuest(entity1);
                }
            }
            return EnumResultType.Failed;
        }

        protected bool IsVisaApplicationFormValid(string fileExtenstion, bool hrefVisibility)
        {
            if (!string.IsNullOrEmpty(fileExtenstion))
            {
                if (MethodFactory.IsValidFile1(fileExtenstion))
                    return true;
            }
            else if (hrefVisibility)
                return true;

            return false;
        }

        protected bool IsPasportCopyValid(string fileExtenstion, bool hrefVisibility)
        {
            if (!string.IsNullOrEmpty(fileExtenstion))
            {
                if (MethodFactory.IsValidImgAndPDF(fileExtenstion))
                    return true;
            }
            else if (hrefVisibility)
                return true;

            return false;
        }

        protected bool IsPersonalPhotoValid(string fileExtenstion, bool hrefVisibility)
        {
            if (!string.IsNullOrEmpty(fileExtenstion))
            {
                if (MethodFactory.IsValidImage(fileExtenstion))
                    return true;
            }
            else if (hrefVisibility)
                return true;

            return false;
        }

        private long SaveContent(SaveStaffGuestDTO StaffGuestDTO, long langid)
        {
            CommonHelperForRepresentative commonHelper = new CommonHelperForRepresentative();

            EnumResultType result = EnumResultType.Failed;
            bool isunique = true;
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {
                using (ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService())
                {
                    var currentExhibition = MethodFactory.GetExhibition(StaffGuestDTO.WebsiteId);
                    if (currentExhibition != null)
                    {
                        isunique = commonHelper.IsUniqueStaffguestForAdd(StaffGuestDTO, langid, currentExhibition.ExhibitionId);
                        if (isunique)
                        {
                            var exhibition = MethodFactory.GetExhibition(StaffGuestDTO.WebsiteId);
                            StaffGuestDTO.ExhibitionId = exhibition.ExhibitionId;
                            DateTime dob = new DateTime();
                            DateTime.TryParse(StaffGuestDTO.DateofBirth, out dob);

                            XsiExhibitionStaffGuest entity = new XsiExhibitionStaffGuest();
                            entity.NameEn = StaffGuestDTO.NameEn;
                            entity.LastName = StaffGuestDTO.LastName;
                            entity.NameAr = StaffGuestDTO.NameAr;
                            entity.MemberId = StaffGuestDTO.MemberId;
                            if (!string.IsNullOrEmpty(StaffGuestDTO.DateofBirth))
                                entity.Dob = dob;
                            entity.PlaceOfBirth = StaffGuestDTO.PlaceOfBirth;
                            entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                            //entity.StaffGuestId = StaffGuestDTO.StaffGuestId;
                            //entity.ExhibitionId = ExhibitionGroupId;
                            entity.CreatedOn = MethodFactory.ArabianTimeNow();
                            entity.CreatedBy = StaffGuestDTO.MemberId;
                            entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                            entity.ModifiedBy = StaffGuestDTO.MemberId;
                            result = ExhibitionStaffGuestService.InsertExhibitionStaffGuest(entity);
                            if (result == EnumResultType.Success)
                            {
                                string filename = string.Empty;
                                using (ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService())
                                {
                                    #region check exhibition active
                                    #region Insert
                                    XsiExhibitionStaffGuestParticipating spentity = new XsiExhibitionStaffGuestParticipating();
                                    spentity.StaffGuestId = ExhibitionStaffGuestService.XsiItemdId;
                                    spentity.ExhibitionId = StaffGuestDTO.ExhibitionId;
                                    if (StaffGuestDTO.NationalityId > 0)
                                        spentity.CountryId = StaffGuestDTO.NationalityId;
                                    spentity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                    spentity.IsEmailSent = EnumConversion.ToString(EnumBool.No);
                                    spentity.IsFromPcregistration = EnumConversion.ToString(EnumBool.No);

                                    if (!string.IsNullOrEmpty(StaffGuestDTO.IsUAEResident) && StaffGuestDTO.IsUAEResident != "undefined")
                                        spentity.IsUaeresident = StaffGuestDTO.IsUAEResident;
                                    else
                                        spentity.IsUaeresident = EnumConversion.ToString(EnumBool.No);

                                    if (StaffGuestDTO.IsVisaNeed == EnumConversion.ToString(EnumBool.No))
                                    {
                                        //spentity.Status = EnumConversion.ToString(EnumStaffGuestStatus.Approved);
                                        spentity.Status = EnumConversion.ToString(EnumStaffGuestStatus.Book);
                                    }
                                    else
                                        spentity.Status = EnumConversion.ToString(EnumStaffGuestStatus.Pending);

                                    spentity.LanguageId = langid;

                                    if (!string.IsNullOrEmpty(StaffGuestDTO.NameEn))
                                        spentity.NameEn = StaffGuestDTO.NameEn.Trim();

                                    if (!string.IsNullOrEmpty(StaffGuestDTO.LastName))
                                        spentity.LastName = StaffGuestDTO.LastName.Trim();

                                    if (!string.IsNullOrEmpty(StaffGuestDTO.NameAr))
                                        spentity.NameAr = StaffGuestDTO.NameAr.Trim();

                                    if (!string.IsNullOrEmpty(StaffGuestDTO.Profession))
                                        spentity.Profession = StaffGuestDTO.Profession.Trim();

                                    if (!string.IsNullOrEmpty(StaffGuestDTO.IsVisitedUae) && StaffGuestDTO.IsVisitedUae != "undefined")
                                        spentity.IsVisitedUae = StaffGuestDTO.IsVisitedUae;
                                    else
                                        spentity.IsVisitedUae = "N";

                                    spentity.Telephone = StaffGuestDTO.PhoneISD + "$" + StaffGuestDTO.PhoneSTD + "$" + StaffGuestDTO.Phone;
                                    spentity.Mobile = StaffGuestDTO.MobileISD + "$" + StaffGuestDTO.MobileSTD + "$" + StaffGuestDTO.Mobile;
                                    spentity.NeedVisa = StaffGuestDTO.IsVisaNeed == EnumConversion.ToString(EnumBool.Yes) ? EnumConversion.ToString(EnumBool.Yes) : EnumConversion.ToString(EnumBool.No);

                                    //if (!string.IsNullOrEmpty(StaffGuestDTO.FileExtensionofCountryUID))
                                    //{
                                    //    filename = string.Format("{0}_{1}{2}", 1, MethodFactory.GetRandomNumber(), StaffGuestDTO.FileExtensionofPersonalPhoto);
                                    //    filename = SaveImage(StaffGuestDTO.FileBase64CountryUID, StaffGuestDTO.FileExtensionofCountryUID, StaffGuestDTO.NameEn, CountryUIDPath, StaffGuestDTO.FileExtensionofCountryUID);
                                    //    spentity.CountryUidno = filename;
                                    //}

                                    //if (StaffGuestDTO.IsVisaNeed == "Y")
                                    //{
                                    if (!string.IsNullOrEmpty(StaffGuestDTO.PassportNumber))
                                        spentity.Passport = StaffGuestDTO.PassportNumber.Trim();
                                    if (!string.IsNullOrEmpty(StaffGuestDTO.PlaceOfIssue))
                                        spentity.PlaceOfIssue = StaffGuestDTO.PlaceOfIssue.Trim();
                                    if (!string.IsNullOrEmpty(StaffGuestDTO.PassportDateOfIssue))
                                        spentity.DateOfIssue = Convert.ToDateTime(StaffGuestDTO.PassportDateOfIssue.Trim());
                                    if (!string.IsNullOrEmpty(StaffGuestDTO.PassportExpiryDate))
                                        spentity.ExpiryDate = Convert.ToDateTime(StaffGuestDTO.PassportExpiryDate.Trim());

                                    if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64Passport))
                                    {
                                        ext = StaffGuestDTO.FileExtensionofPassport;
                                        filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                                        if ("." + ext.ToLower() == ".pdf" || ext.ToLower() == "pdf")
                                            filename = WriteTextOnPDF(StaffGuestDTO.FileBase64Passport, filename, StaffGuestDTO.NameEn, SIBFMemberId, ext);
                                        else
                                            filename = WriteTextOnImage(StaffGuestDTO.FileBase64Passport, filename, StaffGuestDTO.NameEn, SIBFMemberId, ext);
                                        spentity.PassportCopy = filename;
                                    }
                                    if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64Passport2))
                                    {
                                        ext = StaffGuestDTO.FileExtensionofPassport2;
                                        filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                                        if ("." + ext.ToLower() == ".pdf" || ext.ToLower() == "pdf")
                                            filename = WriteTextOnPDF(StaffGuestDTO.FileBase64Passport2, filename, StaffGuestDTO.NameEn, SIBFMemberId, ext);
                                        else
                                            filename = WriteTextOnImage(StaffGuestDTO.FileBase64Passport2, filename, StaffGuestDTO.NameEn, SIBFMemberId, ext);
                                        spentity.PassportCopyTwo = filename;
                                    }
                                    if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64PersonalPhoto))
                                    {
                                        // filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), StaffGuestDTO.FileExtensionofPersonalPhoto);
                                        filename = SaveImage(StaffGuestDTO.FileBase64PersonalPhoto, StaffGuestDTO.FileExtensionofPersonalPhoto, StaffGuestDTO.NameEn, PhotoPath, StaffGuestDTO.FileExtensionofPersonalPhoto);
                                        spentity.Photo = filename;
                                    }
                                    if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64Visa))
                                    {
                                        // filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), StaffGuestDTO.FileExtensionVisa);
                                        //FileUpload3.SaveAs(string.Format("{0}{1}", Server.MapPath(VisaFormPath), filename));
                                        filename = SaveImage(StaffGuestDTO.FileBase64Visa, StaffGuestDTO.FileExtensionVisa, StaffGuestDTO.NameEn, VisaFormPath, StaffGuestDTO.FileExtensionVisa);
                                        spentity.VisaApplicationForm = filename;
                                    }
                                    if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64CountryUID))
                                    {
                                        filename = SaveImage(StaffGuestDTO.FileBase64CountryUID, StaffGuestDTO.FileExtensionofCountryUID, StaffGuestDTO.NameEn, CountryUIDPath, StaffGuestDTO.FileExtensionofCountryUID);
                                        spentity.CountryUidno = filename;
                                    }
                                    if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64FlightTickets))
                                    {
                                        filename = SaveImage(StaffGuestDTO.FileBase64FlightTickets, StaffGuestDTO.FileExtensionofFlightTickets, StaffGuestDTO.NameEn, FlightTicketsPath, StaffGuestDTO.FileExtensionofFlightTickets);
                                        spentity.FlightTickets = filename;
                                    }
                                    //  }

                                    spentity.IsCompanion = StaffGuestDTO.IsCompanion;
                                    if (StaffGuestDTO.IsCompanion == "Y")
                                    {
                                        spentity.MainGuest = StaffGuestDTO.MainGuest;
                                        if (StaffGuestDTO.MainGuestId > 0)
                                            spentity.MainGuestId = StaffGuestDTO.MainGuestId;
                                        spentity.CompanionDescription = StaffGuestDTO.CompanionDescription;
                                    }
                                    if (!string.IsNullOrEmpty(StaffGuestDTO.ActivityDate))
                                        spentity.ActivityDate = Convert.ToDateTime(StaffGuestDTO.ActivityDate.Trim());

                                    spentity.GuestCategory = StaffGuestDTO.GuestCategory;
                                    spentity.GuestType = StaffGuestDTO.GuestType;

                                    if (!string.IsNullOrEmpty(StaffGuestDTO.VisaNoOfMonths))
                                        spentity.NoofVisaMonths = StaffGuestDTO.VisaNoOfMonths;

                                    if (!string.IsNullOrEmpty(StaffGuestDTO.IsPaidVisa) && StaffGuestDTO.IsPaidVisa != "undefined")
                                        spentity.PaidVisa = StaffGuestDTO.IsPaidVisa;
                                    spentity.CreatedOn = MethodFactory.ArabianTimeNow();
                                    spentity.CreatedBy = SIBFMemberId;
                                    spentity.ModifiedOn = MethodFactory.ArabianTimeNow();
                                    spentity.ModifiedBy = SIBFMemberId;

                                    result = ExhibitionStaffGuestParticipatingService.InsertExhibitionStaffGuestParticipating(spentity);
                                    #endregion
                                    if (result == EnumResultType.Success)
                                    {
                                        StringBuilder sb = new StringBuilder();
                                        sb = GetStaffGuestNewAddedInfo(spentity);
                                        NotifyAdminAboutBooking(StaffGuestDTO.MemberId, spentity.StaffGuestId, StaffGuestDTO.ExhibitionId, StaffGuestDTO.WebsiteId, true, sb, 1);
                                        //ResetControls();
                                        //representativeDTO.RepresentativeId = ExhibitionRepresentativeParticipatingService.XsiItemdId;
                                        //SaveToExhibitionRepresentative(representativeDTO);

                                        if (StaffGuestDTO.IsVisaNeed == EnumConversion.ToString(EnumBool.Yes))
                                        {
                                            if (langid == 1)
                                                MessageDTO.Message = "Staff Guest details have been sent for Admin’s approval. We will get back to you at our earliest.";
                                            else
                                                MessageDTO.Message = "تفاصيل المندوب الذي قمت بإضافته سيتم عرضه على الإدارة للموافقة. سيتم الرد عليك في أقرب وقت";//_stringLocalizer["RepresentativeInsertSucessfull"];
                                        }
                                        else
                                        {
                                            if (langid == 1)
                                                MessageDTO.Message = "Staff Guest details have been submitted successfuly. Please check in Guest List tab.";
                                            else
                                                MessageDTO.Message = "تمت إضافة معلومات الضيف بنجاح. يرجى التأكد من شاشة قائمة الضيوف";
                                        }
                                        MessageDTO.MessageTypeResponse = "Success";

                                        //pnlMessage.Visible = false;
                                        //mvRepresentativeForm.SetActiveView(vwThankYou);
                                        return entity.StaffGuestId;
                                    }
                                    else
                                    {
                                        if (langid == 1)
                                            MessageDTO.Message = "Failed to add staff guest, Please try adding again.";// _stringLocalizer["RepresentativeFailedToAdd"];
                                        else
                                            MessageDTO.Message = "لم يتم إضافة المندوب، الرجاء إعادة المحاولة";// _stringLocalizer["RepresentativeFailedToAdd"];

                                        MessageDTO.MessageTypeResponse = "Error";
                                    }
                                    #endregion
                                }
                            }
                            return 0;
                        }
                        else
                        {
                            if (langid == 1)
                                MessageDTO.Message = "Already Registered.";// _stringLocalizer["RepresentativeFailedToAdd"];
                            else
                                MessageDTO.Message = "لم يتم إضافة المندوب، الرجاء إعادة المحاولة";// _stringLocalizer["RepresentativeFailedToAdd"];

                            MessageDTO.MessageTypeResponse = "Error";
                        }
                        return 0;
                    }
                    else
                    {
                        if (langid == 1)
                            MessageDTO.Message = "Failed to add staff guest due to no exhibitionid, Please try adding again later.";// _stringLocalizer["RepresentativeFailedToAdd"];
                        else
                            MessageDTO.Message = "Failed to add staff guest due to no exhibitionid, Please try adding again later.";// _stringLocalizer["RepresentativeFailedToAdd"];

                        MessageDTO.MessageTypeResponse = "Error";
                        return 0;
                    }
                }
            }
        }

        string SaveImage(string fileBase64, string fileName, string name, string path, string fileExtension)
        {
            if (fileBase64 != null && fileBase64.Length > 0)
            {
                //string fileName = string.Empty;
                byte[] imageBytes;
                if (fileBase64.Contains("data:"))
                {
                    var strInfo = fileBase64.Split(",")[0];
                    imageBytes = Convert.FromBase64String(fileBase64.Split(',')[1]);
                }
                else
                {
                    imageBytes = Convert.FromBase64String(fileBase64);
                }

                fileName = string.Format("{0}_{1}_{2}{3}", name, 1, MethodFactory.GetRandomNumber(),
                    "." + fileExtension);
                System.IO.File.WriteAllBytes(
                    Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                 path) + fileName, imageBytes);

                return fileName;
            }
            return string.Empty;
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        [NonAction]
        public string WriteTextOnImage(string fileBase64, string fileName, string name, long exhibitorId, string ext)
        {
            fileName = SaveImage(fileBase64, fileName, name, PassportPathTemp, ext);
            Bitmap b = new Bitmap((Bitmap)System.Drawing.Image.FromFile(_appCustomSettings.UploadsPhysicalPath + PassportPathTemp + fileName));
            Graphics g = Graphics.FromImage(b);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            string StaffGuestName = string.Empty;
            if (name != null)
                StaffGuestName = name.Trim();

            string FileNumber = string.Empty;
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                var entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                if (entity != default(XsiExhibitionMemberApplicationYearly))
                {
                    if (entity.FileNumber != null)
                        if (!string.IsNullOrEmpty(entity.FileNumber))
                            FileNumber = entity.FileNumber;
                }
            }
            drawingFont f = new drawingFont("calibri", 14, FontStyle.Bold);
            g.DrawString("File Number: " + FileNumber, f, SystemBrushes.WindowText, new Point(20, b.Height - 30));
            g.DrawString("StaffGuest Name: " + StaffGuestName, f, SystemBrushes.WindowText, new Point(20, b.Height - 50));
            ext = "." + ext.ToLower();
            if (ext == ".png")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Png);
            else if (ext == ".jpg" || ext == ".jpeg")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Jpeg);
            else if (ext == ".gif")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Gif);
            else if (ext == ".bmp")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Bmp);
            else if (ext == ".tiff" || ext == ".tif")
                b.Save(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, ImageFormat.Tiff);
            b.Dispose();
            return fileName;
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        [NonAction]
        public string WriteTextOnPDF(string fileBase64, string fileName, string name, long memberid, string ext)
        {
            PdfContentByte cb;
            fileName = SaveImage(fileBase64, fileName, name, PassportPathTemp, ext);
            //PPUpload.SaveAs(string.Format("{0}{1}", Server.MapPath(PassportPathTemp), fileName));
            PdfReader reader = new PdfReader(ReadImage(fileBase64, fileName, name, PassportPathTemp));
            using (var fileStream = new FileStream(_appCustomSettings.UploadsPhysicalPath + PassportPath + fileName, FileMode.Create, FileAccess.Write))
            {
                var document = new Document(reader.GetPageSizeWithRotation(1));
                var writer = PdfWriter.GetInstance(document, fileStream);
                document.Open();
                for (var i = 1; i <= reader.NumberOfPages; i++)
                {
                    document.NewPage();
                    var baseFont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    var importedPage = writer.GetImportedPage(reader, i);
                    cb = writer.DirectContent;
                    cb.AddTemplate(importedPage, 0, 0);
                    string StaffGuestName = string.Empty;
                    if (name != null)
                        StaffGuestName = name.Trim();
                    string FileNumber = string.Empty;
                    using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
                    {
                        XsiExhibitionMember entity = ExhibitionMemberService.GetExhibitionMemberByItemId(SIBFMemberId);
                        if (entity != default(XsiExhibitionMember))
                        {
                            if (entity.FileNumber != null)
                                if (!string.IsNullOrEmpty(entity.FileNumber))
                                    FileNumber = entity.FileNumber;
                        }
                    }
                    string text = "Guest Name: " + StaffGuestName;
                    {
                        cb.BeginText();
                        cb.SetFontAndSize(baseFont, 12);
                        cb.SetTextMatrix(document.PageSize.GetLeft(26), document.PageSize.GetBottom(20));
                        cb.ShowText(text);
                        cb.EndText();
                    }
                    text = "File Number: " + FileNumber;
                    {
                        cb.BeginText();
                        cb.SetFontAndSize(baseFont, 12);
                        cb.SetTextMatrix(document.PageSize.GetLeft(26), document.PageSize.GetBottom(30));
                        cb.ShowText(text);
                        cb.EndText();
                    }
                }
                document.Close();
                writer.Close();
            }
            return fileName;
        }

        byte[] ReadImage(string fileBase64, string fileName, string name, string path)
        {
            byte[] responseBytes = null;
            if (fileBase64 != null && fileBase64.Length > 0)
            {
                //string fileName = string.Empty;
                //fileName = string.Format("{0}_{1}_{2}{3}", name, 1, MethodFactory.GetRandomNumber(),
                //    "." + fileExtension);
                responseBytes = System.IO.File.ReadAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                 path) + fileName);
            }
            return responseBytes;
        }

        private void UpdateContent(SaveStaffGuestDTO StaffGuestDTO, long langid)
        {
            CommonHelperForRepresentative commonHelper = new CommonHelperForRepresentative();
            string filename = string.Empty;
            StringBuilder sbchanges = new StringBuilder();
            var currentExhibition = MethodFactory.GetExhibition(StaffGuestDTO.WebsiteId);
            bool isunique = true;
            if (currentExhibition != null)
            {
                isunique = commonHelper.IsUniqueStaffguestForUpdate(StaffGuestDTO, langid, currentExhibition.ExhibitionId);
                if (isunique)
                {
                    using (ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService())
                    {
                        ExhibitionStaffGuestService = new ExhibitionStaffGuestService();
                        var staffGuest = ExhibitionStaffGuestService.GetStaffGuestByItemId(StaffGuestDTO.StaffGuestId);

                        var staffGuestParticipatingOld = ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(new XsiExhibitionStaffGuestParticipating() { StaffGuestId = StaffGuestDTO.StaffGuestId, ExhibitionId = StaffGuestDTO.ExhibitionId }).FirstOrDefault();
                        var staffGuestParticipating = ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(new XsiExhibitionStaffGuestParticipating() { StaffGuestId = StaffGuestDTO.StaffGuestId, ExhibitionId = StaffGuestDTO.ExhibitionId }).FirstOrDefault();

                        if (staffGuestParticipating != null)
                        {
                            staffGuestParticipating.CountryId = StaffGuestDTO.NationalityId;

                            if (StaffGuestDTO.NameEn != null)
                                staffGuestParticipating.NameEn = StaffGuestDTO.NameEn.Trim();
                            if (StaffGuestDTO.LastName != null)
                                staffGuestParticipating.LastName = StaffGuestDTO.LastName.Trim();
                            if (StaffGuestDTO.NameAr != null)
                                staffGuestParticipating.NameAr = StaffGuestDTO.NameAr.Trim();
                            if (StaffGuestDTO.Profession != null)
                                staffGuestParticipating.Profession = StaffGuestDTO.Profession.Trim();

                            if (!string.IsNullOrEmpty(StaffGuestDTO.IsVisitedUae) && StaffGuestDTO.IsVisitedUae != "undefined")
                                staffGuestParticipating.IsVisitedUae = StaffGuestDTO.IsVisitedUae;
                            else
                                staffGuestParticipating.IsVisitedUae = "N";

                            if (!string.IsNullOrEmpty(StaffGuestDTO.IsUAEResident) && StaffGuestDTO.IsUAEResident != "undefined")
                                staffGuestParticipating.IsUaeresident = StaffGuestDTO.IsUAEResident;
                            else
                                staffGuestParticipating.IsUaeresident = "N";

                            staffGuestParticipating.Telephone = StaffGuestDTO.PhoneISD + "$" + StaffGuestDTO.PhoneSTD + "$" + StaffGuestDTO.Phone;
                            staffGuestParticipating.Mobile = StaffGuestDTO.MobileISD + "$" + StaffGuestDTO.MobileSTD + "$" + StaffGuestDTO.Mobile;
                            #region Visa
                            if (StaffGuestDTO.IsVisaNeed == EnumConversion.ToString(EnumBool.Yes))
                            {
                                staffGuestParticipating.NeedVisa = EnumConversion.ToString(EnumBool.Yes);
                            }
                            else
                            {
                                if (staffGuestParticipatingOld.NeedVisa == EnumConversion.ToString(EnumBool.No))
                                    staffGuestParticipating.NeedVisa = EnumConversion.ToString(EnumBool.No);
                            }
                            if (StaffGuestDTO.PassportNumber != null)
                                staffGuestParticipating.Passport = StaffGuestDTO.PassportNumber.Trim();
                            if (StaffGuestDTO.PlaceOfIssue != null)
                                staffGuestParticipating.PlaceOfIssue = StaffGuestDTO.PlaceOfIssue.Trim();

                            if (StaffGuestDTO.PassportDateOfIssue != null)
                                staffGuestParticipating.DateOfIssue = Convert.ToDateTime(StaffGuestDTO.PassportDateOfIssue.Trim());

                            if (StaffGuestDTO.PassportExpiryDate != null)
                                staffGuestParticipating.ExpiryDate = Convert.ToDateTime(StaffGuestDTO.PassportExpiryDate.Trim());
                            if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64Passport))
                            {
                                ext = StaffGuestDTO.FileExtensionofPassport.ToLower();
                                filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);
                                if ("." + ext == ".pdf" || ext == "pdf")
                                    filename = WriteTextOnPDF(StaffGuestDTO.FileBase64Passport, filename, StaffGuestDTO.NameEn, SIBFMemberId, ext);
                                else
                                    filename = WriteTextOnImage(StaffGuestDTO.FileBase64Passport, filename, StaffGuestDTO.NameEn, SIBFMemberId, ext);
                                staffGuestParticipating.PassportCopy = filename;
                            }
                            if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64Passport2))
                            {
                                ext = StaffGuestDTO.FileExtensionofPassport2.ToLower();
                                //filename = string.Format("{0}_{1}{2}", 1, MethodFactory.GetRandomNumber(), ext);
                                if ("." + ext == ".pdf" || ext == "pdf")
                                    filename = WriteTextOnPDF(StaffGuestDTO.FileBase64Passport2, filename, StaffGuestDTO.NameEn, SIBFMemberId, ext);
                                else
                                    filename = WriteTextOnImage(StaffGuestDTO.FileBase64Passport2, filename, StaffGuestDTO.NameEn, SIBFMemberId, ext);
                                staffGuestParticipating.PassportCopyTwo = filename;
                            }
                            if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64PersonalPhoto))
                            {
                                filename = SaveImage(StaffGuestDTO.FileBase64PersonalPhoto, StaffGuestDTO.FileExtensionofPersonalPhoto, StaffGuestDTO.NameEn, PhotoPath, StaffGuestDTO.FileExtensionofPersonalPhoto);
                                staffGuestParticipating.Photo = filename;
                            }
                            if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64Visa))
                            {
                                filename = SaveImage(StaffGuestDTO.FileBase64Visa, StaffGuestDTO.FileExtensionVisa, StaffGuestDTO.NameEn, VisaFormPath, StaffGuestDTO.FileExtensionVisa);
                                staffGuestParticipating.VisaApplicationForm = filename;
                            }
                            if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64CountryUID))
                            {
                                filename = SaveImage(StaffGuestDTO.FileBase64CountryUID, StaffGuestDTO.FileExtensionofCountryUID, StaffGuestDTO.NameEn, CountryUIDPath, StaffGuestDTO.FileExtensionofCountryUID);
                                //fuUploadCountryUID.SaveAs(string.Format("{0}{1}", Server.MapPath(CountryUIDPath), filename));
                                staffGuestParticipating.CountryUidno = filename;
                            }
                            if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64FlightTickets))
                            {
                                filename = SaveImage(StaffGuestDTO.FileBase64FlightTickets, StaffGuestDTO.FileExtensionofFlightTickets, StaffGuestDTO.NameEn, FlightTicketsPath, StaffGuestDTO.FileExtensionofFlightTickets);
                                staffGuestParticipating.FlightTickets = filename;
                            }
                            #endregion

                            if (!string.IsNullOrEmpty(StaffGuestDTO.ActivityDate))
                                staffGuestParticipating.ActivityDate = Convert.ToDateTime(StaffGuestDTO.ActivityDate.Trim());
                            staffGuestParticipating.IsCompanion = StaffGuestDTO.IsCompanion;

                            if (StaffGuestDTO.IsCompanion == "Y")
                            {
                                if (StaffGuestDTO.MainGuestId > 0)
                                    staffGuestParticipating.MainGuestId = StaffGuestDTO.MainGuestId;

                                staffGuestParticipating.MainGuest = StaffGuestDTO.MainGuest;
                                staffGuestParticipating.CompanionDescription = StaffGuestDTO.CompanionDescription;
                            }

                            staffGuestParticipating.GuestCategory = StaffGuestDTO.GuestCategory;
                            staffGuestParticipating.GuestType = StaffGuestDTO.GuestType;

                            if (!string.IsNullOrEmpty(StaffGuestDTO.VisaNoOfMonths))
                                staffGuestParticipating.NoofVisaMonths = StaffGuestDTO.VisaNoOfMonths;

                            if (!string.IsNullOrEmpty(StaffGuestDTO.IsPaidVisa) && StaffGuestDTO.IsPaidVisa != "undefined")
                                staffGuestParticipating.PaidVisa = StaffGuestDTO.IsPaidVisa;

                            staffGuestParticipating.ModifiedOn = MethodFactory.ArabianTimeNow();
                            staffGuestParticipating.ModifiedBy = StaffGuestDTO.MemberId;

                            if (StaffGuestDTO.IsVisaNeed == EnumConversion.ToString(EnumBool.No) && staffGuestParticipatingOld.NeedVisa == EnumConversion.ToString(EnumBool.No))
                            {
                                staffGuestParticipating.Status = EnumConversion.ToString(EnumStaffGuestStatus.Book);
                            }

                            if (staffGuestParticipating.Status != null)
                                if (staffGuestParticipating.Status == EnumConversion.ToString(EnumStaffGuestStatus.PendingDocumentation))
                                    staffGuestParticipating.Status = EnumConversion.ToString(EnumStaffGuestStatus.DocumentsUploaded);

                            if (ExhibitionStaffGuestParticipatingService.UpdateExhibitionStaffGuestParticipating(staffGuestParticipating) == EnumResultType.Success)
                            {
                                sbchanges = GetStaffGuestUpdateChanges(staffGuestParticipatingOld, staffGuestParticipating);
                                UpdateExhibitionStaffGuestFromTabModify(staffGuestParticipating, StaffGuestDTO);
                                StaffGuestDTO.IsEditClicked = false;
                                //ResetControls();
                                if (langid == 1)
                                    MessageDTO.Message = "Staff Guest details updated successfully.";
                                else
                                    MessageDTO.Message = "تم تعديل تفاصيل المندوب بنجاح";
                                MessageDTO.MessageTypeResponse = "Success";
                                NotifyAdminAboutBooking(StaffGuestDTO.MemberId, staffGuestParticipating.StaffGuestId, staffGuestParticipating.ExhibitionId, StaffGuestDTO.WebsiteId, false, sbchanges, 1);
                            }
                            else
                            {
                                if (langid == 1)
                                    MessageDTO.Message = "Failed to update the staff guest details.";
                                else
                                    MessageDTO.Message = "لم يتم تعديل تفاصيل المندوب";// _stringLocalizer["RepresentativeFailedToUpdate"];
                                MessageDTO.MessageTypeResponse = "Error";
                            }
                        }
                    }
                }
                else
                {
                    if (langid == 1)
                        MessageDTO.Message = "Already Registered.";// _stringLocalizer["RepresentativeFailedToAdd"];
                    else
                        MessageDTO.Message = "لم يتم إضافة المندوب، الرجاء إعادة المحاولة";// _stringLocalizer["RepresentativeFailedToAdd"];

                    MessageDTO.MessageTypeResponse = "Error";
                }
            }
            else
            {
                if (langid == 1)
                    MessageDTO.Message = "Failed to add staff guest due to no exhibitionid, Please try adding again later.";// _stringLocalizer["RepresentativeFailedToAdd"];
                else
                    MessageDTO.Message = "Failed to add staff guest due to no exhibitionid, Please try adding again later.";// _stringLocalizer["RepresentativeFailedToAdd"];

                MessageDTO.MessageTypeResponse = "Error";
            }
        }

        private StringBuilder GetStaffGuestNewAddedInfo(XsiExhibitionStaffGuestParticipating staffGuestParticipating)
        {
            StringBuilder sbchanges = new StringBuilder();
            #region Changes
            if (!string.IsNullOrEmpty(staffGuestParticipating.Status))
                sbchanges.Append("<b>Status: </b>" + MethodFactory.GetExhibitionStaffGuestStatus(staffGuestParticipating.Status) + "<br><br>");

            if (!string.IsNullOrEmpty(staffGuestParticipating.NameEn))
                sbchanges.Append("<b>Name (English): </b>" + staffGuestParticipating.NameEn + "<br><br>");

            if (!string.IsNullOrEmpty(staffGuestParticipating.LastName))
                sbchanges.Append("<b>Last Name (English): </b>" + staffGuestParticipating.LastName + "<br><br>");

            if (!string.IsNullOrEmpty(staffGuestParticipating.NameAr))
                sbchanges.Append("<b>Name (Arabic): </b>" + staffGuestParticipating.NameAr + "<br><br>");
            if (staffGuestParticipating.CountryId != null)
                sbchanges.Append("<b>Country: </b>" + MethodFactory.GetCountryName(staffGuestParticipating.CountryId ?? -1, 1) + "<br><br>");
            if (!string.IsNullOrEmpty(staffGuestParticipating.Uidno))
                sbchanges.Append("<b>UID Number: </b>" + staffGuestParticipating.Uidno + "<br><br>");
            if (!string.IsNullOrEmpty(staffGuestParticipating.IsTravelDetails))
                sbchanges.Append("<b>Is Travel Details: </b>" + MethodFactory.GetBooleanValue(staffGuestParticipating.IsTravelDetails) + "<br><br>");

            if (!string.IsNullOrEmpty(staffGuestParticipating.Profession))
                sbchanges.Append("<b>Profession: </b>" + staffGuestParticipating.Profession + "<br><br>");
            if (!string.IsNullOrEmpty(staffGuestParticipating.Telephone) && !string.IsNullOrEmpty(staffGuestParticipating.Telephone.Replace("$", string.Empty)))
                sbchanges.Append("<b>Telephone: </b>" + staffGuestParticipating.Telephone + "<br><br>");
            if (!string.IsNullOrEmpty(staffGuestParticipating.Mobile) && !string.IsNullOrEmpty(staffGuestParticipating.Mobile.Replace("$", string.Empty)))
                sbchanges.Append("<b>Mobile: </b>" + staffGuestParticipating.Mobile + "<br><br>");
            if (!string.IsNullOrEmpty(staffGuestParticipating.TravelDetails))
                sbchanges.Append("<b>Travel Details: </b>" + staffGuestParticipating.TravelDetails + "<br><br>");
            if (!string.IsNullOrEmpty(staffGuestParticipating.PlaceOfIssue))
                sbchanges.Append("<b>Place Of Issue: </b>" + staffGuestParticipating.PlaceOfIssue + "<br><br>");

            if (!string.IsNullOrEmpty(staffGuestParticipating.Passport))
                sbchanges.Append("<b>Passport: </b>" + staffGuestParticipating.Passport + "<br><br>");
            if (staffGuestParticipating.DateOfIssue != null)
                sbchanges.Append("<b>Date Of Issue: </b>" + staffGuestParticipating.DateOfIssue.Value.ToString("dd MMM yyyy") + "<br><br>");
            if (staffGuestParticipating.ExpiryDate != null)
                sbchanges.Append("<b>Expiry Date: </b>" + staffGuestParticipating.ExpiryDate.Value.ToString("dd MMM yyyy") + "<br><br>");
            if (staffGuestParticipating.GuestType != null)
                sbchanges.Append("<b>Guest Type: </b>" + MethodFactory.GetGuestType(staffGuestParticipating.GuestType ?? -1) + "<br><br>");
            if (staffGuestParticipating.GuestCategory != null)
                sbchanges.Append("<b>Guest Category: </b>" + MethodFactory.GetGuestCategory(staffGuestParticipating.GuestCategory ?? -1) + "<br><br>");
            if (staffGuestParticipating.ActivityDate != null)
                sbchanges.Append("<b>Activity Date: </b>" + staffGuestParticipating.ActivityDate.Value.ToString("dd MMM yyyy") + "<br><br>");
            if (!string.IsNullOrEmpty(staffGuestParticipating.AddedBy))
                sbchanges.Append("<b>Added By: </b>" + staffGuestParticipating.AddedBy + "<br><br>");
            if (!string.IsNullOrEmpty(staffGuestParticipating.NeedVisa))
                sbchanges.Append("<b>Need Visa: </b>" + MethodFactory.GetBooleanValue(staffGuestParticipating.NeedVisa) + "<br><br>");
            if (!string.IsNullOrEmpty(staffGuestParticipating.NoofVisaMonths))
                sbchanges.Append("<b>Number of Visa Months: </b>" + staffGuestParticipating.NoofVisaMonths + "<br><br>");
            if (!string.IsNullOrEmpty(staffGuestParticipating.PaidVisa))
                sbchanges.Append("<b>Is Paid Visa: </b>" + MethodFactory.GetBooleanValue(staffGuestParticipating.PaidVisa) + "<br><br>");

            if (!string.IsNullOrEmpty(staffGuestParticipating.VisaType))
                sbchanges.Append("<b>Visa Type: </b>" + MethodFactory.GetVisaType(staffGuestParticipating.VisaType) + "<br><br>");
            if (!string.IsNullOrEmpty(staffGuestParticipating.IsVisitedUae))
                sbchanges.Append("<b>Is Visited UAE: </b>" + MethodFactory.GetBooleanValue(staffGuestParticipating.IsVisitedUae) + "<br><br>");
            if (!string.IsNullOrEmpty(staffGuestParticipating.IsCompanion))
                sbchanges.Append("<b>Is Companion: </b>" + MethodFactory.GetBooleanValue(staffGuestParticipating.IsCompanion) + "<br><br>");
            if (!string.IsNullOrEmpty(staffGuestParticipating.CompanionDescription))
                sbchanges.Append("<b>Companion Description: </b>" + staffGuestParticipating.CompanionDescription + "<br><br>");
            if (staffGuestParticipating.CreatedOn != null)
                sbchanges.Append("<b>Created On: </b>" + staffGuestParticipating.CreatedOn.Value.ToString("dd MMM yyyy hh:mm tt") + "<br><br>");
            if (staffGuestParticipating.ModifiedOn != null)
                sbchanges.Append("<b>Updated On: </b>" + staffGuestParticipating.ModifiedOn.Value.ToString("dd MMM yyyy hh:mm tt") + "<br><br>");

            bool isuploads = false;
            sbchanges.Append("$$uploadssection$$");

            if (!string.IsNullOrEmpty(staffGuestParticipating.PassportCopy))
            {
                isuploads = true;
                sbchanges.Append("<b>Passport Copy: </b>" + staffGuestParticipating.PassportCopy + "<br><br>");
            }

            if (!string.IsNullOrEmpty(staffGuestParticipating.PassportCopyTwo))
            {
                isuploads = true;
                sbchanges.Append("<b>Passport Copy Two: </b>" + staffGuestParticipating.PassportCopyTwo + "<br><br>");
            }

            if (!string.IsNullOrEmpty(staffGuestParticipating.Photo))
            {
                isuploads = true;
                sbchanges.Append("<b>Photo: </b>" + staffGuestParticipating.Photo + "<br><br>");
            }

            if (!string.IsNullOrEmpty(staffGuestParticipating.VisaApplicationForm))
            {
                isuploads = true;
                sbchanges.Append("<b>Visa Application Form: </b>" + staffGuestParticipating.VisaApplicationForm + "<br><br>");
            }

            if (!string.IsNullOrEmpty(staffGuestParticipating.CountryUidno))
            {
                isuploads = true;
                sbchanges.Append("<b>Country UID Number: </b>" + staffGuestParticipating.CountryUidno + "<br><br>");
            }

            if (!string.IsNullOrEmpty(staffGuestParticipating.HealthInsuranceTwo))
            {
                isuploads = true;
                sbchanges.Append("<b>Health Insurance Two: </b>" + staffGuestParticipating.HealthInsuranceTwo + "<br><br>");
            }

            if (isuploads)
                sbchanges.Replace("$$uploadssection$$", "<br><b>Uploads Section:</b> <br>");
            else
                sbchanges.Replace("$$uploadssection$$", string.Empty);
            #endregion
            return sbchanges;
        }
        private StringBuilder GetStaffGuestUpdateChanges(XsiExhibitionStaffGuestParticipating staffGuestParticipatingOld, XsiExhibitionStaffGuestParticipating staffGuestParticipating)
        {
            StringBuilder sbchanges = new StringBuilder();
            #region Changes
            if (staffGuestParticipatingOld.Status != staffGuestParticipating.Status)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.Status))
                    sbchanges.Append("<b>Status: </b>" + MethodFactory.GetExhibitionStaffGuestStatus(staffGuestParticipatingOld.Status) + " to " + MethodFactory.GetExhibitionStaffGuestStatus(staffGuestParticipating.Status) + "<br><br>");
                else
                    sbchanges.Append("<b>Status: </b>" + "-" + " to " + MethodFactory.GetExhibitionStaffGuestStatus(staffGuestParticipating.Status) + "<br><br>");
            }
            if (staffGuestParticipatingOld.NameEn != staffGuestParticipating.NameEn)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.NameEn))
                    sbchanges.Append("<b>Name (English): </b>" + staffGuestParticipatingOld.NameEn + " to " + staffGuestParticipating.NameEn + "<br><br>");
                else
                    sbchanges.Append("<b>Name (English): </b>" + "-" + " to " + staffGuestParticipating.NameEn + "<br><br>");
            }
            if (staffGuestParticipatingOld.LastName != staffGuestParticipating.LastName)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.LastName))
                    sbchanges.Append("<b>Name (English): </b>" + staffGuestParticipatingOld.LastName + " to " + staffGuestParticipating.LastName + "<br><br>");
                else
                    sbchanges.Append("<b>Name (English): </b>" + "-" + " to " + staffGuestParticipating.LastName + "<br><br>");
            }
            if (staffGuestParticipatingOld.NameAr != staffGuestParticipating.NameAr)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.NameAr))
                    sbchanges.Append("<b>Name (Arabic): </b>" + staffGuestParticipatingOld.NameAr + " to " + staffGuestParticipating.NameAr + "<br><br>");
                else
                    sbchanges.Append("<b>Name (Arabic): </b>" + "-" + " to " + staffGuestParticipating.NameAr + "<br><br>");
            }
            if (staffGuestParticipatingOld.CountryId != staffGuestParticipating.CountryId)
            {
                if (staffGuestParticipatingOld.CountryId != null)
                    sbchanges.Append("<b>Country: </b>" + MethodFactory.GetCountryName(staffGuestParticipatingOld.CountryId ?? -1, 1) + " to " + MethodFactory.GetCountryName(staffGuestParticipating.CountryId ?? -1, 1) + "<br><br>");
                else
                    sbchanges.Append("<b>Country: </b>" + "-" + " to " + MethodFactory.GetCountryName(staffGuestParticipating.CountryId ?? -1, 1) + "<br><br>");
            }
            if (staffGuestParticipatingOld.Uidno != staffGuestParticipating.Uidno)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.Uidno))
                    sbchanges.Append("<b>UID Number: </b>" + staffGuestParticipatingOld.Uidno + " to " + staffGuestParticipating.Uidno + "<br><br>");
                else
                    sbchanges.Append("<b>UID Number: </b>" + "-" + " to " + staffGuestParticipating.Uidno + "<br><br>");
            }

            if (staffGuestParticipatingOld.IsTravelDetails != staffGuestParticipating.IsTravelDetails)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.IsTravelDetails))
                    sbchanges.Append("<b>Is Travel Details: </b>" + MethodFactory.GetBooleanValue(staffGuestParticipatingOld.IsTravelDetails) + " to " + MethodFactory.GetBooleanValue(staffGuestParticipating.IsTravelDetails) + "<br>");
                else
                    sbchanges.Append("<b>Is Travel Details: </b>" + "-" + " to " + MethodFactory.GetBooleanValue(staffGuestParticipating.IsTravelDetails) + "<br><br>");
            }

            if (staffGuestParticipatingOld.Profession != staffGuestParticipating.Profession)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.Profession))
                    sbchanges.Append("<b>Profession: </b>" + staffGuestParticipatingOld.Profession + " to " + staffGuestParticipating.Profession + "<br><br>");
                else
                    sbchanges.Append("<b>Profession: </b>" + "-" + " to " + staffGuestParticipating.Profession + "<br><br>");
            }
            if (staffGuestParticipatingOld.Telephone != staffGuestParticipating.Telephone)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.Telephone))
                    sbchanges.Append("<b>Telephone: </b>" + staffGuestParticipatingOld.Telephone.Replace("$", "-") + " to " + staffGuestParticipating.Telephone.Replace("$", "-") + "<br><br>");
                else
                    sbchanges.Append("<b>Telephone: </b>" + "-" + " to " + staffGuestParticipating.Telephone + "<br><br>");
            }
            if (staffGuestParticipatingOld.Mobile != staffGuestParticipating.Mobile)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.Mobile))
                    sbchanges.Append("<b>Mobile: </b>" + staffGuestParticipatingOld.Mobile.Replace("$", "-") + " to " + staffGuestParticipating.Mobile.Replace("$", "-") + "<br><br>");
                else
                    sbchanges.Append("<b>Mobile: </b>" + "-" + " to " + staffGuestParticipating.Mobile + "<br><br>");
            }
            if (staffGuestParticipatingOld.TravelDetails != staffGuestParticipating.TravelDetails)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.TravelDetails))
                    sbchanges.Append("Travel Details: </b>" + staffGuestParticipatingOld.TravelDetails + " to " + staffGuestParticipating.TravelDetails + "<br>");
                else
                    sbchanges.Append("<b>Travel Details: </b>" + "-" + " to " + staffGuestParticipating.TravelDetails + "<br><br>");
            }


            if (staffGuestParticipatingOld.PlaceOfIssue != staffGuestParticipating.PlaceOfIssue)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.PlaceOfIssue))
                    sbchanges.Append("<b>Place Of Issue: </b>" + staffGuestParticipatingOld.PlaceOfIssue + " to " + staffGuestParticipating.PlaceOfIssue + "<br><br>");
                else
                    sbchanges.Append("<b>Place Of Issue: </b>" + "-" + " to " + staffGuestParticipating.PlaceOfIssue + "<br><br>");
            }

            if (staffGuestParticipatingOld.Passport != staffGuestParticipating.Passport)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.Passport))
                    sbchanges.Append("<b>Passport: </b>" + staffGuestParticipatingOld.Passport + " to " + staffGuestParticipating.Passport + "<br><br>");
                else
                    sbchanges.Append("<b>Passport: </b>" + "-" + " to " + staffGuestParticipating.Passport + "<br><br>");
            }
            if (staffGuestParticipatingOld.DateOfIssue != staffGuestParticipating.DateOfIssue)
            {
                if (staffGuestParticipatingOld.DateOfIssue != null)
                    sbchanges.Append("<b>Date Of Issue: </b>" + staffGuestParticipatingOld.DateOfIssue.Value.ToString("dd MMM yyyy") + " to " + staffGuestParticipating.DateOfIssue.Value.ToString("dd MMM yyyy") + "<br><br>");
                else
                    sbchanges.Append("<b>Date Of Issue: </b>" + "-" + " to " + staffGuestParticipating.DateOfIssue.Value.ToString("dd MMM yyyy") + "<br><br>");
            }
            if (staffGuestParticipatingOld.ExpiryDate != staffGuestParticipating.ExpiryDate)
            {
                if (staffGuestParticipatingOld.ExpiryDate != null)
                    sbchanges.Append("<b>Expiry Date: </b>" + staffGuestParticipatingOld.ExpiryDate.Value.ToString("dd MMM yyyy") + " to " + staffGuestParticipating.ExpiryDate.Value.ToString("dd MMM yyyy") + "<br><br>");
                else
                    sbchanges.Append("<b>Expiry Date: </b>" + "-" + " to " + staffGuestParticipating.ExpiryDate.Value.ToString("dd MMM yyyy") + "<br><br>");
            }

            if (staffGuestParticipatingOld.GuestType != staffGuestParticipating.GuestType)
            {
                if (staffGuestParticipatingOld.GuestType != null)
                    sbchanges.Append("<b>Guest Type: </b>" + MethodFactory.GetGuestType(staffGuestParticipatingOld.GuestType ?? -1) + " to " + MethodFactory.GetGuestType(staffGuestParticipating.GuestType ?? -1) + "<br>");
                else
                    sbchanges.Append("<b>Guest Type: </b>" + "-" + " to " + MethodFactory.GetGuestType(staffGuestParticipating.GuestType ?? -1) + "<br><br>");

            }
            if (staffGuestParticipatingOld.GuestCategory != staffGuestParticipating.GuestCategory)
            {
                if (staffGuestParticipatingOld.GuestCategory != null)
                    sbchanges.Append("<b>Guest Category: </b>" + MethodFactory.GetGuestCategory(staffGuestParticipatingOld.GuestCategory ?? -1) + " to " + MethodFactory.GetGuestCategory(staffGuestParticipating.GuestCategory ?? -1) + "<br>");
                else
                    sbchanges.Append("<b>Guest Category: </b>" + "-" + " to " + MethodFactory.GetGuestCategory(staffGuestParticipating.GuestCategory ?? -1) + "<br><br>");
            }
            if (staffGuestParticipatingOld.ActivityDate != staffGuestParticipating.ActivityDate)
            {
                if (staffGuestParticipatingOld.ActivityDate != null)
                    sbchanges.Append("<b>Activity Date: </b>" + staffGuestParticipatingOld.ActivityDate.Value.ToString("dd MMM yyyy") + " to " + staffGuestParticipating.ActivityDate.Value.ToString("dd MMM yyyy") + "<br><br>");
                else
                    sbchanges.Append("<b>Activity Date: </b>" + "-" + " to " + staffGuestParticipating.ActivityDate.Value.ToString("dd MMM yyyy") + "<br><br>");
            }

            if (staffGuestParticipatingOld.AddedBy != staffGuestParticipating.AddedBy)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.AddedBy))
                    sbchanges.Append("<b>Added By: </b>" + staffGuestParticipatingOld.AddedBy + " to " + staffGuestParticipating.AddedBy + "<br><br>");
                else
                    sbchanges.Append("<b>Added By: </b>" + "-" + " to " + staffGuestParticipating.AddedBy + "<br><br>");
            }
            if (staffGuestParticipatingOld.NeedVisa != staffGuestParticipating.NeedVisa)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.NeedVisa))
                    sbchanges.Append("<b>Need Visa: </b>" + MethodFactory.GetBooleanValue(staffGuestParticipatingOld.NeedVisa) + " to " + MethodFactory.GetBooleanValue(staffGuestParticipating.NeedVisa) + "<br><br>");
                else
                    sbchanges.Append("<b>Need Visa: </b>" + "-" + " to " + MethodFactory.GetBooleanValue(staffGuestParticipating.NeedVisa) + "<br><br>");
            }
            if (staffGuestParticipatingOld.NoofVisaMonths != staffGuestParticipating.NoofVisaMonths)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.NoofVisaMonths))
                    sbchanges.Append("<b>Number of Visa Months: </b>" + staffGuestParticipatingOld.NoofVisaMonths + " to " + staffGuestParticipating.NoofVisaMonths + "<br><br>");
                else
                    sbchanges.Append("<b>Number of Visa Months: </b>" + "-" + " to " + staffGuestParticipating.NoofVisaMonths + "<br><br>");
            }
            if (staffGuestParticipatingOld.PaidVisa != staffGuestParticipating.PaidVisa)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.PaidVisa))
                    sbchanges.Append("<b>Is Paid Visa: </b>" + MethodFactory.GetBooleanValue(staffGuestParticipatingOld.PaidVisa) + " to " + MethodFactory.GetBooleanValue(staffGuestParticipating.PaidVisa) + "<br><br>");
                else
                    sbchanges.Append("<b>Is Paid Visa: </b>" + "-" + " to " + MethodFactory.GetBooleanValue(staffGuestParticipating.PaidVisa) + "<br><br>");
            }

            if (staffGuestParticipatingOld.VisaType != staffGuestParticipating.VisaType)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.VisaType))
                    sbchanges.Append("<b>Visa Type: </b>" + MethodFactory.GetVisaType(staffGuestParticipatingOld.VisaType) + " to " + MethodFactory.GetVisaType(staffGuestParticipating.VisaType) + "<br><br>");
                else
                    sbchanges.Append("<b>Visa Type: </b>" + "-" + " to " + MethodFactory.GetVisaType(staffGuestParticipating.VisaType) + "<br><br>");
            }
            if (staffGuestParticipatingOld.IsVisitedUae != staffGuestParticipating.IsVisitedUae)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.IsVisitedUae))
                    sbchanges.Append("<b>Is Visited UAE: </b>" + MethodFactory.GetBooleanValue(staffGuestParticipatingOld.IsVisitedUae) + " to " + MethodFactory.GetBooleanValue(staffGuestParticipating.IsVisitedUae) + "<br><br>");
                else
                    sbchanges.Append("<b>Is Visited UAE: </b>" + "-" + " to " + MethodFactory.GetBooleanValue(staffGuestParticipating.IsVisitedUae) + "<br><br>");
            }
            if (staffGuestParticipatingOld.IsCompanion != staffGuestParticipating.IsCompanion)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.IsCompanion))
                    sbchanges.Append("<b>Is Companion: </b>" + MethodFactory.GetBooleanValue(staffGuestParticipatingOld.IsCompanion) + " to " + MethodFactory.GetBooleanValue(staffGuestParticipating.IsCompanion) + "<br><br>");
                else
                    sbchanges.Append("<b>Is Companion: </b>" + "-" + " to " + MethodFactory.GetBooleanValue(staffGuestParticipating.IsCompanion) + "<br><br>");
            }
            if (staffGuestParticipatingOld.CompanionDescription != staffGuestParticipating.CompanionDescription)
            {
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.CompanionDescription))
                    sbchanges.Append("<b>Companion Description: </b>" + staffGuestParticipatingOld.CompanionDescription + " to " + staffGuestParticipating.CompanionDescription + "<br><br>");
                else
                    sbchanges.Append("<b>Companion Description: </b>" + "-" + " to " + staffGuestParticipating.CompanionDescription + "<br><br>");
            }
            if (staffGuestParticipatingOld.CreatedOn != staffGuestParticipating.CreatedOn)
            {
                if (staffGuestParticipatingOld.CreatedOn != null)
                    sbchanges.Append("<b>Created On: </b>" + staffGuestParticipatingOld.CreatedOn.Value.ToString("dd MMM yyyy hh:mm tt") + " to " + staffGuestParticipating.CreatedOn.Value.ToString("<b>dd MMM yyyy hh:mm tt") + "<br><br>");
                else
                    sbchanges.Append("<b>Created On: </b>" + "-" + " to " + staffGuestParticipating.CreatedOn.Value.ToString("dd MMM yyyy hh:mm tt") + "<br><br>");
            }
            if (staffGuestParticipatingOld.ModifiedOn != staffGuestParticipating.ModifiedOn)
            {
                if (staffGuestParticipatingOld.ModifiedOn != null)
                    sbchanges.Append("<b>Updated On: </b>" + staffGuestParticipatingOld.ModifiedOn.Value.ToString("dd MMM yyyy hh:mm tt") + " to " + staffGuestParticipating.ModifiedOn.Value.ToString("dd MMM yyyy hh:mm tt") + "<br><br>");
                else
                    sbchanges.Append("<b>Updated On: </b>" + "-" + " to " + staffGuestParticipating.ModifiedOn.Value.ToString("dd MMM yyyy hh:mm tt") + "<br><br>");
            }
            bool isuploads = false;
            sbchanges.Append("$$uploadssection$$");
            if (staffGuestParticipatingOld.PassportCopy != staffGuestParticipating.PassportCopy)
            {
                isuploads = true;
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.PassportCopy))
                    sbchanges.Append("<b>Passport Copy: </b>" + staffGuestParticipatingOld.PassportCopy + " to " + staffGuestParticipating.PassportCopy + "<br><br>");
                else
                    sbchanges.Append("<b>Passport Copy: </b>" + "-" + " to " + staffGuestParticipating.PassportCopy + "<br><br>");
            }
            if (staffGuestParticipatingOld.PassportCopyTwo != staffGuestParticipating.PassportCopyTwo)
            {
                isuploads = true;
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.PassportCopyTwo))
                    sbchanges.Append("<b>Passport Copy Two: </b>" + staffGuestParticipatingOld.PassportCopyTwo + " to " + staffGuestParticipating.PassportCopyTwo + "<br><br>");
                else
                    sbchanges.Append("<b>Passport Copy Two: </b>" + "-" + " to " + staffGuestParticipating.PassportCopyTwo + "<br><br>");
            }
            if (staffGuestParticipatingOld.Photo != staffGuestParticipating.Photo)
            {
                isuploads = true;
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.Photo))
                    sbchanges.Append("<b>Photo: </b>" + staffGuestParticipatingOld.Photo + " to " + staffGuestParticipating.Photo + "<br><br>");
                else
                    sbchanges.Append("<b>Photo: </b>" + "-" + " to " + staffGuestParticipating.Photo + "<br><br>");
            }
            if (staffGuestParticipatingOld.VisaApplicationForm != staffGuestParticipating.VisaApplicationForm)
            {
                isuploads = true;
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.VisaApplicationForm))
                    sbchanges.Append("<b>Visa Application Form: </b>" + staffGuestParticipatingOld.VisaApplicationForm + " to " + staffGuestParticipating.VisaApplicationForm + "<br><br>");
                else
                    sbchanges.Append("<b>Visa Application Form: </b>" + "-" + " to " + staffGuestParticipating.VisaApplicationForm + "<br><br>");
            }

            if (staffGuestParticipatingOld.CountryUidno != staffGuestParticipating.CountryUidno)
            {
                isuploads = true;
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.CountryUidno))
                    sbchanges.Append("<b>Country UID Number: </b>" + staffGuestParticipatingOld.CountryUidno + " to " + staffGuestParticipating.CountryUidno + "<br><br>");
                else
                    sbchanges.Append("<b>Country UID Number: </b>" + "-" + " to " + staffGuestParticipating.CountryUidno + "<br><br>");
            }
            if (staffGuestParticipatingOld.HealthInsuranceTwo != staffGuestParticipating.HealthInsuranceTwo)
            {
                isuploads = true;
                if (!string.IsNullOrEmpty(staffGuestParticipatingOld.HealthInsuranceTwo))
                    sbchanges.Append("<b>Health Insurance Two: </b>" + staffGuestParticipatingOld.HealthInsuranceTwo + " to " + staffGuestParticipating.HealthInsuranceTwo + "<br><br>");
                else
                    sbchanges.Append("<b>Health Insurance Two: </b>" + "-" + " to " + staffGuestParticipating.HealthInsuranceTwo + "<br><br>");
            }

            if (isuploads)
                sbchanges.Replace("$$uploadssection$$", "<br><b>Uploads Section:</b> <br>");
            else
                sbchanges.Replace("$$uploadssection$$", string.Empty);
            #endregion
            return sbchanges;
        }

        private bool UpdateNReregister(SaveStaffGuestDTO StaffGuestDTO)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            string filename = string.Empty;
            EnumResultType result = EnumResultType.Failed;
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                #region Exhibition StaffGuest
                using (ExhibitionStaffGuestService ExhibitionStaffGuestService = new ExhibitionStaffGuestService())
                {
                    var staffGuest = ExhibitionStaffGuestService.GetStaffGuestByItemId(StaffGuestDTO.StaffGuestId);
                    if (staffGuest != null)
                    {
                        DateTime dob = new DateTime();
                        DateTime.TryParse(StaffGuestDTO.DateofBirth, out dob);
                        staffGuest.MemberId = SIBFMemberId;
                        if (!string.IsNullOrEmpty(StaffGuestDTO.NameEn))
                            staffGuest.NameEn = StaffGuestDTO.NameEn.Trim();
                        if (!string.IsNullOrEmpty(StaffGuestDTO.LastName))
                            staffGuest.LastName = StaffGuestDTO.LastName.Trim();
                        if (!string.IsNullOrEmpty(StaffGuestDTO.NameAr))
                            staffGuest.NameAr = StaffGuestDTO.NameAr.Trim();
                        if (!string.IsNullOrEmpty(StaffGuestDTO.DateofBirth))
                            staffGuest.Dob = dob;
                        staffGuest.PlaceOfBirth = StaffGuestDTO.PlaceOfBirth;
                        staffGuest.ModifiedBy = SIBFMemberId;
                        staffGuest.ModifiedOn = MethodFactory.ArabianTimeNow();
                        result = ExhibitionStaffGuestService.UpdateExhibitionStaffGuest(staffGuest);
                    }

                    #endregion
                    if (result == EnumResultType.Success)
                    {
                        using (ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService())
                        {
                            var staffGuestParticipating = new XsiExhibitionStaffGuestParticipating();
                            if (staffGuest != null)
                            {
                                //var exhibitor = MethodFactory.GetExhibitor(StaffGuestDTO.MemberId, StaffGuestDTO.WebsiteId, _context);
                                staffGuestParticipating.StaffGuestId = StaffGuestDTO.StaffGuestId;
                                staffGuestParticipating.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                staffGuestParticipating.IsFromPcregistration = EnumConversion.ToString(EnumBool.No);
                                staffGuestParticipating.LanguageId = LangId;
                                staffGuestParticipating.ExhibitionId = StaffGuestDTO.ExhibitionId;
                                staffGuestParticipating.CountryId = StaffGuestDTO.NationalityId;

                                if (StaffGuestDTO.NameEn != null)
                                    staffGuestParticipating.NameEn = StaffGuestDTO.NameEn.Trim();

                                if (StaffGuestDTO.LastName != null)
                                    staffGuestParticipating.LastName = StaffGuestDTO.LastName.Trim();

                                if (StaffGuestDTO.NameAr != null)
                                    staffGuestParticipating.NameAr = StaffGuestDTO.NameAr.Trim();

                                if (StaffGuestDTO.Profession != null)
                                    staffGuestParticipating.Profession = StaffGuestDTO.Profession.Trim();

                                if (!string.IsNullOrEmpty(StaffGuestDTO.IsVisitedUae) && StaffGuestDTO.IsVisitedUae != "undefined")
                                    staffGuestParticipating.IsVisitedUae = StaffGuestDTO.IsVisitedUae;
                                else
                                    staffGuestParticipating.IsVisitedUae = "N";

                                staffGuestParticipating.ArrivalDate = null;
                                staffGuestParticipating.DepartureDate = null;
                                //spentity.ArrivalAirportId = -1;
                                //spentity.ArrivalTerminalId = -1;
                                staffGuestParticipating.VisaCopy = string.Empty;
                                staffGuestParticipating.FlightTickets = string.Empty;
                                staffGuestParticipating.Telephone = StaffGuestDTO.PhoneISD + "$" + StaffGuestDTO.PhoneSTD + "$" + StaffGuestDTO.Phone;
                                staffGuestParticipating.Mobile = StaffGuestDTO.MobileISD + "$" + StaffGuestDTO.MobileSTD + "$" + StaffGuestDTO.Mobile;
                                #region Visa
                                if (StaffGuestDTO.IsVisaNeed == EnumConversion.ToString(EnumBool.No))
                                {
                                    staffGuestParticipating.NeedVisa = EnumConversion.ToString(EnumBool.No);
                                    //staffGuestParticipating.Status = EnumConversion.ToString(EnumStaffGuestStatus.Approved);
                                    staffGuestParticipating.Status = EnumConversion.ToString(EnumStaffGuestStatus.Book);
                                }
                                else
                                {
                                    staffGuestParticipating.Status = EnumConversion.ToString(EnumStaffGuestStatus.Pending);

                                    if (StaffGuestDTO.IsVisaNeed == EnumConversion.ToString(EnumBool.Yes))
                                        staffGuestParticipating.NeedVisa = EnumConversion.ToString(EnumBool.Yes);
                                }

                                if (StaffGuestDTO.PassportNumber != null)
                                    staffGuestParticipating.Passport = StaffGuestDTO.PassportNumber.Trim();
                                if (StaffGuestDTO.PlaceOfIssue != null)
                                    staffGuestParticipating.PlaceOfIssue = StaffGuestDTO.PlaceOfIssue.Trim();
                                if (StaffGuestDTO.PassportDateOfIssue != null)
                                    staffGuestParticipating.DateOfIssue = Convert.ToDateTime(StaffGuestDTO.PassportDateOfIssue.Trim());

                                if (StaffGuestDTO.PassportExpiryDate != null)
                                    staffGuestParticipating.ExpiryDate = Convert.ToDateTime(StaffGuestDTO.PassportExpiryDate.Trim());

                                if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64Passport))
                                {
                                    ext = StaffGuestDTO.FileExtensionofPassport;
                                    filename = string.Format("{0}_{1}.{2}", LangId, MethodFactory.GetRandomNumber(), ext);
                                    if (StaffGuestDTO.FileExtensionofPassport.ToLower() == ".pdf" || StaffGuestDTO.FileExtensionofPassport.ToLower() == "pdf") //1=EnglishId
                                        WriteTextOnPDF(StaffGuestDTO.FileBase64Passport, filename, StaffGuestDTO.NameEn, SIBFMemberId, ext);
                                    else
                                        WriteTextOnImage(StaffGuestDTO.FileBase64Passport, filename, StaffGuestDTO.NameEn, SIBFMemberId, ext);
                                    staffGuestParticipating.PassportCopy = filename;
                                }
                                if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64Passport2))
                                {
                                    ext = StaffGuestDTO.FileExtensionofPassport2;
                                    filename = string.Format("{0}_{1}.{2}", LangId, MethodFactory.GetRandomNumber(), ext);
                                    if (ext.ToLower() == ".pdf" || ext.ToLower() == "pdf")
                                        WriteTextOnPDF(StaffGuestDTO.FileBase64Passport2, filename, StaffGuestDTO.NameEn, SIBFMemberId, ext);
                                    else
                                        WriteTextOnImage(StaffGuestDTO.FileBase64Passport2, filename, StaffGuestDTO.NameEn, SIBFMemberId, ext);
                                    staffGuestParticipating.PassportCopyTwo = filename;
                                }
                                if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64PersonalPhoto))
                                {
                                    filename = string.Format("{0}_{1}.{2}", LangId, MethodFactory.GetRandomNumber(), StaffGuestDTO.FileExtensionofPersonalPhoto);
                                    filename = SaveImage(StaffGuestDTO.FileBase64PersonalPhoto, StaffGuestDTO.FileExtensionofPersonalPhoto, StaffGuestDTO.NameEn, PhotoPath, StaffGuestDTO.FileExtensionofPersonalPhoto);
                                    staffGuestParticipating.Photo = filename;
                                }
                                if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64Visa))
                                {
                                    filename = SaveImage(StaffGuestDTO.FileBase64Visa, StaffGuestDTO.FileExtensionVisa, StaffGuestDTO.NameEn, VisaFormPath, StaffGuestDTO.FileExtensionVisa);
                                    staffGuestParticipating.VisaApplicationForm = filename;
                                }
                                if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64CountryUID))
                                {
                                    filename = SaveImage(StaffGuestDTO.FileBase64CountryUID, StaffGuestDTO.FileExtensionofCountryUID, StaffGuestDTO.NameEn, CountryUIDPath, StaffGuestDTO.FileExtensionofCountryUID);
                                    staffGuestParticipating.CountryUidno = filename;
                                }
                                if (!string.IsNullOrEmpty(StaffGuestDTO.FileBase64FlightTickets))
                                {
                                    filename = SaveImage(StaffGuestDTO.FileBase64FlightTickets, StaffGuestDTO.FileExtensionofFlightTickets, StaffGuestDTO.NameEn, FlightTicketsPath, StaffGuestDTO.FileExtensionofFlightTickets);
                                    staffGuestParticipating.FlightTickets = filename;
                                }
                                #endregion
                                staffGuestParticipating.CreatedOn = MethodFactory.ArabianTimeNow();
                                staffGuestParticipating.CreatedBy = StaffGuestDTO.MemberId;
                                staffGuestParticipating.ModifiedOn = MethodFactory.ArabianTimeNow();
                                staffGuestParticipating.ModifiedBy = StaffGuestDTO.MemberId;
                                if (ExhibitionStaffGuestParticipatingService.InsertExhibitionStaffGuestParticipating(staffGuestParticipating) == EnumResultType.Success)
                                {
                                    StringBuilder sb = new StringBuilder();
                                    sb = GetStaffGuestNewAddedInfo(staffGuestParticipating);
                                    NotifyAdminAboutBooking(StaffGuestDTO.MemberId, staffGuestParticipating.StaffGuestId, staffGuestParticipating.ExhibitionId, StaffGuestDTO.WebsiteId, true, sb, 1);
                                    StaffGuestDTO.IsEditClicked = false;
                                    //ResetControls();
                                    if (StaffGuestDTO.IsVisaNeed == EnumConversion.ToString(EnumBool.Yes))
                                    {
                                        if (LangId == 1)
                                            MessageDTO.Message = "Staff Guest details have been sent for Admin’s approval. We will get back to you at our earliest.";
                                        else
                                            MessageDTO.Message = "تفاصيل المندوب الذي قمت بإضافته سيتم عرضه على الإدارة للموافقة. سيتم الرد عليك في أقرب وقت";//_stringLocalizer["RepresentativeInsertSucessfull"];
                                    }
                                    else
                                    {
                                        if (LangId == 1)
                                            MessageDTO.Message = "Staff Guest details have been submitted successfuly. Please check in Guest List tab.";
                                        else
                                            MessageDTO.Message = "تمت إضافة معلومات الضيف بنجاح. يرجى التأكد من شاشة قائمة الضيوف";
                                    }
                                    MessageDTO.MessageTypeResponse = "Success";
                                }
                                else
                                {
                                    if (LangId == 1)
                                        MessageDTO.Message = "Failed to register the staff guest."; // _stringLocalizer["RepresentativeFailedToReRegister"];
                                    else
                                        MessageDTO.Message = "لم يتم تسجيل المندوب";// _stringLocalizer["RepresentativeFailedToReRegister"];

                                    MessageDTO.MessageTypeResponse = "Error";
                                }
                            }
                        }
                        return true;
                    }
                }
                return false;
            }
        }

        private void UpdateTravelDetails(TravelStaffGuestDTO travelDto)
        {
            //WebsiteId = travelDto.WebsiteId;
            using (ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService())
            {
                StringBuilder sbchanges = new StringBuilder();
                var staffGuestParticipatingOld = ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(new XsiExhibitionStaffGuestParticipating() { StaffGuestId = travelDto.StaffGuestId, ExhibitionId = travelDto.ExhibitionId }).FirstOrDefault();
                var staffGuestParticipating = ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(new XsiExhibitionStaffGuestParticipating() { StaffGuestId = travelDto.StaffGuestId, ExhibitionId = travelDto.ExhibitionId }).FirstOrDefault();

                if (staffGuestParticipating != null)
                {
                    staffGuestParticipating.ArrivalDate = travelDto.ArrivalDate;
                    staffGuestParticipating.DepartureDate = travelDto.DepartureDate;
                    staffGuestParticipating.ArrivalAirportId = travelDto.ArrivalAirportId;
                    staffGuestParticipating.ArrivalTerminalId = travelDto.ArrivalTerminalId;
                    staffGuestParticipating.Status = EnumConversion.ToString(EnumStaffGuestStatus.Approved);
                    staffGuestParticipating.ModifiedOn = MethodFactory.ArabianTimeNow();
                    staffGuestParticipating.ModifiedBy = travelDto.MemberId;
                    if (ExhibitionStaffGuestParticipatingService.UpdateExhibitionStaffGuestParticipating(staffGuestParticipating) == EnumResultType.Success)
                    {
                        sbchanges = GetStaffGuestUpdateChanges(staffGuestParticipatingOld, staffGuestParticipating);
                        #region Exhibition Staff Guest Status Update
                        XsiExhibitionStaffGuestParticipating where = new XsiExhibitionStaffGuestParticipating();
                        where.StaffGuestId = travelDto.StaffGuestId;
                        where.ExhibitionId = travelDto.ExhibitionId;
                        where.Status = EnumConversion.ToString(EnumStaffGuestStatus.VisaUploaded);
                        var entity1 = ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(where).OrderByDescending(o => o.StaffGuestId).FirstOrDefault();
                        if (entity1 != null)
                        {
                            entity1.Status = EnumConversion.ToString(EnumStaffGuestStatus.Approved);
                            ExhibitionStaffGuestParticipatingService.UpdateExhibitionStaffGuestParticipating(entity1);
                            SendEmail(staffGuestParticipating, travelDto.WebsiteId);
                            NotifyAdminAboutBooking(travelDto.MemberId, staffGuestParticipating.StaffGuestId, staffGuestParticipating.ExhibitionId, travelDto.WebsiteId, false, sbchanges, 1);
                        }
                        #endregion
                        //HidePaymentDetails(); 
                    }
                    else
                    {
                        //ShowPaymentDetails();
                        MessageDTO.Message = "Failed to add StaffGuest details, Please try adding again.";
                        MessageDTO.MessageTypeResponse = "Error";
                    }
                }
            }
        }

        void SendEmail(XsiExhibitionStaffGuestParticipating entity, long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                using (EmailContentService EmailContentService = new EmailContentService())
                {
                    HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                    #region Send Email To Visa Admin
                    StringBuilder body = new StringBuilder();
                    #region Email Content
                    SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        ScrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(20130);
                        if (ScrfEmailContent != null)
                        {
                            if (ScrfEmailContent.Body != null)
                                StrEmailContentBody = ScrfEmailContent.Body;
                            if (ScrfEmailContent.Email != null)
                                StrEmailContentEmail = ScrfEmailContent.Email;
                            if (ScrfEmailContent.Subject != null)
                                StrEmailContentSubject = ScrfEmailContent.Subject;
                            StrEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                        }
                    }
                    else
                    {
                        EmailContent = EmailContentService.GetEmailContentByItemId(20167);
                        if (EmailContent != null)
                        {
                            if (EmailContent.Body != null)
                                StrEmailContentBody = EmailContent.Body;
                            if (EmailContent.Email != null)
                                StrEmailContentEmail = EmailContent.Email;
                            if (EmailContent.Subject != null)
                                StrEmailContentSubject = EmailContent.Subject;
                            StrEmailContentAdmin = _appCustomSettings.AdminName;
                        }
                    }
                    #endregion

                    body.Append(htmlContentFactory.BindEmailContent(1, StrEmailContentBody, null, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                    if (entity != null)
                    {
                        if (!string.IsNullOrEmpty(entity.NameEn) && !string.IsNullOrEmpty(entity.LastName))
                            body.Replace("$$RepresentativeName$$", entity.NameEn + " " + entity.LastName);
                        else if (!string.IsNullOrEmpty(entity.NameEn))
                            body.Replace("$$RepresentativeName$$", entity.NameEn);
                        else if (!string.IsNullOrEmpty(entity.LastName))
                            body.Replace("$$RepresentativeName$$", entity.LastName);
                        else
                            body.Replace("$$RepresentativeName$$", string.Empty);

                        if (entity.ArrivalDate != null)
                            body.Replace("$$ArrivalDate$$", entity.ArrivalDate.Value.ToString("dd MMM yyyy"));
                        if (entity.ArrivalAirportId != null)
                            body.Replace("$$ArrivalAirport$$", GetArrivalAirport(entity.ArrivalAirportId.Value));
                    }
                    body.Replace("$$ExhibitorNameEn$$", string.Empty);
                    body.Replace("$$FileNumber$$", string.Empty);
                    if (StrEmailContentEmail != string.Empty)
                        _emailSender.SendEmail(StrEmailContentAdmin, _appCustomSettings.AdminEmail, StrEmailContentEmail, StrEmailContentSubject, body.ToString());
                    //SmtpEmail.SendNetEmail(AdminName, AdminEmailAddress, VisaAdminEmail, emailContent.Subject, body.ToString());
                    #endregion
                }
            }
        }

        string GetArrivalAirport(long itemId)
        {
            var ExhibitionArrivalAirportService = new ExhibitionArrivalAirportService();
            var entity = ExhibitionArrivalAirportService.GetExhibitionArrivalAirportByItemId(itemId);
            if (entity != null)
                if (entity.Title != null)
                    return entity.Title;
            return string.Empty;
        }

        private bool IsExhibitionActive(long websiteId)
        {
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {

                XsiExhibition whereQuery = new XsiExhibition();
                whereQuery.IsActive = EnumConversion.ToString(EnumBool.Yes);
                whereQuery.IsArchive = EnumConversion.ToString(EnumBool.No);

                whereQuery.WebsiteId = websiteId;
                //List<XsiExhibition> ExhibitionList = ExhibitionService.GetExhibition(whereQuery).OrderByDescending(i => i.ItemId).ToList();
                XsiExhibition entity = ExhibitionService.GetExhibition(whereQuery).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                if (entity != null)
                {
                    //if (entity.ExhibitionId > 0)
                    //    StaffGuestDTO.ExhibitionId = entity.ExhibitionId;
                    //if (entity.ExhibitorEndDate != null)
                    //    StaffGuestDTO.ExhibitorEndDate = entity.ExhibitorEndDate.Value;
                    //if (entity.StartDate != null)
                    //    StaffGuestDTO.StartDate = entity.StartDate.Value;
                    //if (entity.DateOfDue != null)
                    //    StaffGuestDTO.PenaltyDate = entity.DateOfDue.Value;
                    //if (entity.ExpiryMonths != null)
                    //    StaffGuestDTO.ExpiryMonths = Convert.ToInt32(entity.ExpiryMonths);
                    return true;
                }
            }
            return false;
        }
        string GetCountryTitle(long? countryId, long langId)
        {
            using (ExhibitionCountryService ExhibitionCountryService = new ExhibitionCountryService())
            {
                XsiExhibitionCountry where = new XsiExhibitionCountry();
                if (countryId.HasValue)
                    where.CountryId = countryId.Value;
                XsiExhibitionCountry entity = ExhibitionCountryService.GetExhibitionCountry(where).FirstOrDefault();
                if (entity != null)
                {
                    if (langId == 1 && entity.CountryName != null)
                        return entity.CountryName;
                    else
                        return entity.CountryNameAr;
                }

                return string.Empty;
            }
        }
        private DateTime? GetExhibitionDateOfDue(long websiteId)
        {
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {
                XsiExhibition whereQuery = new XsiExhibition();
                whereQuery.IsActive = EnumConversion.ToString(EnumBool.Yes);
                whereQuery.IsArchive = EnumConversion.ToString(EnumBool.No);
                whereQuery.WebsiteId = websiteId;
                XsiExhibition entity = ExhibitionService.GetExhibition(whereQuery).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                if (entity != null)
                {
                    if (entity.DateOfDue != null)
                        return entity.DateOfDue.Value;
                }
            }
            return null;
        }
        string GetDateFormated(DateTime date, long langId)
        {
            if (langId == 2)
                return date.ToString("MMM dd, yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
            else
                return date.ToString("MMM dd, yyyy");
        }
        private bool CheckExhibition(long staffguestid, long exhibitionId)
        {
            ExhibitionStaffGuestParticipatingService ExhibitionStaffGuestParticipatingService = new ExhibitionStaffGuestParticipatingService();
            var staffguest = ExhibitionStaffGuestParticipatingService.GetExhibitionStaffGuestParticipating(new XsiExhibitionStaffGuestParticipating() { StaffGuestId = staffguestid }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            if (staffguest != null)
            {
                if (staffguest.ExhibitionId == exhibitionId)
                    return true;
                else
                    return false;
            }
            return true;
        }
        bool CheckParticipated(long itemId, long exhibitionid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var list = _context.XsiExhibitionStaffGuestParticipating.Where(x => x.StaffGuestId == itemId && x.ExhibitionId == exhibitionid).ToList();
                if (list.Count > 0)
                    return true;
                else
                    return false;
            }
        }
        #endregion
    }
}

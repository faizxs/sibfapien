﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using System.IO;
using OfficeOpenXml;
using Xsi.ServicesLayer;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class TranslationGrantWinnersController : ControllerBase
    {
        private IMemoryCache _cache;
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public TranslationGrantWinnersController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IMemoryCache memoryCache, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _cache = memoryCache;
            _context = context;
        }

        #region Get Methods
        [HttpGet]
        [Route("Years")]
        public ActionResult<dynamic> GetYears()
        {
            List<YearModel> List = new List<YearModel>();
            int currentyr = MethodFactory.ArabianTimeNow().Year;
            while (currentyr >= 2011)
            {
                YearModel obj = new YearModel();
                obj.Year = currentyr;
                List.Add(obj);
                currentyr = currentyr - 1;
            }
            //for (int i = 2011; i <= 2050; i++)
            //{
            //    YearModel obj = new YearModel();
            //    obj.Year = i;
            //    List.Add(obj);
            //}

            return Ok(List);
        }
        [HttpGet]
        [Route("Edition")]
        public ActionResult<dynamic> GetEdition()
        {
            List<EditionModel> List = new List<EditionModel>();
            for (int i = 1; i <= 100; i++)
            {
                EditionModel obj = new EditionModel();
                obj.Edition = i;
                List.Add(obj);
            }
            return Ok(List);
        }
        #endregion
        #region Post Methods
        [HttpPost]
        public ActionResult<dynamic> GetTGWinners(TGWinnersSearchDTO model)
        {
            try
            {
                long langid = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langid);

                List<XsiExhibitionTranslationGrantBookWinner> WinnerList = new List<XsiExhibitionTranslationGrantBookWinner>();
                string currentyear = MethodFactory.ArabianTimeNow().Year.ToString();
                var predicateTGWinners = PredicateBuilder.New<XsiExhibitionTranslationGrantBookWinner>();

                predicateTGWinners = predicateTGWinners.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicateTGWinners = predicateTGWinners.And(i => i.Category != null);

                if (model.TranslatedTo != null && model.TranslatedTo > 0)
                    predicateTGWinners = predicateTGWinners.And(i => i.TranslatedTo == model.TranslatedTo);

                if (model.CategoryId != null && model.CategoryId > 0)
                    predicateTGWinners = predicateTGWinners.And(i => i.Category == model.CategoryId);

                if (!string.IsNullOrEmpty(model.Year))
                    predicateTGWinners = predicateTGWinners.And(i => i.Year.Equals(model.Year));
                else
                    predicateTGWinners = predicateTGWinners.And(i => i.Year.Equals(currentyear));

                if (langid == 1)
                {
                    if (!string.IsNullOrEmpty(model.BookTitle))
                        predicateTGWinners = predicateTGWinners.And(i => i.BookTitleEn != null && i.BookTitleEn.Contains(model.BookTitle, StringComparison.OrdinalIgnoreCase));

                    if (!string.IsNullOrEmpty(model.WinnerName))
                        predicateTGWinners = predicateTGWinners.And(i => i.NameEn != null && i.NameEn.Contains(model.WinnerName, StringComparison.OrdinalIgnoreCase));
                }
                else
                {
                    if (!string.IsNullOrEmpty(model.BookTitle))
                        predicateTGWinners = predicateTGWinners.And(i => i.BookTitleAr != null && i.BookTitleAr.Contains(model.BookTitle, StringComparison.OrdinalIgnoreCase));

                    if (!string.IsNullOrEmpty(model.WinnerName))
                        predicateTGWinners = predicateTGWinners.And(i => i.NameAr != null && i.NameAr.Contains(model.WinnerName, StringComparison.OrdinalIgnoreCase));
                }

                List<TGWinnersDTO> List = new List<TGWinnersDTO>();
                var searchcount = _context.XsiExhibitionTranslationGrantBookWinner.Where(predicateTGWinners).Select(i => i.ItemId).ToList().Count;
                List = _context.XsiExhibitionTranslationGrantBookWinner.Where(predicateTGWinners).Select(x => new TGWinnersDTO
                {
                    Name = langid == 1 ? x.NameEn : x.NameAr,
                    BookTitle = langid == 1 ? x.BookTitleEn : (x.BookTitleAr ?? x.BookTitleEn),
                    CategoryTitle = GetGenre(x.Category ?? -1, langid, x.TranslationGrantRegistrationId ?? -1),
                    Year = x.Year,
                    BookLanguageTitle = MethodFactory.GetExhibitionLanuageTitle(x.BookLanguage ?? -1, langid),
                    TranslatedToTitle = MethodFactory.GetExhibitionLanuageTitle(x.TranslatedTo ?? -1, langid),
                    BookThumbnail = GetThumbnail(x.BookThumbnail),
                    //  Edition = x.Edition
                }).OrderBy(i => i.BookTitle).Skip((model.pageNumber.Value - 1) * model.pageSize.Value).Take(model.pageSize.Value).ToList();
                var dto = new { List, TotalCount = searchcount };
                return Ok(dto);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Get TG Winners List action: {ex.Message}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        #endregion
        string GetThumbnail(string bookThumbnail)
        {
            if (bookThumbnail != null && !string.IsNullOrEmpty(bookThumbnail))
            {
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "TranslationGrantBookWinner\\BookThumbnail\\" + bookThumbnail))
                    return _appCustomSettings.UploadsCMSPath + "/TranslationGrantBookWinner/BookThumbnail/" + bookThumbnail;
            }
            return _appCustomSettings.UploadsCMSPath + "/contentnew/content/img/book-cover.jpg";
        }
        string GetGenre(long genreid, long langId, long tgmemberId)
        {
            if (genreid == 9)
            {
                using (var TranslationGrantMemberService = new TranslationGrantMemberService())
                {
                    var entity = TranslationGrantMemberService.GetTranslationGrantMemberByItemId(tgmemberId);
                    if (entity != null && !string.IsNullOrWhiteSpace(entity.Genre))
                    {
                        return entity.Genre;
                    }
                }
                return langId == 1 ? "Other" : "أخرى";
            }
            else
            {
                return GetTranslationGrantGenreId(genreid, langId);
            }
        }
        string GetTranslationGrantGenreId(long genreid, long langId)
        {
            switch (genreid)
            {
                case 1: return (langId == 1) ? "Fiction" : "خيال";
                case 2: return (langId == 1) ? "Non Fiction" : "واقعي";
                case 3: return (langId == 1) ? "Memoir" : "السيرة الذاتية";
                case 4: return (langId == 1) ? "History" : "كتب التاريخ";
                case 5: return (langId == 1) ? "Cookery" : "كتب الطبخ";
                case 6: return (langId == 1) ? "Children's" : "كتب الأطفال";
                case 7: return (langId == 1) ? "Young Adult" : "كتب اليافعين";
                case 8: return (langId == 1) ? "Poetry" : "شعر";

                default: return string.Empty;
            }
        }
    }
}

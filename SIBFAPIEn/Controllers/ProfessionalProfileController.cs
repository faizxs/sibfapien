﻿using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xsi.ServicesLayer;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProfessionalProfileController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly IEmailSender _emailSender;

        public ProfessionalProfileController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IEmailSender emailSender)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _emailSender = emailSender;
        }
        #region Get Methods
        [HttpGet]
        [Route("{registeredid}/{ppsid}")]
        public async Task<ActionResult<dynamic>> GetProfessionalProfile(long registeredid, long? ppsid)
        {
            MessageOtherProfileDTO messageDTO = new MessageOtherProfileDTO();
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
                    {
                        XsiExhibitionProfessionalProgramRegistration entity = MethodFactory.VerifyProfessionalProgramRegistration(memberId);
                        if (entity != default(XsiExhibitionProfessionalProgramRegistration) && entity.Status == "A")
                        {
                            ProfessionalProfileDTO dto = LoadContent(registeredid);
                            messageDTO.ProfessionalProfileDTO = dto;
                            messageDTO.SlotsList = GetSlotses(dto.ProfessionalProgramId, registeredid);
                            messageDTO.MessageTypeResponse = "Success";
                        }
                        else
                        {
                            messageDTO.Message = "You are registration status not approved.";
                            messageDTO.MessageTypeResponse = "Error";
                        }
                    }

                    return Ok(messageDTO);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetProfessionalProfile action: {ex.InnerException}");
                messageDTO.Message = "Something went wrong. Please try again later. ";
                messageDTO.MessageTypeResponse = "Error";
                return Ok(messageDTO);
            }
        }
        #endregion
        #region Post Methods

        #endregion
        #region Helper Methods
        ProfessionalProfileDTO LoadContent(long itemId)
        {
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                ProfessionalProfileDTO model = new ProfessionalProfileDTO();
                XsiExhibitionProfessionalProgramRegistration entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(itemId);
                if (entity != null)
                {

                    #region Registration Fields 
                    model.ItemId = entity.ItemId;
                    model.ProfessionalProgramId = entity.ProfessionalProgramId;
                    model.TableId = entity.TableId;
                    model.MemberId = entity.MemberId;
                    if (entity.FileName != null)
                    {
                        if (System.IO.File.Exists(_appCustomSettings.UploadsCMSPath + "/content/uploads/PPRegistration/Avatar/" + entity.FileName))
                            model.Image = "/content/uploads/PPRegistration/Avatar/" + entity.FileName;
                        else if (entity.Title != null)
                        {
                            if (entity.Title == "M")
                                model.Image = "/Content/assets/images/avatarmale.png";
                            else
                                model.Image = "/Content/assets/images/avatarfemale.png";
                        }
                    }
                    else
                    {
                        if (entity.Title == "M")
                            model.Image = "/Content/assets/images/avatarmale.png";
                        else
                            model.Image = "/Content/assets/images/avatarfemale.png";
                    }
                    string strLastName = string.Empty;
                    if (entity.FirstName != null && entity.FirstName != string.Empty)
                        model.Name = entity.FirstName;
                    else if (entity.FirstNameAr != null && entity.FirstNameAr != string.Empty)
                        model.Name = entity.FirstNameAr;
                    if (entity.LastName != null && entity.LastName != string.Empty)
                        strLastName = " " + entity.LastName;
                    else if (entity.LastNameAr != null && entity.LastNameAr != string.Empty)
                        strLastName = " " + entity.LastNameAr;
                    model.Name += strLastName;

                    model.CompanyName = entity.CompanyName;
                    model.CompanyNameAr = entity.CompanyNameAr;
                    model.JobTitle = entity.JobTitle;
                    model.JobTitleAr = entity.JobTitleAr;
                    model.WorkProfile = entity.WorkProfile;
                    model.WorkProfileAr = entity.WorkProfileAr;
                    model.Website = entity.Website;
                    model.TradeType = entity.TradeType;

                    if (entity.TradeType != null)
                        if (entity.TradeType == EnumConversion.ToString(TradeType.Both))
                            model.TradeTypeName = "Both";
                        else if (entity.TradeType == EnumConversion.ToString(TradeType.Buying))
                            model.TradeTypeName = "Buying";
                        else if (entity.TradeType == EnumConversion.ToString(TradeType.Selling))
                            model.TradeTypeName = "Selling";

                    model.IsEbooks = entity.IsEbooks;
                    model.IsInterested = entity.IsInterested;
                    model.ExamplesOfTranslatedBooks = entity.ExamplesOfTranslatedBooks;
                    model.UnSoldRightsBooks = entity.UnSoldRightsBooks;
                    model.IsAlreadyBoughtRights = entity.IsAlreadyBoughtRights;
                    if (entity.IsAlreadyBoughtRights == EnumConversion.ToString(EnumBool.Yes))
                        model.KeyPublisherTitlesOrBusinessRegion = "Key publishers (and titles)";
                    else
                        model.KeyPublisherTitlesOrBusinessRegion = "What would encourage you to do more business in the region?";
                    model.KeyPublisherTitlesOrBusinessRegion = entity.KeyPublisherTitlesOrBusinessRegion;
                    model.MainTerritories = entity.MainTerritories;
                    if (entity.FileName1 != null && !string.IsNullOrEmpty(entity.FileName1))
                    {
                        model.FileName1 = "/content/uploads/PPRegistration/Document/" + entity.FileName1;
                    }
                    if (entity.BooksUrl != null && !string.IsNullOrEmpty(entity.BooksUrl))
                    {
                        if (entity.BooksUrl.IndexOf("http") > -1)
                            model.BooksURL = entity.BooksUrl;
                        else
                            model.BooksURL = "http://" + entity.BooksUrl;
                    }
                    #endregion
                    #region Load Genre GroupId Tags
                    PPGenreService PPGenreService = new PPGenreService();
                    XsiExhibitionPpgenre ppgenreenttiy = new XsiExhibitionPpgenre();
                    ppgenreenttiy.ProfessionalProgramRegistrationId = entity.ItemId;
                    List<XsiExhibitionPpgenre> PPGenreList = PPGenreService.GetPPGenre(ppgenreenttiy);
                    StringBuilder sb = new StringBuilder();
                    string GenreTitle = string.Empty;
                    foreach (XsiExhibitionPpgenre rows in PPGenreList)
                    {
                        GenreService GenreService = new GenreService();
                        List<XsiGenre> ListGenre = GenreService.GetGenre(new XsiGenre { ItemId = rows.GenreId.Value });
                        foreach (XsiGenre genre in ListGenre)
                        {
                            if (genre.Title != null)
                                if (!string.IsNullOrEmpty(genre.Title))
                                    GenreTitle = genre.Title;
                        }
                        sb.Append("<li><div class='black-tip'><span>" + GenreTitle + "</span><i class='icon-sort-down'></i></div><a data-genregid='" + rows.GenreId.ToString() + "'>" + GenreTitle + " <i class='icon-remove' style='display:none'></i></a></li>");
                    }
                    if (sb != null)
                        model.GenreTags = sb.ToString();
                    #endregion
                }
                return model;
            }
        }
        SlotsDTO GetSlotses(long? professionalprogramid, long ppregid)
        {
            SlotsDTO model = new SlotsDTO();
            model.SlotsList = new List<CustomScheduleDTO>();
            ProfessionalProgramSlotService ProfessionalProgramSlotService;
            PPSlotRegistrationService PPSlotRegistrationService;
            XsiExhibitionPpslotInvitation where1;
            List<XsiSlots> SlotsList = new List<XsiSlots>();
            List<XsiSlots> SlotsListNew = new List<XsiSlots>();
            List<XsiExhibitionProfessionalProgramSlots> ProfessionalProgramSlotList;
            string IsAvailableDayOne = EnumConversion.ToString(EnumBool.No);
            string IsAvailableDayTwo = EnumConversion.ToString(EnumBool.No);
            string IsAvailableDayThree = EnumConversion.ToString(EnumBool.No);

            string IsApprovedStatusDayOne = EnumConversion.ToString(EnumBool.No);
            string IsApprovedStatusDayTwo = EnumConversion.ToString(EnumBool.No);
            string IsApprovedStatusDayThree = EnumConversion.ToString(EnumBool.No);

            long count1 = 0, count2 = 0, sum = 0;
            using (ProfessionalProgramSlotService = new ProfessionalProgramSlotService())
            {
                XsiExhibitionProfessionalProgramSlots where = new XsiExhibitionProfessionalProgramSlots();
                where.ProfessionalProgramId = professionalprogramid;
                where.IsDelete = EnumConversion.ToString(EnumBool.No);
                ProfessionalProgramSlotList = ProfessionalProgramSlotService.GetProfessionalProgramSlot(where);
                if (ProfessionalProgramSlotList.Count() > 0)
                {
                    List<XsiExhibitionProfessionalProgramSlots> ProfessionalProgramSlotList1 = ProfessionalProgramSlotList.Where(p => p.DayId == 1).ToList();
                    List<XsiExhibitionProfessionalProgramSlots> ProfessionalProgramSlotList2 = ProfessionalProgramSlotList.Where(p => p.DayId == 2).ToList();
                    List<XsiExhibitionProfessionalProgramSlots> ProfessionalProgramSlotList3 = ProfessionalProgramSlotList.Where(p => p.DayId == 3).ToList();
                    #region Day One
                    foreach (XsiExhibitionProfessionalProgramSlots entity in ProfessionalProgramSlotList1)
                    {
                        #region Insert DayOne
                        #region Day one slot count
                        PPSlotRegistrationService = new PPSlotRegistrationService();
                        where1 = new XsiExhibitionPpslotInvitation();
                        where1.RequestId = ppregid;
                        where1.SlotId = entity.ItemId;
                        where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        count1 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                        if (count1 < 1)
                        {
                            PPSlotRegistrationService = new PPSlotRegistrationService();
                            where1 = new XsiExhibitionPpslotInvitation();
                            where1.ResponseId = ppregid;
                            where1.SlotId = entity.ItemId;
                            where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                            count2 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                        }
                        #endregion
                        sum = count1 + count2;
                        if (sum < 2)
                            IsAvailableDayOne = EnumConversion.ToString(EnumBool.Yes);
                        if (count1 == 1)
                            IsAvailableDayOne = EnumConversion.ToString(EnumBool.No);

                        //IsApprovedStatusDayOne = PPSlotRegistrationService.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { SlotId = entity.ItemId, Status = "A" }).Count() > 0 ? "Y" : "N";
                        SlotsList.Add(
                            new XsiSlots()
                            {
                                ItemId = entity.ItemId,
                                DayOneId = entity.ItemId,
                                StartTime = entity.StartTime.Value.ToString("HH:mm"),
                                EndTime = entity.EndTime.Value.ToString("HH:mm"),
                                IsBreakDayOne = entity.IsBreak,
                                IsAvailableDayOne = IsAvailableDayOne,
                                Break1Title = entity.BreakTitle

                            });
                        #endregion

                    }
                    #endregion
                    #region Day Two
                    sum = 0; count1 = 0; count2 = 0;
                    bool IsRepeat = false;
                    foreach (XsiExhibitionProfessionalProgramSlots entity in ProfessionalProgramSlotList2)
                    {
                        foreach (XsiSlots entity2 in SlotsList)
                        {
                            #region Update Day Two
                            if (entity.StartTime.Value.ToString("HH:mm") == entity2.StartTime)
                            {
                                entity2.DayTwoId = entity.ItemId;
                                entity2.IsBreakDayTwo = entity.IsBreak;
                                entity2.Break2Title = entity.BreakTitle;
                                #region Day two slot count
                                PPSlotRegistrationService = new PPSlotRegistrationService();
                                where1 = new XsiExhibitionPpslotInvitation();
                                where1.RequestId = ppregid;
                                where1.SlotId = entity.ItemId;
                                where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                count1 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                                if (count1 < 1)
                                {
                                    PPSlotRegistrationService = new PPSlotRegistrationService();
                                    where1 = new XsiExhibitionPpslotInvitation();
                                    where1.ResponseId = ppregid;
                                    where1.SlotId = entity.ItemId;
                                    where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                    count2 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                                }
                                #endregion
                                sum = count1 + count2;
                                if (sum < 2)
                                    IsAvailableDayTwo = EnumConversion.ToString(EnumBool.Yes);
                                if (count1 == 1)
                                    IsAvailableDayTwo = EnumConversion.ToString(EnumBool.No);
                                entity2.IsAvailableDayTwo = IsAvailableDayTwo;
                                sum = 0; count1 = 0; count2 = 0; IsAvailableDayTwo = EnumConversion.ToString(EnumBool.No);
                                IsRepeat = true;
                                break;
                            }
                            else
                                IsRepeat = false;
                            #endregion
                        }
                        if (!IsRepeat)
                        {
                            #region Insert Day Two
                            #region Day two slot count
                            PPSlotRegistrationService = new PPSlotRegistrationService();
                            where1 = new XsiExhibitionPpslotInvitation();
                            where1.RequestId = ppregid;
                            where1.SlotId = entity.ItemId;
                            where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                            count1 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                            if (count1 < 1)
                            {
                                PPSlotRegistrationService = new PPSlotRegistrationService();
                                where1 = new XsiExhibitionPpslotInvitation();
                                where1.ResponseId = ppregid;
                                where1.SlotId = entity.ItemId;
                                where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                count2 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                            }
                            #endregion
                            sum = count1 + count2;
                            if (sum < 2)
                                IsAvailableDayTwo = EnumConversion.ToString(EnumBool.Yes);
                            if (count1 == 1)
                                IsAvailableDayTwo = EnumConversion.ToString(EnumBool.No);
                            //IsApprovedStatusDayTwo = PPSlotRegistrationService.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { SlotId = entity.ItemId, Status = "A" }).Count() > 0 ? "Y" : "N";
                            SlotsList.Add(
                           new XsiSlots()
                           {
                               ItemId = entity.ItemId,
                               DayTwoId = entity.ItemId,
                               StartTime = entity.StartTime.Value.ToString("HH:mm"),
                               EndTime = entity.EndTime.Value.ToString("HH:mm"),
                               IsBreakDayTwo = entity.IsBreak,
                               IsAvailableDayTwo = IsAvailableDayTwo,
                               Break2Title = entity.BreakTitle
                           });
                            #endregion
                        }
                    }
                    #endregion
                    #region Day Three
                    sum = 0; count1 = 0; count2 = 0;
                    IsRepeat = false;
                    foreach (XsiExhibitionProfessionalProgramSlots entity in ProfessionalProgramSlotList3)
                    {
                        foreach (XsiSlots entity3 in SlotsList)
                        {
                            #region Update Day Three
                            if (entity.StartTime.Value.ToString("HH:mm") == entity3.StartTime)
                            {
                                entity3.DayThreeId = entity.ItemId;
                                entity3.IsBreakDayThree = entity.IsBreak;
                                entity3.Break3Title = entity.BreakTitle;
                                #region Day Three slot count
                                PPSlotRegistrationService = new PPSlotRegistrationService();
                                where1 = new XsiExhibitionPpslotInvitation();
                                where1.RequestId = ppregid;
                                where1.SlotId = entity.ItemId;
                                where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                count1 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                                if (count1 < 1)
                                {
                                    PPSlotRegistrationService = new PPSlotRegistrationService();
                                    where1 = new XsiExhibitionPpslotInvitation();
                                    where1.ResponseId = ppregid;
                                    where1.SlotId = entity.ItemId;
                                    where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                    count2 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                                }
                                #endregion
                                sum = count1 + count2;
                                if (sum < 2)
                                    IsAvailableDayThree = EnumConversion.ToString(EnumBool.Yes);
                                if (count1 == 1)
                                    IsAvailableDayThree = EnumConversion.ToString(EnumBool.No);
                                entity3.IsAvailableDayThree = IsAvailableDayThree;
                                sum = 0; count1 = 0; count2 = 0; IsAvailableDayThree = EnumConversion.ToString(EnumBool.No);
                                IsRepeat = true;
                                break;
                            }
                            else
                                IsRepeat = false;
                            #endregion
                        }
                        if (!IsRepeat)
                        {
                            #region Insert Day Three
                            #region Day Three slot count
                            PPSlotRegistrationService = new PPSlotRegistrationService();
                            where1 = new XsiExhibitionPpslotInvitation();
                            where1.RequestId = ppregid;
                            where1.SlotId = entity.ItemId;
                            where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                            count1 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                            if (count1 < 1)
                            {
                                PPSlotRegistrationService = new PPSlotRegistrationService();
                                where1 = new XsiExhibitionPpslotInvitation();
                                where1.ResponseId = ppregid;
                                where1.SlotId = entity.ItemId;
                                where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                count2 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                            }
                            #endregion
                            sum = count1 + count2;
                            if (sum < 2)
                                IsAvailableDayThree = EnumConversion.ToString(EnumBool.Yes);
                            if (count1 == 1)
                                IsAvailableDayThree = EnumConversion.ToString(EnumBool.No);
                            //IsApprovedStatusDayThree = PPSlotRegistrationService.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { SlotId = entity.ItemId, Status = "A" }).Count() > 0 ? "Y" : "N";
                            SlotsList.Add(
                           new XsiSlots()
                           {
                               ItemId = entity.ItemId,
                               DayThreeId = entity.ItemId,
                               StartTime = entity.StartTime.Value.ToString("HH:mm"),
                               EndTime = entity.EndTime.Value.ToString("HH:mm"),
                               IsBreakDayThree = entity.IsBreak,
                               IsAvailableDayThree = IsAvailableDayThree,
                               Break2Title = entity.BreakTitle
                           });
                            #endregion
                        }
                    }
                    #endregion
                }
                //  SlotsListNew = SlotsList.Select(i => new { i.ItemId, i.DayOneId, i.DayTwoId, i.StartTime, i.EndTime, i.IsBreakDayOne, i.IsBreakDayTwo, i.IsAvailableDayOne, i.IsAvailableDayTwo, i.IsApprovedStatusDayOne, i.IsApprovedStatusDayTwo }).ToList();
                PPSlotRegistrationService PPSlotRegistrationService1 = new PPSlotRegistrationService();
                SlotsList = SlotsList.OrderBy(i => i.StartTime).ToList();
                if (SlotsList.Count() > 0)
                {
                    var CustomScheduleList1 = new List<CustomScheduleDTO>();
                    foreach (XsiSlots cols in SlotsList)
                    {
                        CustomScheduleDTO schedule = new CustomScheduleDTO();
                        var daySlotInfo = GetSlotsInfo(cols);
                        schedule.StartTime = cols.StartTime;
                        schedule.EndTime = cols.EndTime;

                        schedule.SlotDayOneText = daySlotInfo.SlotDayOneText;
                        schedule.SlotDayOneClass = daySlotInfo.SlotDayOneClass;
                        schedule.DayOneSlotId = cols.DayOneId.Value;

                        schedule.SlotDayTwoText = daySlotInfo.SlotDayTwoText;
                        schedule.SlotDayTwoClass = daySlotInfo.SlotDayTwoClass;
                        schedule.DayTwoSlotId = cols.DayTwoId.Value;

                        schedule.SlotDayThreeText = daySlotInfo.SlotDayThreeText;
                        schedule.SlotDayThreeClass = daySlotInfo.SlotDayThreeClass;
                        schedule.DayThreeSlotId = cols.DayThreeId.Value;
                        CustomScheduleList1.Add(schedule);
                    }

                   /* SlotsList = SlotsList.Select(i => new XsiSlots
                    {
                        ItemId = i.ItemId,
                        DayOneId = i.DayOneId,
                        DayTwoId = i.DayTwoId,
                        DayThreeId = i.DayThreeId,
                        StartTime = i.StartTime,
                        EndTime = i.EndTime,
                        IsBreakDayOne = i.IsBreakDayOne,
                        IsBreakDayTwo = i.IsBreakDayTwo,
                        IsBreakDayThree = i.IsBreakDayThree,
                        IsAvailableDayOne = i.IsAvailableDayOne,
                        IsAvailableDayTwo = i.IsAvailableDayTwo,
                        IsAvailableDayThree = i.IsAvailableDayThree,
                        IsApprovedStatusDayOne = PPSlotRegistrationService1.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { Status = "A", SlotId = i.DayOneId }).Where(p => p.RequestId == ppregid || p.ResponseId == ppregid).Count() > 0 ? "Y" : "N",
                        IsApprovedStatusDayTwo = PPSlotRegistrationService1.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { Status = "A", SlotId = i.DayTwoId }).Where(p => p.RequestId == ppregid || p.ResponseId == ppregid).Count() > 0 ? "Y" : "N",
                        IsApprovedStatusDayThree = PPSlotRegistrationService1.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { Status = "A", SlotId = i.DayThreeId }).Where(p => p.RequestId == ppregid || p.ResponseId == ppregid).Count() > 0 ? "Y" : "N",
                        Break1Title = i.Break1Title,
                        Break2Title = i.Break2Title,
                        Break3Title = i.Break3Title
                    }).OrderBy(i => i.StartTime).ToList();*/
                    if (CustomScheduleList1.Count() > 0)
                    {
                        model.SlotsList = CustomScheduleList1;
                    }
                }
            }
            var PPEntity = MethodFactory.GetProfessionalProgram();
            if (PPEntity != null)
            {
                if (PPEntity.StartDateDayOne != null)
                    model.DayOne = PPEntity.StartDateDayOne.Value.ToString("dddd dd MMMM");
                if (PPEntity.StartDateDayTwo != null)
                    model.DayTwo = PPEntity.StartDateDayTwo.Value.ToString("dddd dd MMMM");
                if (PPEntity.StartDateDayThree != null)
                    model.DayThree = PPEntity.StartDateDayThree.Value.ToString("dddd dd MMMM");
            }

            return model;
        }
        private CustomScheduleDTO GetSlotsInfo(XsiSlots obj)
        {
            CustomScheduleDTO dto = new CustomScheduleDTO();
            #region Day One
            if (obj.IsBreakDayOne != null)
            {
                if (obj.IsBreakDayOne == "Y" || obj.IsBreakDayOne == "O")
                {
                    dto.SlotDayOneClass = "red-text-bg";
                    dto.SlotDayOneText = obj.Break1Title;
                }
                else
                {
                    if (obj.IsApprovedStatusDayOne == "Y")
                    {
                        dto.SlotDayOneClass = "green-text-bg";
                        dto.SlotDayOneText = "";
                    }
                    else if (obj.IsAvailableDayOne != null)
                    {
                        if (obj.IsAvailableDayOne == "Y")
                        {
                            dto.SlotDayOneClass = "green-text-bg";
                            dto.SlotDayOneText = obj.IsAvailableDayOne;//"<a  data-slot='" + obj.DayOneId + "' class='OpenSendRequest' style='cursor:pointer' data-toggle='modal' data-target='#sendRequestModal'>Click to schedule a meetup</a>";
                        }
                        else
                        {
                            dto.SlotDayOneClass = "green-text-bg";
                            dto.SlotDayOneText = "";
                        }
                    }
                    else
                    {
                        dto.SlotDayOneClass = "red-text-bg";
                        dto.SlotDayOneText = "Seminar programme";
                    }
                }
            }
            else
            {
                dto.SlotDayOneClass = "red-text-bg";
                dto.SlotDayOneText = "Seminar programme";
            }
            #endregion
            #region Day Two
            if (obj.IsBreakDayTwo != null)
            {
                if (obj.IsBreakDayTwo == "Y" || obj.IsBreakDayTwo == "O")
                {
                    dto.SlotDayTwoClass = "red-text-bg";
                    dto.SlotDayTwoText = obj.Break2Title;
                }
                else
                {
                    if (obj.IsApprovedStatusDayTwo == "Y")
                    {
                        dto.SlotDayTwoClass = "green-text-bg";
                        dto.SlotDayTwoText = "";
                    }
                    else if (obj.IsAvailableDayTwo != null)
                    {
                        if (obj.IsAvailableDayTwo == "Y")
                        {
                            dto.SlotDayTwoClass = "green-text-bg";
                            dto.SlotDayTwoText = obj.IsAvailableDayTwo; //"<a  data-slot='" + obj.DayTwoId + "' class='OpenSendRequest' style='cursor:pointer' data-toggle='modal' data-target='#sendRequestModal'>Click to schedule a meetup</a>";
                        }
                        else
                        {
                            dto.SlotDayTwoClass = "green-text-bg";
                            dto.SlotDayTwoText = "";
                        }
                    }
                    else
                    {
                        dto.SlotDayTwoClass = "red-text-bg";
                        dto.SlotDayTwoText = "Seminar programme";
                    }
                }
            }
            else
            {
                dto.SlotDayTwoClass = "red-text-bg";
                dto.SlotDayTwoText = "Seminar programme";
            }
            #endregion
            #region Day Three
            if (obj.IsBreakDayThree != null)
            {
                if (obj.IsBreakDayThree == "Y" || obj.IsBreakDayThree == "O")
                {
                    dto.SlotDayThreeClass = "red-text-bg";
                    dto.SlotDayThreeText = obj.Break2Title;
                }
                else
                {
                    if (obj.IsApprovedStatusDayThree == "Y")
                    {
                        dto.SlotDayThreeClass = "green-text-bg";
                        dto.SlotDayThreeText = "";
                    }
                    else if (obj.IsAvailableDayThree != null)
                    {
                        if (obj.IsAvailableDayThree == "Y")
                        {
                            dto.SlotDayThreeClass = "green-text-bg";
                            dto.SlotDayThreeText = obj.IsAvailableDayThree;// "<a  data-slot='" + obj.DayThreeId + "' class='OpenSendRequest' style='cursor:pointer' data-toggle='modal' data-target='#sendRequestModal'>Click to schedule a meetup</a>";
                        }
                        else
                        {
                            dto.SlotDayThreeClass = "green-text-bg";
                            dto.SlotDayThreeText = "";
                        }
                    }
                    else
                    {
                        dto.SlotDayThreeClass = "red-text-bg";
                        dto.SlotDayThreeText = "Seminar programme";
                    }
                }
            }
            else
            {
                dto.SlotDayThreeClass = "red-text-bg";
                dto.SlotDayThreeText = "Seminar programme";
            }
            #endregion
            return dto;
        }
        #endregion
    }
}

﻿using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SIBFAPIEn.DTO;
using Entities.Models;
using Xsi.ServicesLayer;
using Microsoft.Extensions.Localization;
using System.Text;
using SIBFAPIEn.Utility;
using System.IO;
using QRCoder;
using System.Drawing;
using System.Drawing.Imaging;
using Org.BouncyCastle.Bcpg.OpenPgp;
using Microsoft.EntityFrameworkCore;
//using Entities.Models;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ExhibitionShipmentController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;
        public ExhibitionShipmentController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings, IEmailSender emailSender, sibfnewdbContext context)
        {
            _logger = logger;
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        #region Get
        [HttpGet("{websiteId}/{memberid}")]
        [HttpGet("{websiteId}/{memberid}/{exhibitionid}")]
        public ActionResult<dynamic> GetExhibitionShipmentNew(long websiteId, long memberid, long exhibitionid = -1)
        {

            try
            {
                //websiteId = 1;
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long SIBFMemberId = User.Identity.GetID();
                if (SIBFMemberId == memberid)
                {
                    using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
                    {
                        long exhibitorId = -1;
                        List<ExhibitionShipmentItemDTO> ExhibitionShipmentListNew = new List<ExhibitionShipmentItemDTO>();
                        List<XsiExhibitionShipment> ExhibitionShipmentList = new List<XsiExhibitionShipment>();
                        XsiExhibitionMemberApplicationYearly MemberExhibitionYearly = new XsiExhibitionMemberApplicationYearly();
                        ExhibitionShipmentService ExhibitionShipmentService = new ExhibitionShipmentService();
                        AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService();
                        RestaurantRegistrationService RestaurantRegistrationService = new RestaurantRegistrationService();
                        ExhibitionService ExhibitionService = new ExhibitionService();
                        var Exhibition = ExhibitionService.GetExhibition(new XsiExhibition() { WebsiteId = websiteId, IsActive = "Y" }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault(); // IsArchive = "N"
                        if (Exhibition != null)
                        {
                            MemberExhibitionYearly = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly() { MemberId = memberid, ExhibitionId = Exhibition.ExhibitionId, MemberRoleId = 1 }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                            if (MemberExhibitionYearly == null)
                            {
                                MemberExhibitionYearly = AgencyRegistrationService.GetAgencyRegistration(new XsiExhibitionMemberApplicationYearly() { MemberId = memberid, ExhibitionId = Exhibition.ExhibitionId, MemberRoleId = 2 }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                                if (MemberExhibitionYearly == null)
                                {
                                    MemberExhibitionYearly = RestaurantRegistrationService.GetRestaurantRegistration(new XsiExhibitionMemberApplicationYearly() { MemberId = memberid, ExhibitionId = Exhibition.ExhibitionId, MemberRoleId = 8 }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                                }
                            }

                            if (MemberExhibitionYearly != null)
                            {
                                if (MemberExhibitionYearly.MemberRoleId == 2)
                                    exhibitorId = MemberExhibitionYearly.ParentId ?? -1;
                                else
                                    exhibitorId = MemberExhibitionYearly.MemberExhibitionYearlyId;

                                if (exhibitorId > -1)
                                {
                                    XsiExhibitionShipment where = new XsiExhibitionShipment();
                                    where.MemberId = SIBFMemberId;
                                    where.IsActive = "Y";
                                    if (exhibitionid > -1)
                                    {
                                        where.ExhibitionId = exhibitionid;
                                    }

                                    if (LangId == 1)
                                    {
                                        ExhibitionShipmentListNew = ExhibitionShipmentService.GetExhibitionShipment(where).Where(i => !string.IsNullOrEmpty(i.QrfileName))
                                            .Select(i => new ExhibitionShipmentItemDTO()
                                            {
                                                ItemId = i.ItemId,
                                                ExhibitorName = MethodFactory.GetExhibitorName(i.ExhibitorId ?? -1, LangId),
                                                FileNumber = MethodFactory.GetExhibitorFileNumber(i.ExhibitorId ?? -1),
                                                CountryName = MethodFactory.GetCountryNameByExhibitorIdOrAgencyId(i.ExhibitorId ?? -1, i.AgencyId ?? -1, LangId),
                                                IsAgency = i.IsAgency == "Y" ? "Yes" : "No",
                                                AgencyList = GetAgencyListByShipmentId(i.ItemId, exhibitorId, 1),
                                                ShipmentCount = i.ShipmentCount,
                                                ShipmentTypeName = MethodFactory.GetShipmentTypeName(i.ShipmentType ?? -1, 1),
                                                ShipmentCompanyName = i.ShipmentCompanyName,
                                                BillofLading = i.BillofLading,
                                                ShipmentNumber = i.ShipmentNumber,
                                                ShipmentCountryNameFrom = MethodFactory.GetCountryName(i.ShipmentCountryFrom ?? -1, 1),
                                                ShipmentCountryNameTo = MethodFactory.GetCountryName(i.ShipmentCountryTo ?? -1, 1),
                                                ReceivedShipmentLocationTypeName = MethodFactory.GetShipmentTypeName(i.ReceivedShipmentLocationType ?? -1, 1),
                                                ReceivingCompanyName = i.ReceivingCompanyName,
                                                QrfileName = _appCustomSettings.UploadsCMSPath + "/ExhibitionShipment/" + i.QrfileName,
                                                NoofBooks = i.NoofBooks,
                                                CategoryId = i.CategoryId,
                                                BookCategoryName = GetBookCategoryName(i.CategoryId, LangId),
                                                BookFileName = _appCustomSettings.UploadsCMSPath + "/ExhibitionShipment/" + i.BooksFile,
                                                Status = GetUserStatus(i.Status, LangId),
                                                IsLocalWarehouse = i.IsLocalWarehouse == "Y" ? "Yes" : "No",
                                                WShipmentCount = i.WshipmentCount,
                                                WShipmentCompanyName = i.WshipmentCompanyName,
                                                WShipmentNumber = i.WshipmentNumber,
                                                WNoofBooks = i.WnoofBooks,
                                                WBookFileName = _appCustomSettings.UploadsCMSPath + "/ExhibitionShipment/WBookFile/" + i.WbooksFile,
                                                //i.CountryId,
                                                // i.ShipmentType,
                                                // i.ShipmentCountryFrom,
                                                // i.ShipmentCountryTo,
                                                //i.ReceivedShipmentLocationType,
                                            }).OrderByDescending(i => i.ItemId)
                                      .ToList();
                                    }
                                    else
                                    {
                                        ExhibitionShipmentListNew = ExhibitionShipmentService.GetExhibitionShipment(where).Where(i => !string.IsNullOrEmpty(i.QrfileName))
                                      .Select(i => new ExhibitionShipmentItemDTO()
                                      {
                                          ItemId = i.ItemId,
                                          ExhibitorName = MethodFactory.GetExhibitorName(i.ExhibitorId ?? -1, LangId),
                                          FileNumber = MethodFactory.GetExhibitorFileNumber(i.ExhibitorId ?? -1),
                                          CountryName = MethodFactory.GetCountryNameByExhibitorIdOrAgencyId(i.ExhibitorId ?? -1, i.AgencyId ?? -1, LangId),
                                          IsAgency = i.IsAgency == "Y" ? "Yes" : "No",
                                          AgencyList = GetAgencyListByShipmentId(i.ItemId, exhibitorId, 2),
                                          ShipmentCount = i.ShipmentCount,
                                          ShipmentTypeName = MethodFactory.GetShipmentTypeName(i.ShipmentType ?? -1, 2),
                                          ShipmentCompanyName = i.ShipmentCompanyName,
                                          BillofLading = i.BillofLading,
                                          ShipmentNumber = i.ShipmentNumber,
                                          ShipmentCountryNameFrom = MethodFactory.GetCountryName(i.ShipmentCountryFrom ?? -1, 2),
                                          ShipmentCountryNameTo = MethodFactory.GetCountryName(i.ShipmentCountryTo ?? -1, 2),
                                          ReceivedShipmentLocationTypeName = MethodFactory.GetShipmentTypeName(i.ReceivedShipmentLocationType ?? -1, 2),
                                          ReceivingCompanyName = i.ReceivingCompanyName,
                                          QrfileName = _appCustomSettings.UploadsCMSPath + "/ExhibitionShipment/" + i.QrfileName,
                                          NoofBooks = i.NoofBooks,
                                          CategoryId = i.CategoryId,
                                          BookCategoryName = GetBookCategoryName(i.CategoryId, LangId),
                                          BookFileName = _appCustomSettings.UploadsCMSPath + "/ExhibitionShipment/" + i.BooksFile,
                                          Status = GetUserStatus(i.Status, LangId),
                                          IsLocalWarehouse = (i.IsLocalWarehouse == "Y") ? "نعم" : "لا",
                                          WShipmentCount = i.WshipmentCount,
                                          WShipmentCompanyName = i.WshipmentCompanyName,
                                          WShipmentNumber = i.WshipmentNumber,
                                          WNoofBooks = i.WnoofBooks,
                                          WBookFileName = _appCustomSettings.UploadsCMSPath + "/ExhibitionShipment/WBookFile/" + i.WbooksFile,
                                          //i.CountryId,
                                          // i.ShipmentType,
                                          // i.ShipmentCountryFrom,
                                          // i.ShipmentCountryTo,
                                          //i.ReceivedShipmentLocationType,
                                      }).OrderByDescending(i => i.ItemId)
                                      .ToList();
                                    }

                                    return Ok(new { MessageTypeResponse = "Success", ExhibitionShipmentListNew, ExhibitorId = exhibitorId, MemberId = memberid });
                                }
                            }
                            else
                            {
                                return Ok(new { MessageTypeResponse = "Success", ExhibitionShipmentListNew, ExhibitorId = exhibitorId, MemberId = memberid });
                            }
                        }
                        return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "No data found." });
                    }
                }
                return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "Unauthorised access." });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetExhibitionShipment action: {ex.InnerException}");
                return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "Something went wrong. Please try again later. " });
            }
        }

        [HttpGet("GetAgencyList")]
        public List<DropdownDataDTO> GetAgencyListByExhibitorId(long websiteId, long memberid)
        {
            websiteId = 2;
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            long SIBFMemberId = User.Identity.GetID();
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                ExhibitionService ExhibitionService = new ExhibitionService();
                List<DropdownDataDTO> AgencyList = new List<DropdownDataDTO>();
                var Exhibition = ExhibitionService.GetExhibition(new XsiExhibition() { WebsiteId = websiteId, IsActive = "Y", IsArchive = "N" }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                if (LangId == 1)
                {
                    if (Exhibition != null)
                    {
                        AgencyList = AgencyRegistrationService.GetAgencyRegistration(new XsiExhibitionMemberApplicationYearly() { MemberId = memberid, ExhibitionId = Exhibition.ExhibitionId, MemberRoleId = 2 }, EnumSortlistBy.ByItemIdDesc)
                           .Where(i => (i.Status == EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved) || i.Status == EnumConversion.ToString(EnumAgencyStatus.SIBFApproved)))
                       .Select(i => new DropdownDataDTO() { ItemId = i.MemberExhibitionYearlyId, Title = i.PublisherNameEn })
                       .ToList();
                    }
                }
                else
                {
                    if (Exhibition != null)
                    {
                        AgencyList = AgencyRegistrationService.GetAgencyRegistration(new XsiExhibitionMemberApplicationYearly() { MemberId = memberid, ExhibitionId = Exhibition.ExhibitionId, MemberRoleId = 2 }, EnumSortlistBy.ByItemIdDesc)
                           .Where(i => (i.Status == EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved) || i.Status == EnumConversion.ToString(EnumAgencyStatus.SIBFApproved)))
                       .Select(i => new DropdownDataDTO() { ItemId = i.MemberExhibitionYearlyId, Title = i.PublisherNameAr })
                       .ToList();
                    }
                }
                return AgencyList;
            }
        }

        [HttpGet("ShipmentType")]
        public ActionResult<dynamic> GetExhibitionShipmentType()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                List<DropdownDataDTO> LocationTypeList = new List<DropdownDataDTO>();
                if (LangId == 1)
                {
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = -1, Title = "-Select Shipment Method-" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 1, Title = "Air" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 2, Title = "Sea" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 3, Title = "Land" });
                }
                else
                {
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = -1, Title = "-طريقة الشحن-" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 1, Title = "جوي" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 2, Title = "بحري" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 3, Title = "بري" });
                }
                return Ok(LocationTypeList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetExhibitionShipment action: {ex.InnerException}");
                return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "Something went wrong. Please try again later." });
            }
        }

        [HttpGet("ShipmentLocationTypeById/{shipmenttype}")]
        public ActionResult<dynamic> GetExhibitionShipmentLocationTypeByTypeId(long shipmenttype)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                List<DropdownDataDTO> LocationTypeList = new List<DropdownDataDTO>();
                if (LangId == 1)
                {

                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = -1, Title = "-Select Shipment Location Type-" });
                    if (shipmenttype == 1)
                    {
                        LocationTypeList.Add(new DropdownDataDTO() { ItemId = 1, Title = "Sharjah International Airport (Air)" });
                        LocationTypeList.Add(new DropdownDataDTO() { ItemId = 2, Title = "Dubai International Airport(Air)" });
                    }
                    else if (shipmenttype == 2)
                    {
                        LocationTypeList.Add(new DropdownDataDTO() { ItemId = 3, Title = "Khalid Port  (Sea)" });
                        LocationTypeList.Add(new DropdownDataDTO() { ItemId = 4, Title = "Port Rashid  (Sea)" });
                    }
                    else if (shipmenttype == 3)
                    {
                        LocationTypeList.Add(new DropdownDataDTO() { ItemId = 5, Title = "Al Ghuwaifat Port Station (Land)" });
                        LocationTypeList.Add(new DropdownDataDTO() { ItemId = 6, Title = "Al Wajajah Port (Land)" });
                    }
                }
                else
                {
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = -1, Title = "-Select Shipment Location Type-" });
                    if (shipmenttype == 1)
                    {
                        LocationTypeList.Add(new DropdownDataDTO() { ItemId = 1, Title = "مطار الشارقة الدولي (جوي)" });
                        LocationTypeList.Add(new DropdownDataDTO() { ItemId = 2, Title = "مطار دبي الدولي (جوي)" });
                    }
                    else if (shipmenttype == 2)
                    {
                        LocationTypeList.Add(new DropdownDataDTO() { ItemId = 3, Title = "ميناء خالد الشارقة (بحري)" });
                        LocationTypeList.Add(new DropdownDataDTO() { ItemId = 4, Title = "ميناء راشد دبي (بحري)" });
                    }
                    else if (shipmenttype == 3)
                    {
                        LocationTypeList.Add(new DropdownDataDTO() { ItemId = 5, Title = "المنافذ البرية – الغويفات الحدودي (بري)" });
                        LocationTypeList.Add(new DropdownDataDTO() { ItemId = 6, Title = "المنافذ البرية – الوجاجة الحدودي (بري)" });
                    }
                }
                return Ok(LocationTypeList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetExhibitionShipment action: {ex.InnerException}");
                return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "Something went wrong. Please try again later." });
            }
        }

        [HttpGet("ShipmentLocationType")]
        public ActionResult<dynamic> GetExhibitionShipmentLocationType()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                List<DropdownDataDTO> LocationTypeList = new List<DropdownDataDTO>();
                if (LangId == 1)
                {
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = -1, Title = "-Select-" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 1, Title = "Sharjah International Airport (Air)" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 2, Title = "Dubai International Airport(Air)" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 3, Title = "Khalid Port  (Sea)" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 4, Title = "Port Rashid  (Sea)" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 5, Title = "Al Ghuwaifat Port Station (Land)" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 6, Title = "Al Wajajah Port (Land)" });
                }
                else
                {
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = -1, Title = "-اختر-" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 1, Title = "مطار الشارقة الدولي (جوي)" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 2, Title = "مطار دبي الدولي (جوي)" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 3, Title = "ميناء خالد الشارقة (بحري)" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 4, Title = "ميناء راشد دبي (بحري)" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 5, Title = "المنافذ البرية – الغويفات الحدودي (بري)" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 6, Title = "المنافذ البرية – الوجاجة الحدودي (بري)" });
                }
                return Ok(LocationTypeList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetExhibitionShipment action: {ex.InnerException}");
                return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "Something went wrong. Please try again later." });
            }
        }

        [HttpGet]
        [Route("UserInfo/{exhibitorid}")]
        public ActionResult<dynamic> GetExhibitionShipment(long exhibitorid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long SIBFMemberId = User.Identity.GetID();
                List<DropdownDataDTO> AgencyList = new List<DropdownDataDTO>();

                using (sibfnewdbContext db = new sibfnewdbContext())
                {
                    var MemberExhibitionYearly = db.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == exhibitorid).FirstOrDefault();

                    if (MemberExhibitionYearly != null)
                    {
                        var AgencyEntity = db.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberId == SIBFMemberId && i.ExhibitionId == MemberExhibitionYearly.ExhibitionId && i.MemberRoleId == 2 && (i.Status == "E" || i.Status == "A")).FirstOrDefault();

                        if (MemberExhibitionYearly.MemberRoleId == 2)
                            exhibitorid = MemberExhibitionYearly.ParentId ?? -1;
                        else
                            exhibitorid = MemberExhibitionYearly.MemberExhibitionYearlyId;

                        UserInfoItemDTO dto = new UserInfoItemDTO();
                        dto.ExhibitorId = exhibitorid;

                        dto.ExhibitorName = MemberExhibitionYearly.PublisherNameEn;
                        dto.FileNumber = MemberExhibitionYearly.FileNumber;

                        if (AgencyEntity != null)
                        {
                            dto.AgencyId = AgencyEntity.MemberExhibitionYearlyId;
                            dto.AgencyName = AgencyEntity.PublisherNameEn;
                            dto.IsAgency = EnumConversion.ToString(EnumBool.Yes);

                            AgencyList = db.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == AgencyEntity.MemberExhibitionYearlyId).Select(i => new DropdownDataDTO() { ItemId = i.MemberExhibitionYearlyId, Title = i.PublisherNameEn }).ToList();
                            dto.CountryName = MethodFactory.GetCountryName(AgencyEntity.CountryId ?? -1, LangId);
                        }
                        else
                        {
                            dto.IsAgency = EnumConversion.ToString(EnumBool.No);
                            AgencyList = db.XsiExhibitionMemberApplicationYearly.Where(i => i.ParentId == exhibitorid && (i.Status == "E" || i.Status == "A"))
                           .Select(i => new DropdownDataDTO() { ItemId = i.MemberExhibitionYearlyId, Title = i.PublisherNameEn }).ToList();
                            dto.CountryName = MethodFactory.GetCountryName(MemberExhibitionYearly.CountryId ?? -1, LangId);
                        }

                        if (AgencyList.Count > 0)
                            dto.AgencyList = AgencyList;
                        else
                            dto.AgencyList = new List<DropdownDataDTO>();

                        return Ok(new { MessageTypeResponse = "Success", dto });
                    }
                    return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "No data found." });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Exhibition Shipment UserInfo action: {ex.InnerException}");
                return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "Something went wrong. Please try again later. " });
            }
        }

        [HttpGet("ExhibitionYears/{websiteid}")]
        public async Task<ActionResult<IEnumerable<ExhibitionYearDTO>>> GetExhibitionYears(long websiteid = 1)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibition>();
                predicate = predicate.And(i => i.WebsiteId == websiteid && i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                var years = _context.XsiExhibition.AsQueryable().Where(predicate).OrderBy(i => i.ExhibitionYear).Select(i => new ExhibitionYearDTO { ExhibitionId = i.ExhibitionId, ExhibitionYear = i.ExhibitionYear.Value.ToString() })
                    .OrderByDescending(i => i.ExhibitionYear)
                    .ToListAsync();

                return await years;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibition>();
                predicate = predicate.And(i => i.WebsiteId == websiteid && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                var years = _context.XsiExhibition.AsQueryable().Where(predicate).OrderBy(i => i.ExhibitionYear).Select(i => new ExhibitionYearDTO { ExhibitionId = i.ExhibitionId, ExhibitionYear = i.ExhibitionYear.Value.ToString() })
                    .OrderByDescending(i => i.ExhibitionYear)
                    .ToListAsync();

                return await years;
            }
        }

        #endregion

        [HttpPost]
        [Route("Registration")]
        public ActionResult<dynamic> ExhibitionShipmentRegistration(ShipmentSaveDataDTO dto)
        {
            MessageDTO messageDTO = new MessageDTO();
            try
            {
                messageDTO.MessageTypeResponse = "Error";
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long HWebsiteId = 1;
                long.TryParse(Request.Headers["Websiteid"], out HWebsiteId);

                long websiteId = -1;
                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {

                    var exhibitor = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == dto.ExhibitorId).FirstOrDefault();
                    XsiExhibitionShipment shipmententity = new XsiExhibitionShipment();
                    shipmententity.LanguageUrl = LangId;

                    shipmententity.MemberId = memberId;
                    shipmententity.ExhibitorId = dto.ExhibitorId;
                    if (exhibitor != null)
                    {
                        var exhibition = _context.XsiExhibition.Where(i => i.ExhibitionId == exhibitor.ExhibitionId).FirstOrDefault();
                        websiteId = exhibition.WebsiteId ?? -1;
                        shipmententity.ExhibitionId = exhibitor.ExhibitionId;
                        if (dto.IsAgency == EnumConversion.ToString(EnumBool.Yes))
                        {
                            var agency = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.ExhibitionId == exhibitor.ExhibitionId && i.MemberId == memberId && (i.Status == "E" || i.Status == "A")).FirstOrDefault();
                            if (agency != null)
                            {
                                shipmententity.AgencyId = agency.MemberExhibitionYearlyId;
                            }
                        }

                        if (!string.IsNullOrEmpty(dto.IsAgency))
                            shipmententity.IsAgency = dto.IsAgency;
                        else
                            shipmententity.IsAgency = EnumConversion.ToString(EnumBool.No);

                        shipmententity.ShipmentCount = dto.ShipmentCount;
                        shipmententity.ShipmentType = dto.ShipmentType;
                        shipmententity.ShipmentCompanyName = dto.ShipmentCompanyName;
                        shipmententity.BillofLading = dto.BillofLading;
                        shipmententity.ShipmentNumber = GenerateShipmentNumber(exhibition.ExhibitionId, dto.ExhibitorId, shipmententity.AgencyId, dto.IsAgency); //dto.ShipmentNumber;
                        shipmententity.ShipmentCountryFrom = dto.ShipmentCountryFrom;
                        shipmententity.ShipmentCountryTo = dto.ShipmentCountryTo;
                        shipmententity.ReceivingCompanyName = dto.ReceivingCompanyName;

                        shipmententity.NoofBooks = dto.NoofBooks;
                        if (dto.CategoryId != null)
                            shipmententity.CategoryId = dto.CategoryId;
                        shipmententity.Status = "N";

                        if (dto.FileBase64BookFile != null && dto.FileBase64BookFile.Length > 0)
                        {
                            string fileName = string.Empty;
                            byte[] imageBytes;
                            if (dto.FileBase64BookFile.Contains("data:"))
                            {
                                var strInfo = dto.FileBase64BookFile.Split(",")[0];
                                imageBytes = Convert.FromBase64String(dto.FileBase64BookFile.Split(',')[1]);
                            }
                            else
                            {
                                imageBytes = Convert.FromBase64String(dto.FileBase64BookFile);
                            }

                            fileName = string.Format("{0}_{1}{2}", LangId, MethodFactory.GetRandomNumber(), "." + dto.FileExtensionofBookFile);
                            System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitionShipment\\") + fileName, imageBytes);
                            shipmententity.BooksFile = fileName;
                        }

                        if (dto.IsLocalWarehouse == EnumConversion.ToString(EnumBool.Yes))
                        {
                            shipmententity.WshipmentCount = dto.WShipmentCount;
                            shipmententity.WshipmentCompanyName = dto.WShipmentCompanyName;
                            shipmententity.WshipmentNumber = dto.WShipmentNumber;
                            shipmententity.WnoofBooks = dto.WNoofBooks;

                            if (dto.WFileBase64BookFile != null && dto.WFileBase64BookFile.Length > 0)
                            {
                                string fileName = string.Empty;
                                byte[] imageBytes;
                                if (dto.WFileBase64BookFile.Contains("data:"))
                                {
                                    var strInfo = dto.WFileBase64BookFile.Split(",")[0];
                                    imageBytes = Convert.FromBase64String(dto.WFileBase64BookFile.Split(',')[1]);
                                }
                                else
                                {
                                    imageBytes = Convert.FromBase64String(dto.WFileBase64BookFile);
                                }

                                fileName = string.Format("{0}_{1}{2}", LangId, MethodFactory.GetRandomNumber(), "." + dto.WFileExtensionofBookFile);
                                System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitionShipment\\WBookFile\\") + fileName, imageBytes);
                                shipmententity.WbooksFile = fileName;
                            }
                        }

                        shipmententity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        shipmententity.CreatedOn = MethodFactory.ArabianTimeNow();
                        shipmententity.CreatedBy = -1;
                        shipmententity.ModifiedOn = MethodFactory.ArabianTimeNow();
                        shipmententity.ModifiedBy = -1;

                        shipmententity.ReceivedShipmentLocationType = dto.ReceivedShipmentLocationType;

                        if (!string.IsNullOrEmpty(dto.IsLocalWarehouse))
                            shipmententity.IsLocalWarehouse = dto.IsLocalWarehouse;
                        else
                            shipmententity.IsLocalWarehouse = EnumConversion.ToString(EnumBool.No);

                        _context.Add(shipmententity);
                        _context.SaveChanges();
                        long shipmentid = shipmententity.ItemId;

                        if (shipmentid > 0 && dto.AgencyIds != null && dto.AgencyIds.Count > 0)
                        {
                            foreach (var agencyid in dto.AgencyIds)
                            {
                                if (agencyid > 0)
                                {
                                    XsiExhibitionShipmentAgency shipmentagency = new XsiExhibitionShipmentAgency();
                                    shipmentagency.AgencyId = agencyid;
                                    shipmentagency.ExhibitionShipmentId = shipmentid;
                                    _context.Add(shipmentagency);
                                    _context.SaveChanges();
                                }
                            }
                        }

                        shipmententity.QrfileName = GetColorQRCodeForShipment(shipmentid.ToString(), LangId, HWebsiteId);
                        _context.SaveChanges();

                        if (!string.IsNullOrEmpty(shipmententity.QrfileName))
                        {
                            string strpathtemp = Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitionShipment\\temp") + shipmententity.QrfileName;
                            string strpathtemppink = Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitionShipment\\Red\\temp") + shipmententity.QrfileName;
                            SendEmail(shipmententity.QrfileName, shipmententity.ExhibitorId ?? -1, websiteId, 1);//dto.WebsiteId.Value

                            //if (System.IO.File.Exists(strpathtemp))
                            //    System.IO.File.Delete(strpathtemp);

                            //if (System.IO.File.Exists(strpathtemppink))
                            //    System.IO.File.Delete(strpathtemppink);

                            if (LangId == 1)
                                messageDTO.Message = "Shipment submitted successfully. QR Code has been sent to your email.";
                            else
                                messageDTO.Message = "تم تقديم الشحنة بنجاح، تم إرسال رمز الاستجابة السريع الى بريدك الالكتروني";

                            messageDTO.MessageTypeResponse = "Success";
                            return Ok(messageDTO);
                        }
                        else
                        {
                            messageDTO.Message = "Shipment submitted successfully. QR Code couldn't be generated.";
                            messageDTO.MessageTypeResponse = "Success";
                            return Ok(messageDTO);
                        }
                    }
                    else
                    {
                        messageDTO.Message = "Member need to be register for exibtior.";
                        return Ok(messageDTO);
                    }
                }
                messageDTO.Message = "Unauthorised access. Try login again.";
                return Ok(messageDTO);
            }
            catch (Exception ex)
            {
                messageDTO.Message = "Something went wrong. Please try again later.";
                return Ok(messageDTO);
            }
        }

        #region Methods
        private string GetBookCategoryName(long? categoryId, long langId)
        {
            if (categoryId != null)
            {
                using (ExhibitionBookSubjectService ExhibitionBookSubjectService = new ExhibitionBookSubjectService())
                {
                    var subject = ExhibitionBookSubjectService.GetExhibitionBookSubjectByItemId(categoryId.Value);
                    if (subject != null)
                    {
                        if (langId == 1)
                        {
                            return subject.Title;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(subject.TitleAr))
                            {
                                return subject.TitleAr;
                            }
                            else
                            {
                                return subject.Title;
                            }
                        }
                    }
                }
            }
            return string.Empty;
        }
        private string GenerateShipmentNumber(long exhibitionId, long? exhibitorId, long? agencyId, string isagency)
        {
            string MaxShipmentNumber = string.Empty;
            using (ExhibitionShipmentService ExhibitionShipmentService = new ExhibitionShipmentService())
            {
                if (isagency == "Y")
                {
                    if (agencyId != null)
                    {
                        MaxShipmentNumber = ExhibitionShipmentService.GetExhibitionShipment(new XsiExhibitionShipment() { ExhibitionId = exhibitionId, ExhibitorId = exhibitorId, IsActive = EnumConversion.ToString(EnumBool.Yes) }).Where(i => i.IsAgency == "Y").Max(i => i.ShipmentNumber);
                    }
                }
                else
                {
                    MaxShipmentNumber = ExhibitionShipmentService.GetExhibitionShipment(new XsiExhibitionShipment() { ExhibitionId = exhibitionId, ExhibitorId = exhibitorId, IsActive = EnumConversion.ToString(EnumBool.Yes) }).Where(i => i.IsAgency != "Y").Max(i => i.ShipmentNumber);
                }
            }

            if (string.IsNullOrEmpty(MaxShipmentNumber))
                return "1";

            long num = Convert.ToInt64(MaxShipmentNumber) + 1;
            return num.ToString();
        }
        private List<DropdownDataDTO> GetAgencyListByShipmentId(long shipmentid, long exhibitorId, int langId)
        {
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                ExhibitionShipmentAgencyService ExhibitionShipmentAgencyService = new ExhibitionShipmentAgencyService();
                var shipmentagencyids = ExhibitionShipmentAgencyService.GetExhibitionShipmentAgency(new XsiExhibitionShipmentAgency() { ExhibitionShipmentId = shipmentid }).Select(i => i.AgencyId).ToList();
                List<DropdownDataDTO> AgencyList = new List<DropdownDataDTO>();
                if (langId == 1)
                {
                    AgencyList = AgencyRegistrationService.GetAgencyRegistration(new XsiExhibitionMemberApplicationYearly() { ParentId = exhibitorId }, EnumSortlistBy.ByAlphabetAsc)
                        .Where(i => shipmentagencyids.Contains(i.MemberExhibitionYearlyId) && (i.Status == EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved) || i.Status == EnumConversion.ToString(EnumAgencyStatus.SIBFApproved)))
                   .Select(i => new DropdownDataDTO() { ItemId = i.MemberExhibitionYearlyId, Title = i.PublisherNameEn })
                   .ToList();
                }
                else
                {
                    AgencyList = AgencyRegistrationService.GetAgencyRegistration(new XsiExhibitionMemberApplicationYearly() { ParentId = exhibitorId }, EnumSortlistBy.ByAlphabetAsc)
                            .Where(i => shipmentagencyids.Contains(i.MemberExhibitionYearlyId) && (i.Status == EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved) || i.Status == EnumConversion.ToString(EnumAgencyStatus.SIBFApproved)))
                   .Select(i => new DropdownDataDTO() { ItemId = i.MemberExhibitionYearlyId, Title = i.PublisherNameAr })
                   .ToList();
                }
                return AgencyList;
            }
        }

        private string GetColorQRCodeForShipment(string shipmentid, long langId, long webid = 1)
        {
            try
            {
                string shipmenturl = string.Empty;
                string fileName = string.Format("{0}_{1}_{2}{3}", shipmentid, langId, MethodFactory.GetRandomNumber(), "." + "png");
                //string shipmenturl = "https://www.sibf.com/en/shipmentdetails?itemId=1";

                if (webid == 2)
                    shipmenturl = _appCustomSettings.ServerAddressSCRF + "en/shipmentdetails?itemId=" + shipmentid;
                else
                    shipmenturl = _appCustomSettings.ServerAddressNew + "en/shipmentdetails?itemId=" + shipmentid;

                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(shipmenturl, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);

                Color myColorGreen = Color.FromArgb(104, 254, 154);
                Color myColorRed = Color.FromArgb(254, 0, 102);


                Bitmap qrCodeImage = qrCode.GetGraphic(9, Color.Black, Color.White, true);
                for (int x = 0; x < qrCodeImage.Width; x++)
                {
                    for (int y = 0; y < qrCodeImage.Height; y++)
                    {
                        var pixelColor = qrCodeImage.GetPixel(x, y);
                        if (pixelColor.R == 0 && pixelColor.G == 0 && pixelColor.B == 0)
                        {
                            qrCodeImage.SetPixel(x, y, myColorGreen);
                        }
                    }
                }
                string strpathqr = Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitionShipment\\temp") + fileName;
                string strpathqrtxt = Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitionShipment\\") + fileName;
                var bitmapBytes = BitmapToBytes(qrCodeImage); //Convert bitmap into a byte array
                System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitionShipment\\temp") + fileName, bitmapBytes);
                GetTextOnQR(strpathqr, strpathqrtxt, "Stand");

                string strpathqrpink = Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitionShipment\\Red\\temp") + fileName;
                string strpathqrpinktxt = Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitionShipment\\Red\\") + fileName;
                qrCodeImage = qrCode.GetGraphic(9, Color.Black, Color.White, true);
                for (int x = 0; x < qrCodeImage.Width; x++)
                {
                    for (int y = 0; y < qrCodeImage.Height; y++)
                    {
                        var pixelColor = qrCodeImage.GetPixel(x, y);
                        if (pixelColor.R == 0 && pixelColor.G == 0 && pixelColor.B == 0)
                        {
                            qrCodeImage.SetPixel(x, y, myColorRed);
                        }
                    }
                }
                bitmapBytes = BitmapToBytes(qrCodeImage); //Convert bitmap into a byte array
                System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitionShipment\\Red\\temp") + fileName, bitmapBytes);
                GetTextOnQR(strpathqrpink, strpathqrpinktxt, "Store");
                return fileName;
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetQRCodeForShipment action: {ex.InnerException}");
                return string.Empty;
            }
            // return File(bitmapBytes, "image/jpeg"); //Return as file result
        }

        private void GetTextOnQR(string strsourcepath, string destination, string strtext)
        {
            PointF firstLocation = new PointF(165f, 6f);
            string PathSourceImg = strsourcepath;//@"img\Candicate.jpg";
            string PathDistanceImg = destination;// @"img\Candicate_dist.jpg";

            Bitmap bitmap = (Bitmap)Image.FromFile(PathSourceImg);  //load the image file

            // Create a blank bitmap with the same dimensions
            Bitmap temp = new Bitmap(bitmap.Width, bitmap.Height);

            using (Graphics graphics = Graphics.FromImage(temp))
            {
                // draw a original bitmap onto the graphices of the new bitmap
                graphics.DrawImage(bitmap, 0, 0);

                // draw string to temp bitmap, so you can write string to bitmap
                // using (Font arialFont = new Font("Verdana", 12, FontStyle.Bold))
                using (Font arialFont = new Font("Verdana", 18, FontStyle.Bold))
                {
                    graphics.DrawString(strtext, arialFont, Brushes.Blue, firstLocation);
                }
            }
            temp.Save(PathDistanceImg);//save the image file

        }

        private static byte[] BitmapToBytes(Bitmap img)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }
        private string GetUserStatus(string status, long langid = 1)
        {
            switch (status)
            {
                case "N": return langid == 1 ? "New" : "New";
                case "P": return langid == 1 ? "Pending Documentation" : "Pending Documentation";
                case "A": return langid == 1 ? "Approved" : "Approved";
                case "R": return langid == 1 ? "Rejected" : "Rejected";
                default: return langid == 1 ? "New" : "New";
            }
        }
        void SendEmail(string qrfilename, long exhibitorId, long websiteId, long langId)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                List<string> MailAttachments = new List<string>();
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                XsiExhibitionMember member = new XsiExhibitionMember();
                var exhibitor = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == exhibitorId).First();
                if (exhibitor != null)
                {
                    member = ExhibitionMemberService.GetExhibitionMemberByItemId(exhibitor.MemberId.Value);
                    if (member != default(XsiExhibitionMember))
                    {
                        using (EmailContentService EmailContentService = new EmailContentService())
                        {
                            string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";

                            #region Send Thankyou Email
                            StringBuilder body = new StringBuilder();
                            #region Email Content
                            SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                            if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                            {
                                var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(20120);
                                if (scrfEmailContent != null)
                                {
                                    if (scrfEmailContent.Body != null)
                                        strEmailContentBody = scrfEmailContent.Body;
                                    if (scrfEmailContent.Email != null)
                                        strEmailContentEmail = scrfEmailContent.Email;
                                    if (scrfEmailContent.Subject != null)
                                        strEmailContentSubject = scrfEmailContent.Subject;
                                    strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                }
                            }
                            else
                            {
                                var emailContent = EmailContentService.GetEmailContentByItemId(20142);
                                if (emailContent != null)
                                {
                                    if (emailContent.Body != null)
                                        strEmailContentBody = emailContent.Body;
                                    if (emailContent.Email != null)
                                        strEmailContentEmail = emailContent.Email;
                                    if (emailContent.Subject != null)
                                        strEmailContentSubject = emailContent.Subject;
                                    strEmailContentAdmin = _appCustomSettings.AdminName;
                                }
                            }
                            strEmailContentBody = strEmailContentBody.Replace("$$qrurl$$", _appCustomSettings.UploadsCMSPath + "/ExhibitionShipment/" + qrfilename);
                            strEmailContentBody = strEmailContentBody.Replace("$$qrurlpink$$", _appCustomSettings.UploadsCMSPath + "/ExhibitionShipment/Red/" + qrfilename);
                            // strEmailContentBody = strEmailContentBody.Replace("$$qrurltwo$$", _appCustomSettings.UploadsCMSPath + "/ExhibitionShipment/Red/" + qrfilename);

                            string strFilePath = _appCustomSettings.UploadsPhysicalPath + "\\ExhibitionShipment\\" + qrfilename;
                            string strFilePathRed = _appCustomSettings.UploadsPhysicalPath + "\\ExhibitionShipment\\Red\\" + qrfilename;
                            System.IO.FileInfo targetfile = new System.IO.FileInfo(strFilePath);
                            if (targetfile.Exists)
                                MailAttachments.Add(string.Format("{0}", strFilePath));

                            System.IO.FileInfo targetfileRed = new System.IO.FileInfo(strFilePathRed);
                            if (targetfileRed.Exists)
                                MailAttachments.Add(string.Format("{0}", strFilePathRed));

                            #endregion
                            body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, member, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));

                            if (!string.IsNullOrEmpty(exhibitor.Email) && exhibitor.Email != member.Email)
                                _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, member.Email + "," + exhibitor.Email, strEmailContentSubject, body.ToString(), MailAttachments);
                            else
                                _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, member.Email, strEmailContentSubject, body.ToString(), MailAttachments);
                            #endregion
                        }
                    }
                }
            }
        }
        #endregion
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using SIBFAPIEn.Utility;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Account.Manage.Internal;
using LinqKit;
using System.IO;
using Contracts;
using Xsi.ServicesLayer;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class RepresentativeMobileController : ControllerBase
    {

        private readonly AppCustomSettings _appCustomSettings;
        EnumResultType XsiResult { get; set; }
        #region Properties
        readonly string VisaPath = "Representative\\Visa\\";
        readonly string VisaFormPath = "Representative\\VisaForm\\";
        private ILoggerManager _logger;

        #endregion
        #region Status
        string strApproved = EnumConversion.ToString(EnumRepresentativeStatus.Approved);
        string strPending = EnumConversion.ToString(EnumRepresentativeStatus.Pending);
        //string strFlightDetailsPending = EnumConversion.ToString(EnumRepresentativeStatus.FlightDetailsPending);
        string strPendingDocumentation = EnumConversion.ToString(EnumRepresentativeStatus.PendingDocumentation);
        string strVisaProcessing = EnumConversion.ToString(EnumRepresentativeStatus.VisaProcessing);
        string strVisaUploaded = EnumConversion.ToString(EnumRepresentativeStatus.VisaUploaded);
        string strReIssueVisa = EnumConversion.ToString(EnumRepresentativeStatus.ReIssueVisa);
        string strDocumentsApproved = EnumConversion.ToString(EnumRepresentativeStatus.DocumentsApproved);
        string strDocumentsUploaded = EnumConversion.ToString(EnumRepresentativeStatus.DocumentsUploaded);
        //string strRejected = EnumConversion.ToString(EnumRepresentativeStatus.Rejected);
        //string strCancelled = EnumConversion.ToString(EnumRepresentativeStatus.Cancelled);
        //string strBlocked = EnumConversion.ToString(EnumRepresentativeStatus.Blocked);
        //string strYes = EnumConversion.ToString(EnumBool.Yes);
        #endregion 
        public RepresentativeMobileController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
        }
        //[Route("{websiteid}/{memberroleid}")]
        //public async Task<ActionResult<dynamic>> GetPayment(long websiteid, long memberroleid)
        // GET: api/GetPayment
        [HttpGet]
        [Route("{memberid}")]
        public ActionResult<dynamic> RepresentativeList(long memberid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long webId = 1;
                long.TryParse(Request.Headers["WebsiteId"], out webId);
                //long memberId = User.Identity.GetID();
                long exhibitorId = 0;
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    var exhibition = MethodFactory.GetActiveExhibition(webId, _context);
                    List<RepresentativeMobileDTO> representativeMobileDTOs = new List<RepresentativeMobileDTO>();
                    if (exhibition != null)
                    {
                        //model.ExhibitionId = exhibition.ExhibitionId;
                        var exhibitor = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberRoleId == 1 && i.ParentId == null && i.MemberId == memberid && i.ExhibitionId == exhibition.ExhibitionId).FirstOrDefault();
                        if (exhibitor != null)
                        {
                            exhibitorId = exhibitor.MemberExhibitionYearlyId;
                            using (ExhibitionRepresentativeParticipatingService RepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService())
                            {
                                ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService();
                                var where = new XsiExhibitionRepresentativeParticipating();
                                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                where.MemberExhibitionYearlyId = exhibitorId;

                                //var whereRep = new XsiExhibitionRepresentative();
                                //whereRep.MemberId = memberid;
                                //whereRep.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                //var representativeIds = ExhibitionRepresentativeService.GetExhibitionRepresentative(whereRep).Select(s => s.RepresentativeId).ToList();representativeIds.Contains(p.RepresentativeId) && 

                                //representativeMobileDTOs = RepresentativeParticipatingService.GetExhibitionRepresentativeParticipating(where).Where(p =>
                                //  (p.Status == strApproved || p.Status == strVisaUploaded || p.Status == strPending || p.Status == strPendingDocumentation || p.Status == strVisaProcessing || p.Status == strDocumentsApproved)).OrderBy(o => o.NameEn)
                                //       .Select(i => new RepresentativeMobileDTO()
                                //       {
                                //           RepresentativeId = i.RepresentativeId,
                                //           ExhibitorId = i.MemberExhibitionYearlyId,
                                //           Name = GetRepresentativeName(i.RepresentativeId, i.MemberExhibitionYearlyId, LangId),
                                //           //Nationality = GetCountryTitle(i.CountryId, LangId),
                                //           //Status = MethodFactory.GetReprepresentativeStatus(i.Status, LangId),
                                //           Status = MethodFactory.GetReprepresentativeStatus(i.Status, LangId) == strApproved ? "1" : "0",
                                //           Date = i.DateOfIssue.HasValue ? GetDateFormated(i.DateOfIssue.Value, LangId) : null,
                                //           //Data = Tuple.Create(GetRepresentativeName(i.RepresentativeId, i.MemberExhibitionYearlyId, LangId),
                                //           // MethodFactory.GetReprepresentativeStatus(i.Status, LangId), i.Passport),

                                //           Passport = i.Passport
                                //       }).OrderByDescending(o => o.RepresentativeId).ToList();

                                //var data = new RepresentativeDataMobileDTO();

                                var representativeData = RepresentativeParticipatingService.GetExhibitionRepresentativeParticipating(where).Where(p =>
                                       (p.Status == strApproved || p.Status == strVisaUploaded || p.Status == strPending || p.Status == strPendingDocumentation || p.Status == strVisaProcessing || p.Status == strDocumentsApproved)).OrderBy(o => o.NameEn)
                                           .Select(i => new
                                           {
                                               RepresentativeId = i.RepresentativeId,
                                               ExhibitorId = i.MemberExhibitionYearlyId,
                                               Name = GetRepresentativeName(i.RepresentativeId, i.MemberExhibitionYearlyId, LangId),
                                               //Nationality = GetCountryTitle(i.CountryId, LangId),
                                               Status = MethodFactory.GetReprepresentativeStatus(i.Status, LangId),
                                               StatusType = MethodFactory.GetReprepresentativeStatus(i.Status, LangId) == strApproved ? 1 : 0,
                                               Date = i.DateOfIssue.HasValue ? GetDateFormated(i.DateOfIssue.Value, LangId) : null,
                                               //Data = Tuple.Create(GetRepresentativeName(i.RepresentativeId, i.MemberExhibitionYearlyId, LangId),
                                               // MethodFactory.GetReprepresentativeStatus(i.Status, LangId), i.Passport),
                                               
                                               Passport = i.Passport
                                           }).OrderByDescending(o => o.RepresentativeId).ToList();
                                foreach (var item in representativeData)
                                {
                                    if (LangId == 1)
                                    {
                                        var representativeMobileDTO = new RepresentativeMobileDTO();

                                        representativeMobileDTO.RepresentativeId = item.RepresentativeId;
                                        representativeMobileDTO.Date = item.Date;
                                        representativeMobileDTO.ExhibitorId = item.ExhibitorId;
                                        representativeMobileDTO.Status = item.StatusType;
                                        representativeMobileDTO.Data = new RepresentativeDataMobileDTO();
                                        representativeMobileDTO.Data.Name = item.Name;
                                        representativeMobileDTO.Data.Status = item.Status;
                                        representativeMobileDTO.Data.Passport = item.Passport;
                                        representativeMobileDTOs.Add(representativeMobileDTO);
                                    }
                                    else
                                    {
                                        var representativeMobileDTO = new RepresentativeMobileDTO();

                                        representativeMobileDTO.RepresentativeId = item.RepresentativeId;
                                        representativeMobileDTO.Date = item.Date;
                                        representativeMobileDTO.ExhibitorId = item.ExhibitorId;
                                        representativeMobileDTO.Status = item.StatusType;
                                        representativeMobileDTO.Data = new RepresentativeDataMobileArDTO();
                                        representativeMobileDTO.Data.Name = item.Name;
                                        representativeMobileDTO.Data.Status = item.Status;
                                        representativeMobileDTO.Data.Passport = item.Passport;
                                        representativeMobileDTOs.Add(representativeMobileDTO);
                                    }
                                }
                            }
                            return representativeMobileDTOs;
                        }
                        return Ok("Unauthorized access");
                    }
                }
                return Ok("Something went wrong. Please try again later.");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside RepresentativeList action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet("Representative/{representativeId}/{exhibitorId}")]
        public ActionResult<dynamic> GetRepresentative(long representativeId, long exhibitorId)
        {
            try
            {
                var RepresentativeDetailsMobileDTO = new RepresentativeDetailsMobileDTO();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                RepresentativeDetailsMobileDTO = LoadContent(representativeId, exhibitorId, LangId);

                return Ok(RepresentativeDetailsMobileDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetRepresentative action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
        #region Private Methods 

        RepresentativeDetailsMobileDTO LoadContent(long representativeId, long exhibitorid, long langid)
        {
            //mvRepresentativeForm.SetActiveView(vwRepForm); 
            RepresentativeDetailsMobileDTO RepresentativeDTO = new RepresentativeDetailsMobileDTO();
            using (ExhibitionRepresentativeParticipatingService representativeParticipatingService = new ExhibitionRepresentativeParticipatingService())
            {
                XsiExhibitionRepresentativeParticipating entity = representativeParticipatingService.GetExhibitionRepresentativeParticipating(new XsiExhibitionRepresentativeParticipating() { RepresentativeId = representativeId, MemberExhibitionYearlyId = exhibitorid }).FirstOrDefault();

                if (entity != null)
                {
                    ExhibitionRepresentativeService representativeService = new ExhibitionRepresentativeService();
                    XsiExhibitionRepresentative entityRep = representativeService.GetRepresentativeByItemId(representativeId);


                    RepresentativeDTO.ItemId = entity.RepresentativeId;
                    RepresentativeDTO.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;
                    //RepresentativeDTO.WebsiteId = entity.exhibition;
                    RepresentativeDTO.MemberId = entityRep.MemberId.Value;

                    if (entity.NameEn != null)
                        RepresentativeDTO.NameEnglish = entity.NameEn;
                    if (entity.NameAr != null)
                        RepresentativeDTO.NameArabic = entity.NameAr;
                    if (entity.Profession != null)
                        RepresentativeDTO.Profession = entity.Profession;

                    if (entity.CountryId != null)
                        RepresentativeDTO.Nationality = GetCountryTitle(entity.CountryId, langid);


                    if (entityRep != null)
                    {
                        if (entityRep.Dob != null)
                            RepresentativeDTO.DateOfBirth = entityRep.Dob.Value.ToString("MM/dd/yyyy");
                        if (entityRep.PlaceOfBirth != null)
                            RepresentativeDTO.PlaceOfBirth = entityRep.PlaceOfBirth;
                    }


                    #region Phone,Mobile
                    if (entity.Telephone != null)
                    {
                        RepresentativeDTO.PlaceOfBirth = entity.Telephone;
                    }
                    if (entity.Mobile != null)
                    {
                        RepresentativeDTO.PlaceOfBirth = entity.Mobile;
                    }
                    #endregion
                    #region Visa
                    if (entity.NeedVisa != null)
                    {
                        //ShowVisaControls();
                        if (entity.NeedVisa == EnumConversion.ToString(EnumBool.Yes))
                        {
                            if (entity.VisaApplicationForm != null)
                                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + VisaFormPath + entity.VisaApplicationForm))
                                    RepresentativeDTO.VisaCopy = _appCustomSettings.UploadsCMSPath + "/Representative/VisaForm/" + entity.VisaApplicationForm;
                        }
                    }
                    else
                    {
                    }
                    #endregion
                }
                return RepresentativeDTO;
            }
        }
        string GetCountryTitle(long? countryId, long langId)
        {
            using (ExhibitionCountryService ExhibitionCountryService = new ExhibitionCountryService())
            {
                XsiExhibitionCountry where = new XsiExhibitionCountry();
                if (countryId.HasValue)
                    where.CountryId = countryId.Value;
                XsiExhibitionCountry entity = ExhibitionCountryService.GetExhibitionCountry(where).FirstOrDefault();
                if (entity != null)
                {
                    if (langId == 1 && entity.CountryName != null)
                        return entity.CountryName;
                    else
                        return entity.CountryNameAr;
                }

                return string.Empty;
            }
        }
        private string GetRepresentativeName(long representativeId, long exhibitorid, long langId)
        {
            using (ExhibitionRepresentativeService ExhibitionRepresentativeService = new ExhibitionRepresentativeService())
            {
                ExhibitionRepresentativeParticipatingService ExhibitionRepresentativeParticipatingService = new ExhibitionRepresentativeParticipatingService();
                var entity = ExhibitionRepresentativeService.GetRepresentativeByItemId(representativeId);
                if (entity != null)
                {
                    var representativeParticipating = ExhibitionRepresentativeParticipatingService.GetExhibitionRepresentativeParticipating(new XsiExhibitionRepresentativeParticipating() { MemberExhibitionYearlyId = exhibitorid, RepresentativeId = entity.RepresentativeId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                    if (representativeParticipating != null)
                        return langId == 1 ? representativeParticipating.NameEn : representativeParticipating.NameAr;
                }
                return string.Empty;
            }
        }
        private bool IsExhibitionActive()
        {
            using (ExhibitionService ExhibitionService = new ExhibitionService())
            {

                XsiExhibition whereQuery = new XsiExhibition();
                whereQuery.IsActive = EnumConversion.ToString(EnumBool.Yes);
                whereQuery.IsArchive = EnumConversion.ToString(EnumBool.No);

                // whereQuery.WebsiteId = RepresentativeDTO.WebsiteId;
                //List<XsiExhibition> ExhibitionList = ExhibitionService.GetExhibition(whereQuery).OrderByDescending(i => i.ItemId).ToList();
                XsiExhibition entity = ExhibitionService.GetExhibition(whereQuery).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                if (entity != null)
                {
                    return true;
                }
            }
            return false;
        }

        string GetDateFormated(DateTime date, long langId)
        {
            //if (langId == 2)
            //    return date.ToString("MMM dd, yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
            //else
            //    return date.ToString("MMM dd, yyyy");

            return date.ToString("dd-MM-yyyy");
        }
        #endregion

    }
}

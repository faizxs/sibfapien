﻿using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Org.BouncyCastle.Asn1.Ocsp;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using Xsi.ServicesLayer;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class POSDeviceController : Controller
    {
        private readonly sibfnewdbContext _context;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly IEmailSender _emailSender;
        private ILoggerManager _logger;
        //private readonly PDForImageBookMark _pDForImageBook;
        private string unauthorizedMessage = "Unauthorized access.";

        readonly string PassportPath = "POSDevice\\Passport\\";
        readonly string PassportPathTemp = "POSDevice\\Passport\\temp\\";
        #region Properties
        string strEmailContentBody { get; set; }
        string strEmailContentEmail { get; set; }
        string strEmailContentSubject { get; set; }
        string strEmailContentAdmin { get; set; }
        #endregion
        public POSDeviceController(sibfnewdbContext context, IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IEmailSender emailSender)
        {
            _context = context;
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _emailSender = emailSender;
        }

        // GET api/POSDevice/5
        [HttpGet("{exhibitorid}")]
        public ActionResult<dynamic> Get(long? exhibitorid)
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);

            long websiteid = 1;
            long.TryParse(Request.Headers["websiteId"], out websiteid);

            long memberid = User.Identity.GetID();

            MessageResponseDTO messageDTO = new MessageResponseDTO();
            messageDTO.MessageTypeResponse = EnumResultType.Error.ToString();
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    #region Validations
                    XsiExhibitionMemberApplicationYearly exhibitor = new XsiExhibitionMemberApplicationYearly();
                    exhibitor = _context.XsiExhibitionMemberApplicationYearly.Find(exhibitorid);
                    if (exhibitor == null)
                    {
                        messageDTO.Message = unauthorizedMessage;
                        return Unauthorized(messageDTO);
                    }
                    if (exhibitor.MemberId != memberid)
                    {
                        messageDTO.Message = unauthorizedMessage;
                        return Unauthorized(messageDTO);
                    }
                    if (!exhibitor.Status.Equals("I") && !exhibitor.Status.Equals("A"))
                    {
                        messageDTO.Message = unauthorizedMessage;
                        return Unauthorized(messageDTO);
                    }

                    var registerEntity = _context.XsiExhibitionPosdevice.AsNoTracking().Where(i => i.ExhibitorId == exhibitorid).OrderByDescending(i => i.ItemId).FirstOrDefault();
                    if (registerEntity == null)
                    {
                        var memberentity = _context.XsiExhibitionMember.Find(memberid);
                        messageDTO.MessageTypeResponse = EnumResultType.Success.ToString();
                        messageDTO.Message = string.Empty;
                        messageDTO.ResponseDto = new POSDeviceDto();

                        messageDTO.ResponseDto.PublisherNameEn = memberentity.CompanyName;
                        messageDTO.ResponseDto.PublisherNameAr = memberentity.CompanyNameAr;

                        if (memberentity.CityId != null)
                        {
                            var city = _context.XsiExhibitionCity.Find(memberentity.CityId);
                            if (city != null)
                            {
                                var country = _context.XsiExhibitionCountry.Find(city.CountryId);
                                if (country != null)
                                    messageDTO.ResponseDto.PublisherOriginCountryId = city.CountryId;
                            }
                        }

                        return Ok(messageDTO);
                    }
                    #endregion

                    messageDTO.ResponseDto = GetPOSData(registerEntity);
                    if (messageDTO.ResponseDto.RegistrationId > 0)
                    {
                        messageDTO.MessageTypeResponse = EnumResultType.Success.ToString();
                    }
                    return Ok(messageDTO);
                }
            }
            catch (Exception ex)
            {
                messageDTO.Message = "Exception: Something went wrong. Please retry again later.";
                return Ok(messageDTO);
            }
        }

        // POST api/POSDevice
        [HttpPost]
        public ActionResult<dynamic> POSDeviceRegistrationAdd(AddorUpdatePOSDeviceDto dto)
        {
            MessageResponseDTO messageDTO = new MessageResponseDTO();
            messageDTO.MessageTypeResponse = EnumResultType.Error.ToString();

            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);

            long websiteid = 1;
            long.TryParse(Request.Headers["websiteId"], out websiteid);

            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {

                    long memberid = User.Identity.GetID();

                    #region Validations
                    if (dto.RegistrationId > 0)
                    {
                        messageDTO.Message = "Data already exists";
                        return BadRequest(messageDTO);
                    }

                    XsiExhibitionMemberApplicationYearly exhibitor = new XsiExhibitionMemberApplicationYearly();
                    exhibitor = _context.XsiExhibitionMemberApplicationYearly.Find(dto.ExhibitorId);
                    if (exhibitor == null)
                    {
                        messageDTO.Message = "Invalid Request. ExhibitorId is required.";
                        return BadRequest(messageDTO);
                    }
                    if (exhibitor.MemberId != memberid)
                    {
                        messageDTO.Message = unauthorizedMessage;
                        return Unauthorized(messageDTO);
                    }
                    if (!exhibitor.Status.Equals("I") && !exhibitor.Status.Equals("A"))
                    {
                        messageDTO.Message = unauthorizedMessage + " Please check exhibitor status.";
                        return Unauthorized(messageDTO);
                    }

                    var PosdeviceEntity = _context.XsiExhibitionPosdevice.AsNoTracking().Where(i => i.ExhibitorId == dto.ExhibitorId).OrderByDescending(i => i.ItemId).FirstOrDefault();
                    if (PosdeviceEntity != null)
                    {
                        messageDTO.Message = "Invalid Request. Data already exists.";
                        return BadRequest(messageDTO);
                    }

                    if (dto.IsLocal == "Y")
                    {
                        if (string.IsNullOrEmpty(dto.EmiratedIdnumber))
                        {
                            messageDTO.Message = "Invalid Request. EmiratedId Number is required.";
                            return BadRequest(messageDTO);
                        }
                        if (string.IsNullOrEmpty(dto.MobileNumber))
                        {
                            messageDTO.Message = "Invalid Request. Mobile Number is required.";
                            return BadRequest(messageDTO);
                        }
                        if (string.IsNullOrEmpty(dto.BankNameInsideCountry))
                        {
                            messageDTO.Message = "Invalid Request. Bank Name is required.";
                            return BadRequest(messageDTO);
                        }
                        if (string.IsNullOrEmpty(dto.AccountHolderName))
                        {
                            messageDTO.Message = "Invalid Request. Account Holder Name is required.";
                            return BadRequest(messageDTO);
                        }
                        if (string.IsNullOrEmpty(dto.Ibannumber))
                        {
                            messageDTO.Message = "Invalid Request. IBAN Number is required.";
                            return BadRequest(messageDTO);
                        }
                        if (string.IsNullOrEmpty(dto.AdressPublishingHouseBankAccount))
                        {
                            messageDTO.Message = "Invalid Request. Address is required.";
                            return BadRequest(messageDTO);
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(dto.PassportNumber))
                        {
                            messageDTO.Message = "Invalid Request. Passport Number is required for International Exhibitor.";
                            return BadRequest(messageDTO);
                        }
                    }
                    #endregion

                    XsiExhibitionPosdevice existingEntity = new XsiExhibitionPosdevice();
                    XsiExhibitionPosdevice registerEntity = new XsiExhibitionPosdevice();
                    var regid = AddorUpdateData(registerEntity, dto, exhibitor.ExhibitionId.Value, langId, memberid, _context);
                    if (regid > 0)
                    {
                        messageDTO.MessageTypeResponse = EnumResultType.Success.ToString();
                        if (langId == 1)
                            messageDTO.Message = "Registration submitted successfully.";
                        else
                            messageDTO.Message = "Registration submitted successfully.";
                        //createdatroute
                        SendEmailBasedOnEdit(existingEntity, registerEntity, memberid, websiteid, dto.RegistrationId, langId, false);
                        return Ok(messageDTO);
                    }
                    messageDTO.Message = "Something went wrong. Please retry again later.";
                    return Ok(messageDTO);
                }
            }
            catch (Exception ex)
            {
                messageDTO.Message = "Exception: Something went wrong. Please retry again later.";
                return Ok(messageDTO);
            }
        }

        // PUT api/POSDevice/5
        [HttpPut("{regid}")]
        public ActionResult<dynamic> POSDeviceRegistrationUpate(long regid, AddorUpdatePOSDeviceDto dto)
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);

            long websiteid = 1;
            long.TryParse(Request.Headers["websiteId"], out websiteid);

            long memberid = User.Identity.GetID();

            MessageResponseDTO messageDTO = new MessageResponseDTO();
            messageDTO.MessageTypeResponse = EnumResultType.Error.ToString();

            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    #region Validations
                    if (dto.RegistrationId == 0)
                    {
                        messageDTO.Message = "Invalid Request. RegistrationId is required.";
                        return BadRequest(messageDTO);
                    }

                    if (regid != dto.RegistrationId)
                    {
                        messageDTO.Message = "Invalid Request. RegistrationId is mismatch.";
                        return BadRequest(messageDTO);
                    }
                    XsiExhibitionPosdevice existingEntity = new XsiExhibitionPosdevice();
                    XsiExhibitionPosdevice registerEntity = _context.XsiExhibitionPosdevice.Find(dto.RegistrationId);
                    if (registerEntity == null)
                    {
                        messageDTO.Message = "No Data Found";
                        return NotFound(messageDTO);
                    }

                    existingEntity = _context.XsiExhibitionPosdevice.AsNoTracking().Where(i => i.ItemId == dto.RegistrationId).FirstOrDefault(); ;
                    XsiExhibitionMemberApplicationYearly exhibitor = new XsiExhibitionMemberApplicationYearly();
                    exhibitor = _context.XsiExhibitionMemberApplicationYearly.Find(dto.ExhibitorId);
                    if (exhibitor == null)
                    {
                        messageDTO.Message = "Invalid Request. ExhibitorId is required.";
                        return BadRequest(messageDTO);
                    }
                    if (exhibitor.MemberId != memberid)
                    {
                        messageDTO.Message = unauthorizedMessage;
                        return Unauthorized(messageDTO);
                    }
                    if (!exhibitor.Status.Equals("I") && !exhibitor.Status.Equals("A"))
                    {
                        messageDTO.Message = unauthorizedMessage + " Please check exhibitor status.";
                        return Unauthorized(messageDTO);
                    }
                    if (registerEntity.ExhibitorId != dto.ExhibitorId)
                    {
                        messageDTO.Message = unauthorizedMessage;
                        return Unauthorized(messageDTO);
                    }

                    if (dto.IsLocal == "Y")
                    {
                        if (string.IsNullOrEmpty(dto.EmiratedIdnumber))
                        {
                            messageDTO.Message = "Invalid Request. EmiratedId Number is required.";
                            return BadRequest(messageDTO);
                        }
                        if (string.IsNullOrEmpty(dto.MobileNumber))
                        {
                            messageDTO.Message = "Invalid Request. Mobile Number is required.";
                            return BadRequest(messageDTO);
                        }
                        if (string.IsNullOrEmpty(dto.BankNameInsideCountry))
                        {
                            messageDTO.Message = "Invalid Request. Bank Name is required.";
                            return BadRequest(messageDTO);
                        }
                        if (string.IsNullOrEmpty(dto.AccountHolderName))
                        {
                            messageDTO.Message = "Invalid Request. Account Holder Name is required.";
                            return BadRequest(messageDTO);
                        }
                        if (string.IsNullOrEmpty(dto.Ibannumber))
                        {
                            messageDTO.Message = "Invalid Request. IBAN Number is required.";
                            return BadRequest(messageDTO);
                        }
                        if (string.IsNullOrEmpty(dto.AdressPublishingHouseBankAccount))
                        {
                            messageDTO.Message = "Invalid Request. Address is required.";
                            return BadRequest(messageDTO);
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(dto.PassportNumber))
                        {
                            messageDTO.Message = "Invalid Request. Passport Number is required for International Exhibitor.";
                            return BadRequest(messageDTO);
                        }
                    }
                    #endregion

                    AddorUpdateData(registerEntity, dto, exhibitor.ExhibitionId.Value, langId, memberid, _context);

                    SendEmailBasedOnEdit(existingEntity, registerEntity, memberid, websiteid, dto.RegistrationId, langId, true);

                    messageDTO.MessageTypeResponse = EnumResultType.Success.ToString();
                    if (langId == 1)
                        messageDTO.Message = "Registration updated successfully.";
                    else
                        messageDTO.Message = "Registration updated successfully.";
                    //return NoContent();
                    return Ok(messageDTO);
                }
            }
            catch (Exception ex)
            {
                messageDTO.Message = "Exception: Something went wrong. Please retry again later.";
                return Ok(messageDTO);
            }
        }

        #region Methods

        private POSDeviceDto GetPOSData(XsiExhibitionPosdevice registrationEnity)
        {
            POSDeviceDto dto = new POSDeviceDto();
            dto.RegistrationId = registrationEnity.ItemId;
            dto.LanguageUrl = registrationEnity.LanguageUrl;
            dto.IsLocal = registrationEnity.IsLocal;
            dto.PosdeviceQauntity = registrationEnity.PosdeviceQauntity;
            dto.ExhibitorId = registrationEnity.ExhibitorId;
            dto.IdentificationNumber = registrationEnity.IdentificationNumber;
            dto.FirstName = registrationEnity.FirstName;
            dto.LastName = registrationEnity.LastName;
            dto.FirstNameAr = registrationEnity.FirstNameAr;
            dto.LastNameAr = registrationEnity.LastNameAr;
            dto.RecipientNationalityId = registrationEnity.RecipientNationality;

            dto.PublisherNameEn = registrationEnity.PublisherNameEn;
            dto.PublisherNameAr = registrationEnity.PublisherNameAr;
            dto.PublisherOriginCountryId = registrationEnity.PublisherOriginCountry;

            dto.PassportNumber = registrationEnity.PassportNumber;
            dto.EmiratedIdnumber = registrationEnity.EmiratedIdnumber;
            if (registrationEnity.MobileNumberLocal != null)
            {
                string[] str = registrationEnity.MobileNumberLocal.Split('$');
                if (str.Count() == 3)
                {
                    dto.MobileISD = str[0].Trim();
                    dto.MobileSTD = str[1].Trim();
                    dto.MobileNumber = str[2].Trim();
                }
                else if (str.Count() == 2)
                {
                    dto.MobileSTD = str[0].Trim();
                    dto.MobileNumber = str[1].Trim();
                }
                else
                    dto.MobileNumber = str[0].Trim();
            }

            dto.PublishingHouseEmail = registrationEnity.PublishingHouseEmail;
            dto.BankNameInsideCountry = registrationEnity.BankNameInsideCountry;
            dto.AccountHolderName = registrationEnity.AccountHolderName;
            dto.Ibannumber = registrationEnity.Ibannumber;
            dto.AdressPublishingHouseBankAccount = registrationEnity.AdressPublishingHouseBankAccount;

            if (!string.IsNullOrEmpty(registrationEnity.PassportCopy))
            {
                if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + PassportPath + registrationEnity.PassportCopy))
                    dto.PassportCopy = _appCustomSettings.UploadsCMSPath + "/POSDevice/Passport/" + registrationEnity.PassportCopy;
            }
            return dto;
        }
        private long AddorUpdateData(XsiExhibitionPosdevice registrationEntity, AddorUpdatePOSDeviceDto dto, long exhibitionid, long langId, long memberid, sibfnewdbContext _context)
        {
            string filename = string.Empty;
            string ext = "PNG";
            registrationEntity.LanguageUrl = langId;
            registrationEntity.IsLocal = dto.IsLocal;
            registrationEntity.PosdeviceQauntity = dto.PosdeviceQauntity;
            registrationEntity.ExhibitorId = dto.ExhibitorId;

            registrationEntity.FirstName = dto.FirstName;
            registrationEntity.LastName = dto.LastName;
            registrationEntity.FirstNameAr = dto.FirstNameAr;
            registrationEntity.LastNameAr = dto.LastNameAr;

            if (dto.RecipientNationalityId > 0)
                registrationEntity.RecipientNationality = dto.RecipientNationalityId;

            registrationEntity.PublisherNameEn = dto.PublisherNameEn;
            registrationEntity.PublisherNameAr = dto.PublisherNameAr;

            if (dto.PublisherOriginCountryId > 0)
                registrationEntity.PublisherOriginCountry = dto.PublisherOriginCountryId;

            registrationEntity.EmiratedIdnumber = dto.EmiratedIdnumber;

            if (!string.IsNullOrEmpty(dto.MobileISD) && !string.IsNullOrEmpty(dto.MobileSTD) && !string.IsNullOrEmpty(dto.MobileNumber))
                registrationEntity.MobileNumberLocal = dto.MobileISD + "$" + dto.MobileSTD + "$" + dto.MobileNumber;

            registrationEntity.PublishingHouseEmail = dto.PublishingHouseEmail;
            registrationEntity.BankNameInsideCountry = dto.BankNameInsideCountry;
            registrationEntity.AccountHolderName = dto.AccountHolderName;
            registrationEntity.Ibannumber = dto.Ibannumber;
            registrationEntity.AdressPublishingHouseBankAccount = dto.AdressPublishingHouseBankAccount;
            registrationEntity.PassportNumber = dto.PassportNumber;

            if (!string.IsNullOrEmpty(dto.FileBase64Passport))
            {
                PDForImageBookMark PDForImageBookMarkObj = new PDForImageBookMark();
                ext = dto.FileExtensionofPassport.ToLower();
                filename = string.Format("{0}_{1}.{2}", 1, MethodFactory.GetRandomNumber(), ext);

                if (ext.IndexOf("pdf") > -1)
                    filename = PDForImageBookMarkObj.WriteTextOnPDF(dto.FileBase64Passport, filename, dto.FirstName, registrationEntity.IdentificationNumber, memberid, ext, PassportPath, PassportPathTemp, _appCustomSettings);
                else
                    filename = PDForImageBookMarkObj.WriteTextOnImage(dto.FileBase64Passport, filename, dto.FirstName, registrationEntity.IdentificationNumber, memberid, ext, PassportPath, PassportPathTemp, _appCustomSettings);

                registrationEntity.PassportCopy = filename;
            }
            registrationEntity.IsActive = "Y";
            registrationEntity.ModifiedOn = MethodFactory.ArabianTimeNow();

            if (dto.RegistrationId == 0)
            {
                registrationEntity.CreatedOn = MethodFactory.ArabianTimeNow();
                var exhibitorIDs = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.ExhibitionId == exhibitionid && (i.Status == "I" || i.Status == "A")).Select(i => i.MemberExhibitionYearlyId).ToList();
                var latestposdeviceregEnity = _context.XsiExhibitionPosdevice.Where(i => exhibitorIDs.Contains(i.ExhibitorId.Value) && !string.IsNullOrEmpty(i.IdentificationNumber)).OrderByDescending(i => i.ItemId).FirstOrDefault();

                if (latestposdeviceregEnity == null)
                    registrationEntity.IdentificationNumber = "0001";
                else
                    registrationEntity.IdentificationNumber = GenerateIdentificationNumber(registrationEntity.IdentificationNumber);

                _context.XsiExhibitionPosdevice.Add(registrationEntity);
                _context.SaveChanges();
                return registrationEntity.ItemId;
            }

            _context.XsiExhibitionPosdevice.Update(registrationEntity);
            _context.SaveChanges();
            return registrationEntity.ItemId;
        }
        private string GenerateIdentificationNumber(string identificationnumber)
        {
            long latestidentificationNumber = Convert.ToInt64(identificationnumber) + 1;
            if (latestidentificationNumber.ToString().Length == 1)
                return "000" + (latestidentificationNumber + 1).ToString();
            else if (latestidentificationNumber.ToString().Length == 2)
                return "00" + (latestidentificationNumber + 1).ToString();
            else if (latestidentificationNumber.ToString().Length == 3)
                return "0" + (latestidentificationNumber + 1).ToString();
            else if (latestidentificationNumber.ToString().Length == 4)
                return (latestidentificationNumber + 1).ToString();

            return string.Empty;
        }
        #endregion

        void SendEmailBasedOnEdit(XsiExhibitionPosdevice CurrentExhibitorEntity, XsiExhibitionPosdevice newEntity, long memberId, long websiteId, long registrationid, long langId, bool isupdate = false)
        {
            using (EmailContentService EmailContentService = new EmailContentService())
            {
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                XsiExhibitionMember ExhibitionMemberEntity = new XsiExhibitionMember();
                ExhibitionMemberEntity = _context.XsiExhibitionMember.Find(memberId);
                if (ExhibitionMemberEntity != default(XsiExhibitionMember))
                {
                    #region Send Edit Request Email
                    StringBuilder body = new StringBuilder();
                    #region Email Content
                    SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        XsiScrfemailContent scrfEmailContent = new XsiScrfemailContent();

                        if (isupdate)
                            scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(20147);
                        else
                            scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(20148);

                        if (scrfEmailContent != null)
                        {
                            if (langId == 1)
                            {
                                if (scrfEmailContent.Body != null)
                                    strEmailContentBody = scrfEmailContent.Body;
                                if (scrfEmailContent.Email != null)
                                    strEmailContentEmail = scrfEmailContent.Email;
                                if (scrfEmailContent.Subject != null)
                                    strEmailContentSubject = scrfEmailContent.Subject;
                            }
                            else
                            {
                                if (scrfEmailContent.BodyAr != null)
                                    strEmailContentBody = scrfEmailContent.BodyAr;
                                if (scrfEmailContent.Email != null)
                                    strEmailContentEmail = scrfEmailContent.Email;
                                if (scrfEmailContent.SubjectAr != null)
                                    strEmailContentSubject = scrfEmailContent.SubjectAr;
                            }
                            strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                        }
                    }
                    else
                    {
                        XsiEmailContent emailContent = new XsiEmailContent();

                        if (isupdate)
                            emailContent = EmailContentService.GetEmailContentByItemId(20203);
                        else
                            emailContent = EmailContentService.GetEmailContentByItemId(20204);

                        if (emailContent != null)
                        {
                            if (langId == 1)
                            {
                                if (emailContent.Body != null)
                                    strEmailContentBody = emailContent.Body;
                                if (emailContent.Email != null)
                                    strEmailContentEmail = emailContent.Email;
                                if (emailContent.Subject != null)
                                    strEmailContentSubject = emailContent.Subject;
                            }
                            else
                            {
                                if (emailContent.BodyAr != null)
                                    strEmailContentBody = emailContent.BodyAr;
                                if (emailContent.Email != null)
                                    strEmailContentEmail = emailContent.Email;
                                if (emailContent.SubjectAr != null)
                                    strEmailContentSubject = emailContent.SubjectAr;
                            }
                            strEmailContentAdmin = _appCustomSettings.AdminName;
                        }
                    }
                    #endregion

                    body.Append(htmlContentFactory.BindEmailContent(ExhibitionMemberEntity.LanguageUrl.Value, strEmailContentBody, ExhibitionMemberEntity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                    string strContent = "<p style=\"font-size: 16px; line-height: 32px; font-family: Arial, Helvetica, sans-serif; color: #555555; margin: 0px;\">$$Label$$ : $$Value$$</p>";
                    // XsiExhibitionPosdevice newEntity = _context.XsiExhibitionPosdevice.Find(registrationid);
                    if (newEntity != null)
                    {
                        #region Body
                        bool IsAnyFieldChanged = false;
                        if (newEntity.FirstName != null)
                            body.Replace("$$ExhibitorNameEn$$", newEntity.FirstName);
                        if (newEntity.IdentificationNumber != null)
                            body.Replace("$$FileNumber$$", newEntity.IdentificationNumber);

                        #region IsLocal
                        if (CurrentExhibitorEntity.IsLocal != newEntity.IsLocal)
                        {
                            if (newEntity.IsLocal != null)
                            {
                                body.Replace("$$NewIsLocal$$", strContent);
                                body.Replace("$$Label$$", "Is Local?");
                                body.Replace("$$Value$$", newEntity.IsLocal == "Y" ? "Local" : "International");
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewIsLocal$$", string.Empty);
                            if (CurrentExhibitorEntity.IsLocal != null)
                            {
                                body.Replace("$$OldIsLocal$$", strContent);
                                body.Replace("$$Label$$", "Is Local?");
                                body.Replace("$$Value$$", CurrentExhibitorEntity.IsLocal == "Y" ? "Local" : "International");
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldIsLocal$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewIsLocal$$", string.Empty);
                            body.Replace("$$OldIsLocal$$", string.Empty);
                        }
                        #endregion

                        #region Device Quantity
                        if (CurrentExhibitorEntity.PosdeviceQauntity != newEntity.PosdeviceQauntity)
                        {
                            if (newEntity.PosdeviceQauntity != null)
                            {
                                body.Replace("$$NewPosdeviceQauntity$$", strContent);
                                body.Replace("$$Label$$", "Device Quantity");
                                body.Replace("$$Value$$", newEntity.PosdeviceQauntity.ToString());
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewPosdeviceQauntity$$", string.Empty);
                            if (CurrentExhibitorEntity.PosdeviceQauntity != null)
                            {
                                body.Replace("$$OldPosdeviceQauntity$$", strContent);
                                body.Replace("$$Label$$", "Device Quantity");
                                body.Replace("$$Value$$", CurrentExhibitorEntity.PosdeviceQauntity.ToString());
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldPosdeviceQauntity$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewPosdeviceQauntity$$", string.Empty);
                            body.Replace("$$OldPosdeviceQauntity$$", string.Empty);
                        }
                        #endregion

                        #region First Name
                        if (CurrentExhibitorEntity.FirstName != newEntity.FirstName)
                        {
                            if (newEntity.FirstName != null)
                            {
                                body.Replace("$$NewFirstName$$", strContent);
                                body.Replace("$$Label$$", "First Name in English");
                                body.Replace("$$Value$$", newEntity.FirstName);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewFirstName$$", string.Empty);
                            if (CurrentExhibitorEntity.FirstName != null)
                            {
                                body.Replace("$$OldFirstName$$", strContent);
                                body.Replace("$$Label$$", "First Name in English");
                                body.Replace("$$Value$$", CurrentExhibitorEntity.FirstName);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldFirstName$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewFirstName$$", string.Empty);
                            body.Replace("$$OldFirstName$$", string.Empty);
                        }
                        #endregion

                        #region Last Name English
                        if (CurrentExhibitorEntity.LastName != newEntity.LastName)
                        {
                            if (newEntity.LastName != null)
                            {
                                body.Replace("$$NewLastName$$", strContent);
                                body.Replace("$$Label$$", "Last Name in English");
                                body.Replace("$$Value$$", newEntity.LastName);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewLastName$$", string.Empty);
                            if (CurrentExhibitorEntity.LastName != null)
                            {
                                body.Replace("$$OldLastName$$", strContent);
                                body.Replace("$$Label$$", "Last Name in English");
                                body.Replace("$$Value$$", CurrentExhibitorEntity.LastName);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldLastName$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewLastName$$", string.Empty);
                            body.Replace("$$OldLastName$$", string.Empty);
                        }
                        #endregion

                        #region FirstNameAr English
                        if (CurrentExhibitorEntity.FirstNameAr != newEntity.FirstNameAr)
                        {
                            if (newEntity.FirstNameAr != null)
                            {
                                body.Replace("$$NewFirstNameAr$$", strContent);
                                body.Replace("$$Label$$", "First Name in Arabic");
                                body.Replace("$$Value$$", newEntity.FirstNameAr);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewFirstNameAr$$", string.Empty);
                            if (CurrentExhibitorEntity.FirstNameAr != null)
                            {
                                body.Replace("$$OldFirstNameAr$$", strContent);
                                body.Replace("$$Label$$", "First Name in Arabic");
                                body.Replace("$$Value$$", CurrentExhibitorEntity.FirstNameAr);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldFirstNameAr$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewFirstNameAr$$", string.Empty);
                            body.Replace("$$OldFirstNameAr$$", string.Empty);
                        }
                        #endregion

                        #region LastNameAr English
                        if (CurrentExhibitorEntity.LastNameAr != newEntity.LastNameAr)
                        {
                            if (newEntity.LastNameAr != null)
                            {
                                body.Replace("$$NewLastNameAr$$", strContent);
                                body.Replace("$$Label$$", "Last Name in Arabic");
                                body.Replace("$$Value$$", newEntity.LastNameAr);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewLastNameAr$$", string.Empty);
                            if (CurrentExhibitorEntity.LastNameAr != null)
                            {
                                body.Replace("$$OldLastNameAr$$", strContent);
                                body.Replace("$$Label$$", "Last Name in Arabic");
                                body.Replace("$$Value$$", CurrentExhibitorEntity.LastNameAr);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldLastNameAr$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewLastNameAr$$", string.Empty);
                            body.Replace("$$OldLastNameAr$$", string.Empty);
                        }
                        #endregion

                        #region RecipientNationality
                        if (CurrentExhibitorEntity.RecipientNationality != newEntity.RecipientNationality)
                        {
                            if (newEntity.RecipientNationality != null)
                            {
                                body.Replace("$$NewRecipientNationality$$", strContent);
                                body.Replace("$$Label$$", "Recipient Nationality");
                                if (MethodFactory.GetCountryName(newEntity.RecipientNationality.Value, langId) != null)
                                {
                                    body.Replace("$$Value$$", MethodFactory.GetCountryName(newEntity.RecipientNationality.Value, langId));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewRecipientNationality$$", string.Empty);
                            }
                            else
                                body.Replace("$$NewRecipientNationality$$", string.Empty);
                            if (CurrentExhibitorEntity.RecipientNationality != null)
                            {
                                body.Replace("$$OldRecipientNationality$$", strContent);
                                body.Replace("$$Label$$", "RecipientNationality");
                                if (MethodFactory.GetCountryName(CurrentExhibitorEntity.RecipientNationality.Value, langId) != null)
                                {
                                    body.Replace("$$Value$$", MethodFactory.GetCountryName(CurrentExhibitorEntity.RecipientNationality.Value, langId));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldRecipientNationality$$", string.Empty);
                            }
                            else
                                body.Replace("$$OldRecipientNationality$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewRecipientNationality$$", string.Empty);
                            body.Replace("$$OldRecipientNationality$$", string.Empty);
                        }
                        #endregion

                        #region Publisher English
                        if (CurrentExhibitorEntity.PublisherNameEn != newEntity.PublisherNameEn)
                        {
                            if (newEntity.PublisherNameEn != null)
                            {
                                body.Replace("$$NewPublisherNameEn$$", strContent);
                                body.Replace("$$Label$$", "Publisher Name in English");
                                body.Replace("$$Value$$", newEntity.PublisherNameEn);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewPublisherNameEn$$", string.Empty);
                            if (CurrentExhibitorEntity.PublisherNameEn != null)
                            {
                                body.Replace("$$OldPublisherNameEn$$", strContent);
                                body.Replace("$$Label$$", "Publisher Name in English");
                                body.Replace("$$Value$$", CurrentExhibitorEntity.PublisherNameEn);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldPublisherNameEn$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewPublisherNameEn$$", string.Empty);
                            body.Replace("$$OldPublisherNameEn$$", string.Empty);
                        }
                        #endregion

                        #region Publisher Arabic
                        if (CurrentExhibitorEntity.PublisherNameAr != newEntity.PublisherNameAr)
                        {
                            if (newEntity.PublisherNameAr != null)
                            {
                                body.Replace("$$NewPublisherNameAr$$", strContent);
                                body.Replace("$$Label$$", "Publisher Name in Arabic");
                                body.Replace("$$Value$$", newEntity.PublisherNameAr);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewPublisherNameAr$$", string.Empty);
                            if (CurrentExhibitorEntity.PublisherNameAr != null)
                            {
                                body.Replace("$$OldPublisherNameAr$$", strContent);
                                body.Replace("$$Label$$", "Publisher Name in Arabic");
                                body.Replace("$$Value$$", CurrentExhibitorEntity.PublisherNameAr);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldPublisherNameAr$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewPublisherNameAr$$", string.Empty);
                            body.Replace("$$OldPublisherNameAr$$", string.Empty);
                        }
                        #endregion

                        #region PublisherOriginCountry
                        if (CurrentExhibitorEntity.PublisherOriginCountry != newEntity.PublisherOriginCountry)
                        {
                            if (newEntity.PublisherOriginCountry != null)
                            {
                                body.Replace("$$NewPublisherOriginCountry$$", strContent);
                                body.Replace("$$Label$$", "Publisher Origin Country");
                                if (MethodFactory.GetCountryName(newEntity.PublisherOriginCountry.Value, langId) != null)
                                {
                                    body.Replace("$$Value$$", MethodFactory.GetCountryName(newEntity.PublisherOriginCountry.Value, langId));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewPublisherOriginCountry$$", string.Empty);
                            }
                            else
                                body.Replace("$$NewPublisherOriginCountry$$", string.Empty);
                            if (CurrentExhibitorEntity.PublisherOriginCountry != null)
                            {
                                body.Replace("$$OldPublisherOriginCountry$$", strContent);
                                body.Replace("$$Label$$", "Publisher Origin Country");
                                if (MethodFactory.GetCountryName(CurrentExhibitorEntity.PublisherOriginCountry.Value, langId) != null)
                                {
                                    body.Replace("$$Value$$", MethodFactory.GetCountryName(CurrentExhibitorEntity.PublisherOriginCountry.Value, langId));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldPublisherOriginCountry$$", string.Empty);
                            }
                            else
                                body.Replace("$$OldPublisherOriginCountry$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewPublisherOriginCountry$$", string.Empty);
                            body.Replace("$$OldPublisherOriginCountry$$", string.Empty);
                        }
                        #endregion

                        #region PassportNumber
                        if (CurrentExhibitorEntity.PassportNumber != newEntity.PassportNumber)
                        {
                            if (newEntity.PassportNumber != null)
                            {
                                body.Replace("$$NewPassportNumber$$", strContent);
                                body.Replace("$$Label$$", "Passport Number");
                                body.Replace("$$Value$$", newEntity.PassportNumber);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewPassportNumber$$", string.Empty);
                            if (CurrentExhibitorEntity.PassportNumber != null)
                            {
                                body.Replace("$$OldPassportNumber$$", strContent);
                                body.Replace("$$Label$$", "Passport Number");
                                body.Replace("$$Value$$", CurrentExhibitorEntity.PassportNumber);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldPassportNumber$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewPassportNumber$$", string.Empty);
                            body.Replace("$$OldPassportNumber$$", string.Empty);
                        }
                        #endregion

                        #region Email
                        if (CurrentExhibitorEntity.PublishingHouseEmail != newEntity.PublishingHouseEmail)
                        {
                            if (newEntity.PublishingHouseEmail != null)
                            {
                                body.Replace("$$NewEmail$$", strContent);
                                body.Replace("$$Label$$", "Publishing House Email");
                                body.Replace("$$Value$$", newEntity.PublishingHouseEmail);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewEmail$$", string.Empty);
                            if (CurrentExhibitorEntity.PublishingHouseEmail != null)
                            {
                                body.Replace("$$OldEmail$$", strContent);
                                body.Replace("$$Label$$", "Publishing House Email");
                                body.Replace("$$Value$$", CurrentExhibitorEntity.PublishingHouseEmail);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldEmail$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewEmail$$", string.Empty);
                            body.Replace("$$OldEmail$$", string.Empty);
                        }
                        #endregion

                        if (newEntity.IsLocal == "Y")
                        {
                            #region Emirates Id
                            if (CurrentExhibitorEntity.EmiratedIdnumber != newEntity.EmiratedIdnumber)
                            {
                                if (newEntity.EmiratedIdnumber != null)
                                {
                                    body.Replace("$$NewEmiratedIdnumber$$", strContent);
                                    body.Replace("$$Label$$", "Emirates Id");
                                    body.Replace("$$Value$$", newEntity.EmiratedIdnumber);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewEmiratedIdnumber$$", string.Empty);
                                if (CurrentExhibitorEntity.EmiratedIdnumber != null)
                                {
                                    body.Replace("$$OldEmiratedIdnumber$$", strContent);
                                    body.Replace("$$Label$$", "Emirates Id");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.EmiratedIdnumber);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldEmiratedIdnumber$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewEmiratedIdnumber$$", string.Empty);
                                body.Replace("$$OldEmiratedIdnumber$$", string.Empty);
                            }
                            #endregion

                            #region Mobile
                            if (CurrentExhibitorEntity.MobileNumberLocal != newEntity.MobileNumberLocal)
                            {
                                if (newEntity.MobileNumberLocal != null)
                                {
                                    body.Replace("$$NewMobile$$", strContent);
                                    body.Replace("$$Label$$", "Mobile");
                                    body.Replace("$$Value$$", newEntity.MobileNumberLocal.Replace('$', '-'));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewMobile$$", string.Empty);
                                if (CurrentExhibitorEntity.MobileNumberLocal != null)
                                {
                                    body.Replace("$$OldMobile$$", strContent);
                                    body.Replace("$$Label$$", "Mobile");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.MobileNumberLocal);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldMobile$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewMobile$$", string.Empty);
                                body.Replace("$$OldMobile$$", string.Empty);
                            }
                            #endregion

                            #region BankNameInsideCountry
                            if (CurrentExhibitorEntity.BankNameInsideCountry != newEntity.BankNameInsideCountry)
                            {
                                if (newEntity.BankNameInsideCountry != null)
                                {
                                    body.Replace("$$NewBankNameInsideCountry$$", strContent);
                                    body.Replace("$$Label$$", "Bank Name inside Country");
                                    body.Replace("$$Value$$", newEntity.BankNameInsideCountry);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewBankNameInsideCountry$$", string.Empty);
                                if (CurrentExhibitorEntity.BankNameInsideCountry != null)
                                {
                                    body.Replace("$$OldBankNameInsideCountry$$", strContent);
                                    body.Replace("$$Label$$", "Bank Name inside Country");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.BankNameInsideCountry);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldBankNameInsideCountry$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewBankNameInsideCountry$$", string.Empty);
                                body.Replace("$$OldBankNameInsideCountry$$", string.Empty);
                            }
                            #endregion

                            #region Account Holder Name
                            if (CurrentExhibitorEntity.AccountHolderName != newEntity.AccountHolderName)
                            {
                                if (newEntity.AccountHolderName != null)
                                {
                                    body.Replace("$$NewAccountHolderName$$", strContent);
                                    body.Replace("$$Label$$", "Account Holder Name");
                                    body.Replace("$$Value$$", newEntity.AccountHolderName);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewAccountHolderName$$", string.Empty);
                                if (CurrentExhibitorEntity.AccountHolderName != null)
                                {
                                    body.Replace("$$OldAccountHolderName$$", strContent);
                                    body.Replace("$$Label$$", "Account Holder Name");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.AccountHolderName);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldAccountHolderName$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewAccountHolderName$$", string.Empty);
                                body.Replace("$$OldAccountHolderName$$", string.Empty);
                            }
                            #endregion

                            #region IBan Number
                            if (CurrentExhibitorEntity.Ibannumber != newEntity.Ibannumber)
                            {
                                if (newEntity.Ibannumber != null)
                                {
                                    body.Replace("$$NewIbannumber$$", strContent);
                                    body.Replace("$$Label$$", "IBan Number");
                                    body.Replace("$$Value$$", newEntity.Ibannumber);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewIbannumber$$", string.Empty);
                                if (CurrentExhibitorEntity.Ibannumber != null)
                                {
                                    body.Replace("$$OldIbannumber$$", strContent);
                                    body.Replace("$$Label$$", "IBan Number");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.Ibannumber);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldIbannumber$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewIbannumber$$", string.Empty);
                                body.Replace("$$OldIbannumber$$", string.Empty);
                            }
                            #endregion

                            #region AdressPublishingHouseBankAccount
                            if (CurrentExhibitorEntity.AdressPublishingHouseBankAccount != newEntity.AdressPublishingHouseBankAccount)
                            {
                                if (newEntity.AdressPublishingHouseBankAccount != null)
                                {
                                    body.Replace("$$NewAdressPublishingHouseBankAccount$$", strContent);
                                    body.Replace("$$Label$$", "Adress Publishing House Bank Account");
                                    body.Replace("$$Value$$", newEntity.AdressPublishingHouseBankAccount);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewAdressPublishingHouseBankAccount$$", string.Empty);
                                if (CurrentExhibitorEntity.AdressPublishingHouseBankAccount != null)
                                {
                                    body.Replace("$$OldAdressPublishingHouseBankAccount$$", strContent);
                                    body.Replace("$$Label$$", "Adress Publishing House Bank Account");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.AdressPublishingHouseBankAccount);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldAdressPublishingHouseBankAccount$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewAdressPublishingHouseBankAccount$$", string.Empty);
                                body.Replace("$$OldAdressPublishingHouseBankAccount$$", string.Empty);
                            }
                            #endregion

                            #region Remove International Fields
                            body.Replace("$$NewPassportCopy$$", string.Empty);
                            body.Replace("$$OldPassportCopy$$", string.Empty);
                            #endregion
                        }
                        else
                        {
                            #region PassportCopy
                            if (CurrentExhibitorEntity.PassportCopy != newEntity.PassportCopy)
                            {
                                if (newEntity.PassportCopy != null)
                                {
                                    body.Replace("$$NewPassportCopy$$", strContent);
                                    body.Replace("$$Label$$", "Passport Copy");
                                    body.Replace("$$Value$$", newEntity.PassportCopy);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewPassportCopy$$", string.Empty);
                                if (CurrentExhibitorEntity.PassportCopy != null)
                                {
                                    body.Replace("$$OldPassportCopy$$", strContent);
                                    body.Replace("$$Label$$", "Passport Copy");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.PassportCopy);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldPassportCopy$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewPassportCopy$$", string.Empty);
                                body.Replace("$$OldPassportCopy$$", string.Empty);
                            }
                            #endregion

                            #region Remove Local Fields
                            body.Replace("$$NewEmiratedIdnumber$$", string.Empty);
                            body.Replace("$$OldEmiratedIdnumber$$", string.Empty);

                            body.Replace("$$NewMobile$$", string.Empty);
                            body.Replace("$$OldMobile$$", string.Empty);

                            body.Replace("$$NewBankNameInsideCountry$$", string.Empty);
                            body.Replace("$$OldBankNameInsideCountry$$", string.Empty);

                            body.Replace("$$NewAccountHolderName$$", string.Empty);
                            body.Replace("$$OldAccountHolderName$$", string.Empty);

                            body.Replace("$$NewIbannumber$$", string.Empty);
                            body.Replace("$$OldIbannumber$$", string.Empty);

                            body.Replace("$$NewAdressPublishingHouseBankAccount$$", string.Empty);
                            body.Replace("$$OldAdressPublishingHouseBankAccount$$", string.Empty);
                            #endregion
                        }
                        #endregion
                        if (IsAnyFieldChanged)
                        {
                            if (strEmailContentEmail != string.Empty)
                            {
                                _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString().Replace("$$SpecificName$$", strEmailContentAdmin));
                            }
                        }
                    }
                    #endregion
                }
            }
        }
    }



}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PublisherWeeklyController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;

        public PublisherWeeklyController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings)
        {
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
        }

        // POST: api/PublisherWeekly
        [HttpPost]
        public async Task<ActionResult<dynamic>> GetXsiPublisherWeeklyearch(PublisherWeeklySearchDTO userdto)
        {
            var List1 = new List<XsiPublisherWeekly>();
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    var predicatePublisherWeekly = PredicateBuilder.New<XsiPublisherWeekly>();
                    if (LangId == 1)
                    {
                        predicatePublisherWeekly = predicatePublisherWeekly.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                        if (!string.IsNullOrEmpty(userdto.CategoryType) && userdto.CategoryType != "-1")
                        {
                            predicatePublisherWeekly = predicatePublisherWeekly.And(i => i.Category.Equals(userdto.CategoryType));
                        }

                        if (!string.IsNullOrEmpty(userdto.IssueNumber) && userdto.IssueNumber != "-1")
                        {
                            userdto.IssueNumber = userdto.IssueNumber.ToLower();
                            var likeExpression = "%" + userdto.IssueNumber + "%";
                            predicatePublisherWeekly = predicatePublisherWeekly.And(i => EF.Functions.Like(i.IssueNumber, likeExpression));
                        }

                        if (!string.IsNullOrEmpty(userdto.PublishDate) && userdto.PublishDate != "-1")
                        {
                            predicatePublisherWeekly = predicatePublisherWeekly.And(i => i.PublisherDate != null && i.PublisherDate.Value.ToString("yyyy-MM-dd") == userdto.PublishDate);
                        }

                        var List = await _context.XsiPublisherWeekly.Where(predicatePublisherWeekly)
                                             .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new PublisherWeeklyDTO()
                                             {
                                                 ItemId = i.ItemId,
                                                 // Title = i.Name,
                                                 CategoryType = i.Category == "1" ? "Digital Copy" : "Hardcover",
                                                 PublishDate = i.PublisherDate == null ? string.Empty : i.PublisherDate.Value.ToString("yyyy-MM-dd"),
                                                 Thumbnail = _appCustomSettings.UploadsPath + "/publisherweekly/cover/" + (!string.IsNullOrEmpty(i.MagazineCover) ? i.MagazineCover : "PublisherWeekly.jpg"),
                                                 IssueNumber = i.IssueNumber,
                                                 URL = _appCustomSettings.UploadsPath + "/publisherweekly/pdf/" + i.PdffileName
                                             }).Skip((userdto.PageNumber.Value - 1) * userdto.PageSize.Value).Take(userdto.PageSize.Value).ToListAsync();

                        var totalCount = await _context.XsiPublisherWeekly.Where(predicatePublisherWeekly).Select(i => i.ItemId).CountAsync();

                        var dto = new { List, TotalCount = totalCount, Message = "", MessageTypeResponse = "Success" };
                        return Ok(dto);
                    }
                    else
                    {
                        predicatePublisherWeekly = predicatePublisherWeekly.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                        if (!string.IsNullOrEmpty(userdto.CategoryType) && userdto.CategoryType != "-1")
                        {
                            predicatePublisherWeekly = predicatePublisherWeekly.And(i => i.CategoryAr.Equals(userdto.CategoryType));
                        }
                        else
                        {
                            predicatePublisherWeekly = predicatePublisherWeekly.And(i => i.CategoryAr.Equals("1"));
                        }

                        if (!string.IsNullOrEmpty(userdto.IssueNumber) && userdto.IssueNumber != "-1")
                        {
                            userdto.IssueNumber = userdto.IssueNumber.ToLower();
                            var likeExpression = "%" + userdto.IssueNumber + "%";
                            predicatePublisherWeekly = predicatePublisherWeekly.And(i => EF.Functions.Like(i.IssueNumberAr, likeExpression));
                        }

                        if (!string.IsNullOrEmpty(userdto.PublishDate) && userdto.PublishDate != "-1")
                        {
                            predicatePublisherWeekly = predicatePublisherWeekly.And(i => i.PublisherDateAr != null && i.PublisherDateAr.Value.ToString("yyyy-MM-dd") == userdto.PublishDate);
                        }

                        var List = await _context.XsiPublisherWeekly.Where(predicatePublisherWeekly)
                                             .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new PublisherWeeklyDTO()
                                             {
                                                 ItemId = i.ItemId,
                                                 // Title = i.NameAr,
                                                 CategoryType = i.CategoryAr == "1" ? "Digital Copy" : "Hardcover",
                                                 PublishDate = i.PublisherDateAr == null ? string.Empty : i.PublisherDateAr.Value.ToString("yyyy-MM-dd"),
                                                 Thumbnail = _appCustomSettings.UploadsPath + "/publisherweekly/cover/" + (!string.IsNullOrEmpty(i.MagazineCoverAr) ? i.MagazineCoverAr : "PublisherWeekly.jpg"),
                                                 IssueNumber = i.IssueNumberAr,
                                                 URL = _appCustomSettings.UploadsPath + "/publisherweekly/pdf/" + i.PdffileNameAr
                                             }).Skip((userdto.PageNumber.Value - 1) * userdto.PageSize.Value).Take(userdto.PageSize.Value).ToListAsync();

                        var totalCount = await _context.XsiPublisherWeekly.Where(predicatePublisherWeekly).Select(i => i.ItemId).CountAsync();

                        var dto = new { List, TotalCount = totalCount, Message = "", MessageTypeResponse = "Success" };
                        return Ok(dto);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiPublisherWeekly action: {ex.InnerException}");
                return Ok(new { List1, TotalCount = 0, Message = "Exception: Something went wrong. Please retry again later.", MessageTypeResponse = "Error" });
            }
        }
    }
}

﻿using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class SchoolRegistrationController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private readonly IEmailSender _emailSender;
        public SchoolRegistrationController(IOptions<AppCustomSettings> appCustomSettings, IEmailSender emailSender)
        {
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
        }

        // GET: api/SchoolRegistration/GetDateOfVisit
        [HttpGet]
        [Route("GetDateOfVisit")]
        public async Task<ActionResult<List<Dates>>> GetDateOfVisit()
        {
            List<Dates> list = new List<Dates>();
            var predicate = PredicateBuilder.True<XsiExhibition>();
            predicate = predicate.And(i => i.IsArchive == "N");
            predicate = predicate.And(i => i.WebsiteId == 1);
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    predicate = predicate.And(i => i.IsActive == "Y");

                    XsiExhibition entity = _context.XsiExhibition.Where(predicate).FirstOrDefault();

                    if (entity != null)
                    {
                        DateTime start, end;
                        start = entity.StartDate.Value;
                        end = entity.EndDate.Value;
                        do
                        {
                            Dates dt = new Dates() { Date = start.ToString("dd/MM/yyyy"), DateId = start.ToString("MM/dd/yyyy") };
                            list.Add(dt);
                            start = start.AddDays(1);
                        } while (start <= end);
                    }
                }
                else
                {
                    predicate = predicate.And(i => i.IsActiveAr == "Y");
                    XsiExhibition entity = _context.XsiExhibition.Where(predicate).FirstOrDefault();

                    if (entity != null)
                    {
                        DateTime start, end;
                        start = entity.StartDate.Value;
                        end = entity.EndDate.Value;
                        do
                        {
                            Dates dt = new Dates() { Date = start.ToString("dd/MM/yyyy"), DateId = start.ToString("MM/dd/yyyy") };
                            list.Add(dt);
                            start = start.AddDays(1);
                        } while (start <= end);
                    }
                }
                if (list.Count() > 0)
                    list.RemoveAt(0);
                return await list.ToAsyncEnumerable().ToList();
            }
        }

        // GET: api/SchoolRegistration/GetAgeRange
        [HttpGet]
        [Route("GetAgeRange")]
        public async Task<ActionResult<List<DropdownDataDTO>>> GetAgeRange()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                List<DropdownDataDTO> AgeRangeList = new List<DropdownDataDTO>();
                if (LangId == 1)
                    AgeRangeList.Add(new DropdownDataDTO() { ItemId = 0, Title = "Select Age Range" });
                else
                    AgeRangeList.Add(new DropdownDataDTO() { ItemId = 0, Title = "Select Age Range" });

                AgeRangeList.Add(new DropdownDataDTO() { ItemId = 1, Title = "2-10" });
                AgeRangeList.Add(new DropdownDataDTO() { ItemId = 2, Title = "11-15" });
                AgeRangeList.Add(new DropdownDataDTO() { ItemId = 3, Title = "16-18" });
                return await AgeRangeList.ToAsyncEnumerable().ToList();
            }
        }

        // POST: api/SchoolRegistration
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public async Task<ActionResult<SchoolRegistrationDTO>> PostXsiSchoolRegistration(SchoolRegistrationDTO schoolRegistrationDTO)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    EnumResultType XsiResult = EnumResultType.Failed;
                    long schoolTypeId = schoolRegistrationDTO.SchoolTypeId ?? -1;
                    XsiSchoolRegistration entity = new XsiSchoolRegistration();
                    entity.WebsiteId = Convert.ToInt64(EnumWebsiteId.SIBF);
                    entity.ExhibitionId = GetExhibitionId();
                    entity.Name = schoolRegistrationDTO.SchoolName;
                    entity.Type = schoolTypeId;
                    entity.PersonInCharge = schoolRegistrationDTO.PersonInCharge;
                    //  entity.ContactNumber = schoolRegistrationDTO.MobileNo;

                    if (!string.IsNullOrEmpty(schoolRegistrationDTO.MobileISD) && !string.IsNullOrEmpty(schoolRegistrationDTO.MobileSTD) && !string.IsNullOrEmpty(schoolRegistrationDTO.Mobile))
                        entity.ContactNumber = schoolRegistrationDTO.MobileISD + "$" + schoolRegistrationDTO.MobileSTD + "$" + schoolRegistrationDTO.Mobile;

                    entity.Email = schoolRegistrationDTO.Email;
                    entity.Emirates = schoolRegistrationDTO.EmirateId;
                    entity.NumberOfChildren = schoolRegistrationDTO.NumberofStudents;
                    entity.ChildrenAgeRange = schoolRegistrationDTO.AgeRange;
                    if (schoolRegistrationDTO.DateofVisit != null)
                        entity.DateOfVisit = Convert.ToDateTime(schoolRegistrationDTO.DateofVisit);

                    if (schoolRegistrationDTO.CategoryId != null && schoolRegistrationDTO.CategoryId > 0)
                        entity.EventId = schoolRegistrationDTO.CategoryId;

                    entity.ActivityOfInterest = schoolRegistrationDTO.ActivityOfInterest;
                    entity.CreatedOn = MethodFactory.ArabianTimeNow();
                    entity.ModifiedBy = schoolRegistrationDTO.MemberId;
                    entity.ModifedOn = MethodFactory.ArabianTimeNow();
                    _context.XsiSchoolRegistration.Add(entity);
                    await _context.SaveChangesAsync();
                    long itemId = entity.ItemId;
                    if (itemId > 0)
                    {
                        string strEmails = GetEmailByCategory(schoolTypeId, Convert.ToInt64(EnumEmailCategory.SchoolRegistration));

                        if (strEmails != null && !string.IsNullOrWhiteSpace(strEmails))
                        {
                            StringBuilder body = new StringBuilder();
                            body.Append(GetEmailMessageContent(entity));
                            if (schoolRegistrationDTO.SchoolTypeId == 51)
                                XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, strEmails, "Private School Registration Email", body.ToString());
                            else
                                XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, strEmails, "Public School Registration Email", body.ToString());

                            if (LangId == 1)
                            {
                                //   return Ok("Your request has been sent to the concerned department.");
                                return Ok("Thank You, Your visit is approved");
                            }
                            else
                            {
                                // return Ok("تم إرسال طلبك للإدارة المختصة.");
                                return Ok("شكرا لكم، تم تأكيد الزيارة");
                            }

                        }
                        if (LangId == 1)
                        {
                            //   return Ok("Your request has been sent to the concerned department.");
                            return Ok("Thank You, Your visit is approved");
                        }
                        else
                        {
                            // return Ok("تم إرسال طلبك للإدارة المختصة.");
                            return Ok("شكرا لكم، تم تأكيد الزيارة");
                        }
                        // return Ok("Your request has been submitted succesfully.");
                    }
                    return Ok("Registration couldn't be submitted. Please check data and try again.");
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        #region Methods
        StringBuilder GetEmailMessageContent(XsiSchoolRegistration xsiSchoolRegistration)
        {
            StringBuilder body = new StringBuilder();
            body.Append(string.Format("<p>Dear admin user,</p><p>Please be notified that <strong>User: {0}</strong> has sent a School Registration email.</p>", xsiSchoolRegistration.PersonInCharge));
            body.Append("<p><strong>School Registration Details:</strong></p>");
            body.Append(string.Format("<p>School Name : {0}</p>", xsiSchoolRegistration.Name));
            body.Append(string.Format("<p>School Type : {0}</p>", xsiSchoolRegistration.Type == 51 ? "Private schools" : "Public schools"));
            body.Append(string.Format("<p>Phone : {0}</p>", xsiSchoolRegistration.ContactNumber.Replace("$", "")));
            body.Append(string.Format("<p>Email Address : {0}</p>", xsiSchoolRegistration.Email));
            body.Append(string.Format("<p>Emirates : {0}</p>", xsiSchoolRegistration.Emirates));
            body.Append(string.Format("<p>Number of Students : {0}</p>", xsiSchoolRegistration.NumberOfChildren));
            body.Append(string.Format("<p>Students age Ranges : {0}</p>", xsiSchoolRegistration.ChildrenAgeRange));
            if (xsiSchoolRegistration.DateOfVisit != null)
                body.Append(string.Format("<p>Date of Visit : {0}</p>", xsiSchoolRegistration.DateOfVisit));
            body.Append(string.Format("<p>Activity of Interest : {0}</p>", xsiSchoolRegistration.ActivityOfInterest));

            return body;
        }
        string GetEmailByCategory(long itemId, long categoryId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiEmailCategory where = new XsiEmailCategory();

                var predicate = PredicateBuilder.New<XsiEmailCategory>();

                predicate.And(i => i.ItemId == itemId);
                predicate = predicate.And(i => i.CategoryId == categoryId);
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                XsiEmailCategory EmailCatEntity = _context.XsiEmailCategory.Where(predicate).FirstOrDefault();
                if (EmailCatEntity != null)
                    if (EmailCatEntity.EmailId != null)
                        return EmailCatEntity.EmailId;
                return string.Empty;
            }
        }
        long GetExhibitionId()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var predicate = PredicateBuilder.New<XsiExhibition>();

                XsiExhibition where = new XsiExhibition();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.IsArchive == EnumConversion.ToString(EnumBool.No));
                predicate = predicate.And(i => i.WebsiteId == Convert.ToInt64(EnumWebsiteId.SIBF));

                var entity = _context.XsiExhibition.Where(predicate).FirstOrDefault();
                if (entity != null)
                {
                    return entity.ExhibitionId;
                }
                return -1;
            }
        }
        #endregion
    }
}

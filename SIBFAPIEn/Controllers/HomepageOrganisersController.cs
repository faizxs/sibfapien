﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using LinqKit;
using Contracts;
using Microsoft.Extensions.Caching.Memory;
using Xsi.ServicesLayer;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.IO;


namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class HomepageOrganisersController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        private readonly AppCustomSettings _appCustomSettings;
        private ILoggerManager _logger;
        private IMemoryCache _cache;

        public static List<XsiEventSubCategory> cachedEventSubCategory { get; set; }
        public List<XsiEvent> cachedEvents { get; set; }

        public HomepageOrganisersController(IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context, ILoggerManager logger, IMemoryCache memoryCache)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
            _logger = logger;
            _cache = memoryCache;
        }

        // GET: api/XsiHomepageOrganiser
        [HttpGet]
        public async Task<ActionResult<IEnumerable<HomepageOrganiserDTO>>> GetXsiHomepageOrganiser()
        {
            List<XsiHomepageOrganiser> List = new List<XsiHomepageOrganiser>();
            HomepageOrganiserItemDTO dto = new HomepageOrganiserItemDTO();
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            List = await _context.XsiHomepageOrganiser.AsNoTracking().ToListAsync();
            if (LangId == 1)
            {
                foreach (var item in List)
                {
                    switch (item.ItemId)
                    {
                        case 1:
                            dto.HomeBannerSectionIsActive = item.IsActive == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 3:
                            dto.ActivitiesSectionIsActive = item.IsActive == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 5:
                            dto.CounterSectionIsActive = item.IsActive == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 7:
                            dto.ExhibitorsSectionIsActive = item.IsActive == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 9:
                            dto.BooksSearchIsActive = item.IsActive == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 11:
                            dto.LatestNewsSectionIsActive = item.IsActive == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 13:
                            dto.GuestSectionIsActive = item.IsActive == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 15:
                            dto.SponsorSectionIsActive = item.IsActive == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 17:
                            dto.TabSectionIsActive = item.IsActive == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 18:
                            dto.VipGuestSectionIsActive = item.IsActive == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                    }
                }
            }
            else
            {
                foreach (var item in List)
                {
                    switch (item.ItemId)
                    {
                        case 1:
                            dto.HomeBannerSectionIsActive = item.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 3:
                            dto.ActivitiesSectionIsActive = item.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 5:
                            dto.CounterSectionIsActive = item.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 7:
                            dto.ExhibitorsSectionIsActive = item.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 9:
                            dto.BooksSearchIsActive = item.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 11:
                            dto.LatestNewsSectionIsActive = item.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 13:
                            dto.GuestSectionIsActive = item.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 15:
                            dto.SponsorSectionIsActive = item.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 17:
                            dto.TabSectionIsActive = item.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                        case 18:
                            dto.VipGuestSectionIsActive = item.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                            break;
                    }
                }
            }
            return Ok(dto);
        }

        [HttpGet]
        [Route("HomepageBanner")]
        public async Task<ActionResult<IEnumerable<HomepageBannerDTO>>> GetXsiHomepageBannerNew()
        {
            if (MethodFactory.iSHomepageSectionAllowed(Convert.ToInt64(EnumHomePageSection.SectionOne)))
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var List = await _context.XsiHomepageBannerNew.Where(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.SortOrder).Select(i => new HomepageBannerDTO()
                    {
                        ItemId = i.ItemId,
                        Cmstitle = i.Cmstitle,
                        Title = i.Title,
                        SubTitle = i.SubTitle,
                        SubTitleTwo = (i.ItemId == 59) ? "Opening Hours" : string.Empty,
                        SubTitleThree = (i.ItemId == 59) ? "Sunday – Wednesday: 10:00 – 22:00, Thursday - Saturday: 10:00 – 23:00, Fridays: 16:00-23:00" : string.Empty,
                        HomeBanner = _appCustomSettings.UploadsPath + "/HomepageBannerNew/" + i.HomeBanner,
                        Url = i.Url,
                        IsFeatured = i.IsFeatured,
                        Color = i.Color,
                        SortOrder = i.SortOrder
                    }).AsNoTracking().ToListAsync();
                    return List;
                    //return await List.ToListAsync();
                }
                else
                {
                    var List = await _context.XsiHomepageBannerNew.Where(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.SortOrder).Select(i => new HomepageBannerDTO()
                    {
                        ItemId = i.ItemId,
                        Cmstitle = i.CmstitleAr,
                        Title = i.TitleAr,
                        SubTitle = i.SubTitleAr,
                        SubTitleTwo = (i.ItemId == 59) ? "ساعات العمل" : string.Empty,
                        SubTitleThree = (i.ItemId == 59) ? "من الإثنين إلى الأربعاء من 10:00 ولغاية 22:00، أيام الخميس، السبت والأحد من 10:00 ولغاية 23:00، ما عدا يوم الجمعة من 16:00 إلى 23:00" : string.Empty,
                        HomeBanner = _appCustomSettings.UploadsPath + "/HomepageBannerNew/" + i.HomeBannerAr,
                        Url = i.Urlar,
                        IsFeatured = i.IsFeatured,
                        Color = i.ColorAr,
                        SortOrder = i.SortOrder
                    }).AsNoTracking().ToListAsync();
                    return List;
                   // return await List.ToListAsync();
                }
            }
            return null;

        }
        // GET: api/HomepageSubBanner
        [HttpGet]
        [Route("HomepageSubBanner")]
        public ActionResult<List<HomepageSubBannerNewDTO>> GetXsiHomepageSubBanner()
        {
            List<HomepageSubBannerNewDTO> SubBannerList = new List<HomepageSubBannerNewDTO>();

            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var SubBannerEntity = _context.XsiHomepageSubBanner.Where(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes)).AsNoTracking().OrderBy(i => i.ItemId).FirstOrDefault();

                if (SubBannerEntity != null)
                {
                    if (!string.IsNullOrEmpty(SubBannerEntity.HomeBanner))
                    {
                        HomepageSubBannerNewDTO obj = new HomepageSubBannerNewDTO();
                        obj.Title = SubBannerEntity.Title;
                        obj.SubTitle = SubBannerEntity.SubTitle;
                        if (!string.IsNullOrEmpty(SubBannerEntity.HomeBanner))
                            obj.Image = _appCustomSettings.UploadsPath + "/HomepageSubBanner/" + SubBannerEntity.HomeBanner;

                        obj.Url = SubBannerEntity.Url;
                        obj.Color = SubBannerEntity.Color;
                        SubBannerList.Add(obj);
                    }

                    if (!string.IsNullOrEmpty(SubBannerEntity.HomeBanner2) && SubBannerEntity.IsActive2 == EnumConversion.ToString(EnumBool.Yes))
                    {
                        HomepageSubBannerNewDTO obj = new HomepageSubBannerNewDTO();
                        obj.Title = SubBannerEntity.Title2;
                        obj.SubTitle = SubBannerEntity.SubTitle2;
                        if (!string.IsNullOrEmpty(SubBannerEntity.HomeBanner2))
                            obj.Image = _appCustomSettings.UploadsPath + "/HomepageSubBanner/" + SubBannerEntity.HomeBanner2;

                        obj.Url = SubBannerEntity.Url2;
                        obj.Color = SubBannerEntity.Color2;
                        SubBannerList.Add(obj);
                    }
                }
                return Ok(SubBannerList);
            }
            else
            {
                var SubBannerEntity = _context.XsiHomepageSubBanner.Where(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).AsNoTracking().OrderBy(i => i.ItemId).FirstOrDefault();

                if (SubBannerEntity != null)
                {
                    if (!string.IsNullOrEmpty(SubBannerEntity.HomeBannerAr))
                    {
                        HomepageSubBannerNewDTO obj = new HomepageSubBannerNewDTO();
                        obj.Title = SubBannerEntity.TitleAr;
                        obj.SubTitle = SubBannerEntity.SubTitleAr;
                        if (!string.IsNullOrEmpty(SubBannerEntity.HomeBannerAr))
                            obj.Image = _appCustomSettings.UploadsPath + "/HomepageSubBanner/" + SubBannerEntity.HomeBannerAr;

                        obj.Url = SubBannerEntity.Urlar;
                        obj.Color = SubBannerEntity.ColorAr;
                        SubBannerList.Add(obj);
                    }

                    if (!string.IsNullOrEmpty(SubBannerEntity.HomeBannerAr2) && SubBannerEntity.IsActiveAr2 == EnumConversion.ToString(EnumBool.Yes))
                    {
                        HomepageSubBannerNewDTO obj = new HomepageSubBannerNewDTO();
                        obj.Title = SubBannerEntity.TitleAr2;
                        obj.SubTitle = SubBannerEntity.SubTitleAr2;
                        if (!string.IsNullOrEmpty(SubBannerEntity.HomeBannerAr2))
                            obj.Image = _appCustomSettings.UploadsPath + "/HomepageSubBanner/" + SubBannerEntity.HomeBannerAr2;

                        obj.Url = SubBannerEntity.Urlar2;
                        obj.Color = SubBannerEntity.ColorAr2;
                        SubBannerList.Add(obj);
                    }
                }
                return Ok(SubBannerList);
            }
        }

        // GET: api/HomepageSectionTwo
        [HttpGet]
        [Route("HomepageSectionTwo")]
        public async Task<ActionResult<IEnumerable<HomepageSectionTwoDTO>>> GetXsiHomepageSectionTwo()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var List = _context.XsiHomepageSectionTwo.Where(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.ItemId).AsNoTracking().Select(i => new HomepageSectionTwoDTO()
                {
                    ItemId = i.ItemId,
                    CategoryId = i.CategoryId,
                    // CategoryTitle = i.Category.Title,
                    Title = i.Title,
                    Thumbnail = _appCustomSettings.UploadsPath + "/HomepageSectionTwo/Thumbnail/" + i.Thumbnail,
                    Color = i.Color,
                    Url = i.Url,
                    ActivityCount = GetActivityCount(i.Url, LangId)
                });
                return await List.ToListAsync();
            }
            else
            {
                var List = _context.XsiHomepageSectionTwo.Where(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.ItemId).AsNoTracking().Select(i => new HomepageSectionTwoDTO()
                {
                    ItemId = i.ItemId,
                    CategoryId = i.CategoryId,
                    // CategoryTitle = i.Category.Title,
                    Title = i.TitleAr,
                    Thumbnail = _appCustomSettings.UploadsPath + "/HomepageSectionTwo/Thumbnail/" + i.ThumbnailAr,
                    Color = i.ColorAr,
                    Url = i.Urlar,
                    ActivityCount = GetActivityCount(i.Url, LangId)
                });
                return await List.ToListAsync();
            }
        }

        // GET: api/HomepageSectionFour
        [HttpGet]
        [Route("HomepageSectionFour")]
        public async Task<ActionResult<IEnumerable<HomepageSectionFourDTO>>> GetXsiHomepageSectionFour()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var List = _context.XsiHomepageSectionFour.Where(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes)).AsNoTracking().OrderBy(i => i.ItemId).Select(i => new HomepageSectionFourDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.Title,
                    ViewAllUrl = i.ViewAllUrl,
                    SubSectionOneTitle = i.SubSectionOneTitle,
                    SubSectionOneOverview = i.SubSectionOneOverview,
                    SubSectionOneImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + i.SubSectionOneImage,
                    SubSectionOneUrl = i.SubSectionOneUrl,
                    SubSectionTwoTitle = i.SubSectionTwoTitle,
                    SubSectionTwoOverview = i.SubSectionTwoOverview,
                    SubSectionTwoImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + i.SubSectionTwoImage,
                    SubSectionTwoUrl = i.SubSectionTwoUrl,
                    SubSectionThreeTitle = i.SubSectionThreeTitle,
                    SubSectionThreeOverview = i.SubSectionThreeOverview,
                    SubSectionThreeImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + i.SubSectionThreeImage,
                    SubSectionThreeUrl = i.SubSectionThreeUrl
                });
                return await List.ToListAsync();
            }
            else
            {
                var List = _context.XsiHomepageSectionFour.Where(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).AsNoTracking().OrderBy(i => i.ItemId).Select(i => new HomepageSectionFourDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.TitleAr,
                    ViewAllUrl = i.ViewAllUrlar,
                    SubSectionOneTitle = i.SubSectionOneTitleAr,
                    SubSectionOneOverview = i.SubSectionOneOverviewAr,
                    SubSectionOneImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + i.SubSectionOneImageAr,
                    SubSectionOneUrl = i.SubSectionOneUrlar,
                    SubSectionTwoTitle = i.SubSectionTwoTitleAr,
                    SubSectionTwoOverview = i.SubSectionTwoOverviewAr,
                    SubSectionTwoImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + i.SubSectionTwoImageAr,
                    SubSectionTwoUrl = i.SubSectionTwoUrlar,
                    SubSectionThreeTitle = i.SubSectionThreeTitleAr,
                    SubSectionThreeOverview = i.SubSectionThreeOverviewAr,
                    SubSectionThreeImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + i.SubSectionThreeImageAr,
                    SubSectionThreeUrl = i.SubSectionThreeUrlar
                });
                return await List.ToListAsync();
            }
        }


        // GET: api/News
        [HttpGet]
        [Route("News")]
        public async Task<ActionResult<dynamic>> GetXsiNewsForHomepage()
        {
            long id = Convert.ToInt64(EnumHomePageSection.SectionSix);
            bool isallowed = MethodFactory.iSHomepageSectionAllowed(id);
            if (isallowed)
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                bool IsGenerate = false;
                bool.TryParse(Request.Headers["IsGenerate"], out IsGenerate);
                string pathen = _appCustomSettings.UploadsPhysicalPath + "\\JsonData\\NewsHomePage.json";
                string pathar = _appCustomSettings.UploadsPhysicalPath + "\\JsonData\\NewsHomePageAr.json";

                if (LangId == 1)
                {
                    FileInfo fi = new FileInfo(pathen);
                    var hours = (fi.CreationTime - MethodFactory.ArabianTimeNow()).Hours;
                    if (IsGenerate || hours > 5 || !fi.Exists)
                    {
                        List<HomepageNewsDto> List = _context.XsiNews.Where(i => i.WebsiteId != 2 && i.IsActive == "Y").OrderByDescending(o => o.Dated).ThenByDescending(o => o.ItemId).Take(6).AsNoTracking().Select(i => new HomepageNewsDto()
                        {
                            ItemId = i.ItemId,
                            Title = i.Title,
                            Dated = GetNewsDate(i.Dated, LangId),
                            ImageName = _appCustomSettings.UploadsPath + "/NewsNew/" + i.ImageName,
                        }).AsNoTracking().ToList();

                        System.IO.File.WriteAllText(pathen, JsonConvert.SerializeObject(List));

                        List<HomepageNewsDto> jsonfiledata = JsonConvert.DeserializeObject<List<HomepageNewsDto>>(System.IO.File.ReadAllText(pathen));
                        using (StreamReader file = System.IO.File.OpenText(pathen))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomepageNewsDto> jsonList = (List<HomepageNewsDto>)serializer.Deserialize(file, typeof(List<HomepageNewsDto>));
                            return jsonList;
                        }
                    }
                    else
                    {
                        List<HomepageNewsDto> jsonfiledata = JsonConvert.DeserializeObject<List<HomepageNewsDto>>(System.IO.File.ReadAllText(pathen));
                        using (StreamReader file = System.IO.File.OpenText(pathen))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomepageNewsDto> jsonList = (List<HomepageNewsDto>)serializer.Deserialize(file, typeof(List<HomepageNewsDto>));
                            return jsonList;
                        }
                    }
                }
                else
                {

                    FileInfo fi = new FileInfo(pathar);

                    var hours = (fi.CreationTime - MethodFactory.ArabianTimeNow()).Hours;
                    if (IsGenerate || hours > 5 || !fi.Exists)
                    {
                        var List = _context.XsiNews.Where(i => i.WebsiteId != 2 && i.IsActiveAr == "Y")
                           .OrderByDescending(o => o.DatedAr).ThenByDescending(o => o.ItemId).Take(6).AsNoTracking().Select(i => new
                           {
                               ItemId = i.ItemId,
                               Title = i.TitleAr,
                               Dated = GetNewsDate(i.DatedAr, LangId),
                               ImageName = _appCustomSettings.UploadsPath + "/NewsNew/" + i.ImageNameAr,
                           });

                        System.IO.File.WriteAllText(pathar, JsonConvert.SerializeObject(List));

                        List<HomepageNewsDto> jsonfiledata = JsonConvert.DeserializeObject<List<HomepageNewsDto>>(System.IO.File.ReadAllText(pathar));
                        using (StreamReader file = System.IO.File.OpenText(pathar))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomepageNewsDto> jsonList = (List<HomepageNewsDto>)serializer.Deserialize(file, typeof(List<HomepageNewsDto>));
                            return jsonList;
                        }
                        //  return await List.ToListAsync();
                    }
                    else
                    {
                        List<HomepageNewsDto> jsonfiledata = JsonConvert.DeserializeObject<List<HomepageNewsDto>>(System.IO.File.ReadAllText(pathar));
                        using (StreamReader file = System.IO.File.OpenText(pathar))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomepageNewsDto> jsonList = (List<HomepageNewsDto>)serializer.Deserialize(file, typeof(List<HomepageNewsDto>));
                            return jsonList;
                        }
                    }
                }
            }
            return null;
        }

        // GET: api/AwardList/Home
        [HttpGet]
        [Route("AwardList")]
        public async Task<ActionResult<IEnumerable<AwardsDropdownDataDTO>>> GetXsiAwardNewListForHome()
        {
            List<AwardsDropdownDataDTO> AwardNewList = new List<AwardsDropdownDataDTO>();
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                List<AwardsDropdownDataDTO> AwardList = new List<AwardsDropdownDataDTO>();
                long websiteId = 1;
                var predicateExhibition = PredicateBuilder.True<XsiExhibition>();
                predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicateExhibition = predicateExhibition.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
                predicateExhibition = predicateExhibition.And(i => i.WebsiteId == websiteId);

                XsiExhibition exhibitionentity = MethodFactory.GetActiveExhibitionNew(websiteId, _cache); //_context.XsiExhibition.Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                if (exhibitionentity != null)
                {
                    if (LangId == 1)
                    {
                        var predicate = PredicateBuilder.True<XsiAwardNew>();
                        predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicate = predicate.And(i => (i.WebsiteId == 0 || i.WebsiteId == 1));

                        AwardNewList = await _context.XsiAwardNew.Where(predicate).AsNoTracking().Select(x => new AwardsDropdownDataDTO()
                        {
                            ItemId = x.ItemId,
                            Title = x.Name,
                            RegistrationStartDate = exhibitionentity.AwardStartDate.Value.ToString("MMMM d,yyyy"),
                            RegistrationEndDate = exhibitionentity.AwardEndDate.Value.ToString("MMMM d,yyyy"),
                            RegistrationDeadline = exhibitionentity.AwardEndDate.Value.ToString("MMMM d,yyyy"),
                        }).ToListAsync();

                        var awardpredicate = PredicateBuilder.True<XsiAwards>();
                        awardpredicate = awardpredicate.And(a => a.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        awardpredicate = awardpredicate.And(a => (a.ItemId == 1 || a.ItemId == 9 || a.ItemId == 17));
                        awardpredicate = awardpredicate.And(a => (a.WebsiteId == 0 || a.WebsiteId == 1));
                        AwardList = await _context.XsiAwards.Where(awardpredicate).AsNoTracking()
                            .Select(x => new AwardsDropdownDataDTO()
                            {
                                ItemId = x.ItemId,
                                Title = x.Name,
                                RegistrationStartDate = exhibitionentity.AwardStartDate.Value.ToString("MMMM d,yyyy"),
                                RegistrationEndDate = exhibitionentity.AwardEndDate.Value.ToString("MMMM d,yyyy"),
                                RegistrationDeadline = exhibitionentity.AwardEndDate.Value.ToString("MMMM d,yyyy"),
                            }).ToListAsync();

                        foreach (var award in AwardList.Where(i => i.ItemId == 17))
                        {
                            if (award.ItemId == 17)
                            {
                                award.RegistrationStartDate = exhibitionentity.TurjumanStartDate.Value.ToString("MMMM d,yyyy");
                                award.RegistrationEndDate = exhibitionentity.TurjumanEndDate.Value.ToString("MMMM d,yyyy");
                                award.RegistrationDeadline = exhibitionentity.TurjumanEndDate.Value.ToString("MMMM d,yyyy");
                            }
                        }
                        AwardNewList.AddRange(AwardList);
                        return Ok(AwardNewList.OrderBy(i => i.Title));
                    }
                    else
                    {
                        var predicate = PredicateBuilder.True<XsiAwardNew>();
                        predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                        predicate = predicate.And(i => (i.WebsiteId == 0 || i.WebsiteId == 1));

                        AwardNewList = await _context.XsiAwardNew.Where(predicate).AsNoTracking().Select(x => new AwardsDropdownDataDTO()
                        {
                            ItemId = x.ItemId,
                            Title = x.NameAr,
                            RegistrationStartDate = exhibitionentity.AwardStartDate.Value.ToString("MMMM d,yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")),
                            RegistrationEndDate = exhibitionentity.AwardEndDate.Value.ToString("MMMM d,yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")),
                            RegistrationDeadline = exhibitionentity.AwardEndDate.Value.ToString("MMMM d,yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")),
                        }).ToListAsync();

                        var awardpredicate = PredicateBuilder.True<XsiAwards>();
                        awardpredicate = awardpredicate.And(a => a.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                        awardpredicate = awardpredicate.And(a => (a.ItemId == 1 || a.ItemId == 9 || a.ItemId == 17));
                        awardpredicate = awardpredicate.And(a => (a.WebsiteId == 0 || a.WebsiteId == 1));
                        AwardList = await _context.XsiAwards.Where(awardpredicate).AsNoTracking().Select(x => new AwardsDropdownDataDTO()
                        {
                            ItemId = x.ItemId,
                            Title = x.NameAr,
                            RegistrationStartDate = exhibitionentity.AwardStartDate.Value.ToString("MMMM d,yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")),
                            RegistrationEndDate = exhibitionentity.AwardEndDate.Value.ToString("MMMM d,yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")),
                            RegistrationDeadline = exhibitionentity.AwardEndDate.Value.ToString("MMMM d,yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")),
                        }).ToListAsync();

                        foreach (var award in AwardList.Where(i => i.ItemId == 17))
                        {
                            if (award.ItemId == 17)
                            {
                                award.RegistrationStartDate = exhibitionentity.TurjumanStartDate.Value.ToString("MMMM d,yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
                                award.RegistrationEndDate = exhibitionentity.TurjumanEndDate.Value.ToString("MMMM d,yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
                                award.RegistrationDeadline = exhibitionentity.TurjumanEndDate.Value.ToString("MMMM d,yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
                            }
                        }
                        AwardNewList.AddRange(AwardList);
                        return Ok(AwardNewList.OrderBy(i => i.Title));
                    }
                }
                return Ok(AwardNewList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwardNewsListById action: {ex.InnerException}");
                return Ok(AwardNewList);
                // return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [Route("TranslationGrant/{websiteid}")]
        public ActionResult<TranslationGrantHomeDto> GetTranslationGrantDatesForHome(long websiteid)
        {
            TranslationGrantHomeDto dto = new TranslationGrantHomeDto();
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);


                using (TranslationGrantService TranslationGrantService = new TranslationGrantService())
                {
                    //, IsArchive = "N"
                    ExhibitionService ExhibitionService = new ExhibitionService();
                    var exhibitionEntity = ExhibitionService.GetExhibition(new XsiExhibition() { WebsiteId = websiteid, IsActive = "Y" }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                    if (exhibitionEntity != null)
                    {
                        XsiExhibitionTranslationGrant where = new XsiExhibitionTranslationGrant();
                        where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        where.IsArchive = EnumConversion.ToString(EnumBool.No);
                        where.ExhibitionId = exhibitionEntity.ExhibitionId;

                        XsiExhibitionTranslationGrant tgentity = TranslationGrantService.GetTranslationGrant(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                        if (tgentity != null)
                        {
                            dto.Id = tgentity.ItemId;
                            if (LangId == 1)
                            {

                                if (tgentity.Phase1StartDate != null)
                                    dto.StartDate = tgentity.Phase1StartDate.Value.ToString("dd MMMM yyyy");
                                if (tgentity.Phase1StartDate != null)
                                    dto.EndDate = tgentity.Phase1EndDate.Value.ToString("dd MMMM yyyy");
                            }
                            else
                            {
                                if (tgentity.Phase1StartDate != null)
                                    dto.StartDate = tgentity.Phase1StartDate.Value.ToString("dd MMMM yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
                                if (tgentity.Phase1StartDate != null)
                                    dto.EndDate = tgentity.Phase1EndDate.Value.ToString("dd MMMM yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
                            }
                            dto.Url = "Translationgrant?websiteId=" + websiteid + "&id=" + tgentity.ItemId;
                        }
                        return Ok(dto);
                    }
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetTranslationGrantDatesForHome action: {ex.InnerException}");
                // return StatusCode(500, "Internal server error " + ex.InnerException);
                return Ok(dto);
                //return Ok(new TGMessageDTO() { Message = "Something went wrong. Please try again later. ", MessageTypeResponse = "Error" });
            }
        }
        // GET: api/Sponsors
        [HttpGet]
        [Route("Sponsors")]
        public async Task<ActionResult<dynamic>> GetXsiIconsForSponsor()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var List = _context.XsiIconsForSponsor.Where(i => i.IsActive == "Y").AsQueryable()
                                 .OrderBy(o => o.CreatedOn).AsNoTracking().Select(i => new
                                 {
                                     ItemId = i.ItemId,
                                     Title = i.Title,
                                     Overview = i.Overview,
                                     ImageName = _appCustomSettings.UploadsPath + "/IconsForSponsor/" + i.ImageName,
                                 }).ToListAsync();
                return await List;
            }
            else
            {
                var List = _context.XsiIconsForSponsor.Where(i => i.IsActiveAr == "Y").AsQueryable()
                                   .OrderBy(o => o.CreatedOn).AsNoTracking().Select(i => new
                                   {
                                       ItemId = i.ItemId,
                                       Title = i.TitleAr,
                                       Overview = i.OverviewAr,
                                       ImageName = _appCustomSettings.UploadsPath + "/IconsForSponsor/" + i.ImageNameAr,
                                   }).ToListAsync();
                return await List;
            }
        }

        // GET: api/homepageevent
        [HttpGet]
        [Route("homepageevent")]
        public async Task<ActionResult<dynamic>> GetEventForHomepage()
        {
            try
            {
                DateTime dt = MethodFactory.ArabianTimeNow().Date;
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long websiteid = 1;
                bool IsGenerate = false;
                bool.TryParse(Request.Headers["IsGenerate"], out IsGenerate);

                string pathen = _appCustomSettings.UploadsPhysicalPath + "\\JsonData\\EventHomePage.json";
                string pathar = _appCustomSettings.UploadsPhysicalPath + "\\JsonData\\EventHomePageAr.json";
                if (LangId == 1)
                {
                    FileInfo fi = new FileInfo(pathen);

                    var hours = (fi.CreationTime - MethodFactory.ArabianTimeNow()).Hours;
                    if (IsGenerate || hours > 5 || !fi.Exists)
                    {
                        #region Search Parameters
                        List<long> categoryIDs = new List<long>();
                        List<long> subCategoryIDs = new List<long>();

                        var predicateCategory = PredicateBuilder.True<XsiEventCategory>();
                        var predicateSubCategory = PredicateBuilder.True<XsiEventSubCategory>();
                        var predicateEvent = PredicateBuilder.True<XsiEvent>();
                        var predicateEventDate = PredicateBuilder.True<XsiEventDate>();

                        predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateCategory = predicateCategory.And(i => i.WebsiteId == websiteid || i.WebsiteId == 0);

                        categoryIDs = _context.XsiEventCategory.Where(predicateCategory).Select(p => p.ItemId).ToList();

                        predicateSubCategory = predicateSubCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateSubCategory = predicateSubCategory.And(i => categoryIDs.Contains(i.EventCategoryId.Value));
                        predicateSubCategory = predicateSubCategory.And(i => i.WebsiteId == websiteid || i.WebsiteId == 0);
                        subCategoryIDs = _context.XsiEventSubCategory.Where(predicateSubCategory).Select(p => p.ItemId).ToList();

                        predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");

                        if (subCategoryIDs.Count > 0)
                            predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));

                        var predicateExhibition = PredicateBuilder.True<XsiExhibition>();
                        predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == websiteid);
                        XsiExhibition exhibition = _context.XsiExhibition.Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                        if (exhibition != null)
                        {
                            //predicateEventDate = predicateEventDate.And(i =>
                            //    i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate);
                            //predicateEventDate = predicateEventDate.And(i => (i.StartDate.Value.Date >= dt.Date));

                            var dtNow = DateTime.Now.Date;
                            predicateEventDate = predicateEventDate.And(i => i.StartDate >= dtNow && dtNow <= i.EndDate);
                        }
                        #endregion
                        var EventList = await _context.XsiEventDate.Where(predicateEventDate).OrderBy(i => i.StartDate)
                      .Join(_context.XsiEvent.Where(predicateEvent)
                          , ed => ed.EventId
                          , ev => ev.ItemId
                          , (ed, ev) => new HomeEventDTO
                          {
                              EventDateId = ed.ItemId,
                              Title = ev.Title,
                              Overview = ev.Overview,
                              SubCatId = ev.EventSubCategoryId.Value,
                              EventDate = GetEventDateHome(ed.StartDate, ed.EndDate, LangId)
                          }).Take(3).ToListAsync();
                        System.IO.File.WriteAllText(pathen, JsonConvert.SerializeObject(EventList));
                        List<HomeEventDTO> jsonfiledata = JsonConvert.DeserializeObject<List<HomeEventDTO>>(System.IO.File.ReadAllText(pathen));
                        using (StreamReader file = System.IO.File.OpenText(pathen))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomeEventDTO> List = (List<HomeEventDTO>)serializer.Deserialize(file, typeof(List<HomeEventDTO>));
                            var dto = new { List };
                            return Ok(dto);
                        }
                    }
                    else
                    {
                        List<HomeEventDTO> jsonfiledata = JsonConvert.DeserializeObject<List<HomeEventDTO>>(System.IO.File.ReadAllText(pathen));
                        using (StreamReader file = System.IO.File.OpenText(pathen))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomeEventDTO> List = (List<HomeEventDTO>)serializer.Deserialize(file, typeof(List<HomeEventDTO>));
                            var dto = new { List };
                            return Ok(dto);
                        }
                    }
                }
                else
                {
                    FileInfo fi = new FileInfo(pathar);

                    var hours = (fi.CreationTime - MethodFactory.ArabianTimeNow()).Hours;
                    if (IsGenerate || hours > 5 || !fi.Exists)
                    {
                        #region Search Parameters
                        List<long> categoryIDs = new List<long>();
                        List<long> subCategoryIDs = new List<long>();

                        var predicateCategory = PredicateBuilder.True<XsiEventCategory>();
                        var predicateSubCategory = PredicateBuilder.True<XsiEventSubCategory>();
                        var predicateEvent = PredicateBuilder.True<XsiEvent>();
                        var predicateEventDate = PredicateBuilder.True<XsiEventDate>();

                        predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateCategory = predicateCategory.And(i => i.WebsiteId == websiteid || i.WebsiteId == 0);

                        categoryIDs = _context.XsiEventCategory.Where(predicateCategory).Select(p => p.ItemId).ToList();

                        predicateSubCategory = predicateSubCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateSubCategory = predicateSubCategory.And(i => categoryIDs.Contains(i.EventCategoryId.Value));
                        predicateSubCategory = predicateSubCategory.And(i => i.WebsiteId == websiteid || i.WebsiteId == 0);
                        subCategoryIDs = _context.XsiEventSubCategory.Where(predicateSubCategory).Select(p => p.ItemId).ToList();

                        predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");

                        if (subCategoryIDs.Count > 0)
                            predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));

                        var predicateExhibition = PredicateBuilder.True<XsiExhibition>();
                        predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == websiteid);
                        XsiExhibition exhibition = _context.XsiExhibition.OrderByDescending(i => i.ExhibitionId).Where(predicateExhibition).FirstOrDefault();

                        if (exhibition != null)
                        {
                            ////predicateEventDate = predicateEventDate.And(i =>
                            ////    i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate);
                            // predicateEventDate = predicateEventDate.And(i => (i.StartDate.Value.Date >= dt.Date));

                            var dtNow = DateTime.Now.Date;
                            predicateEventDate = predicateEventDate.And(i => i.StartDate >= dtNow && dtNow <= i.EndDate);
                        }
                        #endregion
                        var EventList = await _context.XsiEventDate.Where(predicateEventDate).OrderBy(i => i.StartDate)
                   .Join(_context.XsiEvent.Where(predicateEvent)
                       , ed => ed.EventId
                       , ev => ev.ItemId
                       , (ed, ev) => new HomeEventDTO
                       {
                           EventDateId = ed.ItemId,
                           Title = ev.TitleAr,
                           Overview = ev.OverviewAr,
                           SubCatId = ev.EventSubCategoryId.Value,
                           EventDate = GetEventDateHome(ed.StartDate, ed.EndDate, LangId)
                       }).Take(3).ToListAsync();

                        System.IO.File.WriteAllText(pathar, JsonConvert.SerializeObject(EventList));
                        List<HomeEventDTO> jsonfiledata = JsonConvert.DeserializeObject<List<HomeEventDTO>>(System.IO.File.ReadAllText(pathar));
                        using (StreamReader file = System.IO.File.OpenText(pathar))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomeEventDTO> List = (List<HomeEventDTO>)serializer.Deserialize(file, typeof(List<HomeEventDTO>));
                            var dto = new { List };
                            return Ok(dto);
                        }
                    }
                    else
                    {
                        List<HomeEventDTO> jsonfiledata = JsonConvert.DeserializeObject<List<HomeEventDTO>>(System.IO.File.ReadAllText(pathar));
                        using (StreamReader file = System.IO.File.OpenText(pathar))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomeEventDTO> List = (List<HomeEventDTO>)serializer.Deserialize(file, typeof(List<HomeEventDTO>));
                            var dto = new { List };
                            return Ok(dto);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside homepageevent action: {ex.InnerException}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        // GET: api/VIPGuest
        [HttpGet]
        [Route("VIPGuest/{pageNumber}/{pageSize}/{strSearch}/{strtitle}/{strletter}")]
        public async Task<ActionResult<dynamic>> GetXsiVipGuest(int? pageNumber = 1, int? pageSize = 15, string strSearch = "", string strtitle = "", string strletter = "")
        {
            var List1 = new List<XsiVipguest>();
            try
            {
                VIPGuestService vIPGuestService = new VIPGuestService();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var predicateVipGuest = PredicateBuilder.True<XsiVipguest>();
                var predicateVipGuestWebsite = PredicateBuilder.True<XsiVipguestWebsite>();
                predicateVipGuestWebsite = predicateVipGuestWebsite.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);
                var guestIDs = _context.XsiVipguestWebsite.Where(predicateVipGuestWebsite).AsNoTracking().Select(i => i.VipguestId).Distinct()
                    .ToList();
                if (guestIDs.Count > 0)
                    predicateVipGuest = predicateVipGuest.And(i => guestIDs.Contains(i.ItemId));
                if (LangId == 1)
                {
                    predicateVipGuest = predicateVipGuest.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    if (!string.IsNullOrEmpty(strletter) && strletter != "-1")
                    {
                        strletter = strletter.ToLower();
                        predicateVipGuest = predicateVipGuest.And(i => i.Name.StartsWith(strletter));
                    }
                    else if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                    {
                        strSearch = strSearch.ToLower();
                        predicateVipGuest = predicateVipGuest.And(i => i.Name.Contains(strSearch, StringComparison.OrdinalIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(strtitle) && strtitle != "-1")
                    {
                        strtitle = strtitle.ToLower();
                        predicateVipGuest = predicateVipGuest.And(i => i.Title.Contains(strtitle, StringComparison.OrdinalIgnoreCase));
                    }

                    var List = await _context.XsiVipguest.Where(predicateVipGuest)
                                         .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).AsNoTracking().Select(i => new VIPGuestDTO()
                                         {
                                             ItemId = i.ItemId,
                                             GuestTitle = i.Title,
                                             GuestPhoto = _appCustomSettings.UploadsPath + "/VIPGuest/ListThumbnailNew/" + (i.Image ?? "guest.jpg"),
                                             GuestName = i.Name,
                                             //Description = i.Description,
                                             //  URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                                         }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    var totalCount = await _context.XsiVipguest.Where(predicateVipGuest).Select(i => i.ItemId).CountAsync();

                    var dto = new { List, TotalCount = totalCount, Message = "", MessageTypeResponse = "Success" };
                    return Ok(dto);
                }
                else
                {
                    predicateVipGuest = predicateVipGuest.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    if (!string.IsNullOrEmpty(strletter) && strletter != "-1")
                    {
                        //strletter = strletter.ToLower();
                        //predicateVipGuest = predicateVipGuest.And(i => i.NameAr.StartsWith(strletter));

                        var likeExpression = strletter + "%";
                        predicateVipGuest = predicateVipGuest.And(i => EF.Functions.Like(i.NameAr, likeExpression));
                    }
                    else if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                    {
                        strSearch = strSearch.ToLower();
                        predicateVipGuest = predicateVipGuest.And(i => i.NameAr.Contains(strSearch, StringComparison.OrdinalIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(strtitle) && strtitle != "-1")
                    {
                        strtitle = strtitle.ToLower();
                        predicateVipGuest = predicateVipGuest.And(i => i.TitleAr.Contains(strtitle, StringComparison.OrdinalIgnoreCase));
                    }

                    var List = await _context.XsiVipguest.AsExpandable().Where(predicateVipGuest)
                        .OrderByDescending(o => o.ItemId).Select(i => new VIPGuestDTO()
                        {
                            ItemId = i.ItemId,
                            GuestTitle = i.TitleAr,
                            GuestPhoto = _appCustomSettings.UploadsPath + "/VIPGuest/ListThumbnailNew/" + (i.ImageAr ?? "guest.jpg"),
                            GuestName = i.NameAr,
                            //  Events = "",
                            //Description = i.DescriptionAr,
                            // URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                        }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    var totalCount = await _context.XsiVipguest.AsExpandable().Where(predicateVipGuest).AsNoTracking().Select(i => i.ItemId).CountAsync();

                    var dto = new { List, TotalCount = totalCount, Message = "", MessageTypeResponse = "Success" };
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiGuest action: {ex.InnerException}");
                return Ok(new { List = new List<VIPGuestDTO>(), TotalCount = 0, Message = "Something went wrong. Please try again later.", MessageTypeResponse = "Error" });
            }
        }

        #region Helper Methods
        long GetActivityCount(string url, long langid)
        {
            if (cachedEventSubCategory == null)
                cachedEventSubCategory = CacheKeys.GetEventSubCategory(_cache);
            if (cachedEvents == null)
                cachedEvents = CacheKeys.GetEvents(_cache);

            long categoryid = -1;
            var str = url.Split('=');
            long.TryParse(str[1], out categoryid);

            if (langid == 1)
            {
                var subcatIDs = cachedEventSubCategory.Where(i => i.EventCategoryId == categoryid && i.IsActive == "Y" && i.WebsiteId == 1).Select(i => i.ItemId).ToList();
                var kount = cachedEvents.Where(i => i.EventSubCategoryId != null && subcatIDs.Contains(i.EventSubCategoryId.Value) && (i.Type == "A" || i.Type == "B") && i.IsActive == "Y").ToList().Count();
                return kount;
            }
            else
            {
                var subcatIDs = cachedEventSubCategory.Where(i => i.EventCategoryId == categoryid && i.IsActiveAr == "Y" && i.WebsiteId == 1).Select(i => i.ItemId).ToList();
                var kount = cachedEvents.Where(i => i.EventSubCategoryId != null && subcatIDs.Contains(i.EventSubCategoryId.Value) && (i.Type == "A" || i.Type == "B") && i.IsActiveAr == "Y").ToList().Count();
                return kount;
            }
        }
        string GetNewsDate(DateTime? dt, long langId)
        {
            if (dt != null)
            {
                return (langId == 1)
                    ? dt.Value.ToString("dd MMM")
                    : dt.Value.ToString("dd MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
            }
            return string.Empty;
        }
        string GetEventDateHome(DateTime? startDate, DateTime? endDate, long langId)
        {
            string strdate = "";
            if (startDate.HasValue)
            {
                if (langId == 1)
                    strdate = GetWeek(startDate.Value.ToString("dd"), langId) + ", " + startDate.Value.ToString("ddd") + " " + GetDay(startDate.Value.ToString("dd"));
                else
                    strdate = GetWeek(startDate.Value.ToString("dd"), langId) + ", " + startDate.Value.ToString("ddd", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")) + " " + GetDay(startDate.Value.ToString("dd"));
            }
            //if (endDate.HasValue)
            //    strdate += endDate.Value.ToString("ddd") + " " + GetDay(endDate.Value.ToString("dd"));
            return strdate;
        }
        string GetWeek(string day, long langId)
        {
            string str = langId == 1 ? "Week" : "أسبوع";
            long d = Convert.ToInt64(day);
            if (d >= 1 && d <= 7)
                return "1st " + str;
            else if (d >= 8 && d <= 14)
                return "2nd " + str;
            else if (d >= 15 && d <= 21)
                return "3rd " + str;
            else if (d >= 22 && d <= 28)
                return "4th " + str;
            else
                return "5th " + str;
        }
        string GetDay(string day)
        {
            long day1 = Convert.ToInt64(day);
            string ordinal = string.Empty;
            switch (day1)
            {
                case 1:
                case 21:
                case 31:
                    ordinal = "st";
                    break;
                case 2:
                case 22:
                    ordinal = "nd";
                    break;
                case 3:
                case 23:
                    ordinal = "rd";
                    break;
                default:
                    ordinal = "th";
                    break;
            }
            return day1.ToString() + ordinal;
        }
        #endregion
    }
}

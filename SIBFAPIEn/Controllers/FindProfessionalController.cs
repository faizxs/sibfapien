﻿using Contracts;
using Entities.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Remotion.Linq.Clauses;
using Xsi.ServicesLayer;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.TagHelpers;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FindProfessionalController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly IEmailSender _emailSender;

        public FindProfessionalController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IEmailSender emailSender)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _emailSender = emailSender;
        }

        #region Tab1
        [HttpGet]
        [Route("MySchedule/{memberid}")]
        public async Task<ActionResult<dynamic>> GetMySchedule(long memberid)
        {
            MyScheduleDTO model = new MyScheduleDTO();
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId == memberid)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
                    {
                        XsiExhibitionProfessionalProgramRegistration entity =
                            MethodFactory.VerifyProfessionalProgramRegistration(memberId);
                        if (entity != default(XsiExhibitionProfessionalProgramRegistration) && entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                        {
                            model = BindCustomSchedule(memberId);
                            model.MessageTypeResponse = "Success";
                        }
                        else
                        {
                            model.MessageTypeResponse = "Error";
                            model.Message = "Your registration is not approved.";
                        }
                    }
                    return Ok(model);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetMySchedule action: {ex.InnerException}");
                model.MessageTypeResponse = "Error";
                model.Message = "Something went wrong. Please try again later.";
                return Ok(model);
            }
        }
        [HttpPost]
        [Route("SubmitInvitation")]
        public async Task<ActionResult<dynamic>> PostSubmitInvitation(SubmitInvitationDTO dto)
        {
            //long EnglishId = long.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["EnglishId"].ToString());
            //long ArabicId = long.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["EnglishId"].ToString());
            bool iscompleted = false;
            try
            {
                string ServerAddress = _appCustomSettings.ServerAddressNew;
                string ToEmail = string.Empty;

                string strAdminEmailAddress = _appCustomSettings.AdminEmail;
                string AdminName = _appCustomSettings.AdminName;
                PPSlotRegistrationService PPSlotRegistrationService;
                using (PPSlotRegistrationService = new PPSlotRegistrationService())
                {
                    HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    XsiExhibitionPpslotInvitation entity = PPSlotRegistrationService.GetPPSlotRegistrationByItemId(dto.TransactionId);
                    if (entity != null)
                    {
                        if (dto.IsAccept)
                        {
                            #region Accept
                            XsiExhibitionPpslotInvitation where = new XsiExhibitionPpslotInvitation();
                            where.ResponseId = entity.ResponseId;
                            where.SlotId = entity.SlotId;
                            List<XsiExhibitionPpslotInvitation> PPSlotRegistrationList = PPSlotRegistrationService.GetPPSlotRegistration(where);
                            if (PPSlotRegistrationList.Count() > 0)
                            {
                                foreach (XsiExhibitionPpslotInvitation rows in PPSlotRegistrationList)
                                {
                                    if (rows.ItemId == dto.TransactionId)
                                    {
                                        rows.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Approved);
                                        rows.IsViewed = EnumConversion.ToString(EnumProfessionalProgramNotificationStatus.No);
                                        PPSlotRegistrationService.UpdatePPSlotRegistration(rows);
                                        iscompleted = true;
                                        #region Approved Email Notification
                                        using (EmailContentService EmailContentService = new EmailContentService())
                                        {
                                            ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService();
                                            ProfessionalProgramSlotService ProfessionalProgramSlotService = new ProfessionalProgramSlotService();

                                            XsiExhibitionProfessionalProgramRegistration UserEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(rows.RequestId.Value);
                                            if (UserEntity != null)
                                            {
                                                #region Email Content Parameters For Approved Slot
                                                if (UserEntity.Email != null)
                                                    ToEmail = UserEntity.Email;
                                                XsiExhibitionProfessionalProgramRegistration ResponseUserEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(rows.ResponseId.Value);
                                                string strAcceptedBy = string.Empty;
                                                if (UserEntity.LanguageId.Value == 1)
                                                {
                                                    if (ResponseUserEntity.FirstName != null && ResponseUserEntity.LastName != null)
                                                        strAcceptedBy = ResponseUserEntity.FirstName + " " + ResponseUserEntity.LastName;
                                                    else if (ResponseUserEntity.FirstName != null)
                                                        strAcceptedBy = ResponseUserEntity.FirstName;
                                                    else if (ResponseUserEntity.LastName != null)
                                                        strAcceptedBy = ResponseUserEntity.LastName;
                                                }
                                                else if (ResponseUserEntity.LanguageId.Value == 1)
                                                {
                                                    if (ResponseUserEntity.FirstNameAr != null && ResponseUserEntity.LastNameAr != null)
                                                        strAcceptedBy = ResponseUserEntity.FirstNameAr + " " + ResponseUserEntity.LastNameAr;
                                                    else if (ResponseUserEntity.FirstNameAr != null)
                                                        strAcceptedBy = ResponseUserEntity.FirstNameAr;
                                                    else if (ResponseUserEntity.LastNameAr != null)
                                                        strAcceptedBy = ResponseUserEntity.LastNameAr;
                                                    else if (ResponseUserEntity.FirstName != null && ResponseUserEntity.LastName != null)
                                                        strAcceptedBy = ResponseUserEntity.FirstName + " " + ResponseUserEntity.LastName;
                                                    else if (ResponseUserEntity.FirstName != null)
                                                        strAcceptedBy = ResponseUserEntity.FirstName;
                                                    else if (ResponseUserEntity.LastName != null)
                                                        strAcceptedBy = ResponseUserEntity.LastName;
                                                }
                                                XsiExhibitionProfessionalProgramSlots SlotEntity = ProfessionalProgramSlotService.GetProfessionalProgramSlotByItemId(rows.SlotId.Value);
                                                string strSlot = MethodFactory.GetTimeSlot(1, SlotEntity);
                                                #endregion
                                                #region Send Email
                                                string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                                                StringBuilder body = new StringBuilder();
                                                XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(67);
                                                if (emailContent != null)
                                                {
                                                    long tableNo = MethodFactory.GetHostTableNumber(entity.ResponseId.Value);
                                                    if (emailContent.Body != null)
                                                        strEmailContentBody = emailContent.Body;
                                                    if (emailContent.Email != null)
                                                        strEmailContentEmail = emailContent.Email;
                                                    if (emailContent.Subject != null)
                                                        strEmailContentSubject = emailContent.Subject;
                                                    strEmailContentAdmin = AdminName;
                                                    body.Append(htmlContentFactory.BindEmailContentPP(LangId, strEmailContentBody, UserEntity, ServerAddress, 1, _appCustomSettings));//TODO WebsiteId
                                                    body.Replace("$$slot$$", strSlot);
                                                    body.Replace("$$acceptedby$$", strAcceptedBy);
                                                    if (tableNo != -1)
                                                        body.Replace("$$tableid$$", tableNo.ToString());
                                                    else
                                                        body.Replace("$$tableid$$", string.Empty);

                                                    if (!string.IsNullOrEmpty(ToEmail))
                                                        _emailSender.SendEmail(strEmailContentAdmin, strAdminEmailAddress, ToEmail, strEmailContentSubject, body.ToString());
                                                }
                                                #endregion
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            //entity.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Rejected);
                            //entity.IsViewed = EnumConversion.ToString(EnumProfessionalProgramNotificationStatus.No);
                            //PPSlotRegistrationService.UpdatePPSlotRegistration(entity);
                            #region Rejected Email Notification
                            using (EmailContentService EmailContentService = new EmailContentService())
                            {

                                ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService();
                                ProfessionalProgramSlotService ProfessionalProgramSlotService = new ProfessionalProgramSlotService();

                                XsiExhibitionProfessionalProgramRegistration UserEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(entity.RequestId.Value);
                                if (UserEntity != null)
                                {
                                    #region Email Content Parameters For Rejected Slot
                                    if (UserEntity.Email != null)
                                        ToEmail = UserEntity.Email;
                                    //string strName = UserEntity.FirstName + " " + UserEntity.MiddleName + " " + UserEntity.LastName;
                                    XsiExhibitionProfessionalProgramRegistration ResponseUserEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(entity.ResponseId.Value);
                                    string strDeclinedBy = string.Empty;
                                    if (UserEntity.LanguageId.Value == 1)
                                    {
                                        if (ResponseUserEntity.FirstName != null && ResponseUserEntity.LastName != null)
                                            strDeclinedBy = ResponseUserEntity.FirstName + " " + ResponseUserEntity.LastName;
                                        else if (ResponseUserEntity.FirstName != null)
                                            strDeclinedBy = ResponseUserEntity.FirstName;
                                        else if (ResponseUserEntity.LastName != null)
                                            strDeclinedBy = ResponseUserEntity.LastName;
                                    }
                                    else if (ResponseUserEntity.LanguageId.Value == 2)
                                    {
                                        if (ResponseUserEntity.FirstNameAr != null && ResponseUserEntity.LastNameAr != null)
                                            strDeclinedBy = ResponseUserEntity.FirstNameAr + " " + ResponseUserEntity.LastNameAr;
                                        else if (ResponseUserEntity.FirstNameAr != null)
                                            strDeclinedBy = ResponseUserEntity.FirstNameAr;
                                        else if (ResponseUserEntity.LastNameAr != null)
                                            strDeclinedBy = ResponseUserEntity.LastNameAr;
                                        else if (ResponseUserEntity.FirstName != null && ResponseUserEntity.LastName != null)
                                            strDeclinedBy = ResponseUserEntity.FirstName + " " + ResponseUserEntity.LastName;
                                        else if (ResponseUserEntity.FirstName != null)
                                            strDeclinedBy = ResponseUserEntity.FirstName;
                                        else if (ResponseUserEntity.LastName != null)
                                            strDeclinedBy = ResponseUserEntity.LastName;
                                    }
                                    XsiExhibitionProfessionalProgramSlots SlotEntity = ProfessionalProgramSlotService.GetProfessionalProgramSlotByItemId(entity.SlotId.Value);
                                    string strSlot = MethodFactory.GetTimeSlot(1, SlotEntity);
                                    #endregion
                                    #region Send Email
                                    string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                                    StringBuilder body = new StringBuilder();
                                    XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(65);
                                    if (emailContent != null)
                                    {
                                        if (emailContent.Body != null)
                                            strEmailContentBody = emailContent.Body;
                                        if (emailContent.Email != null)
                                            strEmailContentEmail = emailContent.Email;
                                        if (emailContent.Subject != null)
                                            strEmailContentSubject = emailContent.Subject;
                                        strEmailContentAdmin = AdminName;
                                        body.Append(htmlContentFactory.BindEmailContentPP(LangId, strEmailContentBody, UserEntity, ServerAddress, 1, _appCustomSettings));//TODO
                                        body.Replace("$$slot$$", strSlot);
                                        body.Replace("$$declinedby$$", strDeclinedBy);
                                        if (EnumResultType.Success == PPSlotRegistrationService.DeletePPSlotRegistration(entity.ItemId))
                                        {
                                            iscompleted = true;
                                            if (!string.IsNullOrEmpty(ToEmail))
                                                _emailSender.SendEmail(strEmailContentAdmin, strAdminEmailAddress, ToEmail, strEmailContentSubject, body.ToString());
                                        }
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                        }
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                return iscompleted;
            }
        }
        [HttpPost]
        [Route("UpdateViewStatus")]
        public async Task<ActionResult<dynamic>> PostUpdateViewStatus(UpdateViewStatusDTO dto)
        {
            using (PPSlotRegistrationService PPSlotRegistrationService = new PPSlotRegistrationService())
            {
                XsiExhibitionPpslotInvitation entity = PPSlotRegistrationService.GetPPSlotRegistrationByItemId(dto.TransactionId);
                entity.IsViewed = EnumConversion.ToString(EnumBool.Yes);
                if (EnumResultType.Success == PPSlotRegistrationService.UpdatePPSlotRegistration(entity))
                    return true;
                return false;
            }
        }
        [HttpPost]
        [Route("CancelRequestSent")]
        public async Task<ActionResult<dynamic>> PostCancelRequestSent(CancelRequestSentDTO dto)
        {
            string ServerAddress = _appCustomSettings.ServerAddressNew;
            PPSlotRegistrationService PPSlotRegistrationService;
            string strAdminEmailAddress = _appCustomSettings.AdminEmail;
            string AdminName = _appCustomSettings.AdminName;
            string ToEmail = string.Empty;
            using (PPSlotRegistrationService = new PPSlotRegistrationService())
            {
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                XsiExhibitionPpslotInvitation existingentity = PPSlotRegistrationService.GetPPSlotRegistrationByItemId(dto.TransactionId);
                if (existingentity != null)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    #region Email Notification to Other Party Informing About Cancelling The Request Sent
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {
                        ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService();
                        ProfessionalProgramSlotService ProfessionalProgramSlotService = new ProfessionalProgramSlotService();
                        XsiExhibitionProfessionalProgramRegistration UserEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(existingentity.ResponseId.Value);
                        if (UserEntity != null)
                        {
                            #region Email Content Parameters For Cancelled Slot
                            if (UserEntity.Email != null)
                                ToEmail = UserEntity.Email;

                            XsiExhibitionProfessionalProgramRegistration CancelledByEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(existingentity.RequestId.Value);
                            string strDeclinedBy = string.Empty;
                            if (UserEntity.LanguageId.Value == 1)
                            {
                                if (CancelledByEntity.FirstName != null && CancelledByEntity.LastName != null)
                                    strDeclinedBy = CancelledByEntity.FirstName + " " + CancelledByEntity.LastName;
                                else if (CancelledByEntity.FirstName != null)
                                    strDeclinedBy = CancelledByEntity.FirstName;
                                else if (CancelledByEntity.LastName != null)
                                    strDeclinedBy = CancelledByEntity.LastName;
                            }
                            else if (CancelledByEntity.LanguageId.Value == 2)
                            {
                                if (CancelledByEntity.FirstNameAr != null && CancelledByEntity.LastNameAr != null)
                                    strDeclinedBy = CancelledByEntity.FirstNameAr + " " + CancelledByEntity.LastNameAr;
                                else if (CancelledByEntity.FirstNameAr != null)
                                    strDeclinedBy = CancelledByEntity.FirstNameAr;
                                else if (CancelledByEntity.LastNameAr != null)
                                    strDeclinedBy = CancelledByEntity.LastNameAr;
                                else if (CancelledByEntity.FirstName != null && CancelledByEntity.LastName != null)
                                    strDeclinedBy = CancelledByEntity.FirstName + " " + CancelledByEntity.LastName;
                                else if (CancelledByEntity.FirstName != null)
                                    strDeclinedBy = CancelledByEntity.FirstName;
                                else if (CancelledByEntity.LastName != null)
                                    strDeclinedBy = CancelledByEntity.LastName;
                            }
                            XsiExhibitionProfessionalProgramSlots SlotEntity = ProfessionalProgramSlotService.GetProfessionalProgramSlotByItemId(existingentity.SlotId.Value);
                            string strSlot = MethodFactory.GetTimeSlot(1, SlotEntity);
                            #endregion
                            #region Send Email
                            string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                            StringBuilder body = new StringBuilder();
                            XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(20053);//change here
                            if (emailContent != null)
                            {
                                if (emailContent.Body != null)
                                    strEmailContentBody = emailContent.Body;
                                if (emailContent.Email != null)
                                    strEmailContentEmail = emailContent.Email;
                                if (emailContent.Subject != null)
                                    strEmailContentSubject = emailContent.Subject;
                                strEmailContentAdmin = AdminName;
                            }
                            body.Append(htmlContentFactory.BindEmailContentPP(LangId, strEmailContentBody, UserEntity, ServerAddress, 1, _appCustomSettings));//TODO
                            body.Replace("$$slot$$", strSlot);
                            body.Replace("$$invitedby$$", strDeclinedBy);
                            if (CancelledByEntity.TableId != null)
                                body.Replace("$$tableid$$", CancelledByEntity.TableId.Value.ToString());
                            else
                                body.Replace("$$tableid$$", string.Empty);
                            if (EnumResultType.Success == PPSlotRegistrationService.DeletePPSlotRegistration(existingentity.ItemId))
                            {
                                if (!string.IsNullOrEmpty(ToEmail))
                                {
                                    _emailSender.SendEmail(strEmailContentAdmin, strAdminEmailAddress, ToEmail, strEmailContentSubject, body.ToString());
                                    return true;
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            return false;
        }
        [HttpPost]
        [Route("CancelAfterApproval")]
        public async Task<ActionResult<dynamic>> PostCancelAfterApproval(CancelAfterApprovalDTO dto)
        {
            string ServerAddress = _appCustomSettings.ServerAddressNew;
            PPSlotRegistrationService PPSlotRegistrationService;
            string strAdminEmailAddress = _appCustomSettings.AdminEmail;
            string AdminName = _appCustomSettings.AdminName;
            string ToEmail = string.Empty;
            using (PPSlotRegistrationService = new PPSlotRegistrationService())
            {
                long memberId = User.Identity.GetID();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                XsiExhibitionPpslotInvitation existingentity = PPSlotRegistrationService.GetPPSlotRegistrationByItemId(dto.TransactionId);
                if (existingentity != null)
                {
                    #region Rejected Email Notification
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {
                        HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                        ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService();
                        ProfessionalProgramSlotService ProfessionalProgramSlotService = new ProfessionalProgramSlotService();
                        XsiExhibitionProfessionalProgramRegistration LoggedInPPRegistration = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistration(new XsiExhibitionProfessionalProgramRegistration() { MemberId = memberId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                        // UserEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(entity.RequestId.Value);
                        if (LoggedInPPRegistration != null)
                        {
                            XsiExhibitionProfessionalProgramSlots SlotEntity = new XsiExhibitionProfessionalProgramSlots();
                            string strSlot = string.Empty;
                            #region Send Email
                            string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                            StringBuilder body = new StringBuilder();
                            XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(20091);
                            if (emailContent != null)
                            {
                                XsiExhibitionPpslotInvitation PPSlotRegEntity = PPSlotRegistrationService.GetPPSlotRegistrationByItemId(dto.TransactionId);
                                if (PPSlotRegEntity != default(XsiExhibitionPpslotInvitation))
                                    SlotEntity = ProfessionalProgramSlotService.GetProfessionalProgramSlotByItemId(PPSlotRegEntity.SlotId.Value);
                                if (SlotEntity != default(XsiExhibitionProfessionalProgramSlots))
                                {
                                    if (SlotEntity.IsDelete != "Y")
                                    {
                                        strSlot = MethodFactory.GetTimeSlot(1, SlotEntity);
                                        if (emailContent.Body != null)
                                            strEmailContentBody = emailContent.Body.Replace("$$Name$$", AdminName);
                                        if (emailContent.Email != null && !string.IsNullOrEmpty(emailContent.Email))
                                            ToEmail = emailContent.Email;
                                        else
                                            ToEmail = strAdminEmailAddress;

                                        if (PPSlotRegEntity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                                        {
                                            if (emailContent.Subject != null)
                                            {
                                                strEmailContentSubject = emailContent.Subject;
                                            }
                                        }
                                        else
                                        {
                                            XsiEmailContent emailContent1 = EmailContentService.GetEmailContentByItemId(20053);
                                            if (emailContent1 != null && emailContent1.Subject != null)
                                            {
                                                strEmailContentSubject = emailContent1.Subject;
                                            }
                                        }

                                        strEmailContentAdmin = AdminName;
                                        body.Append(htmlContentFactory.BindEmailContentPP(LangId, strEmailContentBody, LoggedInPPRegistration, ServerAddress, 1, _appCustomSettings));//TODO
                                        body.Replace("$$slot$$", strSlot);
                                        string strDeclinedBy = string.Empty;
                                        if (LoggedInPPRegistration.FirstName != null && LoggedInPPRegistration.LastName != null)
                                            strDeclinedBy = LoggedInPPRegistration.FirstName + " " + LoggedInPPRegistration.LastName;
                                        else if (LoggedInPPRegistration.FirstName != null)
                                            strDeclinedBy = LoggedInPPRegistration.FirstName;
                                        else if (LoggedInPPRegistration.LastName != null)
                                            strDeclinedBy = LoggedInPPRegistration.LastName;
                                        body.Replace("$$declinedby$$", strDeclinedBy);
                                        body.Replace("$$reason$$", dto.Reason);

                                        //if (EnumResultType.Success == PPSlotRegistrationService.DeletePPSlotRegistration(slotId))
                                        //{
                                        ProfessionalProgramService ProfessionalProgramService = new ProfessionalProgramService();

                                        XsiExhibitionProfessionalProgram PPEntity = ProfessionalProgramService.GetProfessionalProgramByItemId(LoggedInPPRegistration.ProfessionalProgramId.Value);
                                        if (PPEntity != default(XsiExhibitionProfessionalProgram))
                                        {
                                            if (PPSlotRegEntity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                                            {
                                                DateTime dt = MethodFactory.ArabianTimeNow().AddDays(14);
                                                if (dt >= PPEntity.AppointmentStartDate)
                                                {
                                                    //a cancelation letter should be sent to the admin if cancelation is done within 2 weeks before the PP first day.
                                                    if (!string.IsNullOrEmpty(ToEmail))
                                                        _emailSender.SendEmail(strEmailContentAdmin, strAdminEmailAddress, ToEmail, strEmailContentSubject, body.ToString());
                                                }
                                            }
                                            #region Send Email To User
                                            body = new StringBuilder();
                                            emailContent = EmailContentService.GetEmailContentByItemId(20091);
                                            if (emailContent != null)
                                            {
                                                PPSlotRegEntity = PPSlotRegistrationService.GetPPSlotRegistrationByItemId(dto.TransactionId);
                                                if (PPSlotRegEntity != default(XsiExhibitionPpslotInvitation))
                                                    SlotEntity = ProfessionalProgramSlotService.GetProfessionalProgramSlotByItemId(PPSlotRegEntity.SlotId.Value);
                                                if (SlotEntity != default(XsiExhibitionProfessionalProgramSlots))
                                                {
                                                    if (SlotEntity.IsDelete != "Y")
                                                    {
                                                        strSlot = MethodFactory.GetTimeSlot(LangId, SlotEntity);

                                                        ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService();
                                                        XsiExhibitionProfessionalProgramRegistration PPRegistrationResponse = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(PPSlotRegEntity.ResponseId.Value);
                                                        XsiExhibitionProfessionalProgramRegistration PPRegistrationRequestIntiate = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(PPSlotRegEntity.RequestId.Value);
                                                        if (PPRegistrationResponse != null && PPRegistrationRequestIntiate != null)
                                                        {
                                                            XsiExhibitionMember UserEmail;
                                                            if (PPRegistrationRequestIntiate.MemberId.Value == memberId)
                                                                UserEmail = ExhibitionMemberService.GetExhibitionMemberByItemId(PPRegistrationResponse.MemberId.Value);
                                                            else
                                                                UserEmail = ExhibitionMemberService.GetExhibitionMemberByItemId(PPRegistrationRequestIntiate.MemberId.Value);

                                                            if (UserEmail != default(XsiExhibitionMember))
                                                            {
                                                                #region Email Content
                                                                if (emailContent.Body != null)
                                                                    strEmailContentBody = emailContent.Body.Replace("$$Name$$", UserEmail.Firstname + " " + UserEmail.LastName);
                                                                if (UserEmail.Email != null && !string.IsNullOrEmpty(UserEmail.Email))
                                                                    ToEmail = UserEmail.Email;

                                                                //if (emailContent.Subject != null)
                                                                //    strEmailContentSubject = emailContent.Subject;

                                                                if (PPSlotRegEntity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                                                                {
                                                                    if (emailContent.Subject != null)
                                                                    {
                                                                        strEmailContentSubject = emailContent.Subject;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    XsiEmailContent emailContent1 = EmailContentService.GetEmailContentByItemId(20053);
                                                                    if (emailContent1 != null && emailContent1.Subject != null)
                                                                    {
                                                                        strEmailContentSubject = emailContent1.Subject;
                                                                    }
                                                                }

                                                                strEmailContentAdmin = AdminName;
                                                                body.Append(htmlContentFactory.BindEmailContentPP(LangId, strEmailContentBody, LoggedInPPRegistration, ServerAddress, 1, _appCustomSettings));//TODO
                                                                body.Replace("$$slot$$", strSlot);
                                                                strDeclinedBy = string.Empty;
                                                                if (LoggedInPPRegistration.FirstName != null && LoggedInPPRegistration.LastName != null)
                                                                    strDeclinedBy = LoggedInPPRegistration.FirstName + " " + LoggedInPPRegistration.LastName;
                                                                else if (LoggedInPPRegistration.FirstName != null)
                                                                    strDeclinedBy = LoggedInPPRegistration.FirstName;
                                                                else if (LoggedInPPRegistration.LastName != null)
                                                                    strDeclinedBy = LoggedInPPRegistration.LastName;
                                                                body.Replace("$$declinedby$$", strDeclinedBy);
                                                                body.Replace("$$reason$$", dto.Reason);
                                                                #endregion
                                                                if (EnumResultType.Success == PPSlotRegistrationService.DeletePPSlotRegistration(dto.TransactionId))
                                                                {
                                                                    if (!string.IsNullOrEmpty(ToEmail))
                                                                        _emailSender.SendEmail(strEmailContentAdmin, strAdminEmailAddress, ToEmail, strEmailContentSubject, body.ToString());
                                                                    return true;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                PPSlotRegistrationService.DeletePPSlotRegistration(dto.TransactionId);
                                                                return true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                        // }
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            return false;
        }
        [HttpGet]
        [Route("PDF/{memberid}")]
        public async Task<ActionResult<dynamic>> GetPDFForSlots(long memberid)
        {
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    var ppUrl = CheckProfessionalProgram(memberId, LangId);
                    if (string.IsNullOrEmpty(ppUrl))
                    {
                        using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService =
                            new ProfessionalProgramRegistrationService())
                        {
                            XsiExhibitionProfessionalProgramRegistration entity =
                                MethodFactory.VerifyProfessionalProgramRegistration(memberId);
                            var DownloadUrl = GeneratePDF(entity.ItemId, entity.ProfessionalProgramId.Value, LangId);
                            if (!string.IsNullOrEmpty(DownloadUrl))
                                return Ok(DownloadUrl);
                            else
                                return Ok("No schedule found.");
                        }
                    }
                    else
                        return Ok(ppUrl);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetPDFForSlots action: {ex.InnerException}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }

        }

        [HttpGet]
        [Route("ChooseAnotherDateEmail/{itemid}")]
        public async Task<ActionResult<dynamic>> GetChooseAnotherDateEmail(long itemid)
        {
            string ServerAddress = _appCustomSettings.ServerAddressNew;
            PPSlotRegistrationService PPSlotRegistrationService;
            string strAdminEmailAddress = _appCustomSettings.AdminEmail;
            string AdminName = _appCustomSettings.AdminName;
            string ToEmail = string.Empty;
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            using (PPSlotRegistrationService = new PPSlotRegistrationService())
            {
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                XsiExhibitionPpslotInvitation entity = PPSlotRegistrationService.GetPPSlotRegistrationByItemId(itemid);
                if (entity != null)
                {
                    #region Chosen Another Slot Email Notification
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {
                        ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService();
                        ProfessionalProgramSlotService ProfessionalProgramSlotService = new ProfessionalProgramSlotService();
                        XsiExhibitionProfessionalProgramRegistration UserEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(entity.RequestId.Value);
                        if (UserEntity != null)
                        {
                            #region Email Content Parameters For Rejected Slot
                            if (UserEntity.Email != null)
                                ToEmail = UserEntity.Email;

                            XsiExhibitionProfessionalProgramRegistration ResponseUserEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(entity.ResponseId.Value);
                            string strDeclinedBy = string.Empty;
                            if (UserEntity.LanguageId.Value == 1)
                            {
                                if (ResponseUserEntity.FirstName != null && ResponseUserEntity.LastName != null)
                                    strDeclinedBy = ResponseUserEntity.FirstName + " " + ResponseUserEntity.LastName;
                                else if (ResponseUserEntity.FirstName != null)
                                    strDeclinedBy = ResponseUserEntity.FirstName;
                                else if (ResponseUserEntity.LastName != null)
                                    strDeclinedBy = ResponseUserEntity.LastName;
                            }
                            else if (ResponseUserEntity.LanguageId.Value == 2)
                            {
                                if (ResponseUserEntity.FirstName != null && ResponseUserEntity.LastName != null)
                                    strDeclinedBy = ResponseUserEntity.FirstName + " " + ResponseUserEntity.LastName;
                                else if (ResponseUserEntity.FirstName != null)
                                    strDeclinedBy = ResponseUserEntity.FirstName;
                                else if (ResponseUserEntity.LastName != null)
                                    strDeclinedBy = ResponseUserEntity.LastName;
                            }
                            XsiExhibitionProfessionalProgramSlots SlotEntity = ProfessionalProgramSlotService.GetProfessionalProgramSlotByItemId(entity.SlotId.Value);
                            string strSlot = MethodFactory.GetTimeSlot(1, SlotEntity);
                            #endregion
                            #region Send Email
                            string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                            StringBuilder body = new StringBuilder();
                            XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(71);
                            if (emailContent != null)
                            {
                                if (emailContent.Body != null)
                                    strEmailContentBody = emailContent.Body;
                                if (emailContent.Email != null)
                                    strEmailContentEmail = emailContent.Email;
                                if (emailContent.Subject != null)
                                    strEmailContentSubject = emailContent.Subject;
                                strEmailContentAdmin = AdminName;
                            }
                            body.Append(htmlContentFactory.BindEmailContentPP(LangId, strEmailContentBody, UserEntity, ServerAddress, 1, _appCustomSettings));
                            body.Replace("$$slot$$", strSlot);
                            body.Replace("$$by$$", strDeclinedBy);
                            if (ResponseUserEntity.TableId != null)
                                body.Replace("$$tableid$$", ResponseUserEntity.TableId.Value.ToString());
                            else
                                body.Replace("$$tableid$$", string.Empty);
                            if (EnumResultType.Success == PPSlotRegistrationService.DeletePPSlotRegistration(entity.ItemId))
                            {
                                if (!string.IsNullOrEmpty(ToEmail))
                                {
                                    _emailSender.SendEmail(strEmailContentAdmin, strAdminEmailAddress, ToEmail, strEmailContentSubject, body.ToString());
                                    return true;
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            return false;
        }
        #endregion
        #region Tab2
        [HttpPost]
        public async Task<ActionResult<dynamic>> GetFindProfessional(FindProfessionalSearch searchModel)
        {
            FindProfessionalDTO model = new FindProfessionalDTO();
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId == searchModel.MemberId)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService =
                        new ProfessionalProgramRegistrationService())
                    {
                        XsiExhibitionProfessionalProgramRegistration entity =
                            MethodFactory.VerifyProfessionalProgramRegistration(memberId);
                        if (entity != default(XsiExhibitionProfessionalProgramRegistration) && entity.Status ==
                            EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                        {
                            model = BindItems(memberId, searchModel, LangId);
                            model.MessageTypeResponse = "Success";
                        }
                        else
                        {
                            model.MessageTypeResponse = "Error";
                            model.Message = "Your registration is not approved.";
                        }
                    }

                    return Ok(model);
                }

                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetFindProfessional action: {ex.InnerException}");
                model.MessageTypeResponse = "Error";
                model.Message = "Something went wrong. Please try again later.";
                return Ok(model);
            }
        }
        #endregion
        #region Tab3
        [HttpGet]
        [Route("MyProfile/{memberid}")]
        public async Task<ActionResult<dynamic>> GetMyProfile(long memberid)
        {
            MyProfileMessageDTO model = new MyProfileMessageDTO();
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId == memberid)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    XsiExhibitionProfessionalProgramRegistration entity =
                        MethodFactory.VerifyProfessionalProgramRegistration(memberId);
                    if (entity != default(XsiExhibitionProfessionalProgramRegistration) && entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                    {
                        model.MyProfileDTO = LoadUserProfile(entity.ItemId);
                        model.MessageTypeResponse = "Success";
                    }
                    else
                    {
                        model.MessageTypeResponse = "Error";
                        model.Message = "Your registration is not approved.";
                    }
                    return Ok(model);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetMyProfile action: {ex.InnerException}");
                model.MessageTypeResponse = "Error";
                model.Message = "Something went wrong. Please try again later.";
                return Ok(model);
            }
        }
        [HttpGet]
        [Route("MyProfileBooks/{memberid}")]
        public async Task<ActionResult<dynamic>> GetMyProfileBooks(long memberid)
        {
            MyProfileBooksMessageDTO model = new MyProfileBooksMessageDTO();
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId == memberid)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    XsiExhibitionProfessionalProgramRegistration entity =
                        MethodFactory.VerifyProfessionalProgramRegistration(memberId);
                    if (entity != default(XsiExhibitionProfessionalProgramRegistration) && entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                    {
                        model.MyProfileDTO =  LoadUserProfileBooks(entity.ItemId);
                        model.ProfessionalProgramBookList = MethodFactory.GetProfessionalProgramBookList(entity.ItemId);
                        model.MessageTypeResponse = "Success";
                    }
                    else
                    {
                        model.MessageTypeResponse = "Error";
                        model.Message = "Your registration is not approved.";
                    }
                    return Ok(model);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetMyProfile action: {ex.InnerException}");
                model.MessageTypeResponse = "Error";
                model.Message = "Something went wrong. Please try again later.";
                return Ok(model);
            }
        }

        [HttpGet]
        [Route("OthersProfile/{ppregid}")]
        public async Task<ActionResult<dynamic>> OthersProfile(long ppregid)
        {
            MyProfileMessageDTO model = new MyProfileMessageDTO();
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        XsiExhibitionProfessionalProgramRegistration myentity =
                            MethodFactory.VerifyProfessionalProgramRegistration(memberId);
                        if (myentity != null && myentity.Status ==
                            EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                        {
                            model.MyRegistrationId = myentity.ItemId;

                            XsiExhibitionProfessionalProgramRegistration entity = _context
                                .XsiExhibitionProfessionalProgramRegistration.Where(i => i.ItemId == ppregid)
                                .FirstOrDefault();
                            if (entity != default(XsiExhibitionProfessionalProgramRegistration) && entity.Status ==
                                EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                            {
                                model.MyProfileDTO = LoadUserProfile(entity.ItemId, true);
                                model.MessageTypeResponse = "Success";
                            }
                            else
                            {
                                model.MessageTypeResponse = "Error";
                                model.Message = "User not found or not yet approved.";
                            }
                        }
                        else
                        {
                            model.MessageTypeResponse = "Error";
                            model.Message = "Your registration is not approved.";
                        }
                    }

                    return Ok(model);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetMyProfile action: {ex.InnerException}");
                model.MessageTypeResponse = "Error";
                model.Message = "Something went wrong. Please try again later.";
                return Ok(model);
            }
        }
        [HttpGet]
        [Route("OthersProfileSchedule/{ppregid}")]
        public async Task<ActionResult<dynamic>> OthersProfileSchedule(long ppregid)
        {
            MyScheduleDTO model = new MyScheduleDTO();
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
                    {
                        XsiExhibitionProfessionalProgramRegistration myentity =
                            MethodFactory.VerifyProfessionalProgramRegistration(memberId);
                        if (myentity != null && myentity.Status ==
                            EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                        {
                            XsiExhibitionProfessionalProgramRegistration entity =
                                ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(
                                    ppregid);
                            if (entity != default(XsiExhibitionProfessionalProgramRegistration) && entity.Status ==
                                EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                            {
                                model = BindCustomSchedule(entity.MemberId.Value);
                                model.MyRegistrationId = myentity.ItemId;
                                model.MessageTypeResponse = "Success";
                            }
                            else
                            {
                                model.MessageTypeResponse = "Error";
                                model.Message = "User not found or not yet approved.";
                            }
                        }
                        else
                        {
                            model.MessageTypeResponse = "Error";
                            model.Message = "Your registration is not approved.";
                        }
                    }
                    return Ok(model);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetMySchedule action: {ex.InnerException}");
                model.MessageTypeResponse = "Error";
                model.Message = "Something went wrong. Please try again later.";
                return Ok(model);
            }
        }
        //[HttpGet]
        //[Route("MemberProfile/{ppregid}")]
        //public async Task<ActionResult<dynamic>> GetMemberProfile(long ppregid)
        //{
        //    using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
        //    {
        //        XsiExhibitionProfessionalProgramRegistration entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(ppregid);
        //        if (entity != null)
        //        {
        //            if (!string.IsNullOrEmpty(entity.FileName))
        //            {
        //                if (System.IO.File.Exists(_appCustomSettings.UploadsCMSPath + "/content/Uploads/PPRegistration/Avatar/" + entity.FileName))
        //                    return "/content/Uploads/PPRegistration/Avatar/" + entity.FileName;
        //            }
        //            else if (entity.Title != null)
        //            {
        //                if (entity.Title == "M")
        //                    return "/Content/assets/images/avatarmale.png";
        //                else
        //                    return "/Content/assets/images/avatarfemale.png";
        //            }
        //        }
        //        return "/content/assets/images/profile-picture.jpg";
        //    }
        //}
        [HttpPost]
        [Route("UpdateProfile")]
        public async Task<ActionResult<dynamic>> PostUpdateProfile(ExhibitionProfessionalProgramRegistrationDTO itemDto)
        {
            MessageDTO dto = new MessageDTO();
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    var ppUrl = CheckProfessionalProgram(memberId, LangId);
                    if (string.IsNullOrEmpty(ppUrl))
                    {
                        var RegisteredId = MethodFactory.GetRegisteredId(memberId);
                        var OldPPRegistrationEntity = GetCurrentEntityDetails(RegisteredId);
                        #region Update And Load Content
                        using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
                        {
                            XsiExhibitionProfessionalProgramRegistration entity = new XsiExhibitionProfessionalProgramRegistration();
                            // XsiExhibitionProfessionalProgramRegistration existingEntity = new XsiExhibitionProfessionalProgramRegistration();
                            entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(RegisteredId);
                            if (entity != null)
                            {
                                #region Step Two
                                entity.Type = itemDto.Type;
                                if (itemDto.Title != null && !string.IsNullOrEmpty(itemDto.Title))
                                {
                                    entity.Title = itemDto.Title;
                                }
                                entity.FirstName = itemDto.FirstName;
                                entity.LastName = itemDto.LastName;
                                entity.Email = itemDto.Email;
                                entity.CountryId = itemDto.CountryId;
                                entity.CityId = itemDto.CityId;
                                entity.OtherCity = itemDto.OtherCity;

                                entity.PhoneNumber = itemDto.PhoneISD + "$" + itemDto.PhoneSTD + "$" + itemDto.Phone;
                                entity.MobileNumber = itemDto.MobileISD + "$" + itemDto.MobileSTD + "$" + itemDto.Mobile;
                                entity.ZipCode = itemDto.ZipCode;
                                entity.CompanyName = itemDto.CompanyName;
                                entity.JobTitle = itemDto.JobTitle;
                                entity.WorkProfile = itemDto.WorkProfile;
                                entity.Website = itemDto.Website;
                                entity.SocialLinkUrl = itemDto.SocialLinkUrl;

                                #endregion
                                #region Step Three
                                if (itemDto.TradeType == "A" || itemDto.TradeType == "S")  //Both (A), Seller(S)
                                    entity.StepId = 4;
                                else
                                {
                                    if (entity.Status != null && entity.Status != EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                                        entity.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Pending);
                                    itemDto.StepId = 4;
                                }
                                entity.MemberId = memberId;

                                // Step 3
                                entity.IsAttended = itemDto.IsAttended;
                                //entity.HowManyTimes = itemDto.HowManyTimes;
                                entity.IsAttendedTg = itemDto.IsAttendedTg;

                                if (!string.IsNullOrEmpty(itemDto.TGReasonForNotAttend))
                                    entity.TgreasonForNotAttend = itemDto.TGReasonForNotAttend.Trim();

                                if (itemDto.IsAttended == EnumConversion.ToString(EnumBool.Yes))
                                {
                                    entity.HowManyTimes = itemDto.HowManyTimes;
                                }
                                entity.NoofPendingTg = itemDto.NoofPendingTg;
                                entity.NoofPublishedTg = itemDto.NoofPublishedTg;
                                // entity.ProfessionalProgramId = itemDto.ProfessionalProgramId;
                                entity.ProgramOther = itemDto.ProgramOther;

                                entity.TradeType = itemDto.TradeType;
                                entity.IsEbooks = itemDto.IsEbooks;
                                entity.UnSoldRightsBooks = itemDto.UnSoldRightsBooks;
                                entity.CatalogueMainLanguageOfOriginalTitle = itemDto.CatalogueMainLanguageOfOriginalTitle;
                                entity.ExamplesOfTranslatedBooks = itemDto.ExamplesOfTranslatedBooks;
                                entity.OtherGenre = itemDto.OtherGenre;

                                //if (itemDto.IsAttended == EnumConversion.ToString(EnumBool.Yes))
                                //    entity.ReasonToAttendAgain = entity.IsAttendedTg = entity.NoofPendingTg = entity.NoofPublishedTg = string.Empty;

                                #endregion
                                if (itemDto.TradeType == "A" || itemDto.TradeType == "S")
                                {
                                    #region Step Four
                                    entity.BooksUrl = itemDto.BooksUrl;
                                    if (itemDto.FileBase64 != null && itemDto.FileBase64.Length > 0)
                                    {
                                        string fileName = string.Empty;
                                        byte[] imageBytes;
                                        if (itemDto.FileBase64.Contains("data:"))
                                        {
                                            var strInfo = itemDto.FileBase64.Split(",")[0];
                                            imageBytes = Convert.FromBase64String(itemDto.FileBase64.Split(',')[1]);
                                        }
                                        else
                                        {
                                            imageBytes = Convert.FromBase64String(itemDto.FileBase64);
                                        }

                                        fileName = string.Format("{0}_{1}_{2}{3}", itemDto.ItemId, 1, MethodFactory.GetRandomNumber(),
                                            "." + itemDto.FileExtension);
                                        System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\Document\\") + fileName, imageBytes);

                                        //ResizeSettings setting1 = new ResizeSettings() { Width = 200, Height = 282, Mode = FitMode.Crop };
                                        //ImageBuilder.Current.Build(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\Temp\\" + fileName, _appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\Cover\\" + fileName, setting1);
                                        //if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\temp\\" + fileName))
                                        //    System.IO.File.Delete(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\temp\\" + fileName);

                                        entity.FileName1 = fileName;
                                    }
                                    #endregion
                                }
                                entity.ModifiedById = memberId;
                                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                                if (ProfessionalProgramRegistrationService.UpdateProfessionalProgramRegistration(entity) == EnumResultType.Success)
                                {
                                    List<long> OldListPPProgrameSource = new List<long>();
                                    List<long> NewListPPProgrameSource = new List<long>();
                                    List<long> OldListGenre = new List<long>();
                                    List<long> NewListGenre = new List<long>();
                                    if (itemDto.IsAttended == EnumConversion.ToString(EnumBool.No))
                                    {
                                        #region PPProgram Table
                                        bool IsPPProgramOther = false;
                                        PPProgramService PPProgramService = new PPProgramService();
                                        XsiExhibitionPpprogram delPPProgram = new XsiExhibitionPpprogram();
                                        delPPProgram.ProfessionalProgramRegistrationId = RegisteredId;

                                        #region Deleted PP Programe Source Items

                                        OldListPPProgrameSource = PPProgramService.GetPPProgram(delPPProgram).Where(i => i.PpprogramId != null)
                                            .Select(i => i.PpprogramId.Value).ToList();

                                        #endregion

                                        PPProgramService.DeletePPProgram(delPPProgram);
                                        if (itemDto.PPProgramList != null)
                                        {
                                            foreach (var item in itemDto.PPProgramList)
                                            {
                                                XsiExhibitionPpprogram ppprogramentity = new XsiExhibitionPpprogram();
                                                ppprogramentity.PpprogramId = item;
                                                ppprogramentity.ProfessionalProgramRegistrationId = RegisteredId;
                                                PPProgramService.InsertPPProgram(ppprogramentity);
                                                if (item == 15)
                                                    IsPPProgramOther = true;
                                            }

                                            NewListPPProgrameSource = itemDto.PPProgramList; // PPProgramService.GetPPProgram(delPPProgram).Select(i => i.PpprogramId.Value).ToList();
                                        }
                                        #endregion
                                    }
                                    #region PPGenre Table

                                    PPGenreService PPGenreService = new PPGenreService();
                                    XsiExhibitionPpgenre whereExhibitionPPGenre = new XsiExhibitionPpgenre();
                                    whereExhibitionPPGenre.ProfessionalProgramRegistrationId = RegisteredId;
                                    #region Deleted Genre Items
                                    OldListGenre = PPGenreService.GetPPGenre(whereExhibitionPPGenre).Where(i => i.GenreId != null).Select(i => i.GenreId.Value).ToList();
                                    #endregion
                                    if (OldListGenre.Count > 0)
                                        PPGenreService.DeletePPGenre(whereExhibitionPPGenre);

                                    if (itemDto.PPGenreList != null)
                                    {
                                        foreach (var item in itemDto.PPGenreList)
                                        {
                                            XsiExhibitionPpgenre ppgenreentity = new XsiExhibitionPpgenre();
                                            ppgenreentity.GenreId = item;
                                            ppgenreentity.ProfessionalProgramRegistrationId = RegisteredId;
                                            PPGenreService.InsertPPGenre(ppgenreentity);
                                        }

                                        NewListGenre = itemDto.PPGenreList; // PPGenreService.GetPPGenre(delPPGenre).Select(i => i.GenreId.Value).ToList();
                                    }

                                    #endregion
                                    // LoadUserProfile();
                                    // pnlShow.Visible = true;
                                    // lblStatusMessage = "Profile has been updated successfully";
                                    SendEmailBasedOnEdit(entity.ItemId, OldListGenre, NewListGenre, OldListPPProgrameSource, NewListPPProgrameSource, OldPPRegistrationEntity, itemDto.WebsiteId ?? 1, LangId);

                                    dto.Message = "Profile has been updated successfully";
                                    dto.MessageTypeResponse = "Success";
                                    return Ok(dto);
                                }
                                else
                                {
                                    dto.Message = "Problem in updating profile";
                                    dto.MessageTypeResponse = "Error";
                                    return Ok(dto);
                                }
                                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Function", "ShowUpdateProfileView()", true);
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        dto.Message = "Your registration is not approved.";
                        dto.MessageTypeResponse = "Error";
                        dto.ReturnURL = ppUrl;
                        return Ok(dto);
                    }
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside PostUpdateProfile action: {ex.InnerException}");

                dto.Message = "Something went wrong. Please try again later.";
                dto.MessageTypeResponse = "Error";
                return Ok(dto);
            }
        }
        [HttpGet]
        [Route("ProfilePDF/{registeredid}")]
        public async Task<ActionResult<dynamic>> GetProfilePDF(long registeredid)
        {
            MessageDTO dto = new MessageDTO();
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    var ppUrl = CheckProfessionalProgram(memberId, LangId);
                    dto.MessageTypeResponse = "Success";
                    dto.ReturnURL = ppUrl;
                    if (string.IsNullOrEmpty(ppUrl))
                    {
                        dto.ReturnURL = GenerateProfilePDF(registeredid, LangId);
                        return Ok(dto);
                    }
                    return Ok(dto);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetProfilePDF action: {ex.InnerException}");
                dto.MessageTypeResponse = "Success";
                dto.Message = "Something went wrong. Please try again later.";
                return Ok(dto);
            }
        }
        #endregion
        #region Tab4
        [HttpGet]
        [Route("MyBooks/{memberid}")]
        public async Task<ActionResult<dynamic>> GetMyBooks(long memberid)
        {
            MyBooksMessageDTO model = new MyBooksMessageDTO();
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId == memberid)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    XsiExhibitionProfessionalProgramRegistration entity =
                        MethodFactory.VerifyProfessionalProgramRegistration(memberId);
                    if (entity != default(XsiExhibitionProfessionalProgramRegistration) && entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                    {
                        model.MyBooksDTO = BindUserBooks(memberId, LangId);
                        model.MessageTypeResponse = "Success";
                    }
                    else
                    {
                        model.MessageTypeResponse = "Error";
                        model.Message = "Your registration is not approved.";
                    }
                    return Ok(model);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetMyBooks action: {ex.InnerException}");
                model.MessageTypeResponse = "Error";
                model.Message = "Something went wrong. Please try again later.";
                return Ok(model);
            }
        }

        [HttpPost]
        [Route("UpdateCatalogue")]
        public async Task<ActionResult<dynamic>> PostUpdateCatalogue(UpdateCatalogueDTO updateCatalogue)
        {
            MessageDTO dto = new MessageDTO();
            try
            {
                long memberId = User.Identity.GetID();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                if (memberId == updateCatalogue.MemberId)
                {
                    XsiExhibitionProfessionalProgramRegistration entity =
                        MethodFactory.VerifyProfessionalProgramRegistration(memberId);
                    if (entity != default(XsiExhibitionProfessionalProgramRegistration) && entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                    {
                        if (IsValidFile("." + updateCatalogue.FileExtension))
                        {
                            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
                            {
                                XsiExhibitionProfessionalProgramRegistration existingEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(updateCatalogue.ItemId);
                                XsiExhibitionProfessionalProgramRegistration Entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(updateCatalogue.ItemId);
                                Entity.BooksUrl = updateCatalogue.BooksUrl;
                                #region Catalogue
                                if (updateCatalogue.FileBase64 != null && updateCatalogue.FileBase64.Length > 0)
                                {
                                    byte[] imageBytes;
                                    if (updateCatalogue.FileBase64.Contains("data:"))
                                    {
                                        var strInfo = updateCatalogue.FileBase64.Split(",")[0];
                                        imageBytes = Convert.FromBase64String(updateCatalogue.FileBase64.Split(',')[1]);
                                    }
                                    else
                                    {
                                        imageBytes = Convert.FromBase64String(updateCatalogue.FileBase64);
                                    }
                                    string FileName = string.Format("{0}_{1}{2}", LangId, MethodFactory.GetRandomNumber(), "." + updateCatalogue.FileExtension);
                                    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\Document\\") + FileName, imageBytes);
                                    Entity.FileName1 = FileName;
                                }
                                #endregion
                                string DownloadUrl = "";
                                ProfessionalProgramRegistrationService.UpdateProfessionalProgramRegistration(Entity);
                                if (Entity.FileName1 != null && !string.IsNullOrEmpty(Entity.FileName1))
                                {
                                    DownloadUrl = _appCustomSettings.UploadsCMSPath + "/content/uploads/PPRegistration/Document/" + Entity.FileName1;

                                    dto = new MessageDTO() { Message = "Catalogue and URL has been updated successfully.", ReturnURL = DownloadUrl, MessageTypeResponse = "Success" };
                                    return Ok(dto);
                                }
                                return Ok(new MessageDTO() { Message = "Catalogue and URL has been updated successfully.", MessageTypeResponse = "Success" });
                            }
                        }
                        else
                        {
                            return Ok(new MessageDTO() { Message = "Invalid Image Type or File Size", MessageTypeResponse = "Error" });
                        }
                        //model.MyBooksDTO = BindUserBooks(memberId, LangId);
                        //model.MessageTypeResponse = "Success";
                    }
                    else
                    {
                        return Ok(new MessageDTO() { Message = "Your registration is not approved.", MessageTypeResponse = "Error" });
                    }
                }

                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside PostUpdateCatalogue action: {ex.InnerException}");
                return Ok(new MessageDTO() { Message = "Something went wrong. Please try again later ", MessageTypeResponse = "Error" });
            }
        }
        #endregion

        #region Get Methods
        [HttpGet]
        [Route("CheckSendMeetupRequest/{requestid}/{slotid}")]
        public async Task<ActionResult<dynamic>> GetCheckSendMeetupRequest(long requestid, long slotid)
        {
            using (PPSlotRegistrationService PPSlotRegistrationService = new PPSlotRegistrationService())
            {
                if (PPSlotRegistrationService.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { SlotId = slotid, IsActive = EnumConversion.ToString(EnumBool.Yes) }).Where(i => i.RequestId == requestid || i.ResponseId == requestid).ToList().Count == 0)
                    return true;
                return false;
            }
        }
        [HttpGet]
        [Route("GetSlots/{professionalprogramid}/{ppregid}")]
        public async Task<ActionResult<dynamic>> GetSlots(long professionalprogramid, long ppregid)
        {
            SlotsDTO model = new SlotsDTO();
            model.SlotsList = new List<CustomScheduleDTO>();
            ProfessionalProgramSlotService ProfessionalProgramSlotService;
            PPSlotRegistrationService PPSlotRegistrationService;
            XsiExhibitionPpslotInvitation where1;
            List<XsiSlots> SlotsList = new List<XsiSlots>();
            List<XsiSlots> SlotsListNew = new List<XsiSlots>();
            List<XsiExhibitionProfessionalProgramSlots> ProfessionalProgramSlotList;
            string IsAvailableDayOne = EnumConversion.ToString(EnumBool.No);
            string IsAvailableDayTwo = EnumConversion.ToString(EnumBool.No);
            string IsAvailableDayThree = EnumConversion.ToString(EnumBool.No);

            string IsApprovedStatusDayOne = EnumConversion.ToString(EnumBool.No);
            string IsApprovedStatusDayTwo = EnumConversion.ToString(EnumBool.No);
            string IsApprovedStatusDayThree = EnumConversion.ToString(EnumBool.No);

            long count1 = 0, count2 = 0, sum = 0;
            using (ProfessionalProgramSlotService = new ProfessionalProgramSlotService())
            {
                XsiExhibitionProfessionalProgramSlots where = new XsiExhibitionProfessionalProgramSlots();
                where.ProfessionalProgramId = professionalprogramid;
                where.IsDelete = EnumConversion.ToString(EnumBool.No);
                ProfessionalProgramSlotList = ProfessionalProgramSlotService.GetProfessionalProgramSlot(where);
                if (ProfessionalProgramSlotList.Count() > 0)
                {
                    List<XsiExhibitionProfessionalProgramSlots> ProfessionalProgramSlotList1 = ProfessionalProgramSlotList.Where(p => p.DayId == 1).ToList();
                    List<XsiExhibitionProfessionalProgramSlots> ProfessionalProgramSlotList2 = ProfessionalProgramSlotList.Where(p => p.DayId == 2).ToList();
                    List<XsiExhibitionProfessionalProgramSlots> ProfessionalProgramSlotList3 = ProfessionalProgramSlotList.Where(p => p.DayId == 3).ToList();
                    #region Day One
                    foreach (XsiExhibitionProfessionalProgramSlots entity in ProfessionalProgramSlotList1)
                    {
                        #region Insert DayOne
                        #region Day one slot count
                        PPSlotRegistrationService = new PPSlotRegistrationService();
                        where1 = new XsiExhibitionPpslotInvitation();
                        where1.RequestId = ppregid;
                        where1.SlotId = entity.ItemId;
                        where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        count1 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                        if (count1 < 1)
                        {
                            PPSlotRegistrationService = new PPSlotRegistrationService();
                            where1 = new XsiExhibitionPpslotInvitation();
                            where1.ResponseId = ppregid;
                            where1.SlotId = entity.ItemId;
                            where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                            count2 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                        }
                        #endregion
                        sum = count1 + count2;
                        if (sum < 2)
                            IsAvailableDayOne = EnumConversion.ToString(EnumBool.Yes);
                        if (count1 == 1)
                            IsAvailableDayOne = EnumConversion.ToString(EnumBool.No);

                        //IsApprovedStatusDayOne = PPSlotRegistrationService.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { SlotId = entity.ItemId, Status = "A" }).Count() > 0 ? "Y" : "N";
                        SlotsList.Add(
                            new XsiSlots()
                            {
                                ItemId = entity.ItemId,
                                DayOneId = entity.ItemId,
                                StartTime = entity.StartTime.Value.ToString("HH:mm"),
                                EndTime = entity.EndTime.Value.ToString("HH:mm"),
                                IsBreakDayOne = entity.IsBreak,
                                IsAvailableDayOne = IsAvailableDayOne,
                                Break1Title = entity.BreakTitle

                            });
                        #endregion

                    }
                    #endregion
                    #region Day Two
                    sum = 0; count1 = 0; count2 = 0;
                    bool IsRepeat = false;
                    foreach (XsiExhibitionProfessionalProgramSlots entity in ProfessionalProgramSlotList2)
                    {
                        foreach (XsiSlots entity2 in SlotsList)
                        {
                            #region Update Day Two
                            if (entity.StartTime.Value.ToString("HH:mm") == entity2.StartTime)
                            {
                                entity2.DayTwoId = entity.ItemId;
                                entity2.IsBreakDayTwo = entity.IsBreak;
                                entity2.Break2Title = entity.BreakTitle;
                                #region Day two slot count
                                PPSlotRegistrationService = new PPSlotRegistrationService();
                                where1 = new XsiExhibitionPpslotInvitation();
                                where1.RequestId = ppregid;
                                where1.SlotId = entity.ItemId;
                                where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                count1 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                                if (count1 < 1)
                                {
                                    PPSlotRegistrationService = new PPSlotRegistrationService();
                                    where1 = new XsiExhibitionPpslotInvitation();
                                    where1.ResponseId = ppregid;
                                    where1.SlotId = entity.ItemId;
                                    where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                    count2 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                                }
                                #endregion
                                sum = count1 + count2;
                                if (sum < 2)
                                    IsAvailableDayTwo = EnumConversion.ToString(EnumBool.Yes);
                                if (count1 == 1)
                                    IsAvailableDayTwo = EnumConversion.ToString(EnumBool.No);
                                entity2.IsAvailableDayTwo = IsAvailableDayTwo;
                                sum = 0; count1 = 0; count2 = 0; IsAvailableDayTwo = EnumConversion.ToString(EnumBool.No);
                                IsRepeat = true;
                                break;
                            }
                            else
                                IsRepeat = false;
                            #endregion
                        }
                        if (!IsRepeat)
                        {
                            #region Insert Day Two
                            #region Day two slot count
                            PPSlotRegistrationService = new PPSlotRegistrationService();
                            where1 = new XsiExhibitionPpslotInvitation();
                            where1.RequestId = ppregid;
                            where1.SlotId = entity.ItemId;
                            where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                            count1 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                            if (count1 < 1)
                            {
                                PPSlotRegistrationService = new PPSlotRegistrationService();
                                where1 = new XsiExhibitionPpslotInvitation();
                                where1.ResponseId = ppregid;
                                where1.SlotId = entity.ItemId;
                                where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                count2 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                            }
                            #endregion
                            sum = count1 + count2;
                            if (sum < 2)
                                IsAvailableDayTwo = EnumConversion.ToString(EnumBool.Yes);
                            if (count1 == 1)
                                IsAvailableDayTwo = EnumConversion.ToString(EnumBool.No);
                            //IsApprovedStatusDayTwo = PPSlotRegistrationService.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { SlotId = entity.ItemId, Status = "A" }).Count() > 0 ? "Y" : "N";
                            SlotsList.Add(
                           new XsiSlots()
                           {
                               ItemId = entity.ItemId,
                               DayTwoId = entity.ItemId,
                               StartTime = entity.StartTime.Value.ToString("HH:mm"),
                               EndTime = entity.EndTime.Value.ToString("HH:mm"),
                               IsBreakDayTwo = entity.IsBreak,
                               IsAvailableDayTwo = IsAvailableDayTwo,
                               Break2Title = entity.BreakTitle
                           });
                            #endregion
                        }
                    }
                    #endregion
                    #region Day Three
                    sum = 0; count1 = 0; count2 = 0;
                    IsRepeat = false;
                    foreach (XsiExhibitionProfessionalProgramSlots entity in ProfessionalProgramSlotList3)
                    {
                        foreach (XsiSlots entity3 in SlotsList)
                        {
                            #region Update Day Three
                            if (entity.StartTime.Value.ToString("HH:mm") == entity3.StartTime)
                            {
                                entity3.DayThreeId = entity.ItemId;
                                entity3.IsBreakDayThree = entity.IsBreak;
                                entity3.Break3Title = entity.BreakTitle;
                                #region Day Three slot count
                                PPSlotRegistrationService = new PPSlotRegistrationService();
                                where1 = new XsiExhibitionPpslotInvitation();
                                where1.RequestId = ppregid;
                                where1.SlotId = entity.ItemId;
                                where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                count1 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                                if (count1 < 1)
                                {
                                    PPSlotRegistrationService = new PPSlotRegistrationService();
                                    where1 = new XsiExhibitionPpslotInvitation();
                                    where1.ResponseId = ppregid;
                                    where1.SlotId = entity.ItemId;
                                    where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                    count2 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                                }
                                #endregion
                                sum = count1 + count2;
                                if (sum < 2)
                                    IsAvailableDayThree = EnumConversion.ToString(EnumBool.Yes);
                                if (count1 == 1)
                                    IsAvailableDayThree = EnumConversion.ToString(EnumBool.No);
                                entity3.IsAvailableDayThree = IsAvailableDayThree;
                                sum = 0; count1 = 0; count2 = 0; IsAvailableDayThree = EnumConversion.ToString(EnumBool.No);
                                IsRepeat = true;
                                break;
                            }
                            else
                                IsRepeat = false;
                            #endregion
                        }
                        if (!IsRepeat)
                        {
                            #region Insert Day Three
                            #region Day Three slot count
                            PPSlotRegistrationService = new PPSlotRegistrationService();
                            where1 = new XsiExhibitionPpslotInvitation();
                            where1.RequestId = ppregid;
                            where1.SlotId = entity.ItemId;
                            where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                            count1 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                            if (count1 < 1)
                            {
                                PPSlotRegistrationService = new PPSlotRegistrationService();
                                where1 = new XsiExhibitionPpslotInvitation();
                                where1.ResponseId = ppregid;
                                where1.SlotId = entity.ItemId;
                                where1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                count2 = PPSlotRegistrationService.GetPPSlotRegistration(where1).Count();
                            }
                            #endregion
                            sum = count1 + count2;
                            if (sum < 2)
                                IsAvailableDayThree = EnumConversion.ToString(EnumBool.Yes);
                            if (count1 == 1)
                                IsAvailableDayThree = EnumConversion.ToString(EnumBool.No);
                            //IsApprovedStatusDayThree = PPSlotRegistrationService.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { SlotId = entity.ItemId, Status = "A" }).Count() > 0 ? "Y" : "N";
                            SlotsList.Add(
                           new XsiSlots()
                           {
                               ItemId = entity.ItemId,
                               DayThreeId = entity.ItemId,
                               StartTime = entity.StartTime.Value.ToString("HH:mm"),
                               EndTime = entity.EndTime.Value.ToString("HH:mm"),
                               IsBreakDayThree = entity.IsBreak,
                               IsAvailableDayThree = IsAvailableDayThree,
                               Break2Title = entity.BreakTitle
                           });
                            #endregion
                        }
                    }
                    #endregion
                }
                //  SlotsListNew = SlotsList.Select(i => new { i.ItemId, i.DayOneId, i.DayTwoId, i.StartTime, i.EndTime, i.IsBreakDayOne, i.IsBreakDayTwo, i.IsAvailableDayOne, i.IsAvailableDayTwo, i.IsApprovedStatusDayOne, i.IsApprovedStatusDayTwo }).ToList();
                PPSlotRegistrationService PPSlotRegistrationService1 = new PPSlotRegistrationService();
                SlotsList = SlotsList.OrderBy(i => i.StartTime).ToList();
                if (SlotsList.Count() > 0)
                {
                    SlotsList = SlotsList.Select(i => new XsiSlots
                    {
                        ItemId = i.ItemId,
                        DayOneId = i.DayOneId,
                        DayTwoId = i.DayTwoId,
                        DayThreeId = i.DayThreeId,
                        StartTime = i.StartTime,
                        EndTime = i.EndTime,
                        IsBreakDayOne = i.IsBreakDayOne,
                        IsBreakDayTwo = i.IsBreakDayTwo,
                        IsBreakDayThree = i.IsBreakDayThree,
                        IsAvailableDayOne = i.IsAvailableDayOne,
                        IsAvailableDayTwo = i.IsAvailableDayTwo,
                        IsAvailableDayThree = i.IsAvailableDayThree,
                        IsApprovedStatusDayOne = PPSlotRegistrationService1.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { Status = "A", SlotId = i.DayOneId }).Where(p => p.RequestId == ppregid || p.ResponseId == ppregid).Count() > 0 ? "Y" : "N",
                        IsApprovedStatusDayTwo = PPSlotRegistrationService1.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { Status = "A", SlotId = i.DayTwoId }).Where(p => p.RequestId == ppregid || p.ResponseId == ppregid).Count() > 0 ? "Y" : "N",
                        IsApprovedStatusDayThree = PPSlotRegistrationService1.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { Status = "A", SlotId = i.DayThreeId }).Where(p => p.RequestId == ppregid || p.ResponseId == ppregid).Count() > 0 ? "Y" : "N",
                        Break1Title = i.Break1Title,
                        Break2Title = i.Break2Title,
                        Break3Title = i.Break3Title
                    }).OrderBy(i => i.StartTime).ToList();
                    if (SlotsList.Count() > 0)
                    {
                        // model.SlotsList = SlotsList; // TODO
                    }
                }
            }
            var PPEntity = MethodFactory.GetProfessionalProgram();
            if (PPEntity != null)
            {
                if (PPEntity.StartDateDayOne != null)
                    model.DayOne = PPEntity.StartDateDayOne.Value.ToString("dddd dd MMMM");
                if (PPEntity.StartDateDayTwo != null)
                    model.DayTwo = PPEntity.StartDateDayTwo.Value.ToString("dddd dd MMMM");
                if (PPEntity.StartDateDayThree != null)
                    model.DayThree = PPEntity.StartDateDayThree.Value.ToString("dddd dd MMMM");
            }

            return Ok(model);
        }
        [HttpGet]
        [Route("GetInviteeDetails/{itemid}/{slotId}/{inviteeppregid}")]
        public async Task<ActionResult<dynamic>> GetInviteeDetails(long itemid, long slotid, long inviteeppregid)
        {

            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            InviteeDetailsMessageDTO data = new InviteeDetailsMessageDTO();
            ProfessionalProgramSlotService ProfessionalProgramSlotService;
            PPSlotRegistrationService PPSlotRegistrationService;
            ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService;
            GenreService GenreService;
            XsiInvitee obj = new XsiInvitee();

            using (ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                obj.ItemId = itemid;
                XsiExhibitionProfessionalProgramRegistration entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(inviteeppregid);
                if (entity != null)
                {
                    #region Invitee Profile Info
                    if (entity.FileName != null)
                    {
                        if (System.IO.File.Exists(_appCustomSettings.ServerAddressNew + "/content/Uploads/PPRegistration/Avatar/" + entity.FileName))
                            obj.FileName = "/content/Uploads/PPRegistration/Avatar/" + entity.FileName;
                        else
                            obj.FileName = "/content/assets/images/profile-picture.jpg";
                    }
                    obj.MemberId = entity.ItemId;
                    if (entity.FirstName != null)
                        obj.Name = entity.FirstName;
                    if (entity.LastName != null)
                        obj.Name += " " + entity.LastName;
                    if (entity.CompanyName != null)
                        obj.Company = entity.CompanyName;

                    #region  Get GenreGroupIds From PP Genre
                    GenreService = new GenreService();
                    PPGenreService PPGenreService = new PPGenreService();
                    List<long> GenreIds = new List<long>();
                    GenreIds = PPGenreService.GetPPGenre(new XsiExhibitionPpgenre { ProfessionalProgramRegistrationId = entity.ItemId }).Where(i => i.GenreId != null).Select(i => i.GenreId.Value).ToList();

                    List<XsiGenre> GenreList = GenreService.GetGenre(new XsiGenre { IsActive = EnumConversion.ToString(EnumBool.Yes) }).Where(i => GenreIds.Contains(i.ItemId)).ToList();

                    if (LangId == 1)
                        obj.PPGenreTextList = GenreService.GetGenre(new XsiGenre() { IsActive = "Y" }).Where(i => i.Title != null && GenreIds.Contains(i.ItemId)).Select(x => x.Title).ToList();
                    else
                        obj.PPGenreTextList = GenreService.GetGenre(new XsiGenre() { IsActive = "Y" }).Where(i => i.Title != null && GenreIds.Contains(i.ItemId)).Select(x => x.TitleAr).ToList();

                    StringBuilder sb = new StringBuilder();
                    foreach (XsiGenre rows in GenreList)
                    {
                        if (rows.Title != null)
                            sb.Append(rows.Title + ", ");

                    }
                    if (sb.ToString().Length > 0)
                        obj.Genre = sb.ToString().Substring(0, sb.ToString().Length - 1);
                    #endregion
                    #endregion
                }
                PPSlotRegistrationService = new PPSlotRegistrationService();
                XsiExhibitionPpslotInvitation entity2 = PPSlotRegistrationService.GetPPSlotRegistrationByItemId(itemid);
                if (entity2 != null)
                    if (entity2.Message != null)
                    {
                        obj.Message = entity2.Message;
                        if (entity2.Status == "A")
                            obj.Meetup = "Table No: " + MethodFactory.GetHostTableNumber(entity2.ResponseId.Value) + " ";
                    }
                ProfessionalProgramSlotService = new ProfessionalProgramSlotService();
                XsiExhibitionProfessionalProgramSlots entity3 = ProfessionalProgramSlotService.GetProfessionalProgramSlotByItemId(slotid);
                if (entity3 != null && entity3.IsDelete == "N")
                {
                    if (entity3.StartTime != null)
                        obj.Meetup += entity3.StartTime.Value.ToString("dddd dd MMMM") + "  " + entity3.StartTime.Value.ToString("HH:mm");
                    if (entity3.EndTime != null)
                        obj.Meetup += " - " + entity3.EndTime.Value.ToString("HH:mm");
                    if (entity3.StartTime != null)
                        obj.SDate = entity3.StartTime.Value.ToString("MM/dd/yyyy HH:mm:ss");
                    if (entity3.EndTime != null)
                        obj.EDate = entity3.EndTime.Value.ToString("MM/dd/yyyy HH:mm:ss");
                }
            }

            if (obj.ItemId > 0)
            {
                data.Invitee = obj;
                data.MessageTypeResponse = "Success";
                return Ok(data);
            }
            else
            {
                data.MessageTypeResponse = "Error";
                data.Message = "Something went wrong. Please try again later.";
                return Ok(data);
            }
        }


        [HttpGet]
        [Route("SlotsForUserToRespond/{memid}/{slotid}")]
        public async Task<ActionResult<dynamic>> GetSlotsForUserToRespond(long memid, long slotid)
        {
            string str = string.Empty;
            using (PPSlotRegistrationService PPSlotRegistrationService = new PPSlotRegistrationService())
            {
                List<XsiExhibitionPpslotInvitation> PPSlotRegistrationList = PPSlotRegistrationService.GetPPSlotRegistration(EnumSortlistBy.ByItemIdDesc).ToList();
                List<XsiExhibitionPpslotInvitation> PPSlotRegistrationPendingList;
                List<XsiExhibitionPpslotInvitation> PPSlotRegistrationScheduledList;
                //PPSlotRegistrationList = PPSlotRegistrationList.Where(i => (i.RequestId == UCRegisteredId && (i.Status == "A" || i.Status == "R") && i.IsViewed == "N") || (i.ResponseId == UCRegisteredId && i.Status == "P" && i.IsViewed == "N")).OrderBy(i => i.CreatedOn).ToList();
                PPSlotRegistrationPendingList = PPSlotRegistrationList.Where(i => i.ResponseId == memid && i.Status == "P" && i.SlotId == slotid).OrderBy(i => i.CreatedOn).ToList();
                long kount = PPSlotRegistrationPendingList.Count();
                if (kount > 0)
                {
                    XsiExhibitionProfessionalProgramRegistration requestedentity;
                    foreach (var cols in PPSlotRegistrationPendingList)
                    {
                        ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService();
                        requestedentity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(cols.RequestId.Value);
                        str += "<a href='#' data-toggle='modal' data-slotid='" + cols.SlotId + "' data-transactionid='" + cols.ItemId + "' data-val='" + requestedentity.ItemId + "' class='left ShowMeetupPopup' data-target='#myModal'>" + requestedentity.FirstName + " " + requestedentity.LastName + "</a> ";
                    }
                }
                else
                {
                    PPSlotRegistrationScheduledList = PPSlotRegistrationList.Where(i => (i.RequestId == memid || i.ResponseId == memid) && i.Status == "A" && i.SlotId == slotid).OrderBy(i => i.CreatedOn).ToList();
                    kount = PPSlotRegistrationScheduledList.Count();
                    if (kount > 0)
                        str = "This slot is already scheduled.";
                }
            }
            return str;
        }
        #endregion

        #region Post Methods
        [HttpPost]
        [Route("SendMeetupRequest")]
        public async Task<ActionResult<dynamic>> PostSendMeetupRequest(SendMeetupRequest model)
        {
            string RejectedTimings = string.Empty;
            string ServerAddress = _appCustomSettings.ServerAddressNew;

            string ToEmail = string.Empty;
            long memberID = -1, languageID = 1;
            using (PPSlotRegistrationService PPSlotRegistrationService = new PPSlotRegistrationService())
            {
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                #region Delete Existing Slot
                if (model.PPSlotItemID != -1)
                {
                    XsiExhibitionPpslotInvitation entity = PPSlotRegistrationService.GetPPSlotRegistrationByItemId(model.PPSlotItemID);
                    if (entity != null)
                    {
                        if (entity.RequestId != null)
                        {
                            if (entity.RequestId == model.ResponseId)
                            {
                                ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService();
                                XsiExhibitionProfessionalProgramRegistration UserEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(model.RequestId);
                                ProfessionalProgramSlotService ProfessionalProgramSlotService = new ProfessionalProgramSlotService();
                                XsiExhibitionProfessionalProgramSlots SlotEntity = ProfessionalProgramSlotService.GetProfessionalProgramSlotByItemId(entity.SlotId.Value);
                                RejectedTimings = MethodFactory.GetTimeSlot(LangId, SlotEntity);
                                PPSlotRegistrationService.DeletePPSlotRegistration(model.PPSlotItemID);
                            }
                            else if (entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                            {
                                ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService();
                                XsiExhibitionProfessionalProgramRegistration UserEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(entity.RequestId.Value);
                                ProfessionalProgramSlotService ProfessionalProgramSlotService = new ProfessionalProgramSlotService();
                                XsiExhibitionProfessionalProgramSlots SlotEntity = ProfessionalProgramSlotService.GetProfessionalProgramSlotByItemId(entity.SlotId.Value);
                                RejectedTimings = MethodFactory.GetTimeSlot(LangId, SlotEntity);
                                PPSlotRegistrationService.DeletePPSlotRegistration(model.PPSlotItemID);
                            }
                        }
                    }
                }
                #endregion
                if (PPSlotRegistrationService.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { RequestId = model.RequestId, SlotId = model.SlotId, IsActive = EnumConversion.ToString(EnumBool.Yes) }).ToList().Count == 0)
                {
                    XsiExhibitionPpslotInvitation entity = new XsiExhibitionPpslotInvitation();
                    entity.RequestId = model.RequestId;
                    entity.ResponseId = model.ResponseId;
                    entity.SlotId = model.SlotId;
                    entity.Message = model.Message;
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    entity.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Pending);
                    entity.IsViewed = EnumConversion.ToString(EnumBool.No);
                    entity.InviteUrl = model.InviteURL;
                    entity.CreatedById = -1;
                    entity.CreatedOn = MethodFactory.ArabianTimeNow();
                    entity.ModifiedById = -1;
                    entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                    if (PPSlotRegistrationService.InsertPPSlotRegistration(entity) == EnumResultType.Success)
                    {
                        #region EMAIL NOTIFICATION for SLOT INVITATION

                        string strAdminEmailAddress = _appCustomSettings.AdminEmail;
                        string AdminName = _appCustomSettings.AdminName;
                        using (EmailContentService EmailContentService = new EmailContentService())
                        {
                            ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService();
                            ProfessionalProgramSlotService ProfessionalProgramSlotService = new ProfessionalProgramSlotService();

                            XsiExhibitionProfessionalProgramRegistration ResponseUserEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(model.ResponseId);
                            if (ResponseUserEntity != null)
                            {
                                #region Email Content Parameters For SIBF Publishers Conference Slot Invitation
                                if (ResponseUserEntity.Email != null)
                                    ToEmail = ResponseUserEntity.Email;
                                if (ResponseUserEntity.MemberId != null)
                                {
                                    memberID = ResponseUserEntity.MemberId.Value;
                                    languageID = 1;// MethodFactory.GetLanguageIDByMember(memberID);
                                }

                                XsiExhibitionProfessionalProgramRegistration InvitedByUserEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(model.RequestId);
                                string strInvitedBy = string.Empty;
                                if (languageID == 1)
                                {
                                    if (InvitedByUserEntity.FirstName != null && InvitedByUserEntity.LastName != null)
                                        strInvitedBy = InvitedByUserEntity.FirstName + " " + InvitedByUserEntity.LastName;
                                    else if (InvitedByUserEntity.FirstName != null)
                                        strInvitedBy = InvitedByUserEntity.FirstName;
                                    else if (InvitedByUserEntity.LastName != null)
                                        strInvitedBy = InvitedByUserEntity.LastName;
                                }
                                else if (languageID == 2)
                                {
                                    if (InvitedByUserEntity.FirstName != null && InvitedByUserEntity.LastName != null)
                                        strInvitedBy = InvitedByUserEntity.FirstName + " " + InvitedByUserEntity.LastName;
                                    else if (InvitedByUserEntity.FirstName != null)
                                        strInvitedBy = InvitedByUserEntity.FirstName;
                                    else if (InvitedByUserEntity.LastName != null)
                                        strInvitedBy = InvitedByUserEntity.LastName;
                                }
                                XsiExhibitionProfessionalProgramSlots SlotEntity = ProfessionalProgramSlotService.GetProfessionalProgramSlotByItemId(model.SlotId);
                                if (SlotEntity.IsDelete != "Y")
                                {
                                    string strSlot = MethodFactory.GetTimeSlot(1, SlotEntity);
                                    #endregion
                                    string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                                    StringBuilder body = new StringBuilder();
                                    XsiEmailContent emailContent = new XsiEmailContent();
                                    if (model.PPSlotItemID != -1)
                                        if (!string.IsNullOrEmpty(RejectedTimings))
                                            emailContent = EmailContentService.GetEmailContentByItemId(71);
                                        else
                                            emailContent = EmailContentService.GetEmailContentByItemId(69);
                                    else
                                        emailContent = EmailContentService.GetEmailContentByItemId(69);
                                    if (emailContent != null)
                                    {
                                        if (emailContent.Body != null)
                                            strEmailContentBody = emailContent.Body;
                                        if (emailContent.Email != null)
                                            strEmailContentEmail = emailContent.Email;
                                        if (emailContent.Subject != null)
                                            strEmailContentSubject = emailContent.Subject;
                                        strEmailContentAdmin = AdminName;

                                        body.Append(htmlContentFactory.BindEmailContentPP(LangId, strEmailContentBody, ResponseUserEntity, ServerAddress, 1, _appCustomSettings));
                                        body.Replace("$$slot$$", strSlot);
                                        body.Replace("$$inviteurl$$", model.InviteURL);
                                        body.Replace("$$invitedby$$", strInvitedBy);
                                        if (InvitedByUserEntity.TableId != null)
                                            body.Replace("$$tableid$$", InvitedByUserEntity.TableId.ToString());
                                        else
                                            body.Replace("$$tableid$$", string.Empty);
                                        if (model.PPSlotItemID != -1)
                                            if (!string.IsNullOrEmpty(RejectedTimings))
                                                body.Replace("$$RejectedSlot$$", RejectedTimings);
                                        if (!string.IsNullOrEmpty(ToEmail))
                                            _emailSender.SendEmail(strEmailContentAdmin, strAdminEmailAddress, ToEmail, strEmailContentSubject, body.ToString());
                                    }
                                }
                            }
                        }
                        #endregion
                        return true;
                    }
                }
                return false;
            }
        }

        #endregion

        /*
         [HttpGet]
        [Route("AddNewBooks/{memberid}")]
        public async Task<ActionResult<dynamic>> GetAddNewBooks(long memberid)
        {
            //#region For New Book
            //BindUserBooks();
            //hfSIBFMemberId.Value = SIBFMemberId.ToString();
            //hfRegId.Value = RegisteredId.ToString();
            //mvMyBooks.SetActiveView(vwAddNewBooks);
            //#endregion
            //hfCurrentTab.Value = "4";
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Function", "ShowPPMyBooksView()", true);
            //btnAddNewBooks.Visible = false;
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId == memberid)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    var ppUrl = CheckProfessionalProgram(memberId, LangId);
                    if (string.IsNullOrEmpty(ppUrl))
                    {
                        var model = BindUserBooks(memberId, LangId);
                        return Ok(model);
                    }
                    else
                        return Ok(ppUrl);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiCareers action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
        [HttpGet]
        [Route("EditBook/{bookid}")]
        public async Task<ActionResult<dynamic>> GetEditBook(long bookid)
        {
            //MethodFactory.BindProfessionalRegistrationGenre(ddlGenre1, EnglishId);
            //MethodFactory.BindExhibitionLanguages1(ddlSoldInOtherLanguage1, EnglishId);
            //btnUpdateBook.CommandArgument = e.CommandArgument.ToString();
            //LoadPPBook(bookid);
            // hfCurrentTab.Value = "4";
            // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Function", "ShowPPMyBooksView()", true);
            //if (hfCount.Value == "10")
            //   btnAddNewBooks.Visible = false;
            //else
            //  btnAddNewBooks.Visible = true;
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    var ppUrl = CheckProfessionalProgram(memberId, LangId);
                    if (string.IsNullOrEmpty(ppUrl))
                    {
                        var model = LoadPPBook(bookid);
                        return Ok(model);
                    }
                    else
                        return Ok(ppUrl);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiCareers action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
        [HttpGet]
        [Route("DeleteBook/{bookid}")]
        public async Task<ActionResult<dynamic>> GetDeleteBook(long bookid)
        {
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    var ppUrl = CheckProfessionalProgram(memberId, LangId);
                    if (string.IsNullOrEmpty(ppUrl))
                    {
                        try
                        {
                            using (ProfessionalProgramBookService ProfessionalProgramBookService = new ProfessionalProgramBookService())
                            {
                                XsiExhibitionProfessionalProgramBook Entity = ProfessionalProgramBookService.GetProfessionalProgramBookByItemId(bookid);
                                if (Entity != default(XsiExhibitionProfessionalProgramBook))
                                {
                                    PPBooksLanguageService PPBooksLanguageService = new PPBooksLanguageService();
                                    PPBooksLanguageService.DeletePPBooksLanguage(new XsiExhibitionPpbooksLanguage() { PpbookId = bookid });
                                    string LocationBookCoverPath = "/Content/Uploads/PPRegistration/Cover/";
                                    if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + LocationBookCoverPath + Entity.FileName))
                                        System.IO.File.Delete(_appCustomSettings.UploadsPhysicalPath + LocationBookCoverPath + Entity.FileName);
                                    ProfessionalProgramBookService.DeleteProfessionalProgramBook(bookid);
                                    return Ok("Book has been deleted successfully.");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            return Ok("An exception occured while deleting : " + ex.ToString());
                        }
                    }
                    else
                        return Ok(ppUrl);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiCareers action: {ex.InnerException}");
                return Ok(ex.Message);
            }
        }
         
        [HttpPost]
        [Route("UpdateBook")]
        public async Task<ActionResult<dynamic>> PostUpdateBook(MyBooks model)
        {
            try
            {
                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    var ppUrl = CheckProfessionalProgram(memberId, LangId);
                    if (string.IsNullOrEmpty(ppUrl))
                    {
                        long bookId = model.ItemId;
                        #region Update Professional Program Book
                        using (ProfessionalProgramBookService bookService = new ProfessionalProgramBookService())
                        {
                            XsiExhibitionProfessionalProgramBook book = new XsiExhibitionProfessionalProgramBook();
                            book = bookService.GetProfessionalProgramBookByItemId(bookId);
                            XsiExhibitionProfessionalProgramBook existingBook = new XsiExhibitionProfessionalProgramBook();
                            existingBook = bookService.GetProfessionalProgramBookByItemId(bookId);
                            if (book != default(XsiExhibitionProfessionalProgramBook))
                            {
                                //    book.ProfessionalProgramRegistrationId = ProfessionalProgramRegistrationId;
                                book.LanguageId = LangId;
                                book.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                book.AuthorName = model.AuthorName;
                                book.Title = model.Title;
                                book.GenreId = model.GenreId;
                                book.OtherGenre = model.OtherGenre;
                                book.Synopsis = model.Synopsis;
                                book.IsRightsAvailable = model.IsRightsAvailable;
                                book.IsOwnRights = model.IsOwnRights;
                                book.RightsOwner = model.RightsOwner;

                                //string UploadLocationMapFileName2 = string.Empty;

                                if (model.FileName != null && model.FileName.Length > 0)
                                {
                                    byte[] imageBytes;
                                    if (model.FileName.Contains("data:"))
                                    {
                                        var strInfo = model.FileName.Split(",")[0];
                                        imageBytes = Convert.FromBase64String(model.FileName.Split(',')[1]);
                                    }
                                    else
                                    {
                                        imageBytes = Convert.FromBase64String(model.FileName);
                                    }
                                    string FileName = string.Format("{0}_{1}{2}", LangId, MethodFactory.GetRandomNumber(), model.FileNameExt);
                                    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\temp\\") + FileName, imageBytes);

                                    MethodFactory.ResizeImage(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\temp\\" + FileName, 0, 282, _appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\Cover\\" + FileName);

                                    //ResizeSettings setting = new ResizeSettings() { Width = 200, Height = 282, Mode = FitMode.Crop };
                                    //ImageBuilder.Current.Build(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\temp\\" + FileName, _appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\Cover\\" + FileName, setting);

                                    if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\temp\\" + FileName))
                                        System.IO.File.Delete(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\temp\\" + FileName);

                                    if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\temp\\" + existingBook.FileName))
                                        System.IO.File.Delete(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\temp\\" + existingBook.FileName);

                                    if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\Cover\\" + existingBook.FileName))
                                        System.IO.File.Delete(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\Cover\\" + existingBook.FileName);


                                    book.FileName = FileName;
                                }
                                book.CreatedOn = MethodFactory.ArabianTimeNow();
                                book.ModifiedById = memberId;
                                book.ModifiedOn = MethodFactory.ArabianTimeNow();
                                if (bookService.UpdateProfessionalProgramBook(book) == EnumResultType.Success)
                                {
                                    #region other language
                                    PPBooksLanguageService PPBooksLanguageService = new PPBooksLanguageService();
                                    PPBooksLanguageService.DeletePPBooksLanguage(new XsiExhibitionPpbooksLanguage() { PpbookId = bookId });
                                    XsiExhibitionPpbooksLanguage entity1;
                                    foreach (var item in model.LanguageId)
                                    {
                                        entity1 = new XsiExhibitionPpbooksLanguage();
                                        entity1.PpbookId = bookId;
                                        entity1.LanguageId = item;
                                        PPBooksLanguageService.InsertPPBooksLanguage(entity1);
                                    }
                                    #endregion
                                    //BindUserBooks();
                                    //mvMyBooks.SetActiveView(vwMyBookDetails);
                                    //divMessage.Attributes.Add("class", "alert-bar-success");
                                    //ltrBooksStatusMessage.Text = "Book Updated Successfully.";
                                    return Ok("Book Updated Successfully.");
                                }
                                else
                                {
                                    return Ok("An error occured while updating book.");
                                }
                            }
                            else
                            {
                                return NotFound("Book not available for updating.");
                            }
                        }
                        #endregion
                    }
                    else
                        return Ok(ppUrl);
                }
                return Unauthorized("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiCareers action: {ex.InnerException}");
                return Ok(ex.Message);
            }
        }
         */

        #region Helper Method
        string CheckProfessionalProgram(long memberId, long langId)
        {
            string ReturnUrl = _appCustomSettings.ServerAddressNew + "en/ProfessionalProgram?websiteId=" + Convert.ToInt64(EnumWebsiteId.SIBF);
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                XsiExhibitionProfessionalProgramRegistration entity = MethodFactory.VerifyProfessionalProgramRegistration(memberId);
                if (entity != default(XsiExhibitionProfessionalProgramRegistration))
                {
                    if (entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                        ReturnUrl = string.Empty;
                }
            }
            return ReturnUrl;
        }
        MyScheduleDTO BindCustomSchedule(long memberId)
        {
            MyScheduleDTO model = new MyScheduleDTO();
            string strApproved = EnumConversion.ToString(EnumProfessionalProgramStatus.Approved);
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                var RegisteredId = MethodFactory.GetRegisteredId(memberId);
                model.MemberId = memberId;
                model.PPRegId = RegisteredId;
                PPSlotRegistrationService PPSlotRegistrationService = new PPSlotRegistrationService();
                XsiExhibitionPpslotInvitation where = new XsiExhibitionPpslotInvitation();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                var SlotRegistrationList1 = PPSlotRegistrationService.GetPPSlotRegistration(where).ToList();
                var CustomScheduleList = new List<CustomScheduleDTO>();
                var RegEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(RegisteredId);
                if (RegEntity != null)
                {
                    if (RegEntity.IsHopitalityPackage == "Y")
                    {
                        model.IsHopitalityPackage = "Y";
                    }
                    else
                    {
                        model.IsHopitalityPackage = "N";
                    }
                }
                var SlotRegistrationList2 = SlotRegistrationList1.Where(i => i.RequestId == RegisteredId || i.ResponseId == RegisteredId).Select(s => new { s.ItemId, s.SlotId }).ToList();
                HashSet<long?> PPSlotRegistrationSlotID = new HashSet<long?>(SlotRegistrationList1.Where(i => (i.RequestId == RegisteredId || i.ResponseId == RegisteredId) && i.Status == strApproved).Select(s => s.SlotId).ToList());

                #region Get Main Slots List
                ProfessionalProgramSlotService ProfessionalProgramSlotService = new ProfessionalProgramSlotService();
                XsiExhibitionProfessionalProgramSlots where4 = new XsiExhibitionProfessionalProgramSlots();
                where4.IsDelete = EnumConversion.ToString(EnumBool.No);
                var PPEntity = MethodFactory.GetProfessionalProgram();
                if (PPEntity != null)
                {
                    if (PPEntity.StartDateDayOne != null)
                        model.DayOne = PPEntity.StartDateDayOne.Value.ToString("dddd dd MMMM");
                    if (PPEntity.StartDateDayTwo != null)
                        model.DayTwo = PPEntity.StartDateDayTwo.Value.ToString("dddd dd MMMM");
                    if (PPEntity.StartDateDayThree != null)
                        model.DayThree = PPEntity.StartDateDayThree.Value.ToString("dddd dd MMMM");
                    where4.ProfessionalProgramId = PPEntity.ProfessionalProgramId;

                    List<XsiExhibitionProfessionalProgramSlots> PPSlotList = ProfessionalProgramSlotService.GetProfessionalProgramSlot(where4);
                    #endregion
                    foreach (var cols in PPSlotList)
                    {
                        //var SlotInfo = GetSlotsInfo(cols.ItemId, cols.DayId.Value, RegisteredId);
                        if (cols.DayId == 1)
                            CustomScheduleList.Add(new CustomScheduleDTO() { SlotDayOne = cols.ItemId, StartTime = cols.StartTime.Value.ToString("HH:mm"), EndTime = cols.EndTime.Value.ToString("HH:mm") });
                        else if (cols.DayId == 2)
                            CustomScheduleList.Add(new CustomScheduleDTO() { SlotDayTwo = cols.ItemId, StartTime = cols.StartTime.Value.ToString("HH:mm"), EndTime = cols.EndTime.Value.ToString("HH:mm") });
                        else if (cols.DayId == 3)
                            CustomScheduleList.Add(new CustomScheduleDTO() { SlotDayThree = cols.ItemId, StartTime = cols.StartTime.Value.ToString("HH:mm"), EndTime = cols.EndTime.Value.ToString("HH:mm") });

                        #region Matching Records updating above list "CustomScheduleList"
                        var slotIds = PPSlotList.Where(x => x.DayId != cols.DayId && x.StartTime.Value.ToString("HH:mm") == cols.StartTime.Value.ToString("HH:mm")).ToList();
                        foreach (var item in slotIds)
                        {
                            if (item.DayId == 1)
                            {
                                foreach (CustomScheduleDTO row in CustomScheduleList)
                                {
                                    if (row.StartTime == item.StartTime.Value.ToString("HH:mm"))
                                    {
                                        row.SlotDayOne = item.ItemId;
                                        break;
                                    }
                                }
                            }
                            else if (item.DayId == 2)
                            {
                                foreach (CustomScheduleDTO row in CustomScheduleList)
                                {
                                    if (row.StartTime == item.StartTime.Value.ToString("HH:mm"))
                                    {
                                        row.SlotDayTwo = item.ItemId;
                                        break;
                                    }
                                }
                            }
                            else if (item.DayId == 3)
                            {
                                foreach (CustomScheduleDTO row in CustomScheduleList)
                                {
                                    if (row.StartTime == item.StartTime.Value.ToString("HH:mm"))
                                    {
                                        row.SlotDayThree = item.ItemId;
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    var CustomScheduleList1 = new List<CustomScheduleDTO>();
                    model.CustomScheduleList = CustomScheduleList1;
                    foreach (CustomScheduleDTO cols in CustomScheduleList)
                    {
                        if (!CustomScheduleList1.Any(p => p.StartTime == cols.StartTime))
                        {
                            var schedule = CustomScheduleList.Where(p => p.StartTime == cols.StartTime).FirstOrDefault();
                            var dayOneSlotInfo = GetSlotsInfo(schedule.SlotDayOne, 1, RegisteredId);
                            var dayTwoSlotInfo = GetSlotsInfo(schedule.SlotDayTwo, 2, RegisteredId);
                            var dayThreeSlotInfo = GetSlotsInfo(schedule.SlotDayThree, 3, RegisteredId);
                            #region Updating CustomScheduleList
                            if (dayOneSlotInfo != null)
                            {
                                schedule.SlotDayOneText = dayOneSlotInfo.DisplayNameAndTableNo;
                                schedule.SlotDayOneClass = dayOneSlotInfo.SlotDayClass;
                                schedule.DayOneStatus = dayOneSlotInfo.Status;
                                schedule.DayOneSentInvites = dayOneSlotInfo.SentInvites;
                                schedule.DayOneSlotId = dayOneSlotInfo.SlotId;
                                schedule.DayOneTransactionId = dayOneSlotInfo.TransactionId;
                                schedule.DayOneRequestId = dayOneSlotInfo.RequestId;
                                schedule.DayOneRegisteredEntityTwo = dayOneSlotInfo.RegisteredEntityTwo;
                                schedule.DayOneCount = dayOneSlotInfo.Count;
                            }
                            if (dayTwoSlotInfo != null)
                            {
                                schedule.SlotDayTwoText = dayTwoSlotInfo.DisplayNameAndTableNo;
                                schedule.SlotDayTwoClass = dayTwoSlotInfo.SlotDayClass;
                                schedule.DayTwoStatus = dayTwoSlotInfo.Status;
                                schedule.DayTwoSentInvites = dayTwoSlotInfo.SentInvites;
                                schedule.DayTwoSlotId = dayTwoSlotInfo.SlotId;
                                schedule.DayTwoTransactionId = dayTwoSlotInfo.TransactionId;
                                schedule.DayTwoRequestId = dayTwoSlotInfo.RequestId;
                                schedule.DayTwoRegisteredEntityTwo = dayTwoSlotInfo.RegisteredEntityTwo;
                                schedule.DayTwoCount = dayTwoSlotInfo.Count;
                            }
                            if (dayThreeSlotInfo != null)
                            {
                                schedule.SlotDayThreeText = dayThreeSlotInfo.DisplayNameAndTableNo;
                                schedule.SlotDayThreeClass = dayThreeSlotInfo.SlotDayClass;
                                schedule.DayThreeStatus = dayThreeSlotInfo.Status;
                                schedule.DayThreeSentInvites = dayThreeSlotInfo.SentInvites;
                                schedule.DayThreeSlotId = dayThreeSlotInfo.SlotId;
                                schedule.DayThreeTransactionId = dayThreeSlotInfo.TransactionId;
                                schedule.DayThreeRequestId = dayThreeSlotInfo.RequestId;
                                schedule.DayThreeRegisteredEntityTwo = dayThreeSlotInfo.RegisteredEntityTwo;
                                schedule.DayThreeCount = dayThreeSlotInfo.Count;
                            }
                            #endregion
                            CustomScheduleList1.Add(schedule);
                        }
                    }

                    var PrintList = CustomScheduleList1.Where(p => PPSlotRegistrationSlotID.Contains(p.SlotDayOne) || PPSlotRegistrationSlotID.Contains(p.SlotDayTwo)).OrderBy(i => i.StartTime).ToList();

                    if (PrintList.Count > 0)
                        model.IsShowPDF = true;
                    else
                        model.IsShowPDF = false;
                    if (CustomScheduleList1.Count > 0)
                    {
                        model.CustomScheduleList = CustomScheduleList1.OrderBy(i => i.StartTime).ToList();
                    }
                    else
                    {
                        model.Message = "No meetups.";
                    }
                }
                else
                {
                    model.Message = "No meetups.";
                }
            }
            return model;
        }
        FindProfessionalDTO BindItems(long SIBFMemberId, FindProfessionalSearch model, long langId)
        {
            var RegisteredId = MethodFactory.GetRegisteredId(SIBFMemberId);
            long ProfessionalProgramId = -1;
            var entity = MethodFactory.GetProfessionalProgram();
            if (entity != null)
                ProfessionalProgramId = entity.ProfessionalProgramId;
            FindProfessionalDTO ReturnModel = new FindProfessionalDTO();
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                List<long> ProfessionalSuggestionIds = new List<long>();
                long suggestionCount = 0;

                #region Suggested Professionals
                if (model.IsSuggestions)
                {
                    //XsiExhibitionProfessionalProgramRegistration wherePPRegistration = new XsiExhibitionProfessionalProgramRegistration();
                    //wherePPRegistration.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    //wherePPRegistration.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Approved);
                    //wherePPRegistration.ProfessionalProgramId = ProfessionalProgramId;
                    //wherePPRegistration.ItemId = RegisteredId;
                    //var PPRegistrationSuggestionList = ProfessionalProgramRegistrationService.GetSuggestedProfessionalProgramRegistration(wherePPRegistration);
                    //if (PPRegistrationSuggestionList.Count() > 0)
                    //{
                    //    ProfessionalSuggestionIds = PPRegistrationSuggestionList.OrderBy(x => Guid.NewGuid()).Take(3).Select(i => i.ItemId).ToList();
                    //    suggestionCount = ProfessionalSuggestionIds.Count;
                    //    var list = PPRegistrationSuggestionList.OrderBy(x => Guid.NewGuid()).Take(3).ToList();
                    //    ReturnModel.SuggestionList = GetFindProfessionalsList(list, entity); ;
                    // }
                }
                #endregion
                #region Other Professionals
                List<XsiExhibitionProfessionalProgramRegistration> PPRegistrationList = new List<XsiExhibitionProfessionalProgramRegistration>();
                XsiExhibitionProfessionalProgramRegistration where2 = new XsiExhibitionProfessionalProgramRegistration();
                where2.IsActive = EnumConversion.ToString(EnumBool.Yes);
                where2.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Approved);
                where2.ProfessionalProgramId = ProfessionalProgramId;
                //where2.ItemId = RegisteredId;
                #endregion

                PPRegistrationList = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistration(where2)
                    .Where(i => i.ItemId != RegisteredId).ToList();


                //if (!string.IsNullOrEmpty(name) && !name.IsDefault())
                //{
                //    predicateOr = predicateOr.Or(p => (p.FirstName + " " + p.LastName).Contains(name));
                //    predicateOr = predicateOr.Or(p => (p.FirstNameAr + " " + p.LastNameAr).Contains(name));
                //    predicatePPRegistration = predicatePPRegistration.And(predicateOr.Expand());
                //}



                if (model.CountryId != null && model.CountryId.Count > 0 && model.CountryId[0] != 0)
                {
                    if (model.CountryId.Count == 1)
                        PPRegistrationList = PPRegistrationList.Where(x => x.CountryId != null && model.CountryId[0] == x.CountryId.Value).ToList();
                    else
                        PPRegistrationList = PPRegistrationList.Where(x => x.CountryId != null && model.CountryId.Contains(x.CountryId.Value)).ToList();
                }
                if (model.GenreIds != null && model.GenreIds.Count > 0 && model.GenreIds[0] != 0)
                {
                    using (PPGenreService PPGenreService = new PPGenreService())
                    {
                        var registrationIds = new HashSet<long>(PPGenreService.GetPPGenre(new XsiExhibitionPpgenre()).Where(x => x.ProfessionalProgramRegistrationId != null && x.GenreId != null && model.GenreIds.Contains(x.GenreId.Value)).Select(p => p.ProfessionalProgramRegistrationId.Value).Distinct().ToList());

                        //  var registrationIds = ProfessionalProgramBookService.GetProfessionalProgramBook(new XsiExhibitionProfessionalProgramBook() { LanguageId = langId, IsActive = EnumConversion.ToString(EnumBool.Yes) }).Where(i => i.ProfessionalProgramRegistrationId != null && PPBookIds.Contains(i.ItemId)).Select(i => i.ProfessionalProgramRegistrationId).ToList();
                        //  var registrationIds = new HashSet<long>(list.Where(x => x.ProfessionalProgramRegistrationId != null && PPBookIds.Contains(x.ProfessionalProgramRegistrationId)).Select(x => x.ProfessionalProgramRegistrationId.Value).ToList());
                        PPRegistrationList = PPRegistrationList.Where(x => registrationIds.Contains(x.ItemId)).ToList();
                    }
                }
                if (model.LanguageIds != null && model.LanguageIds.Count > 0 && model.LanguageIds[0] != 0)
                {
                    using (PPBooksLanguageService PPBooksLanguageService = new PPBooksLanguageService())
                    {
                        var PPBookIds = new HashSet<long>(PPBooksLanguageService.GetPPBooksLanguage(new XsiExhibitionPpbooksLanguage()).Where(x => x.PpbookId != null && model.LanguageIds.Contains(x.LanguageId.Value)).Select(p => p.PpbookId.Value).ToList());
                        using (ProfessionalProgramBookService ProfessionalProgramBookService = new ProfessionalProgramBookService())
                        {
                            var registrationIds = ProfessionalProgramBookService.GetProfessionalProgramBook(new XsiExhibitionProfessionalProgramBook() { LanguageId = langId, IsActive = EnumConversion.ToString(EnumBool.Yes) }).Where(i => i.ProfessionalProgramRegistrationId != null && PPBookIds.Contains(i.ItemId)).Select(i => i.ProfessionalProgramRegistrationId).ToList();
                            //  var registrationIds = new HashSet<long>(list.Where(x => x.ProfessionalProgramRegistrationId != null && PPBookIds.Contains(x.ProfessionalProgramRegistrationId)).Select(x => x.ProfessionalProgramRegistrationId.Value).ToList());
                            PPRegistrationList = PPRegistrationList.Where(x => registrationIds.Contains(x.ItemId)).ToList();
                        }
                    }
                }
                long count = PPRegistrationList.Count;
                if (count > 0)
                {
                    //model.pageSize.Value
                    //if (!string.IsNullOrEmpty(model.Name))
                    //{
                    //    PPRegistrationList = PPRegistrationList.Where(p =>
                    //        (p.FirstName + " " + p.LastName).Contains(model.Name, StringComparison.OrdinalIgnoreCase) ||
                    //        (p.FirstNameAr + " " + p.LastNameAr).Contains(model.Name) ||
                    //        p.CompanyName.Contains(model.Name, StringComparison.OrdinalIgnoreCase) || p.CompanyNameAr.Contains(model.Name)).ToList();
                    //}

                    if (!string.IsNullOrEmpty(model.Name))
                    {
                        model.Name = model.Name.ToLower();
                        var likeExpression = "%" + model.Name + "%";
                        PPRegistrationList = PPRegistrationList.Where(i => EF.Functions.Like(i.CompanyName, likeExpression)
                        || EF.Functions.Like(i.CompanyNameAr, likeExpression)
                        || EF.Functions.Like(i.FirstName, likeExpression)
                        || EF.Functions.Like(i.LastName, likeExpression)
                        || EF.Functions.Like(i.FirstNameAr, likeExpression)
                        || EF.Functions.Like(i.LastNameAr, likeExpression)
                        ).ToList();
                        // i.GuestNameAr.StartsWith(userdto.Strletter));
                    }

                    PPRegistrationList = PPRegistrationList.Skip((model.pageNumber.Value - 1) * model.pageSize.Value).Take(model.pageSize.Value).ToList();
                    ReturnModel.MainList = GetFindProfessionalsList(PPRegistrationList, entity);
                }
                else
                {
                    ReturnModel.Message = "Not found";
                }
                ReturnModel.Count = count + suggestionCount;
            }
            return ReturnModel;
        }

        FindProfessionalDTO BindItemsOld(long SIBFMemberId, FindProfessionalSearch model, long langId)
        {
            var RegisteredId = MethodFactory.GetRegisteredId(SIBFMemberId);
            long ProfessionalProgramId = -1;
            var entity = MethodFactory.GetProfessionalProgram();
            if (entity != null)
                ProfessionalProgramId = entity.ProfessionalProgramId;
            FindProfessionalDTO ReturnModel = new FindProfessionalDTO();
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                List<long> ProfessionalSuggestionIds = new List<long>();
                long suggestionCount = 0;

                #region Suggested Professionals
                if (model.IsSuggestions)
                {
                    //XsiExhibitionProfessionalProgramRegistration wherePPRegistration = new XsiExhibitionProfessionalProgramRegistration();
                    //wherePPRegistration.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    //wherePPRegistration.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Approved);
                    //wherePPRegistration.ProfessionalProgramId = ProfessionalProgramId;
                    //wherePPRegistration.ItemId = RegisteredId;
                    //var PPRegistrationSuggestionList = ProfessionalProgramRegistrationService.GetSuggestedProfessionalProgramRegistration(wherePPRegistration);
                    //if (PPRegistrationSuggestionList.Count() > 0)
                    //{
                    //    ProfessionalSuggestionIds = PPRegistrationSuggestionList.OrderBy(x => Guid.NewGuid()).Take(3).Select(i => i.ItemId).ToList();
                    //    suggestionCount = ProfessionalSuggestionIds.Count;
                    //    var list = PPRegistrationSuggestionList.OrderBy(x => Guid.NewGuid()).Take(3).ToList();
                    //    ReturnModel.SuggestionList = GetFindProfessionalsList(list, entity); ;
                    // }
                }
                #endregion
                #region Other Professionals
                List<XsiExhibitionProfessionalProgramRegistration> PPRegistrationList = new List<XsiExhibitionProfessionalProgramRegistration>();
                XsiExhibitionProfessionalProgramRegistration where2 = new XsiExhibitionProfessionalProgramRegistration();
                where2.IsActive = EnumConversion.ToString(EnumBool.Yes);
                where2.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Approved);
                where2.ProfessionalProgramId = ProfessionalProgramId;
                where2.ItemId = RegisteredId;
                #endregion
                if (!string.IsNullOrEmpty(model.Name) && model.GenreIds.Count > 0 && model.GenreIds[0] != 0)
                    PPRegistrationList = ProfessionalProgramRegistrationService.GetOtherProfessionalsSearch(model.GenreIds, ProfessionalSuggestionIds, where2, model.Name.ToLower().Trim());
                else
                {
                    if (!string.IsNullOrEmpty(model.Name))
                        PPRegistrationList = ProfessionalProgramRegistrationService.GetOtherProfessionalsSearch(where2, model.Name.ToLower().Trim());
                    else
                        PPRegistrationList = ProfessionalProgramRegistrationService.GetOtherProfessionalsSearch(null, ProfessionalSuggestionIds, where2, model.Name);
                }

                if (model.CountryId != null && model.CountryId.Count > 0 && model.CountryId[0] != 0)
                {
                    PPRegistrationList = PPRegistrationList.Where(x => model.CountryId.Contains(x.CountryId.Value)).ToList();
                }
                if (model.LanguageIds != null && model.LanguageIds.Count > 0 && model.LanguageIds[0] != 0)
                {
                    using (PPBooksLanguageService PPBooksLanguageService = new PPBooksLanguageService())
                    {
                        var PPBookIds = new HashSet<long>(PPBooksLanguageService.GetPPBooksLanguage(new XsiExhibitionPpbooksLanguage()).Where(x => x.PpbookId != null && model.LanguageIds.Contains(x.LanguageId.Value)).Select(p => p.PpbookId.Value).ToList());
                        using (ProfessionalProgramBookService ProfessionalProgramBookService = new ProfessionalProgramBookService())
                        {
                            var list = ProfessionalProgramBookService.GetProfessionalProgramBook(new XsiExhibitionProfessionalProgramBook() { LanguageId = langId, IsActive = EnumConversion.ToString(EnumBool.Yes) }).ToList();
                            var registrationIds = new HashSet<long>(list.Where(x => x.ProfessionalProgramRegistrationId != null && PPBookIds.Contains(x.ItemId)).Select(x => x.ProfessionalProgramRegistrationId.Value).ToList());
                            PPRegistrationList = PPRegistrationList.Where(x => registrationIds.Contains(x.ItemId)).ToList();
                        }
                    }
                }
                long count = PPRegistrationList.Count;
                if (count > 0)
                {
                    //model.pageSize.Value
                    PPRegistrationList = PPRegistrationList.Skip((model.pageNumber.Value - 1) * model.pageSize.Value).Take(200).ToList();
                    ReturnModel.MainList = GetFindProfessionalsList(PPRegistrationList, entity);
                }
                else
                {
                    ReturnModel.Message = "Not found";
                }
                ReturnModel.Count = count + suggestionCount;
            }
            return ReturnModel;
        }
        string AssignAvatar(string FileName, string Title)
        {
            string LocationMapPath = _appCustomSettings.UploadsCMSPath + "/PPRegistration/";
            string imagePath = "";
            if (!string.IsNullOrEmpty(FileName))
            {
                if (System.IO.File.Exists(LocationMapPath + "Avatar/" + FileName))
                    imagePath = LocationMapPath + "Avatar/" + FileName;
                else
                {
                    imagePath = _appCustomSettings.ServerAddressCMS + "/Content/assets/images/avatarmale.png";
                    if (!string.IsNullOrEmpty(Title))
                        if (Title == "F")
                            imagePath = _appCustomSettings.ServerAddressCMS + "/Content/assets/images/avatarfemale.png";
                }
            }
            else
            {
                imagePath = _appCustomSettings.ServerAddressCMS + "/Content/assets/images/avatarmale.png";
                if (!string.IsNullOrEmpty(Title))
                    if (Title == "F")
                        imagePath = _appCustomSettings.ServerAddressCMS + "/Content/assets/images/avatarfemale.png";
            }
            return imagePath;
        }
        List<FindProfessional> GetFindProfessionalsList(List<XsiExhibitionProfessionalProgramRegistration> list, XsiExhibitionProfessionalProgram entity)
        {
            List<FindProfessional> professionalList = new List<FindProfessional>();
            foreach (var item in list)
            {
                string strLastName = string.Empty;
                FindProfessional findProfessional = new FindProfessional();
                findProfessional.ItemId = item.ItemId;
                findProfessional.Image = AssignAvatar(item.FileName, item.Title);

                if (!string.IsNullOrEmpty(item.FirstName))
                    findProfessional.Name = item.FirstName;
                else if (!string.IsNullOrEmpty(item.FirstNameAr))
                    findProfessional.Name = item.FirstNameAr;
                if (!string.IsNullOrEmpty(item.LastName))
                    strLastName = " " + item.LastName;
                else if (!string.IsNullOrEmpty(item.LastNameAr))
                    strLastName = " " + item.LastNameAr;
                findProfessional.Name += strLastName;
                if (entity != null)
                {
                    if (entity.StartDateDayOne != null)
                        findProfessional.DayOne = entity.StartDateDayOne.Value.ToString("dddd dd MMMM");
                    if (entity.StartDateDayTwo != null)
                        findProfessional.DayTwo = entity.StartDateDayTwo.Value.ToString("dddd dd MMMM");
                }
                if (!string.IsNullOrEmpty(item.CompanyName))
                    findProfessional.CompanyName = item.CompanyName;
                else if (!string.IsNullOrEmpty(item.CompanyNameAr))
                    findProfessional.CompanyName = item.CompanyNameAr;

                findProfessional.IsScheduleMeetup = MethodFactory.IsAppointmentDateAvailable(item.ProfessionalProgramId.Value);
                professionalList.Add(findProfessional);
            }
            return professionalList;
        }
        ExhibitionProfessionalProgramRegistrationDTO LoadUserProfile(long ppregid, bool isotherprofile = false, long langid = 1)
        {
            ExhibitionProfessionalProgramRegistrationDTO model = new ExhibitionProfessionalProgramRegistrationDTO();
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                XsiExhibitionProfessionalProgramRegistration entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(ppregid);
                if (entity != null)
                {
                    model.ItemId = entity.ItemId;
                    //form related 
                    if (entity.ProfessionalProgramId != null)
                        model.ProfessionalProgramId = entity.ProfessionalProgramId.Value;
                    model.Type = entity.Type;
                    model.Title = entity.Title;
                    if (entity.TableId != null)
                        model.TableId = entity.TableId.Value;
                    model.FirstName = entity.FirstName;
                    model.FirstNameAr = entity.FirstNameAr;
                    model.LastName = entity.LastName;
                    model.LastNameAr = entity.LastNameAr;
                    model.Email = entity.Email;
                    if (entity.CountryId != null)
                        model.CountryId = entity.CountryId.Value;
                    if (entity.CityId != null)
                        model.CityId = entity.CityId.Value;
                    model.OtherCity = entity.OtherCity;
                    #region Phone,Mobile
                    if (entity.PhoneNumber != null)
                    {
                        string[] str = entity.PhoneNumber.Split('$');
                        if (str.Count() == 3)
                        {
                            model.PhoneISD = str[0].Trim();
                            model.PhoneSTD = str[1].Trim();
                            model.Phone = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            model.PhoneSTD = str[0].Trim();
                            model.Phone = str[1].Trim();
                        }
                        else
                            model.Phone = str[0].Trim();
                    }
                    if (entity.MobileNumber != null)
                    {
                        string[] str = entity.MobileNumber.Split('$');
                        if (str.Count() == 3)
                        {
                            model.MobileISD = str[0].Trim();
                            model.MobileSTD = str[1].Trim();
                            model.Mobile = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            model.MobileSTD = str[0].Trim();
                            model.Mobile = str[1].Trim();
                        }
                        else
                            model.Mobile = str[0].Trim();
                    }
                    #endregion
                    model.CompanyName = entity.CompanyName;
                    model.CompanyNameAr = entity.CompanyNameAr;
                    model.JobTitle = entity.JobTitle;
                    model.JobTitleAr = entity.JobTitleAr;
                    model.WorkProfile = entity.WorkProfile;
                    model.WorkProfileAr = entity.WorkProfileAr;
                    model.Website = entity.Website;
                    model.SocialLinkUrl = entity.SocialLinkUrl;
                    model.IsAttended = entity.IsAttended;
                    model.HowManyTimes = entity.IsAttended == EnumConversion.ToString(EnumBool.Yes) ? entity.HowManyTimes : string.Empty;
                    model.ReasonToAttend = entity.ReasonToAttend;
                    if (entity.FileName != null)
                    {
                        string LocationMapPath = _appCustomSettings.UploadsCMSPath + "/PPRegistration/";
                        if (System.IO.File.Exists(LocationMapPath + "Avatar/" + entity.FileName))
                            model.FileName = LocationMapPath + "Avatar/" + entity.FileName;
                        else if (entity.Title != null)
                        {
                            if (entity.Title == "M")
                                model.FileName = _appCustomSettings.UploadsCMSPath + "/Content/assets/images/avatarmale.png";
                            else
                                model.FileName = _appCustomSettings.UploadsCMSPath + "/Content/assets/images/avatarfemale.png";
                        }
                    }
                    else
                    {
                        if (entity.Title == "M")
                            model.FileName = _appCustomSettings.UploadsCMSPath + "/Content/assets/images/avatarmale.png";
                        else
                            model.FileName = _appCustomSettings.UploadsCMSPath + "/Content/assets/images/avatarfemale.png";
                    }
                    #region Load Genre GroupId Tags
                    GenreService GenreService = new GenreService();

                    List<long> PPGenreList = new List<long>();
                    PPGenreService PPGenreService = new PPGenreService();
                    XsiExhibitionPpgenre ppgenreenttiy = new XsiExhibitionPpgenre();
                    ppgenreenttiy.ProfessionalProgramRegistrationId = ppregid;
                    PPGenreList = PPGenreService.GetPPGenre(ppgenreenttiy).Where(i => i.GenreId != null).Select(x => x.GenreId.Value).ToList();

                    if (isotherprofile)
                    {
                        if (langid == 1)
                            model.PPGenreTextList = GenreService.GetGenre(new XsiGenre() { IsActive = "Y" }).Where(i => i.Title != null && PPGenreList.Contains(i.ItemId)).Select(x => x.Title).ToList();
                        else
                            model.PPGenreTextList = GenreService.GetGenre(new XsiGenre() { IsActive = "Y" }).Where(i => i.Title != null && PPGenreList.Contains(i.ItemId)).Select(x => x.TitleAr).ToList();
                    }


                    model.PPGenreList = PPGenreList;
                    model.ProfessionalProgramBookList = MethodFactory.GetProfessionalProgramBookList(entity.ItemId);
                    #endregion
                    model.OtherGenre = entity.OtherGenre;
                    model.TradeType = entity.TradeType;
                    model.IsEbooks = entity.IsEbooks;
                    model.ExamplesOfTranslatedBooks = entity.ExamplesOfTranslatedBooks;
                    model.IsInterested = entity.IsInterested;
                    //if (entity.IsInterested == EnumConversion.ToString(EnumBool.Yes))
                    //{
                    //    rbtnInterestYes.Checked = true;
                    //    rbtnInterestNo.Checked = false;
                    //    model.MoreInfo.Enabled = true;
                    //    model.MoreInfo.Attributes.Add("style", "background-color:#fff");
                    //}
                    //else
                    //{
                    //    rbtnInterestYes.Checked = false;
                    //    rbtnInterestNo.Checked = true;
                    //    model.MoreInfo.Enabled = false;
                    //    model.MoreInfo.Attributes.Add("style", "background-color:#e8e8e8");
                    //}
                    model.InterestedInfo = entity.InterestedInfo;
                    if (entity.TradeType == "A" || entity.TradeType == "S")
                    {
                        if (!string.IsNullOrEmpty(entity.BooksUrl))
                        {
                            if (entity.BooksUrl.IndexOf("http") > -1)
                                model.BooksUrl = entity.BooksUrl;
                            else
                                model.BooksUrl = "http://" + entity.BooksUrl;
                        }
                        model.BooksUrl = entity.BooksUrl;
                        if (!string.IsNullOrEmpty(entity.FileName1))
                            model.FileName1 = _appCustomSettings.UploadsCMSPath + "/PPRegistration/Document/" + entity.FileName1;
                    }

                    //if (ahrefDownload.Visible)
                    //{
                    //    if (entity.FileName1 != null && !string.IsNullOrEmpty(entity.FileName1))
                    //    {
                    //        ahrefDownload.Visible = true;
                    //        ahrefDownload.HRef = "/content/uploads/PPRegistration/Document/" + entity.FileName1;
                    //    }
                    //    else
                    //        ahrefDownload.Visible = false;
                    //}
                    //model.BooksUrl = entity.BooksUrl;
                    model.ZipCode = entity.ZipCode;

                    #region Load PPProgram GroupId Tags
                    PPProgramService PPProgramService = new PPProgramService();
                    XsiExhibitionPpprogram PPWhere = new XsiExhibitionPpprogram();
                    PPWhere.ProfessionalProgramRegistrationId = ppregid;
                    List<long> PPProgramList = PPProgramService.GetPPProgram(PPWhere).Where(i => i.PpprogramId != null).Select(x => x.PpprogramId.Value).ToList();
                    model.PPProgramList = PPProgramList;

                    #endregion
                    model.ProgramOther = entity.ProgramOther;
                    model.ReasonToAttendAgain = entity.ReasonToAttendAgain;
                    model.IsAttendedTg = entity.IsAttendedTg;

                    if (!string.IsNullOrEmpty(entity.TgreasonForNotAttend))
                        model.TGReasonForNotAttend = entity.TgreasonForNotAttend.Trim();

                    model.NoofPendingTg = entity.NoofPendingTg;
                    model.NoofPublishedTg = entity.NoofPublishedTg;

                    model.UnSoldRightsBooks = entity.UnSoldRightsBooks;
                    model.AcquireRightsToPublished = entity.AcquireRightsToPublished;
                    model.IsAlreadyBoughtRights = entity.IsAlreadyBoughtRights;
                    model.CatalogueMainLanguageOfOriginalTitle = entity.CatalogueMainLanguageOfOriginalTitle;
                    model.MainTerritories = entity.MainTerritories;
                    model.SuggestionToSupportApp = entity.SuggestionToSupportApp;
                    model.RoundTableTopics = entity.RoundTableTopics;
                    model.SpeakOnRoundTable = entity.SpeakOnRoundTable;
                    model.RecommendPublishers = entity.RecommendPublishers;

                    model.MemberId = entity.MemberId;
                    if (entity.IsHopitalityPackage == "Y")
                        model.IsHopitalityPackage = "Y";
                    else
                        model.IsHopitalityPackage = "N";
                }
            }
            return model;
        }

        ProfileBooksDTO LoadUserProfileBooks(long ppregid, bool isotherprofile = false, long langid = 1)
        {
            ProfileBooksDTO model = new ProfileBooksDTO();
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                XsiExhibitionProfessionalProgramRegistration entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(ppregid);
                if (entity != null)
                {
                    model.ItemId = entity.ItemId;
                    //form related 
                    if (entity.ProfessionalProgramId != null)
                        model.ProfessionalProgramId = entity.ProfessionalProgramId.Value;
                    if (entity.TradeType == "A" || entity.TradeType == "S")
                    {
                        if (!string.IsNullOrEmpty(entity.BooksUrl))
                        {
                            if (entity.BooksUrl.IndexOf("http") > -1)
                                model.BooksUrl = entity.BooksUrl;
                            else
                                model.BooksUrl = "http://" + entity.BooksUrl;
                        }
                        model.BooksUrl = entity.BooksUrl;
                        if (!string.IsNullOrEmpty(entity.FileName1))
                            model.FileName1 = _appCustomSettings.UploadsCMSPath + "/PPRegistration/Document/" + entity.FileName1;
                    }
                }
            }
            return model;
        }
        XsiExhibitionProfessionalProgramRegistration GetCurrentEntityDetails(long RegisteredId)
        {
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                return ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(RegisteredId);
            }
        }
        void UpdateEditRequest(long itemID)
        {
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                XsiExhibitionProfessionalProgramRegistration entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(itemID);
                if (entity != default(XsiExhibitionProfessionalProgramRegistration))
                {
                    entity.IsEditRequest = EnumConversion.ToString(EnumProfessionalProgramEditStatus.New);
                    ProfessionalProgramRegistrationService.UpdateProfessionalProgramRegistration(entity);
                }
            }
        }
        string GetTradeType(string tradeType)
        {
            switch (tradeType)
            {
                case "B": return "Buying";
                case "S": return "Selling";
                case "A": return "Both";
                default: return string.Empty;
            }
        }
        MyBooksDTO BindUserBooks(long memberId, long langId)
        {
            using (ProfessionalProgramBookService ProfessionalProgramBookService = new ProfessionalProgramBookService())
            {
                MyBooksDTO MyBooksDTO = new MyBooksDTO();
                var RegisteredId = MethodFactory.GetRegisteredId(memberId);
                MyBooksDTO.RegisteredId = RegisteredId;
                var ProfessionalProgramBookList = ProfessionalProgramBookService.GetProfessionalProgramBook(new XsiExhibitionProfessionalProgramBook { ProfessionalProgramRegistrationId = RegisteredId });
                long kount = ProfessionalProgramBookList.Count();
                if (kount > 0)
                {
                    var booksList = ProfessionalProgramBookList.Select(x => new MyBooks
                    {
                        ItemId = x.ItemId,
                        AuthorName = x.AuthorName,
                        Title = x.Title,
                        GenreId = x.GenreId,
                        Genre = MethodFactory.GetGenreName(x.GenreId, langId),
                        OtherGenre = x.OtherGenre,
                        Language = MethodFactory.GetLanguage(x.ItemId, langId),
                        IsRightsAvailable = x.IsRightsAvailable,
                        IsOwnRights = x.IsOwnRights,
                        RightsOwner = x.RightsOwner,
                        Synopsis = x.Synopsis,
                        FileName = _appCustomSettings.UploadsCMSPath + "/PPRegistration/Cover/" + x.FileName
                    }).ToList();
                    MyBooksDTO.BooksList = booksList;
                    MyBooksDTO.Count = booksList.Count;
                }
                else
                {
                    //if (kount == 0)
                    //    hfCount.Value = "1";
                    //divFristBook.Visible = true;
                    MyBooksDTO.BooksList = new List<MyBooks>(); ;
                    MyBooksDTO.Count = 1;
                }
                return MyBooksDTO;
            }
        }
        MyBooks LoadPPBook(long bookId)
        {
            using (ProfessionalProgramBookService ProfessionalProgramBookService = new ProfessionalProgramBookService())
            {
                MyBooks myBooks = new MyBooks();
                XsiExhibitionProfessionalProgramBook entity = ProfessionalProgramBookService.GetProfessionalProgramBookByItemId(bookId);
                if (entity != default(XsiExhibitionProfessionalProgramBook))
                {
                    myBooks.AuthorName = entity.AuthorName;
                    myBooks.Title = entity.Title;
                    myBooks.OtherGenre = entity.OtherGenre;
                    myBooks.Synopsis = entity.Synopsis;
                    myBooks.RightsOwner = entity.RightsOwner;
                    myBooks.IsRightsAvailable = entity.IsRightsAvailable;

                    myBooks.IsOwnRights = entity.IsOwnRights;
                    if (entity.GenreId != null)
                    {
                        myBooks.GenreId = entity.GenreId;
                    }

                    //#region Load PP Books Language Tags
                    //PPBooksLanguageService PPBooksLanguageService = new PPBooksLanguageService();
                    //XsiPPBooksLanguage entity1 = new XsiPPBooksLanguage();
                    //entity1.PPBookId = bookId;
                    //List<XsiPPBooksLanguage> PPBooksLanguageList = PPBooksLanguageService.GetPPBooksLanguage(entity1);
                    //foreach (var rows in PPBooksLanguageList)
                    //{
                    //    if (ddlSoldInOtherLanguage1.Items.FindByValue(rows.LanguageGroupId.ToString()) != null)
                    //        ddlSoldInOtherLanguage1.Items.FindByValue(rows.LanguageGroupId.ToString()).Selected = true;
                    //}
                    //#endregion

                    //mvMyBooks.SetActiveView(vwEditMyBook);
                }
                return myBooks;
            }
        }
        public static bool IsValidFile(string filename)
        {
            string filetype = filename;// GetFileExtention(filename).ToLower();
            return (filetype == ".pdf" || filetype == ".doc" || filetype == ".docx" || filetype == ".gif" || filetype == ".jpg" || filetype == ".jpeg" || filetype == ".png") ? true : false;
        }
        SlotInfo GetSlotsInfo(long slotid, long day, long RegisteredId)
        {
            SlotInfo slotInfo = new SlotInfo();
            string strDisplayNameAndTableNo = string.Empty;
            string strDisplayName = string.Empty;
            string strInfo = string.Empty;
            long ppSlotRegistrationItemId2 = -1;
            long ppSlotRegistrationSlotId2 = -1;
            using (PPSlotRegistrationService PPSlotRegistrationService = new PPSlotRegistrationService())
            {
                ProfessionalProgramSlotService ProfessionalProgramSlotService = new ProfessionalProgramSlotService();
                XsiExhibitionProfessionalProgramSlots entity = ProfessionalProgramSlotService.GetProfessionalProgramSlotByItemId(slotid);
                if (entity != null)
                {
                    if (entity.DayId == day && entity.IsBreak != EnumConversion.ToString(EnumBool.Yes) && entity.IsBreak != "O")
                    {
                        XsiExhibitionPpslotInvitation where = new XsiExhibitionPpslotInvitation();
                        where.SlotId = slotid;
                        var SlotRegistrationList1 = PPSlotRegistrationService.GetPPSlotRegistration(where).ToList();
                        //strSlotInfo
                        ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService();
                        foreach (XsiExhibitionPpslotInvitation cols in SlotRegistrationList1)
                        {
                            if (cols.RequestId == RegisteredId && cols.SlotId == slotid)
                            {
                                slotInfo.SentInvites = true;
                                #region Sent Invites
                                XsiExhibitionProfessionalProgramRegistration registeredentity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(cols.ResponseId.Value);
                                if (registeredentity != null)
                                {
                                    if (registeredentity.TableId != null)
                                        slotInfo.DisplayNameAndTableNo = registeredentity.FirstName + " " + registeredentity.LastName + " - T# " + registeredentity.TableId.Value;
                                    else
                                        slotInfo.DisplayNameAndTableNo = registeredentity.FirstName + " " + registeredentity.LastName;
                                    if (cols.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                                    {
                                        slotInfo.SlotDayClass = "light-green";
                                        slotInfo.SlotId = cols.SlotId.Value;
                                        slotInfo.TransactionId = cols.ItemId;
                                        slotInfo.RegisteredItemId = registeredentity.ItemId;
                                        slotInfo.RequestId = cols.RequestId.Value;
                                        slotInfo.Status = cols.Status;
                                        strInfo = "light-green,";
                                        //strInfo += registeredentity.FirstName + " " + registeredentity.MiddleName + " " + registeredentity.LastName;
                                        strInfo += "<a data-slotid='" + cols.SlotId + "' data-transactionId='" + cols.ItemId + "' data-val='" + registeredentity.ItemId + "' class='ShowMeetupPopup1' onclick='dff()'>" + strDisplayNameAndTableNo + "</a><a data-reqid=" + cols.RequestId + " class='btncancelinvitesentreq' href='#' onclick='obj.sayHello();' data-status='" + cols.Status + "'>X</a>";
                                        return slotInfo;
                                    }
                                    else if (cols.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Pending))
                                    {
                                        slotInfo.SlotDayClass = "yellow-text-bg";
                                        slotInfo.SlotId = cols.SlotId.Value;
                                        slotInfo.TransactionId = cols.ItemId;
                                        slotInfo.RegisteredItemId = registeredentity.ItemId;
                                        slotInfo.RequestId = cols.RequestId.Value;
                                        slotInfo.Status = cols.Status;
                                        strInfo = "yellow-text-bg,";
                                        strInfo += strDisplayName + "<a data-reqid=" + cols.RequestId + " class='btncancelinvitesentreq' href='#' onclick='fn_cancelinvitationsent(" + cols.ItemId + ");' data-status='" + cols.Status + "'>X</a>";
                                        return slotInfo;
                                    }
                                }
                                #endregion
                            }
                            else if (cols.ResponseId == RegisteredId && cols.SlotId == slotid)
                            {
                                #region Received Invites
                                XsiExhibitionProfessionalProgramRegistration registeredentity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(cols.RequestId.Value);
                                if (cols.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                                {
                                    slotInfo.SlotDayClass = "light-green";
                                    slotInfo.SlotId = cols.SlotId.Value;
                                    slotInfo.TransactionId = cols.ItemId;
                                    slotInfo.RegisteredItemId = registeredentity.ItemId;
                                    slotInfo.RequestId = cols.RequestId.Value;
                                    slotInfo.Status = cols.Status;
                                    slotInfo.DisplayNameAndTableNo = registeredentity.FirstName + " " + registeredentity.LastName + " - T# " + MethodFactory.GetHostTableNumber(cols.ResponseId.Value);

                                    strInfo = "light-green,";
                                    strInfo += "<a href='#' data-toggle='modal' data-slotid='" + cols.SlotId + "' data-transactionId='" + cols.ItemId + "' data-val='" + registeredentity.ItemId + "' class='ShowMeetupPopup1' data-target='#myModal1' onClick={() => this.handleInviteeView()}>" + registeredentity.FirstName + " " + registeredentity.LastName + " - T# " + MethodFactory.GetHostTableNumber(cols.ResponseId.Value) + "</a><a data-reqid=" + cols.RequestId + " class='btncancelinvitesentreq' href='#' onclick='fn_cancelinvitationsent(" + cols.ItemId + ");' data-status='" + cols.Status + "'>X</a>";//registeredentity.FirstName + " " + registeredentity.MiddleName + " " + registeredentity.LastName;
                                    return slotInfo;
                                }
                                else if (cols.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Pending))
                                {
                                    long kount = 0;
                                    string strInvitedBy = string.Empty;

                                    XsiExhibitionProfessionalProgramRegistration registeredentity1 = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(cols.RequestId.Value);
                                    string responseid = string.Empty;
                                    List<XsiExhibitionPpslotInvitation> PPSlotRegistrationList = PPSlotRegistrationService.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation { ResponseId = RegisteredId, SlotId = slotid });
                                    XsiExhibitionProfessionalProgramRegistration registeredentity2 = null;

                                    foreach (XsiExhibitionPpslotInvitation colsmem in PPSlotRegistrationList)
                                    {
                                        if (registeredentity != null)
                                        {
                                            if (registeredentity.ItemId != colsmem.RequestId && colsmem.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Pending))
                                            {

                                                registeredentity2 = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(colsmem.RequestId.Value);
                                                ppSlotRegistrationItemId2 = colsmem.ItemId;
                                                ppSlotRegistrationSlotId2 = colsmem.SlotId.Value;
                                                break;
                                            }
                                        }
                                    }
                                    if (registeredentity1 != null)
                                    {

                                        slotInfo.SlotId = cols.SlotId.Value;
                                        slotInfo.TransactionId = cols.ItemId;
                                        slotInfo.RegisteredItemId = registeredentity1.ItemId;
                                        slotInfo.RequestId = cols.RequestId.Value;
                                        slotInfo.Status = cols.Status;
                                        slotInfo.DisplayNameAndTableNo = registeredentity1.FirstName + " " + registeredentity1.LastName;

                                        strDisplayName = registeredentity1.FirstName + " " + registeredentity1.LastName;
                                        kount = 1;
                                        slotInfo.Count = 1;
                                        strInvitedBy = "<a href='#' data-toggle='modal' data-slotid='" + cols.SlotId + "' data-transactionId='" + cols.ItemId + "' data-val='" + registeredentity1.ItemId + "' class='ShowMeetupPopup' data-target='#myModal' onClick={() => this.handleInviteeView()}>" + strDisplayName + "</a>";//registeredentity1.FirstName + " " + registeredentity1.MiddleName + " " + registeredentity1.LastName;
                                    }
                                    if (registeredentity2 != null && registeredentity2.ItemId != RegisteredId)
                                    {
                                        slotInfo.SlotId = ppSlotRegistrationSlotId2;
                                        slotInfo.TransactionId = ppSlotRegistrationItemId2;
                                        slotInfo.RegisteredItemId = registeredentity2.ItemId;
                                        slotInfo.RequestId = cols.RequestId.Value;
                                        slotInfo.Status = cols.Status;
                                        slotInfo.DisplayNameAndTableNo = registeredentity2.FirstName + " " + registeredentity2.LastName;
                                        strDisplayName = registeredentity2.FirstName + " " + registeredentity2.LastName;
                                        slotInfo.Count = 2;// kount = 2;
                                        strInvitedBy += "<a href='#' data-toggle='modal' data-slotid='" + ppSlotRegistrationSlotId2 + "'   data-transactionId='" + ppSlotRegistrationItemId2 + "' data-val='" + registeredentity2.ItemId + "' class='ShowMeetupPopup' data-target='#myModal'>" + strDisplayName + "</a>";
                                    }
                                    slotInfo.SlotDayClass = "red-texture";
                                    strInfo = "red-texture,";
                                    if (kount > 1)
                                    {
                                        if (registeredentity2 != null)
                                        {
                                            slotInfo.RegisteredEntityTwo = true;
                                            strInfo += "<div  class='rel InvitesContainer' style='display: none;'><div class='pp-invites'><i class='icon-remove'></i>" + strInvitedBy + "</div></div>";
                                            strInfo += "<span  class='seeInvites'>You Have " + kount.ToString() + " Invites</span>";
                                        }
                                        else
                                            strInfo += "<span>" + strInvitedBy + "</span>";
                                    }
                                    else
                                        strInfo += "<span>" + strInvitedBy + "</span>";

                                    return slotInfo;
                                }
                                #endregion
                            }
                        }

                        //if (string.IsNullOrEmpty(strInfo))
                        //    return "red_text_bg,Seminar programme";
                    }
                    else
                    {
                        slotInfo.SlotDayClass = "red-text-bg";
                        slotInfo.DisplayNameAndTableNo = entity.BreakTitle;
                        slotInfo.Status = "B";
                        return slotInfo; //"red_text_bg," + entity.BreakTitle;
                    }
                }
            }
            return slotInfo;
        }
        #endregion
        #region Email Methods
        void SendEmailBasedOnEdit(long ProfRegId, List<long> OldListGenre, List<long> NewListGenre, List<long> OldListPPProgrameSource, List<long> NewListPPProgrameSource, XsiExhibitionProfessionalProgramRegistration OldPPRegistrationEntity, long websiteId, long langId)
        {
            XsiScrfemailContent scrfEmailContent;
            XsiEmailContent emailContent;
            #region Send Edit Request Email
            StringBuilder body = new StringBuilder();

            string strContent = "<p style=\"font-size: 16px; line-height: 32px; font-family: Arial, Helvetica, sans-serif; color: #555555; margin: 0px;\">$$Label$$ : $$Value$$</p>";
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                XsiExhibitionProfessionalProgramRegistration entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(ProfRegId);
                if (entity != null)
                {
                    string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                    #region Email Content
                    EmailContentService EmailContentService = new EmailContentService();
                    SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(20073);
                        if (scrfEmailContent != null)
                        {
                            if (scrfEmailContent.Body != null)
                                strEmailContentBody = scrfEmailContent.Body;
                            if (scrfEmailContent.Email != null)
                                strEmailContentEmail = scrfEmailContent.Email;
                            if (scrfEmailContent.Subject != null)
                                strEmailContentSubject = scrfEmailContent.Subject;
                            strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                        }
                    }
                    else
                    {
                        emailContent = EmailContentService.GetEmailContentByItemId(20085);
                        if (emailContent != null)
                        {
                            if (emailContent.Body != null)
                                strEmailContentBody = emailContent.Body;
                            if (emailContent.Email != null)
                                strEmailContentEmail = emailContent.Email;
                            if (emailContent.Subject != null)
                                strEmailContentSubject = emailContent.Subject;
                            strEmailContentAdmin = _appCustomSettings.AdminName;
                        }
                    }
                    #endregion
                    //  body.Append(MethodFactory.BindEmailContentPP(langId, strEmailContentBody, OldPPRegistrationEntity, _appCustomSettings.ServerAddressNew, websiteId));
                    body.Append(htmlContentFactory.BindEmailContentPP(langId, strEmailContentBody, OldPPRegistrationEntity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                    #region Body
                    bool IsAnyFieldChanged = false;


                    #region Publisher Type
                    if (OldPPRegistrationEntity.Type != entity.Type)
                    {
                        if (entity.Type != null && !string.IsNullOrEmpty(entity.Type))
                        {
                            body.Replace("$$NewType$$", strContent);
                            body.Replace("$$Label$$", "Publisher Type");
                            if (MethodFactory.GetPPType(entity.Type.ToString()) != null)
                            {
                                body.Replace("$$Value$$", MethodFactory.GetPPType(entity.Type.ToString()));
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewType$$", string.Empty);
                        }
                        else
                            body.Replace("$$NewType$$", string.Empty);

                        if (OldPPRegistrationEntity.Type != null && !string.IsNullOrEmpty(OldPPRegistrationEntity.Type))
                        {
                            body.Replace("$$OldType$$", strContent);
                            body.Replace("$$Label$$", "Publisher Type");
                            if (MethodFactory.GetPPType(OldPPRegistrationEntity.Type.ToString()) != null)
                            {
                                body.Replace("$$Value$$", MethodFactory.GetPPType(OldPPRegistrationEntity.Type.ToString()));
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldType$$", string.Empty);
                        }
                        else
                            body.Replace("$$OldType$$", string.Empty);
                    }
                    #endregion
                    #region Title
                    if (OldPPRegistrationEntity.Title != entity.Title)
                    {
                        if (entity.Title != null && !string.IsNullOrEmpty(entity.Title))
                        {
                            body.Replace("$$NewTitle$$", strContent);
                            body.Replace("$$Label$$", "Title");
                            if (MethodFactory.GetPPTitle(entity.Title.ToString()) != null)
                            {
                                body.Replace("$$Value$$", MethodFactory.GetPPTitle(entity.Title.ToString()));
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewTitle$$", string.Empty);
                        }
                        else
                            body.Replace("$$NewTitle$$", string.Empty);

                        if (OldPPRegistrationEntity.Title != null && !string.IsNullOrEmpty(OldPPRegistrationEntity.Title))
                        {
                            body.Replace("$$OldTitle$$", strContent);
                            body.Replace("$$Label$$", "Title");
                            if (MethodFactory.GetPPTitle(OldPPRegistrationEntity.Title.ToString()) != null)
                            {
                                body.Replace("$$Value$$", MethodFactory.GetPPTitle(OldPPRegistrationEntity.Title.ToString()));
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldTitle$$", string.Empty);
                        }
                        else
                            body.Replace("$$OldTitle$$", string.Empty);
                    }
                    #endregion

                    #region First Name
                    if (OldPPRegistrationEntity.FirstName != entity.FirstName)
                    {
                        if (entity.FirstName != null && !string.IsNullOrEmpty(entity.FirstName))
                        {
                            body.Replace("$$NewApplicantFirstNameEn$$", strContent);
                            body.Replace("$$Label$$", "First Name");
                            body.Replace("$$Value$$", entity.FirstName);
                            IsAnyFieldChanged = true;

                        }
                        else
                            body.Replace("$$NewApplicantFirstNameEn$$", string.Empty);

                        if (OldPPRegistrationEntity.FirstName != null && !string.IsNullOrEmpty(OldPPRegistrationEntity.FirstName))
                        {
                            body.Replace("$$OldApplicantFirstNameEn$$", strContent);
                            body.Replace("$$Label$$", "First Name");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.FirstName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldApplicantFirstNameEn$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$OldApplicantFirstNameEn$$", string.Empty);
                        body.Replace("$$NewApplicantFirstNameEn$$", string.Empty);
                    }
                    #endregion
                    #region Last Name
                    if (OldPPRegistrationEntity.LastName != entity.LastName)
                    {
                        if (entity.LastName != null && !string.IsNullOrEmpty(entity.LastName))
                        {
                            body.Replace("$$NewApplicantLastNameEn$$", strContent);
                            body.Replace("$$Label$$", "Last Name");
                            body.Replace("$$Value$$", entity.LastName);
                            IsAnyFieldChanged = true;

                        }
                        else
                            body.Replace("$$NewApplicantLastNameEn$$", string.Empty);

                        if (OldPPRegistrationEntity.FirstName != null && !string.IsNullOrEmpty(OldPPRegistrationEntity.LastName))
                        {
                            body.Replace("$$OldApplicantLastNameEn$$", strContent);
                            body.Replace("$$Label$$", "Last Name");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.LastName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldApplicantLastNameEn$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$OldApplicantLastNameEn$$", string.Empty);
                        body.Replace("$$NewApplicantLastNameEn$$", string.Empty);
                    }
                    #endregion

                    #region First Name Arabic
                    if (OldPPRegistrationEntity.FirstNameAr != entity.FirstNameAr)
                    {
                        if (entity.FirstNameAr != null && !string.IsNullOrEmpty(entity.FirstNameAr))
                        {
                            body.Replace("$$NewApplicantFirstNameAr$$", strContent);
                            body.Replace("$$Label$$", "First Name Arabic");
                            body.Replace("$$Value$$", entity.FirstNameAr);
                            IsAnyFieldChanged = true;

                        }
                        else
                            body.Replace("$$NewApplicantFirstNameAr$$", string.Empty);

                        if (OldPPRegistrationEntity.FirstNameAr != null && !string.IsNullOrEmpty(OldPPRegistrationEntity.FirstNameAr))
                        {
                            body.Replace("$$OldApplicantFirstNameAr$$", strContent);
                            body.Replace("$$Label$$", "First Name Arabic");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.FirstNameAr);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldApplicantFirstNameAr$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$OldApplicantFirstNameAr$$", string.Empty);
                        body.Replace("$$NewApplicantFirstNameAr$$", string.Empty);
                    }
                    #endregion
                    #region Last Name Arabic
                    if (OldPPRegistrationEntity.LastNameAr != entity.LastNameAr)
                    {
                        if (entity.LastNameAr != null && !string.IsNullOrEmpty(entity.LastNameAr))
                        {
                            body.Replace("$$NewApplicantLastNameAr$$", strContent);
                            body.Replace("$$Label$$", "Last Name Arabic");
                            body.Replace("$$Value$$", entity.LastNameAr);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewApplicantLastNameAr$$", string.Empty);

                        if (OldPPRegistrationEntity.LastNameAr != null && !string.IsNullOrEmpty(OldPPRegistrationEntity.LastNameAr))
                        {
                            body.Replace("$$OldApplicantLastNameAr$$", strContent);
                            body.Replace("$$Label$$", "Last Name Arabic");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.LastNameAr);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldApplicantLastNameAr$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$OldApplicantLastNameAr$$", string.Empty);
                        body.Replace("$$NewApplicantLastNameAr$$", string.Empty);
                    }
                    #endregion

                    #region Phone
                    if (OldPPRegistrationEntity.PhoneNumber != entity.PhoneNumber)
                    {
                        if (entity.PhoneNumber != null)
                        {
                            body.Replace("$$NewPhone$$", strContent);
                            body.Replace("$$Label$$", "Phone");
                            body.Replace("$$Value$$", entity.PhoneNumber.Replace('$', '-'));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewPhone$$", string.Empty);
                        if (OldPPRegistrationEntity.PhoneNumber != null)
                        {
                            body.Replace("$$OldPhone$$", strContent);
                            body.Replace("$$Label$$", "Phone");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.PhoneNumber.Replace('$', '-'));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldPhone$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewPhone$$", string.Empty);
                        body.Replace("$$OldPhone$$", string.Empty);
                    }
                    #endregion

                    #region Mobile
                    if (OldPPRegistrationEntity.MobileNumber != entity.MobileNumber)
                    {
                        if (entity.MobileNumber != null)
                        {
                            body.Replace("$$NewMobile$$", strContent);
                            body.Replace("$$Label$$", "Mobile");
                            body.Replace("$$Value$$", entity.MobileNumber.Replace('$', '-'));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewMobile$$", string.Empty);
                        if (OldPPRegistrationEntity.MobileNumber != null)
                        {
                            body.Replace("$$OldMobile$$", strContent);
                            body.Replace("$$Label$$", "Mobile");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.MobileNumber.Replace('$', '-'));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldMobile$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewMobile$$", string.Empty);
                        body.Replace("$$OldMobile$$", string.Empty);
                    }
                    #endregion

                    #region ZipCode
                    if (OldPPRegistrationEntity.ZipCode != entity.ZipCode)
                    {
                        if (entity.ZipCode != null)
                        {
                            body.Replace("$$NewZipCode$$", strContent);
                            body.Replace("$$Label$$", "ZipCode");
                            body.Replace("$$Value$$", entity.ZipCode);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewZipCode$$", string.Empty);
                        if (OldPPRegistrationEntity.ZipCode != null)
                        {
                            body.Replace("$$OldZipCode$$", strContent);
                            body.Replace("$$Label$$", "Mobile");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.ZipCode);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldZipCode$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewZipCode$$", string.Empty);
                        body.Replace("$$OldZipCode$$", string.Empty);
                    }
                    #endregion



                    #region Email
                    if (OldPPRegistrationEntity.Email != entity.Email)
                    {
                        if (entity.Email != null)
                        {
                            body.Replace("$$NewEmail$$", strContent);
                            body.Replace("$$Label$$", "Email");
                            body.Replace("$$Value$$", entity.Email);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewEmail$$", string.Empty);
                        if (OldPPRegistrationEntity.Email != null)
                        {
                            body.Replace("$$OldEmail$$", strContent);
                            body.Replace("$$Label$$", "Email");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.Email);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldEmail$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewEmail$$", string.Empty);
                        body.Replace("$$OldEmail$$", string.Empty);
                    }
                    #endregion

                    #region Country
                    if (OldPPRegistrationEntity.CountryId != entity.CountryId)
                    {
                        if (entity.CountryId != null)
                        {
                            body.Replace("$$NewCountry$$", strContent);
                            body.Replace("$$Label$$", "Country");
                            if (MethodFactory.GetCountryName(entity.CountryId.Value, langId) != null)
                            {
                                body.Replace("$$Value$$", MethodFactory.GetCountryName(entity.CountryId.Value, langId));
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewCountry$$", string.Empty);
                        }
                        else
                            body.Replace("$$NewCountry$$", string.Empty);
                        if (OldPPRegistrationEntity.CountryId != null)
                        {
                            body.Replace("$$OldCountry$$", strContent);
                            body.Replace("$$Label$$", "Country");
                            if (MethodFactory.GetCountryName(OldPPRegistrationEntity.CountryId.Value, langId) != null)
                            {
                                body.Replace("$$Value$$", MethodFactory.GetCountryName(OldPPRegistrationEntity.CountryId.Value, langId));
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldCountry$$", string.Empty);
                        }
                        else
                            body.Replace("$$OldCountry$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewCountry$$", string.Empty);
                        body.Replace("$$OldCountry$$", string.Empty);
                    }
                    #endregion
                    #region City
                    if (OldPPRegistrationEntity.CityId != entity.CityId)
                    {
                        if (entity.CityId != null)
                        {
                            body.Replace("$$NewCity$$", strContent);
                            body.Replace("$$Label$$", "City");
                            if (MethodFactory.GetCountryName(entity.CityId.Value, langId) != null)
                            {
                                body.Replace("$$Value$$", MethodFactory.GetCountryName(entity.CityId.Value, langId));
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewCity$$", string.Empty);
                        }
                        else
                            body.Replace("$$NewCity$$", string.Empty);
                        if (OldPPRegistrationEntity.CityId != null)
                        {
                            body.Replace("$$OldCity$$", strContent);
                            body.Replace("$$Label$$", "City");
                            if (MethodFactory.GetCountryName(OldPPRegistrationEntity.CityId.Value, langId) != null)
                            {
                                body.Replace("$$Value$$", MethodFactory.GetCountryName(OldPPRegistrationEntity.CityId.Value, langId));
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldCity$$", string.Empty);
                        }
                        else
                            body.Replace("$$OldCity$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewCity$$", string.Empty);
                        body.Replace("$$OldCity$$", string.Empty);
                    }
                    #endregion


                    #region Company Name
                    if (OldPPRegistrationEntity.CompanyName != entity.CompanyName)
                    {
                        if (entity.CompanyName != null)
                        {
                            body.Replace("$$NewCompanyNameEn$$", strContent);
                            body.Replace("$$Label$$", "Company Name");
                            body.Replace("$$Value$$", entity.CompanyName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewCompanyNameEn$$", string.Empty);
                        if (OldPPRegistrationEntity.CompanyName != null)
                        {
                            body.Replace("$$OldCompanyNameEn$$", strContent);
                            body.Replace("$$Label$$", "Company Name");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.CompanyName);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldCompanyNameEn$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewCompanyNameEn$$", string.Empty);
                        body.Replace("$$OldCompanyNameEn$$", string.Empty);
                    }
                    #endregion

                    #region Company Name Arabic
                    if (OldPPRegistrationEntity.CompanyNameAr != entity.CompanyNameAr)
                    {
                        if (entity.CompanyNameAr != null)
                        {
                            body.Replace("$$NewCompanyNameAr$$", strContent);
                            body.Replace("$$Label$$", "Company Name Arabic");
                            body.Replace("$$Value$$", entity.CompanyNameAr);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewCompanyNameAr$$", string.Empty);
                        if (OldPPRegistrationEntity.CompanyNameAr != null)
                        {
                            body.Replace("$$OldCompanyNameAr$$", strContent);
                            body.Replace("$$Label$$", "Company Name Arabic");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.CompanyNameAr);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldCompanyNameAr$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewCompanyNameAr$$", string.Empty);
                        body.Replace("$$OldCompanyNameAr$$", string.Empty);
                    }
                    #endregion

                    #region Job Title
                    if (OldPPRegistrationEntity.JobTitle != entity.JobTitle)
                    {
                        if (entity.JobTitle != null)
                        {
                            body.Replace("$$NewJobTitleEn$$", strContent);
                            body.Replace("$$Label$$", "Job Title");
                            body.Replace("$$Value$$", entity.JobTitle);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewJobTitleEn$$", string.Empty);
                        if (OldPPRegistrationEntity.JobTitle != null)
                        {
                            body.Replace("$$OldJobTitleEn$$", strContent);
                            body.Replace("$$Label$$", "Job Title");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.JobTitle);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldJobTitleEn$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewJobTitleEn$$", string.Empty);
                        body.Replace("$$OldJobTitleEn$$", string.Empty);
                    }
                    #endregion

                    #region Job Title Arabic
                    if (OldPPRegistrationEntity.JobTitleAr != entity.JobTitleAr)
                    {
                        if (entity.JobTitleAr != null)
                        {
                            body.Replace("$$NewJobTitleAr$$", strContent);
                            body.Replace("$$Label$$", "Job Title Arabic");
                            body.Replace("$$Value$$", entity.JobTitleAr);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewJobTitleAr$$", string.Empty);
                        if (OldPPRegistrationEntity.JobTitleAr != null)
                        {
                            body.Replace("$$OldJobTitleAr$$", strContent);
                            body.Replace("$$Label$$", "Job Title Arabic");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.JobTitleAr);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldJobTitleAr$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewJobTitleAr$$", string.Empty);
                        body.Replace("$$OldJobTitleAr$$", string.Empty);
                    }
                    #endregion

                    #region Work Profile
                    if (OldPPRegistrationEntity.WorkProfile != entity.WorkProfile)
                    {
                        if (entity.WorkProfile != null)
                        {
                            body.Replace("$$NewWorkProfileEn$$", strContent);
                            body.Replace("$$Label$$", "Work Profile");
                            body.Replace("$$Value$$", entity.WorkProfile);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewWorkProfileEn$$", string.Empty);
                        if (OldPPRegistrationEntity.WorkProfile != null)
                        {
                            body.Replace("$$OldWorkProfileEn$$", strContent);
                            body.Replace("$$Label$$", "Work Profile");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.WorkProfile);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldWorkProfileEn$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewWorkProfileEn$$", string.Empty);
                        body.Replace("$$OldWorkProfileEn$$", string.Empty);
                    }
                    #endregion

                    #region Work Profile Arabic
                    if (OldPPRegistrationEntity.WorkProfileAr != entity.WorkProfileAr)
                    {
                        if (entity.WorkProfileAr != null)
                        {
                            body.Replace("$$NewWorkProfileAr$$", strContent);
                            body.Replace("$$Label$$", "Work Profile Arabic");
                            body.Replace("$$Value$$", entity.WorkProfileAr);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewWorkProfileAr$$", string.Empty);
                        if (OldPPRegistrationEntity.WorkProfileAr != null)
                        {
                            body.Replace("$$OldWorkProfileAr$$", strContent);
                            body.Replace("$$Label$$", "Work Profile Arabic");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.WorkProfileAr);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldWorkProfileAr$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewWorkProfileAr$$", string.Empty);
                        body.Replace("$$OldWorkProfileAr$$", string.Empty);
                    }
                    #endregion

                    #region Website
                    if (OldPPRegistrationEntity.Website != entity.Website)
                    {
                        if (entity.Website != null)
                        {
                            body.Replace("$$NewWebsite$$", strContent);
                            body.Replace("$$Label$$", "Website");
                            body.Replace("$$Value$$", entity.Website);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewWebsite$$", string.Empty);
                        if (OldPPRegistrationEntity.Website != null)
                        {
                            body.Replace("$$OldWebsite$$", strContent);
                            body.Replace("$$Label$$", "Website");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.Website);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldWebsite$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewWebsite$$", string.Empty);
                        body.Replace("$$OldWebsite$$", string.Empty);
                    }
                    #endregion

                    #region Genre
                    //logic for old pp genre and new pp genre
                    long kount2 = 0;
                    if (NewListPPProgrameSource.Count > OldListPPProgrameSource.Count)
                        kount2 = NewListPPProgrameSource.Except(OldListPPProgrameSource).Count();
                    else if (OldListPPProgrameSource.Count > NewListPPProgrameSource.Count)
                        kount2 = OldListPPProgrameSource.Except(NewListPPProgrameSource).Count();
                    if (kount2 > 0)
                    {
                        if (NewListGenre.Count == 0)
                        {
                            body.Replace("$$OldGenre$$", string.Empty);
                            body.Replace("$$NewGenre$$", string.Empty);
                        }
                        else
                        {
                            string oldgenre = string.Empty;
                            string newgenre = string.Empty;
                            if (OldListGenre.Count() > 0)
                            {
                                GenreService GenreService = new GenreService();
                                var ListGenreItems = GenreService.GetGenre(new XsiGenre()).ToList().Where(i => OldListGenre.Contains(i.ItemId)).ToList();
                                foreach (var item in ListGenreItems)
                                {
                                    oldgenre += item.Title + ", ";
                                }
                                if (oldgenre.Length > 0)
                                    oldgenre = oldgenre.Remove(oldgenre.Length - 1);
                            }
                            if (NewListGenre.Count() > 0)
                            {
                                GenreService GenreService = new GenreService();
                                var ListGenreItems = GenreService.GetGenre(new XsiGenre()).ToList().Where(i => NewListGenre.Contains(i.ItemId)).ToList();
                                foreach (var item in ListGenreItems)
                                {
                                    newgenre += item.Title + ", ";
                                }
                                if (newgenre.Length > 0)
                                    newgenre = newgenre.Remove(newgenre.Length - 1);
                            }
                            //body.Replace("$$OldGenre$$", oldgenre);
                            //body.Replace("$$NewGenre$$", newgenre);
                            if (newgenre.Length > 0)
                            {
                                body.Replace("$$NewGenre$$", strContent);
                                body.Replace("$$Label$$", "Which types of publishing/genre will you be representing in Sharjah? Please select as many that apply");
                                body.Replace("$$Value$$", newgenre);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewGenre$$", string.Empty);
                            if (oldgenre.Length > 0)
                            {
                                body.Replace("$$OldGenre$$", strContent);
                                body.Replace("$$Label$$", "Which types of publishing/genre will you be representing in Sharjah? Please select as many that apply");
                                body.Replace("$$Value$$", oldgenre);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldGenre$$", string.Empty);
                        }
                    }
                    else
                    {
                        body.Replace("$$OldGenre$$", string.Empty);
                        body.Replace("$$NewGenre$$", string.Empty);
                    }
                    #endregion
                    #region Other Genre
                    if (OldPPRegistrationEntity.OtherGenre != entity.OtherGenre)
                    {
                        if (entity.OtherGenre != null)
                        {
                            body.Replace("$$NewOtherGenre$$", strContent);
                            body.Replace("$$Label$$", "Other Genre");
                            body.Replace("$$Value$$", entity.OtherGenre);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewOtherGenre$$", string.Empty);
                        if (OldPPRegistrationEntity.OtherGenre != null)
                        {
                            body.Replace("$$OldOtherGenre$$", strContent);
                            body.Replace("$$Label$$", "Other Genre");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.OtherGenre);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldOtherGenre$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewOtherGenre$$", string.Empty);
                        body.Replace("$$OldOtherGenre$$", string.Empty);
                    }
                    #endregion

                    #region Trade Type
                    if (OldPPRegistrationEntity.TradeType != entity.TradeType)
                    {
                        if (entity.TradeType != null)
                        {
                            body.Replace("$$NewTradeType$$", strContent);
                            body.Replace("$$Label$$", "Trade Type");
                            body.Replace("$$Value$$", GetTradeType(entity.TradeType));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewTradeType$$", string.Empty);
                        if (OldPPRegistrationEntity.TradeType != null)
                        {
                            body.Replace("$$OldTradeType$$", strContent);
                            body.Replace("$$Label$$", "Trade Type");
                            body.Replace("$$Value$$", GetTradeType(OldPPRegistrationEntity.TradeType));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldTradeType$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewTradeType$$", string.Empty);
                        body.Replace("$$OldTradeType$$", string.Empty);
                    }
                    #endregion

                    #region Is E Books
                    if (OldPPRegistrationEntity.IsEbooks != entity.IsEbooks)
                    {
                        if (entity.TradeType != null)
                        {
                            body.Replace("$$NewIsEbooks$$", strContent);
                            body.Replace("$$Label$$", "Do you have any ebooks or apps you want to sell?");
                            body.Replace("$$Value$$", entity.IsEbooks == "Y" ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewIsEbooks$$", string.Empty);
                        if (OldPPRegistrationEntity.IsEbooks != null)
                        {
                            body.Replace("$$OldIsEbooks$$", strContent);
                            body.Replace("$$Label$$", "Do you have any ebooks or apps you want to sell?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.IsEbooks == "Y" ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldIsEbooks$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewIsEbooks$$", string.Empty);
                        body.Replace("$$OldIsEbooks$$", string.Empty);
                    }
                    #endregion

                    #region Example of Translated Books
                    if (OldPPRegistrationEntity.ExamplesOfTranslatedBooks != entity.ExamplesOfTranslatedBooks)
                    {
                        if (entity.ExamplesOfTranslatedBooks != null)
                        {
                            body.Replace("$$NewExampleofTranslatedBooks$$", strContent);
                            body.Replace("$$Label$$", "Please provide example (max 5) of books you have bought or sold translation rights in the last five years:");
                            body.Replace("$$Value$$", entity.ExamplesOfTranslatedBooks);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewExampleofTranslatedBooks$$", string.Empty);
                        if (OldPPRegistrationEntity.ExamplesOfTranslatedBooks != null)
                        {
                            body.Replace("$$OldExampleofTranslatedBooks$$", strContent);
                            body.Replace("$$Label$$", "Please provide example (max 5) of books you have bought or sold translation rights in the last five years:");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.ExamplesOfTranslatedBooks);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldExampleofTranslatedBooks$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewExampleofTranslatedBooks$$", string.Empty);
                        body.Replace("$$OldExampleofTranslatedBooks$$", string.Empty);
                    }
                    #endregion

                    #region IsInterested
                    if (OldPPRegistrationEntity.IsInterested != entity.IsInterested)
                    {
                        if (entity.Website != null)
                        {
                            body.Replace("$$NewIsInterested$$", strContent);
                            body.Replace("$$Label$$", "Are you interested in learning about digital publishing?");
                            body.Replace("$$Value$$", entity.IsInterested == "Y" ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewIsInterested$$", string.Empty);
                        if (OldPPRegistrationEntity.IsInterested != null)
                        {
                            body.Replace("$$OldIsInterested$$", strContent);
                            body.Replace("$$Label$$", "Are you interested in learning about digital publishing?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.IsInterested == "Y" ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldIsInterested$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewIsInterested$$", string.Empty);
                        body.Replace("$$OldIsInterested$$", string.Empty);
                    }
                    #endregion

                    #region InterestedInfo
                    if (OldPPRegistrationEntity.InterestedInfo != entity.InterestedInfo)
                    {
                        if (entity.Website != null)
                        {
                            body.Replace("$$NewInterestedInfo$$", strContent);
                            body.Replace("$$Label$$", " If yes, please provide more information");
                            body.Replace("$$Value$$", entity.InterestedInfo);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewInterestedInfo$$", string.Empty);
                        if (OldPPRegistrationEntity.InterestedInfo != null)
                        {
                            body.Replace("$$OldInterestedInfo$$", strContent);
                            body.Replace("$$Label$$", " If yes, please provide more information");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.InterestedInfo);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldInterestedInfo$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewInterestedInfo$$", string.Empty);
                        body.Replace("$$OldInterestedInfo$$", string.Empty);
                    }
                    #endregion
                    #region FileName
                    //logic for Avatar Image
                    #endregion
                    #region FileName1
                    //logic for Catalogue Image
                    #endregion
                    #region IsAttended
                    if (OldPPRegistrationEntity.IsAttended != entity.IsAttended)
                    {
                        if (entity.IsAttended != null)
                        {
                            body.Replace("$$NewIsAttended$$", strContent);
                            body.Replace("$$Label$$", "Have you attended the SIBF Publishers Conference before ?");
                            body.Replace("$$Value$$", entity.IsAttended == "Y" ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewIsAttended$$", string.Empty);
                        if (OldPPRegistrationEntity.IsAttended != null)
                        {
                            body.Replace("$$OldIsAttended$$", strContent);
                            body.Replace("$$Label$$", "Have you attended the SIBF Publishers Conference before ?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.IsAttended == "Y" ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldIsAttended$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewIsAttended$$", string.Empty);
                        body.Replace("$$OldIsAttended$$", string.Empty);
                    }
                    #endregion

                    #region HowManyTimes
                    if (OldPPRegistrationEntity.HowManyTimes != entity.HowManyTimes)
                    {
                        if (entity.HowManyTimes != null)
                        {
                            body.Replace("$$NewHowManyTimes$$", strContent);
                            body.Replace("$$Label$$", " If yes, please specify how many times you have previously attended?");
                            body.Replace("$$Value$$", entity.HowManyTimes);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewHowManyTimes$$", string.Empty);
                        if (OldPPRegistrationEntity.HowManyTimes != null)
                        {
                            body.Replace("$$OldHowManyTimes$$", strContent);
                            body.Replace("$$Label$$", " If yes, please specify how many times you have previously attended?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.HowManyTimes);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldHowManyTimes$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewHowManyTimes$$", string.Empty);
                        body.Replace("$$OldHowManyTimes$$", string.Empty);
                    }
                    #endregion
                    #region Books Catalogue URL
                    if (OldPPRegistrationEntity.BooksUrl != entity.BooksUrl)
                    {
                        if (entity.BooksUrl != null)
                        {
                            body.Replace("$$NewBooksURL$$", strContent);
                            body.Replace("$$Label$$", "Books Catalogue URL");
                            body.Replace("$$Value$$", entity.BooksUrl);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewBooksURL$$", string.Empty);
                        if (OldPPRegistrationEntity.BooksUrl != null)
                        {
                            body.Replace("$$OldBooksURL$$", strContent);
                            body.Replace("$$Label$$", "Books Catalogue URL");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.BooksUrl);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldBooksURL$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewBooksURL$$", string.Empty);
                        body.Replace("$$OldBooksURL$$", string.Empty);
                    }
                    #endregion
                    #region ReasonToAttend
                    if (OldPPRegistrationEntity.ReasonToAttend != entity.ReasonToAttend)
                    {
                        if (entity.ReasonToAttend != null)
                        {
                            body.Replace("$$NewReasonToAttend$$", strContent);
                            body.Replace("$$Label$$", "(For International Applicants Only) Why do you want to attend the SIBF Publishers Conference ?");
                            body.Replace("$$Value$$", entity.ReasonToAttend);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewReasonToAttend$$", string.Empty);
                        if (OldPPRegistrationEntity.ReasonToAttend != null)
                        {
                            body.Replace("$$OldReasonToAttend$$", strContent);
                            body.Replace("$$Label$$", "(For International Applicants Only) Why do you want to attend the SIBF Publishers Conference ?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.ReasonToAttend);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldReasonToAttend$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewReasonToAttend$$", string.Empty);
                        body.Replace("$$OldReasonToAttend$$", string.Empty);
                    }
                    #endregion

                    #region SocialLinkURL
                    if (OldPPRegistrationEntity.SocialLinkUrl != entity.SocialLinkUrl)
                    {
                        if (entity.SocialLinkUrl != null)
                        {
                            body.Replace("$$NewSocialLinkURL$$", strContent);
                            body.Replace("$$Label$$", "Social Link URL");
                            body.Replace("$$Value$$", entity.SocialLinkUrl);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewSocialLinkURL$$", string.Empty);
                        if (OldPPRegistrationEntity.SocialLinkUrl != null)
                        {
                            body.Replace("$$OldSocialLinkURL$$", strContent);
                            body.Replace("$$Label$$", "Social Link URL");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.SocialLinkUrl);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldSocialLinkURL$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewSocialLinkURL$$", string.Empty);
                        body.Replace("$$OldSocialLinkURL$$", string.Empty);
                    }
                    #endregion

                    #region ReasonToAttendAgain
                    if (OldPPRegistrationEntity.ReasonToAttendAgain != entity.ReasonToAttendAgain)
                    {
                        if (entity.ReasonToAttendAgain != null)
                        {
                            body.Replace("$$NewReasonToAttendAgain$$", strContent);
                            body.Replace("$$Label$$", "Reason To Attend Again");
                            body.Replace("$$Value$$", entity.ReasonToAttendAgain);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewReasonToAttendAgain$$", string.Empty);
                        if (OldPPRegistrationEntity.ReasonToAttendAgain != null)
                        {
                            body.Replace("$$OldReasonToAttendAgain$$", strContent);
                            body.Replace("$$Label$$", "Reason To Attend Again");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.ReasonToAttendAgain);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldReasonToAttendAgain$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewReasonToAttendAgain$$", string.Empty);
                        body.Replace("$$OldReasonToAttendAgain$$", string.Empty);
                    }
                    #endregion

                    #region IsAttended TG
                    if (OldPPRegistrationEntity.IsAttendedTg != entity.IsAttendedTg)
                    {
                        if (entity.IsAttendedTg != null)
                        {
                            body.Replace("$$NewIsAttendAgainTG$$", strContent);
                            body.Replace("$$Label$$", "Have you applied for the SIBF Translation Grant?");
                            body.Replace("$$Value$$", entity.IsAttendedTg == "Y" ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewIsAttendAgainTG$$", string.Empty);
                        if (OldPPRegistrationEntity.IsAttendedTg != null)
                        {
                            body.Replace("$$OldIsAttendAgainTG$$", strContent);
                            body.Replace("$$Label$$", "Have you applied for the SIBF Translation Grant?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.IsAttendedTg == "Y" ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldIsAttendAgainTG$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewIsAttendAgainTG$$", string.Empty);
                        body.Replace("$$OldIsAttendAgainTG$$", string.Empty);
                    }
                    #endregion

                    #region NoofPendingTG
                    if (OldPPRegistrationEntity.NoofPendingTg != entity.NoofPendingTg)
                    {
                        if (entity.NoofPendingTg != null)
                        {
                            body.Replace("$$NewNoofPendingTG$$", strContent);
                            body.Replace("$$Label$$", "If you have applied for the SIBF Translation Grant, how many grants have you received? (pending)");
                            body.Replace("$$Value$$", entity.NoofPendingTg);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewNoofPendingTG$$", string.Empty);
                        if (OldPPRegistrationEntity.NoofPendingTg != null)
                        {
                            body.Replace("$$OldNoofPendingTG$$", strContent);
                            body.Replace("$$Label$$", "If you have applied for the SIBF Translation Grant, how many grants have you received? (pending)");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.NoofPendingTg);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldNoofPendingTG$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewNoofPendingTG$$", string.Empty);
                        body.Replace("$$OldNoofPendingTG$$", string.Empty);
                    }
                    #endregion

                    #region NoofPublishedTG
                    if (OldPPRegistrationEntity.NoofPublishedTg != entity.NoofPublishedTg)
                    {
                        if (entity.NoofPublishedTg != null)
                        {
                            body.Replace("$$NewNoofPublishTG$$", strContent);
                            body.Replace("$$Label$$", "If you have applied for the SIBF Translation Grant, how many grants have you received?");
                            body.Replace("$$Value$$", entity.NoofPublishedTg);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewNoofPublishTG$$", string.Empty);
                        if (OldPPRegistrationEntity.NoofPublishedTg != null)
                        {
                            body.Replace("$$OldNoofPublishTG$$", strContent);
                            body.Replace("$$Label$$", "If you have applied for the SIBF Translation Grant, how many grants have you received?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.NoofPublishedTg);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldNoofPublishTG$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewNoofPublishTG$$", string.Empty);
                        body.Replace("$$OldNoofPublishTG$$", string.Empty);
                    }
                    #endregion
                    #region Program Source
                    if (entity.IsAttended == EnumConversion.ToString(EnumBool.No))
                    {
                        #region PP Program Source
                        //logic for old pp genre and new pp genre
                        long kount1 = 0;
                        if (NewListPPProgrameSource.Count > OldListPPProgrameSource.Count)
                            kount1 = NewListPPProgrameSource.Except(OldListPPProgrameSource).Count();
                        else if (OldListPPProgrameSource.Count > NewListPPProgrameSource.Count)
                            kount1 = OldListPPProgrameSource.Except(NewListPPProgrameSource).Count();
                        if (kount1 > 0)
                        {
                            if (NewListGenre.Count == 0)
                            {
                                body.Replace("$$OldPPProgramSource$$", string.Empty);
                                body.Replace("$$NewPPProgramSource$$", string.Empty);
                            }
                            else
                            {
                                string oldppProgramSouce = string.Empty;
                                string newppProgramSouce = string.Empty;
                                if (OldListGenre.Count() > 0)
                                {
                                    PPSourceService PPSourceService = new PPSourceService();
                                    var ListPPSourceItems = PPSourceService.GetPPSource(new XsiExhibitionProfessionalProgramSource()).ToList().Where(i => OldListGenre.Contains(i.ItemId)).ToList();
                                    foreach (var item in ListPPSourceItems)
                                    {
                                        oldppProgramSouce += item.Title + ", ";
                                    }
                                    if (oldppProgramSouce.Length > 0)
                                        oldppProgramSouce = oldppProgramSouce.Remove(oldppProgramSouce.Length - 1);
                                }
                                if (NewListGenre.Count() > 0)
                                {
                                    PPSourceService PPSourceService = new PPSourceService();
                                    var ListPPSourceItems = PPSourceService.GetPPSource(new XsiExhibitionProfessionalProgramSource()).ToList().Where(i => NewListGenre.Contains(i.ItemId)).ToList();
                                    foreach (var item in ListPPSourceItems)
                                    {
                                        newppProgramSouce += item.Title + ", ";
                                    }
                                    if (newppProgramSouce.Length > 0)
                                        newppProgramSouce = newppProgramSouce.Remove(newppProgramSouce.Length - 1);
                                }
                                //body.Replace("$$OldPPProgramSource$$", oldppProgramSouce);
                                //body.Replace("$$NewPPProgramSource$$", newppProgramSouce);
                                if (newppProgramSouce.Length > 0)
                                {
                                    body.Replace("$$NewPPProgramSource$$", strContent);
                                    body.Replace("$$Label$$", "If no, how did you hear about the programme?");
                                    body.Replace("$$Value$$", newppProgramSouce);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewPPProgramSource$$", string.Empty);
                                if (oldppProgramSouce.Length > 0)
                                {
                                    body.Replace("$$OldPPProgramSource$$", strContent);
                                    body.Replace("$$Label$$", " If no, how did you hear about the programme?");
                                    body.Replace("$$Value$$", oldppProgramSouce);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldPPProgramSource$$", string.Empty);
                            }
                        }
                        else
                        {
                            body.Replace("$$OldPPProgramSource$$", string.Empty);
                            body.Replace("$$NewPPProgramSource$$", string.Empty);
                        }
                        #endregion
                        #region Other PP Program Source
                        if (OldPPRegistrationEntity.ProgramOther != entity.ProgramOther)
                        {
                            if (entity.ProgramOther != null)
                            {
                                body.Replace("$$NewOtherProgram$$", strContent);
                                body.Replace("$$Label$$", "If no, how did you hear about the programme?(Other Source))");
                                body.Replace("$$Value$$", entity.ProgramOther);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewOtherProgram$$", string.Empty);
                            if (OldPPRegistrationEntity.ProgramOther != null)
                            {
                                body.Replace("$$OldOtherProgram$$", strContent);
                                body.Replace("$$Label$$", "If no, how did you hear about the programme?(Other Source))");
                                body.Replace("$$Value$$", OldPPRegistrationEntity.ProgramOther);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldOtherProgram$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewOtherProgram$$", string.Empty);
                            body.Replace("$$OldOtherProgram$$", string.Empty);
                        }
                        #endregion
                    }
                    else
                    {
                        body.Replace("$$NewOtherProgram$$", string.Empty);
                        body.Replace("$$OldOtherProgram$$", string.Empty);
                        body.Replace("$$OldPPProgramSource$$", string.Empty);
                        body.Replace("$$NewPPProgramSource$$", string.Empty);
                    }
                    #endregion  
                    #region UnSoldRightsBooks
                    if (OldPPRegistrationEntity.UnSoldRightsBooks != entity.UnSoldRightsBooks)
                    {
                        if (entity.UnSoldRightsBooks != null)
                        {
                            body.Replace("$$NewUnNewRightsBooks$$", strContent);
                            body.Replace("$$Label$$", "Do you own rights to multiple books that have not yet been sold or translated into other languages?");
                            body.Replace("$$Value$$", entity.UnSoldRightsBooks);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewUnNewRightsBooks$$", string.Empty);
                        if (OldPPRegistrationEntity.UnSoldRightsBooks != null)
                        {
                            body.Replace("$$OldUnSoldRightsBooks$$", strContent);
                            body.Replace("$$Label$$", "Do you own rights to multiple books that have not yet been sold or translated into other languages?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.UnSoldRightsBooks);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldUnSoldRightsBooks$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewUnNewRightsBooks$$", string.Empty);
                        body.Replace("$$OldUnSoldRightsBooks$$", string.Empty);
                    }
                    #endregion

                    #region AcquireRightsToPublished
                    if (OldPPRegistrationEntity.AcquireRightsToPublished != entity.AcquireRightsToPublished)
                    {
                        if (entity.AcquireRightsToPublished != null)
                        {
                            body.Replace("$$NewAcquireRightsToPublish$$", strContent);
                            body.Replace("$$Label$$", "Are you actively looking to acquire rights to books published in Arabic which have not yet been translated or sold in your market?");
                            body.Replace("$$Value$$", entity.ProgramOther);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewAcquireRightsToPublish$$", string.Empty);
                        if (OldPPRegistrationEntity.AcquireRightsToPublished != null)
                        {
                            body.Replace("$$OldAcquireRightsToPublish$$", strContent);
                            body.Replace("$$Label$$", "Are you actively looking to acquire rights to books published in Arabic which have not yet been translated or sold in your market?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.AcquireRightsToPublished);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldAcquireRightsToPublish$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewAcquireRightsToPublish$$", string.Empty);
                        body.Replace("$$OldAcquireRightsToPublish$$", string.Empty);
                    }
                    #endregion

                    #region IsAlreadyBoughtRights
                    if (OldPPRegistrationEntity.IsAlreadyBoughtRights != entity.IsAlreadyBoughtRights)
                    {
                        if (entity.IsAlreadyBoughtRights != null)
                        {
                            body.Replace("$$NewIsAlreadyBoughtRights$$", strContent);
                            body.Replace("$$Label$$", "Have you bought or sold book rights to/from the Arabic market previously?");
                            body.Replace("$$Value$$", entity.IsAlreadyBoughtRights == "Y" ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewIsAlreadyBoughtRights$$", string.Empty);
                        if (OldPPRegistrationEntity.IsAlreadyBoughtRights != null)
                        {
                            body.Replace("$$OldIsAlreadyBoughtRights$$", strContent);
                            body.Replace("$$Label$$", "Have you bought or sold book rights to/from the Arabic market previously?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.IsAlreadyBoughtRights == "Y" ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldIsAlreadyBoughtRights$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewIsAlreadyBoughtRights$$", string.Empty);
                        body.Replace("$$OldIsAlreadyBoughtRights$$", string.Empty);
                    }
                    #endregion

                    #region KeyPublisherTitles or Business Region
                    if (entity.IsAlreadyBoughtRights == EnumConversion.ToString(EnumBool.Yes))
                    {
                        #region KeyPublisherTitlesOrBusinessRegion
                        if (OldPPRegistrationEntity.KeyPublisherTitlesOrBusinessRegion != entity.KeyPublisherTitlesOrBusinessRegion)
                        {
                            if (entity.KeyPublisherTitlesOrBusinessRegion != null)
                            {
                                body.Replace("$$NewKeyPublisherTitlesOrBusinessRegion$$", strContent);
                                body.Replace("$$Label$$", "(For International Applicants Only) If yes, please name key publishers (and titles if possible)");
                                body.Replace("$$Value$$", entity.KeyPublisherTitlesOrBusinessRegion);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewKeyPublisherTitlesOrBusinessRegion$$", string.Empty);
                            if (OldPPRegistrationEntity.KeyPublisherTitlesOrBusinessRegion != null)
                            {
                                body.Replace("$$OldKeyPublisherTitlesOrBusinessRegion$$", strContent);
                                body.Replace("$$Label$$", "(For International Applicants Only) If yes, please name key publishers (and titles if possible)");
                                body.Replace("$$Value$$", OldPPRegistrationEntity.KeyPublisherTitlesOrBusinessRegion);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldKeyPublisherTitlesOrBusinessRegion$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewKeyPublisherTitlesOrBusinessRegion$$", string.Empty);
                            body.Replace("$$OldKeyPublisherTitlesOrBusinessRegion$$", string.Empty);
                        }
                        #endregion
                    }
                    else
                    {
                        #region KeyPublisherTitlesOrBusinessRegion
                        if (OldPPRegistrationEntity.KeyPublisherTitlesOrBusinessRegion != entity.KeyPublisherTitlesOrBusinessRegion)
                        {
                            if (entity.KeyPublisherTitlesOrBusinessRegion != null)
                            {
                                body.Replace("$$NewKeyPublisherTitlesOrBusinessRegion$$", strContent);
                                body.Replace("$$Label$$", " If no, what would encourage you to do more business in the region? ");
                                body.Replace("$$Value$$", entity.KeyPublisherTitlesOrBusinessRegion);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewKeyPublisherTitlesOrBusinessRegion$$", string.Empty);
                            if (OldPPRegistrationEntity.KeyPublisherTitlesOrBusinessRegion != null)
                            {
                                body.Replace("$$OldKeyPublisherTitlesOrBusinessRegion$$", strContent);
                                body.Replace("$$Label$$", " If no, what would encourage you to do more business in the region? ");
                                body.Replace("$$Value$$", OldPPRegistrationEntity.KeyPublisherTitlesOrBusinessRegion);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldKeyPublisherTitlesOrBusinessRegion$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewKeyPublisherTitlesOrBusinessRegion$$", string.Empty);
                            body.Replace("$$OldKeyPublisherTitlesOrBusinessRegion$$", string.Empty);
                        }
                        #endregion
                    }
                    #endregion

                    #region CatalogueMainLanguageOfOriginalTitle
                    if (OldPPRegistrationEntity.CatalogueMainLanguageOfOriginalTitle != entity.CatalogueMainLanguageOfOriginalTitle)
                    {
                        if (entity.CatalogueMainLanguageOfOriginalTitle != null)
                        {
                            body.Replace("$$NewCatalogueMainLanguageofOriginalTitle$$", strContent);
                            body.Replace("$$Label$$", "What are the main language(s) of the original titles in your catalogue/list? ");
                            body.Replace("$$Value$$", entity.ProgramOther);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewCatalogueMainLanguageofOriginalTitle$$", string.Empty);
                        if (OldPPRegistrationEntity.CatalogueMainLanguageOfOriginalTitle != null)
                        {
                            body.Replace("$$OldCatalogueMainLanguageofOriginalTitle$$", strContent);
                            body.Replace("$$Label$$", "What are the main language(s) of the original titles in your catalogue/list? ");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.CatalogueMainLanguageOfOriginalTitle);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldCatalogueMainLanguageofOriginalTitle$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewCatalogueMainLanguageofOriginalTitle$$", string.Empty);
                        body.Replace("$$OldCatalogueMainLanguageofOriginalTitle$$", string.Empty);
                    }
                    #endregion

                    #region MainTerritories
                    if (OldPPRegistrationEntity.MainTerritories != entity.MainTerritories)
                    {
                        if (entity.MainTerritories != null)
                        {
                            body.Replace("$$NewMainTerritories$$", strContent);
                            body.Replace("$$Label$$", "What are the main territories you buy/sell into?");
                            body.Replace("$$Value$$", entity.MainTerritories);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewMainTerritories$$", string.Empty);
                        if (OldPPRegistrationEntity.MainTerritories != null)
                        {
                            body.Replace("$$OldMainTerritories$$", strContent);
                            body.Replace("$$Label$$", "What are the main territories you buy/sell into?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.MainTerritories);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldMainTerritories$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewMainTerritories$$", string.Empty);
                        body.Replace("$$OldMainTerritories$$", string.Empty);
                    }
                    #endregion

                    #region SuggestionToSupportApp
                    if (OldPPRegistrationEntity.SuggestionToSupportApp != entity.SuggestionToSupportApp)
                    {
                        if (entity.SuggestionToSupportApp != null)
                        {
                            body.Replace("$$NewSuggestionToSupportApp$$", strContent);
                            body.Replace("$$Label$$", "Do you have anything else you would like to add to help support your application?");
                            body.Replace("$$Value$$", entity.SuggestionToSupportApp);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewSuggestionToSupportApp$$", string.Empty);
                        if (OldPPRegistrationEntity.SuggestionToSupportApp != null)
                        {
                            body.Replace("$$OldSuggestionToSupportApp$$", strContent);
                            body.Replace("$$Label$$", "Do you have anything else you would like to add to help support your application?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.SuggestionToSupportApp);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldSuggestionToSupportApp$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewSuggestionToSupportApp$$", string.Empty);
                        body.Replace("$$OldSuggestionToSupportApp$$", string.Empty);
                    }
                    #endregion

                    #region RoundTableTopics
                    if (OldPPRegistrationEntity.RoundTableTopics != entity.RoundTableTopics)
                    {
                        if (entity.RoundTableTopics != null)
                        {
                            body.Replace("$$NewRoundTableTopics$$", strContent);
                            body.Replace("$$Label$$", "This year there will be a seminar programme, what content/type of topics would be of interest for you for the roundtable discussions? ");
                            body.Replace("$$Value$$", entity.RoundTableTopics);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewRoundTableTopics$$", string.Empty);
                        if (OldPPRegistrationEntity.RoundTableTopics != null)
                        {
                            body.Replace("$$OldRoundTableTopics$$", strContent);
                            body.Replace("$$Label$$", "This year there will be a seminar programme, what content/type of topics would be of interest for you for the roundtable discussions? ");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.RoundTableTopics);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldRoundTableTopics$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewRoundTableTopics$$", string.Empty);
                        body.Replace("$$OldRoundTableTopics$$", string.Empty);
                    }
                    #endregion

                    #region SpeakOnRoundTable
                    if (OldPPRegistrationEntity.SpeakOnRoundTable != entity.SpeakOnRoundTable)
                    {
                        if (entity.SpeakOnRoundTable != null)
                        {
                            body.Replace("$$NewSpeakOnRoundTable$$", strContent);
                            body.Replace("$$Label$$", "Are you interested in speaking on a roundtable/panel event?");
                            body.Replace("$$Value$$", entity.SpeakOnRoundTable);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewSpeakOnRoundTable$$", string.Empty);
                        if (OldPPRegistrationEntity.SpeakOnRoundTable != null)
                        {
                            body.Replace("$$OldSpeakOnRoundTable$$", strContent);
                            body.Replace("$$Label$$", "Are you interested in speaking on a roundtable/panel event?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.SpeakOnRoundTable);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldSpeakOnRoundTable$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewSpeakOnRoundTable$$", string.Empty);
                        body.Replace("$$OldSpeakOnRoundTable$$", string.Empty);
                    }
                    #endregion

                    #region RecommendPublishers
                    if (OldPPRegistrationEntity.RecommendPublishers != entity.RecommendPublishers)
                    {
                        if (entity.RecommendPublishers != null)
                        {
                            body.Replace("$$NewRecommendedPublishers$$", strContent);
                            body.Replace("$$Label$$", " Are there any publishers you would recommend being invited to SIBF Publishers Conference?");
                            body.Replace("$$Value$$", entity.RecommendPublishers);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewRecommendedPublishers$$", string.Empty);
                        if (OldPPRegistrationEntity.RecommendPublishers != null)
                        {
                            body.Replace("$$OldRecommendedPublishers$$", strContent);
                            body.Replace("$$Label$$", " Are there any publishers you would recommend being invited to SIBF Publishers Conference?");
                            body.Replace("$$Value$$", OldPPRegistrationEntity.RecommendPublishers);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldRecommendedPublishers$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewRecommendedPublishers$$", string.Empty);
                        body.Replace("$$OldRecommendedPublishers$$", string.Empty);
                    }
                    #endregion
                    body.Replace("$$OldFileName$$", string.Empty);
                    body.Replace("$$OldFileName1$$", string.Empty);
                    body.Replace("$$NewFileName$$", string.Empty);
                    body.Replace("$$NewFileName1$$", string.Empty);

                    //if (entity.FileNumber != null)
                    //    body.Replace("$$FileNumber$$", entity.FileNumber);
                    body.Replace("$$FileNumber$$", string.Empty);

                    if (OldPPRegistrationEntity.FirstName != null && OldPPRegistrationEntity.LastName != null)
                        body.Replace("$$ApplicantNameEn$$", OldPPRegistrationEntity.FirstName + " " + OldPPRegistrationEntity.LastName);
                    else if (OldPPRegistrationEntity.FirstName != null)
                        body.Replace("$$ApplicantNameEn$$", OldPPRegistrationEntity.FirstName);
                    else if (OldPPRegistrationEntity.LastName != null)
                        body.Replace("$$ApplicantNameEn$$", OldPPRegistrationEntity.LastName);

                    if (IsAnyFieldChanged)
                        if (strEmailContentEmail != string.Empty)
                        {
                            UpdateEditRequest(entity.ItemId);
                            //strEmailContentEmail
                            _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString().Replace("$$SpecificName$$", strEmailContentAdmin));

                            if (OldPPRegistrationEntity != default(XsiExhibitionProfessionalProgramRegistration))
                            {
                                string userbody = body.ToString();
                                if (OldPPRegistrationEntity.FirstName != null && OldPPRegistrationEntity.LastName != null)
                                    userbody = userbody.Replace("$$SpecificName$$", OldPPRegistrationEntity.FirstName + " " + OldPPRegistrationEntity.LastName);
                                else if (OldPPRegistrationEntity.FirstName != null)
                                    userbody = userbody.Replace("$$SpecificName$$", OldPPRegistrationEntity.FirstName);
                                else if (OldPPRegistrationEntity.LastName != null)
                                    userbody = userbody.Replace("$$SpecificName$$", OldPPRegistrationEntity.LastName);

                                if (!string.IsNullOrEmpty(OldPPRegistrationEntity.Email))
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, OldPPRegistrationEntity.Email, strEmailContentSubject, userbody);
                            }
                        }
                    #endregion
                }
            }
            #endregion
        }
        #endregion
        #region PDF Helper Methods
        private string GeneratePDF(long RegisteredId, long professionalprogramid, long langId)
        {
            var ppEntity = MethodFactory.GetProfessionalProgram(professionalprogramid);
            string DayOne = "", DayTwo = "", DayThree = "";
            if (ppEntity != null)
            {
                //  ProfessionalProgramId = ppEntity.ProfessionalProgramId;
                if (ppEntity.StartDateDayOne != null)
                    DayOne = ppEntity.StartDateDayOne.Value.ToString("dddd dd MMMM");
                if (ppEntity.StartDateDayTwo != null)
                    DayTwo = ppEntity.StartDateDayTwo.Value.ToString("dddd dd MMMM");
                if (ppEntity.StartDateDayThree != null)
                    DayThree = ppEntity.StartDateDayThree.Value.ToString("dddd dd MMMM");
            }

            var PrintList = BindCustomSchedule(RegisteredId, professionalprogramid);// GetPrintList(RegisteredId, professionalprogramid);
            if (PrintList.Count > 0)
            {
                #region Font declaration
                //BaseFont bfTimes = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                BaseFont bfTimes = BaseFont.CreateFont(_appCustomSettings.ApplicationPhysicalPath + "Xsi\\Content\\fonts\\theserif_light_plain-webfont.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                Font fontNormal = new Font(bfTimes, 12, Font.NORMAL, new BaseColor(0, 0, 0));
                Font fontNormalNotes = new Font(bfTimes, 10, Font.NORMAL, new BaseColor(0, 0, 0));
                Font fontBoldNotes = new Font(bfTimes, 10, Font.BOLD, new BaseColor(0, 0, 0));
                Font fontBold = new Font(bfTimes, 11, Font.BOLD, new BaseColor(0, 0, 0));
                Font fontHeader = new Font(bfTimes, 12, Font.BOLD, new BaseColor(255, 255, 255));
                Font fontOrangeBold = new Font(bfTimes, 12, Font.BOLD, new BaseColor(241, 88, 34));
                Font fontOrange = new Font(bfTimes, 12, Font.NORMAL, new BaseColor(241, 88, 34));
                Font fontPinkBold = new Font(bfTimes, 12, Font.BOLD, new BaseColor(224, 25, 85));
                Font fontPink = new Font(bfTimes, 12, Font.NORMAL, new BaseColor(224, 25, 85));
                Font fontPurpleBold = new Font(bfTimes, 12, Font.BOLD, new BaseColor(126, 65, 153));
                Font fontPurple = new Font(bfTimes, 12, Font.NORMAL, new BaseColor(126, 65, 153));
                Font fontColorBold = new Font();
                Font fontColor = new Font();
                iTextSharp.text.BaseColor color = null;
                fontColorBold = fontOrangeBold;
                fontColor = fontOrange;
                color = new BaseColor(244, 120, 54); // new BaseColor(244, 120, 54);

                #endregion
                Document document = new Document(iTextSharp.text.PageSize.A4, 25f, 25f, 112f, 100f);
                Utility.HeaderFooter header = new Utility.HeaderFooter();

                string strtimestamp = MethodFactory.ArabianTimeNow().ToString("yyyyMMddHHmmssfff");
                string downloadUrl = _appCustomSettings.UploadsCMSPath + "/PPRegistration/PDFS/" + strtimestamp + "MySchedule.pdf";
                PdfWriter pw = PdfWriter.GetInstance(document, new System.IO.FileStream(_appCustomSettings.UploadsPhysicalPath + "PPRegistration\\PDFS\\" + strtimestamp + "MySchedule.pdf", FileMode.Create));
                pw.PageEvent = header;

                document.Open();
                #region table
                PdfPTable table = new PdfPTable(4);
                table.WidthPercentage = 100;
                table.SpacingBefore = 17f;
                table.DefaultCell.Border = Rectangle.NO_BORDER;
                float[] widths = new float[] { 3f, 3f, 3f, 3f };
                table.SetWidths(widths);
                PdfPCell LeftCell = new PdfPCell();
                PdfPCell MiddleCell = new PdfPCell();
                PdfPCell RightCell = new PdfPCell();
                PdfPCell LastCell = new PdfPCell();
                #region Header
                LeftCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                LeftCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                LeftCell.BackgroundColor = color;
                LeftCell.BorderColor = new BaseColor(204, 204, 204);
                LeftCell.Border = Rectangle.RIGHT_BORDER;
                LeftCell.FixedHeight = 25f;
                LeftCell.PaddingBottom = 5f;
                LeftCell.BorderWidthRight = 0.5f;
                LeftCell.PaddingLeft = 10F;
                LeftCell.Phrase = new Phrase("Time", fontHeader);
                table.AddCell(LeftCell);

                MiddleCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                MiddleCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                MiddleCell.PaddingBottom = 5f;
                MiddleCell.BackgroundColor = color;
                MiddleCell.Border = PdfPCell.NO_BORDER;
                MiddleCell.Phrase = new Phrase(DayOne, fontHeader);
                table.AddCell(MiddleCell);

                RightCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                RightCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                RightCell.PaddingBottom = 5f;
                RightCell.BackgroundColor = color;
                RightCell.BorderColor = new BaseColor(204, 204, 204);
                RightCell.Border = Rectangle.LEFT_BORDER;
                RightCell.BorderWidthLeft = 0.5f;
                RightCell.Phrase = new Phrase(DayTwo, fontHeader);
                table.AddCell(RightCell);

                LastCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                LastCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                LastCell.PaddingBottom = 5f;
                LastCell.BackgroundColor = color;
                LastCell.BorderColor = new BaseColor(204, 204, 204);
                LastCell.Border = Rectangle.LEFT_BORDER;
                LastCell.BorderWidthLeft = 0.5f;
                LastCell.Phrase = new Phrase(DayThree, fontHeader);
                table.AddCell(LastCell);
                #endregion
                #region Body
                //var RegisteredId = MethodFactory.GetRegisteredId(memberId);            
                foreach (var row in PrintList)
                {
                    // TableRecord(fontNormal, table, LeftCell, MiddleCell, RightCell, new iTextSharp.text.Color(249, 251, 253), row.StartTime + " : " + row.EndTime, GetOtherPersonName(row.SlotDayOne, RegisteredId), GetOtherPersonName(row.SlotDayTwo, RegisteredId));

                    TableRecord(fontNormal, table, LeftCell, MiddleCell, RightCell, LastCell, new iTextSharp.text.BaseColor(249, 251, 253), row.StartTime + " : " + row.EndTime, GetOtherPersonName(row.SlotDayOne, RegisteredId), GetOtherPersonName(row.SlotDayTwo, RegisteredId), GetOtherPersonName(row.SlotDayThree, RegisteredId));
                }
                if (table != null)
                    document.Add(table);
                #endregion
                #endregion
                document.Close();
                return downloadUrl;
            }
            return string.Empty;
        }

        private static void TableRecord(Font fontNormal, PdfPTable table, PdfPCell LeftCell, PdfPCell MiddleCell, PdfPCell RightCell, PdfPCell LastCell, BaseColor color, string time, string dayOneName, string dayTwoName, string dayThreeName)
        {
            float fixedHeight = 20f;
            bool useAscender = true;

            LeftCell.HorizontalAlignment = Element.ALIGN_CENTER;
            LeftCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            LeftCell.MinimumHeight = fixedHeight;
            LeftCell.UseAscender = useAscender;
            LeftCell.BackgroundColor = color;
            LeftCell.BorderColor = new BaseColor(204, 204, 204);
            LeftCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
            LeftCell.BorderWidthRight = 0.5f;
            LeftCell.PaddingLeft = 10F;
            LeftCell.Phrase = new Phrase(time, fontNormal);
            table.AddCell(LeftCell);

            MiddleCell.HorizontalAlignment = Element.ALIGN_CENTER;
            MiddleCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            MiddleCell.MinimumHeight = fixedHeight;
            MiddleCell.UseAscender = useAscender;
            MiddleCell.BackgroundColor = color;
            MiddleCell.BorderColor = new BaseColor(204, 204, 204);
            MiddleCell.Border = Rectangle.BOTTOM_BORDER;
            MiddleCell.BorderWidthRight = 0.5f;
            MiddleCell.Phrase = new Phrase(dayOneName, fontNormal);
            table.AddCell(MiddleCell);

            RightCell.HorizontalAlignment = Element.ALIGN_CENTER;
            RightCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            RightCell.MinimumHeight = fixedHeight;
            RightCell.UseAscender = useAscender;
            RightCell.BackgroundColor = color;
            RightCell.BorderColor = new BaseColor(204, 204, 204);
            RightCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
            RightCell.BorderWidthLeft = 0.5f;
            RightCell.Phrase = new Phrase(dayTwoName, fontNormal);
            table.AddCell(RightCell);

            LastCell.HorizontalAlignment = Element.ALIGN_CENTER;
            LastCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            LastCell.MinimumHeight = fixedHeight;
            LastCell.UseAscender = useAscender;
            LastCell.BackgroundColor = color;
            LastCell.BorderColor = new BaseColor(204, 204, 204);
            LastCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
            LastCell.BorderWidthLeft = 0.5f;
            LastCell.Phrase = new Phrase(dayThreeName, fontNormal);
            table.AddCell(LastCell);
        }

        private string GenerateProfilePDF(long RegisteredId, long langId)
        {
            #region Font declaration
            BaseFont arialuni = BaseFont.CreateFont(_appCustomSettings.ApplicationPhysicalPath + "Xsi\\Content\\fonts\\tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font fontNormalAr = new Font(arialuni, 12, Font.NORMAL, new BaseColor(0, 0, 0));

            BaseFont bfTimes = BaseFont.CreateFont(_appCustomSettings.ApplicationPhysicalPath + "Xsi\\Content\\fonts\\theserif_light_plain-webfont.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font fontNormal = new Font(bfTimes, 12, Font.NORMAL, new BaseColor(0, 0, 0));
            Font fontNormalNotes = new Font(bfTimes, 10, Font.NORMAL, new BaseColor(0, 0, 0));
            Font fontBoldNotes = new Font(bfTimes, 10, Font.BOLD, new BaseColor(0, 0, 0));
            Font fontBold = new Font(bfTimes, 11, Font.BOLD, new BaseColor(0, 0, 0));
            Font fontHeader = new Font(bfTimes, 12, Font.BOLD, new BaseColor(255, 255, 255));
            Font fontOrangeBold = new Font(bfTimes, 12, Font.BOLD, new BaseColor(241, 88, 34));
            Font fontOrange = new Font(bfTimes, 12, Font.NORMAL, new BaseColor(241, 88, 34));
            Font fontPinkBold = new Font(bfTimes, 12, Font.BOLD, new BaseColor(224, 25, 85));
            Font fontPink = new Font(bfTimes, 12, Font.NORMAL, new BaseColor(224, 25, 85));
            Font fontPurpleBold = new Font(bfTimes, 12, Font.BOLD, new BaseColor(126, 65, 153));
            Font fontPurple = new Font(bfTimes, 12, Font.NORMAL, new BaseColor(126, 65, 153));
            Font fontColorBold = new Font();
            Font fontColor = new Font();
            iTextSharp.text.BaseColor color = null;
            fontColorBold = fontOrangeBold;
            fontColor = fontOrange;
            color = new BaseColor(244, 120, 54);

            #endregion

            Document document = new Document(iTextSharp.text.PageSize.A4, 25f, 25f, 112f, 100f);
            Utility.HeaderFooter header = new Utility.HeaderFooter();

            string downloadUrl = _appCustomSettings.UploadsCMSPath + "/PPRegistration/PDFS/" + "MyProfile.pdf";
            PdfWriter pw = PdfWriter.GetInstance(document, new System.IO.FileStream(_appCustomSettings.UploadsPhysicalPath + "/PPRegistration/PDFS/" + "MyProfile.pdf", FileMode.Create));
            pw.PageEvent = header;

            document.Open();
            #region Calculations table
            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;
            table.SpacingBefore = 17f;
            table.DefaultCell.Border = Rectangle.NO_BORDER;
            float[] widths = new float[] { 3f, 7f };
            table.SetWidths(widths);
            PdfPCell LeftCell = new PdfPCell();
            PdfPCell RightCell = new PdfPCell();
            #region Table
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                XsiExhibitionProfessionalProgramRegistration entity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(RegisteredId);
                if (entity != null)
                {
                    if (entity.Title != null)
                    {
                        string title = string.Empty;
                        if (entity.Title == "M")
                            title = "Mr";
                        else if (entity.TradeType == "F")
                            title = "Ms";
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Title", title);
                    }
                    if (entity.TableId != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Table Number", entity.TableId.ToString());
                    if (entity.FirstName != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "First Name of Attendee", entity.FirstName);
                    if (entity.LastName != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Last Name of Attendee", entity.LastName);
                    if (entity.FirstNameAr != null)
                        TableRecordProfileArabic(fontNormalAr, fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "First Name of Attendee in Arabic", entity.FirstNameAr);
                    if (entity.LastNameAr != null)
                        TableRecordProfileArabic(fontNormalAr, fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Last Name of Attendee in Arabic", entity.LastNameAr);
                    if (entity.Email != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Email", entity.Email);
                    if (entity.CountryId != null)
                    {
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Country", MethodFactory.GetCountryName(entity.CountryId.Value, langId));
                        if (entity.CityId != null)
                        {
                            string strCity = MethodFactory.GetCityName(entity.CityId.Value, langId);
                            TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "City", strCity);
                            if (strCity == EnumConversion.ToString(EnumOtherText.OtherCity))
                                if (entity.OtherCity != null)
                                    TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Other City", entity.OtherCity);
                        }
                    }
                    if (entity.PhoneNumber != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Phone", entity.PhoneNumber.Replace('$', '-'));
                    if (entity.MobileNumber != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Mobile", entity.MobileNumber.Replace('$', '-'));
                    if (entity.ZipCode != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Zip/Postal Code", entity.ZipCode);
                    if (entity.CompanyName != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Company Name", entity.CompanyName);
                    if (entity.CompanyNameAr != null)
                        TableRecordProfileArabic(fontNormalAr, fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Company Name in Arabic", entity.CompanyNameAr);
                    if (entity.JobTitle != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Job Title", entity.JobTitle);
                    if (entity.JobTitleAr != null)
                        TableRecordProfileArabic(fontNormalAr, fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Job Title in Arabic", entity.JobTitleAr);
                    if (entity.WorkProfile != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "company profile", entity.WorkProfile);
                    if (entity.WorkProfileAr != null)
                        TableRecordProfileArabic(fontNormalAr, fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "company profile in Arabic", entity.WorkProfileAr);
                    if (entity.Website != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Website", entity.Website);
                    if (entity.IsAttended != null)
                    {
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Have you attended the SIBF Publishers Conference before?", entity.IsAttended == EnumConversion.ToString(EnumBool.Yes) ? "Yes" : "No");
                        if (entity.IsAttended == EnumConversion.ToString(EnumBool.Yes))
                            if (entity.HowManyTimes != null)
                                TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "If yes, please specify how many times you have previously attended?", entity.HowManyTimes);
                    }
                    if (entity.ReasonToAttend != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Why do you want to attend the SIBF Publishers Conference", entity.ReasonToAttend);
                    #region Genre
                    string strGenre = string.Empty;
                    PPGenreService PPGenreService = new PPGenreService();
                    XsiExhibitionPpgenre ppgenreenttiy = new XsiExhibitionPpgenre();
                    ppgenreenttiy.ProfessionalProgramRegistrationId = RegisteredId;
                    HashSet<long?> GenreGroupIdList = new HashSet<long?>(PPGenreService.GetPPGenre(ppgenreenttiy).Select(s => s.GenreId).Distinct().ToList());
                    GenreService GenreService = new GenreService();
                    XsiGenre where = new XsiGenre();
                    //where.LanguageId = EnglishId;
                    List<XsiGenre> GenreList = GenreService.GetGenre(where).Where(p => GenreGroupIdList.Contains(p.ItemId)).ToList();
                    foreach (XsiGenre rows in GenreList)
                        strGenre += rows.Title + ", ";
                    TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Which types of publishing/genre do you represent?", strGenre);
                    if (entity.OtherGenre != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Others (If other category)", entity.OtherGenre);
                    #endregion
                    if (entity.TradeType != null)
                    {
                        string tradeType = string.Empty;
                        if (entity.TradeType == "A")
                            tradeType = "Both";
                        else if (entity.TradeType == "B")
                            tradeType = "Buying";
                        else if (entity.TradeType == "S")
                            tradeType = "Selling";
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Are you buying or selling rights?", tradeType);
                    }
                    if (entity.IsEbooks != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Do you have any ebooks or apps you want to sell?", entity.IsEbooks == EnumConversion.ToString(EnumBool.Yes) ? "Yes" : "No");
                    if (entity.ExamplesOfTranslatedBooks != null)
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Examples of rights to books you have bought and translated to date", entity.ExamplesOfTranslatedBooks);
                    if (entity.IsInterested != null)
                    {
                        TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "Are you interested in learning about digital publishing?", entity.IsInterested == EnumConversion.ToString(EnumBool.Yes) ? "Yes" : "No");
                        if (entity.IsInterested == EnumConversion.ToString(EnumBool.Yes))
                            if (entity.InterestedInfo != null)
                                TableRecordProfile(fontNormal, table, LeftCell, RightCell, new iTextSharp.text.BaseColor(249, 251, 253), "If yes, please provide more information", entity.InterestedInfo);
                    }
                }
            }
            if (table != null)
                document.Add(table);
            #endregion
            #endregion
            document.Close();
            return downloadUrl;
        }

        private static void TableRecordProfileArabic(Font fontNormalAr, Font fontNormal, PdfPTable table, PdfPCell LeftCell, PdfPCell RightCell, BaseColor color, string label, string value)
        {
            float fixedHeight = 20f;
            bool useAscender = false;

            LeftCell.HorizontalAlignment = Element.ALIGN_LEFT;
            LeftCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            LeftCell.MinimumHeight = fixedHeight;
            LeftCell.UseAscender = true;
            LeftCell.BackgroundColor = color;
            LeftCell.BorderColor = new BaseColor(204, 204, 204);
            LeftCell.Border = Rectangle.BOX;
            LeftCell.BorderWidthRight = 0.5f;
            LeftCell.PaddingLeft = 10F;
            LeftCell.PaddingTop = 10F;
            LeftCell.PaddingBottom = 10F;
            LeftCell.PaddingRight = 10F;
            LeftCell.Phrase = new Phrase(label, fontNormal);
            table.AddCell(LeftCell);

            RightCell.HorizontalAlignment = Element.ALIGN_LEFT;
            RightCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            RightCell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
            RightCell.MinimumHeight = fixedHeight;
            RightCell.UseAscender = useAscender;
            RightCell.BackgroundColor = color;
            RightCell.BorderColor = new BaseColor(204, 204, 204);
            RightCell.Border = Rectangle.BOX;
            RightCell.BorderWidthLeft = 0.5f;
            RightCell.PaddingLeft = 10F;
            RightCell.PaddingTop = 10F;
            RightCell.PaddingBottom = 10F;
            RightCell.PaddingRight = 10F;
            RightCell.Phrase = new Phrase(value, fontNormalAr);
            table.AddCell(RightCell);
        }
        private static void TableRecord(Font fontNormal, PdfPTable table, PdfPCell LeftCell, PdfPCell MiddleCell, PdfPCell RightCell, BaseColor color, string time, string dayOneName, string dayTwoName)
        {
            float fixedHeight = 20f;
            bool useAscender = true;

            LeftCell.HorizontalAlignment = Element.ALIGN_CENTER;
            LeftCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            LeftCell.MinimumHeight = fixedHeight;
            LeftCell.UseAscender = useAscender;
            LeftCell.BackgroundColor = color;
            LeftCell.BorderColor = new BaseColor(204, 204, 204);
            LeftCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
            LeftCell.BorderWidthRight = 0.5f;
            LeftCell.PaddingLeft = 10F;
            LeftCell.Phrase = new Phrase(time, fontNormal);
            table.AddCell(LeftCell);

            MiddleCell.HorizontalAlignment = Element.ALIGN_CENTER;
            MiddleCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            MiddleCell.MinimumHeight = fixedHeight;
            MiddleCell.UseAscender = useAscender;
            MiddleCell.BackgroundColor = color;
            MiddleCell.BorderColor = new BaseColor(204, 204, 204);
            MiddleCell.Border = Rectangle.BOTTOM_BORDER;
            MiddleCell.BorderWidthRight = 0.5f;
            MiddleCell.Phrase = new Phrase(dayOneName, fontNormal);
            table.AddCell(MiddleCell);

            RightCell.HorizontalAlignment = Element.ALIGN_CENTER;
            RightCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            RightCell.MinimumHeight = fixedHeight;
            RightCell.UseAscender = useAscender;
            RightCell.BackgroundColor = color;
            RightCell.BorderColor = new BaseColor(204, 204, 204);
            RightCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
            RightCell.BorderWidthLeft = 0.5f;
            RightCell.Phrase = new Phrase(dayTwoName, fontNormal);
            table.AddCell(RightCell);
        }

        private static void TableRecordProfile(Font fontNormal, PdfPTable table, PdfPCell LeftCell, PdfPCell RightCell, BaseColor color, string label, string value)
        {
            float fixedHeight = 20f;
            bool useAscender = true;

            LeftCell.HorizontalAlignment = Element.ALIGN_LEFT;
            LeftCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            LeftCell.MinimumHeight = fixedHeight;
            LeftCell.UseAscender = useAscender;
            LeftCell.BackgroundColor = color;
            LeftCell.BorderColor = new BaseColor(204, 204, 204);
            LeftCell.Border = Rectangle.BOX;
            LeftCell.BorderWidthRight = 0.5f;
            LeftCell.PaddingLeft = 10F;
            LeftCell.PaddingTop = 10F;
            LeftCell.PaddingBottom = 10F;
            LeftCell.PaddingRight = 10F;
            LeftCell.Phrase = new Phrase(label, fontNormal);
            table.AddCell(LeftCell);

            RightCell.HorizontalAlignment = Element.ALIGN_LEFT;
            RightCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            RightCell.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
            RightCell.MinimumHeight = fixedHeight;
            RightCell.UseAscender = useAscender;
            RightCell.BackgroundColor = color;
            RightCell.BorderColor = new BaseColor(204, 204, 204);
            RightCell.Border = Rectangle.BOX;
            RightCell.BorderWidthLeft = 0.5f;
            RightCell.PaddingLeft = 10F;
            RightCell.PaddingTop = 10F;
            RightCell.PaddingBottom = 10F;
            RightCell.PaddingRight = 10F;
            RightCell.Phrase = new Phrase(value, fontNormal);
            table.AddCell(RightCell);
        }
        string GetOtherPersonName(long itemId, long RegisteredId)
        {
            string name = string.Empty;
            using (PPSlotRegistrationService PPSlotRegistrationService = new PPSlotRegistrationService())
            {
                XsiExhibitionPpslotInvitation where = new XsiExhibitionPpslotInvitation();
                where.SlotId = itemId;
                where.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Approved);
                XsiExhibitionPpslotInvitation entity = PPSlotRegistrationService.GetPPSlotRegistration(where).Where(p => p.RequestId == RegisteredId || p.ResponseId == RegisteredId).FirstOrDefault();
                if (entity != null)
                {
                    if (entity.RequestId != RegisteredId)
                        name = GetNameOtherUser_TableNoCurrentUser(entity.RequestId.Value, RegisteredId);
                    else if (entity.ResponseId != RegisteredId)
                        name = GetNameAndTableNo_OtherUser(entity.ResponseId.Value);
                }
            }
            return name;
        }
        string GetNameOtherUser_TableNoCurrentUser(long itemId, long RegisteredId)
        {
            string name = string.Empty;
            ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService();
            XsiExhibitionProfessionalProgramRegistration PPRegistrationEntityOtherUser = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(itemId);
            XsiExhibitionProfessionalProgramRegistration PPRegistrationEntityCurrentUser = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(RegisteredId);
            if (PPRegistrationEntityOtherUser != null)
                if (PPRegistrationEntityOtherUser.FirstName != null && PPRegistrationEntityOtherUser.LastName != null)
                    name = PPRegistrationEntityOtherUser.FirstName + " " + PPRegistrationEntityOtherUser.LastName;
                else
                {
                    if (PPRegistrationEntityOtherUser.FirstName != null)
                        name = PPRegistrationEntityOtherUser.FirstName;
                    else if (PPRegistrationEntityOtherUser.LastName != null)
                        name = PPRegistrationEntityOtherUser.LastName;
                }
            if (PPRegistrationEntityCurrentUser != null)
                if (PPRegistrationEntityCurrentUser.TableId != null)
                    name += " - T# " + PPRegistrationEntityCurrentUser.TableId.Value;
            if (PPRegistrationEntityOtherUser.CompanyName != null && PPRegistrationEntityOtherUser.CompanyName != string.Empty)
                name += "\n" + PPRegistrationEntityOtherUser.CompanyName;
            return name;
        }
        string GetNameAndTableNo_OtherUser(long itemId)
        {
            string name = string.Empty;
            ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService();
            XsiExhibitionProfessionalProgramRegistration PPRegistrationEntity = ProfessionalProgramRegistrationService.GetProfessionalProgramRegistrationByItemId(itemId);
            if (PPRegistrationEntity != null)
            {
                if (PPRegistrationEntity.FirstName != null && PPRegistrationEntity.LastName != null)
                    name = PPRegistrationEntity.FirstName + " " + PPRegistrationEntity.LastName;
                else
                {
                    if (PPRegistrationEntity.FirstName != null)
                        name = PPRegistrationEntity.FirstName;
                    else if (PPRegistrationEntity.LastName != null)
                        name = PPRegistrationEntity.LastName;
                }
                if (PPRegistrationEntity.TableId != null)
                    name += " - T# " + PPRegistrationEntity.TableId.Value;
                if (PPRegistrationEntity.CompanyName != null && PPRegistrationEntity.CompanyName != string.Empty)
                    name += "\n" + PPRegistrationEntity.CompanyName;
            }
            return name;
        }
        List<CustomScheduleDTO> GetPrintList(long RegisteredId, long ProfessionalProgramId)
        {
            string strApproved = EnumConversion.ToString(EnumProfessionalProgramStatus.Approved);
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                PPSlotRegistrationService PPSlotRegistrationService = new PPSlotRegistrationService();
                XsiExhibitionPpslotInvitation where = new XsiExhibitionPpslotInvitation();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                var SlotRegistrationList1 = PPSlotRegistrationService.GetPPSlotRegistration(where).ToList();
                var CustomScheduleList = new List<CustomScheduleDTO>();
                var SlotRegistrationList2 = SlotRegistrationList1.Where(i => i.RequestId == RegisteredId || i.ResponseId == RegisteredId).Select(s => new { s.ItemId, s.SlotId }).ToList();
                HashSet<long?> PPSlotRegistrationSlotID = new HashSet<long?>(SlotRegistrationList1.Where(i => (i.RequestId == RegisteredId || i.ResponseId == RegisteredId) && i.Status == strApproved).Select(s => s.SlotId).ToList());

                #region Get Main Slots List
                ProfessionalProgramSlotService ProfessionalProgramSlotService = new ProfessionalProgramSlotService();
                XsiExhibitionProfessionalProgramSlots where4 = new XsiExhibitionProfessionalProgramSlots();
                where4.IsDelete = EnumConversion.ToString(EnumBool.No);
                where4.ProfessionalProgramId = ProfessionalProgramId;
                List<XsiExhibitionProfessionalProgramSlots> PPSlotList = ProfessionalProgramSlotService.GetProfessionalProgramSlot(where4);
                #endregion
                foreach (var cols in PPSlotList)
                {
                    //    CustomScheduleList.Add(new CustomSchedule() { Slot = cols.Value, CellOne = cols.Value, CellTwo = cols.Value });
                    XsiExhibitionProfessionalProgramSlots entity = ProfessionalProgramSlotService.GetProfessionalProgramSlotByItemId(cols.ItemId);
                    /* if (entity.DayId == 1)
                         CustomScheduleList.Add(new CustomScheduleDTO() { DayOneItemId = cols.ItemId, DayTwoItemId = -1, SlotDayOne = entity.ItemId, StartTime = entity.StartTime.Value.ToString("HH:mm"), EndTime = entity.EndTime.Value.ToString("HH:mm") });
                     else if (entity.DayId == 2)
                         CustomScheduleList.Add(new CustomScheduleDTO() { DayOneItemId = -1, DayTwoItemId = cols.ItemId, SlotDayTwo = entity.ItemId, StartTime = entity.StartTime.Value.ToString("HH:mm"), EndTime = entity.EndTime.Value.ToString("HH:mm") });
 */
                    #region Slot List
                    foreach (XsiExhibitionProfessionalProgramSlots rows in PPSlotList)
                    {
                        if (rows.DayId != entity.DayId && rows.StartTime.Value.ToString("HH:mm") == entity.StartTime.Value.ToString("HH:mm"))
                        {
                            if (rows.DayId == 1)
                            {
                                foreach (CustomScheduleDTO row in CustomScheduleList)
                                {
                                    if (row.StartTime == rows.StartTime.Value.ToString("HH:mm"))
                                    {
                                        row.SlotDayOne = rows.ItemId;
                                        break;
                                    }
                                }
                            }
                            else if (rows.DayId == 2)
                            {
                                foreach (CustomScheduleDTO row in CustomScheduleList)
                                {
                                    if (row.StartTime == rows.StartTime.Value.ToString("HH:mm"))
                                    {
                                        row.SlotDayTwo = rows.ItemId;
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                    #endregion
                }
                var CustomScheduleList1 = new List<CustomScheduleDTO>();

                foreach (CustomScheduleDTO cols in CustomScheduleList)
                {
                    if (!CustomScheduleList1.Any(p => p.StartTime == cols.StartTime))
                        CustomScheduleList1.Add(CustomScheduleList.Where(p => p.StartTime == cols.StartTime).FirstOrDefault());
                }

                var PrintList = CustomScheduleList1.Where(p => PPSlotRegistrationSlotID.Contains(p.SlotDayOne) || PPSlotRegistrationSlotID.Contains(p.SlotDayTwo)).OrderBy(i => i.StartTime).ToList();
                return PrintList;
            }
        }
        #endregion

        List<CustomSchedule> BindCustomSchedule(long RegisteredId, long ProfessionalProgramId)
        {
            string strApproved = EnumConversion.ToString(EnumProfessionalProgramStatus.Approved);
            using (ProfessionalProgramRegistrationService ProfessionalProgramRegistrationService = new ProfessionalProgramRegistrationService())
            {
                ProfessionalProgramService ProfessionalProgramService = new ProfessionalProgramService();
                XsiExhibitionProfessionalProgram where1 = new XsiExhibitionProfessionalProgram();
                //where1.LanguageId = EnglishId;
                where1.ProfessionalProgramId = ProfessionalProgramId;
                XsiExhibitionProfessionalProgram ppentity = ProfessionalProgramService.GetProfessionalProgram(where1).FirstOrDefault();
                if (ppentity != null)
                {

                    string DayOne = "", DayTwo = "", DayThree = "";

                    //  ProfessionalProgramId = ppEntity.ProfessionalProgramId;
                    if (ppentity.StartDateDayOne != null)
                        DayOne = ppentity.StartDateDayOne.Value.ToString("dddd dd MMMM");
                    if (ppentity.StartDateDayTwo != null)
                        DayTwo = ppentity.StartDateDayTwo.Value.ToString("dddd dd MMMM");
                    if (ppentity.StartDateDayThree != null)
                        DayThree = ppentity.StartDateDayThree.Value.ToString("dddd dd MMMM");

                    var CategoryId = Convert.ToInt64(ppentity.ProfessionalProgramId);
                    if (ppentity.StartDateDayOne != null)
                        DayOne = ppentity.StartDateDayOne.Value.ToString("dddd dd MMMM");
                    if (ppentity.StartDateDayTwo != null)
                        DayTwo = ppentity.StartDateDayTwo.Value.ToString("dddd dd MMMM");
                    if (ppentity.StartDateDayThree != null)
                        DayThree = ppentity.StartDateDayThree.Value.ToString("dddd dd MMMM");
                }
                PPSlotRegistrationService PPSlotRegistrationService = new PPSlotRegistrationService();
                //XsiExhibitionPPSlotInvitation where = new XsiExhibitionPPSlotInvitation();
                //where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                var SlotRegistrationList1 = PPSlotRegistrationService.GetPPSlotRegistration(new XsiExhibitionPpslotInvitation() { IsActive = "Y" }).ToList();
                var CustomScheduleList = new List<CustomSchedule>();
                var SlotRegistrationList2 = SlotRegistrationList1.Where(i => i.RequestId == RegisteredId || i.ResponseId == RegisteredId).Select(s => new { s.ItemId, s.SlotId }).ToList();
                HashSet<long?> PPSlotRegistrationSlotID = new HashSet<long?>(SlotRegistrationList1.Where(i => (i.RequestId == RegisteredId || i.ResponseId == RegisteredId) && i.Status == strApproved).Select(s => s.SlotId).ToList());

                #region Get Main Slots List
                ProfessionalProgramSlotService ProfessionalProgramSlotService = new ProfessionalProgramSlotService();
                XsiExhibitionProfessionalProgramSlots where4 = new XsiExhibitionProfessionalProgramSlots();
                where4.IsDelete = EnumConversion.ToString(EnumBool.No);
                where4.ProfessionalProgramId = ProfessionalProgramId;
                List<XsiExhibitionProfessionalProgramSlots> PPSlotList = ProfessionalProgramSlotService.GetProfessionalProgramSlot(where4);
                #endregion
                foreach (var cols in PPSlotList)
                {
                    if (cols.DayId == 1)
                        CustomScheduleList.Add(new CustomSchedule() { DayOneItemId = cols.ItemId, DayTwoItemId = -1, DayThreeItemId = -1, SlotDayOne = cols.ItemId, StartTime = cols.StartTime.Value.ToString("HH:mm"), EndTime = cols.EndTime.Value.ToString("HH:mm") });
                    else if (cols.DayId == 2)
                        CustomScheduleList.Add(new CustomSchedule() { DayOneItemId = -1, DayTwoItemId = cols.ItemId, DayThreeItemId = -1, SlotDayTwo = cols.ItemId, StartTime = cols.StartTime.Value.ToString("HH:mm"), EndTime = cols.EndTime.Value.ToString("HH:mm") });
                    else if (cols.DayId == 3)
                        CustomScheduleList.Add(new CustomSchedule() { DayOneItemId = -1, DayTwoItemId = -1, DayThreeItemId = cols.ItemId, SlotDayThree = cols.ItemId, StartTime = cols.StartTime.Value.ToString("HH:mm"), EndTime = cols.EndTime.Value.ToString("HH:mm") });

                    #region Matching Records updating above list "CustomScheduleList"
                    var slotIds = PPSlotList.Where(x => x.DayId != cols.DayId && x.StartTime.Value.ToString("HH:mm") == cols.StartTime.Value.ToString("HH:mm")).ToList();
                    foreach (var item in slotIds)
                    {
                        if (item.DayId == 1)
                        {
                            foreach (CustomSchedule row in CustomScheduleList)
                            {
                                if (row.StartTime == item.StartTime.Value.ToString("HH:mm"))
                                {
                                    row.SlotDayOne = item.ItemId;
                                    break;
                                }
                            }
                        }
                        else if (item.DayId == 2)
                        {
                            foreach (CustomSchedule row in CustomScheduleList)
                            {
                                if (row.StartTime == item.StartTime.Value.ToString("HH:mm"))
                                {
                                    row.SlotDayTwo = item.ItemId;
                                    break;
                                }
                            }
                        }
                        else if (item.DayId == 3)
                        {
                            foreach (CustomSchedule row in CustomScheduleList)
                            {
                                if (row.StartTime == item.StartTime.Value.ToString("HH:mm"))
                                {
                                    row.SlotDayThree = item.ItemId;
                                    break;
                                }
                            }
                        }
                    }
                    #endregion
                }
                var CustomScheduleList1 = new List<CustomSchedule>();

                foreach (CustomSchedule cols in CustomScheduleList)
                {
                    if (!CustomScheduleList1.Any(p => p.StartTime == cols.StartTime))
                        CustomScheduleList1.Add(CustomScheduleList.Where(p => p.StartTime == cols.StartTime).FirstOrDefault());
                }
                return CustomScheduleList1.Where(p => PPSlotRegistrationSlotID.Contains(p.SlotDayOne) || PPSlotRegistrationSlotID.Contains(p.SlotDayTwo) || PPSlotRegistrationSlotID.Contains(p.SlotDayThree)).OrderBy(i => i.StartTime).ToList();
                //if (PrintList.Count > 0)
                //    btnPDF.Visible = true;
                //else
                //    btnPDF.Visible = false;

                //if (CustomScheduleList1.Count > 0)
                //{
                //    rptMySchedule.DataSource = CustomScheduleList1.OrderBy(i => i.StartTime);
                //    rptMySchedule.DataBind();

                //    pnlMsg.Visible = false;
                //    ulStatuses.Visible = true;
                //    dvPPCalendars.Visible = true;
                //}
                //else
                //{
                //    pnlMsg.Visible = true;
                //    ulStatuses.Visible = false;
                //    dvPPCalendars.Visible = false;
                //}
            }
        }
    }
    public class FindProfessionalSearch
    {
        public long MemberId { get; set; }
        public string Name { get; set; }
        public List<long?> GenreIds { get; set; }
        public List<long> CountryId { get; set; }
        public List<long> LanguageIds { get; set; }
        public bool IsSuggestions { get; set; } = true;
        public int? pageNumber { get; set; } = 1;
        public int? pageSize { get; set; } = 10;
    }
    public class CustomSchedule
    {
        public long DayOneItemId { get; set; }
        public long DayTwoItemId { get; set; }
        public long DayThreeItemId { get; set; }
        public long Slot { get; set; }
        public long SlotDayOne { get; set; }
        public long SlotDayTwo { get; set; }
        public long SlotDayThree { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionCountryController : ControllerBase
    {

        private readonly sibfnewdbContext _context;
        public ExhibitionCountryController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/ExhibitionCountry
        [HttpGet]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetXsiExhibitionCountry()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            var predicate = PredicateBuilder.True<XsiExhibitionCountry>();
            predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

            if (LangId == 1)
            {
                var List = await _context.XsiExhibitionCountry.AsNoTracking().AsQueryable().Where(predicate).OrderBy(x => x.CountryName).Select(x => new
                {
                    ItemId = x.CountryId,
                    Title = x.CountryName,
                    IsVisaRequired = x.IsVisaRequired == "Y" ? true : false,
                    CountryCode = x.CountryCode,
                    CountryCodeIsotwo = x.CountryCodeIsotwo
                }).ToListAsync();
                return Ok(List);
            }
            else
            {
                var List = await _context.XsiExhibitionCountry.AsNoTracking().AsQueryable().Where(predicate).OrderBy(x => x.CountryNameAr).Select(x => new
                {
                    ItemId = x.CountryId,
                    Title = x.CountryNameAr,
                    IsVisaRequired = x.IsVisaRequired == "Y" ? true : false,
                    CountryCode = x.CountryCode,
                    CountryCodeIsotwo = x.CountryCodeIsotwo
                }).ToListAsync();
                return Ok(List);
            }

            //using (ExhibitionCountryService ExhibitionCountryService = new ExhibitionCountryService())
            //{
            //    List<CategoryDTO> List = new List<CategoryDTO>();
            //    long LangId = 1;
            //    long.TryParse(Request.Headers["LanguageURL"], out LangId);
            //    if (LangId == 1)
            //        List = ExhibitionCountryService.GetExhibitionCountry().Select(i => new CategoryDTO() { ItemId = i.CountryId, Title = i.CountryName }).ToList();
            //    else
            //        List = ExhibitionCountryService.GetExhibitionCountry().Select(i => new CategoryDTO() { ItemId = i.CountryId, Title = i.CountryNameAr }).ToList();
            //    return Ok(List);
            //}
        }

        [HttpGet]
        [Route("visa")]
        public async Task<ActionResult<IEnumerable<ExhibitionCountryNewDTO>>> GetXsiExhibitionVisaCountry()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            var predicate = PredicateBuilder.True<XsiExhibitionCountry>();
            predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicate = predicate.And(i => i.IsVisaRequired == EnumConversion.ToString(EnumBool.Yes));

            if (LangId == 1)
            {
                var List = await _context.XsiExhibitionCountry.AsNoTracking().AsQueryable().Where(predicate).OrderBy(x => x.CountryName).Select(x => new ExhibitionCountryNewDTO()
                {
                    ItemId = x.CountryId,
                    Title = x.CountryName,
                    CountryCode = x.CountryCode,
                    CountryCodeIsotwo = x.CountryCodeIsotwo
                }).ToListAsync();
                return Ok(List);
            }
            else
            {
                var List = await _context.XsiExhibitionCountry.AsNoTracking().AsQueryable().Where(predicate).OrderBy(x => x.CountryNameAr).Select(x => new ExhibitionCountryNewDTO()
                {
                    ItemId = x.CountryId,
                    Title = x.CountryNameAr,
                    CountryCode = x.CountryCode,
                    CountryCodeIsotwo = x.CountryCodeIsotwo
                }).ToListAsync();
                return Ok(List);
            }

            //using (ExhibitionCountryService ExhibitionCountryService = new ExhibitionCountryService())
            //{
            //    List<CategoryDTO> List = new List<CategoryDTO>();
            //    long LangId = 1;
            //    long.TryParse(Request.Headers["LanguageURL"], out LangId);
            //    if (LangId == 1)
            //        List = ExhibitionCountryService.GetExhibitionCountry().Select(i => new CategoryDTO() { ItemId = i.CountryId, Title = i.CountryName }).ToList();
            //    else
            //        List = ExhibitionCountryService.GetExhibitionCountry().Select(i => new CategoryDTO() { ItemId = i.CountryId, Title = i.CountryNameAr }).ToList();
            //    return Ok(List);
            //}
        }
        [HttpGet]
        [Route("novisa")]
        public async Task<ActionResult<IEnumerable<ExhibitionCountryNewDTO>>> GetXsiExhibitionNoVisaCountry()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            var predicate = PredicateBuilder.True<XsiExhibitionCountry>();
            predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicate = predicate.And(i => i.IsVisaRequired == EnumConversion.ToString(EnumBool.No));

            if (LangId == 1)
            {
                var List = await _context.XsiExhibitionCountry.AsNoTracking().AsQueryable().Where(predicate).OrderBy(x => x.CountryName).Select(x => new ExhibitionCountryNewDTO()
                {
                    ItemId = x.CountryId,
                    Title = x.CountryName,
                    CountryCode = x.CountryCode,
                    CountryCodeIsotwo = x.CountryCodeIsotwo
                }).ToListAsync();
                return Ok(List);
            }
            else
            {
                var List = await _context.XsiExhibitionCountry.AsQueryable().Where(predicate).OrderBy(x => x.CountryNameAr).Select(x => new ExhibitionCountryNewDTO()
                {
                    ItemId = x.CountryId,
                    Title = x.CountryNameAr,
                    CountryCode = x.CountryCode,
                    CountryCodeIsotwo = x.CountryCodeIsotwo
                }).ToListAsync();
                return Ok(List);
            }

            //using (ExhibitionCountryService ExhibitionCountryService = new ExhibitionCountryService())
            //{
            //    List<CategoryDTO> List = new List<CategoryDTO>();
            //    long LangId = 1;
            //    long.TryParse(Request.Headers["LanguageURL"], out LangId);
            //    if (LangId == 1)
            //        List = ExhibitionCountryService.GetExhibitionCountry().Select(i => new CategoryDTO() { ItemId = i.CountryId, Title = i.CountryName }).ToList();
            //    else
            //        List = ExhibitionCountryService.GetExhibitionCountry().Select(i => new CategoryDTO() { ItemId = i.CountryId, Title = i.CountryNameAr }).ToList();
            //    return Ok(List);
            //}
        }
        [HttpGet]
        [Route("MultiSelect")]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetXsiExhibitionCountryMulti()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                var predicate = PredicateBuilder.True<XsiExhibitionCountry>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                if (LangId == 1)
                {
                    var List = await _context.XsiExhibitionCountry.AsNoTracking().AsQueryable().Where(predicate).OrderBy(x => x.CountryName).Select(x => new
                    {
                        value = x.CountryId,
                        label = x.CountryName,
                    }).ToListAsync();
                    return Ok(List);
                }
                else
                {
                    var List = await _context.XsiExhibitionCountry.AsNoTracking().AsQueryable().Where(predicate).OrderBy(x => x.CountryNameAr).Select(x => new
                    {
                        value = x.CountryId,
                        label = x.CountryNameAr,
                    }).ToListAsync();
                    return Ok(List);
                }
            }
        }

        // GET: api/ExhibitionCountry/IsVisaRequired/5
        [HttpGet]
        [Route("IsVisaRequired/{id}")]
        public async Task<ActionResult<CategoryDTO>> GetXsiExhibitionIsVisaRequiredCountry(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var xsiItem = await _context.XsiExhibitionCountry.AsNoTracking().Where(i => i.CountryId == id).FirstOrDefaultAsync();

                if (xsiItem == null)
                {
                    //return NotFound();
                    return Ok(new { Message = "No country found with this country id.", MessageTypeResponse = "Error", IsVisaRequired = false });
                }
                else
                {
                    if (xsiItem.IsVisaRequired == EnumConversion.ToString(EnumBool.Yes))
                    {
                        if (LangId == 1)
                            return Ok(new { Message = "Visa required for selected country. Try adding from Visa tab.", MessageTypeResponse = "Success", IsVisaRequired = true });
                        else

                            return Ok(new { Message = "البلد المحدد يتطلب تأشيرة", MessageTypeResponse = "Success", IsVisaRequired = true });
                    }
                    else
                    {
                        if (LangId == 1)
                            return Ok(new { Message = "Visa not required for the selected country. Please choose No-Visa tab and start the registration process.", MessageTypeResponse = "Success", IsVisaRequired = false });
                        else
                            return Ok(new { Message = "التأشيرة غير مطلوبة للبلد المحدد", MessageTypeResponse = "Success", IsVisaRequired = false });
                    }
                }
            }
        }

        // GET: api/ExhibitionCountry/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ExhibitionCountryNewDTO>> GetXsiExhibitionCountry(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var xsiItem = await _context.XsiExhibitionCountry.AsNoTracking().Where(i => i.CountryId == id).FirstOrDefaultAsync();

                if (xsiItem == null)
                {
                    return NotFound();
                }

                ExhibitionCountryNewDTO itemDTO = new ExhibitionCountryNewDTO()
                {
                    ItemId = xsiItem.CountryId,
                    Title = LangId == 1 ? xsiItem.CountryName : xsiItem.CountryNameAr,
                    CountryCode = xsiItem.CountryCode,
                    CountryCodeIsotwo = xsiItem.CountryCodeIsotwo
                };
                return itemDTO;
            }
        }

        private bool XsiExhibitionCountryExists(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiExhibitionCountry.Any(e => e.CountryId == id);
            }
        }
    }
}

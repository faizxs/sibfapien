﻿using Contracts;
using Entities.Models;
using GIGTools;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OfficeOpenXml;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Xsi.ServicesLayer;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ExhibitorRegistrationController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly IStringLocalizer<ExhibitorRegistrationController> _stringLocalizer;

        List<XsiExhibitionLanguage> BookLanguageList;
        List<XsiExhibitionBookSubject> BookSubjectList;
        List<XsiExhibitionBookSubsubject> BookSubsubjectList;

        #region Properties
        string strEmailContentBody { get; set; }
        string strEmailContentEmail { get; set; }
        string strEmailContentSubject { get; set; }
        string strEmailContentAdmin { get; set; }
        #endregion

        public ExhibitorRegistrationController(IEmailSender emailSender,
            IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger,
            IStringLocalizer<ExhibitorRegistrationController> stringLocalizer)
        {
            _emailSender = emailSender;
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _stringLocalizer = stringLocalizer;
        }
        #region Get Methods
        [HttpGet]
        [Route("Exhibitor/{websiteid}/{memberid}")]
        public ActionResult<dynamic> GetExhibitor(long websiteid, long memberid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                string Message = "";
                long memberId = User.Identity.GetID();
                if (memberId == memberid)
                {
                    ExhibitorRegistrationDTO model = new ExhibitorRegistrationDTO();
                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        var exhibition = MethodFactory.GetActiveExhibition(websiteid, _context);
                        if (exhibition != null)
                        {
                            model.ExhibitionId = exhibition.ExhibitionId;
                            if (_context.XsiExhibitionMemberApplicationYearly.Any(i => i.MemberRoleId == 1 && i.ParentId == null && i.MemberId == memberid && i.ExhibitionId == exhibition.ExhibitionId))
                            {
                                model = GetExhibitorView(memberId, websiteid, exhibition, LangId);
                                DateTime dtnow = MethodFactory.ArabianTimeNow().Date;
                                dtnow = new DateTime(dtnow.Year, dtnow.Month, dtnow.Day, 0, 0, 0);
                                if ((exhibition.ExhibitorStartDate <= dtnow && dtnow <= exhibition.ExhibitorEndDate) || MethodFactory.ExhibitorInRegistrationProcess(memberId, websiteid))
                                {
                                    if (MethodFactory.IsValidForRegistrationFromPreviousPayments(exhibition.ExhibitionId, memberId))
                                    {
                                        if (!MethodFactory.RejectedExhibitor(exhibition.ExhibitionId, memberId, 1) && !MethodFactory.CancelledExhibitor(exhibition.ExhibitionId, memberId, 1))
                                        {
                                            /*model = GetExhibitorView(memberId, websiteid, exhibition, LangId);*/
                                            if (model.ExhibitorId > 0)
                                                UpdateStatusChange(model.ExhibitorId);
                                        }
                                        else
                                        {
                                            model.IsNotesRead = "Y";
                                            model.StepId = 1;
                                            if (LangId == 1)
                                                model.Message = "We regret that we could not confirm your participation for this year. Better luck next time.";
                                            else
                                                model.Message = "نعتذر عن عدم قبول مشاركتكم لهذا العام على أمل أن نلقاكم في الأعوام القادمة.";
                                        }
                                    }
                                    else
                                    {
                                        model.IsNotesRead = "Y";
                                        model.StepId = 1;
                                        if (LangId == 1)
                                            model.Message = "Registration cannot be processed due to pending payments from previous year.";
                                        else
                                            model.Message = "لا يمكنكم استكمال عملية التسجيل نظراً لوجود مستحقات مالية معلقة من الدورة السابقة.";
                                    }
                                }
                                else
                                {
                                    model.StepId = 1;
                                    if (LangId == 1)
                                        model.Message = "Exhibitor Registration Date Expired";
                                    else
                                        model.Message = "Exhibitor Registration Date  Expired";
                                }
                            }
                            else if (_context.XsiExhibitionMemberApplicationYearly.Any(i => i.MemberRoleId == 2 && i.ParentId != null && i.MemberId == memberid && i.ExhibitionId == exhibition.ExhibitionId))
                            {
                                model.StepId = 1;
                                if (LangId == 1)
                                    model.Message = "User already registered for this exhibition as Agency.";
                                else
                                    model.Message = "User already registered for this exhibition as Agency.";
                                var dto1 = new { data = model != null ? model : null, message = Message };

                                return Ok(dto1);
                            }
                            else if (_context.XsiExhibitionMemberApplicationYearly.Any(i => i.MemberRoleId == 8 && i.ParentId != null && i.MemberId == memberid && i.ExhibitionId == exhibition.ExhibitionId))
                            {
                                model.StepId = 1;
                                if (LangId == 1)
                                    model.Message = "User already registered for this exhibition as Restaurant.";
                                else
                                    model.Message = "User already registered for this exhibition as Restaurant.";
                                var dto1 = new { data = model != null ? model : null, message = Message };

                                return Ok(dto1);
                            }
                            else
                            {
                                DateTime dtnow = MethodFactory.ArabianTimeNow().Date;
                                dtnow = new DateTime(dtnow.Year, dtnow.Month, dtnow.Day, 0, 0, 0);
                                if ((exhibition.ExhibitorStartDate <= dtnow && dtnow <= exhibition.ExhibitorEndDate) || MethodFactory.ExhibitorInRegistrationProcess(memberId, websiteid))
                                {
                                    if (MethodFactory.IsValidForRegistrationFromPreviousPayments(exhibition.ExhibitionId, memberId))
                                    {
                                        model = GetExhibitorView(memberId, websiteid, exhibition, LangId);
                                        var dto1 = new { data = model != null ? model : null, message = Message };
                                        return Ok(dto1);
                                    }
                                    else
                                    {
                                        model.StepId = 1;
                                        if (LangId == 1)
                                            model.Message = "Registration cannot be processed due to pending payments from previous year.";
                                        else
                                            model.Message = "لا يمكنكم استكمال عملية التسجيل نظراً لوجود مستحقات مالية معلقة من الدورة السابقة.";
                                    }
                                }
                                else
                                {
                                    model.StepId = 1;
                                    if (LangId == 1)
                                        model.Message = "Exhibitor Registration Date Expired";
                                    else
                                        model.Message = "Exhibitor Registration Date Expired";
                                }
                            }
                        }
                        else
                        {
                            model.StepId = 1;
                            if (LangId == 1)
                                model.Message = "No exhibition available";
                            else
                                model.Message = "No exhibition available";
                        }
                    }
                    var dto = new { data = model != null ? model : null, message = Message };

                    return Ok(dto);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception1: Something went wrong. Please retry again later." + ex.InnerException);
            }
        }
        //[HttpGet]
        //[Route("StartRegistration/{websiteid}/{memberid}")]
        //public ActionResult<dynamic> GetStartRegistration(long websiteid, long memberid)
        //{
        //    long memberId = User.Identity.GetID();
        //    if (memberId == memberid)
        //    {
        //        // CreateInterface(EnumRegistrationViewType.Step2);
        //        var loadExibitor = LoadStepTwoDTOByMemberId(websiteid, memberId);
        //        var dto = new { loadexibitor = loadExibitor, stepid = 2 };
        //        return Ok(dto);
        //    }
        //    return Unauthorized();
        //}

        [HttpGet]
        [Route("StepTwoEdit")]
        public ActionResult<dynamic> GetStepTwoEdit()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    ExhibitorReturnDTO model = new ExhibitorReturnDTO();
                    model.StepId = 2;
                    if (LangId == 1)
                        model.Message = "EnableStep2Controls";
                    else
                        model.Message = "EnableStep2Controls";
                    return Ok(model);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception2: Something went wrong. Please retry again later." + ex.InnerException);
            }
        }
        [HttpGet]
        [Route("StepThreeNext/{exhibitorid}")]
        public ActionResult<dynamic> GetStepThreeNext(long exhibitorid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    ExhibitorReturnDTO model = new ExhibitorReturnDTO();
                    model.StepId = 4;
                    if (LangId == 1)
                        model.Message = "DisableStep3Controls";
                    else
                        model.Message = "DisableStep3Controls";
                    return Ok(model);
                    //CreateInterface(EnumRegistrationViewType.Step4);
                    //DisableStep3Controls();
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception3: Something went wrong. Please retry again later." + ex.InnerException);
            }
        }

        [HttpGet]
        [Route("StepThreeEdit/{exhibitorid}")]
        public ActionResult<dynamic> GetStepThreeEdit(long exhibitorid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    var currentExhibitor = GetCurrentExhibitorDetails(exhibitorid);
                    var dto = new { currentexhibitor = currentExhibitor, stepid = 3, message = "ShowStep3Submit" };
                    return Ok(dto);
                    //EnableStep3Controls();
                    // ShowStep3Submit();
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception4: Something went wrong. Please retry again later." + ex.InnerException);
            }
        }
        [HttpGet]
        [Route("StepThreeBack")]
        public ActionResult<dynamic> GetStepThreeBack()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    ExhibitorReturnDTO model = new ExhibitorReturnDTO();
                    model.StepId = 2;
                    if (LangId == 1)
                        model.Message = "DisableStep2Controls";
                    else
                        model.Message = "DisableStep2Controls";
                    return Ok(model);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception5: Something went wrong. Please retry again later." + ex.InnerException);
            }
        }
        [HttpGet]
        [Route("StepThreeThankyouBack/{exhibitorid}")]
        public ActionResult<dynamic> GetStepThreeThankyouBack(long exhibitorid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    //dvRequiredAreaContainer.Visible = true;
                    // CreateInterface(EnumRegistrationViewType.Step3);
                    //BindDropDowns();
                    bool ShowStep3UnderProcess = false;
                    bool EnableStep3Submit = false;
                    var step3Dta = LoadStepThreeContent(exhibitorid, LangId);
                    var currentExhibitor = GetCurrentExhibitorDetails(exhibitorid);

                    if (step3Dta.CurrentEditRequest == EnumConversion.ToString(EnumExhibitorEditStatus.New))
                        ShowStep3UnderProcess = true;
                    else
                        EnableStep3Submit = true;
                    var dto = new { step3data = step3Dta, currentexhibitor = currentExhibitor, showstep3underprocess = ShowStep3UnderProcess, enablestep3submit = EnableStep3Submit };
                    return Ok(dto);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception6: Something went wrong. Please retry again later." + ex.InnerException);
            }

        }

        [HttpGet]
        [Route("StepFourBack/{exhibitorid}")]
        public ActionResult<dynamic> GetStepFourBack(long exhibitorid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    bool ShowStep3UnderProcess = false;
                    bool EnableStep3Submit = false;
                    var step3Dta = LoadStepThreeContent(exhibitorid, LangId);
                    var currentExhibitor = GetCurrentExhibitorDetails(exhibitorid);

                    if (step3Dta.CurrentEditRequest == EnumConversion.ToString(EnumExhibitorEditStatus.New))
                        ShowStep3UnderProcess = true;
                    else
                        EnableStep3Submit = true;
                    var dto = new { step3data = step3Dta, currentexhibitor = currentExhibitor, showstep3underprocess = ShowStep3UnderProcess, enablestep3submit = EnableStep3Submit, message = "DisableStep3Controls" };
                    return Ok(dto);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception7: Something went wrong. Please retry again later." + ex.InnerException);
            }
        }
        [HttpGet]
        [Route("StepFiveThankyouBack/{exhibitorid}")]
        public ActionResult<dynamic> GetStepFiveThankyouBack(long exhibitorid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    return Ok("Step5");
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception8: Something went wrong. Please retry again later." + ex.InnerException);
            }
            // CreateInterface(EnumRegistrationViewType.Step5);
        }
        [HttpGet]
        [Route("StepFiveBack/{exhibitorid}")]
        public ActionResult<dynamic> GetStepFiveBack(long exhibitorid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    //CreateInterface(EnumRegistrationViewType.Step4);
                    ExhibitorReturnDTO model = new ExhibitorReturnDTO();
                    model.StepId = 4;
                    if (LangId == 1)
                        model.Message = "Step4";
                    else
                        model.Message = "Step4";
                    return Ok(model);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception9: Something went wrong. Please retry again later." + ex.InnerException);
            }

        }

        [HttpGet]
        [Route("NotesRead/{exhibitorid}")]
        public ActionResult<dynamic> GetNotesRead(long exhibitorid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
                    {
                        var exhibitor = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorid);
                        if (exhibitor != null)
                        {
                            exhibitor.IsNotesRead = EnumConversion.ToString(EnumBool.Yes);
                            ExhibitorRegistrationService.UpdateExhibitorRegistration(exhibitor);
                            return Ok(true);
                        }
                    }
                    // var dto = new { step3data = step3Dta, currentexhibitor = currentExhibitor, showstep3underprocess = ShowStep3UnderProcess, enablestep3submit = EnableStep3Submit, message = "DisableStep3Controls" };
                    return Ok(false);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok(false);
                //return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        #endregion
        #region Post Method
        [HttpPost]
        [Route("StepThree")]
        public ActionResult<dynamic> PostStepThree(SaveDataForStepTwoAndThreeDTO model)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId == model.MemberId)
                {
                    ExhibitorDTO returnModel = new ExhibitorDTO();
                    bool validate = true;
                    if (!string.IsNullOrEmpty(model.TotalNumberOfNewTitles) && !string.IsNullOrEmpty(model.TotalNumberOfTitles))
                    {
                        long NewTitles = Convert.ToInt64(model.TotalNumberOfNewTitles.Trim());
                        long TotalTitles = Convert.ToInt64(model.TotalNumberOfTitles.Trim());
                        if (NewTitles > TotalTitles)
                        {
                            var message = string.Empty;
                            if (LangId == 1)
                                message = "Total number of new titles should be less than total number of titles";//_stringLocalizer["NewTitleShouldBeLessThanTotalOfTitles"];
                            else
                                message = "يجب ان يكون عدد العناوين الجديدة اقل من العدد الاجمالي للعناوين";//"عدد العناوين الجديدة يجب أن يكون أقل من عدد العناوين الكلية";
                            return Ok(ShowMessage(message, 3, model.ExhibitorId, model.MemberId, model.WebsiteId, "Error"));
                        }
                    }

                    var prevbalance = MethodFactory.GetCustomerBalanceFromPreviousPayments(memberId);

                    long langId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out langId);

                    if (model.ExhibitorId <= 0)
                    {
                        if (isduplicate(model.ExhibitionId, model.MemberId))
                        {
                            var message = string.Empty;
                            if (LangId == 1)
                                message = @"Too many requests submitted. Please reload page and retry again after some time.";
                            else
                                message = @"Too many requests submitted. Please reload page and retry again after some time.";

                            return Ok(ShowMessage(message, 3, model.ExhibitorId, model.MemberId, model.WebsiteId, "Error"));
                        }
                        if (model.ActivityIds.Count <= 0)
                        {
                            var message = string.Empty;
                            if (LangId == 1)
                                message = @"Category is mandatory. ActivityIDs missing.";
                            else
                                message = @"Category is mandatory. ActivityIDs missing.";

                            return Ok(ShowMessage(message, 3, model.ExhibitorId, model.MemberId, model.WebsiteId, "Error"));
                        }
                        if (MethodFactory.IsValidLogo_TradeLicense(model.LogoExt))
                        {
                            var message = string.Empty;
                            if (LangId == 1)
                                message = @"Invalid Logo";
                            else
                                message = @"Invalid Logo";

                            return Ok(ShowMessage(message, 3, model.ExhibitorId, model.MemberId, model.WebsiteId, "Error"));
                        }
                        if (MethodFactory.IsValidLogo_TradeLicense(model.TradeLicenceExt))
                        {
                            var message = string.Empty;
                            if (LangId == 1)
                                message = @"Invalid Trade License";
                            else
                                message = @"Invalid Trade License";

                            return Ok(ShowMessage(message, 3, model.ExhibitorId, model.MemberId, model.WebsiteId, "Error"));
                        }

                        if (IsBooksRequired(memberId) && string.IsNullOrEmpty(model.excelFileBase64))
                        {
                            var message = string.Empty;
                            if (LangId == 1)
                                message = @"Upload Books field is missing";
                            else
                                message = @"Upload Books field is missing";

                            return Ok(ShowMessage(message, 3, model.ExhibitorId, model.MemberId, model.WebsiteId, "Error"));
                        }
                        model.ExhibitorId = SaveExhibitorRegistration(model, langId);
                        model.MemberId = memberId;
                        model.WebsiteId = model.WebsiteId;
                        model.BookMessageResponse = ImportBooks(model.ExhibitorId, memberId, model, langId);

                        if (model.ExhibitorId > 0)
                        {
                            var message = string.Empty;
                            var duemessage = string.Empty;
                            if (LangId == 1)
                            {
                                if (prevbalance > 100)
                                    duemessage = @"<p>Please note that there are pending financial dues. Accordingly, we will not consider the participation request until the payment is completed.</p>";

                                message = @"Your Exhibition Registration has been submitted. We'll get back to you soon.";
                                return Ok(ShowMessage(message, 3, model.ExhibitorId, model.MemberId, model.WebsiteId, duemessage));
                            }
                            else
                            {
                                if (model.WebsiteId == 1)
                                {
                                    if (prevbalance > 100)
                                        duemessage = @"<p>نود التنويه بأن هناك مستحقات مالية لم يتم دفعها، وبناءً على ذلك، لن نقوم بالنظر في طلب المشاركة حتى يتم استكمال الدفع</p>";

                                    message = @"قد تم إستلام طلبك للتسجيل في المعرض. وسوف يتم الرد عليكم قريباً";
                                }
                                else
                                    message = @"قد تم إستلام طلبك للتسجيل في المهرجان. وسوف يتم الرد عليكم قريباً";
                            }

                            return Ok(ShowMessage(message, 3, model.ExhibitorId, model.MemberId, model.WebsiteId, duemessage));
                        }
                        else
                        {
                            var message = string.Empty;
                            if (LangId == 1)
                                message = @"Something went wrong. Please retry again after some time. model.ExhibitorId: " + model.ExhibitorId;
                            else
                                message = @"Something went wrong. Please retry again after some time. model.ExhibitorId: " + model.ExhibitorId;

                            return Ok(ShowMessage(message, 3, model.ExhibitorId, model.MemberId, model.WebsiteId, "Error"));
                        }
                    }
                    else
                    {
                        var CurrentExhibitorEntity = GetCurrentExhibitorDetails(model.ExhibitorId);
                        if (CurrentExhibitorEntity != null)
                        {
                            if (CurrentExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.New))
                            {
                                UpdateExhibitorRegistration(string.Empty, model);
                                if (CurrentExhibitorEntity.Email != model.Email)
                                    NotifyAdminandUserAboutEmailChange(CurrentExhibitorEntity.MemberExhibitionYearlyId, CurrentExhibitorEntity.Email, model.Email, model.MemberId, model.WebsiteId);
                            }
                            else
                            {
                                if (IsSpaceDependentFieldChanged(CurrentExhibitorEntity, model))
                                {
                                    //if (CurrentExhibitorEntity.RequiredAreaShapeType != ddlAreaShape.SelectedValue || CurrentExhibitorEntity.RequiredAreaId.ToString() != ddlRequiredArea.SelectedValue)
                                    //    UpdateRegistrationInvoice ();
                                    UpdateExhibitorRegistration(EnumConversion.ToString(EnumExhibitorEditStatus.New), model);
                                    //EnableEditReqMsgOnStep3thankyou();
                                }
                                else
                                    UpdateExhibitorRegistration(string.Empty, model);
                                if (IsAnyFieldChanged(CurrentExhibitorEntity, model))
                                    SendEmailBasedOnEdit(CurrentExhibitorEntity, model.MemberId, model.WebsiteId, model.ExhibitorId, langId);
                            }
                        }
                        var message = string.Empty;
                        var duemessage = string.Empty;
                        if (LangId == 1)
                        {
                            if (prevbalance > 100)
                                duemessage = @"<p>Please note that there are pending financial dues. Accordingly, we will not consider the participation request until the payment is completed.</p>";

                            message = @"Your Exhibition Registration has been submitted. We'll get back to you soon.";
                            return Ok(ShowMessage(message, 3, model.ExhibitorId, model.MemberId, model.WebsiteId, duemessage));
                        }
                        else
                        {
                            if (model.WebsiteId == 1)
                            {
                                if (prevbalance > 100)
                                    duemessage = @"<p>نود التنويه بأن هناك مستحقات مالية لم يتم دفعها، وبناءً على ذلك، لن نقوم بالنظر في طلب المشاركة حتى يتم استكمال الدفع</p>";

                                message = @"قد تم إستلام طلبك للتسجيل في المعرض. وسوف يتم الرد عليكم قريباً";
                            }
                            else
                                message = @"قد تم إستلام طلبك للتسجيل في المهرجان. وسوف يتم الرد عليكم قريباً";
                            return Ok(ShowMessage(message, 3, model.ExhibitorId, model.MemberId, model.WebsiteId, duemessage));
                        }
                    }
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Exhibitor Registration StepThree action: {ex.InnerException}");
                // return Ok(ex.StackTrace);
                return Ok("Exception10: Something went wrong. Please retry again later." + ex.ToString());
            }
        }

        private bool isduplicate(long exhibitionId, long memberId)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                var list = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly() { ExhibitionId = exhibitionId, MemberId = memberId }).ToList();
                if (list.Count > 0)
                    return true;
            }
            return false;
        }

        //[HttpPost]
        //[Route("ExhibitingDetailsStepTwo")]
        //public ActionResult<dynamic> PostExhibitingDetailsStepTwo(StepTwoDTO model, long memberID)
        //{
        //    long memberId = User.Identity.GetID();
        //    if (memberId == memberID)
        //    {
        //        long langId = 1;
        //        long.TryParse(Request.Headers["LanguageURL"], out langId);
        //        if (IsTradeLicenceValid(model, langId))
        //        {
        //            if (IsLogoValid(model, langId))
        //                MoveToStep3(model, langId);
        //        }
        //    }
        //    return Unauthorized();
        //}

        [HttpPost]
        [Route("StepFour")]
        public ActionResult<dynamic> PostStepFour()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    ExhibitorReturnDTO model = new ExhibitorReturnDTO();
                    model.StepId = 5;
                    if (LangId == 1)
                        model.Message = "HideError";
                    else
                        model.Message = "HideError";
                    return Ok(model);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception11: Something went wrong. Please retry again later." + ex.InnerException);
            }

            //HideError(lblMessageStep5, pnlErrorMsgStep5);
            //CreateInterface(EnumRegistrationViewType.Step5);

        }
        [HttpPost]
        [Route("StepFive")]
        public ActionResult<dynamic> PostStepFive(UploadPayment model)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);
                long memberId = User.Identity.GetID();
                if (memberId == model.MemberId)
                {
                    if (!string.IsNullOrWhiteSpace(model.TRNNumber))
                    {
                        using (ExhibitorRegistrationService exhibitorRegistrationService = new ExhibitorRegistrationService())
                        {
                            var exhibitor = new XsiExhibitionMemberApplicationYearly();
                            exhibitor = exhibitorRegistrationService.GetExhibitorRegistrationByItemId(model.ExhibitorId);
                            exhibitor.Trn = model.TRNNumber;
                            exhibitor.ModifiedOn = MethodFactory.ArabianTimeNow();
                            exhibitorRegistrationService.UpdateExhibitorRegistration(exhibitor);
                        }
                    }
                    if (model.FileBase64 != null && model.FileBase64.Length > 0)
                    {
                        var message = UploadReceipt(model, langId);// TODO
                                                                   //else
                                                                   //    ShowMessage(Resources.BackendError.InvalidFileFormat, EnumMessageType.Error, lblMessageStep5,
                                                                   //        pnlErrorMsgStep5, liMessage5);
                        if (message == "fail" || message == "Upload receipt missing")
                        {
                            if (langId == 1)
                                return Ok(ShowMessage(message, 5, model.ExhibitorId, model.MemberId, model.WebsiteId, "Error"));
                            else
                                return Ok(ShowMessage(message, 5, model.ExhibitorId, model.MemberId, model.WebsiteId, "Error"));
                        }
                        else
                        {
                            if (langId == 1)
                                return Ok(ShowMessage(message, 5, model.ExhibitorId, model.MemberId, model.WebsiteId, "Success"));
                            else
                                return Ok(ShowMessage(message, 5, model.ExhibitorId, model.MemberId, model.WebsiteId, "Success"));
                        }
                    }
                    else
                    {
                        if (langId == 1)
                            return Ok(ShowMessage("UploadFieldMissing", 5, model.ExhibitorId, model.MemberId, model.WebsiteId, "Error"));
                        else
                            return Ok(ShowMessage("تحميل الحقول المفقودة، الرجاء تحميل الملف", 5, model.ExhibitorId, model.MemberId, model.WebsiteId, "Error"));
                    }
                }
                return Unauthorized();
                //ShowMessage(Resources.BackendError.UploadFieldMissing, EnumMessageType.Error, lblMessageStep5, pnlErrorMsgStep5, liMessage5);
            }
            catch (Exception ex)
            {
                return Ok("Exception12: Something went wrong. Please retry again later." + ex.InnerException);
            }
        }

        [HttpPost]
        [Route("Proceed")]
        public ActionResult<dynamic> PostProceed(CancelRegistrationDTO dto)
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);

            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long memberId = User.Identity.GetID();
                var exhibition = MethodFactory.GetActiveExhibition(dto.WebsiteId, _context);
                if (exhibition == null)
                {
                    if (langId == 1)
                        return Ok(ShowMessage(_stringLocalizer["DateExpired"], 3, dto.ExhibitorId, memberId, dto.WebsiteId, "Error"));
                    else
                        return Ok(ShowMessage(_stringLocalizer["DateExpired"], 3, dto.ExhibitorId, memberId, dto.WebsiteId, "Error"));
                }

                if (MethodFactory.ArabianTimeNow() > exhibition.DateOfDue)
                {
                    if (langId == 1)
                        return Ok(ShowMessage(_stringLocalizer["DateExpired"], 3, dto.ExhibitorId, memberId, dto.WebsiteId, "Error"));
                    else
                        return Ok(ShowMessage(_stringLocalizer["DateExpired"], 3, dto.ExhibitorId, memberId, dto.WebsiteId, "Error"));
                }

                using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
                {
                    XsiExhibitionMemberApplicationYearly exhibitorentity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(dto.ExhibitorId);
                    if (exhibitorentity != null)
                    {
                        exhibitorentity.Status = EnumConversion.ToString(EnumExhibitorStatus.Cancelled);
                        exhibitorentity.ModifiedOn = MethodFactory.ArabianTimeNow();
                        if (ExhibitorRegistrationService.UpdateExhibitorRegistration(exhibitorentity) == EnumResultType.Success)
                        {
                            var details = ExhibitorRegistrationService.GetDetailsById(dto.ExhibitorId);
                            if (details != null && dto.CancellationReason != null)
                            {
                                details.CancellationReason = dto.CancellationReason.Trim();
                                details.ModifiedOn = MethodFactory.ArabianTimeNow();
                                ExhibitorRegistrationService.UpdateExhibitorRegistrationDetails(details);
                            }
                            //send email to user notifying abt the cancellation
                            SendEmail(exhibitorentity, memberId, dto.WebsiteId, langId);

                            LogExhibitorStatus(EnumConversion.ToString(EnumExhibitorStatus.Cancelled), exhibitorentity.MemberExhibitionYearlyId);
                            MethodFactory.InactivateBooksParticipating(exhibitorentity.MemberExhibitionYearlyId);
                            // ExhibitorId = -1;
                            //redirect to dashboard
                            //string pageReferrer = "/en/MemberDashboard";
                            //Response.Redirect(pageReferrer);

                            return Ok(ShowMessage("true", 2, exhibitorentity.MemberExhibitionYearlyId, memberId, dto.WebsiteId, "Success"));
                        }
                    }
                    if (langId == 1)
                        return Ok(ShowMessage("Cancellation Failure, Retry again or contact admin.", 2, exhibitorentity.MemberExhibitionYearlyId, memberId, dto.WebsiteId, "Error"));
                    else
                        return Ok(ShowMessage("Cancellation Failure, Retry again or contact admin.", 2, exhibitorentity.MemberExhibitionYearlyId, memberId, dto.WebsiteId, "Error"));
                }
            }
        }

        #endregion

        #region Insert
        private long SaveExhibitorRegistration(SaveDataForStepTwoAndThreeDTO model, long langId)
        {
            long exhibitorid = 0;
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly entity = new XsiExhibitionMemberApplicationYearly();
                ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService();
                XsiExhibitionMember entityExhibitionMember = ExhibitionMemberService.GetExhibitionMemberByItemId(model.MemberId);
                if (entityExhibitionMember != null)
                    if (entityExhibitionMember.FileNumber != null && !string.IsNullOrEmpty(entityExhibitionMember.FileNumber) && entityExhibitionMember.FileNumber.ToLower() != "new")
                        entity.FileNumber = entityExhibitionMember.FileNumber;
                entity.ExhibitionId = model.ExhibitionId;
                entity.MemberRoleId = 1;
                entity.LanguageUrl = langId;
                entity.MemberId = model.MemberId;
                entity.CountryId = model.CountryId;
                entity.CityId = model.CityId;
                entity.OtherCity = model.OtherCity;
                entity.ExhibitionCategoryId = model.ExhibitionCategoryId;
                entity.PublisherNameEn = model.PublisherNameEn;
                entity.PublisherNameAr = model.PublisherNameAr;

                entity.ContactPersonName = model.ContactPersonName;
                entity.ContactPersonNameAr = model.ContactPersonNameAr;

                entity.ContactPersonTitle = model.ContactPersonTitle;
                entity.ContactPersonTitleAr = model.ContactPersonTitleAr;
                entity.IsFirstTime = EnumConversion.ToString(EnumBool.No);

                if (!string.IsNullOrEmpty(model.PhoneISD) && !string.IsNullOrEmpty(model.PhoneSTD) && !string.IsNullOrEmpty(model.Phone))
                    entity.Phone = model.PhoneISD + "$" + model.PhoneSTD + "$" + model.Phone;
                if (!string.IsNullOrEmpty(model.MobileISD) && !string.IsNullOrEmpty(model.MobileSTD) && !string.IsNullOrEmpty(model.Mobile))
                    entity.Mobile = model.MobileISD + "$" + model.MobileSTD + "$" + model.Mobile;
                if (!string.IsNullOrEmpty(model.FaxISD) && !string.IsNullOrEmpty(model.FaxSTD) && !string.IsNullOrEmpty(model.Fax))
                    entity.Fax = model.FaxISD + "$" + model.FaxSTD + "$" + model.Fax;

                entity.Email = model.Email;
                entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                entity.IsStatusChanged = EnumConversion.ToString(EnumBool.No);
                entity.CreatedOn = MethodFactory.ArabianTimeNow();
                entity.CreatedBy = model.MemberId;
                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                entity.ModifiedBy = model.MemberId;
                if (!string.IsNullOrEmpty(model.TotalNumberOfTitles))
                    entity.TotalNumberOfTitles = model.TotalNumberOfTitles.Trim();
                if (!string.IsNullOrEmpty(model.TotalNumberOfNewTitles))
                    entity.TotalNumberOfNewTitles = model.TotalNumberOfNewTitles.Trim();
                // entity.FileName = model.FileName;

                if (entity.FileNumber == null || string.IsNullOrEmpty(entity.FileNumber))
                    entity.Status = EnumConversion.ToString(EnumExhibitorStatus.New);
                else
                {
                    var ispreapproved = MethodFactory.IsPreApproved(entity.PublisherNameEn, entity.PublisherNameAr, entity.FileNumber);

                    if (ispreapproved)
                        entity.Status = EnumConversion.ToString(EnumExhibitorStatus.PreApproved);
                    else
                        entity.Status = EnumConversion.ToString(EnumExhibitorStatus.New);
                }
                entity.IsNotesRead = EnumConversion.ToString(EnumBool.Yes);

                //if (!string.IsNullOrEmpty(model.IsBooksUpload))
                //{
                //    entity.IsBooksUpload = model.IsBooksUpload == "Y" ? "Y" : "N";
                //}

                var prevbalance = MethodFactory.GetCustomerBalanceFromPreviousPayments(model.MemberId);
                if (prevbalance > 100)
                    entity.IsAllowApprove = "N";
                else
                    entity.IsAllowApprove = "Y";

                if (ExhibitorRegistrationService.InsertExhibitorRegistration(entity) == EnumResultType.Success)
                {
                    exhibitorid = ExhibitorRegistrationService.XsiItemdId;

                    AddExhibitorDetails(model, exhibitorid, langId);
                    LogExhibitorStatus(EnumConversion.ToString(EnumExhibitorStatus.New), exhibitorid);

                    AddExhibitorActivities(model.ActivityIds, exhibitorid);

                    if (entityExhibitionMember.Email != entity.Email)
                        NotifyAdminandUserAboutEmailChange(entity.MemberExhibitionYearlyId, entityExhibitionMember.Email, entity.Email, model.MemberId, model.WebsiteId);
                    SendEmail(entity, model.MemberId, model.WebsiteId, langId);
                }
            }
            return exhibitorid;
        }
        void AddExhibitorDetails(SaveDataForStepTwoAndThreeDTO model, long exhibitorid, long langId)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                #region Exhibitor Details
                XsiExhibitionExhibitorDetails details = new XsiExhibitionExhibitorDetails();
                details.MemberExhibitionYearlyId = exhibitorid;
                details.BoothSectionId = model.BoothSectionId;
                details.BoothSubSectionId = model.BoothSubSectionId;
                details.Apu = model.APU;
                details.RequiredAreaType = model.RequiredAreaShapeType;
                details.BoothDetail = model.BoothDetail;
                if (model.RequiredAreaId > 0)
                {
                    details.RequiredAreaId = model.RequiredAreaId;
                    if (model.RequiredAreaShapeType == EnumConversion.ToString(EnumRequiredAreaType.Open))
                    {
                        long CalculatedArea = 0;
                        if (model.RequiredArea == "3")
                        {
                            CalculatedArea = Convert.ToInt64(model.RequiredAreaValue) / Convert.ToInt64(model.RequiredAreaMeter);
                            details.CalculatedArea = CalculatedArea.ToString() + "x" + model.RequiredAreaMeter;
                        }
                        if (model.RequiredArea == "6")
                        {
                            CalculatedArea = Convert.ToInt64(model.RequiredAreaValue) / Convert.ToInt64(model.RequiredAreaMeter);
                            details.CalculatedArea = CalculatedArea.ToString() + "x" + model.RequiredAreaMeter;
                        }
                    }
                }
                if (model.LogoBase64 != null && model.LogoBase64.Length > 0)
                {
                    string fileName = string.Empty;
                    byte[] imageBytes;
                    if (model.LogoBase64.Contains("data:"))
                    {
                        var strInfo = model.LogoBase64.Split(",")[0];
                        imageBytes = Convert.FromBase64String(model.LogoBase64.Split(',')[1]);
                    }
                    else
                    {
                        imageBytes = Convert.FromBase64String(model.LogoBase64);
                    }

                    fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + model.LogoExt);
                    if (MethodFactory.IsValidLogo_TradeLicense("." + model.LogoExt))
                    {
                        System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\Logo\\") + fileName, imageBytes);
                        details.FileName = fileName;
                    }
                }
                if (model.TradeLicenceBase64 != null && model.TradeLicenceBase64.Length > 0)
                {
                    string fileName = string.Empty;
                    byte[] imageBytes;
                    if (model.TradeLicenceBase64.Contains("data:"))
                    {
                        var strInfo = model.TradeLicenceBase64.Split(",")[0];
                        imageBytes = Convert.FromBase64String(model.TradeLicenceBase64.Split(',')[1]);
                    }
                    else
                    {
                        imageBytes = Convert.FromBase64String(model.TradeLicenceBase64);
                    }

                    fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + model.TradeLicenceExt);
                    if (MethodFactory.IsValidLogo_TradeLicense("." + model.TradeLicenceExt))
                    {
                        System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\TradeLicence\\") + fileName, imageBytes);
                        details.TradeLicence = fileName;
                    }
                }
                if (model.Brief != null)
                    details.Brief = model.Brief.Trim();

                if (model.BriefAr != null)
                    details.BriefAr = model.BriefAr.Trim();

                details.CreatedOn = MethodFactory.ArabianTimeNow();
                details.CreatedBy = model.MemberId;
                details.ModifiedOn = MethodFactory.ArabianTimeNow();
                details.ModifiedBy = model.MemberId;

                if (ExhibitorRegistrationService.InsertExhibitorDetailsRegistration(details) == EnumResultType.Success)
                {
                    //var exhibitordetailsid = ExhibitorRegistrationService.XsiItemdId;
                    // UpdateExhibitionMemberCompanyName(entity);
                }
                #endregion
            }

        }
        void AddExhibitorActivities(List<long> ActivityIds, long exhibitorid)
        {
            if (ActivityIds.Count > 0)
            {
                using (ExhibitorActivityService ExhibitorActivityService = new ExhibitorActivityService())
                {
                    XsiMemberExhibitionActivityYearly entity1;
                    foreach (var item in ActivityIds)
                    {
                        entity1 = new XsiMemberExhibitionActivityYearly();
                        entity1.MemberExhibitionYearlyId = exhibitorid;
                        entity1.ActivityId = Convert.ToInt64(item);
                        ExhibitorActivityService.InsertExhibitorActivity(entity1);
                    }
                }
            }
        }
        #endregion

        #region Import Books Methods
        string ImportBooks(long exhibitorId, long memberid, SaveDataForStepTwoAndThreeDTO dataobj, long LangId)
        {
            if (dataobj.excelFileBase64 != null && dataobj.excelFileBase64.Length > 0)
            {
                long bookAdded = 0;
                string fileName = string.Empty;
                byte[] imageBytes;

                if (dataobj.excelFileBase64.Contains("data:"))
                {
                    var strInfo = dataobj.excelFileBase64.Split(",")[0];
                    imageBytes = Convert.FromBase64String(dataobj.excelFileBase64.Split(',')[1]);
                }
                else
                {
                    imageBytes = Convert.FromBase64String(dataobj.excelFileBase64);
                }

                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    var exhibitor = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == exhibitorId).FirstOrDefault();

                    fileName = string.Format("{0}_{1}{2}", LangId, MethodFactory.GetRandomNumber(), ".xlsx");
                    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\ExhibitorBooks\\") + fileName, imageBytes);
                    exhibitor.BookFileName = fileName;
                    exhibitor.IsBooksUpload = "Y";
                    _context.SaveChanges();

                    using (var transaction = _context.Database.BeginTransaction())
                    {
                        using (var stream = new MemoryStream(imageBytes))
                        {
                            using (var package = new ExcelPackage(stream))
                            {
                                #region Get Dependency List
                                // PublisherList = GetExhibitionBookPublisherList();
                                //AuthorList = GetExhibitionAuthorList();
                                BookLanguageList = GetExhibitionLanguageList();
                                BookSubjectList = GetExhibitionBookSubjectList();
                                BookSubsubjectList = GetExhibitionBookSubsubjectList();
                                #endregion

                                ExcelWorksheet worksheet = package.Workbook.Worksheets.ToList()[0];
                                var rowCount = worksheet.Dimension.Rows;
                                long agencyId = -1;
                                //long bookAuthorId = -1;
                                //long bookPublisherId = -1;
                                long bookLanguageId = -1;
                                long bookSubjectId = -1;
                                long bookSubSubjectId = -1;

                                //if (exhibitorId != -1)
                                //    agencyId = GetAgencyByMember(dataobj.MemberId, exhibitorId);

                                for (int row = 2; row <= rowCount; row++)
                                {
                                    agencyId = -1;
                                    bookLanguageId = -1;
                                    bookSubjectId = -1;
                                    bookSubSubjectId = -1;
                                    if (worksheet.Cells[row, 1].Value != null && worksheet.Cells[row, 2].Value != null && worksheet.Cells[row, 3].Value != null && worksheet.Cells[row, 4].Value != null && worksheet.Cells[row, 5].Value != null && worksheet.Cells[row, 6].Value != null && worksheet.Cells[row, 7].Value != null && worksheet.Cells[row, 8].Value != null)
                                    {
                                        XsiExhibitionBooks books = new XsiExhibitionBooks();

                                        if (worksheet.Cells[row, 1].Value != null)
                                            books.TitleEn = worksheet.Cells[row, 1].Value.ToString().Trim();

                                        if (!string.IsNullOrEmpty(worksheet.Cells[row, 2].Text.Trim()))
                                        {
                                            books.BookPublisherName = worksheet.Cells[row, 2].Text.Trim();
                                            books.BookPublisherNameAr = worksheet.Cells[row, 2].Text.Trim();
                                        }
                                        if (!string.IsNullOrEmpty(worksheet.Cells[row, 3].Text.ToString().Trim()))
                                        {
                                            books.AuthorName = worksheet.Cells[row, 3].Text.Trim();
                                            books.AuthorNameAr = worksheet.Cells[row, 3].Text.Trim();
                                        }

                                        if (worksheet.Cells[row, 4].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 4].Value.ToString().Trim()))
                                        {
                                            string subject = worksheet.Cells[row, 4].Value.ToString().Trim().Split('_')[0];
                                            XsiExhibitionBookSubject entity = new XsiExhibitionBookSubject();
                                            if (LangId == 1)
                                                entity = BookSubjectList.Where(i => i.Title.Trim() == subject).FirstOrDefault();
                                            else
                                                entity = BookSubjectList.Where(i => i.TitleAr.Trim() == subject).FirstOrDefault();
                                            if (entity != null)
                                            {
                                                bookSubjectId = entity.ItemId;
                                            }
                                            if (bookSubjectId > 0)
                                                books.ExhibitionSubjectId = bookSubjectId;
                                        }
                                        if (worksheet.Cells[row, 4].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 4].Value.ToString().Trim()))
                                        {
                                            string subsubject = string.Empty; //worksheet.Cells[row, 4].Value.ToString().Trim().Split('_')[1];
                                            if (worksheet.Cells[row, 4].Value.ToString().Trim().IndexOf('_') > -1)
                                            {
                                                subsubject = worksheet.Cells[row, 4].Value.ToString().Trim().Split('_')[1];
                                            }
                                            XsiExhibitionBookSubsubject entity = new XsiExhibitionBookSubsubject();
                                            if (LangId == 1)
                                                entity = BookSubsubjectList.Where(i => i.Title.Trim() == subsubject).FirstOrDefault();
                                            else
                                                entity = BookSubsubjectList.Where(i => i.TitleAr.Trim() == subsubject).FirstOrDefault();
                                            if (entity != null)
                                            {
                                                bookSubSubjectId = entity.ItemId;
                                            }
                                            if (bookSubSubjectId > 0)
                                                books.ExhibitionSubsubjectId = bookSubSubjectId;
                                        }
                                        if (worksheet.Cells[row, 5].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 5].Value.ToString().Trim()))
                                        {
                                            XsiExhibitionLanguage entity = new XsiExhibitionLanguage();
                                            if (LangId == 1)
                                                entity = BookLanguageList.Where(i => i.Title.Trim() == worksheet.Cells[row, 5].Value.ToString().Trim()).FirstOrDefault();
                                            else
                                                entity = BookLanguageList.Where(i => i.TitleAr.Trim() == worksheet.Cells[row, 5].Value.ToString().Trim()).FirstOrDefault();
                                            if (entity != null)
                                            {
                                                bookLanguageId = entity.ItemId;
                                            }
                                            if (bookLanguageId > 0)
                                                books.BookLanguageId = bookLanguageId;
                                        }
                                        if (worksheet.Cells[row, 6].Value != null)
                                            books.IssueYear = worksheet.Cells[row, 6].Value.ToString().Trim();
                                        if (worksheet.Cells[row, 7].Value != null)
                                            books.Price = worksheet.Cells[row, 7].Value.ToString().Trim();
                                        if (worksheet.Cells[row, 8].Value != null)
                                            books.PriceBeforeDiscount = worksheet.Cells[row, 8].Value.ToString().Trim();
                                        if (worksheet.Cells[row, 9].Value != null)
                                            books.Isbn = worksheet.Cells[row, 9].Value.ToString().Trim();
                                        if (worksheet.Cells[row, 10].Value != null)
                                            books.BookDetails = worksheet.Cells[row, 10].Value.ToString().Trim();
                                        if (worksheet.Cells[row, 11].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 11].Value.ToString().Trim()))
                                        {
                                            if (worksheet.Cells[row, 11].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 11].Value.ToString()))
                                                books.IsBestSeller = GetBestSeller(worksheet.Cells[row, 11].Value.ToString().Trim());
                                            else
                                                books.IsBestSeller = EnumConversion.ToString(EnumBool.No);
                                        }

                                        books.MemberId = memberid;
                                        books.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                        if (books.TitleEn != string.Empty && !string.IsNullOrEmpty(books.BookPublisherName) && !string.IsNullOrEmpty(books.AuthorName) &&
                                            books.ExhibitionSubjectId > 0 && books.ExhibitionSubsubjectId > 0 && books.BookLanguageId > 0 && books.IssueYear != string.Empty &&
                                            books.Price != string.Empty)
                                        {
                                            books.IsEnable = EnumConversion.ToString(EnumBool.Yes);
                                        }
                                        books.IsNew = EnumConversion.ToString(EnumBool.Yes);
                                        books.CreatedBy = memberid;
                                        books.CreatedOn = MethodFactory.ArabianTimeNow();
                                        books.ModifiedBy = memberid;
                                        books.ModifiedOn = MethodFactory.ArabianTimeNow();
                                        _context.XsiExhibitionBooks.Add(books);
                                        _context.SaveChanges();
                                        if (books.BookId > 0)
                                        {
                                            bookAdded = bookAdded + 1;
                                            //if (bookAuthorId > 0)
                                            //{
                                            //    XsiExhibitionBookAuthor bookAuthor = new XsiExhibitionBookAuthor();
                                            //    bookAuthor.BookId = books.BookId;
                                            //    bookAuthor.AuthorId = bookAuthorId;
                                            //    _context.XsiExhibitionBookAuthor.Add(bookAuthor);
                                            //    _context.SaveChanges();
                                            //}

                                            #region Books Participating
                                            XsiExhibitionBookParticipating bookParticipating = new XsiExhibitionBookParticipating();
                                            bookParticipating.BookId = books.BookId;
                                            bookParticipating.ExhibitorId = exhibitorId;

                                            if (bookParticipating.ExhibitorId != -1 && agencyId != -1)
                                                bookParticipating.AgencyId = agencyId;

                                            bookParticipating.IsActive = EnumConversion.ToString(EnumBool.No);
                                            bookParticipating.Price = worksheet.Cells[row, 7].Value.ToString().Trim();
                                            bookParticipating.CreatedOn = MethodFactory.ArabianTimeNow();
                                            bookParticipating.CreatedBy = memberid;
                                            bookParticipating.ModifiedOn = MethodFactory.ArabianTimeNow();
                                            bookParticipating.ModifiedBy = memberid;
                                            _context.XsiExhibitionBookParticipating.Add(bookParticipating);
                                            _context.SaveChanges();
                                            #endregion
                                        }
                                    }
                                }
                                if (bookAdded > 0)
                                {
                                    transaction.Commit();
                                    if (LangId == 1)
                                    {
                                        return bookAdded + " books added successfully";
                                    }
                                    else
                                    {
                                        return bookAdded + " تمت إضافة الكتاب بنجاح";
                                    }
                                }
                                else
                                {
                                    return "Something went wrong while importing books. Please try again later.";
                                }
                            }
                        }
                    }
                }
            }
            return "No books found to import";
        }
        List<XsiExhibitionBookSubject> GetExhibitionBookSubjectList()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiExhibitionBookSubject.Where(x => (x.Title != null && x.TitleAr != null) && x.IsActive == "Y").ToList();
            }
        }
        List<XsiExhibitionBookSubsubject> GetExhibitionBookSubsubjectList()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiExhibitionBookSubsubject.Where(x => (x.Title != null && x.TitleAr != null) && x.IsActive == "Y").ToList();
            }
        }
        List<XsiExhibitionLanguage> GetExhibitionLanguageList()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiExhibitionLanguage.Where(x => (x.Title != null && x.TitleAr != null) && x.IsActive == "Y").ToList();
            }
        }
        string GetBestSeller(string text)
        {
            if (text == "Yes")
                return "Y";
            return "N";
        }
        #endregion

        #region Update
        void UpdateExhibitorRegistration(string IsEditRequest, SaveDataForStepTwoAndThreeDTO model)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                string memberemail = string.Empty;
                XsiExhibitionMemberApplicationYearly entityOld = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(model.ExhibitorId);
                XsiExhibitionMemberApplicationYearly exhibitor = new XsiExhibitionMemberApplicationYearly();
                ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService();
                XsiExhibitionMember exhibitionmemberentity = new XsiExhibitionMember();
                exhibitionmemberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(model.MemberId);
                if (exhibitionmemberentity != default(XsiExhibitionMember) && exhibitionmemberentity.Email != null && !string.IsNullOrEmpty(exhibitionmemberentity.Email))
                    memberemail = exhibitionmemberentity.Email;
                if (entityOld != null)
                {
                    exhibitor.MemberExhibitionYearlyId = model.ExhibitorId;
                    if (entityOld.FileNumber != null)
                        exhibitor.FileNumber = entityOld.FileNumber;
                    if (!string.IsNullOrEmpty(IsEditRequest))
                    {
                        exhibitor.IsEditRequest = IsEditRequest;
                        exhibitor.IsEditRequestEmailSent = EnumConversion.ToString(EnumBool.No);
                    }

                    exhibitor.ExhibitionId = entityOld.ExhibitionId;
                    exhibitor.MemberId = model.MemberId;
                    exhibitor.CountryId = model.CountryId;
                    exhibitor.CityId = model.CityId;
                    exhibitor.OtherCity = model.OtherCity;
                    exhibitor.ExhibitionCategoryId = model.ExhibitionCategoryId;
                    exhibitor.PublisherNameEn = model.PublisherNameEn;
                    exhibitor.PublisherNameAr = model.PublisherNameAr;
                    exhibitor.ContactPersonName = model.ContactPersonName;
                    exhibitor.ContactPersonNameAr = model.ContactPersonNameAr;
                    exhibitor.ContactPersonTitle = model.ContactPersonTitle;
                    exhibitor.ContactPersonTitleAr = model.ContactPersonTitleAr;
                    if (entityOld.IsFirstTime != null)
                        exhibitor.IsFirstTime = entityOld.IsFirstTime;

                    if (!string.IsNullOrEmpty(model.PhoneISD) && !string.IsNullOrEmpty(model.PhoneSTD) && !string.IsNullOrEmpty(model.Phone))
                        exhibitor.Phone = model.PhoneISD + "$" + model.PhoneSTD + "$" + model.Phone;
                    if (!string.IsNullOrEmpty(model.MobileISD) && !string.IsNullOrEmpty(model.MobileSTD) && !string.IsNullOrEmpty(model.Mobile))
                        exhibitor.Mobile = model.MobileISD + "$" + model.MobileSTD + "$" + model.Mobile;
                    if (!string.IsNullOrEmpty(model.FaxISD) && !string.IsNullOrEmpty(model.FaxSTD) && !string.IsNullOrEmpty(model.Fax))
                        exhibitor.Fax = model.FaxISD + "$" + model.FaxSTD + "$" + model.Fax;
                    exhibitor.Email = model.Email;
                    if (entityOld.IsActive != null)
                        exhibitor.IsActive = entityOld.IsActive;
                    if (entityOld.IsStatusChanged != null)
                        exhibitor.IsStatusChanged = entityOld.IsStatusChanged;

                    if (!string.IsNullOrEmpty(model.TotalNumberOfTitles))
                        exhibitor.TotalNumberOfTitles = model.TotalNumberOfTitles.Trim();
                    if (!string.IsNullOrEmpty(model.TotalNumberOfNewTitles))
                        exhibitor.TotalNumberOfNewTitles = model.TotalNumberOfNewTitles.Trim();

                    if (entityOld.Status != null)
                        exhibitor.Status = entityOld.Status;

                    exhibitor.ModifiedOn = MethodFactory.ArabianTimeNow();
                    exhibitor.ModifiedBy = model.MemberId;
                    //  entity.FileName = (!string.IsNullOrEmpty(model.FileName)) ? model.FileName : entityOld.FileName;

                    //if (!string.IsNullOrEmpty(model.IsBooksUpload))
                    //{
                    //    exhibitor.IsBooksUpload = model.IsBooksUpload == "Y" ? "Y" : "N";
                    //}

                    if (ExhibitorRegistrationService.UpdateExhibitorRegistration(exhibitor) == EnumResultType.Success)
                    {
                        UpdateExhibitorDetails(model, model.ExhibitorId, LangId);
                        if (model.ActivityIds.Count > 0)
                        {
                            UpdateActivities(model.ActivityIds, model.ExhibitorId);
                        }
                        ImportBooks(model.ExhibitorId, model.MemberId, model, LangId);

                        //if (IsEditRequest == EnumBool.Yes)
                        //    SendEmailEditRequest(entity);
                        //   NewStatusMsg.Visible = true;

                        //EnableNewStatusMessage();
                        //LogExhibitorStatus(entityOld.Status);
                    }
                }
            }
        }
        void UpdateExhibitorDetails(SaveDataForStepTwoAndThreeDTO model, long exhibitorid, long langId)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                #region Details
                XsiExhibitionExhibitorDetails entityDetailOld = ExhibitorRegistrationService.GetDetailsById(model.ExhibitorId);
                XsiExhibitionExhibitorDetails entityDetail = new XsiExhibitionExhibitorDetails();
                if (entityDetailOld != default(XsiExhibitionExhibitorDetails))
                {
                    entityDetail.ExhibitorDetailsId = entityDetailOld.ExhibitorDetailsId;
                }
                entityDetail.MemberExhibitionYearlyId = exhibitorid;
                if (model.BoothSectionId > 0)
                    entityDetail.BoothSectionId = model.BoothSectionId;
                if (model.BoothSubSectionId > 0)
                    entityDetail.BoothSubSectionId = model.BoothSubSectionId;

                if (model.Brief != null)
                    entityDetail.Brief = model.Brief.Trim();
                if (model.BriefAr != null)
                    entityDetail.BriefAr = model.BriefAr.Trim();
                entityDetail.Apu = model.APU;
                if (model.LogoBase64 != null && model.LogoBase64.Length > 0)
                {
                    string fileName = string.Empty;
                    byte[] imageBytes;
                    if (model.LogoBase64.Contains("data:"))
                    {
                        var strInfo = model.LogoBase64.Split(",")[0];
                        imageBytes = Convert.FromBase64String(model.LogoBase64.Split(',')[1]);
                    }
                    else
                    {
                        imageBytes = Convert.FromBase64String(model.LogoBase64);
                    }

                    fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + model.LogoExt);
                    if (MethodFactory.IsValidLogo_TradeLicense("." + model.LogoExt))
                    {
                        System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\Logo\\") + fileName, imageBytes);
                        entityDetail.FileName = fileName;
                    }
                }
                if (model.TradeLicenceBase64 != null && model.TradeLicenceBase64.Length > 0)
                {
                    string fileName = string.Empty;
                    byte[] imageBytes;
                    if (model.TradeLicenceBase64.Contains("data:"))
                    {
                        var strInfo = model.TradeLicenceBase64.Split(",")[0];
                        imageBytes = Convert.FromBase64String(model.TradeLicenceBase64.Split(',')[1]);
                    }
                    else
                    {
                        imageBytes = Convert.FromBase64String(model.TradeLicenceBase64);
                    }

                    fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + model.TradeLicenceExt);
                    if (MethodFactory.IsValidLogo_TradeLicense("." + model.TradeLicenceExt))
                    {
                        System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\TradeLicence\\") + fileName, imageBytes);
                        entityDetail.TradeLicence = fileName;
                    }
                }
                //entityDetail.TradeLicence = (!string.IsNullOrEmpty(model.TradeLicenceName)) ? model.TradeLicenceName : entityDetailOld.TradeLicence;
                #region Calculate Area
                entityDetail.RequiredAreaType = model.RequiredAreaShapeType;
                if (model.RequiredAreaId > 0)
                {
                    entityDetail.RequiredAreaId = model.RequiredAreaId;
                    if (model.RequiredAreaShapeType == EnumConversion.ToString(EnumRequiredAreaType.Open))
                    {
                        long CalculatedArea = 0;
                        if (model.RequiredArea == "3")
                        {
                            CalculatedArea = Convert.ToInt64(model.RequiredAreaValue) / Convert.ToInt64(model.RequiredAreaMeter);
                            entityDetail.CalculatedArea = CalculatedArea.ToString() + "x" + model.RequiredAreaMeter;
                        }
                        if (model.RequiredArea == "6")
                        {
                            CalculatedArea = Convert.ToInt64(model.RequiredAreaValue) / Convert.ToInt64(model.RequiredAreaMeter);
                            entityDetail.CalculatedArea = CalculatedArea.ToString() + "x" + model.RequiredAreaMeter;
                        }
                    }
                }
                entityDetail.BoothDetail = model.BoothDetail;
                #endregion

                #region Other Fields
                if (entityDetailOld != default(XsiExhibitionExhibitorDetails))
                {
                    if (entityDetail.AllocatedSpace != null)
                        entityDetail.AllocatedSpace = entityDetailOld.AllocatedSpace;
                    if (entityDetail.Area != null)
                        entityDetail.Area = entityDetailOld.Area;
                    if (entityDetail.HallNumber != null)
                        entityDetail.HallNumber = entityDetailOld.HallNumber;
                    if (entityDetail.StandNumberStart != null)
                        entityDetail.StandNumberStart = entityDetailOld.StandNumberStart;
                    if (entityDetail.StandNumberEnd != null)
                        entityDetail.StandNumberEnd = entityDetailOld.StandNumberEnd;
                    if (entityDetail.FirstLocation != null)
                        entityDetail.FirstLocation = entityDetailOld.FirstLocation;
                    if (entityDetail.SecondLocation != null)
                        entityDetail.SecondLocation = entityDetailOld.SecondLocation;
                    if (entityDetail.ThirdLocation != null)
                        entityDetail.ThirdLocation = entityDetailOld.ThirdLocation;
                    if (entityDetail.FourthLocation != null)
                        entityDetail.FourthLocation = entityDetailOld.FourthLocation;
                    if (entityDetail.FifthLocation != null)
                        entityDetail.FifthLocation = entityDetailOld.FifthLocation;
                    if (entityDetail.SixthLocation != null)
                        entityDetail.SixthLocation = entityDetailOld.SixthLocation;
                    if (entityDetail.StandCode != null)
                        entityDetail.StandCode = entityDetailOld.StandCode;
                    if (entityDetail.CancellationReason != null)
                        entityDetail.CancellationReason = entityDetailOld.CancellationReason;
                }
                #endregion

                entityDetail.ModifiedOn = MethodFactory.ArabianTimeNow();
                entityDetail.ModifiedBy = model.MemberId;
                if (ExhibitorRegistrationService.UpdateExhibitorRegistrationDetails(entityDetail) == EnumResultType.Success)
                {
                    // UpdateExhibitionMemberCompanyName(exhibitor);
                }
                #endregion
            }
        }
        void UpdateActivities(List<long> ActivityIds, long exhibitorid)
        {
            using (ExhibitorActivityService ExhibitorActivityService = new ExhibitorActivityService())
            {
                XsiMemberExhibitionActivityYearly existingActivityEntity = new XsiMemberExhibitionActivityYearly();
                existingActivityEntity.MemberExhibitionYearlyId = exhibitorid;
                ExhibitorActivityService.DeleteExhibitorActivity(existingActivityEntity);

                XsiMemberExhibitionActivityYearly entity1 = new XsiMemberExhibitionActivityYearly();

                foreach (var item in ActivityIds)
                {
                    entity1.MemberExhibitionYearlyId = exhibitorid;
                    entity1.ActivityId = Convert.ToInt64(item);
                    ExhibitorActivityService.InsertExhibitorActivity(entity1);
                }
            }
        }
        #endregion

        #region Helper Methods
        private ExhibitorRegistrationDTO GetExhibitorView(long memberId, long websiteId, XsiExhibition exhibition, long LangId = 1)
        {
            ExhibitorRegistrationDTO model = new ExhibitorRegistrationDTO();
            if (exhibition != null)
            {
                using (ExhibitionService ExhibitionService = new ExhibitionService())
                {
                    ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService();

                    long ExhibitorId = 0;
                    bool IsFirstTime = false;

                    model.MemberId = memberId;
                    model.WebsiteId = websiteId;
                    model.ExhibitionId = exhibition.ExhibitionId;

                    var memberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(memberId);
                    if (memberentity != null)
                    {
                        model.IsBooksRequired = (memberentity.IsBooksRequired == "Y") ? true : false;
                    }
                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        model.IsNotesRead = EnumConversion.ToString(EnumBool.No);
                        model.ExhibitionId = exhibition.ExhibitionId;
                        ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                        XsiExhibitionMemberApplicationYearly ExhibitorEntity = null;
                        XsiInvoice InvoiceEntity = new XsiInvoice();
                        string strExhbitorCancelled = EnumConversion.ToString(EnumExhibitorStatus.Cancelled);
                        string strExhbitorReject = EnumConversion.ToString(EnumExhibitorStatus.Rejected);
                        model.StepId = 1;

                        ExhibitorEntity = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly { MemberRoleId = 1, MemberId = memberId, ExhibitionId = exhibition.ExhibitionId, IsActive = EnumConversion.ToString(EnumBool.Yes) }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

                        if (ExhibitorEntity != null)
                        {
                            if (!string.IsNullOrEmpty(ExhibitorEntity.IsNotesRead))
                                model.IsNotesRead = ExhibitorEntity.IsNotesRead;

                            model.Status = ExhibitorEntity.Status;
                            if (ExhibitorEntity.Status != strExhbitorReject && ExhibitorEntity.Status != strExhbitorCancelled)
                            {

                                ExhibitorId = ExhibitorEntity.MemberExhibitionYearlyId;
                                model.ExhibitorId = ExhibitorId;
                                IsFirstTime = false;
                            }
                            else
                            {
                                model.MemberId = memberId;
                                model.WebsiteId = exhibition.WebsiteId.Value;

                                if (LangId == 1)
                                    model.Message = "User already participated for this exhibition.";
                                else
                                    model.Message = "User already participated for this exhibition.";
                                return model;
                            }
                        }
                        else
                        {
                            model.StepId = 1;
                            IsFirstTime = true;
                            model.Message = "";
                            model.Status = EnumConversion.ToString(EnumExhibitorStatus.New);
                            //.Where(p => p.Status == strExhbitorReject && p.Status == strExhbitorCancelled)
                            // ExhibitorEntity = ExhibitorRegistrationService.GetExhibitorRegistration(new XsiExhibitionMemberApplicationYearly { MemberId = memberId, ExhibitionId = exhibition.ExhibitionId, IsActive = EnumConversion.ToString(EnumBool.Yes) }, EnumSortlistBy.ByItemIdDesc).Where(p => p.Status != strExhbitorReject && p.Status != strExhbitorCancelled).FirstOrDefault();
                            model.StepId = 1;
                            if (MethodFactory.GetAgencyRegistered(memberId, websiteId, _context) == null)
                            {
                                var loadExibitor = LoadStepTwoDTOByMemberId(websiteId, memberId);
                                if (LangId == 1)
                                    model.Message = "";
                                else
                                    model.Message = "";
                                model.StepTwoDTO = loadExibitor;
                            }
                            else
                            {
                                if (LangId == 1)
                                    model.Message = "Registered As Agent";
                                else
                                    model.Message = "Registered As Agent";
                            }
                        }

                        if (IsFirstTime)
                        {
                            model.StepId = 1;
                            if (MethodFactory.GetAgencyRegistered(memberId, websiteId, _context) == null)
                            {
                                var loadExibitor = LoadStepTwoDTOByMemberId(websiteId, memberId);
                                if (LangId == 1)
                                    model.Message = "";
                                else
                                    model.Message = "";
                                model.StepTwoDTO = loadExibitor;
                            }
                            else
                            {
                                if (LangId == 1)
                                    model.Message = "Registered As Agent";
                                else
                                    model.Message = "Registered As Agent";
                            }
                        }
                        else
                        {
                            if (ExhibitorEntity != null)
                            {
                                ExhibitorId = ExhibitorEntity.MemberExhibitionYearlyId;
                                model.ExhibitorId = ExhibitorId;

                                if (ExhibitorEntity.IsEditRequest == EnumConversion.ToString(EnumExhibitorEditStatus.New))
                                {
                                    model.StepId = 3;
                                    if (LangId == 1)
                                        model.Message = "Your modification request has been sent and your modified data will be reviewed later.";
                                    else
                                        model.Message = "تم إرسال طلب التعديل وسيتم مراجعة بياناتكم والرد عليكم لاحقا";
                                }
                                else
                                {
                                    #region Status Based View
                                    if (ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.New) || ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.PreApproved) || ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.SampleReceived) || ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval) || ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingApproval) || ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingDocumentation))
                                    {
                                        model.StepId = 3;
                                        if (ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval))
                                        {
                                            model.StepId = 3;
                                            model.StepTwoDTO = LoadStepTwoContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                            model.StepThreeDTO = LoadStepThreeContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                            model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                            model.StepFiveDTO = LoadStepFiveContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                            model.StepSixDTO = LoadStepSixContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);

                                            if (LangId == 1)
                                            {
                                                if (ExhibitorEntity.IsAllowApprove == "N")
                                                {
                                                    model.DueMessage = "<p>Please note that there are pending financial dues. Accordingly, we will not consider the participation request until the payment is completed.</p>";
                                                }

                                                model.Message = @"<p>Your Exhibition Registration has been approved.</p><p>You will be notified shortly regarding payment. Only after receiving the payment as per the payment rules, your participation will be confirmed.</p>";
                                            }
                                            else
                                            {
                                                if (websiteId == 1)
                                                {
                                                    if (ExhibitorEntity.IsAllowApprove == "N")
                                                    {
                                                        model.DueMessage = "<p>نود التنويه بأن هناك مستحقات مالية لم يتم دفعها، وبناءً على ذلك، لن نقوم بالنظر في طلب المشاركة حتى يتم استكمال الدفع</p>";
                                                    }

                                                    model.Message = @"<p>تمت الموافقة على تسجيلكم في المعرض.</p><p>سيتم إخطاركم قريبًا بشأن الدفع. بعد استلام المبلغ وفقًا لقوانين الدفع، سيتم تأكيد مشاركتكم</p>";
                                                }
                                                else
                                                {
                                                    model.Message = @"<p>تمت الموافقة على تسجيلكم في المهرجان.</p><p>سيتم إخطاركم قريبًا بشأن الدفع. بعد استلام المبلغ وفقًا لقوانين الدفع، سيتم تأكيد مشاركتكم</p>";
                                                }
                                            }
                                            /*
                                            #region To User Later
                                                string strCancelled = EnumConversion.ToString(EnumExhibitorStatus.Cancelled);
                                                if (LangId == 1)
                                                    model.Message = "Step3Thankyou";
                                                else
                                                    model.Message = "Step3Thankyou";
                                                #region Check Invoice Table
                                                InvoiceService InvoiceService = new InvoiceService();
                                                XsiInvoice where = new XsiInvoice();
                                                where.ExhibitorId = ExhibitorId;
                                                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                                where.IsRegistrationInvoice = EnumConversion.ToString(EnumBool.Yes);
                                                InvoiceEntity = InvoiceService.GetInvoice(where, EnumSortlistBy.ByItemIdDesc).Where(p => p.Status != strCancelled).OrderBy(o => o.ItemId).FirstOrDefault();
                                                if (InvoiceEntity != null)
                                                {
                                                    model.StepId = 4;
                                                    if (InvoiceEntity.Status != null)
                                                    {
                                                        if (InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.PendingPayment))
                                                        {
                                                            var InvoiceId = InvoiceEntity.ItemId;
                                                            //EnableUpload(InvoiceEntity);
                                                            if (LangId == 1)
                                                                model.Message = "EnableUpload";
                                                            else
                                                                model.Message = "EnableUpload";


                                                            //var payment = LoadStepFourContent(InvoiceEntity, exhibition);
                                                            //payment.InvoiceId = InvoiceId;

                                                            model.StepTwoDTO = LoadStepTwoContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                            model.StepThreeDTO = LoadStepThreeContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                            model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                            model.StepFiveDTO = LoadStepFiveContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                            model.StepSixDTO = LoadStepSixContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                        }
                                                        else if (InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.ReceiptUploaded) || InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.ReceiptReviewed) || InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.Paid) || InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.PartiallyPaid))
                                                        {
                                                            model.StepId = 5;
                                                            // || InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.PaymentInProcess) -- ReceiptReviewed
                                                            var InvoiceId = InvoiceEntity.ItemId;
                                                            //EnableUpload(InvoiceEntity);
                                                            string strtypemessage = string.Empty;
                                                            if (InvoiceEntity.PaymentTypeId == EnumConversion.ToLong(EnumPaymentType.PaidByCash))
                                                            {

                                                            }
                                                            else if (InvoiceEntity.PaymentTypeId == EnumConversion.ToLong(EnumPaymentType.PaidByBankTransfer))
                                                            {
                                                            }
                                                            else if (InvoiceEntity.PaymentTypeId == EnumConversion.ToLong(EnumPaymentType.PaidByChequeTransfer))
                                                            {
                                                                if (LangId == 1)
                                                                    strtypemessage = "Note : Your cheque has been received by SIBF.You will be notified via email once payment has been approved.";
                                                                else
                                                                {
                                                                    if (websiteId == 1)
                                                                    {
                                                                        strtypemessage = "ملاحظة: تم إستلام الشيك الخاص بك من قبل إدارة المعرض. سيتم إعلامك بعد صرف الشيك";
                                                                    }
                                                                    else
                                                                    {
                                                                        strtypemessage = "ملاحظة: تم إستلام الشيك الخاص بك من قبل إدارة المهرجان. سيتم إعلامك بعد صرف الشيك";
                                                                    }
                                                                }

                                                            }
                                                            else if (InvoiceEntity.PaymentTypeId == EnumConversion.ToLong(EnumPaymentType.PaidByOnline))
                                                            {
                                                            }


                                                            if (InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.ReceiptUploaded))
                                                            {
                                                                if (LangId == 1)
                                                                    model.Message = "Note : You will be notified via email after the payments are approved by accounting department.";
                                                                else
                                                                    model.Message = "ملاحظة: سيتم إعلامك بعد أن يتم إعتماد المبلغ من قبل قسم الحسابات";
                                                            }
                                                            else if (InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.ReceiptReviewed))
                                                            {
                                                                if (LangId == 1)
                                                                    model.Message = "Receipt correct and waiting to be in our account.";
                                                                else
                                                                    model.Message = "Receipt correct and waiting to be in our account.";

                                                                if (InvoiceEntity.IsChequeReceived == EnumConversion.ToString(EnumBool.Yes))
                                                                {
                                                                    if (LangId == 1)
                                                                        model.Message = strtypemessage;
                                                                    else
                                                                        model.Message = strtypemessage;
                                                                }
                                                            }
                                                            else if (InvoiceEntity.Status == EnumConversion.ToString(EnumExhibitorInvoiceStatus.Paid))
                                                            {
                                                                if (LangId == 1)
                                                                    model.Message = "Note: Your payment is approved by the accounting department. Your stand details will be sent shortly.";
                                                                else
                                                                    model.Message = "ملاحظة: تم إعتماد المبلغ من قبل قسم الحسابات. سيتم إرسال تفاصيل الجناح الخاص بكم قريبا";
                                                            }
                                                            else
                                                            {
                                                                if (LangId == 1)
                                                                    model.Message = "Note: You will be notified via email after the payments are approved by accounting department.";
                                                                else
                                                                    model.Message = "ملاحظة: سيتم إعلامك بعد أن يتم إعتماد المبلغ من قبل قسم الحسابات";
                                                            }

                                                            model.StepTwoDTO = LoadStepTwoContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                            model.StepThreeDTO = LoadStepThreeContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                            model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                            model.StepFiveDTO = LoadStepFiveContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                            model.StepSixDTO = LoadStepSixContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                            //  MVRegistration.SetActiveView(Step5Thankyou);
                                                        }
                                                        else
                                                        {
                                                            model.StepId = 3;
                                                            if (LangId == 1)
                                                                model.Message = @"<p>Your Exhibition Registration has been approved.</p><p>You will be notified shortly regarding payment. Only after receiving the payment as per the payment rules, your participation will be confirmed.</p>";
                                                            else
                                                            {
                                                                if (websiteId == 1)
                                                                    model.Message = @"<p>تمت الموافقة على تسجيلكم في المعرض.</p><p>سيتم إخطاركم قريبًا بشأن الدفع. بعد استلام المبلغ وفقًا لقوانين الدفع، سيتم تأكيد مشاركتكم</p>";
                                                                else
                                                                    model.Message = @"<p>تمت الموافقة على تسجيلكم في المهرجان.</p><p>سيتم إخطاركم قريبًا بشأن الدفع. بعد استلام المبلغ وفقًا لقوانين الدفع، سيتم تأكيد مشاركتكم</p>";
                                                            }

                                                            model.StepTwoDTO = LoadStepTwoContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                            model.StepThreeDTO = LoadStepThreeContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                            model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                            model.StepFiveDTO = LoadStepFiveContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                            model.StepSixDTO = LoadStepSixContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                            //  MVRegistration.SetActiveView(Step3Thankyou);
                                                            //  NewStatusMsg.Visible = true;
                                                            // EnableInitialApprovalStatusMessage();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        model.StepId = 3;
                                                        model.StepTwoDTO = LoadStepTwoContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                        model.StepThreeDTO = LoadStepThreeContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                        model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                        model.StepFiveDTO = LoadStepFiveContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                        model.StepSixDTO = LoadStepSixContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);

                                                        if (LangId == 1)
                                                            model.Message = @" < p>Your Exhibition Registration has been submitted.</p><p>We'll get back to you soon.</p>";
                                                        else
                                                        {
                                                            if (websiteId == 1)
                                                                model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المعرض.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                                            else
                                                                model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المهرجان.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    model.StepId = 3;
                                                    model.StepTwoDTO = LoadStepTwoContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                    model.StepThreeDTO = LoadStepThreeContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                    model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                    model.StepFiveDTO = LoadStepFiveContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                    model.StepSixDTO = LoadStepSixContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                    if (LangId == 1)
                                                        model.Message = @"<p>Your Exhibition Registration has been submitted.</p><p>We'll get back to you soon.</p>";
                                                    else
                                                    {
                                                        if (websiteId == 1)
                                                            model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المعرض.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                                        else
                                                            model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المهرجان.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                                    }

                                                }
                                                #endregion
                                            #endregion
                                                */
                                        }
                                        else
                                        {
                                            if (LangId == 1)
                                            {
                                                if (ExhibitorEntity.IsAllowApprove == "N")
                                                {
                                                    model.DueMessage = "<p>Please note that there are pending financial dues. Accordingly, we will not consider the participation request until the payment is completed.</p>";
                                                }
                                                model.Message = @"<p>Your Exhibition Registration has been submitted.</p><p>We'll get back to you soon.</p>";
                                            }
                                            else
                                            {
                                                if (websiteId == 1)
                                                {
                                                    if (ExhibitorEntity.IsAllowApprove == "N")
                                                    {
                                                        model.DueMessage = "<p>نود التنويه بأن هناك مستحقات مالية لم يتم دفعها، وبناءً على ذلك، لن نقوم بالنظر في طلب المشاركة حتى يتم استكمال الدفع</p>";
                                                    }
                                                    model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المعرض.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                                }
                                                else
                                                    model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المهرجان.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                            }
                                            model.StepId = 3;
                                            if (ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.PreApproved))
                                            {
                                                if (LangId == 1)
                                                    model.Message = @"<p>Your Exhibition Registration is approved.</p><p>We'll get back to you soon.</p>";
                                                else
                                                    model.Message = @"<p>Your Exhibition Registration is approved.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                            }
                                            else if (ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.New) || ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.SampleReceived) || ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingApproval))
                                            {
                                                if (LangId == 1)
                                                {
                                                    if (ExhibitorEntity.IsAllowApprove == "N")
                                                    {
                                                        model.DueMessage = "<p>Please note that there are pending financial dues. Accordingly, we will not consider the participation request until the payment is completed.</p>";
                                                    }
                                                    model.Message = @"<p>Your Exhibition Registration has been submitted.</p><p>We'll get back to you soon.</p>";
                                                }
                                                else
                                                {
                                                    if (websiteId == 1)
                                                    {
                                                        if (ExhibitorEntity.IsAllowApprove == "N")
                                                        {
                                                            model.DueMessage = "<p>نود التنويه بأن هناك مستحقات مالية لم يتم دفعها، وبناءً على ذلك، لن نقوم بالنظر في طلب المشاركة حتى يتم استكمال الدفع</p>";
                                                        }
                                                        model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المعرض.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                                    }
                                                    else
                                                        model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المهرجان.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                                }
                                            }
                                            else
                                            {
                                                if (LangId == 1)
                                                {
                                                    if (ExhibitorEntity.IsAllowApprove == "N")
                                                    {
                                                        model.DueMessage = "<p>Please note that there are pending financial dues. Accordingly, we will not consider the participation request until the payment is completed.</p>";
                                                    }
                                                    model.Message = @"<p>Your Exhibition Registration has been submitted. </p><p>Please contact us by email or phone to know more about the pending formalities & documentations.</p>";
                                                }
                                                else
                                                {
                                                    if (websiteId == 1)
                                                    {
                                                        if (ExhibitorEntity.IsAllowApprove == "N")
                                                        {
                                                            model.DueMessage = "<p>نود التنويه بأن هناك مستحقات مالية لم يتم دفعها، وبناءً على ذلك، لن نقوم بالنظر في طلب المشاركة حتى يتم استكمال الدفع</p>";
                                                        }
                                                        model.Message = @"<p>تم تقديم طلبك للتسجيل في المعرض للإدارة.</p><p>الرجاء مراجعة بريدك الإلكتروني أو الإتصال بإدارة المعرض بخصوص الوثائق المطلوب تقديمها.</p>";
                                                    }
                                                    else
                                                        model.Message = @"<p>تم تقديم طلبك للتسجيل في المهرجان للإدارة.</p><p>الرجاء مراجعة بريدك الإلكتروني أو الإتصال بإدارة المهرجان بخصوص الوثائق المطلوب تقديمها.</p>";
                                                }

                                            }
                                            model.StepTwoDTO = LoadStepTwoContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                            model.StepThreeDTO = LoadStepThreeContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                            model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                            model.StepFiveDTO = LoadStepFiveContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                            model.StepSixDTO = LoadStepSixContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                        }
                                    }
                                    else if (ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved))
                                    {
                                        model.StepId = 6;
                                        model.StepTwoDTO = LoadStepTwoContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                        model.StepThreeDTO = LoadStepThreeContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                        model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                        model.StepFiveDTO = LoadStepFiveContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                        model.StepSixDTO = LoadStepSixContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                    }
                                    else
                                    {
                                        if (ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.New) || ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingDocumentation))
                                        {
                                            model.StepId = 2;
                                            if (model.ExhibitorId > 0)
                                            {
                                                model.StepTwoDTO = LoadStepTwoContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepThreeDTO = LoadStepThreeContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                model.StepFiveDTO = LoadStepFiveContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepSixDTO = LoadStepSixContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                            }
                                            else
                                                model.StepTwoDTO = LoadStepTwoDTOByMemberId(websiteId, memberId);
                                        }
                                        else
                                        {
                                            model.StepId = 3;
                                            if (LangId == 1)
                                            {
                                                if (ExhibitorEntity.IsAllowApprove == "N")
                                                {
                                                    model.DueMessage = "<p>Please note that there are pending financial dues. Accordingly, we will not consider the participation request until the payment is completed.</p>";
                                                }
                                                model.Message = @"<p>Your Exhibition Registration has been submitted.</p><p>We'll get back to you soon.</p>";
                                            }
                                            else
                                            {
                                                if (websiteId == 1)
                                                {
                                                    if (ExhibitorEntity.IsAllowApprove == "N")
                                                    {
                                                        model.DueMessage = "<p>نود التنويه بأن هناك مستحقات مالية لم يتم دفعها، وبناءً على ذلك، لن نقوم بالنظر في طلب المشاركة حتى يتم استكمال الدفع</p>";
                                                    }
                                                    model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المعرض.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                                }
                                                else
                                                    model.Message = @"<p> قد تم إستلام طلبك للتسجيل في المهرجان.</p><p>وسوف يتم الرد عليكم قريباً</p>";
                                            }

                                            if (model.ExhibitorId > 0)
                                            {
                                                model.StepTwoDTO = LoadStepTwoContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepThreeDTO = LoadStepThreeContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepFourDTO = LoadStepFourContent(InvoiceEntity, exhibition, LangId);
                                                model.StepFiveDTO = LoadStepFiveContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                                model.StepSixDTO = LoadStepSixContent(ExhibitorEntity.MemberExhibitionYearlyId, LangId);
                                            }
                                            else
                                                model.StepTwoDTO = LoadStepTwoDTOByMemberId(websiteId, memberId);
                                        }

                                    }
                                    #endregion
                                }
                            }

                            model.MemberId = memberId;
                            model.WebsiteId = exhibition.WebsiteId.Value;

                            return model;
                        }
                    }
                }

            }
            return model;
        }
        private StepTwoDTO LoadStepTwoContent(long exhibitorId, long langid = 1)
        {
            StepTwoDTO dto = new StepTwoDTO();
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly exhibitor = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                XsiExhibitionExhibitorDetails detail = ExhibitorRegistrationService.GetDetailsById(exhibitorId);
                if (exhibitor != null)
                {
                    //if (entity.IsEditRequest != null)
                    //    dto.CurrentEditRequest = entity.IsEditRequest;

                    if (exhibitor.PublisherNameEn != null)
                        dto.PublisherNameEn = exhibitor.PublisherNameEn;
                    if (exhibitor.PublisherNameAr != null)
                        dto.PublisherNameAr = exhibitor.PublisherNameAr;

                    #region Country and city
                    if (exhibitor.CountryId != null)
                    {
                        dto.CountryId = exhibitor.CountryId.Value;
                    }
                    if (exhibitor.CityId != null)
                    {
                        dto.CityId = exhibitor.CityId.Value;
                    }
                    if (exhibitor.OtherCity != null)
                    {
                        dto.OtherCity = exhibitor.OtherCity;
                    }
                    #endregion

                    if (exhibitor.ContactPersonName != null)
                        dto.ContactPersonName = exhibitor.ContactPersonName;
                    if (exhibitor.ContactPersonNameAr != null)
                        dto.ContactPersonNameAr = exhibitor.ContactPersonNameAr;
                    if (exhibitor.ContactPersonTitle != null)
                        dto.ContactPersonTitle = exhibitor.ContactPersonTitle;
                    if (exhibitor.ContactPersonTitleAr != null)
                        dto.ContactPersonTitleAr = exhibitor.ContactPersonTitleAr;

                    #region Phone,Mobile,Fax
                    if (exhibitor.Phone != null)
                    {
                        string[] str = exhibitor.Phone.Split('$');
                        if (str.Count() == 3)
                        {
                            dto.PhoneISD = str[0].Trim();
                            dto.PhoneSTD = str[1].Trim();
                            dto.Phone = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            dto.PhoneSTD = str[0].Trim();
                            dto.Phone = str[1].Trim();
                        }
                        else
                            dto.Phone = str[0].Trim();
                    }
                    if (exhibitor.Mobile != null)
                    {
                        string[] str = exhibitor.Mobile.Split('$');
                        if (str.Count() == 3)
                        {
                            dto.MobileISD = str[0].Trim();
                            dto.MobileSTD = str[1].Trim();
                            dto.Mobile = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            dto.MobileSTD = str[0].Trim();
                            dto.Mobile = str[1].Trim();
                        }
                        else
                            dto.Mobile = str[0].Trim();
                    }
                    if (exhibitor.Fax != null)
                    {
                        string[] str = exhibitor.Fax.Split('$');
                        if (str.Count() == 3)
                        {
                            dto.FaxISD = str[0].Trim();
                            dto.FaxSTD = str[1].Trim();
                            dto.Fax = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            dto.FaxSTD = str[0].Trim();
                            dto.Fax = str[1].Trim();
                        }
                        else
                            dto.Fax = str[0].Trim();
                    }
                    #endregion
                    if (exhibitor.Email != null)
                        dto.Email = exhibitor.Email;

                    if (exhibitor.BookFileName != null && !string.IsNullOrEmpty(exhibitor.BookFileName))
                        dto.BooksExcelFileName = exhibitor.BookFileName;

                    if (exhibitor.ExhibitionCategoryId.HasValue)
                    {
                        dto.ExhibitionCategoryId = exhibitor.ExhibitionCategoryId.Value;
                        if (IsTradeLicenceCategorySelected(exhibitor.ExhibitionCategoryId.Value))
                        {
                            if (detail != null && detail.TradeLicence != null)
                                dto.TradeLicenceName = detail.TradeLicence;
                        }
                    }

                    if (!string.IsNullOrEmpty(exhibitor.IsBooksUpload))
                    {
                        dto.IsBooksUpload = exhibitor.IsBooksUpload == "Y" ? "Y" : "N";
                    }
                    else
                    {
                        dto.IsBooksUpload = "N";
                    }

                    #region Exhibitor Activity
                    ExhibitorActivityService ExhibitorActivityService = new ExhibitorActivityService();
                    XsiMemberExhibitionActivityYearly where = new XsiMemberExhibitionActivityYearly();
                    where.MemberExhibitionYearlyId = exhibitor.MemberExhibitionYearlyId;
                    var Activities = ExhibitorActivityService.GetExhibitorActivity(where, EnumSortlistBy.ByAlphabetAsc).ToList();
                    List<long> ActivityIDs = new List<long>();

                    foreach (var item in Activities)
                    {
                        ActivityIDs.Add(item.ActivityId);
                    }

                    if (Activities != null)
                        dto.ActivityIds = ActivityIDs;

                    #endregion
                    if (detail != default(XsiExhibitionExhibitorDetails))
                    {
                        if (detail.FileName != null)
                            dto.LogoName = detail.FileName;

                        if (detail.TradeLicence != null)
                            dto.TradeLicenceName = detail.TradeLicence;


                        if (detail.Apu != null)
                            dto.APU = detail.Apu;

                        if (detail.Brief != null)
                            dto.Brief = detail.Brief;

                        if (detail.BriefAr != null)
                            dto.BriefAr = detail.BriefAr;
                    }
                }
            }
            return dto;
        }
        private StepTwoDTO LoadStepTwoDTOByMemberId(long websiteid, long memberId)
        {
            StepTwoDTO dto = new StepTwoDTO();
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                XsiExhibitionMember member = ExhibitionMemberService.GetExhibitionMemberByItemId(memberId);
                if (member != null)
                {
                    ExhibitionService exhibitionService = new ExhibitionService();
                    var exhibition = exhibitionService.GetExhibition(new XsiExhibition() { IsActive = "Y", IsArchive = "N", WebsiteId = websiteid }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                    //dto.ExhibitionId = exhibition.ExhibitionId;
                    #region Contact Fields
                    if (member.CompanyName != null)
                        dto.PublisherNameEn = member.CompanyName;
                    if (member.CompanyNameAr != null)
                        dto.PublisherNameAr = member.CompanyNameAr;
                    #region Country and city
                    if (member.CityId.HasValue)
                    {
                        dto.CityId = member.CityId.Value;
                        ExhibitionCityService ExhibitionCityService = new ExhibitionCityService();
                        dto.CountryId = ExhibitionCityService.GetExhibitionCityByItemId(member.CityId.Value).CountryId;
                    }
                    if (member.OtherCity != null)
                    {
                        dto.OtherCity = member.OtherCity;
                    }
                    #endregion
                    if (member.ContactPerson != null)
                        dto.ContactPersonName = member.ContactPerson;

                    //if (entity.ContactPerson != null)
                    //    dto.ContactPersonNameAr = entity.ContactPerson;

                    if (member.JobTitle != null)
                        dto.ContactPersonTitle = member.JobTitle;
                    if (member.JobTitleAr != null)
                        dto.ContactPersonTitleAr = member.JobTitleAr;
                    #endregion

                    #region Phone,Mobile,Fax
                    if (member.Phone != null)
                    {
                        string[] str = member.Phone.Split('$');
                        if (str.Count() == 3)
                        {
                            dto.PhoneISD = str[0].Trim();
                            dto.PhoneSTD = str[1].Trim();
                            dto.Phone = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            dto.PhoneSTD = str[0].Trim();
                            dto.Phone = str[1].Trim();
                        }
                        else
                            dto.Phone = str[0].Trim();
                    }
                    if (member.Mobile != null)
                    {
                        string[] str = member.Mobile.Split('$');
                        if (str.Count() == 3)
                        {
                            dto.MobileISD = str[0].Trim();
                            dto.MobileSTD = str[1].Trim();
                            dto.Mobile = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            dto.MobileSTD = str[0].Trim();
                            dto.Mobile = str[1].Trim();
                        }
                        else
                            dto.Mobile = str[0].Trim();
                    }
                    if (member.Fax != null)
                    {
                        string[] str = member.Fax.Split('$');
                        if (str.Count() == 3)
                        {
                            dto.FaxISD = str[0].Trim();
                            dto.FaxSTD = str[1].Trim();
                            dto.Fax = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            dto.FaxSTD = str[0].Trim();
                            dto.Fax = str[1].Trim();
                        }
                        else
                            dto.Fax = str[0].Trim();
                    }
                    #endregion

                    if (member.Email != null)
                        dto.Email = member.Email;

                    #region Brief En,Ar
                    if (member.Brief != null)
                        dto.Brief = member.Brief;
                    if (member.BriefAr != null)
                        dto.BriefAr = member.BriefAr;
                    #endregion
                }
            }
            return dto;
        }
        private StepThreeDTO LoadStepThreeContent(long exhibitorId, long langid = 1)
        {
            StepThreeDTO dto = new StepThreeDTO();
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                ExhibitionAreaService ExhibitionAreaService = new ExhibitionAreaService();
                XsiExhibitionMemberApplicationYearly entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                XsiExhibitionExhibitorDetails detail = ExhibitorRegistrationService.GetDetailsById(exhibitorId);
                if (entity != null)
                {
                    if (entity.IsEditRequest != null)
                        dto.CurrentEditRequest = entity.IsEditRequest;

                    if (entity.TotalNumberOfNewTitles != null)
                        dto.TotalNumberOfNewTitles = entity.TotalNumberOfNewTitles;
                    if (entity.TotalNumberOfTitles != null)
                        dto.TotalNumberOfTitles = entity.TotalNumberOfTitles;

                    #region section and subsection
                    if (detail != default(XsiExhibitionExhibitorDetails))
                    {
                        dto.BoothDetail = detail.BoothDetail;
                        if (detail.BoothSectionId != null)
                        {
                            dto.BoothSectionId = detail.BoothSectionId.Value;

                            if (detail.BoothSubSectionId != null)
                            {
                                dto.BoothSubSectionId = detail.BoothSubSectionId.Value;
                                dto.BoothSubSectionTitle = GetBoothSubSection(detail.BoothSubSectionId.Value, detail.BoothSectionId.Value);
                            }
                        }
                        if (detail.RequiredAreaId != null)
                        {
                            dto.RequiredAreaId = detail.RequiredAreaId.Value;
                            dto.RequiredAreaValue = ExhibitionAreaService.GetExhibitionAreaByItemId(detail.RequiredAreaId.Value).Title;
                        }
                        if (detail.RequiredAreaType != null)
                        {
                            dto.RequiredAreaShapeType = detail.RequiredAreaType;
                            if (detail.RequiredAreaType == EnumConversion.ToString(EnumRequiredAreaType.Open))
                            {

                                if (detail.CalculatedArea != null)
                                {
                                    dto.CalculatedArea = detail.CalculatedArea;
                                    if (!string.IsNullOrEmpty(dto.CalculatedArea))
                                        dto.RequiredAreaMeter = dto.RequiredArea = detail.CalculatedArea.Split('x')[1];
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            return dto;
        }
        private StepFourDTO LoadStepFourContent(XsiInvoice entity, XsiExhibition exhibition, long langid = 1)
        {
            StepFourDTO paymentDTO = new StepFourDTO();
            if (entity != null)
            {
                paymentDTO.InvoiceId = entity.ItemId;

                if (entity.AllocatedSpace != null && !string.IsNullOrEmpty(entity.AllocatedSpace))
                    paymentDTO.AllocatedSpace = entity.AllocatedSpace;

                if (entity.InvoiceNumber != null && !string.IsNullOrEmpty(entity.InvoiceNumber))
                    paymentDTO.InvoiceNumber = entity.InvoiceNumber;

                if (entity.ParticipationFee != null && !string.IsNullOrEmpty(entity.ParticipationFee))
                    if (entity.ParticipationFee.Trim() != "0")
                        paymentDTO.ParticipationFee = MethodFactory.AddCommasToCurrency(entity.ParticipationFee);

                if (entity.StandFee != null && !string.IsNullOrEmpty(entity.StandFee))
                    paymentDTO.StandFee = MethodFactory.AddCommasToCurrency(entity.StandFee);

                if (entity.KnowledgeAndResearchFee != null && !string.IsNullOrEmpty(entity.KnowledgeAndResearchFee))
                    paymentDTO.KnowledgeAndResearchFee = MethodFactory.AddCommasToCurrency(entity.KnowledgeAndResearchFee);

                if (entity.AgencyFee != null && !string.IsNullOrEmpty(entity.AgencyFee))
                    paymentDTO.AgencyFee = MethodFactory.AddCommasToCurrency(entity.AgencyFee);

                if (entity.RepresentativeFee != null && !string.IsNullOrEmpty(entity.RepresentativeFee))
                    paymentDTO.RepresentativeFee = MethodFactory.AddCommasToCurrency(entity.RepresentativeFee);

                if (entity.DueFromLastYearCredit != null && !string.IsNullOrEmpty(entity.DueFromLastYearCredit))
                    paymentDTO.DueFromLastYearCredit = MethodFactory.AddCommasToCurrency(entity.DueFromLastYearCredit);

                if (entity.DueFromLastYearDebit != null && !string.IsNullOrEmpty(entity.DueFromLastYearDebit))
                    paymentDTO.DueFromLastYearDebit = MethodFactory.AddCommasToCurrency(entity.DueFromLastYearDebit);

                if (entity.DueCurrentYear != null && !string.IsNullOrEmpty(entity.DueCurrentYear))
                    paymentDTO.DueCurrentYear = MethodFactory.AddCommasToCurrency(entity.DueCurrentYear);

                if (entity.DebitedCurrentYear != null && !string.IsNullOrEmpty(entity.DebitedCurrentYear))
                    paymentDTO.DebitedCurrentYear = MethodFactory.AddCommasToCurrency(entity.DebitedCurrentYear);

                if (entity.Discount != null && !string.IsNullOrEmpty(entity.Discount))
                    paymentDTO.Discount = MethodFactory.AddCommasToCurrency(entity.Discount);

                if (entity.Penalty != null && !string.IsNullOrEmpty(entity.Penalty))
                    paymentDTO.Penalty = MethodFactory.AddCommasToCurrency(entity.Penalty);

                if (entity.Vat != null && !string.IsNullOrEmpty(entity.Vat))
                    paymentDTO.VAT = MethodFactory.AddCommasToCurrency(entity.Vat);

                if (entity.TotalAed != null && !string.IsNullOrEmpty(entity.TotalAed))
                    paymentDTO.TotalAED = MethodFactory.AddCommasToCurrency(entity.TotalAed);

                if (entity.TotalUsd != null && !string.IsNullOrEmpty(entity.TotalUsd))
                    paymentDTO.TotalUSD = MethodFactory.AddCommasToCurrency(entity.TotalUsd);

                if (entity.FileName != null && !string.IsNullOrEmpty(entity.FileName))
                    paymentDTO.FileName = _appCustomSettings.UploadsCMSPath + "/ExhibitorRegistration/Invoice/" + entity.FileName;

                if (exhibition.DateOfDue != null)
                {
                    if (langid == 1)
                        paymentDTO.ExhibitorLastDate = exhibition.DateOfDue.Value.ToString("dddd d MMMM yyyy");
                    else
                        paymentDTO.ExhibitorLastDate = exhibition.DateOfDue.Value.ToString("dddd d MMMM yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
                    if (exhibition.DateOfDue > MethodFactory.ArabianTimeNow())
                        paymentDTO.RemainingDays = Math.Ceiling(exhibition.DateOfDue.Value.Subtract(MethodFactory.ArabianTimeNow()).TotalDays).ToString();
                    else
                        paymentDTO.RemainingDays = "0 ";
                }
            }
            return paymentDTO;
        }
        private StepFiveDTO LoadStepFiveContent(long exhibitorId, long langid = 1)
        {
            return new StepFiveDTO();
        }
        private StepSixDTO LoadStepSixContent(long exhibitorId, long langid = 1)
        {
            StepSixDTO dto = new StepSixDTO();
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly exhibitor = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                XsiExhibitionExhibitorDetails exhibitordetails = ExhibitorRegistrationService.GetDetailsById(exhibitorId);
                if (exhibitordetails != default(XsiExhibitionExhibitorDetails))
                {
                    #region Section
                    if (exhibitordetails.BoothSectionId != null)
                    {
                        dto.BoothSectionId = exhibitordetails.BoothSectionId.Value;
                        dto.Section = GetBoothSection(exhibitordetails.BoothSectionId.Value);

                        #region SubSection
                        if (exhibitordetails.BoothSubSectionId != null)
                        {
                            dto.SubSection = GetBoothSubSection(exhibitordetails.BoothSubSectionId.Value, exhibitordetails.BoothSectionId.Value);
                        }
                        #endregion
                    }
                    #endregion
                    if (exhibitordetails.AllocatedSpace != null)
                        if (exhibitordetails.AllocatedSpace != string.Empty)
                            dto.Area = exhibitordetails.AllocatedSpace;
                    if (exhibitordetails.RequiredAreaType != null)
                    {
                        //dto.RequiredAreaShapeType = detail.RequiredAreaType;
                        if (exhibitordetails.RequiredAreaType == EnumConversion.ToString(EnumRequiredAreaType.Open))
                            dto.Area = exhibitordetails.AllocatedSpace + " " + exhibitordetails.Area;
                    }
                    if (exhibitordetails.HallNumber != null)
                        if (exhibitordetails.HallNumber != "")
                            dto.HallNumber = exhibitordetails.HallNumber;
                    if (exhibitordetails.StandNumberStart != null && exhibitordetails.StandNumberEnd != null)
                        if (exhibitordetails.StandNumberStart != string.Empty && exhibitordetails.StandNumberEnd != string.Empty)
                            dto.Stand = exhibitordetails.StandNumberEnd + exhibitordetails.StandNumberStart;
                    if (exhibitor != null)
                    {
                        if (exhibitor.UploadLocationMap != null && !string.IsNullOrEmpty(exhibitor.UploadLocationMap))
                        {
                            dto.UploadLocationMap = _appCustomSettings.UploadsCMSPath + "/ExhibitorRegistration/LocationMap/" + exhibitor.UploadLocationMap;
                        }
                    }
                }
            }
            return dto;
        }
        private void UpdateStatusChange(long exhibitorId)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                if (entity != null)
                {
                    entity.IsStatusChanged = EnumConversion.ToString(EnumBool.No);
                    ExhibitorRegistrationService.UpdateExhibitorRegistration(entity);
                }
            }
        }
        protected bool IsTradeLicenceCategorySelected(long categoryId)
        {
            if (categoryId == 21 || categoryId == 23 || categoryId == 25)
                return true;
            else
                return false;
        }
        string GetBoothSubSection(long subsectionId, long sectionId, long langid = 1)
        {
            ExhibitionBoothService ExhibitionBoothService = new ExhibitionBoothService();
            XsiExhibitionBooth whereBoothSection = new XsiExhibitionBooth();
            whereBoothSection.ItemId = sectionId;
            XsiExhibitionBooth ExhibitionBoothEntity = ExhibitionBoothService.GetExhibitionBooth(whereBoothSection, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            if (ExhibitionBoothEntity != null)
            {
                ExhibitionBoothSubsectionService ExhibitionBoothSubsectionService = new ExhibitionBoothSubsectionService();
                XsiExhibitionBoothSubsection whereBoothSubSection = new XsiExhibitionBoothSubsection();
                whereBoothSubSection.ItemId = subsectionId;
                whereBoothSubSection.CategoryId = ExhibitionBoothEntity.ItemId;
                XsiExhibitionBoothSubsection ExhibitionBoothSubSectionEntity = ExhibitionBoothSubsectionService.GetExhibitionBoothSubsection(whereBoothSubSection, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
                if (ExhibitionBoothSubSectionEntity != null)
                    return langid == 1 ? ExhibitionBoothSubSectionEntity.Title : ExhibitionBoothSubSectionEntity.TitleAr;
            }
            return string.Empty;
        }
        string GetBoothSection(long itemId, long langid = 1)
        {
            ExhibitionBoothService ExhibitionBoothService = new ExhibitionBoothService();
            XsiExhibitionBooth whereBoothSection = new XsiExhibitionBooth();
            whereBoothSection.ItemId = itemId;
            XsiExhibitionBooth ExhibitionBoothEntity = ExhibitionBoothService.GetExhibitionBooth(whereBoothSection, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            if (ExhibitionBoothEntity != null)
                return langid == 1 ? ExhibitionBoothEntity.Title : ExhibitionBoothEntity.TitleAr;
            return string.Empty;
        }
        private void LogExhibitorStatus(string status, long exhibitorId)
        {
            using (ExhibitorStatusLogService ExhibitorStatusLogService = new ExhibitorStatusLogService())
            {
                XsiExhibitionMemberApplicationYearlyLogs entity = new XsiExhibitionMemberApplicationYearlyLogs();
                entity.MemberExhibitionYearlyId = exhibitorId;
                entity.Status = status;
                entity.CreatedOn = MethodFactory.ArabianTimeNow();
                ExhibitorStatusLogService.InsertExhibitorStatusLog(entity);
            }
        }
        //private bool IsTradeLicenceValid(StepTwoDTO entity, long langId)
        //{
        //    if (entity.TradeLicenceName != null && entity.TradeLicenceName.Length > 0)
        //    {
        //        string fileName = string.Empty;
        //        byte[] imageBytes;
        //        if (entity.TradeLicenceName.Contains("data:"))
        //        {
        //            var strInfo = entity.TradeLicenceName.Split(",")[0];
        //            imageBytes = Convert.FromBase64String(entity.TradeLicenceName.Split(',')[1]);
        //        }
        //        else
        //        {
        //            imageBytes = Convert.FromBase64String(entity.TradeLicenceName);
        //        }

        //        //fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + entity.TradeLicenceExt);
        //        //if (MethodFactory.IsValidLogo_TradeLicense(fileName))
        //        //{
        //        //    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\TradeLicence\\") + fileName, imageBytes);
        //        //}
        //        //else
        //        //    return false;
        //        return true;
        //    }
        //    else
        //    {
        //        if (IsTradeLicenceCategorySelected(entity.ExhibitionCategoryId) && entity.TradeLicenceName == null)
        //        {
        //            //ShowMessage(Resources.BackendError.UploadTradeLicence, EnumMessageType.Error, lblMessageStep2, pnlErrorMsgStep2, liMessage);
        //            return false;
        //        }
        //        else
        //            return true;
        //    }
        //}
        //private bool IsLogoValid(StepTwoDTO entity, long langId)
        //{
        //    if (entity.LogoName != null && entity.LogoName.Length > 0)
        //    {
        //        string fileName = string.Empty;
        //        byte[] imageBytes;
        //        if (entity.LogoName.Contains("data:"))
        //        {
        //            var strInfo = entity.LogoName.Split(",")[0];
        //            imageBytes = Convert.FromBase64String(entity.LogoName.Split(',')[1]);
        //        }
        //        else
        //        {
        //            imageBytes = Convert.FromBase64String(entity.LogoName);
        //        }

        //        //fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + entity.LogoExt);
        //        //if (MethodFactory.IsValidLogo_TradeLicense(fileName))
        //        //{
        //        //    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\Logo\\") + fileName, imageBytes);
        //        //    return true;
        //        //}
        //    }
        //    return false;
        //}
        //private void MoveToStep3(StepTwoDTO entity, long langId)
        //{
        //    //CreateInterface(EnumRegistrationViewType.Step3);
        //    //DisableStep2Controls();
        //    //dvRequiredAreaContainer.Visible = true;

        //    //if (entity.ExhibitorId == -1)
        //    //{
        //    //    //  ShowStep3Submit();
        //    //    //if (CurrentExhibitorEntity != null && CurrentExhibitorEntity.RequiredAreaGroupId != null)
        //    //    //{
        //    //    //}
        //    //    //else
        //    //    //{

        //    //    //}
        //    //    //    dvRequiredAreaContainer.Visible = false;
        //    //}
        //    //else
        //    //{

        //    //    //if (entity.IsStep2EditClicked)
        //    //    //{
        //    //    //    GetCurrentExhibitorDetails(entity.ExhibitorId);
        //    //    //    EnableStep3Controls();
        //    //    //    ShowStep3Submit();
        //    //    //}
        //    //    //else
        //    //    //{
        //    //    //    LoadStep3();
        //    //    //    DisableStep3Controls();
        //    //    //}
        //    //}
        //    //  HideError(lblMessageStep2, pnlErrorMsgStep2);
        //}
        XsiExhibitionMemberApplicationYearly GetCurrentExhibitorDetails(long exhibitorId)
        {
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                return ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
            }
        }

        ExhibitorReturnDTO ShowMessage(string error, long stepId, long exhibitorId, long memberId, long websiteId, string messagetyperesponse, string dueerror = "")
        {
            ExhibitorReturnDTO model = new ExhibitorReturnDTO();
            model.DueMessage = dueerror;
            model.Message = error;
            model.StepId = stepId;
            model.ExhibitorId = exhibitorId;
            model.MemberId = memberId;
            model.WebsiteId = websiteId;
            model.MessageTypeResponse = messagetyperesponse;
            return model;
        }
        bool IsBooksRequired(long memberId)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                var memberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(memberId);
                if (memberentity != null)
                {
                    return (memberentity.IsBooksRequired == "Y") ? true : false;
                }
            }
            return false;
        }
        bool IsSpaceDependentFieldChanged(XsiExhibitionMemberApplicationYearly currentExhibitorEntity, SaveDataForStepTwoAndThreeDTO newDto)
        {
            XsiExhibitionExhibitorDetails prevDetails = new XsiExhibitionExhibitorDetails();
            if (currentExhibitorEntity.XsiExhibitionExhibitorDetails != default(XsiExhibitionExhibitorDetails) && currentExhibitorEntity.XsiExhibitionExhibitorDetails.ToList().Count > 0)
            {
                prevDetails = currentExhibitorEntity.XsiExhibitionExhibitorDetails.ToList()[0];

                if (prevDetails.BoothSection != null)
                    if (prevDetails.BoothSectionId != newDto.BoothSectionId)
                        return true;
                if (prevDetails.BoothSubSectionId != null)
                    if (prevDetails.BoothSubSectionId != newDto.BoothSubSectionId)
                        return true;
                if (prevDetails.RequiredAreaId != null)
                    if (prevDetails.RequiredAreaId != newDto.RequiredAreaId)
                        return true;
                if (prevDetails.RequiredAreaType != null)
                    if (prevDetails.RequiredAreaType != newDto.RequiredAreaShapeType)
                        return true;
                if (currentExhibitorEntity.PublisherNameAr != null)
                    if (currentExhibitorEntity.PublisherNameAr != newDto.PublisherNameAr)
                        return true;
                if (currentExhibitorEntity.PublisherNameEn != null)
                    if (currentExhibitorEntity.PublisherNameEn != newDto.PublisherNameEn)
                        return true;
            }
            return false;
        }
        bool IsAnyFieldChanged(XsiExhibitionMemberApplicationYearly CurrentExhibitorEntity, SaveDataForStepTwoAndThreeDTO newDto)
        {
            XsiExhibitionExhibitorDetails prevDetails = new XsiExhibitionExhibitorDetails();
            if (CurrentExhibitorEntity.XsiExhibitionExhibitorDetails != null && CurrentExhibitorEntity.XsiExhibitionExhibitorDetails.ToList().Count > 0)
            {
                prevDetails = CurrentExhibitorEntity.XsiExhibitionExhibitorDetails.ToList()[0];

                if (CurrentExhibitorEntity.CountryId != null)
                    if (CurrentExhibitorEntity.CountryId != newDto.CountryId)
                        return true;
                if (CurrentExhibitorEntity.CityId != null)
                    if (CurrentExhibitorEntity.CityId != newDto.CityId)
                        return true;
                if (CurrentExhibitorEntity.ExhibitionCategoryId != null)
                    if (CurrentExhibitorEntity.ExhibitionCategoryId != newDto.ExhibitionCategoryId)
                        return true;
                if (prevDetails.BoothSection != null)
                    if (prevDetails.BoothSectionId != newDto.BoothSectionId)
                        return true;
                if (prevDetails.BoothSubSection != null)
                    if (prevDetails.BoothSubSectionId != newDto.BoothSubSectionId)
                        return true;
                if (prevDetails.RequiredAreaId != null)
                    if (prevDetails.RequiredAreaId != newDto.RequiredAreaId)
                        return true;
                if (prevDetails.RequiredAreaType != null)
                    if (prevDetails.RequiredAreaType != newDto.RequiredAreaShapeType)
                        return true;
                if (CurrentExhibitorEntity.PublisherNameAr != null)
                    if (CurrentExhibitorEntity.PublisherNameAr != newDto.PublisherNameAr.Trim())
                        return true;
                if (CurrentExhibitorEntity.PublisherNameEn != null)
                    if (CurrentExhibitorEntity.PublisherNameEn != newDto.PublisherNameEn.Trim())
                        return true;
                if (CurrentExhibitorEntity.ContactPersonName != null)
                    if (CurrentExhibitorEntity.ContactPersonName != newDto.ContactPersonName.Trim())
                        return true;
                if (CurrentExhibitorEntity.ContactPersonNameAr != null)
                    if (CurrentExhibitorEntity.ContactPersonNameAr != newDto.ContactPersonNameAr.Trim())
                        return true;
                if (CurrentExhibitorEntity.ContactPersonTitle != null)
                    if (CurrentExhibitorEntity.ContactPersonTitle != newDto.ContactPersonTitle.Trim())
                        return true;
                if (CurrentExhibitorEntity.ContactPersonTitleAr != null)
                    if (CurrentExhibitorEntity.ContactPersonTitleAr != newDto.ContactPersonTitleAr.Trim())
                        return true;
                if (CurrentExhibitorEntity.Phone != null)
                    if (CurrentExhibitorEntity.Phone != (newDto.PhoneISD + "$" + newDto.PhoneSTD + "$" + newDto.Phone))
                        return true;
                if (CurrentExhibitorEntity.Fax != null)
                    if (CurrentExhibitorEntity.Fax != (newDto.FaxISD + "$" + newDto.FaxSTD + "$" + newDto.Fax))
                        return true;
                if (CurrentExhibitorEntity.Fax != null)
                    if (CurrentExhibitorEntity.Fax != (newDto.MobileISD + "$" + newDto.MobileSTD + "$" + newDto.Mobile))
                        return true;
                if (CurrentExhibitorEntity.Email != null)
                    if (CurrentExhibitorEntity.Email != newDto.Email.Trim())
                        return true;
                if (CurrentExhibitorEntity.TotalNumberOfNewTitles != null)
                    if (CurrentExhibitorEntity.TotalNumberOfNewTitles != newDto.TotalNumberOfNewTitles.Trim())
                        return true;
                if (CurrentExhibitorEntity.TotalNumberOfTitles != null)
                    if (CurrentExhibitorEntity.TotalNumberOfTitles != newDto.TotalNumberOfTitles.Trim())
                        return true;
            }
            return false;
        }
        private string UploadReceipt(UploadPayment model, long langId)
        {
            using (InvoiceService InvoiceService = new InvoiceService())
            {
                XsiInvoice entity = InvoiceService.GetInvoiceByItemId(model.InvoiceId);
                if (entity != null)
                {
                    if (model.FileBase64 != null && !string.IsNullOrEmpty(model.FileBase64))
                    {
                        string fileName = string.Empty;
                        byte[] imageBytes;
                        if (model.FileBase64.Contains("data:"))
                        {
                            var strInfo = model.FileBase64.Split(",")[0];
                            imageBytes = Convert.FromBase64String(model.FileBase64.Split(',')[1]);
                        }
                        else
                        {
                            imageBytes = Convert.FromBase64String(model.FileBase64);
                        }

                        fileName = string.Format("{0}_{1}{2}", langId, MethodFactory.GetRandomNumber(), "." + model.FileExtension);
                        System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\PaymentReceipt\\") + fileName, imageBytes);
                        // string filename = string.Format("{0}_{1}{2}", EnglishId, MethodFactory.GetRandomNumber(), MethodFactory.GetFileExtention(fuFile.FileName));
                        entity.UploadePaymentReceipt = fileName;

                        entity.NoofParcel = model.NoofParcel;
                        if (model.PaymentTypeId == Convert.ToInt64(EnumPaymentType.PaidByBankTransfer))
                        {
                            if (model.InvoiceNumberFromUser != null)
                                entity.InvoiceNumberFromUser = model.InvoiceNumberFromUser.Trim();
                            if (model.BankName != null)
                                entity.BankName = model.BankName.Trim();
                            entity.ReceiptNumber = string.Empty;
                            if (model.TransferDate != null)
                                entity.TransferDate = model.TransferDate;
                        }
                        else if (model.PaymentTypeId == Convert.ToInt64(EnumPaymentType.PaidByChequeTransfer))
                        {
                            if (model.BankName != null)
                                entity.BankName = model.BankName.Trim();
                            if (model.ReceiptNumber != null)
                                entity.ReceiptNumber = model.ReceiptNumber.Trim();
                        }
                        else if (model.PaymentTypeId == Convert.ToInt64(EnumPaymentType.PaidByCash))
                        {
                            entity.BankName = string.Empty;
                            entity.ReceiptNumber = string.Empty;
                        }
                        entity.Status = EnumConversion.ToString(EnumExhibitorStatus.ReceiptUploaded);
                        entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                        entity.PaymentTypeId = model.PaymentTypeId;
                        if (InvoiceService.UpdateInvoice(entity) == EnumResultType.Success)
                        {
                            var invoiceid = InvoiceService.XsiItemdId;
                            LogInvoiceStatus(EnumConversion.ToString(EnumExhibitorStatus.ReceiptUploaded), model.ExhibitorId, invoiceid);
                            //MVRegistration.SetActiveView(Step5Thankyou);
                            //EnableUpload(entity);
                            XsiExhibitionMemberApplicationYearly exhibitorEntity = new XsiExhibitionMemberApplicationYearly();
                            if (entity.ExhibitorId != null)
                            {
                                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                                exhibitorEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(entity.ExhibitorId.Value);
                                if (exhibitorEntity != null)
                                    SendEmail(exhibitorEntity, exhibitorEntity.MemberId.Value, 1, langId, true);
                            }
                            //PushBankTransferDataToGalaxyService(entity, exhibitorEntity, _appCustomSettings.UploadsCMSPath, 1);
                            if (langId == 1)
                                return "You will be notified via email after the payments are approved by accounting department.";
                            else
                                return "ملاحظة: سيتم إعلامك بعد أن يتم إعتماد المبلغ من قبل قسم الحسابات";
                        }
                        else
                        {
                            return "fail";
                            //EnableUpload(entity);
                            //ShowMessage(Resources.BackendError.InsertFailure, EnumMessageType.Error, lblMessageStep5, pnlErrorMsgStep5, liMessage5);
                        }
                    }
                    else
                    {
                        if (langId == 1)
                            return "Upload receipt missing";
                        else
                            return "Upload receipt missing";
                    }
                }
                return "fail";
            }
        }
        private void LogInvoiceStatus(string status, long ExhibitorId, long InvoiceId)
        {
            using (InvoiceStatusLogService InvoiceStatusLogService = new InvoiceStatusLogService())
            {
                XsiInvoiceStatusLog entity = new XsiInvoiceStatusLog();
                entity.ExhibitorId = ExhibitorId;
                entity.InvoiceId = InvoiceId;
                entity.Status = status;
                entity.CreatedOn = MethodFactory.ArabianTimeNow();
                InvoiceStatusLogService.InsertInvoiceStatusLog(entity);
            }
        }
        void PushBankTransferDataToGalaxyService(XsiInvoice invoiceentity, XsiExhibitionMemberApplicationYearly exhibitorentity, string uploadscmspath, long adminuserid)
        {
            try
            {
                using (InvoiceStatusLogService InvoiceStatusLogService = new InvoiceStatusLogService())
                {
                    GIGEncryption gIGEncryption = new GIGEncryption();
                    GIGEncryptionMobile gIGEncryptionMobile = new GIGEncryptionMobile();
                    //string strtimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");

                    DateTime strtimestamp = DateTime.Now;
                    string pwd = "G@123456";
                    string tokenbeforeencryption = "Myahia&" + pwd + "&" + strtimestamp;



                    var tokenwithencryption = gIGEncryptionMobile.encrypt(tokenbeforeencryption, "BE1A8B181E6ABDE36EBD0276C18AB995", "postbanktransferer");  //PostBankTransferer
                                                                                                                                                             //string strmodel = "PostBankTransfer|" + strtimestamp + "|119602" + "|9999991"+ "|SIBF/21ACW/99999/J1"+"|100"+"|1"+"|abc bank"+ "|01-01-2021"+ "|RCPT12345";
                                                                                                                                                             //PostBankTransferer|01-01-2021|123|1|123|1256|1|testBank|01-01-2021 | AE00001232 | testFile
                                                                                                                                                             //SIBF/21ACW/999999/E1

                    #region Fields
                    string strreceiptnumber = "-";
                    string strpaidamount = "0";
                    string strbankname = "-";
                    string strinvoicenumber = "-";
                    string strtransferdate = "-";
                    string strpaidamt = "0";
                    string strpaymenttype = "-";
                    string struploadpaymentreceipt = "-";
                    //3CDBBC4DE6AB3ACCD53BDA60E8E055D2

                    dynamic myObject1 = JsonConvert.DeserializeObject<dynamic>("\"3CDBBC4DE6AB3ACCD53BDA60E8E055D2\"");
                    var strdecrypt1 = gIGEncryption.Decrypt(myObject1, "BE1A8B181E6ABDE36EBD0276C18AB995");

                    if (invoiceentity != null)
                    {
                        if (invoiceentity.PaidAmount != null && !string.IsNullOrEmpty(invoiceentity.PaidAmount))
                            strpaidamount = invoiceentity.PaidAmount;

                        if (invoiceentity.InvoiceNumber != null && !string.IsNullOrEmpty(invoiceentity.InvoiceNumber))
                            strinvoicenumber = invoiceentity.InvoiceNumber;

                        if (invoiceentity.PaidAmount != null && !string.IsNullOrEmpty(invoiceentity.PaidAmount))
                            strpaidamt = invoiceentity.PaidAmount;

                        if (invoiceentity.PaymentTypeId != null)
                            strpaymenttype = invoiceentity.PaymentTypeId.ToString();

                        if (invoiceentity.BankName != null && !string.IsNullOrEmpty(invoiceentity.BankName))
                            strbankname = invoiceentity.BankName;

                        if (invoiceentity.TransferDate != null)
                            strtransferdate = invoiceentity.TransferDate.Value.ToString("dd/MM/yyyy");

                        if (invoiceentity.ReceiptNumber != null && !string.IsNullOrEmpty(invoiceentity.ReceiptNumber))
                            strreceiptnumber = invoiceentity.ReceiptNumber;

                        if (invoiceentity.UploadePaymentReceipt != null && !string.IsNullOrEmpty(invoiceentity.UploadePaymentReceipt))
                            struploadpaymentreceipt = uploadscmspath + "/ExhibitorRegistration/PaymentReceipt/" + invoiceentity.UploadePaymentReceipt;
                    }
                    #endregion
                    // string strmodel = "PostBankTransfer|" + strtimestamp + "|120851" + "|99999" + "|SIBF21ACW99999E1" + "|100" + "|1" + "|xyz bank" + "|01-01-2021" + "|RCPT12345" + "|abcRCPT12345.jpg";

                    string strmodel = "PostBankTransfer|" + strtimestamp + "|" + invoiceentity.ItemId + "|" + exhibitorentity.FileNumber + "|" + strinvoicenumber + "|" + strpaidamt + "|" + strpaymenttype + "|" + strbankname + "|" + strtransferdate + "|" + strreceiptnumber + "|" + struploadpaymentreceipt;

                    var strmodelencrypt = gIGEncryption.Encrypt(strmodel, "BE1A8B181E6ABDE36EBD0276C18AB995");

                    string url = _appCustomSettings.RevenueSystemBillingAPI + "api/PostBankTransfer/" + tokenwithencryption + "?model=" + strmodelencrypt;

                    WebClient client = new WebClient();
                    client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                    Stream data = client.OpenRead(url);
                    StreamReader reader = new StreamReader(data);
                    var responseFromServer = reader.ReadToEnd();
                    dynamic myObject = JsonConvert.DeserializeObject<dynamic>(responseFromServer);
                    var strdecrypt = gIGEncryption.Decrypt(myObject, "BE1A8B181E6ABDE36EBD0276C18AB995");
                    data.Close();
                    reader.Close();
                    var arr = strdecrypt.Split('|');
                    if (arr[1] == "000" || arr[1] == "101")
                    {
                        XsiInvoiceStatusLog log = new XsiInvoiceStatusLog();

                        log.InvoiceId = invoiceentity.ItemId;
                        log.ExhibitorId = invoiceentity.ExhibitorId;
                        log.AdminId = adminuserid;
                        log.GalaxyTeamId = Convert.ToInt64(arr[0]);
                        log.Status = invoiceentity.Status;
                        log.CreatedOn = MethodFactory.ArabianTimeNow();
                        InvoiceStatusLogService.InsertInvoiceStatusLog(log);
                    }
                    //return strdecrypt;
                }
            }
            catch (Exception ex)
            {
                // LogException(ex);
            }
        }
        void UpdateExhibitionMemberCompanyName(XsiExhibitionMemberApplicationYearly entity)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                XsiExhibitionMember member = ExhibitionMemberService.GetExhibitionMemberByItemId(entity.MemberId.Value);
                if (member != null)
                {
                    if (member.CompanyName != null && entity.PublisherNameEn != null)
                    {
                        if (!member.CompanyName.Equals(entity.PublisherNameEn))
                        {
                            member.CompanyName = entity.PublisherNameEn;
                            ExhibitionMemberService.UpdateExhibitionMember(member);
                        }
                    }
                    else
                    {
                        if (entity.PublisherNameEn != null)
                        {
                            member.CompanyName = entity.PublisherNameEn;
                            ExhibitionMemberService.UpdateExhibitionMember(member);
                        }
                    }


                    if (member.CompanyNameAr != null && entity.PublisherNameAr != null)
                    {
                        if (!member.CompanyNameAr.Equals(entity.PublisherNameAr))
                        {
                            member.CompanyNameAr = entity.PublisherNameAr;
                            ExhibitionMemberService.UpdateExhibitionMember(member);
                        }
                    }
                    else
                    {
                        if (entity.PublisherNameAr != null)
                        {
                            member.CompanyNameAr = entity.PublisherNameAr;
                            ExhibitionMemberService.UpdateExhibitionMember(member);
                        }
                    }
                    if (entity.XsiExhibitionExhibitorDetails != default(XsiExhibitionExhibitorDetails) && entity.XsiExhibitionExhibitorDetails.ToList().Count > 0)
                    {
                        if (entity.XsiExhibitionExhibitorDetails.ToList()[0].Brief != null)
                        {
                            member.Brief = entity.XsiExhibitionExhibitorDetails.ToList()[0].Brief;
                            ExhibitionMemberService.UpdateExhibitionMember(member);
                        }
                        if (entity.XsiExhibitionExhibitorDetails.ToList()[0].BriefAr != null)
                        {
                            member.BriefAr = entity.XsiExhibitionExhibitorDetails.ToList()[0].BriefAr;
                            ExhibitionMemberService.UpdateExhibitionMember(member);
                        }
                    }
                }
            }
        }
        #endregion

        #region Email
        void NotifyAdminandUserAboutEmailChange(long exhibitorId, string oldemail, string newemail, long memberId, long websiteId)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                XsiExhibitionMember memberentity = new XsiExhibitionMember();
                memberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(Convert.ToInt64(memberId));
                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                XsiExhibitionMemberApplicationYearly ExhibitorEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                if (memberentity != default(XsiExhibitionMember) && ExhibitorEntity != default(XsiExhibitionMemberApplicationYearly))
                {
                    string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {
                        #region Send Admin Email Notifying about Email Change
                        StringBuilder body = new StringBuilder();
                        #region Email Content
                        SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                        if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                        {
                            var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(20069);
                            if (scrfEmailContent != null)
                            {
                                if (scrfEmailContent.Body != null)
                                    strEmailContentBody = scrfEmailContent.Body;
                                if (scrfEmailContent.Email != null && !string.IsNullOrEmpty(scrfEmailContent.Email))
                                    strEmailContentEmail = scrfEmailContent.Email;
                                else
                                    strEmailContentEmail = _appCustomSettings.AdminEmail;
                                if (scrfEmailContent.Subject != null)
                                    strEmailContentSubject = scrfEmailContent.Subject;
                                strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                            }
                        }
                        else
                        {
                            var emailContent = EmailContentService.GetEmailContentByItemId(20061);
                            if (emailContent != null)
                            {
                                if (emailContent.Body != null)
                                    strEmailContentBody = emailContent.Body;
                                if (emailContent.Email != null && !string.IsNullOrEmpty(emailContent.Email))
                                    strEmailContentEmail = emailContent.Email;
                                else
                                    strEmailContentEmail = _appCustomSettings.AdminEmail;
                                if (emailContent.Subject != null)
                                    strEmailContentSubject = emailContent.Subject;
                                strEmailContentAdmin = _appCustomSettings.AdminName;
                            }
                        }
                        #endregion
                        strEmailContentBody = strEmailContentBody.Replace("$$NewEmail$$", newemail)
                                                                       .Replace("$$OldEmail$$", oldemail);
                        if (ExhibitorEntity.PublisherNameEn != null)
                            strEmailContentBody = strEmailContentBody.Replace("$$ExhibitorNameEn$$", ExhibitorEntity.PublisherNameEn);
                        if (ExhibitorEntity.FileNumber != null)
                            strEmailContentBody = strEmailContentBody.Replace("$$FileNumber$$", ExhibitorEntity.FileNumber);
                        body.Append(htmlContentFactory.BindEmailContent(memberentity.LanguageUrl.Value, strEmailContentBody, memberentity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                        _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString().Replace("$$SpecificName$$", strEmailContentAdmin));
                        if (memberentity != default(XsiExhibitionMember))
                        {
                            string userbody = body.ToString();
                            if (memberentity.FirstNameEmail != null && memberentity.LastNameEmail != null)
                                userbody = userbody.Replace("$$SpecificName$$", memberentity.FirstNameEmail + " " + memberentity.LastNameEmail);
                            else if (memberentity.Firstname != null)
                                userbody = userbody.Replace("$$SpecificName$$", memberentity.FirstNameEmail);
                            else if (memberentity.LastName != null)
                                userbody = userbody.Replace("$$SpecificName$$", memberentity.LastNameEmail);
                            if (ExhibitorEntity.Email != memberentity.Email)
                                _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, ExhibitorEntity.Email + "," + memberentity.Email, strEmailContentSubject, userbody);
                            else
                                _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, memberentity.Email, strEmailContentSubject, userbody);
                        }
                        #endregion
                    }
                }
            }
        }
        void SendEmail(XsiExhibitionMemberApplicationYearly ExhibitorEntity, long memberId, long websiteId, long langId, bool isReceipt = false)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                    XsiExhibitionMember memberentity = new XsiExhibitionMember();
                    memberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(Convert.ToInt64(memberId));
                    if (memberentity != default(XsiExhibitionMember))
                    {
                        using (EmailContentService EmailContentService = new EmailContentService())
                        {
                            string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";
                            if (ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.New))
                            {
                                #region Send Thankyou Email
                                StringBuilder body = new StringBuilder();
                                #region Email Content
                                SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                                {
                                    var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(3);
                                    if (scrfEmailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (scrfEmailContent.Body != null)
                                                strEmailContentBody = scrfEmailContent.Body;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.Subject != null)
                                                strEmailContentSubject = scrfEmailContent.Subject;
                                        }
                                        else
                                        {
                                            if (scrfEmailContent.BodyAr != null)
                                                strEmailContentBody = scrfEmailContent.BodyAr;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.SubjectAr != null)
                                                strEmailContentSubject = scrfEmailContent.SubjectAr;
                                        }

                                        strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                    }
                                }
                                else
                                {
                                    var emailContent = EmailContentService.GetEmailContentByItemId(3);
                                    if (emailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (emailContent.Body != null)
                                                strEmailContentBody = emailContent.Body;
                                            if (emailContent.Email != null)
                                                strEmailContentEmail = emailContent.Email;
                                            if (emailContent.Subject != null)
                                                strEmailContentSubject = emailContent.Subject;
                                        }
                                        else
                                        {
                                            if (emailContent.BodyAr != null)
                                                strEmailContentBody = emailContent.BodyAr;
                                            if (emailContent.Email != null)
                                                strEmailContentEmail = emailContent.Email;
                                            if (emailContent.SubjectAr != null)
                                                strEmailContentSubject = emailContent.SubjectAr;
                                        }

                                        strEmailContentAdmin = _appCustomSettings.AdminName;
                                    }
                                }
                                #endregion
                                body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, memberentity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                                if (ExhibitorEntity.Email != memberentity.Email)
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, ExhibitorEntity.Email + "," + memberentity.Email, strEmailContentSubject, body.ToString());
                                else
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, memberentity.Email, strEmailContentSubject, body.ToString());
                                #endregion

                                if (memberentity.FileNumber == "new" || memberentity.FileNumber == "New-MM" || memberentity.FileNumber == "new - MM" || memberentity.FileNumber == "new- MM" || memberentity.FileNumber == "new -MM")
                                {
                                    #region Send email for first time users
                                    body = new StringBuilder();
                                    #region Email Content
                                    SCRFEmailContentService = new SCRFEmailContentService();
                                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                                    {
                                        var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(49);
                                        if (scrfEmailContent != null)
                                        {
                                            if (langId == 1)
                                            {
                                                if (scrfEmailContent.Body != null)
                                                    strEmailContentBody = scrfEmailContent.Body;
                                                if (scrfEmailContent.Email != null)
                                                    strEmailContentEmail = scrfEmailContent.Email;
                                                if (scrfEmailContent.Subject != null)
                                                    strEmailContentSubject = scrfEmailContent.Subject;
                                            }
                                            else
                                            {
                                                if (scrfEmailContent.BodyAr != null)
                                                    strEmailContentBody = scrfEmailContent.BodyAr;
                                                if (scrfEmailContent.Email != null)
                                                    strEmailContentEmail = scrfEmailContent.Email;
                                                if (scrfEmailContent.SubjectAr != null)
                                                    strEmailContentSubject = scrfEmailContent.SubjectAr;
                                            }

                                            strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                        }
                                    }
                                    else
                                    {
                                        var emailContent = EmailContentService.GetEmailContentByItemId(49);
                                        if (emailContent != null)
                                        {
                                            if (langId == 1)
                                            {
                                                if (emailContent.Body != null)
                                                    strEmailContentBody = emailContent.Body;
                                                if (emailContent.Email != null)
                                                    strEmailContentEmail = emailContent.Email;
                                                if (emailContent.Subject != null)
                                                    strEmailContentSubject = emailContent.Subject;
                                            }
                                            else
                                            {
                                                if (emailContent.BodyAr != null)
                                                    strEmailContentBody = emailContent.BodyAr;
                                                if (emailContent.Email != null)
                                                    strEmailContentEmail = emailContent.Email;
                                                if (emailContent.SubjectAr != null)
                                                    strEmailContentSubject = emailContent.SubjectAr;
                                            }

                                            strEmailContentAdmin = _appCustomSettings.AdminName;
                                        }
                                    }
                                    #endregion
                                    body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, memberentity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                                    if (ExhibitorEntity.Email != memberentity.Email)
                                        _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, ExhibitorEntity.Email + "," + memberentity.Email, strEmailContentSubject, body.ToString());
                                    else
                                        _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, memberentity.Email, strEmailContentSubject, body.ToString());
                                    #endregion
                                }
                            }
                            else if (ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.Cancelled))
                            {
                                #region Send Cancellation Email
                                StringBuilder body = new StringBuilder();
                                #region Email Content
                                SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                                {
                                    var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(55);
                                    if (scrfEmailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (scrfEmailContent.Body != null)
                                                strEmailContentBody = scrfEmailContent.Body;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.Subject != null)
                                                strEmailContentSubject = scrfEmailContent.Subject;
                                        }
                                        else
                                        {
                                            if (scrfEmailContent.BodyAr != null)
                                                strEmailContentBody = scrfEmailContent.BodyAr;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.SubjectAr != null)
                                                strEmailContentSubject = scrfEmailContent.SubjectAr;
                                        }

                                        strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                    }
                                }
                                else
                                {
                                    var emailContent1 = EmailContentService.GetEmailContentByItemId(55);
                                    if (emailContent1 != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (emailContent1.Body != null)
                                                strEmailContentBody = emailContent1.Body;
                                            if (emailContent1.Email != null)
                                                strEmailContentEmail = emailContent1.Email;
                                            if (emailContent1.Subject != null)
                                                strEmailContentSubject = emailContent1.Subject;
                                        }
                                        else
                                        {
                                            if (emailContent1.BodyAr != null)
                                                strEmailContentBody = emailContent1.BodyAr;
                                            if (emailContent1.Email != null)
                                                strEmailContentEmail = emailContent1.Email;
                                            if (emailContent1.SubjectAr != null)
                                                strEmailContentSubject = emailContent1.SubjectAr;
                                        }

                                        strEmailContentAdmin = _appCustomSettings.AdminName;
                                    }
                                }
                                #endregion
                                body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, memberentity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                                if (ExhibitorEntity.Email != memberentity.Email)
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, ExhibitorEntity.Email + "," + memberentity.Email, strEmailContentSubject, body.ToString());
                                else
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, memberentity.Email, strEmailContentSubject, body.ToString());

                                #region Notify admin about exhibitor cancellation
                                body = new StringBuilder();
                                var emailContent = EmailContentService.GetEmailContentByItemId(20113);
                                if (emailContent != null)
                                {
                                    body.Append(htmlContentFactory.BindEmailContent(langId, emailContent.Body, memberentity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));

                                    if (ExhibitorEntity.PublisherNameEn != null)
                                        body.Replace("$$ExhibitorName$$", ExhibitorEntity.PublisherNameEn);
                                    else
                                        body.Replace("$$ExhibitorName$$", "");

                                    body.Replace("$$FileNumber$$", ExhibitorEntity.FileNumber);

                                    if (!string.IsNullOrEmpty(emailContent.Email))
                                        _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, _appCustomSettings.AccountsEmail + "," + emailContent.Email, emailContent.Subject, body.ToString());
                                    else
                                        _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, _appCustomSettings.AccountsEmail, emailContent.Subject, body.ToString());
                                }
                                #endregion

                                #endregion
                            }
                            else if (ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved))
                            {
                                #region Send Approved Email
                                StringBuilder body = new StringBuilder();
                                #region Email Content
                                SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                                {
                                    var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(57);
                                    if (scrfEmailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (scrfEmailContent.Body != null)
                                                strEmailContentBody = scrfEmailContent.Body;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.Subject != null)
                                                strEmailContentSubject = scrfEmailContent.Subject;
                                        }
                                        else
                                        {
                                            if (scrfEmailContent.BodyAr != null)
                                                strEmailContentBody = scrfEmailContent.BodyAr;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.SubjectAr != null)
                                                strEmailContentSubject = scrfEmailContent.SubjectAr;
                                        }

                                        strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                    }
                                }
                                else
                                {
                                    var emailContent = EmailContentService.GetEmailContentByItemId(57);
                                    if (emailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (emailContent.Body != null)
                                                strEmailContentBody = emailContent.Body;
                                            if (emailContent.Email != null)
                                                strEmailContentEmail = emailContent.Email;
                                            if (emailContent.Subject != null)
                                                strEmailContentSubject = emailContent.Subject;
                                        }
                                        else
                                        {
                                            if (emailContent.BodyAr != null)
                                                strEmailContentBody = emailContent.BodyAr;
                                            if (emailContent.Email != null)
                                                strEmailContentEmail = emailContent.Email;
                                            if (emailContent.SubjectAr != null)
                                                strEmailContentSubject = emailContent.SubjectAr;
                                        }

                                        strEmailContentAdmin = _appCustomSettings.AdminName;
                                    }
                                }
                                #endregion
                                List<string> MailAttachments = new List<string>();
                                if (!string.IsNullOrEmpty(ExhibitorEntity.UploadLocationMap))
                                    MailAttachments.Add(string.Format("{0}{1}", _appCustomSettings.UploadsPath + "ExhibitorRegistration/LocationMap/", ExhibitorEntity.UploadLocationMap));

                                body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, memberentity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                                if (ExhibitorEntity.XsiExhibitionExhibitorDetails != null && ExhibitorEntity.XsiExhibitionExhibitorDetails.ToList().Count > 0)
                                {
                                    var details = ExhibitorEntity.XsiExhibitionExhibitorDetails.ToList()[0];
                                    if (details.BoothSectionId != null)
                                    {
                                        ExhibitionBoothService ExhibitionBoothService = new ExhibitionBoothService();
                                        var exhibitionboothEntity = ExhibitionBoothService.GetExhibitionBoothByItemId(details.BoothSectionId.Value);
                                        body.Replace("$$BoothSection$$", GetBoothSection(details.BoothSectionId.Value));
                                        if (details.BoothSubSectionId != null)
                                            body.Replace("$$BoothSubSection$$", GetBoothSubSection(exhibitionboothEntity.ItemId, details.BoothSubSectionId.Value));
                                    }
                                    body.Replace("$$Area$$", details.AllocatedSpace);
                                    body.Replace("$$AreaShape$$", MethodFactory.GetAreaShape(details.AllocatedAreaType, langId));
                                    if (details.AllocatedAreaType != "O")
                                        body.Replace("$$display$$", "display:none");
                                    else
                                        body.Replace("$$display$$", string.Empty);
                                    body.Replace("$$HallNumber$$", details.HallNumber);
                                    //  body.Replace("$$BoothNumber$$", details.StandNumberEnd + details.StandNumberStart);
                                    if (!string.IsNullOrWhiteSpace(details.StandNumberEnd) && !string.IsNullOrWhiteSpace(details.StandNumberStart))
                                        body.Replace("$$BoothNumber$$", details.StandNumberEnd + "-" + details.StandNumberStart);
                                    else if (!string.IsNullOrWhiteSpace(details.StandNumberEnd))
                                        body.Replace("$$BoothNumber$$", details.StandNumberEnd);
                                    else if (!string.IsNullOrWhiteSpace(details.StandNumberStart))
                                        body.Replace("$$BoothNumber$$", details.StandNumberStart);

                                    body.Replace("$$CalculatedAllocatedArea$$", details.Area);
                                }

                                if (ExhibitorEntity.Email != memberentity.Email.Trim())
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, ExhibitorEntity.Email + "," + memberentity.Email, strEmailContentSubject, body.ToString(), MailAttachments);
                                else
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, memberentity.Email, strEmailContentSubject, body.ToString(), MailAttachments);

                                /*if (XsiResult == EnumResultType.Failed)
                                    XsiResult = EnumResultType.Exception;
                                else
                                    XsiResult = EnumResultType.Success;*/
                                #endregion
                            }
                            else if (isReceipt)
                            {
                                #region Send Receipt Uploaded email to accountant
                                string ToEmail = _appCustomSettings.AccountsEmail;
                                StringBuilder body = new StringBuilder();
                                #region Email Content
                                SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                                {
                                    var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(20081);
                                    if (scrfEmailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (scrfEmailContent.Body != null)
                                                strEmailContentBody = scrfEmailContent.Body;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.Subject != null)
                                                strEmailContentSubject = scrfEmailContent.Subject;
                                        }
                                        else
                                        {
                                            if (scrfEmailContent.BodyAr != null)
                                                strEmailContentBody = scrfEmailContent.BodyAr;
                                            if (scrfEmailContent.Email != null)
                                                strEmailContentEmail = scrfEmailContent.Email;
                                            if (scrfEmailContent.SubjectAr != null)
                                                strEmailContentSubject = scrfEmailContent.SubjectAr;
                                        }
                                        strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                    }
                                }
                                else
                                {
                                    var emailContent = EmailContentService.GetEmailContentByItemId(20095);
                                    if (emailContent != null)
                                    {
                                        if (langId == 1)
                                        {
                                            if (emailContent.Body != null)
                                                strEmailContentBody = emailContent.Body;
                                            if (emailContent.Email != null)
                                                strEmailContentEmail = emailContent.Email;
                                            if (emailContent.Subject != null)
                                                strEmailContentSubject = emailContent.Subject;
                                        }
                                        else
                                        {
                                            if (emailContent.BodyAr != null)
                                                strEmailContentBody = emailContent.BodyAr;
                                            if (emailContent.Email != null)
                                                strEmailContentEmail = emailContent.Email;
                                            if (emailContent.SubjectAr != null)
                                                strEmailContentSubject = emailContent.SubjectAr;
                                        }
                                        strEmailContentAdmin = _appCustomSettings.AdminName;
                                    }
                                }
                                #endregion
                                body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, memberentity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                                if (langId == 1)
                                    if (ExhibitorEntity.PublisherNameEn != null)
                                        body.Replace("$$ExhibitorName$$", ExhibitorEntity.PublisherNameEn);
                                    else
                                        body.Replace("$$ExhibitorName$$", "");
                                else
                                        if (ExhibitorEntity.PublisherNameAr != null)
                                    body.Replace("$$ExhibitorName$$", ExhibitorEntity.PublisherNameAr);
                                else
                                    body.Replace("$$ExhibitorName$$", "");
                                if (langId == 1)
                                    if (ExhibitorEntity.ContactPersonName != null)
                                        body.Replace("$$ContactPerson$$", ExhibitorEntity.ContactPersonName);
                                    else
                                        body.Replace("$$ContactPerson$$", "");
                                else
                                    if (ExhibitorEntity.ContactPersonNameAr != null)
                                    body.Replace("$$ContactPerson$$", ExhibitorEntity.ContactPersonNameAr);
                                else
                                    body.Replace("$$ContactPerson$$", "");
                                body.Replace("$$FileNumber$$", ExhibitorEntity.FileNumber);

                                if (!string.IsNullOrEmpty(strEmailContentEmail))
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString());
                                else
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, ToEmail, strEmailContentSubject, body.ToString());

                                #endregion
                            }
                        }
                    }
                }
            }
        }
        void SendEmailBasedOnEdit(XsiExhibitionMemberApplicationYearly CurrentExhibitorEntity, long memberId, long websiteId, long exhibitorId, long langId)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                XsiExhibitionMember ExhibitionMemberEntity = new XsiExhibitionMember();
                ExhibitionMemberEntity = ExhibitionMemberService.GetExhibitionMemberByItemId(Convert.ToInt64(memberId));
                if (ExhibitionMemberEntity != default(XsiExhibitionMember))
                {

                    #region Send Edit Request Email
                    StringBuilder body = new StringBuilder();
                    #region Email Content
                    EmailContentService EmailContentService = new EmailContentService();
                    SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(10003);
                        if (scrfEmailContent != null)
                        {
                            if (langId == 1)
                            {
                                if (scrfEmailContent.Body != null)
                                    strEmailContentBody = scrfEmailContent.Body;
                                if (scrfEmailContent.Email != null)
                                    strEmailContentEmail = scrfEmailContent.Email;
                                if (scrfEmailContent.Subject != null)
                                    strEmailContentSubject = scrfEmailContent.Subject;
                            }
                            else
                            {
                                if (scrfEmailContent.BodyAr != null)
                                    strEmailContentBody = scrfEmailContent.BodyAr;
                                if (scrfEmailContent.Email != null)
                                    strEmailContentEmail = scrfEmailContent.Email;
                                if (scrfEmailContent.SubjectAr != null)
                                    strEmailContentSubject = scrfEmailContent.SubjectAr;
                            }
                            strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                        }
                    }
                    else
                    {
                        var emailContent = EmailContentService.GetEmailContentByItemId(10003);
                        if (emailContent != null)
                        {
                            if (langId == 1)
                            {
                                if (emailContent.Body != null)
                                    strEmailContentBody = emailContent.Body;
                                if (emailContent.Email != null)
                                    strEmailContentEmail = emailContent.Email;
                                if (emailContent.Subject != null)
                                    strEmailContentSubject = emailContent.Subject;
                            }
                            else
                            {
                                if (emailContent.BodyAr != null)
                                    strEmailContentBody = emailContent.BodyAr;
                                if (emailContent.Email != null)
                                    strEmailContentEmail = emailContent.Email;
                                if (emailContent.SubjectAr != null)
                                    strEmailContentSubject = emailContent.SubjectAr;
                            }
                            strEmailContentAdmin = _appCustomSettings.AdminName;
                        }
                    }
                    #endregion
                    body.Append(htmlContentFactory.BindEmailContent(ExhibitionMemberEntity.LanguageUrl.Value, strEmailContentBody, ExhibitionMemberEntity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                    string strContent = "<p style=\"font-size: 16px; line-height: 32px; font-family: Arial, Helvetica, sans-serif; color: #555555; margin: 0px;\">$$Label$$ : $$Value$$</p>";
                    using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
                    {
                        XsiExhibitionMemberApplicationYearly entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                        if (entity != null)
                        {
                            #region Body
                            bool IsAnyFieldChanged = false;
                            if (entity.PublisherNameEn != null)
                                body.Replace("$$ExhibitorNameEn$$", entity.PublisherNameEn);
                            if (entity.FileNumber != null)
                                body.Replace("$$FileNumber$$", entity.FileNumber);
                            #region Publisher English
                            if (CurrentExhibitorEntity.PublisherNameEn != entity.PublisherNameEn)
                            {
                                if (entity.PublisherNameEn != null)
                                {
                                    body.Replace("$$NewPublisherNameEn$$", strContent);
                                    body.Replace("$$Label$$", "Publisher Name in English");
                                    body.Replace("$$Value$$", entity.PublisherNameEn);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewPublisherNameEn$$", string.Empty);
                                if (CurrentExhibitorEntity.PublisherNameEn != null)
                                {
                                    body.Replace("$$OldPublisherNameEn$$", strContent);
                                    body.Replace("$$Label$$", "Publisher Name in English");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.PublisherNameEn);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldPublisherNameEn$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewPublisherNameEn$$", string.Empty);
                                body.Replace("$$OldPublisherNameEn$$", string.Empty);
                            }
                            #endregion
                            #region Publisher Arabic
                            if (CurrentExhibitorEntity.PublisherNameAr != entity.PublisherNameAr)
                            {
                                if (entity.PublisherNameAr != null)
                                {
                                    body.Replace("$$NewPublisherNameAr$$", strContent);
                                    body.Replace("$$Label$$", "Publisher Name in Arabic");
                                    body.Replace("$$Value$$", entity.PublisherNameAr);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewPublisherNameAr$$", string.Empty);
                                if (CurrentExhibitorEntity.PublisherNameAr != null)
                                {
                                    body.Replace("$$OldPublisherNameAr$$", strContent);
                                    body.Replace("$$Label$$", "Publisher Name in Arabic");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.PublisherNameAr);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldPublisherNameAr$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewPublisherNameAr$$", string.Empty);
                                body.Replace("$$OldPublisherNameAr$$", string.Empty);
                            }
                            #endregion
                            #region Stand Section
                            XsiExhibitionExhibitorDetails prevDetails = new XsiExhibitionExhibitorDetails();
                            if (CurrentExhibitorEntity.XsiExhibitionExhibitorDetails != null && CurrentExhibitorEntity.XsiExhibitionExhibitorDetails.ToList().Count > 0)
                            {
                                prevDetails = CurrentExhibitorEntity.XsiExhibitionExhibitorDetails.ToList()[0];
                            }
                            XsiExhibitionExhibitorDetails currDetails = new XsiExhibitionExhibitorDetails();
                            if (entity.XsiExhibitionExhibitorDetails != null && entity.XsiExhibitionExhibitorDetails.ToList().Count > 0)
                            {
                                currDetails = entity.XsiExhibitionExhibitorDetails.ToList()[0];
                            }
                            if (prevDetails.BoothSectionId != currDetails.BoothSectionId)
                            {
                                if (currDetails.BoothSectionId != null)
                                {
                                    body.Replace("$$NewStandSection$$", strContent);
                                    body.Replace("$$Label$$", "Stand Section");
                                    body.Replace("$$Value$$", GetBoothSection(currDetails.BoothSectionId.Value));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewStandSection$$", string.Empty);
                                if (prevDetails.BoothSectionId != null)
                                {
                                    body.Replace("$$OldStandSection$$", strContent);
                                    body.Replace("$$Label$$", "Stand Section");
                                    body.Replace("$$Value$$", prevDetails.BoothSectionId.ToString());
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldStandSection$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewStandSection$$", string.Empty);
                                body.Replace("$$OldStandSection$$", string.Empty);
                            }
                            #endregion
                            #region Stand Sub Section
                            if (prevDetails.BoothSubSectionId != currDetails.BoothSubSectionId)
                            {
                                if (currDetails.BoothSubSectionId != null)
                                {
                                    body.Replace("$$NewStandSubSection$$", strContent);
                                    body.Replace("$$Label$$", "Stand Sub Section");
                                    body.Replace("$$Value$$", GetBoothSubSection(currDetails.BoothSubSectionId.Value, currDetails.BoothSectionId.Value));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewStandSubSection$$", string.Empty);
                                if (prevDetails.BoothSubSectionId != null)
                                {
                                    body.Replace("$$OldStandSubSection$$", strContent);
                                    body.Replace("$$Label$$", "Stand Sub Section");
                                    body.Replace("$$Value$$", GetBoothSubSection(prevDetails.BoothSubSectionId.Value, prevDetails.BoothSectionId.Value));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldStandSubSection$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewStandSubSection$$", string.Empty);
                                body.Replace("$$OldStandSubSection$$", string.Empty);
                            }
                            #endregion
                            #region Required Area
                            if (prevDetails.RequiredAreaId != currDetails.RequiredAreaId)
                            {
                                if (currDetails.RequiredAreaId != null)
                                {
                                    body.Replace("$$NewRequiredArea$$", strContent);
                                    body.Replace("$$Label$$", "Required Area");
                                    if (MethodFactory.BindExhibitionArea(currDetails.RequiredAreaId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.BindExhibitionArea(currDetails.RequiredAreaId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$NewRequiredArea$$", string.Empty);
                                }
                                else
                                    body.Replace("$$NewRequiredArea$$", string.Empty);
                                if (prevDetails.RequiredAreaId != null)
                                {
                                    body.Replace("$$OldRequiredArea$$", strContent);
                                    body.Replace("$$Label$$", "Required Area");
                                    if (MethodFactory.BindExhibitionArea(currDetails.RequiredAreaId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.BindExhibitionArea(currDetails.RequiredAreaId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$OldRequiredArea$$", string.Empty);
                                }
                                else
                                    body.Replace("$$OldRequiredArea$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewRequiredArea$$", string.Empty);
                                body.Replace("$$OldRequiredArea$$", string.Empty);
                            }
                            #endregion
                            #region Calculated Area
                            if (prevDetails.CalculatedArea != currDetails.CalculatedArea)
                            {
                                if (currDetails.CalculatedArea != null)
                                {
                                    body.Replace("$$NewCalculatedArea$$", strContent);
                                    body.Replace("$$Label$$", "Calculated Area");
                                    body.Replace("$$Value$$", currDetails.CalculatedArea);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewCalculatedArea$$", string.Empty);
                                if (prevDetails.CalculatedArea != null)
                                {
                                    body.Replace("$$OldCalculatedArea$$", strContent);
                                    body.Replace("$$Label$$", "Calculated Area");
                                    body.Replace("$$Value$$", prevDetails.CalculatedArea);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldCalculatedArea$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewCalculatedArea$$", string.Empty);
                                body.Replace("$$OldCalculatedArea$$", string.Empty);
                            }
                            #endregion
                            #region Area Shape
                            if (prevDetails.RequiredAreaType != currDetails.RequiredAreaType)
                            {
                                if (currDetails.RequiredAreaType != null)
                                {
                                    body.Replace("$$NewAreaShape$$", strContent);
                                    body.Replace("$$Label$$", "Area Shape");
                                    if (MethodFactory.BindAreaShape(currDetails.RequiredAreaType, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.BindAreaShape(currDetails.RequiredAreaType, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$NewAreaShape$$", string.Empty);
                                }
                                else
                                    body.Replace("$$NewAreaShape$$", string.Empty);
                                if (prevDetails.RequiredAreaType != null)
                                {
                                    body.Replace("$$OldAreaShape$$", strContent);
                                    body.Replace("$$Label$$", "Area Shape");
                                    if (MethodFactory.BindAreaShape(prevDetails.RequiredAreaType, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.BindAreaShape(prevDetails.RequiredAreaType, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$OldAreaShape$$", string.Empty);
                                }
                                else
                                    body.Replace("$$OldAreaShape$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewAreaShape$$", string.Empty);
                                body.Replace("$$OldAreaShape$$", string.Empty);
                            }
                            #endregion
                            #region Country
                            if (CurrentExhibitorEntity.CountryId != entity.CountryId)
                            {
                                if (entity.CountryId != null)
                                {
                                    body.Replace("$$NewCountry$$", strContent);
                                    body.Replace("$$Label$$", "Country");
                                    if (MethodFactory.GetCountryName(entity.CountryId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.GetCountryName(entity.CountryId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$NewCountry$$", string.Empty);
                                }
                                else
                                    body.Replace("$$NewCountry$$", string.Empty);
                                if (CurrentExhibitorEntity.CountryId != null)
                                {
                                    body.Replace("$$OldCountry$$", strContent);
                                    body.Replace("$$Label$$", "Country");
                                    if (MethodFactory.GetCountryName(CurrentExhibitorEntity.CountryId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.GetCountryName(CurrentExhibitorEntity.CountryId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$OldCountry$$", string.Empty);
                                }
                                else
                                    body.Replace("$$OldCountry$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewCountry$$", string.Empty);
                                body.Replace("$$OldCountry$$", string.Empty);
                            }
                            #endregion
                            #region City
                            if (CurrentExhibitorEntity.CityId != entity.CityId)
                            {
                                if (entity.CityId != null)
                                {
                                    body.Replace("$$NewCity$$", strContent);
                                    body.Replace("$$Label$$", "City");
                                    if (MethodFactory.GetCityName(entity.CityId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.GetCityName(entity.CityId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$NewCity$$", string.Empty);
                                }
                                else
                                    body.Replace("$$NewCity$$", string.Empty);
                                if (CurrentExhibitorEntity.CityId != null)
                                {
                                    body.Replace("$$OldCity$$", strContent);
                                    body.Replace("$$Label$$", "City");
                                    if (MethodFactory.GetCityName(CurrentExhibitorEntity.CityId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.GetCityName(CurrentExhibitorEntity.CityId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$OldCity$$", string.Empty);
                                }
                                else
                                    body.Replace("$$OldCity$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewCity$$", string.Empty);
                                body.Replace("$$OldCity$$", string.Empty);
                            }
                            #endregion
                            #region Exhibition Category
                            if (CurrentExhibitorEntity.ExhibitionCategoryId != entity.ExhibitionCategoryId)
                            {
                                if (entity.ExhibitionCategoryId != null)
                                {
                                    body.Replace("$$NewExhibitionCategory$$", strContent);
                                    body.Replace("$$Label$$", "Exhibition Category");
                                    if (MethodFactory.GetExhibitionCategory(entity.ExhibitionCategoryId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.GetExhibitionCategory(entity.ExhibitionCategoryId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$NewExhibitionCategory$$", string.Empty);
                                }
                                else
                                    body.Replace("$$NewExhibitionCategory$$", string.Empty);
                                if (CurrentExhibitorEntity.ExhibitionCategoryId != null)
                                {
                                    body.Replace("$$OldExhibitionCategory$$", strContent);
                                    body.Replace("$$Label$$", "Exhibition Category");
                                    if (MethodFactory.GetExhibitionCategory(CurrentExhibitorEntity.ExhibitionCategoryId.Value, langId) != null)
                                    {
                                        body.Replace("$$Value$$", MethodFactory.GetExhibitionCategory(CurrentExhibitorEntity.ExhibitionCategoryId.Value, langId));
                                        IsAnyFieldChanged = true;
                                    }
                                    else
                                        body.Replace("$$OldExhibitionCategory$$", string.Empty);
                                }
                                else
                                    body.Replace("$$OldExhibitionCategory$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewExhibitionCategory$$", string.Empty);
                                body.Replace("$$OldExhibitionCategory$$", string.Empty);
                            }
                            #endregion
                            #region Contact Person English
                            if (CurrentExhibitorEntity.ContactPersonName != entity.ContactPersonName)
                            {
                                if (entity.ContactPersonName != null)
                                {
                                    body.Replace("$$NewContactPersonNameEn$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Name in English");
                                    body.Replace("$$Value$$", entity.ContactPersonName);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewContactPersonNameEn$$", string.Empty);
                                if (CurrentExhibitorEntity.ContactPersonName != null)
                                {
                                    body.Replace("$$OldContactPersonNameEn$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Name in English");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.ContactPersonName);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldContactPersonNameEn$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewContactPersonNameEn$$", string.Empty);
                                body.Replace("$$OldContactPersonNameEn$$", string.Empty);
                            }
                            #endregion
                            #region Contact Person Arabic
                            if (CurrentExhibitorEntity.ContactPersonNameAr != entity.ContactPersonNameAr)
                            {
                                if (entity.ContactPersonNameAr != null)
                                {
                                    body.Replace("$$NewContactPersonNameAr$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Name in Arabic");
                                    body.Replace("$$Value$$", entity.ContactPersonNameAr);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewContactPersonNameAr$$", string.Empty);
                                if (CurrentExhibitorEntity.ContactPersonNameAr != null)
                                {
                                    body.Replace("$$OldContactPersonNameAr$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Name in Arabic");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.ContactPersonNameAr);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldContactPersonNameAr$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewContactPersonNameAr$$", string.Empty);
                                body.Replace("$$OldContactPersonNameAr$$", string.Empty);
                            }
                            #endregion
                            #region Contact Person Title English
                            if (CurrentExhibitorEntity.ContactPersonTitle != entity.ContactPersonTitle)
                            {
                                if (entity.ContactPersonTitle != null)
                                {
                                    body.Replace("$$NewContactPersonTitleEn$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Title in English");
                                    body.Replace("$$Value$$", entity.ContactPersonTitle);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewCContactPersonTitleEn$$", string.Empty);
                                if (CurrentExhibitorEntity.ContactPersonTitle != null)
                                {
                                    body.Replace("$$OldContactPersonTitleEn$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Title in English");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.ContactPersonTitle);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldContactPersonTitleEn$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewContactPersonTitleEn$$", string.Empty);
                                body.Replace("$$OldContactPersonTitleEn$$", string.Empty);
                            }
                            #endregion
                            #region Contact Person Title Arabic
                            if (CurrentExhibitorEntity.ContactPersonTitleAr != entity.ContactPersonTitleAr)
                            {
                                if (entity.ContactPersonTitleAr != null)
                                {
                                    body.Replace("$$NewContactPersonTitleAr$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Title in Arabic");
                                    body.Replace("$$Value$$", entity.ContactPersonTitleAr);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewCContactPersonTitleAr$$", string.Empty);
                                if (CurrentExhibitorEntity.ContactPersonTitleAr != null)
                                {
                                    body.Replace("$$OldContactPersonTitleAr$$", strContent);
                                    body.Replace("$$Label$$", "Contact Person Title in Arabic");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.ContactPersonTitleAr);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldContactPersonTitleAr$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewContactPersonTitleAr$$", string.Empty);
                                body.Replace("$$OldContactPersonTitleAr$$", string.Empty);
                            }
                            #endregion
                            #region Phone
                            if (CurrentExhibitorEntity.Phone != entity.Phone)
                            {
                                if (entity.Phone != null)
                                {
                                    body.Replace("$$NewPhone$$", strContent);
                                    body.Replace("$$Label$$", "Phone");
                                    body.Replace("$$Value$$", entity.Phone.Replace('$', '-'));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewPhone$$", string.Empty);
                                if (CurrentExhibitorEntity.Phone != null)
                                {
                                    body.Replace("$$OldPhone$$", strContent);
                                    body.Replace("$$Label$$", "Phone");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.Phone);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldPhone$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewPhone$$", string.Empty);
                                body.Replace("$$OldPhone$$", string.Empty);
                            }
                            #endregion
                            #region Fax
                            if (CurrentExhibitorEntity.Fax != entity.Fax)
                            {
                                if (entity.Fax != null)
                                {
                                    body.Replace("$$NewFax$$", strContent);
                                    body.Replace("$$Label$$", "Fax");
                                    body.Replace("$$Value$$", entity.Fax.Replace('$', '-'));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewFax$$", string.Empty);
                                if (CurrentExhibitorEntity.Fax != null)
                                {
                                    body.Replace("$$OldFax$$", strContent);
                                    body.Replace("$$Label$$", "Fax");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.Fax);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldFax$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewFax$$", string.Empty);
                                body.Replace("$$OldFax$$", string.Empty);
                            }
                            #endregion
                            #region Mobile
                            if (CurrentExhibitorEntity.Mobile != entity.Mobile)
                            {
                                if (entity.Mobile != null)
                                {
                                    body.Replace("$$NewMobile$$", strContent);
                                    body.Replace("$$Label$$", "Mobile");
                                    body.Replace("$$Value$$", entity.Mobile.Replace('$', '-'));
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewMobile$$", string.Empty);
                                if (CurrentExhibitorEntity.Mobile != null)
                                {
                                    body.Replace("$$OldMobile$$", strContent);
                                    body.Replace("$$Label$$", "Mobile");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.Mobile);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldMobile$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewMobile$$", string.Empty);
                                body.Replace("$$OldMobile$$", string.Empty);
                            }
                            #endregion
                            #region Email
                            if (CurrentExhibitorEntity.Email != entity.Email)
                            {
                                if (entity.Email != null)
                                {
                                    body.Replace("$$NewEmail$$", strContent);
                                    body.Replace("$$Label$$", "Email");
                                    body.Replace("$$Value$$", entity.Email);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewEmail$$", string.Empty);
                                if (CurrentExhibitorEntity.Email != null)
                                {
                                    body.Replace("$$OldEmail$$", strContent);
                                    body.Replace("$$Label$$", "Email");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.Email);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldEmail$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewEmail$$", string.Empty);
                                body.Replace("$$OldEmail$$", string.Empty);
                            }
                            #endregion
                            #region Total Titles
                            if (CurrentExhibitorEntity.TotalNumberOfTitles != entity.TotalNumberOfTitles)
                            {
                                if (entity.TotalNumberOfTitles != null)
                                {
                                    body.Replace("$$NewTotletitles$$", strContent);
                                    body.Replace("$$Label$$", "Total Number Of Titles");
                                    body.Replace("$$Value$$", entity.TotalNumberOfTitles);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewTotletitles$$", string.Empty);
                                if (CurrentExhibitorEntity.TotalNumberOfTitles != null)
                                {
                                    body.Replace("$$OldTotletitles$$", strContent);
                                    body.Replace("$$Label$$", "Total Number Of Titles");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.TotalNumberOfTitles);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldTotletitles$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewTotletitles$$", string.Empty);
                                body.Replace("$$OldTotletitles$$", string.Empty);
                            }
                            #endregion
                            #region Total New Titles
                            if (CurrentExhibitorEntity.TotalNumberOfNewTitles != entity.TotalNumberOfNewTitles)
                            {
                                if (entity.TotalNumberOfNewTitles != null)
                                {
                                    body.Replace("$$NewTotletitlesNew$$", strContent);
                                    body.Replace("$$Label$$", "Total Number Of New Titles");
                                    body.Replace("$$Value$$", entity.TotalNumberOfNewTitles);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewTotletitlesNew$$", string.Empty);
                                if (CurrentExhibitorEntity.TotalNumberOfNewTitles != null)
                                {
                                    body.Replace("$$OldTotletitlesNew$$", strContent);
                                    body.Replace("$$Label$$", "Total Number Of new Titles");
                                    body.Replace("$$Value$$", CurrentExhibitorEntity.TotalNumberOfNewTitles);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldTotletitlesNew$$", string.Empty);
                            }
                            else
                            {
                                body.Replace("$$NewTotletitlesNew$$", string.Empty);
                                body.Replace("$$OldTotletitlesNew$$", string.Empty);
                            }
                            #endregion
                            #region APU
                            if (prevDetails.Apu != currDetails.Apu)
                            {
                                if (currDetails.Apu != null)
                                {
                                    body.Replace("$$NewAPU$$", strContent);
                                    body.Replace("$$Label$$", "Participation in Publishers Union");
                                    if (currDetails.Apu == "Y")
                                        body.Replace("$$Value$$", "Yes");
                                    else if (currDetails.Apu == "N")
                                        body.Replace("$$Value$$", "No");
                                    else
                                        body.Replace("$$NewAPU$$", string.Empty);
                                }
                                else
                                    body.Replace("$$NewAPU$$", string.Empty);
                                if (prevDetails.Apu != null)
                                {
                                    body.Replace("$$OldAPU$$", strContent);
                                    body.Replace("$$Label$$", "Participation in Publishers Union");
                                    if (prevDetails.Apu == "Y")
                                        body.Replace("$$Value$$", "Yes");
                                    else if (prevDetails.Apu == "N")
                                        body.Replace("$$Value$$", "No");
                                    else
                                        body.Replace("$$OldAPU$$", string.Empty);
                                }
                                else
                                    body.Replace("$$OldAPU$$", string.Empty);
                                IsAnyFieldChanged = true;
                            }
                            else
                            {
                                body.Replace("$$NewAPU$$", string.Empty);
                                body.Replace("$$OldAPU$$", string.Empty);
                            }
                            #endregion
                            #endregion
                            if (IsAnyFieldChanged)
                                if (strEmailContentEmail != string.Empty)
                                {
                                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString().Replace("$$SpecificName$$", strEmailContentAdmin));

                                    //XsiExhibitionMember memberentity = GetMemberEntity(entity.MemberId.Value);
                                    //if (memberentity != default(XsiExhibitionMember))
                                    //{
                                    //    string userbody = body.ToString();
                                    //    if (memberentity.FirstNameEmail != null && memberentity.LastNameEmail != null)
                                    //        userbody = userbody.Replace("$$SpecificName$$", memberentity.FirstNameEmail + " " + memberentity.LastNameEmail);
                                    //    else if (memberentity.Firstname != null)
                                    //        userbody = userbody.Replace("$$SpecificName$$", memberentity.FirstNameEmail);
                                    //    else if (memberentity.LastName != null)
                                    //        userbody = userbody.Replace("$$SpecificName$$", memberentity.LastNameEmail);
                                    //    SmtpEmail.SendNetEmail(strEmailContentAdmin, AdminEmailAddress, memberentity.Email, strEmailContentSubject, userbody);
                                    //}
                                }
                        }
                    }
                    #endregion
                }
            }
        }
        #endregion
    }
}


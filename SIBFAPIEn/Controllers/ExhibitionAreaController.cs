﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionAreaController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionAreaController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/ExhibitionArea
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DropdownDataDTO>>> GetXsiExhibitionArea()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionArea>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionArea.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new DropdownDataDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title,
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionArea>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionArea.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new DropdownDataDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.TitleAr,
                }).ToListAsync();

                return await List;
            }
        }
        [HttpGet]
        [Route("ExhibitionRestaurantArea")]
        public async Task<ActionResult<IEnumerable<DropdownDataDTO>>> GetXsiExhibitionRestaurantArea()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionArea>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionArea.Where(i => Convert.ToInt64(i.Title) <= 18).AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new DropdownDataDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title,
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionArea>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionArea.Where(i => Convert.ToInt64(i.Title) <= 18).AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new DropdownDataDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.TitleAr,
                }).ToListAsync();

                return await List;
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<DropdownDataDTO>> GetXsiExhibitionArea(long id)
        {
            var xsiItem = await _context.XsiExhibitionArea.FindAsync(id);

            if (xsiItem == null)
            {
                return NotFound();
            }
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                DropdownDataDTO itemDTO = new DropdownDataDTO()
                {
                    ItemId = xsiItem.ItemId,
                    Title = xsiItem.Title,
                };
                return itemDTO;
            }
            else
            {
                DropdownDataDTO itemDTO = new DropdownDataDTO()
                {
                    ItemId = xsiItem.ItemId,
                    Title = xsiItem.TitleAr,
                };
                return itemDTO;
            }
        }

        [HttpPost]
        public async Task<ActionResult<dynamic>> BindExhibitionArea(ExhbitionAreaDTO dto)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.New<XsiExhibitionArea>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                if (dto.BoothSubSectionTitle == "Restaurant" || dto.BoothSubSectionTitle == "مطعم")
                    predicate = predicate.And(i => i.Title != "15" && i.Title != "21");
                else
                    predicate = predicate.And(i => i.Title != null && !string.IsNullOrEmpty(i.Title));

                var List = _context.XsiExhibitionArea.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new DropdownDataDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title,
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.New<XsiExhibitionArea>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                if (dto.BoothSubSectionTitle == "Restaurant" || dto.BoothSubSectionTitle == "مطعم")
                    predicate = predicate.And(i => i.TitleAr != "15" && i.TitleAr != "21");
                else
                    predicate = predicate.And(i => i.TitleAr != null && !string.IsNullOrEmpty(i.TitleAr));

                var List = _context.XsiExhibitionArea.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new DropdownDataDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.TitleAr,
                }).ToListAsync();

                return await List;
            }
        }
        // GET: api/ExhibitionArea/5
        private bool XsiExhibitionAreaExists(long id)
        {
            return _context.XsiExhibitionArea.Any(e => e.ItemId == id);
        }
    }
}

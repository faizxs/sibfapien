﻿using Contracts;
using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xsi.BusinessLogicLayer;
using Xsi.ServicesLayer;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class MemberAndExhibitorFinesController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private ILoggerManager _logger;
        private IMemoryCache _cache;
        EnumResultType XsiResult { get; set; }

        private readonly IEmailSender _emailSender;
        private readonly sibfnewdbContext _context;
        public MemberAndExhibitorFinesController(IEmailSender emailSender, IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IMemoryCache memoryCache, sibfnewdbContext context)
        {
            _emailSender = emailSender;
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _cache = memoryCache;
            _context = context;
        }

        public List<XsiExhibition> cachedExhibition { get; set; }
        public List<XsiExhibitionMemberApplicationYearly> cachedExhibitionMemberApplicationYearly { get; set; }

        [HttpGet]
        [Route("GetExhibitionId/{websiteid}")]
        public ActionResult<dynamic> GetExhibitionId(long websiteid)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            string strmessage = string.Empty;
            bool Isvalid = false;
            var predicateExhibition = PredicateBuilder.New<XsiExhibition>();

            predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicateExhibition = predicateExhibition.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
            predicateExhibition = predicateExhibition.And(i => i.WebsiteId == websiteid);

            XsiExhibition entity = _context.XsiExhibition.Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
            if (entity != null)
            {
                var dto1 = new { ExhibitionId = entity.ExhibitionId, MessageTypeResponse = "Success" };
                return Ok(dto1);
            }

            var dto = new { ExhibitionId = 0, MessageTypeResponse = "Error" };
            return Ok(dto);
        }
        [HttpGet]
        [Route("GetFinesCategory")]
        public ActionResult<dynamic> GetFinesCategoryList()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            List<DropdownDataDTO> dtolist = new List<DropdownDataDTO>();
            if (LangId == 1)
                dtolist = _context.XsiExhibitionFinesCategory.Where(i => i.IsActive == "Y").Select(i => new DropdownDataDTO() { ItemId = i.ItemId, Title = i.Title }).ToList();
            else
                dtolist = _context.XsiExhibitionFinesCategory.Where(i => i.IsActiveAr == "Y").Select(i => new DropdownDataDTO() { ItemId = i.ItemId, Title = i.TitleAr }).ToList();
            var dto = new { dtolist, MessageTypeResponse = "Success" };
            return Ok(dto);
        }

        [HttpGet]
        [Route("GetFinesList/{pagesize}/{pagenumber}/{exhibitionid}/{memberid}/{exhibitorid}")]
        public ActionResult<dynamic> GetFinesList(int pagesize, int pagenumber, long exhibitionid = -1, long memberid = -1, long exhibitorid = -1)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            cachedExhibitionMemberApplicationYearly = (List<XsiExhibitionMemberApplicationYearly>)_cache.Get<dynamic>(CacheKeys.ExhibitionMemberApplicationYearlyCacheList);
            if (cachedExhibitionMemberApplicationYearly == null)
                cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

            cachedExhibition = (List<XsiExhibition>)_cache.Get<dynamic>(CacheKeys.Exhibition);
            if (cachedExhibition == null)
                cachedExhibition = CacheKeys.GetExhibition(_cache);

            // var exhibitionlist = _context.XsiExhibition.Where(i => i.IsActive == "Y").Select(i => new { ExhibitionId = i.ExhibitionId, ExhibitionNumber = i.ExhibitionNumber }).ToList();
            // var exhibitorlist = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.ExhibitionId > 44 && i.IsActive == "Y").Select(i => new { i.MemberExhibitionYearlyId, i.PublisherNameEn, i.PublisherNameAr }).ToList();

            string strmessage = string.Empty;
            var predicate = PredicateBuilder.New<XsiExhibitionMemberandExhibitorFines>();

            if (exhibitionid > 0)
                predicate = predicate.And(i => i.ExhibitionId == exhibitionid);

            if (memberid > 0)
                predicate = predicate.And(i => i.MemberId == memberid);

            if (exhibitorid > 0)
                predicate = predicate.And(i => i.ExhibitorId == exhibitorid);

            predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

            if (LangId == 1)
            {
                var FinesList = _context.XsiExhibitionMemberandExhibitorFines.Where(predicate)
               .Select(i => new MemberAndExhibitorFinesListDTO()
               {
                   ItemId = i.ItemId,
                   ExhibitionId = i.ExhibitionId,
                   FineCategoryId = i.FineCategoryId,
                   MemberId = i.MemberId,
                   ExhibitorId = i.ExhibitorId,
                   ExhibitionNumber = cachedExhibition.Where(x => x.ExhibitionId == i.ExhibitionId).Select(x => x.ExhibitionNumber).FirstOrDefault().ToString(),
                   FinesCategoryName = i.FineCategory.Title,
                   UserName = i.Member.UserName,
                   MemberName = i.Member.Firstname ?? "-" + " " + i.Member.LastName ?? "-",
                   ExhibitorName = cachedExhibitionMemberApplicationYearly.Where(x => i.ExhibitorId != null && x.MemberExhibitionYearlyId == i.ExhibitorId.Value).Select(x => x.PublisherNameAr).DefaultIfEmpty("-").First(),
                   Status = i.Status,
                   CreatedOn = i.CreatedOn.Value.ToString()
               }).Take(100)
               .OrderByDescending(i => i.ItemId).Skip((pagenumber - 1) * pagesize).Take(pagesize).ToList();
                var dto = new { FinesList, MessageTypeResponse = "Success" };
                return Ok(dto);
            }
            else
            {
                var FinesList = _context.XsiExhibitionMemberandExhibitorFines.Where(predicate)
               .Select(i => new MemberAndExhibitorFinesListDTO()
               {
                   ItemId = i.ItemId,
                   ExhibitionId = i.ExhibitionId,
                   FineCategoryId = i.FineCategoryId,
                   MemberId = i.MemberId,
                   ExhibitorId = i.ExhibitorId,
                   ExhibitionNumber = cachedExhibition.Where(x => x.ExhibitionId == i.ExhibitionId).Select(x => x.ExhibitionNumber).FirstOrDefault().ToString(),
                   FinesCategoryName = i.FineCategory.Title,
                   UserName = i.Member.UserName,
                   MemberName = i.Member.FirstNameAr ?? (i.Member.Firstname ?? "-") + " " + i.Member.LastNameAr ?? (i.Member.LastName ?? "-"),
                   ExhibitorName = cachedExhibitionMemberApplicationYearly.Where(x => i.ExhibitorId != null && x.MemberExhibitionYearlyId == i.ExhibitorId.Value).Select(x => x.PublisherNameAr ?? x.PublisherNameEn).DefaultIfEmpty("-").First(),
                   Status = i.Status,
                   CreatedOn = i.CreatedOn.Value.ToString()
               })
               .OrderByDescending(i => i.ItemId).Skip((pagenumber - 1) * pagesize).Take(pagesize).ToList();
                var dto = new { FinesList, MessageTypeResponse = "Success" };
                return Ok(dto);
            }
        }

        // POST: api/PostMemberAndExhibitorFines
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> PostMemberAndExhibitorFines(MemberAndExhibitorFinesDTO data)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                bool isvalid = true;
                string strmessage = string.Empty;

                if (data.ExhibitionId <= 0 && data.MemberId <= 0 && data.FineCategoryId <= 0)
                    strmessage = "ExhibitionId, MemberId and FineCategoryId is missing";
                else if (data.ExhibitionId <= 0 && data.MemberId <= 0)
                    strmessage = "ExhibitionId, MemberId  is missing";
                else if (data.ExhibitionId <= 0 && data.FineCategoryId <= 0)
                    strmessage = "ExhibitionId, FineCategoryId is missing";
                else if (data.MemberId <= 0 && data.FineCategoryId <= 0)
                    strmessage = "MemberId, FineCategoryId is missing";
                else if (data.ExhibitionId <= 0)
                    strmessage = "ExhibitionId is missing";
                else if (data.MemberId <= 0)
                    strmessage = "MemberId is missing";
                else if (data.FineCategoryId <= 0)
                    strmessage = "FineCategoryId is missing";

                if (!string.IsNullOrEmpty(strmessage))
                {
                    var dto3 = new { strmessage, data.hdnTempData, MessageTypeResponse = "Error" };
                    return Ok(dto3);
                }
                long itemid = AddFines(data);

                if (itemid > 0)
                {
                    if (LangId == 1)
                    {
                        strmessage = "Form has been submitted successfully.";
                    }
                    else
                    {
                        strmessage = "Form has been submitted successfully.";
                    }
                    var dto = new { strmessage, itemid };

                    SendEmailToExhibitor(itemid, 1, LangId);
                    return Ok(dto);
                }
                else
                {
                    strmessage = "Error while submitting the form";
                    var dto3 = new { strmessage, data.hdnTempData };
                    return Ok(dto3);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside PostMemberAndExhibitorFines action: {ex.InnerException}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        private long AddFines(MemberAndExhibitorFinesDTO data)
        {
            XsiExhibitionMemberandExhibitorFines entity = new XsiExhibitionMemberandExhibitorFines();

            entity.ExhibitionId = data.ExhibitionId;
            entity.MemberId = data.MemberId;
            entity.FineCategoryId = data.FineCategoryId;

            if (data.ExhibitorId > 0)
                entity.ExhibitorId = data.ExhibitorId;

            entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
            entity.Status = EnumConversion.ToString(EnumAwardNominationStatus.New);

            entity.CreatedOn = MethodFactory.ArabianTimeNow();
            entity.ModifiedOn = MethodFactory.ArabianTimeNow();

            if (Convert.ToInt64(data.hdnTempData) <= 0)
                _context.XsiExhibitionMemberandExhibitorFines.Add(entity);

            if (_context.SaveChanges() > 0)
            {
                return entity.ItemId;
            }
            return 0;
        }

        void SendEmailToExhibitor(long itemid, long websiteId, long langId)
        {
            var finesentity = _context.XsiExhibitionMemberandExhibitorFines.Where(i => i.ItemId == itemid).FirstOrDefault();
            if (finesentity != null)
            {
                if (finesentity.MemberId != null)
                {
                    XsiExhibitionMember memberentity = _context.XsiExhibitionMember.Where(i => i.MemberId == finesentity.MemberId.Value).FirstOrDefault();
                    HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                    var emailContent = _context.XsiEmailContent.Where(i => i.ItemId == 20201).FirstOrDefault();
                    string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";

                    #region Email Content
                    StringBuilder body = new StringBuilder();
                    if (emailContent != null)
                    {
                        if (emailContent.Email != null)
                            strEmailContentEmail = emailContent.Email;
                       
                        if (langId == 2)
                        {
                            if (emailContent.BodyAr != null)
                                strEmailContentBody = emailContent.BodyAr;
                           
                            if (emailContent.SubjectAr != null)
                                strEmailContentSubject = emailContent.SubjectAr;
                        }
                        else
                        {
                            if (emailContent.Body != null)
                                strEmailContentBody = emailContent.Body;

                            if (emailContent.Subject != null)
                                strEmailContentSubject = emailContent.Subject;
                        }
                       
                        strEmailContentAdmin = _appCustomSettings.AdminName;

                        // body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, null, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                        body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, memberentity, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                        XsiResult = _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, memberentity.Email, strEmailContentSubject, body.ToString());
                    }
                    #endregion
                }
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using SIBFAPIEn.DTO;
using LinqKit;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class SponsorsController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;
        public SponsorsController(IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        // GET: api/Sponsors
        [HttpGet]
        public async Task<ActionResult<dynamic>> GetXsiIconsForSponsor()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var List = _context.XsiIconsForSponsor.AsNoTracking().Where(i => i.IsActive == "Y").AsQueryable()
                                 .OrderBy(o => o.CreatedOn).Select(i => new
                                 {
                                     ItemId = i.ItemId,
                                     Title = i.Title,
                                     Overview = i.Overview,
                                     ImageName = _appCustomSettings.UploadsPath + "/IconsForSponsor/" + i.ImageName,
                                 }).ToListAsync();
                return await List;
            }
            else
            {
                var List = _context.XsiIconsForSponsor.AsNoTracking().Where(i => i.IsActiveAr == "Y").AsQueryable()
                                   .OrderBy(o => o.CreatedOn).Select(i => new
                                   {
                                       ItemId = i.ItemId,
                                       Title = i.TitleAr,
                                       Overview = i.OverviewAr,
                                       ImageName = _appCustomSettings.UploadsPath + "/IconsForSponsor/" + i.ImageNameAr,
                                   }).ToListAsync();
                return await List;
            }
        }

        // GET: api/Sponsors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<dynamic>> GetXsiIconsForSponsor(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var xsiItem = await _context.XsiIconsForSponsor.FindAsync(id);

                if (xsiItem == null)
                {
                    return NotFound();
                }
                var itemDTO = new
                {
                    ItemId = xsiItem.ItemId,
                    Title = xsiItem.Title,
                    Overview = xsiItem.Overview,
                    ImageName = _appCustomSettings.UploadsPath + "/IconsForSponsor/" + xsiItem.ImageName,
                    Url = xsiItem.Url
                };
                return itemDTO;
            }
            else
            {
                var xsiItem = await _context.XsiIconsForSponsor.FindAsync(id);

                if (xsiItem == null)
                {
                    return NotFound();
                }
                var itemDTO = new
                {
                    ItemId = xsiItem.ItemId,
                    Title = xsiItem.TitleAr,
                    Overview = xsiItem.OverviewAr,
                    ImageName = _appCustomSettings.UploadsPath + "/IconsForSponsor/" + xsiItem.ImageNameAr,
                    Url = xsiItem.Urlar
                };
                return itemDTO;
            }
        }

        private bool XsiIconsForSponsorExists(long id)
        {
            return _context.XsiIconsForSponsor.Any(e => e.ItemId == id);
        }
    }
}

﻿using Entities.Models;
using GIGTools;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xsi.ServicesLayer;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {

        private readonly AppCustomSettings _appCustomSettings;
        EnumResultType XsiResult { get; set; }
        #region Properties
        //protected long ExhibitionId { get; set; }
        // protected long ExhibitorId { get; set; }
        // protected string TotalParticipationFee { get; set; }
        // protected double PriceSqMeter { get; set; }
        // protected double ArabicAgencyFees { get; set; }
        // protected double ForeignAgencyFee { get; set; }
        //  protected double RepresentativeFee { get; set; }
        // protected long TotalAllaocatedSpace { get; set; }
        // protected string PaymentLastDate { get; set; }
        // protected string PaymentRemainingDays { get; set; }
        //protected List<XsiInvoice> InvoiceList { get; set; }
        #endregion

        private readonly IEmailSender _emailSender;
        public PaymentController(IEmailSender emailSender, IOptions<AppCustomSettings> appCustomSettings)
        {
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
        }

        // GET: api/GetPayment
        [HttpGet]
        [Route("{websiteid}/{memberroleid}")]
        public async Task<ActionResult<dynamic>> GetPayment(long websiteid, long memberroleid)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    long memberid = User.Identity.GetID();
                    PaymentDTO model = new PaymentDTO();
                    model.Lang = LangId;
                    if (IsExhibitionActive(websiteid))
                    {
                        XsiExhibitionMemberApplicationYearly ExhibitorEntity = MethodFactory.GetExhibitor(memberid, websiteid, _context, memberroleid);
                        if (ExhibitorEntity != null && ExhibitorEntity.Status != null)
                        {
                            if (ExhibitorEntity.CountryId != null)
                                model.CountryId = ExhibitorEntity.CountryId;

                            if (ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved) || ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval))
                            {
                                if (AtleastOneInvoice(ExhibitorEntity.MemberExhibitionYearlyId, _context))
                                {
                                    // ExhibitorEntity.MemberExhibitionYearlyId;
                                    BindListing(model, LangId, ExhibitorEntity.MemberExhibitionYearlyId);
                                    GetOverviewContent(model, LangId, ExhibitorEntity.MemberExhibitionYearlyId);
                                    UpdateStatusChange(ExhibitorEntity.MemberExhibitionYearlyId);
                                }
                                else
                                    model.ReturnURL = "/en/MemberDashboard";
                            }
                            else
                                model.ReturnURL = "/en/MemberDashboard";
                        }
                        else
                            model.ReturnURL = "/en/MemberDashboard";
                    }
                    else
                    {
                        model.ReturnURL = "/en/MemberDashboard";
                    }
                    return Ok(model);
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }

        }
        // GET: api/GetPayment

        [HttpGet]
        [Route("CheckStatus/{websiteid}/{memberid}")]
        public async Task<ActionResult<dynamic>> CheckStatus(long websiteid, long memberid)
        {
            MessageDTO dto = new MessageDTO();
            dto.MessageTypeResponse = "Error";

            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    long exhibitionmemberid = User.Identity.GetID();

                    List<InquiryItem> InquiryList = new List<InquiryItem>();
                    if (exhibitionmemberid == memberid)
                    {
                        XsiExhibition entity = MethodFactory.GetActiveExhibition(websiteid, _context);
                        if (entity != null)
                        {
                            string strinitialapprove = EnumConversion.ToString(EnumExhibitorStatus.InitialApproval);
                            string strapproved = EnumConversion.ToString(EnumExhibitorStatus.Approved);
                            //&& (i.Status == strinitialapprove || i.Status == strapproved)
                            XsiExhibitionMemberApplicationYearly ExhibitorEntity = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.ExhibitionId == entity.ExhibitionId && i.MemberId == memberid && i.IsActive == "Y").OrderByDescending(x => x.MemberExhibitionYearlyId).FirstOrDefault();
                            if (ExhibitorEntity != null && !string.IsNullOrEmpty(ExhibitorEntity.FileNumber))
                            {
                                dto.MessageTypeResponse = "Success";
                                var strJsonData = GalaxyInvoiceEnquiry(ExhibitorEntity.FileNumber);

                                InvoiceRootDto myObject = JsonConvert.DeserializeObject<InvoiceRootDto>(strJsonData);
                                InquiryList = myObject.Table.Select(i => new InquiryItem() { InvoiceNo = i.InvoiceNo, InvoiceAmount = i.InvoiceAmount, InvoiceDate = GetInvoiceFormattedDate(i.InvoiceDate), InvoiceStatus = i.Status }).OrderByDescending(i => i.InvoiceDate).ToList();

                                return Ok(new { MessageTypeResponse = "Success", List = InquiryList });
                            }
                            dto.Message = "filenumber is missing.";
                        }
                        dto.Message = "There is no active exhibition available.";
                    }
                    else
                    {
                        dto.Message = "Unauthorised access.";
                        return Ok(dto);
                    }

                    dto.Message = "Something went wrong. Please try again later.";
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                dto.Message = "Exception: Something went wrong. Please retry again later.";
                return Ok(dto);
            }
        }
        [HttpGet]
        [Route("CheckStatusAlternate/{filenumber}")]
        public async Task<ActionResult<dynamic>> CheckStatusAlternate(string filenumber)
        {
            MessageDTO dto = new MessageDTO();
            dto.MessageTypeResponse = "Error";

            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    long memberid = User.Identity.GetID();
                    List<InquiryItem> InquiryList = new List<InquiryItem>();

                    if (!string.IsNullOrEmpty(filenumber) && filenumber != "-1")
                    {
                        dto.MessageTypeResponse = "Success";
                        var strJsonData = GalaxyInvoiceEnquiry(filenumber);
                        InvoiceRootDto myObject = JsonConvert.DeserializeObject<InvoiceRootDto>(strJsonData);
                        InquiryList = myObject.Table.Select(i => new InquiryItem()
                        {
                            InvoiceNo = i.InvoiceNo,
                            InvoiceAmount = i.InvoiceAmount,
                            InvoiceDate = GetInvoiceFormattedDate(i.InvoiceDate),
                            InvoiceStatus = i.Status
                        }).OrderByDescending(i => i.InvoiceDate).ToList();
                        return Ok(new { MessageTypeResponse = "Success", List = InquiryList });
                    }

                    dto.Message = "Something went wrong. Please try again later.";
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                dto.Message = "Exception: Something went wrong. Please retry again later.";
                return Ok(dto);
            }
        }

        [HttpGet]
        [Route("CheckCustomerBalance/{filenumber}")]
        public async Task<ActionResult<dynamic>> ChecCheckCustomerBalancekStatus(long websiteid, long memberid)
        {
            MessageDTO dto = new MessageDTO();
            dto.MessageTypeResponse = "Error";

            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    long exhibitionmemberid = User.Identity.GetID();

                    List<InquiryItem> InquiryList = new List<InquiryItem>();
                    if (exhibitionmemberid == memberid)
                    {
                        XsiExhibition entity = MethodFactory.GetActiveExhibition(websiteid, _context);
                        if (entity != null)
                        {
                            string strinitialapprove = EnumConversion.ToString(EnumExhibitorStatus.InitialApproval);
                            string strapproved = EnumConversion.ToString(EnumExhibitorStatus.Approved);
                            //&& (i.Status == strinitialapprove || i.Status == strapproved)
                            XsiExhibitionMemberApplicationYearly ExhibitorEntity = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.ExhibitionId == entity.ExhibitionId && i.MemberId == memberid && i.IsActive == "Y").OrderByDescending(x => x.MemberExhibitionYearlyId).FirstOrDefault();
                            if (ExhibitorEntity != null && !string.IsNullOrEmpty(ExhibitorEntity.FileNumber))
                            {
                                dto.MessageTypeResponse = "Success";
                                var strJsonData = GalaxyInvoiceEnquiry(ExhibitorEntity.FileNumber);

                                InvoiceRootDto myObject = JsonConvert.DeserializeObject<InvoiceRootDto>(strJsonData);
                                InquiryList = myObject.Table.Select(i => new InquiryItem() { InvoiceNo = i.InvoiceNo, InvoiceAmount = i.InvoiceAmount, InvoiceDate = GetInvoiceFormattedDate(i.InvoiceDate), InvoiceStatus = i.Status }).OrderByDescending(i => i.InvoiceDate).ToList();

                                return Ok(new { MessageTypeResponse = "Success", List = InquiryList });
                            }
                            dto.Message = "filenumber is missing.";
                        }
                        dto.Message = "There is no active exhibition available.";
                    }
                    else
                    {
                        dto.Message = "Unauthorised access.";
                        return Ok(dto);
                    }

                    dto.Message = "Something went wrong. Please try again later.";
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {;
                dto.Message = "Exception: Something went wrong. Please retry again later.";
                return Ok(dto);
            }
        }

        [HttpGet]
        [Route("invoice/{invoiceid}")]
        public async Task<ActionResult<dynamic>> GetInvoice(long invoiceid)
        {
            try
            {
                string PaymentLastDate = string.Empty;
                string PaymentRemainingDays = string.Empty;
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    if (invoiceid != 0)
                    {
                        InvoiceDTO invoice = new InvoiceDTO();
                        XsiInvoice entity = _context.XsiInvoice.Where(x => x.ItemId == invoiceid).FirstOrDefault();
                        if (entity != null)
                        {
                            //IsExhibitionActive(1);
                            if (entity.Status != null)
                                if (entity.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingPayment))
                                {
                                    entity.IsNewPending = EnumConversion.ToString(EnumBool.No);
                                    _context.SaveChanges();
                                }

                            XsiExhibitionMemberApplicationYearly exhibitor = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == entity.ExhibitorId).FirstOrDefault();
                            var exhibition = _context.XsiExhibition.Where(i => i.ExhibitionId == exhibitor.ExhibitionId).FirstOrDefault();
                            #region InvoiceEntity
                            invoice.ItemId = entity.ItemId;
                            invoice.ExhibitorId = entity.ExhibitorId;
                            invoice.InvoiceNumber = entity.InvoiceNumber;
                            invoice.FileName = _appCustomSettings.UploadsCMSPath + "/exhibitorregistration/invoice/" + entity.FileName;
                            invoice.AllocatedSpace = entity.AllocatedSpace;
                            invoice.ParticipationFee = entity.ParticipationFee;
                            invoice.StandFee = entity.StandFee;
                            invoice.KnowledgeAndResearchFee = entity.KnowledgeAndResearchFee;
                            invoice.AgencyFee = entity.AgencyFee;
                            invoice.RepresentativeFee = entity.RepresentativeFee;
                            invoice.DueFromLastYearCredit = entity.DueFromLastYearCredit;
                            invoice.DueFromLastYearDebit = entity.DueFromLastYearDebit;
                            invoice.DueCurrentYear = entity.DueCurrentYear;
                            invoice.DebitedCurrentYear = entity.DebitedCurrentYear;
                            invoice.Discount = entity.Discount;
                            invoice.DiscountNote = entity.DiscountNote;
                            invoice.Penalty = entity.Penalty;
                            invoice.VAT = entity.Vat;
                            invoice.TotalAED = entity.TotalAed;
                            invoice.TotalUSD = entity.TotalUsd;
                            invoice.Status = entity.Status;
                            invoice.IsActive = entity.IsActive;
                            invoice.IsChequeReceived = entity.IsChequeReceived;
                            invoice.IsNewPending = entity.IsNewPending;
                            invoice.UploadePaymentReceipt = entity.UploadePaymentReceipt;
                            invoice.PaymentTypeId = entity.PaymentTypeId;
                            invoice.IsStatusChanged = entity.IsStatusChanged;
                            invoice.Notes = entity.Notes;
                            invoice.ExemptionType = entity.ExemptionType;
                            invoice.IsRegistrationInvoice = entity.IsRegistrationInvoice;
                            invoice.DiscountPercent = entity.DiscountPercent;
                            invoice.PenaltyPercent = entity.PenaltyPercent;
                            invoice.RemainingAgencies = entity.RemainingAgencies;
                            invoice.RemainingRepresentatives = entity.RemainingRepresentatives;
                            invoice.CreatedOn = entity.CreatedOn;
                            invoice.CreatedById = entity.CreatedById;
                            invoice.ModifiedOn = entity.ModifiedOn;
                            invoice.ModifiedById = entity.ModifiedById;
                            invoice.PaidAmount = entity.PaidAmount;
                            string strBalanceAmount = string.Empty;
                            if (entity.TotalAed != null && entity.PaidAmount != null)
                            {
                                string strTotalAED = entity.TotalAed;
                                string strPaid = entity.PaidAmount;
                                if (!string.IsNullOrEmpty(strTotalAED) && !string.IsNullOrEmpty(strPaid))
                                    strBalanceAmount = (Convert.ToDouble(strTotalAED) - Convert.ToDouble(strPaid)).ToString();
                                else
                                {
                                    if (!string.IsNullOrEmpty(strTotalAED))
                                        strBalanceAmount = strTotalAED;
                                    else if (!string.IsNullOrEmpty(strPaid))
                                        strBalanceAmount = "-" + strPaid;
                                }
                            }
                            else
                            {
                                if (entity.TotalAed != null)
                                {
                                    if (!string.IsNullOrEmpty(entity.TotalAed))
                                        strBalanceAmount = entity.TotalAed;
                                }
                                else if (entity.PaidAmount != null)
                                    if (!string.IsNullOrEmpty(entity.PaidAmount))
                                        strBalanceAmount = "-" + entity.PaidAmount;

                            }
                            invoice.BalanceAmount = strBalanceAmount;
                            XsiExhibition exhibitionnew = MethodFactory.GetActiveExhibition(exhibition.WebsiteId.Value, _context);
                            if (exhibitionnew != null)
                            {
                                if (exhibitionnew.DateOfDue != null)
                                {
                                    PaymentLastDate = exhibitionnew.DateOfDue.Value.ToString("dddd d MMMM yyyy");
                                    if (exhibitionnew.DateOfDue > MethodFactory.ArabianTimeNow())
                                        PaymentRemainingDays = Math.Ceiling(exhibitionnew.DateOfDue.Value.Subtract(MethodFactory.ArabianTimeNow()).TotalDays).ToString();
                                    else
                                        PaymentRemainingDays = "0";
                                }
                            }

                            if (LangId == 1)
                            {
                                invoice.PaymentLastDate = "To avoid penalty fines make the payment before " + PaymentLastDate;
                                invoice.PaymentRemainingDays = PaymentRemainingDays + " days remaining";
                            }
                            else
                            {
                                invoice.PaymentLastDate = "لتجنب الغرامات الرجاء تسديد المبلغ قبل " + PaymentLastDate;
                                invoice.PaymentRemainingDays = PaymentRemainingDays + " الأيام المتبقية";
                            }
                            #endregion
                            return Ok(invoice);
                        }
                        return Ok("Invoice not found.");
                    }
                    else
                    {
                        return Ok("Invoice not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }

        }

        [HttpGet]
        [Route("uploaddetails/{invoiceid}")]
        public async Task<ActionResult<dynamic>> GetUploadDetails(long invoiceid)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    string PaymentLastDate = string.Empty;
                    string PaymentRemainingDays = string.Empty;
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    long memberid = User.Identity.GetID();
                    if (memberid != 0 && memberid != -1)
                    {
                        if (invoiceid != 0)
                        {
                            InvoiceDTO invoice = new InvoiceDTO();
                            XsiInvoice entity = _context.XsiInvoice.Where(x => x.ItemId == invoiceid).FirstOrDefault();
                            if (entity != null)
                            {
                                // IsExhibitionActive(1);
                                if (entity.Status != null)
                                    if (entity.Status == EnumConversion.ToString(EnumExhibitorStatus.PendingPayment))
                                    {
                                        entity.IsNewPending = EnumConversion.ToString(EnumBool.No);
                                        _context.SaveChanges();
                                    }
                                XsiExhibitionMemberApplicationYearly exhibitor = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == entity.ExhibitorId).FirstOrDefault();
                                var exhibition = _context.XsiExhibition.Where(i => i.ExhibitionId == exhibitor.ExhibitionId).FirstOrDefault();

                                #region InvoiceEntity
                                invoice.ItemId = entity.ItemId;
                                invoice.ExhibitorId = entity.ExhibitorId;
                                invoice.InvoiceNumber = entity.InvoiceNumber;
                                invoice.FileName = _appCustomSettings.UploadsCMSPath + "/exhibitorregistration/invoice/" + entity.FileName;
                                invoice.AllocatedSpace = entity.AllocatedSpace;
                                invoice.ParticipationFee = entity.ParticipationFee;
                                invoice.StandFee = entity.StandFee;
                                invoice.KnowledgeAndResearchFee = entity.KnowledgeAndResearchFee;
                                invoice.AgencyFee = entity.AgencyFee;
                                invoice.RepresentativeFee = entity.RepresentativeFee;
                                invoice.DueFromLastYearCredit = entity.DueFromLastYearCredit;
                                invoice.DueFromLastYearDebit = entity.DueFromLastYearDebit;
                                invoice.DueCurrentYear = entity.DueCurrentYear;
                                invoice.DebitedCurrentYear = entity.DebitedCurrentYear;
                                invoice.Discount = entity.Discount;
                                invoice.DiscountNote = entity.DiscountNote;
                                invoice.Penalty = entity.Penalty;
                                invoice.VAT = entity.Vat;
                                invoice.TotalAED = entity.TotalAed;
                                invoice.TotalUSD = entity.TotalUsd;
                                invoice.Status = entity.Status;
                                invoice.IsActive = entity.IsActive;
                                invoice.IsChequeReceived = entity.IsChequeReceived;
                                invoice.IsNewPending = entity.IsNewPending;
                                invoice.UploadePaymentReceipt = entity.UploadePaymentReceipt;
                                invoice.PaymentTypeId = entity.PaymentTypeId;
                                invoice.IsStatusChanged = entity.IsStatusChanged;
                                invoice.Notes = entity.Notes;
                                invoice.ExemptionType = entity.ExemptionType;
                                invoice.IsRegistrationInvoice = entity.IsRegistrationInvoice;
                                invoice.DiscountPercent = entity.DiscountPercent;
                                invoice.PenaltyPercent = entity.PenaltyPercent;
                                invoice.RemainingAgencies = entity.RemainingAgencies;
                                invoice.RemainingRepresentatives = entity.RemainingRepresentatives;
                                invoice.CreatedOn = entity.CreatedOn;
                                invoice.CreatedById = entity.CreatedById;
                                invoice.ModifiedOn = entity.ModifiedOn;
                                invoice.ModifiedById = entity.ModifiedById;
                                invoice.PaidAmount = entity.PaidAmount;

                                XsiExhibition exhibitionnew = MethodFactory.GetActiveExhibition(exhibition.WebsiteId.Value, _context);
                                if (exhibitionnew != null)
                                {
                                    if (exhibitionnew.DateOfDue != null)
                                    {
                                        PaymentLastDate = exhibitionnew.DateOfDue.Value.ToString("dddd d MMMM yyyy");
                                        if (exhibitionnew.DateOfDue > MethodFactory.ArabianTimeNow())
                                            PaymentRemainingDays = Math.Ceiling(exhibitionnew.DateOfDue.Value.Subtract(MethodFactory.ArabianTimeNow()).TotalDays).ToString();
                                        else
                                            PaymentRemainingDays = "0";
                                    }
                                }

                                if (LangId == 1)
                                {
                                    invoice.PaymentLastDate = "To avoid penalty fines make the payment before " + PaymentLastDate;
                                    invoice.PaymentRemainingDays = PaymentRemainingDays + " days remaining";
                                }
                                else
                                {
                                    invoice.PaymentLastDate = "لتجنب الغرامات الرجاء تسديد المبلغ قبل " + PaymentLastDate;
                                    invoice.PaymentRemainingDays = PaymentRemainingDays + " الأيام المتبقية";
                                }


                                //invoice.PaymentLastDate = "To avoid penalty fines make the payment before " + PaymentLastDate;
                                //invoice.PaymentRemainingDays = PaymentRemainingDays + " days remaining";
                                #endregion
                                return Ok(invoice);
                            }
                            return Ok("Invoice not found.");
                        }
                        else
                        {
                            return Ok("Invoice not found.");
                        }
                    }
                    return Ok("Unauthorized access");
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }

        }

        [HttpGet]
        [Route("acknowledgment/{websiteid}/{memberid}")]
        public ActionResult<dynamic> GetPaymentAknowledgement(long websiteid, long memberId)
        {
            MessageDTO dto = new MessageDTO();
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long memberid = User.Identity.GetID();

                if (memberid == memberId)
                {
                    var exhibition = MethodFactory.GetActiveExhibition(websiteid, _context);
                    if (exhibition != null)
                    {
                        string strInitialApprove = EnumConversion.ToString(EnumExhibitorStatus.InitialApproval);
                        string strApprove = EnumConversion.ToString(EnumExhibitorStatus.Approved);
                        var paymentAknowledgement = _context.XsiExhibitionMemberApplicationYearly.Where(p => p.MemberId == memberid && p.ExhibitionId == exhibition.ExhibitionId && (p.Status == strInitialApprove || p.Status == strApprove)).FirstOrDefault();
                        if (paymentAknowledgement != null)
                        {
                            PaymentAcknowledgementDTO dto1 = new PaymentAcknowledgementDTO();
                            if (LangId == 1)
                            {
                                dto1.DocumentOneName = "Bank Transfer Details";
                                dto1.DocumentTwoName = "Official Acknowledgement";
                                dto1.ExhibitorId = paymentAknowledgement.MemberExhibitionYearlyId;
                                var pagecontent = _context.XsiPagesContentNew.Where(p => p.ItemId == 10375 && p.IsActive == "Y" && p.IsDynamic == "Y").FirstOrDefault();
                                if (pagecontent != null)
                                {
                                    dto1.ContentTitle = pagecontent.PageTitle;
                                    dto1.ContentSubtitle = pagecontent.PageSubTitle;
                                    dto1.Content = pagecontent.PageContent;
                                }

                                dto1.DocumentURL1 = _appCustomSettings.UploadsCMSPath + "/PageContentNew/BankTransferDetails.xls";
                                dto1.DocumentURL2 = _appCustomSettings.UploadsCMSPath + "/PageContentNew/OfficialAcknowledgment.pdf";

                            }
                            else
                            {
                                dto1.DocumentOneName = "تفاصيل التحويل البنكي";
                                dto1.DocumentTwoName = "الإقرار الرسمي";

                                dto1.ExhibitorId = paymentAknowledgement.MemberExhibitionYearlyId;
                                var pagecontent = _context.XsiPagesContentNew.Where(p => p.ItemId == 10375 && p.IsActiveAr == "Y" && p.IsDynamic == "Y").FirstOrDefault();
                                if (pagecontent != null)
                                {
                                    dto1.ContentTitle = pagecontent.PageTitleAr;
                                    dto1.ContentSubtitle = pagecontent.PageSubTitleAr;
                                    dto1.Content = pagecontent.PageContentAr;
                                }
                                dto1.DocumentURL1 = _appCustomSettings.UploadsCMSPath + "/PageContentNew/BankTransferDetailsAr.xls";
                                dto1.DocumentURL2 = _appCustomSettings.UploadsCMSPath + "/PageContentNew/OfficialAcknowledgmentAr.pdf";

                            }
                            return Ok(new { MessageTypeResponse = "Success", PaymentAcknowledgementDTO = dto1 });
                        }
                        else
                        {
                            dto.MessageTypeResponse = "Error";
                            if (LangId == 1)
                                dto.Message = "Exhibitor not found";
                            else
                                dto.Message = "Exhibitor not found";
                            return Ok(dto);
                        }
                    }
                    else
                    {
                        dto.MessageTypeResponse = "Error";
                        if (LangId == 1)
                            dto.Message = "No active exhibition available";
                        else
                            dto.Message = "No active exhibition available";
                        return Ok(dto);
                    }
                }
                else
                {
                    dto.MessageTypeResponse = "Error";
                    if (LangId == 1)
                        dto.Message = "Member login expired or token mismatch";
                    else
                        dto.Message = "Member login expired or token mismatch";
                    return Ok(dto);
                }
            }
        }
        [HttpPost]
        [Route("acknowledgment/upload")]
        public ActionResult<dynamic> PaymentAcknowledment(PaymentAcknowledgementFormDTO paymentAcknowledgementFormDTO)
        {
            MessageDTO dto = new MessageDTO();
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long memberid = User.Identity.GetID();

                if (memberid == paymentAcknowledgementFormDTO.MemberId)
                {
                    string strInitialApprove = EnumConversion.ToString(EnumExhibitorStatus.InitialApproval);
                    string strApprove = EnumConversion.ToString(EnumExhibitorStatus.Approved);
                    var entity = _context.XsiExhibitionMemberApplicationYearly.Where(p => p.MemberExhibitionYearlyId == paymentAcknowledgementFormDTO.ExhibitorId && (p.Status == strInitialApprove || p.Status == strApprove)).FirstOrDefault();
                    if (entity != null)
                    {
                        if (paymentAcknowledgementFormDTO.DocumentURL1Base64 != null && paymentAcknowledgementFormDTO.DocumentURL1Base64.Length > 0)
                        {
                            string fileName = string.Empty;
                            byte[] imageBytes;
                            if (paymentAcknowledgementFormDTO.DocumentURL1Base64.Contains("data:"))
                            {
                                var strInfo = paymentAcknowledgementFormDTO.DocumentURL1Base64.Split(",")[0];
                                imageBytes = Convert.FromBase64String(paymentAcknowledgementFormDTO.DocumentURL1Base64.Split(',')[1]);
                            }
                            else
                            {
                                imageBytes = Convert.FromBase64String(paymentAcknowledgementFormDTO.DocumentURL1Base64);
                            }

                            fileName = string.Format("{0}_{1}{2}", LangId, MethodFactory.GetRandomNumber(), "." + paymentAcknowledgementFormDTO.DocumentURL1Ext);
                            System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\Acknowledgement\\") + fileName, imageBytes);
                            entity.AcknowledgementFileName1 = fileName;
                        }
                        if (paymentAcknowledgementFormDTO.DocumentURL2Base64 != null && paymentAcknowledgementFormDTO.DocumentURL2Base64.Length > 0)
                        {
                            string fileName = string.Empty;
                            byte[] imageBytes;
                            if (paymentAcknowledgementFormDTO.DocumentURL2Base64.Contains("data:"))
                            {
                                var strInfo = paymentAcknowledgementFormDTO.DocumentURL2Base64.Split(",")[0];
                                imageBytes = Convert.FromBase64String(paymentAcknowledgementFormDTO.DocumentURL2Base64.Split(',')[1]);
                            }
                            else
                            {
                                imageBytes = Convert.FromBase64String(paymentAcknowledgementFormDTO.DocumentURL2Base64);
                            }

                            fileName = string.Format("{0}_{1}{2}", LangId, MethodFactory.GetRandomNumber(), "." + paymentAcknowledgementFormDTO.DocumentURL2Ext);
                            System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\Acknowledgement\\") + fileName, imageBytes);
                            entity.AcknowledgementFileName2 = fileName;

                        }

                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes);

                        entity.CreatedOn = MethodFactory.ArabianTimeNow();
                        entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                        entity.CreatedBy = entity.MemberExhibitionYearlyId;
                        entity.ModifiedBy = entity.MemberExhibitionYearlyId;
                        _context.XsiExhibitionMemberApplicationYearly.Update(entity);
                        _context.SaveChanges();

                        long itemId = entity.MemberExhibitionYearlyId;
                        if (itemId > 0)
                        {
                            dto.MessageTypeResponse = "Success";
                            if (LangId == 1)
                                dto.Message = " submitted successfully.";
                            else
                                dto.Message = " submitted successfully.";
                            return Ok(dto);
                        }
                        else
                        {
                            dto.MessageTypeResponse = "Error";
                            if (LangId == 1)
                                dto.Message = "Error while submitting form. Please try again later after sometime.";
                            else
                                dto.Message = "Error while submitting form. Please try again later after sometime.";
                            return Ok(dto);
                        }
                    }
                    else
                    {
                        dto.MessageTypeResponse = "Error";
                        if (LangId == 1)
                            dto.Message = "Exhibitor not found";
                        else
                            dto.Message = "Exhibitor not found";
                        return Ok(dto);
                    }
                }
                else
                {
                    dto.MessageTypeResponse = "Error";
                    if (LangId == 1)
                        dto.Message = "Member login expired or token mismatch";
                    else
                        dto.Message = "Member login expired or token mismatch";
                    return Ok(dto);
                }
            }
        }

        [HttpPost]
        [Route("uploadpayment")]
        public ActionResult<dynamic> PostUploadPayment(UploadPayment itemDto)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    long memberid = User.Identity.GetID();
                    if (memberid == itemDto.MemberId)
                    {
                        XsiExhibitionMember member = _context.XsiExhibitionMember.Where(i =>
                                i.MemberId == memberid &&
                                i.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.Approved))
                            .FirstOrDefault();

                        if (!string.IsNullOrWhiteSpace(itemDto.TRNNumber))
                        {
                            using (ExhibitorRegistrationService exhibitorRegistrationService = new ExhibitorRegistrationService())
                            {
                                var exhibitor = new XsiExhibitionMemberApplicationYearly();
                                exhibitor = exhibitorRegistrationService.GetExhibitorRegistrationByItemId(itemDto.ExhibitorId);
                                exhibitor.Trn = itemDto.TRNNumber;
                                exhibitor.ModifiedOn = MethodFactory.ArabianTimeNow();
                                exhibitorRegistrationService.UpdateExhibitorRegistration(exhibitor);
                            }
                        }

                        XsiInvoice entity = _context.XsiInvoice.Where(x => x.ItemId == itemDto.InvoiceId).FirstOrDefault();
                        if (entity != null)
                        {
                            if (itemDto.FileBase64 != null && itemDto.FileBase64.Length > 0)
                            {
                                string fileName = string.Empty;
                                byte[] imageBytes;
                                if (itemDto.FileBase64.Contains("data:"))
                                {
                                    var strInfo = itemDto.FileBase64.Split(",")[0];
                                    imageBytes = Convert.FromBase64String(itemDto.FileBase64.Split(',')[1]);
                                }
                                else
                                {
                                    imageBytes = Convert.FromBase64String(itemDto.FileBase64);
                                }

                                fileName = string.Format("{0}_{1}{2}", itemDto.LanguageId, MethodFactory.GetRandomNumber(), "." + itemDto.FileExtension);
                                System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\PaymentReceipt\\") + fileName, imageBytes);

                                entity.UploadePaymentReceipt = fileName;
                            }
                            /*string filename = string.Format("{0}_{1}{2}", itemDto.LanguageId, MethodFactory.GetRandomNumber(), MethodFactory.GetFileExtention(postedFile.FileName));
                            postedFile.SaveAs(string.Format("{0}{1}", HttpContext.Current.Server.MapPath("/Content/Uploads/ExhibitorRegistration/PaymentReceipt/"), filename));
                            entity.UploadePaymentReceipt = filename;*/

                            entity.NoofParcel = itemDto.NoofParcel;
                            if (itemDto.PaymentTypeId == Convert.ToInt64(EnumPaymentType.PaidByBankTransfer))
                            {
                                if (itemDto.InvoiceNumberFromUser != null)
                                    entity.InvoiceNumberFromUser = itemDto.InvoiceNumberFromUser.Trim();
                                entity.BankName = itemDto.BankName;
                                entity.ReceiptNumber = string.Empty;
                                if (itemDto.TransferDate.HasValue)
                                    entity.TransferDate = Convert.ToDateTime(itemDto.TransferDate);
                            }
                            else if (itemDto.PaymentTypeId == Convert.ToInt64(EnumPaymentType.PaidByChequeTransfer))
                            {
                                entity.BankName = itemDto.BankName;
                                entity.ReceiptNumber = itemDto.ReceiptNumber;
                            }
                            else
                            {
                                entity.BankName = string.Empty;
                                entity.ReceiptNumber = string.Empty;
                            }
                            entity.Status = EnumConversion.ToString(EnumExhibitorStatus.ReceiptUploaded);
                            entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                            entity.PaymentTypeId = itemDto.PaymentTypeId;
                            _context.SaveChanges();
                            SendEmail(entity, memberid);
                            return Ok("success");
                        }
                    }
                    return Ok("Unauthorized access");
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        #region Private Methods
        private string GetInvoiceFormattedDate(DateTime invoiceDate)
        {
            if (invoiceDate != null)
            {
                return invoiceDate.ToString("dd-MMM-yyyy");
            }
            return string.Empty;
        }
        private bool IsExhibitionActive(long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibition entity = MethodFactory.GetActiveExhibition(websiteId, _context);
                if (entity != null)
                {
                    //  ExhibitionId = entity.ExhibitionId;
                    //if (entity.ParticipationFee != null)
                    //    TotalParticipationFee = entity.ParticipationFee;
                    //if (entity.PriceSqM != null)
                    //    PriceSqMeter = Convert.ToDouble(entity.PriceSqM);
                    //if (entity.ArabicAgencyFees != null)
                    //    ArabicAgencyFees = Convert.ToDouble(entity.ArabicAgencyFees);
                    //if (entity.ForeignAgencyFees != null)
                    //    ForeignAgencyFee = Convert.ToDouble(entity.ForeignAgencyFees);
                    //if (entity.VisaProcessingPrice != null)
                    //    RepresentativeFee = Convert.ToDouble(entity.VisaProcessingPrice);
                    #region Endate
                    if (entity.DateOfDue != null)
                    {
                        //  PaymentLastDate = entity.DateOfDue.Value.ToString("dddd d MMMM yyyy");
                        //if (entity.DateOfDue > MethodFactory.ArabianTimeNow())
                        //    PaymentRemainingDays = Math.Ceiling(entity.DateOfDue.Value.Subtract(MethodFactory.ArabianTimeNow()).TotalDays).ToString();
                        //else
                        //    PaymentRemainingDays = "0";
                    }
                    #endregion
                    return true;
                }
                return false;
            }
        }
        private void BindListing(PaymentDTO model, long langId, long exhibitorid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var predicate = PredicateBuilder.New<XsiInvoice>();
                predicate = predicate.And(i => i.ExhibitorId == exhibitorid);
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                List<XsiInvoice> invoiceList = _context.XsiInvoice.Where(predicate).ToList();

                if (invoiceList.Count() > 0)
                {
                    #region Tab1 Listing
                    string strPendingPayment = EnumConversion.ToString(EnumExhibitorStatus.PendingPayment);
                    string strReceiptUploaded = EnumConversion.ToString(EnumExhibitorStatus.ReceiptUploaded);
                    string strReceiptReviewed = EnumConversion.ToString(EnumExhibitorStatus.PaymentInProcess);
                    List<PendingPaymentDTO> Pending = invoiceList.Where(p => p.Status == strPendingPayment || p.Status == strReceiptUploaded || p.Status == strReceiptReviewed)
                                                        .Select(s => new PendingPaymentDTO
                                                        {
                                                            ItemId = s.ItemId,
                                                            InvoiceNumber = s.InvoiceNumber,
                                                            IsActive = s.IsActive,
                                                            ExhibitorId = s.ExhibitorId,
                                                            TotalAed = s.TotalAed,
                                                            //CreatedOn = s.CreatedOn.HasValue ? s.CreatedOn.Value.ToString("MMM dd, yyyy") : string.Empty,
                                                            CreatedOn = s.CreatedOn.HasValue ? GetDateFormated(s.CreatedOn.Value, langId) : string.Empty,
                                                            IsNewPending = s.IsNewPending,
                                                            Status = langId == 1 ? "In Process" : "قيد الإنجاز",//s.Status,
                                                            IsRegistrationInvoice = s.IsRegistrationInvoice
                                                        }).OrderBy(o => o.ItemId).ToList();
                    model.PendingList = Pending;
                    if (Pending.Count() > 0)
                    {
                        string strNewPending = EnumConversion.ToString(EnumBool.Yes);
                        int Counter = Pending.Where(p => p.IsNewPending == strNewPending).Count();
                        model.PendingCount = Counter;
                    }
                    else
                    {
                        model.Message = "There is no pending payment.";
                    }
                    #endregion
                    #region Tab2 Listing
                    string strPaid = EnumConversion.ToString(EnumExhibitorStatus.Paid);
                    string strPartiallyPaid = EnumConversion.ToString(EnumExhibitorStatus.PartiallyPaid);
                    var InvoiceList = invoiceList.Where(p => p.Status == strPaid || p.Status == strPartiallyPaid).ToList();
                    List<PaidPaymentDTO> Paid = InvoiceList.Select(s => new PaidPaymentDTO
                    {
                        ItemId = s.ItemId,
                        InvoiceNumber = s.InvoiceNumber,
                        IsActive = s.IsActive,
                        ExhibitorId = s.ExhibitorId,
                        TotalAed = s.TotalAed,
                        CreatedOn = s.CreatedOn.HasValue ? GetDateFormated(s.CreatedOn.Value, langId) : string.Empty,
                        //CreatedOn = s.CreatedOn.HasValue ? s.CreatedOn.Value.ToString("MMM dd, yyyy") : string.Empty,
                        IsNewPending = s.IsNewPending,
                        Status = langId == 1 ? "Paid" : "مدفوع",//s.Status,
                        PaymentType = s.PaymentTypeId == null ? "C" : GetPaymentType(s.PaymentTypeId.Value),
                        IsRegistrationInvoice = s.IsRegistrationInvoice
                    }).OrderBy(o => o.ItemId).ToList();

                    model.PaidList = Paid;
                    if (Paid.Count() > 0)
                    {
                        model.PaidCount = Paid.Count;
                    }
                    else
                    {
                        model.Message1 = "Data not available.";
                    }
                    #endregion
                }
                else
                {
                    model.Message = "There is no pending payment.";
                    model.Message1 = "Data not available.";
                }
            }
        }
        private string GetPaymentType(long id)
        {
            if (id == Convert.ToInt32(EnumPaymentType.PaidByBankTransfer))
                return "Bank Transfer";
            else if (id == Convert.ToInt32(EnumPaymentType.PaidByCash))
                return "Cash";
            else if (id == Convert.ToInt32(EnumPaymentType.PaidByChequeTransfer))
                return "Cheque";
            else if (id == Convert.ToInt32(EnumPaymentType.PaidByOnline))
                return "Paid Online";
            return "";
        }
        private void GetOverviewContent(PaymentDTO model, long langId, long exhibitorid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                double TotalAgencyFee = 0, TotalPenalty = 0, TotalAED = 0, TotalUSD = 0, TotalDiscount = 0, TotalPaid = 0, TotalRepresentativeFee = 0;
                long AgencyCount = 0;
                long totalAllocatedSpace = 0;

                var exibitor = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == exhibitorid).FirstOrDefault();
                var exibitorDetails = _context.XsiExhibitionExhibitorDetails.Where(i => i.MemberExhibitionYearlyId == exhibitorid).FirstOrDefault();
                var exhibition = _context.XsiExhibition.Where(i => i.ExhibitionId == exibitor.ExhibitionId).FirstOrDefault();

                if (!string.IsNullOrEmpty(exibitorDetails.AllocatedSpace))
                    totalAllocatedSpace = Convert.ToInt64(exibitorDetails.AllocatedSpace);

                model.TotalAllocatedSpace = totalAllocatedSpace;
                model.TotalParticipationFees = exhibition.ParticipationFee;
                model.TotalStandFee = (Convert.ToInt64(exhibition.PriceSqM) * totalAllocatedSpace).ToString();

                #region Agency Fee
                var predicate = PredicateBuilder.New<XsiExhibitionMemberApplicationYearly>();
                predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate.And(i => i.Status == EnumConversion.ToString(EnumAgencyStatus.SIBFApproved));
                predicate.And(i => i.MemberExhibitionYearlyId == exhibitorid);
                predicate.And(i => i.MemberRoleId == 2);
                List<XsiExhibitionMemberApplicationYearly> AgencyRegistrationGenericList = _context.XsiExhibitionMemberApplicationYearly.Where(predicate).ToList();
                AgencyCount = AgencyRegistrationGenericList.Count();
                #endregion

                #region Invoice
                var invoicepredicate = PredicateBuilder.New<XsiInvoice>();
                invoicepredicate = invoicepredicate.And(i => i.ExhibitorId == exhibitorid);
                invoicepredicate = invoicepredicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                List<XsiInvoice> invoiceList = _context.XsiInvoice.Where(invoicepredicate).ToList();
                string strPaid = EnumConversion.ToString(EnumExhibitorStatus.Paid);
                string strPartiallyPaid = EnumConversion.ToString(EnumExhibitorStatus.PartiallyPaid);
                var InvoiceList = invoiceList.Where(p => p.Status == strPaid || p.Status == strPartiallyPaid).ToList();

                if (InvoiceList != null)
                {
                    if (InvoiceList.Count() > 0)
                    {
                        string strRegistrationInvoice = EnumConversion.ToString(EnumBool.Yes);
                        XsiInvoice entity = InvoiceList.Where(p => p.IsRegistrationInvoice == strRegistrationInvoice).OrderByDescending(o => o.ItemId).FirstOrDefault();
                        if (entity != null)
                        {
                            if (entity.DueFromLastYearDebit != null)
                                model.DueFromLastYearDebit = entity.DueFromLastYearDebit;
                            if (entity.DueFromLastYearCredit != null)
                                model.DueFromLastYearCredit = entity.DueFromLastYearCredit;
                            if (entity.DueCurrentYear != null)
                                model.DueCurrentYear = entity.DueCurrentYear;
                            if (entity.DebitedCurrentYear != null)
                                model.DebitedCurrentYear = entity.DebitedCurrentYear;
                            foreach (XsiInvoice entity1 in InvoiceList)
                            {
                                if (entity1.Discount != null)
                                    if (!string.IsNullOrEmpty(entity1.Discount))
                                        TotalDiscount = TotalDiscount + Convert.ToDouble(entity1.Discount);
                                if (entity1.Penalty != null)
                                    if (!string.IsNullOrEmpty(entity1.Penalty))
                                        TotalPenalty = TotalPenalty + Convert.ToDouble(entity1.Penalty);

                                if (entity1.TotalAed != null)
                                    if (!string.IsNullOrEmpty(entity1.TotalAed))
                                        TotalAED = TotalAED + Convert.ToDouble(entity1.TotalAed);
                                if (entity1.TotalUsd != null)
                                    if (!string.IsNullOrEmpty(entity1.TotalUsd))
                                        TotalUSD = TotalUSD + Convert.ToDouble(entity1.TotalUsd);
                                if (entity1.PaidAmount != null)
                                    if (!string.IsNullOrEmpty(entity1.PaidAmount))
                                        TotalPaid = TotalPaid + Convert.ToDouble(entity1.PaidAmount);
                                if (entity1.AgencyFee != null)
                                    if (!string.IsNullOrEmpty(entity1.AgencyFee))
                                        TotalAgencyFee = TotalAgencyFee + Convert.ToDouble(entity1.AgencyFee);
                                if (entity1.RepresentativeFee != null)
                                    if (!string.IsNullOrEmpty(entity1.RepresentativeFee))
                                        TotalRepresentativeFee = TotalRepresentativeFee + Convert.ToDouble(entity1.RepresentativeFee);
                            }
                        }
                    }
                }
                #endregion

                if (AgencyCount > 0 && TotalAgencyFee > 0)
                {
                    if (langId == 1)
                        model.TotalAgencyFee = "<span>" + TotalAgencyFee.ToString() + " AED</span><span class=\"sep\">No. Of Representatives <b>" + AgencyCount.ToString() + "</b></span>";
                    else
                        model.TotalAgencyFee = "<span>" + TotalAgencyFee.ToString() + " درهم إماراتي</span><span class=\"sep\">عدد الوكالات <b>" + AgencyCount.ToString() + "</b></span>";
                }
                else if (TotalAgencyFee > 0)
                {
                    if (langId == 1)
                        model.TotalAgencyFee = TotalAgencyFee.ToString() + " AED";
                    else
                        model.TotalAgencyFee = TotalAgencyFee.ToString() + " درهم إماراتي";
                }
                if (TotalRepresentativeFee > 0)
                {
                    if (langId == 1)
                        model.TotalRepresentativeFee = TotalRepresentativeFee.ToString() + " AED";
                    else
                        model.TotalRepresentativeFee = TotalRepresentativeFee.ToString() + " درهم إماراتي";
                }
                model.TotalDiscount = TotalDiscount;
                model.TotalPenalty = TotalPenalty;
                model.TotalAED = TotalAED;
                model.TotalUSD = TotalUSD;
                model.TotalPaid = TotalPaid;
                model.TotalBalance = (TotalAED - TotalPaid);
            }
        }
        private bool AtleastOneInvoice(long exhibitorId, sibfnewdbContext _context)
        {
            string strCancelled = EnumConversion.ToString(EnumExhibitorStatus.Cancelled);
            string strRejected = EnumConversion.ToString(EnumExhibitorStatus.Rejected);
            string strNew = EnumConversion.ToString(EnumExhibitorStatus.New);

            var predicate = PredicateBuilder.New<XsiInvoice>();
            predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
            predicate.And(i => i.IsRegistrationInvoice == EnumConversion.ToString(EnumBool.Yes));
            predicate.And(i => i.ExhibitorId == exhibitorId);
            List<XsiInvoice> invoiceList = _context.XsiInvoice.Where(predicate).ToList();
            XsiInvoice entity = invoiceList.Where(p => p.Status != strCancelled && p.Status != strRejected && p.Status != null && p.Status != strNew).FirstOrDefault();
            if (entity != null)
                return true;
            return false;
        }
        private void UpdateStatusChange(long exhibitorid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string isActive = EnumConversion.ToString(EnumBool.Yes);
                XsiInvoice InvoiceEntity = _context.XsiInvoice.Where(x => x.ExhibitorId == exhibitorid && x.IsActive == isActive).FirstOrDefault();
                if (InvoiceEntity != null)
                {
                    InvoiceEntity.IsStatusChanged = EnumConversion.ToString(EnumBool.No);
                    _context.SaveChanges();
                }
            }
        }
        private dynamic GalaxyCustomerRegistrationInvoiceEnquiry(string filenumber)
        {
            string strtimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff"); //SSS fff

            GIGEncryption gIGEncryption = new GIGEncryption();
            GIGEncryptionMobile gIGEncryptionMobile = new GIGEncryptionMobile();

            string pwd = "G@123456";
            string tokenbeforeencryption = "Myahia&" + pwd + "&" + strtimestamp;
            var tokenwithencryption = gIGEncryptionMobile.encrypt(tokenbeforeencryption, "BE1A8B181E6ABDE36EBD0276C18AB995", "customersregestration"); //InvoiceStatusEnqiry000

            // string strmodel = "Operation=getInvoiceStatusEnqiry|transTimeStamp=" + strtimestamp + "|PublisherID=" + filenumber;.
            string strmodel = "GetOne|" + filenumber + "|" + strtimestamp;
            var strmodelencrypt = gIGEncryption.Encrypt(strmodel, "BE1A8B181E6ABDE36EBD0276C18AB995");

            // string url = "http://151.253.144.33:1001/api/InvoiceStatusEnqiry/" + tokenwithencryption + "?model=" + strmodelencrypt;
            string url = _appCustomSettings.RevenueSystemBillingAPI + "api/CustomersRegestration/" + tokenwithencryption + "?model=" + strmodelencrypt;

            WebClient client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            Stream data = client.OpenRead(url);
            StreamReader reader = new StreamReader(data);
            var responseFromServer = reader.ReadToEnd();
            dynamic myObject = JsonConvert.DeserializeObject<dynamic>(responseFromServer);
            var strdecrypt = gIGEncryption.Decrypt(myObject, "BE1A8B181E6ABDE36EBD0276C18AB995");
            data.Close();
            reader.Close();
            return strdecrypt;
        }
        private dynamic GalaxyInvoiceEnquiry(string filenumber)
        {
            string strtimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff"); //SSS fff

            GIGEncryption gIGEncryption = new GIGEncryption();
            GIGEncryptionMobile gIGEncryptionMobile = new GIGEncryptionMobile();

            string pwd = "G@123456";
            string tokenbeforeencryption = "Myahia&" + pwd + "&" + strtimestamp;
            var tokenwithencryption = gIGEncryptionMobile.encrypt(tokenbeforeencryption, "BE1A8B181E6ABDE36EBD0276C18AB995", "GetInvoicesStatus000"); //InvoiceStatusEnqiry000

            // string strmodel = "Operation=getInvoiceStatusEnqiry|transTimeStamp=" + strtimestamp + "|PublisherID=" + filenumber;.
            string strmodel = "getInvoicesStatus|" + filenumber + "|" + strtimestamp;
            var strmodelencrypt = gIGEncryption.Encrypt(strmodel, "BE1A8B181E6ABDE36EBD0276C18AB995");

            // string url = "http://151.253.144.33:1001/api/InvoiceStatusEnqiry/" + tokenwithencryption + "?model=" + strmodelencrypt;
            string url = _appCustomSettings.RevenueSystemBillingAPI + "api/GetInvoicesStatus/" + tokenwithencryption + "?model=" + strmodelencrypt;

            WebClient client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            Stream data = client.OpenRead(url);
            StreamReader reader = new StreamReader(data);
            var responseFromServer = reader.ReadToEnd();
            dynamic myObject = JsonConvert.DeserializeObject<dynamic>(responseFromServer);
            var strdecrypt = gIGEncryption.Decrypt(myObject, "BE1A8B181E6ABDE36EBD0276C18AB995");
            data.Close();
            reader.Close();
            return strdecrypt;
        }
        private static HttpResponseMessage TestMethodForAPIIntegration()
        {
            string strtimestamp = MethodFactory.ArabianTimeNow().ToString("yyyyMMddHHmmssfff"); //SSS fff

            GIGEncryption gIGEncryption = new GIGEncryption();
            GIGEncryptionMobile gIGEncryptionMobile = new GIGEncryptionMobile();

            // string pwd = "G@123456";
            string tokenbeforeencryption = "Myahia&G@123456&" + strtimestamp;

            var tokenwithencryption = gIGEncryptionMobile.encrypt(tokenbeforeencryption, "BE1A8B181E6ABDE36EBD0276C18AB995", "InvoiceStatusEnqiry000");

            string strmodel = "Operation=getInvoiceStatusEnqiry|transTimeStamp=" + strtimestamp + "|PublisherID=7942";
            var strmodelencrypt = gIGEncryption.Encrypt(strmodel, "BE1A8B181E6ABDE36EBD0276C18AB995");

            string url = "http://151.253.144.33:1001/api/InvoiceStatusEnqiry/" + tokenwithencryption + "?model=" + strmodelencrypt;
            HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, url);
            // httpRequest.Content = new StringContent(string.Empty, Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            Task<HttpResponseMessage> response = client.SendAsync(httpRequest);
            var result = response.Result;
            var strdecrypt = gIGEncryption.Decrypt(result.ToString(), "BE1A8B181E6ABDE36EBD0276C18AB995");
            return result;
        }
        #endregion
        #region Email
        private void SendEmail(XsiInvoice entity, long memberId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibitionMember entity1 = new XsiExhibitionMember();
                entity1 = _context.XsiExhibitionMember.Where(x => x.MemberId == memberId).FirstOrDefault();// ExhibitionMemberService.GetExhibitionMemberByItemId(Convert.ToInt64(bp.SIBFMemberId));
                if (entity1 != default(XsiExhibitionMember))
                {
                    var ExhibitorEntity = _context.XsiExhibitionMemberApplicationYearly.Where(x => x.MemberExhibitionYearlyId == entity.ExhibitorId).FirstOrDefault();// ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(entity.ExhibitorId.Value);
                    var websiteId = _context.XsiExhibition.Where(x => x.ExhibitionId == ExhibitorEntity.ExhibitionId.Value).FirstOrDefault().WebsiteId.Value;
                    #region Send Receipt Uploaded email to accountant
                    long LangId = 1;
                    string ToEmail = _appCustomSettings.AccountsEmail;
                    StringBuilder body = new StringBuilder();
                    #region Email Content
                    string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "";
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        //TODO need to work on
                        var scrfEmailContent = _context.XsiScrfemailContent.Where(x => x.ItemId == 82).FirstOrDefault();
                        if (scrfEmailContent != null)
                        {
                            if (scrfEmailContent.Body != null)
                                strEmailContentBody = scrfEmailContent.Body;
                            if (scrfEmailContent.Email != null)
                                strEmailContentEmail = scrfEmailContent.Email;
                            if (scrfEmailContent.Subject != null)
                                strEmailContentSubject = scrfEmailContent.Subject;
                        }
                    }
                    else
                    {
                        var emailContent = _context.XsiEmailContent.Where(x => x.ItemId == 20095).FirstOrDefault();
                        if (emailContent != null)
                        {
                            if (emailContent.Body != null)
                                strEmailContentBody = emailContent.Body;
                            if (emailContent.Email != null)
                                strEmailContentEmail = emailContent.Email;
                            if (emailContent.Subject != null)
                                strEmailContentSubject = emailContent.Subject;
                            // strEmailContentAdmin = AdminName;
                        }
                    }
                    #endregion
                    body.Append(BindEmailContent(LangId, strEmailContentBody, entity1, _appCustomSettings.ServerAddressNew, websiteId));


                    if (ExhibitorEntity.PublisherNameEn != null)
                        body.Replace("$$ExhibitorName$$", ExhibitorEntity.PublisherNameEn);
                    else
                        body.Replace("$$ExhibitorName$$", "");

                    if (ExhibitorEntity.ContactPersonName != null)
                        body.Replace("$$ContactPerson$$", ExhibitorEntity.ContactPersonName);
                    else
                        body.Replace("$$ContactPerson$$", "");

                    body.Replace("$$FileNumber$$", ExhibitorEntity.FileNumber);

                    //Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\PaymentReceipt\\") + fileName


                    List<string> MailAttachments = new List<string>();
                    if (!string.IsNullOrEmpty(entity.UploadePaymentReceipt))
                        MailAttachments.Add(string.Format("{0}{1}", _appCustomSettings.UploadsPath + "ExhibitorRegistration/LocationMap/", ExhibitorEntity.UploadLocationMap));


                    if (!string.IsNullOrEmpty(strEmailContentEmail))
                        _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString(), MailAttachments);
                    else
                        _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, ToEmail, strEmailContentSubject, body.ToString(), MailAttachments);
                    #endregion
                }
            }
        }
        private StringBuilder BindEmailContent(long languageId, string bodyContent, XsiExhibitionMember entity, string ServerAddress, long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string scrfURL = _appCustomSettings.ServerAddressSCRF;
                string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
                string EmailContent = string.Empty;
                if (languageId == 1)
                {
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                        EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRF.html";
                    else
                        EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
                }
                else
                {
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                        EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRF.html";
                    else
                        EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
                }
                StringBuilder body = new StringBuilder();

                if (System.IO.File.Exists(EmailContent))
                {
                    body.Append(System.IO.File.ReadAllText(EmailContent));
                    body.Replace("$$EmailContent$$", bodyContent);
                }
                else
                    body.Append(bodyContent);
                string str = MethodFactory.GetExhibitionDetails(websiteId, languageId, _context);
                string[] strArray = str.Split(',');
                body.Replace("$$exhibitionno$$", strArray[0].ToString());
                body.Replace("$$exhibitiondate$$", strArray[1].ToString());
                body.Replace("$$exhyear$$", strArray[2]);

                string swidth = "width=\"864\"";
                string sreplacewidth = "width=\"100%\"";
                body.Replace(swidth, sreplacewidth);

                body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
                body.Replace("$$ServerAddress$$", ServerAddress);
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    body.Replace("$$ServerAddressSCRF$$", scrfURL);

                if (entity != null)
                {
                    string exhibitionmembername = string.Empty;
                    if (languageId == 1)
                    {
                        if (entity.Firstname != null && entity.LastName != null)
                            exhibitionmembername = entity.Firstname + " " + entity.LastName;
                        else if (entity.Firstname != null)
                            exhibitionmembername = entity.Firstname;
                        else if (entity.LastName != null)
                            exhibitionmembername = entity.LastName;
                    }
                    else
                    {
                        if (entity.FirstNameAr != null && entity.LastNameAr != null)
                            exhibitionmembername = entity.FirstNameAr + " " + entity.LastNameAr;
                        else if (entity.Firstname != null)
                            exhibitionmembername = entity.FirstNameAr;
                        else if (entity.LastName != null)
                            exhibitionmembername = entity.LastNameAr;

                        if (string.IsNullOrEmpty(exhibitionmembername))
                        {
                            if (entity.Firstname != null && entity.LastName != null)
                                exhibitionmembername = entity.Firstname + " " + entity.LastName;
                            else if (entity.Firstname != null)
                                exhibitionmembername = entity.Firstname;
                            else if (entity.LastName != null)
                                exhibitionmembername = entity.LastName;
                        }
                    }
                    body.Replace("$$Name$$", exhibitionmembername);
                }
                return body;
            }
        }
        string GetDateFormated(DateTime date, long langId)
        {
            if (langId == 2)
                return date.ToString("MMM dd, yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
            else
                return date.ToString("MMM dd, yyyy");
        }
        #endregion
    }
}

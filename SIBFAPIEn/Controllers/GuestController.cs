﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Xsi.BusinessLogicLayer;
using System.Runtime.InteropServices;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class GuestController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;
        private IMemoryCache _cache;

        public GuestController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context, IMemoryCache memoryCache)
        {
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
            _cache = memoryCache;
        }

        // GET: api/Guest
        [HttpGet]
        [Route("{pageNumber}/{pageSize}/{strSearch}/{strtitle}/{strletter}")]
        public async Task<ActionResult<dynamic>> GetXsiGuest(int? pageNumber = 1, int? pageSize = 15, string strSearch = "", string strtitle = "", string strletter = "")
        {
            var List1 = new List<XsiGuests>();
            try
            {
                List<long> guestIDs = new List<long>();
                List<long> eventIDs = new List<long>();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var predicateGuest = PredicateBuilder.True<XsiGuests>();
                var predicateGuestWebsite = PredicateBuilder.True<XsiGuestWebsite>();
                if (LangId == 1)
                {
                    predicateGuest = predicateGuest.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateGuest = predicateGuest.And(i => i.ItemId != 44811);

                    if (!string.IsNullOrEmpty(strletter) && strletter != "-1")
                    {
                        strletter = strletter.ToLower();
                        predicateGuest = predicateGuest.And(i => i.GuestName.StartsWith(strletter));
                    }
                    else if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                    {
                        strSearch = strSearch.ToLower();
                        predicateGuest = predicateGuest.And(i => i.GuestName.Contains(strSearch, StringComparison.OrdinalIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(strtitle) && strtitle != "-1")
                    {
                        strtitle = strtitle.ToLower();
                        predicateGuest = predicateGuest.And(i => i.GuestTitle.Contains(strtitle, StringComparison.OrdinalIgnoreCase));
                    }

                    predicateGuestWebsite = predicateGuestWebsite.And(i => (i.WebsiteId == 1));
                    guestIDs = _context.XsiGuestWebsite.Where(predicateGuestWebsite).Select(i => i.GuestId.Value).Distinct().ToList();

                    if (guestIDs.Count > 0)
                    {
                        var predicateEvent = PredicateBuilder.True<XsiEvent>();

                        predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateEvent = predicateEvent.And(i => (i.Type == "A" || i.Type == "E" || i.Type == "B"));

                        eventIDs = _context.XsiEventWebsite.Where(i => i.WebsiteId == 1).Select(i => i.EventId.Value).ToList();
                        predicateEvent = predicateEvent.And(i => eventIDs.Contains(i.ItemId));

                        eventIDs = _context.XsiEvent.Where(predicateEvent).Select(i => i.ItemId).ToList();
                        guestIDs = _context.XsiEventGuest.Where(i => eventIDs.Contains(i.EventId.Value) && guestIDs.Contains(i.GuestId.Value)).Select(i => i.GuestId.Value).ToList();

                        if (guestIDs.Count > 0)
                            predicateGuest = predicateGuest.And(i => guestIDs.Contains(i.ItemId));
                    }

                    var List = await _context.XsiGuests.Where(predicateGuest)
                                         .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new GuestDTO()
                                         {
                                             ItemId = i.ItemId,
                                             GuestTitle = i.GuestTitle,
                                             GuestPhoto = _appCustomSettings.UploadsPath + "/guest/listthumbnailnew/" + (i.GuestPhoto ?? "guest.jpg"),
                                             GuestName = i.GuestName,
                                             Description = i.Description,
                                             URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                                         }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    var totalCount = await _context.XsiGuests.Where(predicateGuest).Select(i => i.ItemId).CountAsync();

                    var dto = new { List, TotalCount = totalCount, Message = "", MessageTypeResponse = "Success" };
                    return Ok(dto);
                }
                else
                {
                    predicateGuest = predicateGuest.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicateGuest = predicateGuest.And(i => i.ItemId != 44811);
                    if (!string.IsNullOrEmpty(strletter) && strletter != "-1")
                    {
                        strletter = strletter.ToLower();
                        predicateGuest = predicateGuest.And(i => i.GuestNameAr.StartsWith(strletter));
                    }
                    else if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                    {
                        strSearch = strSearch.ToLower();
                        predicateGuest = predicateGuest.And(i => i.GuestNameAr.Contains(strSearch, StringComparison.OrdinalIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(strtitle) && strtitle != "-1")
                    {
                        strtitle = strtitle.ToLower();
                        predicateGuest = predicateGuest.And(i => i.GuestTitleAr.Contains(strtitle, StringComparison.OrdinalIgnoreCase));
                    }

                    predicateGuestWebsite = predicateGuestWebsite.And(i => i.WebsiteId == 1);
                    guestIDs = _context.XsiGuestWebsite.Where(predicateGuestWebsite).Select(i => i.GuestId.Value).Distinct().ToList();

                    if (guestIDs.Count > 0)
                    {
                        predicateGuest = predicateGuest.And(i => guestIDs.Contains(i.ItemId));
                    }

                    if (guestIDs.Count > 0)
                    {
                        var predicateEvent = PredicateBuilder.True<XsiEvent>();

                        predicateEvent = predicateEvent.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                        predicateEvent = predicateEvent.And(i => (i.Type == "A" || i.Type == "E" || i.Type == "B"));

                        eventIDs = _context.XsiEventWebsite.Where(i => i.WebsiteId == 1).Select(i => i.EventId.Value).ToList();
                        predicateEvent = predicateEvent.And(i => eventIDs.Contains(i.ItemId));

                        eventIDs = _context.XsiEvent.Where(predicateEvent).Select(i => i.ItemId).ToList();
                        guestIDs = _context.XsiEventGuest.Where(i => eventIDs.Contains(i.EventId.Value) && guestIDs.Contains(i.GuestId.Value)).Select(i => i.GuestId.Value).ToList();

                        if (guestIDs.Count > 0)
                            predicateGuest = predicateGuest.And(i => guestIDs.Contains(i.ItemId));
                    }

                    var List = await _context.XsiGuests.AsExpandable().Where(predicateGuest)
                        .OrderByDescending(o => o.ItemId).Select(i => new GuestDTO()
                        {
                            ItemId = i.ItemId,
                            GuestTitle = i.GuestTitleAr,
                            GuestPhoto = _appCustomSettings.UploadsPath + "/guest/ListThumbnailNew/" + (i.GuestPhotoAr ?? "guest.jpg"),
                            GuestName = i.GuestNameAr,
                            //  Events = "",
                            Description = i.DescriptionAr,
                            URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                        }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    var totalCount = await _context.XsiGuests.AsExpandable().Where(predicateGuest).Select(i => i.ItemId).CountAsync();

                    var dto = new { List, TotalCount = totalCount, Message = "", MessageTypeResponse = "Success" };
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiGuest action: {ex.InnerException}");
                return Ok(new { List1, TotalCount = 0, Message = "Exception: Something went wrong. Please retry again later.", MessageTypeResponse = "Error" });
            }
        }

        // GET: api/Guest/5
        [HttpGet("{id}")]
        public async Task<ActionResult<dynamic>> GetXsiGuest(long id)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiGuests>();
                    predicate = predicate.And(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    //predicateEvent = predicateEvent.And(i => i.WebsiteType == 0 || i.WebsiteType == 1);
                    var dto = await _context.XsiGuests.Where(predicate).Select(i => new GuestDTO()
                    {
                        ItemId = i.ItemId,
                        GuestTitle = i.GuestTitle,
                        GuestPhoto = _appCustomSettings.UploadsCMSPath + "/guest/listthumbnailnew/" + (i.GuestPhoto ?? "guest.jpg"),
                        GuestName = i.GuestName,
                        //  Events = "",
                        Description = i.Description,
                        URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                    }).FirstOrDefaultAsync();

                    if (dto == null)
                    {
                        return NotFound();
                    }

                    return Ok(dto);
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiGuests>();
                    predicate = predicate.And(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    //predicateEvent = predicateEvent.And(i => i.WebsiteType == 0 || i.WebsiteType == 1);
                    var dto = await _context.XsiGuests.Where(predicate).Select(i => new GuestDTO()
                    {
                        ItemId = i.ItemId,
                        GuestTitle = i.GuestTitleAr,
                        GuestPhoto = _appCustomSettings.UploadsCMSPath + "/guest/listthumbnailnew/" + (i.GuestPhotoAr ?? "guest.jpg"),
                        GuestName = i.GuestNameAr,
                        //  Events = "",
                        Description = i.DescriptionAr,
                        URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                    }).FirstOrDefaultAsync();

                    if (dto == null)
                    {
                        return NotFound();
                    }

                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiGuest action: {ex.InnerException}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }


        // POST: api/Guest
        [HttpPost]
        public async Task<ActionResult<dynamic>> GetXsiGuestSearch(GuestSearchDTO userdto)
        {
            var List1 = new List<XsiGuests>();
            try
            {
                //int? pageNumber = 1, int? pageSize = 15, string strSearch = "", string strtitle = "", string strletter = ""
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var predicateGuest = PredicateBuilder.New<XsiGuests>();
                var predicateGuestWebsite = PredicateBuilder.New<XsiGuestWebsite>();
                List<long> guestIDs = new List<long>();
                List<long> eventIDs = new List<long>();

                if (LangId == 1)
                {
                    predicateGuest = predicateGuest.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateGuest = predicateGuest.And(i => i.ItemId != 44811);
                    if (!string.IsNullOrEmpty(userdto.Strletter) && userdto.Strletter != "-1")
                    {
                        userdto.Strletter = userdto.Strletter.ToLower();
                        predicateGuest = predicateGuest.And(i => i.GuestName.StartsWith(userdto.Strletter));
                    }
                    else if (!string.IsNullOrEmpty(userdto.StrSearch) && userdto.StrSearch != "-1")
                    {
                        userdto.StrSearch = userdto.StrSearch.ToLower();
                        var likeExpression = "%" + userdto.StrSearch + "%";
                        predicateGuest = predicateGuest.And(i => EF.Functions.Like(i.GuestName, likeExpression));

                        //userdto.StrSearch = userdto.StrSearch.ToLower();
                        //predicateGuest = predicateGuest.And(i => i.GuestName.Contains(userdto.StrSearch, StringComparison.OrdinalIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(userdto.Strtitle) && userdto.Strtitle != "-1")
                    {
                        userdto.Strtitle = userdto.Strtitle.ToLower();
                        var likeExpression = "%" + userdto.Strtitle + "%";
                        predicateGuest = predicateGuest.And(i => EF.Functions.Like(i.GuestTitle, likeExpression));

                        //userdto.Strtitle = userdto.Strtitle.ToLower();
                        //predicateGuest = predicateGuest.And(i => i.GuestTitle.Contains(userdto.Strtitle, StringComparison.OrdinalIgnoreCase));
                    }

                    predicateGuestWebsite = predicateGuestWebsite.And(i => i.GuestId != null && (i.WebsiteId == 1));

                    guestIDs = _context.XsiGuestWebsite.Where(predicateGuestWebsite).Select(i => i.GuestId.Value).Distinct().ToList();

                    if (guestIDs.Count > 0)
                        predicateGuest = predicateGuest.And(i => guestIDs.Contains(i.ItemId));

                    //if (guestIDs.Count > 0)
                    //{
                    //    var predicateEvent = PredicateBuilder.True<XsiEvent>();

                    //    predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    //    predicateEvent = predicateEvent.And(i => (i.Type == "A" || i.Type == "E" || i.Type == "B"));

                    //    eventIDs = _context.XsiEventWebsite.Where(i => i.WebsiteId == 1).Select(i => i.EventId.Value).ToList();
                    //    predicateEvent = predicateEvent.And(i => eventIDs.Contains(i.ItemId));

                    //    eventIDs = _context.XsiEvent.Where(predicateEvent).Select(i => i.ItemId).ToList();
                    //    var eventguestIDs = _context.XsiEventGuest.Where(i => eventIDs.Contains(i.EventId.Value) && guestIDs.Contains(i.GuestId.Value)).Select(i => i.GuestId.Value).ToList();

                    //    if (eventguestIDs.Count > 0)
                    //        predicateGuest = predicateGuest.And(i => eventguestIDs.Contains(i.ItemId));
                    //    else
                    //        predicateGuest = predicateGuest.And(i => guestIDs.Contains(i.ItemId));
                    //}

                    var List = await _context.XsiGuests.Where(predicateGuest)
                                         .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new GuestDTO()
                                         {
                                             ItemId = i.ItemId,
                                             GuestTitle = i.GuestTitle,
                                             GuestPhoto = _appCustomSettings.UploadsPath + "/guest/listthumbnailnew/" + (i.GuestPhoto ?? "guest.jpg"),
                                             GuestName = i.GuestName,
                                             Description = i.Description,
                                             URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                                         }).Skip((userdto.PageNumber.Value - 1) * userdto.PageSize.Value).Take(userdto.PageSize.Value).ToListAsync();

                    var totalCount = await _context.XsiGuests.Where(predicateGuest).Select(i => i.ItemId).CountAsync();

                    var dto = new { List, TotalCount = totalCount, Message = "", MessageTypeResponse = "Success" };
                    return Ok(dto);
                }
                else
                {
                    predicateGuest = predicateGuest.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicateGuest = predicateGuest.And(i => i.ItemId != 44811);
                    if (!string.IsNullOrEmpty(userdto.Strletter) && userdto.Strletter != "-1")
                    {
                        userdto.Strletter = userdto.Strletter.ToLower();
                        var likeExpression = userdto.Strletter + "%";
                        predicateGuest = predicateGuest.And(i => EF.Functions.Like(i.GuestNameAr, likeExpression)); // i.GuestNameAr.StartsWith(userdto.Strletter));
                    }
                    else if (!string.IsNullOrEmpty(userdto.StrSearch) && userdto.StrSearch != "-1")
                    {
                        userdto.StrSearch = userdto.StrSearch.ToLower();
                        var likeExpression = "%" + userdto.StrSearch + "%";
                        predicateGuest = predicateGuest.And(i => EF.Functions.Like(i.GuestNameAr, likeExpression));

                        //userdto.StrSearch = userdto.StrSearch.ToLower();
                        //predicateGuest = predicateGuest.And(i => i.GuestNameAr.Contains(userdto.StrSearch, StringComparison.OrdinalIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(userdto.Strtitle) && userdto.Strtitle != "-1")
                    {
                        userdto.Strtitle = userdto.Strtitle.ToLower();
                        var likeExpression = "%" + userdto.Strtitle + "%";
                        predicateGuest = predicateGuest.And(i => EF.Functions.Like(i.GuestTitleAr, likeExpression));
                        // predicateGuest = predicateGuest.And(i => i.GuestTitleAr.Contains(userdto.Strtitle, StringComparison.OrdinalIgnoreCase));
                    }

                    predicateGuestWebsite = predicateGuestWebsite.And(i => i.GuestId != null && (i.WebsiteId == 1));
                    guestIDs = _context.XsiGuestWebsite.Where(predicateGuestWebsite).Select(i => i.GuestId.Value).Distinct().ToList();

                    if (guestIDs.Count > 0)
                        predicateGuest = predicateGuest.And(i => guestIDs.Contains(i.ItemId));

                    //if (guestIDs.Count > 0)
                    //{
                    //    var predicateEvent = PredicateBuilder.True<XsiEvent>();

                    //    predicateEvent = predicateEvent.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    //    predicateEvent = predicateEvent.And(i => (i.Type == "A" || i.Type == "E" || i.Type == "B"));

                    //    eventIDs = _context.XsiEventWebsite.Where(i => i.WebsiteId == 1).Select(i => i.EventId.Value).ToList();
                    //    predicateEvent = predicateEvent.And(i => eventIDs.Contains(i.ItemId));

                    //    eventIDs = _context.XsiEvent.Where(predicateEvent).Select(i => i.ItemId).ToList();
                    //    var eventguestIDs = _context.XsiEventGuest.Where(i => eventIDs.Contains(i.EventId.Value) && guestIDs.Contains(i.GuestId.Value)).Select(i => i.GuestId.Value).ToList();


                    //    if (eventguestIDs.Count > 0)
                    //        predicateGuest = predicateGuest.And(i => eventguestIDs.Contains(i.ItemId));
                    //    else
                    //        predicateGuest = predicateGuest.And(i => guestIDs.Contains(i.ItemId));
                    //}

                    var List = await _context.XsiGuests.AsExpandable().Where(predicateGuest)
                        .OrderByDescending(o => o.ItemId).Select(i => new GuestDTO()
                        {
                            ItemId = i.ItemId,
                            GuestTitle = i.GuestTitleAr,
                            GuestPhoto = _appCustomSettings.UploadsPath + "/guest/listthumbnailnew/" + (i.GuestPhotoAr ?? "guest.jpg"),
                            GuestName = i.GuestNameAr,
                            //  Events = "",
                            Description = i.DescriptionAr,
                            URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                        }).Skip((userdto.PageNumber.Value - 1) * userdto.PageSize.Value).Take(userdto.PageSize.Value).ToListAsync();

                    var totalCount = await _context.XsiGuests.AsExpandable().Where(predicateGuest).Select(i => i.ItemId).CountAsync();

                    var dto = new { List, TotalCount = totalCount, Message = "", MessageTypeResponse = "Success" };
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiGuest action: {ex.InnerException}");
                return Ok(new { List1, TotalCount = 0, Message = "Exception: Something went wrong. Please retry again later.", MessageTypeResponse = "Error" });
            }
        }
    }
}

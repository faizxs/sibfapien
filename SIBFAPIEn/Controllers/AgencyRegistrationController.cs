﻿using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using OfficeOpenXml;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xsi.BusinessLogicLayer;
using Xsi.ServicesLayer;
//using Entities.Models;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AgencyRegistrationController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly IStringLocalizer<AgencyRegistrationController> _stringLocalizer;

        List<XsiExhibitionLanguage> BookLanguageList;
        List<XsiExhibitionBookSubject> BookSubjectList;
        List<XsiExhibitionBookSubsubject> BookSubsubjectList;
        private string StrEmailContentBody { get; set; }
        private string StrEmailContentEmail { get; set; }
        private string StrEmailContentSubject { get; set; }
        private string StrEmailContentAdmin { get; set; }

        public AgencyRegistrationController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings,
            IStringLocalizer<AgencyRegistrationController> stringLocalizer, IEmailSender emailSender)
        {
            _logger = logger;
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
            _stringLocalizer = stringLocalizer;
        }

        #region Get
        [HttpGet("{websiteId}/{memberid}")]
        public ActionResult<dynamic> GetAgency(long websiteId, long memberid)
        {
            using (var ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                try
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    string Message = "";
                    long SIBFMemberId = User.Identity.GetID();
                    if (SIBFMemberId == memberid)
                    {
                        AgencyRegistrationDTO model = new AgencyRegistrationDTO();
                        using (sibfnewdbContext _context = new sibfnewdbContext())
                        {
                            model.WebsiteId = websiteId;
                            model.MemberId = memberid;
                            var exhibition = MethodFactory.GetActiveExhibition(websiteId, _context);
                            if (exhibition != null)
                            {
                                model.ExhibitionId = exhibition.ExhibitionId;
                                if (_context.XsiExhibitionMemberApplicationYearly.Any(i => i.MemberRoleId != 2 && i.MemberId == memberid && i.ExhibitionId == exhibition.ExhibitionId))
                                {
                                    model.StepId = 1;
                                    if (LangId == 1)
                                        model.Message = "User already registered for this exhibition as Exhibitor or Restaurant.";
                                    else
                                        model.Message = "User already registered for this exhibition as Exhibitor or Restaurant.";
                                    var dto1 = new { data = model != null ? model : null, message = Message };

                                    return Ok(dto1);
                                }
                                if ((exhibition.AgencyStartDate < MethodFactory.ArabianTimeNow() && exhibition.AgencyEndDate > MethodFactory.ArabianTimeNow()) || MethodFactory.AgencyInRegistrationProcess(SIBFMemberId, websiteId))
                                {
                                    string strExhibitorReject = EnumConversion.ToString(EnumAgencyStatus.ExhibitorReject);
                                    string strSIBFReject = EnumConversion.ToString(EnumAgencyStatus.SIBFReject);
                                    XsiExhibitionMemberApplicationYearly agency = GetCurrentYearAgency(exhibition.ExhibitionId, memberid);

                                    if (agency != null)
                                    {
                                        if (agency.Status == strExhibitorReject || agency.Status == strSIBFReject)
                                            model = CreateInterface(EnumRegistrationViewType.Reject, model, agency.MemberExhibitionYearlyId, LangId);
                                        else
                                        {
                                            model = GetAgencyView(memberid, websiteId, exhibition, LangId);
                                            if (model.AgencyId > 0)
                                                UpdateStatusChange(model.AgencyId);
                                        }
                                    }
                                    else
                                    {
                                        model = GetAgencyView(memberid, websiteId, exhibition, LangId);
                                        if (model.AgencyId > 0)
                                            UpdateStatusChange(model.AgencyId);
                                    }
                                }
                                else
                                    model = CreateInterface(EnumRegistrationViewType.RegistrationExpired, model, LangId);
                            }
                            else
                                model = CreateInterface(EnumRegistrationViewType.ExhibitionUnavailableView, model, LangId);

                            return Ok(model);
                        }
                    }
                    return Unauthorized();
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Something went wrong inside GetAgency action: {ex.InnerException}");
                    return Ok("Something went wrong. Please try again later.");
                }
            }
        }

        [HttpGet]
        [Route("ExhibitorAutoComplete/{websiteId}/{term}")]
        public ActionResult<dynamic> GetExhibitorAutoComplete(long websiteId, string term = "-1")
        {

            try
            {
                using (var ExhibitorRegistrationService = new ExhibitorRegistrationService())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    string strsearch = string.Empty;

                    if (term != "-1")
                        strsearch = term;

                    List<GetExhibitorsResult> exhibitors = new List<GetExhibitorsResult>();
                    var exhibition = MethodFactory.GetExhibition(websiteId);
                    if (exhibition != null)
                    {
                        exhibitors = ExhibitorRegistrationService.GetExhibitorsForAutoComplete(strsearch, LangId, exhibition.ExhibitionId)
                         .Select(s => new GetExhibitorsResult
                         {
                             ItemId = s.ItemId,
                             ExhibitorArabic = s.PublisherNameArabic + ", " + s.CountryNameAr,
                             ExhibitorEnglish = s.PublisherNameEnglish + ", " + s.CountryName
                         }).ToList();
                    }
                    return exhibitors;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetExhibitorAutoComplete action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
        #endregion
        #region Post
        // Post: api/AgencyRegistration
        [HttpPost]
        [Route("StepThree")]
        public ActionResult<dynamic> AgencyRegistration(SaveAgencyDataForStepTwoAndThreeDTO formDto)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long SIBFMemberId = User.Identity.GetID();
                if (SIBFMemberId == formDto.MemberId)
                {
                    AgencyRegistrationDTO model = new AgencyRegistrationDTO();
                    model.MemberId = formDto.MemberId;
                    model.WebsiteId = formDto.WebsiteId;
                    model.ExhibitionId = formDto.ExhibitionId;
                    model.ExhibitorId = formDto.ExhibitorId;
                    long NewTitles = Convert.ToInt64(formDto.TotalNumberOfNewTitles);
                    long TotalTitles = Convert.ToInt64(formDto.TotalNumberOfTitles);

                    if (NewTitles > TotalTitles)
                    {
                        model.StepId = 3;
                        if (LangId == 1)
                            model.Message = "Total number of new titles should be less than total number of titles";
                        else
                            model.Message = "عدد العناوين الجديدة يجب أن يكون أقل من عدد العناوين الكلية";
                        return Ok(model);
                    }
                    if (!IsExhibitorNameExistByItemId(formDto))
                    {
                        model.StepId = 3;
                        if (LangId == 1)
                            model.Message = "Please select the exhibitor from the dropdown list.";
                        else
                            model.Message = "الرجاء اختيار العارض من القائمة المنسدلة";
                        return Ok(model);
                    }

                    if (formDto.AgencyId > 0 && formDto.IsEditRequest)
                    {
                        model.StepId = 3;
                        UpdateRegistration(formDto, LangId);
                        model = CreateInterface(EnumRegistrationViewType.ThankYou, model, formDto.AgencyId, LangId);
                        model.BookMessageResponse = ImportBooks(formDto.AgencyId, model.ExhibitorId.Value, model.MemberId.Value, formDto, LangId);
                        return Ok(model);
                    }
                    else
                    {
                        #region Validations for Add
                        if (formDto.AuhtorizationLetterBase64 == null || string.IsNullOrEmpty(formDto.AuhtorizationLetterBase64) || formDto.AuhtorizationLetterBase64.Length == 0)
                        {
                            model.StepId = 3;
                            if (LangId == 1)
                                model.Message = "Authorization letter is mandatory";
                            else
                                model.Message = "Authorization letter is mandatory";
                            return Ok(model);
                        }

                        //if (IsBooksRequired(model.MemberId.Value) && string.IsNullOrEmpty(formDto.excelFileBase64))
                        //{
                        //    model.StepId = 3;
                        //    var message = string.Empty;
                        //    if (LangId == 1)
                        //        message = @"Upload Books field is missing";
                        //    else
                        //        message = @"Upload Books field is missing";

                        //    return Ok(model);
                        //} 
                        #endregion

                        #region Save
                        model.StepId = 3;
                        model.AgencyId = SaveRegistration(formDto, LangId);
                        if (model.AgencyId > 0)
                        {
                            model.BookMessageResponse = ImportBooks(model.AgencyId, model.ExhibitorId.Value, model.MemberId.Value, formDto, LangId);
                            model = CreateInterface(EnumRegistrationViewType.ThankYou, model, model.AgencyId, LangId);
                            SendEmail(model);
                            if (LangId == 1)
                                model.Message = "Agency application submitted successfully";
                            else
                                model.Message = "تم تقديم طلب إشتراك التوكيل بنجاح";
                            return Ok(model);
                        }

                        model.Message = "Error submitting form.";
                        return Ok(model);

                        #endregion
                    }
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Post Step Three action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        //[HttpPost]
        //[Route("ThankyouBack")]
        //public ActionResult<dynamic> Step4ThankyouBack_Click(AgencyRegistrationDTO agencyRegistrationDto)
        //{
        //    try
        //    {
        //        CreateInterface(EnumRegistrationViewType.Step3);
        //        LoadAgencyContent(agencyRegistrationDto);
        //        return Ok(model);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError($"Something went wrong inside StepThreeback action: {ex.InnerException}");
        //        return Ok("Something went wrong. Please try again later.");
        //    }
        //}
        #endregion

        #region Methods
        bool IsBooksRequired(long memberId)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                var memberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(memberId);
                if (memberentity != null)
                {
                    return (memberentity.IsBooksRequired == "Y") ? true : false;
                }
            }
            return false;
        }
        private bool IsExhibitorNameExistByItemId(SaveAgencyDataForStepTwoAndThreeDTO dto)
        {
            return true;
            long ParentId = Convert.ToInt64(dto.ExhibitorId);
            string exhibitorNameEn = dto.ExhibitorNameEn;
            string exhibitorNameAr = dto.ExhibitorNameAr;
            if (exhibitorNameEn.IndexOf(',') > -1)
                exhibitorNameEn = exhibitorNameEn.Substring(0, exhibitorNameEn.LastIndexOf(','));
            if (exhibitorNameAr.IndexOf(',') > -1)
                exhibitorNameAr = exhibitorNameAr.Substring(0, exhibitorNameAr.LastIndexOf(','));
            using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
            {
                var entity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(ParentId);
                if (entity != null)
                {
                    if (entity.PublisherNameEn != null && entity.PublisherNameAr != null)
                    {
                        if (exhibitorNameAr == entity.PublisherNameAr.Trim().ToLower() && exhibitorNameEn == entity.PublisherNameEn.Trim().ToLower())
                            return true;
                    }
                }
            }
            return false;
        }

        void SendEmailBasedOnEdit(XsiExhibitionMemberApplicationYearly OldAgencyEntity, XsiExhibitionMemberApplicationYearly newEntity, AgencyRegistrationDTO agencyRegistrationDto)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                EmailContentService EmailContentService = new EmailContentService();
                SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();

                XsiExhibitionMember ExhibitionMemberEntity = new XsiExhibitionMember();
                ExhibitionMemberEntity = ExhibitionMemberService.GetExhibitionMemberByItemId(Convert.ToInt64(newEntity.MemberId));
                if (ExhibitionMemberEntity != default(XsiExhibitionMember))
                {
                    #region Send Edit Request Email
                    StringBuilder body = new StringBuilder();
                    #region Email Content
                    if (agencyRegistrationDto.WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        var ScrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(10013);
                        if (ScrfEmailContent != null)
                        {
                            if (ScrfEmailContent.Body != null)
                                StrEmailContentBody = ScrfEmailContent.Body;
                            if (ScrfEmailContent.Email != null)
                                StrEmailContentEmail = ScrfEmailContent.Email;
                            if (ScrfEmailContent.Subject != null)
                                StrEmailContentSubject = ScrfEmailContent.Subject;
                            StrEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                        }
                    }
                    else
                    {
                        var EmailContent = EmailContentService.GetEmailContentByItemId(10013);
                        if (EmailContent != null)
                        {
                            if (EmailContent.Body != null)
                                StrEmailContentBody = EmailContent.Body;
                            if (EmailContent.Email != null)
                                StrEmailContentEmail = EmailContent.Email;
                            if (EmailContent.Subject != null)
                                StrEmailContentSubject = EmailContent.Subject;
                            StrEmailContentAdmin = _appCustomSettings.AdminName;
                        }
                    }
                    #endregion
                    sibfnewdbContext _context = new sibfnewdbContext();

                    body.Append(htmlContentFactory.BindEmailContent(1, StrEmailContentBody, ExhibitionMemberEntity, _appCustomSettings.ServerAddressNew, agencyRegistrationDto.WebsiteId.Value, _appCustomSettings));
                    string strContent = "<p style=\"font-size: 16px; line-height: 32px; font-family: Arial, Helvetica, sans-serif; color: #555555; margin: 0px;\">$$Label$$ : $$Value$$</p>";
                    if (OldAgencyEntity != null && newEntity != null)
                    {
                        #region Body
                        bool IsAnyFieldChanged = false;
                        #region Publisher English
                        if (OldAgencyEntity.PublisherNameEn != newEntity.PublisherNameEn)
                        {
                            if (newEntity.PublisherNameEn != null)
                            {
                                body.Replace("$$NewPublisherNameEn$$", strContent);
                                body.Replace("$$Label$$", "Agency Name in English");
                                body.Replace("$$Value$$", newEntity.PublisherNameEn);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewPublisherNameEn$$", string.Empty);
                            if (OldAgencyEntity.PublisherNameEn != null)
                            {
                                body.Replace("$$OldPublisherNameEn$$", strContent);
                                body.Replace("$$Label$$", "Agency Name in English");
                                body.Replace("$$Value$$", OldAgencyEntity.PublisherNameEn);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldPublisherNameEn$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewPublisherNameEn$$", string.Empty);
                            body.Replace("$$OldPublisherNameEn$$", string.Empty);
                        }
                        #endregion
                        #region Publisher Arabic
                        if (OldAgencyEntity.PublisherNameAr != newEntity.PublisherNameAr)
                        {
                            if (newEntity.PublisherNameAr != null)
                            {
                                body.Replace("$$NewPublisherNameAr$$", strContent);
                                body.Replace("$$Label$$", "Agency Name in Arabic");
                                body.Replace("$$Value$$", newEntity.PublisherNameAr);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewPublisherNameAr$$", string.Empty);
                            if (OldAgencyEntity.PublisherNameAr != null)
                            {
                                body.Replace("$$OldPublisherNameAr$$", strContent);
                                body.Replace("$$Label$$", "Agency Name in Arabic");
                                body.Replace("$$Value$$", OldAgencyEntity.PublisherNameAr);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldPublisherNameAr$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewPublisherNameAr$$", string.Empty);
                            body.Replace("$$OldPublisherNameAr$$", string.Empty);
                        }
                        #endregion
                        #region Exhibitor Name En
                        if (OldAgencyEntity.ParentId != newEntity.ParentId)
                        {
                            if (newEntity.ParentId != null)
                            {
                                body.Replace("$$NewExhibitorNameEn$$", strContent);
                                body.Replace("$$Label$$", "Exhibitor name in English");
                                var getPublisherName = GetPublisherName(newEntity.ParentId.Value, 1);
                                if (!string.IsNullOrEmpty(getPublisherName))
                                {
                                    body.Replace("$$Value$$", getPublisherName);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewExhibitorNameEn$$", string.Empty);
                            }
                            else
                                body.Replace("$$NewExhibitorNameEn$$", string.Empty);
                            if (OldAgencyEntity.ParentId != null)
                            {
                                body.Replace("$$OldExhibitorNameEn$$", strContent);
                                body.Replace("$$Label$$", "Exhibitor name in English");
                                var getPublisherName = GetPublisherName(newEntity.ParentId.Value, 1);
                                if (!string.IsNullOrEmpty(getPublisherName))
                                {
                                    body.Replace("$$Value$$", getPublisherName);
                                    IsAnyFieldChanged = true;
                                }
                            }
                            else
                                body.Replace("$$OldExhibitorNameEn$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewExhibitorNameEn$$", string.Empty);
                            body.Replace("$$OldExhibitorNameEn$$", string.Empty);
                        }
                        #endregion
                        #region Exhibitor Name Ar
                        if (OldAgencyEntity.ParentId != newEntity.ParentId)
                        {
                            if (newEntity.ParentId != null)
                            {
                                body.Replace("$$NewExhibitorNameAr$$", strContent);
                                body.Replace("$$Label$$", "Exhibitor name in Arabic");
                                var getPublisherName = GetPublisherName(newEntity.ParentId.Value, 2);
                                if (!string.IsNullOrEmpty(getPublisherName))
                                {
                                    body.Replace("$$Value$$", getPublisherName);
                                    IsAnyFieldChanged = true;
                                }
                            }
                            else
                                body.Replace("$$NewExhibitorNameAr$$", string.Empty);
                            if (OldAgencyEntity.ParentId != null)
                            {
                                body.Replace("$$OldExhibitorNameAr$$", strContent);
                                body.Replace("$$Label$$", "Exhibitor name in Arabic");
                                var getPublisherName = GetPublisherName(newEntity.ParentId.Value, 2);
                                if (!string.IsNullOrEmpty(getPublisherName))
                                {
                                    body.Replace("$$Value$$", getPublisherName);
                                    IsAnyFieldChanged = true;
                                }
                            }
                            else
                                body.Replace("$$OldExhibitorNameAr$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewExhibitorNameAr$$", string.Empty);
                            body.Replace("$$OldExhibitorNameAr$$", string.Empty);
                        }
                        #endregion
                        #region Country
                        if (OldAgencyEntity.CountryId != newEntity.CountryId)
                        {
                            if (newEntity.CountryId != null)
                            {
                                body.Replace("$$NewCountry$$", strContent);
                                body.Replace("$$Label$$", "Country");
                                var countryName = GetCountryName(newEntity.CountryId.Value);
                                if (!string.IsNullOrEmpty(countryName))
                                {
                                    body.Replace("$$Value$$", countryName);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewCountry$$", string.Empty);
                            }
                            else
                                body.Replace("$$NewCountry$$", string.Empty);
                            if (OldAgencyEntity.CountryId != null)
                            {
                                body.Replace("$$OldCountry$$", strContent);
                                body.Replace("$$Label$$", "Country");
                                var countryName = GetCountryName(OldAgencyEntity.CountryId.Value);
                                if (!string.IsNullOrEmpty(countryName))
                                {
                                    body.Replace("$$Value$$", countryName);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldCountry$$", string.Empty);
                            }
                            else
                                body.Replace("$$OldCountry$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewCountry$$", string.Empty);
                            body.Replace("$$OldCountry$$", string.Empty);
                        }
                        #endregion
                        #region City
                        if (OldAgencyEntity.CityId != newEntity.CityId)
                        {
                            if (newEntity.CityId != null)
                            {
                                body.Replace("$$NewCity$$", strContent);
                                body.Replace("$$Label$$", "City");
                                var cityName = GetCityName(newEntity.CityId.Value);
                                if (!string.IsNullOrEmpty(cityName))
                                {
                                    body.Replace("$$Value$$", cityName);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewCity$$", string.Empty);
                            }
                            else
                                body.Replace("$$NewCity$$", string.Empty);
                            if (OldAgencyEntity.CityId != null)
                            {
                                body.Replace("$$OldCity$$", strContent);
                                body.Replace("$$Label$$", "City");
                                var cityName = GetCityName(OldAgencyEntity.CityId.Value);
                                if (!string.IsNullOrEmpty(cityName))
                                {
                                    body.Replace("$$Value$$", cityName);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldCity$$", string.Empty);
                            }
                            else
                                body.Replace("$$OldCity$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewCity$$", string.Empty);
                            body.Replace("$$OldCity$$", string.Empty);
                        }
                        #endregion
                        #region Exhibition Category
                        if (OldAgencyEntity.ExhibitionCategoryId != newEntity.ExhibitionCategoryId)
                        {
                            if (newEntity.ExhibitionCategoryId != null)
                            {
                                body.Replace("$$NewExhibitionCategory$$", strContent);
                                body.Replace("$$Label$$", "Exhibition Category");
                                var categoryName = GetCategoryName(newEntity.ExhibitionCategoryId.Value);
                                if (!string.IsNullOrEmpty(categoryName))
                                {
                                    body.Replace("$$Value$$", categoryName);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$NewExhibitionCategory$$", string.Empty);
                            }
                            else
                                body.Replace("$$NewExhibitionCategory$$", string.Empty);
                            if (OldAgencyEntity.ExhibitionCategoryId != null)
                            {
                                body.Replace("$$OldExhibitionCategory$$", strContent);
                                body.Replace("$$Label$$", "Exhibition Category");
                                var categoryName = GetCategoryName(OldAgencyEntity.ExhibitionCategoryId.Value);
                                if (!string.IsNullOrEmpty(categoryName))
                                {
                                    body.Replace("$$Value$$", categoryName);
                                    IsAnyFieldChanged = true;
                                }
                                else
                                    body.Replace("$$OldExhibitionCategory$$", string.Empty);
                            }
                            else
                                body.Replace("$$OldExhibitionCategory$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewExhibitionCategory$$", string.Empty);
                            body.Replace("$$OldExhibitionCategory$$", string.Empty);
                        }
                        #endregion
                        #region Contact Person English
                        if (OldAgencyEntity.ContactPersonName != newEntity.ContactPersonName)
                        {
                            if (newEntity.ContactPersonName != null)
                            {
                                body.Replace("$$NewContactPersonNameEn$$", strContent);
                                body.Replace("$$Label$$", "Contact Person Name in English");
                                body.Replace("$$Value$$", newEntity.ContactPersonName);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewContactPersonNameEn$$", string.Empty);
                            if (OldAgencyEntity.ContactPersonName != null)
                            {
                                body.Replace("$$OldContactPersonNameEn$$", strContent);
                                body.Replace("$$Label$$", "Contact Person Name in English");
                                body.Replace("$$Value$$", OldAgencyEntity.ContactPersonName);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldContactPersonNameEn$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewContactPersonNameEn$$", string.Empty);
                            body.Replace("$$OldContactPersonNameEn$$", string.Empty);
                        }
                        #endregion
                        #region Contact Person Arabic
                        if (OldAgencyEntity.ContactPersonNameAr != newEntity.ContactPersonNameAr)
                        {
                            if (newEntity.ContactPersonNameAr != null)
                            {
                                body.Replace("$$NewContactPersonNameAr$$", strContent);
                                body.Replace("$$Label$$", "Contact Person Name in Arabic");
                                body.Replace("$$Value$$", newEntity.ContactPersonNameAr);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewContactPersonNameAr$$", string.Empty);
                            if (OldAgencyEntity.ContactPersonNameAr != null)
                            {
                                body.Replace("$$OldContactPersonNameAr$$", strContent);
                                body.Replace("$$Label$$", "Contact Person Name in Arabic");
                                body.Replace("$$Value$$", OldAgencyEntity.ContactPersonNameAr);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldContactPersonNameAr$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewContactPersonNameAr$$", string.Empty);
                            body.Replace("$$OldContactPersonNameAr$$", string.Empty);
                        }
                        #endregion
                        #region Contact Person Title English
                        if (OldAgencyEntity.ContactPersonTitle != newEntity.ContactPersonTitle)
                        {
                            if (newEntity.ContactPersonTitle != null)
                            {
                                body.Replace("$$NewContactPersonTitleEn$$", strContent);
                                body.Replace("$$Label$$", "Contact Person Title in English");
                                body.Replace("$$Value$$", newEntity.ContactPersonTitle);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewCContactPersonTitleEn$$", string.Empty);
                            if (OldAgencyEntity.ContactPersonTitle != null)
                            {
                                body.Replace("$$OldContactPersonTitleEn$$", strContent);
                                body.Replace("$$Label$$", "Contact Person Title in English");
                                body.Replace("$$Value$$", OldAgencyEntity.ContactPersonTitle);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldContactPersonTitleEn$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewContactPersonTitleEn$$", string.Empty);
                            body.Replace("$$OldContactPersonTitleEn$$", string.Empty);
                        }
                        #endregion
                        #region Contact Person Title Arabic
                        if (OldAgencyEntity.ContactPersonTitleAr != newEntity.ContactPersonTitleAr)
                        {
                            if (newEntity.ContactPersonTitleAr != null)
                            {
                                body.Replace("$$NewContactPersonTitleAr$$", strContent);
                                body.Replace("$$Label$$", "Contact Person Title in Arabic");
                                body.Replace("$$Value$$", newEntity.ContactPersonTitleAr);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewCContactPersonTitleAr$$", string.Empty);
                            if (OldAgencyEntity.ContactPersonTitleAr != null)
                            {
                                body.Replace("$$OldContactPersonTitleAr$$", strContent);
                                body.Replace("$$Label$$", "Contact Person Title in Arabic");
                                body.Replace("$$Value$$", OldAgencyEntity.ContactPersonTitleAr);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldContactPersonTitleAr$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewContactPersonTitleAr$$", string.Empty);
                            body.Replace("$$OldContactPersonTitleAr$$", string.Empty);
                        }
                        #endregion
                        #region Phone
                        if (OldAgencyEntity.Phone != newEntity.Phone)
                        {
                            if (newEntity.Phone != null)
                            {
                                body.Replace("$$NewPhone$$", strContent);
                                body.Replace("$$Label$$", "Phone");
                                body.Replace("$$Value$$", newEntity.Phone.Replace('$', '-'));
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewPhone$$", string.Empty);
                            if (OldAgencyEntity.Phone != null)
                            {
                                body.Replace("$$OldPhone$$", strContent);
                                body.Replace("$$Label$$", "Phone");
                                body.Replace("$$Value$$", OldAgencyEntity.Phone);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldPhone$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewPhone$$", string.Empty);
                            body.Replace("$$OldPhone$$", string.Empty);
                        }
                        #endregion
                        #region Fax
                        if (OldAgencyEntity.Fax != newEntity.Fax)
                        {
                            if (newEntity.Fax != null)
                            {
                                body.Replace("$$NewFax$$", strContent);
                                body.Replace("$$Label$$", "Fax");
                                body.Replace("$$Value$$", newEntity.Fax.Replace('$', '-'));
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewFax$$", string.Empty);
                            if (OldAgencyEntity.Fax != null)
                            {
                                body.Replace("$$OldFax$$", strContent);
                                body.Replace("$$Label$$", "Fax");
                                body.Replace("$$Value$$", OldAgencyEntity.Fax);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldFax$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewFax$$", string.Empty);
                            body.Replace("$$OldFax$$", string.Empty);
                        }
                        #endregion
                        #region Mobile
                        if (OldAgencyEntity.Mobile != newEntity.Mobile)
                        {
                            if (newEntity.Mobile != null)
                            {
                                body.Replace("$$NewMobile$$", strContent);
                                body.Replace("$$Label$$", "Mobile");
                                body.Replace("$$Value$$", newEntity.Mobile.Replace('$', '-'));
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewMobile$$", string.Empty);
                            if (OldAgencyEntity.Mobile != null)
                            {
                                body.Replace("$$OldMobile$$", strContent);
                                body.Replace("$$Label$$", "Mobile");
                                body.Replace("$$Value$$", OldAgencyEntity.Mobile);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldMobile$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewMobile$$", string.Empty);
                            body.Replace("$$OldMobile$$", string.Empty);
                        }
                        #endregion
                        #region Email
                        if (OldAgencyEntity.Email != newEntity.Email)
                        {
                            if (newEntity.Email != null)
                            {
                                body.Replace("$$NewEmail$$", strContent);
                                body.Replace("$$Label$$", "Email");
                                body.Replace("$$Value$$", newEntity.Email);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewEmail$$", string.Empty);
                            if (OldAgencyEntity.Email != null)
                            {
                                body.Replace("$$OldEmail$$", strContent);
                                body.Replace("$$Label$$", "Email");
                                body.Replace("$$Value$$", OldAgencyEntity.Email);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldEmail$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewEmail$$", string.Empty);
                            body.Replace("$$OldEmail$$", string.Empty);
                        }
                        #endregion
                        #region Total Titles
                        if (OldAgencyEntity.TotalNumberOfTitles != newEntity.TotalNumberOfTitles)
                        {
                            if (newEntity.TotalNumberOfTitles != null)
                            {
                                body.Replace("$$NewTotletitles$$", strContent);
                                body.Replace("$$Label$$", "Total Number Of Titles");
                                body.Replace("$$Value$$", newEntity.TotalNumberOfTitles);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewTotletitles$$", string.Empty);
                            if (OldAgencyEntity.TotalNumberOfTitles != null)
                            {
                                body.Replace("$$OldTotletitles$$", strContent);
                                body.Replace("$$Label$$", "Total Number Of Titles");
                                body.Replace("$$Value$$", OldAgencyEntity.TotalNumberOfTitles);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldTotletitles$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewTotletitles$$", string.Empty);
                            body.Replace("$$OldTotletitles$$", string.Empty);
                        }
                        #endregion
                        #region Total New Titles
                        if (OldAgencyEntity.TotalNumberOfNewTitles != newEntity.TotalNumberOfNewTitles)
                        {
                            if (newEntity.TotalNumberOfNewTitles != null)
                            {
                                body.Replace("$$NewTotletitlesNew$$", strContent);
                                body.Replace("$$Label$$", "Total Number Of New Titles");
                                body.Replace("$$Value$$", newEntity.TotalNumberOfNewTitles);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$NewTotletitlesNew$$", string.Empty);
                            if (OldAgencyEntity.TotalNumberOfNewTitles != null)
                            {
                                body.Replace("$$OldTotletitlesNew$$", strContent);
                                body.Replace("$$Label$$", "Total Number Of new Titles");
                                body.Replace("$$Value$$", OldAgencyEntity.TotalNumberOfNewTitles);
                                IsAnyFieldChanged = true;
                            }
                            else
                                body.Replace("$$OldTotletitlesNew$$", string.Empty);
                        }
                        else
                        {
                            body.Replace("$$NewTotletitlesNew$$", string.Empty);
                            body.Replace("$$OldTotletitlesNew$$", string.Empty);
                        }
                        #endregion
                        if (newEntity.PublisherNameEn != null)
                            body.Replace("$$AgencyNameEn$$", newEntity.PublisherNameEn);
                        else
                            body.Replace("$$AgencyNameEn$$", string.Empty);
                        if (newEntity.FileNumber != null)
                            body.Replace("$$FileNumber$$", newEntity.FileNumber);
                        else
                            body.Replace("$$FileNumber$$", string.Empty);
                        #endregion
                        if (IsAnyFieldChanged)
                            if (StrEmailContentEmail != null)
                                _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, StrEmailContentEmail, StrEmailContentSubject, body.ToString());
                        //SmtpEmail.SendNetEmail(AdminName, AdminEmailAddress, SIBFEmail, emailContent.Subject, body.ToString());
                    }
                    #endregion
                    #region Email to Old Exhibitor, Agency and New Exhibitor
                    StringBuilder body1 = new StringBuilder();
                    if (OldAgencyEntity != null && newEntity != null)
                    {
                        if (OldAgencyEntity.ParentId != newEntity.ParentId)
                        {
                            ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                            var OldExhibitorEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(OldAgencyEntity.ParentId.Value);
                            if (OldExhibitorEntity != null)
                            {
                                XsiExhibitionMember OldMemberEntity = ExhibitionMemberService.GetExhibitionMemberByItemId(OldExhibitorEntity.MemberId.Value);
                                if (OldMemberEntity != null)
                                {
                                    #region Email Content
                                    if (agencyRegistrationDto.WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                                    {
                                        var ScrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(10015);
                                        if (ScrfEmailContent != null)
                                        {
                                            if (OldMemberEntity.LanguageUrl.Value == 1)
                                            {
                                                if (ScrfEmailContent.Body != null)
                                                    StrEmailContentBody = ScrfEmailContent.Body;
                                                if (ScrfEmailContent.Email != null)
                                                    StrEmailContentEmail = ScrfEmailContent.Email;
                                                if (ScrfEmailContent.Subject != null)
                                                    StrEmailContentSubject = ScrfEmailContent.Subject;
                                            }
                                            else
                                            {
                                                if (ScrfEmailContent.BodyAr != null)
                                                    StrEmailContentBody = ScrfEmailContent.BodyAr;
                                                if (ScrfEmailContent.Email != null)
                                                    StrEmailContentEmail = ScrfEmailContent.Email;
                                                if (ScrfEmailContent.SubjectAr != null)
                                                    StrEmailContentSubject = ScrfEmailContent.SubjectAr;
                                            }
                                            StrEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                        }
                                    }
                                    else
                                    {
                                        //EmailContent = EmailContentService.GetEmailContentByItemId(48, OldMemberEntity.LanguageUrl.Value);
                                        var EmailContent = EmailContentService.GetEmailContentByItemId(10015);
                                        if (EmailContent != null)
                                        {
                                            if (OldMemberEntity.LanguageUrl.Value == 1)
                                            {
                                                if (EmailContent.Body != null)
                                                    StrEmailContentBody = EmailContent.Body;
                                                if (EmailContent.Email != null)
                                                    StrEmailContentEmail = EmailContent.Email;
                                                if (EmailContent.Subject != null)
                                                    StrEmailContentSubject = EmailContent.Subject;
                                            }
                                            else
                                            {
                                                if (EmailContent.BodyAr != null)
                                                    StrEmailContentBody = EmailContent.BodyAr;
                                                if (EmailContent.Email != null)
                                                    StrEmailContentEmail = EmailContent.Email;
                                                if (EmailContent.SubjectAr != null)
                                                    StrEmailContentSubject = EmailContent.SubjectAr;
                                            }

                                            StrEmailContentAdmin = _appCustomSettings.AdminName;
                                        }
                                    }
                                    #endregion
                                    body1.Append(htmlContentFactory.BindEmailContent(OldMemberEntity.LanguageUrl.Value, StrEmailContentBody, ExhibitionMemberEntity, _appCustomSettings.ServerAddressNew, agencyRegistrationDto.WebsiteId.Value, _appCustomSettings));
                                    if (OldMemberEntity.LanguageUrl == 1)
                                        body1.Replace("$$AgencyName$$", newEntity.PublisherNameEn);
                                    else
                                        body1.Replace("$$AgencyName$$", newEntity.PublisherNameAr);
                                    if (OldMemberEntity.Email != null)
                                        _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, OldMemberEntity.Email, StrEmailContentSubject, body.ToString());
                                    SendEmail(agencyRegistrationDto);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
        }

        void SendEmail(AgencyRegistrationDTO agencyRegistrationDto)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                XsiExhibitionMember entity = new XsiExhibitionMember();
                entity = ExhibitionMemberService.GetExhibitionMemberByItemId(agencyRegistrationDto.MemberId.Value);
                if (entity != default(XsiExhibitionMember))
                {
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {
                        HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                        #region email to agency
                        StringBuilder body = new StringBuilder();
                        #region Email Content

                        if (agencyRegistrationDto.WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                        {
                            var ScrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(13);
                            if (ScrfEmailContent != null)
                            {
                                if (entity.LanguageUrl.Value == 1)
                                {
                                    if (ScrfEmailContent.Body != null)
                                        StrEmailContentBody = ScrfEmailContent.Body;
                                    if (ScrfEmailContent.Email != null)
                                        StrEmailContentEmail = ScrfEmailContent.Email;
                                    if (ScrfEmailContent.Subject != null)
                                        StrEmailContentSubject = ScrfEmailContent.Subject;
                                }
                                else
                                {

                                    if (ScrfEmailContent.BodyAr != null)
                                        StrEmailContentBody = ScrfEmailContent.BodyAr;
                                    if (ScrfEmailContent.Email != null)
                                        StrEmailContentEmail = ScrfEmailContent.Email;
                                    if (ScrfEmailContent.SubjectAr != null)
                                        StrEmailContentSubject = ScrfEmailContent.SubjectAr;
                                }
                                StrEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                            }
                        }
                        else
                        {
                            var EmailContent = EmailContentService.GetEmailContentByItemId(13);
                            if (EmailContent != null)
                            {
                                if (entity.LanguageUrl.Value == 1)
                                {
                                    if (EmailContent.Body != null)
                                        StrEmailContentBody = EmailContent.Body;
                                    if (EmailContent.Email != null)
                                        StrEmailContentEmail = EmailContent.Email;
                                    if (EmailContent.Subject != null)
                                        StrEmailContentSubject = EmailContent.Subject;
                                }
                                else
                                {
                                    if (EmailContent.BodyAr != null)
                                        StrEmailContentBody = EmailContent.BodyAr;
                                    if (EmailContent.Email != null)
                                        StrEmailContentEmail = EmailContent.Email;
                                    if (EmailContent.SubjectAr != null)
                                        StrEmailContentSubject = EmailContent.SubjectAr;
                                }
                                StrEmailContentAdmin = _appCustomSettings.AdminName;
                            }
                        }
                        #endregion
                        #endregion
                        using (sibfnewdbContext _context = new sibfnewdbContext())
                        {
                            body.Append(htmlContentFactory.BindEmailContent(entity.LanguageUrl.Value, StrEmailContentBody, entity, _appCustomSettings.ServerAddressNew, agencyRegistrationDto.WebsiteId.Value, _appCustomSettings));
                            _emailSender.SendEmail(StrEmailContentAdmin, _appCustomSettings.AdminEmail, entity.Email, StrEmailContentSubject, body.ToString());

                            if (agencyRegistrationDto.ExhibitorId != null)
                            {
                                #region email to exhibitor
                                using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
                                {
                                    var entity1 = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(agencyRegistrationDto.ExhibitorId.Value);
                                    if (entity1 != null)
                                    {
                                        if (entity1.MemberId != null)
                                        {
                                            XsiExhibitionMember entity2 = ExhibitionMemberService.GetExhibitionMemberByItemId(entity1.MemberId.Value);
                                            if (entity2 != null)
                                            {
                                                StringBuilder body1 = new StringBuilder();
                                                #region Email Content
                                                if (agencyRegistrationDto.WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                                                {
                                                    var ScrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(15);
                                                    if (ScrfEmailContent != null)
                                                    {
                                                        if (ScrfEmailContent.Body != null)
                                                            StrEmailContentBody = ScrfEmailContent.Body;
                                                        if (ScrfEmailContent.Email != null)
                                                            StrEmailContentEmail = ScrfEmailContent.Email;
                                                        if (ScrfEmailContent.Subject != null)
                                                            StrEmailContentSubject = ScrfEmailContent.Subject;
                                                        StrEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                                    }
                                                }
                                                else
                                                {
                                                    var EmailContent = EmailContentService.GetEmailContentByItemId(15);
                                                    if (EmailContent != null)
                                                    {
                                                        if (EmailContent.Body != null)
                                                            StrEmailContentBody = EmailContent.Body;
                                                        if (EmailContent.Email != null)
                                                            StrEmailContentEmail = EmailContent.Email;
                                                        if (EmailContent.Subject != null)
                                                            StrEmailContentSubject = EmailContent.Subject;
                                                        StrEmailContentAdmin = _appCustomSettings.AdminName;
                                                    }
                                                }
                                                #endregion
                                                body1.Append(htmlContentFactory.BindEmailContent(entity2.LanguageUrl.Value, StrEmailContentBody, entity2, _appCustomSettings.ServerAddressNew, agencyRegistrationDto.WebsiteId.Value, _appCustomSettings));
                                                body1.Replace("$$AgencyName$$", agencyRegistrationDto.StepTwoDTO.PublisherNameEn);
                                                if (entity2.Email != null)
                                                    _emailSender.SendEmail(StrEmailContentAdmin, _appCustomSettings.AdminEmail, entity2.Email, StrEmailContentSubject, body1.ToString());
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }
            }
        }

        private void LogAgencyStatus(string status, long? agencyId)
        {
            using (var AgencyStatusLogService = new AgencyStatusLogService())
            {
                var entity = new XsiExhibitionMemberApplicationYearlyLogs();
                entity.MemberExhibitionYearlyId = agencyId.Value;
                entity.Status = status;
                entity.CreatedOn = MethodFactory.ArabianTimeNow();
                AgencyStatusLogService.InsertAgencyStatusLog(entity);
            }
        }
        private string GetCountryName(long id)
        {
            ExhibitionCountryService exhibitionCountryService = new ExhibitionCountryService();
            var getCountry = exhibitionCountryService.GetExhibitionCountryByItemId(id);
            if (getCountry != null)
            {
                return getCountry.CountryName;
            }
            return string.Empty;
        }

        private string GetCityName(long id)
        {
            ExhibitionCityService exhibitionCityService = new ExhibitionCityService();
            var getCity = exhibitionCityService.GetExhibitionCityByItemId(id);
            if (getCity != null)
            {
                return getCity.CityName;
            }
            return string.Empty;
        }

        private string GetCategoryName(long id)
        {
            ExhibitionCategoryService exhibitionCategoryService = new ExhibitionCategoryService();
            var getCategory = exhibitionCategoryService.GetExhibitionCategoryByItemId(id);
            if (getCategory != null)
            {
                return getCategory.Title;
            }
            return string.Empty;
        }

        private void UpdateStatusChange(long agencyid)
        {
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                if (agencyid > 0)
                {
                    var entity = AgencyRegistrationService.GetAgencyRegistrationByItemId(agencyid);
                    if (entity != null)
                    {
                        entity.IsStatusChanged = EnumConversion.ToString(EnumBool.No);
                        AgencyRegistrationService.UpdateAgencyRegistration(entity);
                    }
                }
            }
        }
        private string GetPublisherName(long id, long languageId)
        {
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly where = new XsiExhibitionMemberApplicationYearly();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                where.ParentId = id;
                where.LanguageUrl = languageId;
                var agency = AgencyRegistrationService.GetAgencyRegistration(where).FirstOrDefault();
                if (agency != null)
                {
                    if (languageId == 1)
                        return agency.PublisherNameEn;
                    else
                        return agency.PublisherNameAr;
                }
            }
            return string.Empty;
        }

        XsiExhibitionMemberApplicationYearly GetCurrentYearAgency(long exhibitionid, long memberid)
        {
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                return AgencyRegistrationService.GetAgencyRegistration(new XsiExhibitionMemberApplicationYearly { MemberId = memberid, ExhibitionId = exhibitionid, IsActive = EnumConversion.ToString(EnumBool.Yes) }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            }
        }
        #endregion
        #region Import Books Methods
        string ImportBooks(long agencyid, long exhibitorId, long memberid, SaveAgencyDataForStepTwoAndThreeDTO dataobj, long LangId)
        {
            if (dataobj.excelFileBase64 != null && dataobj.excelFileBase64.Length > 0)
            {
                long bookAdded = 0;
                string fileName = string.Empty;
                byte[] imageBytes;

                if (dataobj.excelFileBase64.Contains("data:"))
                {
                    var strInfo = dataobj.excelFileBase64.Split(",")[0];
                    imageBytes = Convert.FromBase64String(dataobj.excelFileBase64.Split(',')[1]);
                }
                else
                {
                    imageBytes = Convert.FromBase64String(dataobj.excelFileBase64);
                }

                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    var agency = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == agencyid).FirstOrDefault();

                    fileName = string.Format("{0}_{1}{2}", LangId, MethodFactory.GetRandomNumber(), ".xlsx");
                    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\ExhibitorBooks\\") + fileName, imageBytes);
                    agency.BookFileName = fileName;
                    agency.IsBooksUpload = "Y";
                    _context.SaveChanges();

                    using (var transaction = _context.Database.BeginTransaction())
                    {
                        using (var stream = new MemoryStream(imageBytes))
                        {
                            using (var package = new ExcelPackage(stream))
                            {
                                #region Get Dependency List
                                BookLanguageList = GetExhibitionLanguageList();
                                BookSubjectList = GetExhibitionBookSubjectList();
                                BookSubsubjectList = GetExhibitionBookSubsubjectList();
                                #endregion

                                ExcelWorksheet worksheet = package.Workbook.Worksheets.ToList()[0];
                                var rowCount = worksheet.Dimension.Rows;
                                long bookLanguageId = -1;
                                long bookSubjectId = -1;
                                long bookSubSubjectId = -1;

                                for (int row = 2; row <= rowCount; row++)
                                {
                                    bookLanguageId = -1;
                                    bookSubjectId = -1;
                                    bookSubSubjectId = -1;
                                    if (worksheet.Cells[row, 1].Value != null && worksheet.Cells[row, 2].Value != null && worksheet.Cells[row, 3].Value != null && worksheet.Cells[row, 4].Value != null && worksheet.Cells[row, 5].Value != null && worksheet.Cells[row, 6].Value != null && worksheet.Cells[row, 7].Value != null && worksheet.Cells[row, 8].Value != null)
                                    {
                                        XsiExhibitionBooks books = new XsiExhibitionBooks();

                                        if (worksheet.Cells[row, 1].Value != null)
                                            books.TitleEn = worksheet.Cells[row, 1].Value.ToString().Trim();

                                        if (!string.IsNullOrEmpty(worksheet.Cells[row, 2].Text.Trim()))
                                        {
                                            books.BookPublisherName = worksheet.Cells[row, 2].Text.Trim();
                                            books.BookPublisherNameAr = worksheet.Cells[row, 2].Text.Trim();
                                        }
                                        if (!string.IsNullOrEmpty(worksheet.Cells[row, 3].Text.ToString().Trim()))
                                        {
                                            books.AuthorName = worksheet.Cells[row, 3].Text.Trim();
                                            books.AuthorNameAr = worksheet.Cells[row, 3].Text.Trim();
                                        }

                                        if (worksheet.Cells[row, 4].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 4].Value.ToString().Trim()))
                                        {
                                            string subject = worksheet.Cells[row, 4].Value.ToString().Trim().Split('_')[0];
                                            XsiExhibitionBookSubject entity = new XsiExhibitionBookSubject();
                                            if (LangId == 1)
                                                entity = BookSubjectList.Where(i => i.Title.Trim() == subject).FirstOrDefault();
                                            else
                                                entity = BookSubjectList.Where(i => i.TitleAr.Trim() == subject).FirstOrDefault();
                                            if (entity != null)
                                            {
                                                bookSubjectId = entity.ItemId;
                                            }
                                            if (bookSubjectId > 0)
                                                books.ExhibitionSubjectId = bookSubjectId;
                                        }
                                        if (worksheet.Cells[row, 4].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 4].Value.ToString().Trim()))
                                        {
                                            string subsubject = string.Empty; //worksheet.Cells[row, 4].Value.ToString().Trim().Split('_')[1];
                                            if (worksheet.Cells[row, 4].Value.ToString().Trim().IndexOf('_') > -1)
                                            {
                                                subsubject = worksheet.Cells[row, 4].Value.ToString().Trim().Split('_')[1];
                                            }
                                            XsiExhibitionBookSubsubject entity = new XsiExhibitionBookSubsubject();
                                            if (LangId == 1)
                                                entity = BookSubsubjectList.Where(i => i.Title.Trim() == subsubject).FirstOrDefault();
                                            else
                                                entity = BookSubsubjectList.Where(i => i.TitleAr.Trim() == subsubject).FirstOrDefault();
                                            if (entity != null)
                                            {
                                                bookSubSubjectId = entity.ItemId;
                                            }
                                            if (bookSubSubjectId > 0)
                                                books.ExhibitionSubsubjectId = bookSubSubjectId;
                                        }
                                        if (worksheet.Cells[row, 5].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 5].Value.ToString().Trim()))
                                        {
                                            XsiExhibitionLanguage entity = new XsiExhibitionLanguage();
                                            if (LangId == 1)
                                                entity = BookLanguageList.Where(i => i.Title.Trim() == worksheet.Cells[row, 5].Value.ToString().Trim()).FirstOrDefault();
                                            else
                                                entity = BookLanguageList.Where(i => i.TitleAr.Trim() == worksheet.Cells[row, 5].Value.ToString().Trim()).FirstOrDefault();
                                            if (entity != null)
                                            {
                                                bookLanguageId = entity.ItemId;
                                            }
                                            if (bookLanguageId > 0)
                                                books.BookLanguageId = bookLanguageId;
                                        }
                                        if (worksheet.Cells[row, 6].Value != null)
                                            books.IssueYear = worksheet.Cells[row, 6].Value.ToString().Trim();
                                        if (worksheet.Cells[row, 7].Value != null)
                                            books.Price = worksheet.Cells[row, 7].Value.ToString().Trim();
                                        if (worksheet.Cells[row, 8].Value != null)
                                            books.PriceBeforeDiscount = worksheet.Cells[row, 8].Value.ToString().Trim();
                                        if (worksheet.Cells[row, 9].Value != null)
                                            books.Isbn = worksheet.Cells[row, 9].Value.ToString().Trim();
                                        if (worksheet.Cells[row, 10].Value != null)
                                            books.BookDetails = worksheet.Cells[row, 10].Value.ToString().Trim();
                                        if (worksheet.Cells[row, 11].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 11].Value.ToString().Trim()))
                                        {
                                            if (worksheet.Cells[row, 11].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 11].Value.ToString()))
                                                books.IsBestSeller = GetBestSeller(worksheet.Cells[row, 11].Value.ToString().Trim());
                                            else
                                                books.IsBestSeller = EnumConversion.ToString(EnumBool.No);
                                        }

                                        books.MemberId = memberid;
                                        books.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                        if (books.TitleEn != string.Empty && !string.IsNullOrEmpty(books.BookPublisherName) && !string.IsNullOrEmpty(books.AuthorName) &&
                                            books.ExhibitionSubjectId > 0 && books.ExhibitionSubsubjectId > 0 && books.BookLanguageId > 0 && books.IssueYear != string.Empty &&
                                            books.Price != string.Empty)
                                        {
                                            books.IsEnable = EnumConversion.ToString(EnumBool.Yes);
                                        }
                                        books.IsNew = EnumConversion.ToString(EnumBool.Yes);
                                        books.CreatedBy = memberid;
                                        books.CreatedOn = MethodFactory.ArabianTimeNow();
                                        books.ModifiedBy = memberid;
                                        books.ModifiedOn = MethodFactory.ArabianTimeNow();
                                        _context.XsiExhibitionBooks.Add(books);
                                        _context.SaveChanges();
                                        if (books.BookId > 0)
                                        {
                                            bookAdded = bookAdded + 1;

                                            #region Books Participating
                                            XsiExhibitionBookParticipating bookParticipating = new XsiExhibitionBookParticipating();
                                            bookParticipating.BookId = books.BookId;
                                            bookParticipating.ExhibitorId = exhibitorId;
                                            bookParticipating.AgencyId = agencyid;

                                            bookParticipating.IsActive = EnumConversion.ToString(EnumBool.No);
                                            bookParticipating.Price = worksheet.Cells[row, 7].Value.ToString().Trim();
                                            bookParticipating.CreatedOn = MethodFactory.ArabianTimeNow();
                                            bookParticipating.CreatedBy = memberid;
                                            bookParticipating.ModifiedOn = MethodFactory.ArabianTimeNow();
                                            bookParticipating.ModifiedBy = memberid;
                                            _context.XsiExhibitionBookParticipating.Add(bookParticipating);
                                            _context.SaveChanges();
                                            #endregion
                                        }
                                    }
                                }
                                if (bookAdded > 0)
                                {
                                    transaction.Commit();
                                    if (LangId == 1)
                                    {
                                        return bookAdded + " books added successfully";
                                    }
                                    else
                                    {
                                        return bookAdded + " تمت إضافة الكتاب بنجاح";
                                    }
                                }
                                else
                                {
                                    return "Something went wrong while importing books. Please try again later.";
                                }
                            }
                        }
                    }
                }
            }
            return "No books found to import";
        }
        List<XsiExhibitionBookSubject> GetExhibitionBookSubjectList()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiExhibitionBookSubject.Where(x => (x.Title != null && x.TitleAr != null) && x.IsActive == "Y").ToList();
            }
        }
        List<XsiExhibitionBookSubsubject> GetExhibitionBookSubsubjectList()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiExhibitionBookSubsubject.Where(x => (x.Title != null && x.TitleAr != null) && x.IsActive == "Y").ToList();
            }
        }
        List<XsiExhibitionLanguage> GetExhibitionLanguageList()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiExhibitionLanguage.Where(x => (x.Title != null && x.TitleAr != null) && x.IsActive == "Y").ToList();
            }
        }
        string GetBestSeller(string text)
        {
            if (text == "Yes")
                return "Y";
            return "N";
        }
        #endregion
        #region Load Steps Content, Save,Update
        private void UpdateRegistration(SaveAgencyDataForStepTwoAndThreeDTO updateDto, long langid = 1)
        {
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                XsiExhibitionExhibitorDetails agencydetails = new XsiExhibitionExhibitorDetails();

                var ExistingEntity = AgencyRegistrationService.GetAgencyRegistrationByItemId(updateDto.AgencyId);
                var entity = new XsiExhibitionMemberApplicationYearly();
                entity = AgencyRegistrationService.GetAgencyRegistrationByItemId(updateDto.AgencyId);
                //var detailsExhistingEntity = agencyreg.GetAgencyRegistrationByItemId(agencyRegistrationDto.MemberExhibitionYearlyId);
                //entity.MemberExhibitionYearlyId = agencyRegistrationDto.MemberExhibitionYearlyId;
                entity.ParentId = updateDto.ExhibitorId;
                //entity.ExhibitionId = ExistingEntity.ExhibitionId;
                entity.MemberId = ExistingEntity.MemberId;
                entity.CountryId = updateDto.CountryId;
                entity.CityId = updateDto.CityId;
                //if (ddlExhibitionCity.SelectedItem.Text == EnumConversion.ToString(EnumOtherText.OtherCity))
                //    entity.OtherCity = txtOtherCity.Text;
                //else
                //    entity.OtherCity = string.Empty;
                if (updateDto.ExhibitionCategoryId > 0)
                    entity.ExhibitionCategoryId = updateDto.ExhibitionCategoryId;
                // AgencyNameEn= txtPublisherEnglish.Text;
                entity.PublisherNameEn = updateDto.PublisherNameEn;
                entity.PublisherNameAr = updateDto.PublisherNameAr;
                entity.ContactPersonName = updateDto.ContactPersonName;
                entity.ContactPersonNameAr = updateDto.ContactPersonNameAr;
                entity.ContactPersonTitle = updateDto.ContactPersonTitle;
                entity.ContactPersonTitleAr = updateDto.ContactPersonTitleAr;

                if (!string.IsNullOrEmpty(updateDto.PhoneISD) && !string.IsNullOrEmpty(updateDto.PhoneSTD) && !string.IsNullOrEmpty(updateDto.Phone))
                    entity.Phone = updateDto.PhoneISD + "$" + updateDto.PhoneSTD + "$" + updateDto.Phone;
                if (!string.IsNullOrEmpty(updateDto.MobileISD) && !string.IsNullOrEmpty(updateDto.MobileSTD) && !string.IsNullOrEmpty(updateDto.Mobile))
                    entity.Mobile = updateDto.MobileISD + "$" + updateDto.MobileSTD + "$" + updateDto.Mobile;
                if (!string.IsNullOrEmpty(updateDto.FaxISD) && !string.IsNullOrEmpty(updateDto.FaxSTD) && !string.IsNullOrEmpty(updateDto.Fax))
                    entity.Fax = updateDto.FaxISD + "$" + updateDto.FaxSTD + "$" + updateDto.Fax;

                entity.Email = updateDto.Email;
                entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                entity.IsRegisteredByExhibitor = EnumConversion.ToString(EnumBool.No);
                entity.IsStatusChanged = EnumConversion.ToString(EnumBool.No);
                //entity.IsIncludedInInvoice = EnumConversion.ToString(EnumBool.No);
                //entity.IsArabicAgency = EnumConversion.ToString(EnumBool.No);
                entity.CreatedOn = ExistingEntity.CreatedOn;
                entity.CreatedBy = ExistingEntity.CreatedBy;
                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                entity.ModifiedBy = updateDto.MemberId;

                entity.TotalNumberOfTitles = updateDto.TotalNumberOfTitles;
                entity.TotalNumberOfNewTitles = updateDto.TotalNumberOfNewTitles;
                if (updateDto.AuhtorizationLetterBase64 != null && updateDto.AuhtorizationLetterBase64.Length > 0)
                {
                    string fileName = string.Empty;
                    byte[] imageBytes;
                    if (updateDto.AuhtorizationLetterBase64.Contains("data:"))
                    {
                        var strInfo = updateDto.AuhtorizationLetterBase64.Split(",")[0];
                        imageBytes = Convert.FromBase64String(updateDto.AuhtorizationLetterBase64.Split(',')[1]);
                    }
                    else
                    {
                        imageBytes = Convert.FromBase64String(updateDto.AuhtorizationLetterBase64);
                    }

                    fileName = string.Format("{0}_{1}{2}", langid, MethodFactory.GetRandomNumber(), "." + updateDto.AuhtorizationLetterExtension);
                    //if (MethodFactory.IsValidAgencyAuthorizationLetter("." + updateDto.AuhtorizationLetterExtension))
                    //{
                    //    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\AuthorizationLetter\\") + fileName, imageBytes);
                    //    entity.AuthorizationLetter = fileName;
                    //}
                    //else
                    //{
                    //    System.IO.File.WriteAllBytes(
                    //        Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                    //                     "\\ExhibitorRegistration\\AuthorizationLetter\\") + fileName, imageBytes);
                    //}
                    System.IO.File.WriteAllBytes(
                           Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                        "\\ExhibitorRegistration\\AuthorizationLetter\\") + fileName, imageBytes);
                    entity.AuthorizationLetter = fileName;
                }
                if (AgencyRegistrationService.UpdateAgencyRegistration(entity) == EnumResultType.Success)
                {
                    if (ExistingEntity != null && entity != null)
                    {
                        if (ExistingEntity.PublisherNameEn != entity.PublisherNameEn || ExistingEntity.PublisherNameAr != entity.PublisherNameAr
                            || ExistingEntity.ParentId != entity.ParentId)
                        {
                            entity.IsEditRequest = EnumConversion.ToString(EnumExhibitorEditStatus.New);
                            entity.Status = EnumConversion.ToString(EnumAgencyStatus.New);
                            AgencyRegistrationService.UpdateAgencyRegistration(entity);
                        }
                        ExhibitionService ExhibitionService = new ExhibitionService();
                        var exhibition = ExhibitionService.GetExhibitionByItemId(ExistingEntity.ExhibitionId.Value);
                        var model = GetAgencyView(updateDto.MemberId, updateDto.WebsiteId, exhibition, langid);
                        SendEmailBasedOnEdit(ExistingEntity, entity, model);
                    }

                    //updateDto.Message = _stringLocalizer["AgencyInProcess"];
                    ////StatusMsg.Visible = true;
                    //updateDto.Message = _stringLocalizer["AgencyDetailsAddedSuccessfully"];
                    //updateDto.TypeofMessage = (int)EnumMessageType.Success;


                    //ShowMessage(_stringLocalizer["AgencyDetailsAddedSuccessfully"], EnumMessageType.Success);
                    //model.Message = _stringLocalizer["AgencyDetailsAddedSuccessfully"];
                    LogAgencyStatus(entity.Status, updateDto.AgencyId);
                }
            }
        }
        private AgencyRegistrationDTO GetAgencyView(long memberId, long websiteId, XsiExhibition exhibition, long langid = 1)
        {
            AgencyRegistrationDTO model = new AgencyRegistrationDTO();
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService();
                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                if (exhibition != null)
                {
                    model.MemberId = memberId;
                    model.WebsiteId = exhibition.WebsiteId;
                    model.ExhibitionId = exhibition.ExhibitionId;

                    var memberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(memberId);
                    if (memberentity != null)
                    {
                        model.IsBooksRequired = (memberentity.IsBooksRequired == "Y") ? true : false;
                    }
                    string strExhibitorReject = EnumConversion.ToString(EnumAgencyStatus.ExhibitorReject);
                    string strSIBFReject = EnumConversion.ToString(EnumAgencyStatus.SIBFReject);
                    XsiExhibitionMemberApplicationYearly AgencyEntity = AgencyRegistrationService.GetAgencyRegistration(new XsiExhibitionMemberApplicationYearly { MemberRoleId = 2, MemberId = memberId, ExhibitionId = exhibition.ExhibitionId, IsActive = EnumConversion.ToString(EnumBool.Yes) }).Where(p => p.Status != strSIBFReject && p.Status != strExhibitorReject && p.Status != null).FirstOrDefault();
                    if (AgencyEntity != null)
                    {
                        model.AgencyId = AgencyEntity.MemberExhibitionYearlyId;
                        model.ExhibitorId = AgencyEntity.ParentId;
                        if (AgencyEntity.IsEditRequest != null)
                        {
                            if (AgencyEntity.IsEditRequest == EnumConversion.ToString(EnumExhibitorEditStatus.New))
                            {
                                model = CreateInterface(EnumRegistrationViewType.ThankYou, model, AgencyEntity.MemberExhibitionYearlyId);
                                if (langid == 1)
                                    model.Message = "Your application is being processed. You will be notified via email.";
                                else
                                    model.Message = "Your application is being processed. You will be notified via email.";

                            }
                            else
                            {
                                #region Status based view
                                if (AgencyEntity.Status != null)
                                {
                                    if (AgencyEntity.Status == EnumConversion.ToString(EnumAgencyStatus.New) || AgencyEntity.Status == EnumConversion.ToString(EnumAgencyStatus.Pending) || AgencyEntity.Status == EnumConversion.ToString(EnumAgencyStatus.SIBFApproved) || AgencyEntity.Status == EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved))
                                    {
                                        //StatusMsg.Visible = true;
                                        if (AgencyEntity.Status == EnumConversion.ToString(EnumAgencyStatus.New) || AgencyEntity.Status == EnumConversion.ToString(EnumAgencyStatus.Pending))
                                        {
                                            model = CreateInterface(EnumRegistrationViewType.ThankYou, model, AgencyEntity.MemberExhibitionYearlyId, langid);
                                            if (langid == 1)
                                                model.Message = "Your application is being processed. You will be notified via email."; // _stringLocalizer["AgencyInProcess"];
                                            else
                                                model.Message = "طلبك تحت النظر سيتم إعلامك عبر البريد الإلكتروني لاحقا"; // _stringLocalizer["AgencyInProcess"];

                                        }
                                        else if (AgencyEntity.Status == EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved))
                                        {
                                            model = CreateInterface(EnumRegistrationViewType.ThankYou, model, AgencyEntity.MemberExhibitionYearlyId, langid);
                                            if (langid == 1)
                                                model.Message = "Your application is approved by the Exhibitor, You will be notified once its approved by SIBF.";// _stringLocalizer["AgencyExhibitorApproved"];
                                            else
                                            {
                                                if (model.WebsiteId == 1)
                                                    model.Message = "تمت الموافقة على الوكالة من قبل العارض، وسيتم إعلامك عند الموافقة من قبل إدارة المعرض"; // _stringLocalizer["AgencyExhibitorApproved"];
                                                else
                                                    model.Message = "تمت الموافقة على الوكالة من قبل العارض، وسيتم إعلامك عند الموافقة من قبل إدارة المهرجان"; // _stringLocalizer["AgencyExhibitorApproved"];
                                            }

                                        }
                                        else// if (AgencyEntity.Status == EnumConversion.ToString(EnumAgencyStatus.SIBFApproved))
                                        {
                                            ExhibitorRegistrationService = new ExhibitorRegistrationService();
                                            var agency = AgencyRegistrationService.GetAgencyRegistrationByItemId(AgencyEntity.MemberExhibitionYearlyId);
                                            var exhibitorDetailsEntity = ExhibitorRegistrationService.GetDetailsById(AgencyEntity.ParentId.Value);
                                            if (agency != null)
                                            {
                                                if (agency.Status != EnumConversion.ToString(EnumExhibitorStatus.Approved))
                                                {
                                                    if (langid == 1)
                                                        model.Message = "Your agency application is approved by SIBF, Once exhibitor is approved you will be shown exhibitor details here."; // _stringLocalizer["AgencySIBFApproved"];
                                                    else
                                                    {
                                                        if (model.WebsiteId == 1)
                                                            model.Message = "تمت الموافقة على التوكيل من قبل إدارة المعرض"; // _stringLocalizer["AgencySIBFApproved"];
                                                        else
                                                            model.Message = "تمت الموافقة على التوكيل من قبل إدارة المهرجان"; // _stringLocalizer["AgencySIBFApproved"];
                                                    }
                                                    model = CreateInterface(EnumRegistrationViewType.ThankYou, model, AgencyEntity.MemberExhibitionYearlyId, langid);
                                                }
                                                else
                                                {
                                                    // GetExhibitorBoothInfo(exhibitorDetailsEntity);
                                                    model = CreateInterface(EnumRegistrationViewType.Step4, model, AgencyEntity.MemberExhibitionYearlyId, langid);
                                                }
                                            }
                                            //else
                                            //{
                                            //    model.Message = "Your agency application is approved by SIBF, Once exhibitor is approved you will be shown exhibitor details here."; //_stringLocalizer["AgencySIBFApproved"];
                                            //    model = CreateInterface(EnumRegistrationViewType.ThankYou, model, AgencyEntity.MemberExhibitionYearlyId);

                                            //}
                                        }
                                    }
                                    else
                                    {
                                        model = CreateInterface(EnumRegistrationViewType.Step1, model, AgencyEntity.MemberExhibitionYearlyId, langid);
                                        //HideStep2Edit();
                                        //  EditStep2 = false;
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        model = CreateInterface(EnumRegistrationViewType.Step1, model, -1, langid);
                        //HideStep2Edit();
                        //EditStep2 = false;
                    }
                }
            }
            return model;
        }
        private long SaveRegistration(SaveAgencyDataForStepTwoAndThreeDTO saveDto, long langid)
        {
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly entity = new XsiExhibitionMemberApplicationYearly();
                entity.ParentId = saveDto.ExhibitorId;
                entity.ExhibitionId = saveDto.ExhibitionId;
                entity.MemberId = saveDto.MemberId;
                entity.MemberRoleId = 2;
                entity.CountryId = saveDto.CountryId;
                entity.CityId = saveDto.CityId;
                //if (ddlExhibitionCity.SelectedItem.Text == EnumConversion.ToString(EnumOtherText.OtherCity))
                if (!string.IsNullOrEmpty(saveDto.OtherCity))
                    entity.OtherCity = saveDto.OtherCity;
                else
                    entity.OtherCity = string.Empty;
                if (saveDto.ExhibitionCategoryId > 0)
                    entity.ExhibitionCategoryId = saveDto.ExhibitionCategoryId;
                entity.LanguageUrl = langid;
                entity.IsEditRequest = EnumConversion.ToString(EnumExhibitorEditStatus.NoEdit);
                entity.PublisherNameEn = saveDto.PublisherNameEn;
                entity.PublisherNameAr = saveDto.PublisherNameAr;
                entity.ContactPersonName = saveDto.ContactPersonName;
                entity.ContactPersonNameAr = saveDto.ContactPersonNameAr;
                entity.ContactPersonTitle = saveDto.ContactPersonTitle;
                entity.ContactPersonTitleAr = saveDto.ContactPersonTitleAr;

                if (!string.IsNullOrEmpty(saveDto.PhoneISD) && !string.IsNullOrEmpty(saveDto.PhoneSTD) && !string.IsNullOrEmpty(saveDto.Phone))
                    entity.Phone = saveDto.PhoneISD + "$" + saveDto.PhoneSTD + "$" + saveDto.Phone;
                if (!string.IsNullOrEmpty(saveDto.MobileISD) && !string.IsNullOrEmpty(saveDto.MobileSTD) && !string.IsNullOrEmpty(saveDto.Mobile))
                    entity.Mobile = saveDto.MobileISD + "$" + saveDto.MobileSTD + "$" + saveDto.Mobile;
                if (!string.IsNullOrEmpty(saveDto.FaxISD) && !string.IsNullOrEmpty(saveDto.FaxSTD) && !string.IsNullOrEmpty(saveDto.Fax))
                    entity.Fax = saveDto.FaxISD + "$" + saveDto.FaxSTD + "$" + saveDto.Fax;

                entity.Email = saveDto.Email;
                entity.Status = EnumConversion.ToString(EnumExhibitorStatus.New);
                entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                entity.IsRegisteredByExhibitor = EnumConversion.ToString(EnumBool.No);
                entity.IsStatusChanged = EnumConversion.ToString(EnumBool.No);
                entity.IsIncludedInInvoice = EnumConversion.ToString(EnumBool.No);
                //entity.IsArabicPublisher = EnumConversion.ToString(EnumBool.No);
                entity.CreatedOn = MethodFactory.ArabianTimeNow();
                entity.CreatedBy = saveDto.MemberId;
                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                entity.ModifiedBy = saveDto.MemberId;

                entity.TotalNumberOfTitles = saveDto.TotalNumberOfTitles.Trim();
                entity.TotalNumberOfNewTitles = saveDto.TotalNumberOfNewTitles.Trim();
                if (saveDto.AuhtorizationLetterBase64 != null && saveDto.AuhtorizationLetterBase64.Length > 0)
                {
                    string fileName = string.Empty;
                    byte[] imageBytes;
                    if (saveDto.AuhtorizationLetterBase64.Contains("data:"))
                    {
                        var strInfo = saveDto.AuhtorizationLetterBase64.Split(",")[0];
                        imageBytes = Convert.FromBase64String(saveDto.AuhtorizationLetterBase64.Split(',')[1]);
                    }
                    else
                    {
                        imageBytes = Convert.FromBase64String(saveDto.AuhtorizationLetterBase64);
                    }

                    fileName = string.Format("{0}_{1}{2}", langid, MethodFactory.GetRandomNumber(), "." + saveDto.AuhtorizationLetterExtension);
                    //if (MethodFactory.IsValidAgencyAuthorizationLetter("." + saveDto.AuhtorizationLetterExtension))
                    //{
                    //    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\AuthorizationLetter\\") + fileName, imageBytes);
                    //    entity.AuthorizationLetter = fileName;
                    //}
                    //else
                    //{
                    //    System.IO.File.WriteAllBytes(
                    //        Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                    //                     "\\ExhibitorRegistration\\AuthorizationLetter\\") + fileName, imageBytes);
                    //}
                    System.IO.File.WriteAllBytes(
                         Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                      "\\ExhibitorRegistration\\AuthorizationLetter\\") + fileName, imageBytes);
                    entity.AuthorizationLetter = fileName;
                }
                if (AgencyRegistrationService.InsertAgencyRegistration(entity) == EnumResultType.Success)
                {
                    ExhibitionService ExhibitionService = new ExhibitionService();
                    var exhibition = ExhibitionService.GetExhibitionByItemId(saveDto.ExhibitionId);
                    saveDto.AgencyId = AgencyRegistrationService.XsiItemdId;
                    //var model = GetAgencyView(saveDto.MemberId, saveDto.WebsiteId, exhibition);

                    // agencyRegistrationDto.Message = _stringLocalizer["AgencyInProcess"];
                    // agencyRegistrationDto.IsStatusMsgVisible = true;

                    LogAgencyStatus(EnumConversion.ToString(EnumExhibitorStatus.New), saveDto.AgencyId);
                }
            }
            return saveDto.AgencyId;
        }
        AgencyRegistrationDTO CreateInterface(EnumRegistrationViewType viewType, AgencyRegistrationDTO model, long agencyid = -1, long langid = 1)
        {
            switch (viewType)
            {
                case EnumRegistrationViewType.Step1:
                    model.StepId = 1;
                    if (agencyid > 0)
                    {
                        model.AgencyId = agencyid;
                        model.StepId = 3;
                        model.StepTwoDTO = LoadStepTwoContent(agencyid);
                        model.StepThreeDTO = LoadStepThreeContent(agencyid);
                    }
                    else
                        model.StepTwoDTO = LoadStepTwoContentByMember(model.MemberId.Value);
                    break;
                case EnumRegistrationViewType.Step2:
                    model.StepId = 2;
                    model.AgencyId = agencyid;
                    model.StepTwoDTO = LoadStepTwoContent(agencyid);
                    model.StepThreeDTO = LoadStepThreeContent(agencyid);
                    break;
                case EnumRegistrationViewType.Step3:
                    model.StepId = 3;
                    model.AgencyId = agencyid;
                    model.StepTwoDTO = LoadStepTwoContent(agencyid);
                    model.StepThreeDTO = LoadStepThreeContent(agencyid);
                    model.StepFourDTO = LoadStepFourContent(agencyid, langid);
                    break;
                case EnumRegistrationViewType.Step4:
                    model.StepId = 4;
                    model.AgencyId = agencyid;
                    model.StepTwoDTO = LoadStepTwoContent(agencyid);
                    model.StepThreeDTO = LoadStepThreeContent(agencyid);
                    model.StepFourDTO = LoadStepFourContent(agencyid, langid);
                    break;
                case EnumRegistrationViewType.ThankYou:
                    model.StepId = 3;
                    model.AgencyId = agencyid;
                    model.StepTwoDTO = LoadStepTwoContent(agencyid);
                    model.StepThreeDTO = LoadStepThreeContent(agencyid);
                    //  model.StepFourDTO = LoadStepFourContent(agencyid);
                    if (langid == 1)
                        model.Message = "Your application is being processed. You will be notified via email.";
                    else
                        model.Message = "Your application is being processed. You will be notified via email.";
                    break;
                case EnumRegistrationViewType.ExhibitionUnavailableView:
                    model.StepId = 1;
                    if (langid == 1)
                        model.Message = "No exhibition available";
                    else
                        model.Message = "No exhibition available";
                    break;
                case EnumRegistrationViewType.RegistrationExpired:
                    model.StepId = 1;
                    if (langid == 1)
                        model.Message = "Registration Expired.";
                    else
                        model.Message = "Registration Expired.";
                    break;
                case EnumRegistrationViewType.RegisteredAsExhibitor:
                    model.StepId = 1;
                    if (langid == 1)
                        model.Message = "Agent Registration cannot be processed due to user registered as Exhibitor. ";
                    else
                        model.Message = "لا يمكن معالجة التسجيل كيل بسبب المستخدم مسجلة العارضين. ";
                    break;
                case EnumRegistrationViewType.Reject:
                    model.StepId = 1;
                    if (langid == 1)
                        model.Message = "We regret that we could not confirm your participation for this year. Better luck next time.";
                    else
                        model.Message = "نعتذر عن عدم قبول مشاركتكم لهذا العام على أمل أن نلقاكم في الأعوام القادمة.";

                    break;
            }
            return model;
        }

        private StepTwoAgencyDTO LoadStepTwoContentByMember(long memberid)
        {
            StepTwoAgencyDTO model = new StepTwoAgencyDTO();
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                var memberentity = ExhibitionMemberService.GetExhibitionMemberByItemId(memberid);
                if (memberentity != null)
                {
                    //model.MemberId = SIBFMemberId;
                    //model.ExhibitionId = ExhibitionId;
                    //model.WebsiteId = WebsiteId;

                    #region Contact fields
                    if (memberentity.CompanyName != null)
                        model.PublisherNameEn = memberentity.CompanyName;
                    if (memberentity.CompanyNameAr != null)
                        model.PublisherNameAr = memberentity.CompanyNameAr;

                    if (memberentity.JobTitle != null)
                        model.ContactPersonTitle = memberentity.JobTitle;
                    if (memberentity.JobTitleAr != null)
                        model.ContactPersonTitleAr = memberentity.JobTitleAr;

                    if (memberentity.Firstname != null && memberentity.LastName != null && !string.IsNullOrEmpty(memberentity.Firstname) && !string.IsNullOrEmpty(memberentity.LastName))
                        model.ContactPersonName = memberentity.Firstname + " " + memberentity.LastName;
                    else
                    {
                        if (memberentity.Firstname != null)
                            model.ContactPersonName = memberentity.Firstname;
                        if (memberentity.LastName != null)
                            model.ContactPersonName = memberentity.LastName;
                    }

                    if (memberentity.FirstNameAr != null && memberentity.LastNameAr != null && !string.IsNullOrEmpty(memberentity.FirstNameAr) && !string.IsNullOrEmpty(memberentity.LastNameAr))
                        model.ContactPersonNameAr = memberentity.FirstNameAr + " " + memberentity.LastNameAr;
                    else
                    {
                        if (memberentity.FirstNameAr != null)
                            model.ContactPersonNameAr = memberentity.FirstNameAr;
                        if (memberentity.LastNameAr != null)
                            model.ContactPersonNameAr = memberentity.LastNameAr;
                    }
                    #endregion
                    if (memberentity.Email != null)
                        model.Email = memberentity.Email;
                    #region Country and city
                    if (memberentity.CityId != null)
                    {
                        if (memberentity.CityId != null)
                        {
                            ExhibitionCityService ExhibitionCityService = new ExhibitionCityService();
                            model.CountryId = ExhibitionCityService.GetExhibitionCityByItemId(memberentity.CityId.Value).CountryId;
                            model.CityId = memberentity.CityId;
                            model.OtherCity = memberentity.OtherCity;
                        }
                    }
                    #endregion
                    #region Phone,Mobile,Fax
                    if (memberentity.Phone != null)
                    {
                        string[] str = memberentity.Phone.Split('$');
                        if (str.Count() == 3)
                        {
                            model.PhoneISD = str[0].Trim();
                            model.PhoneSTD = str[1].Trim();
                            model.Phone = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            model.PhoneSTD = str[0].Trim();
                            model.Phone = str[1].Trim();
                        }
                        else
                            model.Phone = str[0].Trim();
                    }
                    if (memberentity.Mobile != null)
                    {
                        string[] str = memberentity.Mobile.Split('$');
                        if (str.Count() == 3)
                        {
                            model.MobileISD = str[0].Trim();
                            model.MobileSTD = str[1].Trim();
                            model.Mobile = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            model.MobileSTD = str[0].Trim();
                            model.Mobile = str[1].Trim();
                        }
                        else
                            model.Mobile = str[0].Trim();
                    }
                    if (memberentity.Fax != null)
                    {
                        string[] str = memberentity.Fax.Split('$');
                        if (str.Count() == 3)
                        {
                            model.FaxISD = str[0].Trim();
                            model.FaxSTD = str[1].Trim();
                            model.Fax = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            model.FaxSTD = str[0].Trim();
                            model.Fax = str[1].Trim();
                        }
                        else
                            model.Fax = str[0].Trim();
                    }
                    #endregion
                }
            }
            return model;
        }
        private StepTwoAgencyDTO LoadStepTwoContent(long agencyid)
        {
            StepTwoAgencyDTO model = new StepTwoAgencyDTO();
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly agencyentity = AgencyRegistrationService.GetAgencyRegistrationByItemId(agencyid);
                if (agencyentity != null)
                {
                    //model.ExhibitorId = entity.ParentId.Value;
                    //model.AgencyId = entity.MemberId;
                    if (agencyentity.PublisherNameEn != null)
                        model.PublisherNameEn = agencyentity.PublisherNameEn;
                    if (agencyentity.PublisherNameAr != null)
                        model.PublisherNameAr = agencyentity.PublisherNameAr;
                    if (agencyentity.ContactPersonName != null)
                        model.ContactPersonName = agencyentity.ContactPersonName;
                    if (agencyentity.ContactPersonNameAr != null)
                        model.ContactPersonNameAr = agencyentity.ContactPersonNameAr;
                    if (agencyentity.ContactPersonTitle != null)
                        model.ContactPersonTitle = agencyentity.ContactPersonTitle;
                    if (agencyentity.ContactPersonTitleAr != null)
                        model.ContactPersonTitleAr = agencyentity.ContactPersonTitleAr;
                    #region Phone,Mobile,Fax
                    if (agencyentity.Phone != null)
                    {
                        string[] str = agencyentity.Phone.Split('$');
                        if (str.Count() == 3)
                        {
                            model.PhoneISD = str[0].Trim();
                            model.PhoneSTD = str[1].Trim();
                            model.Phone = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            model.PhoneSTD = str[0].Trim();
                            model.Phone = str[1].Trim();
                        }
                        else
                            model.Phone = str[0].Trim();
                    }
                    if (agencyentity.Mobile != null)
                    {
                        string[] str = agencyentity.Mobile.Split('$');
                        if (str.Count() == 3)
                        {
                            model.MobileISD = str[0].Trim();
                            model.MobileSTD = str[1].Trim();
                            model.Mobile = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            model.MobileSTD = str[0].Trim();
                            model.Mobile = str[1].Trim();
                        }
                        else
                            model.Mobile = str[0].Trim();
                    }
                    if (agencyentity.Fax != null)
                    {
                        string[] str = agencyentity.Fax.Split('$');
                        if (str.Count() == 3)
                        {
                            model.FaxISD = str[0].Trim();
                            model.FaxSTD = str[1].Trim();
                            model.Fax = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            model.FaxSTD = str[0].Trim();
                            model.Fax = str[1].Trim();
                        }
                        else
                            model.Fax = str[0].Trim();
                    }
                    #endregion
                    if (agencyentity.Email != null)
                        model.Email = agencyentity.Email;
                    #region Country and city
                    if (agencyentity.CountryId != null)
                    {
                        model.CountryId = agencyentity.CountryId;
                        if (agencyentity.CityId != null)
                        {
                            model.CityId = agencyentity.CityId;
                            if (model.CityId.ToString() == EnumConversion.ToString(EnumOtherText.OtherCity))
                            {
                                // model.IsOtherCityVisible = true;
                                model.OtherCity = agencyentity.OtherCity;
                            }
                            else
                            {
                                //  model.IsOtherCityVisible = false;
                                model.OtherCity = string.Empty;
                            }
                        }
                    }

                    #endregion
                    if (agencyentity.ExhibitionCategoryId != null)
                        model.ExhibitionCategoryId = agencyentity.ExhibitionCategoryId;
                }
            }
            return model;
        }
        private StepThreeAgencyDTO LoadStepThreeContent(long agencyid)
        {
            StepThreeAgencyDTO model = new StepThreeAgencyDTO();
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                XsiExhibitionMemberApplicationYearly agencyentity = AgencyRegistrationService.GetAgencyRegistrationByItemId(agencyid);
                if (agencyentity != null)
                {
                    if (agencyentity.ParentId != null)
                        model.ExhibitorId = agencyentity.ParentId.Value;
                    if (agencyentity.TotalNumberOfNewTitles != null)
                        model.TotalNumberOfNewTitles = agencyentity.TotalNumberOfNewTitles;
                    if (agencyentity.TotalNumberOfTitles != null)
                        model.TotalNumberOfTitles = agencyentity.TotalNumberOfTitles;
                    if (!string.IsNullOrEmpty(agencyentity.AuthorizationLetter))
                        model.AuhtorizationLetterFileName = _appCustomSettings.UploadsCMSPath + "/ExhibitorRegistration/AuthorizationLetter/" + agencyentity.AuthorizationLetter;

                    if (!string.IsNullOrEmpty(agencyentity.IsBooksUpload))
                    {
                        model.IsBooksUpload = agencyentity.IsBooksUpload == "Y" ? "Y" : "N";
                    }
                    else
                    {
                        model.IsBooksUpload = "N";
                    }

                    if (agencyentity.BookFileName != null && !string.IsNullOrEmpty(agencyentity.BookFileName))
                        model.BooksExcelFileName = agencyentity.BookFileName;

                    using (ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService())
                    {
                        string strCountryEn = string.Empty;
                        string strCountryAr = string.Empty;
                        XsiExhibitionMemberApplicationYearly exhibitorrentity = new XsiExhibitionMemberApplicationYearly();
                        exhibitorrentity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(agencyentity.ParentId.Value);
                        if (exhibitorrentity != default(XsiExhibitionMemberApplicationYearly))
                        {
                            if (exhibitorrentity.CountryId != null)
                            {
                                ExhibitionCountryService ExhibitionCountryService = new ExhibitionCountryService();
                                var ExhibitionCountry = ExhibitionCountryService.GetExhibitionCountryByItemId(exhibitorrentity.CountryId.Value);
                                if (ExhibitionCountry != null)
                                {
                                    strCountryEn = ExhibitionCountry.CountryName;
                                    strCountryAr = ExhibitionCountry.CountryNameAr;
                                }
                                if (!string.IsNullOrEmpty(strCountryEn))
                                    model.PublisherAutoCompleteEn = exhibitorrentity.PublisherNameEn + "," + strCountryEn;
                                else
                                    model.PublisherAutoCompleteEn = exhibitorrentity.PublisherNameEn;
                                if (!string.IsNullOrEmpty(strCountryAr))
                                    model.PublisherAutoCompleteAr = exhibitorrentity.PublisherNameAr + "," + strCountryAr;
                                else
                                    model.PublisherAutoCompleteAr = exhibitorrentity.PublisherNameAr;
                            }
                        }
                    }
                }
            }
            return model;
        }
        private StepFourAgencyDTO LoadStepFourContent(long agencyid, long langid)
        {
            StepFourAgencyDTO model = new StepFourAgencyDTO();
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                var existingagency = AgencyRegistrationService.GetAgencyRegistrationByItemId(agencyid);
                XsiExhibitionExhibitorDetails exhibitorentity = new XsiExhibitionExhibitorDetails();
                exhibitorentity = ExhibitorRegistrationService.GetDetailsById(existingagency.ParentId.Value);
                if (exhibitorentity != null)
                {
                    #region Section
                    if (exhibitorentity.BoothSectionId != null)
                    {
                        ExhibitionBoothService ExhibitionBoothService = new ExhibitionBoothService();
                        XsiExhibitionBooth where = new XsiExhibitionBooth();
                        where.ItemId = exhibitorentity.BoothSectionId.Value;
                        //where.LanguageUrl = EnglishId;
                        XsiExhibitionBooth entity1 = ExhibitionBoothService.GetExhibitionBooth(where).FirstOrDefault();
                        if (entity1 != null)
                        {
                            if (langid == 1)
                            {
                                if (entity1.Title != null)
                                    model.Section = entity1.Title;
                            }
                            else
                            {
                                if (entity1.TitleAr != null)
                                    model.Section = entity1.TitleAr;
                            }
                            //model.BoothSectionId = entity1.ItemId;


                            #region SubSection
                            if (exhibitorentity.BoothSubSectionId != null)
                            {
                                ExhibitionBoothSubsectionService ExhibitionBoothSubsectionService = new ExhibitionBoothSubsectionService();
                                XsiExhibitionBoothSubsection where1 = new XsiExhibitionBoothSubsection();
                                where1.ItemId = exhibitorentity.BoothSubSectionId.Value;
                                where1.CategoryId = entity1.ItemId;
                                XsiExhibitionBoothSubsection boothsubsection = ExhibitionBoothSubsectionService.GetExhibitionBoothSubsection(where1).FirstOrDefault();
                                if (boothsubsection != null)
                                {
                                    if (langid == 1)
                                    {
                                        if (boothsubsection.Title != null)
                                            model.SubSection = boothsubsection.Title;
                                    }
                                    else
                                    {
                                        if (boothsubsection.TitleAr != null)
                                            model.SubSection = boothsubsection.TitleAr;
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion

                    if (exhibitorentity.AllocatedSpace != null && !string.IsNullOrEmpty(exhibitorentity.AllocatedSpace))
                        model.Area = exhibitorentity.AllocatedSpace;
                    if (exhibitorentity.RequiredAreaType != null)
                    {
                        if (exhibitorentity.RequiredAreaType == EnumConversion.ToString(EnumRequiredAreaType.Open))
                        {
                            model.Area = exhibitorentity.Area;
                        }
                    }

                    if (exhibitorentity.HallNumber != null && !string.IsNullOrEmpty(exhibitorentity.HallNumber))
                        model.HallNumber = exhibitorentity.HallNumber;

                    if (exhibitorentity.StandNumberStart != null && exhibitorentity.StandNumberEnd != null)
                        if (exhibitorentity.StandNumberStart != string.Empty && exhibitorentity.StandNumberEnd != string.Empty)
                            model.Stand = exhibitorentity.StandNumberEnd + exhibitorentity.StandNumberStart;

                    if (existingagency.UploadLocationMap != null && !string.IsNullOrEmpty(existingagency.UploadLocationMap))
                        model.UploadLocationMap = "/Content/Uploads/ExhibitorRegistration/LocationMap/" + existingagency.UploadLocationMap;
                }
            }
            return model;
        }
        #endregion
    }
    public class GetExhibitorsResult
    {
        public long ItemId { get; set; }
        public string ExhibitorArabic { get; set; }
        public string ExhibitorEnglish { get; set; }
    }
}

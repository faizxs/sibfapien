﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using System.IO;
using OfficeOpenXml;
using Xsi.ServicesLayer;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class BooksSearchController : ControllerBase
    {
        private IMemoryCache _cache;
        private ILoggerManager _logger;
        private readonly sibfnewdbContext _context;
        private readonly AppCustomSettings _appCustomSettings;

        public BooksSearchController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IMemoryCache memoryCache, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _cache = memoryCache;
            _context = context;
        }

        public List<XsiExhibitionMemberApplicationYearly> cachedExhibitionMemberApplicationYearly { get; set; }
        public List<XsiGuests> cachedGuest { get; set; }
        public List<XsiExhibitionBooks> cachedBooks { get; set; }
        public List<XsiExhibitionBookParticipating> cachedBookParticipating { get; set; }
        public List<XsiExhibitionBookAuthor> cachedBookAuthors { get; set; }
        public List<XsiExhibitionAuthor> cachedAuthors { get; set; }
        public List<XsiExhibitionCategory> cachedExhibitionCategory { get; set; }
        public List<XsiExhibitionExhibitorDetails> cachedExhibitionExhibitorDetails { get; set; }
        #region Get Methods
        [HttpGet]
        [Route("ApprovedExhibitor/{websiteid}")]
        public ActionResult<dynamic> GetApprovedExhibitors(long websiteid)
        {
            try
            {
                websiteid = 1;
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);

                cachedExhibitionMemberApplicationYearly = (List<XsiExhibitionMemberApplicationYearly>)_cache.Get<dynamic>(CacheKeys.ExhibitionMemberApplicationYearlyCacheList);
                if (cachedExhibitionMemberApplicationYearly == null)
                    cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

                var exhibition = MethodFactory.GetActiveExhibitionNew(websiteid, _cache);

                if (exhibition != null)
                {
                    if (langId == 1)
                    {
                        var exhibitors = cachedExhibitionMemberApplicationYearly.AsQueryable()
                            .Where(i => i.Status == "A" && i.IsActive == "Y" && i.ExhibitionId == exhibition.ExhibitionId && (i.MemberRoleId == 1 || i.MemberRoleId == 2)).Select(i => new DropdownDataDTO()
                            { ItemId = i.MemberExhibitionYearlyId, Title = i.PublisherNameEn ?? i.PublisherNameAr }).OrderBy(i => i.Title).ToList();

                        return Ok(exhibitors);
                    }
                    else
                    {
                        var exhibitors = cachedExhibitionMemberApplicationYearly.AsQueryable()
                           .Where(i => i.Status == "A" && i.IsActive == "Y" && i.ExhibitionId == exhibition.ExhibitionId && (i.MemberRoleId == 1 || i.MemberRoleId == 2)).Select(i => new DropdownDataDTO()
                           { ItemId = i.MemberExhibitionYearlyId, Title = !string.IsNullOrEmpty(i.PublisherNameAr) ? i.PublisherNameAr : i.PublisherNameEn }).OrderBy(i => i.Title).ToList();

                        return Ok(exhibitors);
                    }
                }
                else
                {
                    return Ok(new List<DropdownDataDTO>());
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpGet]
        [Route("ApprovedExhibitorFromParticipatingBooks/{websiteid}")]
        public ActionResult<dynamic> GetApprovedExhibitorsParticipatingBooks(long websiteid)
        {
            try
            {
                websiteid = 1;
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);

                AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService();
                cachedBookParticipating = (List<XsiExhibitionBookParticipating>)_cache.Get<dynamic>(CacheKeys.BookParticipating);
                if (cachedBookParticipating == null)
                    cachedBookParticipating = CacheKeys.GetBooksParticipatingList(_cache);

                var participatingexhibitorids = cachedBookParticipating.Select(i => i.ExhibitorId).ToList();

                cachedExhibitionMemberApplicationYearly = (List<XsiExhibitionMemberApplicationYearly>)_cache.Get<dynamic>(CacheKeys.ExhibitionMemberApplicationYearlyCacheList);
                if (cachedExhibitionMemberApplicationYearly == null)
                    cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);

                var exhibition = MethodFactory.GetActiveExhibitionNew(websiteid, _cache);

                if (langId == 1)
                {
                    var exhibitors = cachedExhibitionMemberApplicationYearly.AsQueryable()
                        .Where(i => i.Status == "A" && i.IsActive == "Y" && i.ExhibitionId == exhibition.ExhibitionId && participatingexhibitorids.Contains(i.MemberExhibitionYearlyId)).Select(i => new DropdownDataDTO()
                        { ItemId = i.MemberExhibitionYearlyId, Title = i.PublisherNameEn ?? i.PublisherNameAr }).OrderBy(i => i.Title).ToList();

                    var agencylist = AgencyRegistrationService.GetAgencyRegistration(new XsiExhibitionMemberApplicationYearly() { Status = "A", ExhibitionId = exhibition.ExhibitionId, MemberRoleId = 2 })
                        .Where(i => participatingexhibitorids.Contains(i.ParentId))
                        .Select(i => new DropdownDataDTO()
                        { ItemId = i.MemberExhibitionYearlyId, Title = i.PublisherNameEn ?? i.PublisherNameAr }).OrderBy(i => i.Title).ToList();

                    if (exhibitors.Count > 0 && agencylist.Count > 0)
                    {
                        exhibitors.AddRange(agencylist);
                        exhibitors = exhibitors.OrderBy(i => i.Title).ToList();
                    }
                    else if (agencylist.Count > 0)
                    {
                        agencylist = agencylist.OrderBy(i => i.Title).ToList();
                        return Ok(agencylist);
                    }
                    return Ok(exhibitors);
                }
                else
                {
                    var exhibitors = cachedExhibitionMemberApplicationYearly.AsQueryable()
                        .Where(i => i.Status == "A" && i.IsActive == "Y" && i.ExhibitionId == exhibition.ExhibitionId)
                        .Select(i => new DropdownDataDTO()
                        { ItemId = i.MemberExhibitionYearlyId, Title = !string.IsNullOrEmpty(i.PublisherNameAr) ? i.PublisherNameAr : i.PublisherNameEn }).ToList();

                    var agencylist = AgencyRegistrationService.GetAgencyRegistration(new XsiExhibitionMemberApplicationYearly() { Status = "A", ExhibitionId = exhibition.ExhibitionId, MemberRoleId = 2 })
                        .Where(i => participatingexhibitorids.Contains(i.ParentId))
                        .Select(i => new DropdownDataDTO()
                        { ItemId = i.MemberExhibitionYearlyId, Title = !string.IsNullOrEmpty(i.PublisherNameAr) ? i.PublisherNameAr : i.PublisherNameEn }).ToList();

                    if (exhibitors.Count > 0 && agencylist.Count > 0)
                    {
                        exhibitors.AddRange(agencylist);
                        exhibitors = exhibitors.OrderBy(i => i.Title).ToList();
                    }
                    else if (agencylist.Count > 0)
                    {
                        agencylist = agencylist.OrderBy(i => i.Title).ToList();
                        return Ok(agencylist);
                    }
                    return Ok(exhibitors);
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpGet]
        [Route("bookauthors/{bookid}")]
        public ActionResult<dynamic> GetBookAuthors(long bookid)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);
                return Ok(GetAuthorName(bookid, langId));
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpGet]
        [Route("bookdetail/{bookid}")]
        public ActionResult<dynamic> GetBookDetails(long bookid)
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);
            using (ExhibitionBookService BookService = new ExhibitionBookService())
            {
                var book = new BooksDetailsDTO();
                if (langId == 1)
                {
                    var entity = _context.XsiExhibitionBooks.Where(x => x.BookId == bookid).FirstOrDefault();
                    if (entity != null)
                    {
                        var booksparticpatingEntity = _context.XsiExhibitionBookParticipating.Where(i => i.BookId == bookid).OrderByDescending(i => i.ItemId).FirstOrDefault();
                        //var bookParticipating = _context.XsiExhibitionBookParticipating.Where(x => x.BookId == bookid).OrderByDescending(i=>i.ItemId).FirstOrDefault();
                        book.BookId = entity.BookId;
                        book.Title = entity.TitleEn;
                        book.AuthorName = entity.AuthorName; //GetAuthorName(entity.BookId, langId);
                                                             //if (entity.BookPublisher != null)
                        book.PublisherName = entity.BookPublisherName; // entity.BookPublisher.Title;
                        book.Price = entity.Price;
                        if (entity.ExhibitionSubjectId != null)
                            book.Subject = GetSubject(entity.ExhibitionSubjectId.Value, langId);
                        if (entity.ExhibitionSubsubjectId != null)
                            book.SubSubject = GetSubSubject(entity.ExhibitionSubsubjectId.Value, langId);

                        book.Thumbnail = _appCustomSettings.UploadsCMSPath + "/Books/Thumbnails/" + (string.IsNullOrEmpty(entity.Thumbnail) ? "default-book_en.jpg" : entity.Thumbnail);
                        book.Image = _appCustomSettings.UploadsCMSPath + "/Books/Thumbnails/" + (string.IsNullOrEmpty(entity.Thumbnail) ? "default-book_en.jpg" : entity.Thumbnail);
                        if (booksparticpatingEntity != null)
                        {
                            var Exhibitor = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == booksparticpatingEntity.ExhibitorId).FirstOrDefault();
                            if (Exhibitor != null)
                            {
                                var Country = _context.XsiExhibitionCountry.Where(i => i.CountryId == Exhibitor.CountryId).FirstOrDefault();
                                if (Country != null)
                                    book.Country = Country.CountryName;

                                var details = _context.XsiExhibitionExhibitorDetails.Where(i => i.MemberExhibitionYearlyId == booksparticpatingEntity.ExhibitorId).FirstOrDefault();
                                if (details != null)
                                {
                                    book.HallNumber = details.HallNumber;

                                    if (details.BoothSectionId != null)
                                        book.BoothSection = GetBoothSection(details.BoothSectionId.Value, langId);

                                    if (details.BoothSubSectionId != null)
                                        book.BoothSubSection = GetBoothSubSection(details.BoothSubSectionId.Value, langId);

                                    book.StandNumber = details.StandNumberEnd + details.StandNumberStart;
                                }
                            }
                        }
                    }
                    return Ok(book);
                }
                else
                {

                    var entity = _context.XsiExhibitionBooks.Where(x => x.BookId == bookid).FirstOrDefault();
                    if (entity != null)
                    {
                        var booksparticpatingEntity = _context.XsiExhibitionBookParticipating.Where(x => x.BookId == bookid).OrderByDescending(i => i.ItemId).FirstOrDefault();
                        book.BookId = entity.BookId;
                        book.Title = entity.TitleEn;
                        book.AuthorName = entity.AuthorNameAr; //GetAuthorName(entity.BookId, langId);
                                                               //if (entity.BookPublisher != null)
                        book.PublisherName = entity.BookPublisherNameAr; // entity.BookPublisher.Title;
                        book.Price = entity.Price;

                        if (entity.ExhibitionSubjectId != null)
                            book.Subject = GetSubject(entity.ExhibitionSubjectId.Value, langId);

                        if (entity.ExhibitionSubsubjectId != null)
                            book.SubSubject = GetSubSubject(entity.ExhibitionSubsubjectId.Value, langId);

                        book.Thumbnail = _appCustomSettings.UploadsCMSPath + "/Books/Thumbnails/" + (string.IsNullOrEmpty(entity.Thumbnail) ? "default-book_ar.jpg" : entity.Thumbnail);
                        book.Image = _appCustomSettings.UploadsCMSPath + "/Books/Thumbnails/" + (string.IsNullOrEmpty(entity.Thumbnail) ? "default-book_en.jpg" : entity.Thumbnail);

                        if (booksparticpatingEntity != null)
                        {
                            var Exhibitor = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == booksparticpatingEntity.ExhibitorId).FirstOrDefault();
                            if (Exhibitor != null)
                            {
                                var Country = _context.XsiExhibitionCountry.Where(i => i.CountryId == Exhibitor.CountryId).FirstOrDefault();
                                if (Country != null)
                                    book.Country = Country.CountryName;

                                var details = _context.XsiExhibitionExhibitorDetails.Where(i => i.MemberExhibitionYearlyId == booksparticpatingEntity.ExhibitorId).FirstOrDefault();
                                if (details != null)
                                {
                                    book.HallNumber = details.HallNumber;

                                    if (details.BoothSectionId != null)
                                        book.BoothSection = GetBoothSection(details.BoothSectionId.Value, langId);

                                    if (details.BoothSubSectionId != null)
                                        book.BoothSubSection = GetBoothSubSection(details.BoothSubSectionId.Value, langId);

                                    book.StandNumber = details.StandNumberEnd + details.StandNumberStart; //details.StandNumberStart + details.StandNumberEnd;
                                }
                            }
                        }
                    }
                    return Ok(book);
                }
            }
        }
        [HttpGet]
        [Route("refill")]
        public async Task<ActionResult<dynamic>> RefillCache()
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);
                if (langId == 7)
                {
                    CacheKeys.RemoveCacheForAll(_cache);
                    CacheKeys.AddCacheForAll(_cache);
                }
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }
        #endregion
        #region Post Methods
        [HttpPost]
        [Route("BooksAutoSearch")]
        public async Task<ActionResult<dynamic>> GetBooksAutoSearch(BooksAutoSearch model)
        {
            try
            {
                cachedBooks = (List<XsiExhibitionBooks>)_cache.Get<dynamic>(CacheKeys.Books);
                cachedBookParticipating = (List<XsiExhibitionBookParticipating>)_cache.Get<dynamic>(CacheKeys.BookParticipating);
                cachedExhibitionMemberApplicationYearly = (List<XsiExhibitionMemberApplicationYearly>)_cache.Get<dynamic>(CacheKeys.ExhibitionMemberApplicationYearlyCacheList);
                if (cachedBooks == null)
                    cachedBooks = CacheKeys.GetBookList(_cache);
                if (cachedBookParticipating == null)
                    cachedBookParticipating = CacheKeys.GetBooksParticipatingList(_cache);
                if (cachedExhibitionMemberApplicationYearly == null)
                    cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);
                //model.WebSiteId = 2;
                //var exhibition = MethodFactory.GetActiveExhibitionNew(model.WebSiteId, _cache);

                long langid = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langid);
                //var exhibition = MethodFactory.GetActiveExhibition(model.WebSiteId, _context);

                var exhibition = MethodFactory.GetActiveExhibitionNew(model.WebSiteId, _cache);

                if (exhibition != null)
                {
                    var predicate = PredicateBuilder.New<XsiExhibitionBooks>();
                    var predicateBookParticipating = PredicateBuilder.New<XsiExhibitionBookParticipating>();

                    #region Search Parameter

                    if (langid == 1)
                    {
                        #region English Fields
                        if (!string.IsNullOrEmpty(model.BookTitle))
                        {
                            var title = model.BookTitle.ToLower();
                            predicate = predicate.And(p => (p.TitleEn != null && p.TitleEn.Contains(title, StringComparison.OrdinalIgnoreCase)) || (p.TitleAr != null && p.TitleAr.Contains(title, StringComparison.OrdinalIgnoreCase)));
                        }
                        #endregion
                    }
                    else
                    {
                        #region Arabic Fields
                        if (!string.IsNullOrEmpty(model.BookTitle))
                        {
                            var titleen = model.BookTitle.ToLower();
                            var title = model.BookTitle;
                            //  predicate = predicate.And(p => (p.TitleEn != null && p.TitleEn.Contains(titleen, StringComparison.OrdinalIgnoreCase)) || (p.TitleAr != null && p.TitleAr.Contains(title, StringComparison.OrdinalIgnoreCase)));
                            if (!string.IsNullOrEmpty(title))
                            {
                                string str1 = title;
                                string str2 = title.Replace("أ", "إ");
                                string str3 = title.Replace("أ", "ا");
                                string str4 = title.Replace("إ", "أ");
                                string str5 = title.Replace("إ", "ا");
                                string str6 = title.Replace("ا", "أ");
                                string str7 = title.Replace("ا", "إ");

                                string str8 = title.Replace("ا", "إ");
                                string str9 = title.Replace("ا", "أ");

                                string str10 = title.Replace("إ", "أ");
                                string str11 = title.Replace("إ", "ا");

                                string str12 = title.Replace("إ", "أ");
                                string str13 = title.Replace("إ", "ا");

                                string str14 = title.Replace("ه", "ة");
                                string str15 = title.Replace("ة", "ه");

                                string str16 = title.Replace("ى", "ي");
                                string str17 = title.Replace("ي", "ى");

                                string str18 = title.Replace("ا", "ء");
                                string str19 = title.Replace("ا", "آ");
                                string str20 = title.Replace("ا", "ؤ");
                                string str21 = title.Replace("ا", "ئ");
                                string str22 = title.Replace("ا", "ا");

                                string str23 = title.Replace("ي", "ي");

                                string str24 = title.Replace("ة", "ة");
                                string str25 = title.Replace("ة", "ه");

                                string str26 = title.Replace("و", "و");
                                string str27 = title.Replace("و", "ؤ");


                                predicate = predicate.And(p => (p.TitleEn != null && p.TitleEn.Contains(titleen, StringComparison.OrdinalIgnoreCase))
                                                    || ((p.TitleAr != null) && (p.TitleAr.Contains(str1) || p.TitleAr.Contains(str2) || p.TitleAr.Contains(str3)
                                                    || p.TitleAr.Contains(str4) || p.TitleAr.Contains(str5) || p.TitleAr.Contains(str6) || p.TitleAr.Contains(str7)
                                                   || p.TitleAr.Contains(str8) || p.TitleAr.Contains(str9) || p.TitleAr.Contains(str10) || p.TitleAr.Contains(str11)
                                                       || p.TitleAr.Contains(str12) || p.TitleAr.Contains(str13) || p.TitleAr.Contains(str14) || p.TitleAr.Contains(str15)
                                                         || p.TitleAr.Contains(str16) || p.TitleAr.Contains(str17)
                                                         || p.TitleAr.Contains(str18) || p.TitleAr.Contains(str19) || p.TitleAr.Contains(str20) || p.TitleAr.Contains(str21)
                                                          || p.TitleAr.Contains(str22) || p.TitleAr.Contains(str23) || p.TitleAr.Contains(str24) || p.TitleAr.Contains(str25)
                                                          || p.TitleAr.Contains(str26) || p.TitleAr.Contains(str27)))
                                                  );

                            }
                        }
                        #endregion
                    }
                    predicateBookParticipating = predicateBookParticipating.And(i => i.IsActive == "Y");
                    predicate = predicate.And(i => i.IsActive == "Y" && i.IsEnable == "Y");
                    #endregion

                    if (langid == 1)
                    {
                        var list = (from b in cachedBooks.AsQueryable().Where(predicate)
                                    join bp in cachedBookParticipating.AsQueryable().Where(predicateBookParticipating) on b.BookId equals bp.BookId
                                    join y in cachedExhibitionMemberApplicationYearly on bp.ExhibitorId equals y
                                        .MemberExhibitionYearlyId
                                    where y.ExhibitionId == exhibition.ExhibitionId && (y.Status == "A" || y.Status == "I")
                                    select new
                                    {
                                        BookId = bp.BookId,
                                        Title = b.TitleEn ?? b.TitleAr,
                                    }).Take(20).OrderBy(i => i.Title).ToList();
                        var dto = new { list };
                        return Ok(dto);
                    }
                    else
                    {
                        var list = (from b in cachedBooks.AsQueryable().Where(predicate)
                                    join bp in cachedBookParticipating.AsQueryable().Where(predicateBookParticipating) on b.BookId equals bp.BookId
                                    join y in cachedExhibitionMemberApplicationYearly on bp.ExhibitorId equals y
                                        .MemberExhibitionYearlyId
                                    where y.ExhibitionId == exhibition.ExhibitionId && (y.Status == "A" || y.Status == "I")
                                    select new
                                    {
                                        BookId = bp.BookId,
                                        Title = !string.IsNullOrEmpty(b.TitleAr) ? b.TitleAr : b.TitleEn,
                                    }).Take(20).OrderBy(i => i.Title).ToList();
                        var dto = new { list };
                        return Ok(dto);
                    }
                }
                return Ok("No data found");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");

            }
        }
        [HttpPost]
        public async Task<ActionResult<dynamic>> GetBooks(BooksSearchModel model)
        {
            try
            {
                //  cachedBookAuthors = (List<XsiExhibitionBookAuthor>)_cache.Get<dynamic>(CacheKeys.BookAuthors);
                // cachedAuthors = (List<XsiExhibitionAuthor>)_cache.Get<dynamic>(CacheKeys.Authors);
                //if (cachedBookAuthors == null)
                //  cachedBookAuthors = CacheKeys.GetBookAuthorsList(_cache);
                //if (cachedAuthors == null)
                //  cachedAuthors = CacheKeys.GetAuthorsList(_cache);

                cachedBooks = (List<XsiExhibitionBooks>)_cache.Get<dynamic>(CacheKeys.Books);
                cachedBookParticipating = (List<XsiExhibitionBookParticipating>)_cache.Get<dynamic>(CacheKeys.BookParticipating);
                cachedExhibitionMemberApplicationYearly = (List<XsiExhibitionMemberApplicationYearly>)_cache.Get<dynamic>(CacheKeys.ExhibitionMemberApplicationYearlyCacheList);
                if (cachedBooks == null)
                    cachedBooks = CacheKeys.GetBookList(_cache);
                if (cachedBookParticipating == null)
                    cachedBookParticipating = CacheKeys.GetBooksParticipatingList(_cache);
             
                if (cachedExhibitionMemberApplicationYearly == null)
                    cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);
                //  model.WebSiteId = 2;

                var exhibition = MethodFactory.GetActiveExhibitionNew(model.WebSiteId, _cache);

                long langid = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langid);
                //var exhibition = MethodFactory.GetActiveExhibition(model.WebSiteId, _context);
                if (exhibition != null)
                {
                    var predicate = PredicateBuilder.New<XsiExhibitionBooks>();
                    var predicateBookParticipating = PredicateBuilder.New<XsiExhibitionBookParticipating>();

                    #region Search Parameter

                    if (langid == 1)
                    {
                        #region English Fields
                        if (!string.IsNullOrEmpty(model.BookTitle))
                        {
                            var title = model.BookTitle.ToLower();
                            predicate = predicate.And(p => (p.TitleEn != null && p.TitleEn.Contains(title, StringComparison.OrdinalIgnoreCase)) || (p.TitleAr != null && p.TitleAr.Contains(title, StringComparison.OrdinalIgnoreCase)));
                        }
                        if (!string.IsNullOrEmpty(model.Author))
                        {
                            //var title = model.Author.ToLower();
                            //HashSet<long> booksIds = new HashSet<long>(cachedAuthors.Join(
                            //       cachedBookAuthors,
                            //        a => a.AuthorId,
                            //        ea => ea.AuthorId,
                            //        (a, ea) => new { a, ea }
                            //    ).Where(p => p.a.Title != null && p.a.Title.Contains(title, StringComparison.OrdinalIgnoreCase))
                            //    .Select(p => p.ea.BookId));

                            //predicate = predicate.And(p => booksIds.Contains(p.BookId));
                            predicate = predicate.And(p => p.AuthorName != null && p.AuthorName.Contains(model.Author));
                        }
                        if (!string.IsNullOrEmpty(model.Publisher))
                        {
                            predicate = predicate.And(p => p.BookPublisherName != null && p.BookPublisherName.Contains(model.Publisher));
                            //var title = model.Publisher.ToLower();
                            //HashSet<long> bookpublisherIds = new HashSet<long>(_context.XsiExhibitionBookPublisher.Where(p => p.Title != null && p.Title.Contains(title, StringComparison.OrdinalIgnoreCase))
                            //    .Select(p => p.ItemId));

                            //predicate = predicate.And(p => bookpublisherIds.Contains(p.BookPublisherId.Value));
                        }
                        #endregion
                    }
                    else
                    {
                        #region Arabic Fields
                        if (!string.IsNullOrEmpty(model.BookTitle))
                        {
                            var titleen = model.BookTitle.ToLower();
                            var title = model.BookTitle;
                            //  predicate = predicate.And(p => (p.TitleEn != null && p.TitleEn.Contains(titleen, StringComparison.OrdinalIgnoreCase)) || (p.TitleAr != null && p.TitleAr.Contains(title, StringComparison.OrdinalIgnoreCase)));
                            if (!string.IsNullOrEmpty(title))
                            {
                                string str1 = title;
                                string str2 = title.Replace("أ", "إ");
                                string str3 = title.Replace("أ", "ا");
                                string str4 = title.Replace("إ", "أ");
                                string str5 = title.Replace("إ", "ا");
                                string str6 = title.Replace("ا", "أ");
                                string str7 = title.Replace("ا", "إ");

                                string str8 = title.Replace("ا", "إ");
                                string str9 = title.Replace("ا", "أ");

                                string str10 = title.Replace("إ", "أ");
                                string str11 = title.Replace("إ", "ا");

                                string str12 = title.Replace("إ", "أ");
                                string str13 = title.Replace("إ", "ا");

                                string str14 = title.Replace("ه", "ة");
                                string str15 = title.Replace("ة", "ه");

                                string str16 = title.Replace("ى", "ي");
                                string str17 = title.Replace("ي", "ى");

                                string str18 = title.Replace("ا", "ء");
                                string str19 = title.Replace("ا", "آ");
                                string str20 = title.Replace("ا", "ؤ");
                                string str21 = title.Replace("ا", "ئ");
                                string str22 = title.Replace("ا", "ا");

                                string str23 = title.Replace("ي", "ي");

                                string str24 = title.Replace("ة", "ة");
                                string str25 = title.Replace("ة", "ه");

                                string str26 = title.Replace("و", "و");
                                string str27 = title.Replace("و", "ؤ");


                                predicate = predicate.And(p => (p.TitleEn != null && p.TitleEn.Contains(titleen, StringComparison.OrdinalIgnoreCase))
                                                    || ((p.TitleAr != null) && (p.TitleAr.Contains(str1) || p.TitleAr.Contains(str2) || p.TitleAr.Contains(str3)
                                                    || p.TitleAr.Contains(str4) || p.TitleAr.Contains(str5) || p.TitleAr.Contains(str6) || p.TitleAr.Contains(str7)
                                                   || p.TitleAr.Contains(str8) || p.TitleAr.Contains(str9) || p.TitleAr.Contains(str10) || p.TitleAr.Contains(str11)
                                                       || p.TitleAr.Contains(str12) || p.TitleAr.Contains(str13) || p.TitleAr.Contains(str14) || p.TitleAr.Contains(str15)
                                                         || p.TitleAr.Contains(str16) || p.TitleAr.Contains(str17)
                                                         || p.TitleAr.Contains(str18) || p.TitleAr.Contains(str19) || p.TitleAr.Contains(str20) || p.TitleAr.Contains(str21)
                                                          || p.TitleAr.Contains(str22) || p.TitleAr.Contains(str23) || p.TitleAr.Contains(str24) || p.TitleAr.Contains(str25)
                                                          || p.TitleAr.Contains(str26) || p.TitleAr.Contains(str27)))
                                                  );

                            }
                        }

                        if (!string.IsNullOrEmpty(model.Author))
                        {
                            var Author = model.Author.ToLower();
                            //HashSet<long> booksIds = new HashSet<long>(cachedAuthors.Join(
                            //       cachedBookAuthors,
                            //        a => a.AuthorId,
                            //        ea => ea.AuthorId,
                            //        (a, ea) => new { a, ea }
                            //    ).Where(p => p.a.TitleAr != null && p.a.TitleAr.Contains(title, StringComparison.OrdinalIgnoreCase))
                            //    .Select(p => p.ea.BookId));

                            //predicate = predicate.And(p => booksIds.Contains(p.BookId));
                            if (!string.IsNullOrEmpty(Author))
                            {
                                //string str = txtSearchKeyword.Text;
                                //string str1 = txtSearchKeyword.Text;
                                //RepresentativeAllList = RepresentativeAllList.Where(i => i.NameEn.Contains(str) || i.NameAr.Contains(str1)).ToList();
                                string str1 = Author;
                                string str2 = Author.Replace("أ", "إ");
                                string str3 = Author.Replace("أ", "ا");
                                string str4 = Author.Replace("إ", "أ");
                                string str5 = Author.Replace("إ", "ا");
                                string str6 = Author.Replace("ا", "أ");
                                string str7 = Author.Replace("ا", "إ");

                                string str8 = Author.Replace("ا", "إ");
                                string str9 = Author.Replace("ا", "أ");

                                string str10 = Author.Replace("إ", "أ");
                                string str11 = Author.Replace("إ", "ا");

                                string str12 = Author.Replace("إ", "أ");
                                string str13 = Author.Replace("إ", "ا");

                                string str14 = Author.Replace("ه", "ة");
                                string str15 = Author.Replace("ة", "ه");

                                string str16 = Author.Replace("ى", "ي");
                                string str17 = Author.Replace("ي", "ى");

                                string str18 = Author.Replace("ا", "ء");
                                string str19 = Author.Replace("ا", "آ");
                                string str20 = Author.Replace("ا", "ؤ");
                                string str21 = Author.Replace("ا", "ئ");
                                string str22 = Author.Replace("ا", "ا");

                                string str23 = Author.Replace("ي", "ي");

                                string str24 = Author.Replace("ة", "ة");
                                string str25 = Author.Replace("ة", "ه");

                                string str26 = Author.Replace("و", "و");
                                string str27 = Author.Replace("و", "ؤ");

                                string strSearchText = Author.ToLower();
                                predicate = predicate.And(p => (p.AuthorName != null && p.AuthorName.Contains(Author, StringComparison.OrdinalIgnoreCase))
                                || ((p.AuthorNameAr != null) && (p.AuthorNameAr.Contains(str1) || p.AuthorNameAr.Contains(str2) || p.AuthorNameAr.Contains(str3) || p.AuthorNameAr.Contains(str4) || p.AuthorNameAr.Contains(str5) || p.AuthorNameAr.Contains(str6) || p.AuthorNameAr.Contains(str7)
                                                   || p.AuthorNameAr.Contains(str8) || p.AuthorNameAr.Contains(str9) || p.AuthorNameAr.Contains(str10) || p.AuthorNameAr.Contains(str11)
                                                       || p.AuthorNameAr.Contains(str12) || p.AuthorNameAr.Contains(str13) || p.AuthorNameAr.Contains(str14) || p.AuthorNameAr.Contains(str15)
                                                         || p.AuthorNameAr.Contains(str16) || p.AuthorNameAr.Contains(str17)
                                                         || p.AuthorNameAr.Contains(str18) || p.AuthorNameAr.Contains(str19) || p.AuthorNameAr.Contains(str20) || p.AuthorNameAr.Contains(str21)
                                                          || p.AuthorNameAr.Contains(str22) || p.AuthorNameAr.Contains(str23) || p.AuthorNameAr.Contains(str24) || p.AuthorNameAr.Contains(str25)
                                                          || p.AuthorNameAr.Contains(str26) || p.AuthorNameAr.Contains(str27)))
                                                  );

                            }
                            // predicate = predicate.And(p => p.AuthorNameAr.Contains(model.Author));
                        }

                        if (!string.IsNullOrEmpty(model.Publisher))
                        {
                            var publisher = model.Publisher.ToLower();
                            if (!string.IsNullOrEmpty(publisher))
                            {
                                //string str = txtSearchKeyword.Text;
                                //string str1 = txtSearchKeyword.Text;
                                //RepresentativeAllList = RepresentativeAllList.Where(i => i.NameEn.Contains(str) || i.NameAr.Contains(str1)).ToList();
                                string str1 = publisher;
                                string str2 = publisher.Replace("أ", "إ");
                                string str3 = publisher.Replace("أ", "ا");
                                string str4 = publisher.Replace("إ", "أ");
                                string str5 = publisher.Replace("إ", "ا");
                                string str6 = publisher.Replace("ا", "أ");
                                string str7 = publisher.Replace("ا", "إ");

                                string str8 = publisher.Replace("ا", "إ");
                                string str9 = publisher.Replace("ا", "أ");

                                string str10 = publisher.Replace("إ", "أ");
                                string str11 = publisher.Replace("إ", "ا");

                                string str12 = publisher.Replace("إ", "أ");
                                string str13 = publisher.Replace("إ", "ا");

                                string str14 = publisher.Replace("ه", "ة");
                                string str15 = publisher.Replace("ة", "ه");

                                string str16 = publisher.Replace("ى", "ي");
                                string str17 = publisher.Replace("ي", "ى");

                                string str18 = publisher.Replace("ا", "ء");
                                string str19 = publisher.Replace("ا", "آ");
                                string str20 = publisher.Replace("ا", "ؤ");
                                string str21 = publisher.Replace("ا", "ئ");
                                string str22 = publisher.Replace("ا", "ا");

                                string str23 = publisher.Replace("ي", "ي");

                                string str24 = publisher.Replace("ة", "ة");
                                string str25 = publisher.Replace("ة", "ه");

                                string str26 = publisher.Replace("و", "و");
                                string str27 = publisher.Replace("و", "ؤ");

                                string strSearchText = publisher.ToLower();
                                predicate = predicate.And(p => (p.BookPublisherName != null && p.BookPublisherName.Contains(publisher, StringComparison.OrdinalIgnoreCase))
                                || ((p.BookPublisherNameAr != null) && (p.BookPublisherNameAr.Contains(str1) || p.BookPublisherNameAr.Contains(str2) || p.BookPublisherNameAr.Contains(str3) || p.BookPublisherNameAr.Contains(str4) || p.BookPublisherNameAr.Contains(str5) || p.BookPublisherNameAr.Contains(str6) || p.BookPublisherNameAr.Contains(str7)
                                                   || p.BookPublisherNameAr.Contains(str8) || p.BookPublisherNameAr.Contains(str9) || p.BookPublisherNameAr.Contains(str10) || p.BookPublisherNameAr.Contains(str11)
                                                       || p.BookPublisherNameAr.Contains(str12) || p.BookPublisherNameAr.Contains(str13) || p.BookPublisherNameAr.Contains(str14) || p.BookPublisherNameAr.Contains(str15)
                                                         || p.BookPublisherNameAr.Contains(str16) || p.BookPublisherNameAr.Contains(str17)
                                                         || p.BookPublisherNameAr.Contains(str18) || p.BookPublisherNameAr.Contains(str19) || p.BookPublisherNameAr.Contains(str20) || p.BookPublisherNameAr.Contains(str21)
                                                          || p.BookPublisherNameAr.Contains(str22) || p.BookPublisherNameAr.Contains(str23) || p.BookPublisherNameAr.Contains(str24) || p.BookPublisherNameAr.Contains(str25)
                                                          || p.BookPublisherNameAr.Contains(str26) || p.BookPublisherNameAr.Contains(str27)))
                                                  );

                            }

                            // predicate = predicate.And(p => p.BookPublisherNameAr.Contains(model.Publisher));
                            //HashSet<long> bookpublisherIds = new HashSet<long>(_context.XsiExhibitionBookPublisher.Where(p => p.TitleAr != null && p.TitleAr.Contains(title, StringComparison.OrdinalIgnoreCase))
                            //    .Select(p => p.ItemId));

                            //predicate = predicate.And(p => bookpublisherIds.Contains(p.BookPublisherId.Value));
                        }
                        #endregion
                    }

                    if (model.SubjectId != null && model.SubjectId != 0)
                    {
                        predicate = predicate.And(p => p.ExhibitionSubjectId == model.SubjectId);
                    }

                    if (model.SubSubjectId != null && model.SubSubjectId != 0)
                    {
                        predicate = predicate.And(p => p.ExhibitionSubsubjectId == model.SubSubjectId);
                    }

                    if (model.BookTypeId != null && model.BookTypeId != 0)
                    {
                        predicate = predicate.And(p => p.BookTypeId == model.BookTypeId);
                    }

                    if (model.CurencyId != null && model.CurencyId != 0)
                    {
                        predicate = predicate.And(p => p.ExhibitionCurrencyId == model.CurencyId);
                    }

                    if (!string.IsNullOrEmpty(model.ISBN))
                    {
                        predicate = predicate.And(p => p.Isbn == model.ISBN);
                    }

                    if (!string.IsNullOrEmpty(model.IssueYear))
                    {
                        predicate = predicate.And(p => p.IssueYear != null && p.IssueYear.Contains(model.IssueYear));
                    }

                    if (!string.IsNullOrEmpty(model.FromPrice))
                    {
                        var fromPrice = Convert.ToDouble(model.FromPrice);
                        predicate = predicate.And(p => Convert.ToDouble(p.Price) >= fromPrice);
                    }
                    if (!string.IsNullOrEmpty(model.ToPrice))
                    {
                        var toPrice = Convert.ToDouble(model.ToPrice);
                        predicate = predicate.And(p => Convert.ToDouble(p.Price) <= toPrice);
                    }
                    //if (model.ExhibitorId != null && model.ExhibitorId != 0)
                    //{
                    //    predicateBookParticipating = predicateBookParticipating.And(p => p.ExhibitorId == model.ExhibitorId);
                    //}
                    if (model.ExhibitorId != null && model.ExhibitorId != 0)
                    {
                        var agencyentity = cachedExhibitionMemberApplicationYearly.Where(i => i.ParentId != null && i.MemberExhibitionYearlyId == model.ExhibitorId).FirstOrDefault();
                        if (agencyentity != null)
                            predicateBookParticipating = predicateBookParticipating.And(p => p.AgencyId == agencyentity.MemberExhibitionYearlyId);
                        else
                            predicateBookParticipating = predicateBookParticipating.And(p => p.ExhibitorId == model.ExhibitorId);
                    }
                    predicateBookParticipating = predicateBookParticipating.And(i => i.IsActive == "Y");
                    predicate = predicate.And(i => i.IsActive == "Y" && i.IsEnable == "Y");
                    #endregion

                    string defaultarpath = _appCustomSettings.UploadsCMSPath + "/default-book_ar.jpg";
                    string thumbnailpath = _appCustomSettings.UploadsCMSPath + "/Books/Thumbnails/Small/";
                    var searchcount = (from b in cachedBooks.AsQueryable().Where(predicate)
                                       join bp in cachedBookParticipating.AsQueryable().Where(predicateBookParticipating) on b.BookId equals bp.BookId
                                       join y in cachedExhibitionMemberApplicationYearly on bp.ExhibitorId equals y
                                           .MemberExhibitionYearlyId
                                       join s in _context.XsiExhibitionBookSubject on b.ExhibitionSubjectId equals s.ItemId
                                       join ss in _context.XsiExhibitionBookSubsubject on b.ExhibitionSubsubjectId equals ss
                                           .ItemId
                                       //join p in _context.XsiExhibitionBookPublisher on b.BookPublisherId equals p.ItemId
                                       //join ba in cachedBookAuthors on b.BookId equals ba.BookId
                                       //join a in cachedAuthors on ba.AuthorId equals a.AuthorId
                                       where y.ExhibitionId == exhibition.ExhibitionId && (y.Status == "A" || y.Status == "I")
                                       select b.BookId).ToList().Count;

                    if (langid == 1)
                    {
                        var list = (from b in cachedBooks.AsQueryable().Where(predicate)
                                    join bp in cachedBookParticipating.AsQueryable().Where(predicateBookParticipating) on b.BookId equals bp.BookId
                                    join y in cachedExhibitionMemberApplicationYearly on bp.ExhibitorId equals y
                                        .MemberExhibitionYearlyId
                                    join s in _context.XsiExhibitionBookSubject on b.ExhibitionSubjectId equals s.ItemId
                                    join ss in _context.XsiExhibitionBookSubsubject on b.ExhibitionSubsubjectId equals ss
                                        .ItemId
                                    //join p in _context.XsiExhibitionBookPublisher on b.BookPublisherId equals p.ItemId
                                    //join ba in cachedBookAuthors on b.BookId equals ba.BookId
                                    //join a in cachedAuthors on ba.AuthorId equals a.AuthorId
                                    where y.ExhibitionId == exhibition.ExhibitionId && (y.Status == "A" || y.Status == "I")
                                    select new BookSearchDTO
                                    {
                                        BookId = bp.BookId,
                                        Thumbnail = string.IsNullOrEmpty(b.Thumbnail)
                                        ? defaultarpath : thumbnailpath + b.Thumbnail,
                                        Title = b.TitleEn ?? b.TitleAr,
                                        Exhibitor = y.PublisherNameEn,
                                        Price = b.Price,
                                        BookPublisher = b.BookPublisherName,
                                        IsBestSeller = b.IsBestSeller,
                                        ExhibitionSubject = s.Title,
                                        ExhibitionSubsubject = ss.Title,
                                        Isbn = b.Isbn,
                                        IssueYear = b.IssueYear,
                                        Author = b.AuthorName
                                    }).OrderBy(i => i.Title).Skip((model.pageNumber.Value - 1) * model.pageSize.Value).Take(model.pageSize.Value)
                       .ToList();
                        var dto = new { list, TotalCount = searchcount };
                        return Ok(dto);
                    }
                    else
                    {
                        var list = (from b in cachedBooks.AsQueryable().Where(predicate)
                                    join bp in cachedBookParticipating.AsQueryable().Where(predicateBookParticipating) on b.BookId equals bp.BookId
                                    join y in cachedExhibitionMemberApplicationYearly on bp.ExhibitorId equals y
                                        .MemberExhibitionYearlyId
                                    join s in _context.XsiExhibitionBookSubject on b.ExhibitionSubjectId equals s.ItemId
                                    join ss in _context.XsiExhibitionBookSubsubject on b.ExhibitionSubsubjectId equals ss
                                        .ItemId
                                    //join p in _context.XsiExhibitionBookPublisher on b.BookPublisherId equals p.ItemId
                                    //join ba in cachedBookAuthors on b.BookId equals ba.BookId
                                    //join a in cachedAuthors on ba.AuthorId equals a.AuthorId
                                    where y.ExhibitionId == exhibition.ExhibitionId && (y.Status == "A" || y.Status == "I")
                                    select new BookSearchDTO
                                    {
                                        BookId = bp.BookId,
                                        Thumbnail = string.IsNullOrEmpty(b.Thumbnail)
                                        ? defaultarpath : thumbnailpath + b.Thumbnail,
                                        Title = !string.IsNullOrEmpty(b.TitleAr) ? b.TitleAr : b.TitleEn,
                                        Exhibitor = y.PublisherNameAr,
                                        Price = b.Price,
                                        BookPublisher = b.BookPublisherNameAr,
                                        IsBestSeller = b.IsBestSeller,
                                        ExhibitionSubject = s.TitleAr,
                                        ExhibitionSubsubject = ss.TitleAr,
                                        Isbn = b.Isbn,
                                        IssueYear = b.IssueYear,
                                        Author = b.AuthorNameAr
                                    }).OrderBy(i => i.Title).Skip((model.pageNumber.Value - 1) * model.pageSize.Value).Take(model.pageSize.Value)
                       .ToList();
                        var dto = new { list, TotalCount = searchcount };
                        return Ok(dto);
                    }
                }
                //var list1 = new List<BookSearchDTO>();
                //var dto1 = new { list= list1, TotalCount = 0, Message = "No data found" };
                //return Ok(dto1);
                return Ok("No data found");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }
        [HttpPost]
        [Route("viewexportbooks")]
        public async Task<ActionResult<dynamic>> GetViewExportBooks(BooksSearchExportDTO model)
        {
            try
            {
                if (cachedExhibitionMemberApplicationYearly == null)
                    cachedExhibitionMemberApplicationYearly = CacheKeys.GetExhibitionMemberApplicationYearly(_cache);
                if (cachedBookParticipating == null)
                    cachedBookParticipating = CacheKeys.GetBooksParticipatingList(_cache);
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);

                List<XsiExhibitionBookForUi> bookslist = new List<XsiExhibitionBookForUi>();
                if (model.bookIds.Count > 0)
                {
                    var predicateBooks = PredicateBuilder.New<XsiExhibitionBookParticipating>();
                    predicateBooks = predicateBooks.And(p => p.IsActive == "Y");
                    predicateBooks = predicateBooks.And(p => model.bookIds.Contains(p.BookId));

                    var exhibition = MethodFactory.GetActiveExhibitionNew(model.WebsiteId, _cache);

                    // model.WebsiteId = 2;
                    if (langId == 1)
                    {
                        if (exhibition != null)
                        {
                            HashSet<long> exhibitorIds = new HashSet<long>(cachedExhibitionMemberApplicationYearly.AsQueryable().Where(p => (p.Status == "A" || p.Status == "I") && p.ExhibitionId == exhibition.ExhibitionId).Select(x => x.MemberExhibitionYearlyId));

                            var list = _context.XsiExhibitionBookParticipating.Where(x => exhibitorIds.Contains(x.ExhibitorId.Value) && model.bookIds.Contains(x.BookId)).Select(
                                b => new BookSearchDTO
                                {
                                    BookId = b.Book.BookId,
                                    Thumbnail = b.Book.Thumbnail,
                                    Title = b.Book.TitleEn,
                                    Exhibitor = b.Exhibitor.PublisherNameEn,
                                    Price = b.Price,
                                    BookPublisher = b.Book.BookPublisherName, //b.Book.BookPublisher.Title,
                                    IsBestSeller = b.Book.IsBestSeller,
                                    ExhibitionSubject = b.Book.ExhibitionSubject.Title,
                                    ExhibitionSubsubject = b.Book.ExhibitionSubsubject.Title,
                                    Isbn = b.Book.Isbn,
                                    BookType = GetBookType(b.BookId, langId),
                                    IssueYear = b.Book.IssueYear,
                                    HallNumber = GetHallNumber(b.ExhibitorId),
                                    StandNumber = GetStandNumber(b.Exhibitor)
                                }).ToList();
                            return Ok(list);
                        }
                    }
                    else
                    {
                        if (exhibition != null)
                        {
                            HashSet<long> exhibitorIds = new HashSet<long>(cachedExhibitionMemberApplicationYearly.AsQueryable().Where(p => (p.Status == "A" || p.Status == "I") && p.ExhibitionId == exhibition.ExhibitionId).Select(x => x.MemberExhibitionYearlyId));

                            var list = _context.XsiExhibitionBookParticipating.Where(x => exhibitorIds.Contains(x.ExhibitorId.Value) && model.bookIds.Contains(x.BookId)).Select(
                               b => new BookSearchDTO
                               {
                                   BookId = b.Book.BookId,
                                   Thumbnail = b.Book.Thumbnail,
                                   Title = b.Book.TitleAr,
                                   Exhibitor = b.Exhibitor.PublisherNameAr,
                                   Price = b.Price,
                                   BookPublisher = b.Book.BookPublisherNameAr, //b.Book.BookPublisher.TitleAr,
                                   IsBestSeller = b.Book.IsBestSeller,
                                   ExhibitionSubject = b.Book.ExhibitionSubject.TitleAr,
                                   ExhibitionSubsubject = b.Book.ExhibitionSubsubject.TitleAr,
                                   Isbn = b.Book.Isbn,
                                   BookType = GetBookType(b.BookId, langId),
                                   IssueYear = b.Book.IssueYear,
                                   HallNumber = GetHallNumber(b.ExhibitorId),
                                   StandNumber = GetStandNumber(b.Exhibitor)
                               }).ToList();
                            return Ok(list);
                        }
                    }
                }
                return Ok(bookslist);
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpPost]
        [Route("exportexcel")]
        public async Task<ActionResult<dynamic>> GetExportExcel(List<long> bookIds)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                if (bookIds.Count > 0)
                {
                    var downloadUrl = ExportBooksToExcel(bookIds, LangId, _context);
                    return Ok(downloadUrl);
                }
                return Ok("No data found");
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        #endregion
        #region Helper Methods
        private string GetHallNumber(long? exhibitorId)
        {
            if (cachedExhibitionExhibitorDetails == null)
                cachedExhibitionExhibitorDetails = CacheKeys.GetExhibitionExhibitorDetails(_cache);
            if (cachedExhibitionExhibitorDetails != null)
            {
                var details = cachedExhibitionExhibitorDetails.Where(i => i.MemberExhibitionYearlyId == exhibitorId).FirstOrDefault();
                if (details != null)
                {
                    return details.HallNumber;
                }
            }
            return "-";
        }
        private string GetStandNumber(XsiExhibitionMemberApplicationYearly exhibitor)
        {
            long exhibitorId = -1;
            if (exhibitor != null)
            {
                if (exhibitor.MemberRoleId == 1)
                    exhibitorId = exhibitor.MemberExhibitionYearlyId;
                else
                    exhibitorId = exhibitor.ParentId ?? -1;
            }
            if (cachedExhibitionExhibitorDetails == null)
                cachedExhibitionExhibitorDetails = CacheKeys.GetExhibitionExhibitorDetails(_cache);
            if (cachedExhibitionExhibitorDetails != null)
            {
                var details = cachedExhibitionExhibitorDetails.Where(i => i.MemberExhibitionYearlyId == exhibitorId).FirstOrDefault();
                if (details != null)
                {
                    return details.StandNumberStart + details.StandNumberEnd;
                }
            }
            return "-";
        }
        private string GetAuthorName(long bookId, long langId)
        {

            HashSet<long> authorIds = new HashSet<long>(_context.XsiExhibitionBookAuthor.Where(x => x.BookId == bookId).Select(x => x.AuthorId));
            List<string> authorNames = new List<string>();
            if (langId == 1)
                authorNames = _context.XsiExhibitionAuthor.Where(x => authorIds.Contains(x.AuthorId) && x.IsActive == "Y" && x.Status == "A").Select(x => x.Title).ToList();
            else
                authorNames = _context.XsiExhibitionAuthor.Where(x => authorIds.Contains(x.AuthorId) && x.IsActive == "Y" && x.Status == "A").Select(x => x.TitleAr).ToList();

            return String.Join(",", authorNames.ToArray());
        }
        private string GetBoothSection(long itemid, long langId)
        {
            var entity = _context.XsiExhibitionBooth.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
            if (entity != null)
                return langId == 1 ? entity.Title : entity.TitleAr;

            return string.Empty;
        }
        private string GetSubject(long itemid, long langId)
        {
            var entity = _context.XsiExhibitionBookSubject.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
            if (entity != null)
                return langId == 1 ? entity.Title : entity.TitleAr;

            return string.Empty;
        }
        private string GetSubSubject(long itemid, long langId)
        {
            var entity = _context.XsiExhibitionBookSubsubject.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
            if (entity != null)
                return langId == 1 ? entity.Title : entity.TitleAr;

            return string.Empty;
        }
        private string GetBoothSubSection(long itemid, long langId)
        {
            var entity = _context.XsiExhibitionBoothSubsection.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
            if (entity != null)
                return langId == 1 ? entity.Title : entity.TitleAr;

            return string.Empty;
        }
        private string GetBookType(long itemid, long langId)
        {
            var entity = _context.XsiExhibitionBookType.Where(x => x.ItemId == itemid && x.IsActive == "Y").FirstOrDefault();
            if (entity != null)
                return langId == 1 ? entity.Title : entity.TitleAr;

            return string.Empty;
        }

        private string ExportBooksToExcel(List<long> bookIds, long langId, sibfnewdbContext _context)
        {
            var predicate = PredicateBuilder.New<XsiExhibitionBooks>();
            predicate = predicate.And(i => bookIds.Contains(i.BookId));

            var list = _context.XsiExhibitionBookParticipating.Where(x => bookIds.Contains(x.BookId)).Select(
                       bp => new GetBooksToExport_Result
                       {
                           TitleAr = bp.Book.TitleAr,
                           TitleEn = bp.Book.TitleEn,
                           AuthorAr = bp.Book.AuthorNameAr,// GetAutorName(bp.BookId, 2),
                           AuthorEn = bp.Book.AuthorName,//GetAutorName(bp.BookId, 1),
                           BookDetails = bp.Book.BookDetails,
                           BookDetailsAr = bp.Book.BookDetailsAr,
                           Boothsection = GetBoothSectionByExhibitorId(bp.Exhibitor.MemberExhibitionYearlyId, langId),
                           Boothsubsection = GetBoothSubSectionByExhibitorId(bp.Exhibitor.MemberExhibitionYearlyId, langId),
                           ExhibitionLanguage = bp.Book.BookLanguage.Title,
                           ExhibitorAr = bp.Exhibitor.PublisherNameAr,
                           ExhibitorEn = bp.Exhibitor.PublisherNameEn,
                           hallnumber = bp.Exhibitor.XsiExhibitionExhibitorDetails.ToList()[0].HallNumber,
                           ISBN = bp.Book.Isbn,
                           IssueYear = bp.Book.IssueYear,
                           Price = bp.Book.Price,
                           PublisherAr = bp.Book.BookPublisherNameAr, //bp.Book.BookPublisher.TitleAr,
                           PublisherEn = bp.Book.BookPublisherName, //bp.Book.BookPublisher.Title,
                           Standnumberend = bp.Exhibitor.XsiExhibitionExhibitorDetails.ToList()[0].StandNumberEnd,
                           Standnumberstart = bp.Exhibitor.XsiExhibitionExhibitorDetails.ToList()[0].StandNumberStart,
                           Subject = bp.Book.ExhibitionSubject.Title,
                           Subsubject = bp.Book.ExhibitionSubsubject.Title,
                       }).ToList();


            string folder = _appCustomSettings.UploadsPhysicalPath + "\\BooksExport\\";
            string excelName = "BooksList" + Guid.NewGuid() + ".xlsx";
            //string downloadUrl = string.Format("{0}://{1}/{2}/{3}", Request.Scheme, Request.Host, "Content/Uploads/BooksExport", excelName);

            string downloadUrl = _appCustomSettings.UploadsCMSPath + "/BooksExport/" + excelName;
            FileInfo file = new FileInfo(Path.Combine(folder, excelName));
            if (!Directory.Exists(folder))
            {
                System.IO.Directory.CreateDirectory(folder);
            }
            if (file.Exists)
            {
                //file.Delete();
                file = new FileInfo(Path.Combine(folder, excelName));
            }
            using (var package = new ExcelPackage(file))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromCollection(list, true);
                package.Save();
            }
            return downloadUrl;
        }
        private string GetBoothSectionByExhibitorId(long exhibitorId, long langId)
        {
            string title = string.Empty;
            var boothSection = _context.XsiExhibitionExhibitorDetails
                .Where(x => x.MemberExhibitionYearlyId == exhibitorId).OrderByDescending(x => x.ExhibitorDetailsId)
                .FirstOrDefault().BoothSection;
            if (boothSection != null)
            {
                if (langId == 1)
                    title = boothSection.Title;
                else
                    title = boothSection.TitleAr;
            }
            return title;
        }
        private string GetBoothSubSectionByExhibitorId(long exhibitorId, long langId)
        {
            string title = string.Empty;
            var boothSection = _context.XsiExhibitionExhibitorDetails
                .Where(x => x.MemberExhibitionYearlyId == exhibitorId).OrderByDescending(x => x.ExhibitorDetailsId)
                .FirstOrDefault().BoothSection;
            if (boothSection != null)
            {
                if (langId == 1)
                    title = boothSection.Title;
                else
                    title = boothSection.TitleAr;
            }

            return title;
        }
        #endregion
    }

    public class BookSearchDTO
    {
        public long? BookId { get; set; }
        public string Thumbnail { get; set; }
        public string Title { get; set; }
        public string Exhibitor { get; set; }
        public string Price { get; set; }
        public string BookPublisher { get; set; }
        public string IsBestSeller { get; set; }
        // public string BookType { get; set; }
        // public string ExhibitionCurrency { get; set; }
        public string ExhibitionSubject { get; set; }
        public string ExhibitionSubsubject { get; set; }
        public string Isbn { get; set; }
        public string IssueYear { get; set; }
        public string Author { get; set; }
        public string BookType { get; set; }
        public string HallNumber { get; set; }
        public string StandNumber { get; set; }
    }
    public class BooksSearchExportDTO
    {
        public long WebsiteId { get; set; }
        public List<long> bookIds { get; set; }
    }
    public class BooksSearchModel
    {
        public string BookTitle { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public long? SubjectId { get; set; }
        public long? SubSubjectId { get; set; }
        public long? BookTypeId { get; set; }
        public long? ExhibitorId { get; set; }
        public long? CurencyId { get; set; }
        public string ISBN { get; set; }
        public string IssueYear { get; set; }
        public string FromPrice { get; set; }
        public string ToPrice { get; set; }
        public long WebSiteId { get; set; }
        public int? pageNumber { get; set; } = 1;
        public int? pageSize { get; set; } = 12;
    }
    public class BooksAutoSearch
    {
        public string BookTitle { get; set; }
        public long WebSiteId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionLanguageController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionLanguageController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/ExhibitionLanguage
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryDTO>>> GetXsiExhibitionLanguage()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            List<CategoryDTO> languageList = new List<CategoryDTO>();
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionLanguage>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                languageList = await _context.XsiExhibitionLanguage.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(x => new CategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title,
                }).ToListAsync();

                CategoryDTO categoryentity = languageList.Where(i => i.ItemId == 1).FirstOrDefault();
                languageList.Remove(categoryentity);

                categoryentity = languageList.Where(i => i.ItemId == 3).FirstOrDefault();
                languageList.Remove(categoryentity);

                languageList.Insert(0, new CategoryDTO() { ItemId = 3, Title = "English" });
                languageList.Insert(1, new CategoryDTO() { ItemId = 1, Title = "Arabic" });
                return languageList;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionLanguage>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                languageList = await _context.XsiExhibitionLanguage.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr).Select(x => new CategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.TitleAr,
                }).ToListAsync();

                CategoryDTO categoryentity = languageList.Where(i => i.ItemId == 1).FirstOrDefault();
                languageList.Remove(categoryentity);

                categoryentity = languageList.Where(i => i.ItemId == 3).FirstOrDefault();
                languageList.Remove(categoryentity);

                languageList.Insert(0, new CategoryDTO() { ItemId = 3, Title = "English" });
                languageList.Insert(1, new CategoryDTO() { ItemId = 1, Title = "Arabic" });
                return languageList;
            }
        }

        [HttpGet]
        [Route("MultiSelect")]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetXsiExhibitionLanguageMulti()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionLanguage>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionLanguage.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new
                {
                    value = x.ItemId,
                    label = x.Title,
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionLanguage>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionLanguage.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new
                {
                    value = x.ItemId,
                    label = x.TitleAr,
                }).ToListAsync();

                return await List;
            }
        }

        // GET: api/ExhibitionLanguage/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDTO>> GetXsiExhibitionLanguage(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            var xsiItem = await _context.XsiExhibitionLanguage.FindAsync(id);

            if (xsiItem == null)
            {
                return NotFound();
            }

            CategoryDTO itemDTO = new CategoryDTO()
            {
                ItemId = xsiItem.ItemId,
                Title = LangId == 1 ? xsiItem.Title : xsiItem.TitleAr,
            };
            return itemDTO;
        }

        private bool XsiExhibitionLanguageExists(long id)
        {
            return _context.XsiExhibitionLanguage.Any(e => e.ItemId == id);
        }
    }
}

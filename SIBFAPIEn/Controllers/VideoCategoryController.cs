﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class VideoCategoryController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public VideoCategoryController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/VideoCategory
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VideoCategoryDTO>>> GetXsiVideoCategory()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiVideoCategory>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiVideoCategory.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(x => new VideoCategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title,
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiVideoCategory>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiVideoCategory.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr).Select(x => new VideoCategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.TitleAr,
                }).ToListAsync();

                return await List;
            }
        }

        // GET: api/VideoCategory/5
        [HttpGet("{id}")]
        public async Task<ActionResult<VideoCategoryDTO>> GetXsiVideoCategory(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            var xsiItem = await _context.XsiVideoCategory.FindAsync(id);

            if (xsiItem == null)
            {
                return NotFound();
            }

            VideoCategoryDTO itemDTO = new VideoCategoryDTO()
            {
                ItemId = xsiItem.ItemId,
                Title = LangId == 1 ? xsiItem.Title : xsiItem.TitleAr,
            };
            return itemDTO;
        }

        private bool XsiVideoCategoryExists(long id)
        {
            return _context.XsiVideoCategory.Any(e => e.ItemId == id);
        }
    }
}

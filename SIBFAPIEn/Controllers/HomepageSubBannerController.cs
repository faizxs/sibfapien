﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class HomepageSubBannerController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public HomepageSubBannerController(IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        // GET: api/HomepageSubBanner
        [HttpGet]
        public ActionResult<List<HomepageSubBannerNewDTO>> GetXsiHomepageSubBanner()
        {
            List<HomepageSubBannerNewDTO> SubBannerList = new List<HomepageSubBannerNewDTO>();

            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var SubBannerEntity = _context.XsiHomepageSubBanner.Where(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.ItemId).FirstOrDefault();

                if (SubBannerEntity != null)
                {
                    if (!string.IsNullOrEmpty(SubBannerEntity.HomeBanner))
                    {
                        HomepageSubBannerNewDTO obj = new HomepageSubBannerNewDTO();
                        obj.Title = SubBannerEntity.Title;
                        obj.SubTitle = SubBannerEntity.SubTitle;
                        if (!string.IsNullOrEmpty(SubBannerEntity.HomeBanner))
                            obj.Image = _appCustomSettings.UploadsPath + "/HomepageSubBanner/" + SubBannerEntity.HomeBanner;

                        obj.Url = SubBannerEntity.Url;
                        obj.Color = SubBannerEntity.Color;
                        SubBannerList.Add(obj);
                    }

                    if (!string.IsNullOrEmpty(SubBannerEntity.HomeBanner2) && SubBannerEntity.IsActive2 == EnumConversion.ToString(EnumBool.Yes))
                    {
                        HomepageSubBannerNewDTO obj = new HomepageSubBannerNewDTO();
                        obj.Title = SubBannerEntity.Title2;
                        obj.SubTitle = SubBannerEntity.SubTitle2;
                        if (!string.IsNullOrEmpty(SubBannerEntity.HomeBanner2))
                            obj.Image = _appCustomSettings.UploadsPath + "/HomepageSubBanner/" + SubBannerEntity.HomeBanner2;

                        obj.Url = SubBannerEntity.Url2;
                        obj.Color = SubBannerEntity.Color2;
                        SubBannerList.Add(obj);
                    }
                }
                return Ok(SubBannerList);
            }
            else
            {
                var SubBannerEntity = _context.XsiHomepageSubBanner.Where(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.ItemId).FirstOrDefault();

                if (SubBannerEntity != null)
                {
                    if (!string.IsNullOrEmpty(SubBannerEntity.HomeBannerAr))
                    {
                        HomepageSubBannerNewDTO obj = new HomepageSubBannerNewDTO();
                        obj.Title = SubBannerEntity.TitleAr;
                        obj.SubTitle = SubBannerEntity.SubTitleAr;
                        if (!string.IsNullOrEmpty(SubBannerEntity.HomeBannerAr))
                            obj.Image = _appCustomSettings.UploadsPath + "/HomepageSubBanner/" + SubBannerEntity.HomeBannerAr;

                        obj.Url = SubBannerEntity.Urlar;
                        obj.Color = SubBannerEntity.ColorAr;
                        SubBannerList.Add(obj);
                    }

                    if (!string.IsNullOrEmpty(SubBannerEntity.HomeBannerAr2) && SubBannerEntity.IsActiveAr2 == EnumConversion.ToString(EnumBool.Yes))
                    {
                        HomepageSubBannerNewDTO obj = new HomepageSubBannerNewDTO();
                        obj.Title = SubBannerEntity.TitleAr2;
                        obj.SubTitle = SubBannerEntity.SubTitleAr2;
                        if (!string.IsNullOrEmpty(SubBannerEntity.HomeBannerAr2))
                            obj.Image = _appCustomSettings.UploadsPath + "/HomepageSubBanner/" + SubBannerEntity.HomeBannerAr2;

                        obj.Url = SubBannerEntity.Urlar2;
                        obj.Color = SubBannerEntity.ColorAr2;
                        SubBannerList.Add(obj);
                    }
                }
                return Ok(SubBannerList);
            }
        }
    }
}

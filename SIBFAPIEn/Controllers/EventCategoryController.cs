﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class EventCategoryController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public EventCategoryController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/EventCategory
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryDTO>>> GetXsiEventCategory()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var predicate = PredicateBuilder.True<XsiEventCategory>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);
                if (LangId == 1)
                {
                    var List = _context.XsiEventCategory.AsQueryable().Where(predicate).OrderBy(x => x.Title.Trim()).Select(x => new CategoryDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.Title,
                        Color = x.Color
                    }).ToListAsync();

                    return await List;
                }
                else
                {
                    var List = _context.XsiEventCategory.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr.Trim()).Select(x => new CategoryDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.TitleAr,
                        Color = x.ColorAr
                    }).ToListAsync();

                    return await List;
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }
        // GET: api/EventCategory
        [HttpGet]
        [Route("Filter/{webid}")]
        public async Task<ActionResult<IEnumerable<CategoryDTO>>> GetXsiEventCategoryFilter(long webid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var predicate = PredicateBuilder.True<XsiEventCategory>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.WebsiteId == 0 || i.WebsiteId == webid);
                if (LangId == 1)
                {
                    var List = _context.XsiEventCategory.AsQueryable().Where(predicate).OrderBy(x => x.Title.Trim()).Select(x => new CategoryDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.Title,
                        Color = x.Color
                    }).ToListAsync();

                    return await List;
                }
                else
                {
                    var List = _context.XsiEventCategory.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr.Trim()).Select(x => new CategoryDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.TitleAr,
                        Color = x.ColorAr
                    }).ToListAsync();

                    return await List;
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        // GET: api/EventCategory/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDTO>> GetXsiEventCategory(long id)
        {
            var xsiItem = await _context.XsiEventCategory.FindAsync(id);

            if (xsiItem == null)
            {
                return NotFound();
            }
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                CategoryDTO itemDTO = new CategoryDTO()
                {
                    ItemId = xsiItem.ItemId,
                    Title = xsiItem.Title,
                    Color = xsiItem.Color
                };
                return itemDTO;
            }
            else
            {
                CategoryDTO itemDTO = new CategoryDTO()
                {
                    ItemId = xsiItem.ItemId,
                    Title = xsiItem.TitleAr,
                    Color = xsiItem.ColorAr
                };
                return itemDTO;
            }
        }
        private bool XsiEventCategoryExists(long id)
        {
            return _context.XsiEventCategory.Any(e => e.ItemId == id);
        }
    }
}

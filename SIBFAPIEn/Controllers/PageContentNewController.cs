﻿using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using System.Linq;
using Xsi.ServicesLayer;
namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PageContentNewController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public PageContentNewController(IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        // GET: api/PageContentNew/5
        [HttpGet("{id}")]
        public ActionResult<ContentDTO> GetXsiPagesContentNew(long id)
        {
            MethodFactory.MainSubjectById(1, 1);
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            var contentDto = new ContentDTO();
            string baseurl = string.Empty;
            string pageUrl = string.Empty;
            if (LangId == 1)
            {
                baseurl = "https://www.sibf.com/en/";
                using (PageContentNewService PageContentNewService = new PageContentNewService())
                {
                    XsiPagesContentNew where = new XsiPagesContentNew();
                    where.ItemId = id;
                    where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    var entity = PageContentNewService.GetPageContentNew(where).FirstOrDefault();

                    if (entity == null)
                    {
                        return NotFound(contentDto);
                    }
                    contentDto.ItemId = entity.ItemId;
                    contentDto.ExplorerTitle = entity.ExplorerTitle;
                    //  ContentDataNew.HierarchicalGroupId = entity.HierarchicalGroupId;
                    contentDto.ExplorerTitle = entity.ExplorerTitle;
                    contentDto.PageMetaKeywords = entity.PageMetaKeywords;
                    contentDto.PageMetaDescription = entity.PageMetaDescription;
                    if (!string.IsNullOrEmpty(entity.HeaderImage))
                    {
                        //if (System.IO.File.Exists(BasePage.UploadsPhysicalPath + "/PageContentNew/Header/" + entity.HeaderImage))
                        //    ContentDataNew.HeaderImage = BasePage.Uploads + "/PageContentNew/Header/" + entity.HeaderImage;
                        //else
                        //    ContentDataNew.HeaderImage = string.Empty;
                        contentDto.InnerBanner = _appCustomSettings.UploadsPath + "/PageContentNew/Header/" + entity.HeaderImage;
                    }
                    else
                        contentDto.HeaderImage = string.Empty;
                    if (!string.IsNullOrEmpty(entity.InnerBanner))
                    {
                        //if (System.IO.File.Exists(BasePage.UploadsPhysicalPath + "/PageContentNew/Inner/" + entity.InnerBanner))
                        //    ContentDataNew.InnerBanner = BasePage.Uploads + "/PageContentNew/Inner/" + entity.InnerBanner;
                        //else
                        //    ContentDataNew.InnerBanner = string.Empty;
                        contentDto.InnerBanner = _appCustomSettings.UploadsPath + "/PageContentNew/Inner/" + entity.InnerBanner;
                    }
                    else
                        contentDto.InnerBanner = _appCustomSettings.UploadsPath + "/contentnew/content/img/bg/slide-0.jpg";
                    contentDto.PageTitle = entity.PageTitle;
                    contentDto.TitleGray = entity.TitleGray;
                    contentDto.PageSubTitle = entity.PageSubTitle;
                    contentDto.PageContent = entity.PageContent;
                    contentDto.PageSubContent = string.Empty; // entity.PageSubContent; //Its used for overview (cms) used for searching
                    contentDto.PageSubSubContent = entity.PageSubSubContent;
                    contentDto.StrDir = "ltr";

                    if (entity.PageTitle != null && !string.IsNullOrEmpty(entity.PageTitle))
                    {
                        pageUrl = entity.PageTitle
                            .Replace(" ", "-")
                            .Replace("&-", "")
                            .Replace(":-", "-")
                            .Replace("---", "-");
                        pageUrl = pageUrl.ToLower();
                    }

                    contentDto.CanonicalURL = baseurl + pageUrl;
                }
                return Ok(contentDto);
            }
            else
            {
                baseurl = "https://www.sibf.com/ar/";
                using (PageContentNewService PageContentNewService = new PageContentNewService())
                {
                    XsiPagesContentNew where = new XsiPagesContentNew();
                    where.ItemId = id;
                    where.IsActiveAr = EnumConversion.ToString(EnumBool.Yes);
                    var entity = PageContentNewService.GetPageContentNew(where).FirstOrDefault();

                    if (entity == null)
                    {
                        return NotFound(contentDto);
                    }
                    contentDto.ItemId = entity.ItemId;
                    contentDto.ExplorerTitle = entity.ExplorerTitleAr;
                    //  ContentDataNew.HierarchicalGroupId = entity.HierarchicalGroupId;
                    contentDto.ExplorerTitle = entity.ExplorerTitleAr;
                    contentDto.PageMetaKeywords = entity.PageMetaKeywordsAr;
                    contentDto.PageMetaDescription = entity.PageMetaDescriptionAr;
                    if (!string.IsNullOrEmpty(entity.HeaderImageAr))
                    {
                        //if (System.IO.File.Exists(BasePage.UploadsPhysicalPath + "/PageContentNew/Header/" + entity.HeaderImage))
                        //    ContentDataNew.HeaderImage = BasePage.Uploads + "/PageContentNew/Header/" + entity.HeaderImage;
                        //else
                        //    ContentDataNew.HeaderImage = string.Empty;
                        contentDto.InnerBanner = _appCustomSettings.UploadsPath + "/PageContentNew/Header/" + entity.HeaderImageAr;
                    }
                    else
                        contentDto.HeaderImage = string.Empty;
                    if (!string.IsNullOrEmpty(entity.InnerBannerAr))
                    {
                        //if (System.IO.File.Exists(BasePage.UploadsPhysicalPath + "/PageContentNew/Inner/" + entity.InnerBanner))
                        //    ContentDataNew.InnerBanner = BasePage.Uploads + "/PageContentNew/Inner/" + entity.InnerBanner;
                        //else
                        //    ContentDataNew.InnerBanner = string.Empty;
                        contentDto.InnerBanner = _appCustomSettings.UploadsPath + "/PageContentNew/Inner/" + entity.InnerBannerAr;
                    }
                    else
                        contentDto.InnerBanner = _appCustomSettings.UploadsPath + "/contentnew/content/img/bg/slide-0.jpg";
                    contentDto.PageTitle = entity.PageTitleAr;
                    contentDto.TitleGray = entity.TitleGrayAr;
                    contentDto.PageSubTitle = entity.PageSubTitleAr;
                    contentDto.PageContent = entity.PageContentAr;
                    contentDto.PageSubContent = string.Empty;// entity.PageSubContentAr; //Its used for overview (cms) used for searching
                    contentDto.PageSubSubContent = entity.PageSubSubContentAr;
                    contentDto.StrDir = "rtl";
                    pageUrl = entity.PageTitle
                            .Replace(" ", "-")
                            .Replace("&-", "")
                            .Replace(":-", "-")
                            .Replace("---", "-");
                    pageUrl = pageUrl.ToLower();

                    contentDto.CanonicalURL = baseurl + pageUrl;
                }
                return Ok(contentDto);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Xsi.ServicesLayer;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PCSearchController : ControllerBase
    {
        private IMemoryCache _cache;
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;

        public PCSearchController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IMemoryCache memoryCache)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _cache = memoryCache;
        }
        public List<XsiExhibitionMemberApplicationYearly> cachedExhibitionMemberApplicationYearly { get; set; }
        public List<XsiExhibitionExhibitorDetails> cachedExhibitionExhibitorDetails { get; set; }

        // GET: api/PCSearch
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public async Task<ActionResult<dynamic>> GetPCQRInfo(PCSearchModel model)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long langId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out langId);
                    List<PCQRUSerDTO> result = new List<PCQRUSerDTO>();
                    #region Current Exhibition
                    // model.WebSiteId = 2;
                    var exhibition = MethodFactory.GetActiveExhibition(model.WebSiteId, _context);
                    #endregion
                    if (exhibition != null)
                    {
                        var predicate = PredicateBuilder.New<XsiExhibitionProfessionalProgramRegistration>();
                        predicate = predicate.And(i => i.IsActive == "Y");
                        predicate = predicate.And(i => i.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved));
                        predicate = predicate.And(i => !string.IsNullOrEmpty(i.Qrcode));
                        predicate = predicate.And(i => i.ProfessionalProgramId == 38);

                        if (!string.IsNullOrEmpty(model.PublisherName))
                        {
                            var name = model.PublisherName.ToLower();
                            predicate = predicate.And(i => ((i.FirstName != null && i.FirstName.ToLower().Contains(name)) || (i.LastName != null && i.LastName.ToLower().Contains(name))));
                        }

                        if (!string.IsNullOrEmpty(model.CompanyName))
                        {
                            var cname = model.CompanyName.ToLower();
                            predicate = predicate.And(i => i.CompanyName != null && i.CompanyName.ToLower().Contains(cname));
                        }

                        result = _context.XsiExhibitionProfessionalProgramRegistration.Where(predicate).OrderBy(i => i.FirstName).Select(i => new PCQRUSerDTO
                        {
                            ItemId = i.ItemId,
                            MemberId = i.MemberId,
                            PublisherName = i.FirstName + " " + i.LastName,
                            CompanyName = i.CompanyName,
                            QRCodeURL = "https://xsi.sibf.com/content/uploads/PCTGQRCodes/" + i.Qrcode
                        }).Skip((model.pageNumber.Value - 1) * model.pageSize.Value).Take(model.pageSize.Value).ToList();

                        //  int totalCount = _context.XsiExhibitionMemberApplicationYearly.Where(predicate).Select(i => i.MemberExhibitionYearlyId).Count();
                        int totalCount = _context.XsiExhibitionProfessionalProgramRegistration.Where(predicate).Select(i => i.ItemId).Count();

                        result = result.ToList();
                        var dto = new { result, count = totalCount };
                        return Ok(dto);
                    }
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside PCSearch action: {ex.Message}");
                return Ok("Something went wrong. Please try again later.");
            }
        }



    }
    public class PCSearchModel
    {
        public string PublisherName { get; set; }
        public string CompanyName { get; set; }
        public long WebSiteId { get; set; }
        public int? pageNumber { get; set; } = 1;
        public int? pageSize { get; set; } = 16;
    }
    public class PCQRUSerDTO
    {
        public long ItemId { get; set; }
        public long? MemberId { get; set; }
        public string PublisherName { get; set; }
        public string CompanyName { get; set; }
        public string QRCodeURL { get; set; }
    }
}

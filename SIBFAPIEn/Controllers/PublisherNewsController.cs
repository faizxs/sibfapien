﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using SIBFAPIEn.DTO;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PublisherNewsController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        public PublisherNewsController(IOptions<AppCustomSettings> appCustomSettings)
        {
            this._appCustomSettings = appCustomSettings.Value;
        }

        // GET: api/PublisherNews
        [HttpGet]
        [Route("{pageNumber}/{pageSize}/{strSearch}")]
        public async Task<ActionResult<dynamic>> GetXsiPublisherNews(int? pageNumber = 1, int? pageSize = 15, string strSearch = "")
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiPublisherNews>();

                    predicate = predicate.And(i => i.WebsiteId != 2);
                    predicate = predicate.And(i => i.Dated != null);
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                        predicate = predicate.And(i => i.Dated.Value.Year.ToString() == strSearch);

                    var List = await _context.XsiPublisherNews.AsQueryable().Where(predicate)
                                         .OrderByDescending(o => o.Dated).ThenByDescending(o => o.ItemId).Select(i => new
                                         {
                                             ItemId = i.ItemId,
                                             Title = i.Title,
                                             Dated = GetDate(i.Dated, LangId),
                                             Detail = i.Detail,
                                             Overview = i.Overview,
                                             ImageName = _appCustomSettings.UploadsPath + "/PublisherNews/" + i.ImageName,
                                             FileName = i.FileName,
                                         }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    var count = _context.XsiPublisherNews.Where(predicate).Count();
                    var dto = new { List, TotalCount = count };
                    return dto;
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiPublisherNews>();

                    predicate = predicate.And(i => i.WebsiteId != 2);
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.DatedAr != null);
                    if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                        predicate = predicate.And(i => i.DatedAr.Value.Year.ToString() == strSearch);

                    var List = await _context.XsiPublisherNews.AsQueryable().Where(predicate)
                                         .OrderByDescending(o => o.DatedAr).ThenByDescending(o => o.ItemId).Select(i => new
                                         {
                                             ItemId = i.ItemId,
                                             Title = i.TitleAr,
                                             Dated = GetDate(i.DatedAr, LangId),// i.DatedAr == null ? string.Empty : i.DatedAr.Value.ToString("dd MMM"),
                                             Detail = i.DetailAr,
                                             Overview = i.OverviewAr,
                                             ImageName = _appCustomSettings.UploadsPath + "/PublisherNews/" + i.ImageNameAr,
                                             FileName = i.FileNameAr,
                                         }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    var count = _context.XsiPublisherNews.Where(predicate).Count();
                    var dto = new { List, TotalCount = count };
                    return dto;
                }
            }
        }

      

        // GET: api/PublisherNews/5
        [HttpGet("{id}")]
        public async Task<ActionResult<dynamic>> GetXsiPublisherNews(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var xsiPublisherNews = await _context.XsiPublisherNews.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                    if (xsiPublisherNews == null)
                    {
                        return NotFound();
                    }
                    var itemDTO = new
                    {
                        ItemId = xsiPublisherNews.ItemId,
                        Title = xsiPublisherNews.Title,
                        Dated = xsiPublisherNews.Dated == null ? string.Empty : xsiPublisherNews.Dated.Value.ToString("dd MMM"),
                        Overview = xsiPublisherNews.Overview,
                        Detail = xsiPublisherNews.Detail,
                        ImageName = string.IsNullOrWhiteSpace(xsiPublisherNews.ImageName) ? string.Empty : _appCustomSettings.UploadsPath + "/PublisherNews/" + xsiPublisherNews.ImageName,
                        FileName = xsiPublisherNews.FileName,
                        newsPhotoGalleries = _context.XsiPublisherNewsPhotoGallery.Where(i => i.NewsId == xsiPublisherNews.ItemId && i.IsActive == "Y").OrderBy(i => i.SortIndex).Select(p => new
                        {
                            ItemId = p.ItemId,
                            Title = p.Title,
                            ImageName = p.ImageName,
                            SortIndex = p.SortIndex
                        }).ToList()
                    };

                    var List = _context.XsiPublisherNewsPhotoGallery.Where(i => i.NewsId == id && i.IsActive == "Y").OrderBy(i => i.SortIndex).Select(p => new NewsDetailsPGDTO()
                    {
                        src = GetThumbnailFileName(p.ImageName),
                        linkUrl = GetLInkFileName(p.ImageName),
                        SortIndex = p.SortIndex
                    }).ToList();

                    if (List.Count() > 0)
                    {
                        List = List.Where(i => !string.IsNullOrEmpty(i.src) && !string.IsNullOrEmpty(i.linkUrl)).ToList();

                        List<string> strList = new List<string>();
                        string str = string.Empty;
                        foreach (var item in List)
                        {
                            strList.Add(item.linkUrl);
                        }
                        return Ok(new { PageTitle = itemDTO.Title, Title = "Publisher News", details = itemDTO, dataCollection = List, newsgallerylb = strList });
                    }
                    else
                    {
                        return Ok(new { PageTitle = itemDTO.Title, Title = "Publisher News", details = itemDTO, dataCollection = new List<NewsDetailsPGDTO>(), newsgallerylb = new List<string>() });
                    }
                }
                else
                {
                    var xsiPublisherNews = await _context.XsiPublisherNews.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                    if (xsiPublisherNews == null)
                    {
                        return NotFound();
                    }
                    var itemDTO = new
                    {
                        ItemId = xsiPublisherNews.ItemId,
                        Title = xsiPublisherNews.TitleAr,
                        Dated = xsiPublisherNews.DatedAr == null ? string.Empty : xsiPublisherNews.DatedAr.Value.ToString("dd MMM"),
                        Overview = xsiPublisherNews.OverviewAr,
                        Detail = xsiPublisherNews.DetailAr,
                        ImageName = string.IsNullOrWhiteSpace(xsiPublisherNews.ImageNameAr) ? string.Empty : _appCustomSettings.UploadsPath + "/PublisherNews/" + xsiPublisherNews.ImageNameAr,
                        FileName = xsiPublisherNews.FileNameAr,
                        newsPhotoGalleries = _context.XsiPublisherNewsPhotoGallery.Where(i => i.NewsId == xsiPublisherNews.ItemId && i.IsActive == "Y").OrderBy(i => i.SortIndex).Select(p => new
                        {
                            ItemId = p.ItemId,
                            Title = p.TitleAr,
                            ImageName = p.ImageName,
                            SortIndex = p.SortIndex
                        }).ToList()
                    };
                    var List = _context.XsiPublisherNewsPhotoGallery.Where(i => i.NewsId == id && i.IsActive == "Y").OrderBy(i => i.SortIndex).Select(p => new NewsDetailsPGDTO()
                    {
                        src = GetThumbnailFileName(p.ImageName),
                        linkUrl = GetLInkFileName(p.ImageName),
                        SortIndex = p.SortIndex
                    }).ToList();

                    if (List.Count() > 0)
                    {
                        List = List.Where(i => !string.IsNullOrEmpty(i.src) && !string.IsNullOrEmpty(i.linkUrl)).ToList();

                        List<string> strList = new List<string>();
                        string str = string.Empty;
                        foreach (var item in List)
                        {
                            strList.Add(item.linkUrl);
                        }
                        return Ok(new { PageTitle = itemDTO.Title, Title = "الأخبار", details = itemDTO, dataCollection = List, newsgallerylb = strList });
                    }
                    else
                    {
                        return Ok(new { PageTitle = itemDTO.Title, Title = "الأخبار", details = itemDTO, dataCollection = new List<NewsDetailsPGDTO>(), newsgallerylb = new List<string>() });
                    }
                }
            }
        }



        // GET: api/PublisherNews/GetYears
        [HttpGet("GetYears")]
        [ResponseCache(Duration = 60, Location = ResponseCacheLocation.Client)]
        public async Task<ActionResult<IEnumerable<YearDTO>>> GetYears()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiPublisherNews>();

                    predicate = predicate.And(i => i.Dated != null);
                    predicate = predicate.And(i => i.WebsiteId != 2);
                    predicate = predicate.And(i => i.IsActive == "Y");
                    var Year = _context.XsiPublisherNews.AsQueryable().Where(predicate)
                        .GroupBy(g => g.Dated.Value.Year)
                         .OrderByDescending(o => o.Key).Select(i => new YearDTO() { Id = i.Key, Year = i.Key })
                        .ToListAsync();

                    return await Year;
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiPublisherNews>();

                    predicate = predicate.And(i => i.DatedAr != null);
                    predicate = predicate.And(i => i.WebsiteId != 2);
                    predicate = predicate.And(i => i.IsActiveAr == "Y");
                    var Year = _context.XsiPublisherNews.AsQueryable().Where(predicate)
                        .GroupBy(g => g.DatedAr.Value.Year)
                         .OrderByDescending(o => o.Key).Select(i => new YearDTO() { Id = i.Key, Year = i.Key })
                        .ToListAsync();

                    return await Year;
                }
            }
        }

        private string GetThumbnailFileName(string fileName)
        {
            if (!string.IsNullOrEmpty(fileName) && System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\PublisherNewsPhotoGallery\\" + fileName))
                return _appCustomSettings.UploadsPath + "/PublisherNewsPhotoGallery/" + fileName;
            return string.Empty;
        }
        private string GetLInkFileName(string fileName)
        {
            if (!string.IsNullOrEmpty(fileName) && System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\PublisherNewsPhotoGallery\\" + fileName))
                return _appCustomSettings.UploadsPath + "/PublisherNewsPhotoGallery/" + fileName;
            return string.Empty;
        }
        private bool XsiPublisherNewsExists(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiPublisherNews.Any(e => e.ItemId == id);
            }
        }
        private string GetDate(DateTime? dt, long langId)
        {
            if (dt != null)
            {
                return (langId == 1)
                    ? dt.Value.ToString("dd MMM")
                    : dt.Value.ToString("dd MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
            }
            return string.Empty;
        }
    }
}

﻿using LinqKit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionCityController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionCityController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/ExhibitionCity/5
        [HttpGet("{countryid}")]
        public async Task<ActionResult<IEnumerable<SubCategoryDTO>>> GetXsiExhibitionCity(int countryid)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionCity>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.CountryId == countryid);

                var List = _context.XsiExhibitionCity.AsQueryable().Where(predicate).OrderBy(x => x.CityName).Select(i => new SubCategoryDTO()
                {
                    ItemId = i.CityId,
                    Title = i.CityName,
                    //CategoryTitle = i.Country.CountryName,
                    //CategoryId = i.Country.CountryId
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionCity>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.CountryId == countryid);

                var List = _context.XsiExhibitionCity.AsQueryable().Where(predicate).OrderBy(x => x.CityNameAr).Select(i => new SubCategoryDTO()
                {
                    ItemId = i.CityId,
                    Title = i.CityNameAr,
                    //CategoryTitle = i.Country.CountryName,
                    //CategoryId = i.Country.CountryId
                }).ToListAsync();

                return await List;
            }
        }

        // GET: api/ExhibitionCity/5/1
        [HttpGet("{countryid}/{id}")]
        public async Task<ActionResult<SubCategoryDTO>> GetXsiExhibitionCity(int countryid, long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var subCategory = await _context.XsiExhibitionCity.Where(i => i.CityId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes))
                  .Select(i => new SubCategoryDTO()
                  {
                      ItemId = i.CityId,
                      Title = i.CityName,
                      //CategoryTitle = i.Country.CountryName,
                      //CategoryId = i.Country.CountryId
                  }).FirstOrDefaultAsync();

                if (subCategory == null)
                {
                    return NotFound();
                }

                return subCategory;
            }
            else
            {
                var subCategory = await _context.XsiExhibitionCity.Where(i => i.CityId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes))
                   .Select(i => new SubCategoryDTO()
                   {
                       ItemId = i.CityId,
                       Title = i.CityNameAr,
                       //CategoryTitle = i.Country.CountryName,
                       //CategoryId = i.Country.CountryId
                   }).FirstOrDefaultAsync();

                if (subCategory == null)
                {
                    return NotFound();
                }

                return subCategory;
            }
        }

        private bool XsiExhibitionCityExists(long id)
        {
            return _context.XsiExhibitionCity.Any(e => e.CityId == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using System.IO;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class NewsController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public NewsController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _context = context;
        }

        // GET: api/News
        [HttpGet]
        [Route("{pageNumber}/{pageSize}/{strSearch}")]
        public async Task<ActionResult<dynamic>> GetXsiNews(int? pageNumber = 1, int? pageSize = 15, string strSearch = "")
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiNews>();

                    predicate = predicate.And(i => i.WebsiteId != 2);
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                        predicate = predicate.And(i => i.Dated != null && i.Dated.Value.Year.ToString() == strSearch);

                    var List = await _context.XsiNews.AsNoTracking().AsQueryable().Where(predicate)
                                         .OrderByDescending(o => o.Dated).ThenByDescending(o => o.ItemId).Select(i => new
                                         {
                                             ItemId = i.ItemId,
                                             Title = i.Title,
                                             Dated = GetDate(i.Dated, LangId),
                                             Detail = i.Detail,
                                             Overview = i.Overview,
                                             ImageName = string.IsNullOrWhiteSpace(i.ImageName) ? string.Empty : _appCustomSettings.UploadsPath + "/NewsNew/" + i.ImageName,
                                             FileName = i.FileName,
                                         }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();
                    var kount = _context.XsiNews.Where(predicate).Count();
                    var dto = new { List, TotalCount = kount };

                    return Ok(dto);
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiNews>();

                    predicate = predicate.And(i => i.WebsiteId != 2);
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                        predicate = predicate.And(i => i.DatedAr != null && i.DatedAr.Value.Year.ToString() == strSearch);

                    var List = await _context.XsiNews.AsNoTracking().AsQueryable().Where(predicate)
                                         .OrderByDescending(o => o.DatedAr).ThenByDescending(o => o.ItemId).Select(i => new
                                         {
                                             ItemId = i.ItemId,
                                             Title = i.TitleAr,
                                             Dated = GetDate(i.DatedAr, LangId),
                                             Detail = i.DetailAr,
                                             Overview = i.OverviewAr,
                                             ImageName = string.IsNullOrWhiteSpace(i.ImageNameAr) ? string.Empty : _appCustomSettings.UploadsPath + "/NewsNew/" + i.ImageNameAr,
                                             FileName = i.FileNameAr,
                                         }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();
                    var kount = _context.XsiNews.Where(predicate).Count();
                    var dto = new { List, TotalCount = kount };

                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiNews action: {ex.Message}");
                return Ok("Something went wrong. Please try again later.");

                //_logger.LogDebug("Here is debug message from our values controller.");
                //_logger.LogWarn("Here is warn message from our values controller.");
            }
        }

        // GET: api/HomepageNews
        [HttpGet]
        [Route("HomepageNews")]
        public async Task<ActionResult<dynamic>> GetXsiNewsForHomepage()
        {
            long id = Convert.ToInt64(EnumHomePageSection.SectionSix);
            bool isallowed = MethodFactory.iSHomepageSectionAllowed(id);
            if (isallowed)
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                bool IsGenerate = false;
                bool.TryParse(Request.Headers["IsGenerate"], out IsGenerate);
                string pathen = _appCustomSettings.UploadsPhysicalPath + "\\JsonData\\NewsHomePage.json";
                string pathar = _appCustomSettings.UploadsPhysicalPath + "\\JsonData\\NewsHomePageAr.json";

                if (LangId == 1)
                {
                    FileInfo fi = new FileInfo(pathen);
                    var hours = (fi.CreationTime - MethodFactory.ArabianTimeNow()).Hours;
                    if (IsGenerate || hours > 5 || !fi.Exists)
                    {
                        List<HomepageNewsDto> List = _context.XsiNews.AsNoTracking().Where(i => i.WebsiteId != 2 && i.IsActive == "Y").OrderByDescending(o => o.Dated).ThenByDescending(o => o.ItemId).Take(6).Select(i => new HomepageNewsDto()
                        {
                            ItemId = i.ItemId,
                            Title = i.Title,
                            Dated = GetDate(i.Dated, LangId),
                            ImageName = _appCustomSettings.UploadsPath + "/NewsNew/" + i.ImageName,
                        }).ToList();

                        System.IO.File.WriteAllText(pathen, JsonConvert.SerializeObject(List));

                        List<HomepageNewsDto> jsonfiledata = JsonConvert.DeserializeObject<List<HomepageNewsDto>>(System.IO.File.ReadAllText(pathen));
                        using (StreamReader file = System.IO.File.OpenText(pathen))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomepageNewsDto> jsonList = (List<HomepageNewsDto>)serializer.Deserialize(file, typeof(List<HomepageNewsDto>));
                            return jsonList;
                        }
                    }
                    else
                    {
                        List<HomepageNewsDto> jsonfiledata = JsonConvert.DeserializeObject<List<HomepageNewsDto>>(System.IO.File.ReadAllText(pathen));
                        using (StreamReader file = System.IO.File.OpenText(pathen))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomepageNewsDto> jsonList = (List<HomepageNewsDto>)serializer.Deserialize(file, typeof(List<HomepageNewsDto>));
                            return jsonList;
                        }
                    }
                }
                else
                {

                    FileInfo fi = new FileInfo(pathar);

                    var hours = (fi.CreationTime - MethodFactory.ArabianTimeNow()).Hours;
                    if (IsGenerate || hours > 5 || !fi.Exists)
                    {
                        var List = _context.XsiNews.AsNoTracking().Where(i => i.WebsiteId != 2 && i.IsActiveAr == "Y")
                           .OrderByDescending(o => o.DatedAr).ThenByDescending(o => o.ItemId).Take(6).Select(i => new
                           {
                               ItemId = i.ItemId,
                               Title = i.TitleAr,
                               Dated = GetDate(i.DatedAr, LangId),
                               ImageName = _appCustomSettings.UploadsPath + "/NewsNew/" + i.ImageNameAr,
                           });

                        System.IO.File.WriteAllText(pathar, JsonConvert.SerializeObject(List));

                        List<HomepageNewsDto> jsonfiledata = JsonConvert.DeserializeObject<List<HomepageNewsDto>>(System.IO.File.ReadAllText(pathar));
                        using (StreamReader file = System.IO.File.OpenText(pathar))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomepageNewsDto> jsonList = (List<HomepageNewsDto>)serializer.Deserialize(file, typeof(List<HomepageNewsDto>));
                            return jsonList;
                        }
                        //  return await List.ToListAsync();
                    }
                    else
                    {
                        List<HomepageNewsDto> jsonfiledata = JsonConvert.DeserializeObject<List<HomepageNewsDto>>(System.IO.File.ReadAllText(pathar));
                        using (StreamReader file = System.IO.File.OpenText(pathar))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomepageNewsDto> jsonList = (List<HomepageNewsDto>)serializer.Deserialize(file, typeof(List<HomepageNewsDto>));
                            return jsonList;
                        }
                    }
                }
            }
            return null;
        }
        // GET: api/News/5
        [HttpGet("{id}")]
        public async Task<ActionResult<dynamic>> GetXsiNews(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var xsiNews = await _context.XsiNews.AsNoTracking().Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                if (xsiNews == null)
                {
                    var itemDTO1 = new
                    {
                        ItemId = id,
                        Title = "No News Title",
                        Dated = "-",
                        Overview = "No news details Found",
                        Detail = "No news details Found",
                        ImageName = "",
                        FileName = ""
                    };
                    //  return NotFound();
                    return Ok(new { PageTitle = "-", Title = "Latest News", details = itemDTO1, dataCollection = new List<NewsDetailsPGDTO>(), newsgallerylb = new List<string>() });
                }
                var itemDTO = new
                {
                    ItemId = xsiNews.ItemId,
                    Title = xsiNews.Title,
                    Dated = GetDate(xsiNews.Dated, LangId),
                    Overview = xsiNews.Overview,
                    Detail = xsiNews.Detail,
                    ImageName = _appCustomSettings.UploadsPath + "/NewsNew/" + xsiNews.ImageName,
                    FileName = xsiNews.FileName
                };
                var List = _context.XsiNewsPhotoGallery.AsNoTracking().Where(i => i.NewsId == xsiNews.ItemId && i.IsActive == "Y").OrderBy(i => i.SortIndex).Select(p => new NewsDetailsPGDTO()
                {
                    src = GetThumbnailFileName(p.ImageName),
                    linkUrl = GetLInkFileName(p.ImageName),
                    SortIndex = p.SortIndex
                }).ToList();

                if (List.Count() > 0)
                {
                    List = List.Where(i => !string.IsNullOrEmpty(i.src) && !string.IsNullOrEmpty(i.linkUrl)).ToList();

                    List<string> strList = new List<string>();
                    string str = string.Empty;
                    foreach (var item in List)
                    {
                        strList.Add(item.linkUrl);
                    }
                    return Ok(new { PageTitle = itemDTO.Title, Title = "Latest News", details = itemDTO, dataCollection = List, newsgallerylb = strList });
                }
                else
                {
                    return Ok(new { PageTitle = itemDTO.Title, Title = "Latest News", details = itemDTO, dataCollection = new List<NewsDetailsPGDTO>(), newsgallerylb = new List<string>() });
                }
            }
            else
            {
                var xsiNews = await _context.XsiNews.AsNoTracking().Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                if (xsiNews == null)
                {
                    var itemDTO1 = new
                    {
                        ItemId = id,
                        Title = "No News Title",
                        Dated = "-",
                        Overview = "No news details Found",
                        Detail = "No news details Found",
                        ImageName = "",
                        FileName = ""
                    };
                    //  return NotFound();
                    return Ok(new { PageTitle = "-", Title = "Latest News", details = itemDTO1, dataCollection = new List<NewsDetailsPGDTO>(), newsgallerylb = new List<string>() });
                }
                var itemDTO = new
                {
                    ItemId = xsiNews.ItemId,
                    Title = xsiNews.TitleAr,
                    Dated = GetDate(xsiNews.DatedAr, LangId),
                    Overview = xsiNews.OverviewAr,
                    Detail = xsiNews.DetailAr,
                    ImageName = _appCustomSettings.UploadsPath + "/NewsNew/" + xsiNews.ImageNameAr,
                    FileName = xsiNews.FileNameAr
                };

                var List = _context.XsiNewsPhotoGallery.AsNoTracking().Where(i => i.NewsId == xsiNews.ItemId && i.IsActive == "Y").OrderBy(i => i.SortIndex).Select(p => new
                {
                    src = GetThumbnailFileName(p.ImageName),
                    linkUrl = GetLInkFileName(p.ImageName),
                    SortIndex = p.SortIndex
                }).ToList();

                if (List.Count() > 0)
                {
                    List = List.Where(i => !string.IsNullOrEmpty(i.src) && !string.IsNullOrEmpty(i.linkUrl)).ToList();

                    List<string> strList = new List<string>();
                    string str = string.Empty;
                    foreach (var item in List)
                    {
                        strList.Add(item.linkUrl);
                    }
                    return Ok(new { PageTitle = itemDTO.Title, Title = "Latest News", details = itemDTO, dataCollection = List, newsgallerylb = strList });
                }
                else
                {
                    return Ok(new { PageTitle = itemDTO.Title, Title = "Latest News", details = itemDTO, dataCollection = new List<NewsDetailsPGDTO>(), newsgallerylb = new List<string>() });
                }
            }
        }

        private string GetThumbnailFileName(string fileName)
        {
            if (!string.IsNullOrEmpty(fileName) && System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\NewsPhotoGallery\\" + fileName))
                return _appCustomSettings.UploadsPath + "/NewsPhotoGallery/" + fileName;
            return string.Empty;
        }
        private string GetLInkFileName(string fileName)
        {
            if (!string.IsNullOrEmpty(fileName) && System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\NewsPhotoGallery\\" + fileName))
                return _appCustomSettings.UploadsPath + "/NewsPhotoGallery/" + fileName;
            return string.Empty;
        }

        // GET: api/News/GetYears
        [HttpGet("GetYears")]
        [ResponseCache(Duration = 60, Location = ResponseCacheLocation.Client)]
        public async Task<ActionResult<IEnumerable<YearDTO>>> GetYears()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiNews>();

                predicate = predicate.And(i => i.Dated != null);
                predicate = predicate.And(i => i.WebsiteId != 2);
                predicate = predicate.And(i => i.IsActive == "Y");
                var Year = _context.XsiNews.AsNoTracking().AsQueryable().Where(predicate)
                    .GroupBy(g => g.Dated.Value.Year)
                     .OrderByDescending(o => o.Key).Select(i => new YearDTO() { Id = i.Key, Year = i.Key })
                    .ToListAsync();

                return await Year;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiNews>();

                predicate = predicate.And(i => i.DatedAr != null);
                predicate = predicate.And(i => i.WebsiteId != 2);
                predicate = predicate.And(i => i.IsActiveAr == "Y");
                var Year = _context.XsiNews.AsNoTracking().AsQueryable().Where(predicate)
                    .GroupBy(g => g.DatedAr.Value.Year)
                     .OrderByDescending(o => o.Key).Select(i => new YearDTO() { Id = i.Key, Year = i.Key })
                    .ToListAsync();

                return await Year;
            }
        }

        private bool XsiNewsExists(long id)
        {
            return _context.XsiNews.Any(e => e.ItemId == id);
        }
        private string GetDate(DateTime? dt, long langId)
        {
            if (dt != null)
            {
                return (langId == 1)
                    ? dt.Value.ToString("dd MMM")
                    : dt.Value.ToString("dd MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
            }
            return string.Empty;
        }
    }
}

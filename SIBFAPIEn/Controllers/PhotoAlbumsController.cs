﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PhotoAlbumsController : ControllerBase
    {
        string strthumbnail = "book-image.jpg";
        private readonly AppCustomSettings _appCustomSettings;
        public PhotoAlbumsController(IOptions<AppCustomSettings> appCustomSettings)
        {
            this._appCustomSettings = appCustomSettings.Value;
        }
        // GET: api/PhotoAlbum
        [HttpGet]
        [Route("{pageNumber}/{pageSize}/{categoryId}/{subcategoryId}/{strSearch}")]
        public async Task<ActionResult<dynamic>> GetXsiPhotoAlbum(int? pageNumber = 1, int? pageSize = 15, long? categoryId = -1, long? subcategoryId = -1, string strSearchYear = "")
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string strpath = _appCustomSettings.UploadsPath + "/PhotoGallery/";
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiPhotoAlbum>();

                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    if (!string.IsNullOrEmpty(strSearchYear) && strSearchYear != "-1")
                        predicate = predicate.And(i => i.Dated != null && i.Dated.Value.Year.ToString() == strSearchYear);

                    if (categoryId != -1 && subcategoryId != -1)
                    {
                        List<long> subcatIds = _context.XsiPhotoSubCategory.Where(i => i.CategoryId == categoryId && i.ItemId == subcategoryId).Select(i => i.ItemId).ToList();
                        predicate = predicate.And(i => subcatIds.Contains(i.SubCategoryId.Value) && i.SubCategoryId == subcategoryId);
                    }
                    else if (categoryId != -1)
                    {
                        List<long> subcatIds = _context.XsiPhotoSubCategory.Where(i => i.CategoryId == categoryId).Select(i => i.ItemId).ToList();
                        predicate = predicate.And(i => subcatIds.Contains(i.SubCategoryId.Value));
                    }
                    else if (subcategoryId != -1)
                        predicate = predicate.And(i => i.SubCategoryId == subcategoryId);


                    var kount = await _context.XsiPhotoAlbum.AsQueryable().Where(predicate).CountAsync();

                    var List = await _context.XsiPhotoAlbum.AsQueryable().Where(predicate).OrderByDescending(o => o.Dated).ThenByDescending(o => o.ItemId).Select(i => new AlbumDTO()
                    {
                        ItemId = i.ItemId,
                        PhotoCategoryId = i.SubCategory.CategoryId.Value,
                        CategoryTitle = i.SubCategory.Category.Title,
                        SubCategoryId = i.SubCategoryId.Value,
                        SubCategoryTitle = i.SubCategory.Title,
                        Title = i.Title,
                        Description = i.Description.Length > 140 ? i.Description.Substring(0, 140) + "..." : i.Description,
                        StrDated = i.Dated == null ? string.Empty : i.Dated.Value.ToString("dd MMM yyyy"),
                        Year = i.Dated == null ? string.Empty : i.Dated.Value.Year.ToString(),
                        SortIndex = i.SortIndex,
                        Count = _context.XsiPhoto.Where(p => p.PhotoAlbumId == i.ItemId && p.IsActive == "Y").Count(),
                        //Thumbnail = _appCustomSettings.UploadsPath + "/PhotoGallery/AlbumThumbnailNew/" + GetThumbnail(i.ItemId),
                        CreatedOn = i.CreatedOn
                    }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    foreach (var album in List)
                    {
                        string str = GetThumbnail(album.ItemId, false);
                        album.Thumbnail = strpath + "AlbumThumbnailNew/" + str;
                        album.MiniThumbnail = strpath + "/MiniThumbnail/" + str;
                        album.LargeThumbnail = strpath + "/AlbumBanner/" + str;
                    }

                    AlbumPageDTO albumPageDTO = new AlbumPageDTO();
                    albumPageDTO.AlbumDTO = List;
                    albumPageDTO.TopAlbumItemDTO = List.FirstOrDefault();

                    var dto = new { albumPageDTO, TotalCount = kount };
                    return Ok(dto);
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiPhotoAlbum>();

                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    if (!string.IsNullOrEmpty(strSearchYear) && strSearchYear != "-1")
                        predicate = predicate.And(i => i.DatedAr != null && i.DatedAr.Value.Year.ToString() == strSearchYear);

                    if (categoryId != -1 && subcategoryId != -1)
                    {
                        List<long> subcatIds = _context.XsiPhotoSubCategory.Where(i => i.CategoryId == categoryId && i.ItemId == subcategoryId).Select(i => i.ItemId).ToList();
                        predicate = predicate.And(i => subcatIds.Contains(i.SubCategoryId.Value) && i.SubCategoryId == subcategoryId);
                    }
                    else if (categoryId != -1)
                    {
                        List<long> subcatIds = _context.XsiPhotoSubCategory.Where(i => i.CategoryId == categoryId).Select(i => i.ItemId).ToList();
                        predicate = predicate.And(i => subcatIds.Contains(i.SubCategoryId.Value));
                    }
                    else if (subcategoryId != -1)
                        predicate = predicate.And(i => i.SubCategoryId == subcategoryId);


                    var kount = await _context.XsiPhotoAlbum.AsQueryable().Where(predicate).CountAsync();

                    var List = await _context.XsiPhotoAlbum.AsQueryable().Where(predicate).OrderByDescending(o => o.Dated).ThenByDescending(o => o.ItemId).Select(i => new AlbumDTO()
                    {
                        ItemId = i.ItemId,
                        PhotoCategoryId = i.SubCategory.CategoryId.Value,
                        CategoryTitle = i.SubCategory.Category.TitleAr,
                        SubCategoryId = i.SubCategoryId.Value,
                        SubCategoryTitle = i.SubCategory.TitleAr,
                        Title = i.TitleAr,
                        Description = i.DescriptionAr.Length > 140 ? i.DescriptionAr.Substring(0, 140) + "..." : i.DescriptionAr,
                        StrDated = i.DatedAr == null ? string.Empty : i.DatedAr.Value.ToString("dd MMM yyyy"),
                        Year = i.DatedAr == null ? string.Empty : i.DatedAr.Value.Year.ToString(),
                        SortIndex = i.SortIndex,
                        Count = _context.XsiPhoto.Where(p => p.PhotoAlbumId == i.ItemId && p.IsActiveAr == "Y").Count(),
                        //Thumbnail = _appCustomSettings.UploadsPath + "/PhotoGallery/AlbumThumbnailNew/" + GetThumbnail(i.ItemId),
                        CreatedOn = i.CreatedOn
                    }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    foreach (var album in List)
                    {
                        string str = GetThumbnailAr(album.ItemId, false);
                        album.Thumbnail = strpath + "AlbumThumbnailNew/" + str;
                        album.MiniThumbnail = strpath + "/MiniThumbnail/" + str;
                        album.LargeThumbnail = strpath + "/AlbumBanner/" + str;
                    }

                    AlbumPageDTO albumPageDTO = new AlbumPageDTO();
                    albumPageDTO.AlbumDTO = List;
                    albumPageDTO.TopAlbumItemDTO = List.FirstOrDefault();

                    var dto = new { albumPageDTO, TotalCount = kount };
                    return Ok(dto);
                }
            }
        }

        // GET: api/PhotoAlbum/5
        [HttpGet("{id}")]
        public ActionResult<AlbumDTO> GetXsiPhotoAlbum(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string strpath = _appCustomSettings.UploadsPath + "/PhotoGallery/";
                AlbumDTO album = new AlbumDTO();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    album = _context.XsiPhotoAlbum.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes))
                     .Select(i => new AlbumDTO()
                     {
                         ItemId = i.ItemId,
                         PhotoCategoryId = i.SubCategory.CategoryId.Value,
                         CategoryTitle = i.SubCategory.Category.Title,
                         SubCategoryId = i.SubCategoryId.Value,
                         SubCategoryTitle = i.SubCategory.Title,
                         Title = i.Title,
                         Description = i.Description,
                         StrDated = i.Dated == null ? string.Empty : i.Dated.Value.ToString("dd MMM yyyy"),
                         Year = i.Dated == null ? string.Empty : i.Dated.Value.Year.ToString(),
                         SortIndex = i.SortIndex,
                         Count = _context.XsiPhoto.Where(p => p.PhotoAlbumId == p.ItemId && p.IsActive == "Y").Count(),
                         CreatedOn = i.CreatedOn
                     }).FirstOrDefault();
                    if (album == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        string str = GetThumbnail(album.ItemId, false);
                        album.Thumbnail = strpath + "AlbumThumbnailNew/" + str;
                        album.MiniThumbnail = strpath + "/MiniThumbnail/" + str;
                        album.LargeThumbnail = strpath + "/AlbumBanner/" + str;
                    }
                    return album;
                }
                else
                {
                    album = _context.XsiPhotoAlbum.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes))
                     .Select(i => new AlbumDTO()
                     {
                         ItemId = i.ItemId,
                         PhotoCategoryId = i.SubCategory.CategoryId.Value,
                         CategoryTitle = i.SubCategory.Category.TitleAr,
                         SubCategoryId = i.SubCategoryId.Value,
                         SubCategoryTitle = i.SubCategory.TitleAr,
                         Title = i.TitleAr,
                         Description = i.DescriptionAr,
                         StrDated = i.DatedAr == null ? string.Empty : i.DatedAr.Value.ToString("dd MMM yyyy"),
                         Year = i.DatedAr == null ? string.Empty : i.DatedAr.Value.Year.ToString(),
                         SortIndex = i.SortIndex,
                         Count = _context.XsiPhoto.Where(p => p.PhotoAlbumId == p.ItemId && p.IsActiveAr == "Y").Count(),
                         CreatedOn = i.CreatedOn
                     }).FirstOrDefault();
                    if (album == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        string str = GetThumbnailAr(album.ItemId, false);
                        album.Thumbnail = strpath + "AlbumThumbnailNew/" + str;
                        album.MiniThumbnail = strpath + "/MiniThumbnail/" + str;
                        album.LargeThumbnail = strpath + "/AlbumBanner/" + str;
                    }
                    return album;
                }
            }
        }


        // GET: api/PhotoAlbum/GetYears
        [HttpGet("GetYears")]
        public async Task<ActionResult<IEnumerable<YearDTO>>> GetYears()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiPhotoAlbum>();

                    predicate = predicate.And(i => i.Dated != null);
                    predicate = predicate.And(i => i.IsActive == "Y");
                    var Year = _context.XsiPhotoAlbum.AsQueryable().Where(predicate)
                        .GroupBy(g => g.Dated.Value.Year)
                         .OrderByDescending(o => o.Key).Select(i => new YearDTO() { Id = i.Key, Year = i.Key })
                        .ToListAsync();

                    return await Year;
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiPhotoAlbum>();

                    predicate = predicate.And(i => i.Dated != null);
                    predicate = predicate.And(i => i.IsActiveAr == "Y");
                    var Year = _context.XsiPhotoAlbum.AsQueryable().Where(predicate)
                        .GroupBy(g => g.DatedAr.Value.Year)
                         .OrderByDescending(o => o.Key).Select(i => new YearDTO() { Id = i.Key, Year = i.Key })
                        .ToListAsync();

                    return await Year;
                }
            }
        }


        string GetThumbnail(long albumId, bool isSimilarId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string strimage = "book-image.jpg";
                if (isSimilarId)
                {
                    return strthumbnail;
                }
                else
                {
                    strimage = "book-image.jpg";

                    var entity = _context.XsiPhoto.Where(p => p.Thumbnail != null && p.IsAlbumCover == "Y" && p.IsActive == "Y" && p.PhotoAlbumId == albumId).OrderByDescending(i => i.CreatedOn).FirstOrDefault();
                    if (entity != null && entity.Thumbnail != null && !string.IsNullOrEmpty(entity.Thumbnail))
                        strimage = entity.Thumbnail;
                    else
                    {
                        entity = _context.XsiPhoto.Where(p => p.Thumbnail != null && p.IsActive == "Y" && p.PhotoAlbumId == albumId).OrderByDescending(i => i.CreatedOn).FirstOrDefault();
                        if (entity != null && entity.Thumbnail != null && !string.IsNullOrEmpty(entity.Thumbnail))
                            strimage = entity.Thumbnail;
                    }
                    strthumbnail = strimage;
                }
                return strimage;
            }
        }
        string GetThumbnailAr(long albumId, bool isSimilarId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string strimage = "book-image.jpg";
                if (isSimilarId)
                {
                    return strthumbnail;
                }
                else
                {
                    strimage = "book-image.jpg";

                    var entity = _context.XsiPhoto.Where(p => p.ThumbnailAr != null && p.IsAlbumCoverAr == "Y" && p.IsActiveAr == "Y" && p.PhotoAlbumId == albumId).OrderByDescending(i => i.CreatedOn).FirstOrDefault();
                    if (entity != null && entity.ThumbnailAr != null && !string.IsNullOrEmpty(entity.ThumbnailAr))
                        strimage = entity.ThumbnailAr;
                    else
                    {
                        entity = _context.XsiPhoto.Where(p => p.ThumbnailAr != null && p.IsActiveAr == "Y" && p.PhotoAlbumId == albumId).OrderByDescending(i => i.CreatedOn).FirstOrDefault();
                        if (entity != null && entity.ThumbnailAr != null && !string.IsNullOrEmpty(entity.ThumbnailAr))
                            strimage = entity.ThumbnailAr;
                    }
                    strthumbnail = strimage;
                }
                return strimage;
            }
        }
    }
}

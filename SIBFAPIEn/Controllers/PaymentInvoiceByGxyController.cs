﻿using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SIBFAPIEn.DTO;
using System;
using System.Linq;
using Xsi.ServicesLayer;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PaymentInvoiceByGxyController : ControllerBase
    {
        public PaymentInvoiceByGxyController()
        {
        }

        [HttpPost]
        [Route("UpdateStatus")]
        public ActionResult<dynamic> PostUpdateStatus(PaymentStatusDTO itemDto)
        {
            MessageGxyDTO dto = new MessageGxyDTO();
            dto.MessageTypeResponse = "0";
            try
            {
                if (itemDto.GalaxyId == 1)
                {
                    if (!string.IsNullOrEmpty(itemDto.Status))
                    {
                        using (sibfnewdbContext _context = new sibfnewdbContext())
                        {
                            var ExhibitionMemberApplicationYearly = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == itemDto.ApplicationId && i.FileNumber == itemDto.FileNumber && (i.MemberRoleId == 1 || i.MemberRoleId == 8)).FirstOrDefault();
                            if (ExhibitionMemberApplicationYearly != null)
                            {
                                XsiInvoice invoiceEntity = _context.XsiInvoice.Where(x => x.ItemId == itemDto.InvoiceId && x.ExhibitorId == ExhibitionMemberApplicationYearly.MemberExhibitionYearlyId && x.InvoiceNumber == itemDto.Invoicenumber).OrderByDescending(i => i.ItemId).FirstOrDefault();
                                if (invoiceEntity != null)
                                {
                                    invoiceEntity.Status = itemDto.Status;
                                    invoiceEntity.Notes = itemDto.Notes; //Notes for admin for reject status
                                    invoiceEntity.ModifiedOn = MethodFactory.ArabianTimeNow();
                                    _context.SaveChanges();

                                    XsiInvoiceStatusLog logentity = new XsiInvoiceStatusLog();
                                    logentity.InvoiceId = invoiceEntity.ItemId;
                                    logentity.GalaxyTeamId = 1;
                                    logentity.Status = itemDto.Status;
                                    logentity.CreatedOn = MethodFactory.ArabianTimeNow();
                                    _context.XsiInvoiceStatusLog.Add(logentity);
                                    _context.SaveChanges();

                                    dto.MessageTypeResponse = "1";
                                    dto.Message = "Status updated successfully";
                                    return Ok(dto);
                                }
                                else
                                {
                                    dto.Message = "Invoice Not Found";
                                    return Ok(dto);
                                }
                            }
                            else
                            {
                                dto.Message = "No Application found with this file number";
                                return Ok(dto);
                            }
                        }
                    }
                    else
                    {
                        dto.Message = "Status cannot be empty";
                        return Ok(dto);
                    }
                }
                else
                {
                    dto.Message = "Unauthorised access.";
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                dto.Message = "Something went wrong. Please try again later. ";
                return Ok(dto);
            }
        }
    }
}

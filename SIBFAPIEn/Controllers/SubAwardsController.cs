﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class SubAwardsController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public SubAwardsController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        // GET: api/SubAwards
        [HttpGet("{awardid}")]
        public async Task<ActionResult<dynamic>> GetXsiSubAwards(long? awardid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    long websiteId = Convert.ToInt32(EnumWebsiteTypeId.SIBF);
                    var predicate = PredicateBuilder.True<XsiSubAwards>();

                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.AwardId == awardid);

                    var List = _context.XsiSubAwards.AsQueryable().Where(predicate).OrderBy(x => x.Name).Select(i => new DropdownDataDTO()
                    {
                        ItemId = i.ItemId,
                        Title = i.Name
                    }).ToListAsync();

                    var dto = await List;

                    return Ok(dto);
                }
                else
                {
                    long websiteId = Convert.ToInt32(EnumWebsiteTypeId.SIBF);
                    var predicate = PredicateBuilder.True<XsiSubAwards>();

                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.AwardId == awardid);

                    var List = _context.XsiSubAwards.AsQueryable().Where(predicate).OrderBy(x => x.NameAr).Select(i => new DropdownDataDTO()
                    {
                        ItemId = i.ItemId,
                        Title = i.NameAr
                    }).ToListAsync();

                    var dto = await List;

                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiSubAwards action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        // GET: api/SubAwards/5
        [HttpGet]
        [Route("details/{id}")]
        public async Task<ActionResult<dynamic>> GetXsiSubAwards(long id)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiSubAwards>();
                    predicate = predicate.And(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    var dto = await _context.XsiSubAwards.AsQueryable().Where(predicate)
                        .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new
                        {
                            ItemId = i.ItemId,
                            Name = i.Name,
                        }).FirstOrDefaultAsync();

                    if (dto == null)
                    {
                        return NotFound();
                    }

                    return Ok(dto);
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiSubAwards>();
                    predicate = predicate.And(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    var dto = await _context.XsiSubAwards.AsQueryable().Where(predicate)
                        .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new
                        {
                            ItemId = i.ItemId,
                            Name = i.NameAr,
                        }).FirstOrDefaultAsync();

                    if (dto == null)
                    {
                        return NotFound();
                    }

                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiSubAwards action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
    }
}

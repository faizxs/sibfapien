﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class HomepageSectionFourController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public HomepageSectionFourController(IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        // GET: api/HomepageSectionFour
        [HttpGet]
        public async Task<ActionResult<IEnumerable<HomepageSectionFourDTO>>> GetXsiHomepageSectionFour()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var List = _context.XsiHomepageSectionFour.Where(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.ItemId).Select(i => new HomepageSectionFourDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.Title,
                    ViewAllUrl = i.ViewAllUrl,
                    SubSectionOneTitle = i.SubSectionOneTitle,
                    SubSectionOneOverview = i.SubSectionOneOverview,
                    SubSectionOneImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + i.SubSectionOneImage,
                    SubSectionOneUrl = i.SubSectionOneUrl,
                    SubSectionTwoTitle = i.SubSectionTwoTitle,
                    SubSectionTwoOverview = i.SubSectionTwoOverview,
                    SubSectionTwoImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + i.SubSectionTwoImage,
                    SubSectionTwoUrl = i.SubSectionTwoUrl,
                    SubSectionThreeTitle = i.SubSectionThreeTitle,
                    SubSectionThreeOverview = i.SubSectionThreeOverview,
                    SubSectionThreeImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + i.SubSectionThreeImage,
                    SubSectionThreeUrl = i.SubSectionThreeUrl
                });
                return await List.ToListAsync();
            }
            else
            {
                var List = _context.XsiHomepageSectionFour.Where(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.ItemId).Select(i => new HomepageSectionFourDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.TitleAr,
                    ViewAllUrl = i.ViewAllUrlar,
                    SubSectionOneTitle = i.SubSectionOneTitleAr,
                    SubSectionOneOverview = i.SubSectionOneOverviewAr,
                    SubSectionOneImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + i.SubSectionOneImageAr,
                    SubSectionOneUrl = i.SubSectionOneUrlar,
                    SubSectionTwoTitle = i.SubSectionTwoTitleAr,
                    SubSectionTwoOverview = i.SubSectionTwoOverviewAr,
                    SubSectionTwoImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + i.SubSectionTwoImageAr,
                    SubSectionTwoUrl = i.SubSectionTwoUrlar,
                    SubSectionThreeTitle = i.SubSectionThreeTitleAr,
                    SubSectionThreeOverview = i.SubSectionThreeOverviewAr,
                    SubSectionThreeImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + i.SubSectionThreeImageAr,
                    SubSectionThreeUrl = i.SubSectionThreeUrlar
                });
                return await List.ToListAsync();
            }
        }

        // GET: api/HomepageSectionFour/5
        [HttpGet("{id}")]
        public async Task<ActionResult<HomepageSectionFourDTO>> GetXsiHomepageSectionFour(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var xsiHomepageSectionFour = await _context.XsiHomepageSectionFour.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                if (xsiHomepageSectionFour == null)
                {
                    return NotFound();
                }
                HomepageSectionFourDTO dto = new HomepageSectionFourDTO();
                dto.ItemId = xsiHomepageSectionFour.ItemId;
                dto.Title = xsiHomepageSectionFour.Title;
                dto.ViewAllUrl = xsiHomepageSectionFour.ViewAllUrl;
                dto.SubSectionOneTitle = xsiHomepageSectionFour.SubSectionOneTitle;
                dto.SubSectionOneOverview = xsiHomepageSectionFour.SubSectionOneOverview;
                dto.SubSectionOneImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + xsiHomepageSectionFour.SubSectionOneImage;
                dto.SubSectionOneUrl = xsiHomepageSectionFour.SubSectionOneUrl;
                dto.SubSectionTwoTitle = xsiHomepageSectionFour.SubSectionTwoTitle;
                dto.SubSectionTwoOverview = xsiHomepageSectionFour.SubSectionTwoOverview;
                dto.SubSectionTwoImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + xsiHomepageSectionFour.SubSectionTwoImage;
                dto.SubSectionTwoUrl = xsiHomepageSectionFour.SubSectionTwoUrl;
                dto.SubSectionThreeTitle = xsiHomepageSectionFour.SubSectionThreeTitle;
                dto.SubSectionThreeOverview = xsiHomepageSectionFour.SubSectionThreeOverview;
                dto.SubSectionThreeImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + xsiHomepageSectionFour.SubSectionThreeImage;
                dto.SubSectionThreeUrl = xsiHomepageSectionFour.SubSectionThreeUrl;

                return dto;
            }
            else
            {
                var xsiHomepageSectionFour = await _context.XsiHomepageSectionFour.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                if (xsiHomepageSectionFour == null)
                {
                    return NotFound();
                }
                HomepageSectionFourDTO dto = new HomepageSectionFourDTO();
                dto.ItemId = xsiHomepageSectionFour.ItemId;
                dto.Title = xsiHomepageSectionFour.TitleAr;
                dto.ViewAllUrl = xsiHomepageSectionFour.ViewAllUrlar;
                dto.SubSectionOneTitle = xsiHomepageSectionFour.SubSectionOneTitleAr;
                dto.SubSectionOneOverview = xsiHomepageSectionFour.SubSectionOneOverviewAr;
                dto.SubSectionOneImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + xsiHomepageSectionFour.SubSectionOneImageAr;
                dto.SubSectionOneUrl = xsiHomepageSectionFour.SubSectionOneUrlar;
                dto.SubSectionTwoTitle = xsiHomepageSectionFour.SubSectionTwoTitleAr;
                dto.SubSectionTwoOverview = xsiHomepageSectionFour.SubSectionTwoOverviewAr;
                dto.SubSectionTwoImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + xsiHomepageSectionFour.SubSectionTwoImageAr;
                dto.SubSectionTwoUrl = xsiHomepageSectionFour.SubSectionTwoUrlar;
                dto.SubSectionThreeTitle = xsiHomepageSectionFour.SubSectionThreeTitleAr;
                dto.SubSectionThreeOverview = xsiHomepageSectionFour.SubSectionThreeOverviewAr;
                dto.SubSectionThreeImage = _appCustomSettings.UploadsPath + "/HomepageSectionFour/Thumbnail/" + xsiHomepageSectionFour.SubSectionThreeImageAr;
                dto.SubSectionThreeUrl = xsiHomepageSectionFour.SubSectionThreeUrlar;

                return dto;
            }
        }

        private bool XsiHomepageSectionFourExists(long id)
        {
            return _context.XsiHomepageSectionFour.Any(e => e.ItemId == id);
        }
    }
}

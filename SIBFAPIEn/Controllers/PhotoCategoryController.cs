﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PhotoCategoryController : ControllerBase
    {
        public PhotoCategoryController()
        {
        }

        // GET: api/PhotoCategory
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DropdownDataDTO>>> GetXsiPhotoCategory()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiPhotoCategory>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    var List = _context.XsiPhotoCategory.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(x => new DropdownDataDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.Title,
                    }).ToListAsync();

                    return await List;
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiPhotoCategory>();
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                    var List = _context.XsiPhotoCategory.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr).Select(x => new DropdownDataDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.TitleAr,
                    }).ToListAsync();

                    return await List;
                }
            }
        }

        // GET: api/PhotoCategory/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DropdownDataDTO>> GetXsiPhotoCategory(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                
                var xsiItem = await _context.XsiPhotoCategory.FindAsync(id);

                if (xsiItem == null)
                {
                    return NotFound();
                }

                DropdownDataDTO itemDTO = new DropdownDataDTO()
                {
                    ItemId = xsiItem.ItemId,
                    Title = LangId==1?xsiItem.Title: xsiItem.TitleAr,
                };
                return itemDTO;
            }
        }
    }
}

﻿using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SIBFAPIEn.DTO;
using Entities.Models;
using Xsi.ServicesLayer;
using Microsoft.Extensions.Localization;
using System.Text;
using SIBFAPIEn.Utility;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Xsi.BusinessLogicLayer;
using iTextSharp.text;
using NPOI.OpenXmlFormats.Dml.Diagram;
//using Entities.Models;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PCMemberEventController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly IStringLocalizer<AgencyRegistrationController> _stringLocalizer;
        public PCMemberEventController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings,
            IStringLocalizer<AgencyRegistrationController> stringLocalizer, IEmailSender emailSender)
        {
            _logger = logger;
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
            _stringLocalizer = stringLocalizer;
        }


        // GET: api/PCEvents
        [HttpGet]
        [Route("{pageNumber}/{pageSize}/{strSearch}/{strGuest}")]
        public async Task<ActionResult<dynamic>> GetPCEvents(int? pageNumber = 1, int? pageSize = 15, string strSearch = "", string strGuest = "")
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long memberid = User.Identity.GetID();
                    XsiExhibition activeExhibition = _context.XsiExhibition.Where(i => i.IsActive == "Y" && i.WebsiteId == 1).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                    var ProfessionalProgramEntity = _context.XsiExhibitionProfessionalProgram.Where(i => i.IsActive == "Y" && i.ExhibitionId == activeExhibition.ExhibitionId).OrderByDescending(i => i.ProfessionalProgramId).FirstOrDefault();
                    //&& i.IsCheckedIn == "Y"
                    if (ProfessionalProgramEntity != null)
                    {
                        var registration = _context.XsiExhibitionProfessionalProgramRegistration.Where(i => i.IsActive == "Y" && i.Status == "A" && i.MemberId == memberid && i.ProfessionalProgramId == ProfessionalProgramEntity.ProfessionalProgramId).OrderByDescending(i => i.ItemId).FirstOrDefault();

                        if (registration != null)
                        {
                            DateTime dt = MethodFactory.ArabianTimeNow().Date;
                            long LangId = 1;
                            long.TryParse(Request.Headers["LanguageURL"], out LangId);

                            long websiteid = 1;
                            List<long> categoryIDs = new List<long>();
                            List<long> subCategoryIDs = new List<long>();

                            var predicateEvent = PredicateBuilder.New<XsiPcevents>();

                            if (LangId == 1)
                            {
                                predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                                if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                                    predicateEvent = predicateEvent.And(i => i.Title.Contains(strSearch));

                                if (!string.IsNullOrEmpty(strGuest) && strGuest != "-1")
                                    predicateEvent = predicateEvent.And(i => i.GuestName.Contains(strGuest));

                                var List = await _context.XsiPcevents.Where(predicateEvent).Select(ev => new
                                {
                                    ItemId = ev.ItemId,
                                    Title = ev.Title,
                                    Guest = ev.GuestName,
                                    IsRegistered = IsRegisteredForEvent(ev.ItemId, memberid),
                                    SeatCapacity = ev.SeatCapacity,
                                    Available = GetSeatCount(ev.ItemId, ev.SeatCapacity ?? 0),
                                    EventDate = GetDateForUI(ev.EventDate, LangId),
                                    Time = ev.Time,
                                    Overview = ev.Overview
                                }).OrderByDescending(i => i.ItemId).Select(s => s).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                                var totalCount = await _context.XsiPcevents.Where(predicateEvent).Select(ev => ev.ItemId).Select(s => s).CountAsync();

                                if (totalCount == 0)
                                {
                                    var dto = new { MessageTypeResponse = "Success", List, TotalCount = totalCount, Message = "No data found" };
                                    return Ok(dto);
                                }
                                else
                                {
                                    var dto = new { MessageTypeResponse = "Success", List, TotalCount = totalCount };
                                    return Ok(dto);
                                }
                            }
                            else
                            {
                                predicateEvent = predicateEvent.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                                if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                                    predicateEvent = predicateEvent.And(i => i.TitleAr.Contains(strSearch));

                                if (!string.IsNullOrEmpty(strGuest) && strGuest != "-1")
                                    predicateEvent = predicateEvent.And(i => i.GuestNameAr.Contains(strGuest));

                                var List = await _context.XsiPcevents.Where(predicateEvent).Select(ev => new
                                {
                                    ItemId = ev.ItemId,
                                    Title = ev.TitleAr,
                                    Guest = ev.GuestNameAr,
                                    IsRegistered = IsRegisteredForEvent(ev.ItemId, memberid),
                                    SeatCapacity = ev.SeatCapacity,
                                    Available = GetSeatCount(ev.ItemId, ev.SeatCapacity ?? 0),
                                    EventDate = GetDateForUI(ev.EventDate, LangId),
                                    Time = ev.Time,
                                    Overview = ev.OverviewAr
                                }).OrderByDescending(i => i.ItemId).Select(s => s).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                                var totalCount = await _context.XsiPcevents.Where(predicateEvent).Select(ev => ev.ItemId).Select(s => s).CountAsync();

                                if (totalCount == 0)
                                {
                                    var dto = new { MessageTypeResponse = "Success", List, TotalCount = totalCount, Message = "No data found" };
                                    return Ok(dto);
                                }
                                else
                                {
                                    var dto = new { MessageTypeResponse = "Success", List, TotalCount = totalCount };
                                    return Ok(dto);
                                }

                            }
                        }
                        else
                        {
                            var List = new List<string>();
                            var dto = new { MessageTypeResponse = "Error", Message = "PC User need to be approved", List, TotalCount = 0 };
                            return Ok(dto);
                        }
                    }
                    else
                    {
                        var List = new List<string>();
                        var dto = new { MessageTypeResponse = "Error", Message = "No exhibition data found", List, TotalCount = 0 };
                        return Ok(dto);
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiPcevents action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        private bool IsRegisteredForEvent(long eventid, long memberid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var entity = _context.XsiPcmemberEvents.Where(i => i.EventId == eventid && i.MemberId == memberid && i.Status == "B").FirstOrDefault();
                if (entity != null)
                    return true;
                return false;
            }
        }

        // GET: api/PCEvents/1
        [HttpGet("{id}")]
        public async Task<ActionResult<dynamic>> GetPCEventById(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long memberid = User.Identity.GetID();
                var xsiItem = await _context.XsiPcevents.FindAsync(id);

                if (xsiItem == null)
                {
                    MessageDTO dto = new MessageDTO();
                    dto.MessageTypeResponse = "Error";
                    if (LangId == 1)
                        dto.Message = "Event doesn't exist";
                    else
                        dto.Message = "Event doesn't exist";
                    return Ok(dto);
                }

                if (LangId == 1)
                {
                    var predicateEvent = PredicateBuilder.New<XsiPcevents>();
                    predicateEvent = predicateEvent.And(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    var List = await _context.XsiPcevents.Where(predicateEvent).Select(ev => new
                    {
                        ItemId = ev.ItemId,
                        Title = ev.Title,
                        Guest = ev.GuestName,
                        IsRegistered = IsRegisteredForEvent(ev.ItemId, memberid),
                        SeatCapacity = ev.SeatCapacity,
                        Available = GetSeatCount(ev.ItemId, ev.SeatCapacity ?? 0),
                        EventDate = GetDateForUI(ev.EventDate, LangId),
                        Time = ev.Time,
                        Overview = ev.Overview,
                        Description = ev.Description
                    }).Take(1).ToListAsync();

                    //var entity = await _context.XsiPcevents.Where(predicateEvent).Select(ev => new
                    //{
                    //    ItemId = ev.ItemId,
                    //    Title = ev.Title,
                    //    Guest = ev.GuestName,
                    //    Available = GetSeatCount(ev.ItemId, ev.SeatCapacity ?? 0),
                    //    EventDate = GetDateForUI(ev.EventDate, LangId),
                    //    Time = ev.Time,
                    //    Overview = ev.Overview
                    //}).FirstOrDefaultAsync();

                    var dto = new { MessageTypeResponse = "Success", List };
                    return Ok(dto);
                }
                else
                {
                    var predicateEvent = PredicateBuilder.New<XsiPcevents>();
                    predicateEvent = predicateEvent.And(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                    var List = await _context.XsiPcevents.Where(predicateEvent).Select(ev => new
                    {
                        ItemId = ev.ItemId,
                        Title = ev.TitleAr,
                        Guest = ev.GuestNameAr,
                        IsRegistered = IsRegisteredForEvent(ev.ItemId, memberid),
                        SeatCapacity = ev.SeatCapacity,
                        Available = GetSeatCount(ev.ItemId, ev.SeatCapacity ?? 0),
                        EventDate = GetDateForUI(ev.EventDate, LangId),
                        Time = ev.Time,
                        Overview = ev.OverviewAr,
                        Description = ev.DescriptionAr
                    }).Take(1).ToListAsync();

                    var dto = new { MessageTypeResponse = "Success", List };
                    return Ok(dto);
                }
            }
        }

        [HttpPost]
        public ActionResult<dynamic> PostPCProgramRegistration(PCMemberEventsBookingDTO itemDto)
        {
            MessageDTO dto = new MessageDTO();
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long memberid = User.Identity.GetID();
                    if (memberid == itemDto.MemberId)
                    {
                        string strMessage = string.Empty;
                        if (itemDto.EventId > 0)
                        {
                            if (_context.XsiPcevents.Any(i => i.ItemId == itemDto.EventId))
                            {
                                var kount = _context.XsiPcmemberEvents.Where(i => i.EventId == itemDto.EventId && i.MemberId == itemDto.MemberId && i.IsActive == "Y" && i.Status == "B").Count();
                                if (kount == 0)
                                {
                                    dto = SaveEventBooking(itemDto, LangId, _context);
                                    return Ok(dto);
                                }
                                else
                                {
                                    dto.MessageTypeResponse = "Error";
                                    if (LangId == 1)
                                        dto.Message = "You have already registered for the event.";
                                    else
                                        dto.Message = "You have already registered for the event.";
                                    return Ok(dto);
                                }
                            }
                            else
                            {
                                dto.MessageTypeResponse = "Error";
                                if (LangId == 1)
                                    dto.Message = "Event doesn't exist.";
                                else
                                    dto.Message = "Event doesn't exist.";
                                return Ok(dto);
                            }
                        }
                        else
                        {
                            dto.MessageTypeResponse = "Error";
                            if (LangId == 1)
                                dto.Message = "User need to select event before booking";
                            else
                                dto.Message = "User need to select event before booking";
                            return Ok(dto);
                        }
                    }
                    return Unauthorized();
                    //itemDto.Message = "Unauthorized access";
                    //return Ok(itemDto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Event Booking action: {ex.InnerException}");
                dto.MessageTypeResponse = "Error";
                dto.Message = "Something went wrong. Please try again later. ";// + ex.ToString();
                return Ok(dto);
            }
        }

        #region Methods
        private long GetSeatCount(long eventid, long? totalcount)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var kount = _context.XsiPcmemberEvents.Where(i => i.EventId == eventid).Count();
                totalcount = totalcount - kount;
                return totalcount ?? 0;
            }
        }
        private MessageDTO SaveEventBooking(PCMemberEventsBookingDTO itemDto, long langid, sibfnewdbContext _context)
        {
            MessageDTO dto = new MessageDTO();
            XsiPcmemberEvents booking = new XsiPcmemberEvents();
            booking.EventId = itemDto.EventId;
            booking.MemberId = itemDto.MemberId;
            booking.TicketCount = itemDto.TicketCount;
            booking.IsActive = "Y";
            booking.Status = "B";
            booking.CreatedOn = MethodFactory.ArabianTimeNow();
            booking.ModifiedOn = MethodFactory.ArabianTimeNow();
            _context.XsiPcmemberEvents.Add(booking);

            if (_context.SaveChanges() > 0)
            {
                var bookingid = booking.ItemId;
                dto.MessageTypeResponse = "Success";
                if (langid == 1)
                    dto.Message = "Thank you. Event registration has been completed. Your booking reference number:" + bookingid.ToString();
                else
                    dto.Message = "Thank you. Event registration has been completed. Your booking reference number:" + bookingid.ToString();
                return dto;
            }
            else
            {
                dto.MessageTypeResponse = "Error";
                if (langid == 1)
                    dto.Message = "Event booking couldn't be done. Please try again later.";
                else
                    dto.Message = "Event booking couldn't be done. Please try again later.";
                return dto;
            }
        }
        private string GetDateForUI(DateTime? startDate, long langId)
        {
            if (startDate != null)
                return langId == 1 ? startDate.Value.ToString("d MMM yyyy") : startDate.Value.ToString("d MMM yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));

            return string.Empty;
        }
        #endregion
    }
}

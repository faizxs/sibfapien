﻿using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionMemberRegistrationController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        EnumResultType XsiResult { get; set; }
        private readonly IEmailSender _emailSender;
        private readonly sibfnewdbContext _context;

        public ExhibitionMemberRegistrationController(IEmailSender emailSender, IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        // POST: api/ExhibitionMemberRegistration
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> PostXsiExhibitionMember(ExhibitionMemberDTO itemDto)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            long websiteid = 1;
            long.TryParse(Request.Headers["websiteId"], out websiteid);

            if (websiteid > 0)
            {
                itemDto.WebsiteId = websiteid;
            }
            else
            {
                websiteid = 1;
            }

            try
            {
                List<long> CityIDs = new List<long>();
                string username = itemDto.UserName.ToLower();
                string stremail = itemDto.Email.ToLower();

                var useremaillist = _context.XsiExhibitionMember.Select(i => new { i.UserName, i.Email, i.EmailNew, i.CompanyName, i.CityId }).ToList();

                #region Validations
                if (useremaillist.Any(i => i.UserName.ToLower() == username))
                {
                    if (LangId == 1)
                        return BadRequest("Username already exists.");
                    else
                        return BadRequest("الإسم مستخدم سابقاً");
                }

                if (useremaillist.Any(i => i.Email != null && i.Email.ToLower() == stremail))
                {
                    if (LangId == 1)
                        return Ok("Email already exists.");
                    else
                        return Ok("لقد تم التسجيل بهذا البريد الإلكتروني سابقاً.");
                }

                if (useremaillist.Any(i => i.EmailNew != null && i.EmailNew.ToLower() == stremail))
                {
                    if (LangId == 1)
                        return Ok("Email already exists.");
                    else
                        return Ok("لقد تم التسجيل بهذا البريد الإلكتروني سابقاً.");
                }

                var CityEntity = _context.XsiExhibitionCity.Where(i => i.CityId == itemDto.CityId).FirstOrDefault();
                if (CityEntity != null)
                {
                    CityIDs = _context.XsiExhibitionCity.Where(i => i.CountryId == CityEntity.CountryId).Select(i => i.CityId).ToList();
                }

                if (itemDto.ReasonIds.Any(reason => reason == 3))
                {
                    if (useremaillist.Where(i => i.CityId != null && CityIDs.Contains(i.CityId.Value) && i.CompanyName == itemDto.CompanyName).ToList().Count > 0)
                    {
                        if (LangId == 1)
                            return Ok("User already registered with Company Name and Country Name.");
                        else
                            return Ok("User already registered with Company Name and Country Name.");
                    }
                }
                #endregion

                #region Save If unqiue
                XsiExhibitionMember memberEntity = SaveExhibitionMemberRegistration(itemDto, LangId);
                long memberid = memberEntity.MemberId;
                if (memberid > 0)
                {
                    LogMemberStatus(memberEntity);
                    AddMemberReason(memberid, itemDto.OtherReason, itemDto.ReasonIds);

                    if (SendVerificationLinkToMember(memberid, itemDto.WebsiteId) == EnumResultType.Success)
                    {
                        if (LangId == 1)
                            return Ok("Thank you for registering, Please check your email for email verification.");
                        else
                            return Ok("شكرا لتسجيلكم بالموقع، يرجى تأكيد صحة البريد الالكتروني");
                    }
                    else
                    {
                        if (LangId == 1)
                            return Ok("Registered Successfully. Email couldn't be sent for verification. Please contact administrator");
                        else
                            return Ok("التسجيل ناجح. لم نتمكن من إرسال رسالة لتأكيد بريدكم الإلكتروني. الرجاء الإتصال بمدير النظام.");
                    }
                }

                if (LangId == 1)
                    return Ok("Errors while submitting the form. Team will review the form submission issue.");
                else
                    return Ok("Errors while submitting the form. Team will review the form submission issue.");

                #endregion
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later." + ex.InnerException + "<br>" + ex.ToString());
                //throw;
            }
        }

        // POST: api/ExhibitionMemberRegistration
        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("VerifyExhibitionMemberAccount")]
        public ActionResult<dynamic> VerifyExhibitionMemberRegistration([FromBody] VerifyDTO dto)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            long itemid = -1;
            string strMessage = "Account Activation Failed";
            if (IsValidLongQueryStringKey(dto.id, out itemid, false))
            {
                XsiExhibitionMember entity = _context.XsiExhibitionMember.Find(itemid);
                if (entity != null)
                {
                    if (entity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.EmailNotVerified))
                    {
                        //mandatory line
                        entity.MemberId = itemid;
                        //form related 
                        entity.Status = EnumConversion.ToString(EnumExhibitionMemberStatus.EmailVerified);
                        entity.ModifiedById = itemid;
                        entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                        if (_context.SaveChanges() > 0)
                            XsiResult = EnumResultType.Success;

                        if (XsiResult == EnumResultType.Success)
                            strMessage = LangId == 1 ? "<strong>Congratulations!</strong><br /> Your account has been verified. You will be notified after admin approval." : "<strong>Congratulations!</strong><br /> Your account has been verified. You will be notified after admin approval.";
                        else if (XsiResult == EnumResultType.NotFound)
                            strMessage = LangId == 1 ? "Account Activation Failed" : "Account Activation Failed"; //Resources.BackendError.AccountAcitvationFailed;
                    }
                    else if (entity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.Approved))
                        strMessage = LangId == 1 ? "Account Already Activated. Invalid Request" : "تم تفعيل الحساب سابقاً. طلب غير صحيح.";// Resources.BackendError.EnumExhibitionMemberStatusApproved;
                    else if (entity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.Pending))
                        strMessage = LangId == 1 ? "Account Registration Pending" : "Account Registration Pending";// Resources.BackendError.EnumExhibitionMemberStatusPending;
                    else if (entity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.EmailVerified))
                        strMessage = LangId == 1 ? "Email Verified and awaiting for admin approval" : "Email Verified and awaiting for admin approval";// Resources.BackendError.EnumExhibitionMemberStatusEmailVerfied;
                    else if (entity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.Rejected))
                        strMessage = LangId == 1 ? "Account Registration Rejected by admin" : "Account Registration Rejected by admin";// Resources.BackendError.EnumExhibitionMemberStatusRejected;
                    else if (entity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.CredentialNotSent))
                        strMessage = LangId == 1 ? "Account Registration Approved but Credential Not Sent" : "تم إعتماد الحساب لكن الموافقة لم يتم إرسالها";// Resources.BackendError.EnumExhibitionMemberStatusCredentialNotSent;
                    else if (entity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.New))
                        strMessage = LangId == 1 ? "Need to send verification Email to complete registration process" : "Need to send verification Email to complete registration process";// Resources.BackendError.EnumExhibitionMemberStatusNew;
                    LogMemberStatus(entity);
                    return Ok(strMessage);
                }
            }
            return Ok(StatusCodes.Status400BadRequest);
        }

        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("DeleteExhibitionMemberAccount/{id}")]
        public ActionResult<dynamic> DeleteExhibitionMemberAccount(string id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            long memberId = -1;
            string strMessage = "Account Deletion Failed";
            if (IsValidLongQueryStringKey(id, out memberId, false))//TODO Decrypt
            {
                XsiResult = DeleteAnonymousExhibitionMemberRegistration(memberId);
                if (XsiResult == EnumResultType.Success)
                    strMessage = "Deleted successfully.";// Resources.BackendError.DeleteSuccess;
                else if (XsiResult == EnumResultType.NotFound)
                    strMessage = "Account Does Not Exist";//Resources.BackendError.AccountDeosNotExist;
                else if (XsiResult == EnumResultType.ValueInUse)
                    strMessage = "Your account has references. Please contact admin.";// Resources.BackendError.AccountHasReferences;
                return Ok(strMessage);
            }
            return Ok(StatusCodes.Status400BadRequest);
        }

        // POST: api/VerifyForgotPassword
        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("VerifyForgotPassword")]
        public ActionResult<dynamic> VerifyForgotPassword([FromBody] VerifyForgotPasswordDTO dto)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            long itemid = -1;
            string strMessage = "Change Password Process Failed";
            if (IsValidLongQueryStringKey(dto.id, out itemid, false))
            {
                XsiExhibitionMember entity = _context.XsiExhibitionMember.Find(itemid);
                if (entity != null)
                {
                    if (entity.ForgotPasswordExpiresAt == null || MethodFactory.ArabianTimeNow() > entity.ForgotPasswordExpiresAt)
                    {
                        strMessage = "Password reset link expired. Please try again."; // Resources.BackendError.InactiveUserState;
                        return Ok(strMessage);
                    }
                    if (entity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.Approved))
                    {
                        //mandatory line
                        entity.MemberId = itemid;
                        //form related 
                        entity.IsNewPasswordChange = EnumConversion.ToString(EnumBool.Yes);
                        //entity.Password = dto.Password;
                        byte[] passwordHash, passwordSalt;
                        PasswordHasher.CreatePasswordHash(dto.Password, out passwordHash, out passwordSalt);
                        entity.PasswordSalt = passwordSalt;
                        entity.PasswordHash = passwordHash;
                        entity.ForgotPasswordExpiresAt = null;
                        entity.ModifiedById = itemid;
                        entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                        if (_context.SaveChanges() > 0)
                            XsiResult = EnumResultType.Success;

                        if (XsiResult == EnumResultType.Success)
                            strMessage = "Password changed successfuly.";
                        else if (XsiResult == EnumResultType.Failed)
                            strMessage = "Please try again password not changed."; //Resources.BackendError.AccountAcitvationFailed;
                        return Ok(strMessage);
                    }
                    else
                    {
                        strMessage = "User is not authorized to login at the moment."; // Resources.BackendError.InactiveUserState;
                        return Ok(strMessage);
                    }
                }
            }
            return Ok(StatusCodes.Status400BadRequest);
        }


        // POST: api/VerifyEditProfileAccount
        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("VerifyEditProfileAccount")]
        public ActionResult<dynamic> VerifyEditProfileAccount([FromBody] VerifyChangeEmailDTO dto)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            long memberid = Convert.ToInt64(dto.id);
            XsiExhibitionMember entity = _context.XsiExhibitionMember.Where(i => i.MemberId == memberid).FirstOrDefault();
            if (entity != null)
            {
                if (entity.EmailNew != null && entity.Email.ToLower() == entity.EmailNew.ToLower())
                {
                    return Ok("Already verified");
                }
                else
                {
                    entity.Email = entity.EmailNew;
                    entity.ModifiedById = entity.MemberId;
                    entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                    _context.SaveChanges();

                    #region Update Email in Exhibitor Registration
                    XsiExhibition exhibition = new XsiExhibition();
                    if (dto.websiteid == 2 || dto.websiteid == 1)
                    {
                        exhibition = _context.XsiExhibition.Where(i => i.WebsiteId == dto.websiteid && i.IsActive == "Y").OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                    }
                    else
                    {
                        exhibition = _context.XsiExhibition.Where(i => i.WebsiteId == 1 && i.IsActive == "Y").OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                    }

                    if (exhibition != null)
                    {
                        XsiExhibitionMemberApplicationYearly exhibitorentity = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberId == memberid && i.ExhibitionId == exhibition.ExhibitionId && i.MemberRoleId == 1).OrderByDescending(i => i.MemberExhibitionYearlyId).FirstOrDefault();
                        if (exhibitorentity != null)
                        {
                            exhibitorentity.Email = entity.EmailNew;
                            entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                            _context.SaveChanges();
                        }
                    }
                    #endregion

                    if (XsiResult == EnumResultType.Success)
                        return Ok("<strong>Congratulations!</strong><br /> Your email has been updated.");
                    else
                        return Ok("Update Failure");
                }
            }

            return Ok(StatusCodes.Status400BadRequest);
        }

        // POST: api/VerifyChangeEmail
        [Authorize]
        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("VerifyChangeEmail")]
        public ActionResult<dynamic> VerifyChangeEmailThisMethodNotinUse([FromBody] VerifyChangeEmailDTO dto)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            long itemid = -1;
            string strMessage = "Change Email Process Failed. User need to login and click on verify.";
            if (IsValidLongQueryStringKey(dto.id, out itemid, true))
            {
                XsiExhibitionMember entity = _context.XsiExhibitionMember.Find(itemid);
                if (entity != null && entity.EmailNew != null)
                {
                    if (entity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.Approved))
                    {
                        //mandatory line
                        entity.MemberId = itemid;
                        //form related 
                        entity.Email = entity.EmailNew;
                        entity.ModifiedById = itemid;
                        entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                        if (_context.SaveChanges() > 0)
                            XsiResult = EnumResultType.Success;

                        if (XsiResult == EnumResultType.Success)
                            strMessage = "<strong>Congratulations!</strong><br /> Your email has been updated.";
                    }
                    else
                        strMessage = "User is not authorized to login at the moment."; // Resources.BackendError.InactiveUserState;
                    return Ok(strMessage);
                }
            }
            return Ok(StatusCodes.Status400BadRequest);
        }

        [HttpGet]
        public string Decrypt(string value)
        {
            try
            {
                string passPhrase = "Pas5pr@se";        // can be any string
                string saltValue = "s@1tValue";        // can be any string
                string hashAlgorithm = "SHA1";             // can be "MD5"
                int passwordIterations = 2;                  // can be any number
                string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
                int keySize = 256;                // can be 192 or 128

                string strDecrypted = value;

                strDecrypted = Encryption.Decrypt(strDecrypted,
                    passPhrase,
                    saltValue,
                    hashAlgorithm,
                    passwordIterations,
                    initVector,
                    keySize);
                return strDecrypted;
            }
            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong in Decrypt : {ex.Message}");
                return null;
            }
        }

        #region Methods
        private XsiExhibitionMember SaveExhibitionMemberRegistration(ExhibitionMemberDTO itemDto, long LangId)
        {
            XsiExhibitionMember memberEntity = new XsiExhibitionMember();
            memberEntity.UserName = itemDto.UserName;
            //memberEntity.Password = itemDto.Password;
            byte[] passwordHash, passwordSalt;
            PasswordHasher.CreatePasswordHash(itemDto.Password, out passwordHash, out passwordSalt);
            memberEntity.PasswordSalt = passwordSalt;
            memberEntity.PasswordHash = passwordHash;

            memberEntity.Email = itemDto.Email;
            memberEntity.CompanyName = itemDto.CompanyName;
            memberEntity.CompanyNameAr = itemDto.CompanyNameAr;

            memberEntity.LanguageUrl = LangId;

            if (itemDto.CityId != null)
                memberEntity.CityId = itemDto.CityId;
            if (!string.IsNullOrEmpty(itemDto.OtherCity))
                memberEntity.OtherCity = itemDto.OtherCity;

            memberEntity.Firstname = itemDto.FirstName;
            memberEntity.LastName = itemDto.LastName;
            memberEntity.JobTitle = itemDto.ContactTitle;
            memberEntity.JobTitleAr = itemDto.ContactTitleAr;

            memberEntity.CompanyFieldofWork = itemDto.CompanyFieldofWork;
            memberEntity.CompanyFieldofWorkAr = itemDto.CompanyFieldofWorkAr;

            memberEntity.FirstNameAr = itemDto.FirstNameAr;
            memberEntity.LastNameAr = itemDto.LastNameAr;
            memberEntity.Phone = itemDto.ISD + "$" + itemDto.STD + "$" + itemDto.Phone;
            memberEntity.Mobile = itemDto.MobileISD + "$" + itemDto.MobileSTD + "$" + itemDto.Mobile;
            memberEntity.Website = itemDto.Website;
            memberEntity.Pobox = itemDto.POBox;
            memberEntity.Address = itemDto.Address;

            memberEntity.IsVerified = EnumConversion.ToString(EnumBool.No);
            memberEntity.IsEmailSent = EnumConversion.ToString(EnumBool.No);
            memberEntity.Status = EnumConversion.ToString(EnumExhibitionMemberStatus.New);
            memberEntity.IsBooksRequired = EnumConversion.ToString(EnumBool.Yes);
            memberEntity.IsNewPasswordChange = EnumConversion.ToString(EnumBool.Yes);
            memberEntity.CreatedOn = MethodFactory.ArabianTimeNow();
            memberEntity.ModifiedOn = MethodFactory.ArabianTimeNow();
            memberEntity.ModifiedById = -1;
            if (itemDto.WebsiteId > 0)
            {
                memberEntity.WebsiteId = itemDto.WebsiteId;
            }
            else
            {
                memberEntity.WebsiteId = 2;
            }
            _context.XsiExhibitionMember.Add(memberEntity);
            _context.SaveChanges();

            return memberEntity;
        }
        void LogMemberStatus(XsiExhibitionMember exhibitionMember)
        {
            XsiExhibitionMemberStatusLog entity = new XsiExhibitionMemberStatusLog();
            entity.ExhibitionMemberId = exhibitionMember.MemberId;
            entity.Status = exhibitionMember.Status;
            entity.CreatedOn = MethodFactory.ArabianTimeNow();
            _context.XsiExhibitionMemberStatusLog.Add(entity);
            _context.SaveChanges();
        }
        void AddMemberReason(long memberid, string otherreason, List<long> reasonIDs)
        {
            XsiMemberReason entity1 = new XsiMemberReason();
            foreach (var item in reasonIDs)
            {
                if (item == 1)
                {
                    entity1.Title = otherreason;
                }

                entity1.MemberId = memberid;
                entity1.ReasonId = item;
                _context.XsiMemberReason.Add(entity1);
            }
            _context.SaveChanges();
        }
        EnumResultType SendVerificationLinkToMember(long memberId, long websiteId = 1)
        {
            long webid = 1;
            long.TryParse(Request.Headers["websiteId"], out webid);

            long LangId = 1;
            long.TryParse(Request.Headers["Languageurl"], out LangId);

            if (websiteId == -1)
                websiteId = 1;

            websiteId = webid;

            XsiResult = EnumResultType.Failed;
            var member = _context.XsiExhibitionMember.Find(memberId);
            if (member != default(XsiExhibitionMember))
            {
                string verifyAccLink = string.Empty;
                string deleteAccLink = string.Empty;
                string encryptedMemberId = memberId.ToString();// MethodFactory.Encrypt(memberId.ToString(), true);//TODO Encrypt
                if (EnumConversion.ToString(EnumExhibitionMemberStatus.New) == member.Status)
                {
                    #region Send Login details
                    StringBuilder body = new StringBuilder();
                    if (websiteId == 2)
                    {
                        verifyAccLink = _appCustomSettings.ServerAddressSCRF + "en/VerifyExhibitionMemberAccount?mid=" + encryptedMemberId; // Page.ResolveClientUrl(string.Format("{0}Content/EmailMedia/VerifyExhibitionMemberAccount.aspx?mid={1}", serverAddress, encryptedMemberId));
                        deleteAccLink = _appCustomSettings.ServerAddressSCRF + "en/DeleteExhibitionMemberAccount?mid=" + encryptedMemberId; //Page.ResolveClientUrl(string.Format("{0}Content/EmailMedia/DeleteExhibitionMemberAccount.aspx?mid={1}", serverAddress, encryptedMemberId));
                    }
                    else
                    {
                        verifyAccLink = _appCustomSettings.ServerAddressNew + "en/VerifyExhibitionMemberAccount?mid=" + encryptedMemberId; // Page.ResolveClientUrl(string.Format("{0}Content/EmailMedia/VerifyExhibitionMemberAccount.aspx?mid={1}", serverAddress, encryptedMemberId));
                        deleteAccLink = _appCustomSettings.ServerAddressNew + "en/DeleteExhibitionMemberAccount?mid=" + encryptedMemberId; //Page.ResolveClientUrl(string.Format("{0}Content/EmailMedia/DeleteExhibitionMemberAccount.aspx?mid={1}", serverAddress, encryptedMemberId));
                    }

                    XsiEmailContent emailContent = _context.XsiEmailContent.Where(i => i.ItemId == 5).FirstOrDefault();// EmailContentService.GetEmailContent(3, entity.LanguageId.Value);
                    if (emailContent != null)
                    {
                        body.Append(GetEmailContentAndTemplate(LangId, emailContent.Body, member, _appCustomSettings.ServerAddressNew, _appCustomSettings.ServerAddressCMS, websiteId));
                        body.Replace("$$EmailAddress$$", member.Email.Trim());
                        body.Replace("$$ActivateMemberLink$$", verifyAccLink);
                        body.Replace("$$DeleteMemberLink$$", deleteAccLink);
                        body.Replace("$$LoginPage$$", _appCustomSettings.ServerAddressNew); //Page.ResolveClientUrl(string.Format("{0}{1}", serverAddress, "default"))
                        body.Replace("$$pstyle$$", "");
                        body.Replace("$$pstyle$$", "");
                        XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, member.Email, emailContent.Subject, body.ToString(), null);
                        if (XsiResult == EnumResultType.Success)
                        {
                            XsiExhibitionMember xsiExhibitionMember = _context.XsiExhibitionMember.Find(memberId);
                            //mandatory line
                            xsiExhibitionMember.MemberId = member.MemberId;
                            //form related 
                            xsiExhibitionMember.Status = EnumConversion.ToString(EnumExhibitionMemberStatus.EmailNotVerified);
                            xsiExhibitionMember.ModifiedById = member.MemberId;
                            xsiExhibitionMember.ModifiedOn = MethodFactory.ArabianTimeNow();
                            _context.SaveChanges();
                        }
                    }
                    #endregion
                }
            }

            return XsiResult;
        }
        EnumResultType DeleteAnonymousExhibitionMemberRegistration(long itemId)
        {
            XsiExhibitionMember entity = _context.XsiExhibitionMember.Find(itemId);
            if (entity != null)
            {
                if (entity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.Banned))
                {
                    var memberReasons = _context.XsiMemberReason.Where(i => i.MemberId == itemId).ToList();
                    foreach (var reason in memberReasons)
                    {
                        _context.XsiMemberReason.Remove(reason);
                    }
                    _context.SaveChanges();

                    _context.XsiExhibitionMember.Remove(entity);
                    _context.SaveChanges();

                    return EnumResultType.Success;
                }
                return EnumResultType.ValueInUse;
            }
            return EnumResultType.NotFound;
        }
        #endregion
        #region Email Utility
        StringBuilder GetEmailContentAndTemplate(long languageId, string bodyContent, XsiExhibitionMember entity, string serverAddress, string serverAddressCMS, long Wid)
        {
            if (Wid == -1 || Wid == 0)
                Wid = 1;
            string scrfURL = _appCustomSettings.ServerAddressSCRF;
            string emailContent = string.Empty;
            if (languageId == 1)
            {
                if (Wid == Convert.ToInt64(EnumWebsiteId.SCRF))
                    emailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRF.html";
                else
                    emailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
            }
            else
            {
                if (Wid == Convert.ToInt64(EnumWebsiteId.SCRF))
                    emailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRF.html";
                else
                    emailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
            }
            StringBuilder body = new StringBuilder();

            if (System.IO.File.Exists(emailContent))
            {
                body.Append(System.IO.File.ReadAllText(emailContent));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);
            string str = GetExhibitionDetails(Wid, languageId);
            string[] strArray = str.Split(',');
            body.Replace("$$exhibitionno$$", strArray[0].ToString());
            body.Replace("$$exhibitiondate$$", strArray[1].ToString());
            body.Replace("$$exhyear$$", strArray[2]);

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            body.Replace(swidth, sreplacewidth);


            body.Replace("$$ServerAddressCMS$$", serverAddressCMS);
            body.Replace("$$ServerAddress$$", serverAddress);
            if (Wid == Convert.ToInt64(EnumWebsiteId.SCRF))
                body.Replace("$$ServerAddressSCRF$$", scrfURL);

            if (entity != null)
            {
                string illustratorname = string.Empty;
                if (languageId == 1)
                {
                    if (entity.Firstname != null && entity.LastName != null)
                        illustratorname = entity.Firstname + " " + entity.LastName;
                    else if (entity.Firstname != null)
                        illustratorname = entity.Firstname;
                    else if (entity.LastName != null)
                        illustratorname = entity.LastName;
                }
                else
                {
                    if (entity.FirstNameAr != null && entity.LastNameAr != null)
                        illustratorname = entity.FirstNameAr + " " + entity.LastNameAr;
                    else if (entity.Firstname != null)
                        illustratorname = entity.FirstNameAr;
                    else if (entity.LastName != null)
                        illustratorname = entity.LastNameAr;

                    if (string.IsNullOrEmpty(illustratorname))
                    {
                        if (entity.Firstname != null && entity.LastName != null)
                            illustratorname = entity.Firstname + " " + entity.LastName;
                        else if (entity.Firstname != null)
                            illustratorname = entity.Firstname;
                        else if (entity.LastName != null)
                            illustratorname = entity.LastName;
                    }
                }
                body.Replace("$$Name$$", illustratorname);
            }
            return body;
        }
        string GetExhibitionDetails(long websiteId, long langid = 1)
        {
            long? exhibitionNumber = -1;
            string strdate = string.Empty;
            string stryear = MethodFactory.ArabianTimeNow().Year.ToString();

            XsiExhibition entity = _context.XsiExhibition.Where(i => i.WebsiteId == websiteId && i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.IsArchive == EnumConversion.ToString(EnumBool.No)).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
            string startdaysuffix = string.Empty;
            string enddaysuffix = string.Empty;

            if (entity != default(XsiExhibition))
            {
                if (entity.ExhibitionNumber != null)
                    exhibitionNumber = entity.ExhibitionNumber;
                long dayno = entity.StartDate.Value.Date.Day;
                if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                    startdaysuffix = "th";
                else
                    startdaysuffix = GetSuffix(entity.StartDate.Value.Date.Day % 10, entity.StartDate.Value);
                dayno = entity.EndDate.Value.Date.Day;
                if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                    enddaysuffix = "th";
                else
                    enddaysuffix = GetSuffix(entity.EndDate.Value.Date.Day % 10, entity.EndDate.Value);

                //if (entity.StartDate.Value.Year == entity.EndDate.Value.Year)
                //{
                //    if (entity.StartDate.Value.Month == entity.EndDate.Value.Month)
                //        strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.StartDate.Value.ToString("MMM") + " " + entity.StartDate.Value.ToString("yyyy");
                //    else
                //        strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " + entity.StartDate.Value.ToString("MMM")
                //             + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.EndDate.Value.ToString("MMM")
                //            + " " + entity.StartDate.Value.ToString("yyyy");
                //}
                //else
                //{
                //    strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " + entity.StartDate.Value.ToString("MMM") + " " + entity.StartDate.Value.ToString("yyyy")
                //         + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.EndDate.Value.ToString("MMM")
                //        + " " + entity.EndDate.Value.ToString("yyyy");
                //}

                if (langid == 1)
                {
                    #region English

                    if (entity.StartDate.Value.Year == entity.EndDate.Value.Year)
                    {
                        if (entity.StartDate.Value.Month == entity.EndDate.Value.Month)
                            strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " - " +
                                      entity.EndDate.Value.Date.Day + enddaysuffix + " " +
                                      entity.StartDate.Value.ToString("MMM") + " " +
                                      entity.StartDate.Value.ToString("yyyy");
                        else
                            strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " +
                                      entity.StartDate.Value.ToString("MMM")
                                      + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " +
                                      entity.EndDate.Value.ToString("MMM")
                                      + " " + entity.StartDate.Value.ToString("yyyy");
                    }
                    else
                    {
                        strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " +
                                  entity.StartDate.Value.ToString("MMM") + " " +
                                  entity.StartDate.Value.ToString("yyyy")
                                  + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " +
                                  entity.EndDate.Value.ToString("MMM")
                                  + " " + entity.EndDate.Value.ToString("yyyy");
                    }

                    #endregion
                }
                else
                {
                    #region Arabic
                    if (entity.StartDate.Value.Year == entity.EndDate.Value.Year)
                    {
                        if (entity.StartDate.Value.Month == entity.EndDate.Value.Month)
                            strdate = entity.StartDate.Value.Date.Day + " - " +
                                      entity.EndDate.Value.Date.Day + " " +
                                      entity.StartDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")) + " " +
                                      entity.StartDate.Value.ToString("yyyy");
                        else
                            strdate = entity.StartDate.Value.Date.Day + " " +
                                      entity.StartDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"))
                                      + " - " + entity.EndDate.Value.Date.Day + " " +
                                      entity.EndDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"))
                                      + " " + entity.StartDate.Value.ToString("yyyy");
                    }
                    else
                    {
                        strdate = entity.StartDate.Value.Date.Day + " " +
                                  entity.StartDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")) + " " +
                                  entity.StartDate.Value.ToString("yyyy")
                                  + " - " + entity.EndDate.Value.Date.Day + " " +
                                  entity.EndDate.Value.ToString("MMM", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"))
                                  + " " + entity.EndDate.Value.ToString("yyyy");
                    }
                    #endregion
                }

                if (entity.ExhibitionYear != null)
                    stryear = entity.ExhibitionYear.ToString();
            }
            return exhibitionNumber.ToString() + "," + strdate + "," + stryear;
        }
        string GetSuffix(int startday, DateTime dt)
        {
            string daysuffix;
            if (startday != 11 && dt.Date.Day != 11)
            {
                if (startday == 1)
                    daysuffix = "st";
                else if (startday == 2)
                    daysuffix = "nd";
                else if (startday == 3)
                    daysuffix = "rd";
                else
                    daysuffix = "th";
            }
            else
                daysuffix = "th";
            return daysuffix;
        }
        bool IsValidLongQueryStringKey(string key, out long id, bool isEncrypted = false)
        {
            if (isEncrypted)
            {
                string value = MethodFactory.Decrypt(key);
                if (long.TryParse(value, out id))
                    return true;
            }
            else
            {
                if (long.TryParse(key, out id))
                    return true;
            }

            id = -1;
            return false;
        }
        long CheckIfNewCityAndAdd(long? entityCityId, string cityName = "")
        {
            var exhibitionCity = _context.XsiExhibitionCity.Find(entityCityId);
            if (exhibitionCity != null)
            {
                if (exhibitionCity.CityName == "Other" || exhibitionCity.CityNameAr == "أخرى")
                {
                    if (!string.IsNullOrWhiteSpace(cityName))
                    {
                        XsiExhibitionCity city = new XsiExhibitionCity();
                        city.CityName = cityName;
                        city.CityNameAr = cityName;
                        _context.XsiExhibitionCity.Add(city);
                        _context.SaveChanges();
                        return city.CityId;
                    }
                }
            }

            return entityCityId ?? -1;
        }
        #endregion
    }
}

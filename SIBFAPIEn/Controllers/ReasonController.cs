﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;

using SIBFAPIEn.Utility;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ReasonController : ControllerBase
    {
        public ReasonController()
        {
        }

        // GET: api/Reason
        [HttpGet]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetXsiReason()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiReason>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    var list = _context.XsiReason.AsQueryable().Where(predicate).OrderBy(x => x.ReasonId).Select(x => new {
                        value = x.ReasonId,
                        label = x.Title,
                    }).ToListAsync();

                    return await list;
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiReason>();
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                    var list = _context.XsiReason.AsQueryable().Where(predicate).OrderBy(x => x.ReasonId).Select(x => new 
                    {
                        value = x.ReasonId,
                        label = x.TitleAr,
                    }).ToListAsync();

                    return await list;
                }
            }
        }

        // GET: api/Reason/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DropdownDataDTO>> GetXsiReason(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var xsiItem = await _context.XsiReason.FindAsync(id);

                if (xsiItem == null)
                {
                    return NotFound();
                }

                DropdownDataDTO itemDto = new DropdownDataDTO()
                {
                    ItemId = xsiItem.ReasonId,
                    Title = LangId==1?xsiItem.Title: xsiItem.TitleAr,
                };
                return itemDto;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class HomepageSectionTwoController : ControllerBase
    {

        private readonly sibfnewdbContext _context;
        private IMemoryCache _cache;
        private readonly AppCustomSettings _appCustomSettings;
        public static List<XsiEventCategory> cachedEventCategory { get; set; }
        public static List<XsiEventSubCategory> cachedEventSubCategory { get; set; }

        public List<XsiEvent> cachedEvents { get; set; }
        public HomepageSectionTwoController(IOptions<AppCustomSettings> appCustomSettings, IMemoryCache memoryCache, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _cache = memoryCache;
            _context = context;
        }

        // GET: api/HomepageSectionTwo
        [HttpGet]
        public async Task<ActionResult<IEnumerable<HomepageSectionTwoDTO>>> GetXsiHomepageSectionTwo()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var List = _context.XsiHomepageSectionTwo.Where(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.ItemId).Select(i => new HomepageSectionTwoDTO()
                {
                    ItemId = i.ItemId,
                    CategoryId = i.CategoryId,
                    // CategoryTitle = i.Category.Title,
                    Title = i.Title,
                    Thumbnail = _appCustomSettings.UploadsPath + "/HomepageSectionTwo/Thumbnail/" + i.Thumbnail,
                    Color = i.Color,
                    Url = i.Url,
                    ActivityCount = GetActivityCount(i.Url, LangId)
                });
                return await List.ToListAsync();
            }
            else
            {
                var List = _context.XsiHomepageSectionTwo.Where(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.ItemId).Select(i => new HomepageSectionTwoDTO()
                {
                    ItemId = i.ItemId,
                    CategoryId = i.CategoryId,
                    // CategoryTitle = i.Category.Title,
                    Title = i.TitleAr,
                    Thumbnail = _appCustomSettings.UploadsPath + "/HomepageSectionTwo/Thumbnail/" + i.ThumbnailAr,
                    Color = i.ColorAr,
                    Url = i.Urlar,
                    ActivityCount = GetActivityCount(i.Url, LangId)
                });
                return await List.ToListAsync();
            }
        }

        // GET: api/HomepageSectionTwo/5
        [HttpGet("{id}")]
        public async Task<ActionResult<HomepageSectionTwoDTO>> GetXsiHomepageSectionTwo(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var xsiHomepageSectionTwo = await _context.XsiHomepageSectionTwo.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes))
               .Select(i => new HomepageSectionTwoDTO()
               {
                   ItemId = i.ItemId,
                   CategoryId = i.CategoryId,
                   // CategoryTitle = i.Category.Title,
                   Title = i.Title,
                   Thumbnail = _appCustomSettings.UploadsPath + "/HomepageSectionTwo/Thumbnail/" + i.Thumbnail,
                   Color = i.Color,
                   Url = i.Url
               }).FirstOrDefaultAsync();

                if (xsiHomepageSectionTwo == null)
                {
                    return NotFound();
                }

                return xsiHomepageSectionTwo;
            }
            else
            {
                var xsiHomepageSectionTwo = await _context.XsiHomepageSectionTwo.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes))
               .Select(i => new HomepageSectionTwoDTO()
               {
                   ItemId = i.ItemId,
                   CategoryId = i.CategoryId,
                   // CategoryTitle = i.Category.Title,
                   Title = i.TitleAr,
                   Thumbnail = _appCustomSettings.UploadsPath + "/HomepageSectionTwo/Thumbnail/" + i.ThumbnailAr,
                   Color = i.ColorAr,
                   Url = i.Urlar
               }).FirstOrDefaultAsync();

                if (xsiHomepageSectionTwo == null)
                {
                    return NotFound();
                }

                return xsiHomepageSectionTwo;
            }
        }

        private bool XsiHomepageSectionTwoExists(long id)
        {
            return _context.XsiHomepageSectionTwo.Any(e => e.ItemId == id);
        }

        long GetActivityCount(string url, long langid)
        {
            if (cachedEventSubCategory == null)
                cachedEventSubCategory = CacheKeys.GetEventSubCategory(_cache);
            if (cachedEvents == null)
                cachedEvents = CacheKeys.GetEvents(_cache);

            long categoryid = -1;
            var str = url.Split('=');
            long.TryParse(str[1], out categoryid);

            if (langid == 1)
            {
                var subcatIDs = cachedEventSubCategory.Where(i => i.EventCategoryId == categoryid && i.IsActive == "Y" && i.WebsiteId == 1).Select(i => i.ItemId).ToList();
                var kount = cachedEvents.Where(i => i.EventSubCategoryId != null && subcatIDs.Contains(i.EventSubCategoryId.Value) && (i.Type == "A" || i.Type == "B") && i.IsActive == "Y").ToList().Count();
                return kount;
            }
            else
            {
                var subcatIDs = cachedEventSubCategory.Where(i => i.EventCategoryId == categoryid && i.IsActiveAr == "Y" && i.WebsiteId == 1).Select(i => i.ItemId).ToList();
                var kount = cachedEvents.Where(i => i.EventSubCategoryId != null && subcatIDs.Contains(i.EventSubCategoryId.Value) && (i.Type == "A" || i.Type == "B") && i.IsActiveAr == "Y").ToList().Count();
                return kount;
            }
        }        
    }
}

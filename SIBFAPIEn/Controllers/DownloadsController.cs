﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using System.IO;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    public class DownloadsController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly sibfnewdbContext _context;
        private readonly AppCustomSettings _appCustomSettings;

        public DownloadsController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _context = context;
        }

        // GET: api/Downloads
        [HttpGet]
        [Route("{pageNumber}/{pageSize}/{categoryId}")]
        public async Task<ActionResult<dynamic>> GetXsiDownloads(int? pageNumber = 1, int? pageSize = 15, long? categoryId = null)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiDownloads>();
                    predicate = predicate.And(i => i.WebsiteId != 2);
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    if (categoryId != null && categoryId != -1)
                        predicate = predicate.And(i => i.Dated != null && i.DownloadCategoryId == categoryId);

                    var List = await _context.XsiDownloads.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new
                    {
                        ItemId = x.ItemId,
                        CategoryId = x.DownloadCategoryId,
                        CategoryTitle = x.DownloadCategory.Title,
                        Title = x.Title,
                        Dated = x.Dated == null ? string.Empty : x.Dated.Value.ToString("dd MMMM yyyy"),
                        FileName = _appCustomSettings.UploadsPath + "/downloads/" + x.FileName
                    }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    var count = _context.XsiDownloads.Where(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes)).ToList()
                        .Count;
                    var dto = new { List, TotalCount = count };
                    return dto;
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiDownloads>();
                    predicate = predicate.And(i => i.WebsiteId != 2);
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    if (categoryId != null && categoryId != -1)
                        predicate = predicate.And(i => i.Dated != null && i.DownloadCategoryId == categoryId);

                    var List = await _context.XsiDownloads.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new
                    {
                        ItemId = x.ItemId,
                        CategoryId = x.DownloadCategoryId,
                        CategoryTitle = x.DownloadCategory.TitleAr,
                        Title = x.TitleAr,
                        Dated = x.DatedAr == null ? string.Empty : x.DatedAr.Value.ToString("dd MMMM yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")),
                        FileName = _appCustomSettings.UploadsPath + "/downloads/" + x.FileNameAr
                    }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    var count = _context.XsiDownloads.Where(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).ToList()
                        .Count;
                    var dto = new { List, TotalCount = count };
                    return dto;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiDownloads action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        // GET: api/Downloads/5
        [HttpGet("{id}")]
        public async Task<ActionResult<dynamic>> GetXsiDownloads(long id)
        {
            try
            {
                var xsiItem = await _context.XsiDownloads.FindAsync(id);

                if (xsiItem == null)
                {
                    return NotFound();
                }
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var itemDTO = new
                    {
                        ItemId = xsiItem.ItemId,
                        CategoryId = xsiItem.DownloadCategoryId,
                        CategoryTitle = xsiItem.DownloadCategoryId.ToString(),
                        Title = xsiItem.Title,
                        Dated = xsiItem.Dated == null ? string.Empty : xsiItem.Dated.Value.ToString("dd MMMM yyyy"),
                        FileName = _appCustomSettings.UploadsPath + "/downloads/" + xsiItem.FileName
                    };
                    return itemDTO;
                }
                else
                {
                    var itemDTO = new
                    {
                        ItemId = xsiItem.ItemId,
                        CategoryId = xsiItem.DownloadCategoryId,
                        CategoryTitle = xsiItem.DownloadCategoryId.ToString(),
                        Title = xsiItem.TitleAr,
                        Dated = xsiItem.DatedAr == null ? string.Empty : xsiItem.DatedAr.Value.ToString("dd MMMM yyyy", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")),
                        FileName = _appCustomSettings.UploadsPath + "/downloads/" + xsiItem.FileNameAr
                    };
                    return itemDTO;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiDownloads by id action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");

            }
        }

        [HttpGet]
        [Route("ClearBooksExport")]
        public Task ClearBooksExport()
        {
            return Task.Factory.StartNew(() =>
            {
                string path = _appCustomSettings.UploadsPhysicalPath + "\\BooksExport\\";
                Parallel.ForEach(Directory.GetFiles(path),
                    f =>
                    {
                        var fi = new FileInfo(f);
                        var hours = (MethodFactory.ArabianTimeNow() - fi.LastAccessTime).TotalHours;
                        if (hours > 12)
                            fi.Delete();
                    });
            });
        }
        private bool XsiDownloadsExists(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiDownloads.Any(e => e.ItemId == id);
            }
        }
        private string GetDownloadCategoryTitle(long catId)
        {
            var entity = _context.XsiDownloadCategory.Where(e => e.IsActive == "Y" && e.ItemId == catId).FirstOrDefault();
            if (entity != null && !string.IsNullOrWhiteSpace(entity.Title))
                return entity.Title;
            return string.Empty;
        }
    }
}

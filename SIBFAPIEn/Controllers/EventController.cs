﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Org.BouncyCastle.Asn1.Cms;
using SIBFAPIEn.DTO;
using Entities.Models;
using System.IO;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class EventsController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public EventsController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        // GET: api/Events
        [HttpGet]
        [Route("{pageNumber}/{pageSize}/{category}/{subcategory}/{type}/{strSearch}/{guestid}")]
        public async Task<ActionResult<dynamic>> GetXsiEvents(int? pageNumber = 1, int? pageSize = 15, long category = -1, long subcategory = -1, string type = "A", string strSearch = "", long guestid = -1)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long websiteid = 1;
                List<long> categoryIDs = new List<long>();
                List<long> subCategoryIDs = new List<long>();
                List<long> eventIDsfromEventGuest = new List<long>();
                var predicateCategory = PredicateBuilder.True<XsiEventCategory>();
                var predicateSubCategory = PredicateBuilder.True<XsiEventSubCategory>();
                var predicateEvent = PredicateBuilder.True<XsiEvent>();
                var predicateEventDate = PredicateBuilder.True<XsiEventDate>();

                List<long> eventIDfromEventGuests = new List<long>();
                List<long> eventIDsfromEventWebsite = new List<long>();
                if (LangId == 1)
                {
                    predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateCategory = predicateCategory.And(i => (i.WebsiteId == websiteid || i.WebsiteId == 0));

                    if (subcategory > -1)
                    {
                        predicateSubCategory = predicateSubCategory.And(i => i.ItemId == subcategory);
                    }
                    else if (category > -1)
                    {
                        predicateCategory = predicateCategory.And(i => i.ItemId == category);
                        categoryIDs = _context.XsiEventCategory.Where(predicateCategory).Select(p => p.ItemId).ToList();
                        predicateSubCategory = predicateSubCategory.And(i => categoryIDs.Contains(i.EventCategoryId.Value));
                    }

                    predicateSubCategory = predicateSubCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateSubCategory = predicateSubCategory.And(i => (i.WebsiteId == websiteid || i.WebsiteId == 0));
                    subCategoryIDs = _context.XsiEventSubCategory.Where(predicateSubCategory).Select(p => p.ItemId).ToList();
                    predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));

                    predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateEvent = predicateEvent.And(i => i.Type == type || i.Type == "E" || i.Type == "B");

                    var eventWEBSITEIDs = _context.XsiEventWebsite.Where(i => i.WebsiteId == 1).Select(i => i.EventId).ToList();
                    predicateEvent = predicateEvent.And(i => eventWEBSITEIDs.Contains(i.ItemId));

                    if (guestid > -1)
                    {
                        eventIDsfromEventGuest = _context.XsiEventGuest.Where(i => i.GuestId == guestid).Select(i => i.EventId.Value).ToList();
                        predicateEvent = predicateEvent.And(i => eventIDsfromEventGuest.Contains(i.ItemId));
                    }
                    else
                    {
                        if (eventIDsfromEventGuest.Count() > 0)
                            predicateEvent = predicateEvent.And(i => eventIDsfromEventGuest.Contains(i.ItemId));
                    }
                    //else
                    //{
                    //    eventIDsfromEventGuest = _context.XsiEventGuest.Select(i => i.EventId.Value).ToList();
                    //}

                  

                    if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                        predicateEvent = predicateEvent.And(i => i.Title.Contains(strSearch));

                    var predicateExhibition = PredicateBuilder.True<XsiExhibition>();
                    predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == websiteid);
                    XsiExhibition exhibition = _context.XsiExhibition.Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        var dtNow = DateTime.Now.Date;
                        //predicateEventDate = predicateEventDate.And(i => (i.StartDate >= exhibition.StartDate && i.StartDate <= exhibition.EndDate) || (i.EndDate >= dtNow)); //&& dtNow <= exhibition.EndDate

                        // predicateEventDate = predicateEventDate.And(i => i.StartDate >= dtNow && dtNow <= exhibition.EndDate); //during exhibition 

                        predicateEventDate = predicateEventDate.And(i => i.StartDate >= dtNow && dtNow <= i.EndDate);
                    }

                    var List = await _context.XsiEventDate.Where(predicateEventDate)
                         .Join(_context.XsiEvent.Where(predicateEvent)
                             , ed => ed.EventId
                             , ev => ev.ItemId
                             , (ed, ev) => new
                             {
                                 ItemId = ed.ItemId,
                                 EventItemId = ed.ItemId,
                                 ed.EventId,
                                 ev.EventSubCategoryId,
                                 ev.PhotoAlbumId,
                                 ev.VideoAlbumId,
                                 ev.Type,
                                 ev.IsActive,
                                 ev.IsFeatured,
                                 ev.Title,
                                 ev.Overview,
                                 ev.Details,
                                 CategoryTitle = ev.EventSubCategory.EventCategory.Title,
                                 Color = ev.EventSubCategory.EventCategory.Color,
                                 SubCategoryTitle = ev.EventSubCategory.Title,
                                 StartDate1 = GetDateForUI(ed.StartDate, LangId),
                                 EndDate1 = GetDateForUI(ed.EndDate, LangId),
                                 StartDateNew1 = GetDateForUI(ed.StartDate, LangId),
                                 EndDateNew1 = GetDateForUI(ed.EndDate, LangId),
                                 StartDateSort = ed.StartDate,
                                 EndDateSort = ed.EndDate,
                                 ev.Guest,
                                 ev.Location,
                                 ev.FileName,
                                 ev.EventThumbnailNew,
                                 ev.InnerTopImageOne,
                                 ev.InnerTopImageTwo,
                                 Time = GetTimeForUI(ed.StartDate, LangId),
                                 TimeEnd = GetTimeForUI(ed.EndDate, LangId),
                                 BookingLink = ev.BookingLink
                             })
                         .OrderBy(i => i.StartDateSort).ThenBy(i => i.EndDateSort)
                         .Select(s => s).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();
                    //.ThenBy(i => i.EndDate1)
                    var totalCount = await _context.XsiEventDate.Where(predicateEventDate)
                        .Join(_context.XsiEvent.Where(predicateEvent)
                            , ed => ed.EventId
                            , ev => ev.ItemId
                            , (ed, ev) => new { }).CountAsync();

                    var dto = new { List, TotalCount = totalCount };

                    return Ok(dto);
                }
                else
                {
                    predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateCategory = predicateCategory.And(i => (i.WebsiteId == websiteid || i.WebsiteId == 0));

                    if (subcategory > -1)
                    {
                        predicateSubCategory = predicateSubCategory.And(i => i.ItemId == subcategory);
                    }
                    else if (category > -1)
                    {
                        predicateCategory = predicateCategory.And(i => i.ItemId == category);
                        categoryIDs = _context.XsiEventCategory.Where(predicateCategory).Select(p => p.ItemId).ToList();
                        predicateSubCategory = predicateSubCategory.And(i => categoryIDs.Contains(i.EventCategoryId.Value));
                    }


                    predicateSubCategory = predicateSubCategory.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicateSubCategory = predicateSubCategory.And(i => (i.WebsiteId == websiteid || i.WebsiteId == 0));
                    subCategoryIDs = _context.XsiEventSubCategory.Where(predicateSubCategory).Select(p => p.ItemId).ToList();
                    predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));

                    predicateEvent = predicateEvent.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicateEvent = predicateEvent.And(i => i.Type == type || i.Type == "E" || i.Type == "B");

                    var eventWEBSITEIDs = _context.XsiEventWebsite.Where(i => i.WebsiteId == 1).Select(i => i.EventId).ToList();
                    predicateEvent = predicateEvent.And(i => eventWEBSITEIDs.Contains(i.ItemId));

                    if (guestid > -1)
                    {
                        eventIDsfromEventGuest = _context.XsiEventGuest.Where(i => i.GuestId == guestid).Select(i => i.EventId.Value).ToList();
                    }
                    //else
                    //{
                    //    eventIDsfromEventGuest = _context.XsiEventGuest.Select(i => i.EventId.Value).ToList();
                    //}

                    if (eventIDsfromEventGuest.Count() > 0)
                        predicateEvent = predicateEvent.And(i => eventIDsfromEventGuest.Contains(i.ItemId));

                    if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                        predicateEvent = predicateEvent.And(i => i.Title.Contains(strSearch));

                    var predicateExhibition = PredicateBuilder.True<XsiExhibition>();
                    predicateExhibition = predicateExhibition.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == websiteid);
                    XsiExhibition exhibition = _context.XsiExhibition.Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                    if (exhibition != null)
                    {
                        /*   var dtNow = DateTime.Now.Date;
                           //   predicateEventDate = predicateEventDate.And(i => (i.StartDate >= exhibition.StartDate && i.StartDate <= exhibition.EndDate) || (i.EndDate >= dtNow)); //&& dtNow <= exhibition.EndDate
                           predicateEventDate = predicateEventDate.And(i => i.StartDate >= dtNow && dtNow <= exhibition.EndDate);
                        */

                        var dtNow = DateTime.Now.Date;
                        //predicateEventDate = predicateEventDate.And(i => (i.StartDate >= exhibition.StartDate && i.StartDate <= exhibition.EndDate) || (i.EndDate >= dtNow)); //&& dtNow <= exhibition.EndDate

                        // predicateEventDate = predicateEventDate.And(i => i.StartDate >= dtNow && dtNow <= exhibition.EndDate); //during exhibition 

                        predicateEventDate = predicateEventDate.And(i => i.StartDate >= dtNow && dtNow <= i.EndDate);
                    }

                    var List = await _context.XsiEventDate.Where(predicateEventDate)
                         .Join(_context.XsiEvent.Where(predicateEvent)
                             , ed => ed.EventId
                             , ev => ev.ItemId
                             , (ed, ev) => new
                             {
                                 ItemId = ed.ItemId,
                                 EventItemId = ed.ItemId,
                                 ed.EventId,
                                 ev.EventSubCategoryId,
                                 ev.PhotoAlbumId,
                                 ev.VideoAlbumId,
                                 ev.Type,
                                 IsActive = ev.IsActiveAr,
                                 ev.IsFeatured,
                                 Title = ev.TitleAr ?? ev.Title,
                                 Overview = ev.OverviewAr ?? ev.Overview,
                                 Details = ev.DetailsAr ?? ev.Details,
                                 CategoryTitle = ev.EventSubCategory.EventCategory.TitleAr,
                                 Color = ev.EventSubCategory.EventCategory.ColorAr,
                                 SubCategoryTitle = ev.EventSubCategory.TitleAr,
                                 StartDate1 = GetDateForUI(ed.StartDate, LangId),
                                 EndDate1 = GetDateForUI(ed.EndDate, LangId),
                                 StartDateNew1 = GetDateForUI(ed.StartDate, LangId),
                                 EndDateNew1 = GetDateForUI(ed.EndDate, LangId),
                                 StartDateSort = ed.StartDate,
                                 EndDateSort = ed.EndDate,
                                 Guest = ev.GuestAr ?? ev.Guest,
                                 Location = ev.LocationAr ?? ev.Location,
                                 FileName = ev.FileNameAr ?? ev.FileName,
                                 EventThumbnailNew = ev.EventThumbnailNewAr ?? ev.EventThumbnailNew,
                                 InnerTopImageOne = ev.InnerTopImageOneAr ?? ev.InnerTopImageOne,
                                 InnerTopImageTwo = ev.InnerTopImageTwoAr ?? ev.InnerTopImageTwo,
                                 Time = GetTimeForUI(ed.StartDate, LangId),
                                 TimeEnd = GetTimeForUI(ed.EndDate, LangId),
                                 BookingLink = ev.BookingLink
                             }).OrderBy(i => i.StartDateSort).ThenBy(i => i.EndDateSort)
                            .Select(s => s).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();
                    //.ThenBy(i => i.EndDate1)
                    var totalCount = await _context.XsiEventDate.Where(predicateEventDate)
                        .Join(_context.XsiEvent.Where(predicateEvent)
                            , ed => ed.EventId
                            , ev => ev.ItemId
                            , (ed, ev) => new { }).CountAsync();

                    var dto = new { List, TotalCount = totalCount };

                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiEvents action: {ex.InnerException}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        // GET: api/events/1
        [HttpGet("{id}")]
        public async Task<ActionResult<dynamic>> GetXsiEvent(long id)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var predicateEvent = PredicateBuilder.True<XsiEvent>();
                if (LangId == 1)
                {
                    //predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    var event1 = await _context.XsiEventDate.Where(i => i.ItemId == id)
                    .Join(_context.XsiEvent.Where(predicateEvent)
                        , ed => ed.EventId
                        , ev => ev.ItemId
                        , (ed, ev) => new
                        {
                            ItemId = ed.ItemId,
                            EventItemId = ed.ItemId,
                            ed.EventId,
                            ev.EventSubCategoryId,
                            ev.PhotoAlbumId,
                            ev.VideoAlbumId,
                            ev.Type,
                            ev.IsActive,
                            ev.IsFeatured,
                            ev.Title,
                            ev.Overview,
                            ev.Details,
                            CategoryTitle = ev.EventSubCategory.EventCategory.Title,
                            Color = ev.EventSubCategory.EventCategory.Color,
                            SubCategoryTitle = ev.EventSubCategory.Title,
                            StartDate1 = GetDateForUI(ed.StartDate, LangId),
                            EndDate1 = GetDateForUI(ed.EndDate, LangId),
                            StartDateNew1 = GetDateForUI(ed.StartDate, LangId),
                            EndDateNew1 = GetDateForUI(ed.EndDate, LangId),
                            ev.Guest,
                            ev.Location,
                            ev.FileName,
                            EventThumbnailNew = _appCustomSettings.UploadsCMSPath + "/eventnew/" + ev.EventThumbnailNew ?? "activity1.png",
                            ev.InnerTopImageOne,
                            ev.InnerTopImageTwo,
                            Time = GetTimeForUI(ed.StartDate, LangId),
                            TimeEnd = GetTimeForUI(ed.EndDate, LangId),
                            BookingLink = ev.BookingLink
                        }).FirstOrDefaultAsync();

                    if (event1 == null)
                    {
                        return NotFound();
                    }

                    return Ok(event1);
                }
                else
                {
                    //predicateEvent = predicateEvent.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    var event1 = await _context.XsiEventDate.Where(i => i.ItemId == id)
                    .Join(_context.XsiEvent.Where(predicateEvent)
                        , ed => ed.EventId
                        , ev => ev.ItemId
                        , (ed, ev) => new
                        {
                            ItemId = ed.ItemId,
                            EventItemId = ed.ItemId,
                            ed.EventId,
                            ev.EventSubCategoryId,
                            ev.PhotoAlbumId,
                            ev.VideoAlbumId,
                            ev.Type,
                            ev.IsActiveAr,
                            ev.IsFeatured,
                            Title = ev.TitleAr,
                            Overview = ev.OverviewAr,
                            Details = ev.DetailsAr,
                            CategoryTitle = ev.EventSubCategory.EventCategory.TitleAr,
                            Color = ev.EventSubCategory.EventCategory.ColorAr,
                            SubCategoryTitle = ev.EventSubCategory.TitleAr,
                            StartDate1 = GetDateForUI(ed.StartDate, LangId),
                            EndDate1 = GetDateForUI(ed.EndDate, LangId),
                            StartDateNew1 = GetDateForUI(ed.StartDate, LangId),
                            EndDateNew1 = GetDateForUI(ed.EndDate, LangId),
                            Guest = ev.GuestAr,
                            Location = ev.LocationAr,
                            FileName = ev.FileNameAr,
                            EventThumbnailNew = _appCustomSettings.UploadsCMSPath + "/eventnew/" + ev.EventThumbnailNewAr ?? "activity1.png",
                            InnerTopImageOne = ev.InnerTopImageOneAr,
                            InnerTopImageTw = ev.InnerTopImageTwoAr,
                            Time = GetTimeForUI(ed.StartDate, LangId),
                            TimeEnd = GetTimeForUI(ed.EndDate, LangId),
                            BookingLink = ev.BookingLink
                        }).FirstOrDefaultAsync();

                    if (event1 == null)
                    {
                        return NotFound();
                    }

                    return Ok(event1);
                }
                //if (type == "E")
                //    MethodFactory.LoadPageContentsNew(14, langId);
                //else
                //    MethodFactory.LoadPageContentsNew(12, langId);
                //predicateEvent = predicateEvent.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiEvents action: {ex.InnerException}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        // GET: api/homepageevent
        [HttpGet]
        [Route("homepageevent")]
        public async Task<ActionResult<dynamic>> GetEventForHomepage()
        {
            try
            {
                DateTime dt = MethodFactory.ArabianTimeNow().Date;
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long websiteid = 1;
                bool IsGenerate = false;
                bool.TryParse(Request.Headers["IsGenerate"], out IsGenerate);

                string pathen = _appCustomSettings.UploadsPhysicalPath + "\\JsonData\\EventHomePage.json";
                string pathar = _appCustomSettings.UploadsPhysicalPath + "\\JsonData\\EventHomePageAr.json";
                if (LangId == 1)
                {
                    FileInfo fi = new FileInfo(pathen);

                    var hours = (fi.CreationTime - MethodFactory.ArabianTimeNow()).Hours;
                    if (IsGenerate || hours > 5 || !fi.Exists)
                    {
                        #region Search Parameters
                        List<long> categoryIDs = new List<long>();
                        List<long> subCategoryIDs = new List<long>();

                        var predicateCategory = PredicateBuilder.True<XsiEventCategory>();
                        var predicateSubCategory = PredicateBuilder.True<XsiEventSubCategory>();
                        var predicateEvent = PredicateBuilder.True<XsiEvent>();
                        var predicateEventDate = PredicateBuilder.True<XsiEventDate>();

                        predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateCategory = predicateCategory.And(i => i.WebsiteId == websiteid || i.WebsiteId == 0);

                        categoryIDs = _context.XsiEventCategory.Where(predicateCategory).Select(p => p.ItemId).ToList();

                        predicateSubCategory = predicateSubCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateSubCategory = predicateSubCategory.And(i => categoryIDs.Contains(i.EventCategoryId.Value));
                        predicateSubCategory = predicateSubCategory.And(i => i.WebsiteId == websiteid || i.WebsiteId == 0);
                        subCategoryIDs = _context.XsiEventSubCategory.Where(predicateSubCategory).Select(p => p.ItemId).ToList();

                        predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");

                        if (subCategoryIDs.Count > 0)
                            predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));

                        var predicateExhibition = PredicateBuilder.True<XsiExhibition>();
                        predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == websiteid);
                        XsiExhibition exhibition = _context.XsiExhibition.Where(predicateExhibition).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();

                        if (exhibition != null)
                        {
                            //predicateEventDate = predicateEventDate.And(i =>
                            //    i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate);
                            //predicateEventDate = predicateEventDate.And(i => (i.StartDate.Value.Date >= dt.Date));

                            var dtNow = DateTime.Now.Date;
                            predicateEventDate = predicateEventDate.And(i => i.StartDate >= dtNow && dtNow <= i.EndDate);
                        }
                        #endregion
                        var EventList = await _context.XsiEventDate.Where(predicateEventDate).OrderBy(i => i.StartDate)
                      .Join(_context.XsiEvent.Where(predicateEvent)
                          , ed => ed.EventId
                          , ev => ev.ItemId
                          , (ed, ev) => new HomeEventDTO
                          {
                              EventDateId = ed.ItemId,
                              Title = ev.Title,
                              Overview = ev.Overview,
                              SubCatId = ev.EventSubCategoryId.Value,
                              EventDate = GetEventDateHome(ed.StartDate, ed.EndDate, LangId)
                          }).Take(3).ToListAsync();
                        System.IO.File.WriteAllText(pathen, JsonConvert.SerializeObject(EventList));
                        List<HomeEventDTO> jsonfiledata = JsonConvert.DeserializeObject<List<HomeEventDTO>>(System.IO.File.ReadAllText(pathen));
                        using (StreamReader file = System.IO.File.OpenText(pathen))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomeEventDTO> List = (List<HomeEventDTO>)serializer.Deserialize(file, typeof(List<HomeEventDTO>));
                            var dto = new { List };
                            return Ok(dto);
                        }
                    }
                    else
                    {
                        List<HomeEventDTO> jsonfiledata = JsonConvert.DeserializeObject<List<HomeEventDTO>>(System.IO.File.ReadAllText(pathen));
                        using (StreamReader file = System.IO.File.OpenText(pathen))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomeEventDTO> List = (List<HomeEventDTO>)serializer.Deserialize(file, typeof(List<HomeEventDTO>));
                            var dto = new { List };
                            return Ok(dto);
                        }
                    }
                }
                else
                {
                    FileInfo fi = new FileInfo(pathar);

                    var hours = (fi.CreationTime - MethodFactory.ArabianTimeNow()).Hours;
                    if (IsGenerate || hours > 5 || !fi.Exists)
                    {
                        #region Search Parameters
                        List<long> categoryIDs = new List<long>();
                        List<long> subCategoryIDs = new List<long>();

                        var predicateCategory = PredicateBuilder.True<XsiEventCategory>();
                        var predicateSubCategory = PredicateBuilder.True<XsiEventSubCategory>();
                        var predicateEvent = PredicateBuilder.True<XsiEvent>();
                        var predicateEventDate = PredicateBuilder.True<XsiEventDate>();

                        predicateCategory = predicateCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateCategory = predicateCategory.And(i => i.WebsiteId == websiteid || i.WebsiteId == 0);

                        categoryIDs = _context.XsiEventCategory.Where(predicateCategory).Select(p => p.ItemId).ToList();

                        predicateSubCategory = predicateSubCategory.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateSubCategory = predicateSubCategory.And(i => categoryIDs.Contains(i.EventCategoryId.Value));
                        predicateSubCategory = predicateSubCategory.And(i => i.WebsiteId == websiteid || i.WebsiteId == 0);
                        subCategoryIDs = _context.XsiEventSubCategory.Where(predicateSubCategory).Select(p => p.ItemId).ToList();

                        predicateEvent = predicateEvent.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicateEvent = predicateEvent.And(i => i.Type == "A" || i.Type == "B");

                        if (subCategoryIDs.Count > 0)
                            predicateEvent = predicateEvent.And(i => subCategoryIDs.Contains(i.EventSubCategoryId.Value));

                        var predicateExhibition = PredicateBuilder.True<XsiExhibition>();
                        predicateExhibition = predicateExhibition.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.WebsiteId == websiteid);
                        XsiExhibition exhibition = _context.XsiExhibition.OrderByDescending(i => i.ExhibitionId).Where(predicateExhibition).FirstOrDefault();

                        if (exhibition != null)
                        {
                            ////predicateEventDate = predicateEventDate.And(i =>
                            ////    i.StartDate >= exhibition.StartDate && i.EndDate <= exhibition.EndDate);
                            // predicateEventDate = predicateEventDate.And(i => (i.StartDate.Value.Date >= dt.Date));

                            var dtNow = DateTime.Now.Date;
                            predicateEventDate = predicateEventDate.And(i => i.StartDate >= dtNow && dtNow <= i.EndDate);
                        }
                        #endregion
                        var EventList = await _context.XsiEventDate.Where(predicateEventDate).OrderBy(i => i.StartDate)
                   .Join(_context.XsiEvent.Where(predicateEvent)
                       , ed => ed.EventId
                       , ev => ev.ItemId
                       , (ed, ev) => new HomeEventDTO
                       {
                           EventDateId = ed.ItemId,
                           Title = ev.TitleAr,
                           Overview = ev.OverviewAr,
                           SubCatId = ev.EventSubCategoryId.Value,
                           EventDate = GetEventDateHome(ed.StartDate, ed.EndDate, LangId)
                       }).Take(3).ToListAsync();

                        System.IO.File.WriteAllText(pathar, JsonConvert.SerializeObject(EventList));
                        List<HomeEventDTO> jsonfiledata = JsonConvert.DeserializeObject<List<HomeEventDTO>>(System.IO.File.ReadAllText(pathar));
                        using (StreamReader file = System.IO.File.OpenText(pathar))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomeEventDTO> List = (List<HomeEventDTO>)serializer.Deserialize(file, typeof(List<HomeEventDTO>));
                            var dto = new { List };
                            return Ok(dto);
                        }
                    }
                    else
                    {
                        List<HomeEventDTO> jsonfiledata = JsonConvert.DeserializeObject<List<HomeEventDTO>>(System.IO.File.ReadAllText(pathar));
                        using (StreamReader file = System.IO.File.OpenText(pathar))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            List<HomeEventDTO> List = (List<HomeEventDTO>)serializer.Deserialize(file, typeof(List<HomeEventDTO>));
                            var dto = new { List };
                            return Ok(dto);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside homepageevent action: {ex.InnerException}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        // GET: api/guestlist
        [HttpGet]
        [Route("guestlist")]
        public async Task<ActionResult<dynamic>> GetEventGuestList()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var predicateGuest = PredicateBuilder.New<XsiGuests>();
                var predicateEventGuest = PredicateBuilder.New<XsiEventGuest>();
                List<DropdownDataDTO> MainList = new List<DropdownDataDTO>();
                DropdownDataDTO obj = new DropdownDataDTO();
                obj.ItemId = -1;
                obj.Title = LangId == 1 ? "Select Guest" : "Select GuestAr";
                MainList.Add(obj);

                if (LangId == 1)
                {
                    predicateGuest = predicateGuest.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    var List = await _context.XsiGuests.Where(predicateGuest)
                                        .OrderBy(o => o.GuestName).Select(i => new DropdownDataDTO()
                                        {
                                            ItemId = i.ItemId,
                                            Title = i.GuestName,
                                        }).ToListAsync();
                    MainList = MainList.Concat(List).ToList();
                    var dto = new { MainList, Message = "", MessageTypeResponse = "Success" };
                    return Ok(dto);
                }
                else
                {
                    predicateGuest = predicateGuest.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                    var List = await _context.XsiGuests.AsExpandable().Where(predicateGuest)
                        .OrderBy(o => o.GuestNameAr).Select(i => new DropdownDataDTO()
                        {
                            ItemId = i.ItemId,
                            Title = i.GuestNameAr ?? i.GuestName,
                        }).ToListAsync();
                    MainList = MainList.Concat(List).ToList();
                    var dto = new { MainList, Message = "", MessageTypeResponse = "Success" };
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetEventGuestList action: {ex.InnerException}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        #region Methods
        string GetWeek(string day, long langId)
        {
            string str = langId == 1 ? "Week" : "أسبوع";
            long d = Convert.ToInt64(day);
            if (d >= 1 && d <= 7)
                return "1st " + str;
            else if (d >= 8 && d <= 14)
                return "2nd " + str;
            else if (d >= 15 && d <= 21)
                return "3rd " + str;
            else if (d >= 22 && d <= 28)
                return "4th " + str;
            else
                return "5th " + str;
        }
        string GetDay(string day)
        {
            long day1 = Convert.ToInt64(day);
            string ordinal = string.Empty;
            switch (day1)
            {
                case 1:
                case 21:
                case 31:
                    ordinal = "st";
                    break;
                case 2:
                case 22:
                    ordinal = "nd";
                    break;
                case 3:
                case 23:
                    ordinal = "rd";
                    break;
                default:
                    ordinal = "th";
                    break;
            }
            return day1.ToString() + ordinal;
        }
        string GetGuestNameByEventId(long eventid, int langid)
        {
            string strguestname = string.Empty;
            var guestIDs = _context.XsiEventGuest.Where(i => i.EventId == eventid).Select(i => i.GuestId).ToList();
            if (langid == 1)
            {
                var guestlist = _context.XsiGuests.Where(i => i.IsActive == "Y" && guestIDs.Contains(i.ItemId)).ToList();

                foreach (var guest in guestlist)
                {
                    if (!string.IsNullOrEmpty(guest.GuestName))
                        strguestname = strguestname + guest.GuestName + ",";
                }
                if (strguestname.IndexOf(",") > -1)
                    strguestname = strguestname.Substring(0, strguestname.Length - 1);
            }
            else
            {
                var guestlist = _context.XsiGuests.Where(i => i.IsActive == "Y" && guestIDs.Contains(i.ItemId)).ToList();
                foreach (var guest in guestlist)
                {
                    if (!string.IsNullOrEmpty(guest.GuestNameAr))
                        strguestname = strguestname + guest.GuestNameAr + ",";
                }
                if (strguestname.IndexOf(",") > -1)
                    strguestname = strguestname.Substring(0, strguestname.Length - 1);
            }
            return strguestname;
        }
        private string GetEventDate(DateTime? startDate, DateTime? endDate, long langId)
        {
            string strdate = "";
            if (startDate.HasValue)
                strdate = GetWeek(startDate.Value.ToString("dd"), langId) + ", " + startDate.Value.ToString("ddd") + " " + GetDay(startDate.Value.ToString("dd"));
            if (endDate.HasValue)
                strdate += endDate.Value.ToString("ddd") + " " + GetDay(endDate.Value.ToString("dd"));
            return strdate;
        }
        private string GetEventDateHome(DateTime? startDate, DateTime? endDate, long langId)
        {
            string strdate = "";
            if (startDate.HasValue)
            {
                if (langId == 1)
                    strdate = GetWeek(startDate.Value.ToString("dd"), langId) + ", " + startDate.Value.ToString("ddd") + " " + GetDay(startDate.Value.ToString("dd"));
                else
                    strdate = GetWeek(startDate.Value.ToString("dd"), langId) + ", " + startDate.Value.ToString("ddd", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE")) + " " + GetDay(startDate.Value.ToString("dd"));
            }
            //if (endDate.HasValue)
            //    strdate += endDate.Value.ToString("ddd") + " " + GetDay(endDate.Value.ToString("dd"));
            return strdate;
        }
        private string GetDateForUI(DateTime? startDate, long langId)
        {
            if (startDate != null)
                return langId == 1 ? startDate.Value.ToString("yyyy-MM-dd") : startDate.Value.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));

            return string.Empty;
        }
        private string GetTimeForUI(DateTime? dt, long langId)
        {
            if (dt != null)
                return langId == 1 ? dt.Value.ToString("t") : dt.Value.ToString("t", System.Globalization.CultureInfo.CreateSpecificCulture("ar-AE"));
            return string.Empty;
        }
        #endregion
    }
}

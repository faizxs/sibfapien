﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionBoothSectionController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionBoothSectionController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/ExhibitionBoothSection
        [HttpGet("{websiteid}")]
        public async Task<ActionResult<IEnumerable<CategoryDTO>>> GetXsiExhibitionBoothSection(long websiteid)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionBooth>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => (i.WebsiteType == 0 || i.WebsiteType == websiteid));
                predicate = predicate.And(i => i.Title != "Restaurant");
                var List = _context.XsiExhibitionBooth.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(x => new CategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = LangId == 1 ? x.Title : x.TitleAr
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionBooth>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => (i.WebsiteType == 0 || i.WebsiteType == websiteid));
                predicate = predicate.And(i => i.Title != "Restaurant");

                var List = _context.XsiExhibitionBooth.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr).Select(x => new CategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.TitleAr
                }).ToListAsync();

                return await List;
            }
        }


        // GET: api/ExhibitionBoothSection/5
        [HttpGet]
        [Route("GetExhibitionBoothSectionById/{id}")]
        public async Task<ActionResult<CategoryDTO>> GetXsiExhibitionBoothSectionById(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

            var xsiItem = await _context.XsiExhibitionBooth.FindAsync(id);

            if (xsiItem == null)
            {
                return NotFound();
            }

            CategoryDTO itemDTO = new CategoryDTO()
            {
                ItemId = xsiItem.ItemId,
                Title = LangId == 1 ? xsiItem.Title : xsiItem.TitleAr
            };
            return itemDTO;
        }

        private bool XsiExhibitionBoothSectionExists(long id)
        {
            return _context.XsiExhibitionBooth.Any(e => e.ItemId == id);
        }
    }
}

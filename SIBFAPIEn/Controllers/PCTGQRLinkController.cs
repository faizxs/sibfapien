﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using SIBFAPIEn.Utility;
using SIBFAPIEn.DTO;
using Microsoft.AspNetCore.Cors;
using LinqKit;
using Microsoft.Extensions.Options;
using Xsi.ServicesLayer;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Net.Http.Headers;
using Contracts;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PCTGQRLinkController : ControllerBase
    {
        EnumResultType XsiResult { get; set; }
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;
        private ILoggerManager _logger;

        public PCTGQRLinkController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IEmailSender emailSender)
        {
            _emailSender = emailSender;
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
        }

        [HttpPost]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> PostPCTGQRCheckIn(PCTGQRLinkDTO pcdto)
        {
            MessageDTO dto = new MessageDTO();
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    if (pcdto.ItemId > 0)
                    {
                        var pcregistration = _context.XsiExhibitionProfessionalProgramRegistration.Where(i => i.ItemId == pcdto.ItemId).FirstOrDefault();
                        if (pcregistration != null)
                        {
                            if (pcregistration.Status == "A")
                            {
                                if (pcregistration.IsCheckedIn == EnumConversion.ToString(EnumBool.Yes))
                                {
                                    dto.MessageTypeResponse = "Error";
                                    if (LangId == 1)
                                        dto.Message = "Already marked as checked in.";
                                    else
                                        dto.Message = "Already marked as checked in.";
                                    return Ok(dto);
                                }
                                else
                                {
                                    pcregistration.IsCheckedIn = EnumConversion.ToString(EnumBool.Yes);
                                    _context.SaveChanges();
                                    dto.MessageTypeResponse = "Success";
                                    if (LangId == 1)
                                        dto.Message = "Thank you. User is Marked as checked in.";
                                    else
                                        dto.Message = "Thank you. User is Marked as checked in.";
                                    return Ok(dto);
                                    
                                    //XsiResult = SendEmailToUser(pcregistration.ItemId, LangId);
                                    //if (XsiResult == EnumResultType.Success || XsiResult == EnumResultType.EmailSent)
                                    //{
                                    //    dto.MessageTypeResponse = "Success";
                                    //    if (LangId == 1)
                                    //        dto.Message = "Thank you. An email is sent to your registered Email Id.";
                                    //    else
                                    //        dto.Message = "Thank you. An email is sent to your registered Email Id.";
                                    //    return Ok(dto);
                                    //}
                                    //else
                                    //{
                                    //    dto.MessageTypeResponse = "Error";
                                    //    if (LangId == 1)
                                    //        dto.Message = "Record updated. An exception occured while sending email";
                                    //    else
                                    //        dto.Message = "Record updated. An exception occured while sending email";
                                    //    return Ok(dto);
                                    //}
                                }
                            }
                            else
                            {
                                dto.MessageTypeResponse = "Error";
                                if (LangId == 1)
                                    dto.Message = "Registration is not yet approved.";
                                else
                                    dto.Message = "Registration is not yet approved.";
                                return Ok(dto);
                            }
                        }
                    }
                    dto.MessageTypeResponse = "Error";
                    if (LangId == 1)
                        dto.Message = "No data found.";
                    else
                        dto.Message = "No data found.";
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                dto.MessageTypeResponse = "Error";
                dto.Message = "Something went wrong. Please try again later.";
                _logger.LogError($"Something went wrong inside PPCTG link update: {ex.InnerException}");
                return Ok(dto);
            }
        }

        private EnumResultType SendEmailToUser(long pcregistrationid, long langid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                var pcregistrationentity = _context.XsiExhibitionProfessionalProgramRegistration.Where(i => i.ItemId == pcregistrationid).FirstOrDefault();
                var member = _context.XsiExhibitionMember.Where(i => i.MemberId == pcregistrationentity.MemberId).FirstOrDefault();
                var pcentity = _context.XsiExhibitionProfessionalProgram.Where(i => i.ProfessionalProgramId == pcregistrationentity.ProfessionalProgramId).FirstOrDefault();
                var exhibition = _context.XsiExhibition.Where(i => i.ExhibitionId == pcentity.ExhibitionId).First();

                #region Send  Email to User
                StringBuilder body = new StringBuilder();
                string strEmailContentBody = "";
                string strEmailContentEmail = "";
                string strEmailContentSubject = "";
                string strEmailContentAdmin = "";
                #region Email Content
                if (exhibition.WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                {
                    var scrfEmailContent = _context.XsiScrfemailContent.Where(x => x.ItemId == 20136).FirstOrDefault();
                    if (scrfEmailContent != null)
                    {
                        if (langid == 1)
                        {
                            if (scrfEmailContent.Body != null)
                                strEmailContentBody = scrfEmailContent.Body;
                            if (scrfEmailContent.Email != null)
                                strEmailContentEmail = scrfEmailContent.Email;
                            if (scrfEmailContent.Subject != null)
                                strEmailContentSubject = scrfEmailContent.Subject;
                        }
                        else
                        {
                            if (scrfEmailContent.BodyAr != null)
                                strEmailContentBody = scrfEmailContent.BodyAr;
                            if (scrfEmailContent.Email != null)
                                strEmailContentEmail = scrfEmailContent.Email;
                            if (scrfEmailContent.SubjectAr != null)
                                strEmailContentSubject = scrfEmailContent.SubjectAr;
                        }

                        strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                    }
                }
                else
                {
                    var emailContent = _context.XsiEmailContent.Where(x => x.ItemId == 20174).FirstOrDefault();
                    if (emailContent != null)
                    {
                        if (langid == 1)
                        {
                            if (emailContent.Body != null)
                                strEmailContentBody = emailContent.Body;
                            if (emailContent.Email != null)
                                strEmailContentEmail = emailContent.Email;
                            if (emailContent.Subject != null)
                                strEmailContentSubject = emailContent.Subject;
                        }
                        else
                        {
                            if (emailContent.BodyAr != null)
                                strEmailContentBody = emailContent.BodyAr;
                            if (emailContent.Email != null)
                                strEmailContentEmail = emailContent.Email;
                            if (emailContent.SubjectAr != null)
                                strEmailContentSubject = emailContent.SubjectAr;
                        }
                        strEmailContentAdmin = _appCustomSettings.AdminName;
                    }
                }
                #endregion

                body.Append(htmlContentFactory.BindEmailContent(langid, strEmailContentBody, member, _appCustomSettings.ServerAddressNew, exhibition.WebsiteId ?? 1, _appCustomSettings));
                XsiResult = _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, member.Email, strEmailContentSubject, body.ToString(), null);
                #endregion
            }
            return XsiResult;
        }
    }
}

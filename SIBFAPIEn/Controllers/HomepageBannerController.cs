﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class HomepageBannerController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public HomepageBannerController(IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        // GET: api/XsiHomepageBanner
        [HttpGet]
        public async Task<ActionResult<IEnumerable<HomepageBannerDTO>>> GetXsiHomepageBannerNew()
        {
            if (MethodFactory.iSHomepageSectionAllowed(Convert.ToInt64(EnumHomePageSection.SectionOne)))
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var List = _context.XsiHomepageBannerNew.AsNoTracking().Where(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.SortOrder).Select(i => new HomepageBannerDTO()
                    {
                        ItemId = i.ItemId,
                        Cmstitle = i.Cmstitle,
                        Title = i.Title,
                        SubTitle = i.SubTitle,
                        SubTitleTwo = (i.ItemId == 59) ? "Opening Hours" : string.Empty,
                        SubTitleThree = (i.ItemId == 59) ? "Sunday – Wednesday: 10:00 – 22:00, Thursday - Saturday: 10:00 – 23:00, Fridays: 16:00-23:00" : string.Empty,
                        HomeBanner = _appCustomSettings.UploadsPath + "/HomepageBannerNew/" + i.HomeBanner,
                        Url = i.Url,
                        IsFeatured = i.IsFeatured,
                        Color = i.Color,
                        SortOrder = i.SortOrder
                    });

                    return await List.ToListAsync();
                }
                else
                {
                    var List = _context.XsiHomepageBannerNew.AsNoTracking().Where(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).OrderBy(i => i.SortOrder).Select(i => new HomepageBannerDTO()
                    {
                        ItemId = i.ItemId,
                        Cmstitle = i.CmstitleAr,
                        Title = i.TitleAr,
                        SubTitle = i.SubTitleAr,
                        SubTitleTwo = (i.ItemId == 59) ? "ساعات العمل" : string.Empty,
                        SubTitleThree = (i.ItemId == 59) ? "من الإثنين إلى الأربعاء من 10:00 ولغاية 22:00، أيام الخميس، السبت والأحد من 10:00 ولغاية 23:00، ما عدا يوم الجمعة من 16:00 إلى 23:00" : string.Empty,
                        HomeBanner = _appCustomSettings.UploadsPath + "/HomepageBannerNew/" + i.HomeBannerAr,
                        Url = i.Urlar,
                        IsFeatured = i.IsFeatured,
                        Color = i.ColorAr,
                        SortOrder = i.SortOrder
                    });

                    return await List.ToListAsync();
                }
            }
            return null;

        }

        // GET: api/XsiHomepageBannerNew/5
        [HttpGet("{id}")]
        public async Task<ActionResult<HomepageBannerDTO>> GetXsiHomepageBannerNew(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var xsiHomepageBannerNew = await _context.XsiHomepageBannerNew.AsNoTracking().Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                if (xsiHomepageBannerNew == null)
                {
                    return NotFound();
                }
                HomepageBannerDTO dto = new HomepageBannerDTO();
                dto.ItemId = xsiHomepageBannerNew.ItemId;
                dto.Cmstitle = xsiHomepageBannerNew.Cmstitle;
                dto.Title = xsiHomepageBannerNew.Title;
                dto.SubTitle = xsiHomepageBannerNew.SubTitle;
                dto.HomeBanner = _appCustomSettings.UploadsPath + "/HomepageBannerNew/" + xsiHomepageBannerNew.HomeBanner;
                dto.Url = xsiHomepageBannerNew.Url;
                dto.IsFeatured = xsiHomepageBannerNew.IsFeatured;
                dto.Color = xsiHomepageBannerNew.Color;
                dto.SortOrder = xsiHomepageBannerNew.SortOrder;

                return dto;
            }
            else
            {
                var xsiHomepageBannerNew = await _context.XsiHomepageBannerNew.AsNoTracking().Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                if (xsiHomepageBannerNew == null)
                {
                    return NotFound();
                }
                HomepageBannerDTO dto = new HomepageBannerDTO();
                dto.ItemId = xsiHomepageBannerNew.ItemId;
                dto.Cmstitle = xsiHomepageBannerNew.Cmstitle;
                dto.Title = xsiHomepageBannerNew.Title;
                dto.SubTitle = xsiHomepageBannerNew.SubTitle;
                dto.HomeBanner = _appCustomSettings.UploadsPath + "/HomepageBannerNew/" + xsiHomepageBannerNew.HomeBanner;
                dto.Url = xsiHomepageBannerNew.Url;
                dto.IsFeatured = xsiHomepageBannerNew.IsFeatured;
                dto.Color = xsiHomepageBannerNew.Color;
                dto.SortOrder = xsiHomepageBannerNew.SortOrder;

                return dto;
            }
        }

        private bool XsiHomepageBannerNewExists(long id)
        {
            return _context.XsiHomepageBannerNew.Any(e => e.ItemId == id);
        }
    }
}

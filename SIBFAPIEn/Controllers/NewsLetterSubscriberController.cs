﻿using Contracts;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SIBFAPIEn.DTO;
using Entities.Models;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class NewsLetterSubscriberController : ControllerBase
    {

        private ILoggerManager _logger;
        EnumResultType XsiResult { get; set; }

        private readonly IEmailSender _emailSender;
        private readonly sibfnewdbContext _context;
        public NewsLetterSubscriberController(ILoggerManager logger, sibfnewdbContext context)
        {
            _logger = logger;
            _context = context;
        }

        // POST: api/NewsLetterSubscriber
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> PostXsiNewsletterSubscribers(NewsLetterSubscriberDTO data)
        {
            try
            {
                string response = string.Empty;
                if (data.IsSubscribe == "Y")
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    EnumResultType objResult = EnumResultType.Success;
                    if (LangId == 1)
                        objResult = Subscribe(data, 1);
                    else
                        objResult = SubscribeAr(data);
                    if (objResult == EnumResultType.Success)
                        response = "Subscribed Successfully";
                    else if (objResult == EnumResultType.AlreadyExists)
                        response = "Email Address Already Exist.";
                    else
                        response = "Error! insert operation failed.";
                }
                else
                {
                    EnumResultType objResult = UnSubscribe(data);
                    if (objResult == EnumResultType.Success)
                        response = "إلغاء اشتراكك بنجاح";
                    else if (objResult == EnumResultType.NotFound)
                        response = "البريد الإلكتروني غير موجود لإلغاء الاشتراك";
                    else
                        response = "Error! update operation failed.";
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside PostXsiNewsletterSubscribers action: {ex.InnerException}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        EnumResultType Subscribe(NewsLetterSubscriberDTO data, long langId)
        {
            XsiNewsletterSubscribers entity = new XsiNewsletterSubscribers();
            List<XsiNewsletterSubscribers> newsLetterSubscriberList = _context.XsiNewsletterSubscribers.Where(i => i.Email == data.Email).ToList();
            if (newsLetterSubscriberList.Count > 0)
            {
                if (newsLetterSubscriberList[0].IsSubscribe != null)
                {
                    if (newsLetterSubscriberList[0].IsSubscribe != EnumConversion.ToString(EnumBool.Yes))
                    {
                        entity.ModifiedById = newsLetterSubscriberList[0].ItemId;
                        entity.ItemId = newsLetterSubscriberList[0].ItemId;
                        entity.IsSubscribe = EnumConversion.ToString(EnumBool.Yes);
                        if (_context.SaveChanges() > 0)
                            XsiResult = EnumResultType.Success;
                        return XsiResult;
                    }
                    else return EnumResultType.AlreadyExists;
                }
                else
                {
                    entity.ItemId = newsLetterSubscriberList[0].ItemId;
                    entity.IsSubscribe = EnumConversion.ToString(EnumBool.Yes);
                    if (_context.SaveChanges() > 0)
                        XsiResult = EnumResultType.Success;
                    return XsiResult;
                }
            }
            else
            {
                entity.LanguageId = langId;
                entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                entity.IsSubscribe = EnumConversion.ToString(EnumBool.Yes);
                entity.Name = data.Name;
                entity.Email = data.Email;
                entity.CountryId = data.CountryId;
                entity.CreatedOn = MethodFactory.ArabianTimeNow();
                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                _context.XsiNewsletterSubscribers.Add(entity);

                if (_context.SaveChanges() > 0)
                {
                    return EnumResultType.Success;
                }
            }
            return EnumResultType.Failed;
        }

        EnumResultType UnSubscribe(NewsLetterSubscriberDTO data)
        {
            var newsLetterSubscriber =
            _context.XsiNewsletterSubscribers.Where(i => i.Email == data.Email).FirstOrDefault();
            if (newsLetterSubscriber != null)
            {
                XsiNewsletterSubscribers entity = new XsiNewsletterSubscribers();
                entity = _context.XsiNewsletterSubscribers.Where(i => i.ItemId == newsLetterSubscriber.ItemId).FirstOrDefault();
                entity.IsSubscribe = EnumConversion.ToString(EnumBool.No);
                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                entity.ModifiedById = newsLetterSubscriber.ItemId;
                if (_context.SaveChanges() > 0)
                {
                    return EnumResultType.Success;
                }
            }
            else
            {
                return EnumResultType.NotFound;
            }

            return EnumResultType.Failed;
        }
        EnumResultType SubscribeAr(NewsLetterSubscriberDTO data)
        {
            XsiNewsletterSubscribers entity = new XsiNewsletterSubscribers();
            List<XsiNewsletterSubscribers> newsLetterSubscriberList = _context.XsiNewsletterSubscribers.Where(i => i.Email == data.Email).ToList();
            if (newsLetterSubscriberList.Count > 0)
            {
                entity = _context.XsiNewsletterSubscribers.Where(i => i.ItemId == newsLetterSubscriberList[0].ItemId).FirstOrDefault();
                if (newsLetterSubscriberList[0].IsSubscribe != null)
                {
                    if (newsLetterSubscriberList[0].IsSubscribe != EnumConversion.ToString(EnumBool.Yes))
                    {
                        entity.ModifiedById = newsLetterSubscriberList[0].ItemId;
                        entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                        entity.IsSubscribe = EnumConversion.ToString(EnumBool.Yes);
                        if (_context.SaveChanges() > 0)
                            XsiResult = EnumResultType.Success;
                        return XsiResult;
                    }
                    else return EnumResultType.AlreadyExists;
                }
                else
                {
                    entity.ModifiedById = newsLetterSubscriberList[0].ItemId;
                    entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                    entity.IsSubscribe = EnumConversion.ToString(EnumBool.Yes);
                    if (_context.SaveChanges() > 0)
                        XsiResult = EnumResultType.Success;
                    return XsiResult;
                }
            }
            else
            {
                entity.LanguageId = 2;
                entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes);
                entity.IsSubscribe = EnumConversion.ToString(EnumBool.Yes);
                entity.NameAr = data.Name;
                entity.Email = data.Email;
                entity.CountryId = data.CountryId;
                entity.CreatedOn = MethodFactory.ArabianTimeNow();
                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                _context.XsiNewsletterSubscribers.Add(entity);

                if (_context.SaveChanges() > 0)
                {
                    return EnumResultType.Success;
                }
            }
            return EnumResultType.Failed;
        }
    }
}

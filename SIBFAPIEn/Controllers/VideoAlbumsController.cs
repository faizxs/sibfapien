﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class VideoAlbumsController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;
        public VideoAlbumsController(IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }
        // GET: api/VideoAlbum
        [HttpGet]
        [Route("{pageNumber}/{pageSize}/{categoryId}/{strSearch}")]
        public async Task<ActionResult<dynamic>> GetXsiVideoAlbum(int? pageNumber = 1, int? pageSize = 15, long? categoryId = -1, string strSearch = "")
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                string strpath = _appCustomSettings.UploadsCMSPath;
                var predicate = PredicateBuilder.True<XsiVideoAlbum>();

                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                if (categoryId != -1)
                {
                    List<long> catIds = _context.XsiVideoCategory.Where(i => i.ItemId == categoryId).Select(i => i.ItemId).ToList();
                    predicate = predicate.And(i => catIds.Contains(i.VideoCategoryId.Value));
                }

                if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                    predicate = predicate.And(i => i.Dated != null && i.Dated.Value.Year.ToString() == strSearch);

                var kount = await _context.XsiVideoAlbum.AsQueryable().Where(predicate).CountAsync();

                var List = await _context.XsiVideoAlbum.AsQueryable().Where(predicate)
                                     .OrderByDescending(o => o.Dated).ThenByDescending(o => o.ItemId).Select(i => new VideoAlbumDTO()
                                     {
                                         ItemId = i.ItemId,
                                         CategoryId = i.VideoCategoryId,
                                         CategoryTitle = i.VideoCategory.Title,
                                         Title = i.Title,
                                         Description = i.Description.Length > 140 ? i.Description.Substring(0, 140) + "..." : i.Description,
                                         StrDated = i.Dated == null ? string.Empty : i.Dated.Value.ToString("dd MMM yyyy"),
                                         Year = i.Dated == null ? string.Empty : i.Dated.Value.Year.ToString(),
                                         SortIndex = i.SortIndex,
                                         Count = _context.XsiVideo.Where(p => p.VideoAlbumId == i.ItemId && p.IsActive == "Y").Count(),
                                         Thumbnail = strpath + "/VideoGallery/Thumbnailnew/" + GetThumbnail(i.ItemId),
                                         // Thumbnail = _appCustomSettings.UploadsPath + "/VideoGallery/Thumbnailnew/" + GetThumbnail(i.ItemId),
                                         CreatedOn = i.CreatedOn
                                     }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                //foreach (var album in List)
                //{
                //    string str = GetThumbnail(album.ItemId);
                //    album.Thumbnail = strpath + "/VideoGallery/" + str;
                //}
                VideoAlbumPageDTO albumPageDTO = new VideoAlbumPageDTO();
                albumPageDTO.AlbumDTO = List;
                albumPageDTO.TopAlbumItemDTO = List.FirstOrDefault();

                var dto = new { albumPageDTO, TotalCount = kount };
                return dto;
            }
            else
            {
                string strpath = _appCustomSettings.UploadsCMSPath;
                var predicate = PredicateBuilder.True<XsiVideoAlbum>();

                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                if (categoryId != -1)
                {
                    List<long> catIds = _context.XsiVideoCategory.Where(i => i.ItemId == categoryId).Select(i => i.ItemId).ToList();
                    predicate = predicate.And(i => catIds.Contains(i.VideoCategoryId.Value));
                }

                if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                    predicate = predicate.And(i => i.DatedAr != null && i.DatedAr.Value.Year.ToString() == strSearch);

                var kount = await _context.XsiVideoAlbum.AsQueryable().Where(predicate).CountAsync();

                var List = await _context.XsiVideoAlbum.AsQueryable().Where(predicate)
                                     .OrderByDescending(o => o.Dated).ThenByDescending(o => o.ItemId).Select(i => new VideoAlbumDTO()
                                     {
                                         ItemId = i.ItemId,
                                         CategoryId = i.VideoCategoryId,
                                         CategoryTitle = i.VideoCategory.TitleAr,
                                         Title = i.TitleAr,
                                         Description = i.DescriptionAr.Length > 140 ? i.DescriptionAr.Substring(0, 140) + "..." : i.DescriptionAr,
                                         StrDated = i.DatedAr == null ? string.Empty : i.DatedAr.Value.ToString("dd MMM yyyy"),
                                         Year = i.DatedAr == null ? string.Empty : i.DatedAr.Value.Year.ToString(),
                                         SortIndex = i.SortIndex,
                                         Count = _context.XsiVideo.Where(p => p.VideoAlbumId == i.ItemId && p.IsActiveAr == "Y").Count(),
                                         Thumbnail = strpath + "/VideoGallery/Thumbnailnew/" + GetThumbnailAr(i.ItemId),
                                         // Thumbnail = _appCustomSettings.UploadsPath + "/VideoGallery/Thumbnailnew/" + GetThumbnail(i.ItemId),
                                         CreatedOn = i.CreatedOn
                                     }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                //foreach (var album in List)
                //{
                //    string str = GetThumbnailAr(album.ItemId);
                //    album.Thumbnail = strpath + "/VideoGallery/" + str;
                //}
                VideoAlbumPageDTO albumPageDTO = new VideoAlbumPageDTO();
                albumPageDTO.AlbumDTO = List;
                albumPageDTO.TopAlbumItemDTO = List.FirstOrDefault();

                var dto = new { albumPageDTO, TotalCount = kount };
                return dto;
            }
        }


        // GET: api/VideoAlbum/5
        [HttpGet("{id}")]
        public async Task<ActionResult<VideoAlbumDTO>> GetXsiVideoAlbum(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                string strpath = _appCustomSettings.UploadsPath;
                var album = await _context.XsiVideoAlbum.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes))
                    .Select(i => new VideoAlbumDTO()
                    {
                        ItemId = i.ItemId,
                        CategoryId = i.VideoCategoryId,
                        CategoryTitle = i.VideoCategory.Title,
                        Title = i.Title,
                        Description = i.Description,
                        StrDated = i.Dated == null ? string.Empty : i.Dated.Value.ToString("dd MMM yyyy"),
                        Year = i.Dated == null ? string.Empty : i.Dated.Value.Year.ToString(),
                        SortIndex = i.SortIndex,
                        Count = _context.XsiVideo.Where(p => p.VideoAlbumId == i.ItemId && p.IsActive == "Y").Count(),
                        // Thumbnail = _appCustomSettings.UploadsPath + "/VideoGallery/Thumbnailnew/" + GetThumbnail(i.ItemId),
                        CreatedOn = i.CreatedOn
                    })
                    .FirstOrDefaultAsync();

                if (album == null)
                {
                    return NotFound();
                }
                else
                {


                    string str = GetThumbnail(album.ItemId);
                    album.Thumbnail = strpath + "VideoGallery/" + str;
                }

                return album;
            }
            else
            {
                string strpath = _appCustomSettings.UploadsPath;
                var album = await _context.XsiVideoAlbum.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes))
                    .Select(i => new VideoAlbumDTO()
                    {
                        ItemId = i.ItemId,
                        CategoryId = i.VideoCategoryId,
                        CategoryTitle = i.VideoCategory.TitleAr,
                        Title = i.TitleAr,
                        Description = i.DescriptionAr,
                        StrDated = i.DatedAr == null ? string.Empty : i.DatedAr.Value.ToString("dd MMM yyyy"),
                        Year = i.DatedAr == null ? string.Empty : i.DatedAr.Value.Year.ToString(),
                        SortIndex = i.SortIndex,
                        Count = _context.XsiVideo.Where(p => p.VideoAlbumId == i.ItemId && p.IsActiveAr == "Y").Count(),
                        // Thumbnail = _appCustomSettings.UploadsPath + "/VideoGallery/Thumbnailnew/" + GetThumbnail(i.ItemId),
                        CreatedOn = i.CreatedOn
                    })
                    .FirstOrDefaultAsync();

                if (album == null)
                {
                    return NotFound();
                }
                else
                {


                    string str = GetThumbnailAr(album.ItemId);
                    album.Thumbnail = strpath + "VideoGallery/" + str;
                }

                return album;
            }
        }


        // GET: api/VideoAlbum/GetYears
        [HttpGet("GetYears")]
        public async Task<ActionResult<IEnumerable<YearDTO>>> GetYears()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiVideoAlbum>();

                predicate = predicate.And(i => i.Dated != null);
                predicate = predicate.And(i => i.IsActive == "Y");
                var PressReleaseYear = _context.XsiVideoAlbum.AsQueryable().Where(predicate)
                    .GroupBy(g => g.Dated.Value.Year)
                     .OrderByDescending(o => o.Key).Select(i => new YearDTO() { Id = i.Key, Year = i.Key })
                    .ToListAsync();

                return await PressReleaseYear;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiVideoAlbum>();

                predicate = predicate.And(i => i.DatedAr != null);
                predicate = predicate.And(i => i.IsActiveAr == "Y");
                var PressReleaseYear = _context.XsiVideoAlbum.AsQueryable().Where(predicate)
                    .GroupBy(g => g.DatedAr.Value.Year)
                     .OrderByDescending(o => o.Key).Select(i => new YearDTO() { Id = i.Key, Year = i.Key })
                    .ToListAsync();

                return await PressReleaseYear;
            }
        }

        private bool XsiVideoAlbumExists(long id)
        {
            return _context.XsiVideoAlbum.Any(e => e.ItemId == id);
        }

        string GetThumbnail(long albumId)
        {
            string strimage = "book-image.jpg";

            var entity = _context.XsiVideo.Where(p => p.Thumbnail != null && p.IsAlbumCover == "Y" && p.IsActive == "Y" && p.VideoAlbumId == albumId).OrderByDescending(i => i.CreatedOn).FirstOrDefault();
            if (entity != null && entity.Thumbnail != null && !string.IsNullOrEmpty(entity.Thumbnail))
                strimage = entity.Thumbnail;
            else
            {
                entity = _context.XsiVideo.Where(p => p.Thumbnail != null && p.IsActive == "Y" && p.VideoAlbumId == albumId).OrderByDescending(i => i.CreatedOn).FirstOrDefault();
                if (entity != null && entity.Thumbnail != null && !string.IsNullOrEmpty(entity.Thumbnail))
                    strimage = entity.Thumbnail;
            }

            return strimage;
        }
        string GetThumbnailAr(long albumId)
        {
            string strimage = "book-image.jpg";

            var entity = _context.XsiVideo.Where(p => p.ThumbnailAr != null && p.IsAlbumCoverAr == "Y" && p.IsActiveAr == "Y" && p.VideoAlbumId == albumId).OrderByDescending(i => i.CreatedOn).FirstOrDefault();
            if (entity != null && entity.ThumbnailAr != null && !string.IsNullOrEmpty(entity.ThumbnailAr))
                strimage = entity.ThumbnailAr;
            else
            {
                entity = _context.XsiVideo.Where(p => p.ThumbnailAr != null && p.IsActiveAr == "Y" && p.VideoAlbumId == albumId).OrderByDescending(i => i.CreatedOn).FirstOrDefault();
                if (entity != null && entity.ThumbnailAr != null && !string.IsNullOrEmpty(entity.ThumbnailAr))
                    strimage = entity.ThumbnailAr;
            }

            return strimage;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Contracts;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class HamzatWasslController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;

        public HamzatWasslController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
        }

        // GET: api/HamzatWassl
        [HttpGet]
        [Route("{pageNumber}/{pageSize}/{strSearch}")]
        public async Task<ActionResult<dynamic>> GetXsiHamzatWasl(int? pageNumber = 1, int? pageSize = 15, string strSearch = "")
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    if (LangId == 1)
                    {
                        var predicate = PredicateBuilder.True<XsiHamzatWasl>();

                        predicate = predicate.And(i => i.WebsiteId != 2);
                        predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                        if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                            predicate = predicate.And(i => i.Dated != null && i.Dated.Value.Year.ToString() == strSearch);

                        var List = await _context.XsiHamzatWasl.AsQueryable().Where(predicate).OrderBy(i => i.ItemId).AsNoTracking().Select(i => new
                        {
                            ItemId = i.ItemId,
                            Title = i.Title,
                            IssueNumber = i.IssueNumber,
                            Url = _appCustomSettings.UploadsPath + "/PressRelease/" + i.FileName
                        }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                        var count = _context.XsiHamzatWasl.Where(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes)).AsNoTracking().ToList()
                            .Count;

                        var dto = new { List, TotalCount = count };
                        return dto;
                    }
                    else
                    {
                        var predicate = PredicateBuilder.True<XsiHamzatWasl>();

                        predicate = predicate.And(i => i.WebsiteId != 2);
                        predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                        if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                            predicate = predicate.And(i => i.DatedAr != null && i.DatedAr.Value.Year.ToString() == strSearch);

                        var List = await _context.XsiHamzatWasl.AsQueryable().Where(predicate).OrderBy(i => i.ItemId).AsNoTracking().Select(i => new
                        {
                            ItemId = i.ItemId,
                            Title = i.TitleAr,
                            IssueNumber = i.IssueNumber,
                            Url = _appCustomSettings.UploadsPath + "/PressRelease/" + i.FileNameAr
                        }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                        var count = _context.XsiHamzatWasl.Where(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).AsNoTracking().ToList()
                            .Count;

                        var dto = new { List, TotalCount = count };
                        return dto;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiHamzatWasl (Hamzat wasl) action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        // GET: api/HamzatWassl/5
        [HttpGet("{id}")]
        public async Task<ActionResult<dynamic>> GetXsiHamzatWasl(long id)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    if (LangId == 1)
                    {
                        var xsiItem = await _context.XsiHamzatWasl.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes)).AsNoTracking().FirstOrDefaultAsync();

                        if (xsiItem == null)
                        {
                            return NotFound();
                        }
                        var itemDTO = new
                        {
                            ItemId = xsiItem.ItemId,
                            Title = xsiItem.Title,
                            Date = xsiItem.Dated == null ? string.Empty : xsiItem.Dated.Value.ToString("dd MMMM yyyy"),
                            IssueNumber = xsiItem.IssueNumber,
                            Url = _appCustomSettings.UploadsPath + "/PressRelease/" + xsiItem.FileName
                        };
                        return itemDTO;
                    }
                    else
                    {
                        var xsiItem = await _context.XsiHamzatWasl.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).AsNoTracking().FirstOrDefaultAsync();

                        if (xsiItem == null)
                        {
                            return NotFound();
                        }
                        var itemDTO = new
                        {
                            ItemId = xsiItem.ItemId,
                            Title = xsiItem.TitleAr,
                            Date = xsiItem.DatedAr == null ? string.Empty : xsiItem.DatedAr.Value.ToString("dd MMMM yyyy"),
                            IssueNumber = xsiItem.IssueNumber,
                            Url = _appCustomSettings.UploadsPath + "/PressRelease/" + xsiItem.FileNameAr
                        };
                        return itemDTO;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiHamzatWasl by id (Hamzat wasl) action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        // GET: api/HamzatWassl/GetYears
        [HttpGet("GetYears")]
        public async Task<ActionResult<IEnumerable<YearDTO>>> GetYears()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiHamzatWasl>();

                    predicate = predicate.And(i => i.Dated != null);
                    predicate = predicate.And(i => i.WebsiteId != 2);
                    predicate = predicate.And(i => i.IsActive == "Y");
                    var PressReleaseYear = _context.XsiHamzatWasl.AsQueryable().Where(predicate)
                        .GroupBy(g => g.Dated.Value.Year)
                         .OrderByDescending(o => o.Key).Select(i => new YearDTO() { Id = i.Key, Year = i.Key })
                        .ToListAsync();

                    return await PressReleaseYear;
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiHamzatWasl>();

                    predicate = predicate.And(i => i.DatedAr != null);
                    predicate = predicate.And(i => i.WebsiteId != 2);
                    predicate = predicate.And(i => i.IsActiveAr == "Y");
                    var PressReleaseYear = _context.XsiHamzatWasl.AsQueryable().Where(predicate)
                        .GroupBy(g => g.DatedAr.Value.Year)
                         .OrderByDescending(o => o.Key).Select(i => new YearDTO() { Id = i.Key, Year = i.Key })
                        .ToListAsync();

                    return await PressReleaseYear;
                }
            }
        }
    }
}

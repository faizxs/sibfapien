﻿using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SIBFAPIEn.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class EmailCategoryController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public EmailCategoryController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/EmailCategory
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<CategoryDTO>>> GetXsiEmailCategory(long id)
        {
            long websiteID = Convert.ToInt64(EnumWebsiteId.SCRF);
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                long categoryTpe = Convert.ToInt64(EnumEmailCategory.ContactUs);
                var predicate = PredicateBuilder.True<XsiEmailCategory>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.WebsiteId != websiteID);

                predicate = predicate.And(i => i.CategoryId == id);

                var List = _context.XsiEmailCategory.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new CategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title,
                }).ToList();

                CategoryDTO category = List.Where(p => p.Title == "Other").FirstOrDefault();
                if (category != null)
                {
                    List.Remove(category);
                    List.Add(category);
                }
                return await List.ToAsyncEnumerable().ToList();
            }
            else
            {
                long categoryTpe = Convert.ToInt64(EnumEmailCategory.ContactUs);
                var predicate = PredicateBuilder.True<XsiEmailCategory>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.WebsiteId != websiteID);

                predicate = predicate.And(i => i.CategoryId == id);

                var List = _context.XsiEmailCategory.AsQueryable().Where(predicate).OrderBy(x => x.ItemId).Select(x => new CategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.TitleAr,
                }).ToList();

                CategoryDTO category = List.Where(p => p.Title == "أخرى").FirstOrDefault();
                if (category != null)
                {
                    List.Remove(category);
                    List.Add(category);
                }
                return await List.ToAsyncEnumerable().ToList();
            }
        }

        // GET: api/EmailCategory/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<CategoryDTO>> GetXsiEmailCategory(long id)
        //{
        //    var xsiItem = await _context.XsiEmailCategory.FindAsync(id);

        //    if (xsiItem == null)
        //    {
        //        return NotFound();
        //    }

        //    CategoryDTO itemDTO = new CategoryDTO()
        //    {
        //        ItemId = xsiItem.ItemId,
        //        Title = xsiItem.Title,
        //    };
        //    return itemDTO;
        //}

        private bool XsiEmailCategoryExists(long id)
        {
            return _context.XsiEmailCategory.Any(e => e.ItemId == id);
        }
    }
}

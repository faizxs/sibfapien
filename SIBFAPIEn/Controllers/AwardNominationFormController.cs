﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using SIBFAPIEn.Utility;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Routing;
using Contracts;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using Xsi.ServicesLayer;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Authorization;
namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AwardNominationFormController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private ILoggerManager _logger;
        private IMemoryCache _cache;
        EnumResultType XsiResult { get; set; }

        private readonly IEmailSender _emailSender;
        private readonly sibfnewdbContext _context;
        public AwardNominationFormController(IEmailSender emailSender, IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IMemoryCache memoryCache, sibfnewdbContext context)
        {
            _emailSender = emailSender;
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _cache = memoryCache;
            _context = context;
        }

        [HttpGet]
        [Route("IsValidAwardDates/{websiteId}")]
        public ActionResult<dynamic> IsValidAwardDates(long websiteId)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

                string strmessage = string.Empty;
                var dto = MethodFactory.IsValidAwardDates(websiteId, LangId, _cache);
                if (LangId == 1)
                    strmessage = "Registration is closed.";
                else
                    strmessage = "Registration is closed.";
                var dto1 = new { Isvalid = dto, strmessage };
                return Ok(dto1);
        }

        [HttpGet]
        [Route("IsValidTurjumanAwardDates/{websiteId}")]
        public ActionResult<dynamic> IsValidTurjumanAwardDates(long websiteId)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);

                string strmessage = string.Empty;
                var dto = MethodFactory.IsValidTurjumanAwardDates(websiteId, LangId, _cache);
                if (LangId == 1)
                    strmessage = "Registration is closed.";
                else
                    strmessage = "Registration is closed.";

                var dto1 = new { Isvalid = dto, strmessage };
                return Ok(dto1);
        }

        // POST: api/AwardNominationForm
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> PostAwardNominationFormBook(AwardNominationFormDTO data)
        {
            try
            {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    string strmessage = string.Empty;
                    bool isvalid = true;
                    string pdf = string.Empty;
                    XsiAwardsNominationForms entity = new XsiAwardsNominationForms();
                    if (data.hdnTempData != null && data.hdnTempData != "0" && data.hdnTempData != "" &&
                        data.hdnTempData != "-1")
                    {
                        entity = _context.XsiAwardsNominationForms.Where(i => i.ItemId == Convert.ToInt64(data.hdnTempData))
                            .FirstOrDefault();
                        if (entity != default(XsiAwardsNominationForms))
                        {
                            entity.ItemId = Convert.ToInt64(data.hdnTempData);
                        }
                    }

                    entity.AwardId = data.AwardId;

                    if (data.SubAwardId != null && data.SubAwardId != 0 && data.SubAwardId != -1)
                        entity.SubAwardId = data.SubAwardId;

                    var exhibition = MethodFactory.GetActiveExhibitionNew(Convert.ToInt64(EnumWebsiteId.SIBF), _cache);
                    entity.ExhibitionId = exhibition.ExhibitionId;

                    if (data.CountryId != null && data.CountryId != -1 && data.CountryId != 0)
                        entity.CountryId = data.CountryId;
                    if (data.CityId != null && data.CityId != -1 && data.CityId != 0)
                    {
                        entity.CityId = data.CityId;

                        entity.OtherCity = data.OtherCity;
                    }

                    entity.AuthorName = data.AuthorName;
                    entity.AuthorNumber = data.AuthorISD + "$" + data.AuthorSTD + "$" + data.AuthorPhone;
                    if (data.AuthorNationalityId != null && data.AuthorNationalityId != -1 && data.AuthorNationalityId != 0)
                        entity.AuthorNationalityId = data.AuthorNationalityId;
                    entity.AuthorEmail = data.AuthorEmail;

                    entity.Date = Convert.ToDateTime(data.Date);
                    entity.Email = data.Email;
                    entity.EstablishedYear = data.EstablishedYear;
                    entity.Phone = data.ISD + "$" + data.STD + "$" + data.Phone;
                    entity.Fax = data.FaxISD + "$" + data.FaxSTD + "$" + data.Fax;
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    entity.Status = EnumConversion.ToString(EnumAwardNominationStatus.New);
                    entity.Isbn = data.ISBN;
                    entity.MailAddress = data.MailAddress;
                    entity.NominationType = data.NominationType;
                    entity.OwnerOfPubHouse = data.OwnerOfPubHouse;
                    entity.Pobox = data.POBox;

                    entity.PublishedYear = data.PublishedYear;
                    entity.Publisher = data.Publisher;
                    entity.PublisherNumber = data.PublisherISD + "$" + data.PublisherSTD + "$" + data.PublisherPhone;
                    if (data.PublisherCountryId != null && data.PublisherCountryId != -1 && data.PublisherCountryId != 0)
                        entity.PublisherCountryId = data.PublisherCountryId;
                    entity.PublisherEmail = data.PublisherEmail;
                    entity.PublishersSpecialization = data.PublishersSpecialization;

                    entity.Title = data.Title;
                    entity.PostalCode = data.PostalCode;
                    entity.FirstParticipatedYear = data.FirstParticipatedYear;
                    entity.NoOfPublications = data.NoOfPublications;
                    entity.NoOfPublicationsInCurrentYear = data.NoOfPublicationsInCurrentYear;
                    entity.GeneralManagerOfPubHouse = data.GeneralManagerOfPubHouse;

                    entity.CreatedOn = MethodFactory.ArabianTimeNow();
                    entity.ModifiedOn = MethodFactory.ArabianTimeNow();

                    if (data.FileBase64 != null && data.FileBase64.Length > 0)
                    {
                        string fileName = string.Empty;
                        byte[] imageBytes;
                        if (data.FileBase64.Contains("data:"))
                        {
                            var strInfo = data.FileBase64.Split(",")[0];
                            imageBytes = Convert.FromBase64String(data.FileBase64.Split(',')[1]);
                        }
                        else
                            imageBytes = Convert.FromBase64String(data.FileBase64);

                        fileName = string.Format("{0}_{1}_{2}{3}", data.AuthorName, LangId, MethodFactory.GetRandomNumber(), "." + data.DocExtension);
                        System.IO.File.WriteAllBytes(
                            Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                         "\\AwardFormPassportCopy\\") + fileName, imageBytes);

                        entity.PassportCopy = fileName;
                    }

                    if (Convert.ToInt64(data.hdnTempData) <= 0)
                        _context.XsiAwardsNominationForms.Add(entity);

                    if (_context.SaveChanges() > 0)
                    {
                        long participantid = entity.ItemId;
                        data.hdnTempData = participantid.ToString();

                        XsiResult = EnumResultType.Success;
                        pdf = DownloadAwardsRegistrationPDF(entity, LangId);

                        if (entity.AwardId == 9)
                        {
                            //if (LangId == 1)
                            //{
                            //    strmessage = @" <p>We would like to thank you for participating in Sharjah International Book Fair Awards 2024. In order to complete your participation, it is required to submit the following:</p>
                            //    <p>1- List of publications<br />
                            //       2- A document of the participation form that has been submitted on the website.<br />
                            //      3- A passport copy of publisher house owner.
                            //    </p>
                            //    <p>Kindly send the requirements to the following E-mail: <a href='mailto: awards@sibf.com' target='_blank'>awards@sibf.com</a></p>";
                            //}
                            //else
                            //{
                            //    strmessage = @"<p>نشكركم على مشاركتكم في جوائز معرض الشارقة الدولي للكتاب 2024</p>
                            //  <p>لاستكمال المشاركة، يرجى تقديم ما يلي:</p>
                            //  <p>1- قائمة بإصدارات الدار<br>2-	وثيقة نموذج المشاركة المقدم على الموقع الإلكتروني.<br>3-	صورة عن جواز السفر لصاحب الدار</p>
                            //  <p>الرجاء إرسال الوثائق المطلوبة إلى البريد الالكتروني: <a href='awards@sibf.com' target='_blank'>awards@sibf.com</a></p>";
                            //}
                            if (LangId == 1)
                            {
                                strmessage = @" <p>We would like to thank you for participating in Sharjah International Book Fair Awards 2024. In order to complete your participation, it is required to submit the following:</p>
                                <p>1- Three hard copies of the participating book (note that these copies will not be sent back)<br />
                                   2- A document of the participation form that has been submitted on the website.<br />
                                </p>
                                <p>Kindly send the requirements to the following address:</p>
                                <p>
                                   <b>Sharjah Book Authority (Cultural Awards Department)</b> <br /> 
                                    Awards Adminstration<br />
                                    Sheikh Mohammed bin Zayed Road - Al Zahia - Sharjah,<br />
                                    P.O. Box:73111<br />

                                    Tel: +971 56 603 6623,<br />
                                    E-mail: <a href='mailto: logistics@sharjahbookfair.com' target='_blank'>logistics@sharjahbookfair.com</a></p>";
                            }
                            else
                            {
                                strmessage = @"<p>نشكركم على مشاركتكم في جوائز معرض الشارقة الدولي للكتاب 2024.</p>
                              <p>لاستكمال المشاركة، يرجى تقديم ما يلي :</p>
<p>1-	ثلاث نسخ ورقية من الكتاب المشارك (يرجى ملاحظة أنه لن يتم إرجاع هذه النسخ)	<br>2-	وثيقة نموذج المشاركة المقدم على الموقع الإلكتروني</p>
<p>الرجاء إرسال الوثائق المطلوبة إلى العنوان التالي:</p>

<p><b>هيئة الشارقة للكتاب) قسم الجوائز الثقافية)</b>
<br>طريق الشيخ محمد بن زايد - الزاهية - الشارقة 

<br>  ص.ب : 73111 
<br> <span dir='ltr'>+971 56 603 6623</span>
<br> بريد الكتروني:<a href='logistics@sharjahbookfair.com' target='_blank'>logistics@sharjahbookfair.com</a></p>";
                            }
                        }
                        else
                        {
                            if (LangId == 1)
                            {
                                strmessage = @" <p>We would like to thank you for participating in Sharjah International Book Fair Awards 2024. In order to complete your participation, it is required to submit the following:</p>
                                <p>1- Three hard copies of the participating book (note that these copies will not be sent back)<br />
                                   2- A document of the participation form that has been submitted on the website.<br />
                                </p>
                                <p>Kindly send the requirements to the following address:</p>
                                <p>
                                   <b>Sharjah Book Authority (Cultural Awards Department)</b> <br /> 
                                    Awards Adminstration<br />
                                    Sheikh Mohammed bin Zayed Road - Al Zahia - Sharjah,<br />
                                    P.O. Box:73111<br />

                                    Tel: +971 56 603 6623,<br />
                                    E-mail: <a href='mailto: logistics@sharjahbookfair.com' target='_blank'>logistics@sharjahbookfair.com</a></p>";
                            }
                            else
                            {
                                strmessage = @"<p>نشكركم على مشاركتكم في جوائز معرض الشارقة الدولي للكتاب 2024.</p>
                              <p>لاستكمال المشاركة، يرجى تقديم ما يلي :</p>
<p>1-	ثلاث نسخ ورقية من الكتاب المشارك (يرجى ملاحظة أنه لن يتم إرجاع هذه النسخ)	<br>2-	وثيقة نموذج المشاركة المقدم على الموقع الإلكتروني</p>
<p>الرجاء إرسال الوثائق المطلوبة إلى العنوان التالي:</p>

<p><b>هيئة الشارقة للكتاب) قسم الجوائز الثقافية)</b>
<br>طريق الشيخ محمد بن زايد - الزاهية - الشارقة 

<br>  ص.ب : 73111 
<br> <span dir='ltr'>+971 56 603 6623</span>
<br> بريد الكتروني:<a href='logistics@sharjahbookfair.com' target='_blank'>logistics@sharjahbookfair.com</a></p>";
                            }
                        }

                        if (!string.IsNullOrEmpty(pdf))
                        {
                            pdf = Path.GetFileName(pdf);
                            isvalid = true;

                            var dto1 = new { Isvalid = isvalid, PDFFileName = _appCustomSettings.UploadsPath + "/AwardFormPassportCopy/downloadpdf/" + pdf, strmessage, data.hdnTempData };
                            SendEmailToUserForPublisherRecognitionAward(participantid, LangId);
                            return Ok(dto1);
                        }
                        else
                        {
                            isvalid = true;
                            //  strmessage = "Form submitted. Pdf Couldn't be generated. Try again later.";
                            var dto = new { Isvalid = isvalid, strmessage, data.hdnTempData };
                            SendEmailToUserForPublisherRecognitionAward(participantid, LangId);
                            return Ok(dto);
                        }
                    }

                    isvalid = false;
                    strmessage = "Error while submitting the form";
                    var dto3 = new { Isvalid = isvalid, strmessage, data.hdnTempData };
                    return Ok(dto3);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwardNominationForm action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        // POST: api/AwardNominationFormBookForTurjumanAward
        [HttpPost]
        [Route("TurjumanAward")]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> PostAwardNominationFormBookForTurjumanAward(TurjumanAwardNominationFormDTO data)
        {
            try
            {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    string strmessage = string.Empty;
                    bool isvalid = true;
                    string pdf = string.Empty;
                    XsiSharjahAwardForTranslation entity = new XsiSharjahAwardForTranslation();
                    if (data.hdnTempData != "-1")
                    {
                        entity = _context.XsiSharjahAwardForTranslation.Where(i => i.ItemId == Convert.ToInt64(data.hdnTempData)).FirstOrDefault();
                        if (entity != default(XsiSharjahAwardForTranslation))
                        {
                            entity.ItemId = Convert.ToInt64(data.hdnTempData);
                        }
                    }

                    entity.AwardId = Convert.ToInt64(data.AwardId);

                    // entity.ExhibitionId = MethodFactory.GetActiveExhibition(Convert.ToInt64(EnumWebsiteId.SIBF));

                    var exhibition = MethodFactory.GetActiveExhibitionNew(Convert.ToInt64(EnumWebsiteId.SIBF), _cache);
                    entity.ExhibitionId = exhibition.ExhibitionId;


                    if (data.CountryId != null && data.CountryId != -1 && data.CountryId != 0)
                        entity.CountryId = data.CountryId;

                    if (data.TranslationLanguageId != null && data.TranslationLanguageId != -1 && data.TranslationLanguageId != 0)
                        entity.TranslationLanguageId = data.TranslationLanguageId;

                    entity.TranslatedBookName = data.TranslatedBookName;
                    entity.PublishedYearOfFirstTranslation = data.PublishedYearOfFirstTranslation;
                    entity.TranslatorName = data.TranslatorName;
                    entity.BookNameArabic = data.BookNameArabic;
                    entity.PublishedYearOfFirstArabicTranslation = data.PublishedYearOfFirstArabicTranslation;
                    entity.AuthorName = data.AuthorName;
                    entity.ForeignPublishName = data.ForeignPublishName;

                    entity.CorrespondenceAddress = data.CorrespondenceAddress;
                    entity.Pobox = data.POBox;

                    entity.Phone = data.ISD + "$" + data.STD + "$" + data.Phone;
                    entity.Fax = data.FaxISD + "$" + data.FaxSTD + "$" + data.Fax;
                    entity.Email = data.Email;

                    entity.ArabPublishHouseName = data.ArabPublishHouseName;

                    if (data.ArabPublishHouseCountryId != null && data.ArabPublishHouseCountryId != -1 && data.ArabPublishHouseCountryId != 0)
                        entity.ArabPublishHouseCountryId = data.ArabPublishHouseCountryId;

                    entity.ArabPublishHouseNameAdress = data.ArabPublishHouseNameAdress;
                    entity.ArabPublishHousePobox = data.ArabPublishHousePOBox;

                    entity.ArabPublishHousePhone = data.ArabISD + "$" + data.ArabSTD + "$" + data.ArabPublishHousePhone;
                    entity.ArabPublishHouseFax = data.ArabPublishHouseFaxISD + "$" + data.ArabPublishHouseFaxSTD + "$" + data.ArabPublishHouseFax;
                    entity.ArabPublishHouseEmail = data.Email;
                    entity.ArabPublishHouseNominationBody = data.ArabPublishHouseNominationBody;

                    entity.Date = Convert.ToDateTime(data.Date);
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    entity.Status = EnumConversion.ToString(EnumAwardNominationStatus.New);

                    entity.CreatedOn = MethodFactory.ArabianTimeNow();
                    entity.ModifiedOn = MethodFactory.ArabianTimeNow();

                    //if (data.FileBase64 != null && data.FileBase64.Length > 0)
                    //{
                    //    string fileName = string.Empty;
                    //    byte[] imageBytes;
                    //    if (data.FileBase64.Contains("data:"))
                    //    {
                    //        var strInfo = data.FileBase64.Split(",")[0];
                    //        imageBytes = Convert.FromBase64String(data.FileBase64.Split(',')[1]);
                    //    }
                    //    else
                    //        imageBytes = Convert.FromBase64String(data.FileBase64);

                    //    fileName = string.Format("{0}_{1}_{2}{3}", data.AuthorName, LangId, MethodFactory.GetRandomNumber(), "." + data.DocExtension);
                    //    System.IO.File.WriteAllBytes(
                    //        Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                    //                     "\\AwardFormPassportCopy\\") + fileName, imageBytes);

                    //    entity.PassportCopy = fileName;
                    //}

                    if (Convert.ToInt64(data.hdnTempData) <= 0)
                        _context.XsiSharjahAwardForTranslation.Add(entity);

                    if (_context.SaveChanges() > 0)
                    {
                        long participantid = entity.ItemId;
                        data.hdnTempData = participantid.ToString();

                        XsiResult = EnumResultType.Success;
                        pdf = DownloadAwardsRegistrationForTurjumanPDF(entity, LangId);

                        //                        if (LangId == 1)
                        //                        {
                        //                            strmessage = @" <p>We would like to thank you for participating in Sharjah Translation Awards ( Turjuman) <br>In order to complete your participation, it is required to submit the following:</p>
                        //                                <p>1- Three hard copies of the participated book (note that these copies will not be sent back)<br />
                        //                                   2- A document of the participation form that has been submitted on the website.<br />
                        //                                  3- A passport copy.
                        //                                </p>
                        //                                <p>Kindly send the requirements to the following address:</p>
                        //                                <p>
                        //                                   <b>Sharjah Book Authority</b> <br /> 
                        //                                    Awards Adminstration<br />
                        //                                    Sheikh Mohammed bin Zayed Road,<br />
                        //                                    Al Zahya Area, Sharjah, UAE<br />

                        //                                    Tel: 00971-6-5140138, 00971-6-5140127,<br />
                        //                                    Fax: 00971-6-5140111
                        //                                    E-mail: <a href='mailto: turjuman@sibf.com' target='_blank'>turjuman@sibf.com</a></p>";
                        //                        }
                        //                        else
                        //                        {
                        //                            strmessage = @"<p>نشكركم على مشاركتكم في جائزة الشارقة للترجمة (ترجمان)</p>
                        //                              <p>لاستكمال المشاركة، يرجى تقديم ما يلي:</p>

                        //<p>1-	ثلاث نسخ ورقية من الكتاب المشارك (يرجى ملاحظة أنه لن يتم إرجاع هذه النسخ)<br>2-	وثيقة نموذج المشاركة المقدم على الموقع الإلكتروني.<br>3-	صورة عن جواز السفر</p>
                        //<p>الرجاء إرسال الوثائق المطلوبة إلى العنوان التالي:</p>
                        //<p><b>هيئة الشارقة للكتاب</b><br>قسم الجوائز الثقافية <br>شارع الشيخ محمد بن زايد، <br>منطقة الزاهية، الشارقة، الإمارات العربية المتحدة <br>  هاتف:  <span dir='ltr'>00971-6-5140138</span><span dir='ltr'>00971-6-5140127</span><br>  فاكس :  <span dir='ltr'>00971-6-5140111</span><br> بريد الكتروني:<a href='turjuman@sibf.com' target='_blank'>turjuman@sibf.com</a></p>";
                        //                        }

                        if (LangId == 1)
                        {
                            strmessage = @" <p>We would like to thank you for participating in Sharjah International Book Fair Awards 2024. In order to complete your participation, it is required to submit the following:</p>
                                <p>1- Three hard copies of the participating book (note that these copies will not be sent back)<br />
                                   2- A document of the participation form that has been submitted on the website.<br />
                                </p>
                                <p>Kindly send the requirements to the following address:</p>
                                <p>
                                   <b>Sharjah Book Authority (Cultural Awards Department)</b> <br /> 
                                    Awards Adminstration<br />
                                    Sheikh Mohammed bin Zayed Road - Al Zahia - Sharjah,<br />
                                    P.O. Box:73111<br />

                                    Tel: +971 56 603 6623,<br />
                                    E-mail: <a href='mailto: logistics@sharjahbookfair.com' target='_blank'>logistics@sharjahbookfair.com</a></p>";
                        }
                        else
                        {
                            strmessage = @"<p>نشكركم على مشاركتكم في جوائز معرض الشارقة الدولي للكتاب 2024.</p>
                              <p>لاستكمال المشاركة، يرجى تقديم ما يلي :</p>
<p>1-	ثلاث نسخ ورقية من الكتاب المشارك (يرجى ملاحظة أنه لن يتم إرجاع هذه النسخ)	<br>2-	وثيقة نموذج المشاركة المقدم على الموقع الإلكتروني</p>
<p>الرجاء إرسال الوثائق المطلوبة إلى العنوان التالي:</p>

<p><b>هيئة الشارقة للكتاب) قسم الجوائز الثقافية)</b>
<br>طريق الشيخ محمد بن زايد - الزاهية - الشارقة 

<br>  ص.ب : 73111 
<br> <span dir='ltr'>+971 56 603 6623</span>
<br> بريد الكتروني:<a href='logistics@sharjahbookfair.com' target='_blank'>logistics@sharjahbookfair.com</a></p>";
                        }

                        if (!string.IsNullOrEmpty(pdf))
                        {
                            pdf = Path.GetFileName(pdf);
                            isvalid = true;

                            var dto1 = new { Isvalid = isvalid, PDFFileName = _appCustomSettings.UploadsPath + "/AwardFormPassportCopy/downloadpdf/" + pdf, strmessage, data.hdnTempData };
                            SendEmailToUserForTurjumanAward(participantid, LangId);
                            return Ok(dto1);
                        }
                        else
                        {
                            isvalid = true;
                            var dto = new { Isvalid = isvalid, strmessage, data.hdnTempData };
                            SendEmailToUserForTurjumanAward(participantid, LangId);
                            return Ok(dto);
                        }
                    }
                    isvalid = false;
                    strmessage = "Error while submitting the form";
                    var dto3 = new { Isvalid = isvalid, strmessage, data.hdnTempData };
                    return Ok(dto3);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside PostAwardNominationFormBookForTurjumanAward action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        void SendEmailToUserForPublisherRecognitionAward(long itemid, long langid)
        {
                var entity = _context.XsiAwardsNominationForms.Where(i => i.ItemId == itemid).FirstOrDefault();
                if (entity != default(XsiAwardsNominationForms))
                {
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {
                        HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                        #region Email to Author
                        StringBuilder body = new StringBuilder();
                        string StrEmailContentBody = string.Empty;
                        string StrEmailContentSubject = string.Empty;
                        #region Email Content
                        var EmailContent = EmailContentService.GetEmailContentByItemId(20198);
                        if (EmailContent != null)
                        {
                            if (langid == 1)
                            {
                                if (EmailContent.Body != null)
                                    StrEmailContentBody = EmailContent.Body;

                                if (EmailContent.Subject != null)
                                    StrEmailContentSubject = EmailContent.Subject;
                            }
                            else
                            {
                                if (EmailContent.BodyAr != null)
                                    StrEmailContentBody = EmailContent.BodyAr;
                                if (EmailContent.SubjectAr != null)
                                    StrEmailContentSubject = EmailContent.SubjectAr;
                            }
                        }
                        #endregion
                        #endregion

                        body.Append(htmlContentFactory.BindEmailContentForPublisherRecognitionAward(langid, StrEmailContentBody, entity, _appCustomSettings.ServerAddressNew, _appCustomSettings));
                        _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, entity.Email, StrEmailContentSubject, body.ToString());
                    }
                }
        }
        void SendEmailToUserForTurjumanAward(long itemid, long langid)
        {
                var entity = _context.XsiSharjahAwardForTranslation.Where(i => i.ItemId == itemid).FirstOrDefault();
                if (entity != default(XsiSharjahAwardForTranslation))
                {
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {
                        HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                        #region Email to Author
                        StringBuilder body = new StringBuilder();
                        string StrEmailContentBody = string.Empty;
                        string StrEmailContentSubject = string.Empty;
                        #region Email Content
                        var EmailContent = EmailContentService.GetEmailContentByItemId(20199);
                        if (EmailContent != null)
                        {
                            if (langid == 1)
                            {
                                if (EmailContent.Body != null)
                                    StrEmailContentBody = EmailContent.Body;

                                if (EmailContent.Subject != null)
                                    StrEmailContentSubject = EmailContent.Subject;
                            }
                            else
                            {
                                if (EmailContent.BodyAr != null)
                                    StrEmailContentBody = EmailContent.BodyAr;
                                if (EmailContent.SubjectAr != null)
                                    StrEmailContentSubject = EmailContent.SubjectAr;
                            }
                        }
                        #endregion
                        #endregion

                        body.Append(htmlContentFactory.BindEmailContentForAwardsSIBFTurjuman(langid, StrEmailContentBody, entity, _appCustomSettings.ServerAddressNew, _appCustomSettings));
                        _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, entity.Email, StrEmailContentSubject, body.ToString());
                    }
                }
        }
        internal string DownloadAwardsRegistrationPDF(XsiAwardsNominationForms entity, long langId)
        {
            string fileName = string.Empty;
            #region PDF
            StringBuilder applicationform = new StringBuilder();
            if (entity != default(XsiAwardsNominationForms))
            {
                var XsiCurrentLanguageId = 1;

                if (entity.AwardId == 1)
                    applicationform.Append(System.IO.File.ReadAllText(Path.Combine(_appCustomSettings.ApplicationPhysicalPath + "\\Content\\EmailMedia\\AwardFormEmiratiBook.html")));
                else if (entity.AwardId == 3)
                    applicationform.Append(System.IO.File.ReadAllText(Path.Combine(_appCustomSettings.ApplicationPhysicalPath + "\\Content\\EmailMedia\\AwardFormEmiratiBook.html")));

                else if (entity.AwardId == 5)
                    applicationform.Append(System.IO.File.ReadAllText(Path.Combine(_appCustomSettings.ApplicationPhysicalPath + "\\Content\\EmailMedia\\AwardFormInternationalBook.html")));

                else if (entity.AwardId == 7)
                    applicationform.Append(System.IO.File.ReadAllText(Path.Combine(_appCustomSettings.ApplicationPhysicalPath + "\\Content\\EmailMedia\\AwardFormInternationalBook.html")));

                else if (entity.AwardId == 9)
                    applicationform.Append(System.IO.File.ReadAllText(Path.Combine(_appCustomSettings.ApplicationPhysicalPath + "\\Content\\EmailMedia\\AwardFormPublisherRecognition.html")));

                //  applicationform.Append(System.IO.File.ReadAllText(HPath.Combine("/Content/EmailMedia/AwardFormEmiratiBookAr.html")));

                #region Replace Placeholders
                applicationform.Replace("$$ServerAddress$$", _appCustomSettings.ServerAddressNew);
                applicationform.Replace("$$ServerAddressCMS$$", _appCustomSettings.ServerAddressCMS);
                string str = MethodFactory.GetExhibitionDetails(Convert.ToInt64(EnumWebsiteId.SIBF));
                string[] strArray = str.Split(',');

                if (strArray[0] == "10" || strArray[0] == "11" || strArray[0] == "12" || strArray[0] == "13")
                    applicationform.Replace("$$sf$$", "th");
                else
                    applicationform.Replace("$$sf$$", MethodFactory.GetSuffix(Convert.ToInt32(strArray[0]) % 10));

                //applicationform.Replace("$$sf$$", GetSuffix(Convert.ToInt32(strArray[0])));
                //applicationform.Replace("$$exhibitiondate$$", strArray[1]);

                applicationform.Replace("$$exhibitionno$$", strArray[0].ToString());
                applicationform.Replace("$$exhibitiondate$$", strArray[1].ToString());
                applicationform.Replace("$$exhyear$$", strArray[2]);

                applicationform.Replace("$$award$$", MethodFactory.GetAwardOrSubAwardTitle(-1, XsiCurrentLanguageId, entity.AwardId.Value));// MethodFactory.GetAwardTitle(entity.AwardGroupId.Value, XsiCurrentLanguageId));
                applicationform.Replace("$$subaward$$", MethodFactory.GetAwardOrSubAwardTitle(entity.SubAwardId ?? -1, XsiCurrentLanguageId, entity.AwardId.Value));
                applicationform.Replace("$$titleofthenominatedbook$$", entity.Title);
                applicationform.Replace("$$yearpublished$$", entity.PublishedYear);
                applicationform.Replace("$$isbn$$", entity.Isbn);

                if (entity.NominationType == "A")
                    applicationform.Replace("$$nominationtype$$", "Personal");
                else if (entity.NominationType == "B")
                    applicationform.Replace("$$nominationtype$$", "Publisher");
                else
                    applicationform.Replace("$$nominationtype$$", string.Empty);

                applicationform.Replace("$$authorname$$", entity.AuthorName);

                if (!string.IsNullOrEmpty(entity.AuthorNumber))
                    applicationform.Replace("$$authornumber$$", entity.AuthorNumber.Replace("$", ""));
                else
                    applicationform.Replace("$$publishernumber$$", string.Empty);

                applicationform.Replace("$$authoremail$$", entity.AuthorEmail);

                if (entity.AuthorNationalityId != null && entity.AuthorNationalityId != -1 && entity.AuthorNationalityId != 0)
                    applicationform.Replace("$$authornationality$$", MethodFactory.GetCountryName(entity.AuthorNationalityId ?? -1, langId));
                else
                    applicationform.Replace("$$authornationality$$", string.Empty);

                applicationform.Replace("$$publisher$$", entity.Publisher);

                if (!string.IsNullOrEmpty(entity.PublisherNumber))
                    applicationform.Replace("$$publishernumber$$", entity.PublisherNumber.Replace("$", ""));
                else
                    applicationform.Replace("$$publishernumber$$", string.Empty);

                applicationform.Replace("$$publisheremail$$", entity.PublisherEmail);

                if (entity.PublisherCountryId != null && entity.PublisherCountryId != -1 && entity.PublisherCountryId != 0)
                    applicationform.Replace("$$publishercountry$$", MethodFactory.GetCountryName(entity.PublisherCountryId ?? -1, langId));
                else
                    applicationform.Replace("$$publishercountry$$", string.Empty);

                if (entity.CountryId != null && entity.CountryId != -1 && entity.CountryId != 0)
                    applicationform.Replace("$$country$$", MethodFactory.GetCountryName(entity.CountryId ?? -1, langId));
                else
                    applicationform.Replace("$$country$$", string.Empty);

                if (!string.IsNullOrEmpty(entity.OtherCity))
                    applicationform.Replace("$$city$$", entity.OtherCity);
                else
                {
                    if (entity.CityId != null && entity.CityId != -1 && entity.CityId != 0)
                    {
                        applicationform.Replace("$$city$$",
                            MethodFactory.GetCityName(entity.CityId.Value, XsiCurrentLanguageId));
                    }
                    else
                        applicationform.Replace("$$city$$", string.Empty);
                }

                applicationform.Replace("$$publisherspecialization$$", entity.PublishersSpecialization);
                applicationform.Replace("$$yearestablished$$", entity.EstablishedYear);

                applicationform.Replace("$$yearoffirstparticipationinsibf$$", entity.FirstParticipatedYear);

                if (entity.NoOfPublications != null)
                    applicationform.Replace("$$noofpublications$$", entity.NoOfPublications.ToString());
                else
                    applicationform.Replace("$$noofpublications$$", string.Empty);

                if (entity.NoOfPublications != null)
                    applicationform.Replace("$$noofpublicationsincurrentyear$$", entity.NoOfPublicationsInCurrentYear.ToString());
                else
                    applicationform.Replace("$$noofpublicationsincurrentyear$$", string.Empty);

                applicationform.Replace("$$pobox$$", entity.Pobox);
                applicationform.Replace("$$address$$", entity.MailAddress);

                if (entity.Phone != null)
                    applicationform.Replace("$$phone$$", entity.Phone.Replace("$", ""));
                else
                    applicationform.Replace("$$phone$$", string.Empty);

                if (entity.Fax != null)
                    applicationform.Replace("$$fax$$", entity.Fax.Replace("$", ""));
                else
                    applicationform.Replace("$$fax$$", string.Empty);
                applicationform.Replace("$$email$$", entity.Email);
                applicationform.Replace("$$publisherhouse$$", entity.OwnerOfPubHouse);
                applicationform.Replace("$$generalmanagerofpublisherhouse$$", entity.GeneralManagerOfPubHouse);
                if (entity.Date != null)
                    applicationform.Replace("$$date$$", entity.Date.Value.ToString("dd MMM yyy"));
                else
                    applicationform.Replace("$$date$$", string.Empty);

                applicationform.Replace("$$passportcopy$$", entity.PassportCopy);
                #endregion
                fileName = string.Format("{0}_{1}", XsiCurrentLanguageId, "AwardsRegistration_" + MethodFactory.GetRandomNumber());
                Process PDFProcess = GetPDFForAwardRegistrationApplication(applicationform.ToString(), fileName);
                while (true)
                {
                    if (PDFProcess.HasExited)
                    {
                        fileName = Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                                "\\AwardFormPassportCopy\\") + "downloadpdf/" + fileName + ".pdf";
                        break;
                    }
                }
            }
            return fileName;
            #endregion
        }
        internal string DownloadAwardsRegistrationForTurjumanPDF(XsiSharjahAwardForTranslation entity, long langId)
        {
            string fileName = string.Empty;
            #region PDF
            StringBuilder applicationform = new StringBuilder();
            if (entity != default(XsiSharjahAwardForTranslation))
            {
                var XsiCurrentLanguageId = 1;

                applicationform.Append(System.IO.File.ReadAllText(Path.Combine(_appCustomSettings.ApplicationPhysicalPath + "\\Content\\EmailMedia\\AwardTurjuman.html")));

                //  applicationform.Append(System.IO.File.ReadAllText(HPath.Combine("/Content/EmailMedia/AwardTurjumanAr.html")));

                #region Replace Placeholders
                applicationform.Replace("$$ServerAddress$$", _appCustomSettings.ServerAddressNew);
                applicationform.Replace("$$ServerAddressCMS$$", _appCustomSettings.ServerAddressCMS);
                string str = MethodFactory.GetExhibitionDetails(Convert.ToInt64(EnumWebsiteId.SIBF));
                string[] strArray = str.Split(',');

                if (strArray[0] == "10" || strArray[0] == "11" || strArray[0] == "12" || strArray[0] == "13")
                    applicationform.Replace("$$sf$$", "th");
                else
                    applicationform.Replace("$$sf$$", MethodFactory.GetSuffix(Convert.ToInt32(strArray[0]) % 10));

                //applicationform.Replace("$$sf$$", GetSuffix(Convert.ToInt32(strArray[0])));
                //applicationform.Replace("$$exhibitiondate$$", strArray[1]);

                applicationform.Replace("$$exhibitionno$$", strArray[0].ToString());
                applicationform.Replace("$$exhibitiondate$$", strArray[1].ToString());
                applicationform.Replace("$$exhyear$$", strArray[2]);

                applicationform.Replace("$$award$$", MethodFactory.GetAwardOrSubAwardTitle(-1, XsiCurrentLanguageId, entity.AwardId.Value));// MethodFactory.GetAwardTitle(entity.AwardGroupId.Value, XsiCurrentLanguageId));

                applicationform.Replace("$$nameoftranslatedbook$$", entity.TranslatedBookName);
                applicationform.Replace("$$publicationyearoffirsttranslation$$", entity.PublishedYearOfFirstTranslation);
                applicationform.Replace("$$translatorname$$", entity.TranslatorName);
                applicationform.Replace("$$nameofarabicbook$$", entity.BookNameArabic);
                applicationform.Replace("$$publicationyearofarabicfirstedition$$", entity.PublishedYearOfFirstArabicTranslation);
                applicationform.Replace("$$authorname$$", entity.AuthorName);
                applicationform.Replace("$$nameofforeignpublishinghouse$$", entity.ForeignPublishName);
                applicationform.Replace("$$correspondenceaddress$$", entity.CorrespondenceAddress);
                applicationform.Replace("$$pobox$$", entity.Pobox);
                applicationform.Replace("$$email$$", entity.Email);

                if (entity.Phone != null)
                    applicationform.Replace("$$phone$$", entity.Phone.Replace("$", ""));
                else
                    applicationform.Replace("$$phone$$", string.Empty);

                if (entity.Fax != null)
                    applicationform.Replace("$$fax$$", entity.Fax.Replace("$", ""));
                else
                    applicationform.Replace("$$fax$$", string.Empty);

                applicationform.Replace("$$nameofarabpublishhouse$$", entity.ArabPublishHouseName);

                if (entity.ArabPublishHouseCountryId != null && entity.ArabPublishHouseCountryId != -1 && entity.ArabPublishHouseCountryId != 0)
                    applicationform.Replace("$$arabcountry$$", MethodFactory.GetCountryName(entity.ArabPublishHouseCountryId ?? -1, langId));
                else
                    applicationform.Replace("$$arabcountry$$", string.Empty);

                applicationform.Replace("$$arabcorrespondenceaddress$$", entity.ArabPublishHouseNameAdress);
                applicationform.Replace("$$arabpobox$$", entity.ArabPublishHousePobox);

                if (entity.ArabPublishHousePhone != null)
                    applicationform.Replace("$$arabphone$$", entity.ArabPublishHousePhone.Replace("$", ""));
                else
                    applicationform.Replace("$$arabphone$$", string.Empty);

                if (entity.ArabPublishHouseFax != null)
                    applicationform.Replace("$$arabfax$$", entity.ArabPublishHouseFax.Replace("$", ""));
                else
                    applicationform.Replace("$$arabfax$$", string.Empty);

                applicationform.Replace("$$arabemail$$", entity.ArabPublishHouseEmail);
                applicationform.Replace("$$arabnominationbody$$", entity.ArabPublishHouseNominationBody);

                if (entity.Date != null)
                    applicationform.Replace("$$date$$", entity.Date.Value.ToString("dd MMM yyy"));
                else
                    applicationform.Replace("$$date$$", string.Empty);

                if (entity.TranslationLanguageId != null)
                    applicationform.Replace("$$translatedlanguage$$", MethodFactory.GetExhibitionLanuageTitle(entity.TranslationLanguageId.Value, langId));
                else
                    applicationform.Replace("$$translatedlanguage$$", string.Empty);

                //applicationform.Replace("$$country$$", GetCountryTitle(entity.CountryId, langId));
                //applicationform.Replace("$$passportcopy$$", entity.PassportCopy);
                #endregion
                fileName = string.Format("{0}_{1}", XsiCurrentLanguageId, "AwardsRegistrationTurjuman_" + MethodFactory.GetRandomNumber());
                Process PDFProcess = GetPDFForAwardRegistrationApplication(applicationform.ToString(), fileName);
                while (true)
                {
                    if (PDFProcess.HasExited)
                    {
                        fileName = Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                                "\\AwardFormPassportCopy\\") + "downloadpdf/" + fileName + ".pdf";
                        break;
                    }
                }
            }
            return fileName;
            #endregion
        }
        Process GetPDFForAwardRegistrationApplication(string html, string strFileName)
        {
            string HtmltopdfPath = _appCustomSettings.HtmltopdfPath; // WebConfigurationManager.AppSettings["HtmltopdfPath"];
            string pdfPath = Path.Combine(_appCustomSettings.ApplicationPhysicalPath + "Content\\uploads\\AwardFormPassportCopy/");
            html = @"<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'><head><title>Application Form</title></head>
                <meta http-equiv='content-Type' content='text/html; charset=utf-8' /><body>" + html + "</body></html>";
            Process PDFProcess = new Process();
            string pdfPath1 = Path.Combine(_appCustomSettings.ApplicationPhysicalPath +
                                           "\\Content\\uploads\\AwardFormPassportCopy\\downloadpdf\\" + strFileName +
                                           ".html");
            System.IO.File.WriteAllText(pdfPath1, html);
            Thread.Sleep(4000);
            if (System.IO.File.Exists(pdfPath1))
            {
                PDFProcess = Process.Start(new ProcessStartInfo
                {
                    FileName = @HtmltopdfPath,
                    Arguments = String.Format("{0} {1}", pdfPath1, pdfPath + "/downloadpdf/" + strFileName + ".pdf"),
                    UseShellExecute = false
                });

                while (true)
                {
                    if (PDFProcess.HasExited)
                    {
                        pdfPath = Path.Combine(_appCustomSettings.ApplicationPhysicalPath +
                                               "\\Content\\uploads\\AwardFormPassportCopy\\downloadpdf\\" + strFileName +
                                               ".pdf");
                        if (System.IO.File.Exists(pdfPath))
                        {
                            if (System.IO.File.Exists(pdfPath1))
                                System.IO.File.Delete(pdfPath1);
                        }
                        break;
                    }
                }
            }
            return PDFProcess;
        }

    }
}

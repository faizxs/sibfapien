﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using SIBFAPIEn.Utility;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Account.Manage.Internal;
using Contracts;
using LinqKit;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class GetEmailContentTemplateController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        EnumResultType XsiResult { get; set; }

        private readonly IEmailSender _emailSender;
        public GetEmailContentTemplateController(IEmailSender emailSender, IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger)
        {
            _emailSender = emailSender;
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
        }

        // GET: api/GetExhibitionMemberRegistration
        [HttpGet]
        public ActionResult<dynamic> GetEmailContentTemplate(long languageId, string bodyContent, XsiExhibitionMember entity, string serverAddress, long Wid)
        {
            try
            {
                string scrfURL = _appCustomSettings.ServerAddressSCRF;
                string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
                string emailContent = string.Empty;
                if (languageId == 1)
                {
                    if (Wid == Convert.ToInt64(EnumWebsiteId.SCRF))
                        emailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRF.html";
                    else
                        emailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
                }
                else
                {
                    if (Wid == Convert.ToInt64(EnumWebsiteId.SCRF))
                        emailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRF.html";
                    else
                        emailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
                }
                StringBuilder body = new StringBuilder();

                if (System.IO.File.Exists(emailContent))
                {
                    body.Append(System.IO.File.ReadAllText(emailContent));
                    body.Replace("$$EmailContent$$", bodyContent);
                }
                else
                    body.Append(bodyContent);
                string str = GetExhibitionDetails(Wid);
                string[] strArray = str.Split(',');
                body.Replace("$$exhibitionno$$", strArray[0].ToString());
                body.Replace("$$exhibitiondate$$", strArray[1].ToString());
                body.Replace("$$exhyear$$", strArray[2]);

                string swidth = "width=\"864\"";
                string sreplacewidth = "width=\"100%\"";
                body.Replace(swidth, sreplacewidth);

                body.Replace("$$ServerAddress$$", ServerAddressCMS);
                body.Replace("$$ServerAddress$$", serverAddress);
                if (Wid == Convert.ToInt64(EnumWebsiteId.SCRF))
                    body.Replace("$$ServerAddressSCRF$$", scrfURL);

                if (entity != null)
                {
                    string illustratorname = string.Empty;
                    if (languageId == 1)
                    {
                        if (entity.Firstname != null && entity.LastName != null)
                            illustratorname = entity.Firstname + " " + entity.LastName;
                        else if (entity.Firstname != null)
                            illustratorname = entity.Firstname;
                        else if (entity.LastName != null)
                            illustratorname = entity.LastName;
                    }
                    else
                    {
                        if (entity.FirstNameAr != null && entity.LastNameAr != null)
                            illustratorname = entity.FirstNameAr + " " + entity.LastNameAr;
                        else if (entity.Firstname != null)
                            illustratorname = entity.FirstNameAr;
                        else if (entity.LastName != null)
                            illustratorname = entity.LastNameAr;

                        if (string.IsNullOrEmpty(illustratorname))
                        {
                            if (entity.Firstname != null && entity.LastName != null)
                                illustratorname = entity.Firstname + " " + entity.LastName;
                            else if (entity.Firstname != null)
                                illustratorname = entity.Firstname;
                            else if (entity.LastName != null)
                                illustratorname = entity.LastName;
                        }
                    }
                    body.Replace("$$Name$$", illustratorname);
                }
                return body;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        #region Email Utility
        string GetExhibitionDetails(long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long? exhibitionNumber = -1;
                string strdate = string.Empty;
                string stryear = MethodFactory.ArabianTimeNow().Year.ToString();

                XsiExhibition entity = _context.XsiExhibition.Where(i => i.WebsiteId == websiteId && i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.IsArchive == EnumConversion.ToString(EnumBool.No)).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                string startdaysuffix = string.Empty;
                string enddaysuffix = string.Empty;

                if (entity != default(XsiExhibition))
                {
                    if (entity.ExhibitionNumber != null)
                        exhibitionNumber = entity.ExhibitionNumber;
                    long dayno = entity.StartDate.Value.Date.Day;
                    if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                        startdaysuffix = "th";
                    else
                        startdaysuffix = GetSuffix(entity.StartDate.Value.Date.Day % 10, entity.StartDate.Value);
                    dayno = entity.EndDate.Value.Date.Day;
                    if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                        enddaysuffix = "th";
                    else
                        enddaysuffix = GetSuffix(entity.EndDate.Value.Date.Day % 10, entity.EndDate.Value);

                    if (entity.StartDate.Value.Year == entity.EndDate.Value.Year)
                    {
                        if (entity.StartDate.Value.Month == entity.EndDate.Value.Month)
                            strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.StartDate.Value.ToString("MMM") + " " + entity.StartDate.Value.ToString("yyyy");
                        else
                            strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " + entity.StartDate.Value.ToString("MMM")
                                 + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.EndDate.Value.ToString("MMM")
                                + " " + entity.StartDate.Value.ToString("yyyy");
                    }
                    else
                    {
                        strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " + entity.StartDate.Value.ToString("MMM") + " " + entity.StartDate.Value.ToString("yyyy")
                             + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.EndDate.Value.ToString("MMM")
                            + " " + entity.EndDate.Value.ToString("yyyy");
                    }
                    if (entity.ExhibitionYear != null)
                        stryear = entity.ExhibitionYear.ToString();
                }
                return exhibitionNumber.ToString() + "," + strdate + "," + stryear;
            }
        }
        string GetSuffix(int startday, DateTime dt)
        {
            string daysuffix;
            if (startday != 11 && dt.Date.Day != 11)
            {
                if (startday == 1)
                    daysuffix = "st";
                else if (startday == 2)
                    daysuffix = "nd";
                else if (startday == 3)
                    daysuffix = "rd";
                else
                    daysuffix = "th";
            }
            else
                daysuffix = "th";
            return daysuffix;
        }
        #endregion
    }
}

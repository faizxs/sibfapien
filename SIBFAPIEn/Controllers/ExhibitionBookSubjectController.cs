﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionBookSubjectController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionBookSubjectController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/ExhibitionBookSubject
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryDTO>>> GetXsiExhibitionSubject()
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibitionBookSubject>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionBookSubject.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(x => new CategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.Title,
                }).ToListAsync();

                return await List;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibitionBookSubject>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                var List = _context.XsiExhibitionBookSubject.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(x => new CategoryDTO()
                {
                    ItemId = x.ItemId,
                    Title = x.TitleAr,
                }).ToListAsync();

                return await List;
            }
        }


        // GET: api/ExhibitionBookSubject/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDTO>> GetXsiExhibitionSubject(long id)
        {
            var xsiItem = await _context.XsiExhibitionBookSubject.FindAsync(id);

            if (xsiItem == null)
            {
                return NotFound();
            }
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                CategoryDTO itemDTO = new CategoryDTO()
                {
                    ItemId = xsiItem.ItemId,
                    Title = xsiItem.Title,
                };
                return itemDTO;
            }
            else
            {
                CategoryDTO itemDTO = new CategoryDTO()
                {
                    ItemId = xsiItem.ItemId,
                    Title = xsiItem.TitleAr,
                };
                return itemDTO;
            }
        }

        private bool XsiExhibitionSubjectExists(long id)
        {
            return _context.XsiExhibitionBookSubject.Any(e => e.ItemId == id);
        }
    }
}

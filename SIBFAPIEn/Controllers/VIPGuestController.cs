﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Xsi.ServicesLayer;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class VIPGuestController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public VIPGuestController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        // GET: api/VIPGuest
        [HttpGet]
        [Route("{pageNumber}/{pageSize}/{strSearch}/{strtitle}/{strletter}")]
        public async Task<ActionResult<dynamic>> GetXsiVipGuest(int? pageNumber = 1, int? pageSize = 15, string strSearch = "", string strtitle = "", string strletter = "")
        {
            var List1 = new List<XsiVipguest>();
            try
            {
                VIPGuestService vIPGuestService = new VIPGuestService();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var predicateVipGuest = PredicateBuilder.True<XsiVipguest>();
                var predicateVipGuestWebsite = PredicateBuilder.True<XsiVipguestWebsite>();
                predicateVipGuestWebsite = predicateVipGuestWebsite.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);
                var guestIDs = _context.XsiVipguestWebsite.Where(predicateVipGuestWebsite).Select(i => i.VipguestId).Distinct()
                    .ToList();
                if (guestIDs.Count > 0)
                    predicateVipGuest = predicateVipGuest.And(i => guestIDs.Contains(i.ItemId));
                if (LangId == 1)
                {
                    predicateVipGuest = predicateVipGuest.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    if (!string.IsNullOrEmpty(strletter) && strletter != "-1")
                    {
                        strletter = strletter.ToLower();
                        predicateVipGuest = predicateVipGuest.And(i => i.Name.StartsWith(strletter));
                    }
                    else if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                    {
                        strSearch = strSearch.ToLower();
                        predicateVipGuest = predicateVipGuest.And(i => i.Name.Contains(strSearch, StringComparison.OrdinalIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(strtitle) && strtitle != "-1")
                    {
                        strtitle = strtitle.ToLower();
                        predicateVipGuest = predicateVipGuest.And(i => i.Title.Contains(strtitle, StringComparison.OrdinalIgnoreCase));
                    }

                    var List = await _context.XsiVipguest.Where(predicateVipGuest)
                                         .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new VIPGuestDTO()
                                         {
                                             ItemId = i.ItemId,
                                             GuestTitle = i.Title,
                                             GuestPhoto = _appCustomSettings.UploadsPath + "/VIPGuest/ListThumbnailNew/" + (i.Image ?? "guest.jpg"),
                                             GuestName = i.Name,
                                             //Description = i.Description,
                                             //  URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                                         }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    var totalCount = await _context.XsiVipguest.Where(predicateVipGuest).Select(i => i.ItemId).CountAsync();

                    var dto = new { List, TotalCount = totalCount, Message = "", MessageTypeResponse = "Success" };
                    return Ok(dto);
                }
                else
                {
                    predicateVipGuest = predicateVipGuest.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    if (!string.IsNullOrEmpty(strletter) && strletter != "-1")
                    {
                        //strletter = strletter.ToLower();
                        //predicateVipGuest = predicateVipGuest.And(i => i.NameAr.StartsWith(strletter));

                        var likeExpression = strletter + "%";
                        predicateVipGuest = predicateVipGuest.And(i => EF.Functions.Like(i.NameAr, likeExpression));
                    }
                    else if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                    {
                        strSearch = strSearch.ToLower();
                        predicateVipGuest = predicateVipGuest.And(i => i.NameAr.Contains(strSearch, StringComparison.OrdinalIgnoreCase));
                    }

                    if (!string.IsNullOrEmpty(strtitle) && strtitle != "-1")
                    {
                        strtitle = strtitle.ToLower();
                        predicateVipGuest = predicateVipGuest.And(i => i.TitleAr.Contains(strtitle, StringComparison.OrdinalIgnoreCase));
                    }

                    var List = await _context.XsiVipguest.AsExpandable().Where(predicateVipGuest)
                        .OrderByDescending(o => o.ItemId).Select(i => new VIPGuestDTO()
                        {
                            ItemId = i.ItemId,
                            GuestTitle = i.TitleAr,
                            GuestPhoto = _appCustomSettings.UploadsPath + "/VIPGuest/ListThumbnailNew/" + (i.ImageAr ?? "guest.jpg"),
                            GuestName = i.NameAr,
                            //  Events = "",
                            //Description = i.DescriptionAr,
                            // URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                        }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    var totalCount = await _context.XsiVipguest.AsExpandable().Where(predicateVipGuest).Select(i => i.ItemId).CountAsync();

                    var dto = new { List, TotalCount = totalCount, Message = "", MessageTypeResponse = "Success" };
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiGuest action: {ex.InnerException}");
                return Ok(new { List = new List<VIPGuestDTO>(), TotalCount = 0, Message = "Something went wrong. Please try again later.", MessageTypeResponse = "Error" });
            }
        }

        // GET: api/VIPGuest/5
        [HttpGet("{id}")]
        public async Task<ActionResult<dynamic>> GetXsiGuest(long id)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {

                    var predicate = PredicateBuilder.True<XsiVipguest>();
                    predicate = predicate.And(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    var dto = await _context.XsiVipguest.Where(predicate).Select(i => new VIPGuestDTO()
                    {
                        ItemId = i.ItemId,
                        GuestTitle = i.Title,
                        GuestPhoto = _appCustomSettings.UploadsCMSPath + "/VIPGuest/ListThumbnailNew/" + (i.Image ?? "guest.jpg"),
                        GuestName = i.Name,
                        //  Events = "",
                        //Description = i.Description,
                        // URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                    }).FirstOrDefaultAsync();

                    if (dto == null)
                    {
                        return NotFound();
                    }

                    return Ok(dto);
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiVipguest>();
                    predicate = predicate.And(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    var dto = await _context.XsiVipguest.Where(predicate).Select(i => new VIPGuestDTO()
                    {
                        ItemId = i.ItemId,
                        GuestTitle = i.TitleAr,
                        GuestPhoto = _appCustomSettings.UploadsCMSPath + "/VIPGuest/ListThumbnailNew/" + (i.ImageAr ?? "guest.jpg"),
                        GuestName = i.NameAr,
                        //  Events = "",
                        // Description = i.DescriptionAr,
                        // URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                    }).FirstOrDefaultAsync();

                    if (dto == null)
                    {
                        return Ok(new { dto1 = new VIPGuestDTO(), Message = "No record found", MessageTypeResponse = "Error" });
                    }
                    return Ok(new { dto, Message = "", MessageTypeResponse = "Success" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiGuest action: {ex.InnerException}");
                return Ok(new { dto1 = new VIPGuestDTO(), Message = "Something went wrong please try again later.", MessageTypeResponse = "Error" });
            }
        }

        [HttpGet]
        [Route("homepagevipguest")]
        public async Task<ActionResult<dynamic>> GetXsiHomePageVIPGuest()
        {
            //var homepageorganiser = _context.XsiHomepageOrganiser.Where(a => a.ItemId == 18 && a.IsActive=="Y").Select(a => a.IsActive).FirstOrDefault();
            //if (homepageorganiser != null)
            //{
            //    if (homepageorganiser == "Y")
            //    {
            var List1 = new List<XsiVipguest>();
            try
            {

                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                var predicateVipGuest = PredicateBuilder.True<XsiVipguest>();
                var predicateVipGuestWebsite = PredicateBuilder.True<XsiVipguestWebsite>();

                predicateVipGuestWebsite = predicateVipGuestWebsite.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);
                var guestIDs = _context.XsiVipguestWebsite.Where(predicateVipGuestWebsite).Select(i => i.VipguestId).ToList();
                if (guestIDs.Count > 0)
                    predicateVipGuest = predicateVipGuest.And(i => guestIDs.Contains(i.ItemId));

                if (LangId == 1)
                {
                    predicateVipGuest = predicateVipGuest.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    var List = await _context.XsiVipguest.Where(predicateVipGuest)
                                         .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new VIPGuestDTO()
                                         {
                                             ItemId = i.ItemId,
                                             GuestTitle = i.Title,
                                             GuestPhoto = _appCustomSettings.UploadsPath + "/VIPGuest/ListThumbnailNew/" + (i.Image ?? "guest.jpg"),
                                             GuestName = i.Name,
                                             //Description = i.Description,
                                             // URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                                         }).Take(3).ToListAsync();


                    return Ok(new { List, Message = "", MessageTypeResponse = "Success" });

                }
                else
                {
                    predicateVipGuest = predicateVipGuest.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));



                    var List = await _context.XsiVipguest.AsExpandable().Where(predicateVipGuest)
                        .OrderByDescending(o => o.ItemId).Select(i => new VIPGuestDTO()
                        {
                            ItemId = i.ItemId,
                            GuestTitle = i.TitleAr,
                            GuestPhoto = _appCustomSettings.UploadsPath + "/VIPGuest/ListThumbnailNew/" + (i.ImageAr ?? "guest.jpg"),
                            GuestName = i.NameAr,
                            //  Events = "",
                            //Description = i.DescriptionAr,
                            // URL = "activity?category=-1&subcategory=-1&guestid=" + i.ItemId
                        }).Take(3).ToListAsync();


                    var dto = new { List, Message = "", MessageTypeResponse = "Success" };
                    return Ok(dto);
                }


            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiGuest action: {ex.InnerException}");
                return Ok(new { List = new List<VIPGuestDTO>(), Message = "Something went wrong. Please try again later", MessageTypeResponse = "Error" });
            }
            //    }

            //}
            //return Ok(new { dto1 = new VIPGuestDTO(), Message = "Please Publish in HomePageOrganiser.", MessageTypeResponse = "Error" });
        }
    }
}

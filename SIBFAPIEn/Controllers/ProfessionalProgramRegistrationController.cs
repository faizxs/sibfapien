﻿using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xsi.ServicesLayer;
using Contracts;
namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProfessionalProgramRegistrationController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        EnumResultType XsiResult { get; set; }

        private readonly IEmailSender _emailSender;
        public ProfessionalProgramRegistrationController(IEmailSender emailSender, ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings)
        {
            _emailSender = emailSender;
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
        }

        // GET: api/GetProfessionalProgramRegistration
        [HttpGet]
        [Route("{memberid}/{websiteid}")]
        public ActionResult<dynamic> GetProfessionalProgramRegistration(long memberid, long websiteid = 1)
        {
            ExhibitionProfessionalProgramRegistrationDTO model = new ExhibitionProfessionalProgramRegistrationDTO();
            try
            {
                int dtyear = MethodFactory.ArabianTimeNow().Year;
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long memberId = User.Identity.GetID();
                if (memberid == memberId)
                {
                    model.MemberId = memberid;
                    var exhibitionPPRegistration = VerifyProfessionalProgramRegistration(memberid, LangId);
                    if (exhibitionPPRegistration != default(XsiExhibitionProfessionalProgramRegistration))
                    {
                        //model.Status = exhibitionPPRegistration.Status;
                        model.MemberId = memberid;
                        model.ItemId = exhibitionPPRegistration.ItemId;
                        model.ReturnUrl = "";
                        model.StepId = exhibitionPPRegistration.StepId;
                        model.IsHopitalityPackage = exhibitionPPRegistration.IsHopitalityPackage == EnumConversion.ToString(EnumBool.Yes) ? "Y" : "N";
                        //model.ProfessionalProgramId = exhibitionPPRegistration.ProfessionalProgramId;
                        if (exhibitionPPRegistration.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                        {
                            model.MessageTypeResponse = "Success";
                            model.ReturnUrl = "en/findprofessional";  ///" + Convert.ToInt64(EnumWebsiteId.SIBF);
                            return Ok(model);
                        }
                        else if (exhibitionPPRegistration.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Pending))
                        {
                            model.Message = @"<h1>Thank you for your application to the SIBF Publishers Conference</h1><p>Thank you very much for your application to the Sharjah International Book Fair(SIBF) Publishers Conference " + dtyear + ". The SIBF management team will now review your application and be in touch soon.</p>";
                            model.MessageTypeResponse = "Success";
                            model.Success = true;
                            return Ok(model);
                        }
                        else if (exhibitionPPRegistration.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Rejected))
                        {
                            model.Reject = true;
                            model.Message = "We regret that we could not confirm your participation for this year. Better luck next time";
                            model.MessageTypeResponse = "Success";
                            return Ok(model);
                        }
                        else
                        {
                            #region Manage UI
                            model = LoadContent(exhibitionPPRegistration, model);
                            if (IsExhibitionActive(websiteid))
                            {
                                model.MessageTypeResponse = "Success";
                                return Ok(model);
                            }
                            else
                            {
                                model.StepId = 1;
                                model.MessageTypeResponse = "Error";
                                model.Message = "No exhibition available";
                                return Ok(model);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        var entity = GetLatestProfessionalProgramRegistration();
                        #region Manage UI
                        model = LoadContent(exhibitionPPRegistration, model);
                        DateTime dtNow = MethodFactory.ArabianTimeNow();
                        if (IsExhibitionActive(websiteid))
                        {
                            if (entity != null)
                            {
                                if (dtNow >= entity.RegistrationStartDate && dtNow <= entity.RegistrationEndDate)
                                {
                                    model.MessageTypeResponse = "Success";
                                    return Ok(model);
                                }
                                else
                                {
                                    model.StepId = 1;
                                    model.MessageTypeResponse = "Error";
                                    model.Message = "Registration date expired.";
                                    return Ok(model);
                                }
                            }
                            else
                            {
                                model.StepId = 1;
                                model.MessageTypeResponse = "Error";
                                model.Message = "No exhibition available";
                                return Ok(model);
                            }

                        }
                        else
                        {
                            model.StepId = 1;
                            model.MessageTypeResponse = "Error";
                            model.Message = "No exhibition available";
                            return Ok(model);
                        }
                        #endregion
                    }
                }
                else
                {
                    model.StepId = 1;
                    model.MessageTypeResponse = "Error";
                    model.Message = "Un authorized access.";
                    return Ok(model);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetProfessionalProgramRegistration action: {ex.InnerException}");
                model.MessageTypeResponse = "Error";
                model.Message = "Something went wrong. Please try again later.";
                return Ok(model);
            }
        }

        [HttpPost]
        [Route("Registration")]
        public ActionResult<dynamic> PostProfessionalProgramRegistration(PPRegistrationStepTwo itemDto)
        {
            MessageDTO data = new MessageDTO();
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                itemDto.LanguageId = LangId;
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long memberid = User.Identity.GetID();
                    if (memberid == itemDto.MemberId)
                    {
                        string strMessage = string.Empty;
                        if (itemDto.ItemId == -1 || itemDto.ItemId == 0)
                        {
                            itemDto.StepId = 3;
                            long profProgramId = GetProfessionalProgramId();
                            if (!IsAlreadyRegistered(memberid, profProgramId))
                            {
                                itemDto.ProfessionalProgramId = profProgramId;
                                itemDto = SaveProfessionalProgramRegistrationStepTwo(itemDto);
                                if (itemDto.ItemId > 0)
                                {
                                    itemDto.MessageTypeResponse = "Success";
                                    itemDto.Message = "Registered successfully.";
                                    return Ok(itemDto);
                                }
                                else
                                {
                                    itemDto.MessageTypeResponse = "Error";
                                    itemDto.Message = "Insert operation failed.";
                                    return Ok(itemDto);
                                }
                            }
                            else
                            {
                                itemDto.MessageTypeResponse = "Error";
                                itemDto.Message = "User already registered.";
                                return Ok(itemDto);
                            }
                        }
                        else
                        {
                            if (UpdateProfessionalStepTwo(itemDto))
                            {
                                itemDto.MessageTypeResponse = "Success";
                                itemDto.Message = "Updated successfully..";
                                return Ok(itemDto);
                            }
                            else
                            {
                                itemDto.MessageTypeResponse = "Error";
                                itemDto.Message = "Update Failure.";
                                return Ok(itemDto);
                            }
                        }
                    }
                    return Unauthorized();
                    //itemDto.Message = "Unauthorized access";
                    //return Ok(itemDto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside PostProfessionalProgramRegistration action: {ex.InnerException}");
                itemDto.MessageTypeResponse = "Error";
                itemDto.Message = "Something went wrong. Please try again later. ";// + ex.ToString();
                return Ok(itemDto);
            }
        }
        [HttpPost]
        [Route("Registration/StepThree")]
        public ActionResult<dynamic> PPRegistrationStepThree(PPRegistrationStepThree itemDto)
        {
            PPMessageDTO msgDto = new PPMessageDTO();
            try
            {
                int dtyear = MethodFactory.ArabianTimeNow().Year;
                long memberid = User.Identity.GetID();
                if (memberid == itemDto.MemberId)
                {
                    if (itemDto.TradeType == "A" || itemDto.TradeType == "S")
                        msgDto.StepId = 4;
                    else
                        msgDto.StepId = 3;
                    msgDto.MemberId = memberid;
                    if (itemDto.ItemId > 0)
                        msgDto.RegistrationId = itemDto.ItemId;
                    if (UpdateStepThree(itemDto))
                    {
                        if (msgDto.StepId == 3)
                        {
                            SendThankYouEmailToUser(memberid);
                            NotifyAdminForApproval(memberid);
                            msgDto.Message = @"<h1>Thank you for your application to the SIBF Publishers Conference</h1><p>Thank you very much for your application to the Sharjah International Book Fair(SIBF) Publishers Conference " + dtyear + ". The SIBF management team will now review your application and be in touch soon.</p>";
                        }
                        msgDto.MessageTypeResponse = "Success";
                    }
                    else
                    {
                        msgDto.MessageTypeResponse = "Error";
                        msgDto.Message = "Update Failure";
                    }
                    return Ok(msgDto);
                }

                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside PPRegistrationStepThree action: {ex.InnerException}");
                msgDto.MessageTypeResponse = "Error";
                msgDto.Message = "Update Failure : ";
                return Ok(msgDto);
            }
        }
        [HttpPost]
        [Route("Registration/StepFour")]
        public ActionResult<dynamic> PPRegistrationStepFour(PPRegistrationStepFour itemDto)
        {
            PPMessageDTO msgDto = new PPMessageDTO();
            try
            {
                int dtyear = MethodFactory.ArabianTimeNow().Year;
                long memberid = User.Identity.GetID();
                if (memberid == itemDto.MemberId)
                {
                    if (UpdateStepFour(itemDto))
                    {
                        SendThankYouEmailToUser(memberid);
                        NotifyAdminForApproval(memberid);
                        msgDto.Message = @"<h1>Thank you for your application to the SIBF Publishers Conference</h1><p>Thank you very much for your application to the Sharjah International Book Fair(SIBF) Publishers Conference " + dtyear + ". The SIBF management team will now review your application and be in touch soon.</p>";
                        msgDto.MessageTypeResponse = "Success";
                        return Ok(msgDto);
                    }
                    else
                    {
                        msgDto.MessageTypeResponse = "Error";
                        msgDto.Message = "Update Failure";
                        return Ok(msgDto);
                    }
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside PPRegistrationStepFour action: {ex.InnerException}");
                msgDto.MessageTypeResponse = "Error";
                msgDto.Message = "Update Failure : ";
                return Ok(msgDto);
            }
        }

        #region Private Method
        private bool IsExhibitionActive(long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var predicate = PredicateBuilder.True<XsiExhibition>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
                predicate = predicate.And(i => i.WebsiteId == websiteId);
                XsiExhibition entity = _context.XsiExhibition.AsQueryable().Where(predicate).OrderByDescending(x => x.ExhibitionId).FirstOrDefault();
                if (entity != null)
                {
                    //ExhibitionGroupId = entity.GroupId.Value;
                    return true;
                }
                return false;
            }
        }
        private XsiExhibitionProfessionalProgramRegistration VerifyProfessionalProgramRegistration(long memberId, long languageId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibitionProfessionalProgramRegistration model = new XsiExhibitionProfessionalProgramRegistration();
                var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgram>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
                XsiExhibitionProfessionalProgram exhibitionPP = _context.XsiExhibitionProfessionalProgram.AsQueryable().Where(predicate).OrderByDescending(x => x.ProfessionalProgramId).FirstOrDefault();
                if (exhibitionPP != null)
                {
                    var predicate1 = PredicateBuilder.True<XsiExhibitionProfessionalProgramRegistration>();
                    predicate1 = predicate1.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate1 = predicate1.And(i => i.MemberId == memberId);
                    predicate1 = predicate1.And(i => i.ProfessionalProgramId == exhibitionPP.ProfessionalProgramId);
                    model = _context.XsiExhibitionProfessionalProgramRegistration.AsQueryable().Where(predicate1).OrderByDescending(x => x.ItemId).FirstOrDefault();
                }
                return model;
            }
        }
        private XsiExhibitionProfessionalProgram GetLatestProfessionalProgramRegistration()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibitionProfessionalProgram model = new XsiExhibitionProfessionalProgram();
                var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgram>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
                XsiExhibitionProfessionalProgram exhibitionPP = _context.XsiExhibitionProfessionalProgram.AsQueryable().Where(predicate).OrderByDescending(x => x.ProfessionalProgramId).FirstOrDefault();
                if (exhibitionPP != null)
                {
                    return exhibitionPP;
                }
                return model;
            }
        }
        private PPRegistrationStepTwo SaveProfessionalProgramRegistrationStepTwo(PPRegistrationStepTwo itemDto)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibitionProfessionalProgramRegistration entity = new XsiExhibitionProfessionalProgramRegistration();
                entity.StepId = itemDto.StepId ?? 2;
                entity.Type = itemDto.Type;
                entity.MemberId = itemDto.MemberId;
                entity.ProfessionalProgramId = GetProfessionalProgramId();
                entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                entity.IsHopitalityPackage = EnumConversion.ToString(EnumBool.No); //to avoid null

                entity.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.New);
                entity.LanguageId = itemDto.LanguageId ?? 1;
                if (itemDto.Title != null && !string.IsNullOrEmpty(itemDto.Title))
                {
                    entity.Title = itemDto.Title;
                }
                entity.FirstName = itemDto.FirstName;
                // model.FirstNameAr = entity.FirstNameAr;
                entity.LastName = itemDto.LastName;
                //model.LastNameAr = entity.LastNameAr;
                entity.Email = itemDto.Email;
                entity.CountryId = itemDto.CountryId;
                entity.CityId = itemDto.CityId;
                entity.OtherCity = itemDto.OtherCity;

                entity.PhoneNumber = itemDto.PhoneISD + "$" + itemDto.PhoneSTD + "$" + itemDto.Phone;
                entity.MobileNumber = itemDto.MobileISD + "$" + itemDto.MobileSTD + "$" + itemDto.Mobile;
                entity.ZipCode = itemDto.ZipCode;

                entity.CompanyName = itemDto.CompanyName;
                //model.CompanyNameAr = entity.CompanyNameAr;
                entity.JobTitle = itemDto.JobTitle;
                //model.JobTitleAr = entity.JobTitleAr;
                entity.WorkProfile = itemDto.WorkProfile;
                //model.WorkProfileAr = entity.WorkProfileAr;
                entity.Website = itemDto.Website;
                entity.SocialLinkUrl = itemDto.SocialLinkUrl;
                entity.IsEditRequest = EnumConversion.ToString(EnumProfessionalProgramEditStatus.NoEdit);
                entity.CreatedOn = MethodFactory.ArabianTimeNow();
                entity.CreatedById = itemDto.MemberId;
                entity.ModifiedById = itemDto.MemberId;
                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                _context.XsiExhibitionProfessionalProgramRegistration.Add(entity);
                if (_context.SaveChanges() > 0)
                {
                    itemDto.ItemId = entity.ItemId;

                    XsiExhibitionProfessionalProgramRegistrationStatusLogs logentity = new XsiExhibitionProfessionalProgramRegistrationStatusLogs();
                    logentity.RegistrationId = itemDto.ItemId;
                    logentity.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.New);
                    logentity.CreatedOn = MethodFactory.ArabianTimeNow();
                    _context.SaveChanges();
                }
                else
                {
                    itemDto.ItemId = 0;
                }
                return itemDto;
            }
        }
        private bool UpdateProfessionalStepTwo(PPRegistrationStepTwo itemDto)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibitionProfessionalProgramRegistration entity = _context.XsiExhibitionProfessionalProgramRegistration.Where(x => x.ItemId == itemDto.ItemId).FirstOrDefault();
                if (entity != null)
                {
                    //if ((itemDto.TradeType == "B" && itemDto.TradeType != "S") || itemDto.StepId == 4)
                    //{
                    //    entity.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Pending);
                    //    itemDto.StepId = 4;
                    //}

                    // entity.ProfessionalProgramId = GetProfessionalProgramId();
                    entity.Type = itemDto.Type;
                    entity.MemberId = itemDto.MemberId;
                    if (itemDto.Title != null && !string.IsNullOrEmpty(itemDto.Title))
                    {
                        entity.Title = itemDto.Title;
                    }
                    entity.FirstName = itemDto.FirstName;
                    entity.LastName = itemDto.LastName;
                    entity.Email = itemDto.Email;
                    entity.CountryId = itemDto.CountryId;
                    entity.CityId = itemDto.CityId;
                    entity.OtherCity = itemDto.OtherCity;

                    entity.PhoneNumber = itemDto.PhoneISD + "$" + itemDto.PhoneSTD + "$" + itemDto.Phone;
                    entity.MobileNumber = itemDto.MobileISD + "$" + itemDto.MobileSTD + "$" + itemDto.Mobile;
                    entity.ZipCode = itemDto.ZipCode;
                    entity.CompanyName = itemDto.CompanyName;
                    entity.JobTitle = itemDto.JobTitle;
                    entity.WorkProfile = itemDto.WorkProfile;
                    entity.Website = itemDto.Website;
                    entity.SocialLinkUrl = itemDto.SocialLinkUrl;
                    entity.ModifiedById = itemDto.MemberId;
                    entity.ModifiedOn = MethodFactory.ArabianTimeNow();

                    _context.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        private bool UpdateStepThree(PPRegistrationStepThree itemDto)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                bool isstatuschange = false;
                XsiExhibitionProfessionalProgramRegistration entity = _context.XsiExhibitionProfessionalProgramRegistration.Where(x => x.ItemId == itemDto.ItemId).FirstOrDefault();
                if (entity != null)
                {
                    if (itemDto.TradeType == "A" || itemDto.TradeType == "S")  //Both (A), Seller(S)
                        entity.StepId = 4;
                    else
                    {
                        if (!string.IsNullOrEmpty(entity.Status) && entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.New))
                        {
                            entity.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Pending);
                            isstatuschange = true;
                        }
                        itemDto.StepId = 4;
                    }
                    entity.MemberId = itemDto.MemberId;

                    // Step 3
                    entity.IsAttended = itemDto.IsAttended;
                    //entity.HowManyTimes = itemDto.HowManyTimes;
                    entity.IsAttendedTg = itemDto.IsAttendedTg;
                    if (itemDto.IsAttended == EnumConversion.ToString(EnumBool.Yes))
                    {
                        entity.HowManyTimes = itemDto.HowManyTimes;
                    }

                    if (!string.IsNullOrEmpty(itemDto.TGReasonForNotAttend))
                        entity.TgreasonForNotAttend = itemDto.TGReasonForNotAttend.Trim();

                    entity.NoofPendingTg = itemDto.NoofPendingTg;
                    entity.NoofPublishedTg = itemDto.NoofPublishedTg;
                    // entity.ProfessionalProgramId = itemDto.ProfessionalProgramId;
                    entity.ProgramOther = itemDto.ProgramOther;

                    entity.TradeType = itemDto.TradeType;
                    entity.IsEbooks = itemDto.IsEbooks;

                    if (!string.IsNullOrEmpty(itemDto.UnSoldRightsBooksOption))
                    {
                        entity.UnSoldRightsBooks = itemDto.UnSoldRightsBooksOption;
                    }
                    //entity.UnSoldRightsBooks = itemDto.UnSoldRightsBooks;

                    entity.CatalogueMainLanguageOfOriginalTitle = itemDto.CatalogueMainLanguageOfOriginalTitle;
                    entity.ExamplesOfTranslatedBooks = itemDto.ExamplesOfTranslatedBooks;
                    entity.OtherGenre = itemDto.OtherGenre;

                    if (itemDto.IsAttended == EnumConversion.ToString(EnumBool.Yes))
                        entity.ReasonToAttendAgain = entity.IsAttendedTg = entity.NoofPendingTg = entity.NoofPublishedTg = string.Empty;
                    else
                        entity.ProgramOther = string.Empty;

                    entity.ModifiedById = itemDto.MemberId;
                    entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                    _context.SaveChanges();
                    if (isstatuschange)
                    {
                        LogRegstrationStatus(itemDto.ItemId, entity.Status);
                    }

                    if (itemDto.IsAttended == EnumConversion.ToString(EnumBool.No))
                    {
                        AddPPProgram(itemDto.ItemId, itemDto.PPProgramList);
                    }
                    #region PPGenre Table
                    AddPPGenre(itemDto.ItemId, itemDto.PPGenreList);
                    #endregion

                    #region PP Books
                    AddPPBooks(itemDto.ItemId, itemDto.ProfessionalProgramBookList);
                    #endregion
                    return true;
                }
                return false;
            }
        }
        private void AddPPBooks(long ppregid, List<ProfessionalProgramBook> ProfessionalProgramBookList)
        {
            if (ProfessionalProgramBookList.Count() > 0)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    var ProfessionalProgramBookListToDel = _context.XsiExhibitionProfessionalProgramBook.Where(i => i.ProfessionalProgramRegistrationId == ppregid).ToList();
                    if (ProfessionalProgramBookListToDel.Count() > 0)
                    {
                        foreach (var ppbooks in ProfessionalProgramBookListToDel)
                        {
                            var TranslatedLanguageList = _context.XsiExhibitionPpbooksLanguage.Where(i => i.PpbookId == ppbooks.ItemId).ToList();
                            _context.XsiExhibitionPpbooksLanguage.RemoveRange(TranslatedLanguageList);
                            _context.SaveChanges();
                        }

                        _context.XsiExhibitionProfessionalProgramBook.RemoveRange(ProfessionalProgramBookListToDel);
                        _context.SaveChanges();
                    }

                    foreach (var ppbookitem in ProfessionalProgramBookList)
                    {
                        XsiExhibitionProfessionalProgramBook ppbooks = new XsiExhibitionProfessionalProgramBook()
                        {
                            Title = ppbookitem.Title,
                            AuthorName = ppbookitem.AuthorName,
                            LanguageId = ppbookitem.OriginalLangugeId,
                            ProfessionalProgramRegistrationId = ppregid,
                            IsActive = "Y",
                            CreatedOn = MethodFactory.ArabianTimeNow(),
                            ModifiedOn = MethodFactory.ArabianTimeNow()
                        };
                        _context.XsiExhibitionProfessionalProgramBook.Add(ppbooks);
                        _context.SaveChanges();
                        var ppbookid = ppbooks.ItemId;

                        foreach (var translatedlangid in ppbookitem.TranslatedLanguageList)
                        {
                            XsiExhibitionPpbooksLanguage ppBooksLanguage = new XsiExhibitionPpbooksLanguage();
                            ppBooksLanguage.PpbookId = ppbookid;
                            ppBooksLanguage.LanguageId = translatedlangid;
                            _context.XsiExhibitionPpbooksLanguage.Add(ppBooksLanguage);
                            _context.SaveChanges();
                        }
                    }
                }
            }
        }
        private void AddPPProgram(long ppregid, List<long> PPProgramList)
        {
            if (PPProgramList.Count() > 0)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    #region PPProgram Table
                    var ppPrograms = _context.XsiExhibitionPpprogram.Where(x => x.ProfessionalProgramRegistrationId == ppregid).ToList();
                    if (ppPrograms.Count > 0)
                    {
                        _context.XsiExhibitionPpprogram.RemoveRange(ppPrograms);
                        _context.SaveChanges();
                    }

                    foreach (var programid in PPProgramList)
                    {
                        XsiExhibitionPpprogram program = new XsiExhibitionPpprogram()
                        {
                            PpprogramId = programid,
                            ProfessionalProgramRegistrationId = ppregid
                        };
                        _context.XsiExhibitionPpprogram.Add(program);
                        _context.SaveChanges();
                    }
                    #endregion
                }
            }
        }
        private void AddPPGenre(long ppregid, List<long> PPGenreList)
        {
            if (PPGenreList.Count > 0)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    var ppGenres = _context.XsiExhibitionPpgenre.Where(x => x.ProfessionalProgramRegistrationId == ppregid).ToList();
                    if (ppGenres.Count > 0)
                    {
                        _context.XsiExhibitionPpgenre.RemoveRange(ppGenres);
                        _context.SaveChanges();
                    }

                    foreach (var item in PPGenreList)
                    {
                        XsiExhibitionPpgenre genre = new XsiExhibitionPpgenre()
                        {
                            GenreId = item,
                            ProfessionalProgramRegistrationId = ppregid
                        };
                        _context.XsiExhibitionPpgenre.Add(genre);
                        _context.SaveChanges();
                    }
                }
            }
        }

      

        private bool UpdateStepFour(PPRegistrationStepFour itemDto)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                bool isstatuschange = false;
                XsiExhibitionProfessionalProgramRegistration entity = _context.XsiExhibitionProfessionalProgramRegistration.Where(x => x.ItemId == itemDto.ItemId).FirstOrDefault();
                if (entity != null)
                {
                    entity.StepId = 4;
                    if (!string.IsNullOrEmpty(entity.Status) && entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.New))
                    {
                        entity.Status = EnumConversion.ToString(EnumProfessionalProgramStatus.Pending);
                        isstatuschange = true;
                    }
                    entity.MemberId = itemDto.MemberId;
                    // Step 4
                    entity.ModifiedById = itemDto.MemberId;
                    entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                    entity.BooksUrl = itemDto.BooksUrl;
                    if (itemDto.FileBase64 != null && itemDto.FileBase64.Length > 0)
                    {
                        string fileName = string.Empty;
                        byte[] imageBytes;
                        if (itemDto.FileBase64.Contains("data:"))
                        {
                            var strInfo = itemDto.FileBase64.Split(",")[0];
                            imageBytes = Convert.FromBase64String(itemDto.FileBase64.Split(',')[1]);
                        }
                        else
                        {
                            imageBytes = Convert.FromBase64String(itemDto.FileBase64);
                        }

                        fileName = string.Format("{0}_{1}_{2}{3}", itemDto.ItemId, 1, MethodFactory.GetRandomNumber(),
                            "." + itemDto.FileExtension);

                        System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\PPRegistration\\Document\\") + fileName, imageBytes);
                        entity.FileName1 = fileName;
                    }
                    entity.ModifiedById = itemDto.MemberId;
                    entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                    _context.SaveChanges();
                    if (isstatuschange)
                    {
                        LogRegstrationStatus(itemDto.ItemId, entity.Status);
                    }

                    return true;
                }
                return false;
            }
        }

        private void LogRegstrationStatus(long registrationid, string strstatus)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibitionProfessionalProgramRegistrationStatusLogs logentity = new XsiExhibitionProfessionalProgramRegistrationStatusLogs();
                logentity.RegistrationId = registrationid;
                logentity.Status = strstatus;
                logentity.CreatedOn = MethodFactory.ArabianTimeNow();
                _context.SaveChanges();
            }
        }

        private ExhibitionProfessionalProgramRegistrationDTO LoadContent(XsiExhibitionProfessionalProgramRegistration entity, ExhibitionProfessionalProgramRegistrationDTO model)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (entity != null)
                {
                    if (entity.ProfessionalProgramId != null)
                        model.ProfessionalProgramId = entity.ProfessionalProgramId.Value;
                    //Step2
                    if (entity.StepId != null)
                        model.StepId = entity.StepId.Value;

                    model.Type = entity.Type;
                    if (entity.Title != null && !string.IsNullOrEmpty(entity.Title))
                    {
                        model.Title = entity.Title;
                    }
                    model.IsHopitalityPackage = (entity.IsHopitalityPackage == EnumConversion.ToString(EnumBool.Yes)) ? "Y" : "N";
                    if (entity.FirstName != null && !string.IsNullOrEmpty(entity.FirstName))
                        model.FirstName = entity.FirstName;
                    if (entity.FirstNameAr != null && !string.IsNullOrEmpty(entity.FirstNameAr))
                        model.FirstNameAr = entity.FirstNameAr;
                    if (entity.LastName != null && !string.IsNullOrEmpty(entity.LastName))
                        model.LastName = entity.LastName;
                    if (entity.LastNameAr != null && !string.IsNullOrEmpty(entity.LastNameAr))
                        model.LastNameAr = entity.LastNameAr;
                    model.Email = entity.Email;

                    if (entity.CityId != null && entity.CountryId == null)
                    {
                        var city = _context.XsiExhibitionCity.Where(x => x.CityId == entity.CityId).FirstOrDefault();
                        if (city != null)
                        {
                            model.CountryId = city.Country.CountryId;
                        }
                    }
                    else if (entity.CountryId != null)
                        model.CountryId = entity.CountryId;

                    if (entity.CityId != null)
                        model.CityId = entity.CityId;
                    model.OtherCity = entity.OtherCity;

                    #region Phone and Mobile
                    if (entity.PhoneNumber != null)
                    {
                        string[] str = entity.PhoneNumber.Split('$');
                        if (str.Count() == 3)
                        {
                            model.PhoneISD = str[0].Trim();
                            model.PhoneSTD = str[1].Trim();
                            model.Phone = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            model.PhoneSTD = str[0].Trim();
                            model.Phone = str[1].Trim();
                        }
                        else
                            model.Phone = str[0].Trim();
                    }
                    if (entity.MobileNumber != null)
                    {
                        string[] str = entity.MobileNumber.Split('$');
                        if (str.Count() == 3)
                        {
                            model.MobileISD = str[0].Trim();
                            model.MobileSTD = str[1].Trim();
                            model.Mobile = str[2].Trim();
                        }
                        else if (str.Count() == 2)
                        {
                            model.MobileSTD = str[0].Trim();
                            model.Mobile = str[1].Trim();
                        }
                        else
                            model.Mobile = str[0].Trim();
                    }
                    #endregion

                    model.ZipCode = entity.ZipCode;

                    if (entity.CompanyName != null)
                        model.CompanyName = entity.CompanyName;
                    if (entity.CompanyNameAr != null)
                        model.CompanyNameAr = entity.CompanyNameAr;
                    model.JobTitle = entity.JobTitle;
                    if (entity.JobTitleAr != null)
                        model.JobTitleAr = entity.JobTitleAr;
                    if (entity.WorkProfile != null)
                        model.WorkProfile = entity.WorkProfile;
                    if (entity.WorkProfileAr != null)
                        model.WorkProfileAr = entity.WorkProfileAr;
                    model.Website = entity.Website;
                    model.SocialLinkUrl = entity.SocialLinkUrl;

                    // Step 3
                    model.IsAttended = entity.IsAttended;
                    model.HowManyTimes = entity.IsAttended == EnumConversion.ToString(EnumBool.Yes) ? entity.HowManyTimes : string.Empty;

                    model.IsAttendedTg = entity.IsAttendedTg;
                    model.NoofPendingTg = entity.NoofPendingTg;
                    model.NoofPublishedTg = entity.NoofPublishedTg;

                    model.TradeType = entity.TradeType;
                    model.IsEbooks = entity.IsEbooks;
                    model.UnSoldRightsBooks = entity.UnSoldRightsBooks;
                    model.CatalogueMainLanguageOfOriginalTitle = entity.CatalogueMainLanguageOfOriginalTitle;
                    model.ExamplesOfTranslatedBooks = entity.ExamplesOfTranslatedBooks;
                    model.GenreId = entity.GenreId;
                    model.OtherGenre = entity.OtherGenre;

                    if (entity.IsAttended == EnumConversion.ToString(EnumBool.No))
                    {
                        model.ProgramOther = entity.ProgramOther;
                        #region Load PPProgram Tags
                        List<long> PPProgramIDs = _context.XsiExhibitionPpprogram.Where(x => x.PpprogramId != null && x.ProfessionalProgramRegistrationId == entity.ItemId).Select(x => x.PpprogramId.Value).ToList();
                        model.PPProgramList = PPProgramIDs;
                        #endregion
                    }
                    else
                    {
                        model.PPProgramList = new List<long>();
                        model.ProgramOther = string.Empty;
                    }

                    #region Load Genre GroupId Tags
                    List<long> PPGenreList = _context.XsiExhibitionPpgenre.Where(x => x.GenreId != null && x.ProfessionalProgramRegistrationId == entity.ItemId).Select(x => x.GenreId.Value).ToList();
                    model.PPGenreList = PPGenreList;
                    #endregion
                    // Step 4
                    model.BooksUrl = entity.BooksUrl;

                    if (!string.IsNullOrEmpty(entity.FileName1))
                        model.CatalogFileName = _appCustomSettings.UploadsCMSPath + "/PPRegistration/Document/" + entity.FileName1;

                    model.ProfessionalProgramBookList = MethodFactory.GetProfessionalProgramBookList(entity.ItemId);

                    //if (File.Exists(Server.MapPath(LocationMapPath + "Document/" + existingEntity.FileName1)))
                    //    File.Delete(Server.MapPath(LocationMapPath + "Document/" + existingEntity.FileName1));
                    //string filename = string.Format("{0}_{1}{2}", EnglishId, MethodFactory.GetRandomNumber(), MethodFactory.GetFileExtention(fuCatalog.FileName));
                    //fuCatalog.SaveAs(string.Format("{0}{1}", Server.MapPath(LocationMapPath + "Document/"), filename));
                    //Entity.FileName1 = filename;

                    if (entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Approved))
                        model.Success = true;
                    else
                    if (entity.Status == EnumConversion.ToString(EnumProfessionalProgramStatus.Rejected))
                        model.Reject = true;
                }
                else
                {
                    long memberid = User.Identity.GetID();
                    var predicate = PredicateBuilder.True<XsiExhibitionMember>();
                    predicate = predicate.And(i => i.MemberId == memberid);
                    XsiExhibitionMember member = _context.XsiExhibitionMember.AsQueryable().Where(predicate).FirstOrDefault();
                    if (member != null)
                    {
                        if (member.CityId != null && member.CityId > 0)
                        {
                            var city = _context.XsiExhibitionCity.Where(x => x.CityId == member.CityId).FirstOrDefault();
                            if (city != null)
                            {
                                model.CountryId = city.CountryId;
                                model.CityId = city.CityId;
                            }
                        }

                        model.StepId = GetStepIdForMember(memberid);
                        model.FirstName = member.Firstname;
                        model.LastName = member.LastName;
                        model.JobTitle = member.JobTitle;
                        model.FirstNameAr = member.FirstNameAr;
                        model.LastNameAr = member.LastNameAr;
                        model.JobTitleAr = member.JobTitleAr;
                        model.CompanyName = member.CompanyName;
                        model.CompanyNameAr = member.CompanyNameAr;
                        model.Email = member.Email;
                        if (member.Phone != null)
                        {
                            string[] str = member.Phone.Split('$');
                            if (str.Count() == 3)
                            {
                                model.PhoneISD = str[0].Trim();
                                model.PhoneSTD = str[1].Trim();
                                model.Phone = str[2].Trim();
                            }
                            else if (str.Count() == 2)
                            {
                                model.PhoneSTD = str[0].Trim();
                                model.Phone = str[1].Trim();
                            }
                            else
                                model.Phone = str[0].Trim();
                        }
                        if (member.Mobile != null)
                        {
                            string[] str = member.Mobile.Split('$');
                            if (str.Count() == 3)
                            {
                                model.MobileISD = str[0].Trim();
                                model.MobileSTD = str[1].Trim();
                                model.Mobile = str[2].Trim();
                            }
                            else if (str.Count() == 2)
                            {
                                model.MobileSTD = str[0].Trim();
                                model.Mobile = str[1].Trim();
                            }
                            else
                                model.Mobile = str[0].Trim();
                        }
                        model.Website = member.Website;
                    }
                }
            }
            return model;
        }
        private long? GetStepIdForMember(long memberid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberId == memberid).ToList().Count == 0
                    ? 1
                    : 2;
            }
        }
        private long GetProfessionalProgramId()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgram>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
                XsiExhibitionProfessionalProgram entity = _context.XsiExhibitionProfessionalProgram.Where(predicate).OrderByDescending(x => x.ProfessionalProgramId).FirstOrDefault();
                if (entity != null)
                {
                    //ExhibitionGroupId = entity.GroupId.Value;
                    return entity.ProfessionalProgramId;
                }
                return -1;
            }
        }
        private bool IsAlreadyRegistered(long memberId, long profProgramId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgramRegistration>();
                // predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.MemberId == memberId);
                predicate = predicate.And(i => i.ProfessionalProgramId == profProgramId);
                XsiExhibitionProfessionalProgramRegistration entity = _context.XsiExhibitionProfessionalProgramRegistration.Where(predicate).FirstOrDefault();
                if (entity != null)
                {
                    return true;
                }
                return false;
            }
        }
        private void SendThankYouEmailToUser(long memberid)
        {
            using (ExhibitionMemberService memberEntity = new ExhibitionMemberService())
            {
                HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                XsiExhibitionMember entity = new XsiExhibitionMember();
                entity = memberEntity.GetExhibitionMemberByItemId(Convert.ToInt64(memberid));
                if (entity != default(XsiExhibitionMember))
                {
                    long LangId = 1;// Convert.ToInt64(System.Web.Configuration.WebConfigurationManager.AppSettings["EnglishId"].ToString());
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {

                        StringBuilder body = new StringBuilder();
                        XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(20103);
                        if (emailContent != null)
                        {
                            //bp.WebsiteId = Convert.ToInt64(EnumWebsiteId.SIBF);
                            body.Append(htmlContentFactory.BindEmailContent(LangId, emailContent.Body, entity, _appCustomSettings.ServerAddressNew, 1, _appCustomSettings));

                            body.Replace("$$Name$$", entity.Firstname + " " + entity.LastName);
                            if (entity.Email != null)
                                _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, entity.Email, emailContent.Subject, body.ToString());
                        }
                    }
                }
            }
        }
        private void NotifyAdminForApproval(long memberid)
        {
            using (ExhibitionMemberService member = new ExhibitionMemberService())
            {
                XsiExhibitionMember entity1 = new XsiExhibitionMember();
                entity1 = member.GetExhibitionMemberByItemId(Convert.ToInt64(memberid));
                if (entity1 != default(XsiExhibitionMember))
                {
                    long LangId = 1;
                    using (EmailContentService EmailContentService = new EmailContentService())
                    {

                        StringBuilder body = new StringBuilder();
                        XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(59);
                        if (emailContent != null)
                        {
                            body.Append(emailContent.Body);
                            body.Replace("$$Name$$", entity1.Firstname + " " + entity1.LastName);
                            if (emailContent.Email != null)
                                _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, emailContent.Email, emailContent.Subject, body.ToString());
                            //else
                            //    _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, emailContent.SIBFEmail, emailContent.Subject, body.ToString());
                            //SmtpEmail.SendNetEmail(AdminName, WebConfigurationManager.AppSettings["AdminEmail"], WebConfigurationManager.AppSettings["SIBFEmail"], emailContent.Subject, body.ToString());
                        }
                    }
                }
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using SIBFAPIEn.Utility;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Account.Manage.Internal;
using LinqKit;
using System.IO;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MyAgencyController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;
        private readonly IEmailSender _emailSender;
        public MyAgencyController(IEmailSender emailSender, IOptions<AppCustomSettings> appCustomSettings)
        {
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
        }

        #region Get
        // GET: api/GetMyAgency
        [HttpGet]
        [Route("{websiteid}")]
        public async Task<ActionResult<dynamic>> GetMyAgency(long websiteid)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    long memberid = User.Identity.GetID();
                    MyAgencyDTO model = new MyAgencyDTO();

                    if (IsExhibitionActive(websiteid))
                    {
                        XsiExhibitionMemberApplicationYearly ExhibitorEntity = MethodFactory.GetExhibitor(memberid, websiteid, _context);
                        if (ExhibitorEntity != null)
                        {
                            if (ExhibitorEntity.Status != null)
                            {
                                if (ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.New) || ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.InitialApproval) || ExhibitorEntity.Status == EnumConversion.ToString(EnumExhibitorStatus.Approved))
                                {
                                    model.ExhibitorId = ExhibitorEntity.MemberExhibitionYearlyId;
                                    model.ExhibitionId = ExhibitorEntity.ExhibitionId.Value;
                                    //ExhbitiorBoothSectionId = ExhibitorEntity.
                                    //GetExhibitor();
                                    BindAgency(model, LangId);
                                }
                                else
                                    model.ReturnURL = LangId == 1 ? "/en/MemberDashboard" : "/ar/MemberDashboard";
                            }
                            else
                                model.ReturnURL = LangId == 1 ? "/en/MemberDashboard" : "/ar/MemberDashboard";
                        }
                        else
                            model.ReturnURL = LangId == 1 ? "/en/MemberDashboard" : "/ar/MemberDashboard";
                    }
                    else
                    {
                        model.FormMessage = LangId == 1 ? "Exhibition Unavailable." : "المعرض غير متوفر";
                    }
                    return Ok(model);
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpGet]
        [Route("agency/{agencyid}")]
        public async Task<ActionResult<dynamic>> GetAgencyDetails(long agencyid)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    string strActivities = string.Empty;
                    #region Agency
                    XsiExhibitionMemberApplicationYearly AgencyRegistrationEntity;
                    AgencyRegistrationEntityCompleteDTO agencydto = new AgencyRegistrationEntityCompleteDTO();
                    AgencyRegistrationEntity = _context.XsiExhibitionMemberApplicationYearly.Where(x => x.MemberExhibitionYearlyId == agencyid).FirstOrDefault();
                    XsiExhibitionMemberApplicationYearly ExhibitorEntity = new XsiExhibitionMemberApplicationYearly();
                    if (AgencyRegistrationEntity != null)
                    {
                        if (AgencyRegistrationEntity.ParentId != null)
                        {
                            ExhibitorEntity = _context.XsiExhibitionMemberApplicationYearly.Where(x => x.MemberExhibitionYearlyId == AgencyRegistrationEntity.ParentId).FirstOrDefault();
                            #region AgencyRegistration Entity
                            agencydto.ItemId = AgencyRegistrationEntity.MemberExhibitionYearlyId;
                            agencydto.MemberId = AgencyRegistrationEntity.MemberId;
                            agencydto.ExhibitorId = AgencyRegistrationEntity.ParentId;
                            agencydto.CountryId = AgencyRegistrationEntity.CountryId;
                            agencydto.LanguageId = AgencyRegistrationEntity.LanguageUrl;
                            agencydto.ExhibitionCategoryId = AgencyRegistrationEntity.ExhibitionCategoryId;
                            agencydto.IsActive = AgencyRegistrationEntity.IsActive;
                            agencydto.Status = AgencyRegistrationEntity.Status;
                            agencydto.AgencyName = LangId == 1 ? AgencyRegistrationEntity.PublisherNameEn : AgencyRegistrationEntity.PublisherNameAr;
                            agencydto.ContactPersonName = LangId == 1 ? AgencyRegistrationEntity.ContactPersonName : AgencyRegistrationEntity.ContactPersonNameAr;
                            agencydto.ContactPersonTitle = LangId == 1 ? AgencyRegistrationEntity.ContactPersonTitle : AgencyRegistrationEntity.ContactPersonTitleAr;

                            if (AgencyRegistrationEntity.Phone != null)
                                agencydto.Phone = AgencyRegistrationEntity.Phone.Replace("$", "");
                            if (AgencyRegistrationEntity.Fax != null)
                                agencydto.Fax = AgencyRegistrationEntity.Fax.Replace("$", "");
                            agencydto.Email = AgencyRegistrationEntity.Email;
                            agencydto.TotalNumberTitles = AgencyRegistrationEntity.TotalNumberOfTitles;
                            agencydto.TotalNumberNewTitles = AgencyRegistrationEntity.TotalNumberOfNewTitles;

                            agencydto.FileNumber = AgencyRegistrationEntity.FileNumber;
                            if (AgencyRegistrationEntity.Mobile != null)
                                agencydto.Mobile = AgencyRegistrationEntity.Mobile.Replace("$", "");
                            agencydto.Website = AgencyRegistrationEntity.Website;
                            agencydto.ExactNameBoard = AgencyRegistrationEntity.ExactNameBoard;
                            agencydto.UploadLocationMap = AgencyRegistrationEntity.UploadLocationMap;

                            agencydto.Address = AgencyRegistrationEntity.Address;
                            agencydto.CreatedOn = AgencyRegistrationEntity.CreatedOn;
                            agencydto.CreatedBy = AgencyRegistrationEntity.CreatedBy;
                            agencydto.ModifiedOn = AgencyRegistrationEntity.ModifiedOn;
                            agencydto.ModifiedBy = AgencyRegistrationEntity.ModifiedBy;

                            agencydto.ExhbitionActivity = null;
                            agencydto.ExhibitionCategory = null;
                            agencydto.BoothSubSection = null;
                            agencydto.ExhibitionCity = null;
                            agencydto.ExhibitionCountry = null;
                            agencydto.ExhibitorName = LangId == 1 ? ExhibitorEntity.PublisherNameEn : ExhibitorEntity.PublisherNameAr;
                            if (!string.IsNullOrEmpty(AgencyRegistrationEntity.AuthorizationLetter))
                                agencydto.AuhtorizationLetterFileName = AgencyRegistrationEntity.AuthorizationLetter;
                            #endregion
                            #region Get Activities
                            HashSet<long> agencyActivityIds = new HashSet<long>(_context.XsiMemberExhibitionActivityYearly.Where(x => x.MemberExhibitionYearlyId == AgencyRegistrationEntity.MemberExhibitionYearlyId).Select(x => x.ActivityId));
                            if (agencyActivityIds.Count > 0)
                            {
                                List<XsiExhibitionActivity> ExhibitionActivityList = _context.XsiExhibitionActivity.Where(x => agencyActivityIds.Contains(x.ItemId) && x.IsActive == "Y").ToList();
                                foreach (XsiExhibitionActivity entity in ExhibitionActivityList)
                                    strActivities += ", " + entity.Title;
                                if (!string.IsNullOrEmpty(strActivities) && strActivities.IndexOf(',') != -1)
                                    agencydto.ExhbitionActivity = strActivities.Substring(1);
                            }
                            #endregion
                            #region Exhibition Category
                            XsiExhibitionCategory category = _context.XsiExhibitionCategory.Where(x => x.ItemId == AgencyRegistrationEntity.ExhibitionCategoryId).FirstOrDefault();
                            if (category != null)
                                agencydto.ExhibitionCategory = category.Title;
                            #endregion
                            #region Country and City
                            XsiExhibitionCountry ExhibitionCountryEntity = _context.XsiExhibitionCountry.Where(x => x.CountryId == AgencyRegistrationEntity.CountryId).FirstOrDefault();
                            if (ExhibitionCountryEntity != null)
                            {
                                if (ExhibitionCountryEntity.CountryName != null)
                                    agencydto.ExhibitionCountry = ExhibitionCountryEntity.CountryName;
                                if (AgencyRegistrationEntity.CityId != null)
                                {
                                    XsiExhibitionCity ExhibitionCityEntity = _context.XsiExhibitionCity.Where(x => x.CityId == AgencyRegistrationEntity.CityId && x.CountryId == ExhibitionCountryEntity.CountryId).FirstOrDefault();
                                    if (ExhibitionCityEntity != null)
                                        if (ExhibitionCityEntity.CityName != null)
                                            agencydto.ExhibitionCity = ExhibitionCityEntity.CityName;
                                }
                                else
                                    agencydto.ExhibitionCity = string.Empty;
                            }
                            else
                            {
                                agencydto.ExhibitionCountry = string.Empty;
                                agencydto.ExhibitionCity = string.Empty;
                            }
                            #endregion
                            #region Exhibitor dependent fields
                            XsiExhibitionExhibitorDetails exhibitorentity = _context.XsiExhibitionExhibitorDetails.Where(x => x.MemberExhibitionYearlyId == AgencyRegistrationEntity.MemberExhibitionYearlyId).FirstOrDefault(); ;
                            if (exhibitorentity != default(XsiExhibitionExhibitorDetails))
                            {
                                agencydto.HallNumber = exhibitorentity.HallNumber;
                                agencydto.StandNumberStart = exhibitorentity.StandNumberStart;
                                agencydto.StandNumberEnd = exhibitorentity.StandNumberEnd;
                                #region Section and Subsection
                                XsiExhibitionBooth ExhibitionBoothEntity = _context.XsiExhibitionBooth.Where(x => x.ItemId == exhibitorentity.BoothSectionId).FirstOrDefault();
                                if (ExhibitionBoothEntity != null)
                                {
                                    if (ExhibitionBoothEntity.Title != null)
                                        agencydto.BoothSection = ExhibitionBoothEntity.Title;
                                    if (exhibitorentity.BoothSubSectionId != null)
                                    {
                                        XsiExhibitionBoothSubsection ExhibitionBoothSubEntity = _context.XsiExhibitionBoothSubsection.Where(x => x.ItemId == exhibitorentity.BoothSubSectionId && x.CategoryId == ExhibitionBoothEntity.ItemId).FirstOrDefault();
                                        if (ExhibitionBoothSubEntity != null)
                                            if (ExhibitionBoothSubEntity.Title != null)
                                                agencydto.BoothSubSection = ExhibitionBoothSubEntity.Title;
                                    }
                                    else
                                        agencydto.BoothSubSection = string.Empty;
                                }
                                else
                                {
                                    agencydto.BoothSection = string.Empty;
                                    agencydto.BoothSubSection = string.Empty;
                                }
                                #endregion
                            }
                            #endregion
                            return Ok(agencydto);
                        }
                    }
                    #endregion
                    return Ok("data not found");
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        #endregion
        #region Post

        [HttpPost]
        [Route("AddAgency")]
        public ActionResult<dynamic> PostMyAgency(List<AgencyPostDTO> itemDto)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    long memberid = User.Identity.GetID();
                    if (memberid > 0)
                    {
                        XsiExhibitionMember member = _context.XsiExhibitionMember.Where(i =>
                                i.MemberId == memberid &&
                                i.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.Approved))
                            .FirstOrDefault();
                        foreach (var item in itemDto)
                        {
                            #region Exhibition List
                            var predicate = PredicateBuilder.New<XsiExhibition>();
                            predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                            predicate = predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
                            predicate = predicate.And(i => i.WebsiteId == item.WebSiteId);
                            XsiExhibition exhibition = _context.XsiExhibition.Where(predicate).FirstOrDefault();
                            #endregion
                            #region Check Exhibition Active
                            if (exhibition != null)
                            {
                                if (item.ExhibitorId > 0)
                                {
                                    string strSibfApproved = EnumConversion.ToString(EnumAgencyStatus.SIBFApproved);
                                    string strExhibitorApproved = EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved);
                                    string strNew = EnumConversion.ToString(EnumAgencyStatus.SIBFApproved);
                                    var predicate2 = PredicateBuilder.New<XsiExhibitionMemberApplicationYearly>();
                                    predicate2 = predicate2.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                                    predicate2 = predicate2.And(i => i.ParentId == item.ExhibitorId);
                                    predicate2 = predicate2.And(i => i.ExhibitionId == exhibition.ExhibitionId);
                                    predicate2 = predicate2.And(i => i.MemberRoleId == 2);
                                    List<XsiExhibitionMemberApplicationYearly> AgencyRegistrationList = _context.XsiExhibitionMemberApplicationYearly.Where(predicate2).ToList();
                                    long count = 0;
                                    if (!string.IsNullOrEmpty(item.PublisherNameAr))
                                        count = AgencyRegistrationList.Where(p => (p.Status == strSibfApproved || p.Status == strExhibitorApproved || p.Status == strNew) && (p.PublisherNameEn.ToLower() == item.PublisherNameEn.ToLower() || p.PublisherNameAr.ToLower() == item.PublisherNameAr.ToLower())).Count();
                                    else
                                        count = AgencyRegistrationList.Where(p => (p.Status == strSibfApproved || p.Status == strExhibitorApproved || p.Status == strNew) && (p.PublisherNameEn.ToLower() == item.PublisherNameEn.ToLower())).Count();
                                    if (count > 0)
                                    {
                                        return Ok("Already Exists");
                                    }
                                    else
                                    {
                                        if (item.AuhtorizationLetterBase64 != null && item.AuhtorizationLetterBase64.Length > 0)
                                        {
                                            #region Insert
                                            XsiExhibitionMemberApplicationYearly entity1 = new XsiExhibitionMemberApplicationYearly();
                                            entity1.PublisherNameEn = item.PublisherNameEn;
                                            entity1.PublisherNameAr = item.PublisherNameAr;
                                            entity1.CountryId = item.CountryId;
                                            entity1.CityId = item.CityId;
                                            entity1.TotalNumberOfTitles = item.TotalNumberOfTitles;
                                            entity1.TotalNumberOfNewTitles = item.TotalNumberOfNewTitles;
                                            entity1.ParentId = item.ExhibitorId;
                                            entity1.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                            entity1.IsRegisteredByExhibitor = EnumConversion.ToString(EnumBool.Yes);
                                            entity1.Status = EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved);
                                            entity1.MemberId = item.MemberId;
                                            entity1.ExhibitionId = exhibition.ExhibitionId;
                                            entity1.LanguageUrl = LangId;

                                            if (item.AuhtorizationLetterBase64 != null && item.AuhtorizationLetterBase64.Length > 0)
                                            {
                                                string fileName = string.Empty;
                                                byte[] imageBytes;
                                                if (item.AuhtorizationLetterBase64.Contains("data:"))
                                                {
                                                    var strInfo = item.AuhtorizationLetterBase64.Split(",")[0];
                                                    imageBytes = Convert.FromBase64String(item.AuhtorizationLetterBase64.Split(',')[1]);
                                                }
                                                else
                                                {
                                                    imageBytes = Convert.FromBase64String(item.AuhtorizationLetterBase64);
                                                }

                                                fileName = string.Format("{0}_{1}{2}", LangId, MethodFactory.GetRandomNumber(), "." + item.AuhtorizationLetterExtension);
                                                //if (MethodFactory.IsValidAgencyAuthorizationLetter("." + item.AuhtorizationLetterExtension))
                                                //{
                                                //    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\ExhibitorRegistration\\AuthorizationLetter\\") + fileName, imageBytes);
                                                //    entity1.AuthorizationLetter = fileName;
                                                //}
                                                //else
                                                //{
                                                //    System.IO.File.WriteAllBytes(
                                                //        Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                                //                     "\\ExhibitorRegistration\\AuthorizationLetter\\") + fileName, imageBytes);
                                                //}
                                                System.IO.File.WriteAllBytes(
                                                        Path.Combine(_appCustomSettings.UploadsPhysicalPath +
                                                                     "\\ExhibitorRegistration\\AuthorizationLetter\\") + fileName, imageBytes);

                                                entity1.AuthorizationLetter = fileName;
                                            }
                                            entity1.MemberRoleId = 2;
                                            entity1.CreatedOn = MethodFactory.ArabianTimeNow();
                                            entity1.CreatedBy = item.MemberId;
                                            entity1.ModifiedOn = MethodFactory.ArabianTimeNow();
                                            entity1.ModifiedBy = item.MemberId;
                                            _context.XsiExhibitionMemberApplicationYearly.Add(entity1);
                                            _context.SaveChanges();
                                            #endregion
                                        }
                                        else
                                        {
                                            return Ok("Auhtorization Letter is mandatory.");
                                        }
                                    }
                                }
                                else
                                {
                                    return Ok("Exhibitor Not found");
                                }
                            }
                            else
                            {
                                return Ok("Not found");
                            }
                            #endregion
                        }
                        return Ok("Added successfully");
                    }
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpPost]
        [Route("RejectAgency")]
        public ActionResult<dynamic> PostRejectAgency(AgencyAcceptOrRejectDTO itemDto)
        {
            AcceptOrRejectStatusMessage dto = new AcceptOrRejectStatusMessage();
            long LangId = 1;
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    var entity = _context.XsiExhibitionMemberApplicationYearly.Where(x => x.MemberExhibitionYearlyId == itemDto.ItemId).FirstOrDefault();
                    if (entity != null)
                    {
                        if (entity.Status == EnumConversion.ToString(EnumAgencyStatus.SIBFApproved))
                        {
                            dto.IsSuccess = false;
                            dto.Message = LangId == 1 ? "Approved agency cannot be cancelled" : "Approved agency cannot be cancelled";
                        }
                        else
                        {
                            entity.Status = EnumConversion.ToString(EnumAgencyStatus.ExhibitorReject);
                            _context.SaveChanges();
                            //AgencyRegistrationService.UpdateAgencyRegistration(entity);
                            //BindAgency();
                            if (entity.MemberId != null)
                                SendEmail(Convert.ToInt64(entity.MemberId), entity.PublisherNameEn, itemDto.Websiteid);
                            dto.IsSuccess = true;
                            dto.Message = string.Empty;
                        }
                        return Ok(dto);
                    }
                    dto.IsSuccess = false;
                    dto.Message = LangId == 1 ? "Some thing went wrong.Please retry after sometime." : "Some thing went wrong.Please retry after sometime.";
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                dto.IsSuccess = false;
                dto.Message = LangId == 1 ? "Some thing went wrong.Please retry after sometime." : "Some thing went wrong.Please retry after sometime.";
                return Ok(dto);
            }
        }

        [HttpPost]
        [Route("AcceptInvitation")]
        public ActionResult<dynamic> PostAcceptInvitation(AgencyAcceptOrRejectDTO itemDto)
        {
            try
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    AcceptOrRejectStatusMessage dto = new AcceptOrRejectStatusMessage();
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);

                    var entity = _context.XsiExhibitionMemberApplicationYearly.Where(x => x.MemberExhibitionYearlyId == itemDto.ItemId).FirstOrDefault();
                    if (entity != null)
                    {
                        entity.Status = EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved);
                        _context.SaveChanges();
                        //AgencyRegistrationService.UpdateAgencyRegistration(entity);
                        //BindAgency();
                        if (entity.MemberId != null && entity.MemberExhibitionYearlyId != 0)
                            SendEmailAccept(Convert.ToInt64(entity.MemberId), entity.PublisherNameEn, MethodFactory.GetExhibitorName(Convert.ToInt64(entity.MemberExhibitionYearlyId), _context), itemDto.Websiteid);
                        dto.IsSuccess = true;
                        dto.Message = string.Empty;
                        return Ok(dto);
                    }
                    dto.IsSuccess = false;
                    dto.Message = LangId == 1 ? "Some thing went wrong.Please retry after sometime." : "Some thing went wrong.Please retry after sometime.";
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        #endregion

        #region Private Methods
        private bool IsExhibitionActive(long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibition entity = MethodFactory.GetActiveExhibition(websiteId, _context);
                if (entity != null)
                {
                    //ExhibitionId = entity.ExhibitionId;
                    return true;
                }
                return false;
            }
        }
        private void BindAgency(MyAgencyDTO model, long langid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                List<XsiExhibitionMemberApplicationYearly> AgencyRegistrationGenericList = new List<XsiExhibitionMemberApplicationYearly>();
                List<XsiExhibitionMemberApplicationYearly> AgencyRegistrationInvitationGenericList = new List<XsiExhibitionMemberApplicationYearly>();
                string strSIBFApproved = EnumConversion.ToString(EnumAgencyStatus.SIBFApproved);
                string strExhibitorApproved = EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved);
                string strPending = EnumConversion.ToString(EnumAgencyStatus.Pending);
                string strNew = EnumConversion.ToString(EnumAgencyStatus.New);

                var predicate = PredicateBuilder.New<XsiExhibitionMemberApplicationYearly>();
                predicate = predicate.And(i => i.ParentId == model.ExhibitorId);
                predicate = predicate.And(i => i.MemberRoleId == 2);
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                AgencyRegistrationGenericList = _context.XsiExhibitionMemberApplicationYearly.Where(predicate).ToList();
                AgencyRegistrationGenericList = AgencyRegistrationGenericList.Where(p => (p.Status == strSIBFApproved || p.Status == strExhibitorApproved || p.Status == strPending || p.Status == strNew) && p.Status != null).ToList();

                model.CurrentAgencyCount = AgencyRegistrationGenericList.Count();
                if (IsDateExpired(model.ExhibitionId.Value))
                {
                    model.FormMessage = "Agency registration date expired.";
                    model.HideForm = true;
                }
                else
                {
                    model.ShowAgencyCount = model.CurrentAgencyCount;
                    //if (model.CurrentAgencyCount >= 3)
                    //{
                    //    model.ShowAgencyCount = 5;
                    //}
                    //else
                    //{
                    //    model.ShowAgencyCount = 3;
                    //}
                }
                if (langid == 1)
                {
                    #region My Agency
                    if (AgencyRegistrationGenericList.Count() > 0)
                    {
                        model.AgencyList = AgencyRegistrationGenericList.Select(x =>
                                new AgencyDTO
                                {
                                    AgencyId = x.MemberExhibitionYearlyId,
                                    ExhibitorId = x.ParentId.Value,
                                    AgencyName = x.PublisherNameEn,
                                    ContactPerson = x.ContactPersonName,
                                    TotalTitles = x.TotalNumberOfTitles,
                                    NewTitles = x.TotalNumberOfNewTitles,
                                    Country = GetCountryName(x.CountryId.Value, langid),
                                    City = GetCityName(x.CityId.Value, langid),
                                    IsPending = (x.Status == EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved) || x.Status == EnumConversion.ToString(EnumAgencyStatus.Pending)),
                                    IsAddedByExhibitor= (x.IsRegisteredByExhibitor == "Y")
                                }).ToList();
                    }
                    else
                    {
                        model.AgencyMessage = "Agencies Unavailable.";
                    }
                    #endregion
                }
                else
                {
                    #region My Agency
                    if (AgencyRegistrationGenericList.Count() > 0)
                    {
                        model.AgencyList = AgencyRegistrationGenericList.Select(x =>
                                new AgencyDTO
                                {
                                    AgencyId = x.MemberExhibitionYearlyId,
                                    ExhibitorId = x.ParentId.Value,
                                    AgencyName = x.PublisherNameAr,
                                    ContactPerson = x.ContactPersonNameAr,
                                    TotalTitles = x.TotalNumberOfTitles,
                                    NewTitles = x.TotalNumberOfNewTitles,
                                    Country = GetCountryName(x.CountryId.Value, langid),
                                    City = GetCityName(x.CityId.Value, langid),
                                    IsPending = (x.Status == EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved) || x.Status == EnumConversion.ToString(EnumAgencyStatus.Pending)),
                                    IsAddedByExhibitor = (x.IsRegisteredByExhibitor == "Y")
                                }).ToList();
                    }
                    else
                    {
                        model.AgencyMessage = "Agencies Unavailable.";
                    }
                    #endregion
                }
                #region Invitations
                var predicate1 = PredicateBuilder.New<XsiExhibitionMemberApplicationYearly>();
                predicate1 = predicate1.And(i => i.ParentId == model.ExhibitorId);
                predicate1 = predicate1.And(i => i.MemberRoleId == 2);
                predicate1 = predicate1.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate1 = predicate1.And(i => i.Status == EnumConversion.ToString(EnumAgencyStatus.New));
                AgencyRegistrationInvitationGenericList = _context.XsiExhibitionMemberApplicationYearly.Where(predicate1).ToList();
                int count = AgencyRegistrationInvitationGenericList.Count();
                if (count > 0)
                {
                    if (langid == 1)
                    {
                        model.InvitationList = AgencyRegistrationInvitationGenericList.Select(x =>
                                new AgencyDTO
                                {
                                    AgencyId = x.MemberExhibitionYearlyId,
                                    ExhibitorId = x.ParentId.Value,
                                    AgencyName = x.PublisherNameEn,
                                    ContactPerson = x.ContactPersonName,
                                    TotalTitles = x.TotalNumberOfTitles,
                                    NewTitles = x.TotalNumberOfNewTitles,
                                    Country = GetCountryName(x.CountryId.Value, langid),
                                    City = GetCityName(x.CityId.Value, langid)
                                }).ToList();
                    }
                    else
                    {
                        model.InvitationList = AgencyRegistrationInvitationGenericList.Select(x =>
                               new AgencyDTO
                               {
                                   AgencyId = x.MemberExhibitionYearlyId,
                                   ExhibitorId = x.ParentId.Value,
                                   AgencyName = x.PublisherNameAr,
                                   ContactPerson = x.ContactPersonNameAr,
                                   TotalTitles = x.TotalNumberOfTitles,
                                   NewTitles = x.TotalNumberOfNewTitles,
                                   Country = GetCountryName(x.CountryId.Value, langid),
                                   City = GetCityName(x.CityId.Value, langid)
                               }).ToList();
                    }
                    model.CurrentInvitationCount = count;
                }
                else
                {
                    if (langid == 1)
                        model.InvitationMessage = "You do not have invitations.";
                    else
                        model.InvitationMessage = "ليس لديك دعوة";
                }
                #endregion
            }
        }
        private bool IsDateExpired(long exhibitionid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var predicate = PredicateBuilder.New<XsiExhibition>();
                predicate.And(i => i.ExhibitionId == exhibitionid);
                var entity = _context.XsiExhibition.Where(predicate).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                if (entity != default(XsiExhibition))
                {
                    if (MethodFactory.ArabianTimeNow() > entity.AgencyEndDate)
                        return true;
                    else
                        return false;
                }
                return true;
            }
        }
        private string GetCountryName(long countryId, long langid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var predicate = PredicateBuilder.New<XsiExhibitionCountry>();
                predicate.And(i => i.CountryId == countryId);
                var entity = _context.XsiExhibitionCountry.Where(predicate).OrderByDescending(i => i.CountryId).FirstOrDefault();
                if (entity != default(XsiExhibitionCountry))
                    return langid == 1 ? entity.CountryName : entity.CountryNameAr;
                return "";
            }
        }
        private string GetCityName(long cityId, long langid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var predicate = PredicateBuilder.New<XsiExhibitionCity>();
                predicate.And(i => i.CityId == cityId);
                var entity = _context.XsiExhibitionCity.Where(predicate).OrderByDescending(i => i.CityId).FirstOrDefault();
                if (entity != default(XsiExhibitionCity))
                    return langid == 1 ? entity.CityName : entity.CityNameAr;
                return "";
            }
        }
        #endregion
        #region Email
        void SendEmail(long itemid, string agencyname, long websiteid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibitionMember entity = new XsiExhibitionMember();
                entity = _context.XsiExhibitionMember.Where(x => x.MemberId == itemid).FirstOrDefault();
                if (entity != default(XsiExhibitionMember))
                {
                    #region Send Reject Email to Agency
                    StringBuilder body = new StringBuilder();
                    string strEmailContentBody = "";
                    string strEmailContentEmail = "";
                    string strEmailContentSubject = "";
                    string strEmailContentAdmin = "";
                    #region Email Content
                    if (websiteid == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        var scrfEmailContent = _context.XsiScrfemailContent.Where(x => x.ItemId == 21).FirstOrDefault();
                        if (scrfEmailContent != null)
                        {
                            if (entity.LanguageUrl.Value == 1)
                            {
                                if (scrfEmailContent.Body != null)
                                    strEmailContentBody = scrfEmailContent.Body;
                                if (scrfEmailContent.Email != null)
                                    strEmailContentEmail = scrfEmailContent.Email;
                                if (scrfEmailContent.Subject != null)
                                    strEmailContentSubject = scrfEmailContent.Subject;
                            }
                            else
                            {
                                if (scrfEmailContent.BodyAr != null)
                                    strEmailContentBody = scrfEmailContent.BodyAr;
                                if (scrfEmailContent.Email != null)
                                    strEmailContentEmail = scrfEmailContent.Email;
                                if (scrfEmailContent.SubjectAr != null)
                                    strEmailContentSubject = scrfEmailContent.SubjectAr;
                            }

                            strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                        }
                    }
                    else
                    {
                        var emailContent = _context.XsiEmailContent.Where(x => x.ItemId == 21).FirstOrDefault();
                        if (emailContent != null)
                        {
                            if (entity.LanguageUrl.Value == 1)
                            {
                                if (emailContent.Body != null)
                                    strEmailContentBody = emailContent.Body;
                                if (emailContent.Email != null)
                                    strEmailContentEmail = emailContent.Email;
                                if (emailContent.Subject != null)
                                    strEmailContentSubject = emailContent.Subject;
                            }
                            else
                            {
                                if (emailContent.BodyAr != null)
                                    strEmailContentBody = emailContent.BodyAr;
                                if (emailContent.Email != null)
                                    strEmailContentEmail = emailContent.Email;
                                if (emailContent.SubjectAr != null)
                                    strEmailContentSubject = emailContent.SubjectAr;
                            }
                            strEmailContentAdmin = _appCustomSettings.AdminName;
                        }
                    }
                    #endregion
                    body.Append(BindEmailContent(entity.LanguageUrl.Value, strEmailContentBody, entity, _appCustomSettings.ServerAddressNew, websiteid));
                    //SmtpEmail.SendNetEmail(strEmailContentAdmin, AdminEmailAddress, entity.Email, strEmailContentSubject, body.ToString());
                    _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, entity.Email, strEmailContentSubject, body.ToString(), null);
                    #endregion
                    #region Email to SIBF When Exhibitor Rejected Agency
                    StringBuilder body1 = new StringBuilder();
                    #region Email Content
                    if (websiteid == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        var scrfEmailContent = _context.XsiScrfemailContent.Where(x => x.ItemId == 23).FirstOrDefault();
                        //scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContent(12, entity.LanguageId.Value);
                        if (scrfEmailContent != null)
                        {
                            if (scrfEmailContent.Body != null)
                                strEmailContentBody = scrfEmailContent.Body;
                            if (scrfEmailContent.Email != null)
                                strEmailContentEmail = scrfEmailContent.Email;
                            if (scrfEmailContent.Subject != null)
                                strEmailContentSubject = scrfEmailContent.Subject;

                            strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                        }
                    }
                    else
                    {
                        var emailContent = _context.XsiEmailContent.Where(x => x.ItemId == 23).FirstOrDefault();
                        //emailContent = EmailContentService.GetEmailContent(12, entity.LanguageId.Value);
                        if (emailContent != null)
                        {
                            if (emailContent.Body != null)
                                strEmailContentBody = emailContent.Body;
                            if (emailContent.Email != null)
                                strEmailContentEmail = emailContent.Email;
                            if (emailContent.Subject != null)
                                strEmailContentSubject = emailContent.Subject;

                            strEmailContentAdmin = _appCustomSettings.AdminName;
                        }
                    }
                    #endregion
                    body1.Append(BindEmailContent(entity.LanguageUrl.Value, strEmailContentBody, null, _appCustomSettings.ServerAddressNew, websiteid));
                    body1.Replace("$$AgencyName$$", agencyname);
                    if (strEmailContentEmail != null)
                        _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body1.ToString());
                    //SmtpEmail.SendNetEmail(AdminName, AdminEmailAddress, SIBFEmail, emailContent1.Subject, body1.ToString());
                    #endregion
                }
            }
        }
        void SendEmailAccept(long itemid, string agencyname, string exhibitorName, long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibitionMember entity = new XsiExhibitionMember();
                entity = _context.XsiExhibitionMember.Where(x => x.MemberId == itemid).FirstOrDefault();
                if (entity != default(XsiExhibitionMember))
                {
                    #region Send Accept Email to Agency
                    StringBuilder body = new StringBuilder();
                    string strEmailContentBody = "";
                    string strEmailContentEmail = "";
                    string strEmailContentSubject = "";
                    string strEmailContentAdmin = "";
                    #region Email Content
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        var scrfEmailContent = _context.XsiScrfemailContent.Where(x => x.ItemId == 10041).FirstOrDefault();
                        if (scrfEmailContent != null)
                        {
                            if (entity.LanguageUrl.Value == 1)
                            {
                                if (scrfEmailContent.Body != null)
                                    strEmailContentBody = scrfEmailContent.Body;
                                if (scrfEmailContent.Email != null)
                                    strEmailContentEmail = scrfEmailContent.Email;
                                if (scrfEmailContent.Subject != null)
                                    strEmailContentSubject = scrfEmailContent.Subject;
                            }
                            else
                            {
                                if (scrfEmailContent.BodyAr != null)
                                    strEmailContentBody = scrfEmailContent.BodyAr;
                                if (scrfEmailContent.Email != null)
                                    strEmailContentEmail = scrfEmailContent.Email;
                                if (scrfEmailContent.SubjectAr != null)
                                    strEmailContentSubject = scrfEmailContent.SubjectAr;
                            }
                            strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                        }
                    }
                    else
                    {
                        var emailContent = _context.XsiEmailContent.Where(x => x.ItemId == 10041).FirstOrDefault();
                        if (emailContent != null)
                        {
                            if (entity.LanguageUrl.Value == 1)
                            {
                                if (emailContent.Body != null)
                                    strEmailContentBody = emailContent.Body;
                                if (emailContent.Email != null)
                                    strEmailContentEmail = emailContent.Email;
                                if (emailContent.Subject != null)
                                    strEmailContentSubject = emailContent.Subject;
                            }
                            else
                            {
                                if (emailContent.BodyAr != null)
                                    strEmailContentBody = emailContent.BodyAr;
                                if (emailContent.Email != null)
                                    strEmailContentEmail = emailContent.Email;
                                if (emailContent.SubjectAr != null)
                                    strEmailContentSubject = emailContent.SubjectAr;
                            }

                            strEmailContentAdmin = _appCustomSettings.AdminName;
                        }
                    }
                    #endregion
                    body.Append(BindEmailContent(entity.LanguageUrl.Value, strEmailContentBody, entity, _appCustomSettings.ServerAddressNew, websiteId));
                    body.Replace("$$ExhibitorName$$", exhibitorName);
                    _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, entity.Email, strEmailContentSubject, body.ToString());
                    #endregion
                }
            }
        }
        private StringBuilder BindEmailContent(long languageId, string bodyContent, XsiExhibitionMember entity, string ServerAddress, long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string scrfURL = _appCustomSettings.ServerAddressSCRF;
                string ServerAddressCMS = _appCustomSettings.ServerAddressCMS;
                string EmailContent = string.Empty;
                if (languageId == 1)
                {
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                        EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRF.html";
                    else
                        EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
                }
                else
                {
                    if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                        EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRF.html";
                    else
                        EmailContent = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
                }
                StringBuilder body = new StringBuilder();

                if (System.IO.File.Exists(EmailContent))
                {
                    body.Append(System.IO.File.ReadAllText(EmailContent));
                    body.Replace("$$EmailContent$$", bodyContent);
                }
                else
                    body.Append(bodyContent);
                string str = MethodFactory.GetExhibitionDetails(websiteId, languageId, _context);
                string[] strArray = str.Split(',');
                body.Replace("$$exhibitionno$$", strArray[0].ToString());
                body.Replace("$$exhibitiondate$$", strArray[1].ToString());
                body.Replace("$$exhyear$$", strArray[2]);

                string swidth = "width=\"864\"";
                string sreplacewidth = "width=\"100%\"";
                body.Replace(swidth, sreplacewidth);

                body.Replace("$$ServerAddressCMS$$", ServerAddressCMS);
                body.Replace("$$ServerAddress$$", ServerAddress);
                if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    body.Replace("$$ServerAddressSCRF$$", scrfURL);

                if (entity != null)
                {
                    string exhibitionmembername = string.Empty;
                    if (languageId == 1)
                    {
                        if (entity.Firstname != null && entity.LastName != null)
                            exhibitionmembername = entity.Firstname + " " + entity.LastName;
                        else if (entity.Firstname != null)
                            exhibitionmembername = entity.Firstname;
                        else if (entity.LastName != null)
                            exhibitionmembername = entity.LastName;
                    }
                    else
                    {
                        if (entity.FirstNameAr != null && entity.LastNameAr != null)
                            exhibitionmembername = entity.FirstNameAr + " " + entity.LastNameAr;
                        else if (entity.Firstname != null)
                            exhibitionmembername = entity.FirstNameAr;
                        else if (entity.LastName != null)
                            exhibitionmembername = entity.LastNameAr;

                        if (string.IsNullOrEmpty(exhibitionmembername))
                        {
                            if (entity.Firstname != null && entity.LastName != null)
                                exhibitionmembername = entity.Firstname + " " + entity.LastName;
                            else if (entity.Firstname != null)
                                exhibitionmembername = entity.Firstname;
                            else if (entity.LastName != null)
                                exhibitionmembername = entity.LastName;
                        }
                    }
                    body.Replace("$$Name$$", exhibitionmembername);
                }
                return body;
            }
        }
        #endregion
    }
}

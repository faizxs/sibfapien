﻿using Contracts;
using Entities.Models;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using OfficeOpenXml;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xsi.ServicesLayer;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BooksAddController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly IEmailSender _emailSender;
        List<XsiExhibitionBookPublisher> PublisherList;
        List<XsiExhibitionAuthor> AuthorList;
        List<XsiExhibitionLanguage> BookLanguageList;
        List<XsiExhibitionBookSubject> BookSubjectList;
        List<XsiExhibitionBookSubsubject> BookSubsubjectList;

        public BooksAddController(IOptions<AppCustomSettings> appCustomSettings, ILoggerManager logger, IEmailSender emailSender)
        {
            this._appCustomSettings = appCustomSettings.Value;
            _logger = logger;
            _emailSender = emailSender;
        }
        #region Get Methods
        [HttpGet]
        [Route("{websiteid}")]
        public async Task<ActionResult<dynamic>> GetAddBooks(long websiteid)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);
                long memberid = User.Identity.GetID();
                if (memberid > 0)
                {
                    using (sibfnewdbContext _context = new sibfnewdbContext())
                    {
                        var BookParticipationg = "/en/BooksParticipating/" + websiteid;
                        var BookExhibitionId = GetExhibitionGroupId(websiteid);
                        long AgencyId = 0, ExhibitorId = 0;
                        if (BookExhibitionId != "-1")
                        {
                            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
                            {
                                XsiExhibitionMemberApplicationYearly entity = AgencyRegistrationService.GetAgencyRegistration(new XsiExhibitionMemberApplicationYearly { MemberId = memberid, IsActive = EnumConversion.ToString(EnumBool.Yes), ExhibitionId = Convert.ToInt64(BookExhibitionId), Status = EnumConversion.ToString(EnumAgencyStatus.SIBFApproved) }).FirstOrDefault();
                                if (entity != null)
                                {
                                    AgencyId = entity.MemberExhibitionYearlyId;
                                    if (entity.ParentId != null)
                                        ExhibitorId = entity.ParentId.Value;
                                }
                            }
                            if (AgencyId == -1)
                                ExhibitorId = MethodFactory.GetExhibitorByMember(memberid, websiteid, _context);
                        }
                        if (AgencyId < 0 && ExhibitorId < 0)
                            return Ok("/en/MemberDashboard/" + websiteid);
                        var dto = new { agencyid = AgencyId, exhibitorid = ExhibitorId };
                        return Ok(dto);
                    }

                }
                return Ok("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiCareers action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [Route("bookdetails/{bookid}/{exhibitorid}")]
        public async Task<ActionResult<dynamic>> GetBookDetails(long bookid, long exhibitorid)
        {
            try
            {
                long langId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out langId);
                long memberid = User.Identity.GetID();
                if (memberid > 0)
                {
                    using (ExhibitionBookService BookService = new ExhibitionBookService())
                    {
                        string isparticipating = "N";
                        XsiExhibitionBooks existingEntity = BookService.GetBookByItemId(bookid);

                        if (existingEntity != null)
                        {
                            BooksParticipatingService BooksParticipatingService = new BooksParticipatingService();
                            var booksParticipating = BooksParticipatingService
                                .GetBooksParticipating(new XsiExhibitionBookParticipating()
                                { ExhibitorId = exhibitorid, BookId = bookid }).FirstOrDefault();
                            if (booksParticipating != null)
                            {
                                isparticipating = booksParticipating != null ? "Y" : "N";

                                XsiBookComplete entity = new XsiBookComplete();
                                entity.ItemId = existingEntity.BookId;
                                //  entity.PublisherId = existingEntity.BookPublisherId;

                                entity.ExhibitorId = booksParticipating.ExhibitorId;
                                entity.AgencyId = booksParticipating.AgencyId;
                                entity.BookLanguageId = existingEntity.BookLanguageId;
                                entity.ExhibitionSubjectId = existingEntity.ExhibitionSubjectId;
                                entity.ExhibitionSubsubjectId = existingEntity.ExhibitionSubsubjectId;
                                entity.ExhibitionCurrencyId = existingEntity.ExhibitionCurrencyId;
                                entity.BookTypeId = existingEntity.BookTypeId;
                                entity.Thumbnail = existingEntity.Thumbnail;
                                entity.IsAvailableOnline = existingEntity.IsAvailableOnline;
                                entity.IsNew = existingEntity.IsNew;
                                entity.IsBestSeller = existingEntity.IsBestSeller;
                                entity.TitleEn = existingEntity.TitleEn;
                                entity.TitleAr = existingEntity.TitleAr;
                                entity.ISBN = existingEntity.Isbn;
                                entity.IssueYear = existingEntity.IssueYear;
                                entity.Price = existingEntity.Price;
                                entity.PriceBeforeDiscount = existingEntity.PriceBeforeDiscount;
                                entity.BookDetails = existingEntity.BookDetails;
                                entity.BookDetailsAr = existingEntity.BookDetailsAr;
                                entity.IsParticipating = isparticipating;

                                #region Publisher Name
                                if (langId == 1)
                                    entity.PublisherName = existingEntity.BookPublisherName;
                                else
                                    entity.PublisherName = existingEntity.BookPublisherNameAr;

                                //if (existingEntity.BookPublisherId != null)
                                //    if (existingEntity.BookPublisherId != -1)
                                //    {
                                //        ExhibitionPublisherService ExhibitionPublisherService =
                                //            new ExhibitionPublisherService();
                                //        XsiExhibitionBookPublisher PublisherEntity =
                                //            ExhibitionPublisherService.GetExhibitionPublisherByItemId(existingEntity
                                //                .BookPublisherId.Value);
                                //        if (langId == 1)
                                //        {
                                //            if (PublisherEntity != null)
                                //                if (PublisherEntity.Title != null)
                                //                    entity.PublisherName = PublisherEntity.Title;
                                //                else
                                //                    entity.PublisherName = string.Empty;
                                //            else
                                //                entity.PublisherName = string.Empty;
                                //        }
                                //        else
                                //        {
                                //            if (PublisherEntity != null)
                                //                if (PublisherEntity.TitleAr != null)
                                //                    entity.PublisherName = PublisherEntity.TitleAr;
                                //                else
                                //                    entity.PublisherName = string.Empty;
                                //            else
                                //                entity.PublisherName = string.Empty;
                                //        }
                                //    }
                                //    else
                                //        entity.PublisherName = string.Empty;
                                //else
                                //    entity.PublisherName = string.Empty;

                                #endregion

                                #region Author Name
                                if (langId == 1)
                                    entity.AuthorName = existingEntity.AuthorName;
                                else
                                    entity.AuthorName = existingEntity.AuthorNameAr;
                                //  using (sibfnewdbContext _context = new sibfnewdbContext())
                                // {
                                //var bookauthorEntity = _context.XsiExhibitionBookAuthor
                                //    .Where(x => x.BookId == existingEntity.BookId).OrderByDescending(x => x.BookId)
                                //    .FirstOrDefault();
                                //if (bookauthorEntity != null && bookauthorEntity.AuthorId != -1)
                                //{
                                //    //XsiExhibitionAuthor AuthorEntity = _context.XsiExhibitionAuthor.Where(i => i.AuthorId == bookauthorEntity.AuthorId).FirstOrDefault();
                                //    //entity.AuthorId = bookauthorEntity.AuthorId;
                                //    //if (langId == 1)
                                //    //{
                                //    //    if (AuthorEntity != null && AuthorEntity.Title != null)
                                //    //        entity.AuthorName = AuthorEntity.Title;
                                //    //    else
                                //    //        entity.AuthorName = string.Empty;
                                //    //}
                                //    //else
                                //    //{
                                //    //    if (AuthorEntity != null && AuthorEntity.TitleAr != null)
                                //    //        entity.AuthorName = AuthorEntity.TitleAr;
                                //    //    else
                                //    //        entity.AuthorName = string.Empty;
                                //    //}
                                //}
                                //else
                                //    entity.AuthorName = string.Empty;
                                //   }
                                #endregion

                                return Ok(entity);
                            }
                            else
                            {
                                XsiBookComplete entity = new XsiBookComplete();
                                entity.ItemId = existingEntity.BookId;
                                // entity.PublisherId = existingEntity.BookPublisherId;
                                entity.ExhibitorId = exhibitorid;
                                //entity.AgencyId = booksParticipating.AgencyId;
                                entity.BookLanguageId = existingEntity.BookLanguageId;
                                entity.ExhibitionSubjectId = existingEntity.ExhibitionSubjectId;
                                entity.ExhibitionSubsubjectId = existingEntity.ExhibitionSubsubjectId;
                                entity.ExhibitionCurrencyId = existingEntity.ExhibitionCurrencyId;
                                entity.BookTypeId = existingEntity.BookTypeId;
                                entity.Thumbnail = existingEntity.Thumbnail;
                                entity.IsAvailableOnline = existingEntity.IsAvailableOnline;
                                entity.IsNew = existingEntity.IsNew;
                                entity.IsBestSeller = existingEntity.IsBestSeller;
                                entity.TitleEn = existingEntity.TitleEn;
                                entity.TitleAr = existingEntity.TitleAr;
                                entity.ISBN = existingEntity.Isbn;
                                entity.IssueYear = existingEntity.IssueYear;
                                entity.Price = existingEntity.Price;
                                entity.PriceBeforeDiscount = existingEntity.PriceBeforeDiscount;
                                entity.BookDetails = existingEntity.BookDetails;
                                entity.BookDetailsAr = existingEntity.BookDetailsAr;
                                entity.IsParticipating = isparticipating;

                                #region Publisher Name
                                if (langId == 1)
                                    entity.PublisherName = existingEntity.BookPublisherName;
                                else
                                    entity.PublisherName = existingEntity.BookPublisherNameAr;

                                //if (existingEntity.BookPublisherId != null)
                                //    if (existingEntity.BookPublisherId != -1)
                                //    {
                                //        ExhibitionPublisherService ExhibitionPublisherService =
                                //            new ExhibitionPublisherService();
                                //        XsiExhibitionBookPublisher PublisherEntity =
                                //            ExhibitionPublisherService.GetExhibitionPublisherByItemId(existingEntity
                                //                .BookPublisherId.Value);
                                //        if (langId == 1)
                                //        {
                                //            if (PublisherEntity != null)
                                //                if (PublisherEntity.Title != null)
                                //                    entity.PublisherName = PublisherEntity.Title;
                                //                else
                                //                    entity.PublisherName = string.Empty;
                                //            else
                                //                entity.PublisherName = string.Empty;
                                //        }
                                //        else
                                //        {
                                //            if (PublisherEntity != null)
                                //                if (PublisherEntity.TitleAr != null)
                                //                    entity.PublisherName = PublisherEntity.TitleAr;
                                //                else
                                //                    entity.PublisherName = string.Empty;
                                //            else
                                //                entity.PublisherName = string.Empty;
                                //        }
                                //    }
                                //    else
                                //        entity.PublisherName = string.Empty;
                                //else
                                //    entity.PublisherName = string.Empty;

                                #endregion

                                #region Author Name
                                if (langId == 1)
                                    entity.AuthorName = existingEntity.AuthorName;
                                else
                                    entity.AuthorName = existingEntity.AuthorNameAr;
                                //using (sibfnewdbContext _context = new sibfnewdbContext())
                                //{
                                //    var bookauthorEntity = _context.XsiExhibitionBookAuthor
                                //        .Where(x => x.BookId == existingEntity.BookId).OrderByDescending(x => x.BookId)
                                //        .FirstOrDefault();
                                //    if (bookauthorEntity != null && bookauthorEntity.AuthorId != -1)
                                //    {
                                //        XsiExhibitionAuthor AuthorEntity = _context.XsiExhibitionAuthor.Where(i => i.AuthorId == bookauthorEntity.AuthorId).FirstOrDefault();
                                //        entity.AuthorId = bookauthorEntity.AuthorId;
                                //        if (langId == 1)
                                //        {
                                //            if (AuthorEntity != null && AuthorEntity.Title != null)
                                //                entity.AuthorName = AuthorEntity.Title;
                                //            else
                                //                entity.AuthorName = string.Empty;
                                //        }
                                //        else
                                //        {
                                //            if (AuthorEntity != null && AuthorEntity.TitleAr != null)
                                //                entity.AuthorName = AuthorEntity.TitleAr;
                                //            else
                                //                entity.AuthorName = string.Empty;
                                //        }
                                //    }
                                //    else
                                //        entity.AuthorName = string.Empty;
                                //}
                                #endregion

                                return Ok(entity);
                            }
                        }
                    }
                }
                return Ok("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBookDetails action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("publisher/{publisherterm}")]
        public async Task<ActionResult<dynamic>> GetPublishers(string publisherterm)
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);
            List<CategoryDTO> publisherList = new List<CategoryDTO>();
            using (ExhibitionPublisherService ExhibitionPublisherService = new ExhibitionPublisherService())
            {
                XsiExhibitionBookPublisher where = new XsiExhibitionBookPublisher();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                //where.Status = EnumConversion.ToString(EnumPublisherStatus.Approved);
                List<XsiExhibitionBookPublisher> PublisherList = ExhibitionPublisherService.GetExhibitionPublisher(where, EnumSortlistBy.ByAlphabetAsc);
                if (langId == 1)
                {
                    if (PublisherList.Count() > 0)
                        publisherList = PublisherList.Where(x => x.Title != null && x.Title.ToLower().Contains(publisherterm)).Select(x => new CategoryDTO { Title = x.Title, ItemId = x.ItemId }).OrderBy(x => x.Title.Trim()).ToList();
                }
                else
                {
                    if (PublisherList.Count() > 0)
                        publisherList = PublisherList.Where(x => x.TitleAr != null && x.TitleAr.ToLower().Contains(publisherterm)).Select(x => new CategoryDTO { Title = x.TitleAr, ItemId = x.ItemId }).OrderBy(x => x.Title.Trim()).ToList();
                }
            }

            return Ok(publisherList);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("publishernew")]
        public async Task<ActionResult<dynamic>> GetPublishernew()
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);
            List<CategoryDTO> publisherList = new List<CategoryDTO>();
            using (ExhibitionPublisherService ExhibitionPublisherService = new ExhibitionPublisherService())
            {
                XsiExhibitionBookPublisher where = new XsiExhibitionBookPublisher();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                List<XsiExhibitionBookPublisher> PublisherList = ExhibitionPublisherService.GetExhibitionPublisher(where, EnumSortlistBy.ByAlphabetAsc);

                if (langId == 1)
                {
                    if (PublisherList.Count() > 0)
                        publisherList = PublisherList.Where(x => x.Title != null).Select(x => new CategoryDTO { Title = x.Title, ItemId = x.ItemId }).OrderBy(x => x.Title.Trim()).ToList();
                }
                else
                {
                    if (PublisherList.Count() > 0)
                        publisherList = PublisherList.Where(x => x.TitleAr != null).Select(x => new CategoryDTO { Title = x.TitleAr, ItemId = x.ItemId }).OrderBy(x => x.Title.Trim()).ToList();
                }
            }
            return Ok(publisherList);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("author/{authorterm}")]
        public async Task<ActionResult<dynamic>> GetAuthors(string authorterm)
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);
            List<CategoryDTO> authorList = new List<CategoryDTO>();
            using (ExhibitionAuthorService ExhibitionAuthorService = new ExhibitionAuthorService())
            {
                XsiExhibitionAuthor where = new XsiExhibitionAuthor();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                //where.Status = EnumConversion.ToString(EnumAuthorStatus.Approved);
                List<XsiExhibitionAuthor> AuthorList = ExhibitionAuthorService.GetExhibitionAuthor(where, EnumSortlistBy.ByAlphabetAsc);
                if (langId == 1)
                {
                    if (AuthorList.Count() > 0)
                        authorList = AuthorList.Where(x => x.Title != null && x.Title.ToLower().Contains(authorterm)).Select(x => new CategoryDTO { Title = x.Title, ItemId = x.AuthorId }).OrderBy(x => x.Title.Trim()).ToList();
                }
                else
                {
                    if (AuthorList.Count() > 0)
                        authorList = AuthorList.Where(x => x.TitleAr != null && x.TitleAr.ToLower().Contains(authorterm)).Select(x => new CategoryDTO { Title = x.TitleAr, ItemId = x.AuthorId }).OrderBy(x => x.Title.Trim()).ToList();
                }
            }
            return Ok(authorList);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("authornew")]
        public async Task<ActionResult<dynamic>> GetAuthornew()
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);
            List<CategoryDTO> authorList = new List<CategoryDTO>();
            using (ExhibitionAuthorService ExhibitionAuthorService = new ExhibitionAuthorService())
            {
                XsiExhibitionAuthor where = new XsiExhibitionAuthor();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                List<XsiExhibitionAuthor> AuthorList = ExhibitionAuthorService.GetExhibitionAuthor(where, EnumSortlistBy.ByAlphabetAsc);

                if (langId == 1)
                {
                    if (AuthorList.Count() > 0)
                        authorList = AuthorList.Where(x => x.Title != null).Select(x => new CategoryDTO { Title = x.Title, ItemId = x.AuthorId }).OrderBy(x => x.Title.Trim()).ToList();
                }
                else
                {
                    if (AuthorList.Count() > 0)
                        authorList = AuthorList.Where(x => x.TitleAr != null).Select(x => new CategoryDTO { Title = x.TitleAr, ItemId = x.AuthorId }).OrderBy(x => x.Title.Trim()).ToList();
                }
            }
            return Ok(authorList);
        }

        [HttpGet]
        [Route("langauage")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> GetLanguage()
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);
            List<CategoryDTO> languageList = new List<CategoryDTO>();
            using (ExhibitionLanguageService ExhibitionLanguageService = new ExhibitionLanguageService())
            {
                XsiExhibitionLanguage where = new XsiExhibitionLanguage();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                List<XsiExhibitionLanguage> LanguageList = ExhibitionLanguageService.GetExhibitionLanguage(where);

                if (langId == 1)
                {
                    if (LanguageList.Count() > 0)
                    {
                        languageList = LanguageList.Select(s => new CategoryDTO { ItemId = s.ItemId, Title = s.Title }).OrderBy(x => x.Title.Trim()).ToList();
                        CategoryDTO categoryentity = languageList.Where(i => i.ItemId == 1).FirstOrDefault();
                        languageList.Remove(categoryentity);

                        categoryentity = languageList.Where(i => i.ItemId == 3).FirstOrDefault();
                        languageList.Remove(categoryentity);

                        languageList.Insert(0, new CategoryDTO() { ItemId = 3, Title = "English" });
                        languageList.Insert(1, new CategoryDTO() { ItemId = 1, Title = "Arabic" });
                    }
                }
                else
                {
                    if (LanguageList.Count() > 0)
                    {
                        languageList = LanguageList.Select(s => new CategoryDTO { ItemId = s.ItemId, Title = s.TitleAr }).OrderBy(x => x.Title.Trim()).ToList();
                        CategoryDTO categoryentity = languageList.Where(i => i.ItemId == 1).FirstOrDefault();
                        languageList.Remove(categoryentity);

                        categoryentity = languageList.Where(i => i.ItemId == 3).FirstOrDefault();
                        languageList.Remove(categoryentity);

                        languageList.Insert(0, new CategoryDTO() { ItemId = 3, Title = "الإنجليزية" });
                        languageList.Insert(1, new CategoryDTO() { ItemId = 1, Title = "العربية" });
                    }
                }
            }
            return Ok(languageList);
        }

        [HttpGet]
        [Route("currency")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> GetCurrency()
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);
            List<CategoryDTO> currencyList = new List<CategoryDTO>();
            using (ExhibitionCurrencyService ExhibitionCurrencyService = new ExhibitionCurrencyService())
            {
                XsiExhibitionCurrency where = new XsiExhibitionCurrency();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                List<XsiExhibitionCurrency> CurrencyList = ExhibitionCurrencyService.GetExhibitionCurrency(where);
                if (langId == 1)
                {
                    if (CurrencyList.Count() > 0)
                        currencyList = CurrencyList.Select(s => new CategoryDTO { ItemId = s.ItemId, Title = s.Title }).OrderBy(x => x.Title.Trim()).ToList();
                }
                else
                {
                    if (CurrencyList.Count() > 0)
                        currencyList = CurrencyList.Select(s => new CategoryDTO { ItemId = s.ItemId, Title = s.TitleAr }).OrderBy(x => x.Title.Trim()).ToList();
                }
            }
            return Ok(currencyList);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("booktype")]
        public async Task<ActionResult<dynamic>> GetBookType()
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);
            List<CategoryDTO> bookTypeList = new List<CategoryDTO>();
            using (ExhibitionBookTypeService ExhibitionBookTypeService = new ExhibitionBookTypeService())
            {
                XsiExhibitionBookType where = new XsiExhibitionBookType();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                List<XsiExhibitionBookType> BookTypeList = ExhibitionBookTypeService.GetExhibitionBookType(where);
                if (langId == 1)
                {
                    if (BookTypeList.Count() > 0)
                        bookTypeList = BookTypeList.Select(s => new CategoryDTO { ItemId = s.ItemId, Title = s.Title }).OrderBy(x => x.Title.Trim()).ToList();
                }
                else
                {
                    if (BookTypeList.Count() > 0)
                        bookTypeList = BookTypeList.Select(s => new CategoryDTO { ItemId = s.ItemId, Title = s.TitleAr }).OrderBy(x => x.Title.Trim()).ToList();
                }
            }
            return Ok(bookTypeList);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("mainsubject")]
        public async Task<ActionResult<dynamic>> GetMainSubject()
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);
            List<CategoryDTO> mainSubjectList = new List<CategoryDTO>();
            using (ExhibitionBookSubjectService ExhibitionBookSubjectService = new ExhibitionBookSubjectService())
            {
                XsiExhibitionBookSubject where = new XsiExhibitionBookSubject();
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                List<XsiExhibitionBookSubject> MainSubjectList = ExhibitionBookSubjectService.GetExhibitionBookSubject(where);
                if (langId == 1)
                {
                    if (MainSubjectList.Count() > 0)
                        mainSubjectList = MainSubjectList.Select(s => new CategoryDTO { ItemId = s.ItemId, Title = s.Title }).OrderBy(x => x.Title.Trim()).ToList();
                }
                else
                {
                    if (MainSubjectList.Count() > 0)
                        mainSubjectList = MainSubjectList.Select(s => new CategoryDTO { ItemId = s.ItemId, Title = s.TitleAr }).OrderBy(x => x.Title.Trim()).ToList();
                }
            }
            return Ok(mainSubjectList);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("subsubject/{categoryId}")]
        public async Task<ActionResult<dynamic>> GetSubSubject(long categoryId)
        {
            long langId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out langId);
            List<CategoryDTO> subSubjectList = new List<CategoryDTO>();
            using (ExhibitionBookSubjectService ExhibitionBookSubjectService = new ExhibitionBookSubjectService())
            {
                ExhibitionBookSubsubjectService ExhibitionBookSubsubjectService = new ExhibitionBookSubsubjectService();
                {
                    XsiExhibitionBookSubsubject where = new XsiExhibitionBookSubsubject();
                    where.CategoryId = categoryId;
                    where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    List<XsiExhibitionBookSubsubject> SubsubjectList = ExhibitionBookSubsubjectService.GetExhibitionBookSubsubject(where);
                    if (langId == 1)
                    {
                        if (SubsubjectList.Count() > 0)
                            subSubjectList = SubsubjectList.Select(x => new CategoryDTO { ItemId = x.ItemId, Title = x.Title }).OrderBy(x => x.Title.Trim()).ToList();
                    }
                    else
                    {
                        if (SubsubjectList.Count() > 0)
                            subSubjectList = SubsubjectList.Select(x => new CategoryDTO { ItemId = x.ItemId, Title = x.TitleAr }).OrderBy(x => x.Title.Trim()).ToList();
                    }
                }
            }
            return Ok(subSubjectList);
        }
        #endregion
        #region Post Methods
        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("addbooks")]
        public ActionResult<dynamic> PostAddBooks(List<BooksAddDTO> model)
        {
            try
            {
                bool isupdated = false;
                long memberid = User.Identity.GetID();
                if (memberid > 0)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    #region Book Code
                    bool isBookSave = false;
                    foreach (var item in model)
                    {
                        if (memberid == item.MemberId)
                        {
                            if (item.IsBookSave)
                            {
                                isBookSave = true;
                                #region Save Book
                                SaveBookData(item, memberid, LangId);
                                #endregion
                            }
                            else
                            {
                                #region Update Book
                                isupdated = UpdateBookData(item, memberid, LangId);
                                #endregion
                            }
                        }
                        else
                        {
                            return Unauthorized();
                        }
                    }
                    if (isBookSave)
                        return Ok("Book(s) Added Successfuly.");
                    else
                    {
                        if (isupdated)
                            return Ok("Book Updated Successfuly.");
                        else
                            return Ok("Something went wrong. Please retry again later.");
                    }

                    #endregion
                }
                return Unauthorized();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                //return StatusCode(500, ex.ToString());
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                //return StatusCode(500, ex.ToString());
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpPost]
        [Route("addpublisher")]
        [AllowAnonymous]
        public ActionResult<dynamic> PostAddPublisher(PublisherDTO model)
        {
            try
            {
                long memberid = User.Identity.GetID();
                if (memberid == model.MemberId)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    long count = 0;
                    using (ExhibitionPublisherService ExhibitionPublisherService = new ExhibitionPublisherService())
                    {

                        #region Count                        
                        List<XsiExhibitionBookPublisher> PublisherList = ExhibitionPublisherService.GetExhibitionPublisher();
                        if (!string.IsNullOrEmpty(model.PublisherEn) && !string.IsNullOrEmpty(model.PublisherAr))
                            count = ExhibitionPublisherService.GetExhibitionPublisher().Where(p => p.Title == model.PublisherEn || p.TitleAr == model.PublisherAr).Count();
                        else
                        {
                            if (!string.IsNullOrEmpty(model.PublisherEn))
                                count = ExhibitionPublisherService.GetExhibitionPublisher().Where(p => p.Title == model.PublisherEn).Count();
                            else if (!string.IsNullOrEmpty(model.PublisherAr))
                                count = ExhibitionPublisherService.GetExhibitionPublisher().Where(p => p.TitleAr == model.PublisherAr).Count();
                        }
                        #endregion
                        if (count == 0)
                        {
                            XsiExhibitionBookPublisher entity = new XsiExhibitionBookPublisher();
                            entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                            entity.CreatedBy = model.MemberId;
                            entity.CreatedOn = MethodFactory.ArabianTimeNow();
                            entity.ModifiedBy = model.MemberId;
                            entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                            entity.Title = model.PublisherEn;
                            entity.TitleAr = model.PublisherAr;
                            entity.Status = EnumConversion.ToString(EnumPublisherStatus.New);
                            if (ExhibitionPublisherService.InsertExhibitionPublisher(entity) == EnumResultType.Success)
                                return Ok("Publisher's name was successfully added, it will only be displayed on the list after admin approval.");
                            else
                                return Ok("Error (Adding a publisher): Please try again. ");
                        }
                        else
                            return Ok("Publisher already exist.");
                    }
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpPost]
        [Route("addauthor")]
        [AllowAnonymous]
        public ActionResult<dynamic> PostAddAuthor(AuthorPostDTO model)
        {
            try
            {
                long memberid = User.Identity.GetID();
                if (memberid == model.MemberId)
                {
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    long count = 0;
                    using (ExhibitionAuthorService ExhibitionAuthorService = new ExhibitionAuthorService())
                    {

                        #region Count                        
                        List<XsiExhibitionAuthor> AuthorList = ExhibitionAuthorService.GetExhibitionAuthor();
                        if (!string.IsNullOrEmpty(model.AuthorEn) && !string.IsNullOrEmpty(model.AuthorAr))
                            count = ExhibitionAuthorService.GetExhibitionAuthor().Where(p => p.Title == model.AuthorEn || p.TitleAr == model.AuthorAr).Count();
                        else
                        {
                            if (!string.IsNullOrEmpty(model.AuthorEn))
                                count = ExhibitionAuthorService.GetExhibitionAuthor().Where(p => p.Title == model.AuthorEn).Count();
                            else if (!string.IsNullOrEmpty(model.AuthorAr))
                                count = ExhibitionAuthorService.GetExhibitionAuthor().Where(p => p.TitleAr == model.AuthorAr).Count();
                        }
                        #endregion
                        if (count == 0)
                        {
                            XsiExhibitionAuthor entity = new XsiExhibitionAuthor();
                            entity.LanguageId = LangId;
                            entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                            entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes);
                            entity.CreatedBy = memberid;
                            entity.CreatedOn = MethodFactory.ArabianTimeNow();
                            entity.ModifiedBy = memberid;
                            entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                            entity.Title = model.AuthorEn;
                            entity.TitleAr = model.AuthorAr;
                            entity.Status = EnumConversion.ToString(EnumPublisherStatus.New);
                            if (ExhibitionAuthorService.InsertExhibitionAuthor(entity) == EnumResultType.Success)
                                return Ok("Author name has been successfully added and will be displayed in the author\'s list after admin approval. ");
                            else
                                return Ok("Failure to add author, please try again. ");
                        }
                        else
                            return Ok("Author already exists.");
                    }
                }
                return Ok("Unauthorized access");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");

            }
        }

        [HttpPost()]
        [Route("import")]
        public async Task<ActionResult<dynamic>> PostImport(ImportExcelDTO dataobj)
        {
            try
            {
                long memberid = User.Identity.GetID();
                if (memberid > 0)
                {
                    long bookAdded = 0;
                    long LangId = 1;
                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    if (dataobj.excelFileBase64 != null && dataobj.excelFileBase64.Length > 0)
                    {
                        string fileName = string.Empty;
                        byte[] imageBytes;
                        if (dataobj.excelFileBase64.Contains("data:"))
                        {
                            var strInfo = dataobj.excelFileBase64.Split(",")[0];
                            imageBytes = Convert.FromBase64String(dataobj.excelFileBase64.Split(',')[1]);
                        }
                        else
                        {
                            imageBytes = Convert.FromBase64String(dataobj.excelFileBase64);
                        }
                        using (sibfnewdbContext _context = new sibfnewdbContext())
                        {
                            using (var transaction = _context.Database.BeginTransaction())
                            {
                                using (var stream = new MemoryStream(imageBytes))
                                {
                                    using (var package = new ExcelPackage(stream))
                                    {
                                        #region Get Dependency List
                                        // PublisherList = GetExhibitionBookPublisherList();
                                        //AuthorList = GetExhibitionAuthorList();
                                        BookLanguageList = GetExhibitionLanguageList();
                                        BookSubjectList = GetExhibitionBookSubjectList();
                                        BookSubsubjectList = GetExhibitionBookSubsubjectList();
                                        #endregion

                                        ExcelWorksheet worksheet = package.Workbook.Worksheets.ToList()[0];
                                        var rowCount = worksheet.Dimension.Rows;
                                        long agencyId = -1;
                                        //long bookAuthorId = -1;
                                        //long bookPublisherId = -1;
                                        long bookLanguageId = -1;
                                        long bookSubjectId = -1;
                                        long bookSubSubjectId = -1;

                                        var exhibitorId = GetExhibitorByMember(dataobj.MemberId, dataobj.WebsiteId);
                                        if (exhibitorId != -1)
                                            agencyId = GetAgencyByMember(dataobj.MemberId, exhibitorId);
                                        for (int row = 2; row <= rowCount; row++)
                                        {
                                            agencyId = -1;
                                            bookLanguageId = -1;
                                            bookSubjectId = -1;
                                            bookSubSubjectId = -1;
                                            if (worksheet.Cells[row, 1].Value != null && worksheet.Cells[row, 2].Value != null && worksheet.Cells[row, 3].Value != null && worksheet.Cells[row, 4].Value != null && worksheet.Cells[row, 5].Value != null && worksheet.Cells[row, 6].Value != null && worksheet.Cells[row, 7].Value != null && worksheet.Cells[row, 8].Value != null)
                                            {
                                                XsiExhibitionBooks books = new XsiExhibitionBooks();

                                                if (worksheet.Cells[row, 1].Value != null)
                                                    books.TitleEn = worksheet.Cells[row, 1].Value.ToString().Trim();

                                                if (!string.IsNullOrEmpty(worksheet.Cells[row, 2].Text.Trim()))
                                                {
                                                    //if (LangId == 1)
                                                    //{

                                                    //}
                                                    books.BookPublisherName = worksheet.Cells[row, 2].Text.Trim();
                                                    books.BookPublisherNameAr = worksheet.Cells[row, 2].Text.Trim();

                                                    //XsiExhibitionBookPublisher entity = new XsiExhibitionBookPublisher();
                                                    //if (LangId == 1)
                                                    //    entity = PublisherList.Where(i => i.Title == worksheet.Cells[row, 2].Text.Trim()).FirstOrDefault();
                                                    //else
                                                    //    entity = PublisherList.Where(i => i.TitleAr == worksheet.Cells[row, 2].Text.Trim()).FirstOrDefault();
                                                    //if (entity != null)
                                                    //{
                                                    //    bookPublisherId = entity.ItemId;
                                                    //}
                                                    //books.BookPublisherId = bookPublisherId;
                                                }
                                                if (!string.IsNullOrEmpty(worksheet.Cells[row, 3].Text.ToString().Trim()))
                                                {
                                                    //  if (LangId == 1)

                                                    books.AuthorName = worksheet.Cells[row, 3].Text.Trim();
                                                    books.AuthorNameAr = worksheet.Cells[row, 3].Text.Trim();

                                                    //XsiExhibitionAuthor authorrEntity = new XsiExhibitionAuthor();
                                                    //if (LangId == 1)
                                                    //    authorrEntity = AuthorList.Where(i => i.Title == worksheet.Cells[row, 3].Value.ToString().Trim()).FirstOrDefault();
                                                    //else
                                                    //    authorrEntity = AuthorList.Where(i => i.TitleAr == worksheet.Cells[row, 3].Value.ToString().Trim()).FirstOrDefault();
                                                    //if (authorrEntity != null)
                                                    //{
                                                    //    bookAuthorId = authorrEntity.AuthorId;
                                                    //}
                                                }

                                                if (worksheet.Cells[row, 4].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 4].Value.ToString().Trim()))
                                                {
                                                    string subject = worksheet.Cells[row, 4].Value.ToString().Trim().Split('_')[0];
                                                    XsiExhibitionBookSubject entity = new XsiExhibitionBookSubject();
                                                    if (LangId == 1)
                                                        entity = BookSubjectList.Where(i => i.Title.Trim() == subject).FirstOrDefault();
                                                    else
                                                        entity = BookSubjectList.Where(i => i.TitleAr.Trim() == subject).FirstOrDefault();
                                                    if (entity != null)
                                                    {
                                                        bookSubjectId = entity.ItemId;
                                                    }
                                                    if (bookSubjectId > 0)
                                                        books.ExhibitionSubjectId = bookSubjectId;
                                                }
                                                if (worksheet.Cells[row, 4].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 4].Value.ToString().Trim()))
                                                {
                                                    string subsubject = string.Empty; //worksheet.Cells[row, 4].Value.ToString().Trim().Split('_')[1];
                                                    if (worksheet.Cells[row, 4].Value.ToString().Trim().IndexOf('_') > -1)
                                                    {
                                                        subsubject = worksheet.Cells[row, 4].Value.ToString().Trim().Split('_')[1];
                                                    }
                                                    XsiExhibitionBookSubsubject entity = new XsiExhibitionBookSubsubject();
                                                    if (LangId == 1)
                                                        entity = BookSubsubjectList.Where(i => i.Title.Trim() == subsubject).FirstOrDefault();
                                                    else
                                                        entity = BookSubsubjectList.Where(i => i.TitleAr.Trim() == subsubject).FirstOrDefault();
                                                    if (entity != null)
                                                    {
                                                        bookSubSubjectId = entity.ItemId;
                                                    }
                                                    if (bookSubSubjectId > 0)
                                                        books.ExhibitionSubsubjectId = bookSubSubjectId;
                                                }
                                                if (worksheet.Cells[row, 5].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 5].Value.ToString().Trim()))
                                                {
                                                    XsiExhibitionLanguage entity = new XsiExhibitionLanguage();
                                                    if (LangId == 1)
                                                        entity = BookLanguageList.Where(i => i.Title.Trim() == worksheet.Cells[row, 5].Value.ToString().Trim()).FirstOrDefault();
                                                    else
                                                        entity = BookLanguageList.Where(i => i.TitleAr.Trim() == worksheet.Cells[row, 5].Value.ToString().Trim()).FirstOrDefault();
                                                    if (entity != null)
                                                    {
                                                        bookLanguageId = entity.ItemId;
                                                    }
                                                    if (bookLanguageId > 0)
                                                        books.BookLanguageId = bookLanguageId;
                                                }
                                                if (worksheet.Cells[row, 6].Value != null)
                                                    books.IssueYear = worksheet.Cells[row, 6].Value.ToString().Trim();
                                                if (worksheet.Cells[row, 7].Value != null)
                                                    books.Price = worksheet.Cells[row, 7].Value.ToString().Trim();
                                                if (worksheet.Cells[row, 8].Value != null)
                                                    books.PriceBeforeDiscount = worksheet.Cells[row, 8].Value.ToString().Trim();
                                                if (worksheet.Cells[row, 9].Value != null)
                                                    books.Isbn = worksheet.Cells[row, 9].Value.ToString().Trim();
                                                if (worksheet.Cells[row, 10].Value != null)
                                                    books.BookDetails = worksheet.Cells[row, 10].Value.ToString().Trim();
                                                if (worksheet.Cells[row, 11].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 11].Value.ToString().Trim()))
                                                {
                                                    if (worksheet.Cells[row, 11].Value != null && !string.IsNullOrEmpty(worksheet.Cells[row, 11].Value.ToString()))
                                                        books.IsBestSeller = GetBestSeller(worksheet.Cells[row, 11].Value.ToString().Trim());
                                                    else
                                                        books.IsBestSeller = EnumConversion.ToString(EnumBool.No);
                                                }

                                                books.MemberId = memberid;
                                                books.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                                if (books.TitleEn != string.Empty && !string.IsNullOrEmpty(books.BookPublisherName) && !string.IsNullOrEmpty(books.AuthorName) &&
                                                    books.ExhibitionSubjectId > 0 && books.ExhibitionSubsubjectId > 0 && books.BookLanguageId > 0 && books.IssueYear != string.Empty &&
                                                    books.Price != string.Empty)
                                                {
                                                    books.IsEnable = EnumConversion.ToString(EnumBool.Yes);
                                                }
                                                books.IsNew = EnumConversion.ToString(EnumBool.Yes);
                                                books.CreatedBy = memberid;
                                                books.CreatedOn = MethodFactory.ArabianTimeNow();
                                                books.ModifiedBy = memberid;
                                                books.ModifiedOn = MethodFactory.ArabianTimeNow();
                                                _context.XsiExhibitionBooks.Add(books);
                                                _context.SaveChanges();
                                                if (books.BookId > 0)
                                                {
                                                    bookAdded = bookAdded + 1;
                                                    //if (bookAuthorId > 0)
                                                    //{
                                                    //    XsiExhibitionBookAuthor bookAuthor = new XsiExhibitionBookAuthor();
                                                    //    bookAuthor.BookId = books.BookId;
                                                    //    bookAuthor.AuthorId = bookAuthorId;
                                                    //    _context.XsiExhibitionBookAuthor.Add(bookAuthor);
                                                    //    _context.SaveChanges();
                                                    //}

                                                    #region Books Participating
                                                    XsiExhibitionBookParticipating bookParticipating = new XsiExhibitionBookParticipating();
                                                    bookParticipating.BookId = books.BookId;
                                                    bookParticipating.ExhibitorId = exhibitorId;

                                                    if (bookParticipating.ExhibitorId != -1 && agencyId != -1)
                                                        bookParticipating.AgencyId = agencyId;

                                                    bookParticipating.IsActive = EnumConversion.ToString(EnumBool.No);
                                                    bookParticipating.Price = worksheet.Cells[row, 7].Value.ToString().Trim();
                                                    bookParticipating.CreatedOn = MethodFactory.ArabianTimeNow();
                                                    bookParticipating.CreatedBy = memberid;
                                                    bookParticipating.ModifiedOn = MethodFactory.ArabianTimeNow();
                                                    bookParticipating.ModifiedBy = memberid;
                                                    _context.XsiExhibitionBookParticipating.Add(bookParticipating);
                                                    _context.SaveChanges();
                                                    #endregion
                                                }
                                            }
                                        }
                                        if (bookAdded > 0)
                                        {
                                            transaction.Commit();
                                            if (LangId == 1)
                                            {
                                                return Ok(new MessageDTO() { Message = bookAdded + " books added successfully", MessageTypeResponse = "Success" });
                                            }
                                            else
                                            {
                                                return Ok(new MessageDTO() { Message = bookAdded + " تمت إضافة الكتاب بنجاح", MessageTypeResponse = "Success" });
                                            }
                                        }
                                        else
                                        {
                                            return Ok(new MessageDTO() { Message = "Something went wrong. Please try again later", MessageTypeResponse = "Error" });
                                        }

                                    }
                                }
                            }
                        }
                    }
                    return Ok(new MessageDTO() { Message = "File not found", MessageTypeResponse = "Error" });
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                // return Ok("Something went wrong. Please try again later.");
                return Ok(new MessageDTO() { Message = "Something went wrong. Please try again later", MessageTypeResponse = "Error" });
            }
        }

        [HttpPost()]
        [Route("exportbooks")]
        public async Task<ActionResult<dynamic>> GetExportBooks()
        {
            try
            {
                long memberid = User.Identity.GetID();
                if (memberid > 0)
                {
                    #region File Generate
                    long LangId = 1;

                    List<ExportBooksDTO> listEn = new List<ExportBooksDTO>();
                    List<ExportBooksArDTO> listAr = new List<ExportBooksArDTO>();

                    long.TryParse(Request.Headers["LanguageURL"], out LangId);
                    string folder = _appCustomSettings.UploadsPhysicalPath + "\\Books\\ExportBooks\\";
                    string excelName = Guid.NewGuid().ToString() + memberid.ToString() + "_Books_30.xlsx";
                    string downloadUrl = _appCustomSettings.UploadsCMSPath + "/Books/ExportBooks/" + excelName;
                    FileInfo file = new FileInfo(Path.Combine(folder, excelName));

                    if (LangId == 1)
                    {
                        for (int i = 0; i <= 501; i++)
                        {
                            listEn.Add(new ExportBooksDTO());
                        }
                    }
                    else
                    {
                        for (int i = 0; i <= 501; i++)
                        {
                            listAr.Add(new ExportBooksArDTO());
                        }
                    }
                    #endregion
                    using (var package = new ExcelPackage(file))
                    {
                        var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                        var hdnworkSheet = package.Workbook.Worksheets.Add("Sheet2");
                        if (LangId == 1)
                            workSheet.Cells.LoadFromCollection(listEn, true);
                        else
                            workSheet.Cells.LoadFromCollection(listAr, true);
                        hdnworkSheet.Hidden = OfficeOpenXml.eWorkSheetHidden.Hidden;

                        // LoadPublisher(workSheet, hdnworkSheet, LangId);
                        //LoadAuthors(workSheet, hdnworkSheet, LangId);
                        LoadSubjectAndSubSubject(workSheet, hdnworkSheet, LangId);
                        LoadLanguages(workSheet, hdnworkSheet, LangId);
                        LoadBestSeller(workSheet, LangId);
                        #region Header Style and Color
                        workSheet.Cells.AutoFitColumns();
                        workSheet.Column(1).Width = 30;
                        workSheet.Column(2).Width = 50;
                        workSheet.Column(3).Width = 50;
                        workSheet.Column(4).Width = 50;
                        workSheet.Column(5).Width = 20;
                        workSheet.Column(9).Width = 18;
                        workSheet.Column(10).Width = 30;

                        workSheet.Cells["A1:H1"].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                        #endregion
                        package.Save();
                    }
                    return Ok(downloadUrl);
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok("Exception: Something went wrong. Please retry again later.");
            }
        }

        [HttpPost]
        [Route("deletebooks")]
        public ActionResult<dynamic> DeleteBooksByUser(RemoveBooksDTO model)
        {
            try
            {
                long bookcount = 0;
                long memberid = User.Identity.GetID();
                if (memberid > 0)
                {
                    if (model.BookIds != null && model.BookIds.Count > 0)
                    {
                        using (BooksParticipatingService BooksParticipatingService = new BooksParticipatingService())
                        {
                            ExhibitionBookService ExhibitionBookService = new ExhibitionBookService();
                            //model.BookIds = ExhibitionBookService.GetBook(new XsiExhibitionBooks() { MemberId = memberid }).Where(i => model.BookIds.Contains(i.BookId)).Select(i => i.BookId).ToList();
                            if (model.BookIds.Count > 0)
                            {
                                foreach (var bookid in model.BookIds)
                                {
                                    BooksParticipatingService.DeleteBooksParticipating(new XsiExhibitionBookParticipating() { BookId = bookid });

                                    var book = ExhibitionBookService.GetBookByItemId(bookid);
                                    if (book != null)
                                    {
                                        if (!string.IsNullOrEmpty(book.Thumbnail))
                                        {
                                            if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\" + book.Thumbnail))
                                                System.IO.File.Delete(_appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\" + book.Thumbnail);

                                            if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\Small\\" + book.Thumbnail))
                                                System.IO.File.Delete(_appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\Small\\" + book.Thumbnail);

                                            if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + book.Thumbnail))
                                                System.IO.File.Delete(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + book.Thumbnail);
                                        }
                                        ExhibitionBookService.DeleteBook(bookid);
                                        bookcount = bookcount + 1;
                                    }
                                }
                                if (bookcount == model.BookIds.Count)
                                    return Ok(new MessageDTO() { Message = bookcount + " books deleted successfully.", MessageTypeResponse = "Success" });
                                else
                                    return Ok(new MessageDTO() { Message = bookcount + " books deleted successfully.", MessageTypeResponse = "Error" });
                            }
                            else
                            {
                                return Ok(new MessageDTO() { Message = "No book(s) found.", MessageTypeResponse = "Error" });
                            }
                        }
                    }
                    else
                    {
                        return Ok(new MessageDTO() { Message = "Please select atleast one book in order to delete.", MessageTypeResponse = "Error" });
                    }
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetBooks action: {ex.Message}");
                return Ok(new MessageDTO() { Message = "Something went wrong. Please try again later.", MessageTypeResponse = "Error" });
            }
        }
        #endregion

        #region Helper Method
        string GetExhibitionGroupId(long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibition entity = MethodFactory.GetActiveExhibition(websiteId, _context);
                if (entity != null)
                    return entity.ExhibitionId.ToString();
                return "-1";
            }
        }
        private long SaveBookData(BooksAddDTO itemDto, long memberId, long langId)
        {
            using (ExhibitionBookService BookService = new ExhibitionBookService())
            {
                BooksParticipatingService BooksParticipatingService = new BooksParticipatingService();
                XsiExhibitionBooks entity = new XsiExhibitionBooks();

                // var BookAuthorDb = GetAuthor(itemDto.BookAuthorId);
                //var BookPublisherDb = GetPublisher(itemDto.BookPublisherId);
                #region Thumbnail
                if (itemDto.ThumbnailFileBase64 != null && itemDto.ThumbnailFileBase64.Length > 0)
                {
                    byte[] imageBytes;
                    if (itemDto.ThumbnailFileBase64.Contains("data:"))
                    {
                        var strInfo = itemDto.ThumbnailFileBase64.Split(",")[0];
                        imageBytes = Convert.FromBase64String(itemDto.ThumbnailFileBase64.Split(',')[1]);
                    }
                    else
                    {
                        imageBytes = Convert.FromBase64String(itemDto.ThumbnailFileBase64);
                    }
                    string FileName = string.Format("{0}_{1}.{2}", langId, MethodFactory.GetRandomNumber(), itemDto.ThumbnailFileExtension);
                    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\") + FileName, imageBytes);
                    MethodFactory.ResizeImage(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName, 0, 282, _appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\" + FileName);
                    MethodFactory.ResizeImage(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName, 0, 102, _appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\Small\\" + FileName);

                    //ResizeSettings setting = new ResizeSettings() { Width = 200, Height = 282, Mode = FitMode.Crop };
                    //ImageBuilder.Current.Build(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName, _appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\" + FileName, setting);
                    //setting = new ResizeSettings() { Width = 73, Height = 102, Mode = FitMode.Crop };
                    //ImageBuilder.Current.Build(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName, _appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\Small\\" + FileName, setting);
                    if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName))
                        System.IO.File.Delete(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName);


                    entity.Thumbnail = FileName;
                }
                #endregion
                //if (!string.IsNullOrEmpty(itemDto.BookPublisher) && itemDto.BookPublisherId > 0)
                //    if (BookPublisherDb.Trim() == itemDto.BookPublisher.Trim())
                //        entity.BookPublisherId = itemDto.BookPublisherId;
                //    else
                //        entity.BookPublisherId = -1;
                //else
                //    entity.BookPublisherId = -1;

                //if (itemDto.BookPublisherId > 0)
                //    entity.BookPublisherId = itemDto.BookPublisherId;

                entity.BookPublisherName = itemDto.BookPublisher;
                entity.BookPublisherNameAr = itemDto.BookPublisher;
                entity.AuthorName = itemDto.BookAuthor;
                entity.AuthorNameAr = itemDto.BookAuthor;

                entity.MemberId = itemDto.MemberId;
                entity.BookLanguageId = itemDto.BookLanguageId;
                entity.ExhibitionSubjectId = itemDto.ExhibitionSubjectId;
                entity.ExhibitionSubsubjectId = itemDto.ExhibitionSubsubjectId;
                entity.ExhibitionCurrencyId = itemDto.ExhibitionCurrencyId;
                entity.BookTypeId = itemDto.BookTypeId;

                //entity.IsAvailableOnline = itemDto.IsAvailableOnline;
                //entity.IsNew = itemDto.IsNew;
                //entity.IsBestSeller = itemDto.IsBestSeller;

                if (itemDto.IsAvailableOnline == "Y" || itemDto.IsAvailableOnline == "N")
                    entity.IsAvailableOnline = itemDto.IsAvailableOnline;
                else
                    entity.IsAvailableOnline = itemDto.IsAvailableOnline == "true" ? "Y" : "N";

                entity.IsNew = "Y";

                if (itemDto.IsBestSeller == "Y" || itemDto.IsBestSeller == "N")
                    entity.IsBestSeller = itemDto.IsBestSeller;
                else
                    entity.IsBestSeller = itemDto.IsBestSeller == "true" ? "Y" : "N";

                entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                if (!string.IsNullOrEmpty(itemDto.TitleAr) && !string.IsNullOrEmpty(itemDto.TitleEn))
                {
                    entity.TitleAr = itemDto.TitleAr;
                    entity.TitleEn = itemDto.TitleEn;
                }
                else if (!string.IsNullOrEmpty(itemDto.TitleAr) && string.IsNullOrEmpty(itemDto.TitleEn))
                {
                    entity.TitleAr = itemDto.TitleAr;
                    entity.TitleEn = itemDto.TitleAr;
                }
                else if (string.IsNullOrEmpty(itemDto.TitleAr) && !string.IsNullOrEmpty(itemDto.TitleEn))
                {
                    entity.TitleAr = itemDto.TitleEn;
                    entity.TitleEn = itemDto.TitleEn;
                }
                entity.Isbn = itemDto.Isbn;
                entity.IssueYear = itemDto.IssueYear;
                entity.Price = itemDto.Price;
                entity.PriceBeforeDiscount = itemDto.PriceBeforeDiscount;
                entity.BookDetails = itemDto.BookDetails;
                entity.BookDetailsAr = itemDto.BookDetailsAr;
                entity.CreatedOn = MethodFactory.ArabianTimeNow();
                entity.CreatedBy = memberId;
                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                entity.ModifiedBy = memberId;
                entity.IsEnable = EnumConversion.ToString(EnumBool.No);

                if (itemDto.TitleEn != string.Empty && itemDto.BookPublisher != string.Empty && itemDto.BookAuthor != string.Empty && itemDto.IssueYear != string.Empty && itemDto.Price != string.Empty && itemDto.PriceBeforeDiscount != string.Empty
                    && itemDto.ExhibitionSubjectId != 0 && itemDto.ExhibitionSubsubjectId != 0 && itemDto.BookLanguageId != 0)
                {
                    // if (BookPublisherDb.Trim() == itemDto.BookPublisher.Trim() && BookAuthorDb.Trim() == itemDto.BookAuthor.Trim())
                    entity.IsEnable = EnumConversion.ToString(EnumBool.Yes);
                }
                if (BookService.InsertBook(entity) == EnumResultType.Success)
                {
                    long bookid = BookService.XsiItemdId; // entity.BookId;
                                                          //dto.ExhibitorId = 6537;
                                                          //if (dto.ExhibitorId == -1)
                                                          //    dto.AgencyId = -1;
                                                          //else
                                                          //    dto.AgencyId = -1;


                    //#region Add BookAuthor
                    //XsiExhibitionBookAuthor bookAuthor = new XsiExhibitionBookAuthor();
                    //bookAuthor.BookId = bookid;
                    //bookAuthor.AuthorId = itemDto.BookAuthorId;
                    //BookService.InsertBookAuthor(bookAuthor);
                    //#endregion
                    #region Books Participating
                    XsiExhibitionBookParticipating bookParticipating = new XsiExhibitionBookParticipating();
                    bookParticipating.BookId = bookid;
                    bookParticipating.ExhibitorId = GetExhibitorByMember(entity.MemberId.Value, itemDto.WebsiteId);

                    if (bookParticipating.ExhibitorId != -1)
                        bookParticipating.AgencyId = GetAgencyByMember(entity.MemberId.Value, bookParticipating.ExhibitorId.Value);

                    bookParticipating.IsActive = EnumConversion.ToString(EnumBool.No);
                    bookParticipating.Price = itemDto.Price;
                    bookParticipating.PriceBeforeDiscount = itemDto.PriceBeforeDiscount;
                    bookParticipating.CreatedOn = MethodFactory.ArabianTimeNow();
                    bookParticipating.CreatedBy = memberId;
                    bookParticipating.ModifiedOn = MethodFactory.ArabianTimeNow();
                    bookParticipating.ModifiedBy = memberId;
                    BooksParticipatingService.InsertBooksParticipating(bookParticipating);
                    #endregion
                    return bookid;
                }
                return -1;
            }

        }
        long GetExhibitorByMember(long SIBFMemberId, long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var predicate = PredicateBuilder.New<XsiExhibition>();

                predicate = predicate.And(i => i.IsArchive == EnumConversion.ToString(EnumBool.No));
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.WebsiteId == websiteId);
                List<long> ExhibitionIdList = _context.XsiExhibition.Where(predicate).Select(s => s.ExhibitionId).ToList();
                if (ExhibitionIdList.Count() > 0)
                {
                    string strNew = EnumConversion.ToString(EnumExhibitorStatus.New);
                    string strInitialApprove = EnumConversion.ToString(EnumExhibitorStatus.InitialApproval);
                    string strApprove = EnumConversion.ToString(EnumExhibitorStatus.Approved);
                    string strActive = EnumConversion.ToString(EnumBool.Yes);

                    var isActive = EnumConversion.ToString(EnumBool.Yes);
                    List<XsiExhibitionMemberApplicationYearly> ExhibitorList = _context.XsiExhibitionMemberApplicationYearly.Where(x => x.MemberId == SIBFMemberId && x.MemberRoleId == 1 && x.IsActive == isActive).ToList();
                    XsiExhibitionMemberApplicationYearly entity = ExhibitorList.Where(p => (p.Status == strNew || p.Status == strInitialApprove || p.Status == strApprove) && ExhibitionIdList.Contains(p.ExhibitionId.Value)).FirstOrDefault();
                    if (entity != null)
                        return entity.MemberExhibitionYearlyId;
                }
                return -1;
            }
        }
        long GetAgencyByMember(long memberId, long exhibitorid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var predicate = PredicateBuilder.True<XsiExhibitionMemberApplicationYearly>();

                predicate = predicate.And(i => i.MemberId == memberId);
                predicate = predicate.And(i => i.MemberRoleId == 2);
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.ParentId == exhibitorid);
                predicate = predicate.And(i => i.Status == EnumConversion.ToString(EnumAgencyStatus.SIBFApproved));

                var agencyEntity = _context.XsiExhibitionMemberApplicationYearly.Where(predicate).OrderByDescending(i => i.MemberExhibitionYearlyId)
                     .FirstOrDefault();
                if (agencyEntity != null)
                    return agencyEntity.MemberExhibitionYearlyId;
            }
            return -1;
        }
        private bool UpdateBookData(BooksAddDTO itemDto, long memberId, long langId)
        {
            using (sibfnewdbContext db = new sibfnewdbContext())
            {
                #region Update
                BooksParticipatingService BooksParticipatingService = new BooksParticipatingService();
                XsiExhibitionBooks existingEntity = db.XsiExhibitionBooks.Where(i => i.BookId == itemDto.BookId).FirstOrDefault(); // ExhibitionBookService.GetBookByItemId(itemDto.BookId);
                                                                                                                                   //   var BookAuthorDb = GetAuthor(itemDto.BookAuthorId);
                                                                                                                                   //  var BookPublisherDb = GetPublisher(itemDto.BookPublisherId);
                XsiExhibitionBooks entity = new XsiExhibitionBooks();
                entity = db.XsiExhibitionBooks.Where(i => i.BookId == itemDto.BookId).FirstOrDefault();// ExhibitionBookService.GetBookByItemId(itemDto.BookId);// itemDto.BookId;
                #region Thumbnail
                if (itemDto.ThumbnailFileBase64 != null && itemDto.ThumbnailFileBase64.Length > 0)
                {
                    byte[] imageBytes;
                    if (itemDto.ThumbnailFileBase64.Contains("data:"))
                    {
                        var strInfo = itemDto.ThumbnailFileBase64.Split(",")[0];
                        imageBytes = Convert.FromBase64String(itemDto.ThumbnailFileBase64.Split(',')[1]);
                    }
                    else
                    {
                        imageBytes = Convert.FromBase64String(itemDto.ThumbnailFileBase64);
                    }
                    string FileName = string.Format("{0}_{1}.{2}", langId, MethodFactory.GetRandomNumber(), itemDto.ThumbnailFileExtension);
                    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\") + FileName, imageBytes);

                    MethodFactory.ResizeImage(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName, 0, 282, _appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\" + FileName);
                    MethodFactory.ResizeImage(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName, 0, 102, _appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\Small\\" + FileName);


                    //ResizeSettings setting = new ResizeSettings() { Width = 200, Height = 282, Mode = FitMode.Crop };
                    //ImageBuilder.Current.Build(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName, _appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\" + FileName, setting);
                    //setting = new ResizeSettings() { Width = 73, Height = 102, Mode = FitMode.Crop };
                    //ImageBuilder.Current.Build(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName, _appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\Small\\" + FileName, setting);
                    if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName))
                        System.IO.File.Delete(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName);


                    entity.Thumbnail = FileName;
                }
                else if (existingEntity.Thumbnail != null)
                    entity.Thumbnail = existingEntity.Thumbnail;
                #endregion
                //if (!string.IsNullOrEmpty(itemDto.BookPublisher) && itemDto.BookPublisherId > 0)
                //    if (BookPublisherDb.Trim() == itemDto.BookPublisher.Trim())
                //        entity.BookPublisherId = itemDto.BookPublisherId;
                //    else
                //        entity.BookPublisherId = existingEntity.BookPublisherId;
                //else
                //    entity.BookPublisherId = existingEntity.BookPublisherId;

                //if (itemDto.BookPublisherId > 0)
                //    entity.BookPublisherId = itemDto.BookPublisherId;
                //else
                //    entity.BookPublisherId = existingEntity.BookPublisherId;


                if (!string.IsNullOrEmpty(itemDto.BookPublisher))
                {
                    if (langId == 1)
                        entity.BookPublisherName = itemDto.BookPublisher;
                    else
                        entity.BookPublisherNameAr = itemDto.BookPublisher;
                }

                if (!string.IsNullOrEmpty(itemDto.BookAuthor))
                {
                    if (langId == 1)
                        entity.AuthorName = itemDto.BookAuthor;
                    else
                        entity.AuthorNameAr = itemDto.BookAuthor;
                }

                entity.MemberId = existingEntity.MemberId;
                entity.BookLanguageId = itemDto.BookLanguageId;
                entity.ExhibitionSubjectId = itemDto.ExhibitionSubjectId;
                entity.ExhibitionSubsubjectId = itemDto.ExhibitionSubsubjectId;
                entity.ExhibitionCurrencyId = itemDto.ExhibitionCurrencyId;
                entity.BookTypeId = itemDto.BookTypeId;

                if (itemDto.IsAvailableOnline == "Y" || itemDto.IsAvailableOnline == "N")
                    entity.IsAvailableOnline = itemDto.IsAvailableOnline;
                else
                    entity.IsAvailableOnline = itemDto.IsAvailableOnline == "true" ? "Y" : "N";

                entity.IsNew = existingEntity.IsNew ?? "N";

                if (itemDto.IsBestSeller == "Y" || itemDto.IsBestSeller == "N")
                    entity.IsBestSeller = itemDto.IsBestSeller;
                else
                    entity.IsBestSeller = itemDto.IsBestSeller == "true" ? "Y" : "N";

                entity.IsActive = existingEntity.IsActive ?? EnumConversion.ToString(EnumBool.No);

                if (!string.IsNullOrEmpty(itemDto.TitleAr) && !string.IsNullOrEmpty(itemDto.TitleEn))
                {
                    entity.TitleAr = itemDto.TitleAr;
                    entity.TitleEn = itemDto.TitleEn;
                }
                else if (!string.IsNullOrEmpty(itemDto.TitleAr) && string.IsNullOrEmpty(itemDto.TitleEn))
                {
                    entity.TitleAr = itemDto.TitleAr;
                    entity.TitleEn = itemDto.TitleAr;
                }
                else if (string.IsNullOrEmpty(itemDto.TitleAr) && !string.IsNullOrEmpty(itemDto.TitleEn))
                {
                    entity.TitleAr = itemDto.TitleEn;
                    entity.TitleEn = itemDto.TitleEn;
                }
                entity.Isbn = itemDto.Isbn;
                entity.IssueYear = itemDto.IssueYear;
                entity.Price = itemDto.Price;
                entity.PriceBeforeDiscount = itemDto.PriceBeforeDiscount;
                entity.BookDetails = itemDto.BookDetails;
                entity.BookDetailsAr = itemDto.BookDetailsAr;
                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                entity.ModifiedBy = memberId;
                if (itemDto.TitleEn != string.Empty && itemDto.BookAuthor != string.Empty && itemDto.BookAuthor != string.Empty && itemDto.IssueYear != string.Empty && itemDto.Price != string.Empty && itemDto.PriceBeforeDiscount != string.Empty
                  && itemDto.ExhibitionSubjectId != 0 && itemDto.ExhibitionSubsubjectId != 0 && itemDto.BookLanguageId != 0)
                {
                    entity.IsEnable = EnumConversion.ToString(EnumBool.Yes);
                }
                else
                    entity.IsEnable = EnumConversion.ToString(EnumBool.No);

                bool IsUpdated = false;
                #endregion
                db.Entry(entity).State = EntityState.Modified;
                if (db.SaveChanges() > 0)
                {
                    // #region Add Book Author
                    //List<XsiExhibitionBookAuthor> aBook = db.XsiExhibitionBookAuthor.Where(x => x.BookId == entity.BookId).ToList();
                    //foreach (var item in aBook)
                    //{
                    //    db.XsiExhibitionBookAuthor.Remove(item);
                    //    db.SaveChanges();
                    //}

                    //XsiExhibitionBookAuthor bookAuthor = new XsiExhibitionBookAuthor();
                    //bookAuthor.BookId = entity.BookId;
                    //bookAuthor.AuthorId = itemDto.BookAuthorId;
                    //db.XsiExhibitionBookAuthor.Add(bookAuthor);
                    //db.SaveChanges();
                    // #endregion

                    #region Books Participating
                    long agencyId = -1;
                    var exhibitorId = GetExhibitorByMember(memberId, itemDto.WebsiteId);

                    if (exhibitorId != -1)
                        agencyId = GetAgencyByMember(memberId, exhibitorId);

                    XsiExhibitionBookParticipating bookParticipating = new XsiExhibitionBookParticipating();
                    bookParticipating = BooksParticipatingService
                       .GetBooksParticipating(new XsiExhibitionBookParticipating()
                       {
                           ExhibitorId = exhibitorId,
                           BookId = entity.BookId
                       }).FirstOrDefault();

                    if (bookParticipating == null)
                        bookParticipating = new XsiExhibitionBookParticipating();

                    bookParticipating.BookId = entity.BookId;
                    bookParticipating.ExhibitorId = exhibitorId;
                    bookParticipating.AgencyId = agencyId;
                    bookParticipating.Price = itemDto.Price;
                    bookParticipating.PriceBeforeDiscount = itemDto.PriceBeforeDiscount;
                    bookParticipating.ModifiedOn = MethodFactory.ArabianTimeNow();
                    bookParticipating.ModifiedBy = memberId;
                    if (bookParticipating == null || bookParticipating.ItemId <= 0)
                    {
                        bookParticipating.IsActive = EnumConversion.ToString(EnumBool.No);
                        bookParticipating.CreatedOn = MethodFactory.ArabianTimeNow();
                        bookParticipating.CreatedBy = memberId;
                        BooksParticipatingService.InsertBooksParticipating(bookParticipating);
                    }
                    else
                    {
                        bookParticipating.IsActive = bookParticipating.IsActive ?? EnumConversion.ToString(EnumBool.No);
                        BooksParticipatingService.UpdateBooksParticipating(bookParticipating);
                    }
                    #endregion

                    ExhibitionService ExhibitionService = new ExhibitionService();
                    long ActiveExhibitionId = ExhibitionService.GetExhibition(new XsiExhibition() { IsActive = "Y", IsArchive = "N", WebsiteId = itemDto.WebsiteId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault().ExhibitionId;
                    if (BooksParticipatingService.GetBooksParticipating(new XsiExhibitionBookParticipating() { ExhibitorId = exhibitorId, BookId = existingEntity.BookId, IsActive = EnumConversion.ToString(EnumBool.Yes) }).ToList().Count > 0)
                    {
                        #region Notify Admin About Book Information Changes
                        SendEmailBasedOnEdit(existingEntity, entity, langId, ActiveExhibitionId, exhibitorId);
                        #endregion
                    }
                    IsUpdated = true;
                }
                if (IsUpdated)
                    return true;
                return false;
            }
        }
        private static IEnumerable<string> GetAllowedValues()
        {
            return new string[] { "Doctor", "Baker", "Candlestick Maker" };
        }
        private bool UpdateBookDataOld(BooksAddDTO itemDto, long memberId, long langId)
        {
            using (ExhibitionBookService ExhibitionBookService = new ExhibitionBookService())
            {
                BooksParticipatingService BooksParticipatingService = new BooksParticipatingService();
                XsiExhibitionBooks existingEntity = ExhibitionBookService.GetBookByItemId(itemDto.BookId);
                //var BookAuthorDb = GetAuthor(itemDto.BookAuthorId);
                // var BookPublisherDb = GetPublisher(itemDto.BookPublisherId);
                XsiExhibitionBooks entity = new XsiExhibitionBooks();
                entity.BookId = itemDto.BookId;// ExhibitionBookService.GetBookByItemId(itemDto.BookId);// itemDto.BookId;
                #region Thumbnail
                if (itemDto.ThumbnailFileBase64 != null && itemDto.ThumbnailFileBase64.Length > 0)
                {
                    byte[] imageBytes;
                    if (itemDto.ThumbnailFileBase64.Contains("data:"))
                    {
                        var strInfo = itemDto.ThumbnailFileBase64.Split(",")[0];
                        imageBytes = Convert.FromBase64String(itemDto.ThumbnailFileBase64.Split(',')[1]);
                    }
                    else
                    {
                        imageBytes = Convert.FromBase64String(itemDto.ThumbnailFileBase64);
                    }
                    string FileName = string.Format("{0}_{1}.{2}", langId, MethodFactory.GetRandomNumber(), itemDto.ThumbnailFileExtension);
                    System.IO.File.WriteAllBytes(Path.Combine(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\") + FileName, imageBytes);

                    MethodFactory.ResizeImage(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName, 0, 282, _appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\" + FileName);
                    MethodFactory.ResizeImage(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName, 0, 102, _appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\Small\\" + FileName);


                    //ResizeSettings setting = new ResizeSettings() { Width = 200, Height = 282, Mode = FitMode.Crop };
                    //ImageBuilder.Current.Build(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName, _appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\" + FileName, setting);
                    //setting = new ResizeSettings() { Width = 73, Height = 102, Mode = FitMode.Crop };
                    //ImageBuilder.Current.Build(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName, _appCustomSettings.UploadsPhysicalPath + "\\Books\\Thumbnails\\Small\\" + FileName, setting);
                    if (System.IO.File.Exists(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName))
                        System.IO.File.Delete(_appCustomSettings.UploadsPhysicalPath + "\\Books\\temp\\" + FileName);


                    entity.Thumbnail = FileName;
                }
                else if (existingEntity.Thumbnail != null)
                    entity.Thumbnail = existingEntity.Thumbnail;
                #endregion
                //if (!string.IsNullOrEmpty(itemDto.BookPublisher) && itemDto.BookPublisherId > 0)
                //    if (BookPublisherDb.Trim() == itemDto.BookPublisher.Trim())
                //        entity.BookPublisherId = itemDto.BookPublisherId;
                //    else
                //        entity.BookPublisherId = existingEntity.BookPublisherId;
                //else
                //    entity.BookPublisherId = existingEntity.BookPublisherId;

                //if (itemDto.BookPublisherId > 0)
                //    entity.BookPublisherId = itemDto.BookPublisherId;
                //else
                //    entity.BookPublisherId = existingEntity.BookPublisherId;

                entity.MemberId = existingEntity.MemberId;
                entity.BookLanguageId = itemDto.BookLanguageId;
                entity.ExhibitionSubjectId = itemDto.ExhibitionSubjectId;
                entity.ExhibitionSubsubjectId = itemDto.ExhibitionSubsubjectId;
                entity.ExhibitionCurrencyId = itemDto.ExhibitionCurrencyId;
                entity.BookTypeId = itemDto.BookTypeId;
                entity.IsAvailableOnline = itemDto.IsAvailableOnline;
                entity.IsNew = itemDto.IsNew;
                entity.IsBestSeller = itemDto.IsBestSeller;
                entity.IsActive = existingEntity.IsActive ?? EnumConversion.ToString(EnumBool.No);
                if (!string.IsNullOrEmpty(itemDto.TitleAr) && !string.IsNullOrEmpty(itemDto.TitleEn))
                {
                    entity.TitleAr = itemDto.TitleAr;
                    entity.TitleEn = itemDto.TitleEn;
                }
                else if (!string.IsNullOrEmpty(itemDto.TitleAr) && string.IsNullOrEmpty(itemDto.TitleEn))
                {
                    entity.TitleAr = itemDto.TitleAr;
                    entity.TitleEn = itemDto.TitleAr;
                }
                else if (string.IsNullOrEmpty(itemDto.TitleAr) && !string.IsNullOrEmpty(itemDto.TitleEn))
                {
                    entity.TitleAr = itemDto.TitleEn;
                    entity.TitleEn = itemDto.TitleEn;
                }
                entity.Isbn = itemDto.Isbn;
                entity.IssueYear = itemDto.IssueYear;
                entity.Price = itemDto.Price;
                entity.PriceBeforeDiscount = itemDto.PriceBeforeDiscount;
                entity.BookDetails = itemDto.BookDetails;
                entity.BookDetailsAr = itemDto.BookDetailsAr;
                entity.ModifiedOn = MethodFactory.ArabianTimeNow();
                entity.ModifiedBy = memberId;

                if (itemDto.TitleEn != string.Empty && itemDto.BookPublisher != string.Empty && itemDto.BookAuthor != string.Empty && itemDto.IssueYear != string.Empty && itemDto.Price != string.Empty && itemDto.PriceBeforeDiscount != string.Empty
                  && itemDto.ExhibitionSubjectId != 0 && itemDto.ExhibitionSubsubjectId != 0 && itemDto.BookLanguageId != 0)
                {
                    //if (BookPublisherDb.Trim() == itemDto.BookPublisher.Trim() && BookAuthorDb.Trim() == itemDto.BookAuthor.Trim())
                    entity.IsEnable = EnumConversion.ToString(EnumBool.Yes);
                }
                else
                    entity.IsEnable = EnumConversion.ToString(EnumBool.No);

                bool IsUpdated = false;

                if (ExhibitionBookService.UpdateBook(entity) == EnumResultType.Success)
                {
                    #region Add Book Author
                    //using (sibfnewdbContext _context1 = new sibfnewdbContext())
                    //{
                    //    List<XsiExhibitionBookAuthor> aBook = _context1.XsiExhibitionBookAuthor.Where(x => x.BookId == entity.BookId).ToList();
                    //    foreach (var item in aBook)
                    //    {
                    //        _context1.XsiExhibitionBookAuthor.Remove(item);
                    //        _context1.SaveChanges();
                    //    }
                    //}
                    // ExhibitionBookService.DeleteBookAuthor(entity.BookId);
                    //XsiExhibitionBookAuthor bookAuthor = new XsiExhibitionBookAuthor();
                    //bookAuthor.BookId = entity.BookId;
                    //bookAuthor.AuthorId = itemDto.BookAuthorId;
                    //ExhibitionBookService.InsertBookAuthor(bookAuthor);
                    #endregion
                    #region Books Participating
                    long agencyId = -1;
                    var exhibitorId = GetExhibitorByMember(memberId, itemDto.WebsiteId);

                    if (exhibitorId != -1)
                        agencyId = GetAgencyByMember(memberId, exhibitorId);

                    XsiExhibitionBookParticipating bookParticipating = new XsiExhibitionBookParticipating();
                    bookParticipating = BooksParticipatingService
                       .GetBooksParticipating(new XsiExhibitionBookParticipating()
                       {
                           ExhibitorId = exhibitorId,
                           BookId = entity.BookId
                       }).FirstOrDefault();

                    if (bookParticipating == null)
                        bookParticipating = new XsiExhibitionBookParticipating();

                    bookParticipating.BookId = entity.BookId;
                    bookParticipating.ExhibitorId = exhibitorId;
                    bookParticipating.AgencyId = agencyId;
                    bookParticipating.Price = itemDto.Price;
                    bookParticipating.PriceBeforeDiscount = itemDto.PriceBeforeDiscount;
                    bookParticipating.ModifiedOn = MethodFactory.ArabianTimeNow();
                    bookParticipating.ModifiedBy = memberId;
                    if (bookParticipating == null || bookParticipating.ItemId <= 0)
                    {
                        bookParticipating.CreatedOn = MethodFactory.ArabianTimeNow();
                        bookParticipating.CreatedBy = memberId;
                        BooksParticipatingService.InsertBooksParticipating(bookParticipating);
                    }
                    else
                        BooksParticipatingService.UpdateBooksParticipating(bookParticipating);
                    #endregion

                    ExhibitionService ExhibitionService = new ExhibitionService();
                    long ActiveExhibitionId = ExhibitionService.GetExhibition(new XsiExhibition() { IsActive = "Y", IsArchive = "N", WebsiteId = itemDto.WebsiteId }, EnumSortlistBy.ByItemIdDesc).FirstOrDefault().ExhibitionId;
                    if (BooksParticipatingService.GetBooksParticipating(new XsiExhibitionBookParticipating() { ExhibitorId = exhibitorId, BookId = existingEntity.BookId, IsActive = EnumConversion.ToString(EnumBool.Yes) }).ToList().Count > 0)
                    {
                        #region Notify Admin About Book Information Changes
                        SendEmailBasedOnEdit(existingEntity, entity, langId, ActiveExhibitionId, exhibitorId);
                        #endregion
                    }
                    IsUpdated = true;
                }
                if (IsUpdated)
                    return true;
                return false;
            }
        }
        private string GetAuthor(long itemId)
        {
            using (ExhibitionAuthorService ExhibitionAuthorService = new ExhibitionAuthorService())
            {
                XsiExhibitionAuthor entity = ExhibitionAuthorService.GetExhibitionAuthorByItemId(itemId);
                if (entity != null)
                    if (entity.Title != null)
                        return entity.Title;
                return string.Empty;
            }
        }
        private string GetPublisher(long itemId)
        {
            using (ExhibitionPublisherService ExhibitionPublisherService = new ExhibitionPublisherService())
            {
                XsiExhibitionBookPublisher entity = ExhibitionPublisherService.GetExhibitionPublisherByItemId(itemId);
                if (entity != null)
                    if (entity.Title != null)
                        return entity.Title;
                return string.Empty;
            }
        }
        #endregion
        #region Excel Helper Methods
        private void LoadPublisher(ExcelWorksheet workSheet, ExcelWorksheet hndworkSheet, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                List<string> list = new List<string>();
                if (langId == 1)
                    list = _context.XsiExhibitionBookPublisher.Where(x => x.IsActive == "Y" && x.Status == "A").Select(x => x.Title).ToList();
                else
                    list = _context.XsiExhibitionBookPublisher.Where(x => x.IsActive == "Y" && x.Status == "A").Select(x => x.TitleAr).ToList();
                int i = 2;
                foreach (var item in list)
                {
                    hndworkSheet.Cells["A" + i + ""].Value = item;
                    i++;
                }
                for (int k = 2; k <= 501; k++)
                {
                    var validation = workSheet.DataValidations.AddListValidation("B" + k);
                    validation.AllowBlank = false;
                    validation.Formula.ExcelFormula = "Sheet2!A1:A" + list.Count;
                }
            }
        }
        private void LoadAuthors(ExcelWorksheet workSheet, ExcelWorksheet hndworkSheet, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                List<string> list = new List<string>();
                if (langId == 1)
                    list = _context.XsiExhibitionAuthor.Where(x => x.IsActive == "Y" && x.Status == "A").Select(x => x.Title).ToList();
                else
                    list = _context.XsiExhibitionAuthor.Where(x => x.IsActive == "Y" && x.Status == "A").Select(x => x.TitleAr).ToList();
                int i = 1;
                foreach (var item in list)
                {
                    hndworkSheet.Cells["B" + i + ""].Value = item;
                    i++;
                }
                for (int k = 2; k <= 501; k++)
                {
                    var validation = workSheet.DataValidations.AddListValidation("C" + k);
                    validation.AllowBlank = false;
                    validation.Formula.ExcelFormula = "Sheet2!B1:B" + list.Count;
                }
            }
        }
        private void LoadSubjectAndSubSubject(ExcelWorksheet workSheet, ExcelWorksheet hndworkSheet, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                if (langId == 1)
                {
                    var list = _context.XsiExhibitionBookSubsubject.Where(x => x.IsActive == "Y").Select(x => new { Title = x.Category.Title + "_" + x.Title }).OrderBy(x => x.Title).ToList();
                    int i = 1;
                    foreach (var item in list)
                    {
                        hndworkSheet.Cells["C" + i + ""].Value = item.Title;
                        i++;
                    }
                    for (int k = 2; k <= 501; k++)
                    {
                        var validation = workSheet.DataValidations.AddListValidation("D" + k);
                        validation.AllowBlank = false;
                        validation.Formula.ExcelFormula = "Sheet2!C1:C" + list.Count;
                    }
                }
                else
                {
                    var list = _context.XsiExhibitionBookSubsubject.Where(x => x.IsActive == "Y").Select(x => new { Title = x.Category.TitleAr + "_" + x.TitleAr }).OrderBy(x => x.Title).ToList();
                    int i = 1;
                    foreach (var item in list)
                    {
                        hndworkSheet.Cells["C" + i + ""].Value = item.Title;
                        i++;
                    }
                    for (int k = 2; k <= 501; k++)
                    {
                        var validation = workSheet.DataValidations.AddListValidation("D" + k);
                        validation.AllowBlank = false;
                        validation.Formula.ExcelFormula = "Sheet2!C1:C" + list.Count;
                    }
                }
            }
        }
        private void LoadLanguages(ExcelWorksheet workSheet, ExcelWorksheet hndworkSheet, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                //3,16
                List<string> list = new List<string>();
                if (langId == 1)
                {
                    list = _context.XsiExhibitionLanguage.Where(x => x.IsActive == "Y").Select(x => x.Title).OrderBy(x => x).ToList();
                    list.Remove("Arabic");
                    list.Remove("English");

                    list.Insert(0, "English");
                    list.Insert(1, "Arabic");
                }

                else
                {
                    list = _context.XsiExhibitionLanguage.Where(x => x.IsActive == "Y").Select(x => x.TitleAr).OrderBy(x => x).ToList();
                    list.Remove("Arabic");
                    list.Remove("English");

                    list.Insert(0, "الإنجليزية");
                    list.Insert(1, "العربية");
                }



                int i = 1;
                foreach (var item in list)
                {
                    hndworkSheet.Cells["D" + i + ""].Value = item;
                    i++;
                }
                for (int k = 2; k <= 501; k++)
                {
                    var validation = workSheet.DataValidations.AddListValidation("E" + k);
                    validation.AllowBlank = false;
                    validation.Formula.ExcelFormula = "Sheet2!D1:D" + list.Count;
                }
            }
        }
        private void LoadBestSeller(ExcelWorksheet workSheet, long langId)
        {
            var validation = workSheet.DataValidations.AddListValidation("K2:K30");
            validation.Formula.Values.Add("Yes");
            validation.Formula.Values.Add("No");
        }

        long GetPublisherId(string text, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long id = 0;
                if (langId == 1)
                    id = _context.XsiExhibitionBookPublisher.Where(x => x.IsActive == "Y" && x.Title == text).FirstOrDefault().ItemId;
                else
                    id = _context.XsiExhibitionBookPublisher.Where(x => x.IsActive == "Y" && x.TitleAr == text).FirstOrDefault().ItemId;
                return id;
            }
        }
        long GetAuthorsId(string text, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibitionAuthor author = new XsiExhibitionAuthor();
                long id = -1;
                if (langId == 1)
                    author = _context.XsiExhibitionAuthor.Where(x => x.IsActive == "Y" && x.Title == text).FirstOrDefault();
                else
                    author = _context.XsiExhibitionAuthor.Where(x => x.IsActive == "Y" && x.TitleAr == text).FirstOrDefault();
                if (author != null)
                    return author.AuthorId;
                return id;
            }
        }
        long GetSubjectId(string text, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string subject = text.Split('_')[0];
                long id = 0;
                if (langId == 1)
                    id = _context.XsiExhibitionBookSubject.Where(x => x.IsActive == "Y" && x.Title == subject).FirstOrDefault().ItemId;
                else
                    id = _context.XsiExhibitionBookSubject.Where(x => x.IsActive == "Y" && x.TitleAr == subject).FirstOrDefault().ItemId;
                return id;
            }
        }
        long GetSubSubjectId(string text, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                string subject = text.Split('_')[1];
                long id = 0;
                if (langId == 1)
                    id = _context.XsiExhibitionBookSubsubject.Where(x => x.IsActive == "Y" && x.Title == subject).FirstOrDefault().ItemId;
                else
                    id = _context.XsiExhibitionBookSubsubject.Where(x => x.IsActive == "Y" && x.TitleAr == subject).FirstOrDefault().ItemId;
                return id;
            }
        }
        long GetLanguagesId(string text, long langId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long id = 0;
                if (langId == 1)
                    id = _context.XsiExhibitionLanguage.Where(x => x.IsActive == "Y" && x.Title == text).FirstOrDefault().ItemId;
                else
                    id = _context.XsiExhibitionLanguage.Where(x => x.IsActive == "Y" && x.TitleAr == text).FirstOrDefault().ItemId;
                return id;
            }
        }
        string GetBestSeller(string text)
        {
            if (text == "Yes")
                return "Y";
            return "N";
        }

        #region Global Dropdowns
        List<XsiExhibitionBookPublisher> GetExhibitionBookPublisherList()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiExhibitionBookPublisher.Where(x => (x.Title != null && x.TitleAr != null) && x.IsActive == "Y").ToList();
            }
        }
        List<XsiExhibitionAuthor> GetExhibitionAuthorList()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiExhibitionAuthor.Where(x => (x.Title != null && x.TitleAr != null) && x.IsActive == "Y").ToList();
            }
        }
        List<XsiExhibitionBookSubject> GetExhibitionBookSubjectList()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiExhibitionBookSubject.Where(x => (x.Title != null && x.TitleAr != null) && x.IsActive == "Y").ToList();
            }
        }
        List<XsiExhibitionBookSubsubject> GetExhibitionBookSubsubjectList()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiExhibitionBookSubsubject.Where(x => (x.Title != null && x.TitleAr != null) && x.IsActive == "Y").ToList();
            }
        }
        List<XsiExhibitionLanguage> GetExhibitionLanguageList()
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiExhibitionLanguage.Where(x => (x.Title != null && x.TitleAr != null) && x.IsActive == "Y").ToList();
            }
        }
        #endregion
        #region Email
        private void SendEmailBasedOnEdit(XsiExhibitionBooks oldEntity, XsiExhibitionBooks newEntity, long langId, long exhibitionId, long exhibitorId)
        {
            #region Send Edit Request Email
            StringBuilder body = new StringBuilder();
            string strEmailContentBody = string.Empty;
            string strEmailContentSubject = string.Empty;
            string strEmailContentEmail = string.Empty;
            string strEmailContentAdmin = string.Empty;

            #region Email Content
            if (oldEntity != newEntity)
            {
                using (ExhibitionService ExhibitionService = new ExhibitionService())
                {
                    HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                    ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                    XsiExhibitionMemberApplicationYearly ExhibitorRegistrationEntity = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(exhibitorId);
                    long WebsiteId = ExhibitionService.GetExhibitionByItemId(exhibitionId).WebsiteId.Value;
                    EmailContentService EmailContentService = new EmailContentService();
                    SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                    string ServerAddress = _appCustomSettings.ServerAddressNew;
                    if (WebsiteId == 1)
                        ServerAddress = _appCustomSettings.ServerAddressNew;
                    else
                        ServerAddress = _appCustomSettings.ServerAddressSCRF;
                    if (WebsiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                    {
                        XsiScrfemailContent scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(20083);
                        if (scrfEmailContent != null)
                        {
                            if (scrfEmailContent.Body != null)
                                strEmailContentBody = scrfEmailContent.Body;
                            if (scrfEmailContent.Email != null)
                                strEmailContentEmail = scrfEmailContent.Email;
                            if (scrfEmailContent.Subject != null)
                                strEmailContentSubject = scrfEmailContent.Subject;
                            //strEmailContentAdmin = AdminNameSCRF;
                        }
                    }
                    else
                    {
                        XsiEmailContent emailContent = EmailContentService.GetEmailContentByItemId(20097);
                        if (emailContent != null)
                        {
                            if (emailContent.Body != null)
                                strEmailContentBody = emailContent.Body;
                            if (emailContent.Email != null)
                                strEmailContentEmail = emailContent.Email;
                            if (emailContent.Subject != null)
                                strEmailContentSubject = emailContent.Subject;
                            //strEmailContentAdmin = AdminName;
                        }
                    }
                    #endregion
                    XsiExhibitionMember ExhibitionMemberEntity = new XsiExhibitionMember();
                    body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, ExhibitionMemberEntity, ServerAddress, WebsiteId, _appCustomSettings));
                    string strContent = "<p style=\"font-size: 16px; line-height: 32px; font-family: Arial, Helvetica, sans-serif; color: #555555; margin: 0px;\">$$Label$$ : $$Value$$</p>";
                    #region Body
                    bool IsAnyFieldChanged = false;
                    if (ExhibitorRegistrationEntity.PublisherNameEn != null)
                        body.Replace("$$ExhibitorNameEn$$", ExhibitorRegistrationEntity.PublisherNameEn);
                    if (ExhibitorRegistrationEntity.FileNumber != null)
                        body.Replace("$$FileNumber$$", ExhibitorRegistrationEntity.FileNumber);

                    if (oldEntity.TitleEn != null)
                        body.Replace("$$ActualBookTitleEn$$", oldEntity.TitleEn);

                    #region Book Title English
                    if (oldEntity.TitleEn != newEntity.TitleEn)
                    {
                        if (newEntity.TitleEn != null)
                        {
                            body.Replace("$$NewBookTitleEn$$", strContent);
                            body.Replace("$$Label$$", "Book Title in English");
                            body.Replace("$$Value$$", newEntity.TitleEn);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewBookTitleEn$$", string.Empty);
                        if (oldEntity.TitleEn != null)
                        {
                            body.Replace("$$OldBookTitleEn$$", strContent);
                            body.Replace("$$Label$$", "Book Title in English");
                            body.Replace("$$Value$$", oldEntity.TitleEn);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldBookTitleEn$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewBookTitleEn$$", string.Empty);
                        body.Replace("$$OldBookTitleEn$$", string.Empty);
                    }
                    #endregion
                    #region Book Title Arabic
                    if (oldEntity.TitleAr != newEntity.TitleAr)
                    {
                        if (newEntity.TitleAr != null)
                        {
                            body.Replace("$$NewBookTitleAr$$", strContent);
                            body.Replace("$$Label$$", "Book Title in Arabic");
                            body.Replace("$$Value$$", newEntity.TitleAr);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewBookTitleAr$$", string.Empty);
                        if (oldEntity.TitleAr != null)
                        {
                            body.Replace("$$OldBookTitleAr$$", strContent);
                            body.Replace("$$Label$$", "Publisher Name in Arabic");
                            body.Replace("$$Value$$", oldEntity.TitleAr);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldBookTitleAr$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewBookTitleAr$$", string.Empty);
                        body.Replace("$$OldBookTitleAr$$", string.Empty);
                    }
                    #endregion
                    #region Book Publisher
                    if (oldEntity.BookPublisherName != newEntity.BookPublisherName)
                    {
                        if (newEntity.BookPublisherName != null)
                        {
                            body.Replace("$$NewBookPublisherName$$", strContent);
                            body.Replace("$$Label$$", "Book Publisher");
                            body.Replace("$$Value$$", newEntity.BookPublisherName); //, GetPublisher(newEntity.BookPublisherId.Value)
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewBookPublisherName$$", string.Empty);

                        if (oldEntity.BookPublisherName != null)
                        {
                            body.Replace("$$OldBookPublisherName$$", strContent);
                            body.Replace("$$Label$$", "Book Publisher");
                            body.Replace("$$Value$$", oldEntity.BookPublisherName); //GetPublisher(oldEntity.BookPublisherId.Value)
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldBookPublisherName$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewBookPublisherName$$", string.Empty);
                        body.Replace("$$OldBookPublisherName$$", string.Empty);
                    }
                    #endregion

                    #region Book Author
                    if (oldEntity.AuthorName != newEntity.AuthorName)
                    {
                        if (newEntity.AuthorName != null)
                        {
                            body.Replace("$$NewAuthorName$$", strContent);
                            body.Replace("$$Label$$", "Book Author");
                            body.Replace("$$Value$$", newEntity.AuthorName); //GetAuthor(newEntity.AuthorId.Value)
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewAuthorName$$", string.Empty);

                        if (oldEntity.AuthorName != null)
                        {
                            body.Replace("$$OldAuthorName$$", strContent);
                            body.Replace("$$Label$$", "Book Author");
                            body.Replace("$$Value$$", oldEntity.AuthorName); // GetAuthor(oldEntity.AuthorId.Value)
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldAuthorName$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewAuthorName$$", string.Empty);
                        body.Replace("$$OldAuthorName$$", string.Empty);
                    }
                    #endregion

                    #region Is New Book
                    if (oldEntity.IsNew != newEntity.IsNew)
                    {
                        if (newEntity.IsNew != null)
                        {
                            body.Replace("$$NewIsItNewBook$$", strContent);
                            body.Replace("$$Label$$", "Is New");
                            body.Replace("$$Value$$", newEntity.IsNew == EnumConversion.ToString(EnumBool.Yes) ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewIsItNewBook$$", string.Empty);
                        if (oldEntity.IsNew != null)
                        {
                            body.Replace("$$OldIsItNewBook$$", strContent);
                            body.Replace("$$Label$$", "Is New");
                            body.Replace("$$Value$$", oldEntity.IsNew == EnumConversion.ToString(EnumBool.Yes) ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldIsItNewBook$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewIsItNewBook$$", string.Empty);
                        body.Replace("$$OldIsItNewBook$$", string.Empty);
                    }
                    #endregion
                    #region Main Subject
                    if (oldEntity.ExhibitionSubjectId != newEntity.ExhibitionSubjectId)
                    {
                        if (newEntity.ExhibitionSubjectId != null)
                        {
                            body.Replace("$$NewMainSubject$$", strContent);
                            body.Replace("$$Label$$", "Main Subject");
                            body.Replace("$$Value$$", MethodFactory.MainSubjectById(newEntity.ExhibitionSubjectId.Value, langId));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewMainSubject$$", string.Empty);
                        if (oldEntity.ExhibitionSubjectId != null)
                        {
                            body.Replace("$$OldMainSubject$$", strContent);
                            body.Replace("$$Label$$", "Main Subject");
                            body.Replace("$$Value$$", MethodFactory.MainSubjectById(oldEntity.ExhibitionSubjectId.Value, langId));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldMainSubject$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewMainSubject$$", string.Empty);
                        body.Replace("$$OldMainSubject$$", string.Empty);
                    }
                    #endregion
                    #region Sub Subject
                    if (oldEntity.ExhibitionSubsubjectId != newEntity.ExhibitionSubsubjectId)
                    {
                        if (newEntity.ExhibitionSubsubjectId != null)
                        {
                            body.Replace("$$NewSubSubject$$", strContent);
                            body.Replace("$$Label$$", "Subject");
                            body.Replace("$$Value$$", MethodFactory.SubSubjectById(newEntity.ExhibitionSubsubjectId.Value, newEntity.ExhibitionSubjectId.Value, langId));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewSubSubject$$", string.Empty);
                        if (oldEntity.ExhibitionSubsubjectId != null)
                        {
                            body.Replace("$$OldSubSubject$$", strContent);
                            body.Replace("$$Label$$", "Subject");
                            body.Replace("$$Value$$", MethodFactory.SubSubjectById(oldEntity.ExhibitionSubsubjectId.Value, oldEntity.ExhibitionSubjectId.Value, langId));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldSubSubject$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewSubSubject$$", string.Empty);
                        body.Replace("$$OldSubSubject$$", string.Empty);
                    }
                    #endregion
                    #region Book Language
                    /*if (oldEntity.ExhibitionLanguageId != newEntity.ExhibitionLanguageId)
                    {
                        if (newEntity.ExhibitionLanguageId != null)
                        {
                            body.Replace("$$NewBookLanguage$$", strContent);
                            body.Replace("$$Label$$", "Exhibition Language");
                            body.Replace("$$Value$$", MethodFactory.GetExhibitionLanuageTitle(newEntity.ExhibitionLanguageId.Value, EnglishId));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewBookLanguage$$", string.Empty);
                        if (oldEntity.ExhibitionLanguageId != null)
                        {
                            body.Replace("$$OldBookLanguage$$", strContent);
                            body.Replace("$$Label$$", "Exhibition Language");
                            body.Replace("$$Value$$", MethodFactory.GetExhibitionLanuageTitle(oldEntity.ExhibitionLanguageId.Value, EnglishId));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldBookLanguage$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewBookLanguage$$", string.Empty);
                        body.Replace("$$OldBookLanguage$$", string.Empty);
                    }*/
                    body.Replace("$$NewBookLanguage$$", string.Empty);
                    body.Replace("$$OldBookLanguage$$", string.Empty);
                    #endregion
                    #region Book Type
                    if (oldEntity.BookTypeId != newEntity.BookTypeId)
                    {
                        if (newEntity.BookTypeId != null)
                        {
                            body.Replace("$$NewBookType$$", strContent);
                            body.Replace("$$Label$$", "Book Type");
                            body.Replace("$$Value$$", MethodFactory.GetBookTypeTitle(newEntity.BookTypeId.Value, langId));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewBookType$$", string.Empty);
                        if (oldEntity.BookTypeId != null)
                        {
                            body.Replace("$$OldBookType$$", strContent);
                            body.Replace("$$Label$$", "Book Type");
                            body.Replace("$$Value$$", MethodFactory.GetBookTypeTitle(oldEntity.BookTypeId.Value, langId));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldBookType$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewBookType$$", string.Empty);
                        body.Replace("$$OldBookType$$", string.Empty);
                    }
                    #endregion
                    #region Issue Year
                    if (oldEntity.IssueYear != newEntity.IssueYear)
                    {
                        if (newEntity.IssueYear != null)
                        {
                            body.Replace("$$NewIssueYear$$", strContent);
                            body.Replace("$$Label$$", "Issue Year");
                            body.Replace("$$Value$$", newEntity.IssueYear);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewIssueYear$$", string.Empty);
                        if (oldEntity.IssueYear != null)
                        {
                            body.Replace("$$OldIssueYear$$", strContent);
                            body.Replace("$$Label$$", "Issue Year");
                            body.Replace("$$Value$$", oldEntity.IssueYear.ToString());
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldIssueYear$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewIssueYear$$", string.Empty);
                        body.Replace("$$OldIssueYear$$", string.Empty);
                    }
                    #endregion
                    #region Book Price
                    if (oldEntity.Price != newEntity.Price)
                    {
                        if (newEntity.Price != null)
                        {
                            body.Replace("$$NewPrice$$", strContent);
                            body.Replace("$$Label$$", "Price");
                            body.Replace("$$Value$$", newEntity.Price);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewPrice$$", string.Empty);
                        if (oldEntity.Price != null)
                        {
                            body.Replace("$$OldPrice$$", strContent);
                            body.Replace("$$Label$$", "Price");
                            body.Replace("$$Value$$", oldEntity.Price.ToString());
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldPrice$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewPrice$$", string.Empty);
                        body.Replace("$$OldPrice$$", string.Empty);
                    }
                    #endregion
                    #region Book Price Before Discount
                    if (oldEntity.PriceBeforeDiscount != newEntity.PriceBeforeDiscount)
                    {
                        if (newEntity.PriceBeforeDiscount != null)
                        {
                            body.Replace("$$NewPriceBeforeDisucount$$", strContent);
                            body.Replace("$$Label$$", "Price Before Discount");
                            body.Replace("$$Value$$", newEntity.PriceBeforeDiscount);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewPriceBeforeDisucount$$", string.Empty);
                        if (oldEntity.PriceBeforeDiscount != null)
                        {
                            body.Replace("$$OldPriceBeforeDisucount$$", strContent);
                            body.Replace("$$Label$$", "Price Before Discount");
                            body.Replace("$$Value$$", oldEntity.PriceBeforeDiscount.ToString());
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldPriceBeforeDisucount$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewPriceBeforeDisucount$$", string.Empty);
                        body.Replace("$$OldPriceBeforeDisucount$$", string.Empty);
                    }
                    #endregion
                    #region ISBN
                    if (oldEntity.Isbn != newEntity.Isbn)
                    {
                        if (newEntity.Isbn != null)
                        {
                            body.Replace("$$NewISBN$$", strContent);
                            body.Replace("$$Label$$", "ISBN");
                            body.Replace("$$Value$$", newEntity.Isbn);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewISBN$$", string.Empty);
                        if (oldEntity.Isbn != null)
                        {
                            body.Replace("$$OldISBN$$", strContent);
                            body.Replace("$$Label$$", "ISBN");
                            body.Replace("$$Value$$", oldEntity.Isbn.ToString());
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldISBN$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewISBN$$", string.Empty);
                        body.Replace("$$OldISBN$$", string.Empty);
                    }
                    #endregion
                    #region Currency
                    if (oldEntity.ExhibitionCurrencyId != newEntity.ExhibitionCurrencyId)
                    {
                        if (newEntity.ExhibitionCurrencyId != null)
                        {
                            body.Replace("$$NewCurrency$$", strContent);
                            body.Replace("$$Label$$", "Currency");
                            body.Replace("$$Value$$", MethodFactory.GetCurrencyTitle(newEntity.ExhibitionCurrencyId.Value, langId));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewCurrency$$", string.Empty);
                        if (oldEntity.ExhibitionCurrencyId != null)
                        {
                            body.Replace("$$OldCurrency$$", strContent);
                            body.Replace("$$Label$$", "Currency");
                            body.Replace("$$Value$$", MethodFactory.GetCurrencyTitle(oldEntity.ExhibitionCurrencyId.Value, langId));
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldCurrency$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewCurrency$$", string.Empty);
                        body.Replace("$$OldCurrency$$", string.Empty);
                    }
                    #endregion
                    #region Is Available Online
                    if (oldEntity.IsAvailableOnline != newEntity.IsAvailableOnline)
                    {
                        if (newEntity.IsAvailableOnline != null)
                        {
                            body.Replace("$$NewIsAvailableOnline$$", strContent);
                            body.Replace("$$Label$$", "Is Available Online");
                            body.Replace("$$Value$$", newEntity.IsAvailableOnline == EnumConversion.ToString(EnumBool.Yes) ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewIsAvailableOnline$$", string.Empty);
                        if (oldEntity.IsAvailableOnline != null)
                        {
                            body.Replace("$$OldIsAvailableOnline$$", strContent);
                            body.Replace("$$Label$$", "Is Available Online");
                            body.Replace("$$Value$$", oldEntity.IsAvailableOnline == EnumConversion.ToString(EnumBool.Yes) ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldIsAvailableOnline$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewIsAvailableOnline$$", string.Empty);
                        body.Replace("$$OldIsAvailableOnline$$", string.Empty);
                    }
                    #endregion

                    #region Thumbnail
                    if (oldEntity.Thumbnail != newEntity.Thumbnail)
                    {
                        if (newEntity.Thumbnail != null)
                        {
                            body.Replace("$$NewUploadBookCover$$", strContent);
                            body.Replace("$$Label$$", "Book Cover");
                            body.Replace("$$Value$$", newEntity.Thumbnail);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewUploadBookCover$$", string.Empty);
                        if (oldEntity.Thumbnail != null)
                        {
                            body.Replace("$$OldUploadBookCover$$", strContent);
                            body.Replace("$$Label$$", "Book Cover");
                            body.Replace("$$Value$$", oldEntity.Thumbnail);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldUploadBookCover$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewUploadBookCover$$", string.Empty);
                        body.Replace("$$OldUploadBookCover$$", string.Empty);
                    }
                    #endregion


                    #region Book Details English
                    if (oldEntity.BookDetails != newEntity.BookDetails)
                    {
                        if (newEntity.BookDetails != null)
                        {
                            body.Replace("$$NewBookDetailEn$$", strContent);
                            body.Replace("$$Label$$", "Book Details English");
                            body.Replace("$$Value$$", newEntity.BookDetails);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewBookDetailEn$$", string.Empty);
                        if (oldEntity.BookDetails != null)
                        {
                            body.Replace("$$OldBookDetailEn$$", strContent);
                            body.Replace("$$Label$$", "Book Details English");
                            body.Replace("$$Value$$", oldEntity.BookDetails);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldBookDetailEn$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewBookDetailEn$$", string.Empty);
                        body.Replace("$$OldBookDetailEn$$", string.Empty);
                    }
                    #endregion
                    #region Book Details Arabic
                    if (oldEntity.BookDetailsAr != newEntity.BookDetailsAr)
                    {
                        if (newEntity.BookDetailsAr != null)
                        {
                            body.Replace("$$NewBookDetailAr$$", strContent);
                            body.Replace("$$Label$$", "Book Details Arabic");
                            body.Replace("$$Value$$", newEntity.BookDetailsAr);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewBookDetailAr$$", string.Empty);
                        if (oldEntity.BookDetailsAr != null)
                        {
                            body.Replace("$$OldBookDetailAr$$", strContent);
                            body.Replace("$$Label$$", "Book Details Arabic");
                            body.Replace("$$Value$$", oldEntity.BookDetailsAr);
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldBookDetailAr$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewBookDetailAr$$", string.Empty);
                        body.Replace("$$OldBookDetailAr$$", string.Empty);
                    }
                    #endregion
                    #region Is Best Seller
                    if (oldEntity.IsBestSeller != newEntity.IsBestSeller)
                    {
                        if (newEntity.IsBestSeller != null)
                        {
                            body.Replace("$$NewBestSeller$$", strContent);
                            body.Replace("$$Label$$", "Is Best Seller");
                            body.Replace("$$Value$$", newEntity.IsBestSeller == EnumConversion.ToString(EnumBool.Yes) ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$NewBestSeller$$", string.Empty);
                        if (oldEntity.IsBestSeller != null)
                        {
                            body.Replace("$$OldBestSeller$$", strContent);
                            body.Replace("$$Label$$", "Is Best Seller");
                            body.Replace("$$Value$$", oldEntity.IsBestSeller == EnumConversion.ToString(EnumBool.Yes) ? "Yes" : "No");
                            IsAnyFieldChanged = true;
                        }
                        else
                            body.Replace("$$OldBestSeller$$", string.Empty);
                    }
                    else
                    {
                        body.Replace("$$NewBestSeller$$", string.Empty);
                        body.Replace("$$OldBestSeller$$", string.Empty);
                    }
                    #endregion
                    #endregion
                    if (IsAnyFieldChanged)
                        if (strEmailContentEmail != string.Empty)
                        {
                            //  _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString().Replace("$$SpecificName$$", strEmailContentAdmin));
                        }
                }
            }
            #endregion
        }
        #endregion
        #endregion
    }
}

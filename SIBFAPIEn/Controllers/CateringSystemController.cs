﻿using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SIBFAPIEn.DTO;
using Entities.Models;
using Xsi.ServicesLayer;
using Microsoft.Extensions.Localization;
using System.Text;
using SIBFAPIEn.Utility;
using System.IO;
using QRCoder;
using System.Drawing;
using System.Drawing.Imaging;
//using Entities.Models;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CateringSystemController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;
        public CateringSystemController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings, IEmailSender emailSender)
        {
            _logger = logger;
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
        }

        #region Get
        [HttpGet("{websiteId}/{memberid}")]
        public ActionResult<dynamic> GetExhibitionCateringSystem(long websiteId, long memberid)
        {
            List<CateringSystemLoadDTO> CateringSystemList = new List<CateringSystemLoadDTO>();
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long SIBFMemberId = User.Identity.GetID();

                if (SIBFMemberId == memberid)
                {
                    if (MethodFactory.IsSIBFSTaff(SIBFMemberId))
                    {
                        using (sibfnewdbContext _context = new sibfnewdbContext())
                        {

                            var list = _context.XsiExhibitionCateringSystemRegistration.Where(i => i.WebsiteId == websiteId && i.MemberId == memberid && i.IsActive == "Y").ToList();
                            if (list.Count == 0)
                            {
                                return Ok(new { MessageTypeResponse = "Error", CateringSystemList, MemberId = memberid, Message = "No data found." });
                            }
                            else
                            {
                                CateringSystemList = _context.XsiExhibitionCateringSystemRegistration.Where(i => i.WebsiteId == websiteId && i.MemberId == memberid && i.IsActive == "Y").Select(i => new CateringSystemLoadDTO()
                                {
                                    ItemId = i.ItemId,
                                    MemberId = i.MemberId,
                                    ExhibitionId = i.ExhibitionId,
                                    Status = GetCateringSystemRegistrationStatus(i.Status),
                                    WebsiteId = i.WebsiteId,
                                    StaffguestId = i.StaffguestId,
                                    StaffguestName = i.StaffguestName,
                                    EventName = i.EventName,
                                    EventType = i.EventType,
                                    CateringType = i.CateringType,
                                    StartDate = i.StartDate.Value.ToString("dd/MM/yyyy"),
                                    //Duration = i.Duration,
                                    NoofDays = i.NoofDays,
                                    StartTime = i.StartTime,
                                    StartTimeMeridiain = i.StartTimeMeridiain,
                                    EndTime = i.EndTime,
                                    EndTimeMeridiain = i.EndTimeMeridiain,
                                    NoofTables = i.NoofTables,
                                    NoofChairs = i.NoofChairs,
                                    NoofGuests = i.NoofGuests,
                                    ContactPerson = i.ContactPerson,
                                    ContactEmail = i.ContactEmail,
                                    ContactNumber = i.ContactNumber,
                                    Notes = i.Notes,
                                    CreatedOn = i.CreatedOn.Value.ToString("dd/MM/yyyy")
                                }).OrderByDescending(i => i.ItemId).ToList();

                                foreach (CateringSystemLoadDTO item in CateringSystemList)
                                {
                                    string[] str = item.ContactNumber.Split('$');

                                    if (str.Count() == 3)
                                    {
                                        item.ContactNumberISD = str[0].Trim();
                                        item.ContactNumberSTD = str[1].Trim();
                                        item.ContactNumber = str[2].Trim();
                                    }
                                    else if (str.Count() == 2)
                                    {
                                        item.ContactNumberSTD = str[0].Trim();
                                        item.ContactNumber = str[1].Trim();
                                    }
                                    else
                                        item.ContactNumber = str[0].Trim();
                                }

                                if (CateringSystemList.Count() > 0)
                                {
                                    return Ok(new { MessageTypeResponse = "Success", CateringSystemList, WebstiteId = websiteId, MemberId = memberid });
                                }

                                return Ok(new { MessageTypeResponse = "Error", CateringSystemList, Message = "No data found.", MemberId = memberid });
                            }
                        }
                    }
                    else
                    {
                        return Ok(new { MessageTypeResponse = "Error", CateringSystemList, Message = "Member is not Staff Guest", MemberId = memberid });
                    }
                }
                return Ok(new { MessageTypeResponse = "Error", CateringSystemList, Message = "Unauthorised access." });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetExhibitionCateringSystem action: {ex.InnerException}");
                return Ok(new { MessageTypeResponse = "Error", CateringSystemList, Message = "Something went wrong. Please try again later. " });
            }
        }

        [HttpGet("GetRegistrationDetails/{itemid}/{memberid}")]
        public ActionResult<dynamic> GetExhibitionCateringSystemByItemId(long itemid, long memberid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long SIBFMemberId = User.Identity.GetID();
                if (SIBFMemberId == memberid)
                {
                    if (MethodFactory.IsSIBFSTaff(SIBFMemberId))
                    {
                        using (sibfnewdbContext _context = new sibfnewdbContext())
                        {
                            CateringSystemLoadDTO obj = new CateringSystemLoadDTO();

                            obj = _context.XsiExhibitionCateringSystemRegistration.Where(i => i.ItemId == itemid).Select(i => new CateringSystemLoadDTO()
                            {
                                ItemId = i.ItemId,
                                MemberId = i.MemberId,
                                ExhibitionId = i.ExhibitionId,
                                Status = GetCateringSystemRegistrationStatus(i.Status),
                                WebsiteId = i.WebsiteId,
                                StaffguestId = i.StaffguestId,
                                StaffguestName = i.StaffguestName,
                                EventName = i.EventName,
                                EventType = i.EventType,
                                CateringType = i.CateringType,
                                StartDate = i.StartDate.Value.ToString("dd/MM/yyyy"),
                                //Duration = i.Duration,
                                NoofDays = i.NoofDays,
                                StartTime = i.StartTime,
                                StartTimeMeridiain = i.StartTimeMeridiain,
                                EndTime = i.EndTime,
                                EndTimeMeridiain = i.EndTimeMeridiain,
                                NoofTables = i.NoofTables,
                                NoofChairs = i.NoofChairs,
                                NoofGuests = i.NoofGuests,
                                ContactPerson = i.ContactPerson,
                                ContactEmail = i.ContactEmail,
                                ContactNumber = i.ContactNumber,
                                Notes = i.Notes,
                                CreatedOn = i.CreatedOn.Value.ToString("dd/MM/yyyy")
                            }).FirstOrDefault();

                            if (obj != null)
                            {
                                string[] str = obj.ContactNumber.Split('$');
                                if (str.Count() == 3)
                                {
                                    obj.ContactNumberISD = str[0].Trim();
                                    obj.ContactNumberSTD = str[1].Trim();
                                    obj.ContactNumber = str[2].Trim();
                                }
                                else if (str.Count() == 2)
                                {
                                    obj.ContactNumberSTD = str[0].Trim();
                                    obj.ContactNumber = str[1].Trim();
                                }
                                else
                                    obj.ContactNumber = str[0].Trim();

                                return Ok(new { MessageTypeResponse = "Success", CateringSystemLoadDTO = obj, MemberId = memberid });
                            }

                            return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "No data found." });
                        }
                    }
                    else
                    {
                        return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "Member is not Staff Guest" });
                    }
                }
                return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "Unauthorised access." });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetExhibitionCateringSystem action: {ex.InnerException}");
                return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "Something went wrong. Please try again later. " });
            }
        }

        // GET: api/GetStaffGuestListForMember/1
        [HttpGet("GetStaffGuestListForMember/{websiteId}")]
        public async Task<ActionResult<IEnumerable<DropdownDataDTO>>> GetStaffGuestListForMember(long websiteId)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                var exhibition = _context.XsiExhibition.Where(i => i.WebsiteId == websiteId && i.IsActive == "Y").OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
                List<DropdownDataDTO> List = new List<DropdownDataDTO>();

                string strApproved = EnumConversion.ToString(EnumStaffGuestStatus.Approved);
                string strReIssueVisa = EnumConversion.ToString(EnumStaffGuestStatus.ReIssueVisa);
                string strVisaUploaded = EnumConversion.ToString(EnumStaffGuestStatus.VisaUploaded);

                long memberid = User.Identity.GetID();
                if (exhibition != null)
                {
                    var predicate = PredicateBuilder.True<XsiExhibitionStaffGuest>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.MemberId == memberid);

                    var predicateSGP = PredicateBuilder.True<XsiExhibitionStaffGuestParticipating>();
                    predicateSGP = predicateSGP.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicateSGP = predicateSGP.And(i => i.ExhibitionId == exhibition.ExhibitionId);
                    predicateSGP = predicateSGP.And(i => (i.Status == strApproved || i.Status == strReIssueVisa || i.Status == strVisaUploaded));

                    List = _context.XsiExhibitionStaffGuest.Where(predicate)
                       .Join(_context.XsiExhibitionStaffGuestParticipating.Where(predicateSGP),
                         sg => sg.StaffGuestId,
                         sgp => sgp.StaffGuestId,
                         (sg, sgp) => new DropdownDataDTO()
                         {
                             ItemId = sg.StaffGuestId,
                             Title = sgp.NameEn
                         }).OrderBy(x => x.Title).ToList();

                }
                return List;
            }
        }

        [HttpGet("EventType")]
        public ActionResult<dynamic> GetCateringEventType()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                List<DropdownDataDTO> LocationTypeList = new List<DropdownDataDTO>();
                if (LangId == 1)
                {
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = -1, Title = "-Select Event Type-" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 1, Title = "VIP" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 2, Title = "Normal" });
                }
                else
                {
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = -1, Title = "-Select Event Type-" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 1, Title = "كبار الشخصيات " });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 2, Title = "ضيف" });
                }
                return Ok(LocationTypeList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetCateringEventType action: {ex.InnerException}");
                return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "Something went wrong. Please try again later." });
            }
        }

        [HttpGet("CateringType")]
        public ActionResult<dynamic> GetCateringCateringType()
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                List<DropdownDataDTO> LocationTypeList = new List<DropdownDataDTO>();
                if (LangId == 1)
                {
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = -1, Title = "-Select Catering Type-" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 1, Title = "Cofee Break" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 2, Title = "Lunch" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 3, Title = "Dinner" });
                }
                else
                {
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = -1, Title = "-Select Catering Type-" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 1, Title = "استراحة قهوة" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 2, Title = "الغداء" });
                    LocationTypeList.Add(new DropdownDataDTO() { ItemId = 3, Title = "العشاء" });
                }
                return Ok(LocationTypeList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetCateringCateringType action: {ex.InnerException}");
                return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "Something went wrong. Please try again later." });
            }
        }
        #endregion

        [HttpPost]
        [Route("Registration")]
        public ActionResult<dynamic> ExhibitionCateringSystemRegistration(CateringSystemSaveDataDTO dto)
        {
            MessageDTO messageDTO = new MessageDTO();
            try
            {
                messageDTO.MessageTypeResponse = "Error";
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long memberId = User.Identity.GetID();
                if (memberId > 0)
                {
                    using (sibfnewdbContext db = new sibfnewdbContext())
                    {
                        XsiExhibitionCateringSystemRegistration registrationentity = new XsiExhibitionCateringSystemRegistration();

                        registrationentity.MemberId = memberId;
                        registrationentity.WebsiteId = dto.WebsiteId;
                        if (dto.WebsiteId > 0)
                        {
                            var exhibition = MethodFactory.GetExhibition(dto.WebsiteId.Value);
                            registrationentity.ExhibitionId = exhibition.ExhibitionId;
                        }

                        //if (dto.StaffguestId > 0)
                        //{
                        //    registrationentity.StaffguestId = dto.StaffguestId;
                        //    registrationentity.StaffguestName = GetStaffGuestNameById(dto.StaffguestId);
                        //}
                        registrationentity.StaffguestName = dto.StaffguestName;

                       registrationentity.EventName = dto.EventName;
                        registrationentity.EventType = dto.EventType;
                        registrationentity.CateringType = dto.CateringType;

                        if (!string.IsNullOrEmpty(dto.StartDate))
                            registrationentity.StartDate = Convert.ToDateTime(dto.StartDate.Trim());

                        if (!string.IsNullOrEmpty(dto.EndDate))
                            registrationentity.EndDate = Convert.ToDateTime(dto.EndDate.Trim());

                        //registrationentity.Duration = dto.Duration;
                        registrationentity.NoofDays = dto.NoofDays;
                        registrationentity.StartTime = dto.StartTime;
                        registrationentity.StartTimeMeridiain = dto.StartTimeMeridiain;
                        registrationentity.EndTime = dto.EndTime;
                        registrationentity.EndTimeMeridiain = dto.EndTimeMeridiain;

                        registrationentity.NoofTables = dto.NoofTables;
                        registrationentity.NoofChairs = dto.NoofChairs;
                        registrationentity.NoofGuests = dto.NoofGuests;
                        registrationentity.ContactPerson = dto.ContactPerson;
                        registrationentity.ContactEmail = dto.ContactEmail;
                        registrationentity.ContactNumber = dto.ContactNumberISD + "$" + dto.ContactNumberSTD + "$" + dto.ContactNumber;
                        registrationentity.Notes = dto.Notes;

                        registrationentity.Status = "N";
                        registrationentity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        registrationentity.CreatedOn = MethodFactory.ArabianTimeNow();
                        registrationentity.CreatedById = -1;
                        registrationentity.ModifiedOn = MethodFactory.ArabianTimeNow();
                        registrationentity.ModifiedById = -1;

                        db.Add(registrationentity);
                        db.SaveChanges();
                        long itemid = registrationentity.ItemId;

                        db.SaveChanges();

                        SendEmail(memberId, dto.WebsiteId.Value, LangId);
                        if (LangId == 1)
                        {
                            messageDTO.Message = "Thank you! Your application has been submitted successfully";
                            messageDTO.MessageTypeResponse = "Success";
                        }
                        else
                        {
                            messageDTO.Message = "Thank you! Your application has been submitted successfully";
                            messageDTO.MessageTypeResponse = "Success";
                        }
                    }
                    return Ok(messageDTO);
                }

                messageDTO.Message = "Unauthorised access. Try login again.";
                return Ok(messageDTO);
            }
            catch (Exception ex)
            {
                messageDTO.Message = "Something went wrong. Please try again later.";
                return Ok(messageDTO);
            }
        }

        private string GetStaffGuestNameById(long? staffguestId)
        {
            using (sibfnewdbContext db = new sibfnewdbContext())
            {
                var entity = db.XsiExhibitionStaffGuestParticipating.Where(i => i.StaffGuestId == staffguestId).FirstOrDefault();
                if (entity != null)
                {
                    return entity.NameEn;
                }
            }
            return string.Empty;
        }

        #region Methods 
        private string GetCateringSystemRegistrationStatus(string status)
        {
            switch (status)
            {
                case "N": return "New";
                case "C": return "Cancelled";
                case "A": return "Approved";
                default: return "New";
            }
        }
        void SendEmail(long memberid, long websiteId, long langId)
        {
            using (ExhibitionMemberService ExhibitionMemberService = new ExhibitionMemberService())
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    List<string> MailAttachments = new List<string>();
                    HtmlContentFactory htmlContentFactory = new HtmlContentFactory(_logger);
                    XsiExhibitionMember member = new XsiExhibitionMember();

                    member = ExhibitionMemberService.GetExhibitionMemberByItemId(memberid);
                    if (member != default(XsiExhibitionMember))
                    {
                        using (EmailContentService EmailContentService = new EmailContentService())
                        {
                            string strEmailContentBody = "", strEmailContentEmail = "", strEmailContentSubject = "", strEmailContentAdmin = "";

                            #region Send Thankyou Email
                            StringBuilder body = new StringBuilder();
                            #region Email Content
                            SCRFEmailContentService SCRFEmailContentService = new SCRFEmailContentService();
                            if (websiteId == Convert.ToInt64(EnumWebsiteId.SCRF))
                            {
                                var scrfEmailContent = SCRFEmailContentService.GetSCRFEmailContentByItemId(20135);
                                if (scrfEmailContent != null)
                                {
                                    if (scrfEmailContent.Body != null)
                                        strEmailContentBody = scrfEmailContent.Body;
                                    if (scrfEmailContent.Email != null)
                                        strEmailContentEmail = scrfEmailContent.Email;
                                    if (scrfEmailContent.Subject != null)
                                        strEmailContentSubject = scrfEmailContent.Subject;
                                    strEmailContentAdmin = _appCustomSettings.AdminNameSCRF;
                                }
                            }
                            else
                            {
                                var emailContent = EmailContentService.GetEmailContentByItemId(20173);
                                if (emailContent != null)
                                {
                                    if (emailContent.Body != null)
                                        strEmailContentBody = emailContent.Body;
                                    if (emailContent.Email != null)
                                        strEmailContentEmail = emailContent.Email;
                                    if (emailContent.Subject != null)
                                        strEmailContentSubject = emailContent.Subject;
                                    strEmailContentAdmin = _appCustomSettings.AdminName;
                                }
                            }

                            #endregion
                            body.Append(htmlContentFactory.BindEmailContent(langId, strEmailContentBody, member, _appCustomSettings.ServerAddressNew, websiteId, _appCustomSettings));
                            body.Replace("$$participantname$$", member.Firstname + " " + member.LastName);
                            if (!string.IsNullOrEmpty(strEmailContentEmail))
                                _emailSender.SendEmail(strEmailContentAdmin, _appCustomSettings.AdminEmail, strEmailContentEmail, strEmailContentSubject, body.ToString());
                            #endregion
                        }
                    }
                }
            }
        }
        #endregion
    }
}



﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AwardsController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;

        public AwardsController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        // GET: api/Awards
        [HttpGet]
        [Route("{pageNumber}/{pageSize}")]
        public async Task<ActionResult<dynamic>> GetXsiAwards(int? pageNumber = 1, int? pageSize = 15)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    long websiteId = Convert.ToInt32(EnumWebsiteTypeId.SIBF);
                    var predicate = PredicateBuilder.True<XsiAwards>();

                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.WebsiteId == websiteId);

                    //MethodFactory.LoadPageContentsNew(91, langID);
                    var thumbnailpath = _appCustomSettings.UploadsCMSPath + "/Awards/Thumbnail/";
                    var List = await _context.XsiAwards.Where(predicate).OrderBy(i => i.Name).Select(x => new AwardsListDTO()
                    {
                        ItemId = x.ItemId,
                        Name = x.Name,
                        Thumbnail = thumbnailpath + (x.Thumbnail ?? "award-tile.jpg")
                    }).ToListAsync();

                    var totalCount = List.Count;

                    List = List.Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToList();
                    var dto = new { List, TotalCount = totalCount };

                    return Ok(dto);
                }
                else
                {
                    long websiteId = Convert.ToInt32(EnumWebsiteTypeId.SIBF);
                    var predicate = PredicateBuilder.True<XsiAwards>();

                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.WebsiteId == websiteId);

                    //MethodFactory.LoadPageContentsNew(91, langID);
                    var thumbnailpath = _appCustomSettings.UploadsCMSPath + "/Awards/Thumbnail/";
                    var List = await _context.XsiAwards.Where(predicate).OrderBy(i => i.NameAr).Select(x => new AwardsListDTO()
                    {
                        ItemId = x.ItemId,
                        Name = x.NameAr,
                        Thumbnail = thumbnailpath + (x.ThumbnailAr ?? "award-tile.jpg")
                    }).ToListAsync();

                    var totalCount = List.Count;

                    List = List.Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToList();
                    var dto = new { List, TotalCount = totalCount };

                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwards action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        // GET: api/Awards/5
        [HttpGet("{id}")]
        public async Task<ActionResult<dynamic>> GetXsiAwards(long id)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiAwards>();
                    predicate = predicate.And(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    var thumbnailpath = _appCustomSettings.UploadsCMSPath + "/Awards/Thumbnail/";
                    var dto = await _context.XsiAwards.AsQueryable().Where(predicate)
                        .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new
                        {
                            ItemId = i.ItemId,
                            Name = i.Name,
                            Thumbnail = thumbnailpath + i.Thumbnail
                        }).FirstOrDefaultAsync();

                    if (dto == null)
                    {
                        return NotFound();
                    }

                    return Ok(dto);
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiAwards>();
                    predicate = predicate.And(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    var thumbnailpath = _appCustomSettings.UploadsCMSPath + "/Awards/Thumbnail/";
                    var dto = await _context.XsiAwards.AsQueryable().Where(predicate)
                        .OrderByDescending(o => o.CreatedOn).ThenByDescending(o => o.ItemId).Select(i => new
                        {
                            ItemId = i.ItemId,
                            Name = i.NameAr,
                            Thumbnail = thumbnailpath + i.ThumbnailAr
                        }).FirstOrDefaultAsync();

                    if (dto == null)
                    {
                        return NotFound();
                    }

                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwards action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }
        // GET: api/AwardListById
        [HttpGet]
        [Route("AwardListById/{id}")]
        public async Task<ActionResult<IEnumerable<DropdownDataDTO>>> GetXsiAwardListById(long id)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiAwards>();
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);
                    predicate = predicate.And(i => i.ItemId == id);

                    var List = await _context.XsiAwards.AsQueryable().Where(predicate).OrderBy(x => x.Name.Trim()).Select(x => new DropdownDataDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.Name
                    }).ToListAsync();

                    return Ok(List);
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiAwards>();
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    predicate = predicate.And(i => i.WebsiteId == 0 || i.WebsiteId == 1);
                    predicate = predicate.And(i => i.ItemId == id);

                    var List = await _context.XsiAwards.AsQueryable().Where(predicate).OrderBy(x => x.NameAr.Trim()).Select(x => new DropdownDataDTO()
                    {
                        ItemId = x.ItemId,
                        Title = x.NameAr
                    }).ToListAsync();

                    return Ok(List);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwardsListById action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        // GET: api/AwardListById
        [HttpGet]
        [Route("AwardMenuById/{awardid?}/{subawardid?}")]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetAwardMenuById(long? awardid = -1, long? subawardid = -1)
        {
            try
            {
                AwardsDetailsDTO details = new AwardsDetailsDTO();
                List<AwardMenuTabDto> menu = new List<AwardMenuTabDto>();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    AwardsItemDetailsDTO award = new AwardsItemDetailsDTO();
                    var thumbnailpath = _appCustomSettings.UploadsCMSPath + "/Awards/Thumbnail/";
                    if (subawardid != null && subawardid > 0)
                    {
                        award = await _context.XsiAwards.Where(p => p.IsActive == "Y")
                    .Join(_context.XsiSubAwards.Where(p => p.IsActive == "Y" && p.ItemId == subawardid), a => a.ItemId, sa => sa.AwardId, (a, sa) =>
                             new AwardsItemDetailsDTO
                             {
                                 ItemId = sa.ItemId,
                                 Title = sa.Name,
                                 Thumbnail = thumbnailpath + a.Thumbnail,
                                 Introduction = sa.Introduction,
                                 //Category = sa.Category,
                                 //Objectives = sa.Objectives,
                                 Conditions = sa.Conditions,
                                 Requirements = sa.Rrequirements,
                                 URLTitle = GetURLTitle(awardid.Value, sa.Name, LangId),
                                 Url = GetURL(awardid.Value, subawardid, LangId)
                             }).FirstOrDefaultAsync();

                        if (award != null)
                        {
                            details.ItemId = award.ItemId;
                            details.Name = award.Title;
                            details.Thumbnail = award.Thumbnail;
                        }
                    }
                    else
                    {
                        var predicate = PredicateBuilder.True<XsiAwards>();
                        predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicate = predicate.And(i => (i.WebsiteId == 0 || i.WebsiteId == 1));
                        predicate = predicate.And(i => i.ItemId == awardid);

                        award = await _context.XsiAwards.AsQueryable().Where(predicate).OrderBy(x => x.Name.Trim()).Select(x => new AwardsItemDetailsDTO
                        {
                            ItemId = x.ItemId,
                            Title = x.Name,
                            Thumbnail = thumbnailpath + x.Thumbnail,
                            Introduction = x.Introduction,
                            Category = x.Category,
                            Objectives = x.Objectives,
                            Conditions = x.Conditions,
                            Requirements = x.Requirements,
                            // URLTitle = x.Name,
                            URLTitle = GetURLTitle(awardid.Value, x.Name, LangId),
                            Url = GetURL(awardid.Value, 0, LangId)
                        }).FirstOrDefaultAsync();

                        if (award != null)
                        {
                            details.ItemId = award.ItemId;
                            details.Name = award.Title;
                            details.Thumbnail = award.Thumbnail;
                        }
                    }

                    if (award != null)
                    {
                        #region Bind Award Menu
                        AwardMenuTabDto obj = new AwardMenuTabDto();

                        if (!string.IsNullOrWhiteSpace(award.Introduction))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = nameof(award.Introduction);
                            obj.Value = award.Introduction;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Category))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = nameof(award.Category);
                            obj.Value = award.Category;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Objectives))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = nameof(award.Objectives);
                            obj.Value = award.Objectives;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Conditions))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "General Conditions"; //nameof(award.Conditions);
                            obj.Value = award.Conditions;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Requirements))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "Arbitration and Criteria"; //nameof(award.Requirements);
                            obj.Value = award.Requirements;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.URLTitle))
                        {
                            obj = new AwardMenuTabDto();

                            if (award.ItemId == 3 || award.ItemId == 5 || award.ItemId == 7)
                            {
                                obj.Key = "To register click here";
                                obj.Value = "/en/sibfawards";
                            }
                            else
                            {
                                obj.Key = award.URLTitle;
                                obj.Value = award.Url;
                            }

                            obj.IsURL = true;
                            menu.Add(obj);
                        }

                        #endregion
                    }
                    details.AwardMenuList = menu;
                    return Ok(details);
                }
                else
                {
                    AwardsItemDetailsDTO award = new AwardsItemDetailsDTO();
                    var thumbnailpath = _appCustomSettings.UploadsCMSPath + "/Awards/Thumbnail/";
                    if (subawardid != null && subawardid > 0)
                    {
                        award = await _context.XsiAwards.Where(p => p.IsActiveAr == "Y")
                    .Join(_context.XsiSubAwards.Where(p => p.IsActiveAr == "Y" && p.ItemId == subawardid), a => a.ItemId, sa => sa.AwardId, (a, sa) =>
                             new AwardsItemDetailsDTO
                             {
                                 ItemId = sa.ItemId,
                                 Title = sa.NameAr,
                                 Thumbnail = thumbnailpath + a.ThumbnailAr,
                                 Introduction = sa.IntroductionAr,
                                 //Category = sa.CategoryAr,
                                 //Objectives = sa.ObjectivesAr,
                                 Conditions = sa.ConditionsAr,
                                 Requirements = sa.RrequirementsAr,
                                 URLTitle = sa.NameAr,
                                 Url = GetURL(awardid.Value, subawardid, LangId)
                             }).FirstOrDefaultAsync();

                        if (award != null)
                        {
                            details.ItemId = award.ItemId;
                            details.Name = award.Title;
                            details.Thumbnail = award.Thumbnail;
                        }
                    }
                    else
                    {
                        var predicate = PredicateBuilder.True<XsiAwards>();
                        predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                        predicate = predicate.And(i => (i.WebsiteId == 0 || i.WebsiteId == 1));
                        predicate = predicate.And(i => i.ItemId == awardid);

                        award = await _context.XsiAwards.AsQueryable().Where(predicate).OrderBy(x => x.Name.Trim()).Select(x => new AwardsItemDetailsDTO
                        {
                            ItemId = x.ItemId,
                            Title = x.NameAr,
                            Thumbnail = thumbnailpath + x.ThumbnailAr,
                            Introduction = x.IntroductionAr,
                            Category = x.CategoryAr,
                            Objectives = x.ObjectivesAr,
                            Conditions = x.ConditionsAr,
                            Requirements = x.RequirementsAr,
                            // URLTitle = x.NameAr,
                            URLTitle = GetURLTitle(awardid.Value, x.NameAr, LangId),
                            Url = GetURL(awardid.Value, 0, LangId)
                        }).FirstOrDefaultAsync();

                        if (award != null)
                        {
                            details.ItemId = award.ItemId;
                            details.Name = award.Title;
                            details.Thumbnail = award.Thumbnail;
                        }
                    }
                    if (award != null)
                    {
                        #region Bind Award Menu
                        AwardMenuTabDto obj = new AwardMenuTabDto();

                        if (!string.IsNullOrWhiteSpace(award.Introduction))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "المقدمة";
                            obj.Value = award.Introduction;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Category))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "الفئة";
                            obj.Value = award.Category;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Objectives))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "الأهداف";
                            obj.Value = award.Objectives;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Conditions))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "الشروط العامة";
                            obj.Value = award.Conditions;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Requirements))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "المرفقات اللازمة";
                            obj.Value = award.Requirements;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.URLTitle))
                        {
                            obj = new AwardMenuTabDto();

                            if (award.ItemId == 3 || award.ItemId == 5 || award.ItemId == 7)
                            {
                                obj.Key = "للتسجيل بالجائزة اضغط";
                                obj.Value = "/ar/sibfawards";
                            }
                            else
                            {
                                obj.Key = award.URLTitle;
                                obj.Value = award.Url;
                            }

                            obj.IsURL = true;
                            menu.Add(obj);
                        }
                        #endregion
                    }
                    details.AwardMenuList = menu;
                    return Ok(details);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwardsListById action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }

        // GET: api/AwardListById
        [HttpGet]
        [Route("AwardMenuById/undefined")]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetAwardMenuById()
        {
            try
            {
                long? awardid = -1;
                long? subawardid = -1;
                AwardsDetailsDTO details = new AwardsDetailsDTO();
                List<AwardMenuTabDto> menu = new List<AwardMenuTabDto>();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    AwardsItemDetailsDTO award = new AwardsItemDetailsDTO();
                    var thumbnailpath = _appCustomSettings.UploadsCMSPath + "/Awards/Thumbnail/";
                    if (subawardid != null && subawardid > 0)
                    {
                        award = await _context.XsiAwards.Where(p => p.IsActive == "Y")
                    .Join(_context.XsiSubAwards.Where(p => p.IsActive == "Y" && p.ItemId == subawardid), a => a.ItemId, sa => sa.AwardId, (a, sa) =>
                             new AwardsItemDetailsDTO
                             {
                                 ItemId = sa.ItemId,
                                 Title = sa.Name,
                                 Thumbnail = thumbnailpath + a.Thumbnail,
                                 Introduction = sa.Introduction,
                                 //Category = sa.Category,
                                 //Objectives = sa.Objectives,
                                 Conditions = sa.Conditions,
                                 Requirements = sa.Rrequirements,
                                 URLTitle = GetURLTitle(awardid.Value, sa.Name, LangId),
                                 Url = GetURL(awardid.Value, subawardid, LangId)
                             }).FirstOrDefaultAsync();

                        if (award != null)
                        {
                            details.ItemId = award.ItemId;
                            details.Name = award.Title;
                            details.Thumbnail = award.Thumbnail;
                        }
                    }
                    else
                    {
                        var predicate = PredicateBuilder.True<XsiAwards>();
                        predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                        predicate = predicate.And(i => (i.WebsiteId == 0 || i.WebsiteId == 1));
                        predicate = predicate.And(i => i.ItemId == awardid);

                        award = await _context.XsiAwards.AsQueryable().Where(predicate).OrderBy(x => x.Name.Trim()).Select(x => new AwardsItemDetailsDTO
                        {
                            ItemId = x.ItemId,
                            Title = x.Name,
                            Thumbnail = thumbnailpath + x.Thumbnail,
                            Introduction = x.Introduction,
                            Category = x.Category,
                            Objectives = x.Objectives,
                            Conditions = x.Conditions,
                            Requirements = x.Requirements,
                            // URLTitle = x.Name,
                            URLTitle = GetURLTitle(awardid.Value, x.Name, LangId),
                            Url = GetURL(awardid.Value, 0, LangId)
                        }).FirstOrDefaultAsync();

                        if (award != null)
                        {
                            details.ItemId = award.ItemId;
                            details.Name = award.Title;
                            details.Thumbnail = award.Thumbnail;
                        }
                    }

                    if (award != null)
                    {
                        #region Bind Award Menu
                        AwardMenuTabDto obj = new AwardMenuTabDto();

                        if (!string.IsNullOrWhiteSpace(award.Introduction))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = nameof(award.Introduction);
                            obj.Value = award.Introduction;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Category))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = nameof(award.Category);
                            obj.Value = award.Category;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Objectives))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = nameof(award.Objectives);
                            obj.Value = award.Objectives;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Conditions))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "General Conditions"; //nameof(award.Conditions);
                            obj.Value = award.Conditions;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Requirements))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "Arbitration and Criteria"; //nameof(award.Requirements);
                            obj.Value = award.Requirements;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.URLTitle))
                        {
                            obj = new AwardMenuTabDto();

                            if (award.ItemId == 3 || award.ItemId == 5 || award.ItemId == 7)
                            {
                                obj.Key = "To register click here";
                                obj.Value = "/en/sibfawards";
                            }
                            else
                            {
                                obj.Key = award.URLTitle;
                                obj.Value = award.Url;
                            }

                            obj.IsURL = true;
                            menu.Add(obj);
                        }

                        #endregion
                    }
                    details.AwardMenuList = menu;
                    return Ok(details);
                }
                else
                {
                    AwardsItemDetailsDTO award = new AwardsItemDetailsDTO();
                    var thumbnailpath = _appCustomSettings.UploadsCMSPath + "/Awards/Thumbnail/";
                    if (subawardid != null && subawardid > 0)
                    {
                        award = await _context.XsiAwards.Where(p => p.IsActiveAr == "Y")
                    .Join(_context.XsiSubAwards.Where(p => p.IsActiveAr == "Y" && p.ItemId == subawardid), a => a.ItemId, sa => sa.AwardId, (a, sa) =>
                             new AwardsItemDetailsDTO
                             {
                                 ItemId = sa.ItemId,
                                 Title = sa.NameAr,
                                 Thumbnail = thumbnailpath + a.ThumbnailAr,
                                 Introduction = sa.IntroductionAr,
                                 //Category = sa.CategoryAr,
                                 //Objectives = sa.ObjectivesAr,
                                 Conditions = sa.ConditionsAr,
                                 Requirements = sa.RrequirementsAr,
                                 URLTitle = sa.NameAr,
                                 Url = GetURL(awardid.Value, subawardid, LangId)
                             }).FirstOrDefaultAsync();

                        if (award != null)
                        {
                            details.ItemId = award.ItemId;
                            details.Name = award.Title;
                            details.Thumbnail = award.Thumbnail;
                        }
                    }
                    else
                    {
                        var predicate = PredicateBuilder.True<XsiAwards>();
                        predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                        predicate = predicate.And(i => (i.WebsiteId == 0 || i.WebsiteId == 1));
                        predicate = predicate.And(i => i.ItemId == awardid);

                        award = await _context.XsiAwards.AsQueryable().Where(predicate).OrderBy(x => x.Name.Trim()).Select(x => new AwardsItemDetailsDTO
                        {
                            ItemId = x.ItemId,
                            Title = x.NameAr,
                            Thumbnail = thumbnailpath + x.ThumbnailAr,
                            Introduction = x.IntroductionAr,
                            Category = x.CategoryAr,
                            Objectives = x.ObjectivesAr,
                            Conditions = x.ConditionsAr,
                            Requirements = x.RequirementsAr,
                            // URLTitle = x.NameAr,
                            URLTitle = GetURLTitle(awardid.Value, x.NameAr, LangId),
                            Url = GetURL(awardid.Value, 0, LangId)
                        }).FirstOrDefaultAsync();

                        if (award != null)
                        {
                            details.ItemId = award.ItemId;
                            details.Name = award.Title;
                            details.Thumbnail = award.Thumbnail;
                        }
                    }
                    if (award != null)
                    {
                        #region Bind Award Menu
                        AwardMenuTabDto obj = new AwardMenuTabDto();

                        if (!string.IsNullOrWhiteSpace(award.Introduction))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "المقدمة";
                            obj.Value = award.Introduction;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Category))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "الفئة";
                            obj.Value = award.Category;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Objectives))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "الأهداف";
                            obj.Value = award.Objectives;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Conditions))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "الشروط العامة";
                            obj.Value = award.Conditions;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.Requirements))
                        {
                            obj = new AwardMenuTabDto();
                            obj.Key = "المرفقات اللازمة";
                            obj.Value = award.Requirements;
                            obj.IsURL = false;
                            menu.Add(obj);
                        }

                        if (!string.IsNullOrWhiteSpace(award.URLTitle))
                        {
                            obj = new AwardMenuTabDto();

                            if (award.ItemId == 3 || award.ItemId == 5 || award.ItemId == 7)
                            {
                                obj.Key = "للتسجيل بالجائزة اضغط";
                                obj.Value = "/ar/sibfawards";
                            }
                            else
                            {
                                obj.Key = award.URLTitle;
                                obj.Value = award.Url;
                            }

                            obj.IsURL = true;
                            menu.Add(obj);
                        }
                        #endregion
                    }
                    details.AwardMenuList = menu;
                    return Ok(details);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetXsiAwardsListById action: {ex.InnerException}");
                return Ok("Something went wrong. Please try again later.");
            }
        }



        // GET: api/Awards/ExhibitionYears
        [HttpGet("ExhibitionYears")]
        public async Task<ActionResult<IEnumerable<ExhibitionYearDTO>>> GetExhibitionYears()
        {
            long websiteId = 1;
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var predicate = PredicateBuilder.True<XsiExhibition>();
                predicate = predicate.And(i => i.WebsiteId == websiteId && i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                var years = _context.XsiExhibition.AsQueryable().Where(predicate).OrderBy(i => i.ExhibitionYear).Select(i => new ExhibitionYearDTO { ExhibitionId = i.ExhibitionId, ExhibitionYear = i.ExhibitionYear.Value.ToString() })
                    .OrderByDescending(i => i.ExhibitionYear)
                    .ToListAsync();

                return await years;
            }
            else
            {
                var predicate = PredicateBuilder.True<XsiExhibition>();
                predicate = predicate.And(i => i.WebsiteId == websiteId && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                var years = _context.XsiExhibition.AsQueryable().Where(predicate).OrderBy(i => i.ExhibitionYear).Select(i => new ExhibitionYearDTO { ExhibitionId = i.ExhibitionId, ExhibitionYear = i.ExhibitionYear.Value.ToString() })
                    .OrderByDescending(i => i.ExhibitionYear)
                    .ToListAsync();

                return await years;
            }
        }

        private string GetURLTitle(long awardid, string name, long langid)
        {
            if (awardid == 3 || awardid == 5 || awardid == 7)
                return langid == 1 ? "To register click here" : "للتسجيل بالجائزة اضغط";

            return name;
        }
        private string GetURL(long awardid, long? subawardid, long langid)
        {
            string str = "/en/";
            if (langid == 2)
                str = "/ar/";

            if (awardid == 3 || awardid == 5 || awardid == 7)
            {
                return str + "sibfawards";
            }
            else
            {
                switch (awardid)
                {
                    case 1: return subawardid > 0 ? (str + "awards?awardid=" + awardid + "&subawardid=" + subawardid) : (str + "awards?awardid=" + awardid);
                    //case 3: return str + "AwardFormEmiratiBook";
                    //case 5: return str + "AwardFormArabicNovel";
                    //case 7: return str + "AwardFormInternationalBook";

                    //case 3: return str + "sibfawards";
                    //case 5: return str + "sibfawards";
                    //case 7: return str + "sibfawards";

                    case 9: return str + "AwardFormPublisherRecognition";
                    case 17: return str + "sharjahawardformfortranslation";
                    default:
                        return str + "sibfawards";
                }
            }
        }
    }
}

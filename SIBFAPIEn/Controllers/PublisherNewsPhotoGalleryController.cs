﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using SIBFAPIEn.DTO;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PublisherNewsPhotosGalleryController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;

        public PublisherNewsPhotosGalleryController(IOptions<AppCustomSettings> appCustomSettings)
        {
            this._appCustomSettings = appCustomSettings.Value;
        }

        // GET: api/PublisherNewsPhotosGallery/5
        [HttpGet]
        [Route("{newsid}")]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetXsiNewsPhotosGallery(long newsid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicateNewsPhotosGallery = PredicateBuilder.True<XsiPublisherNewsPhotoGallery>();

                    predicateNewsPhotosGallery = predicateNewsPhotosGallery.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    predicateNewsPhotosGallery = predicateNewsPhotosGallery.And(i => i.NewsId == newsid);
                    var PhotoList = _context.XsiPublisherNewsPhotoGallery.AsQueryable().Where(predicateNewsPhotosGallery).OrderBy(i => i.SortIndex)
                         .Select(i => new
                         {
                             ItemId = i.ItemId,
                             Title = i.Title,
                             SortIndex = i.SortIndex,
                             ImageName = _appCustomSettings.UploadsPath + "/PublisherNewsPhotoGallery/" + (i.ImageName ?? "book-image.jpg"),
                         }).ToList();

                    return await PhotoList.ToAsyncEnumerable().ToList();
                }
                else
                {
                    var predicateNewsPhotosGallery = PredicateBuilder.True<XsiPublisherNewsPhotoGallery>();

                    predicateNewsPhotosGallery = predicateNewsPhotosGallery.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                    predicateNewsPhotosGallery = predicateNewsPhotosGallery.And(i => i.NewsId == newsid);
                    var PhotoList = _context.XsiPublisherNewsPhotoGallery.AsQueryable().Where(predicateNewsPhotosGallery).OrderBy(i => i.SortIndex)
                         .Select(i => new
                         {
                             ItemId = i.ItemId,
                             Title = i.TitleAr,
                             SortIndex = i.SortIndex,
                             ImageName = _appCustomSettings.UploadsPath + "/PublisherNewsPhotoGallery/" + (i.ImageNameAr ?? "book-image.jpg"),
                         }).ToList();

                    return await PhotoList.ToAsyncEnumerable().ToList();
                }
            }
        }

        // GET: api/PublisherNewsPhotosGallery/5/1
        [HttpGet("{newsid}/{id}")]
        public async Task<ActionResult<dynamic>> GetXsiNewsPhotosGallery(long newsid, long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var publisherNewsphoto = await _context.XsiPublisherNewsPhotoGallery.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                    if (publisherNewsphoto == null)
                    {
                        return NotFound();
                    }
                    var itemDTO = new
                    {
                        ItemId = publisherNewsphoto.ItemId,
                        Title = publisherNewsphoto.Title,
                        SortIndex = publisherNewsphoto.SortIndex,
                        ImageName = _appCustomSettings.UploadsPath + "/PublisherNewsPhotoGallery/" + (publisherNewsphoto.ImageName ?? "book-image.jpg"),
                    };
                    return itemDTO;
                }
                else
                {
                    var publisherNewsphoto = await _context.XsiPublisherNewsPhotoGallery.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                    if (publisherNewsphoto == null)
                    {
                        return NotFound();
                    }
                    var itemDTO = new
                    {
                        ItemId = publisherNewsphoto.ItemId,
                        Title = publisherNewsphoto.TitleAr,
                        SortIndex = publisherNewsphoto.SortIndex,
                        ImageName = _appCustomSettings.UploadsPath + "/PublisherNewsPhotoGallery/" + (publisherNewsphoto.ImageNameAr ?? "book-image.jpg"),
                    };
                    return itemDTO;
                }
            }
        }

        private bool XsiNewsPhotoExists(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                return _context.XsiNewsPhotoGallery.Any(e => e.ItemId == id);
            }
        }
    }
}



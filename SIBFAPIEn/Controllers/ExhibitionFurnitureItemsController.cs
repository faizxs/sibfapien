﻿using Contracts;
using LinqKit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SIBFAPIEn.DTO;
using Entities.Models;
using Xsi.ServicesLayer;
using Microsoft.Extensions.Localization;
using System.Text;
using SIBFAPIEn.Utility;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using drawingFont = System.Drawing.Font;
using System.Drawing.Imaging;
using Microsoft.AspNetCore.Cors;
using System.Diagnostics;
namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ExhibitionFurnitureItemsController : ControllerBase
    {

        #region Variables
        private ILoggerManager _logger;
        private readonly IEmailSender _emailSender;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;
        #endregion

        private MessageDTO MessageDTO { get; set; }
        public long SIBFMemberId { get; set; }

        public ExhibitionFurnitureItemsController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings, IEmailSender emailSender, sibfnewdbContext context)
        {
            _logger = logger;
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        #region Get Methods
        [HttpGet]
        [Route("{memberid}/{exhibitorid}")]
        public ActionResult<dynamic> ExhibitionFurnitureItemsPageLoad(long memberid, long exhibitorid)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                long memberId = User.Identity.GetID();
                List<FurnitureItemDTO> furnitureItems = new List<FurnitureItemDTO>();
                MessageDTO MessageDTO = new MessageDTO();
                MessageDTO.MessageTypeResponse = "Error";
                SaveStaffGuestBookingDTO dto = new SaveStaffGuestBookingDTO();
                if (memberId == memberid)
                {
                    var Entity = _context.XsiExhibitionMemberApplicationYearly.Where(i => i.MemberExhibitionYearlyId == exhibitorid && (i.MemberRoleId == 1 || i.MemberRoleId == 8)).FirstOrDefault();
                    if (Entity != null)
                    {
                        if (Entity.Status == "I" || Entity.Status == "A")
                        {
                            string strpath = _appCustomSettings.UploadsPath + "/ExhibitionFurnitureItem/Thumbnail/";
                            if (LangId == 1)
                            {
                                furnitureItems = _context.XsiExhibitionFurnitureItems.Where(i => i.IsActive == "Y").Select(i => new FurnitureItemDTO()
                                {
                                    FurnitureId = i.ItemId,
                                    Title = i.Title,
                                    Details = i.Description,
                                    Thumbnail = strpath + (i.Thumbnail ?? "default.jpg"),
                                    Price = i.Price ?? 0
                                }).ToList();
                            }
                            else
                            {
                                furnitureItems = _context.XsiExhibitionFurnitureItems.Where(i => i.IsActiveAr == "Y").Select(i => new FurnitureItemDTO()
                                {
                                    FurnitureId = i.ItemId,
                                    Title = i.TitleAr,
                                    Details = i.DescriptionAr,
                                    Thumbnail = strpath + (i.ThumbnailAr ?? "default.jpg"),
                                    Price = i.Price ?? 0
                                }).ToList();
                            }

                            return Ok(new { MessageTypeResponse = "Success", MemberId = memberid, ExhibitorId = exhibitorid, FurnitureItemList = furnitureItems });
                        }

                        MessageDTO.Message = "Order can be placed only after Initial approval or Approved status.";
                        return Ok(MessageDTO);
                    }
                }

                MessageDTO.Message = "Unauthorized access";
                return Ok(MessageDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Furniture Item action: {ex.InnerException}");
                MessageDTO.Message = "Something went wrong. Please try again later.";
                return Ok(MessageDTO);
            }
        }

        #endregion
        #region Post Methods
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> PostSaveData(FurnitureItemOrderDTO dto)
        {
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                MessageDTO = new MessageDTO();
                MessageDTO.MessageTypeResponse = "Error";
                SIBFMemberId = dto.MemberId;
                long memberId = User.Identity.GetID();
                string strtype = string.Empty;
                EnumResultType xsiresult = EnumResultType.Failed;
                if (memberId == SIBFMemberId)
                {
                    if (dto.ExhbitorId > 0)
                    {
                        using (ExhibitionFurnitureItemOrdersService ExhibitionFurnitureItemOrdersService = new ExhibitionFurnitureItemOrdersService())
                        {
                            XsiExhibitionFurnitureItemOrders order = new XsiExhibitionFurnitureItemOrders();
                            XsiExhibitionFurnitureItemOrderDetails orderdetail;
                            order.ExhibitorId = dto.ExhbitorId;
                            order.IsActive = EnumConversion.ToString(EnumBool.Yes);
                            order.Status = "N";
                            order.SubTotal = dto.Subtotal;
                            order.CreatedOn = MethodFactory.ArabianTimeNow();
                            order.ModifiedOn = MethodFactory.ArabianTimeNow();
                            xsiresult = ExhibitionFurnitureItemOrdersService.InsertExhibitionFurnitureItemOrders(order);
                            var orderitemid = ExhibitionFurnitureItemOrdersService.XsiItemdId;

                            var orderentity = ExhibitionFurnitureItemOrdersService.GetExhibitionFurnitureItemOrdersByItemId(orderitemid);
                            if (orderentity != null)
                            {
                                orderentity.InvoiceNumber = orderitemid.ToString();
                                xsiresult = ExhibitionFurnitureItemOrdersService.UpdateExhibitionFurnitureItemOrders(orderentity);
                            }

                            foreach (FurnitureItemPostDTO item in dto.OrderItems)
                            {
                                ExhibitionFurnitureItemOrderDetailsService ExhibitionFurnitureItemOrderDetailsService = new ExhibitionFurnitureItemOrderDetailsService();
                                orderdetail = new XsiExhibitionFurnitureItemOrderDetails();
                                orderdetail.FurnitureItemId = item.FurnitureId;
                                orderdetail.Price = item.Price;
                                orderdetail.Quantity = item.Quantity;
                                orderdetail.Total = item.ItemTotal;
                                orderdetail.IsActive = EnumConversion.ToString(EnumBool.Yes);
                                orderdetail.CreatedOn = MethodFactory.ArabianTimeNow();
                                orderdetail.ModifiedOn = MethodFactory.ArabianTimeNow();
                                ExhibitionFurnitureItemOrderDetailsService.InsertExhibitionFurnitureItemOrderDetails(orderdetail);
                            }

                            ExhibitionFurnitureItemInvoiceStatusLogService ExhibitionFurnitureItemInvoiceStatusLogService = new ExhibitionFurnitureItemInvoiceStatusLogService();
                            XsiExhibitionFurnitureItemStatusLogs log = new XsiExhibitionFurnitureItemStatusLogs();
                            log.InvoiceId = orderitemid;
                            log.ExhibitorId = dto.ExhbitorId;
                            log.Status = "N";
                            log.CreatedOn = MethodFactory.ArabianTimeNow();
                            ExhibitionFurnitureItemInvoiceStatusLogService.InsertExhibitionFurnitureItemInvoiceStatusLog(log);

                            MessageDTO.Message = "Furniture order submitted successfully.";
                            MessageDTO.MessageTypeResponse = "Success";
                            return Ok(MessageDTO);
                        }
                    }

                    MessageDTO.Message = "ExhibitorId is missing.";
                    return Ok(MessageDTO);
                }

                MessageDTO.Message = "Unauthorized access";
                return Ok(MessageDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside SaveData action: {ex.InnerException}");
                MessageDTO.MessageTypeResponse = "Error";
                MessageDTO.Message = "Something went wrong. Please try again later.";
                return Ok(MessageDTO);
            }
        }
        #endregion
    }
}

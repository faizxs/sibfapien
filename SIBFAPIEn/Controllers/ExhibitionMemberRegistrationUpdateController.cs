﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using Entities.Models;
using SIBFAPIEn.Utility;
using System;
using System.Linq;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ExhibitionMemberRegistrationUpdateController : ControllerBase
    {

        private readonly AppCustomSettings _appCustomSettings;
        EnumResultType XsiResult { get; set; }
        private readonly IEmailSender _emailSender;
        private readonly sibfnewdbContext _context;
        public ExhibitionMemberRegistrationUpdateController(IEmailSender emailSender, IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            _emailSender = emailSender;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        // GET: api/GetExhibitionMemberRegistration
        [HttpGet]
        [Route("{memberid}")]
        public ActionResult<EditExhibitionMemberDTO> GetExhibitionMemberRegistration(long memberid)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            long memberId = User.Identity.GetID();
            if (memberid == memberId)
            {
                var xsiItem = _context.XsiExhibitionMember.Where(i => i.MemberId == memberid && i.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.Approved)).FirstOrDefault();

                if (xsiItem == null)
                {
                    return NotFound();
                }

                EditExhibitionMemberDTO itemDTO = new EditExhibitionMemberDTO();
                itemDTO.MemberId = xsiItem.MemberId;
                itemDTO.LanguageURL = xsiItem.LanguageUrl ?? 1;
                // itemDTO.WebsiteId = xsiItem.WebsiteId;
                itemDTO.FirstName = xsiItem.Firstname;
                itemDTO.LastName = xsiItem.LastName;
                itemDTO.FirstNameAr = xsiItem.FirstNameAr;
                itemDTO.LastNameAr = xsiItem.LastNameAr;
                itemDTO.CompanyName = xsiItem.CompanyName;
                itemDTO.CompanyNameAr = xsiItem.CompanyNameAr;
                itemDTO.ContactTitle = xsiItem.JobTitle;
                itemDTO.ContactTitleAr = xsiItem.JobTitleAr;
                itemDTO.CompanyFieldofWork = xsiItem.CompanyFieldofWork;
                itemDTO.CompanyFieldofWorkAr = xsiItem.CompanyFieldofWorkAr;

                if (xsiItem.CityId != null)
                {
                    itemDTO.CountryId = GetCountryIdByCityId(xsiItem.CityId);
                    itemDTO.CityId = xsiItem.CityId;
                }


                //itemDTO.OtherCity = xsiItem;
                itemDTO.Email = xsiItem.Email;
                itemDTO.Website = xsiItem.Website;


                #region Phone, Mobile,Fax
                if (xsiItem.Phone != null)
                {
                    string[] str = xsiItem.Phone.Split('$');
                    if (str.Count() == 3)
                    {
                        itemDTO.ISD = str[0].Trim();
                        itemDTO.STD = str[1].Trim();
                        itemDTO.Phone = str[2].Trim();
                    }
                    else if (str.Count() == 2)
                    {
                        itemDTO.STD = str[0].Trim();
                        itemDTO.Phone = str[1].Trim();
                    }
                    else
                        itemDTO.Phone = str[0].Trim();
                }
                if (xsiItem.Mobile != null)
                {
                    string[] str = xsiItem.Mobile.Split('$');
                    if (str.Count() == 3)
                    {
                        itemDTO.MobileISD = str[0].Trim();
                        itemDTO.MobileSTD = str[1].Trim();
                        itemDTO.Mobile = str[2].Trim();
                    }
                    else if (str.Count() == 2)
                    {
                        itemDTO.MobileSTD = str[0].Trim();
                        itemDTO.Mobile = str[1].Trim();
                    }
                    else
                        itemDTO.Mobile = str[0].Trim();
                }
                if (xsiItem.Fax != null)
                {
                    string[] str = xsiItem.Fax.Split('$');
                    if (str.Count() == 3)
                    {
                        itemDTO.FaxISD = str[0].Trim();
                        itemDTO.FaxSTD = str[1].Trim();
                        itemDTO.Fax = str[2].Trim();
                    }
                    else if (str.Count() == 2)
                    {
                        itemDTO.FaxSTD = str[0].Trim();
                        itemDTO.Fax = str[1].Trim();
                    }
                    else
                        itemDTO.Fax = str[0].Trim();
                }
                #endregion

                itemDTO.POBox = xsiItem.Pobox;
                itemDTO.Address = xsiItem.Address;
                itemDTO.UserName = xsiItem.UserName;

                //itemDTO.ReasondIds = xsiItem.MemberId;
                //itemDTO.OtherReason = xsiItem.MemberId;

                return Ok(itemDTO);
            }
            return Unauthorized();
        }

        // POST: api/ExhibitionMemberRegistrationModify
        [HttpPost]
        [EnableCors("AllowOrigin")]
        public ActionResult<dynamic> PostXsiExhibitionMember(EditExhibitionMemberDTO itemDto)
        {
            // long categoryId = itemDTO.CategoryId ?? -1;
            MessageDTO MessageDTO = new MessageDTO();
            EnumResultType enummailsent = EnumResultType.Failed;
            try
            {
                long memberid = User.Identity.GetID();
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);

                long websiteid = 1;
                long.TryParse(Request.Headers["websiteId"], out websiteid);

                if (websiteid > 0)
                {
                    itemDto.WebsiteId = websiteid;
                }
                else
                {
                    websiteid = 1;
                    itemDto.WebsiteId = websiteid;
                }

                if (memberid == itemDto.MemberId)
                {
                    XsiExhibitionMember memberEntity = _context.XsiExhibitionMember.Where(i =>
                            i.MemberId == memberid &&
                            i.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.Approved))
                        .FirstOrDefault();
                    string strMessage = string.Empty;
                    if (IsEmailUnique(itemDto.Email, itemDto.MemberId))
                    {
                        #region Update

                        XsiExhibitionMember existingEntity = _context.XsiExhibitionMember
                            .Where(i => i.MemberId == itemDto.MemberId).FirstOrDefault();
                        if (memberEntity != null)
                        {
                            if ((existingEntity.CityId != itemDto.CityId) && itemDto.CityId != null)
                                memberEntity.CityId = itemDto.CityId;
                            if (!string.IsNullOrEmpty(itemDto.OtherCity))
                                memberEntity.OtherCity = itemDto.OtherCity;
                            //if (itemDto.CountryId>0)
                            //    entity.countr = long.Parse(ddlExhibitionCountry.SelectedValue);

                            //if ((existingEntity.CityId != itemDto.CityId) && itemDto.CityId != null)
                            //    entity.CityId = CheckIfNewCityAndAdd(itemDto.CityId, itemDto.OtherCity);


                            if (LangId == 1)
                            {
                                memberEntity.FirstNameEmail = itemDto.FirstName;
                                memberEntity.LastNameEmail = itemDto.LastName;
                            }
                            else if (LangId == 2)
                            {
                                memberEntity.FirstNameEmail = itemDto.FirstNameAr;
                                memberEntity.LastNameEmail = itemDto.LastNameAr;
                            }
                            if (!string.IsNullOrEmpty(itemDto.FirstName))
                                memberEntity.Firstname = itemDto.FirstName;
                            if (!string.IsNullOrEmpty(itemDto.LastName))
                                memberEntity.LastName = itemDto.LastName;

                            memberEntity.FirstNameAr = itemDto.FirstNameAr;
                            memberEntity.LastNameAr = itemDto.LastNameAr;
                            memberEntity.CompanyNameAr = itemDto.CompanyNameAr;
                            memberEntity.JobTitleAr = itemDto.ContactTitleAr;
                            memberEntity.CompanyFieldofWorkAr = itemDto.CompanyFieldofWorkAr;

                            if (!string.IsNullOrEmpty(itemDto.CompanyName))
                                memberEntity.CompanyName = itemDto.CompanyName;

                            if (!string.IsNullOrEmpty(itemDto.ContactTitle))
                                memberEntity.JobTitle = itemDto.ContactTitle;

                            if (!string.IsNullOrEmpty(itemDto.CompanyFieldofWork))
                                memberEntity.CompanyFieldofWork = itemDto.CompanyFieldofWork;

                            memberEntity.Phone = itemDto.ISD + "$" + itemDto.STD + "$" + itemDto.Phone;
                            memberEntity.Mobile = itemDto.MobileISD + "$" + itemDto.MobileSTD + "$" + itemDto.Mobile;
                            memberEntity.Fax = itemDto.FaxISD + "$" + itemDto.FaxSTD + "$" + itemDto.Fax;

                            memberEntity.EmailNew = itemDto.Email;
                            memberEntity.Website = itemDto.Website;
                            memberEntity.Pobox = itemDto.POBox;
                            memberEntity.Address = itemDto.Address;
                            memberEntity.ModifiedOn = MethodFactory.ArabianTimeNow();
                            memberEntity.ModifiedById = memberid;
                            if (memberEntity.CompanyName.ToLower() != existingEntity.CompanyName.ToLower())
                                memberEntity.EditRequest = EnumConversion.ToString(EnumExhibitionMemberEditStatus.New);
                            if (_context.SaveChanges() > 0)
                            {
                                long itemId = memberEntity.MemberId;

                                if (existingEntity != null)
                                {
                                    if (memberEntity.EmailNew.ToLower() != existingEntity.Email.ToLower())
                                    {
                                        enummailsent = SendVerificationLinkToMember(itemId, itemDto.WebsiteId);
                                        if (LangId == 1)
                                        {
                                            if (enummailsent == EnumResultType.EmailSent || enummailsent == EnumResultType.Success)
                                                strMessage =
                                                    "An email is sent to the new email id. Please verify it by clicking the link that we sent to the new email id.";
                                            else
                                                strMessage +=
                                                    "Failed to send verification email. Please contact SIBF admin for approval of email change.";
                                        }
                                        else
                                        {
                                            if (enummailsent == EnumResultType.EmailSent || enummailsent == EnumResultType.Success)
                                                strMessage =
                                                    "An email is sent to the new email id. Please verify it by clicking the link that we sent to the new email id.";
                                            else
                                                strMessage +=
                                                    "Failed to send verification email. Please contact SIBF admin for approval of email change.";
                                        }
                                    }

                                    if (memberEntity.CompanyName.ToLower() != existingEntity.CompanyName.ToLower() ||
                                        memberEntity.CompanyNameAr.ToLower() != existingEntity.CompanyNameAr.ToLower())
                                    {
                                        enummailsent = SendCompanyChangeEmailToAdmin(existingEntity, itemDto.CompanyName,
                                            itemDto.CompanyNameAr);

                                        if (LangId == 1)
                                        {
                                            if (enummailsent == EnumResultType.EmailSent || enummailsent == EnumResultType.Success)
                                                strMessage += "<br>The company name is sent for admin approval.";
                                            else
                                                strMessage +=
                                                    "<br>Failed to send company name approval email to admin. Please contact SIBF admin for approval of company name.";

                                        }
                                        else
                                        {
                                            if (enummailsent == EnumResultType.EmailSent || enummailsent == EnumResultType.Success)
                                                strMessage += "<br>The company name is sent for admin approval.";
                                            else
                                                strMessage +=
                                                    "<br>Failed to send company name approval email to admin. Please contact SIBF admin for approval of company name.";
                                        }
                                    }
                                }

                                //LogMemberStatus(entity);
                                if (XsiResult == EnumResultType.Success && enummailsent == EnumResultType.EmailSent)
                                    return Ok(new MessageDTO { Message = strMessage, MessageTypeResponse = "Success" });
                                else if (XsiResult == EnumResultType.Success)
                                    return LangId == 1 ? Ok(new MessageDTO { Message = "Registration Updated", MessageTypeResponse = "Success" }) : Ok(new MessageDTO { Message = "Registration Updated", MessageTypeResponse = "Success" }); //ShowMessage(Resources.BackendError.RegistrationUpdated, EnumMessageType.Success);
                                else
                                    return LangId == 1 ? Ok(new MessageDTO { Message = "Update Failure", MessageTypeResponse = "Error" }) : Ok(new MessageDTO { Message = "Update Failure", MessageTypeResponse = "Error" });
                                //  ShowMessage(Resources.BackendError.UpdateFailure, EnumMessageType.Error);
                                // ResetControls();
                                //pnlForm.Visible = false;
                                //dvButtons.Visible = false;
                            }
                        }

                        return LangId == 1 ? Ok(new MessageDTO { Message = "Update Failure", MessageTypeResponse = "Error" }) : Ok(new MessageDTO { Message = "Update Failure", MessageTypeResponse = "Error" });
                        // ShowMessage(Resources.BackendError.UpdateFailure, EnumMessageType.Error);

                        #endregion
                    }
                    return LangId == 1 ? Ok(new MessageDTO { Message = "Email Already Exist", MessageTypeResponse = "Error" }) : Ok(new MessageDTO { Message = "Email Already Exist", MessageTypeResponse = "Error" });
                    //ShowMessage(Resources.BackendError.EmailAlreadyExist, EnumMessageType.Error);
                }

                return Unauthorized();
            }
            catch (Exception ex)
            {
                return Ok("Exception: Something went wrong. Please retry again later.");
                //throw;
            }
        }

        //[AllowAnonymous]
        [HttpPost]
        [EnableCors("AllowOrigin")]
        [Route("ChangePasswordWithNewPattern")]
        public ActionResult<dynamic> ChangePasswordWithNewPattern([FromBody] VerifyForgotPasswordDTO dto)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            long itemid = -1;
            string strMessage = "Change Password Process Failed";
            long memberid = User.Identity.GetID();
            if (memberid.ToString() == dto.id)
            {
                if (IsValidLongQueryStringKey(dto.id, out itemid, false))
                {
                    XsiExhibitionMember memeberentity = _context.XsiExhibitionMember.Find(itemid);
                    if (memeberentity != null)
                    {
                        if (memeberentity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.Approved))
                        {
                            //mandatory line
                            memeberentity.MemberId = itemid;
                            //form related 
                            //memeberentity.Password = dto.Password;
                            byte[] passwordHash, passwordSalt;
                            PasswordHasher.CreatePasswordHash(dto.Password, out passwordHash, out passwordSalt);
                            memeberentity.PasswordSalt = passwordSalt;
                            memeberentity.PasswordHash = passwordHash;
                            memeberentity.IsNewPasswordChange = EnumConversion.ToString(EnumBool.Yes);
                            memeberentity.ModifiedById = itemid;
                            memeberentity.ModifiedOn = MethodFactory.ArabianTimeNow();
                            if (_context.SaveChanges() > 0)
                                XsiResult = EnumResultType.Success;

                            if (XsiResult == EnumResultType.Success)
                                strMessage = "Password changed successfuly.";
                            else if (XsiResult == EnumResultType.Failed)
                                strMessage = "Please try again password not changed."; //Resources.BackendError.AccountAcitvationFailed;
                        }
                        else
                            strMessage = "User is not authorized to login at the moment."; // Resources.BackendError.InactiveUserState;
                        return Ok(strMessage);
                    }
                }
            }
            return Ok(strMessage);
        }


        #region Methods
        long CheckIfNewCityAndAdd(long? entityCityId, string cityName = "")
        {
            var exhibitionCity = _context.XsiExhibitionCity.Find(entityCityId);
            if (exhibitionCity != null)
            {
                if (exhibitionCity.CityName == "Other" || exhibitionCity.CityNameAr == "أخرى")
                {
                    if (!string.IsNullOrWhiteSpace(cityName))
                    {
                        XsiExhibitionCity city = new XsiExhibitionCity();
                        city.CityName = cityName;
                        city.CityNameAr = cityName;
                        _context.XsiExhibitionCity.Add(city);
                        _context.SaveChanges();
                        return city.CityId;
                    }
                }
            }

            return entityCityId ?? -1;
        }

        void LogMemberStatus(XsiExhibitionMember exhibitionMember)
        {
            XsiExhibitionMemberStatusLog entity = new XsiExhibitionMemberStatusLog();
            entity.ExhibitionMemberId = exhibitionMember.MemberId;
            entity.Status = exhibitionMember.Status;
            entity.CreatedOn = MethodFactory.ArabianTimeNow();
            _context.XsiExhibitionMemberStatusLog.Add(entity);
            _context.SaveChanges();
        }

        bool IsEmailUnique(string email, long memberId)
        {
            if (_context.XsiExhibitionMember.Any(i => i.Email == email && i.MemberId != memberId))
                return false;

            if (_context.XsiExhibitionMember.Any(i => i.EmailNew == email && i.MemberId != memberId))
                return false;
            return true;
        }

        EnumResultType SendVerificationLinkToMember(long memberId, long websiteId = 1)
        {
            XsiResult = EnumResultType.Failed;
            var member = _context.XsiExhibitionMember.Find(memberId);
            if (member != default(XsiExhibitionMember))
            {
                //mandatory line
                // XsiItemdId = entity.ItemId;
                string encryptedMemberId = memberId.ToString();// MethodFactory.Encrypt(memberId.ToString(), true);
                if (websiteId == 2)
                {
                    #region Send Login details
                    StringBuilder body = new StringBuilder();
                    string verifyAccLink = _appCustomSettings.ServerAddressSCRF + "en/VerifyEditProfileAccount?mid=" + encryptedMemberId; // Page.ResolveClientUrl(string.Format("{0}Content/EmailMedia/VerifyExhibitionMemberAccount.aspx?mid={1}", serverAddress, encryptedMemberId));
                    string deleteAccLink = _appCustomSettings.ServerAddressSCRF + "en/VerifyEditProfileAccount?mid=" + encryptedMemberId; //Page.ResolveClientUrl(string.Format("{0}Content/EmailMedia/DeleteExhibitionMemberAccount.aspx?mid={1}", serverAddress, encryptedMemberId));
                    XsiScrfemailContent emailContent = _context.XsiScrfemailContent.Where(i => i.ItemId == 10001).FirstOrDefault();// EmailContentService.GetEmailContent(3, entity.LanguageId.Value);
                    if (emailContent != null && !string.IsNullOrEmpty(member.EmailNew))
                    {
                        body.Append(GetEmailContentAndTemplate(1, emailContent.Body, member, _appCustomSettings.ServerAddressSCRF, websiteId));
                        body.Replace("$$EmailAddress$$", member.EmailNew);
                        body.Replace("$$ActivateMemberLink$$", verifyAccLink);
                        body.Replace("$$LoginPage$$", _appCustomSettings.ServerAddressSCRF + "en/home"); //Page.ResolveClientUrl(string.Format("{0}{1}", serverAddress, "default"))
                        body.Replace("$$pstyle$$", "");
                        body.Replace("$$pstyle$$", "");

                        XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminNameSCRF, _appCustomSettings.AdminEmail, member.EmailNew, emailContent.Subject + " (" + member.EmailNew + ")", body.ToString(), null);
                    }
                    #endregion
                }
                else
                {
                    #region Send Login details
                    StringBuilder body = new StringBuilder();
                    string verifyAccLink = _appCustomSettings.ServerAddressNew + "en/VerifyEditProfileAccount?mid=" + encryptedMemberId; // Page.ResolveClientUrl(string.Format("{0}Content/EmailMedia/VerifyExhibitionMemberAccount.aspx?mid={1}", serverAddress, encryptedMemberId));
                    string deleteAccLink = _appCustomSettings.ServerAddressNew + "en/VerifyEditProfileAccount?mid=" + encryptedMemberId; //Page.ResolveClientUrl(string.Format("{0}Content/EmailMedia/DeleteExhibitionMemberAccount.aspx?mid={1}", serverAddress, encryptedMemberId));
                    XsiEmailContent emailContent = _context.XsiEmailContent.Where(i => i.ItemId == 10001).FirstOrDefault();// EmailContentService.GetEmailContent(3, entity.LanguageId.Value);
                    if (emailContent != null && !string.IsNullOrEmpty(member.EmailNew))
                    {
                        body.Append(GetEmailContentAndTemplate(1, emailContent.Body, member, _appCustomSettings.ServerAddressNew, websiteId));
                        body.Replace("$$EmailAddress$$", member.EmailNew);
                        body.Replace("$$ActivateMemberLink$$", verifyAccLink);
                        body.Replace("$$LoginPage$$", _appCustomSettings.ServerAddressNew + "en/home"); //Page.ResolveClientUrl(string.Format("{0}{1}", serverAddress, "default"))
                        body.Replace("$$pstyle$$", "");
                        body.Replace("$$pstyle$$", "");

                        XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, member.EmailNew, emailContent.Subject + " (" + member.EmailNew + ")", body.ToString(), null);
                    }
                    #endregion
                }

            }

            return XsiResult;
        }

        EnumResultType SendCompanyChangeEmailToAdmin(XsiExhibitionMember entity, string newCompanyName, string newCompanyNameAr, long websiteId = 1)
        {
            XsiResult = EnumResultType.Failed;
            if (entity != null)
            {
                var member = _context.XsiExhibitionMember.Find(entity.MemberId);
                //mandatory line
                #region Member details

                StringBuilder body = new StringBuilder();

                //XsiEmailContent emailContent = EmailContentService.GetEmailContent(62, EnglishId);
                var emailContent = _context.XsiEmailContent.Where(i => i.ItemId == 20041).FirstOrDefault();
                if (emailContent != null)
                {
                    string strContent = "<p style=\"font-size: 16px; line-height: 32px; font-family: Arial, Helvetica, sans-serif; color: #555555; margin: 0px;\">$$Label$$ : $$Value$$</p>";
                    string strContentAr = "<p style=\"font-size: 16px; line-height: 32px; font-family: Arial, Helvetica, sans-serif; color: #555555; margin: 0px;\">$$LabelAr$$ : $$ValueAr$$</p>";
                    body.Append(GetEmailContentAndTemplate(1, emailContent.Body, member, _appCustomSettings.ServerAddressNew, websiteId));

                    #region English
                    if (newCompanyName != string.Empty)
                    {
                        body.Replace("$$NewCompanyName$$", strContent);
                        body.Replace("$$Label$$", "Company Name");
                        body.Replace("$$Value$$", newCompanyName);
                    }
                    else
                        body.Replace("$$NewCompanyName$$", string.Empty);
                    if (entity.CompanyName != null)
                    {
                        body.Replace("$$OldCompanyName$$", strContent);
                        body.Replace("$$Label$$", "Company Name");
                        body.Replace("$$Value$$", entity.CompanyName);
                    }
                    else
                        body.Replace("$$OldCompanyName$$", string.Empty);
                    #endregion

                    #region Arabic
                    if (newCompanyNameAr != string.Empty)
                    {
                        body.Replace("$$NewCompanyNameAr$$", strContentAr);
                        body.Replace("$$LabelAr$$", "Company Name in Arabic");
                        body.Replace("$$ValueAr$$", newCompanyNameAr);
                    }
                    else
                        body.Replace("$$NewCompanyNameAr$$", string.Empty);
                    if (entity.CompanyNameAr != null)
                    {
                        body.Replace("$$OldCompanyNameAr$$", strContentAr);
                        body.Replace("$$LabelAr$$", "Company Name in Arabic");
                        body.Replace("$$ValueAr$$", entity.CompanyNameAr);
                    }
                    else
                        body.Replace("$$OldCompanyNameAr$$", string.Empty);
                    #endregion

                    if (entity.FileNumber != null)
                        body.Replace("$$FileNumber$$", entity.FileNumber);
                    if (entity.Firstname != null && entity.LastName != null)
                        body.Replace("$$MemberName$$", entity.Firstname + " " + entity.LastName);
                    else
                    {
                        if (entity.FirstNameEmail != null)
                            body.Replace("$$MemberName$$", entity.FirstNameEmail);
                        else if (entity.LastNameEmail != null)
                            body.Replace("$$MemberName$$", entity.LastNameEmail);
                        else
                            body.Replace("$$MemberName$$", string.Empty);
                    }
                    if (emailContent.Email != null)
                        XsiResult = _emailSender.SendEmail(_appCustomSettings.AdminName, _appCustomSettings.AdminEmail, emailContent.Email, emailContent.Subject, body.ToString());

                    // XsiResult = SmtpEmail.SendNetEmail(AdminName, AdminEmailAddress, emailContent.Email, emailContent.Subject, body.ToString());
                    if (XsiResult == EnumResultType.Success)
                        XsiResult = EnumResultType.EmailSent;
                    return XsiResult;
                }

                #endregion
            }
            return XsiResult;
        }

        private long? GetCountryIdByCityId(long? cityId)
        {
            var city = _context.XsiExhibitionCity.Where(i => i.CityId == cityId).FirstOrDefault();
            if (city != null)
            {
                return city.CountryId;
            }
            return -1;
        }
        #endregion

        #region Email Utility
        StringBuilder GetEmailContentAndTemplate(long languageId, string bodyContent, XsiExhibitionMember entity, string serverAddress, long Wid)
        {
            string scrfURL = _appCustomSettings.ServerAddressSCRF;
            string emailContentTemplate = string.Empty;
            if (languageId == 1)
            {
                if (Wid == Convert.ToInt64(EnumWebsiteId.SCRF))
                    emailContentTemplate = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentForSCRF.html";
                else
                    emailContentTemplate = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContent.html";
            }
            else
            {
                if (Wid == Convert.ToInt64(EnumWebsiteId.SCRF))
                    emailContentTemplate = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabicForSCRF.html";
                else
                    emailContentTemplate = _appCustomSettings.ApplicationPhysicalPath + "content\\EmailMedia\\EmailContentArabic.html";
            }
            StringBuilder body = new StringBuilder();

            if (System.IO.File.Exists(emailContentTemplate))
            {
                body.Append(System.IO.File.ReadAllText(emailContentTemplate));
                body.Replace("$$EmailContent$$", bodyContent);
            }
            else
                body.Append(bodyContent);
            string str = GetExhibitionDetails(Wid);
            string[] strArray = str.Split(',');
            body.Replace("$$exhibitionno$$", strArray[0].ToString());
            body.Replace("$$exhibitiondate$$", strArray[1].ToString());
            body.Replace("$$exhyear$$", strArray[2]);

            string swidth = "width=\"864\"";
            string sreplacewidth = "width=\"100%\"";
            body.Replace(swidth, sreplacewidth);

            body.Replace("$$ServerAddress$$", serverAddress);
            if (Wid == Convert.ToInt64(EnumWebsiteId.SCRF))
                body.Replace("$$ServerAddressSCRF$$", scrfURL);

            if (entity != null)
            {
                string illustratorname = string.Empty;
                if (languageId == 1)
                {
                    if (entity.Firstname != null && entity.LastName != null)
                        illustratorname = entity.Firstname + " " + entity.LastName;
                    else if (entity.Firstname != null)
                        illustratorname = entity.Firstname;
                    else if (entity.LastName != null)
                        illustratorname = entity.LastName;
                }
                else
                {
                    if (entity.FirstNameAr != null && entity.LastNameAr != null)
                        illustratorname = entity.FirstNameAr + " " + entity.LastNameAr;
                    else if (entity.Firstname != null)
                        illustratorname = entity.FirstNameAr;
                    else if (entity.LastName != null)
                        illustratorname = entity.LastNameAr;

                    if (string.IsNullOrEmpty(illustratorname))
                    {
                        if (entity.Firstname != null && entity.LastName != null)
                            illustratorname = entity.Firstname + " " + entity.LastName;
                        else if (entity.Firstname != null)
                            illustratorname = entity.Firstname;
                        else if (entity.LastName != null)
                            illustratorname = entity.LastName;
                    }
                }
                body.Replace("$$Name$$", illustratorname);
            }
            return body;
        }
        string GetExhibitionDetails(long websiteId)
        {
            long? exhibitionNumber = -1;
            string strdate = string.Empty;

            XsiExhibition entity = _context.XsiExhibition.Where(i => i.WebsiteId == websiteId && i.IsActive == EnumConversion.ToString(EnumBool.Yes) && i.IsArchive == EnumConversion.ToString(EnumBool.No)).OrderByDescending(i => i.ExhibitionId).FirstOrDefault();
            string startdaysuffix = string.Empty;
            string enddaysuffix = string.Empty;
            string stryear = MethodFactory.ArabianTimeNow().Year.ToString();

            if (entity != default(XsiExhibition))
            {
                if (entity.ExhibitionNumber != null)
                    exhibitionNumber = entity.ExhibitionNumber;
                long dayno = entity.StartDate.Value.Date.Day;
                if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                    startdaysuffix = "th";
                else
                    startdaysuffix = GetSuffix(entity.StartDate.Value.Date.Day % 10, entity.StartDate.Value);
                dayno = entity.EndDate.Value.Date.Day;
                if (dayno == 10 || dayno == 11 || dayno == 12 || dayno == 13)
                    enddaysuffix = "th";
                else
                    enddaysuffix = GetSuffix(entity.EndDate.Value.Date.Day % 10, entity.EndDate.Value);

                if (entity.StartDate.Value.Year == entity.EndDate.Value.Year)
                {
                    if (entity.StartDate.Value.Month == entity.EndDate.Value.Month)
                        strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.StartDate.Value.ToString("MMM") + " " + entity.StartDate.Value.ToString("yyyy");
                    else
                        strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " + entity.StartDate.Value.ToString("MMM")
                             + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.EndDate.Value.ToString("MMM")
                            + " " + entity.StartDate.Value.ToString("yyyy");
                }
                else
                {
                    strdate = entity.StartDate.Value.Date.Day + startdaysuffix + " " + entity.StartDate.Value.ToString("MMM") + " " + entity.StartDate.Value.ToString("yyyy")
                         + " - " + entity.EndDate.Value.Date.Day + enddaysuffix + " " + entity.EndDate.Value.ToString("MMM")
                        + " " + entity.EndDate.Value.ToString("yyyy");
                }
                if (entity.ExhibitionYear != null)
                    stryear = entity.ExhibitionYear.ToString();
            }
            return exhibitionNumber.ToString() + "," + strdate + "," + stryear;
        }
        string GetSuffix(int startday, DateTime dt)
        {
            string daysuffix;
            if (startday != 11 && dt.Date.Day != 11)
            {
                if (startday == 1)
                    daysuffix = "st";
                else if (startday == 2)
                    daysuffix = "nd";
                else if (startday == 3)
                    daysuffix = "rd";
                else
                    daysuffix = "th";
            }
            else
                daysuffix = "th";
            return daysuffix;
        }

        bool IsValidLongQueryStringKey(string key, out long id, bool isEncrypted = false)
        {
            if (isEncrypted)
            {
                string value = MethodFactory.Decrypt(key);
                if (long.TryParse(value, out id))
                    return true;
            }
            else
            {
                if (long.TryParse(key, out id))
                    return true;
            }

            id = -1;
            return false;
        }
        EnumResultType DeleteAnonymousExhibitionMemberRegistration(long itemId)
        {
            XsiExhibitionMember entity = _context.XsiExhibitionMember.Find(itemId);
            if (entity != null)
            {
                if (entity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.EmailNotVerified))
                {
                    var memberReasons = _context.XsiMemberReason.Where(i => i.MemberId == itemId).ToList();
                    foreach (var reason in memberReasons)
                    {
                        _context.XsiMemberReason.Remove(reason);
                    }
                    _context.SaveChanges();
                    _context.XsiExhibitionMember.Remove(entity);
                    _context.SaveChanges();
                    if (_context.SaveChanges() > 0)
                        return EnumResultType.Success;
                    return EnumResultType.Failed;
                }
                return EnumResultType.ValueInUse;
            }
            return EnumResultType.NotFound;
        }
        #endregion
    }

}

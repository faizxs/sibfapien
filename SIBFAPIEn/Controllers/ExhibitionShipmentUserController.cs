﻿using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SIBFAPIEn.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using Xsi.ServicesLayer;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    public class ExhibitionShipmentUserController : ControllerBase
    {
        private ILoggerManager _logger;
        private readonly AppCustomSettings _appCustomSettings;
        private readonly sibfnewdbContext _context;
        public ExhibitionShipmentUserController(ILoggerManager logger, IOptions<AppCustomSettings> appCustomSettings, sibfnewdbContext context)
        {
            _logger = logger;
            this._appCustomSettings = appCustomSettings.Value;
            _context = context;
        }

        #region Get
        [HttpGet]
        [Route("Shipment/{shipmentid}")]
        public ActionResult<dynamic> GetExhibitionShipmentByItemId(long shipmentid)
        {
            MessageDTO messageDTO = new MessageDTO();
            messageDTO.MessageTypeResponse = "Error";
            try
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                //long SIBFMemberId = User.Identity.GetID();
                using (ExhibitionShipmentService ExhibitionShipmentService = new ExhibitionShipmentService())
                {
                    ExhibitorRegistrationService ExhibitorRegistrationService = new ExhibitorRegistrationService();
                    var ShipmentInfo = ExhibitionShipmentService.GetExhibitionShipmentByItemId(shipmentid);
                    var Exhibitor = ExhibitorRegistrationService.GetExhibitorRegistrationByItemId(ShipmentInfo.ExhibitorId.Value);

                    ExhibitionShipmentItemDTO dto = new ExhibitionShipmentItemDTO();
                    dto.ItemId = ShipmentInfo.ItemId;
                    dto.ExhibitorName = MethodFactory.GetExhibitorName(ShipmentInfo.ExhibitorId ?? -1, LangId);
                    dto.FileNumber = MethodFactory.GetExhibitorFileNumber(ShipmentInfo.ExhibitorId ?? -1);
                    dto.CountryName = MethodFactory.GetCountryNameByExhibitorIdOrAgencyId(ShipmentInfo.ExhibitorId ?? -1, ShipmentInfo.AgencyId ?? -1, LangId);
                    
                    if (LangId == 1)
                        dto.IsAgency = ShipmentInfo.IsAgency == "Y" ? "Yes" : "No";
                    else
                        dto.IsAgency = ShipmentInfo.IsAgency == "Y" ? "نعم" : "لا";
                  
                    dto.AgencyList = GetAgencyListByShipmentId(ShipmentInfo.ItemId, ShipmentInfo.ExhibitorId.Value, LangId);
                    dto.ShipmentCount = ShipmentInfo.ShipmentCount;
                    dto.ShipmentTypeName = MethodFactory.GetShipmentTypeName(ShipmentInfo.ShipmentType ?? -1, LangId);
                    dto.ShipmentCompanyName = ShipmentInfo.ShipmentCompanyName;
                    dto.BillofLading = ShipmentInfo.BillofLading;
                    dto.ShipmentNumber = ShipmentInfo.ShipmentNumber;
                    dto.ShipmentCountryNameFrom = MethodFactory.GetCountryName(ShipmentInfo.ShipmentCountryFrom ?? -1, LangId);
                    dto.ShipmentCountryNameTo = MethodFactory.GetCountryName(ShipmentInfo.ShipmentCountryTo ?? -1, LangId);
                  
                    dto.ReceivedShipmentLocationTypeName = MethodFactory.GetShipmentTypeName(ShipmentInfo.ReceivedShipmentLocationType ?? -1, LangId);

                    if (!string.IsNullOrEmpty(ShipmentInfo.QrfileName))
                        dto.QrfileName = _appCustomSettings.UploadsCMSPath + "/ExhibitionShipment/" + ShipmentInfo.QrfileName;

                    dto.ReceivingCompanyName = ShipmentInfo.ReceivingCompanyName;

                    if (LangId == 1)
                        dto.IsLocalWarehouse = ShipmentInfo.IsLocalWarehouse == "Y" ? "Yes" : "No";
                    else
                        dto.IsLocalWarehouse = ShipmentInfo.IsLocalWarehouse == "Y" ? "نعم" : "لا";

                    dto.NoofBooks = ShipmentInfo.NoofBooks ?? 0;
                    dto.BookFileName = _appCustomSettings.UploadsCMSPath + "/ExhibitionShipment/" + ShipmentInfo.BooksFile;
                  
                    dto.WShipmentCount = ShipmentInfo.WshipmentCount;
                    dto.WShipmentCompanyName = ShipmentInfo.WshipmentCompanyName;
                    dto.WShipmentNumber = ShipmentInfo.WshipmentNumber;
                    dto.WNoofBooks = ShipmentInfo.WnoofBooks;
                    dto.WBookFileName = _appCustomSettings.UploadsCMSPath + "/ExhibitionShipment/WBookFile/" + ShipmentInfo.WbooksFile;

                    return Ok(new { MessageTypeResponse = "Success", dto });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetExhibitionShipment action: {ex.InnerException}");
                return Ok(new MessageDTO() { MessageTypeResponse = "Error", Message = "Something went wrong. Please try again later." });
            }
        }
        #endregion

        #region Methods
        private List<DropdownDataDTO> GetAgencyListByShipmentId(long shipmentid, long exhibitorId, long langId)
        {
            using (AgencyRegistrationService AgencyRegistrationService = new AgencyRegistrationService())
            {
                ExhibitionShipmentAgencyService ExhibitionShipmentAgencyService = new ExhibitionShipmentAgencyService();
                var shipmentagencyids = ExhibitionShipmentAgencyService.GetExhibitionShipmentAgency(new XsiExhibitionShipmentAgency() { ExhibitionShipmentId = shipmentid }).Select(i => i.AgencyId).ToList();
                List<DropdownDataDTO> AgencyList = new List<DropdownDataDTO>();
                if (langId == 1)
                {
                    AgencyList = AgencyRegistrationService.GetAgencyRegistration(new XsiExhibitionMemberApplicationYearly() { ParentId = exhibitorId }, EnumSortlistBy.ByAlphabetAsc)
                        .Where(i => shipmentagencyids.Contains(i.MemberExhibitionYearlyId) && (i.Status == EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved) || i.Status == EnumConversion.ToString(EnumAgencyStatus.SIBFApproved)))
                   .Select(i => new DropdownDataDTO() { ItemId = i.MemberExhibitionYearlyId, Title = i.PublisherNameEn })
                   .ToList();
                }
                else
                {
                    AgencyList = AgencyRegistrationService.GetAgencyRegistration(new XsiExhibitionMemberApplicationYearly() { ParentId = exhibitorId }, EnumSortlistBy.ByAlphabetAsc)
                            .Where(i => shipmentagencyids.Contains(i.MemberExhibitionYearlyId) && (i.Status == EnumConversion.ToString(EnumAgencyStatus.ExhibitorApproved) || i.Status == EnumConversion.ToString(EnumAgencyStatus.SIBFApproved)))
                   .Select(i => new DropdownDataDTO() { ItemId = i.MemberExhibitionYearlyId, Title = i.PublisherNameAr })
                   .ToList();
                }
                return AgencyList;
            }
        }
        #endregion
    }
}



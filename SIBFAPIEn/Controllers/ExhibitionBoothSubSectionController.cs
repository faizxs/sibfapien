﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIBFAPIEn.DTO;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ExhibitionBoothSubSectionController : ControllerBase
    {
        private readonly sibfnewdbContext _context;
        public ExhibitionBoothSubSectionController(sibfnewdbContext context)
        {
            _context = context;
        }

        // GET: api/ExhibitionBoothSubSection/5
        [HttpGet("{categoryid}")]
        public async Task<ActionResult<IEnumerable<SubCategoryDTO>>> GetXsiExhibitionBoothSubSection(int categoryid)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                long websiteId = _context.XsiExhibitionBooth.Where(i => i.ItemId == categoryid).FirstOrDefault().WebsiteType ?? -1;
                var predicate = PredicateBuilder.True<XsiExhibitionBoothSubsection>();
                predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.CategoryId == categoryid);
                predicate = predicate.And(i => (i.WebsiteType == 0 || i.WebsiteType == websiteId));
                predicate = predicate.And(i => i.Title.ToLower() != "restaurant" && i.Title.ToLower() != "cafe");

                var List = _context.XsiExhibitionBoothSubsection.AsQueryable().Where(predicate).OrderBy(x => x.Title).Select(i => new SubCategoryDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.Title,
                    Color = i.Color,
                    CategoryTitle = i.Category.Title,
                    CategoryId = i.Category.ItemId
                }).ToListAsync();

                return await List;
            }
            else
            {
                long websiteId = _context.XsiExhibitionBooth.Where(i => i.ItemId == categoryid).FirstOrDefault().WebsiteType ?? -1;
                var predicate = PredicateBuilder.True<XsiExhibitionBoothSubsection>();
                predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                predicate = predicate.And(i => i.CategoryId == categoryid);
                predicate = predicate.And(i => (i.WebsiteType == 0 || i.WebsiteType == websiteId));
                predicate = predicate.And(i => i.Title.ToLower() != "restaurant" && i.Title.ToLower() != "cafe");

                var List = _context.XsiExhibitionBoothSubsection.AsQueryable().Where(predicate).OrderBy(x => x.TitleAr).Select(i => new SubCategoryDTO()
                {
                    ItemId = i.ItemId,
                    Title = i.TitleAr,
                    Color = i.Color,
                    CategoryTitle = i.Category.TitleAr,
                    CategoryId = i.Category.ItemId
                }).ToListAsync();

                return await List;
            }
        }

        // GET: api/ExhibitionBoothSubSection/5/1
        [HttpGet]
        [Route("GetExhibitionBoothSubSectionById/{id}")]
        public async Task<ActionResult<SubCategoryDTO>> GetXsiExhibitionBoothSubSectionById(long id)
        {
            long LangId = 1;
            long.TryParse(Request.Headers["LanguageURL"], out LangId);
            if (LangId == 1)
            {
                var subCategory = await _context.XsiExhibitionBoothSubsection.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes))
                   .Select(i => new SubCategoryDTO()
                   {
                       ItemId = i.ItemId,
                       Title = LangId == 1 ? i.Title : i.TitleAr,
                       Color = LangId == 1 ? i.Color : i.Color,
                       CategoryTitle = LangId == 1 ? i.Category.Title : i.Category.TitleAr,
                       CategoryId = i.Category.ItemId
                   }).FirstOrDefaultAsync();

                if (subCategory == null)
                {
                    return NotFound();
                }

                return subCategory;
            }
            else
            {
                var subCategory = await _context.XsiExhibitionBoothSubsection.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes))
                   .Select(i => new SubCategoryDTO()
                   {
                       ItemId = i.ItemId,
                       Title = i.TitleAr,
                       Color = i.Color,
                       CategoryTitle = i.Category.TitleAr,
                       CategoryId = i.Category.ItemId
                   }).FirstOrDefaultAsync();

                if (subCategory == null)
                {
                    return NotFound();
                }

                return subCategory;
            }
        }

        private bool XsiExhibitionBoothSubSectionExists(long id)
        {
            return _context.XsiExhibitionBoothSubsection.Any(e => e.ItemId == id);
        }
    }
}

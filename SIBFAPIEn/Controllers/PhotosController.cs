﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities.Models;
using SIBFAPIEn.DTO;
using Microsoft.Extensions.Options;
using System.Dynamic;
using Microsoft.AspNetCore.Authorization;

namespace SIBFAPIEn.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class PhotosController : ControllerBase
    {
        private readonly AppCustomSettings _appCustomSettings;

        public PhotosController(IOptions<AppCustomSettings> appCustomSettings)
        {
            this._appCustomSettings = appCustomSettings.Value;
        }
        // GET: api/Photos
        [HttpGet]
        [Route("{albumid}/{pageNumber}/{pageSize}/{strSearch}")]
        public async Task<ActionResult<dynamic>> GetXsiPhotos(long? albumid = -1, int? pageNumber = 1, int? pageSize = 15, string strSearch = "")
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                PhotosGalleryDetailsDTO model = new PhotosGalleryDetailsDTO(); long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var predicate = PredicateBuilder.True<XsiPhotoAlbum>();

                    predicate = predicate.And(i => i.ItemId == albumid);
                    predicate = predicate.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));
                    if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                        predicate.And(i => i.Dated != null && i.Dated.Value.Year.ToString() == strSearch);

                    XsiPhotoAlbum entity = new XsiPhotoAlbum();
                    entity = _context.XsiPhotoAlbum.AsQueryable().Where(predicate).FirstOrDefault();

                    dynamic album = new ExpandoObject();
                    if (entity != default(XsiPhotoAlbum))
                    {
                        album.ItemId = entity.ItemId;
                        album.Title = entity.Title;
                        album.StrDated = entity.Dated == null ? string.Empty : entity.Dated.Value.ToString("dd MMM yyyy");
                    }

                    #region Photo List
                    var predicatePhotos = PredicateBuilder.True<XsiPhoto>();

                    predicatePhotos = predicatePhotos.And(i => i.IsActive == EnumConversion.ToString(EnumBool.Yes));

                    if (albumid != -1)
                    {
                        predicatePhotos = predicatePhotos.And(i => i.PhotoAlbumId == albumid);
                    }
                    var kount = _context.XsiPhoto.Where(predicatePhotos).Count();
                    List<PhotoDTO> PhotoList = new List<PhotoDTO>();
                    string path = _appCustomSettings.UploadsPath;
                    PhotoList = await _context.XsiPhoto.AsQueryable().Where(predicatePhotos)
                        .Select(i => new PhotoDTO()
                        {
                            ItemId = i.ItemId,
                            Title = i.Title,
                            Description = i.Description,
                            SortIndex = i.SortIndex,
                            Thumbnail = path + "/PhotoGallery/AlbumThumbnailNew/" + (i.Thumbnail ?? "book-image.jpg"),
                            MiniThumbnail = path + "/PhotoGallery/MiniThumbnail/" + (i.Thumbnail ?? "book-image.jpg"),
                            LargeThumbnail = path + "/PhotoGallery/AlbumBanner/" + (i.Thumbnail ?? "book-image.jpg"),
                            CreatedOn = i.CreatedOn
                        }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    //if (PhotoList.Count() > 0)
                    //{
                    //    model.LatestPhotos = PhotoList.OrderByDescending(i => i.CreatedOn).Take(3).ToList();
                    //}
                    #endregion
                    //model.Photos = await PhotoList.ToAsyncEnumerable().ToList();
                    //return model;

                    var dto = new { Album = album, LatestPhotos = PhotoList.OrderByDescending(i => i.CreatedOn).Take(3).ToList(), PhotoList, TotalCount = kount };
                    return dto;
                }
                else
                {
                    var predicate = PredicateBuilder.True<XsiPhotoAlbum>();

                    predicate = predicate.And(i => i.ItemId == albumid);
                    predicate = predicate.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));
                    if (!string.IsNullOrEmpty(strSearch) && strSearch != "-1")
                        predicate.And(i => i.DatedAr != null && i.DatedAr.Value.Year.ToString() == strSearch);

                    XsiPhotoAlbum entity = new XsiPhotoAlbum();
                    entity = _context.XsiPhotoAlbum.AsQueryable().Where(predicate).FirstOrDefault();

                    dynamic album = new ExpandoObject();
                    if (entity != default(XsiPhotoAlbum))
                    {
                        album.ItemId = entity.ItemId;
                        album.Title = entity.TitleAr;
                        album.StrDated = entity.DatedAr == null ? string.Empty : entity.DatedAr.Value.ToString("dd MMM yyyy");
                    }

                    #region Photo List
                    var predicatePhotos = PredicateBuilder.True<XsiPhoto>();

                    predicatePhotos = predicatePhotos.And(i => i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes));

                    if (albumid != -1)
                    {
                        predicatePhotos = predicatePhotos.And(i => i.PhotoAlbumId == albumid);
                    }
                    var kount = _context.XsiPhoto.Where(predicatePhotos).Count();
                    List<PhotoDTO> PhotoList = new List<PhotoDTO>();
                    string path = _appCustomSettings.UploadsPath;
                    PhotoList = await _context.XsiPhoto.AsQueryable().Where(predicatePhotos)
                        .Select(i => new PhotoDTO()
                        {
                            ItemId = i.ItemId,
                            Title = i.TitleAr,
                            Description = i.DescriptionAr,
                            SortIndex = i.SortIndex,
                            Thumbnail = path + "/PhotoGallery/AlbumThumbnailNew/" + (i.ThumbnailAr ?? "book-image.jpg"),
                            MiniThumbnail = path + "/PhotoGallery/MiniThumbnail/" + (i.ThumbnailAr ?? "book-image.jpg"),
                            LargeThumbnail = path + "/PhotoGallery/AlbumBanner/" + (i.ThumbnailAr ?? "book-image.jpg"),
                            CreatedOn = i.CreatedOn
                        }).Skip((pageNumber.Value - 1) * pageSize.Value).Take(pageSize.Value).ToListAsync();

                    //if (PhotoList.Count() > 0)
                    //{
                    //    model.LatestPhotos = PhotoList.OrderByDescending(i => i.CreatedOn).Take(3).ToList();
                    //}
                    #endregion
                    //model.Photos = await PhotoList.ToAsyncEnumerable().ToList();
                    //return model;

                    var dto = new { Album = album, LatestPhotos = PhotoList.OrderByDescending(i => i.CreatedOn).Take(3).ToList(), PhotoList, TotalCount = kount };
                    return dto;
                }
            }
        }

        // GET: api/Photo/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PhotoDTO>> GetXsiPhoto(long id)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                long LangId = 1;
                long.TryParse(Request.Headers["LanguageURL"], out LangId);
                if (LangId == 1)
                {
                    var photo = await _context.XsiPhoto.Where(i => i.ItemId == id && i.IsActive == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                    if (photo == null)
                    {
                        return NotFound();
                    }
                    string path = _appCustomSettings.UploadsPath;
                    PhotoDTO itemDTO = new PhotoDTO()
                    {
                        ItemId = photo.ItemId,
                        Title = photo.Title,
                        Description = photo.Description,
                        SortIndex = photo.SortIndex,
                        Thumbnail = path + "/PhotoGallery/AlbumThumbnailNew/" + (photo.Thumbnail ?? "book-image.jpg"),
                        MiniThumbnail = path + "/PhotoGallery/MiniThumbnail/" + (photo.Thumbnail ?? "book-image.jpg"),
                        LargeThumbnail = path + "/PhotoGallery/AlbumBanner/" + (photo.Thumbnail ?? "book-image.jpg"),
                        CreatedOn = photo.CreatedOn
                    };
                    return itemDTO;
                }
                else
                {
                    var photo = await _context.XsiPhoto.Where(i => i.ItemId == id && i.IsActiveAr == EnumConversion.ToString(EnumBool.Yes)).FirstOrDefaultAsync();

                    if (photo == null)
                    {
                        return NotFound();
                    }
                    string path = _appCustomSettings.UploadsPath;
                    PhotoDTO itemDTO = new PhotoDTO()
                    {
                        ItemId = photo.ItemId,
                        Title = photo.TitleAr,
                        Description = photo.DescriptionAr,
                        SortIndex = photo.SortIndex,
                        Thumbnail = path + "/PhotoGallery/AlbumThumbnailNew/" + (photo.ThumbnailAr ?? "book-image.jpg"),
                        MiniThumbnail = path + "/PhotoGallery/MiniThumbnail/" + (photo.ThumbnailAr ?? "book-image.jpg"),
                        LargeThumbnail = path + "/PhotoGallery/AlbumBanner/" + (photo.ThumbnailAr ?? "book-image.jpg"),
                        CreatedOn = photo.CreatedOn
                    };
                    return itemDTO;
                }
            }
        }
    }
}



﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SIBFAPIEn.DTO;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using Contracts;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Org.BouncyCastle.Crypto.Signers;

namespace SIBFAPIEn
{
    public interface IAdminMemberUserServiceService
    {
        AdminLoggedinUserInfo AuthenticateAdminLoginForMember(string username, string adminusername, string password, long langid);
        AdminLoggedinUserInfo AuthenticateAdminLoginForMemberUsingOTP(string username, string adminusername, string otp, long langid);
    }

    public class AdminMemberUserService : IAdminMemberUserServiceService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ILoggerManager _logger;
        private List<AdminLoggedinUserInfo> _users = new List<AdminLoggedinUserInfo>();
        private readonly AppCustomSettings _appSettings;
        private readonly IConfiguration _configuration;

        public AdminMemberUserService(IOptions<AppCustomSettings> appSettings, IConfiguration configuration, ILoggerManager logger, IHttpContextAccessor httpContextAccessor)
        {
            _appSettings = appSettings.Value;
            _configuration = configuration;
            _logger = logger;
            this._httpContextAccessor = httpContextAccessor;
        }
        public AdminLoggedinUserInfo AuthenticateAdminLoginForMember(string username, string adminusername, string password, long langid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                AdminLoggedinUserInfo userInfo = new AdminLoggedinUserInfo();
                List<long> adminuserIDs = new List<long>(){
                   1,8,10086,10088,10089,10090,10091,10115,10111
                };

                userInfo.IsValidLoggedInMember = false;
                var memberentity = _context.XsiExhibitionMember.SingleOrDefault(x => x.UserName == username && x.Status == "A");
                if (memberentity == null)
                {
                    userInfo.StatusMessage = langid == 1 ? "Bad Request or Invalid username/password" : "Bad Request or Invalid username/password";
                    userInfo.PopUpId = "2";
                    return userInfo;
                }

                var adminuser = _context.XsiAdminUsers.SingleOrDefault(x => x.UserName == adminusername && x.IsActive == "Y");
                if (adminuser == null)
                {
                    userInfo.StatusMessage = langid == 1 ? "Bad Request or Invalid admin username/password" : "Bad Request or Invalid admin username/password";
                    userInfo.PopUpId = "2";
                    return userInfo;
                }
                if (!adminuserIDs.Contains(adminuser.ItemId))
                {
                    userInfo.StatusMessage = langid == 1 ? "Bad Request or Unauthorised user" : "Bad Request or Unauthorised user";
                    userInfo.PopUpId = "2";
                    return userInfo;
                }

                string decryptval = PasswordHashingAdm.DecryptPassword(adminuser.Password);
                if (!password.Equals(decryptval))
                {
                    userInfo.StatusMessage = langid == 1 ? "Invalid username/password." : "Invalid username/password."; //"2";
                    userInfo.PopUpId = "2";
                    return userInfo;
                }
                if (password.Equals(decryptval))
                {
                    userInfo.PopUpId = "-2";
                    userInfo.SIBFMemberId = memberentity.MemberId;
                    userInfo.AdminUserId = adminuser.ItemId;
                    return userInfo;
                }
                return userInfo;
            }
        }
        public AdminLoggedinUserInfo AuthenticateAdminLoginForMemberUsingOTP(string username, string adminusername, string otp, long langid)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                AdminLoggedinUserInfo userInfo = new AdminLoggedinUserInfo();
                List<long> adminuserIDs = new List<long>(){
                   1,8,10086,10088,10089,10090,10091,10115,10111
                };
                userInfo.IsValidLoggedInMember = false;
                var memberentity = _context.XsiExhibitionMember.SingleOrDefault(x => x.UserName == username && x.Status == "A");
                // return null if user not found
                if (memberentity == null)
                {
                    userInfo.StatusMessage = langid == 1 ? "Bad Request or Invalid member username/password" : "Bad Request or Invalid username/password";
                    userInfo.PopUpId = "2";
                    return userInfo;
                }

                var adminuser = _context.XsiAdminUsers.SingleOrDefault(x => x.UserName == adminusername && x.IsActive == "Y");
                if (adminuser == null)
                {
                    userInfo.StatusMessage = langid == 1 ? "Bad Request or Invalid admin username/password" : "Bad Request or Invalid admin username/password";
                    userInfo.PopUpId = "2";
                    return userInfo;
                }
                if (!adminuserIDs.Contains(adminuser.ItemId))
                {
                    userInfo.StatusMessage = langid == 1 ? "Bad Request or Unauthorised user" : "Bad Request or Unauthorised user";
                    userInfo.PopUpId = "2";
                    return userInfo;
                }

                #region OTP Validations
                if (memberentity.AdminEmailOtp == null || string.IsNullOrEmpty(memberentity.AdminEmailOtp))
                {
                    userInfo.StatusMessage = langid == 1 ? "Invalid username/password/otp" : "Invalid username/password/otp";
                    userInfo.PopUpId = "2";
                    return userInfo;
                }

                if (memberentity.AdminEmailOtpexpiresAt == null || MethodFactory.ArabianTimeNow() > memberentity.AdminEmailOtpexpiresAt)
                {
                    userInfo.StatusMessage = langid == 1 ? "OTP Expired. Please try again" : "OTP Expired. Please try again";
                    userInfo.PopUpId = "2";
                    return userInfo;
                }

                if (!memberentity.AdminEmailOtp.Equals(otp))
                {
                    userInfo.StatusMessage = langid == 1 ? "Invalid otp" : "Invalid otp";
                    userInfo.PopUpId = "2";
                    return userInfo;
                }
                #endregion
                if (memberentity.AdminEmailOtp.Equals(otp))
                {
                    userInfo.IsValidLoggedInMember = true;
                    userInfo.SIBFMemberId = memberentity.MemberId;
                    userInfo.AdminUserId = adminuser.ItemId;
                    userInfo.SIBFMemberPublicName = memberentity.UserName;
                    userInfo.IsLibraryMember = string.Equals(memberentity.IsLibraryMember, EnumConversion.ToString(EnumBool.Yes), StringComparison.OrdinalIgnoreCase);
                    //(entity.IsLibraryMember == EnumConversion.ToString(EnumBool.Yes)) ? true : false;
                    userInfo.IsNewPatternChanged = string.IsNullOrEmpty(memberentity.IsNewPasswordChange) ? "N" : memberentity.IsNewPasswordChange;
                    userInfo.StatusMessage = "/en/MemberDashboard";
                    userInfo.PopUpId = "3";
                    LogUser(memberentity.MemberId, adminuser.ItemId);
                    userInfo.Token = JWTTokenGenerate(userInfo);
                    int expiryInMinutes = Convert.ToInt32(_configuration["Jwt:ExpiryInMinutes"]);
                    userInfo.expiration = DateTime.UtcNow.AddMinutes(expiryInMinutes);
                    return userInfo;
                }

                return userInfo;
            }
        }

        void LogUser(long memberId, long? adminuserid = null)
        {
            using (sibfnewdbContext _context = new sibfnewdbContext())
            {
                XsiExhibitionMemberLog memberLog = new XsiExhibitionMemberLog();
                memberLog.ExhibitionMemberId = memberId;
                if (adminuserid > 0)
                    memberLog.AdminUserId = adminuserid;
                memberLog.CreatedOn = MethodFactory.ArabianTimeNow();
                _context.XsiExhibitionMemberLog.Add(memberLog);
                _context.SaveChanges();
            }
        }
        string JWTTokenGenerate(AdminLoggedinUserInfo userInfo)
        {
            var claim = new[]
              {
                new Claim(JwtRegisteredClaimNames.FamilyName, userInfo.SIBFMemberPublicName),
                new Claim("ID", Convert.ToString(userInfo.SIBFMemberId))
                };

            var signinKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_configuration["Jwt:SigningKey"]));

            int expiryInMinutes = Convert.ToInt32(_configuration["Jwt:ExpiryInMinutes"]);

            var token = new JwtSecurityToken(
                claims: claim,
                issuer: _configuration["Jwt:Site"],
                audience: _configuration["Jwt:Site"],
                expires: DateTime.UtcNow.AddMinutes(expiryInMinutes),
                signingCredentials: new SigningCredentials(signinKey, SecurityAlgorithms.HmacSha256)
            );
            userInfo.expiration = token.ValidTo;
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
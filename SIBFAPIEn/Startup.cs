﻿using Entities.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NLog;
using SIBFAPIEn.DTO;
using SIBFAPIEn.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace SIBFAPIEn
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddDbContext<sibfnewdbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.ConfigureCors();
            services.ConfigureLoggerService();
            services.AddOptions();
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
            services.Configure<SCRFEmailSettings>(Configuration.GetSection("SCRFEmailSettings"));
            services.AddTransient<IEmailSender, SmtpEmail>();


            services.Configure<AppCustomSettings>(Configuration.GetSection("AppCustomSettings"));

            services.Configure<FormOptions>(o =>
            {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = int.MaxValue;
            });
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowOrigin"));
            });

            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture("en-US");
            });


            services.AddAuthentication(option =>
            {
                option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                option.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = true;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = Configuration["Jwt:Site"],
                    ValidIssuer = Configuration["Jwt:Site"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:SigningKey"]))
                };
            });

            /*
               services.AddAuthentication(option =>
              {
                  option.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                  //option.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                  //option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                  //option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
              })
              .AddCookie(x =>
              {
                  x.Cookie.Name = "token";
                  x.Cookie.HttpOnly = true;
                  x.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                  x.Cookie.IsEssential = true;
                  x.Cookie.Expiration = TimeSpan.FromDays(1);
              })
              .AddJwtBearer(options =>
              {
                  options.RequireHttpsMetadata = true;
                  options.SaveToken = true;
                  options.TokenValidationParameters = new TokenValidationParameters()
                  {
                      ValidateIssuer = true,
                      ValidateAudience = true,
                      ValidateLifetime = true,
                      ValidateIssuerSigningKey = true,

                      ValidIssuer = Configuration["Jwt:Site"],
                      ValidAudience = Configuration["Jwt:Site"],
                      IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:SigningKey"]))
                  };

                  options.Events = new JwtBearerEvents
                  {
                      OnMessageReceived = context =>
                      {
                          context.Request.Cookies.TryGetValue("token", out var accessToken);
                          if (!string.IsNullOrEmpty(accessToken))
                              context.Token = accessToken;

                          return Task.CompletedTask;
                      }
                  };
              });
             */

            services.AddLocalization(options => options.ResourcesPath = "BackendError");

            services.AddMemoryCache();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var appSettingsSection = Configuration.GetSection("AppCustomSettings");

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAdminMemberUserServiceService, AdminMemberUserService>();

            // services.AddSingleton<IEmailSender, SmtpEmail>();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen();

            //});

            services.AddApiVersioning(o => o.ApiVersionReader = new HeaderApiVersionReader("api-version"));
            services.AddResponseCaching();

            //var connection = @"Server=DESKTOP-2Q8KOUQ\\SQLEXPRESS01;Database=sibfnewdb;User Id=sibf;Password=sibf123#;ConnectRetryCount=0";
            //services.AddDbContext<sibfnewdbContext>(options => options.UseSqlServer(connection));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            string[] strtest = { "http://testui.sibf.com", "https://testui.sibf.com", "http://localhost:5000", "https://localhost:5000" };
            string[] strlive = { "http://localhost:3000", "https://localhost:3000", "http://www.sibf.com", "https://www.sibf.com", "http://sibf.com", "https://sibf.com", "https://sibf-ala.com", "http://sibf-ala.com", "https://map.scrf.ae", "http://map.scrf.ae", "https://map.sibf.com", "http://map.sibf.com" };
            string[] strall = { "http://10.1.1.173:1002", "http://localhost:11242", "http://www.sibf.com", "https://www.sibf.com", "http://sibf.com", "https://sibf.com", "http://www.scrf.ae", "https://www.scrf.ae", "http://scrf.ae", "https://scrf.ae", "http://testui.sibf.com", "https://testui.sibf.com", "http://demoui.sibf.com", "https://demoui.sibf.com", "https://localhost:8080", "http://localhost:8080", "http://localhost:3000", "https://localhost:3000", "http://localhost:4000", "https://localhost:4000", "http://localhost:4001", "https://localhost:4001", "http://localhost:57794", "http://localhost:5000", "https://localhost:5000", "https://localhost:1984", "http://localhost:1984", "https://sibf-ala.com", "http://sibf-ala.com", "https://map.scrf.ae", "http://map.scrf.ae", "https://map.sibf.com", "http://map.sibf.com", "https://xsi.sibf.com", "http://sba-bffinap-01.sharjahba.ae", "https://paygate.sibf.com", "http://paygate.sibf.com", "https://demo.scrf.ae", "http://demo.scrf.ae" };

            app.UseSwagger();
            app.UseCors("AllowOrigin");
            app.UseCors(builder => builder
                 .WithOrigins(strall)
            .AllowAnyMethod()
             .AllowAnyHeader()
             .AllowCredentials());

            app.UseHttpsRedirection();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "SIBF API V1");
                c.RoutePrefix = String.Empty;
            });

            var supportedCultures = new List<CultureInfo>
            {
                new CultureInfo("en-US"),
                new CultureInfo("ar-AE")
            };

            var options = new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("en-US"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures,
            };

            app.UseRequestLocalization(options);

            app.UseRequestLocalization();
            app.UseResponseCaching();

            app.UseAuthentication();
            app.UseMvc();
        }
    }
}

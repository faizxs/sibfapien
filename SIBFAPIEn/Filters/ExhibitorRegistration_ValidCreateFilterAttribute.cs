﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Org.BouncyCastle.Asn1.Ocsp;
using SIBFAPIEn.DTO;
using System.Buffers;
using System.Net;

namespace SIBFAPIEn.Filters
{
    public class ExhibitorRegistration_ValidCreateFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            long langId = 1;
            long.TryParse(context.HttpContext.Request.Headers["LanguageURL"], out langId);

            SaveDataForStepTwoAndThreeDTO dto = context.ActionArguments["dto"] as SaveDataForStepTwoAndThreeDTO;

            if (dto == null)
            {
                if (langId == 1)
                {
                    context.ModelState.AddModelError("dto", "Exhibitor Registration object is null");
                }
                else
                {
                    context.ModelState.AddModelError("dto", "Exhibitor Registration Ar object is null");
                }

                var problemDetails = new ValidationProblemDetails(context.ModelState)
                {
                    //Title = "Invalid Request",
                    //Detail = "Exhibitor Registration object is null",
                    Status = StatusCodes.Status400BadRequest
                };
                context.Result = new BadRequestObjectResult(problemDetails);
            }
            else
            {

            }
        }
    }
}

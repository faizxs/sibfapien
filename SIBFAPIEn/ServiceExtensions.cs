﻿using Contracts;
using LoggerService;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System.Net;

namespace SIBFAPIEn
{
    public static class ServiceExtensions
    {

        public static void ConfigureCors(this IServiceCollection services)
        {
            string[] strtest = { "http://testui.sibf.com", "https://testui.sibf.com", "http://localhost:5000", "https://localhost:5000" };
            string[] strlive = { "http://localhost:3000", "https://localhost:3000", "http://www.sibf.com", "https://www.sibf.com", "http://sibf.com", "https://sibf.com", "https://sibf-ala.com", "http://sibf-ala.com", "https://map.scrf.ae", "http://map.scrf.ae", "https://map.sibf.com", "http://map.sibf.com" };
            string[] strall = { "http://10.1.1.173:1002", "http://localhost:11242", "http://www.sibf.com", "https://www.sibf.com", "http://sibf.com", "https://sibf.com", "http://www.scrf.ae", "https://www.scrf.ae", "http://scrf.ae", "https://scrf.ae", "http://testui.sibf.com", "https://testui.sibf.com","http://demoui.sibf.com", "https://demoui.sibf.com", "https://localhost:8080", "http://localhost:8080", "http://localhost:3000", "https://localhost:3000", "http://localhost:4000", "https://localhost:4000", "http://localhost:4001", "https://localhost:4001", "http://localhost:57794", "http://localhost:5000", "https://localhost:5000", "https://localhost:1984", "http://localhost:1984", "https://sibf-ala.com", "http://sibf-ala.com", "https://map.scrf.ae", "http://map.scrf.ae", "https://map.sibf.com", "http://map.sibf.com", "https://xsi.sibf.com", "http://sba-bffinap-01.sharjahba.ae", "https://paygate.sibf.com", "http://paygate.sibf.com", "https://demo.scrf.ae", "http://demo.scrf.ae" };

            services.AddCors(options =>
            {
                options.AddPolicy("AllowOrigin",
                    builder => builder
                    .WithOrigins(strall)
            .AllowAnyHeader()
                        .SetIsOriginAllowedToAllowWildcardSubdomains()
                     .AllowAnyMethod()
                     .AllowCredentials());
            });
        }
        public static void AddSwaggerGen(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "SIBF API", Version = "v1" });
            });
        }
        public static void ConfigureLoggerService(this IServiceCollection services)
        {
            services.AddSingleton<ILoggerManager, LoggerManager>();
        }
        //    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        //WebHost.CreateDefaultBuilder(args)
        //    .UseKestrel(options =>
        //    {
        //        options.Listen(IPAddress.Loopback, 5000);
        //        options.Listen(IPAddress.Loopback, 5001, listenOptions =>
        //        {
        //            listenOptions.UseHttps("certificate.pfx", "topsecret");
        //        });
        //        options.Listen(IPAddress.Loopback, 44333);
        //        options.Listen(IPAddress.Loopback, 44333, listenOptions =>
        //        {
        //            listenOptions.UseHttps("certificate.pfx", "topsecret");
        //        });

        //    }).UseStartup<Startup>();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;

namespace Xsi.Repositories
{
    public static class CachingRepository
    {
        private static IMemoryCache _cache;
        public static string ExhibitionKey { get { return "_Exhibition"; } }
        public static string LanguageKey { get { return "_Language"; } }
        public static string ExhibitionCountryKey { get { return "_ExhibitionCountry"; } }

        public static List<XsiExhibition> CachedExhibition { get; set; }
        public static List<XsiLanguages> CachedLanguage { get; set; }
        public static List<XsiExhibitionCountry> CachedExhibitionCountry { get; set; }
        //public  List<XsiExhibition> CachedExhibition
        //{
        //    get
        //    {
        //        if (_cache.Get<dynamic>(ExhibitionKey) != null)
        //            return (List<XsiExhibition>)_cache.Get<dynamic>(ExhibitionKey);
        //        return null;
        //    }
        //}
        //public  List<XsiLanguages> CachedLanguage
        //{
        //    get
        //    {
        //        if (_cache.Get<dynamic>(LanguageKey) != null)
        //            return (List<XsiLanguages>)_cache.Get<dynamic>(LanguageKey);
        //        return null;
        //    }
        //}
        //public  List<XsiExhibitionCountry> CachedExhibitionCountry
        //{
        //    get
        //    {
        //        if (_cache.Get<dynamic>(ExhibitionCountryKey) != null)
        //            return (List<XsiExhibitionCountry>)_cache.Get<dynamic>(ExhibitionCountryKey);
        //        return null;
        //    }
        //}

        public static List<XsiExhibition> GetExhibition(IMemoryCache _cache)
        {
            if (CachedExhibition == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    CachedExhibition = _cache.GetOrCreate(ExhibitionKey, entry =>
                 {
                     entry.SlidingExpiration = TimeSpan.FromSeconds(3600);
                     return _context.XsiExhibition.ToList();
                 });
                }
            }
            return CachedExhibition;
        }
        public static List<XsiLanguages> GetLanguages(IMemoryCache _cache)
        {
            if (CachedLanguage == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    CachedLanguage = _cache.GetOrCreate(LanguageKey, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(3600);
                        return _context.XsiLanguages.ToList();
                    });
                }
            }

            return CachedLanguage;
        }
        public static List<XsiExhibitionCountry> GetExhibitionCountry(IMemoryCache _cache)
        {
            if (CachedExhibitionCountry == null)
            {
                using (sibfnewdbContext _context = new sibfnewdbContext())
                {
                    CachedExhibitionCountry = _cache.GetOrCreate(ExhibitionCountryKey, entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromSeconds(3600);
                        return _context.XsiExhibitionCountry.ToList();
                    });
                }
            }
            return CachedExhibitionCountry;
        }

        /*#region Properties
        public static IList<XsiExhibitionBooks> CachedBooks
        {
            get
            {
                if (HttpContext.Session["CachedBooks"] != null)
                    return (IList<XsiExhibitionBooks>)HttpContext.Current.Cache["CachedBooks"];
                return null;
            }
            set { HttpContext.Current.Cache["CachedBooks"] = value; }
        }
        public static IList<XsiExhibitionBookParticipating> CachedBooksParticipating
        {
            get
            {
                if (HttpContext.Current.Cache["CachedBooksParticipating"] != null)
                    return (IList<XsiExhibitionBookParticipating>)HttpContext.Current.Cache["CachedBooksParticipating"];
                return null;
            }
            set { HttpContext.Current.Cache["CachedBooksParticipating"] = value; }
        }
        public static IList<XsiExhibitionAuthor> CachedAuthors
        {
            get
            {
                if (HttpContext.Current.Cache["CachedAuthors"] != null)
                    return (IList<XsiExhibitionAuthor>)HttpContext.Current.Cache["CachedAuthors"];
                return null;
            }
            set { HttpContext.Current.Cache["CachedAuthors"] = value; }
        }
        public static IList<XsiExhibitionBookPublisher> CachedPublishers
        {
            get
            {
                if (HttpContext.Current.Cache["CachedPublishers"] != null)
                    return (IList<XsiExhibitionBookPublisher>)HttpContext.Current.Cache["CachedPublishers"];
                return null;
            }
            set { HttpContext.Current.Cache["CachedPublishers"] = value; }
        }
        public static IList<XsiPageMenuNew> CachedMenu
        {
            get
            {
                if (HttpContext.Current.Cache["CachedMenu"] != null)
                    return (IList<XsiPageMenuNew>)HttpContext.Current.Cache["CachedMenu"];
                return null;
            }
            set { HttpContext.Current.Cache["CachedMenu"] = value; }
        }
        public static IList<XsiPageMenuNew> CachedMenuNew
        {
            get
            {
                if (HttpContext.Current.Cache["CachedMenuNew"] != null)
                    return (IList<XsiPageMenuNew>)HttpContext.Current.Cache["CachedMenuNew"];
                return null;
            }
            set { HttpContext.Current.Cache["CachedMenuNew"] = value; }
        }
        #endregion*/

        #region Properties
        public static IList<XsiExhibitionBooks> CachedBooks
        {
            get; set;
        }
        public static IList<XsiExhibitionBookParticipating> CachedBooksParticipating
        {
            get; set;
        }
        public static IList<XsiExhibitionAuthor> CachedAuthors
        {
            get; set;
        }
        public static IList<XsiExhibitionBookPublisher> CachedPublishers
        {
            get; set;
        }
        public static IList<XsiPageMenuNew> CachedMenu
        {
            get; set;
        }
        public static IList<XsiPageMenuNew> CachedMenuNew
        {
            get; set;
        }
        #endregion
    }
}

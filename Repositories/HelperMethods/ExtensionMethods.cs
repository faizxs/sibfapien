﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xsi.Repositories
{
    /// <summary>
    /// To include functions for validation on basic data types i.e. if value is default or null
    /// </summary>    
    public static class ObjectExtensions
    {
        /// <summary>
        /// Checks to see if the object has null default value for basic types
        /// </summary>
        /// <typeparam name="T">Type of object being passed</typeparam>
        /// <param name="value">Object whose value needs to be checked</param>
        /// <returns>true if the value is null default. Otherwise returns false</returns>
        public static bool IsDefault<T>(this T value)
        {
            return (Equals(value, default(T)));
        }
    }
}

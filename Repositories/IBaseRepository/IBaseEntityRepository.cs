﻿using System.Collections.Generic;
using Entities.Models;
using System;
using System.Linq;

namespace Xsi.Repositories
{
    public interface IBaseEntityRepository<TEntity> : IDisposable
    {
        TEntity GetById(long id);
        List<TEntity> Select();
        List<TEntity> Select(TEntity entity);
        TEntity Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Delete(long id);
        int SubmitChanges();
        void Dispose();
    }
}

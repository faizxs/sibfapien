﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFAwardsNominationFormRepository : ISCRFAwardNominationFormRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFAwardsNominationFormRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFAwardsNominationFormRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFAwardsNominationFormRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfawardNominationForm GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfawardNominationForm.Find(itemId);
        }

        public List<XsiScrfawardNominationForm> Select()
        {
            return XsiContext.Context.XsiScrfawardNominationForm.ToList();
        }
        public List<XsiScrfawardNominationForm> Select(XsiScrfawardNominationForm entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfawardNominationForm>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.AwardId.IsDefault())
                predicate = predicate.And(p => p.AwardId == entity.AwardId);

            if (!entity.SubAwardId.IsDefault())
                predicate = predicate.And(p => p.SubAwardId == entity.SubAwardId);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.CityId.IsDefault())
                predicate = predicate.And(p => p.CityId == entity.CityId);

            if (!entity.OtherCity.IsDefault())
                predicate = predicate.And(p => p.OtherCity.Contains(entity.OtherCity));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IllustratorName.IsDefault())
                predicate = predicate.And(p => p.IllustratorName.Contains(entity.IllustratorName));

            if (!entity.Isbn.IsDefault())
                predicate = predicate.And(p => p.Isbn.Contains(entity.Isbn));

            if (!entity.Publisher.IsDefault())
                predicate = predicate.And(p => p.Publisher.Contains(entity.Publisher));

            if (!entity.AuthorName.IsDefault())
                predicate = predicate.And(p => p.AuthorName.Contains(entity.AuthorName));

            if (!entity.Publisher.IsDefault())
                predicate = predicate.And(p => p.Publisher.Contains(entity.Publisher));

            if (!entity.AgeGroupCategory.IsDefault())
                predicate = predicate.And(p => p.AgeGroupCategory == entity.AgeGroupCategory);

            if (!entity.AgeGroupCategory2.IsDefault())
                predicate = predicate.And(p => p.AgeGroupCategory2 == entity.AgeGroupCategory2);

            if (!entity.CopyrightHolder.IsDefault())
                predicate = predicate.And(p => p.CopyrightHolder.Contains(entity.CopyrightHolder));

            if (!entity.IllustratorOriginal.IsDefault())
                predicate = predicate.And(p => p.IllustratorOriginal.Contains(entity.IllustratorOriginal));

            if (!entity.AuthorOriginal.IsDefault())
                predicate = predicate.And(p => p.AuthorOriginal.Contains(entity.AuthorOriginal));

            if (!entity.Pobox.IsDefault())
                predicate = predicate.And(p => p.Pobox.Contains(entity.Pobox));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Contains(entity.Email));

            if (!entity.Phone.IsDefault())
                predicate = predicate.And(p => p.Phone.Contains(entity.Phone));

            if (!entity.IllustratorTactile.IsDefault())
                predicate = predicate.And(p => p.IllustratorTactile.Contains(entity.IllustratorTactile));

            if (!entity.AuthorTactile.IsDefault())
                predicate = predicate.And(p => p.AuthorTactile.Contains(entity.AuthorTactile));

            if (!entity.Name.IsDefault())
                predicate = predicate.And(p => p.Name.Contains(entity.Name));

            if (!entity.Address.IsDefault())
                predicate = predicate.And(p => p.Address.Contains(entity.Address));

            if (!entity.PublishingYear.IsDefault())
                predicate = predicate.And(p => p.PublishingYear.Contains(entity.PublishingYear));

            if (!entity.Comments.IsDefault())
                predicate = predicate.And(p => p.Comments.Contains(entity.Comments));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Contains(entity.IsActive));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Contains(entity.FileName));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (!entity.Date.IsDefault())
                predicate = predicate.And(p => p.Date == entity.Date);

            return XsiContext.Context.XsiScrfawardNominationForm.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfawardNominationForm Add(XsiScrfawardNominationForm entity)
        {
            XsiScrfawardNominationForm SCRFAwardsNominationForm = new XsiScrfawardNominationForm();
            SCRFAwardsNominationForm.LanguageId = entity.LanguageId;
            SCRFAwardsNominationForm.Status = entity.Status;
            SCRFAwardsNominationForm.ExhibitionId = entity.ExhibitionId;
            SCRFAwardsNominationForm.AwardId = entity.AwardId;
            SCRFAwardsNominationForm.SubAwardId = entity.SubAwardId;
            SCRFAwardsNominationForm.CountryId = entity.CountryId;
            SCRFAwardsNominationForm.CityId = entity.CityId;
            if (!entity.OtherCity.IsDefault())
                SCRFAwardsNominationForm.OtherCity = entity.OtherCity.Trim();
            if (!entity.Email.IsDefault())
                SCRFAwardsNominationForm.Email = entity.Email.Trim();
            if (!entity.Title.IsDefault())
                SCRFAwardsNominationForm.Title = entity.Title.Trim();
            if (!entity.PublishingYear.IsDefault())
                SCRFAwardsNominationForm.PublishingYear = entity.PublishingYear.Trim();
            if (!entity.Isbn.IsDefault())
                SCRFAwardsNominationForm.Isbn = entity.Isbn.Trim();
            if (!entity.Publisher.IsDefault())
                SCRFAwardsNominationForm.Publisher = entity.Publisher.Trim();
            if (!entity.AuthorName.IsDefault())
                SCRFAwardsNominationForm.AuthorName = entity.AuthorName.Trim();
            if (!entity.IllustratorName.IsDefault())
                SCRFAwardsNominationForm.IllustratorName = entity.IllustratorName.Trim();
            if (!entity.AgeGroupCategory.IsDefault())
                SCRFAwardsNominationForm.AgeGroupCategory = entity.AgeGroupCategory.Trim();
            if (!entity.AgeGroupCategory2.IsDefault())
                SCRFAwardsNominationForm.AgeGroupCategory2 = entity.AgeGroupCategory2.Trim();
            if (!entity.CopyrightHolder.IsDefault())
                SCRFAwardsNominationForm.CopyrightHolder = entity.CopyrightHolder.Trim();
            if (!entity.IllustratorOriginal.IsDefault())
                SCRFAwardsNominationForm.IllustratorOriginal = entity.IllustratorOriginal.Trim();
            if (!entity.Pobox.IsDefault())
                SCRFAwardsNominationForm.Pobox = entity.Pobox.Trim();
            if (!entity.Phone.IsDefault())
                SCRFAwardsNominationForm.Phone = entity.Phone.Trim();
            if (!entity.AuthorOriginal.IsDefault())
                SCRFAwardsNominationForm.AuthorOriginal = entity.AuthorOriginal.Trim();
            if (!entity.IllustratorTactile.IsDefault())
                SCRFAwardsNominationForm.IllustratorTactile = entity.IllustratorTactile.Trim();
            if (!entity.AuthorTactile.IsDefault())
                SCRFAwardsNominationForm.AuthorTactile = entity.AuthorTactile.Trim();
            if (!entity.Name.IsDefault())
                SCRFAwardsNominationForm.Name = entity.Name.Trim();
            if (!entity.Address.IsDefault())
                SCRFAwardsNominationForm.Address = entity.Address.Trim();
            if (!entity.Phone.IsDefault())
            SCRFAwardsNominationForm.Phone = entity.Phone.Trim();
            if (!entity.Comments.IsDefault())
                SCRFAwardsNominationForm.Comments = entity.Comments.Trim();
            SCRFAwardsNominationForm.IsActive = entity.IsActive;
            SCRFAwardsNominationForm.FileName = entity.FileName;
            SCRFAwardsNominationForm.CreatedOn = entity.CreatedOn;
            SCRFAwardsNominationForm.CreatedBy = entity.CreatedBy;
            SCRFAwardsNominationForm.ModifiedOn = entity.ModifiedOn;
            SCRFAwardsNominationForm.ModifiedBy = entity.ModifiedBy;
            SCRFAwardsNominationForm.Date = entity.Date;

            XsiContext.Context.XsiScrfawardNominationForm.Add(SCRFAwardsNominationForm);
            SubmitChanges();
            return SCRFAwardsNominationForm;

        }
        public void Update(XsiScrfawardNominationForm entity)
        {
            XsiScrfawardNominationForm SCRFAwardsNominationForm = XsiContext.Context.XsiScrfawardNominationForm.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFAwardsNominationForm).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                SCRFAwardsNominationForm.Title = entity.Title.Trim();

            if (!entity.Status.IsDefault())
                SCRFAwardsNominationForm.Status = entity.Status;

            if (!entity.ExhibitionId.IsDefault())
                SCRFAwardsNominationForm.ExhibitionId = entity.ExhibitionId;

            if (!entity.AwardId.IsDefault())
                SCRFAwardsNominationForm.AwardId = entity.AwardId;

            if (!entity.SubAwardId.IsDefault())
                SCRFAwardsNominationForm.SubAwardId = entity.SubAwardId;

            if (!entity.CountryId.IsDefault())
                SCRFAwardsNominationForm.CountryId = entity.CountryId;

            if (!entity.CityId.IsDefault())
                SCRFAwardsNominationForm.CityId = entity.CityId;

            if (!entity.OtherCity.IsDefault())
                SCRFAwardsNominationForm.OtherCity = entity.OtherCity.Trim();

            if (!entity.PublishingYear.IsDefault())
                SCRFAwardsNominationForm.PublishingYear = entity.PublishingYear.Trim();

            if (!entity.Isbn.IsDefault())
                SCRFAwardsNominationForm.Isbn = entity.Isbn.Trim();

            if (!entity.IllustratorName.IsDefault())
                SCRFAwardsNominationForm.IllustratorName = entity.IllustratorName.Trim();

            if (!entity.AuthorName.IsDefault())
                SCRFAwardsNominationForm.AuthorName = entity.AuthorName.Trim();
            if (!entity.Publisher.IsDefault())
                SCRFAwardsNominationForm.Publisher = entity.Publisher.Trim();
            if (!entity.AgeGroupCategory.IsDefault())
                SCRFAwardsNominationForm.AgeGroupCategory = entity.AgeGroupCategory.Trim();
            if (!entity.AgeGroupCategory2.IsDefault())
                SCRFAwardsNominationForm.AgeGroupCategory2 = entity.AgeGroupCategory2.Trim();
            if (!entity.CopyrightHolder.IsDefault())
                SCRFAwardsNominationForm.CopyrightHolder = entity.CopyrightHolder.Trim();
            if (!entity.IllustratorOriginal.IsDefault())
                SCRFAwardsNominationForm.IllustratorOriginal = entity.IllustratorOriginal.Trim();
            if (!entity.IllustratorTactile.IsDefault())
                SCRFAwardsNominationForm.IllustratorTactile = entity.IllustratorTactile.Trim();
            if (!entity.Pobox.IsDefault())
                SCRFAwardsNominationForm.Pobox = entity.Pobox.Trim();
            if (!entity.Phone.IsDefault())
                SCRFAwardsNominationForm.Phone = entity.Phone.Trim();
            if (!entity.AuthorOriginal.IsDefault())
                SCRFAwardsNominationForm.AuthorOriginal = entity.AuthorOriginal.Trim();
            if (!entity.AuthorTactile.IsDefault())
                SCRFAwardsNominationForm.AuthorTactile = entity.AuthorTactile.Trim();
            if (!entity.Name.IsDefault())
                SCRFAwardsNominationForm.Name = entity.Name.Trim();
            if (!entity.Address.IsDefault())
                SCRFAwardsNominationForm.Address = entity.Address.Trim();

            if (!entity.Comments.IsDefault())
                SCRFAwardsNominationForm.Comments = entity.Comments.Trim();

            if (!entity.IsActive.IsDefault())
                SCRFAwardsNominationForm.IsActive = entity.IsActive;
            if (!entity.ModifiedOn.IsDefault())
                SCRFAwardsNominationForm.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                SCRFAwardsNominationForm.ModifiedBy = entity.ModifiedBy;

            if (!entity.Email.IsDefault())
                SCRFAwardsNominationForm.Email = entity.Email.Trim();

            if (!entity.FileName.IsDefault())
                SCRFAwardsNominationForm.FileName = entity.FileName;

            if (!entity.Date.IsDefault())
                SCRFAwardsNominationForm.Date = entity.Date;
        }
        public void Delete(XsiScrfawardNominationForm entity)
        {
            foreach (XsiScrfawardNominationForm SCRFAwardsNominationForm in Select(entity))
            {
                XsiScrfawardNominationForm aSCRFAwardsNominationForm = XsiContext.Context.XsiScrfawardNominationForm.Find(SCRFAwardsNominationForm.ItemId);
                XsiContext.Context.XsiScrfawardNominationForm.Remove(aSCRFAwardsNominationForm);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfawardNominationForm aSCRFAwardsNominationForm = XsiContext.Context.XsiScrfawardNominationForm.Find(itemId);
            XsiContext.Context.XsiScrfawardNominationForm.Remove(aSCRFAwardsNominationForm);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFCountryRepository : ISCRFCountryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFCountryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFCountryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFCountryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfcountry GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfcountry.Find(itemId);
        }

        public List<XsiScrfcountry> Select()
        {
            return XsiContext.Context.XsiScrfcountry.ToList();
        }
        public List<XsiScrfcountry> SelectOr(XsiScrfcountry entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfcountry>();
            var Outer = PredicateBuilder.True<XsiScrfcountry>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

           /* if (!entity.LanguageId.IsDefault())
                Outer = Outer.And(p => p.LanguageId == entity.LanguageId);*/

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiScrfcountry.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiScrfcountry> Select(XsiScrfcountry entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfcountry>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            /*if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.GroupId.IsDefault())
                predicate = predicate.And(p => p.GroupId == entity.GroupId);*/

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiScrfcountry.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiScrfcountry> SelectOtherLanguageCategory(XsiScrfcountry entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfcountry>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            //var GroupId = XsiContext.Context.XsiScrfcountry.AsExpandable().Where(predicate).Single().GroupId.Value;
            predicate = PredicateBuilder.True<XsiScrfcountry>();
           // predicate = predicate.And(p => p.GroupId == GroupId);
           // predicate = predicate.And(p => p.LanguageId != entity.LanguageId);
            return XsiContext.Context.XsiScrfcountry.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiScrfcountry> SelectCurrentLanguageCategory(XsiScrfcountry entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfcountry>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

           // var GroupId = XsiContext.Context.XsiScrfcountry.AsExpandable().Where(predicate).Single().GroupId.Value;
            predicate = PredicateBuilder.True<XsiScrfcountry>();
           // predicate = predicate.And(p => p.GroupId == GroupId);
           // predicate = predicate.And(p => p.LanguageId == entity.LanguageId);
            return XsiContext.Context.XsiScrfcountry.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiScrfcountry> SelectComplete(XsiScrfcountry entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfcountry>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            /*if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.GroupId.IsDefault())
                predicate = predicate.And(p => p.GroupId == entity.GroupId);*/

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiScrfcountry.AsExpandable().Where(predicate).ToList();
            //return XsiContext.Context.XsiScrfcountry.Include(p => p.XsiScrfcity).AsExpandable().Where(predicate).ToList();
        }
        public XsiScrfcountry Add(XsiScrfcountry entity)
        {
            XsiScrfcountry SCRFCountry = new XsiScrfcountry();
           // SCRFCountry.GroupId = entity.GroupId;
           // SCRFCountry.LanguageId = entity.LanguageId;
            SCRFCountry.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
            SCRFCountry.Title = entity.Title.Trim();
            SCRFCountry.CreatedOn = entity.CreatedOn;
            SCRFCountry.CreatedBy = entity.CreatedBy;
            SCRFCountry.ModifiedOn = entity.ModifiedOn;
            SCRFCountry.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfcountry.Add(SCRFCountry);
            SubmitChanges();
            return SCRFCountry;

        }
        public void Update(XsiScrfcountry entity)
        {
            XsiScrfcountry SCRFCountry = XsiContext.Context.XsiScrfcountry.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFCountry).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                SCRFCountry.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                SCRFCountry.Title = entity.Title.Trim();

            if (!entity.ModifiedOn.IsDefault())
                SCRFCountry.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                SCRFCountry.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiScrfcountry entity)
        {
            foreach (XsiScrfcountry SCRFCountry in Select(entity))
            {
                XsiScrfcountry aSCRFCountry = XsiContext.Context.XsiScrfcountry.Find(SCRFCountry.ItemId);
                XsiContext.Context.XsiScrfcountry.Remove(aSCRFCountry);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfcountry aSCRFCountry = XsiContext.Context.XsiScrfcountry.Find(itemId);
            XsiContext.Context.XsiScrfcountry.Remove(aSCRFCountry);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
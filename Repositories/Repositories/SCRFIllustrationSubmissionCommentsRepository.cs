﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFIllustrationSubmissionCommentRepository : ISCRFIllustrationSubmissionCommentsRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFIllustrationSubmissionCommentRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFIllustrationSubmissionCommentRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFIllustrationSubmissionCommentRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfillustrationSubmissionComments GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfillustrationSubmissionComments.Find(itemId);
        }

        public List<XsiScrfillustrationSubmissionComments> Select()
        {
            return XsiContext.Context.XsiScrfillustrationSubmissionComments.ToList();
        }
        public List<XsiScrfillustrationSubmissionComments> Select(XsiScrfillustrationSubmissionComments entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfillustrationSubmissionComments>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IllustrationSubmissionId.IsDefault())
                predicate = predicate.And(p => p.IllustrationSubmissionId == entity.IllustrationSubmissionId);

            if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Name.IsDefault())
                predicate = predicate.And(p => p.Name.Contains(entity.Name));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Contains(entity.Email));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);


            return XsiContext.Context.XsiScrfillustrationSubmissionComments.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfillustrationSubmissionComments Add(XsiScrfillustrationSubmissionComments entity)
        {
            XsiScrfillustrationSubmissionComments SCRFIllustrationSubmissionComment = new XsiScrfillustrationSubmissionComments();

            SCRFIllustrationSubmissionComment.IllustrationSubmissionId = entity.IllustrationSubmissionId;
            SCRFIllustrationSubmissionComment.LanguageId = entity.LanguageId;
            SCRFIllustrationSubmissionComment.IsActive = entity.IsActive;
            if (!entity.Name.IsDefault())
                SCRFIllustrationSubmissionComment.Name = entity.Name.Trim();
            if (!entity.Email.IsDefault())
                SCRFIllustrationSubmissionComment.Email = entity.Email.Trim();
            if (!entity.Description.IsDefault())
            SCRFIllustrationSubmissionComment.Description = entity.Description.Trim();
            SCRFIllustrationSubmissionComment.CreatedOn = entity.CreatedOn;
            SCRFIllustrationSubmissionComment.ModifiedOn = entity.ModifiedOn;

            XsiContext.Context.XsiScrfillustrationSubmissionComments.Add(SCRFIllustrationSubmissionComment);
            SubmitChanges();
            return SCRFIllustrationSubmissionComment;

        }
        public void Update(XsiScrfillustrationSubmissionComments entity)
        {
            XsiScrfillustrationSubmissionComments SCRFIllustrationSubmissionComment = XsiContext.Context.XsiScrfillustrationSubmissionComments.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFIllustrationSubmissionComment).State = EntityState.Modified;

            if (!entity.IllustrationSubmissionId.IsDefault())
                SCRFIllustrationSubmissionComment.IllustrationSubmissionId = entity.IllustrationSubmissionId;

            if (!entity.LanguageId.IsDefault())
                SCRFIllustrationSubmissionComment.LanguageId = entity.LanguageId;

            if (!entity.IsActive.IsDefault())
                SCRFIllustrationSubmissionComment.IsActive = entity.IsActive;

            if (!entity.Name.IsDefault())
                SCRFIllustrationSubmissionComment.Name = entity.Name.Trim();

            if (!entity.Email.IsDefault())
                SCRFIllustrationSubmissionComment.Email = entity.Email.Trim();

            if (!entity.Description.IsDefault())
                SCRFIllustrationSubmissionComment.Description = entity.Description.Trim();

            if (!entity.CreatedOn.IsDefault())
                SCRFIllustrationSubmissionComment.CreatedOn = entity.CreatedOn;

            if (!entity.ModifiedOn.IsDefault())
                SCRFIllustrationSubmissionComment.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiScrfillustrationSubmissionComments entity)
        {
            foreach (XsiScrfillustrationSubmissionComments SCRFIllustrationSubmissionComment in Select(entity))
            {
                XsiScrfillustrationSubmissionComments aSCRFIllustrationSubmissionComment = XsiContext.Context.XsiScrfillustrationSubmissionComments.Find(SCRFIllustrationSubmissionComment.ItemId);
                XsiContext.Context.XsiScrfillustrationSubmissionComments.Remove(aSCRFIllustrationSubmissionComment);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfillustrationSubmissionComments aSCRFIllustrationSubmissionComment = XsiContext.Context.XsiScrfillustrationSubmissionComments.Find(itemId);
            XsiContext.Context.XsiScrfillustrationSubmissionComments.Remove(aSCRFIllustrationSubmissionComment);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class VideoAlbumRepository : IVideoAlbumRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public VideoAlbumRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public VideoAlbumRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public VideoAlbumRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiVideoAlbum GetById(long itemId)
        {
            return XsiContext.Context.XsiVideoAlbum.Find(itemId);
        }

        public List<XsiVideoAlbum> Select()
        {
            return XsiContext.Context.XsiVideoAlbum.ToList();
        }
        public List<XsiVideoAlbum> Select(XsiVideoAlbum entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiVideoAlbum>();
            var predicate = PredicateBuilder.True<XsiVideoAlbum>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.VideoCategoryId.IsDefault())
                predicate = predicate.And(p => p.VideoCategoryId == entity.VideoCategoryId);

         
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.TitleAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.TitleAr.Contains(searchKeys.str1) || p.TitleAr.Contains(searchKeys.str2) || p.TitleAr.Contains(searchKeys.str3) || p.TitleAr.Contains(searchKeys.str4) || p.TitleAr.Contains(searchKeys.str5)
                    || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                    || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                    || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated == entity.Dated);

            if (!entity.SortIndex.IsDefault())
                predicate = predicate.And(p => p.SortIndex == entity.SortIndex);

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.DescriptionAr.IsDefault())
                predicate = predicate.And(p => p.DescriptionAr.Contains(entity.DescriptionAr));

            if (!entity.DatedAr.IsDefault())
                predicate = predicate.And(p => p.DatedAr == entity.DatedAr);

            if (!entity.SortIndexAr.IsDefault())
                predicate = predicate.And(p => p.SortIndexAr == entity.SortIndexAr);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiVideoAlbum.AsExpandable().Where(predicate).ToList();
        }

        public XsiVideoAlbum Add(XsiVideoAlbum entity)
        {
            XsiVideoAlbum VideoAlbum = new XsiVideoAlbum();
            VideoAlbum.VideoCategoryId = entity.VideoCategoryId;
            VideoAlbum.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                VideoAlbum.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
            VideoAlbum.Description = entity.Description.Trim();
            VideoAlbum.Dated = entity.Dated;
            VideoAlbum.SortIndex = entity.SortIndex;

            VideoAlbum.IsActiveAr = entity.IsActiveAr;
            if (!entity.TitleAr.IsDefault())
                VideoAlbum.TitleAr = entity.TitleAr.Trim();
            if (!entity.DescriptionAr.IsDefault())
                VideoAlbum.DescriptionAr = entity.DescriptionAr.Trim();
            VideoAlbum.DatedAr = entity.DatedAr;
            VideoAlbum.SortIndexAr = entity.SortIndexAr;

            VideoAlbum.CreatedOn = entity.CreatedOn;
            VideoAlbum.CreatedBy = entity.CreatedBy;
            VideoAlbum.ModifiedOn = entity.ModifiedOn;
            VideoAlbum.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiVideoAlbum.Add(VideoAlbum);
            SubmitChanges();
            return VideoAlbum;

        }
        public void Update(XsiVideoAlbum entity)
        {
            XsiVideoAlbum VideoAlbum = XsiContext.Context.XsiVideoAlbum.Find(entity.ItemId);
            XsiContext.Context.Entry(VideoAlbum).State = EntityState.Modified;

            if (!entity.VideoCategoryId.IsDefault())
                VideoAlbum.VideoCategoryId = entity.VideoCategoryId;
        
            if (!entity.IsActive.IsDefault())
                VideoAlbum.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                VideoAlbum.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                VideoAlbum.Description = entity.Description.Trim();

            if (!entity.Dated.IsDefault())
                VideoAlbum.Dated = entity.Dated;

            if (!entity.SortIndex.IsDefault())
                VideoAlbum.SortIndex = entity.SortIndex;


            if (!entity.IsActiveAr.IsDefault())
                VideoAlbum.IsActiveAr = entity.IsActiveAr;

            if (!entity.TitleAr.IsDefault())
                VideoAlbum.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                VideoAlbum.DescriptionAr = entity.DescriptionAr.Trim();

            if (!entity.DatedAr.IsDefault())
                VideoAlbum.DatedAr = entity.DatedAr;

            if (!entity.SortIndexAr.IsDefault())
                VideoAlbum.SortIndexAr = entity.SortIndexAr;


            if (!entity.CreatedOn.IsDefault())
                VideoAlbum.CreatedOn = entity.CreatedOn;

            if (!entity.CreatedBy.IsDefault())
                VideoAlbum.CreatedBy = entity.CreatedBy;

            if (!entity.ModifiedOn.IsDefault())
                VideoAlbum.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                VideoAlbum.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiVideoAlbum entity)
        {
            foreach (XsiVideoAlbum VideoAlbum in Select(entity))
            {
                XsiVideoAlbum aVideoAlbum = XsiContext.Context.XsiVideoAlbum.Find(VideoAlbum.ItemId);
                XsiContext.Context.XsiVideoAlbum.Remove(aVideoAlbum);
            }
        }
        public void Delete(long itemId)
        {
            XsiVideoAlbum aVideoAlbum = XsiContext.Context.XsiVideoAlbum.Find(itemId);
            XsiContext.Context.XsiVideoAlbum.Remove(aVideoAlbum);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
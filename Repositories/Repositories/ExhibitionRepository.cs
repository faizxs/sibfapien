﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;


namespace Xsi.Repositories
{
    public class ExhibitionRepository : IExhibitionRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibition GetById(long itemId)
        {
            if (CachingRepository.CachedExhibition == null)
                CachingRepository.CachedExhibition = XsiContext.Context.XsiExhibition.ToList();
            return CachingRepository.CachedExhibition.AsQueryable().Where(i => i.ExhibitionId == itemId).FirstOrDefault();

            //return XsiContext.Context.XsiExhibition.Find(itemId);
        }

        public List<XsiExhibition> Select()
        {
            if (CachingRepository.CachedExhibition == null)
                CachingRepository.CachedExhibition = XsiContext.Context.XsiExhibition.ToList();
            return CachingRepository.CachedExhibition;

            // return XsiContext.Context.XsiExhibition.ToList();
        }
        public List<XsiExhibition> Select(XsiExhibition entity)
        {
            if (CachingRepository.CachedExhibition == null)
                CachingRepository.CachedExhibition = XsiContext.Context.XsiExhibition.ToList();

            var predicate = PredicateBuilder.True<XsiExhibition>();
            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            //if (!entity.ActivityIds.IsDefault())
            //    predicate = predicate.And(p => p.ActivityIds.Equals(entity.ActivityIds));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsArchive.IsDefault())
                predicate = predicate.And(p => p.IsArchive.Equals(entity.IsArchive));

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.ExhibitionYear.IsDefault())
                predicate = predicate.And(p => p.ExhibitionYear == entity.ExhibitionYear);

            if (!entity.ExhibitionNumber.IsDefault())
            {
                if (entity.ExhibitionNumber == 0)
                    entity.ExhibitionNumber = null;
                predicate = predicate.And(p => p.ExhibitionNumber.Equals(entity.ExhibitionNumber));
            }
            if (!entity.TopContent.IsDefault())
                predicate = predicate.And(p => p.TopContent.Contains(entity.TopContent));

            if (!entity.StartDate.IsDefault())
                predicate = predicate.And(p => p.StartDate == entity.StartDate);

            if (!entity.EndDate.IsDefault())
                predicate = predicate.And(p => p.EndDate == entity.EndDate);

            if (!entity.AwardStartDate.IsDefault())
                predicate = predicate.And(p => p.AwardStartDate == entity.AwardStartDate);

            if (!entity.AwardEndDate.IsDefault())
                predicate = predicate.And(p => p.AwardEndDate == entity.AwardEndDate);

            if (!entity.TurjumanStartDate.IsDefault())
                predicate = predicate.And(p => p.TurjumanStartDate == entity.TurjumanStartDate);

            if (!entity.TurjumanEndDate.IsDefault())
                predicate = predicate.And(p => p.TurjumanEndDate == entity.TurjumanEndDate);

            if (!entity.DateOfCacellation.IsDefault())
                predicate = predicate.And(p => p.DateOfCacellation == entity.DateOfCacellation);

            if (!entity.CancellationFee.IsDefault())
                predicate = predicate.And(p => p.CancellationFee.Equals(entity.CancellationFee));

            if (!entity.RepresentativeCancellationFee.IsDefault())
                predicate = predicate.And(p => p.RepresentativeCancellationFee.Equals(entity.RepresentativeCancellationFee));

            if (!entity.DateOfDue.IsDefault())
                predicate = predicate.And(p => p.DateOfDue == entity.DateOfDue);

            if (!entity.Penalty.IsDefault())
                predicate = predicate.And(p => p.Penalty.Equals(entity.Penalty));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Equals(entity.FileName));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Equals(entity.Url));

            if (!entity.TotalArea.IsDefault())
                predicate = predicate.And(p => p.TotalArea.Equals(entity.TotalArea));

            if (!entity.ExhibitorStartDate.IsDefault())
                predicate = predicate.And(p => p.ExhibitorStartDate == entity.ExhibitorStartDate);

            if (!entity.ExhibitorEndDate.IsDefault())
                predicate = predicate.And(p => p.ExhibitorEndDate == entity.ExhibitorEndDate);

            if (!entity.RestaurantStartDate.IsDefault())
                predicate = predicate.And(p => p.RestaurantStartDate == entity.RestaurantStartDate);

            if (!entity.RestaurantEndDate.IsDefault())
                predicate = predicate.And(p => p.RestaurantEndDate == entity.RestaurantEndDate);

            if (!entity.AgencyStartDate.IsDefault())
                predicate = predicate.And(p => p.AgencyStartDate == entity.AgencyStartDate);

            if (!entity.AgencyEndDate.IsDefault())
                predicate = predicate.And(p => p.AgencyEndDate == entity.AgencyEndDate);

            if (!entity.ParticipationFee.IsDefault())
                predicate = predicate.And(p => p.ParticipationFee == entity.ParticipationFee);

            if (!entity.PriceSqM.IsDefault())
                predicate = predicate.And(p => p.PriceSqM == entity.PriceSqM);

            if (!entity.VisaProcessingPrice.IsDefault())
                predicate = predicate.And(p => p.VisaProcessingPrice == entity.VisaProcessingPrice);

            if (!entity.ArabicAgencyFees.IsDefault())
                predicate = predicate.And(p => p.ArabicAgencyFees == entity.ArabicAgencyFees);

            if (!entity.ForeignAgencyFees.IsDefault())
                predicate = predicate.And(p => p.ForeignAgencyFees == entity.ForeignAgencyFees);

            if (!entity.AdvertisementRates.IsDefault())
                predicate = predicate.And(p => p.AdvertisementRates == entity.AdvertisementRates);

            if (!entity.ImportantInformation.IsDefault())
                predicate = predicate.And(p => p.ImportantInformation == entity.ImportantInformation);

            if (!entity.ExpiryMonths.IsDefault())
                predicate = predicate.And(p => p.ExpiryMonths.Equals(entity.ExpiryMonths));

            //if (!entity.ExhibitorStep1.IsDefault())
            //    predicate = predicate.And(p => p.ExhibitorStep1.Contains(entity.ExhibitorStep1));

            //if (!entity.ExhibitorStep2.IsDefault())
            //    predicate = predicate.And(p => p.ExhibitorStep2.Contains(entity.ExhibitorStep2));

            //if (!entity.ExhibitorStep3.IsDefault())
            //    predicate = predicate.And(p => p.ExhibitorStep3.Contains(entity.ExhibitorStep3));

            //if (!entity.ExhibitorStep4.IsDefault())
            //    predicate = predicate.And(p => p.ExhibitorStep4.Contains(entity.ExhibitorStep4));

            //if (!entity.ExhibitorStep5.IsDefault())
            //    predicate = predicate.And(p => p.ExhibitorStep5.Contains(entity.ExhibitorStep5));

            //if (!entity.ExhibitorStep6.IsDefault())
            //    predicate = predicate.And(p => p.ExhibitorStep6.Contains(entity.ExhibitorStep6));

            //if (!entity.AgencyStep1.IsDefault())
            //    predicate = predicate.And(p => p.AgencyStep1.Contains(entity.AgencyStep1));

            //if (!entity.AgencyStep2.IsDefault())
            //    predicate = predicate.And(p => p.AgencyStep2.Contains(entity.AgencyStep2));

            //if (!entity.AgencyStep3.IsDefault())
            //    predicate = predicate.And(p => p.AgencyStep3.Contains(entity.AgencyStep3));

            //if (!entity.AgencyStep4.IsDefault())
            //    predicate = predicate.And(p => p.AgencyStep4.Contains(entity.AgencyStep4));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return CachingRepository.CachedExhibition.AsQueryable().Where(predicate).ToList();
            //return XsiContext.Context.XsiExhibition.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibition Add(XsiExhibition entity)
        {
            if (CachingRepository.CachedExhibition == null)
                CachingRepository.CachedExhibition = XsiContext.Context.XsiExhibition.ToList();

            XsiExhibition Exhibition = new XsiExhibition();

            //Exhibition.ActivityIds = entity.ActivityIds;

            Exhibition.IsActive = entity.IsActive;
            Exhibition.IsArchive = entity.IsArchive;
            Exhibition.WebsiteId = entity.WebsiteId;
            if (!entity.ExhibitionYear.IsDefault())
                Exhibition.ExhibitionYear = entity.ExhibitionYear;
            if (!entity.ExhibitionNumber.IsDefault())
                Exhibition.ExhibitionNumber = entity.ExhibitionNumber;
            if (!entity.TopContent.IsDefault())
                Exhibition.TopContent = entity.TopContent.Trim();
            Exhibition.StartDate = entity.StartDate;
            Exhibition.EndDate = entity.EndDate;
            Exhibition.AwardStartDate = entity.AwardStartDate;
            Exhibition.AwardEndDate = entity.AwardEndDate;
            Exhibition.TurjumanStartDate = entity.TurjumanStartDate;
            Exhibition.TurjumanEndDate = entity.TurjumanEndDate;
            Exhibition.ExhibitorStartDate = entity.ExhibitorStartDate;
            Exhibition.ExhibitorEndDate = entity.ExhibitorEndDate;

            Exhibition.RestaurantStartDate = entity.RestaurantStartDate;
            Exhibition.RestaurantEndDate = entity.RestaurantEndDate;

            Exhibition.AgencyStartDate = entity.AgencyStartDate;
            Exhibition.AgencyEndDate = entity.AgencyEndDate;
            if (!entity.ParticipationFee.IsDefault())
                Exhibition.ParticipationFee = entity.ParticipationFee;
            if (!entity.PriceSqM.IsDefault())
                Exhibition.PriceSqM = entity.PriceSqM;
            Exhibition.DateOfCacellation = entity.DateOfCacellation;
            if (!entity.CancellationFee.IsDefault())
                Exhibition.CancellationFee = entity.CancellationFee.Trim();
            if (!entity.RepresentativeCancellationFee.IsDefault())
                Exhibition.RepresentativeCancellationFee = entity.RepresentativeCancellationFee.Trim();
            Exhibition.DateOfDue = entity.DateOfDue;
            if (!entity.Penalty.IsDefault())
                Exhibition.Penalty = entity.Penalty;

            if (!entity.TotalArea.IsDefault())
                Exhibition.TotalArea = entity.TotalArea.Trim();
            if (!entity.VisaProcessingPrice.IsDefault())
                Exhibition.VisaProcessingPrice = entity.VisaProcessingPrice;
            if (!entity.ArabicAgencyFees.IsDefault())
                Exhibition.ArabicAgencyFees = entity.ArabicAgencyFees;
            if (!entity.ForeignAgencyFees.IsDefault())
                Exhibition.ForeignAgencyFees = entity.ForeignAgencyFees;
            if (!entity.AdvertisementRates.IsDefault())
                Exhibition.AdvertisementRates = entity.AdvertisementRates;
            if (!entity.ImportantInformation.IsDefault())
                Exhibition.ImportantInformation = entity.ImportantInformation;
            if (!entity.ExpiryMonths.IsDefault())
                Exhibition.ExpiryMonths = entity.ExpiryMonths;
            //if (!entity.ExhibitorStep1.IsDefault())
            //    Exhibition.ExhibitorStep1 = entity.ExhibitorStep1.Trim();
            //if (!entity.ExhibitorStep2.IsDefault())
            //    Exhibition.ExhibitorStep2 = entity.ExhibitorStep2.Trim();
            //if (!entity.ExhibitorStep3.IsDefault())
            //    Exhibition.ExhibitorStep3 = entity.ExhibitorStep3.Trim();
            //if (!entity.ExhibitorStep4.IsDefault())
            //    Exhibition.ExhibitorStep4 = entity.ExhibitorStep4.Trim();
            //if (!entity.ExhibitorStep5.IsDefault())
            //    Exhibition.ExhibitorStep5 = entity.ExhibitorStep5.Trim();
            //if (!entity.ExhibitorStep6.IsDefault())
            //    Exhibition.ExhibitorStep6 = entity.ExhibitorStep6.Trim();
            //if (!entity.AgencyStep1.IsDefault())
            //    Exhibition.AgencyStep1 = entity.AgencyStep1.Trim();
            //if (!entity.AgencyStep2.IsDefault())
            //    Exhibition.AgencyStep2 = entity.AgencyStep2.Trim();
            //if (!entity.AgencyStep3.IsDefault())
            //    Exhibition.AgencyStep3 = entity.AgencyStep3.Trim();
            //if (!entity.AgencyStep4.IsDefault())
            //    Exhibition.AgencyStep4 = entity.AgencyStep4.Trim();
            #region Ar
            Exhibition.IsActiveAr = entity.IsActiveAr;
            Exhibition.FileNameAr = entity.FileNameAr;
            if (!entity.Urlar.IsDefault())
                Exhibition.Urlar = entity.Urlar.Trim();
            #endregion Ar
            Exhibition.CreatedOn = entity.CreatedOn;
            Exhibition.CreatedBy = entity.CreatedBy;
            Exhibition.ModifiedOn = entity.ModifiedOn;
            Exhibition.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibition.Add(Exhibition);
            SubmitChanges();
            CachingRepository.CachedExhibition.Add(Exhibition);
            return Exhibition;

        }
        public void Update(XsiExhibition entity)
        {
            if (CachingRepository.CachedExhibition == null)
                CachingRepository.CachedExhibition = XsiContext.Context.XsiExhibition.ToList();

            XsiExhibition Exhibition = XsiContext.Context.XsiExhibition.Find(entity.ExhibitionId);
            XsiContext.Context.Entry(Exhibition).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                Exhibition.IsActive = entity.IsActive;

            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                Exhibition.IsActiveAr = entity.IsActiveAr;
            if (!entity.FileNameAr.IsDefault())
                Exhibition.FileNameAr = entity.FileNameAr;
            if (!entity.Urlar.IsDefault())
                Exhibition.Urlar = entity.Urlar.Trim();
            #endregion Ar

            if (!entity.IsArchive.IsDefault())
                Exhibition.IsArchive = entity.IsArchive;

            //if (!entity.ActivityIds.IsDefault())
            //    Exhibition.ActivityIds = entity.ActivityIds;

            if (!entity.WebsiteId.IsDefault())
                Exhibition.WebsiteId = entity.WebsiteId;

            if (!entity.ExhibitionYear.IsDefault())
                Exhibition.ExhibitionYear = entity.ExhibitionYear;

            if (!entity.ExhibitionNumber.IsDefault())
                Exhibition.ExhibitionNumber = entity.ExhibitionNumber;

            if (!entity.TopContent.IsDefault())
                Exhibition.TopContent = entity.TopContent.Trim();

            if (!entity.StartDate.IsDefault())
                Exhibition.StartDate = entity.StartDate;

            if (!entity.EndDate.IsDefault())
                Exhibition.EndDate = entity.EndDate;

            if (!entity.AwardStartDate.IsDefault())
                Exhibition.AwardStartDate = entity.AwardStartDate;

            if (!entity.AwardEndDate.IsDefault())
                Exhibition.AwardEndDate = entity.AwardEndDate;

            if (!entity.TurjumanStartDate.IsDefault())
                Exhibition.TurjumanStartDate = entity.TurjumanStartDate;

            if (!entity.TurjumanEndDate.IsDefault())
                Exhibition.TurjumanEndDate = entity.TurjumanEndDate;

            if (!entity.ExhibitorStartDate.IsDefault())
                Exhibition.ExhibitorStartDate = entity.ExhibitorStartDate;

            if (!entity.ExhibitorEndDate.IsDefault())
                Exhibition.ExhibitorEndDate = entity.ExhibitorEndDate;

            if (!entity.RestaurantStartDate.IsDefault())
                Exhibition.RestaurantStartDate = entity.RestaurantStartDate;

            if (!entity.RestaurantEndDate.IsDefault())
                Exhibition.RestaurantEndDate = entity.RestaurantEndDate;

            if (!entity.DateOfCacellation.IsDefault())
                Exhibition.DateOfCacellation = entity.DateOfCacellation;

            if (!entity.CancellationFee.IsDefault())
                Exhibition.CancellationFee = entity.CancellationFee;

            if (!entity.RepresentativeCancellationFee.IsDefault())
                Exhibition.RepresentativeCancellationFee = entity.RepresentativeCancellationFee;

            if (!entity.DateOfDue.IsDefault())
                Exhibition.DateOfDue = entity.DateOfDue;

            if (!entity.Penalty.IsDefault())
                Exhibition.Penalty = entity.Penalty;

            if (!entity.FileName.IsDefault())
                Exhibition.FileName = entity.FileName;

            if (!entity.Url.IsDefault())
                Exhibition.Url = entity.Url.Trim();

            if (!entity.TotalArea.IsDefault())
                Exhibition.TotalArea = entity.TotalArea.Trim();

            if (!entity.AgencyStartDate.IsDefault())
                Exhibition.AgencyStartDate = entity.AgencyStartDate;

            if (!entity.AgencyEndDate.IsDefault())
                Exhibition.AgencyEndDate = entity.AgencyEndDate;

            if (!entity.ParticipationFee.IsDefault())
                Exhibition.ParticipationFee = entity.ParticipationFee;

            if (!entity.PriceSqM.IsDefault())
                Exhibition.PriceSqM = entity.PriceSqM;

            if (!entity.VisaProcessingPrice.IsDefault())
                Exhibition.VisaProcessingPrice = entity.VisaProcessingPrice;

            if (!entity.ArabicAgencyFees.IsDefault())
                Exhibition.ArabicAgencyFees = entity.ArabicAgencyFees;

            if (!entity.ForeignAgencyFees.IsDefault())
                Exhibition.ForeignAgencyFees = entity.ForeignAgencyFees;

            if (!entity.AdvertisementRates.IsDefault())
                Exhibition.AdvertisementRates = entity.AdvertisementRates;

            if (!entity.ImportantInformation.IsDefault())
                Exhibition.ImportantInformation = entity.ImportantInformation;

            if (!entity.ExpiryMonths.IsDefault())
                Exhibition.ExpiryMonths = entity.ExpiryMonths;

            //if (!entity.ExhibitorStep1.IsDefault())
            //    Exhibition.ExhibitorStep1 = entity.ExhibitorStep1;

            //if (!entity.ExhibitorStep2.IsDefault())
            //    Exhibition.ExhibitorStep2 = entity.ExhibitorStep2.Trim();

            //if (!entity.ExhibitorStep3.IsDefault())
            //    Exhibition.ExhibitorStep3 = entity.ExhibitorStep3.Trim();

            //if (!entity.ExhibitorStep4.IsDefault())
            //    Exhibition.ExhibitorStep4 = entity.ExhibitorStep4.Trim();

            //if (!entity.ExhibitorStep5.IsDefault())
            //    Exhibition.ExhibitorStep5 = entity.ExhibitorStep5.Trim();

            //if (!entity.ExhibitorStep6.IsDefault())
            //    Exhibition.ExhibitorStep6 = entity.ExhibitorStep6.Trim();

            //if (!entity.AgencyStep1.IsDefault())
            //    Exhibition.AgencyStep1 = entity.AgencyStep1.Trim();

            //if (!entity.AgencyStep2.IsDefault())
            //    Exhibition.AgencyStep2 = entity.AgencyStep2.Trim();

            //if (!entity.AgencyStep3.IsDefault())
            //    Exhibition.AgencyStep3 = entity.AgencyStep3.Trim();

            //if (!entity.AgencyStep4.IsDefault())
            //    Exhibition.AgencyStep4 = entity.AgencyStep4.Trim();

            if (!entity.ModifiedOn.IsDefault())
                Exhibition.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                Exhibition.ModifiedBy = entity.ModifiedBy;
            CachingRepository.CachedExhibition.Remove(Exhibition);
            CachingRepository.CachedExhibition.Add(Exhibition);
        }
        public void Delete(XsiExhibition entity)
        {
            if (CachingRepository.CachedExhibition == null)
                CachingRepository.CachedExhibition = XsiContext.Context.XsiExhibition.ToList();

            foreach (XsiExhibition Exhibition in Select(entity))
            {
                XsiExhibition aExhibition = XsiContext.Context.XsiExhibition.Find(Exhibition.ExhibitionId);
                XsiContext.Context.XsiExhibition.Remove(aExhibition);
            }
        }
        public void Delete(long itemId)
        {
            if (CachingRepository.CachedExhibition == null)
                CachingRepository.CachedExhibition = XsiContext.Context.XsiExhibition.ToList();

            XsiExhibition aExhibition = XsiContext.Context.XsiExhibition.Find(itemId);
            XsiContext.Context.XsiExhibition.Remove(aExhibition);
            CachingRepository.CachedExhibition.Remove(aExhibition);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
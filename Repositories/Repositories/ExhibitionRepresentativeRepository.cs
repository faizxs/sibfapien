﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionRepresentativeRepository : IExhibitionRepresentativeRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionRepresentativeRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionRepresentativeRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionRepresentativeRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionRepresentative GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionRepresentative.Find(itemId);
        }
        public List<XsiExhibitionRepresentative> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionRepresentative> Select()
        {
            return XsiContext.Context.XsiExhibitionRepresentative.ToList();
        }
        public List<XsiExhibitionRepresentative> Select(XsiExhibitionRepresentative entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionRepresentative>();
            if (!entity.RepresentativeId.IsDefault())
                predicate = predicate.And(p => p.RepresentativeId == entity.RepresentativeId);

            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsEmailSent.IsDefault())
                predicate = predicate.And(p => p.IsEmailSent.Equals(entity.IsEmailSent));

            if (!entity.NameEn.IsDefault())
                predicate = predicate.And(p => p.NameEn.Contains(entity.NameEn));

            if (!entity.NameAr.IsDefault())
                predicate = predicate.And(p => p.NameAr.Contains(entity.NameAr));

            if (!entity.Dob.IsDefault())
                predicate = predicate.And(p => p.Dob == entity.Dob);


            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionRepresentative.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionRepresentative Add(XsiExhibitionRepresentative entity)
        {
            XsiExhibitionRepresentative Representative = new XsiExhibitionRepresentative();
            Representative.MemberId = entity.MemberId;

            Representative.IsActive = entity.IsActive;
            Representative.IsEmailSent = entity.IsEmailSent;
            if (!entity.NameEn.IsDefault())
                Representative.NameEn = entity.NameEn.Trim();
            if (!entity.NameAr.IsDefault())
                Representative.NameAr = entity.NameAr.Trim();
            if (entity.Dob != null)
                Representative.Dob = entity.Dob;
            Representative.PlaceOfBirth = entity.PlaceOfBirth;
            Representative.CreatedOn = entity.CreatedOn;
            Representative.CreatedBy = entity.CreatedBy;
            Representative.ModifiedOn = entity.ModifiedOn;
            Representative.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionRepresentative.Add(Representative);
            SubmitChanges();
            return Representative;

        }
        public void Update(XsiExhibitionRepresentative entity)
        {
            XsiExhibitionRepresentative Representative = XsiContext.Context.XsiExhibitionRepresentative.Find(entity.RepresentativeId);
            XsiContext.Context.Entry(Representative).State = EntityState.Modified;

            if (!entity.MemberId.IsDefault())
                Representative.MemberId = entity.MemberId;

            if (!entity.IsActive.IsDefault())
                Representative.IsActive = entity.IsActive;


            if (!entity.IsEmailSent.IsDefault())
                Representative.IsEmailSent = entity.IsEmailSent;

            if (!entity.NameEn.IsDefault())
                Representative.NameEn = entity.NameEn.Trim();

            if (!entity.NameAr.IsDefault())
                Representative.NameAr = entity.NameAr.Trim();

            if (!entity.Dob.IsDefault())
                Representative.Dob = entity.Dob;

            if (!entity.PlaceOfBirth.IsDefault())
                Representative.PlaceOfBirth = entity.PlaceOfBirth;

            if (!entity.ModifiedOn.IsDefault())
                Representative.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                Representative.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionRepresentative entity)
        {
            foreach (XsiExhibitionRepresentative Representative in Select(entity))
            {
                XsiExhibitionRepresentative aRepresentative = XsiContext.Context.XsiExhibitionRepresentative.Find(Representative.RepresentativeId);
                XsiContext.Context.XsiExhibitionRepresentative.Remove(aRepresentative);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionRepresentative aRepresentative = XsiContext.Context.XsiExhibitionRepresentative.Find(itemId);
            XsiContext.Context.XsiExhibitionRepresentative.Remove(aRepresentative);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
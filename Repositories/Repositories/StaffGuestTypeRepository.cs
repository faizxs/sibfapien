﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class StaffGuestTypeRepository : IStaffGuestTypeRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public StaffGuestTypeRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public StaffGuestTypeRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public StaffGuestTypeRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiStaffGuestType GetById(long itemId)
        {
            return XsiContext.Context.XsiStaffGuestType.Find(itemId);
        }
        public List<XsiStaffGuestType> Select()
        {
            return XsiContext.Context.XsiStaffGuestType.ToList();
        }
        public List<XsiStaffGuestType> Select(XsiStaffGuestType entity)
        {
            var predicate = PredicateBuilder.True<XsiStaffGuestType>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.ToLower().Contains(entity.TitleAr));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiStaffGuestType.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiStaffGuestType> SelectOr(XsiStaffGuestType entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiStaffGuestType>();
            var Outer = PredicateBuilder.True<XsiStaffGuestType>();

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.TitleAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.TitleAr.Contains(searchKeys.str1) || p.TitleAr.Contains(searchKeys.str2) || p.TitleAr.Contains(searchKeys.str3) || p.TitleAr.Contains(searchKeys.str4) || p.TitleAr.Contains(searchKeys.str5)
                    || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                    || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                    || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsActiveAr.IsDefault())
                Outer = Outer.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiStaffGuestType.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiStaffGuestType Add(XsiStaffGuestType entity)
        {
            XsiStaffGuestType StaffGuestType = new XsiStaffGuestType();
            if (!entity.Title.IsDefault())
                StaffGuestType.Title = entity.Title.Trim();
            StaffGuestType.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                StaffGuestType.TitleAr = entity.TitleAr.Trim();
            StaffGuestType.IsActiveAr = entity.IsActiveAr;
            StaffGuestType.CreatedOn = entity.CreatedOn;
            StaffGuestType.CreatedBy = entity.CreatedBy;
            StaffGuestType.ModifiedOn = entity.ModifiedOn;
            StaffGuestType.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiStaffGuestType.Add(StaffGuestType);
            SubmitChanges();
            return StaffGuestType;

        }
        public void Update(XsiStaffGuestType entity)
        {
            XsiStaffGuestType StaffGuestType = XsiContext.Context.XsiStaffGuestType.Find(entity.ItemId);
            XsiContext.Context.Entry(StaffGuestType).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                StaffGuestType.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                StaffGuestType.IsActive = entity.IsActive;
            if (!entity.TitleAr.IsDefault())
                StaffGuestType.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                StaffGuestType.IsActiveAr = entity.IsActiveAr;
            if (!entity.ModifiedOn.IsDefault())
                StaffGuestType.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                StaffGuestType.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiStaffGuestType entity)
        {
            foreach (XsiStaffGuestType StaffGuestType in Select(entity))
            {
                XsiStaffGuestType aStaffGuestType = XsiContext.Context.XsiStaffGuestType.Find(StaffGuestType.ItemId);
                XsiContext.Context.XsiStaffGuestType.Remove(aStaffGuestType);
            }
        }
        public void Delete(long itemId)
        {
            XsiStaffGuestType aStaffGuestType = XsiContext.Context.XsiStaffGuestType.Find(itemId);
            XsiContext.Context.XsiStaffGuestType.Remove(aStaffGuestType);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFVideoCategoryRepository : ISCRFVideoCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFVideoCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFVideoCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFVideoCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfvideoCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfvideoCategory.Find(itemId);
        }
         
        public List<XsiScrfvideoCategory> Select()
        {
            return XsiContext.Context.XsiScrfvideoCategory.ToList();
        }
        public List<XsiScrfvideoCategory> Select(XsiScrfvideoCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfvideoCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiScrfvideoCategory.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiScrfvideoCategory> SelectOr(XsiScrfvideoCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfvideoCategory>();
            var Outer = PredicateBuilder.True<XsiScrfvideoCategory>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
             
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiScrfvideoCategory.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiScrfvideoCategory Add(XsiScrfvideoCategory entity)
        {
            XsiScrfvideoCategory VideoCategory = new XsiScrfvideoCategory();
            
            if (!entity.Title.IsDefault())
                VideoCategory.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
            VideoCategory.Description = entity.Description.Trim();
            VideoCategory.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                VideoCategory.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                VideoCategory.DescriptionAr = entity.DescriptionAr.Trim();

            if (!entity.IsActiveAr.IsDefault())
                VideoCategory.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            VideoCategory.CreatedOn = entity.CreatedOn;
            VideoCategory.CreatedBy = entity.CreatedBy;
            VideoCategory.ModifiedOn = entity.ModifiedOn;
            VideoCategory.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfvideoCategory.Add(VideoCategory);
            SubmitChanges();
            return VideoCategory;

        }
        public void Update(XsiScrfvideoCategory entity)
        {
            XsiScrfvideoCategory VideoCategory = XsiContext.Context.XsiScrfvideoCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(VideoCategory).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                VideoCategory.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                VideoCategory.Description = entity.Description.Trim();

            if (!entity.IsActive.IsDefault())
                VideoCategory.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                VideoCategory.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                VideoCategory.DescriptionAr = entity.DescriptionAr.Trim();

            if (!entity.IsActiveAr.IsDefault())
                VideoCategory.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                VideoCategory.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                VideoCategory.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiScrfvideoCategory entity)
        {
            foreach (XsiScrfvideoCategory VideoCategory in Select(entity))
            {
                XsiScrfvideoCategory aVideoCategory = XsiContext.Context.XsiScrfvideoCategory.Find(VideoCategory.ItemId);
                XsiContext.Context.XsiScrfvideoCategory.Remove(aVideoCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfvideoCategory aVideoCategory = XsiContext.Context.XsiScrfvideoCategory.Find(itemId);
            XsiContext.Context.XsiScrfvideoCategory.Remove(aVideoCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
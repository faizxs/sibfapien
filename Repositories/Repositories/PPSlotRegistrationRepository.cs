﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PPSlotRegistrationRepository : IPPSlotRegistrationRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PPSlotRegistrationRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PPSlotRegistrationRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PPSlotRegistrationRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionPpslotInvitation GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionPpslotInvitation.Find(itemId);
        }

        public List<XsiExhibitionPpslotInvitation> Select()
        {
            return XsiContext.Context.XsiExhibitionPpslotInvitation.ToList();
        }
        public List<XsiExhibitionPpslotInvitation> Select(XsiExhibitionPpslotInvitation entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionPpslotInvitation>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.RequestId.IsDefault())
                predicate = predicate.And(p => p.RequestId == entity.RequestId);

            if (!entity.ResponseId.IsDefault())
                predicate = predicate.And(p => p.ResponseId == entity.ResponseId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.IsViewed.IsDefault())
                predicate = predicate.And(p => p.IsViewed.Equals(entity.IsViewed));

            if (!entity.Message.IsDefault())
                predicate = predicate.And(p => p.Message.Equals(entity.Message));

            if (!entity.InviteUrl.IsDefault())
                predicate = predicate.And(p => p.InviteUrl.Equals(entity.InviteUrl));
            
            if (!entity.IsInviteUrlclicked.IsDefault())
                predicate = predicate.And(p => p.IsInviteUrlclicked.Equals(entity.IsInviteUrlclicked));

            if (!entity.SlotId.IsDefault())
                predicate = predicate.And(p => p.SlotId == entity.SlotId);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedById.IsDefault())
                predicate = predicate.And(p => p.CreatedById == entity.CreatedById);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.CreatedById.IsDefault())
                predicate = predicate.And(p => p.ModifiedById == entity.ModifiedById);


            return XsiContext.Context.XsiExhibitionPpslotInvitation.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionPpslotInvitation Add(XsiExhibitionPpslotInvitation entity)
        {
            XsiExhibitionPpslotInvitation PPSlotRegistration = new XsiExhibitionPpslotInvitation();
            PPSlotRegistration.IsActive = entity.IsActive;
            PPSlotRegistration.RequestId = entity.RequestId;
            PPSlotRegistration.ResponseId = entity.ResponseId;
            PPSlotRegistration.SlotId = entity.SlotId;
            PPSlotRegistration.Status = entity.Status;
            PPSlotRegistration.IsViewed = entity.IsViewed;
            if (!entity.Message.IsDefault())
            PPSlotRegistration.Message = entity.Message.Trim();
            PPSlotRegistration.InviteUrl = entity.InviteUrl;
            PPSlotRegistration.IsInviteUrlclicked = entity.IsInviteUrlclicked;
            PPSlotRegistration.CreatedOn = entity.CreatedOn;
            PPSlotRegistration.CreatedById = entity.CreatedById;
            PPSlotRegistration.ModifiedOn = entity.ModifiedOn;
            PPSlotRegistration.ModifiedById = entity.ModifiedById;

            XsiContext.Context.XsiExhibitionPpslotInvitation.Add(PPSlotRegistration);
            SubmitChanges();
            return PPSlotRegistration;

        }
        public void Update(XsiExhibitionPpslotInvitation entity)
        {
            XsiExhibitionPpslotInvitation PPSlotRegistration = XsiContext.Context.XsiExhibitionPpslotInvitation.Find(entity.ItemId);
            XsiContext.Context.Entry(PPSlotRegistration).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                PPSlotRegistration.IsActive = entity.IsActive;

            if (!entity.RequestId.IsDefault())
                PPSlotRegistration.RequestId = entity.RequestId;

            if (!entity.ResponseId.IsDefault())
                PPSlotRegistration.ResponseId = entity.ResponseId;

            if (!entity.SlotId.IsDefault())
                PPSlotRegistration.SlotId = entity.SlotId;

            if (!entity.Status.IsDefault())
                PPSlotRegistration.Status = entity.Status;

            if (!entity.IsViewed.IsDefault())
                PPSlotRegistration.IsViewed = entity.IsViewed;

            if (!entity.Message.IsDefault())
                PPSlotRegistration.Message = entity.Message.Trim();

            if (!entity.InviteUrl.IsDefault())
                PPSlotRegistration.InviteUrl = entity.InviteUrl.Trim();

            if (!entity.IsInviteUrlclicked.IsDefault())
                PPSlotRegistration.IsInviteUrlclicked = entity.IsInviteUrlclicked.Trim();

            if (!entity.ModifiedById.IsDefault())
                PPSlotRegistration.ModifiedById = entity.ModifiedById;

            if (!entity.ModifiedOn.IsDefault())
                PPSlotRegistration.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiExhibitionPpslotInvitation entity)
        {
            foreach (XsiExhibitionPpslotInvitation PPSlotRegistration in Select(entity))
            {
                XsiExhibitionPpslotInvitation aPPSlotRegistration = XsiContext.Context.XsiExhibitionPpslotInvitation.Find(PPSlotRegistration.ItemId);
                XsiContext.Context.XsiExhibitionPpslotInvitation.Remove(aPPSlotRegistration);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionPpslotInvitation aPPSlotRegistration = XsiContext.Context.XsiExhibitionPpslotInvitation.Find(itemId);
            XsiContext.Context.XsiExhibitionPpslotInvitation.Remove(aPPSlotRegistration);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
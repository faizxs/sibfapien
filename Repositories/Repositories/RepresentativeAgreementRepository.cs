﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class RepresentativeAgreementRepository : IRepresentativeAgreementRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public RepresentativeAgreementRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public RepresentativeAgreementRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public RepresentativeAgreementRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiRepresentativeAgreement GetById(long itemId)
        {
            return XsiContext.Context.XsiRepresentativeAgreement.Find(itemId);
        }
        public List<XsiRepresentativeAgreement> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiRepresentativeAgreement> Select()
        {
            return XsiContext.Context.XsiRepresentativeAgreement.ToList();
        }
        public List<XsiRepresentativeAgreement> Select(XsiRepresentativeAgreement entity)
        {
            var predicate = PredicateBuilder.True<XsiRepresentativeAgreement>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ExhibitorId.IsDefault())
                predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

            if (!entity.Agreement.IsDefault())
                predicate = predicate.And(p => p.Agreement == entity.Agreement);

            if (!entity.SignedAgreement.IsDefault())
                predicate = predicate.And(p => p.SignedAgreement == entity.SignedAgreement);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            return XsiContext.Context.XsiRepresentativeAgreement.AsExpandable().Where(predicate).ToList();
        }

        public XsiRepresentativeAgreement Add(XsiRepresentativeAgreement entity)
        {
            XsiRepresentativeAgreement RepresentativeAgreement = new XsiRepresentativeAgreement();

            RepresentativeAgreement.ExhibitorId = entity.ExhibitorId;
            RepresentativeAgreement.Agreement = entity.Agreement;
            RepresentativeAgreement.SignedAgreement = entity.SignedAgreement;
            RepresentativeAgreement.CreatedOn = entity.CreatedOn;
            RepresentativeAgreement.ModifiedOn = entity.ModifiedOn;

            XsiContext.Context.XsiRepresentativeAgreement.Add(RepresentativeAgreement);
            SubmitChanges();
            return RepresentativeAgreement;

        }
        public void Update(XsiRepresentativeAgreement entity)
        {
            XsiRepresentativeAgreement RepresentativeAgreement = XsiContext.Context.XsiRepresentativeAgreement.Find(entity.ItemId);
            XsiContext.Context.Entry(RepresentativeAgreement).State = EntityState.Modified;


            if (!entity.ExhibitorId.IsDefault())
                RepresentativeAgreement.ExhibitorId = entity.ExhibitorId;

            if (!entity.Agreement.IsDefault())
                RepresentativeAgreement.Agreement = entity.Agreement;

            if (!entity.SignedAgreement.IsDefault())
                RepresentativeAgreement.SignedAgreement = entity.SignedAgreement;

            if (!entity.ModifiedOn.IsDefault())
                RepresentativeAgreement.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiRepresentativeAgreement entity)
        {
            foreach (XsiRepresentativeAgreement RepresentativeAgreement in Select(entity))
            {
                XsiRepresentativeAgreement aRepresentativeAgreement = XsiContext.Context.XsiRepresentativeAgreement.Find(RepresentativeAgreement.ItemId);
                XsiContext.Context.XsiRepresentativeAgreement.Remove(aRepresentativeAgreement);
            }
        }
        public void Delete(long itemId)
        {
            XsiRepresentativeAgreement aRepresentativeAgreement = XsiContext.Context.XsiRepresentativeAgreement.Find(itemId);
            XsiContext.Context.XsiRepresentativeAgreement.Remove(aRepresentativeAgreement);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
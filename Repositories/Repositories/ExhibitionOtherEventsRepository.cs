﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;
using System.Dynamic;

namespace Xsi.Repositories
{
    public class ExhibitionOtherEventsRepository : IExhibitionOtherEventsRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionOtherEventsRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionOtherEventsRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionOtherEventsRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionOtherEvents GetById(long itemId)
        {
            //if (CachingRepository.CachedExhibitionOtherEvents == null)
            //    CachingRepository.CachedExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.ToList();
            //return CachingRepository.CachedExhibitionOtherEvents.AsQueryable().Where(i => i.ExhibitionId == itemId).FirstOrDefault();

            return XsiContext.Context.XsiExhibitionOtherEvents.Find(itemId);
        }

        public List<XsiExhibitionOtherEvents> Select()
        {
            //if (CachingRepository.CachedExhibitionOtherEvents == null)
            //    CachingRepository.CachedExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.ToList();
            //return CachingRepository.CachedExhibitionOtherEvents;

            return XsiContext.Context.XsiExhibitionOtherEvents.ToList();
        }
        public List<ExhibitionDashboard> SelectDashBoard()
        {
            string strsqlquery = @"SELECT y.ExhibitionId ,e.WebsiteId, e.ExhibitionYear, COUNT(y.ExhibitionId) as TotalRegistrationCount
,(select count(innertable.ExhibitionId) from XsiExhibitionOtherEventMemberApplicationYearly innertable where ExhibitionId=y.ExhibitionId and (innertable.Status='I' or innertable.Status='E' or innertable.Status='A')) as TotalApprovedCount 
,(select count(innertable.ExhibitionId) from XsiExhibitionOtherEventMemberApplicationYearly innertable where MemberRoleId=1 and ExhibitionId=y.ExhibitionId and (innertable.Status='I' or innertable.Status='A')) as ExhibitorApprovedCount 
,(select count(innertable.ExhibitionId) from XsiExhibitionOtherEventMemberApplicationYearly innertable where MemberRoleId=2 and ExhibitionId=y.ExhibitionId and (innertable.Status='E' or innertable.Status='A')) as AgencyApprovedCount 
,(select count(innertable.ExhibitionId) from XsiExhibitionOtherEventMemberApplicationYearly innertable where MemberRoleId=8 and ExhibitionId=y.ExhibitionId and (innertable.Status='I' or innertable.Status='A')) as ResaurantApprovedCount 

,(select count(innertable.ExhibitionId) from XsiExhibitionOtherEventMemberApplicationYearly innertable where MemberRoleId=1 and ExhibitionId=y.ExhibitionId and (innertable.Status!='I' and innertable.Status!='A')) as ExhibitorOtherCount 
,(select count(innertable.ExhibitionId) from XsiExhibitionOtherEventMemberApplicationYearly innertable where MemberRoleId=2 and ExhibitionId=y.ExhibitionId and (innertable.Status!='E' and innertable.Status!='A')) as AgencyOtherCount 
,(select count(innertable.ExhibitionId) from XsiExhibitionOtherEventMemberApplicationYearly innertable where MemberRoleId=8 and ExhibitionId=y.ExhibitionId and (innertable.Status!='I' and innertable.Status!='A')) as RestaurantOtherCount 

FROM XsiExhibitionOtherEvents e JOIN XsiExhibitionOtherEventMemberApplicationYearly y  
on e.ExhibitionId=y.ExhibitionId
group by y.ExhibitionId,e.WebsiteId,e.ExhibitionYear order by ExhibitionId desc";
            // return XsiContext.Context.Database.SqlQuery<ExhibitionDashboard>(strsqlquery).ToList();
            return XsiContext.Context.ExhibitionDashboard.FromSql(strsqlquery).ToList();
        }
        public List<XsiExhibitionOtherEvents> Select(XsiExhibitionOtherEvents entity)
        {
            //if (CachingRepository.CachedExhibitionOtherEvents == null)
            //    CachingRepository.CachedExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.ToList();

            var predicate = PredicateBuilder.True<XsiExhibitionOtherEvents>();
            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            //if (!entity.ActivityIds.IsDefault())
            //    predicate = predicate.And(p => p.ActivityIds.Equals(entity.ActivityIds));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.IsArchive.IsDefault())
                predicate = predicate.And(p => p.IsArchive.Equals(entity.IsArchive));

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.ExhibitionYear.IsDefault())
                predicate = predicate.And(p => p.ExhibitionYear == entity.ExhibitionYear);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.OverviewAr.IsDefault())
                predicate = predicate.And(p => p.OverviewAr.Contains(entity.OverviewAr));

            if (!entity.DescriptionAr.IsDefault())
                predicate = predicate.And(p => p.DescriptionAr.Contains(entity.DescriptionAr));

            if (!entity.ExhibitionNumber.IsDefault())
            {
                if (entity.ExhibitionNumber == 0)
                    entity.ExhibitionNumber = null;
                predicate = predicate.And(p => p.ExhibitionNumber.Equals(entity.ExhibitionNumber));
            }
            if (!entity.TopContent.IsDefault())
                predicate = predicate.And(p => p.TopContent.Contains(entity.TopContent));

            if (!entity.StartDate.IsDefault())
                predicate = predicate.And(p => p.StartDate == entity.StartDate);

            if (!entity.EndDate.IsDefault())
                predicate = predicate.And(p => p.EndDate == entity.EndDate);

            if (!entity.Penalty.IsDefault())
                predicate = predicate.And(p => p.Penalty.Equals(entity.Penalty));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Equals(entity.FileName));

            if (!entity.FileNameAr.IsDefault())
                predicate = predicate.And(p => p.FileNameAr.Equals(entity.FileNameAr));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Equals(entity.Url));

            if (!entity.Urlar.IsDefault())
                predicate = predicate.And(p => p.Urlar.Equals(entity.Urlar));

            if (!entity.TotalArea.IsDefault())
                predicate = predicate.And(p => p.TotalArea.Equals(entity.TotalArea));

            if (!entity.StaffGuestStartDate.IsDefault())
                predicate = predicate.And(p => p.StaffGuestStartDate == entity.StaffGuestStartDate);

            if (!entity.StaffGuestEndDate.IsDefault())
                predicate = predicate.And(p => p.StaffGuestEndDate == entity.StaffGuestEndDate);

            if (!entity.StaffGuestBookingExpiryDate.IsDefault())
                predicate = predicate.And(p => p.StaffGuestBookingExpiryDate == entity.StaffGuestBookingExpiryDate);

            if (!entity.ParticipationFee.IsDefault())
                predicate = predicate.And(p => p.ParticipationFee == entity.ParticipationFee);

            if (!entity.PriceSqM.IsDefault())
                predicate = predicate.And(p => p.PriceSqM == entity.PriceSqM);

            if (!entity.VisaProcessingPrice.IsDefault())
                predicate = predicate.And(p => p.VisaProcessingPrice == entity.VisaProcessingPrice);

            if (!entity.AdvertisementRates.IsDefault())
                predicate = predicate.And(p => p.AdvertisementRates == entity.AdvertisementRates);

            if (!entity.ImportantInformation.IsDefault())
                predicate = predicate.And(p => p.ImportantInformation == entity.ImportantInformation);

            if (!entity.ExpiryMonths.IsDefault())
                predicate = predicate.And(p => p.ExpiryMonths.Equals(entity.ExpiryMonths));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            //  return CachingRepository.CachedExhibitionOtherEvents.AsQueryable().Where(predicate).ToList();
            return XsiContext.Context.XsiExhibitionOtherEvents.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionOtherEvents Add(XsiExhibitionOtherEvents entity)
        {
            //if (CachingRepository.CachedExhibitionOtherEvents == null)
            //    CachingRepository.CachedExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.ToList();

            XsiExhibitionOtherEvents ExhibitionOtherEvents = new XsiExhibitionOtherEvents();

            //ExhibitionOtherEvents.ActivityIds = entity.ActivityIds;

            ExhibitionOtherEvents.IsActive = entity.IsActive;
            ExhibitionOtherEvents.IsArchive = entity.IsArchive;
            ExhibitionOtherEvents.WebsiteId = entity.WebsiteId;
            if (!entity.ExhibitionYear.IsDefault())
                ExhibitionOtherEvents.ExhibitionYear = entity.ExhibitionYear;
            if (!entity.ExhibitionNumber.IsDefault())
                ExhibitionOtherEvents.ExhibitionNumber = entity.ExhibitionNumber;
           
            if (!entity.Title.IsDefault())
                ExhibitionOtherEvents.Title = entity.Title.Trim();
            if (!entity.Overview.IsDefault())
                ExhibitionOtherEvents.Overview = entity.Overview.Trim();
            if (!entity.Description.IsDefault())
                ExhibitionOtherEvents.Description = entity.Description.Trim();

            if (!entity.TitleAr.IsDefault())
                ExhibitionOtherEvents.TitleAr = entity.TitleAr.Trim();
            if (!entity.OverviewAr.IsDefault())
                ExhibitionOtherEvents.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.DescriptionAr.IsDefault())
                ExhibitionOtherEvents.DescriptionAr = entity.DescriptionAr.Trim();

            if (!entity.TopContent.IsDefault())
                ExhibitionOtherEvents.TopContent = entity.TopContent.Trim();

            if (!entity.StartDate.IsDefault())
                ExhibitionOtherEvents.StartDate = entity.StartDate;
            if (!entity.EndDate.IsDefault())
                ExhibitionOtherEvents.EndDate = entity.EndDate;
           
            ExhibitionOtherEvents.StaffGuestStartDate = entity.StaffGuestStartDate;
            ExhibitionOtherEvents.StaffGuestEndDate = entity.StaffGuestEndDate;
            ExhibitionOtherEvents.StaffGuestBookingExpiryDate = entity.StaffGuestBookingExpiryDate;
            if (!entity.ParticipationFee.IsDefault())
                ExhibitionOtherEvents.ParticipationFee = entity.ParticipationFee;
            if (!entity.PriceSqM.IsDefault())
                ExhibitionOtherEvents.PriceSqM = entity.PriceSqM;
          
            if (!entity.Penalty.IsDefault())
                ExhibitionOtherEvents.Penalty = entity.Penalty;

            if (!entity.TotalArea.IsDefault())
                ExhibitionOtherEvents.TotalArea = entity.TotalArea.Trim();
            if (!entity.VisaProcessingPrice.IsDefault())
                ExhibitionOtherEvents.VisaProcessingPrice = entity.VisaProcessingPrice;
            if (!entity.AdvertisementRates.IsDefault())
                ExhibitionOtherEvents.AdvertisementRates = entity.AdvertisementRates;
            if (!entity.ImportantInformation.IsDefault())
                ExhibitionOtherEvents.ImportantInformation = entity.ImportantInformation;
            if (!entity.ExpiryMonths.IsDefault())
                ExhibitionOtherEvents.ExpiryMonths = entity.ExpiryMonths;
            //if (!entity.ExhibitorStep1.IsDefault())
            //    ExhibitionOtherEvents.ExhibitorStep1 = entity.ExhibitorStep1.Trim();
            //if (!entity.ExhibitorStep2.IsDefault())
            //    ExhibitionOtherEvents.ExhibitorStep2 = entity.ExhibitorStep2.Trim();
            //if (!entity.ExhibitorStep3.IsDefault())
            //    ExhibitionOtherEvents.ExhibitorStep3 = entity.ExhibitorStep3.Trim();
            //if (!entity.ExhibitorStep4.IsDefault())
            //    ExhibitionOtherEvents.ExhibitorStep4 = entity.ExhibitorStep4.Trim();
            //if (!entity.ExhibitorStep5.IsDefault())
            //    ExhibitionOtherEvents.ExhibitorStep5 = entity.ExhibitorStep5.Trim();
            //if (!entity.ExhibitorStep6.IsDefault())
            //    ExhibitionOtherEvents.ExhibitorStep6 = entity.ExhibitorStep6.Trim();
            //if (!entity.AgencyStep1.IsDefault())
            //    ExhibitionOtherEvents.AgencyStep1 = entity.AgencyStep1.Trim();
            //if (!entity.AgencyStep2.IsDefault())
            //    ExhibitionOtherEvents.AgencyStep2 = entity.AgencyStep2.Trim();
            //if (!entity.AgencyStep3.IsDefault())
            //    ExhibitionOtherEvents.AgencyStep3 = entity.AgencyStep3.Trim();
            //if (!entity.AgencyStep4.IsDefault())
            //    ExhibitionOtherEvents.AgencyStep4 = entity.AgencyStep4.Trim();

            if (!entity.Url.IsDefault())
                ExhibitionOtherEvents.Url = entity.Url.Trim();
            ExhibitionOtherEvents.FileName = entity.FileName;

            #region Ar
            ExhibitionOtherEvents.IsActiveAr = entity.IsActiveAr;
            ExhibitionOtherEvents.FileNameAr = entity.FileNameAr;

            if (!entity.Urlar.IsDefault())
                ExhibitionOtherEvents.Urlar = entity.Urlar.Trim();
            #endregion Ar
            ExhibitionOtherEvents.CreatedOn = entity.CreatedOn;
            ExhibitionOtherEvents.CreatedBy = entity.CreatedBy;
            ExhibitionOtherEvents.ModifiedOn = entity.ModifiedOn;
            ExhibitionOtherEvents.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionOtherEvents.Add(ExhibitionOtherEvents);
            SubmitChanges();

            //  CachingRepository.CachedExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.ToList();
            //  CachingRepository.CachedExhibitionOtherEvents.Add(ExhibitionOtherEvents);
            return ExhibitionOtherEvents;
        }
        public void Update(XsiExhibitionOtherEvents entity)
        {
            //if (CachingRepository.CachedExhibitionOtherEvents == null)
            //    CachingRepository.CachedExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.ToList();

            XsiExhibitionOtherEvents ExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.Find(entity.ExhibitionId);
            XsiContext.Context.Entry(ExhibitionOtherEvents).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                ExhibitionOtherEvents.IsActive = entity.IsActive;

            if (!entity.Url.IsDefault())
                ExhibitionOtherEvents.Url = entity.Url.Trim();

            if (!entity.FileName.IsDefault())
                ExhibitionOtherEvents.FileName = entity.FileName;

            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                ExhibitionOtherEvents.IsActiveAr = entity.IsActiveAr;
            if (!entity.FileNameAr.IsDefault())
                ExhibitionOtherEvents.FileNameAr = entity.FileNameAr;
            if (!entity.Urlar.IsDefault())
                ExhibitionOtherEvents.Urlar = entity.Urlar.Trim();
            #endregion Ar

            if (!entity.IsArchive.IsDefault())
                ExhibitionOtherEvents.IsArchive = entity.IsArchive;

            //if (!entity.ActivityIds.IsDefault())
            //    ExhibitionOtherEvents.ActivityIds = entity.ActivityIds;

            if (!entity.WebsiteId.IsDefault())
                ExhibitionOtherEvents.WebsiteId = entity.WebsiteId;

            if (!entity.ExhibitionYear.IsDefault())
                ExhibitionOtherEvents.ExhibitionYear = entity.ExhibitionYear;

            if (!entity.ExhibitionNumber.IsDefault())
                ExhibitionOtherEvents.ExhibitionNumber = entity.ExhibitionNumber;

            if (!entity.Title.IsDefault())
                ExhibitionOtherEvents.Title = entity.Title.Trim();

            if (!entity.Overview.IsDefault())
                ExhibitionOtherEvents.Overview = entity.Overview.Trim();

            if (!entity.Description.IsDefault())
                ExhibitionOtherEvents.Description = entity.Description.Trim();

            if (!entity.TitleAr.IsDefault())
                ExhibitionOtherEvents.TitleAr = entity.TitleAr.Trim();

            if (!entity.OverviewAr.IsDefault())
                ExhibitionOtherEvents.OverviewAr = entity.OverviewAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                ExhibitionOtherEvents.DescriptionAr = entity.DescriptionAr.Trim();

            if (!entity.TopContent.IsDefault())
                ExhibitionOtherEvents.TopContent = entity.TopContent.Trim();

            if (!entity.StartDate.IsDefault())
                ExhibitionOtherEvents.StartDate = entity.StartDate;

            if (!entity.EndDate.IsDefault())
                ExhibitionOtherEvents.EndDate = entity.EndDate;
          
            if (!entity.Penalty.IsDefault())
                ExhibitionOtherEvents.Penalty = entity.Penalty;

            if (!entity.FileName.IsDefault())
                ExhibitionOtherEvents.FileName = entity.FileName;

            if (!entity.Url.IsDefault())
                ExhibitionOtherEvents.Url = entity.Url.Trim();

            if (!entity.TotalArea.IsDefault())
                ExhibitionOtherEvents.TotalArea = entity.TotalArea.Trim();

            if (!entity.VisaProcessingPrice.IsDefault())
                ExhibitionOtherEvents.VisaProcessingPrice = entity.VisaProcessingPrice;

            if (!entity.StaffGuestStartDate.IsDefault())
                ExhibitionOtherEvents.StaffGuestStartDate = entity.StaffGuestStartDate;

            if (!entity.StaffGuestEndDate.IsDefault())
                ExhibitionOtherEvents.StaffGuestEndDate = entity.StaffGuestEndDate;

            if (!entity.StaffGuestBookingExpiryDate.IsDefault())
                ExhibitionOtherEvents.StaffGuestBookingExpiryDate = entity.StaffGuestBookingExpiryDate;

            if (!entity.ParticipationFee.IsDefault())
                ExhibitionOtherEvents.ParticipationFee = entity.ParticipationFee;

            if (!entity.PriceSqM.IsDefault())
                ExhibitionOtherEvents.PriceSqM = entity.PriceSqM;

            if (!entity.VisaProcessingPrice.IsDefault())
                ExhibitionOtherEvents.VisaProcessingPrice = entity.VisaProcessingPrice;

            if (!entity.AdvertisementRates.IsDefault())
                ExhibitionOtherEvents.AdvertisementRates = entity.AdvertisementRates;

            if (!entity.ImportantInformation.IsDefault())
                ExhibitionOtherEvents.ImportantInformation = entity.ImportantInformation;

            if (!entity.ExpiryMonths.IsDefault())
                ExhibitionOtherEvents.ExpiryMonths = entity.ExpiryMonths;

            //if (!entity.ExhibitorStep1.IsDefault())
            //    ExhibitionOtherEvents.ExhibitorStep1 = entity.ExhibitorStep1;

            //if (!entity.ExhibitorStep2.IsDefault())
            //    ExhibitionOtherEvents.ExhibitorStep2 = entity.ExhibitorStep2.Trim();

            //if (!entity.ExhibitorStep3.IsDefault())
            //    ExhibitionOtherEvents.ExhibitorStep3 = entity.ExhibitorStep3.Trim();

            //if (!entity.ExhibitorStep4.IsDefault())
            //    ExhibitionOtherEvents.ExhibitorStep4 = entity.ExhibitorStep4.Trim();

            //if (!entity.ExhibitorStep5.IsDefault())
            //    ExhibitionOtherEvents.ExhibitorStep5 = entity.ExhibitorStep5.Trim();

            //if (!entity.ExhibitorStep6.IsDefault())
            //    ExhibitionOtherEvents.ExhibitorStep6 = entity.ExhibitorStep6.Trim();

            //if (!entity.AgencyStep1.IsDefault())
            //    ExhibitionOtherEvents.AgencyStep1 = entity.AgencyStep1.Trim();

            //if (!entity.AgencyStep2.IsDefault())
            //    ExhibitionOtherEvents.AgencyStep2 = entity.AgencyStep2.Trim();

            //if (!entity.AgencyStep3.IsDefault())
            //    ExhibitionOtherEvents.AgencyStep3 = entity.AgencyStep3.Trim();

            //if (!entity.AgencyStep4.IsDefault())
            //    ExhibitionOtherEvents.AgencyStep4 = entity.AgencyStep4.Trim();

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionOtherEvents.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionOtherEvents.ModifiedBy = entity.ModifiedBy;

            // CachingRepository.CachedExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.ToList();
            //CachingRepository.CachedExhibitionOtherEvents.Remove(ExhibitionOtherEvents);
            //CachingRepository.CachedExhibitionOtherEvents.Add(ExhibitionOtherEvents);
        }
        public void Delete(XsiExhibitionOtherEvents entity)
        {
            //if (CachingRepository.CachedExhibitionOtherEvents == null)
            //    CachingRepository.CachedExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.ToList();

            foreach (XsiExhibitionOtherEvents ExhibitionOtherEvents in Select(entity))
            {
                XsiExhibitionOtherEvents aExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.Find(ExhibitionOtherEvents.ExhibitionId);
                if (aExhibitionOtherEvents != null)
                    XsiContext.Context.XsiExhibitionOtherEvents.Remove(aExhibitionOtherEvents);
            }

            // CachingRepository.CachedExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.ToList();
        }
        public void Delete(long itemId)
        {
            //if (CachingRepository.CachedExhibitionOtherEvents == null)
            //    CachingRepository.CachedExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.ToList();

            XsiExhibitionOtherEvents aExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.Find(itemId);
            if (aExhibitionOtherEvents != null)
                XsiContext.Context.XsiExhibitionOtherEvents.Remove(aExhibitionOtherEvents);
            // CachingRepository.CachedExhibitionOtherEvents.Remove(aExhibitionOtherEvents);

            //  CachingRepository.CachedExhibitionOtherEvents = XsiContext.Context.XsiExhibitionOtherEvents.ToList();
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
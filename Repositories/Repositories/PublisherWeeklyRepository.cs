﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PublisherWeeklyRepository : IPublisherWeeklyRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PublisherWeeklyRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PublisherWeeklyRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PublisherWeeklyRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiPublisherWeekly GetById(long itemId)
        {
            return XsiContext.Context.XsiPublisherWeekly.Find(itemId);
        }
        
        public List<XsiPublisherWeekly> Select()
        {
            return XsiContext.Context.XsiPublisherWeekly.ToList();
        }
        public List<XsiPublisherWeekly> SelectOr(XsiPublisherWeekly entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiPublisherWeekly>();
            var Outer = PredicateBuilder.True<XsiPublisherWeekly>();

            if (!entity.NameAr.IsDefault())
            {
                string strSearchText = entity.NameAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.NameAr.Contains(searchKeys.str1) || p.NameAr.Contains(searchKeys.str2) || p.NameAr.Contains(searchKeys.str3) || p.NameAr.Contains(searchKeys.str4) || p.NameAr.Contains(searchKeys.str5)
                    || p.NameAr.Contains(searchKeys.str6) || p.NameAr.Contains(searchKeys.str7) || p.NameAr.Contains(searchKeys.str8) || p.NameAr.Contains(searchKeys.str9) || p.NameAr.Contains(searchKeys.str10)
                    || p.NameAr.Contains(searchKeys.str11) || p.NameAr.Contains(searchKeys.str12) || p.NameAr.Contains(searchKeys.str13)
                    || p.NameAr.Contains(searchKeys.str14) || p.NameAr.Contains(searchKeys.str15) || p.NameAr.Contains(searchKeys.str16) || p.NameAr.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Name.IsDefault())
                {
                    string strSearchText = entity.Name.ToLower();
                    Inner = Inner.Or(p => p.Name.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.WebsiteId.IsDefault())
                Outer = Outer.And(p => p.WebsiteId.Equals(entity.WebsiteId));

            if (!entity.Category.IsDefault())
                Outer = Outer.And(p => p.Category.Equals(entity.Category));

            if (!entity.IssueNumber.IsDefault())
                Outer = Outer.And(p => p.IssueNumber.Equals(entity.IssueNumber));

            if (!entity.PublisherDate.IsDefault())
                Outer = Outer.And(p => p.PublisherDate == entity.PublisherDate);

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CategoryAr.IsDefault())
                Outer = Outer.And(p => p.CategoryAr.Equals(entity.CategoryAr));

            if (!entity.IssueNumberAr.IsDefault())
                Outer = Outer.And(p => p.IssueNumberAr.Equals(entity.IssueNumberAr));

            if (!entity.PublisherDateAr.IsDefault())
                Outer = Outer.And(p => p.PublisherDateAr == entity.PublisherDateAr);

            if (!entity.IsActiveAr.IsDefault())
                Outer = Outer.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiPublisherWeekly.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiPublisherWeekly> Select(XsiPublisherWeekly entity)
        {
            var predicate = PredicateBuilder.True<XsiPublisherWeekly>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.Name.IsDefault())
                predicate = predicate.And(p => p.Name.Contains(entity.Name));

            if (!entity.Category.IsDefault())
                predicate = predicate.And(p => p.Category.Equals(entity.Category));

            if (!entity.IssueNumber.IsDefault())
                predicate = predicate.And(p => p.IssueNumber.Equals(entity.IssueNumber));

            if (!entity.PublisherDate.IsDefault())
                predicate = predicate.And(p => p.PublisherDate==entity.PublisherDate);

            if (!entity.PdffileName.IsDefault())
                predicate = predicate.And(p => p.PdffileName.Equals(entity.PdffileName));

            if (!entity.MagazineCover.IsDefault())
                predicate = predicate.And(p => p.MagazineCover.Equals(entity.MagazineCover));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.NameAr.IsDefault())
                predicate = predicate.And(p => p.NameAr.Contains(entity.NameAr));

            if (!entity.CategoryAr.IsDefault())
                predicate = predicate.And(p => p.CategoryAr.Equals(entity.CategoryAr));

            if (!entity.IssueNumberAr.IsDefault())
                predicate = predicate.And(p => p.IssueNumberAr.Equals(entity.IssueNumberAr));

            if (!entity.PublisherDateAr.IsDefault())
                predicate = predicate.And(p => p.PublisherDateAr == entity.PublisherDateAr);

            if (!entity.PdffileNameAr.IsDefault())
                predicate = predicate.And(p => p.PdffileNameAr.Equals(entity.PdffileNameAr));

            if (!entity.MagazineCoverAr.IsDefault())
                predicate = predicate.And(p => p.MagazineCoverAr.Equals(entity.MagazineCoverAr));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiPublisherWeekly.AsExpandable().Where(predicate).ToList();
        }

        public XsiPublisherWeekly Add(XsiPublisherWeekly entity)
        {
            XsiPublisherWeekly PublisherWeekly = new XsiPublisherWeekly();
           
            if (!entity.Name.IsDefault())
                PublisherWeekly.Name = entity.Name.Trim();
            if (!entity.Category.IsDefault())
                PublisherWeekly.Category = entity.Category.Trim();
            if (!entity.IssueNumber.IsDefault())
                PublisherWeekly.IssueNumber = entity.IssueNumber.Trim();
            if (!entity.PdffileName.IsDefault())
                PublisherWeekly.PdffileName = entity.PdffileName.Trim();
            if (!entity.MagazineCover.IsDefault())
                PublisherWeekly.MagazineCover = entity.MagazineCover.Trim();
            if (!entity.IsActive.IsDefault())
                PublisherWeekly.IsActive = entity.IsActive;
            #region Ar
            if (!entity.NameAr.IsDefault())
                PublisherWeekly.NameAr = entity.NameAr.Trim();
            if (!entity.CategoryAr.IsDefault())
                PublisherWeekly.CategoryAr = entity.CategoryAr.Trim();
            if (!entity.IssueNumberAr.IsDefault())
                PublisherWeekly.IssueNumberAr = entity.IssueNumberAr.Trim();
            if (!entity.PdffileNameAr.IsDefault())
                PublisherWeekly.PdffileNameAr = entity.PdffileNameAr.Trim();
            if (!entity.MagazineCoverAr.IsDefault())
                PublisherWeekly.MagazineCoverAr = entity.MagazineCoverAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                PublisherWeekly.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            PublisherWeekly.CreatedOn = entity.CreatedOn;
            PublisherWeekly.CreatedBy = entity.CreatedBy;
            PublisherWeekly.ModifiedOn = entity.ModifiedOn;
            PublisherWeekly.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiPublisherWeekly.Add(PublisherWeekly);
            SubmitChanges();
            return PublisherWeekly;

        }
        public void Update(XsiPublisherWeekly entity)
        {
            XsiPublisherWeekly PublisherWeekly = XsiContext.Context.XsiPublisherWeekly.Find(entity.ItemId);
            XsiContext.Context.Entry(PublisherWeekly).State = EntityState.Modified;

            if (!entity.Category.IsDefault())
                PublisherWeekly.Category = entity.Category.Trim();

            if (!entity.Name.IsDefault())
                PublisherWeekly.Name = entity.Name.Trim();

            if (!entity.IssueNumber.IsDefault())
                PublisherWeekly.IssueNumber = entity.IssueNumber.Trim();

            if (!entity.PublisherDate.IsDefault())
                PublisherWeekly.PublisherDate = entity.PublisherDate;

            if (!entity.PdffileName.IsDefault())
                PublisherWeekly.PdffileName = entity.PdffileName.Trim();

            if (!entity.MagazineCover.IsDefault())
                PublisherWeekly.MagazineCover = entity.MagazineCover.Trim();

            if (!entity.IsActive.IsDefault())
                PublisherWeekly.IsActive = entity.IsActive;
            #region Ar
            if (!entity.CategoryAr.IsDefault())
                PublisherWeekly.CategoryAr = entity.CategoryAr.Trim();

            if (!entity.NameAr.IsDefault())
                PublisherWeekly.NameAr = entity.NameAr.Trim();

            if (!entity.IssueNumberAr.IsDefault())
                PublisherWeekly.IssueNumberAr = entity.IssueNumberAr.Trim();

            if (!entity.PublisherDateAr.IsDefault())
                PublisherWeekly.PublisherDateAr = entity.PublisherDateAr;

            if (!entity.PdffileNameAr.IsDefault())
                PublisherWeekly.PdffileNameAr = entity.PdffileNameAr.Trim();

            if (!entity.MagazineCoverAr.IsDefault())
                PublisherWeekly.MagazineCoverAr = entity.MagazineCoverAr.Trim();

            if (!entity.IsActiveAr.IsDefault())
                PublisherWeekly.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                PublisherWeekly.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                PublisherWeekly.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiPublisherWeekly entity)
        {
            foreach (XsiPublisherWeekly PublisherWeekly in Select(entity))
            {
                XsiPublisherWeekly aPublisherWeekly = XsiContext.Context.XsiPublisherWeekly.Find(PublisherWeekly.ItemId);
                XsiContext.Context.XsiPublisherWeekly.Remove(aPublisherWeekly);
            }
        }
        public void Delete(long itemId)
        {
            XsiPublisherWeekly aPublisherWeekly = XsiContext.Context.XsiPublisherWeekly.Find(itemId);
            XsiContext.Context.XsiPublisherWeekly.Remove(aPublisherWeekly);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
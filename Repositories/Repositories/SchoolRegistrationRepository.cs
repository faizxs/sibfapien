﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SchoolRegistrationRepository : ISchoolRegistrationRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SchoolRegistrationRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SchoolRegistrationRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SchoolRegistrationRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiSchoolRegistration GetById(long itemId)
        {
            return XsiContext.Context.XsiSchoolRegistration.Find(itemId);
        }
        public List<XsiSchoolRegistration> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiSchoolRegistration> Select()
        {
            return XsiContext.Context.XsiSchoolRegistration.ToList();
        }
        public List<XsiSchoolRegistration> SelectOr(XsiSchoolRegistration entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiSchoolRegistration>();
            var Outer = PredicateBuilder.True<XsiSchoolRegistration>();

            if (!entity.Name.IsDefault())
            {
                string strSearchText = entity.Name;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);
                Inner = Inner.Or(p => p.Name.Contains(searchKeys.str1) || p.Name.Contains(searchKeys.str2) || p.Name.Contains(searchKeys.str3) || p.Name.Contains(searchKeys.str4) || p.Name.Contains(searchKeys.str5)
                    || p.Name.Contains(searchKeys.str6) || p.Name.Contains(searchKeys.str7) || p.Name.Contains(searchKeys.str8) || p.Name.Contains(searchKeys.str9) || p.Name.Contains(searchKeys.str10)
                    || p.Name.Contains(searchKeys.str11) || p.Name.Contains(searchKeys.str12) || p.Name.Contains(searchKeys.str13)
                    || p.Name.Contains(searchKeys.str14) || p.Name.Contains(searchKeys.str15) || p.Name.Contains(searchKeys.str16) || p.Name.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Name.IsDefault())
                {
                    string strSearchText = entity.Name.ToLower();
                    Inner = Inner.Or(p => p.Name.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
              
            if (!entity.Email.IsDefault())
                Outer = Outer.And(p => p.Email.Equals(entity.Email));
             
            if (!entity.EventId.IsDefault())
                Outer = Outer.And(p => p.EventId == entity.EventId);

            if (!entity.WebsiteId.IsDefault())
                Outer = Outer.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.ExhibitionId.IsDefault())
                Outer = Outer.And(p => p.ExhibitionId == entity.ExhibitionId);

            if (!entity.Type.IsDefault())
                Outer = Outer.And(p => p.Type == entity.Type);

            if (!entity.PersonInCharge.IsDefault())
                Outer = Outer.And(p => p.PersonInCharge.Contains(entity.PersonInCharge));

            if (!entity.ContactNumber.IsDefault())
                Outer = Outer.And(p => p.ContactNumber.Contains(entity.ContactNumber));

            if (!entity.Emirates.IsDefault())
                Outer = Outer.And(p => p.Emirates == entity.Emirates);

            if (!entity.NumberOfChildren.IsDefault())
                Outer = Outer.And(p => p.NumberOfChildren.Contains(entity.NumberOfChildren));

            if (!entity.ChildrenAgeRange.IsDefault())
                Outer = Outer.And(p => p.ChildrenAgeRange.Contains(entity.ChildrenAgeRange));

            if (!entity.DateOfVisit.IsDefault())
                Outer = Outer.And(p => p.DateOfVisit.Equals(entity.DateOfVisit));

            if (!entity.ActivityOfInterest.IsDefault())
                Outer = Outer.And(p => p.ActivityOfInterest == entity.ActivityOfInterest);

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiSchoolRegistration.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiSchoolRegistration> Select(XsiSchoolRegistration entity)
        {
            var predicate = PredicateBuilder.True<XsiSchoolRegistration>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.EventId.IsDefault())
                predicate = predicate.And(p => p.EventId == entity.EventId);
             
            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            if (!entity.Name.IsDefault())
                predicate = predicate.And(p => p.Name.Contains(entity.Name));

            if (!entity.Type.IsDefault())
                predicate = predicate.And(p => p.Type == entity.Type);

            if (!entity.PersonInCharge.IsDefault())
                predicate = predicate.And(p => p.PersonInCharge.Contains(entity.PersonInCharge));

            if (!entity.ContactNumber.IsDefault())
                predicate = predicate.And(p => p.ContactNumber.Contains(entity.ContactNumber));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Equals(entity.Email));

            if (!entity.Emirates.IsDefault())
                predicate = predicate.And(p => p.Emirates == entity.Emirates);

            if (!entity.NumberOfChildren.IsDefault())
                predicate = predicate.And(p => p.NumberOfChildren.Contains(entity.NumberOfChildren));

            if (!entity.ChildrenAgeRange.IsDefault())
                predicate = predicate.And(p => p.ChildrenAgeRange.Contains(entity.ChildrenAgeRange));

            if (!entity.DateOfVisit.IsDefault())
                predicate = predicate.And(p => p.DateOfVisit.Equals(entity.DateOfVisit));

            if (!entity.ActivityOfInterest.IsDefault())
                predicate = predicate.And(p => p.ActivityOfInterest == entity.ActivityOfInterest);

            return XsiContext.Context.XsiSchoolRegistration.AsExpandable().Where(predicate).ToList();
        }

        public XsiSchoolRegistration Add(XsiSchoolRegistration entity)
        {
            XsiSchoolRegistration SchoolRegistration = new XsiSchoolRegistration();
             
            if (!entity.Name.IsDefault())
                SchoolRegistration.Name = entity.Name.Trim();
            SchoolRegistration.WebsiteId = entity.WebsiteId;
            SchoolRegistration.ExhibitionId = entity.ExhibitionId;
            SchoolRegistration.EventId = entity.EventId;
            SchoolRegistration.Type = entity.Type;
            if (!entity.PersonInCharge.IsDefault())
                SchoolRegistration.PersonInCharge = entity.PersonInCharge.Trim();
            if (!entity.ContactNumber.IsDefault())
                SchoolRegistration.ContactNumber = entity.ContactNumber.Trim();
            if (!entity.Email.IsDefault())
                SchoolRegistration.Email = entity.Email.Trim();
            SchoolRegistration.Emirates = entity.Emirates;
            if (!entity.NumberOfChildren.IsDefault())
                SchoolRegistration.NumberOfChildren = entity.NumberOfChildren.Trim();
            if (!entity.ChildrenAgeRange.IsDefault())
                SchoolRegistration.ChildrenAgeRange = entity.ChildrenAgeRange.Trim();
            SchoolRegistration.DateOfVisit = entity.DateOfVisit;
            if (!entity.ActivityOfInterest.IsDefault())
                SchoolRegistration.ActivityOfInterest = entity.ActivityOfInterest.Trim();
            SchoolRegistration.CreatedOn = entity.CreatedOn;
            SchoolRegistration.ModifedOn = entity.ModifedOn;
            SchoolRegistration.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiSchoolRegistration.Add(SchoolRegistration);
            SubmitChanges();
            return SchoolRegistration;

        }
        public void Update(XsiSchoolRegistration entity)
        {
            XsiSchoolRegistration SchoolRegistration = XsiContext.Context.XsiSchoolRegistration.Find(entity.ItemId);
            XsiContext.Context.Entry(SchoolRegistration).State = EntityState.Modified;
             
            if (!entity.WebsiteId.IsDefault())
                SchoolRegistration.WebsiteId = entity.WebsiteId;

            if (!entity.ExhibitionId.IsDefault())
                SchoolRegistration.ExhibitionId = entity.ExhibitionId;

            if (!entity.EventId.IsDefault())
                SchoolRegistration.EventId = entity.EventId;

            if (!entity.Name.IsDefault())
                SchoolRegistration.Name = entity.Name.Trim();

            if (!entity.Type.IsDefault())
                SchoolRegistration.Type = entity.Type;

            if (!entity.PersonInCharge.IsDefault())
                SchoolRegistration.PersonInCharge = entity.PersonInCharge.Trim();

            if (!entity.ContactNumber.IsDefault())
                SchoolRegistration.ContactNumber = entity.ContactNumber.Trim();

            if (!entity.Email.IsDefault())
                SchoolRegistration.Email = entity.Email.Trim();

            if (!entity.Emirates.IsDefault())
                SchoolRegistration.Emirates = entity.Emirates;

            if (!entity.NumberOfChildren.IsDefault())
                SchoolRegistration.NumberOfChildren = entity.NumberOfChildren.Trim();

            if (!entity.ChildrenAgeRange.IsDefault())
                SchoolRegistration.ChildrenAgeRange = entity.ChildrenAgeRange.Trim();

            if (!entity.DateOfVisit.IsDefault())
                SchoolRegistration.DateOfVisit = entity.DateOfVisit;

            if (!entity.ActivityOfInterest.IsDefault())
                SchoolRegistration.ActivityOfInterest = entity.ActivityOfInterest.Trim();

            if (!entity.ModifedOn.IsDefault())
                SchoolRegistration.ModifedOn = entity.ModifedOn;

            if (!entity.ModifiedBy.IsDefault())
                SchoolRegistration.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiSchoolRegistration entity)
        {
            foreach (XsiSchoolRegistration SchoolRegistration in Select(entity))
            {
                XsiSchoolRegistration aSchoolRegistration = XsiContext.Context.XsiSchoolRegistration.Find(SchoolRegistration.ItemId);
                XsiContext.Context.XsiSchoolRegistration.Remove(aSchoolRegistration);
            }
        }
        public void Delete(long itemId)
        {
            XsiSchoolRegistration aSchoolRegistration = XsiContext.Context.XsiSchoolRegistration.Find(itemId);
            XsiContext.Context.XsiSchoolRegistration.Remove(aSchoolRegistration);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
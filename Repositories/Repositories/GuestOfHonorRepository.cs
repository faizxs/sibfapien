﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class GuestOfHonorRepository : IGuestOfHonorRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public GuestOfHonorRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public GuestOfHonorRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public GuestOfHonorRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiGuestOfHonor GetById(long itemId)
        {
            return XsiContext.Context.XsiGuestOfHonor.Find(itemId);
        }

        public List<XsiGuestOfHonor> Select()
        {
            return XsiContext.Context.XsiGuestOfHonor.ToList();
        }
        public List<XsiGuestOfHonor> Select(XsiGuestOfHonor entity)
        {
            var predicate = PredicateBuilder.True<XsiGuestOfHonor>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Name.IsDefault())
                predicate = predicate.And(p => p.Name.Contains(entity.Name));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.SubTitle.IsDefault())
                predicate = predicate.And(p => p.SubTitle.Contains(entity.SubTitle));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Contains(entity.ImageName));

            if (!entity.FlagImageName.IsDefault())
                predicate = predicate.And(p => p.FlagImageName.Contains(entity.FlagImageName));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsCentreBanner.IsDefault())
                predicate = predicate.And(p => p.IsCentreBanner.Equals(entity.IsCentreBanner));

            return XsiContext.Context.XsiGuestOfHonor.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiGuestOfHonor> SelectOr(XsiGuestOfHonor entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiGuestOfHonor>();
            var Outer = PredicateBuilder.True<XsiGuestOfHonor>();

            if (!entity.Name.IsDefault())
            {
                string strSearchText = entity.Name;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Name.Contains(searchKeys.str1) || p.Name.Contains(searchKeys.str2) || p.Name.Contains(searchKeys.str3) || p.Name.Contains(searchKeys.str4) || p.Name.Contains(searchKeys.str5)
                     || p.Name.Contains(searchKeys.str6) || p.Name.Contains(searchKeys.str7) || p.Name.Contains(searchKeys.str8) || p.Name.Contains(searchKeys.str9) || p.Name.Contains(searchKeys.str10)
                     || p.Name.Contains(searchKeys.str11) || p.Name.Contains(searchKeys.str12) || p.Name.Contains(searchKeys.str13)
                     || p.Name.Contains(searchKeys.str14) || p.Name.Contains(searchKeys.str15) || p.Name.Contains(searchKeys.str16) || p.Name.Contains(searchKeys.str17)
                     );
                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Name.IsDefault())
                {
                    string strSearchText = entity.Name.ToLower();
                    Inner = Inner.Or(p => p.Name.ToLower().Contains(strSearchText));
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiGuestOfHonor.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public XsiGuestOfHonor Add(XsiGuestOfHonor entity)
        {
            XsiGuestOfHonor GuestOfHonor = new XsiGuestOfHonor();
            if (!entity.Name.IsDefault())
                GuestOfHonor.Name = entity.Name.Trim();
            if (!entity.Title.IsDefault())
                GuestOfHonor.Title = entity.Title.Trim();
            if (!entity.SubTitle.IsDefault())
                GuestOfHonor.SubTitle = entity.SubTitle.Trim();
            if (!entity.Url.IsDefault())
                GuestOfHonor.Url = entity.Url.Trim();
            GuestOfHonor.ImageName = entity.ImageName;
            GuestOfHonor.FlagImageName = entity.FlagImageName;
            if (!entity.Description.IsDefault())
                GuestOfHonor.Description = entity.Description.Trim();
            GuestOfHonor.IsActive = entity.IsActive;
            GuestOfHonor.IsCentreBanner = entity.IsCentreBanner;
            GuestOfHonor.CreatedOn = entity.CreatedOn;
            GuestOfHonor.CreatedBy = entity.CreatedBy;
            GuestOfHonor.ModifiedOn = entity.ModifiedOn;
            GuestOfHonor.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiGuestOfHonor.Add(GuestOfHonor);
            SubmitChanges();
            return GuestOfHonor;

        }
        public void Update(XsiGuestOfHonor entity)
        {
            XsiGuestOfHonor GuestOfHonor = XsiContext.Context.XsiGuestOfHonor.Find(entity.ItemId);
            XsiContext.Context.Entry(GuestOfHonor).State = EntityState.Modified;

            if (!entity.Name.IsDefault())
                GuestOfHonor.Name = entity.Name.Trim();
            if (!entity.Title.IsDefault())
                GuestOfHonor.Title = entity.Title.Trim();
            if (!entity.SubTitle.IsDefault())
                GuestOfHonor.SubTitle = entity.SubTitle.Trim();
            if (!entity.Url.IsDefault())
                GuestOfHonor.Url = entity.Url.Trim();
            if (!entity.ImageName.IsDefault())
                GuestOfHonor.ImageName = entity.ImageName;
            if (!entity.FlagImageName.IsDefault())
                GuestOfHonor.FlagImageName = entity.FlagImageName;
            if (!entity.Description.IsDefault())
                GuestOfHonor.Description = entity.Description.Trim();
            if (!entity.IsActive.IsDefault())
                GuestOfHonor.IsActive = entity.IsActive;
            if (!entity.IsCentreBanner.IsDefault())
                GuestOfHonor.IsCentreBanner = entity.IsCentreBanner;
            if (!entity.ModifiedOn.IsDefault())
                GuestOfHonor.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                GuestOfHonor.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiGuestOfHonor entity)
        {
            foreach (XsiGuestOfHonor GuestOfHonor in Select(entity))
            {
                XsiGuestOfHonor aGuestOfHonor = XsiContext.Context.XsiGuestOfHonor.Find(GuestOfHonor.ItemId);
                XsiContext.Context.XsiGuestOfHonor.Remove(aGuestOfHonor);
            }
        }
        public void Delete(long itemId)
        {
            XsiGuestOfHonor aGuestOfHonor = XsiContext.Context.XsiGuestOfHonor.Find(itemId);
            XsiContext.Context.XsiGuestOfHonor.Remove(aGuestOfHonor);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
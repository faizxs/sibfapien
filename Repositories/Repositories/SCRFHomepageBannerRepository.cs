﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFHomepageBannerRepository : ISCRFHomepageBannerRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFHomepageBannerRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFHomepageBannerRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFHomepageBannerRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfhomepageBanner GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfhomepageBanner.Find(itemId);
        }

        public List<XsiScrfhomepageBanner> Select()
        {
            return XsiContext.Context.XsiScrfhomepageBanner.ToList();
        }
        public List<XsiScrfhomepageBanner> Select(XsiScrfhomepageBanner entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfhomepageBanner>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Cmstitle.IsDefault())
                predicate = predicate.And(p => p.Cmstitle.Contains(entity.Cmstitle));
            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.SubTitle.IsDefault())
                predicate = predicate.And(p => p.SubTitle.Contains(entity.SubTitle));
            if (!entity.HomeBanner.IsDefault())
                predicate = predicate.And(p => p.HomeBanner.Equals(entity.HomeBanner));
            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.IsFeatured.IsDefault())
                predicate = predicate.And(p => p.IsFeatured.Equals(entity.IsFeatured));
            return XsiContext.Context.XsiScrfhomepageBanner.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfhomepageBanner Add(XsiScrfhomepageBanner entity)
        {
            XsiScrfhomepageBanner SCRFHomepageBanner = new XsiScrfhomepageBanner();
            if (!entity.Cmstitle.IsDefault())
                SCRFHomepageBanner.Cmstitle = entity.Cmstitle.Trim();
            if (!entity.Title.IsDefault())
                SCRFHomepageBanner.Title = entity.Title.Trim();
            if (!entity.SubTitle.IsDefault())
            SCRFHomepageBanner.SubTitle = entity.SubTitle.Trim();
            SCRFHomepageBanner.HomeBanner = entity.HomeBanner;
            if (!entity.Url.IsDefault())
            SCRFHomepageBanner.Url = entity.Url.Trim();
            SCRFHomepageBanner.IsActive = entity.IsActive;
            SCRFHomepageBanner.IsFeatured = entity.IsFeatured;
            SCRFHomepageBanner.SortOrder = entity.SortOrder;
            SCRFHomepageBanner.CreatedOn = entity.CreatedOn;
            SCRFHomepageBanner.CreatedBy = entity.CreatedBy;
            SCRFHomepageBanner.ModifiedOn = entity.ModifiedOn;
            SCRFHomepageBanner.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfhomepageBanner.Add(SCRFHomepageBanner);
            SubmitChanges();
            return SCRFHomepageBanner;

        }
        public void Update(XsiScrfhomepageBanner entity)
        {
            XsiScrfhomepageBanner SCRFHomepageBanner = XsiContext.Context.XsiScrfhomepageBanner.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFHomepageBanner).State = EntityState.Modified;

            if (!entity.Cmstitle.IsDefault())
                SCRFHomepageBanner.Cmstitle = entity.Cmstitle.Trim();
            if (!entity.Title.IsDefault())
                SCRFHomepageBanner.Title = entity.Title.Trim();
            if (!entity.SubTitle.IsDefault())
                SCRFHomepageBanner.SubTitle = entity.SubTitle.Trim();
            if (!entity.HomeBanner.IsDefault())
                SCRFHomepageBanner.HomeBanner = entity.HomeBanner;
            if (!entity.Url.IsDefault())
                SCRFHomepageBanner.Url = entity.Url.Trim();
            if (!entity.IsActive.IsDefault())
                SCRFHomepageBanner.IsActive = entity.IsActive;
            if (!entity.IsFeatured.IsDefault())
                SCRFHomepageBanner.IsFeatured = entity.IsFeatured;
            if (!entity.SortOrder.IsDefault())
                SCRFHomepageBanner.SortOrder = entity.SortOrder;
            if (!entity.ModifiedOn.IsDefault())
                SCRFHomepageBanner.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                SCRFHomepageBanner.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiScrfhomepageBanner entity)
        {
            foreach (XsiScrfhomepageBanner SCRFHomepageBanner in Select(entity))
            {
                XsiScrfhomepageBanner aSCRFHomepageBanner = XsiContext.Context.XsiScrfhomepageBanner.Find(SCRFHomepageBanner.ItemId);
                XsiContext.Context.XsiScrfhomepageBanner.Remove(aSCRFHomepageBanner);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfhomepageBanner aSCRFHomepageBanner = XsiContext.Context.XsiScrfhomepageBanner.Find(itemId);
            XsiContext.Context.XsiScrfhomepageBanner.Remove(aSCRFHomepageBanner);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
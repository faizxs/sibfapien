﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionMemberLogRepository : IExhibitionMemberLogRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionMemberLogRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionMemberLogRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionMemberLogRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionMemberLog GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionMemberLog.Find(itemId);
        }
        public List<XsiExhibitionMemberLog> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionMemberLog> Select()
        {
            return XsiContext.Context.XsiExhibitionMemberLog.ToList();
        }
        public List<XsiExhibitionMemberLog> Select(XsiExhibitionMemberLog entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMemberLog>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ExhibitionMemberId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionMemberId == entity.ExhibitionMemberId);

            if (!entity.AdminUserId.IsDefault())
                predicate = predicate.And(p => p.AdminUserId == entity.AdminUserId);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);


            return XsiContext.Context.XsiExhibitionMemberLog.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionMemberLog Add(XsiExhibitionMemberLog entity)
        {
            XsiExhibitionMemberLog ExhibitionMemberLog = new XsiExhibitionMemberLog();
            ExhibitionMemberLog.ExhibitionMemberId = entity.ExhibitionMemberId;
            if (!entity.AdminUserId.IsDefault())
                ExhibitionMemberLog.AdminUserId = entity.AdminUserId;
            ExhibitionMemberLog.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiExhibitionMemberLog.Add(ExhibitionMemberLog);
            SubmitChanges();
            return ExhibitionMemberLog;

        }
        public void Update(XsiExhibitionMemberLog entity)
        {
            XsiExhibitionMemberLog ExhibitionMemberLog = XsiContext.Context.XsiExhibitionMemberLog.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionMemberLog).State = EntityState.Modified;

            if (!entity.ExhibitionMemberId.IsDefault())
                ExhibitionMemberLog.ExhibitionMemberId = entity.ExhibitionMemberId;

            if (!entity.AdminUserId.IsDefault())
                ExhibitionMemberLog.AdminUserId = entity.AdminUserId;
            if (!entity.CreatedOn.IsDefault())
                ExhibitionMemberLog.CreatedOn = entity.CreatedOn;
        }
        public void Delete(XsiExhibitionMemberLog entity)
        {
            foreach (XsiExhibitionMemberLog ExhibitionMemberLog in Select(entity))
            {
                XsiExhibitionMemberLog aExhibitionMemberLog = XsiContext.Context.XsiExhibitionMemberLog.Find(ExhibitionMemberLog.ItemId);
                XsiContext.Context.XsiExhibitionMemberLog.Remove(aExhibitionMemberLog);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionMemberLog aExhibitionMemberLog = XsiContext.Context.XsiExhibitionMemberLog.Find(itemId);
            XsiContext.Context.XsiExhibitionMemberLog.Remove(aExhibitionMemberLog);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
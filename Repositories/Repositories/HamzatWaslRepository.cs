﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class HamzatWaslRepository : IHamzatWaslRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public HamzatWaslRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public HamzatWaslRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public HamzatWaslRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiHamzatWasl GetById(long itemId)
        {
            return XsiContext.Context.XsiHamzatWasl.Find(itemId);
        }

        public List<XsiHamzatWasl> Select()
        {
            return XsiContext.Context.XsiHamzatWasl.ToList();
        }
        public List<XsiHamzatWasl> Select(XsiHamzatWasl entity)
        {
            var predicate = PredicateBuilder.True<XsiHamzatWasl>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated == entity.Dated);

            if (!entity.Source.IsDefault())
                predicate = predicate.And(p => p.Source.Contains(entity.Source));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName == entity.FileName);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiHamzatWasl.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiHamzatWasl> SelectOr(XsiHamzatWasl entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiHamzatWasl>();
            var Outer = PredicateBuilder.True<XsiHamzatWasl>();

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.Dated.IsDefault())
                Outer = Outer.And(p => p.Dated == entity.Dated);

            if (!entity.Source.IsDefault())
                Outer = Outer.And(p => p.Source.Contains(entity.Source));

            if (!entity.FileName.IsDefault())
                Outer = Outer.And(p => p.FileName == entity.FileName);

            if (!entity.WebsiteId.IsDefault())
                Outer = Outer.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.Overview.IsDefault())
                Outer = Outer.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                Outer = Outer.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                Outer = Outer.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.CreatedBy.IsDefault())
                Outer = Outer.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedBy.IsDefault())
                Outer = Outer.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiHamzatWasl.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiHamzatWasl Add(XsiHamzatWasl entity)
        {
            XsiHamzatWasl HamzatWasl = new XsiHamzatWasl();
            if (!entity.Title.IsDefault())
                HamzatWasl.Title = entity.Title.Trim();
            HamzatWasl.Dated = entity.Dated;
            if (!entity.Source.IsDefault())
                HamzatWasl.Source = entity.Source.Trim();
            HamzatWasl.FileName = entity.FileName;
            if (!entity.Overview.IsDefault())
                HamzatWasl.Overview = entity.Overview.Trim();
            HamzatWasl.WebsiteId = entity.WebsiteId;
            HamzatWasl.IsActive = entity.IsActive;
            HamzatWasl.CreatedOn = entity.CreatedOn;
            HamzatWasl.CreatedBy = entity.CreatedBy;
            HamzatWasl.ModifiedOn = entity.ModifiedOn;
            HamzatWasl.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiHamzatWasl.Add(HamzatWasl);
            SubmitChanges();
            return HamzatWasl;

        }
        public void Update(XsiHamzatWasl entity)
        {
            XsiHamzatWasl HamzatWasl = XsiContext.Context.XsiHamzatWasl.Find(entity.ItemId);
            XsiContext.Context.Entry(HamzatWasl).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                HamzatWasl.Title = entity.Title.Trim();

            if (!entity.Dated.IsDefault())
                HamzatWasl.Dated = entity.Dated;

            if (!entity.Source.IsDefault())
                HamzatWasl.Source = entity.Source.Trim();

            if (!entity.FileName.IsDefault())
                HamzatWasl.FileName = entity.FileName;

            if (!entity.Overview.IsDefault())
                HamzatWasl.Overview = entity.Overview.Trim();

            if (!entity.WebsiteId.IsDefault())
                HamzatWasl.WebsiteId = entity.WebsiteId;

            if (!entity.IsActive.IsDefault())
                HamzatWasl.IsActive = entity.IsActive;

            if (!entity.ModifiedOn.IsDefault())
                HamzatWasl.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                HamzatWasl.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiHamzatWasl entity)
        {
            foreach (XsiHamzatWasl HamzatWasl in Select(entity))
            {
                XsiHamzatWasl aHamzatWasl = XsiContext.Context.XsiHamzatWasl.Find(HamzatWasl.ItemId);
                XsiContext.Context.XsiHamzatWasl.Remove(aHamzatWasl);
            }
        }
        public void Delete(long itemId)
        {
            XsiHamzatWasl aHamzatWasl = XsiContext.Context.XsiHamzatWasl.Find(itemId);
            XsiContext.Context.XsiHamzatWasl.Remove(aHamzatWasl);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Entities.Models;
using System.Web;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace Xsi.Repositories
{
    public class BookRepository : IBookRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public BookRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public BookRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public BookRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionBooks GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionBooks.Find(itemId);
        }

        public List<XsiExhibitionBooks> Select()
        {
            return XsiContext.Context.XsiExhibitionBooks.ToList();
        }
        public List<XsiExhibitionBooks> Select(XsiExhibitionBooks entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBooks>();
            if (!entity.BookId.IsDefault())
                predicate = predicate.And(p => p.BookId == entity.BookId);

            //if (!entity.AuthorId.IsDefault())
            //    predicate = predicate.And(p => p.AuthorId == entity.AuthorId);

            if (!entity.BookPublisherId.IsDefault())
                predicate = predicate.And(p => p.BookPublisherId == entity.BookPublisherId);

            //if (!entity.AgencyId.IsDefault())
            //    predicate = predicate.And(p => p.AgencyId == entity.AgencyId);

            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);


            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsEnable.IsDefault())
                predicate = predicate.And(p => p.IsEnable.Equals(entity.IsEnable));

            if (!entity.ExhibitionCurrencyId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionCurrencyId == entity.ExhibitionCurrencyId);

            if (!entity.BookLanguageId.IsDefault())
                predicate = predicate.And(p => p.BookLanguageId == entity.BookLanguageId);

            if (!entity.ExhibitionSubjectId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionSubjectId == entity.ExhibitionSubjectId);

            if (!entity.ExhibitionSubsubjectId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionSubsubjectId == entity.ExhibitionSubsubjectId);

            if (!entity.BookTypeId.IsDefault())
                predicate = predicate.And(p => p.BookTypeId == entity.BookTypeId);

            if (!entity.IsNew.IsDefault())
                predicate = predicate.And(p => p.IsNew.Equals(entity.IsNew));

            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail == entity.Thumbnail);

            if (!entity.BookDetails.IsDefault())
                predicate = predicate.And(p => p.BookDetails.Contains(entity.BookDetails));

            if (!entity.BookDetailsAr.IsDefault())
                predicate = predicate.And(p => p.BookDetailsAr.Contains(entity.BookDetailsAr));

            if (!entity.IsAvailableOnline.IsDefault())
                predicate = predicate.And(p => p.IsAvailableOnline.Equals(entity.IsAvailableOnline));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.ToLower().Contains(entity.TitleAr));

            if (!entity.TitleEn.IsDefault())
                predicate = predicate.And(p => p.TitleEn.ToLower().Contains(entity.TitleEn));

            if (!entity.Isbn.IsDefault())
                predicate = predicate.And(p => p.Isbn.Contains(entity.Isbn));

            if (!entity.IssueYear.IsDefault())
                predicate = predicate.And(p => p.IssueYear.Contains(entity.IssueYear));

            if (!entity.Price.IsDefault())
                predicate = predicate.And(p => p.Price.Contains(entity.Price));

            if (!entity.PriceBeforeDiscount.IsDefault())
                predicate = predicate.And(p => p.PriceBeforeDiscount.Contains(entity.PriceBeforeDiscount));

            if (!entity.IsBestSeller.IsDefault())
                predicate = predicate.And(p => p.IsBestSeller.Equals(entity.IsBestSeller));

            if (!entity.ClickCount.IsDefault())
                predicate = predicate.And(p => p.ClickCount == entity.ClickCount);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            return CachingRepository.CachedBooks.AsQueryable().Where(predicate).ToList();
        }

        public List<XsiExhibitionBooks> GetXsiExhibitionBooksByExhibitorId(long ehibitorId)
        {
            var exhibitionbook = XsiContext.Context.XsiExhibitionBookParticipating.Where(p => p.ExhibitorId == ehibitorId).Select(s => s.Book).ToList();

            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            return exhibitionbook;
        }

        public List<XsiExhibitionBookForUi> SelectBooksForUI(XsiExhibitionBooks entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBookParticipating>();

            if (!entity.BookLanguageId.IsDefault())
                predicate = predicate.And(p => p.Book.BookLanguageId.Equals(entity.BookLanguageId));

            if (!entity.ExhibitionCurrencyId.IsDefault())
                predicate = predicate.And(p => p.Book.ExhibitionCurrencyId.Equals(entity.ExhibitionCurrencyId));

            //if (!entity.AuthorId.IsDefault())
            //    predicate = predicate.And(p => p.Book.AuthorId == entity.AuthorId);

            if (!entity.BookPublisherId.IsDefault())
                predicate = predicate.And(p => p.Book.BookPublisherId == entity.BookPublisherId);

            //if (!entity.XsiExhibitionBookParticipating.ExhibitorId.IsDefault())
            //    predicate = predicate.And(p => p.Book.ExhibitorId == entity.ExhibitorId);

            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.Book.MemberId == entity.MemberId);


            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.Book.IsActive.Equals(entity.IsActive));

            if (!entity.IsEnable.IsDefault())
                predicate = predicate.And(p => p.Book.IsEnable.Equals(entity.IsEnable));

            if (!entity.ExhibitionSubjectId.IsDefault())
                predicate = predicate.And(p => p.Book.ExhibitionSubjectId.Equals(entity.ExhibitionSubjectId));

            if (!entity.ExhibitionSubsubjectId.IsDefault())
                predicate = predicate.And(p => p.Book.ExhibitionSubsubjectId.Equals(entity.ExhibitionSubsubjectId));

            if (!entity.BookTypeId.IsDefault())
                predicate = predicate.And(p => p.Book.BookTypeId == entity.BookTypeId);

            if (!entity.IsNew.IsDefault())
                predicate = predicate.And(p => p.Book.IsNew.Equals(entity.IsNew));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.Book.TitleAr != null && p.Book.TitleAr.Contains(entity.TitleAr));

            if (!entity.TitleEn.IsDefault())
                predicate = predicate.And(p => p.Book.TitleEn != null && p.Book.TitleEn.Contains(entity.TitleEn));

            if (!entity.Isbn.IsDefault())
                predicate = predicate.And(p => p.Book.Isbn != null && p.Book.Isbn.Contains(entity.Isbn));

            if (!entity.IssueYear.IsDefault())
                predicate = predicate.And(p => p.Book.IssueYear != null && p.Book.IssueYear.Contains(entity.IssueYear));

            if (!entity.Price.IsDefault())
                predicate = predicate.And(p => p.Book.Price != null && p.Book.Price.Contains(entity.Price));

            if (!entity.IsBestSeller.IsDefault())
                predicate = predicate.And(p => p.Book.IsBestSeller.Equals(entity.IsBestSeller));

            if (!entity.PriceBeforeDiscount.IsDefault())
                predicate = predicate.And(p => p.Book.PriceBeforeDiscount.Contains(entity.PriceBeforeDiscount));

            //if (!entity.ExhibitionGroupId.IsDefault())
            //    predicate = predicate.And(p => p.ExhibitionGroupId == entity.ExhibitionId);

            predicate = predicate.And(p => p.IsActive == "Y");

            //////////////////////////////////////////////////////////////////////////////

            //if (CachingRepository.CachedBooksParticipating == null)
            //    CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.ToList();


            //var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.ExhibitionGroupId == entity.ExhibitionGroupId && p.IsActive == "Y" && p.BookId != null && (ExhibitorIDs.Contains(p.ExhibitorId.Value) || AgencyIDs.Contains(p.AgencyId.Value))).Select(s => s.BookId.Value).Distinct());
            //if (CachingRepository.CachedBooks == null)
            //    CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //// above 1.5 items in books, 84,000 to be search in contains.
            //var SearchLists = CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.ItemId)).ToList();

            //return SearchLists.AsQueryable().Where(predicate).Select(p =>
            // new XsiExhibitionBookForUi
            // {
            //     ItemId = p.ItemId,
            //     Thumbnail = p.Thumbnail,
            //     TitleAr = p.TitleAr,
            //     TitleEn = p.TitleEn,
            //     Price = p.Price,
            //     AuthorId = p.AuthorId,
            //     IsBestSeller = p.IsBestSeller,
            //     BookTypeId = p.BookTypeId,
            //     ExhibitionCurrencyId = p.ExhibitionCurrencyId,
            // }
            // ).ToList();
            //////////////////////////////////////////////////////////////////////////////



            return XsiContext.Context.XsiExhibitionBookParticipating.Include(p => p.Book).AsExpandable().Where(predicate).Select(p =>
                new XsiExhibitionBookForUi
                {
                    BookId = p.Book.BookId,
                    Thumbnail = p.Book.Thumbnail,
                    TitleAr = p.Book.TitleAr,
                    TitleEn = p.Book.TitleEn,
                    Price = p.Book.Price,
                    //AuthorId = p.Book.,
                    BookTypeId = p.Book.BookTypeId,

                    Isbn = p.Book.Isbn,
                    IsNew = p.Book.IsNew,
                    IssueYear = p.Book.IssueYear,
                    PriceBeforeDiscount = p.PriceBeforeDiscount,
                    ExhibitionLanguageId = p.Book.BookLanguageId,
                    ExhibitionSubjectId = p.Book.ExhibitionSubjectId,
                    ExhibitionSubsubjectId = p.Book.ExhibitionSubsubjectId,
                    ExhibitionCurrencyId = p.Book.ExhibitionCurrencyId,
                    IsBestSeller = p.Book.IsBestSeller,
                    PublisherId = p.Book.BookPublisherId,
                    //ExhibitorId = p.Book.ExhibitorId,
                    //AgencyId = p.Book.AgencyId,
                }
                ).ToList();
        }
        public List<XsiExhibitionBookForUi> SelectBooksUIAnd(XsiExhibitionBooks entity, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            {
                var predicate = PredicateBuilder.True<XsiExhibitionBooks>();
                var predicateOrSubject = PredicateBuilder.False<XsiExhibitionBooks>();
                var predicateOrSubsubject = PredicateBuilder.False<XsiExhibitionBooks>();
                var predicateOrBookType = PredicateBuilder.False<XsiExhibitionBooks>();

                if (!entity.ExhibitionCurrencyId.IsDefault())
                    predicate = predicate.And(p => p.ExhibitionCurrencyId == entity.ExhibitionCurrencyId);

                if (!entity.BookPublisherId.IsDefault())
                    predicate = predicate.And(p => p.BookPublisherId == entity.BookPublisherId);

                //if (!entity.AuthorId.IsDefault())
                //    predicate = predicate.And(p => p.AuthorId == entity.AuthorId);

                //if (!entity.ExhibitorId.IsDefault())
                //    predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

                //if (!entity.AgencyId.IsDefault())
                //    predicate = predicate.And(p => p.AgencyId == entity.AgencyId);

                //if (!entity.ExhibitionGroupId.IsDefault())
                //    predicate = predicate.And(p => p.ExhibitionGroupId == entity.ExhibitionGroupId);

                if (!entity.IsActive.IsDefault())
                    predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

                if (!entity.IsEnable.IsDefault())
                    predicate = predicate.And(p => p.IsEnable.Equals(entity.IsEnable));

                if (!entity.IsBestSeller.IsDefault())
                    predicate = predicate.And(p => p.IsBestSeller.Equals(entity.IsBestSeller));

                if (!string.IsNullOrEmpty(subjectIDs))
                {
                    string[] strArray = subjectIDs.Split(',');
                    foreach (string str in strArray)
                        predicateOrSubject = predicateOrSubject.Or(p => p.ExhibitionSubjectId == Convert.ToInt64(str));
                    predicate = predicate.And(predicateOrSubject);
                }

                if (!string.IsNullOrEmpty(subsubjectIDs))
                {
                    string[] strArray = subsubjectIDs.Split(',');
                    foreach (string str in strArray)
                        predicateOrSubsubject = predicateOrSubsubject.Or(p => p.ExhibitionSubsubjectId == Convert.ToInt64(str));
                    predicate = predicate.And(predicateOrSubsubject);
                }

                if (!string.IsNullOrEmpty(bookTypeIDs))
                {
                    string[] strArray = bookTypeIDs.Split(',');
                    foreach (string str in strArray)
                        predicateOrBookType = predicateOrBookType.Or(p => p.BookTypeId == Convert.ToInt64(str));
                    predicate = predicate.And(predicateOrBookType);
                }

                if (!entity.TitleAr.IsDefault())
                    predicate = predicate.And(p => p.TitleAr != null && p.TitleAr.Contains(entity.TitleAr));

                if (!entity.TitleEn.IsDefault())
                    predicate = predicate.And(p => p.TitleEn != null && p.TitleEn.ToLower().Contains(entity.TitleEn.ToLower()));

                if (!entity.Isbn.IsDefault())
                    predicate = predicate.And(p => p.Isbn != null && p.Isbn.Contains(entity.Isbn));

                if (!entity.IssueYear.IsDefault())
                    predicate = predicate.And(p => p.IssueYear != null && p.IssueYear.Contains(entity.IssueYear));

                if (priceFrom != 0)
                    predicate = predicate.And(p => Convert.ToDouble(p.Price) >= priceFrom);

                if (priceTo != 0)
                    predicate = predicate.And(p => Convert.ToDouble(p.Price) <= priceTo);

                if (CachingRepository.CachedBooksParticipating == null)
                {
                    //XsiContext.Context.Query("select 1 A, 2 B union all select 3, 4");
                    CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.ToList();
                }
                //else
                //{
                //    var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBookParticipating");
                //    if (CachingRepository.CachedBooksParticipating.Count() != count)
                //        CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.ToList();
                //}

               // HashSet<long> ExhibitorIDs = new HashSet<long>(XsiContext.Context.GetApprovedExhibitorIds(websiteId).Select(s => s.Value));
                DataTable dt=new DataTable();
                string sql = "GetApprovedExhibitorIds";
                using (SqlConnection sqlConn = new SqlConnection("Your Connection String Here"))
                {
                    using (SqlCommand sqlCmd = new SqlCommand(sql, sqlConn))
                    {
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@WebsiteId", websiteId);
                        sqlConn.Open();
                        using (SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCmd))
                        {
                            sqlAdapter.Fill(dt);
                        }
                    }
                }
                HashSet<long> ExhibitorIDs = new HashSet<long>();
                foreach (var item in dt)
                {
                    ExhibitorIDs.Add(item);
                }


                        HashSet<long> AgencyIDs = new HashSet<long>(XsiContext.Context.GetApproveAgencyIds(websiteId).Select(s => s.Value));

                var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.IsActive == "Y" && p.BookId > 0 && (ExhibitorIDs.Contains(p.ExhibitorId.Value) || AgencyIDs.Contains(p.AgencyId.Value))).Select(s => s.BookId).Distinct());
                if (CachingRepository.CachedBooks == null)
                    CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.Take(100000).ToList();
                //else
                //{
                //    var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooks");
                //    if (CachingRepository.CachedBooks.Count() != count)
                //        CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
                //}
                // above 1.5 items in books, 84,000 to be search in contains.
                var SearchLists = CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.BookId)).ToList();

                return SearchLists.AsQueryable().Where(predicate).Select(p =>
                 new XsiExhibitionBookForUi
                 {
                     BookId = p.BookId,
                     Thumbnail = p.Thumbnail,
                     TitleAr = p.TitleAr,
                     TitleEn = p.TitleEn,
                     Price = p.Price,
                     //AuthorId = p.AuthorId,
                     PublisherId = p.BookPublisherId,
                     IsBestSeller = p.IsBestSeller,
                     BookTypeId = p.BookTypeId,
                     ExhibitionCurrencyId = p.ExhibitionCurrencyId,
                     ExhibitionSubjectId = p.ExhibitionSubjectId,
                     ExhibitionSubsubjectId = p.ExhibitionSubsubjectId,
                     Isbn = p.Isbn,
                     IssueYear = p.IssueYear,
                     //ExhibitorId = p.ExhibitorId
                 }
                 ).ToList();
            }
        }
        public List<XsiExhibitionBookForUi> SelectBooksUIAnd1(XsiExhibitionBooks entity, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            {
                var predicate = PredicateBuilder.True<XsiExhibitionBooks>();
                var predicateOrSubject = PredicateBuilder.False<XsiExhibitionBooks>();
                var predicateOrSubsubject = PredicateBuilder.False<XsiExhibitionBooks>();
                var predicateOrBookType = PredicateBuilder.False<XsiExhibitionBooks>();

                if (!entity.ExhibitionCurrencyId.IsDefault())
                    predicate = predicate.And(p => p.ExhibitionCurrencyId == entity.ExhibitionCurrencyId);

                if (!entity.BookPublisherId.IsDefault())
                    predicate = predicate.And(p => p.BookPublisherId == entity.BookPublisherId);

                //if (!entity.AuthorId.IsDefault())
                //    predicate = predicate.And(p => p.AuthorId == entity.AuthorId);

                //if (!entity.ExhibitorId.IsDefault())
                //    predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

                //if (!entity.AgencyId.IsDefault())
                //    predicate = predicate.And(p => p.AgencyId == entity.AgencyId);

                //if (!entity.ExhibitionGroupId.IsDefault())
                //    predicate = predicate.And(p => p.ExhibitionGroupId == entity.ExhibitionGroupId);

                if (!entity.IsActive.IsDefault())
                    predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

                if (!entity.IsEnable.IsDefault())
                    predicate = predicate.And(p => p.IsEnable.Equals(entity.IsEnable));

                if (!entity.IsBestSeller.IsDefault())
                    predicate = predicate.And(p => p.IsBestSeller.Equals(entity.IsBestSeller));

                if (!entity.ExhibitionSubjectId.IsDefault())
                    predicate = predicate.And(p => p.ExhibitionSubjectId.Equals(entity.ExhibitionSubjectId));

                if (!entity.ExhibitionSubsubjectId.IsDefault())
                    predicate = predicate.And(p => p.ExhibitionSubsubjectId.Equals(entity.ExhibitionSubsubjectId));

                if (!entity.BookTypeId.IsDefault())
                    predicate = predicate.And(p => p.BookTypeId.Equals(entity.BookTypeId));

                //if (!string.IsNullOrEmpty(subjectIDs))
                //{
                //    string[] strArray = subjectIDs.Split(',');
                //    foreach (string str in strArray)
                //        predicateOrSubject = predicateOrSubject.Or(p => p.ExhibitionSubjectId == Convert.ToInt64(str));
                //    predicate = predicate.And(predicateOrSubject);
                //}

                //if (!string.IsNullOrEmpty(subsubjectIDs))
                //{
                //    string[] strArray = subsubjectIDs.Split(',');
                //    foreach (string str in strArray)
                //        predicateOrSubsubject = predicateOrSubsubject.Or(p => p.ExhibitionSubsubjectId == Convert.ToInt64(str));
                //    predicate = predicate.And(predicateOrSubsubject);
                //}

                //if (!string.IsNullOrEmpty(bookTypeIDs))
                //{
                //    string[] strArray = bookTypeIDs.Split(',');
                //    foreach (string str in strArray)
                //        predicateOrBookType = predicateOrBookType.Or(p => p.BookTypeId == Convert.ToInt64(str));
                //    predicate = predicate.And(predicateOrBookType);
                //}

                if (!entity.TitleAr.IsDefault())
                    predicate = predicate.And(p => p.TitleAr != null && p.TitleAr.Contains(entity.TitleAr));

                if (!entity.TitleEn.IsDefault())
                    predicate = predicate.And(p => p.TitleEn != null && p.TitleEn.ToLower().Contains(entity.TitleEn.ToLower()));

                if (!entity.Isbn.IsDefault())
                    predicate = predicate.And(p => p.Isbn != null && p.Isbn.Contains(entity.Isbn));

                if (!entity.IssueYear.IsDefault())
                    predicate = predicate.And(p => p.IssueYear != null && p.IssueYear.Contains(entity.IssueYear));

                if (priceFrom != 0)
                    predicate = predicate.And(p => Convert.ToDouble(p.Price) >= priceFrom);

                if (priceTo != 0)
                    predicate = predicate.And(p => Convert.ToDouble(p.Price) <= priceTo);

                if (CachingRepository.CachedBooksParticipating == null)
                {

                    //XsiContext.Context.Query("select 1 A, 2 B union all select 3, 4");
                    CachingRepository.CachedBooksParticipating = db.Query<XsiExhibitionBookParticipating>("select * from XsiExhibitionBookParticipating").ToList();//XsiContext.Context.XsiExhibitionBookParticipating.ToList();
                }
                //else
                //{
                //    var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBookParticipating");
                //    if (CachingRepository.CachedBooksParticipating.Count() != count)
                //        CachingRepository.CachedBooksParticipating = db.Query<XsiExhibitionBookParticipating>("select * from XsiExhibitionBookParticipating").ToList();
                //}

                //HashSet<long> ExhibitorIDs = new HashSet<long>(XsiContext.Context.GetApprovedExhibitorIds(websiteId).Select(s => s.Value));
                //HashSet<long> AgencyIDs = new HashSet<long>(XsiContext.Context.GetApproveAgencyIds(websiteId).Select(s => s.Value));
                HashSet<long> ExhibitorIDs = new HashSet<long>(db.Query<long>("GetApprovedExhibitorIds", new { WebsiteId = websiteId }, commandType: CommandType.StoredProcedure));
                HashSet<long> AgencyIDs = new HashSet<long>(db.Query<long>("GetApproveAgencyIds", new { WebsiteId = websiteId }, commandType: CommandType.StoredProcedure));

                var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.IsActive == "Y" && p.BookId > 0
                && (ExhibitorIDs.Contains(p.ExhibitorId.Value) || AgencyIDs.Contains(p.AgencyId.Value))).Select(s => s.BookId).Distinct());
                //if (CachingRepository.CachedBooks == null)
                //    CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.Take(100000).ToList();
                if (CachingRepository.CachedBooks == null)
                    CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
                //else
                //{
                //    var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooks");
                //    if (CachingRepository.CachedBooks.Count() != count)
                //        CachingRepository.CachedBooks = db.Query<XsiExhibitionBooks>("select * from XsiExhibitionBooks").ToList();
                //}
                // above 1.5 items in books, 84,000 to be search in contains.
                var SearchLists = CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.BookId)).ToList();

                return SearchLists.AsQueryable().Where(predicate).Select(p =>
                 new XsiExhibitionBookForUi
                 {
                     BookId = p.BookId,

                     Thumbnail = p.Thumbnail,
                     TitleAr = p.TitleAr,
                     TitleEn = p.TitleEn,
                     Price = p.Price,
                     //AuthorId = p.AuthorId,
                     PublisherId = p.BookPublisherId,
                     IsBestSeller = p.IsBestSeller,
                     BookTypeId = p.BookTypeId,
                     ExhibitionCurrencyId = p.ExhibitionCurrencyId,
                     ExhibitionSubjectId = p.ExhibitionSubjectId,
                     ExhibitionSubsubjectId = p.ExhibitionSubsubjectId,
                     Isbn = p.Isbn,
                     IssueYear = p.IssueYear,
                     //ExhibitorId = p.ExhibitorId
                 }
                 ).ToList();
            }
        }
        public List<XsiExhibitionBookForUi> SelectBooksUIOr(XsiExhibitionBooks entity, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBooks>();
            var predicateOr = PredicateBuilder.False<XsiExhibitionBooks>();
            var predicateOrSubject = PredicateBuilder.False<XsiExhibitionBooks>();
            var predicateOrSubsubject = PredicateBuilder.False<XsiExhibitionBooks>();
            var predicateOrBookType = PredicateBuilder.False<XsiExhibitionBooks>();

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsEnable.IsDefault())
                predicate = predicate.And(p => p.IsEnable.Equals(entity.IsEnable));

            if (!entity.IsBestSeller.IsDefault())
                predicate = predicate.And(p => p.IsBestSeller.Equals(entity.IsBestSeller));

            if (!string.IsNullOrEmpty(subjectIDs))
            {
                string[] strArray = subjectIDs.Split(',');
                foreach (string str in strArray)
                    predicateOrSubject = predicateOrSubject.Or(p => p.ExhibitionSubjectId == Convert.ToInt64(str));
                predicate = predicate.And(predicateOrSubject);
            }

            if (!string.IsNullOrEmpty(subsubjectIDs))
            {
                string[] strArray = subsubjectIDs.Split(',');
                foreach (string str in strArray)
                    predicateOrSubsubject = predicateOrSubsubject.Or(p => p.ExhibitionSubsubjectId == Convert.ToInt64(str));
                predicate = predicate.And(predicateOrSubsubject);
            }

            if (!string.IsNullOrEmpty(bookTypeIDs))
            {
                string[] strArray = bookTypeIDs.Split(',');
                foreach (string str in strArray)
                    predicateOrBookType = predicateOrBookType.Or(p => p.BookTypeId == Convert.ToInt64(str));
                predicate = predicate.And(predicateOrBookType);
            }

            if (!entity.ExhibitionCurrencyId.IsDefault())
            {
                predicateOr = predicateOr.Or(p => p.ExhibitionCurrencyId == entity.ExhibitionCurrencyId);
                predicate = predicate.And(predicateOr);
            }

            if (!entity.BookPublisherId.IsDefault())
            {
                predicateOr = predicateOr.Or(p => p.BookPublisherId == entity.BookPublisherId);
                predicate = predicate.And(predicateOr);
            }

            //if (!entity.AuthorId.IsDefault())
            //{
            //    predicateOr = predicateOr.Or(p => p.AuthorId == entity.AuthorId);
            //    predicate = predicate.And(predicateOr);
            //}

            //if (!entity.ExhibitorId.IsDefault())
            //{
            //    predicateOr = predicateOr.Or(p => p.ExhibitorId == entity.ExhibitorId);
            //    predicate = predicate.And(predicateOr);
            //}

            //if (!entity.AgencyId.IsDefault())
            //{
            //    predicateOr = predicateOr.Or(p => p.AgencyId == entity.AgencyId);
            //    predicate = predicate.And(predicateOr);
            //}

            if (!entity.TitleAr.IsDefault())
            {
                predicateOr = predicateOr.Or(p => p.TitleAr != null && p.TitleAr.Contains(entity.TitleAr));
                predicate = predicate.And(predicateOr);
            }

            if (!entity.TitleEn.IsDefault())
            {
                predicateOr = predicateOr.Or(p => p.TitleEn != null && p.TitleEn.ToLower().Contains(entity.TitleEn.ToLower()));
                predicate = predicate.And(predicateOr);
            }

            if (!entity.Isbn.IsDefault())
            {
                predicateOr = predicateOr.Or(p => p.Isbn != null && p.Isbn.Contains(entity.Isbn));
                predicate = predicate.And(predicateOr);
            }

            if (!entity.IssueYear.IsDefault())
            {
                predicateOr = predicateOr.Or(p => p.IssueYear != null && p.IssueYear.Contains(entity.IssueYear));
                predicate = predicate.And(predicateOr);
            }

            if (priceFrom != 0)
            {
                predicateOr = predicateOr.Or(p => Convert.ToDouble(p.Price) >= priceFrom);
                predicate = predicate.And(predicateOr);
            }

            if (priceTo != 0)
            {
                predicateOr = predicateOr.Or(p => Convert.ToDouble(p.Price) <= priceTo);
                predicate = predicate.And(predicateOr);
            }

            if (CachingRepository.CachedBooksParticipating == null)
                CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.ToList();
            //else
            //{
            //    using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //    {
            //        var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBookParticipating");
            //        if (CachingRepository.CachedBooksParticipating.Count() != count)
            //            CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.ToList();
            //    }
            //}

            HashSet<long> ExhibitorIDs = new HashSet<long>(XsiContext.Context.GetApprovedExhibitorIds(websiteId).Select(s => s.Value));
            HashSet<long> AgencyIDs = new HashSet<long>(XsiContext.Context.GetApproveAgencyIds(websiteId).Select(s => s.Value));

            var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.IsActive == "Y" && p.BookId > 0
            && (ExhibitorIDs.Contains(p.ExhibitorId.Value) || AgencyIDs.Contains(p.AgencyId.Value))).Select(s => s.BookId).Distinct());
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //else
            //{
            //    using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //    {
            //        var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooks");
            //        if (CachingRepository.CachedBooks.Count() != count)
            //            CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //    }
            //}
            // above 1.5 items in books, 84,000 to be search in contains.
            var SearchLists = CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.BookId)).ToList();

            return SearchLists.AsQueryable().Where(predicate).Select(p =>
             new XsiExhibitionBookForUi
             {
                 BookId = p.BookId,
                 Thumbnail = p.Thumbnail,
                 TitleAr = p.TitleAr,
                 TitleEn = p.TitleEn,
                 Price = p.Price,
                 //AuthorId = p.AuthorId,
                 PublisherId = p.BookPublisherId,
                 IsBestSeller = p.IsBestSeller,
                 BookTypeId = p.BookTypeId,
                 ExhibitionCurrencyId = p.ExhibitionCurrencyId,
             }
             ).ToList();
        }
        public List<XsiExhibitionBookForUi> SelectBooksUITopSearchable(XsiExhibitionBooks entity, long websiteId)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBooks>();

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsEnable.IsDefault())
                predicate = predicate.And(p => p.IsEnable.Equals(entity.IsEnable));

            if (CachingRepository.CachedBooksParticipating == null)
                CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.ToList();
            //else
            //{
            //    using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //    {
            //        var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBookParticipating");
            //        if (CachingRepository.CachedBooksParticipating.Count() != count)
            //            CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.ToList();
            //    }
            //}

            HashSet<long> ExhibitorIDs = new HashSet<long>(XsiContext.Context.GetApprovedExhibitorIds(websiteId).Select(s => s.Value));
            HashSet<long> AgencyIDs = new HashSet<long>(XsiContext.Context.GetApproveAgencyIds(websiteId).Select(s => s.Value));

            var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.IsActive == "Y" && p.BookId > 0
            && (ExhibitorIDs.Contains(p.ExhibitorId.Value) || AgencyIDs.Contains(p.AgencyId.Value))).Select(s => s.BookId).Distinct());
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //else
            //{
            //    using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //    {
            //        var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooks");
            //        if (CachingRepository.CachedBooks.Count() != count)
            //            CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //    }
            //}
            // above 1.5 items in books, 84,000 to be search in contains.
            var SearchLists = CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.BookId)).OrderByDescending(o => o.ClickCount).Take(6).ToList();

            return SearchLists.AsQueryable().Where(predicate).Select(p =>
             new XsiExhibitionBookForUi
             {
                 BookId = p.BookId,
                 Thumbnail = p.Thumbnail,
                 TitleAr = p.TitleAr,
                 TitleEn = p.TitleEn,
                 Price = p.Price,
                 //AuthorId = p.AuthorId,
                 PublisherId = p.BookPublisherId,
                 IsBestSeller = p.IsBestSeller,
                 BookTypeId = p.BookTypeId,
                 ExhibitionCurrencyId = p.ExhibitionCurrencyId,
             }
             ).ToList();
        }
        public List<XsiExhibitionBookForUi> SelectBooksTitle(XsiExhibitionBooks entity, long websiteId)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBooks>();

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsEnable.IsDefault())
                predicate = predicate.And(p => p.IsEnable.Equals(entity.IsEnable));

            if (!entity.IsBestSeller.IsDefault())
                predicate = predicate.And(p => p.IsBestSeller.Equals(entity.IsBestSeller));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr != null && p.TitleAr.Contains(entity.TitleAr));

            if (!entity.TitleEn.IsDefault())
                predicate = predicate.And(p => p.TitleEn != null && p.TitleEn.ToLower().Contains(entity.TitleEn.ToLower()));

            if (CachingRepository.CachedBooksParticipating == null)
                CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.ToList();
            //else
            //{
            //    using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //    {
            //        var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBookParticipating");
            //        if (CachingRepository.CachedBooksParticipating.Count() != count)
            //            CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.ToList();
            //    }
            //}

            HashSet<long> ExhibitorIDs = new HashSet<long>(XsiContext.Context.GetApprovedExhibitorIds(websiteId).Select(s => s.Value));
            HashSet<long> AgencyIDs = new HashSet<long>(XsiContext.Context.GetApproveAgencyIds(websiteId).Select(s => s.Value));

            var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.IsActive == "Y" && p.BookId > 0
            && (ExhibitorIDs.Contains(p.ExhibitorId.Value) || AgencyIDs.Contains(p.AgencyId.Value))).Select(s => s.BookId).Distinct());
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //else
            //{
            //    using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //    {
            //        var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooks");
            //        if (CachingRepository.CachedBooks.Count() != count)
            //            CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //    }
            //}
            // above 1.5 items in books, 84,000 to be search in contains.
            var SearchLists = CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.BookId)).ToList();

            return SearchLists.AsQueryable().Where(predicate).Take(30).Select(p =>
             new XsiExhibitionBookForUi
             {
                 BookId = p.BookId,
                 TitleAr = p.TitleAr,
                 TitleEn = p.TitleEn,
             }
             ).ToList();
        }
        //public List<GetBooksToExport_Result> SelectBooksToExport(string bookIds, string languageId)
        //{
        //    return XsiContext.Context.GetBooksToExport(bookIds, languageId).AsQueryable().ToList();
        //}

        public XsiExhibitionBooks Add(XsiExhibitionBooks entity)
        {
            XsiExhibitionBooks Book = new XsiExhibitionBooks();

            //Book.AuthorId = entity.AuthorId;
            Book.BookPublisherId = entity.BookPublisherId;
            Book.MemberId = entity.MemberId;

            Book.BookLanguageId = entity.BookLanguageId;
            Book.ExhibitionSubjectId = entity.ExhibitionSubjectId;
            Book.ExhibitionSubsubjectId = entity.ExhibitionSubsubjectId;
            Book.ExhibitionCurrencyId = entity.ExhibitionCurrencyId;
            Book.BookTypeId = entity.BookTypeId;
            Book.IsActive = entity.IsActive;
            Book.IsEnable = entity.IsEnable;
            Book.IsNew = entity.IsNew;
            if (!entity.BookDetails.IsDefault())
                Book.BookDetails = entity.BookDetails.Trim();
            if (!entity.BookDetailsAr.IsDefault())
                Book.BookDetailsAr = entity.BookDetailsAr.Trim();
            Book.IsAvailableOnline = entity.IsAvailableOnline;
            if (!entity.TitleAr.IsDefault())
                Book.TitleAr = entity.TitleAr.Trim();
            if (!entity.TitleEn.IsDefault())
                Book.TitleEn = entity.TitleEn.Trim();
            Book.Thumbnail = entity.Thumbnail;
            if (!entity.Isbn.IsDefault())
                Book.Isbn = entity.Isbn.Trim();
            if (!entity.IssueYear.IsDefault())
                Book.IssueYear = entity.IssueYear.Trim();
            if (!entity.Price.IsDefault())
                Book.Price = entity.Price.Trim();
            if (!entity.PriceBeforeDiscount.IsDefault())
                Book.PriceBeforeDiscount = entity.PriceBeforeDiscount.Trim();
            Book.IsBestSeller = entity.IsBestSeller;
            Book.ClickCount = entity.ClickCount;
            Book.CreatedOn = entity.CreatedOn;
            Book.CreatedBy = entity.CreatedBy;
            Book.ModifiedOn = entity.ModifiedOn;
            Book.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionBooks.Add(Book);
            SubmitChanges();
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            else
                CachingRepository.CachedBooks.Add(Book);
            return Book;

        }
        public void Update(XsiExhibitionBooks entity)
        {
            XsiExhibitionBooks Book = XsiContext.Context.XsiExhibitionBooks.Find(entity.BookId);
            XsiContext.Context.Entry(Book).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                Book.IsActive = entity.IsActive;
            if (!entity.IsEnable.IsDefault())
                Book.IsEnable = entity.IsEnable;
            if (!entity.IsNew.IsDefault())
                Book.IsNew = entity.IsNew;
            if (!entity.BookDetails.IsDefault())
                Book.BookDetails = entity.BookDetails.Trim();
            if (!entity.BookDetailsAr.IsDefault())
                Book.BookDetailsAr = entity.BookDetailsAr.Trim();
            if (!entity.IsAvailableOnline.IsDefault())
                Book.IsAvailableOnline = entity.IsAvailableOnline;
            if (!entity.TitleAr.IsDefault())
                Book.TitleAr = entity.TitleAr.Trim();
            if (!entity.TitleEn.IsDefault())
                Book.TitleEn = entity.TitleEn.Trim();
            if (!entity.Thumbnail.IsDefault())
                Book.Thumbnail = entity.Thumbnail;

            if (!entity.BookPublisherId.IsDefault())
                Book.BookPublisherId = entity.BookPublisherId;
            if (!entity.MemberId.IsDefault())
                Book.MemberId = entity.MemberId;

            if (!entity.ExhibitionSubsubjectId.IsDefault())
                Book.ExhibitionSubsubjectId = entity.ExhibitionSubsubjectId;
            if (!entity.ExhibitionSubjectId.IsDefault())
                Book.ExhibitionSubjectId = entity.ExhibitionSubjectId;
            if (!entity.BookLanguageId.IsDefault())
                Book.BookLanguageId = entity.BookLanguageId;
            if (!entity.ExhibitionCurrencyId.IsDefault())
                Book.ExhibitionCurrencyId = entity.ExhibitionCurrencyId;
            if (!entity.BookTypeId.IsDefault())
                Book.BookTypeId = entity.BookTypeId;
            if (!entity.Isbn.IsDefault())
                Book.Isbn = entity.Isbn.Trim();
            if (!entity.IssueYear.IsDefault())
                Book.IssueYear = entity.IssueYear.Trim();
            if (!entity.Price.IsDefault())
                Book.Price = entity.Price.Trim();
            if (!entity.PriceBeforeDiscount.IsDefault())
                Book.PriceBeforeDiscount = entity.PriceBeforeDiscount.Trim();
            if (!entity.IsBestSeller.IsDefault())
                Book.IsBestSeller = entity.IsBestSeller;
            if (!entity.ClickCount.IsDefault())
                Book.ClickCount = entity.ClickCount;
            if (!entity.ModifiedOn.IsDefault())
                Book.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                Book.ModifiedBy = entity.ModifiedBy;
            //if (!entity.SIBF_BookID.IsDefault())
            //    Book.MemberId = entity.SIBF_BookID;
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            else
            {
                #region Update Cache
                var ItemToModify = CachingRepository.CachedBooks.Where(p => p.BookId == entity.BookId).FirstOrDefault();

                if (!entity.IsActive.IsDefault())
                    ItemToModify.IsActive = entity.IsActive;
                if (!entity.IsEnable.IsDefault())
                    ItemToModify.IsEnable = entity.IsEnable;
                if (!entity.IsNew.IsDefault())
                    ItemToModify.IsNew = entity.IsNew;
                if (!entity.BookDetails.IsDefault())
                    ItemToModify.BookDetails = entity.BookDetails.Trim();
                if (!entity.BookDetailsAr.IsDefault())
                    ItemToModify.BookDetailsAr = entity.BookDetailsAr.Trim();
                if (!entity.IsAvailableOnline.IsDefault())
                    ItemToModify.IsAvailableOnline = entity.IsAvailableOnline;
                if (!entity.TitleAr.IsDefault())
                    ItemToModify.TitleAr = entity.TitleAr.Trim();
                if (!entity.TitleEn.IsDefault())
                    ItemToModify.TitleEn = entity.TitleEn.Trim();
                if (!entity.Thumbnail.IsDefault())
                    ItemToModify.Thumbnail = entity.Thumbnail;

                if (!entity.BookPublisherId.IsDefault())
                    ItemToModify.BookPublisherId = entity.BookPublisherId;
                if (!entity.MemberId.IsDefault())
                    ItemToModify.MemberId = entity.MemberId;

                if (!entity.ExhibitionSubsubjectId.IsDefault())
                    ItemToModify.ExhibitionSubsubjectId = entity.ExhibitionSubsubjectId;
                if (!entity.ExhibitionSubjectId.IsDefault())
                    ItemToModify.ExhibitionSubjectId = entity.ExhibitionSubjectId;
                if (!entity.BookLanguageId.IsDefault())
                    ItemToModify.BookLanguageId = entity.BookLanguageId;
                if (!entity.ExhibitionCurrencyId.IsDefault())
                    ItemToModify.ExhibitionCurrencyId = entity.ExhibitionCurrencyId;
                if (!entity.BookTypeId.IsDefault())
                    ItemToModify.BookTypeId = entity.BookTypeId;
                if (!entity.Isbn.IsDefault())
                    ItemToModify.Isbn = entity.Isbn.Trim();
                if (!entity.IssueYear.IsDefault())
                    ItemToModify.IssueYear = entity.IssueYear.Trim();
                if (!entity.Price.IsDefault())
                    ItemToModify.Price = entity.Price.Trim();
                if (!entity.PriceBeforeDiscount.IsDefault())
                    ItemToModify.PriceBeforeDiscount = entity.PriceBeforeDiscount.Trim();
                if (!entity.IsBestSeller.IsDefault())
                    ItemToModify.IsBestSeller = entity.IsBestSeller;
                if (!entity.ClickCount.IsDefault())
                    ItemToModify.ClickCount = entity.ClickCount;
                if (!entity.ModifiedOn.IsDefault())
                    ItemToModify.ModifiedOn = entity.ModifiedOn;
                if (!entity.ModifiedBy.IsDefault())
                    ItemToModify.ModifiedBy = entity.ModifiedBy;
                #endregion
            }
        }
        public void UpdatePublisherId(long? oldPublisherID, long? newPublisherID)
        {
            long resultCount = XsiContext.Context.UpdatePublisherInBooks(oldPublisherID, newPublisherID, 1);
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            else
            {
                var ItemToModify = CachingRepository.CachedBooks.Where(p => p.BookPublisherId == oldPublisherID);
                foreach (XsiExhibitionBooks row in ItemToModify)
                    row.BookPublisherId = newPublisherID;
            }
        }
        public void UpdateAuthorId(long? oldAuthorID, long? newAuthorID)
        {
            long resultCount = XsiContext.Context.UpdateAuthorInBooks(oldAuthorID, newAuthorID, 1);
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            else
            {
                ///useful code /usefulcode
                //var ItemToModify = CachingRepository.CachedBooks.Where(p => p.AuthorId == oldAuthorID);
                //foreach (XsiExhibitionBooks row in ItemToModify)
                //    row.AuthorId = newAuthorID;
            }
        }
        public void Delete(XsiExhibitionBooks entity)
        {
            foreach (XsiExhibitionBooks Book in Select(entity))
            {
                XsiExhibitionBooks aBook = XsiContext.Context.XsiExhibitionBooks.Find(Book.BookId);
                XsiContext.Context.XsiExhibitionBooks.Remove(aBook);
                if (CachingRepository.CachedBooks == null)
                    CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
                CachingRepository.CachedBooks.Remove(CachingRepository.CachedBooks.SingleOrDefault(x => x.BookId == Book.BookId));
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionBooks aBook = XsiContext.Context.XsiExhibitionBooks.Find(itemId);
            XsiContext.Context.XsiExhibitionBooks.Remove(aBook);
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            CachingRepository.CachedBooks.Remove(CachingRepository.CachedBooks.SingleOrDefault(x => x.BookId == itemId));
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }


        public void UpdateAuthorInBooks(long oldAuthorId, long newAuthorId, long AdminUserId)
        {
            XsiContext.Context.UpdateAuthorInBooks(oldAuthorId, newAuthorId, AdminUserId);
        }

        public void UpdatePublisherInBooks(long oldPublisherId, long newPublisherId, long AdminUserId)
        {
            XsiContext.Context.UpdatePublisherInBooks(oldPublisherId, newPublisherId, AdminUserId);
        }

        #endregion
    }
}
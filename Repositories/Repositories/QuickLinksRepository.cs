﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class QuickLinksRepository : IQuickLinksRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public QuickLinksRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public QuickLinksRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public QuickLinksRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiQuickLinks GetById(long itemId)
        {
            return XsiContext.Context.XsiQuickLinks.Find(itemId);
        }

        public List<XsiQuickLinks> Select()
        {
            return XsiContext.Context.XsiQuickLinks.ToList();
        }
        public List<XsiQuickLinks> Select(XsiQuickLinks entity)
        {
            var predicate = PredicateBuilder.True<XsiQuickLinks>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Equals(entity.Title));

            if (!entity.PageUrl.IsDefault())
                predicate = predicate.And(p => p.PageUrl.Equals(entity.PageUrl));

            if (!entity.PageContentGroupId.IsDefault())
                predicate = predicate.And(p => p.PageContentGroupId == entity.PageContentGroupId);

            if (!entity.SortOrder.IsDefault())
                predicate = predicate.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.IsExternal.IsDefault())
                predicate = predicate.And(p => p.IsExternal.Equals(entity.IsExternal));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy.Equals(entity.CreatedBy));

            if (!entity.ModifyBy.IsDefault())
                predicate = predicate.And(p => p.ModifyBy.Equals(entity.ModifyBy));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn.Equals(entity.CreatedOn));

            if (!entity.ModifyOn.IsDefault())
                predicate = predicate.And(p => p.ModifyOn.Equals(entity.ModifyOn));

            return XsiContext.Context.XsiQuickLinks.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiQuickLinks> SelectOr(XsiQuickLinks entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiQuickLinks>();
            var Outer = PredicateBuilder.True<XsiQuickLinks>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.PageUrl.IsDefault())
                Outer = Outer.And(p => p.PageUrl.Equals(entity.PageUrl));

            if (!entity.PageContentGroupId.IsDefault())
                Outer = Outer.And(p => p.PageContentGroupId == entity.PageContentGroupId);

            if (!entity.SortOrder.IsDefault())
                Outer = Outer.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.IsExternal.IsDefault())
                Outer = Outer.And(p => p.IsExternal.Equals(entity.IsExternal));

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiQuickLinks.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiQuickLinks Add(XsiQuickLinks entity)
        {
            XsiQuickLinks QuickLinks = new XsiQuickLinks();
            if (!entity.Title.IsDefault())
                QuickLinks.Title = entity.Title.Trim();
            if (!entity.PageUrl.IsDefault())
                QuickLinks.PageUrl = entity.PageUrl.Trim();
            QuickLinks.PageContentGroupId = entity.PageContentGroupId;
            QuickLinks.SortOrder = entity.SortOrder;
            QuickLinks.IsExternal = entity.IsExternal;
            QuickLinks.IsActive = entity.IsActive;
            QuickLinks.CreatedBy = entity.CreatedBy;
            QuickLinks.CreatedOn = entity.CreatedOn;
            QuickLinks.ModifyOn = entity.ModifyOn;
            QuickLinks.ModifyBy = entity.ModifyBy;
            XsiContext.Context.XsiQuickLinks.Add(QuickLinks);
            SubmitChanges();
            return QuickLinks;

        }
        public void Update(XsiQuickLinks entity)
        {
            XsiQuickLinks QuickLinks = XsiContext.Context.XsiQuickLinks.Find(entity.ItemId);
            XsiContext.Context.Entry(QuickLinks).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                QuickLinks.Title = entity.Title.Trim();

            if (!entity.PageUrl.IsDefault())
                QuickLinks.PageUrl = entity.PageUrl.Trim();

            if (!entity.PageContentGroupId.IsDefault())
                QuickLinks.PageContentGroupId = entity.PageContentGroupId;

            if (!entity.SortOrder.IsDefault())
                QuickLinks.SortOrder = entity.SortOrder;

            if (!entity.IsExternal.IsDefault())
                QuickLinks.IsExternal = entity.IsExternal;

            if (!entity.IsActive.IsDefault())
                QuickLinks.IsActive = entity.IsActive;

            if (!entity.ModifyBy.IsDefault())
                QuickLinks.ModifyBy = entity.ModifyBy;

            if (!entity.ModifyOn.IsDefault())
                QuickLinks.ModifyOn = entity.ModifyOn;
        }
        public void Delete(XsiQuickLinks entity)
        {
            foreach (XsiQuickLinks QuickLinks in Select(entity))
            {
                XsiQuickLinks aQuickLinks = XsiContext.Context.XsiQuickLinks.Find(QuickLinks.ItemId);
                XsiContext.Context.XsiQuickLinks.Remove(aQuickLinks);
            }
        }
        public void Delete(long itemId)
        {
            XsiQuickLinks aQuickLinks = XsiContext.Context.XsiQuickLinks.Find(itemId);
            XsiContext.Context.XsiQuickLinks.Remove(aQuickLinks);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
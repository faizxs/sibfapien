﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class InvoiceRepository : IInvoiceRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public InvoiceRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public InvoiceRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public InvoiceRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiInvoice GetById(long itemId)
        {
            return XsiContext.Context.XsiInvoice.Find(itemId);
        }
        public List<XsiInvoice> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiInvoice> Select()
        {
            return XsiContext.Context.XsiInvoice.ToList();
        }
        public List<XsiInvoice> Select(XsiInvoice entity)
        {
            var predicate = PredicateBuilder.True<XsiInvoice>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ExhibitorId.IsDefault())
                predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

            if (!entity.InvoiceNumber.IsDefault())
                predicate = predicate.And(p => p.InvoiceNumber.Contains(entity.InvoiceNumber));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Equals(entity.FileName));

            if (!entity.AllocatedSpace.IsDefault())
                predicate = predicate.And(p => p.AllocatedSpace.Equals(entity.AllocatedSpace));

            if (!entity.ParticipationFee.IsDefault())
                predicate = predicate.And(p => p.ParticipationFee.Equals(entity.ParticipationFee));

            if (!entity.StandFee.IsDefault())
                predicate = predicate.And(p => p.StandFee.Equals(entity.StandFee));

            if (!entity.KnowledgeAndResearchFee.IsDefault())
                predicate = predicate.And(p => p.KnowledgeAndResearchFee.Equals(entity.KnowledgeAndResearchFee));

            if (!entity.AgencyFee.IsDefault())
                predicate = predicate.And(p => p.AgencyFee.Equals(entity.AgencyFee));

            if (!entity.RepresentativeFee.IsDefault())
                predicate = predicate.And(p => p.RepresentativeFee.Equals(entity.RepresentativeFee));

            if (!entity.DueFromLastYearCredit.IsDefault())
                predicate = predicate.And(p => p.DueFromLastYearCredit.Equals(entity.DueFromLastYearCredit));

            if (!entity.DueFromLastYearDebit.IsDefault())
                predicate = predicate.And(p => p.DueFromLastYearDebit.Equals(entity.DueFromLastYearDebit));

            if (!entity.DueCurrentYear.IsDefault())
                predicate = predicate.And(p => p.DueCurrentYear.Equals(entity.DueCurrentYear));

            if (!entity.DebitedCurrentYear.IsDefault())
                predicate = predicate.And(p => p.DebitedCurrentYear.Equals(entity.DebitedCurrentYear));

            if (!entity.Discount.IsDefault())
                predicate = predicate.And(p => p.Discount.Equals(entity.Discount));

            if (!entity.DiscountNote.IsDefault())
                predicate = predicate.And(p => p.DiscountNote.Contains(entity.DiscountNote));

            if (!entity.Penalty.IsDefault())
                predicate = predicate.And(p => p.Penalty.Equals(entity.Penalty));

            if (!entity.Vat.IsDefault())
                predicate = predicate.And(p => p.Vat.Equals(entity.Vat));

            if (!entity.SubTotalAed.IsDefault())
                predicate = predicate.And(p => p.SubTotalAed.Equals(entity.SubTotalAed));

            if (!entity.SubTotalUsd.IsDefault())
                predicate = predicate.And(p => p.SubTotalUsd.Equals(entity.SubTotalUsd));

            if (!entity.TotalAed.IsDefault())
                predicate = predicate.And(p => p.TotalAed.Equals(entity.TotalAed));

            if (!entity.TotalUsd.IsDefault())
                predicate = predicate.And(p => p.TotalUsd.Equals(entity.TotalUsd));

            if (!entity.PaidAmount.IsDefault())
                predicate = predicate.And(p => p.PaidAmount.Equals(entity.PaidAmount));

            if (!entity.PaidAmountUsd.IsDefault())
                predicate = predicate.And(p => p.PaidAmountUsd.Equals(entity.PaidAmountUsd));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.IsPaymentInAed.IsDefault())
                predicate = predicate.And(p => p.IsPaymentInAed.Equals(entity.IsPaymentInAed));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsChequeReceived.IsDefault())
                predicate = predicate.And(p => p.IsChequeReceived.Equals(entity.IsChequeReceived));

            if (!entity.IsNewPending.IsDefault())
                predicate = predicate.And(p => p.IsNewPending.Equals(entity.IsNewPending));

            if (!entity.ReceiptNumber.IsDefault())
                predicate = predicate.And(p => p.ReceiptNumber.Equals(entity.ReceiptNumber));

            if (!entity.BankName.IsDefault())
                predicate = predicate.And(p => p.BankName.Contains(entity.BankName));

            if (!entity.TransferDate.IsDefault())
                predicate = predicate.And(p => p.TransferDate == entity.TransferDate);

            if (!entity.UploadePaymentReceipt.IsDefault())
                predicate = predicate.And(p => p.UploadePaymentReceipt.Equals(entity.UploadePaymentReceipt));

            if (!entity.PaymentTypeId.IsDefault())
                predicate = predicate.And(p => p.PaymentTypeId == entity.PaymentTypeId);

            if (!entity.IsStatusChanged.IsDefault())
                predicate = predicate.And(p => p.IsStatusChanged.Equals(entity.IsStatusChanged));

            if (!entity.PgtransactionId.IsDefault())
                predicate = predicate.And(p => p.PgtransactionId.Equals(entity.PgtransactionId));

            if (!entity.Pgstatus.IsDefault())
                predicate = predicate.And(p => p.Pgstatus.Equals(entity.Pgstatus));

            if (!entity.Notes.IsDefault())
                predicate = predicate.And(p => p.Notes.Contains(entity.Notes));

            if (!entity.GeneralNotes.IsDefault())
                predicate = predicate.And(p => p.GeneralNotes.Contains(entity.GeneralNotes));

            if (!entity.ExemptionType.IsDefault())
                predicate = predicate.And(p => p.ExemptionType.Equals(entity.ExemptionType));

            if (!entity.IsRegistrationInvoice.IsDefault())
                predicate = predicate.And(p => p.IsRegistrationInvoice.Equals(entity.IsRegistrationInvoice));

            if (!entity.DiscountPercent.IsDefault())
                predicate = predicate.And(p => p.DiscountPercent.Equals(entity.DiscountPercent));

            if (!entity.PenaltyPercent.IsDefault())
                predicate = predicate.And(p => p.PenaltyPercent.Equals(entity.PenaltyPercent));

            if (!entity.RemainingAgencies.IsDefault())
                predicate = predicate.And(p => p.RemainingAgencies.Equals(entity.RemainingAgencies));

            if (!entity.RemainingRepresentatives.IsDefault())
                predicate = predicate.And(p => p.RemainingRepresentatives.Equals(entity.RemainingRepresentatives));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedById.IsDefault())
                predicate = predicate.And(p => p.CreatedById == entity.CreatedById);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedById.IsDefault())
                predicate = predicate.And(p => p.ModifiedById == entity.ModifiedById);

            if (!entity.InvoiceNumberFromUser.IsDefault())
                predicate = predicate.And(p => p.InvoiceNumberFromUser.Equals(entity.InvoiceNumberFromUser));

            if (!entity.NoofParcel.IsDefault())
                predicate = predicate.And(p => p.NoofParcel.Equals(entity.NoofParcel));

            return XsiContext.Context.XsiInvoice.AsExpandable().Where(predicate).ToList();
        }

        public XsiInvoice Add(XsiInvoice entity)
        {
            XsiInvoice Invoice = new XsiInvoice();
            Invoice.ExhibitorId = entity.ExhibitorId;
            if (!entity.InvoiceNumber.IsDefault())
                Invoice.InvoiceNumber = entity.InvoiceNumber.Trim();
            Invoice.FileName = entity.FileName;
            if (!entity.AllocatedSpace.IsDefault())
                Invoice.AllocatedSpace = entity.AllocatedSpace.Trim();
            if (!entity.ParticipationFee.IsDefault())
                Invoice.ParticipationFee = entity.ParticipationFee.Trim();
            if (!entity.StandFee.IsDefault())
                Invoice.StandFee = entity.StandFee.Trim();
            if (!entity.KnowledgeAndResearchFee.IsDefault())
                Invoice.KnowledgeAndResearchFee = entity.KnowledgeAndResearchFee.Trim();
            if (!entity.AgencyFee.IsDefault())
                Invoice.AgencyFee = entity.AgencyFee.Trim();
            if (!entity.RepresentativeFee.IsDefault())
                Invoice.RepresentativeFee = entity.RepresentativeFee.Trim();
            if (!entity.DueFromLastYearCredit.IsDefault())
                Invoice.DueFromLastYearCredit = entity.DueFromLastYearCredit.Trim();
            if (!entity.DueFromLastYearDebit.IsDefault())
                Invoice.DueFromLastYearDebit = entity.DueFromLastYearDebit.Trim();
            if (!entity.DueCurrentYear.IsDefault())
                Invoice.DueCurrentYear = entity.DueCurrentYear.Trim();
            if (!entity.DebitedCurrentYear.IsDefault())
                Invoice.DebitedCurrentYear = entity.DebitedCurrentYear.Trim();
            if (!entity.Discount.IsDefault())
                Invoice.Discount = entity.Discount.Trim();
            if (!entity.DiscountNote.IsDefault())
                Invoice.DiscountNote = entity.DiscountNote.Trim();
            if (!entity.Penalty.IsDefault())
                Invoice.Penalty = entity.Penalty.Trim();
            if (!entity.Vat.IsDefault())
                Invoice.Vat = entity.Vat.Trim();
            if (!entity.SubTotalAed.IsDefault())
                Invoice.SubTotalAed = entity.SubTotalAed.Trim();
            if (!entity.SubTotalUsd.IsDefault())
                Invoice.SubTotalUsd = entity.SubTotalUsd.Trim();
            if (!entity.TotalAed.IsDefault())
                Invoice.TotalAed = entity.TotalAed.Trim();
            if (!entity.TotalUsd.IsDefault())
                Invoice.TotalUsd = entity.TotalUsd.Trim();
            if (!entity.PaidAmount.IsDefault())
                Invoice.PaidAmount = entity.PaidAmount.Trim();
            if (!entity.PaidAmountUsd.IsDefault())
                Invoice.PaidAmountUsd = entity.PaidAmountUsd.Trim();
            if (!entity.ReceiptNumber.IsDefault())
                Invoice.ReceiptNumber = entity.ReceiptNumber.Trim();
            if (!entity.BankName.IsDefault())
                Invoice.BankName = entity.BankName.Trim();
            if (!entity.TransferDate.IsDefault())
                Invoice.TransferDate = entity.TransferDate;
            Invoice.UploadePaymentReceipt = entity.UploadePaymentReceipt;
            Invoice.PaymentTypeId = entity.PaymentTypeId;
            Invoice.Status = entity.Status;
            if (!entity.Notes.IsDefault())
                Invoice.Notes = entity.Notes.Trim();
            if (!entity.GeneralNotes.IsDefault())
                Invoice.GeneralNotes = entity.GeneralNotes.Trim();
            Invoice.ExemptionType = entity.ExemptionType;
            Invoice.IsPaymentInAed = entity.IsPaymentInAed;
            Invoice.IsActive = entity.IsActive;
            Invoice.IsChequeReceived = entity.IsChequeReceived;
            Invoice.IsNewPending = entity.IsNewPending;
            Invoice.IsStatusChanged = entity.IsStatusChanged;
            Invoice.IsRegistrationInvoice = entity.IsRegistrationInvoice;
            if (!entity.DiscountPercent.IsDefault())
                Invoice.DiscountPercent = entity.DiscountPercent.Trim();
            if (!entity.PenaltyPercent.IsDefault())
                Invoice.PenaltyPercent = entity.PenaltyPercent.Trim();
            if (!entity.RemainingAgencies.IsDefault())
                Invoice.RemainingAgencies = entity.RemainingAgencies.Trim();
            if (!entity.RemainingRepresentatives.IsDefault())
                Invoice.RemainingRepresentatives = entity.RemainingRepresentatives.Trim();
            if (!entity.Pgstatus.IsDefault())
                Invoice.Pgstatus = entity.Pgstatus;
            if (!entity.PgtransactionId.IsDefault())
                Invoice.PgtransactionId = entity.PgtransactionId;

            if (!entity.InvoiceNumberFromUser.IsDefault())
                Invoice.InvoiceNumberFromUser = entity.InvoiceNumberFromUser;

            if (!entity.NoofParcel.IsDefault())
                Invoice.NoofParcel = entity.NoofParcel;

            Invoice.CreatedOn = entity.CreatedOn;
            Invoice.CreatedById = entity.CreatedById;
            Invoice.ModifiedOn = entity.ModifiedOn;
            Invoice.ModifiedById = entity.ModifiedById;

            XsiContext.Context.XsiInvoice.Add(Invoice);
            SubmitChanges();
            return Invoice;

        }
        public void Update(XsiInvoice entity)
        {
            XsiInvoice Invoice = XsiContext.Context.XsiInvoice.Find(entity.ItemId);
            XsiContext.Context.Entry(Invoice).State = EntityState.Modified;

            if (!entity.InvoiceNumber.IsDefault())
                Invoice.InvoiceNumber = entity.InvoiceNumber.Trim();

            if (!entity.FileName.IsDefault())
                Invoice.FileName = entity.FileName;

            if (!entity.AllocatedSpace.IsDefault())
                Invoice.AllocatedSpace = entity.AllocatedSpace.Trim();

            if (!entity.ParticipationFee.IsDefault())
                Invoice.ParticipationFee = entity.ParticipationFee.Trim();

            if (!entity.StandFee.IsDefault())
                Invoice.StandFee = entity.StandFee.Trim();

            if (!entity.KnowledgeAndResearchFee.IsDefault())
                Invoice.KnowledgeAndResearchFee = entity.KnowledgeAndResearchFee.Trim();

            if (!entity.AgencyFee.IsDefault())
                Invoice.AgencyFee = entity.AgencyFee.Trim();

            if (!entity.RepresentativeFee.IsDefault())
                Invoice.RepresentativeFee = entity.RepresentativeFee.Trim();

            if (!entity.DueFromLastYearCredit.IsDefault())
                Invoice.DueFromLastYearCredit = entity.DueFromLastYearCredit.Trim();

            if (!entity.DueFromLastYearDebit.IsDefault())
                Invoice.DueFromLastYearDebit = entity.DueFromLastYearDebit.Trim();

            if (!entity.DueCurrentYear.IsDefault())
                Invoice.DueCurrentYear = entity.DueCurrentYear.Trim();

            if (!entity.DebitedCurrentYear.IsDefault())
                Invoice.DebitedCurrentYear = entity.DebitedCurrentYear.Trim();

            if (!entity.Discount.IsDefault())
                Invoice.Discount = entity.Discount.Trim();

            if (!entity.DiscountNote.IsDefault())
                Invoice.DiscountNote = entity.DiscountNote.Trim();

            if (!entity.Penalty.IsDefault())
                Invoice.Penalty = entity.Penalty.Trim();

            if (!entity.Vat.IsDefault())
                Invoice.Vat = entity.Vat.Trim();

            if (!entity.SubTotalAed.IsDefault())
                Invoice.SubTotalAed = entity.SubTotalAed.Trim();

            if (!entity.SubTotalUsd.IsDefault())
                Invoice.SubTotalUsd = entity.SubTotalUsd.Trim();

            if (!entity.TotalAed.IsDefault())
                Invoice.TotalAed = entity.TotalAed.Trim();

            if (!entity.TotalUsd.IsDefault())
                Invoice.TotalUsd = entity.TotalUsd.Trim();

            if (!entity.PaidAmount.IsDefault())
                Invoice.PaidAmount = entity.PaidAmount.Trim();

            if (!entity.PaidAmountUsd.IsDefault())
                Invoice.PaidAmountUsd = entity.PaidAmountUsd.Trim();

            if (!entity.ReceiptNumber.IsDefault())
                Invoice.ReceiptNumber = entity.ReceiptNumber.Trim();

            if (!entity.BankName.IsDefault())
                Invoice.BankName = entity.BankName.Trim();

            if (!entity.TransferDate.IsDefault())
                Invoice.TransferDate = entity.TransferDate;

            if (!entity.UploadePaymentReceipt.IsDefault())
                Invoice.UploadePaymentReceipt = entity.UploadePaymentReceipt;

            if (!entity.PaymentTypeId.IsDefault())
                Invoice.PaymentTypeId = entity.PaymentTypeId;

            if (!entity.Status.IsDefault())
                Invoice.Status = entity.Status;

            if (!entity.Notes.IsDefault())
                Invoice.Notes = entity.Notes.Trim();

            if (!entity.GeneralNotes.IsDefault())
                Invoice.GeneralNotes = entity.GeneralNotes.Trim();

            if (!entity.ExemptionType.IsDefault())
                Invoice.ExemptionType = entity.ExemptionType;

            if (!entity.IsPaymentInAed.IsDefault())
                Invoice.IsPaymentInAed = entity.IsPaymentInAed;

            if (!entity.IsActive.IsDefault())
                Invoice.IsActive = entity.IsActive;

            if (!entity.IsChequeReceived.IsDefault())
                Invoice.IsChequeReceived = entity.IsChequeReceived;

            if (!entity.IsNewPending.IsDefault())
                Invoice.IsNewPending = entity.IsNewPending;

            if (!entity.IsStatusChanged.IsDefault())
                Invoice.IsStatusChanged = entity.IsStatusChanged;

            if (!entity.IsRegistrationInvoice.IsDefault())
                Invoice.IsRegistrationInvoice = entity.IsRegistrationInvoice;

            if (!entity.DiscountPercent.IsDefault())
                Invoice.DiscountPercent = entity.DiscountPercent.Trim();

            if (!entity.PenaltyPercent.IsDefault())
                Invoice.PenaltyPercent = entity.PenaltyPercent.Trim();

            if (!entity.RemainingAgencies.IsDefault())
                Invoice.RemainingAgencies = entity.RemainingAgencies.Trim();

            if (!entity.RemainingRepresentatives.IsDefault())
                Invoice.RemainingRepresentatives = entity.RemainingRepresentatives.Trim();

            if (!entity.InvoiceNumberFromUser.IsDefault())
                Invoice.InvoiceNumberFromUser = entity.InvoiceNumberFromUser;

            if (!entity.NoofParcel.IsDefault())
                Invoice.NoofParcel = entity.NoofParcel;

            if (!entity.Pgstatus.IsDefault())
                Invoice.Pgstatus = entity.Pgstatus;

            if (!entity.PgtransactionId.IsDefault())
                Invoice.PgtransactionId = entity.PgtransactionId;

            if (!entity.ModifiedOn.IsDefault())
                Invoice.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedById.IsDefault())
                Invoice.ModifiedById = entity.ModifiedById;
        }
        public void Delete(XsiInvoice entity)
        {
            foreach (XsiInvoice Invoice in Select(entity))
            {
                XsiInvoice aInvoice = XsiContext.Context.XsiInvoice.Find(Invoice.ItemId);
                XsiContext.Context.XsiInvoice.Remove(aInvoice);
            }
        }
        public void Delete(long itemId)
        {
            XsiInvoice aInvoice = XsiContext.Context.XsiInvoice.Find(itemId);
            XsiContext.Context.XsiInvoice.Remove(aInvoice);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
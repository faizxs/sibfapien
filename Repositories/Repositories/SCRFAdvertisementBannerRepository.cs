﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFAdvertisementBannerRepository : ISCRFAdvertisementBannerRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFAdvertisementBannerRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFAdvertisementBannerRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFAdvertisementBannerRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfadvertisementBanner GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfadvertisementBanner.Find(itemId);
        }

        public List<XsiScrfadvertisementBanner> Select()
        {
            return XsiContext.Context.XsiScrfadvertisementBanner.ToList();
        }
        public List<XsiScrfadvertisementBanner> Select(XsiScrfadvertisementBanner entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfadvertisementBanner>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CompanyName.IsDefault())
                predicate = predicate.And(p => p.CompanyName.Contains(entity.CompanyName));

            if (!entity.ContactPerson.IsDefault())
                predicate = predicate.And(p => p.ContactPerson.Contains(entity.ContactPerson));

            if (!entity.PhoneNo.IsDefault())
                predicate = predicate.And(p => p.PhoneNo.Equals(entity.PhoneNo));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Equals(entity.Email));

            if (!entity.HomeBanner.IsDefault())
                predicate = predicate.And(p => p.HomeBanner.Equals(entity.HomeBanner));

            if (!entity.SubpageBanner.IsDefault())
                predicate = predicate.And(p => p.SubpageBanner.Equals(entity.SubpageBanner));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            if (!entity.StartDate.IsDefault())
                predicate = predicate.And(p => p.StartDate == entity.StartDate);

            if (!entity.EndDate.IsDefault())
                predicate = predicate.And(p => p.EndDate == entity.EndDate);

            if (!entity.PageIds.IsDefault())
                predicate = predicate.And(p => p.PageIds.Equals(entity.PageIds));

            return XsiContext.Context.XsiScrfadvertisementBanner.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiScrfadvertisementBanner> SelectOr(XsiScrfadvertisementBanner entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfadvertisementBanner>();
            var Outer = PredicateBuilder.True<XsiScrfadvertisementBanner>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiScrfadvertisementBanner.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public XsiScrfadvertisementBanner Add(XsiScrfadvertisementBanner entity)
        {
            XsiScrfadvertisementBanner SCRFAdvertisementBanner = new XsiScrfadvertisementBanner();
            if (!entity.Title.IsDefault())
            SCRFAdvertisementBanner.Title = entity.Title.Trim();
            SCRFAdvertisementBanner.IsActive = entity.IsActive;
            if (!entity.CompanyName.IsDefault())
                SCRFAdvertisementBanner.CompanyName = entity.CompanyName.Trim();
            if (!entity.ContactPerson.IsDefault())
                SCRFAdvertisementBanner.ContactPerson = entity.ContactPerson.Trim();
            if (!entity.PhoneNo.IsDefault())
                SCRFAdvertisementBanner.PhoneNo = entity.PhoneNo.Trim();
            if (!entity.Email.IsDefault())
            SCRFAdvertisementBanner.Email = entity.Email.Trim();
            SCRFAdvertisementBanner.HomeBanner = entity.HomeBanner;
            SCRFAdvertisementBanner.SubpageBanner = entity.SubpageBanner;
            if (!entity.Url.IsDefault())
            SCRFAdvertisementBanner.Url = entity.Url.Trim();
            SCRFAdvertisementBanner.StartDate = entity.StartDate;
            SCRFAdvertisementBanner.EndDate = entity.EndDate;
            SCRFAdvertisementBanner.PageIds = entity.PageIds;
            SCRFAdvertisementBanner.CreatedOn = entity.CreatedOn;
            SCRFAdvertisementBanner.CreatedBy = entity.CreatedBy;
            SCRFAdvertisementBanner.ModifiedOn = entity.ModifiedOn;
            SCRFAdvertisementBanner.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfadvertisementBanner.Add(SCRFAdvertisementBanner);
            SubmitChanges();
            return SCRFAdvertisementBanner;

        }
        public void Update(XsiScrfadvertisementBanner entity)
        {
            XsiScrfadvertisementBanner SCRFAdvertisementBanner = XsiContext.Context.XsiScrfadvertisementBanner.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFAdvertisementBanner).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                SCRFAdvertisementBanner.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                SCRFAdvertisementBanner.IsActive = entity.IsActive;
            if (!entity.CompanyName.IsDefault())
                SCRFAdvertisementBanner.CompanyName = entity.CompanyName.Trim();
            if (!entity.ContactPerson.IsDefault())
                SCRFAdvertisementBanner.ContactPerson = entity.ContactPerson.Trim();
            if (!entity.PhoneNo.IsDefault())
                SCRFAdvertisementBanner.PhoneNo = entity.PhoneNo.Trim();
            if (!entity.Email.IsDefault())
                SCRFAdvertisementBanner.Email = entity.Email.Trim();
            if (!entity.HomeBanner.IsDefault())
                SCRFAdvertisementBanner.HomeBanner = entity.HomeBanner;
            if (!entity.SubpageBanner.IsDefault())
                SCRFAdvertisementBanner.SubpageBanner = entity.SubpageBanner;
            if (!entity.Url.IsDefault())
                SCRFAdvertisementBanner.Url = entity.Url.Trim();
            if (!entity.StartDate.IsDefault())
                SCRFAdvertisementBanner.StartDate = entity.StartDate;
            if (!entity.EndDate.IsDefault())
                SCRFAdvertisementBanner.EndDate = entity.EndDate;
            if (!entity.PageIds.IsDefault())
                SCRFAdvertisementBanner.PageIds = entity.PageIds;
            if (!entity.ModifiedOn.IsDefault())
                SCRFAdvertisementBanner.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                SCRFAdvertisementBanner.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiScrfadvertisementBanner entity)
        {
            foreach (XsiScrfadvertisementBanner SCRFAdvertisementBanner in Select(entity))
            {
                XsiScrfadvertisementBanner aSCRFAdvertisementBanner = XsiContext.Context.XsiScrfadvertisementBanner.Find(SCRFAdvertisementBanner.ItemId);
                XsiContext.Context.XsiScrfadvertisementBanner.Remove(aSCRFAdvertisementBanner);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfadvertisementBanner aSCRFAdvertisementBanner = XsiContext.Context.XsiScrfadvertisementBanner.Find(itemId);
            XsiContext.Context.XsiScrfadvertisementBanner.Remove(aSCRFAdvertisementBanner);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
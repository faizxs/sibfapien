﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class WinnersRepository : IWinnersRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public WinnersRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public WinnersRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public WinnersRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiAwardWinners GetById(long itemId)
        {
            return XsiContext.Context.XsiAwardWinners.Find(itemId);
        }
        public List<XsiAwardWinners> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiAwardWinners> Select()
        {
            return XsiContext.Context.XsiAwardWinners.ToList();
        }
        public List<XsiAwardWinners> Select(XsiAwardWinners entity)
        {
            var predicate = PredicateBuilder.True<XsiAwardWinners>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            if (!entity.AwardsUrl.IsDefault())
                predicate = predicate.And(p => p.AwardsUrl.Contains(entity.AwardsUrl));

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.FirstNameAr.IsDefault())
                predicate = predicate.And(p => p.FirstNameAr.Contains(entity.FirstNameAr));

            if (!entity.FirstNameEn.IsDefault())
                predicate = predicate.And(p => p.FirstNameEn.Contains(entity.FirstNameEn));

            if (!entity.Image.IsDefault())
                predicate = predicate.And(p => p.Image.Equals(entity.Image));

            if (!entity.BookThumbnail.IsDefault())
                predicate = predicate.And(p => p.BookThumbnail.Equals(entity.BookThumbnail));

            if (!entity.PublisherThumbnail.IsDefault())
                predicate = predicate.And(p => p.PublisherThumbnail.Equals(entity.PublisherThumbnail));

            if (!entity.LastNameAr.IsDefault())
                predicate = predicate.And(p => p.LastNameAr.Contains(entity.LastNameAr));

            if (!entity.LastNameEn.IsDefault())
                predicate = predicate.And(p => p.LastNameEn.Contains(entity.LastNameEn));

            if (!entity.AwardId.IsDefault())
                predicate = predicate.And(p => p.AwardId == entity.AwardId);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifieidBy.IsDefault())
                predicate = predicate.And(p => p.ModifieidBy == entity.ModifieidBy);

            if (!entity.NominationFormId.IsDefault())
                predicate = predicate.And(p => p.NominationFormId == entity.NominationFormId);

            if (!entity.OverviewAr.IsDefault())
                predicate = predicate.And(p => p.OverviewAr.Contains(entity.OverviewAr));

            if (!entity.OverviewEn.IsDefault())
                predicate = predicate.And(p => p.OverviewEn.Contains(entity.OverviewEn));

            if (!entity.SubAwardId.IsDefault())
                predicate = predicate.And(p => p.SubAwardId == entity.SubAwardId);

            if (!entity.UserRegistrationNumber.IsDefault())
                predicate = predicate.And(p => p.UserRegistrationNumber == entity.UserRegistrationNumber);

            if (!entity.BookTitleEn.IsDefault())
                predicate = predicate.And(p => p.BookTitleEn.Contains(entity.BookTitleEn));

            if (!entity.BookTitleAr.IsDefault())
                predicate = predicate.And(p => p.BookTitleAr.Contains(entity.BookTitleAr));

            if (!entity.PublisherEn.IsDefault())
                predicate = predicate.And(p => p.PublisherEn.Contains(entity.PublisherEn));

            if (!entity.PublisherAr.IsDefault())
                predicate = predicate.And(p => p.PublisherAr.Contains(entity.PublisherAr));

            if (!entity.BookOverviewEn.IsDefault())
                predicate = predicate.And(p => p.BookOverviewEn.Contains(entity.BookOverviewEn));

            if (!entity.BookOverviewAr.IsDefault())
                predicate = predicate.And(p => p.BookOverviewAr.Contains(entity.BookOverviewAr));

            return XsiContext.Context.XsiAwardWinners.AsExpandable().Where(predicate).ToList();
        }

        public XsiAwardWinners Add(XsiAwardWinners entity)
        {
            XsiAwardWinners Award = new XsiAwardWinners();
            Award.ExhibitionId = entity.ExhibitionId;
            if (!entity.AwardsUrl.IsDefault())
                Award.AwardsUrl = entity.AwardsUrl.Trim();
            Award.IsActive = entity.IsActive;
            Award.CreatedBy = entity.CreatedBy;
            Award.CreatedOn = entity.CreatedOn;
            if (!entity.FirstNameAr.IsDefault())
                Award.FirstNameAr = entity.FirstNameAr.Trim();
            if (!entity.FirstNameEn.IsDefault())
                Award.FirstNameEn = entity.FirstNameEn.Trim();
            Award.Image = entity.Image;
            Award.BookThumbnail = entity.BookThumbnail;
            Award.PublisherThumbnail = entity.PublisherThumbnail;
            if (!entity.LastNameAr.IsDefault())
                Award.LastNameAr = entity.LastNameAr.Trim();
            if (!entity.LastNameEn.IsDefault())
                Award.LastNameEn = entity.LastNameEn.Trim();
            Award.AwardId = entity.AwardId;
            Award.ModifiedOn = entity.ModifiedOn;
            Award.ModifieidBy = entity.ModifieidBy;
            Award.NominationFormId = entity.NominationFormId;
            if (!entity.OverviewAr.IsDefault())
                Award.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.OverviewEn.IsDefault())
                Award.OverviewEn = entity.OverviewEn.Trim();
            Award.SubAwardId = entity.SubAwardId;
            Award.UserRegistrationNumber = entity.UserRegistrationNumber;
            if (!entity.BookTitleEn.IsDefault())
                Award.BookTitleEn = entity.BookTitleEn.Trim();
            if (!entity.BookTitleAr.IsDefault())
                Award.BookTitleAr = entity.BookTitleAr.Trim();
            if (!entity.PublisherEn.IsDefault())
                Award.PublisherEn = entity.PublisherEn.Trim();
            if (!entity.PublisherAr.IsDefault())
                Award.PublisherAr = entity.PublisherAr.Trim();
            if (!entity.BookOverviewEn.IsDefault())
                Award.BookOverviewEn = entity.BookOverviewEn.Trim();
            if (!entity.BookOverviewAr.IsDefault())
                Award.BookOverviewAr = entity.BookOverviewAr.Trim();

            XsiContext.Context.XsiAwardWinners.Add(Award);
            SubmitChanges();
            return Award;

        }
        public void Update(XsiAwardWinners entity)
        {
            XsiAwardWinners Award = XsiContext.Context.XsiAwardWinners.Find(entity.ItemId);
            XsiContext.Context.Entry(Award).State = EntityState.Modified;

            if (!entity.ExhibitionId.IsDefault())
                Award.ExhibitionId = entity.ExhibitionId;
            if (!entity.AwardsUrl.IsDefault())
                Award.AwardsUrl = entity.AwardsUrl.Trim();
            if (!entity.IsActive.IsDefault())
                Award.IsActive = entity.IsActive;
            if (!entity.CreatedBy.IsDefault())
                Award.CreatedBy = entity.CreatedBy;
            if (!entity.CreatedOn.IsDefault())
                Award.CreatedOn = entity.CreatedOn;
            if (!entity.FirstNameAr.IsDefault())
                Award.FirstNameAr = entity.FirstNameAr.Trim();
            if (!entity.FirstNameEn.IsDefault())
                Award.FirstNameEn = entity.FirstNameEn.Trim();
            if (!entity.Image.IsDefault())
                Award.Image = entity.Image;
            if (!entity.BookThumbnail.IsDefault())
                Award.BookThumbnail = entity.BookThumbnail;
            if (!entity.PublisherThumbnail.IsDefault())
                Award.PublisherThumbnail = entity.PublisherThumbnail;
            if (!entity.LastNameAr.IsDefault())
                Award.LastNameAr = entity.LastNameAr.Trim();
            if (!entity.LastNameEn.IsDefault())
                Award.LastNameEn = entity.LastNameEn.Trim();
            if (!entity.AwardId.IsDefault())
                Award.AwardId = entity.AwardId;
            if (!entity.ModifiedOn.IsDefault())
                Award.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifieidBy.IsDefault())
                Award.ModifieidBy = entity.ModifieidBy;
            if (!entity.NominationFormId.IsDefault())
                Award.NominationFormId = entity.NominationFormId;
            if (!entity.OverviewAr.IsDefault())
                Award.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.OverviewEn.IsDefault())
                Award.OverviewEn = entity.OverviewEn.Trim();
            if (!entity.SubAwardId.IsDefault())
                Award.SubAwardId = entity.SubAwardId;
            if (!entity.UserRegistrationNumber.IsDefault())
                Award.UserRegistrationNumber = entity.UserRegistrationNumber;
            if (!entity.BookTitleEn.IsDefault())
                Award.BookTitleEn = entity.BookTitleEn.Trim();
            if (!entity.BookTitleAr.IsDefault())
                Award.BookTitleAr = entity.BookTitleAr.Trim();
            if (!entity.PublisherEn.IsDefault())
                Award.PublisherEn = entity.PublisherEn.Trim();
            if (!entity.PublisherAr.IsDefault())
                Award.PublisherAr = entity.PublisherAr.Trim();
            if (!entity.BookOverviewEn.IsDefault())
                Award.BookOverviewEn = entity.BookOverviewEn.Trim();
            if (!entity.BookOverviewAr.IsDefault())
                Award.BookOverviewAr = entity.BookOverviewAr.Trim();
        }
        public void Delete(XsiAwardWinners entity)
        {
            foreach (XsiAwardWinners Award in Select(entity))
            {
                XsiAwardWinners aAward = XsiContext.Context.XsiAwardWinners.Find(Award.ItemId);
                XsiContext.Context.XsiAwardWinners.Remove(aAward);
            }
        }
        public void Delete(long itemId)
        {
            XsiAwardWinners aAward = XsiContext.Context.XsiAwardWinners.Find(itemId);
            XsiContext.Context.XsiAwardWinners.Remove(aAward);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionCategoryRepository : IExhibitionCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionCategory.Find(itemId);
        }

        public List<XsiExhibitionCategory> Select()
        {
            return XsiContext.Context.XsiExhibitionCategory.ToList();
        }
        public List<XsiExhibitionCategory> Select(XsiExhibitionCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiExhibitionCategory.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiExhibitionCategory> SelectOr(XsiExhibitionCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionCategory>();
            var Outer = PredicateBuilder.True<XsiExhibitionCategory>();

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.TitleAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.TitleAr.Contains(searchKeys.str1) || p.TitleAr.Contains(searchKeys.str2) || p.TitleAr.Contains(searchKeys.str3) || p.TitleAr.Contains(searchKeys.str4) || p.TitleAr.Contains(searchKeys.str5)
                    || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                    || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                    || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsActiveAr.IsDefault())
                Outer = Outer.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionCategory.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiExhibitionCategory Add(XsiExhibitionCategory entity)
        {
            XsiExhibitionCategory ExhibitionCategory = new XsiExhibitionCategory();
            if (!entity.Title.IsDefault())
                ExhibitionCategory.Title = entity.Title.Trim();
            ExhibitionCategory.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                ExhibitionCategory.TitleAr = entity.TitleAr.Trim();
            ExhibitionCategory.IsActiveAr = entity.IsActiveAr;

            ExhibitionCategory.CreatedOn = entity.CreatedOn;
            ExhibitionCategory.CreatedBy = entity.CreatedBy;
            ExhibitionCategory.ModifiedOn = entity.ModifiedOn;
            ExhibitionCategory.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionCategory.Add(ExhibitionCategory);
            SubmitChanges();
            return ExhibitionCategory;

        }
        public void Update(XsiExhibitionCategory entity)
        {
            XsiExhibitionCategory ExhibitionCategory = XsiContext.Context.XsiExhibitionCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionCategory).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionCategory.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                ExhibitionCategory.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                ExhibitionCategory.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                ExhibitionCategory.IsActiveAr = entity.IsActiveAr;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionCategory.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                ExhibitionCategory.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionCategory entity)
        {
            foreach (XsiExhibitionCategory ExhibitionCategory in Select(entity))
            {
                XsiExhibitionCategory aExhibitionCategory = XsiContext.Context.XsiExhibitionCategory.Find(ExhibitionCategory.ItemId);
                XsiContext.Context.XsiExhibitionCategory.Remove(aExhibitionCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionCategory aExhibitionCategory = XsiContext.Context.XsiExhibitionCategory.Find(itemId);
            XsiContext.Context.XsiExhibitionCategory.Remove(aExhibitionCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
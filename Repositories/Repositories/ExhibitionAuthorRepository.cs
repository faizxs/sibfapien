﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionAuthorRepository : IExhibitionAuthorRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionAuthorRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionAuthorRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionAuthorRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionAuthor GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionAuthor.Find(itemId);
        }
        public List<XsiExhibitionAuthor> GetByGroupId(long groupId)
        {
            return null;
        }
        public List<XsiExhibitionAuthor> Select()
        {
            return XsiContext.Context.XsiExhibitionAuthor.ToList();
        }
        public List<XsiExhibitionAuthor> Select(XsiExhibitionAuthor entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionAuthor>();
            if (!entity.AuthorId.IsDefault())
                predicate = predicate.And(p => p.AuthorId == entity.AuthorId);

            if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            return XsiContext.Context.XsiExhibitionAuthor.AsExpandable().Where(predicate).ToList();
        }
        public Array SelectUI(XsiExhibitionAuthor entity, long exhibitionGroupID)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionAuthor>();
            if (!entity.AuthorId.IsDefault())
                predicate = predicate.And(p => p.AuthorId == entity.AuthorId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (CachingRepository.CachedAuthors == null)
                CachingRepository.CachedAuthors = XsiContext.Context.XsiExhibitionAuthor.ToList();
            if (CachingRepository.CachedBooksParticipating == null)
                CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.ToList();
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            return null;
            //below 5 line code useful
            //var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.IsActive == "Y" && p.BookId != 0).Select(s => s.BookId).Distinct());
            //var AuthorIDs = new HashSet<long>(CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.BookId)
            //&& p.AuthorId != null).Select(s => s.AuthorId.Value).Distinct());
            //var SearchLists = CachingRepository.CachedAuthors.Where(p => AuthorIDs.Contains(p.AuthorId)).ToList();
            //return SearchLists.AsQueryable().AsExpandable().Where(predicate).Take(50).Select(s => new { ItemId = s.AuthorId, Title = s.Title, TitleAr = s.TitleAr }).ToArray();
        }
        public Array SelectUIForCMS(XsiExhibitionAuthor entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionAuthor>();
            var predicateOr = PredicateBuilder.False<XsiExhibitionAuthor>();

            if (!entity.Title.IsDefault())
                predicateOr = predicateOr.Or(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.TitleAr.IsDefault())
                predicateOr = predicateOr.Or(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
            predicate = predicate.And(predicateOr.Expand());

            if (CachingRepository.CachedAuthors == null)
                CachingRepository.CachedAuthors = XsiContext.Context.XsiExhibitionAuthor.ToList();
            var SearchLists = CachingRepository.CachedAuthors.ToList();
            return SearchLists.AsQueryable().AsExpandable().Where(predicate).Take(50).OrderBy(o => o.Title).Select(s => new { AuthorId = s.AuthorId, Title = s.Title, TitleAr = s.TitleAr }).ToArray();
        }

        public List<XsiExhibitionAuthor> SelectSCRFUI(XsiExhibitionAuthor entity, long exhibitionGroupID)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionAuthor>();
            if (!entity.AuthorId.IsDefault())
                predicate = predicate.And(p => p.AuthorId == entity.AuthorId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (CachingRepository.CachedAuthors == null)
                CachingRepository.CachedAuthors = XsiContext.Context.XsiExhibitionAuthor.ToList();
            if (CachingRepository.CachedBooksParticipating == null)
                CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.ToList();
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            return null;
            //below 4 line code useful
            //var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.ExhibitionGroupId == exhibitionGroupID && p.IsActive == "Y" && p.BookId != null).Select(s => s.BookId.Value).Distinct());
            //var AuthorIDs = new HashSet<long>(CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.au) && p.AuthorId != null).Select(s => s.AuthorId.Value).Distinct());
            //var SearchLists = CachingRepository.CachedAuthors.Where(p => AuthorIDs.Contains(p.AuthorId)).ToList();
            //return SearchLists.AsQueryable().AsExpandable().Where(predicate).Take(50).Select(s => new XsiExhibitionAuthor { AuthorId = s.AuthorId, Title = s.Title, TitleAr = s.TitleAr }).ToList();
        }

        public XsiExhibitionAuthor Add(XsiExhibitionAuthor entity)
        {
            XsiExhibitionAuthor ExhibitionAuthor = new XsiExhibitionAuthor();
            ExhibitionAuthor.LanguageId = entity.LanguageId;
            if (!entity.Title.IsDefault())
                ExhibitionAuthor.Title = entity.Title.Trim();
            if (!entity.TitleAr.IsDefault())
                ExhibitionAuthor.TitleAr = entity.TitleAr.Trim();
            ExhibitionAuthor.IsActive = entity.IsActive;
            ExhibitionAuthor.IsActiveAr = entity.IsActiveAr;
            ExhibitionAuthor.CreatedOn = entity.CreatedOn;
            ExhibitionAuthor.CreatedBy = entity.CreatedBy;
            ExhibitionAuthor.ModifiedOn = entity.ModifiedOn;
            ExhibitionAuthor.ModifiedBy = entity.ModifiedBy;
            ExhibitionAuthor.Status = entity.Status;

            XsiContext.Context.XsiExhibitionAuthor.Add(ExhibitionAuthor);
            SubmitChanges();
            //if (CachingRepository.CachedAuthors == null)
            //    CachingRepository.CachedAuthors = XsiContext.Context.XsiExhibitionAuthor.ToList();
            //else
            //    CachingRepository.CachedAuthors.Add(ExhibitionAuthor);
            return ExhibitionAuthor;

        }
        public void Update(XsiExhibitionAuthor entity)
        {
            XsiExhibitionAuthor ExhibitionAuthor = XsiContext.Context.XsiExhibitionAuthor.Find(entity.AuthorId);
            XsiContext.Context.Entry(ExhibitionAuthor).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionAuthor.Title = entity.Title.Trim();
            if (!entity.TitleAr.IsDefault())
                ExhibitionAuthor.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActive.IsDefault())
                ExhibitionAuthor.IsActive = entity.IsActive;
            if (!entity.IsActiveAr.IsDefault())
                ExhibitionAuthor.IsActiveAr = entity.IsActiveAr;
            if (!entity.ModifiedOn.IsDefault())
                ExhibitionAuthor.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                ExhibitionAuthor.ModifiedBy = entity.ModifiedBy;
            if (!entity.Status.IsDefault())
                ExhibitionAuthor.Status = entity.Status;
            if (CachingRepository.CachedAuthors == null)
                CachingRepository.CachedAuthors = XsiContext.Context.XsiExhibitionAuthor.ToList();
            else
            {
                #region Update Cache
                var ItemToModify = CachingRepository.CachedAuthors.Where(p => p.AuthorId == entity.AuthorId).FirstOrDefault();
                if (!entity.Title.IsDefault())
                    ItemToModify.Title = entity.Title.Trim();
                if (!entity.TitleAr.IsDefault())
                    ItemToModify.TitleAr = entity.TitleAr.Trim();
                if (!entity.IsActive.IsDefault())
                    ItemToModify.IsActive = entity.IsActive;
                if (!entity.ModifiedOn.IsDefault())
                    ItemToModify.ModifiedOn = entity.ModifiedOn;
                if (!entity.ModifiedBy.IsDefault())
                    ItemToModify.ModifiedBy = entity.ModifiedBy;
                if (!entity.Status.IsDefault())
                    ItemToModify.Status = entity.Status;
                #endregion
            }
        }
        public void Delete(XsiExhibitionAuthor entity)
        {
            foreach (XsiExhibitionAuthor ExhibitionAuthor in Select(entity))
            {
                XsiExhibitionAuthor aExhibitionAuthor = XsiContext.Context.XsiExhibitionAuthor.Find(ExhibitionAuthor.AuthorId);
                XsiContext.Context.XsiExhibitionAuthor.Remove(aExhibitionAuthor);
                if (CachingRepository.CachedAuthors == null)
                    CachingRepository.CachedAuthors = XsiContext.Context.XsiExhibitionAuthor.ToList();
                CachingRepository.CachedAuthors.Remove(CachingRepository.CachedAuthors.SingleOrDefault(x => x.AuthorId == ExhibitionAuthor.AuthorId));
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionAuthor aExhibitionAuthor = XsiContext.Context.XsiExhibitionAuthor.Find(itemId);
            XsiContext.Context.XsiExhibitionAuthor.Remove(aExhibitionAuthor);
            if (CachingRepository.CachedAuthors == null)
                CachingRepository.CachedAuthors = XsiContext.Context.XsiExhibitionAuthor.ToList();
            CachingRepository.CachedAuthors.Remove(CachingRepository.CachedAuthors.SingleOrDefault(x => x.AuthorId == itemId));
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
        //Custom
        public long GetNameCount(long itemID, string title, string titlear)
        {
            return XsiContext.Context.XsiExhibitionAuthor.Where(p => p.AuthorId != itemID && (p.Title == title || p.TitleAr == titlear)).Count();
        }
    }
}
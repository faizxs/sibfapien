﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
   
    public class ExhibitorBadgeRepository : IExhibitorBadgeRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitorBadgeRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitorBadgeRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitorBadgeRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitorBadges GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitorBadges.Find(itemId);
        }
        public List<XsiExhibitorBadges> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitorBadges> Select()
        {
            return XsiContext.Context.XsiExhibitorBadges.ToList();
        }
        //public List<XsiExhibitorBadges> SelectOr(XsiExhibitorBadges entity)
        //{
        //    bool isInner = false;
        //    var Inner = PredicateBuilder.False<XsiExhibitorBadges>();
        //    var Outer = PredicateBuilder.True<XsiExhibitorBadges>();

        //    if (!entity.Name.IsDefault())
        //    {
        //        string strSearchText = entity.Name;
        //        var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);
        //        Inner = Inner.Or(p => p.Name.Contains(searchKeys.str1) || p.Name.Contains(searchKeys.str2) || p.Name.Contains(searchKeys.str3) || p.Name.Contains(searchKeys.str4) || p.Name.Contains(searchKeys.str5)
        //            || p.Name.Contains(searchKeys.str6) || p.Name.Contains(searchKeys.str7) || p.Name.Contains(searchKeys.str8) || p.Name.Contains(searchKeys.str9) || p.Name.Contains(searchKeys.str10)
        //            || p.Name.Contains(searchKeys.str11) || p.Name.Contains(searchKeys.str12) || p.Name.Contains(searchKeys.str13)
        //            || p.Name.Contains(searchKeys.str14) || p.Name.Contains(searchKeys.str15) || p.Name.Contains(searchKeys.str16) || p.Name.Contains(searchKeys.str17)
        //            );
        //        isInner = true;
        //    }
        //    else
        //    {
        //        if (!entity.Name.IsDefault())
        //        {
        //            string strSearchText = entity.Name.ToLower();
        //            Inner = Inner.Or(p => p.Name.ToLower().Contains(strSearchText));
        //            isInner = true;
        //        }
        //    }

        //    if (!entity.Email.IsDefault())
        //        Outer = Outer.And(p => p.Email.Equals(entity.Email));

        //    if (!entity.EventId.IsDefault())
        //        Outer = Outer.And(p => p.EventId == entity.EventId);

        //    if (!entity.WebsiteId.IsDefault())
        //        Outer = Outer.And(p => p.WebsiteId == entity.WebsiteId);

        //    if (!entity.ExhibitionId.IsDefault())
        //        Outer = Outer.And(p => p.ExhibitionId == entity.ExhibitionId);

        //    if (!entity.Type.IsDefault())
        //        Outer = Outer.And(p => p.Type == entity.Type);

        //    if (!entity.PersonInCharge.IsDefault())
        //        Outer = Outer.And(p => p.PersonInCharge.Contains(entity.PersonInCharge));

        //    if (!entity.ContactNumber.IsDefault())
        //        Outer = Outer.And(p => p.ContactNumber.Contains(entity.ContactNumber));

        //    if (!entity.Emirates.IsDefault())
        //        Outer = Outer.And(p => p.Emirates == entity.Emirates);

        //    if (!entity.NumberOfChildren.IsDefault())
        //        Outer = Outer.And(p => p.NumberOfChildren.Contains(entity.NumberOfChildren));

        //    if (!entity.ChildrenAgeRange.IsDefault())
        //        Outer = Outer.And(p => p.ChildrenAgeRange.Contains(entity.ChildrenAgeRange));

        //    if (!entity.DateOfVisit.IsDefault())
        //        Outer = Outer.And(p => p.DateOfVisit.Equals(entity.DateOfVisit));

        //    if (!entity.ActivityOfInterest.IsDefault())
        //        Outer = Outer.And(p => p.ActivityOfInterest == entity.ActivityOfInterest);

        //    if (isInner)
        //        Outer = Outer.And(Inner.Expand());

        //    return XsiContext.Context.XsiExhibitorBadges.AsExpandable().Where(Outer.Expand()).ToList();
        //}
        public List<XsiExhibitorBadges> Select(XsiExhibitorBadges entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitorBadges>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive == entity.IsActive);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status == entity.Status);

            if (!entity.MemberExhibitionYearlyId.IsDefault())
                predicate = predicate.And(p => p.MemberExhibitionYearlyId == entity.MemberExhibitionYearlyId);

            if (!entity.Name.IsDefault())
                predicate = predicate.And(p => p.Name == entity.Name);

            if (!entity.NameAr.IsDefault())
                predicate = predicate.And(p => p.NameAr == entity.NameAr);

           
            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Equals(entity.Email));

            if (!entity.IsEmailSent.IsDefault())
                predicate = predicate.And(p => p.IsEmailSent.Equals(entity.IsEmailSent));

            if (!entity.ContactNo.IsDefault())
                predicate = predicate.And(p => p.ContactNo.Contains(entity.ContactNo));

            if (!entity.PrintCount.IsDefault())
                predicate = predicate.And(p => p.PrintCount == entity.PrintCount);

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Contains(entity.FileName));

          

            return XsiContext.Context.XsiExhibitorBadges.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitorBadges Add(XsiExhibitorBadges entity)
        {
            XsiExhibitorBadges ExhibitorBadges = new XsiExhibitorBadges();

            if (!entity.Name.IsDefault())
                ExhibitorBadges.Name = entity.Name.Trim();
           
            ExhibitorBadges.LanguageId = entity.LanguageId;
            ExhibitorBadges.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;
            ExhibitorBadges.IsActive = entity.IsActive;
            if (!entity.Status.IsDefault())
                ExhibitorBadges.Status = entity.Status.Trim();
            if (!entity.NameAr.IsDefault())
                ExhibitorBadges.NameAr = entity.NameAr.Trim();
            if (!entity.ContactNo.IsDefault())
                ExhibitorBadges.ContactNo = entity.ContactNo.Trim();
            if (!entity.Email.IsDefault())
                ExhibitorBadges.Email = entity.Email.Trim();
            if (!entity.IsEmailSent.IsDefault())
                ExhibitorBadges.IsEmailSent = entity.IsEmailSent.Trim();
           
            ExhibitorBadges.PrintCount = entity.PrintCount;
            if (!entity.FileName.IsDefault())
                ExhibitorBadges.FileName = entity.FileName.Trim();
            
            ExhibitorBadges.CreatedOn = entity.CreatedOn;
            ExhibitorBadges.CreatedBy = entity.CreatedBy;
            ExhibitorBadges.ModifiedOn = entity.ModifiedOn;
            ExhibitorBadges.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitorBadges.Add(ExhibitorBadges);
            SubmitChanges();
            return ExhibitorBadges;

        }
        public void Update(XsiExhibitorBadges entity)
        {
            XsiExhibitorBadges ExhibitorBadges = XsiContext.Context.XsiExhibitorBadges.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitorBadges).State = EntityState.Modified;

            if (!entity.LanguageId.IsDefault())
                ExhibitorBadges.LanguageId = entity.LanguageId;

            if (!entity.MemberExhibitionYearlyId.IsDefault())
                ExhibitorBadges.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;

            if (!entity.IsActive.IsDefault())
                ExhibitorBadges.IsActive = entity.IsActive;

            if (!entity.Status.IsDefault())
                ExhibitorBadges.Status = entity.Status;

            if (!entity.Name.IsDefault())
                ExhibitorBadges.Name = entity.Name.Trim();

            if (!entity.NameAr.IsDefault())
                ExhibitorBadges.NameAr = entity.NameAr.Trim();

            if (!entity.ContactNo.IsDefault())
                ExhibitorBadges.ContactNo = entity.ContactNo.Trim();


            if (!entity.IsEmailSent.IsDefault())
                ExhibitorBadges.IsEmailSent = entity.IsEmailSent.Trim();

            if (!entity.PrintCount.IsDefault())
                ExhibitorBadges.PrintCount = entity.PrintCount;

            if (!entity.FileName.IsDefault())
                ExhibitorBadges.FileName = entity.FileName.Trim();


            if (!entity.ModifiedOn.IsDefault())
                ExhibitorBadges.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitorBadges.ModifiedBy = entity.ModifiedBy;
            if (!entity.CreatedOn.IsDefault())
                ExhibitorBadges.CreatedOn = entity.CreatedOn;

            if (!entity.CreatedBy.IsDefault())
                ExhibitorBadges.CreatedBy = entity.CreatedBy;
        }
        public void Delete(XsiExhibitorBadges entity)
        {
            foreach (XsiExhibitorBadges ExhibitorBadges in Select(entity))
            {
                XsiExhibitorBadges aExhibitorBadges = XsiContext.Context.XsiExhibitorBadges.Find(ExhibitorBadges.ItemId);
                XsiContext.Context.XsiExhibitorBadges.Remove(aExhibitorBadges);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitorBadges aExhibitorBadges = XsiContext.Context.XsiExhibitorBadges.Find(itemId);
            XsiContext.Context.XsiExhibitorBadges.Remove(aExhibitorBadges);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}

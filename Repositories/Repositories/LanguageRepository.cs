﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;
namespace Xsi.Repositories
{
    public class LanguageRepository : ILanguageRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public LanguageRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public LanguageRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public LanguageRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiLanguages GetById(long itemId)
        {
            if (CachingRepository.CachedLanguage == null)
                CachingRepository.CachedLanguage = XsiContext.Context.XsiLanguages.ToList();
            return CachingRepository.CachedLanguage.AsQueryable().Where(i => i.ItemId == itemId).FirstOrDefault();
        }
        public List<XsiLanguages> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiLanguages> Select()
        {
            if (CachingRepository.CachedLanguage == null)
                CachingRepository.CachedLanguage = XsiContext.Context.XsiLanguages.ToList();
            return CachingRepository.CachedLanguage;
        }
        public List<XsiLanguages> Select(XsiLanguages entity)
        {
            var predicate = PredicateBuilder.True<XsiLanguages>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Name.IsDefault())
                predicate = predicate.And(p => p.Name == entity.Name);

            if (!entity.Direction.IsDefault())
                predicate = predicate.And(p => p.Direction == entity.Direction);

            if (!entity.Culture.IsDefault())
                predicate = predicate.And(p => p.Culture == entity.Culture);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive == entity.IsActive);

            if (!entity.IsDefault.IsDefault())
                predicate = predicate.And(p => p.IsDefault == entity.IsDefault);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (CachingRepository.CachedLanguage == null)
                CachingRepository.CachedLanguage = XsiContext.Context.XsiLanguages.ToList();

            return CachingRepository.CachedLanguage.AsQueryable().Where(predicate).ToList();
            //return XsiContext.Context.XsiLanguages.AsExpandable().Where(predicate).ToList();
        }

        public XsiLanguages Add(XsiLanguages entity)
        {
            XsiLanguages Language = new XsiLanguages();
            if (!entity.Name.IsDefault())
                Language.Name = entity.Name.Trim();
            if (!entity.Direction.IsDefault())
                Language.Direction = entity.Direction.Trim();
            if (!entity.Culture.IsDefault())
                Language.Culture = entity.Culture.Trim();
            Language.IsActive = entity.IsActive;
            Language.IsDefault = entity.IsDefault;
            Language.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiLanguages.Add(Language);
            SubmitChanges();
            return Language;

        }
        public void Update(XsiLanguages entity)
        {
            XsiLanguages Language = XsiContext.Context.XsiLanguages.Find(entity.ItemId);
            XsiContext.Context.Entry(Language).State = EntityState.Modified;

            if (!entity.Name.IsDefault())
                Language.Name = entity.Name.Trim();

            if (!entity.Direction.IsDefault())
                Language.Direction = entity.Direction.Trim();

            if (!entity.Culture.IsDefault())
                Language.Culture = entity.Culture.Trim();

            if (!entity.IsActive.IsDefault())
                Language.IsActive = entity.IsActive;

            if (!entity.IsDefault.IsDefault())
                Language.IsDefault = entity.IsDefault;

            if (!entity.CreatedOn.IsDefault())
                Language.CreatedOn = entity.CreatedOn;
        }
        public void Delete(XsiLanguages entity)
        {
            foreach (XsiLanguages Language in Select(entity))
            {
                XsiLanguages aLanguage = XsiContext.Context.XsiLanguages.Find(Language.ItemId);
                XsiContext.Context.XsiLanguages.Remove(aLanguage);
            }
        }
        public void Delete(long itemId)
        {
            XsiLanguages aLanguage = XsiContext.Context.XsiLanguages.Find(itemId);
            XsiContext.Context.XsiLanguages.Remove(aLanguage);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
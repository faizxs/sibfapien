﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class DiscountRepository : IDiscountRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public DiscountRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public DiscountRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public DiscountRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiDiscount GetById(long itemId)
        {
            return XsiContext.Context.XsiDiscount.Find(itemId);
        }
        public List<XsiDiscount> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiDiscount> Select()
        {
            return XsiContext.Context.XsiDiscount.ToList();
        }
        public List<XsiDiscount> Select(XsiDiscount entity)
        {
            var predicate = PredicateBuilder.True<XsiDiscount>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ExhibitorId.IsDefault())
                predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsManagerChecked.IsDefault())
                predicate = predicate.And(p => p.IsManagerChecked.Equals(entity.IsManagerChecked));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.ParticipationFee.IsDefault())
                predicate = predicate.And(p => p.ParticipationFee.Equals(entity.ParticipationFee));

            if (!entity.AllocatedSpace.IsDefault())
                predicate = predicate.And(p => p.AllocatedSpace.Equals(entity.AllocatedSpace));

            if (!entity.AllocatedSpacePrice.IsDefault())
                predicate = predicate.And(p => p.AllocatedSpacePrice.Equals(entity.AllocatedSpacePrice));

            if (!entity.Total.IsDefault())
                predicate = predicate.And(p => p.Total.Equals(entity.Total));

            if (!entity.DiscountAmount.IsDefault())
                predicate = predicate.And(p => p.DiscountAmount.Equals(entity.DiscountAmount));

            if (!entity.DiscountPercentage.IsDefault())
                predicate = predicate.And(p => p.DiscountPercentage.Equals(entity.DiscountPercentage));

            if (!entity.FinalTotal.IsDefault())
                predicate = predicate.And(p => p.FinalTotal.Contains(entity.FinalTotal));

            if (!entity.Comment.IsDefault())
                predicate = predicate.And(p => p.Comment.Contains(entity.Comment));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedById.IsDefault())
                predicate = predicate.And(p => p.CreatedById == entity.CreatedById);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedById.IsDefault())
                predicate = predicate.And(p => p.ModifiedById == entity.ModifiedById);



            return XsiContext.Context.XsiDiscount.AsExpandable().Where(predicate).ToList();
        }

        public XsiDiscount Add(XsiDiscount entity)
        {
            XsiDiscount Discount = new XsiDiscount();
            Discount.ExhibitorId = entity.ExhibitorId;
            Discount.IsActive = entity.IsActive;
            Discount.IsManagerChecked = entity.IsManagerChecked;
            Discount.Status = entity.Status;
            if (!entity.ParticipationFee.IsDefault())
                Discount.ParticipationFee = entity.ParticipationFee.Trim();
            if (!entity.AllocatedSpace.IsDefault())
                Discount.AllocatedSpace = entity.AllocatedSpace.Trim();
            if (!entity.AllocatedSpacePrice.IsDefault())
                Discount.AllocatedSpacePrice = entity.AllocatedSpacePrice.Trim();
            if (!entity.Total.IsDefault())
                Discount.Total = entity.Total.Trim();
            if (!entity.DiscountAmount.IsDefault())
                Discount.DiscountAmount = entity.DiscountAmount.Trim();
            if (!entity.DiscountPercentage.IsDefault())
                Discount.DiscountPercentage = entity.DiscountPercentage.Trim();
            if (!entity.FinalTotal.IsDefault())
                Discount.FinalTotal = entity.FinalTotal.Trim();
            if (!entity.Comment.IsDefault())
                Discount.Comment = entity.Comment.Trim();
            Discount.CreatedOn = entity.CreatedOn;
            Discount.CreatedById = entity.CreatedById;
            Discount.ModifiedOn = entity.ModifiedOn;
            Discount.ModifiedById = entity.ModifiedById;

            XsiContext.Context.XsiDiscount.Add(Discount);
            SubmitChanges();
            return Discount;

        }
        public void Update(XsiDiscount entity)
        {
            XsiDiscount Discount = XsiContext.Context.XsiDiscount.Find(entity.ItemId);
            XsiContext.Context.Entry(Discount).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                Discount.IsActive = entity.IsActive;

            if (!entity.IsManagerChecked.IsDefault())
                Discount.IsManagerChecked = entity.IsManagerChecked;

            if (!entity.Status.IsDefault())
                Discount.Status = entity.Status;

            if (!entity.ParticipationFee.IsDefault())
                Discount.ParticipationFee = entity.ParticipationFee.Trim();

            if (!entity.AllocatedSpace.IsDefault())
                Discount.AllocatedSpace = entity.AllocatedSpace.Trim();

            if (!entity.AllocatedSpacePrice.IsDefault())
                Discount.AllocatedSpacePrice = entity.AllocatedSpacePrice.Trim();

            if (!entity.Total.IsDefault())
                Discount.Total = entity.Total;

            if (!entity.DiscountAmount.IsDefault())
                Discount.DiscountAmount = entity.DiscountAmount.Trim();

            if (!entity.DiscountPercentage.IsDefault())
                Discount.DiscountPercentage = entity.DiscountPercentage.Trim();

            if (!entity.FinalTotal.IsDefault())
                Discount.FinalTotal = entity.FinalTotal.Trim();

            if (!entity.Comment.IsDefault())
                Discount.Comment = entity.Comment.Trim();

            if (!entity.ModifiedOn.IsDefault())
                Discount.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedById.IsDefault())
                Discount.ModifiedById = entity.ModifiedById;

        }
        public void Delete(XsiDiscount entity)
        {
            foreach (XsiDiscount Discount in Select(entity))
            {
                XsiDiscount aDiscount = XsiContext.Context.XsiDiscount.Find(Discount.ItemId);
                XsiContext.Context.XsiDiscount.Remove(aDiscount);
            }
        }
        public void Delete(long itemId)
        {
            XsiDiscount aDiscount = XsiContext.Context.XsiDiscount.Find(itemId);
            XsiContext.Context.XsiDiscount.Remove(aDiscount);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
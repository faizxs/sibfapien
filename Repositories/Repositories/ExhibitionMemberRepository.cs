﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionMemberRepository : IExhibitionMemberRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionMemberRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionMemberRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionMemberRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionMember GetById(long MemberId)
        {
            return XsiContext.Context.XsiExhibitionMember.Find(MemberId);
        }
        public List<XsiExhibitionMember> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionMember> Select()
        {
            return XsiContext.Context.XsiExhibitionMember.ToList();
        }
        public List<XsiExhibitionMember> Select(XsiExhibitionMember entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMember>();
            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);

            if (!entity.LanguageUrl.IsDefault())
                predicate = predicate.And(p => p.LanguageUrl == entity.LanguageUrl);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.CityId.IsDefault())
                predicate = predicate.And(p => p.CityId == entity.CityId);

            if (!entity.OtherCity.IsDefault())
                predicate = predicate.And(p => p.OtherCity.Contains(entity.OtherCity));

            //if (!entity.Firstname.IsDefault())
            //    predicate = predicate.And(p => p.Firstname.Contains(entity.Firstname));

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));

            if (!entity.Firstname.IsDefault())
                predicate = predicate.And(p => (p.Firstname + " " + p.LastName).Contains(entity.Firstname));

            if (!entity.FirstNameAr.IsDefault())
                predicate = predicate.And(p => p.FirstNameAr.Contains(entity.FirstNameAr));

            if (!entity.LastNameAr.IsDefault())
                predicate = predicate.And(p => (p.FirstNameAr + " " + p.LastNameAr).Contains(entity.LastNameAr));

            if (!entity.FileNumber.IsDefault())
                predicate = predicate.And(p => p.FileNumber.Contains(entity.FileNumber));

            //if (!entity.Manager.IsDefault())
            //    predicate = predicate.And(p => p.Manager.Contains(entity.Manager));

            if (!entity.ContactPerson.IsDefault())
                predicate = predicate.And(p => p.ContactPerson.Contains(entity.ContactPerson));

            if (!entity.JobTitle.IsDefault())
                predicate = predicate.And(p => p.JobTitle.Contains(entity.JobTitle));

            if (!entity.JobTitleAr.IsDefault())
                predicate = predicate.And(p => p.JobTitleAr.Contains(entity.JobTitleAr));

            if (!entity.CompanyFieldofWork.IsDefault())
                predicate = predicate.And(p => p.CompanyFieldofWork.Contains(entity.CompanyFieldofWork));

            if (!entity.CompanyFieldofWorkAr.IsDefault())
                predicate = predicate.And(p => p.CompanyFieldofWorkAr.Contains(entity.CompanyFieldofWorkAr));

            if (!entity.Phone.IsDefault())
                predicate = predicate.And(p => p.Phone.Contains(entity.Phone));

            if (!entity.Mobile.IsDefault())
                predicate = predicate.And(p => p.Mobile.Contains(entity.Mobile));

            if (!entity.Fax.IsDefault())
                predicate = predicate.And(p => p.Fax.Contains(entity.Fax));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Equals(entity.Email));

            if (!entity.EmailNew.IsDefault())
                predicate = predicate.And(p => p.EmailNew.Equals(entity.EmailNew));

            if (!entity.Website.IsDefault())
                predicate = predicate.And(p => p.Website.Contains(entity.Website));

            if (!entity.Pobox.IsDefault())
                predicate = predicate.And(p => p.Pobox.Contains(entity.Pobox));

            if (!entity.Address.IsDefault())
                predicate = predicate.And(p => p.Address.Contains(entity.Address));

            if (!entity.UserName.IsDefault())
                predicate = predicate.And(p => p.UserName == entity.UserName);

            if (!entity.Password.IsDefault())
                predicate = predicate.And(p => p.Password.Equals(entity.Password));

            //if (!entity.PasswordHash.IsDefault())
            //    predicate = predicate.And(p => p.PasswordHash.Equals(entity.PasswordHash));

            //if (!entity.PasswordSalt.IsDefault())
            //    predicate = predicate.And(p => p.PasswordSalt.Equals(entity.PasswordSalt));

            if (!entity.IsVerified.IsDefault())
                predicate = predicate.And(p => p.IsVerified.Equals(entity.IsVerified));

            if (!entity.IsEmailSent.IsDefault())
                predicate = predicate.And(p => p.IsEmailSent.Equals(entity.IsEmailSent));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.EditRequest.IsDefault())
                predicate = predicate.And(p => p.EditRequest.Equals(entity.EditRequest));

            if (!entity.FirstNameEmail.IsDefault())
                predicate = predicate.And(p => p.FirstNameEmail.Contains(entity.FirstNameEmail));

            if (!entity.LastNameEmail.IsDefault())
                predicate = predicate.And(p => (p.FirstNameEmail + " " + p.LastNameEmail).Contains(entity.LastNameEmail));

            if (!entity.Brief.IsDefault())
                predicate = predicate.And(p => p.Brief.Contains(entity.Brief));

            if (!entity.BriefAr.IsDefault())
                predicate = predicate.And(p => p.BriefAr.Contains(entity.BriefAr));

            if (!entity.IsSibfdepartment.IsDefault())
                predicate = predicate.And(p => p.IsSibfdepartment.Equals(entity.IsSibfdepartment));

            if (!entity.SibfdepartmentId.IsDefault())
                predicate = predicate.And(p => p.SibfdepartmentId==entity.SibfdepartmentId);

            if (!entity.IsBooksRequired.IsDefault())
                predicate = predicate.And(p => p.IsBooksRequired.Equals(entity.IsBooksRequired));

            if (!entity.Notes.IsDefault())
                predicate = predicate.And(p => p.Notes.Contains(entity.Notes));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn.Equals(entity.CreatedOn));

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn.Equals(entity.ModifiedOn));

            if (!entity.ModifiedById.IsDefault())
                predicate = predicate.And(p => p.ModifiedById == entity.ModifiedById);
             
            return XsiContext.Context.XsiExhibitionMember.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionMember> SelectOr(XsiExhibitionMember entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionMember>();
             if (!entity.LastName.IsDefault())
            {
                Inner = Inner.Or(p => p.LastName.Contains(entity.LastName));
                isInner = true;
            }
            if (!entity.Firstname.IsDefault())
            {
                Inner = Inner.Or(p => (p.Firstname + " " + p.LastName).Contains(entity.Firstname));
                isInner = true;
            }
            if (!entity.LastNameAr.IsDefault())
            {
                string strSearchText = entity.LastNameAr;
                string str1 = strSearchText;
                string str2 = strSearchText.Replace("أ", "إ");
                string str3 = strSearchText.Replace("أ", "ا");
                string str4 = strSearchText.Replace("إ", "أ");
                string str5 = strSearchText.Replace("إ", "ا");
                string str6 = strSearchText.Replace("ا", "أ");
                string str7 = strSearchText.Replace("ا", "إ");

                string str8 = strSearchText.Replace("ا", "إ");
                string str9 = strSearchText.Replace("ا", "أ");

                string str10 = strSearchText.Replace("إ", "أ");
                string str11 = strSearchText.Replace("إ", "ا");

                string str12 = strSearchText.Replace("إ", "أ");
                string str13 = strSearchText.Replace("إ", "ا");

                string str14 = strSearchText.Replace("ه", "ة");
                string str15 = strSearchText.Replace("ة", "ه");

                string str16 = strSearchText.Replace("ى", "ي");
                string str17 = strSearchText.Replace("ي", "ى");

                // Inner = Inner.Or(p => p.LastNameAr.Contains(entity.LastNameAr));
                Inner = Inner.Or(p => p.LastNameAr.Contains(str1) || p.LastNameAr.Contains(str2) || p.LastNameAr.Contains(str3) || p.LastNameAr.Contains(str4) || p.LastNameAr.Contains(str5)
                || p.LastNameAr.Contains(str6) || p.LastNameAr.Contains(str7) || p.LastNameAr.Contains(str8) || p.LastNameAr.Contains(str9) || p.LastNameAr.Contains(str10)
                || p.LastNameAr.Contains(str11) || p.LastNameAr.Contains(str12) || p.LastNameAr.Contains(str13)
                 || p.LastNameAr.Contains(str14) || p.LastNameAr.Contains(str15) || p.LastNameAr.Contains(str16) || p.LastNameAr.Contains(str17)
                );
                isInner = true;
            }
            if (!entity.FirstNameAr.IsDefault())
            {
                //  Inner = Inner.Or(p => (p.FirstNameAr + " " + p.LastNameAr).Contains(entity.FirstNameAr));
                string strSearchText = entity.FirstNameAr;
                string str1 = strSearchText;
                string str2 = strSearchText.Replace("أ", "إ");
                string str3 = strSearchText.Replace("أ", "ا");
                string str4 = strSearchText.Replace("إ", "أ");
                string str5 = strSearchText.Replace("إ", "ا");
                string str6 = strSearchText.Replace("ا", "أ");
                string str7 = strSearchText.Replace("ا", "إ");

                string str8 = strSearchText.Replace("ا", "إ");
                string str9 = strSearchText.Replace("ا", "أ");

                string str10 = strSearchText.Replace("إ", "أ");
                string str11 = strSearchText.Replace("إ", "ا");

                string str12 = strSearchText.Replace("إ", "أ");
                string str13 = strSearchText.Replace("إ", "ا");

                string str14 = strSearchText.Replace("ه", "ة");
                string str15 = strSearchText.Replace("ة", "ه");

                string str16 = strSearchText.Replace("ى", "ي");
                string str17 = strSearchText.Replace("ي", "ى");

                Inner = Inner.Or(p => (p.FirstNameAr + " " + p.LastNameAr).Contains(entity.FirstNameAr) || p.FirstNameAr.Contains(str1) || p.FirstNameAr.Contains(str2) || p.FirstNameAr.Contains(str3) || p.FirstNameAr.Contains(str4) || p.FirstNameAr.Contains(str5)
                || p.FirstNameAr.Contains(str6) || p.FirstNameAr.Contains(str7) || p.FirstNameAr.Contains(str8) || p.FirstNameAr.Contains(str9) || p.FirstNameAr.Contains(str10)
                || p.FirstNameAr.Contains(str11) || p.FirstNameAr.Contains(str12) || p.FirstNameAr.Contains(str13)
                 || p.FirstNameAr.Contains(str14) || p.FirstNameAr.Contains(str15) || p.FirstNameAr.Contains(str16) || p.FirstNameAr.Contains(str17)
                );
                isInner = true;
            }
            if (!entity.FileNumber.IsDefault())
            {
                Inner = Inner.Or(p => p.FileNumber.Contains(entity.FileNumber));
                isInner = true;
            }
            var Outer = PredicateBuilder.True<XsiExhibitionMember>();

            if (!entity.Status.IsDefault())
                Outer = Outer.And(p => p.Status.Equals(entity.Status));
            
            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionMember.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public XsiExhibitionMember Add(XsiExhibitionMember entity)
        {
            XsiExhibitionMember ExhibitionMember = new XsiExhibitionMember();
            ExhibitionMember.LanguageUrl = entity.LanguageUrl;

            if (!entity.WebsiteId.IsDefault())
                ExhibitionMember.WebsiteId = entity.WebsiteId;

            ExhibitionMember.CityId = entity.CityId;
            if (!entity.OtherCity.IsDefault())
                ExhibitionMember.OtherCity = entity.OtherCity.Trim();
            if (!entity.Firstname.IsDefault())
                ExhibitionMember.Firstname = entity.Firstname.Trim();
            if (!entity.LastName.IsDefault())
                ExhibitionMember.LastName = entity.LastName.Trim();
            if (!entity.FirstNameAr.IsDefault())
                ExhibitionMember.FirstNameAr = entity.FirstNameAr.Trim();
            if (!entity.LastNameAr.IsDefault())
                ExhibitionMember.LastNameAr = entity.LastNameAr.Trim();
            if (!entity.FileNumber.IsDefault())
                ExhibitionMember.FileNumber = entity.FileNumber.Trim();
              
            if (!entity.Phone.IsDefault())
                ExhibitionMember.Phone = entity.Phone.Trim();
            if (!entity.Mobile.IsDefault())
                ExhibitionMember.Mobile = entity.Mobile.Trim();
            if (!entity.Fax.IsDefault())
                ExhibitionMember.Fax = entity.Fax.Trim();
            if (!entity.Email.IsDefault())
                ExhibitionMember.Email = entity.Email.Trim();
            if (!entity.EmailNew.IsDefault())
                ExhibitionMember.EmailNew = entity.EmailNew.Trim();
            if (!entity.Website.IsDefault())
                ExhibitionMember.Website = entity.Website.Trim();
            if (!entity.Pobox.IsDefault())
                ExhibitionMember.Pobox = entity.Pobox.Trim();
            if (!entity.Address.IsDefault())
                ExhibitionMember.Address = entity.Address.Trim();
            if (!entity.UserName.IsDefault())
                ExhibitionMember.UserName = entity.UserName.Trim();

            if (!entity.Password.IsDefault())
                ExhibitionMember.Password = entity.Password.Trim();

            if (!entity.PasswordHash.IsDefault())
                ExhibitionMember.PasswordHash = entity.PasswordHash;

            if (!entity.PasswordSalt.IsDefault())
                ExhibitionMember.PasswordSalt = entity.PasswordSalt;

            ExhibitionMember.IsVerified = entity.IsVerified;
            ExhibitionMember.IsEmailSent = entity.IsEmailSent;
            ExhibitionMember.Status = entity.Status;
            ExhibitionMember.EditRequest = entity.EditRequest;
            //if (!entity.Brief.IsDefault())
            //    ExhibitionMember.Brief = entity.Brief.Trim();
            //if (!entity.BriefAr.IsDefault())
            //    ExhibitionMember.BriefAr = entity.BriefAr.Trim();

            if (!entity.JobTitle.IsDefault())
                ExhibitionMember.JobTitle = entity.JobTitle.Trim();

            if (!entity.JobTitleAr.IsDefault())
                ExhibitionMember.JobTitleAr = entity.JobTitleAr.Trim();

            if (!entity.CompanyFieldofWork.IsDefault())
                ExhibitionMember.CompanyFieldofWork = entity.CompanyFieldofWork.Trim();

            if (!entity.CompanyFieldofWorkAr.IsDefault())
                ExhibitionMember.CompanyFieldofWorkAr = entity.CompanyFieldofWorkAr.Trim();

            if (!entity.ContactPerson.IsDefault())
                ExhibitionMember.ContactPerson = entity.ContactPerson.Trim();

            if (!entity.CompanyName.IsDefault())
                ExhibitionMember.CompanyName = entity.CompanyName.Trim();
            if (!entity.CompanyNameAr.IsDefault())
                ExhibitionMember.CompanyNameAr = entity.CompanyNameAr.Trim();
          
            if (entity.LanguageUrl == 1)
            {
                if (!entity.FirstNameEmail.IsDefault())
                    ExhibitionMember.FirstNameEmail = entity.Firstname.Trim();
                if (!entity.LastNameEmail.IsDefault())
                    ExhibitionMember.LastNameEmail = entity.LastName.Trim();
            }
            else
            {
                if (entity.FirstNameAr != null && !string.IsNullOrEmpty(entity.FirstNameAr) && entity.LastNameAr != null && !string.IsNullOrEmpty(entity.LastNameAr))
                {
                    if (!entity.FirstNameEmail.IsDefault())
                        ExhibitionMember.FirstNameEmail = entity.FirstNameAr.Trim();
                    if (!entity.LastNameEmail.IsDefault())
                        ExhibitionMember.LastNameEmail = entity.LastNameAr.Trim();
                }
                else
                {
                    if (!entity.FirstNameEmail.IsDefault())
                        ExhibitionMember.FirstNameEmail = entity.Firstname.Trim();
                    if (!entity.LastNameEmail.IsDefault())
                        ExhibitionMember.LastNameEmail = entity.LastName.Trim();
                }
            }
            ExhibitionMember.IsSibfdepartment = entity.IsSibfdepartment;
            ExhibitionMember.SibfdepartmentId = entity.SibfdepartmentId;
            ExhibitionMember.IsLibraryMember = entity.IsLibraryMember;

            if (!entity.IsBooksRequired.IsDefault())
                ExhibitionMember.IsBooksRequired = entity.IsBooksRequired.Trim();

            if (!entity.Notes.IsDefault())
                ExhibitionMember.Notes = entity.Notes.Trim();

            ExhibitionMember.CreatedOn = entity.CreatedOn;
            ExhibitionMember.ModifiedOn = entity.ModifiedOn;
            ExhibitionMember.ModifiedById = entity.ModifiedById;
            
            XsiContext.Context.XsiExhibitionMember.Add(ExhibitionMember);
            SubmitChanges();
            return ExhibitionMember;

        }
        public void Update(XsiExhibitionMember entity)
        {
            XsiExhibitionMember ExhibitionMember = XsiContext.Context.XsiExhibitionMember.Find(entity.MemberId);
            XsiContext.Context.Entry(ExhibitionMember).State = EntityState.Modified;

            if (!entity.LanguageUrl.IsDefault())
                ExhibitionMember.LanguageUrl = entity.LanguageUrl;

            //if (!entity.WebsiteId.IsDefault())
            //    ExhibitionMember.WebsiteId = entity.WebsiteId;

            if (!entity.CityId.IsDefault())
                ExhibitionMember.CityId = entity.CityId;

            if (!entity.OtherCity.IsDefault())
                ExhibitionMember.OtherCity = entity.OtherCity.Trim();

            if (!entity.Firstname.IsDefault())
                ExhibitionMember.Firstname = entity.Firstname.Trim();

            if (!entity.LastName.IsDefault())
                ExhibitionMember.LastName = entity.LastName.Trim();

            if (!entity.FirstNameAr.IsDefault())
                ExhibitionMember.FirstNameAr = entity.FirstNameAr.Trim();

            if (!entity.LastNameAr.IsDefault())
                ExhibitionMember.LastNameAr = entity.LastNameAr.Trim();

            if (!entity.FileNumber.IsDefault())
                ExhibitionMember.FileNumber = entity.FileNumber.Trim();
             
            if (!entity.Phone.IsDefault())
                ExhibitionMember.Phone = entity.Phone.Trim();

            if (!entity.Mobile.IsDefault())
                ExhibitionMember.Mobile = entity.Mobile.Trim();

            if (!entity.Fax.IsDefault())
                ExhibitionMember.Fax = entity.Fax.Trim();

            if (!entity.Email.IsDefault())
                ExhibitionMember.Email = entity.Email.Trim();

            if (!entity.EmailNew.IsDefault())
                ExhibitionMember.EmailNew = entity.EmailNew.Trim();

            if (!entity.Website.IsDefault())
                ExhibitionMember.Website = entity.Website.Trim();

            if (!entity.Pobox.IsDefault())
                ExhibitionMember.Pobox = entity.Pobox.Trim();

            if (!entity.Address.IsDefault())
                ExhibitionMember.Address = entity.Address.Trim();

            //if (!entity.UserName.IsDefault())
            //    ExhibitionMember.UserName = entity.UserName;

            if (!entity.Password.IsDefault())
                ExhibitionMember.Password = entity.Password.Trim();

            if (!entity.PasswordHash.IsDefault())
                ExhibitionMember.PasswordHash = entity.PasswordHash;

            if (!entity.PasswordSalt.IsDefault())
                ExhibitionMember.PasswordSalt = entity.PasswordSalt;

            if (!entity.IsVerified.IsDefault())
                ExhibitionMember.IsVerified = entity.IsVerified;

            if (!entity.IsEmailSent.IsDefault())
                ExhibitionMember.IsEmailSent = entity.IsEmailSent;

            if (!entity.Status.IsDefault())
                ExhibitionMember.Status = entity.Status;

            if (!entity.EditRequest.IsDefault())
                ExhibitionMember.EditRequest = entity.EditRequest;

            //if (!entity.Brief.IsDefault())
            //    ExhibitionMember.Brief = entity.Brief.Trim();
            //if (!entity.BriefAr.IsDefault())
            //    ExhibitionMember.BriefAr = entity.BriefAr.Trim();
           
            if (!entity.JobTitle.IsDefault())
                ExhibitionMember.JobTitle = entity.JobTitle.Trim();

            if (!entity.JobTitleAr.IsDefault())
                ExhibitionMember.JobTitleAr = entity.JobTitleAr.Trim();

            if (!entity.CompanyFieldofWork.IsDefault())
                ExhibitionMember.CompanyFieldofWork = entity.CompanyFieldofWork.Trim();

            if (!entity.CompanyFieldofWorkAr.IsDefault())
                ExhibitionMember.CompanyFieldofWorkAr = entity.CompanyFieldofWorkAr.Trim();

            if (!entity.ContactPerson.IsDefault())
                ExhibitionMember.ContactPerson = entity.ContactPerson.Trim();

            if (!entity.CompanyName.IsDefault())
                ExhibitionMember.CompanyName = entity.CompanyName.Trim();
            if (!entity.CompanyNameAr.IsDefault())
                ExhibitionMember.CompanyNameAr = entity.CompanyNameAr.Trim();

            if (entity.LanguageUrl == 1)
            {
                if (!entity.FirstNameEmail.IsDefault())
                    ExhibitionMember.FirstNameEmail = entity.Firstname.Trim();

                if (!entity.LastNameEmail.IsDefault())
                    ExhibitionMember.LastNameEmail = entity.LastName.Trim();
            }
            else
            {
                if (entity.FirstNameAr != null && !string.IsNullOrEmpty(entity.FirstNameAr) && entity.LastNameAr != null && !string.IsNullOrEmpty(entity.LastNameAr))
                {
                    if (!entity.FirstNameAr.IsDefault())
                        ExhibitionMember.FirstNameEmail = entity.FirstNameAr.Trim();

                    if (!entity.LastNameAr.IsDefault())
                        ExhibitionMember.LastNameEmail = entity.LastNameAr.Trim();
                }
                else
                {
                    if (!entity.FirstNameEmail.IsDefault())
                        ExhibitionMember.FirstNameEmail = entity.Firstname.Trim();

                    if (!entity.LastNameEmail.IsDefault())
                        ExhibitionMember.LastNameEmail = entity.LastName.Trim();
                }
            }

            if (!entity.IsSibfdepartment.IsDefault())
                ExhibitionMember.IsSibfdepartment = entity.IsSibfdepartment;
            
            if (!entity.SibfdepartmentId.IsDefault())
                ExhibitionMember.SibfdepartmentId = entity.SibfdepartmentId;

            if (!entity.IsLibraryMember.IsDefault())
                ExhibitionMember.IsLibraryMember = entity.IsLibraryMember;

            if (!entity.IsBooksRequired.IsDefault())
                ExhibitionMember.IsBooksRequired = entity.IsBooksRequired;

            if (!entity.Notes.IsDefault())
                ExhibitionMember.Notes = entity.Notes;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionMember.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedById.IsDefault())
                ExhibitionMember.ModifiedById = entity.ModifiedById;
        }
        public void Delete(XsiExhibitionMember entity)
        {
            foreach (XsiExhibitionMember ExhibitionMember in Select(entity))
            {
                XsiExhibitionMember aExhibitionMember = XsiContext.Context.XsiExhibitionMember.Find(ExhibitionMember.MemberId);
                XsiContext.Context.XsiExhibitionMember.Remove(aExhibitionMember);
            }
        }
        public void Delete(long MemberId)
        {
            XsiExhibitionMember aExhibitionMember = XsiContext.Context.XsiExhibitionMember.Find(MemberId);
            XsiContext.Context.XsiExhibitionMember.Remove(aExhibitionMember);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
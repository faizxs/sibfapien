﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class EventGuestRepository : IEventGuestRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public EventGuestRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public EventGuestRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public EventGuestRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiEventGuest GetById(long itemId)
        {
            return XsiContext.Context.XsiEventGuest.Find(itemId);
        }
        public List<XsiEventGuest> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiEventGuest> Select()
        {
            return XsiContext.Context.XsiEventGuest.ToList();
        }
        public List<XsiEventGuest> Select(XsiEventGuest entity)
        {
            var predicate = PredicateBuilder.True<XsiEventGuest>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.EventId.IsDefault())
                predicate = predicate.And(p => p.EventId == entity.EventId);

            if (!entity.GuestId.IsDefault())
                predicate = predicate.And(p => p.GuestId == entity.GuestId);

            return XsiContext.Context.XsiEventGuest.AsExpandable().Where(predicate).ToList();
        }

        public XsiEventGuest Add(XsiEventGuest entity)
        {
            XsiEventGuest EventGuest = new XsiEventGuest();
            EventGuest.EventId = entity.EventId;
            EventGuest.GuestId = entity.GuestId;
            XsiContext.Context.XsiEventGuest.Add(EventGuest);
            SubmitChanges();
            return EventGuest;

        }
        public void Update(XsiEventGuest entity)
        {
            XsiEventGuest EventGuest = XsiContext.Context.XsiEventGuest.Find(entity.ItemId);
            XsiContext.Context.Entry(EventGuest).State = EntityState.Modified;

            if (!entity.EventId.IsDefault())
                EventGuest.EventId = entity.EventId;

            if (!entity.GuestId.IsDefault())
                EventGuest.GuestId = entity.GuestId;
        }
        public void Delete(XsiEventGuest entity)
        {
            foreach (XsiEventGuest EventGuest in Select(entity))
            {
                XsiEventGuest aEventGuest = XsiContext.Context.XsiEventGuest.Find(EventGuest.ItemId);
                XsiContext.Context.XsiEventGuest.Remove(aEventGuest);
            }
        }
        public void Delete(long itemId)
        {
            XsiEventGuest aEventGuest = XsiContext.Context.XsiEventGuest.Find(itemId);
            XsiContext.Context.XsiEventGuest.Remove(aEventGuest);
        }

        /*public GetEvents_Result GetEventbyGuest(long guestId)
        {
            HashSet<long> EventIds = new HashSet<long>(XsiContext.Context.XsiEventGuest.Where(x => x.GuestId == guestId).Select(s => s.EventId.Value));
            return (from ed in XsiContext.Context.XsiEventDate
                    join ev in XsiContext.Context.XsiEvents on ed.EventId equals ev.ItemId
                    where  ed.StartDate > DateTime.Now
                    && EventIds.Contains(ed.EventId.Value)
                    orderby ed.StartDate
                    select new
                    {
                        EventItemId = ed.ItemId,
                        GroupId = ed.EventId,
                        StartDate1 = ed.StartDate,
                        EndDate1 = ed.EndDate,
                        StartDateNew1 = ed.StartDate,
                        EndDateNew1 = ed.EndDate,
                        Location = ev.Location,
                        FileName = ev.FileName,
                        EventThumbnailNew = ev.EventThumbnailNew,
                        InnerTopImageOne = ev.InnerTopImageOne,
                        InnerTopImageTwo = ev.InnerTopImageTwo
                    }).ToList().Select(x => new GetEvents_Result
                    {
                        EventItemId = x.EventItemId,
                        GroupId = x.GroupId,
                        StartDate1 = x.StartDate1,
                        EndDate1 = x.EndDate1,
                        StartDateNew1 = x.StartDate1,
                        EndDateNew1 = x.EndDate1,
                        Location = x.Location,
                        FileName = x.FileName,
                        //EventThumbnailNew = x.EventThumbnailNew,
                        //InnerTopImageOne = x.InnerTopImageOne,
                        //InnerTopImageTwo = x.InnerTopImageTwo
                    }).FirstOrDefault();
        }

        public List<GetEvents_Result> GetEventbyGuestList(long guestId)
        {
            HashSet<long> EventIds = new HashSet<long>(XsiContext.Context.XsiEventGuest.Where(x => x.GuestId == guestId).Select(s => s.EventId.Value));
            return (from ed in XsiContext.Context.XsiEventDate
                    join ev in XsiContext.Context.XsiEvents.Where(x => x.IsActive == "Y") on ed.EventId equals ev.ItemId
                    where ed.StartDate > DateTime.Now
                    && EventIds.Contains(ed.EventId.Value) && (ev.Type == "A" || ev.Type == "B")
                    orderby ed.StartDate
                    select new
                    {
                        EventItemId = ed.ItemId,
                        GroupId = ed.EventId,
                        StartDate1 = ed.StartDate,
                        EndDate1 = ed.EndDate,
                        StartDateNew1 = ed.StartDate,
                        EndDateNew1 = ed.EndDate,
                        Location = ev.Location,
                        Guest = ev.Guest,
                        Title = ev.Title
                    }).ToList().Select(x => new GetEvents_Result
                    {
                        EventItemId = x.EventItemId,
                        GroupId = x.GroupId,
                        StartDate1 = x.StartDate1,
                        EndDate1 = x.EndDate1,
                        StartDateNew1 = x.StartDate1,
                        EndDateNew1 = x.EndDate1,
                        Location = x.Location,
                        Guest = x.Guest,
                        Title = x.Title
                    }).ToList();
        }

        public List<GetEvents_Result> GetActivityList(string type, long guestGid)
        {
            HashSet<long> eventguestGids = new HashSet<long>();

            HashSet<long> EventIds = new HashSet<long>();
            HashSet<long> EventIdsList = new HashSet<long>(XsiContext.Context.XsiEventWebsite.Where(p => p.WebsiteId == 1).Select(s => s.EventId.Value).Distinct());

            if (guestGid != -1)
            {
                eventguestGids = new HashSet<long>(XsiContext.Context.XsiEventGuest.Where(p => p.GuestId == guestGid).Select(s => s.EventId.Value).Distinct());
                EventIds = new HashSet<long>(XsiContext.Context.XsiEvents.Where(p => eventguestGids.Contains(p.ItemId) && EventIdsList.Contains(p.ItemId) &&p.IsActive == "Y" && (p.Type == type || p.Type == "B")).Select(s => s.ItemId).Distinct().ToList());
            }
            else
            {
                EventIds = new HashSet<long>(XsiContext.Context.XsiEvents.Where(p => EventIdsList.Contains(p.ItemId) && p.IsActive == "Y" && (p.Type == type || p.Type == "B")).Select(s => s.ItemId).Distinct().ToList());
            }
             
            return (from ed in XsiContext.Context.XsiEventDate
                    join ev in XsiContext.Context.XsiEvents on ed.EventId equals ev.ItemId
                    where ed.StartDate > DateTime.Now
                    && EventIds.Contains(ed.EventId.Value) && (ev.Type == type || ev.Type == "B")
                    orderby ed.StartDate
                    select new
                    {
                        EventItemId = ed.ItemId,
                        GroupId = ed.EventId,
                        StartDate1 = ed.StartDate,
                        EndDate1 = ed.EndDate,
                        StartDateNew1 = ed.StartDate,
                        EndDateNew1 = ed.EndDate,
                        Location = ev.Location,
                        Title = ev.Title,
                        Overview = ev.Overview,
                        Guest = ev.Guest,
                        CategoryId = ev.EventSubCategoryId,
                        FileName = ev.FileName,
                        EventThumbnailNew = ev.EventThumbnailNew,
                        InnerTopImageOne = ev.InnerTopImageOne,
                        InnerTopImageTwo = ev.InnerTopImageTwo
                    }).ToList().Select(x => new GetEvents_Result
                    {
                        EventItemId = x.EventItemId,
                        GroupId = x.GroupId,
                        StartDate1 = x.StartDate1,
                        EndDate1 = x.EndDate1,
                        StartDateNew1 = x.StartDate1,
                        EndDateNew1 = x.EndDate1,
                        Location = x.Location,
                        Title = x.Title,
                        Overview = x.Overview,
                        Guest = x.Guest,
                        CategoryId = x.CategoryId,
                        FileName = x.FileName,
                        //EventThumbnailNew = x.EventThumbnailNew,
                        //InnerTopImageOne = x.InnerTopImageOne,
                        //InnerTopImageTwo = x.InnerTopImageTwo
                    }).ToList();
        }*/

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
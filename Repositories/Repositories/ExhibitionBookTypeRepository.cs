﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionBookTypeRepository : IExhibitionBookTypeRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionBookTypeRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionBookTypeRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionBookTypeRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionBookType GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionBookType.Find(itemId);
        }
      
        public List<XsiExhibitionBookType> Select()
        {
            return XsiContext.Context.XsiExhibitionBookType.ToList();
        }
        public List<XsiExhibitionBookType> Select(XsiExhibitionBookType entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBookType>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionBookType.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionBookType> SelectOr(XsiExhibitionBookType entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionBookType>();
            var Outer = PredicateBuilder.True<XsiExhibitionBookType>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
            
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionBookType.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public XsiExhibitionBookType Add(XsiExhibitionBookType entity)
        {
            XsiExhibitionBookType ExhibitionBookType = new XsiExhibitionBookType();
            
            if (!entity.Title.IsDefault())
            ExhibitionBookType.Title = entity.Title.Trim();
            ExhibitionBookType.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionBookType.TitleAr = entity.TitleAr.Trim();
            ExhibitionBookType.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            ExhibitionBookType.CreatedOn = entity.CreatedOn;
            ExhibitionBookType.CreatedBy = entity.CreatedBy;
            ExhibitionBookType.ModifiedOn = entity.ModifiedOn;
            ExhibitionBookType.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionBookType.Add(ExhibitionBookType);
            SubmitChanges();
            return ExhibitionBookType;

        }
        public void Update(XsiExhibitionBookType entity)
        {
            XsiExhibitionBookType ExhibitionBookType = XsiContext.Context.XsiExhibitionBookType.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionBookType).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionBookType.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                ExhibitionBookType.IsActive = entity.IsActive;
          
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionBookType.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                ExhibitionBookType.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionBookType.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                ExhibitionBookType.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionBookType entity)
        {
            foreach (XsiExhibitionBookType ExhibitionBookType in Select(entity))
            {
                XsiExhibitionBookType aExhibitionBookType = XsiContext.Context.XsiExhibitionBookType.Find(ExhibitionBookType.ItemId);
                XsiContext.Context.XsiExhibitionBookType.Remove(aExhibitionBookType);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionBookType aExhibitionBookType = XsiContext.Context.XsiExhibitionBookType.Find(itemId);
            XsiContext.Context.XsiExhibitionBookType.Remove(aExhibitionBookType);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionBookSubsubjectRepository : IExhibitionBookSubsubjectRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionBookSubsubjectRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionBookSubsubjectRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionBookSubsubjectRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionBookSubsubject GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionBookSubsubject.Find(itemId);
        }
        
        public List<XsiExhibitionBookSubsubject> Select()
        {
            return XsiContext.Context.XsiExhibitionBookSubsubject.ToList();
        }
        public List<XsiExhibitionBookSubsubject> Select(XsiExhibitionBookSubsubject entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBookSubsubject>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
 
            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionBookSubsubject.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionBookSubsubject> SelectComeplete(XsiExhibitionBookSubsubject entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionBookSubsubject>();
            var predicate = PredicateBuilder.True<XsiExhibitionBookSubsubject>();

            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.CategoryId.IsDefault())
            {
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);
                XsiExhibitionBookSubject MainEntity = XsiContext.Context.XsiExhibitionBookSubject.Where(p => p.ItemId == entity.CategoryId).FirstOrDefault();

                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title;
                    var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                    Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                        || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                        || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                        || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                        );

                    isInner = true;
                }
                else
                {
                    if (!entity.Title.IsDefault())
                    {
                        string strSearchText = entity.Title.ToLower();
                        Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                        isInner = true;
                    }
                }
            }
            else
            {
                if (!entity.Title.IsDefault())
                    predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));
            }
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionBookSubsubject.Include(p => p.Category).AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionBookSubsubject Add(XsiExhibitionBookSubsubject entity)
        {
            XsiExhibitionBookSubsubject ExhibitionBookSubsubject = new XsiExhibitionBookSubsubject();
            
            ExhibitionBookSubsubject.CategoryId = entity.CategoryId;
            if (!entity.Title.IsDefault())
                ExhibitionBookSubsubject.Title = entity.Title.Trim();
            ExhibitionBookSubsubject.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionBookSubsubject.TitleAr = entity.TitleAr.Trim();
            ExhibitionBookSubsubject.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            ExhibitionBookSubsubject.CreatedOn = entity.CreatedOn;
            ExhibitionBookSubsubject.CreatedBy = entity.CreatedBy;
            ExhibitionBookSubsubject.ModifiedOn = entity.ModifiedOn;
            ExhibitionBookSubsubject.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionBookSubsubject.Add(ExhibitionBookSubsubject);
            SubmitChanges();
            return ExhibitionBookSubsubject;

        }
        public void Update(XsiExhibitionBookSubsubject entity)
        {
            XsiExhibitionBookSubsubject ExhibitionBookSubsubject = XsiContext.Context.XsiExhibitionBookSubsubject.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionBookSubsubject).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionBookSubsubject.Title = entity.Title.Trim();

            if (!entity.IsActive.IsDefault())
                ExhibitionBookSubsubject.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionBookSubsubject.TitleAr = entity.TitleAr.Trim();

            if (!entity.IsActiveAr.IsDefault())
                ExhibitionBookSubsubject.IsActiveAr = entity.IsActiveAr;
            #endregion Ar
            if (!entity.ModifiedOn.IsDefault())
                ExhibitionBookSubsubject.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionBookSubsubject.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionBookSubsubject entity)
        {
            foreach (XsiExhibitionBookSubsubject ExhibitionBookSubsubject in Select(entity))
            {
                XsiExhibitionBookSubsubject aExhibitionBookSubsubject = XsiContext.Context.XsiExhibitionBookSubsubject.Find(ExhibitionBookSubsubject.ItemId);
                XsiContext.Context.XsiExhibitionBookSubsubject.Remove(aExhibitionBookSubsubject);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionBookSubsubject aExhibitionBookSubsubject = XsiContext.Context.XsiExhibitionBookSubsubject.Find(itemId);
            XsiContext.Context.XsiExhibitionBookSubsubject.Remove(aExhibitionBookSubsubject);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
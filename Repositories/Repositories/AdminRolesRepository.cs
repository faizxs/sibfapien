﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using LinqKit;
using System.Data;
using Entities.Models;

namespace Xsi.Repositories
{
    public class AdminRolesRepository : IAdminRoleRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public AdminRolesRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public AdminRolesRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public AdminRolesRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdminRoles GetById(long itemId)
        {
            return XsiContext.Context.XsiAdminRoles.Find(itemId);
        }
        public List<XsiAdminRoles> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiAdminRoles> Select()
        {
            return XsiContext.Context.XsiAdminRoles.ToList();
        }
        public List<XsiAdminRoles> Select(XsiAdminRoles entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminRoles>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Role.IsDefault())
                predicate = predicate.And(p => p.Role == entity.Role);


            return XsiContext.Context.XsiAdminRoles.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiAdminRoles> SelectAdminRoleAndUsers(XsiAdminRoles entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminRoles>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Role.IsDefault())
                predicate = predicate.And(p => p.Role == entity.Role);

            return XsiContext.Context.XsiAdminRoles.Include(u => u.XsiAdminUsers).AsExpandable().Where(predicate).ToList();
        }
        public List<XsiAdminRoles> SelectAdminRoleAndPermissions(XsiAdminRoles entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminRoles>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Role.IsDefault())
                predicate = predicate.And(p => p.Role == entity.Role);

            return XsiContext.Context.XsiAdminRoles.Include(p => p.XsiAdminPermissions).AsExpandable().Where(predicate).ToList();
        }
        
        public XsiAdminRoles Add(XsiAdminRoles entity)
        {
            XsiAdminRoles AdminRole = new XsiAdminRoles();
            if (!entity.Role.IsDefault())
            AdminRole.Role = entity.Role.Trim();

            XsiContext.Context.XsiAdminRoles.Add(AdminRole);
            SubmitChanges();
            return AdminRole;

        }
        public void Update(XsiAdminRoles entity)
        {
            XsiAdminRoles AdminRole = XsiContext.Context.XsiAdminRoles.Find(entity.ItemId);
            XsiContext.Context.Entry(AdminRole).State = EntityState.Modified;

            if (!entity.Role.IsDefault())
                AdminRole.Role = entity.Role.Trim();

        }
        public void Delete(XsiAdminRoles entity)
        {
            foreach (XsiAdminRoles AdminRole in Select(entity))
            {
                XsiAdminRoles arole = XsiContext.Context.XsiAdminRoles.Find(AdminRole.ItemId);
                XsiContext.Context.XsiAdminRoles.Remove(arole);
            }
        }
        public void Delete(long itemId)
        {
            XsiAdminRoles arole = XsiContext.Context.XsiAdminRoles.Find(itemId);
            XsiContext.Context.XsiAdminRoles.Remove(arole);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}

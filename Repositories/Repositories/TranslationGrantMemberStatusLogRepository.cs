﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class TranslationGrantMemberStatusLogRepository : ITranslationGrantMemberStatusLogRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public TranslationGrantMemberStatusLogRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public TranslationGrantMemberStatusLogRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public TranslationGrantMemberStatusLogRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionTranslationGrantMemberStatusLog GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionTranslationGrantMemberStatusLog.Find(itemId);
        }
        public List<XsiExhibitionTranslationGrantMemberStatusLog> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionTranslationGrantMemberStatusLog> Select()
        {
            return XsiContext.Context.XsiExhibitionTranslationGrantMemberStatusLog.ToList();
        }
        public List<XsiExhibitionTranslationGrantMemberStatusLog> Select(XsiExhibitionTranslationGrantMemberStatusLog entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionTranslationGrantMemberStatusLog>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.TranslationGrantMemberId.IsDefault())
                predicate = predicate.And(p => p.TranslationGrantMemberId == entity.TranslationGrantMemberId);

            if (!entity.AdminId.IsDefault())
                predicate = predicate.And(p => p.AdminId == entity.AdminId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);


            return XsiContext.Context.XsiExhibitionTranslationGrantMemberStatusLog.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionTranslationGrantMemberStatusLog Add(XsiExhibitionTranslationGrantMemberStatusLog entity)
        {
            XsiExhibitionTranslationGrantMemberStatusLog TranslationGrantMemberStatusLog = new XsiExhibitionTranslationGrantMemberStatusLog();
            TranslationGrantMemberStatusLog.TranslationGrantMemberId = entity.TranslationGrantMemberId;
            TranslationGrantMemberStatusLog.AdminId = entity.AdminId;
            TranslationGrantMemberStatusLog.Status = entity.Status;
            TranslationGrantMemberStatusLog.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiExhibitionTranslationGrantMemberStatusLog.Add(TranslationGrantMemberStatusLog);
            SubmitChanges();
            return TranslationGrantMemberStatusLog;

        }
        public void Update(XsiExhibitionTranslationGrantMemberStatusLog entity)
        {
            XsiExhibitionTranslationGrantMemberStatusLog TranslationGrantMemberStatusLog = XsiContext.Context.XsiExhibitionTranslationGrantMemberStatusLog.Find(entity.ItemId);
            XsiContext.Context.Entry(TranslationGrantMemberStatusLog).State = EntityState.Modified;

            if (!entity.Status.IsDefault())
                TranslationGrantMemberStatusLog.Status = entity.Status;
        }
        public void Delete(XsiExhibitionTranslationGrantMemberStatusLog entity)
        {
            foreach (XsiExhibitionTranslationGrantMemberStatusLog TranslationGrantMemberStatusLog in Select(entity))
            {
                XsiExhibitionTranslationGrantMemberStatusLog aTranslationGrantMemberStatusLog = XsiContext.Context.XsiExhibitionTranslationGrantMemberStatusLog.Find(TranslationGrantMemberStatusLog.ItemId);
                XsiContext.Context.XsiExhibitionTranslationGrantMemberStatusLog.Remove(aTranslationGrantMemberStatusLog);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionTranslationGrantMemberStatusLog aTranslationGrantMemberStatusLog = XsiContext.Context.XsiExhibitionTranslationGrantMemberStatusLog.Find(itemId);
            XsiContext.Context.XsiExhibitionTranslationGrantMemberStatusLog.Remove(aTranslationGrantMemberStatusLog);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
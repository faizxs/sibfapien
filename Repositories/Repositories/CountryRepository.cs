﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class CountryRepository : ICountryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public CountryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public CountryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public CountryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiCountry GetById(long itemId)
        {
            return XsiContext.Context.XsiCountry.Find(itemId);
        }
        public List<XsiCountry> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiCountry> Select()
        {
            return XsiContext.Context.XsiCountry.ToList();
        }
        public List<XsiCountry> Select(XsiCountry entity)
        {
            var predicate = PredicateBuilder.True<XsiCountry>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.CountryName.IsDefault())
                predicate = predicate.And(p => p.CountryName.Contains(entity.CountryName));

            if (!entity.CountryCode.IsDefault())
                predicate = predicate.And(p => p.CountryName.Equals(entity.CountryCode));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            return XsiContext.Context.XsiCountry.AsExpandable().Where(predicate).ToList();
        }

        public XsiCountry Add(XsiCountry entity)
        {
            XsiCountry Country = new XsiCountry();
            if (!entity.CountryName.IsDefault())
                Country.CountryName = entity.CountryName.Trim();
            if (!entity.CountryCode.IsDefault())
                Country.CountryCode = entity.CountryCode.Trim();
            Country.IsActive = entity.IsActive;

            XsiContext.Context.XsiCountry.Add(Country);
            SubmitChanges();
            return Country;

        }
        public void Update(XsiCountry entity)
        {
            XsiCountry Country = XsiContext.Context.XsiCountry.Find(entity.ItemId);
            XsiContext.Context.Entry(Country).State = EntityState.Modified;

            if (!entity.CountryName.IsDefault())
                Country.CountryName = entity.CountryName.Trim();

            if (!entity.CountryCode.IsDefault())
                Country.CountryCode = entity.CountryCode.Trim();

            if (!entity.IsActive.IsDefault())
                Country.IsActive = entity.IsActive;
        }
        public void Delete(XsiCountry entity)
        {
            foreach (XsiCountry Country in Select(entity))
            {
                XsiCountry aCountry = XsiContext.Context.XsiCountry.Find(Country.ItemId);
                XsiContext.Context.XsiCountry.Remove(aCountry);
            }
        }
        public void Delete(long itemId)
        {
            XsiCountry aCountry = XsiContext.Context.XsiCountry.Find(itemId);
            XsiContext.Context.XsiCountry.Remove(aCountry);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
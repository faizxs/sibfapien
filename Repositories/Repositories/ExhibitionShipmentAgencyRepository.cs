﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionShipmentAgencyRepository : IExhibitionShipmentAgencyRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionShipmentAgencyRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionShipmentAgencyRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionShipmentAgencyRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionShipmentAgency GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionShipmentAgency.Find(itemId);
        }
        public List<XsiExhibitionShipmentAgency> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionShipmentAgency> Select()
        {
            return XsiContext.Context.XsiExhibitionShipmentAgency.ToList();
        }
        public List<XsiExhibitionShipmentAgency> Select(XsiExhibitionShipmentAgency entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionShipmentAgency>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ExhibitionShipmentId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionShipmentId == entity.ExhibitionShipmentId);

            if (!entity.AgencyId.IsDefault())
                predicate = predicate.And(p => p.AgencyId == entity.AgencyId);

            return XsiContext.Context.XsiExhibitionShipmentAgency.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionShipmentAgency Add(XsiExhibitionShipmentAgency entity)
        {
            XsiExhibitionShipmentAgency ExhibitionShipmentAgency = new XsiExhibitionShipmentAgency();
            ExhibitionShipmentAgency.ExhibitionShipmentId = entity.ExhibitionShipmentId;
            ExhibitionShipmentAgency.AgencyId = entity.AgencyId;
            XsiContext.Context.XsiExhibitionShipmentAgency.Add(ExhibitionShipmentAgency);
            SubmitChanges();
            return ExhibitionShipmentAgency;

        }
        public void Update(XsiExhibitionShipmentAgency entity)
        {
            XsiExhibitionShipmentAgency ExhibitionShipmentAgency = XsiContext.Context.XsiExhibitionShipmentAgency.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionShipmentAgency).State = EntityState.Modified;

            if (!entity.ExhibitionShipmentId.IsDefault())
                ExhibitionShipmentAgency.ExhibitionShipmentId = entity.ExhibitionShipmentId;

            if (!entity.AgencyId.IsDefault())
                ExhibitionShipmentAgency.AgencyId = entity.AgencyId;
        }
        public void Delete(XsiExhibitionShipmentAgency entity)
        {
            foreach (XsiExhibitionShipmentAgency ExhibitionShipmentAgency in Select(entity))
            {
                XsiExhibitionShipmentAgency aExhibitionShipmentAgency = XsiContext.Context.XsiExhibitionShipmentAgency.Find(ExhibitionShipmentAgency.ItemId);
                XsiContext.Context.XsiExhibitionShipmentAgency.Remove(aExhibitionShipmentAgency);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionShipmentAgency aExhibitionShipmentAgency = XsiContext.Context.XsiExhibitionShipmentAgency.Find(itemId);
            XsiContext.Context.XsiExhibitionShipmentAgency.Remove(aExhibitionShipmentAgency);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
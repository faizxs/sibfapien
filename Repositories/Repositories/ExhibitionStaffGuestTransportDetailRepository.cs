﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionStaffGuestTransportDetailRepository : IExhibitionStaffGuestTransportDetailsRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionStaffGuestTransportDetailRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionStaffGuestTransportDetailRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionStaffGuestTransportDetailRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionStaffGuestTransportDetails GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionStaffGuestTransportDetails.Find(itemId);
        }
        public List<XsiExhibitionStaffGuestTransportDetails> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionStaffGuestTransportDetails> Select()
        {
            return XsiContext.Context.XsiExhibitionStaffGuestTransportDetails.ToList();
        }

        public List<XsiExhibitionStaffGuestTransportDetails> Select(XsiExhibitionStaffGuestTransportDetails entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionStaffGuestTransportDetails>();

            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.StaffGuestId.IsDefault())
                predicate = predicate.And(p => p.StaffGuestId == entity.StaffGuestId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            #region Transport

            if (!entity.TransportationType.IsDefault())
                predicate = predicate.And(p => p.TransportationType.Equals(entity.TransportationType));

            if (!entity.TransportStartDate.IsDefault())
                predicate = predicate.And(p => p.TransportStartDate == entity.TransportStartDate);

            if (!entity.TransportEndDate.IsDefault())
                predicate = predicate.And(p => p.TransportEndDate == entity.TransportEndDate);

            if (!entity.TransportContactPerson.IsDefault())
                predicate = predicate.And(p => p.TransportContactPerson.Contains(entity.TransportContactPerson));

            if (!entity.TransportContactNumber.IsDefault())
                predicate = predicate.And(p => p.TransportContactNumber.Contains(entity.TransportContactNumber));

            if (!entity.TransportationStatus.IsDefault())
                predicate = predicate.And(p => p.TransportationStatus.Equals(entity.TransportationStatus));

            if (!entity.TransportationTypeAllotted.IsDefault())
                predicate = predicate.And(p => p.TransportationTypeAllotted.Equals(entity.TransportationTypeAllotted));

            if (!entity.NoofPeople.IsDefault())
                predicate = predicate.And(p => p.NoofPeople == entity.NoofPeople);

            #endregion

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiExhibitionStaffGuestTransportDetails.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionStaffGuestTransportDetails Add(XsiExhibitionStaffGuestTransportDetails entity)
        {
            XsiExhibitionStaffGuestTransportDetails detailsentity = new XsiExhibitionStaffGuestTransportDetails();

            detailsentity.StaffGuestId = entity.StaffGuestId;
            detailsentity.ExhibitionId = entity.ExhibitionId;

            #region Transport Details
            detailsentity.TransportationType = entity.TransportationType;
            detailsentity.TransportStartDate = entity.TransportStartDate;
            detailsentity.TransportEndDate = entity.TransportEndDate;
            detailsentity.TransportContactPerson = entity.TransportContactPerson;
            detailsentity.TransportContactNumber = entity.TransportContactNumber;
            detailsentity.TransportationStatus = entity.TransportationStatus;
            detailsentity.TransportationTypeAllotted = entity.TransportationTypeAllotted;
            detailsentity.NoofPeople = entity.NoofPeople;
            #endregion

            detailsentity.IsActive = entity.IsActive;
            detailsentity.CreatedOn = entity.CreatedOn;
            detailsentity.CreatedBy = entity.CreatedBy;
            detailsentity.ModifiedOn = entity.ModifiedOn;
            detailsentity.ModifiedBy = entity.ModifiedBy;



            XsiContext.Context.XsiExhibitionStaffGuestTransportDetails.Add(detailsentity);
            SubmitChanges();
            return detailsentity;

        }
        public void Update(XsiExhibitionStaffGuestTransportDetails entity)
        {
            XsiExhibitionStaffGuestTransportDetails detailsentity = XsiContext.Context.XsiExhibitionStaffGuestTransportDetails.Find(entity.ItemId);

            XsiContext.Context.Entry(detailsentity).State = EntityState.Modified;

            if (!entity.StaffGuestId.IsDefault())
                detailsentity.StaffGuestId = entity.StaffGuestId;

            if (!entity.ExhibitionId.IsDefault())
                detailsentity.ExhibitionId = entity.ExhibitionId;

            #region Transport

            if (!entity.TransportationType.IsDefault())
                detailsentity.TransportationType = entity.TransportationType.Trim();

            if (!entity.TransportStartDate.IsDefault())
                detailsentity.TransportStartDate = entity.TransportStartDate;

            if (!entity.TransportEndDate.IsDefault())
                detailsentity.TransportEndDate = entity.TransportEndDate;

            if (!entity.TransportContactPerson.IsDefault())
                detailsentity.TransportContactPerson = entity.TransportContactPerson.Trim();

            if (!entity.TransportContactNumber.IsDefault())
                detailsentity.TransportContactNumber = entity.TransportContactNumber.Trim();

            if (!entity.TransportationStatus.IsDefault())
                detailsentity.TransportationStatus = entity.TransportationStatus.Trim();

            if (!entity.TransportationTypeAllotted.IsDefault())
                detailsentity.TransportationTypeAllotted = entity.TransportationTypeAllotted.Trim();

            if (!entity.NoofPeople.IsDefault())
                detailsentity.NoofPeople = entity.NoofPeople;

            #endregion

            if (!entity.IsActive.IsDefault())
                detailsentity.IsActive = entity.IsActive;

            if (!entity.ModifiedBy.IsDefault())
                detailsentity.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                detailsentity.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiExhibitionStaffGuestTransportDetails entity)
        {
            foreach (XsiExhibitionStaffGuestTransportDetails detailsentity in Select(entity))
            {
                XsiExhibitionStaffGuestTransportDetails aExhibitionStaffGuestParticipating = XsiContext.Context.XsiExhibitionStaffGuestTransportDetails.Find(detailsentity.ItemId);
                XsiContext.Context.XsiExhibitionStaffGuestTransportDetails.Remove(aExhibitionStaffGuestParticipating);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionStaffGuestTransportDetails detailsentity = XsiContext.Context.XsiExhibitionStaffGuestTransportDetails.Where(p => p.ItemId == itemId).OrderByDescending(o => o.ItemId).FirstOrDefault();
            XsiContext.Context.XsiExhibitionStaffGuestTransportDetails.Remove(detailsentity);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
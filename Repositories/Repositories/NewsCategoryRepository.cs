﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class NewsCategoryRepository : INewsCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public NewsCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public NewsCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public NewsCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiNewsCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiNewsCategory.Find(itemId);
        }

        public List<XsiNewsCategory> Select()
        {
            return XsiContext.Context.XsiNewsCategory.ToList();
        }
        public List<XsiNewsCategory> SelectOr(XsiNewsCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiNewsCategory>();
            var Outer = PredicateBuilder.True<XsiNewsCategory>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }


            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiNewsCategory.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiNewsCategory> Select(XsiNewsCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiNewsCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);


            return XsiContext.Context.XsiNewsCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiNewsCategory> SelectOtherLanguageCategory(XsiNewsCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiNewsCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            predicate = PredicateBuilder.True<XsiNewsCategory>();

            return XsiContext.Context.XsiNewsCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiNewsCategory> SelectCurrentLanguageCategory(XsiNewsCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiNewsCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
            predicate = PredicateBuilder.True<XsiNewsCategory>();

            return XsiContext.Context.XsiNewsCategory.AsExpandable().Where(predicate).ToList();
        }

        public XsiNewsCategory Add(XsiNewsCategory entity)
        {
            XsiNewsCategory NewsCategory = new XsiNewsCategory();

            NewsCategory.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                NewsCategory.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
                NewsCategory.Description = entity.Description.Trim();
            NewsCategory.CreatedOn = entity.CreatedOn;
            NewsCategory.CreatedBy = entity.CreatedBy;

            XsiContext.Context.XsiNewsCategory.Add(NewsCategory);
            SubmitChanges();
            return NewsCategory;

        }
        public void Update(XsiNewsCategory entity)
        {
            XsiNewsCategory NewsCategory = XsiContext.Context.XsiNewsCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(NewsCategory).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                NewsCategory.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                NewsCategory.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                NewsCategory.Description = entity.Description.Trim();

        }
        public void Delete(XsiNewsCategory entity)
        {
            foreach (XsiNewsCategory NewsCategory in Select(entity))
            {
                XsiNewsCategory aNewsCategory = XsiContext.Context.XsiNewsCategory.Find(NewsCategory.ItemId);
                XsiContext.Context.XsiNewsCategory.Remove(aNewsCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiNewsCategory aNewsCategory = XsiContext.Context.XsiNewsCategory.Find(itemId);
            XsiContext.Context.XsiNewsCategory.Remove(aNewsCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
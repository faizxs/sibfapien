﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionRepresentativeParticipatingNoVisaRepository : IExhibitionRepresentativeParticipatingNoVisaRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionRepresentativeParticipatingNoVisaRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionRepresentativeParticipatingNoVisaRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionRepresentativeParticipatingNoVisaRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionRepresentativeParticipatingNoVisa GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionRepresentativeParticipatingNoVisa.Find(itemId);
        }
        public XsiExhibitionRepresentativeParticipatingNoVisa GetByRepresentativeId(long representativeId)
        {
            return XsiContext.Context.XsiExhibitionRepresentativeParticipatingNoVisa.Include("XsiExhibitionRepresentative").Where(p => p.RepresentativeId == representativeId && p.IsActive == "Y").FirstOrDefault();
        }

        public List<XsiExhibitionRepresentativeParticipatingNoVisa> Select()
        {
            return XsiContext.Context.XsiExhibitionRepresentativeParticipatingNoVisa.ToList();
        }
        public List<XsiExhibitionRepresentativeParticipatingNoVisa> Select(XsiExhibitionRepresentativeParticipatingNoVisa entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionRepresentativeParticipatingNoVisa>();
            if (!entity.RepresentativeId.IsDefault())
                predicate = predicate.And(p => p.RepresentativeId == entity.RepresentativeId);

            //if (!entity.MemberId.IsDefault())
            //    predicate = predicate.And(p => p.MemberId == entity.);

            if (!entity.RepresentativeId.IsDefault())
                predicate = predicate.And(p => p.RepresentativeId == entity.RepresentativeId);

            //if (!entity.ExhibitionGroupId.IsDefault())
            //    predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionGroupId);

            if (!entity.MemberExhibitionYearlyId.IsDefault())
                predicate = predicate.And(p => p.MemberExhibitionYearlyId == entity.MemberExhibitionYearlyId);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.Uidno.IsDefault())
                predicate = predicate.And(p => p.Uidno.Equals(entity.Uidno));

            if (!entity.CountryUidno.IsDefault())
                predicate = predicate.And(p => p.CountryUidno.Equals(entity.CountryUidno));

            if (!entity.IsTravelDetails.IsDefault())
                predicate = predicate.And(p => p.IsTravelDetails.Equals(entity.IsTravelDetails));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));


            if (!entity.Profession.IsDefault())
                predicate = predicate.And(p => p.Profession.Contains(entity.Profession));

            //if (!entity.POB.IsDefault())
            //    predicate = predicate.And(p => p.POB == entity.POB);

            if (!entity.Telephone.IsDefault())
                predicate = predicate.And(p => p.Telephone.Contains(entity.Telephone));

            if (!entity.Mobile.IsDefault())
                predicate = predicate.And(p => p.Mobile.Contains(entity.Mobile));

            if (!entity.TravelDetails.IsDefault())
                predicate = predicate.And(p => p.TravelDetails.Contains(entity.TravelDetails));

            if (!entity.NeedVisa.IsDefault())
                predicate = predicate.And(p => p.NeedVisa.Equals(entity.NeedVisa));

            if (!entity.ArrivalDate.IsDefault())
                predicate = predicate.And(p => p.ArrivalDate == entity.ArrivalDate);

            if (!entity.ArrivalAirportId.IsDefault())
                predicate = predicate.And(p => p.ArrivalAirportId == entity.ArrivalAirportId);

            if (!entity.ArrivalTerminalId.IsDefault())
                predicate = predicate.And(p => p.ArrivalTerminalId == entity.ArrivalTerminalId);

            if (!entity.DepartureDate.IsDefault())
                predicate = predicate.And(p => p.DepartureDate == entity.DepartureDate);

            if (!entity.Passport.IsDefault())
                predicate = predicate.And(p => p.Passport.Contains(entity.Passport));

            if (!entity.PlaceOfIssue.IsDefault())
                predicate = predicate.And(p => p.PlaceOfIssue.Contains(entity.PlaceOfIssue));

            if (!entity.DateOfIssue.IsDefault())
                predicate = predicate.And(p => p.DateOfIssue == entity.DateOfIssue);

            if (!entity.ExpiryDate.IsDefault())
                predicate = predicate.And(p => p.ExpiryDate == entity.ExpiryDate);

            if (!entity.PassportCopy.IsDefault())
                predicate = predicate.And(p => p.PassportCopy.Contains(entity.PassportCopy));

            if (!entity.Photo.IsDefault())
                predicate = predicate.And(p => p.Photo.Contains(entity.Photo));

            if (!entity.FlightTickets.IsDefault())
                predicate = predicate.And(p => p.FlightTickets.Contains(entity.FlightTickets));

            if (!entity.VisaCopy.IsDefault())
                predicate = predicate.And(p => p.VisaCopy.Contains(entity.VisaCopy));

            if (!entity.Invoice.IsDefault())
                predicate = predicate.And(p => p.Invoice.Contains(entity.Invoice));

            if (!entity.Notes.IsDefault())
                predicate = predicate.And(p => p.Notes.Contains(entity.Notes));

            if (!entity.VisaApplicationForm.IsDefault())
                predicate = predicate.And(p => p.VisaApplicationForm.Contains(entity.VisaApplicationForm));

            if (!entity.NoofVisaMonths.IsDefault())
                predicate = predicate.And(p => p.NoofVisaMonths.Equals(entity.NoofVisaMonths));

            if (!entity.GuestType.IsDefault())
                predicate = predicate.And(p => p.GuestType.Equals(entity.GuestType));

            if (!entity.GuestCategory.IsDefault())
                predicate = predicate.And(p => p.GuestCategory.Equals(entity.GuestCategory));

            if (!entity.ActivityDate.IsDefault())
                predicate = predicate.And(p => p.ActivityDate == entity.ActivityDate);

            if (!entity.PaidVisa.IsDefault())
                predicate = predicate.And(p => p.PaidVisa.Contains(entity.PaidVisa));

            if (!entity.PassportCopyTwo.IsDefault())
                predicate = predicate.And(p => p.PassportCopyTwo.Contains(entity.PassportCopyTwo));

            if (!entity.VisaType.IsDefault())
                predicate = predicate.And(p => p.VisaType.Equals(entity.VisaType));

            if (!entity.HealthInsurance.IsDefault())
                predicate = predicate.And(p => p.HealthInsurance.Contains(entity.HealthInsurance));

            if (!entity.HealthInsuranceTwo.IsDefault())
                predicate = predicate.And(p => p.HealthInsuranceTwo.Contains(entity.HealthInsuranceTwo));

            if (!entity.RepresentativeAdminNote.IsDefault())
                predicate = predicate.And(p => p.RepresentativeAdminNote.Contains(entity.RepresentativeAdminNote));

            if (!entity.IsVisitedUae.IsDefault())
                predicate = predicate.And(p => p.IsVisitedUae.Equals(entity.IsVisitedUae));

            if (!entity.RejectedStatus.IsDefault())
                predicate = predicate.And(p => p.RejectedStatus.Equals(entity.RejectedStatus));

            if (!entity.IsCompanion.IsDefault())
                predicate = predicate.And(p => p.IsCompanion.Equals(entity.IsCompanion));

            if (!entity.CompanionDescription.IsDefault())
                predicate = predicate.And(p => p.CompanionDescription.Contains(entity.CompanionDescription));

            if (!entity.ReIssueVisaCount.IsDefault())
                predicate = predicate.And(p => p.ReIssueVisaCount == entity.ReIssueVisaCount);

            if (!entity.PassportModifiedCountApproved.IsDefault())
                predicate = predicate.And(p => p.PassportModifiedCountApproved == entity.PassportModifiedCountApproved);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsUaeresident.IsDefault())
                predicate = predicate.And(p => p.IsUaeresident.Equals(entity.IsUaeresident));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);
            //if (!entity.XsiExhibitionMemberApplicationYearly.ExhibitionId.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionMemberApplicationYearly.ExhibitionId.Equals(entity.XsiExhibitionMemberApplicationYearly.ExhibitionId));

            return XsiContext.Context.XsiExhibitionRepresentativeParticipatingNoVisa.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionRepresentativeParticipatingNoVisa Add(XsiExhibitionRepresentativeParticipatingNoVisa entity)
        {
            XsiExhibitionRepresentativeParticipatingNoVisa ExhibitionRepresentative = new XsiExhibitionRepresentativeParticipatingNoVisa();
            //ExhibitionRepresentative.MemberId = entity.MemberId;
            ExhibitionRepresentative.RepresentativeId = entity.RepresentativeId;

            if (!entity.NameEn.IsDefault())
                ExhibitionRepresentative.NameEn = entity.NameEn.Trim();

            if (!entity.NameAr.IsDefault())
                ExhibitionRepresentative.NameAr = entity.NameAr.Trim();

            ExhibitionRepresentative.CountryId = entity.CountryId;
            if (!entity.Uidno.IsDefault())
                ExhibitionRepresentative.Uidno = entity.Uidno.Trim();
            if (!entity.CountryUidno.IsDefault())
                ExhibitionRepresentative.CountryUidno = entity.CountryUidno.Trim();
            ExhibitionRepresentative.IsTravelDetails = entity.IsTravelDetails;
            ExhibitionRepresentative.Status = entity.Status;
            ExhibitionRepresentative.LanguageId = entity.LanguageId;
            if (!entity.Profession.IsDefault())
                ExhibitionRepresentative.Profession = entity.Profession.Trim();
            //if (!entity.POB.IsDefault())
            //    ExhibitionRepresentative.POB = entity.POB.Trim();
            if (!entity.Telephone.IsDefault())
                ExhibitionRepresentative.Telephone = entity.Telephone.Trim();
            if (!entity.Mobile.IsDefault())
                ExhibitionRepresentative.Mobile = entity.Mobile.Trim();
            if (!entity.TravelDetails.IsDefault())
                ExhibitionRepresentative.TravelDetails = entity.TravelDetails.Trim();
            if (!entity.NeedVisa.IsDefault())
                ExhibitionRepresentative.NeedVisa = entity.NeedVisa.Trim();
            ExhibitionRepresentative.ArrivalDate = entity.ArrivalDate;
            ExhibitionRepresentative.ArrivalAirportId = entity.ArrivalAirportId;
            ExhibitionRepresentative.ArrivalTerminalId = entity.ArrivalTerminalId;
            ExhibitionRepresentative.DepartureDate = entity.DepartureDate;
            if (!entity.Passport.IsDefault())
                ExhibitionRepresentative.Passport = entity.Passport.Trim();
            if (!entity.PlaceOfIssue.IsDefault())
                ExhibitionRepresentative.PlaceOfIssue = entity.PlaceOfIssue.Trim();
            ExhibitionRepresentative.DateOfIssue = entity.DateOfIssue;
            ExhibitionRepresentative.ExpiryDate = entity.ExpiryDate;
            ExhibitionRepresentative.PassportCopy = entity.PassportCopy;
            ExhibitionRepresentative.Photo = entity.Photo;
            ExhibitionRepresentative.FlightTickets = entity.FlightTickets;
            ExhibitionRepresentative.VisaCopy = entity.VisaCopy;
            if (!entity.Invoice.IsDefault())
                ExhibitionRepresentative.Invoice = entity.Invoice.Trim();
            if (!entity.Notes.IsDefault())
                ExhibitionRepresentative.Notes = entity.Notes.Trim();
            if (!entity.VisaApplicationForm.IsDefault())
                ExhibitionRepresentative.VisaApplicationForm = entity.VisaApplicationForm.Trim();

            #region Guest Details
            ExhibitionRepresentative.NoofVisaMonths = entity.NoofVisaMonths;
            ExhibitionRepresentative.GuestType = entity.GuestType;
            ExhibitionRepresentative.GuestCategory = entity.GuestCategory;
            ExhibitionRepresentative.ActivityDate = entity.ActivityDate;
            ExhibitionRepresentative.PaidVisa = entity.PaidVisa;
            ExhibitionRepresentative.RepresentativeAdminNote = entity.RepresentativeAdminNote;
            #endregion

            ExhibitionRepresentative.PassportCopyTwo = entity.PassportCopyTwo;
            ExhibitionRepresentative.VisaType = entity.VisaType;
            ExhibitionRepresentative.HealthInsurance = entity.HealthInsurance;
            ExhibitionRepresentative.HealthInsuranceTwo = entity.HealthInsuranceTwo;
            ExhibitionRepresentative.AddedBy = entity.AddedBy;
            ExhibitionRepresentative.IsVisitedUae = entity.IsVisitedUae;
            ExhibitionRepresentative.RejectedStatus = entity.RejectedStatus;
            ExhibitionRepresentative.IsCompanion = entity.IsCompanion;
            ExhibitionRepresentative.CompanionDescription = entity.CompanionDescription;
            ExhibitionRepresentative.ReIssueVisaCount = entity.ReIssueVisaCount;
            ExhibitionRepresentative.PassportModifiedCountApproved = entity.PassportModifiedCountApproved;

            ExhibitionRepresentative.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;
            ExhibitionRepresentative.IsActive = entity.IsActive;

            if (!entity.IsUaeresident.IsDefault())
                ExhibitionRepresentative.IsUaeresident = entity.IsUaeresident.Trim();

            ExhibitionRepresentative.Status = entity.Status;
            ExhibitionRepresentative.CreatedBy = entity.CreatedBy;
            ExhibitionRepresentative.CreatedOn = entity.CreatedOn;
            ExhibitionRepresentative.ModifiedBy = entity.ModifiedBy;
            ExhibitionRepresentative.ModifiedOn = entity.ModifiedOn;

            XsiContext.Context.XsiExhibitionRepresentativeParticipatingNoVisa.Add(ExhibitionRepresentative);
            SubmitChanges();
            return ExhibitionRepresentative;

        }
        public void Update(XsiExhibitionRepresentativeParticipatingNoVisa entity)
        {
            XsiExhibitionRepresentativeParticipatingNoVisa ExhibitionRepresentative = XsiContext.Context.XsiExhibitionRepresentativeParticipatingNoVisa.Where(p => p.RepresentativeId == entity.RepresentativeId && p.MemberExhibitionYearlyId == entity.MemberExhibitionYearlyId).OrderByDescending(o => o.RepresentativeId).FirstOrDefault();
            XsiContext.Context.Entry(ExhibitionRepresentative).State = EntityState.Modified;

            if (!entity.RepresentativeId.IsDefault())
                ExhibitionRepresentative.RepresentativeId = entity.RepresentativeId;

            if (!entity.MemberExhibitionYearlyId.IsDefault())
                ExhibitionRepresentative.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;

            //if (!entity.POB.IsDefault())
            //    ExhibitionRepresentative.POB = entity.POB.Trim();

            if (!entity.NameEn.IsDefault())
                ExhibitionRepresentative.NameEn = entity.NameEn.Trim();

            if (!entity.NameAr.IsDefault())
                ExhibitionRepresentative.NameAr = entity.NameAr.Trim();

            if (!entity.Uidno.IsDefault())
                ExhibitionRepresentative.Uidno = entity.Uidno.Trim();

            if (!entity.CountryUidno.IsDefault())
                ExhibitionRepresentative.CountryUidno = entity.CountryUidno.Trim();

            if (!entity.Telephone.IsDefault())
                ExhibitionRepresentative.Telephone = entity.Telephone.Trim();

            if (!entity.Mobile.IsDefault())
                ExhibitionRepresentative.Mobile = entity.Mobile.Trim();

            if (!entity.TravelDetails.IsDefault())
                ExhibitionRepresentative.TravelDetails = entity.TravelDetails.Trim();

            if (!entity.NeedVisa.IsDefault())
                ExhibitionRepresentative.NeedVisa = entity.NeedVisa;

            if (!entity.ArrivalDate.IsDefault())
                ExhibitionRepresentative.ArrivalDate = entity.ArrivalDate;

            if (!entity.ArrivalAirportId.IsDefault())
                ExhibitionRepresentative.ArrivalAirportId = entity.ArrivalAirportId;

            if (!entity.ArrivalTerminalId.IsDefault())
                ExhibitionRepresentative.ArrivalTerminalId = entity.ArrivalTerminalId;

            if (!entity.DepartureDate.IsDefault())
                ExhibitionRepresentative.DepartureDate = entity.DepartureDate;

            if (!entity.Passport.IsDefault())
                ExhibitionRepresentative.Passport = entity.Passport.Trim();

            if (!entity.PlaceOfIssue.IsDefault())
                ExhibitionRepresentative.PlaceOfIssue = entity.PlaceOfIssue.Trim();

            if (!entity.DateOfIssue.IsDefault())
                ExhibitionRepresentative.DateOfIssue = entity.DateOfIssue;

            if (!entity.ExpiryDate.IsDefault())
                ExhibitionRepresentative.ExpiryDate = entity.ExpiryDate;

            if (!entity.PassportCopy.IsDefault())
                ExhibitionRepresentative.PassportCopy = entity.PassportCopy;

            if (!entity.Photo.IsDefault())
                ExhibitionRepresentative.Photo = entity.Photo;

            if (!entity.FlightTickets.IsDefault())
                ExhibitionRepresentative.FlightTickets = entity.FlightTickets;

            if (!entity.VisaCopy.IsDefault())
                ExhibitionRepresentative.VisaCopy = entity.VisaCopy;

            if (!entity.Invoice.IsDefault())
                ExhibitionRepresentative.Invoice = entity.Invoice.Trim();

            if (!entity.Notes.IsDefault())
                ExhibitionRepresentative.Notes = entity.Notes.Trim();

            if (!entity.VisaApplicationForm.IsDefault())
                ExhibitionRepresentative.VisaApplicationForm = entity.VisaApplicationForm.Trim();

            if (!entity.NoofVisaMonths.IsDefault())
                ExhibitionRepresentative.NoofVisaMonths = entity.NoofVisaMonths;

            if (!entity.GuestType.IsDefault())
                ExhibitionRepresentative.GuestType = entity.GuestType;

            if (!entity.GuestCategory.IsDefault())
                ExhibitionRepresentative.GuestCategory = entity.GuestCategory;

            if (!entity.ActivityDate.IsDefault())
                ExhibitionRepresentative.ActivityDate = entity.ActivityDate;

            if (!entity.PaidVisa.IsDefault())
                ExhibitionRepresentative.PaidVisa = entity.PaidVisa;

            if (!entity.RepresentativeAdminNote.IsDefault())
                ExhibitionRepresentative.RepresentativeAdminNote = entity.RepresentativeAdminNote.Trim();

            if (!entity.PassportCopyTwo.IsDefault())
                ExhibitionRepresentative.PassportCopyTwo = entity.PassportCopyTwo.Trim();

            if (!entity.VisaType.IsDefault())
                ExhibitionRepresentative.VisaType = entity.VisaType.Trim();

            if (!entity.HealthInsurance.IsDefault())
                ExhibitionRepresentative.HealthInsurance = entity.HealthInsurance.Trim();

            if (!entity.HealthInsuranceTwo.IsDefault())
                ExhibitionRepresentative.HealthInsuranceTwo = entity.HealthInsuranceTwo.Trim();

            if (!entity.IsVisitedUae.IsDefault())
                ExhibitionRepresentative.IsVisitedUae = entity.IsVisitedUae;

            if (!entity.RejectedStatus.IsDefault())
                ExhibitionRepresentative.RejectedStatus = entity.RejectedStatus;

            if (!entity.IsCompanion.IsDefault())
                ExhibitionRepresentative.IsCompanion = entity.IsCompanion;

            if (!entity.CompanionDescription.IsDefault())
                ExhibitionRepresentative.CompanionDescription = entity.CompanionDescription;

            if (!entity.ReIssueVisaCount.IsDefault())
                ExhibitionRepresentative.ReIssueVisaCount = entity.ReIssueVisaCount;

            if (!entity.PassportModifiedCountApproved.IsDefault())
                ExhibitionRepresentative.PassportModifiedCountApproved = entity.PassportModifiedCountApproved;

            if (!entity.AddedBy.IsDefault())
                ExhibitionRepresentative.AddedBy = entity.AddedBy;

            if (!entity.IsActive.IsDefault())
                ExhibitionRepresentative.IsActive = entity.IsActive;

            if (!entity.IsUaeresident.IsDefault())
                ExhibitionRepresentative.IsUaeresident = entity.IsUaeresident;

            if (!entity.Status.IsDefault())
                ExhibitionRepresentative.Status = entity.Status;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionRepresentative.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionRepresentative.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiExhibitionRepresentativeParticipatingNoVisa entity)
        {
            foreach (XsiExhibitionRepresentativeParticipatingNoVisa ExhibitionRepresentative in Select(entity))
            {
                XsiExhibitionRepresentativeParticipatingNoVisa aExhibitionRepresentative = XsiContext.Context.XsiExhibitionRepresentativeParticipatingNoVisa.Where(p => p.RepresentativeId == ExhibitionRepresentative.RepresentativeId && p.MemberExhibitionYearlyId == ExhibitionRepresentative.MemberExhibitionYearlyId).OrderByDescending(o => o.RepresentativeId).FirstOrDefault();
                XsiContext.Context.XsiExhibitionRepresentativeParticipatingNoVisa.Remove(aExhibitionRepresentative);
            }
        }
        public void Delete(long itemId)
        {
            //XsiExhibitionRepresentativeParticipatingNoVisa aExhibitionRepresentative = XsiContext.Context.XsiExhibitionRepresentativeParticipatingNoVisas.Where(p => p.RepresentativeId == itemId).FirstOrDefault();
            //XsiContext.Context.XsiExhibitionRepresentativeParticipatingNoVisa.Remove(aExhibitionRepresentative);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
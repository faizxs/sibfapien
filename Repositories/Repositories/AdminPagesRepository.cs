﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using LinqKit;
using System.Data;
using Entities.Models;

namespace Xsi.Repositories
{
    public class AdminPagesRepository : IAdminPageRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public AdminPagesRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public AdminPagesRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public AdminPagesRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdminPages GetById(long itemId)
        {
            return XsiContext.Context.XsiAdminPages.Find(itemId);
        }
        public List<XsiAdminPages> Select()
        {
            return XsiContext.Context.XsiAdminPages.ToList();
        }
        public List<XsiAdminPages> Select(XsiAdminPages entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminPages>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.PageName.IsDefault())
                predicate = predicate.And(p => p.PageName == entity.PageName);

            if (!entity.PageUrl.IsDefault())
                predicate = predicate.And(p => p.PageUrl == entity.PageUrl);

            return XsiContext.Context.XsiAdminPages.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiAdminPages> SelectAdminPageAndPermissions(XsiAdminPages entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminPages>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.PageName.IsDefault())
                predicate = predicate.And(p => p.PageName == entity.PageName);

            if (!entity.PageUrl.IsDefault())
                predicate = predicate.And(p => p.PageUrl == entity.PageUrl);

            return XsiContext.Context.XsiAdminPages.Include(p => p.XsiAdminPermissions).AsExpandable().Where(predicate).ToList();
        }

        public XsiAdminPages Add(XsiAdminPages entity)
        {
            XsiAdminPages AdminPage = new XsiAdminPages();
            if (!entity.PageName.IsDefault())
                AdminPage.PageName = entity.PageName.Trim();
            if (!entity.PageUrl.IsDefault())
                AdminPage.PageUrl = entity.PageUrl.Trim();

            XsiContext.Context.XsiAdminPages.Add(AdminPage);

            SubmitChanges();
            return AdminPage;
        }
        public void Update(XsiAdminPages entity)
        {
            XsiAdminPages AdminPage = XsiContext.Context.XsiAdminPages.Find(entity.ItemId);
            XsiContext.Context.Entry(AdminPage).State = EntityState.Modified;

            if (!entity.PageName.IsDefault())
                AdminPage.PageName = entity.PageName.Trim();

            if (!entity.PageUrl.IsDefault())
                AdminPage.PageUrl = entity.PageUrl.Trim();
        }
        public void Delete(XsiAdminPages entity)
        {
            foreach (XsiAdminPages AdminPage in Select(entity))
            {
                XsiAdminPages apage = XsiContext.Context.XsiAdminPages.Find(AdminPage.ItemId);
                XsiContext.Context.XsiAdminPages.Remove(apage);
            }
        }
        public void Delete(long itemId)
        {
            XsiAdminPages apage = XsiContext.Context.XsiAdminPages.Find(itemId);
            XsiContext.Context.XsiAdminPages.Remove(apage);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}

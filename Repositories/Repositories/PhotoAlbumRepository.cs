﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PhotoAlbumRepository : IPhotoAlbumRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PhotoAlbumRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PhotoAlbumRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PhotoAlbumRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiPhotoAlbum GetById(long itemId)
        {
            return XsiContext.Context.XsiPhotoAlbum.Find(itemId);
        }

        public List<XsiPhotoAlbum> Select()
        {
            return XsiContext.Context.XsiPhotoAlbum.ToList();
        }
        public List<XsiPhotoAlbum> Select(XsiPhotoAlbum entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiPhotoAlbum>();
            var predicate = PredicateBuilder.True<XsiPhotoAlbum>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.SubCategoryId.IsDefault())
                predicate = predicate.And(p => p.SubCategoryId == entity.SubCategoryId);

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.TitleAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.TitleAr.Contains(searchKeys.str1) || p.TitleAr.Contains(searchKeys.str2) || p.TitleAr.Contains(searchKeys.str3) || p.TitleAr.Contains(searchKeys.str4) || p.TitleAr.Contains(searchKeys.str5)
                    || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                    || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                    || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
            
            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated == entity.Dated);

            if (!entity.SortIndex.IsDefault())
                predicate = predicate.And(p => p.SortIndex == entity.SortIndex);

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.DescriptionAr.IsDefault())
                predicate = predicate.And(p => p.DescriptionAr.Contains(entity.DescriptionAr));

            if (!entity.DatedAr.IsDefault())
                predicate = predicate.And(p => p.DatedAr == entity.DatedAr);

            if (!entity.SortIndexAr.IsDefault())
                predicate = predicate.And(p => p.SortIndexAr == entity.SortIndexAr);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiPhotoAlbum.AsExpandable().Where(predicate).ToList();
        }

        public XsiPhotoAlbum Add(XsiPhotoAlbum entity)
        {
            XsiPhotoAlbum PhotoAlbum = new XsiPhotoAlbum();
            PhotoAlbum.SubCategoryId = entity.SubCategoryId;
            PhotoAlbum.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                PhotoAlbum.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
                PhotoAlbum.Description = entity.Description.Trim();
            PhotoAlbum.Dated = entity.Dated;
            PhotoAlbum.SortIndex = entity.SortIndex;

            PhotoAlbum.SubCategoryId = entity.SubCategoryId;
            PhotoAlbum.IsActiveAr = entity.IsActiveAr;
            if (!entity.TitleAr.IsDefault())
                PhotoAlbum.TitleAr = entity.TitleAr.Trim();
            if (!entity.DescriptionAr.IsDefault())
                PhotoAlbum.DescriptionAr = entity.DescriptionAr.Trim();
            PhotoAlbum.DatedAr = entity.DatedAr;
            PhotoAlbum.SortIndexAr = entity.SortIndexAr;
            PhotoAlbum.CreatedOn = entity.CreatedOn;
            PhotoAlbum.CreatedBy = entity.CreatedBy;
            PhotoAlbum.ModifiedOn = entity.ModifiedOn;
            PhotoAlbum.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiPhotoAlbum.Add(PhotoAlbum);
            SubmitChanges();
            return PhotoAlbum;

        }
        public void Update(XsiPhotoAlbum entity)
        {
            XsiPhotoAlbum PhotoAlbum = XsiContext.Context.XsiPhotoAlbum.Find(entity.ItemId);
            XsiContext.Context.Entry(PhotoAlbum).State = EntityState.Modified;

            if (!entity.SubCategoryId.IsDefault())
                PhotoAlbum.SubCategoryId = entity.SubCategoryId;

            if (!entity.IsActive.IsDefault())
                PhotoAlbum.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                PhotoAlbum.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                PhotoAlbum.Description = entity.Description.Trim();

            if (!entity.Dated.IsDefault())
                PhotoAlbum.Dated = entity.Dated;

            if (!entity.SortIndex.IsDefault())
                PhotoAlbum.SortIndex = entity.SortIndex;

            if (!entity.IsActive.IsDefault())
                PhotoAlbum.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                PhotoAlbum.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                PhotoAlbum.DescriptionAr = entity.DescriptionAr.Trim();

            if (!entity.DatedAr.IsDefault())
                PhotoAlbum.DatedAr = entity.DatedAr;

            if (!entity.SortIndexAr.IsDefault())
                PhotoAlbum.SortIndexAr = entity.SortIndexAr;

            if (!entity.CreatedOn.IsDefault())
                PhotoAlbum.CreatedOn = entity.CreatedOn;

            if (!entity.CreatedBy.IsDefault())
                PhotoAlbum.CreatedBy = entity.CreatedBy;

            if (!entity.ModifiedOn.IsDefault())
                PhotoAlbum.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                PhotoAlbum.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiPhotoAlbum entity)
        {
            foreach (XsiPhotoAlbum PhotoAlbum in Select(entity))
            {
                XsiPhotoAlbum aPhotoAlbum = XsiContext.Context.XsiPhotoAlbum.Find(PhotoAlbum.ItemId);
                XsiContext.Context.XsiPhotoAlbum.Remove(aPhotoAlbum);
            }
        }
        public void Delete(long itemId)
        {
            XsiPhotoAlbum aPhotoAlbum = XsiContext.Context.XsiPhotoAlbum.Find(itemId);
            XsiContext.Context.XsiPhotoAlbum.Remove(aPhotoAlbum);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
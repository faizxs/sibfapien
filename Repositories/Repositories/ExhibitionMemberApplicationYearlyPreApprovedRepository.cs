﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionMemberApplicationYearlyPreApprovedRepository : IExhibitionMemberApplicationYearlyPreApprovedRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionMemberApplicationYearlyPreApprovedRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionMemberApplicationYearlyPreApprovedRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionMemberApplicationYearlyPreApprovedRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionMemberApplicationYearlyPreApproved GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionMemberApplicationYearlyPreApproved.Find(itemId);
        }
        public List<XsiExhibitionMemberApplicationYearlyPreApproved> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionMemberApplicationYearlyPreApproved> Select()
        {
            return XsiContext.Context.XsiExhibitionMemberApplicationYearlyPreApproved.ToList();
        }
        public List<XsiExhibitionMemberApplicationYearlyPreApproved> Select(XsiExhibitionMemberApplicationYearlyPreApproved entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMemberApplicationYearlyPreApproved>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.MemberExhibitionYearlyId.IsDefault())
                predicate = predicate.And(p => p.MemberExhibitionYearlyId == entity.MemberExhibitionYearlyId);

            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);

            if (!entity.MemberRoleId.IsDefault())
                predicate = predicate.And(p => p.MemberRoleId == entity.MemberRoleId);

            if (!entity.PublisherNameEn.IsDefault())
                predicate = predicate.And(p => p.PublisherNameEn.Equals(entity.PublisherNameEn));

            if (!entity.PublisherNameAr.IsDefault())
                predicate = predicate.And(p => p.PublisherNameAr.Equals(entity.PublisherNameAr));

            if (!entity.FileNumber.IsDefault())
                predicate = predicate.And(p => p.FileNumber.Equals(entity.FileNumber));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn.Equals(entity.CreatedOn));

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn.Equals(entity.ModifiedOn));

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiExhibitionMemberApplicationYearlyPreApproved.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionMemberApplicationYearlyPreApproved Add(XsiExhibitionMemberApplicationYearlyPreApproved entity)
        {
            XsiExhibitionMemberApplicationYearlyPreApproved ExhibitionMemberApplicationYearlyPreApproved = new XsiExhibitionMemberApplicationYearlyPreApproved();
          
            if (!entity.MemberExhibitionYearlyId.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;

            if (!entity.MemberId.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.MemberId = entity.MemberId;

            if (!entity.MemberRoleId.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.MemberRoleId = entity.MemberRoleId;

            if (!entity.PublisherNameEn.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.PublisherNameEn = entity.PublisherNameEn.Trim();
           
            if (!entity.PublisherNameAr.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.PublisherNameAr = entity.PublisherNameAr.Trim();
            
            if (!entity.FileNumber.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.FileNumber = entity.FileNumber.Trim();
            
            ExhibitionMemberApplicationYearlyPreApproved.IsActive = entity.IsActive;

            ExhibitionMemberApplicationYearlyPreApproved.CreatedOn = entity.CreatedOn;
            ExhibitionMemberApplicationYearlyPreApproved.CreatedBy = entity.CreatedBy;
            ExhibitionMemberApplicationYearlyPreApproved.ModifiedOn = entity.ModifiedOn;
            ExhibitionMemberApplicationYearlyPreApproved.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionMemberApplicationYearlyPreApproved.Add(ExhibitionMemberApplicationYearlyPreApproved);
            SubmitChanges();
            return ExhibitionMemberApplicationYearlyPreApproved;

        }
        public void Update(XsiExhibitionMemberApplicationYearlyPreApproved entity)
        {
            XsiExhibitionMemberApplicationYearlyPreApproved ExhibitionMemberApplicationYearlyPreApproved = XsiContext.Context.XsiExhibitionMemberApplicationYearlyPreApproved.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionMemberApplicationYearlyPreApproved).State = EntityState.Modified;

            if (!entity.MemberExhibitionYearlyId.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;

            if (!entity.MemberId.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.MemberId = entity.MemberId;

            if (!entity.MemberRoleId.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.MemberRoleId = entity.MemberRoleId;

            if (!entity.PublisherNameEn.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.PublisherNameEn = entity.PublisherNameEn.Trim();

            if (!entity.PublisherNameAr.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.PublisherNameAr = entity.PublisherNameAr.Trim();

            if (!entity.FileNumber.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.FileNumber = entity.FileNumber.Trim();

            if (!entity.IsActive.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.IsActive = entity.IsActive;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionMemberApplicationYearlyPreApproved.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiExhibitionMemberApplicationYearlyPreApproved entity)
        {
            foreach (XsiExhibitionMemberApplicationYearlyPreApproved ExhibitionMemberApplicationYearlyPreApproved in Select(entity))
            {
                XsiExhibitionMemberApplicationYearlyPreApproved aExhibitionMemberApplicationYearlyPreApproved = XsiContext.Context.XsiExhibitionMemberApplicationYearlyPreApproved.Find(ExhibitionMemberApplicationYearlyPreApproved.ItemId);
                XsiContext.Context.XsiExhibitionMemberApplicationYearlyPreApproved.Remove(aExhibitionMemberApplicationYearlyPreApproved);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionMemberApplicationYearlyPreApproved aExhibitionMemberApplicationYearlyPreApproved = XsiContext.Context.XsiExhibitionMemberApplicationYearlyPreApproved.Find(itemId);
            XsiContext.Context.XsiExhibitionMemberApplicationYearlyPreApproved.Remove(aExhibitionMemberApplicationYearlyPreApproved);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionBoothRepository : IExhibitionBoothRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionBoothRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionBoothRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionBoothRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionBooth GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionBooth.Find(itemId);
        }

        public List<XsiExhibitionBooth> Select()
        {
            return XsiContext.Context.XsiExhibitionBooth.ToList();
        }
        public List<XsiExhibitionBooth> Select(XsiExhibitionBooth entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBooth>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.WebsiteType.IsDefault())
                predicate = predicate.And(p => p.WebsiteType.Equals(entity.WebsiteType));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionBooth.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiExhibitionBooth> SelectOr(XsiExhibitionBooth entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionBooth>();
            var Outer = PredicateBuilder.True<XsiExhibitionBooth>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionBooth.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiExhibitionBooth> SelectOtherLanguageCategory(XsiExhibitionBooth entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBooth>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            predicate = PredicateBuilder.True<XsiExhibitionBooth>();

            return XsiContext.Context.XsiExhibitionBooth.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionBooth> SelectCurrentLanguageCategory(XsiExhibitionBooth entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBooth>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            //predicate = PredicateBuilder.True<XsiExhibitionBooth>();
            return XsiContext.Context.XsiExhibitionBooth.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionBooth> SelectComplete(XsiExhibitionBooth entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBooth>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.WebsiteType.IsDefault())
                predicate = predicate.And(p => p.WebsiteType.Equals(entity.WebsiteType));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            //return XsiContext.Context.XsiExhibitionBooth.AsExpandable().Where(predicate);
            return XsiContext.Context.XsiExhibitionBooth.Include(p => p.XsiExhibitionBoothSubsection).AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionBooth Add(XsiExhibitionBooth entity)
        {
            XsiExhibitionBooth ExhibitionBooth = new XsiExhibitionBooth();

            ExhibitionBooth.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                ExhibitionBooth.Title = entity.Title.Trim();
            ExhibitionBooth.WebsiteType = entity.WebsiteType;

            #region Ar
            ExhibitionBooth.IsActiveAr = entity.IsActiveAr;
            if (!entity.TitleAr.IsDefault())
                ExhibitionBooth.TitleAr = entity.TitleAr.Trim();
            #endregion Ar

            ExhibitionBooth.CreatedOn = entity.CreatedOn;
            ExhibitionBooth.CreatedBy = entity.CreatedBy;
            ExhibitionBooth.ModifiedOn = entity.ModifiedOn;
            ExhibitionBooth.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionBooth.Add(ExhibitionBooth);
            SubmitChanges();
            return ExhibitionBooth;

        }
        public void Update(XsiExhibitionBooth entity)
        {
            XsiExhibitionBooth ExhibitionBooth = XsiContext.Context.XsiExhibitionBooth.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionBooth).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                ExhibitionBooth.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                ExhibitionBooth.Title = entity.Title.Trim();

            if (!entity.WebsiteType.IsDefault())
                ExhibitionBooth.WebsiteType = entity.WebsiteType;

            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                ExhibitionBooth.IsActiveAr = entity.IsActiveAr;
            if (!entity.TitleAr.IsDefault())
                ExhibitionBooth.TitleAr = entity.TitleAr.Trim();
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionBooth.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionBooth.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiExhibitionBooth entity)
        {
            foreach (XsiExhibitionBooth ExhibitionBooth in Select(entity))
            {
                XsiExhibitionBooth aExhibitionBooth = XsiContext.Context.XsiExhibitionBooth.Find(ExhibitionBooth.ItemId);
                XsiContext.Context.XsiExhibitionBooth.Remove(aExhibitionBooth);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionBooth aExhibitionBooth = XsiContext.Context.XsiExhibitionBooth.Find(itemId);
            XsiContext.Context.XsiExhibitionBooth.Remove(aExhibitionBooth);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
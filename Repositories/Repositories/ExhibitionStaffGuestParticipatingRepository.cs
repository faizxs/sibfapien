﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionStaffGuestParticipatingRepository : IExhibitionStaffGuestParticipatingRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionStaffGuestParticipatingRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionStaffGuestParticipatingRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionStaffGuestParticipatingRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionStaffGuestParticipating GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionStaffGuestParticipating.Where(x => x.StaffGuestId == itemId).FirstOrDefault();
            // return XsiContext.Context.XsiExhibitionStaffGuestParticipating.Find(itemId);
        }
        public List<XsiExhibitionStaffGuestParticipating> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionStaffGuestParticipating> Select()
        {
            return XsiContext.Context.XsiExhibitionStaffGuestParticipating.ToList();
        }
        public XsiExhibitionStaffGuestParticipating GetByStaffGuestId(long itemId)
        {
            return XsiContext.Context.XsiExhibitionStaffGuestParticipating.Where(p => p.StaffGuestId == itemId).FirstOrDefault();
        }
        public List<XsiExhibitionStaffGuestParticipating> Select(XsiExhibitionStaffGuestParticipating entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionStaffGuestParticipating>();

            if (!entity.StaffGuestId.IsDefault())
                predicate = predicate.And(p => p.StaffGuestId == entity.StaffGuestId);

            //if (!entity.MemberExhibitionYearlyId.IsDefault())
            //    predicate = predicate.And(p => p.MemberExhibitionYearlyId == entity.MemberExhibitionYearlyId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            /*if (!entity.ExhibitorId.IsDefault())
                predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);*/
            //if (entity.XsiExhibitionMemberApplicationYearly != null)
            //{
            //    predicate = predicate.And(p => p.XsiExhibitionMemberApplicationYearly.MemberId == entity.XsiExhibitionMemberApplicationYearly.MemberId);
            //}
            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsUaeresident.IsDefault())
                predicate = predicate.And(p => p.IsUaeresident.Equals(entity.IsUaeresident));

            if (!entity.Uidno.IsDefault())
                predicate = predicate.And(p => p.Uidno.Equals(entity.Uidno));

            if (!entity.CountryUidno.IsDefault())
                predicate = predicate.And(p => p.CountryUidno.Equals(entity.CountryUidno));

            if (!entity.IsTravelDetails.IsDefault())
                predicate = predicate.And(p => p.IsTravelDetails.Equals(entity.IsTravelDetails));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.IsEmailSent.IsDefault())
                predicate = predicate.And(p => p.IsEmailSent.Equals(entity.IsEmailSent));

            if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.NameEn.IsDefault())
                predicate = predicate.And(p => p.NameEn.Contains(entity.NameEn));

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));

            if (!entity.NameAr.IsDefault())
                predicate = predicate.And(p => p.NameAr.Contains(entity.NameAr));

            if (!entity.Profession.IsDefault())
                predicate = predicate.And(p => p.Profession.Contains(entity.Profession));

            /*if (!entity.DOB.IsDefault())
                predicate = predicate.And(p => p.DOB == entity.DOB);

            if (!entity.POB.IsDefault())
                predicate = predicate.And(p => p.POB == entity.POB);*/

            if (!entity.Telephone.IsDefault())
                predicate = predicate.And(p => p.Telephone.Contains(entity.Telephone));

            if (!entity.Mobile.IsDefault())
                predicate = predicate.And(p => p.Mobile.Contains(entity.Mobile));

            if (!entity.TravelDetails.IsDefault())
                predicate = predicate.And(p => p.TravelDetails.Contains(entity.TravelDetails));

            if (!entity.NeedVisa.IsDefault())
                predicate = predicate.And(p => p.NeedVisa.Equals(entity.NeedVisa));

            if (!entity.ArrivalDate.IsDefault())
                predicate = predicate.And(p => p.ArrivalDate == entity.ArrivalDate);

            if (!entity.ArrivalAirportId.IsDefault())
                predicate = predicate.And(p => p.ArrivalAirportId == entity.ArrivalAirportId);

            if (!entity.ArrivalTerminalId.IsDefault())
                predicate = predicate.And(p => p.ArrivalTerminalId == entity.ArrivalTerminalId);

            if (!entity.DepartureDate.IsDefault())
                predicate = predicate.And(p => p.DepartureDate == entity.DepartureDate);

            if (!entity.Passport.IsDefault())
                predicate = predicate.And(p => p.Passport.Contains(entity.Passport));

            if (!entity.PlaceOfIssue.IsDefault())
                predicate = predicate.And(p => p.PlaceOfIssue.Contains(entity.PlaceOfIssue));

            if (!entity.DateOfIssue.IsDefault())
                predicate = predicate.And(p => p.DateOfIssue == entity.DateOfIssue);

            if (!entity.ExpiryDate.IsDefault())
                predicate = predicate.And(p => p.ExpiryDate == entity.ExpiryDate);

            if (!entity.PassportCopy.IsDefault())
                predicate = predicate.And(p => p.PassportCopy.Contains(entity.PassportCopy));

            if (!entity.Photo.IsDefault())
                predicate = predicate.And(p => p.Photo.Contains(entity.Photo));

            if (!entity.FlightTickets.IsDefault())
                predicate = predicate.And(p => p.FlightTickets.Contains(entity.FlightTickets));

            if (!entity.VisaCopy.IsDefault())
                predicate = predicate.And(p => p.VisaCopy.Contains(entity.VisaCopy));

            if (!entity.Invoice.IsDefault())
                predicate = predicate.And(p => p.Invoice.Contains(entity.Invoice));

            if (!entity.Notes.IsDefault())
                predicate = predicate.And(p => p.Notes.Contains(entity.Notes));

            if (!entity.VisaApplicationForm.IsDefault())
                predicate = predicate.And(p => p.VisaApplicationForm.Contains(entity.VisaApplicationForm));

            if (!entity.NoofVisaMonths.IsDefault())
                predicate = predicate.And(p => p.NoofVisaMonths.Equals(entity.NoofVisaMonths));

            if (!entity.GuestType.IsDefault())
                predicate = predicate.And(p => p.GuestType.Equals(entity.GuestType));

            if (!entity.GuestCategory.IsDefault())
                predicate = predicate.And(p => p.GuestCategory.Equals(entity.GuestCategory));

            if (!entity.ActivityDate.IsDefault())
                predicate = predicate.And(p => p.ActivityDate == entity.ActivityDate);

            if (!entity.PaidVisa.IsDefault())
                predicate = predicate.And(p => p.PaidVisa.Contains(entity.PaidVisa));

            if (!entity.PassportCopyTwo.IsDefault())
                predicate = predicate.And(p => p.PassportCopyTwo.Contains(entity.PassportCopyTwo));

            if (!entity.VisaType.IsDefault())
                predicate = predicate.And(p => p.VisaType.Equals(entity.VisaType));

            if (!entity.HealthInsurance.IsDefault())
                predicate = predicate.And(p => p.HealthInsurance.Contains(entity.HealthInsurance));

            if (!entity.HealthInsuranceTwo.IsDefault())
                predicate = predicate.And(p => p.HealthInsuranceTwo.Contains(entity.HealthInsuranceTwo));

            //if (!entity.StaffGuestAdminNote.IsDefault())
            //    predicate = predicate.And(p => p.StaffGuestAdminNote.Contains(entity.StaffGuestAdminNote));

            if (!entity.IsVisitedUae.IsDefault())
                predicate = predicate.And(p => p.IsVisitedUae.Equals(entity.IsVisitedUae));

            if (!entity.RejectedStatus.IsDefault())
                predicate = predicate.And(p => p.RejectedStatus.Equals(entity.RejectedStatus));

            if (!entity.MainGuestId.IsDefault())
                predicate = predicate.And(p => p.MainGuestId == entity.MainGuestId);

            if (!entity.MainGuest.IsDefault())
                predicate = predicate.And(p => p.MainGuest.Contains(entity.MainGuest));

            if (!entity.IsCompanion.IsDefault())
                predicate = predicate.And(p => p.IsCompanion.Equals(entity.IsCompanion));

            if (!entity.CompanionDescription.IsDefault())
                predicate = predicate.And(p => p.CompanionDescription.Contains(entity.CompanionDescription));

            if (!entity.IsFromPcregistration.IsDefault())
                predicate = predicate.And(p => p.IsFromPcregistration.Equals(entity.IsFromPcregistration));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionStaffGuestParticipating.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionStaffGuestParticipating Add(XsiExhibitionStaffGuestParticipating entity)
        {
            XsiExhibitionStaffGuestParticipating ExhibitionStaffGuestParticipating = new XsiExhibitionStaffGuestParticipating();

            //ExhibitionStaffGuestParticipating.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;
            //StaffGuest.ExhibitorId = entity.ExhibitorId;
            ExhibitionStaffGuestParticipating.StaffGuestId = entity.StaffGuestId;
            ExhibitionStaffGuestParticipating.ExhibitionId = entity.ExhibitionId;
            ExhibitionStaffGuestParticipating.CountryId = entity.CountryId;
            ExhibitionStaffGuestParticipating.IsActive = entity.IsActive;
            ExhibitionStaffGuestParticipating.IsUaeresident = entity.IsUaeresident;
            if (!entity.Uidno.IsDefault())
                ExhibitionStaffGuestParticipating.Uidno = entity.Uidno.Trim();
            if (!entity.CountryUidno.IsDefault())
                ExhibitionStaffGuestParticipating.CountryUidno = entity.CountryUidno.Trim();
            ExhibitionStaffGuestParticipating.IsTravelDetails = entity.IsTravelDetails;
            ExhibitionStaffGuestParticipating.Status = entity.Status;
            ExhibitionStaffGuestParticipating.IsEmailSent = entity.IsEmailSent;
            ExhibitionStaffGuestParticipating.LanguageId = entity.LanguageId;
            if (!entity.NameEn.IsDefault())
                ExhibitionStaffGuestParticipating.NameEn = entity.NameEn.Trim();
            if (!entity.LastName.IsDefault())
                ExhibitionStaffGuestParticipating.LastName = entity.LastName.Trim();
            if (!entity.NameAr.IsDefault())
                ExhibitionStaffGuestParticipating.NameAr = entity.NameAr.Trim();
            if (!entity.Profession.IsDefault())
                ExhibitionStaffGuestParticipating.Profession = entity.Profession.Trim();
            /*StaffGuest.DOB = entity.DOB;
            if (!entity.POB.IsDefault())
                StaffGuest.POB = entity.POB.Trim();*/
            if (!entity.Telephone.IsDefault())
                ExhibitionStaffGuestParticipating.Telephone = entity.Telephone.Trim();
            if (!entity.Mobile.IsDefault())
                ExhibitionStaffGuestParticipating.Mobile = entity.Mobile.Trim();
            if (!entity.TravelDetails.IsDefault())
                ExhibitionStaffGuestParticipating.TravelDetails = entity.TravelDetails.Trim();
            if (!entity.NeedVisa.IsDefault())
                ExhibitionStaffGuestParticipating.NeedVisa = entity.NeedVisa.Trim();
            ExhibitionStaffGuestParticipating.ArrivalDate = entity.ArrivalDate;
            ExhibitionStaffGuestParticipating.ArrivalAirportId = entity.ArrivalAirportId;
            ExhibitionStaffGuestParticipating.ArrivalTerminalId = entity.ArrivalTerminalId;
            ExhibitionStaffGuestParticipating.DepartureDate = entity.DepartureDate;
            if (!entity.Passport.IsDefault())
                ExhibitionStaffGuestParticipating.Passport = entity.Passport.Trim();
            if (!entity.PlaceOfIssue.IsDefault())
                ExhibitionStaffGuestParticipating.PlaceOfIssue = entity.PlaceOfIssue.Trim();
            ExhibitionStaffGuestParticipating.DateOfIssue = entity.DateOfIssue;
            ExhibitionStaffGuestParticipating.ExpiryDate = entity.ExpiryDate;
            ExhibitionStaffGuestParticipating.PassportCopy = entity.PassportCopy;
            ExhibitionStaffGuestParticipating.Photo = entity.Photo;
            ExhibitionStaffGuestParticipating.FlightTickets = entity.FlightTickets;
            ExhibitionStaffGuestParticipating.VisaCopy = entity.VisaCopy;
            if (!entity.Invoice.IsDefault())
                ExhibitionStaffGuestParticipating.Invoice = entity.Invoice.Trim();
            if (!entity.Notes.IsDefault())
                ExhibitionStaffGuestParticipating.Notes = entity.Notes.Trim();
            if (!entity.VisaApplicationForm.IsDefault())
                ExhibitionStaffGuestParticipating.VisaApplicationForm = entity.VisaApplicationForm.Trim();

            #region Guest Details
            
            if (!entity.MainGuestId.IsDefault())
                ExhibitionStaffGuestParticipating.MainGuestId = entity.MainGuestId;

            ExhibitionStaffGuestParticipating.MainGuest = entity.MainGuest;
            ExhibitionStaffGuestParticipating.IsCompanion = entity.IsCompanion;
            ExhibitionStaffGuestParticipating.NoofVisaMonths = entity.NoofVisaMonths;
            ExhibitionStaffGuestParticipating.GuestType = entity.GuestType;
            ExhibitionStaffGuestParticipating.GuestCategory = entity.GuestCategory;
            ExhibitionStaffGuestParticipating.ActivityDate = entity.ActivityDate;
            ExhibitionStaffGuestParticipating.PaidVisa = entity.PaidVisa;
            //ExhibitionStaffGuestParticipating.StaffGuestAdminNote = entity.StaffGuestAdminNote;
            #endregion

            ExhibitionStaffGuestParticipating.PassportCopyTwo = entity.PassportCopyTwo;
            ExhibitionStaffGuestParticipating.VisaType = entity.VisaType;
            ExhibitionStaffGuestParticipating.HealthInsurance = entity.HealthInsurance;
            ExhibitionStaffGuestParticipating.HealthInsuranceTwo = entity.HealthInsuranceTwo;
            ExhibitionStaffGuestParticipating.AddedBy = entity.AddedBy;
            ExhibitionStaffGuestParticipating.IsVisitedUae = entity.IsVisitedUae;
            ExhibitionStaffGuestParticipating.RejectedStatus = entity.RejectedStatus;
            ExhibitionStaffGuestParticipating.MainGuest = entity.MainGuest;
            ExhibitionStaffGuestParticipating.IsCompanion = entity.IsCompanion;
            ExhibitionStaffGuestParticipating.CompanionDescription = entity.CompanionDescription;
            if (!entity.IsFromPcregistration.IsDefault())
                ExhibitionStaffGuestParticipating.IsFromPcregistration = entity.IsFromPcregistration.Trim();
            ExhibitionStaffGuestParticipating.CreatedOn = entity.CreatedOn;
            ExhibitionStaffGuestParticipating.CreatedBy = entity.CreatedBy;
            ExhibitionStaffGuestParticipating.ModifiedOn = entity.ModifiedOn;
            ExhibitionStaffGuestParticipating.ModifiedBy = entity.ModifiedBy;



            XsiContext.Context.XsiExhibitionStaffGuestParticipating.Add(ExhibitionStaffGuestParticipating);
            SubmitChanges();
            return ExhibitionStaffGuestParticipating;

        }
        public void Update(XsiExhibitionStaffGuestParticipating entity)
        {
            //XsiExhibitionStaffGuestParticipating ExhibitionStaffGuestParticipating = XsiContext.Context.XsiExhibitionStaffGuestParticipating.Find(entity.StaffGuestId);
            XsiExhibitionStaffGuestParticipating ExhibitionStaffGuestParticipating = XsiContext.Context.XsiExhibitionStaffGuestParticipating.Where(x => x.StaffGuestId == entity.StaffGuestId && x.ExhibitionId == entity.ExhibitionId).FirstOrDefault();
            XsiContext.Context.Entry(ExhibitionStaffGuestParticipating).State = EntityState.Modified;


            //if (!entity.MemberExhibitionYearlyId.IsDefault())
            //    ExhibitionStaffGuestParticipating.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;

            /*if (!entity.ExhibitorId.IsDefault())
                ExhibitionStaffGuestParticipating.ExhibitorId = entity.ExhibitorId;*/

            if (!entity.ExhibitionId.IsDefault())
                ExhibitionStaffGuestParticipating.ExhibitionId = entity.ExhibitionId;

            if (!entity.CountryId.IsDefault())
                ExhibitionStaffGuestParticipating.CountryId = entity.CountryId;

            if (!entity.IsActive.IsDefault())
                ExhibitionStaffGuestParticipating.IsActive = entity.IsActive;

            if (!entity.Uidno.IsDefault())
                ExhibitionStaffGuestParticipating.Uidno = entity.Uidno.Trim();

            if (!entity.CountryUidno.IsDefault())
                ExhibitionStaffGuestParticipating.CountryUidno = entity.CountryUidno.Trim();

            if (!entity.IsTravelDetails.IsDefault())
                ExhibitionStaffGuestParticipating.IsTravelDetails = entity.IsTravelDetails;

            if (!entity.Status.IsDefault())
                ExhibitionStaffGuestParticipating.Status = entity.Status;

            if (!entity.IsUaeresident.IsDefault())
                ExhibitionStaffGuestParticipating.IsUaeresident = entity.IsUaeresident;

            if (!entity.IsEmailSent.IsDefault())
                ExhibitionStaffGuestParticipating.IsEmailSent = entity.IsEmailSent;

            if (!entity.LanguageId.IsDefault())
                ExhibitionStaffGuestParticipating.LanguageId = entity.LanguageId;

            if (!entity.NameEn.IsDefault())
                ExhibitionStaffGuestParticipating.NameEn = entity.NameEn.Trim();

            if (!entity.LastName.IsDefault())
                ExhibitionStaffGuestParticipating.LastName = entity.LastName.Trim();

            if (!entity.NameAr.IsDefault())
                ExhibitionStaffGuestParticipating.NameAr = entity.NameAr.Trim();

            if (!entity.Profession.IsDefault())
                ExhibitionStaffGuestParticipating.Profession = entity.Profession.Trim();

            /*if (!entity.DOB.IsDefault())
                ExhibitionStaffGuestParticipating.DOB = entity.DOB;

            if (!entity.POB.IsDefault())
                ExhibitionStaffGuestParticipating.POB = entity.POB.Trim();*/

            if (!entity.Telephone.IsDefault())
                ExhibitionStaffGuestParticipating.Telephone = entity.Telephone.Trim();

            if (!entity.Mobile.IsDefault())
                ExhibitionStaffGuestParticipating.Mobile = entity.Mobile.Trim();

            if (!entity.TravelDetails.IsDefault())
                ExhibitionStaffGuestParticipating.TravelDetails = entity.TravelDetails.Trim();

            if (!entity.NeedVisa.IsDefault())
                ExhibitionStaffGuestParticipating.NeedVisa = entity.NeedVisa;

            if (!entity.ArrivalDate.IsDefault())
                ExhibitionStaffGuestParticipating.ArrivalDate = entity.ArrivalDate;

            if (!entity.ArrivalAirportId.IsDefault())
                ExhibitionStaffGuestParticipating.ArrivalAirportId = entity.ArrivalAirportId;

            if (!entity.ArrivalTerminalId.IsDefault())
                ExhibitionStaffGuestParticipating.ArrivalTerminalId = entity.ArrivalTerminalId;

            if (!entity.DepartureDate.IsDefault())
                ExhibitionStaffGuestParticipating.DepartureDate = entity.DepartureDate;

            if (!entity.Passport.IsDefault())
                ExhibitionStaffGuestParticipating.Passport = entity.Passport.Trim();

            if (!entity.PlaceOfIssue.IsDefault())
                ExhibitionStaffGuestParticipating.PlaceOfIssue = entity.PlaceOfIssue.Trim();

            if (!entity.DateOfIssue.IsDefault())
                ExhibitionStaffGuestParticipating.DateOfIssue = entity.DateOfIssue;

            if (!entity.ExpiryDate.IsDefault())
                ExhibitionStaffGuestParticipating.ExpiryDate = entity.ExpiryDate;

            if (!entity.PassportCopy.IsDefault())
                ExhibitionStaffGuestParticipating.PassportCopy = entity.PassportCopy;

            if (!entity.Photo.IsDefault())
                ExhibitionStaffGuestParticipating.Photo = entity.Photo;

            if (!entity.FlightTickets.IsDefault())
                ExhibitionStaffGuestParticipating.FlightTickets = entity.FlightTickets;

            if (!entity.VisaCopy.IsDefault())
                ExhibitionStaffGuestParticipating.VisaCopy = entity.VisaCopy;

            if (!entity.Invoice.IsDefault())
                ExhibitionStaffGuestParticipating.Invoice = entity.Invoice.Trim();

            if (!entity.Notes.IsDefault())
                ExhibitionStaffGuestParticipating.Notes = entity.Notes.Trim();

            if (!entity.VisaApplicationForm.IsDefault())
                ExhibitionStaffGuestParticipating.VisaApplicationForm = entity.VisaApplicationForm.Trim();

            if (!entity.NoofVisaMonths.IsDefault())
                ExhibitionStaffGuestParticipating.NoofVisaMonths = entity.NoofVisaMonths;

            if (!entity.GuestType.IsDefault())
                ExhibitionStaffGuestParticipating.GuestType = entity.GuestType;

            if (!entity.GuestCategory.IsDefault())
                ExhibitionStaffGuestParticipating.GuestCategory = entity.GuestCategory;

            if (!entity.ActivityDate.IsDefault())
                ExhibitionStaffGuestParticipating.ActivityDate = entity.ActivityDate;

            if (!entity.PaidVisa.IsDefault())
                ExhibitionStaffGuestParticipating.PaidVisa = entity.PaidVisa;

            //if (!entity.StaffGuestAdminNote.IsDefault())
            //    ExhibitionStaffGuestParticipating.StaffGuestAdminNote = entity.StaffGuestAdminNote.Trim();

            if (!entity.PassportCopyTwo.IsDefault())
                ExhibitionStaffGuestParticipating.PassportCopyTwo = entity.PassportCopyTwo.Trim();

            if (!entity.VisaType.IsDefault())
                ExhibitionStaffGuestParticipating.VisaType = entity.VisaType.Trim();

            if (!entity.HealthInsurance.IsDefault())
                ExhibitionStaffGuestParticipating.HealthInsurance = entity.HealthInsurance.Trim();

            if (!entity.HealthInsuranceTwo.IsDefault())
                ExhibitionStaffGuestParticipating.HealthInsuranceTwo = entity.HealthInsuranceTwo.Trim();

            if (!entity.IsVisitedUae.IsDefault())
                ExhibitionStaffGuestParticipating.IsVisitedUae = entity.IsVisitedUae;

            if (!entity.RejectedStatus.IsDefault())
                ExhibitionStaffGuestParticipating.RejectedStatus = entity.RejectedStatus;

            if (!entity.MainGuestId.IsDefault())
                ExhibitionStaffGuestParticipating.MainGuestId = entity.MainGuestId;

            if (!entity.MainGuest.IsDefault())
                ExhibitionStaffGuestParticipating.MainGuest = entity.MainGuest;

            if (!entity.IsCompanion.IsDefault())
                ExhibitionStaffGuestParticipating.IsCompanion = entity.IsCompanion;

            if (!entity.CompanionDescription.IsDefault())
                ExhibitionStaffGuestParticipating.CompanionDescription = entity.CompanionDescription;

            if (!entity.AddedBy.IsDefault())
                ExhibitionStaffGuestParticipating.AddedBy = entity.AddedBy;

            if (!entity.StaffGuestId.IsDefault())
                ExhibitionStaffGuestParticipating.StaffGuestId = entity.StaffGuestId;

            if (!entity.IsActive.IsDefault())
                ExhibitionStaffGuestParticipating.IsActive = entity.IsActive;

            if (!entity.IsFromPcregistration.IsDefault())
                ExhibitionStaffGuestParticipating.IsFromPcregistration = entity.IsFromPcregistration;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionStaffGuestParticipating.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionStaffGuestParticipating.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiExhibitionStaffGuestParticipating entity)
        {
            foreach (XsiExhibitionStaffGuestParticipating ExhibitionStaffGuestParticipating in Select(entity))
            {
                XsiExhibitionStaffGuestParticipating aExhibitionStaffGuestParticipating = XsiContext.Context.XsiExhibitionStaffGuestParticipating.Where(i => i.StaffGuestId == ExhibitionStaffGuestParticipating.StaffGuestId && i.ExhibitionId == ExhibitionStaffGuestParticipating.ExhibitionId).FirstOrDefault();
                XsiContext.Context.XsiExhibitionStaffGuestParticipating.Remove(aExhibitionStaffGuestParticipating);
            }
        }
        public void Delete(long itemId)
        {
            // XsiExhibitionStaffGuestParticipating aExhibitionStaffGuestParticipating = XsiContext.Context.XsiExhibitionStaffGuestParticipating.Find(itemId);
            XsiExhibitionStaffGuestParticipating aExhibitionStaffGuestParticipating = XsiContext.Context.XsiExhibitionStaffGuestParticipating.Where(x => x.StaffGuestId == itemId).FirstOrDefault();
            XsiContext.Context.XsiExhibitionStaffGuestParticipating.Remove(aExhibitionStaffGuestParticipating);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
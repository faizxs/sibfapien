﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFPhotoRepository : ISCRFPhotoRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFPhotoRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFPhotoRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFPhotoRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfphoto GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfphoto.Find(itemId);
        }
       
        public List<XsiScrfphoto> Select()
        {
            return XsiContext.Context.XsiScrfphoto.ToList();
        }
        public List<XsiScrfphoto> Select(XsiScrfphoto entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfphoto>();
            var predicate = PredicateBuilder.True<XsiScrfphoto>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            //if (!entity.CategoryId.IsDefault())
            //    predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

           
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsAlbumCover.IsDefault())
                predicate = predicate.And(p => p.IsAlbumCover.Equals(entity.IsAlbumCover));

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail.Contains(entity.Thumbnail));

            if (!entity.SortIndex.IsDefault())
                predicate = predicate.And(p => p.SortIndex == entity.SortIndex);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiScrfphoto.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfphoto Add(XsiScrfphoto entity)
        {
            XsiScrfphoto Photo = new XsiScrfphoto();
            //Photo.CategoryId = entity.CategoryId;
          
            Photo.IsActive = entity.IsActive;
            Photo.IsAlbumCover = entity.IsAlbumCover;
            if (!entity.Title.IsDefault())
                Photo.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
            Photo.Description = entity.Description.Trim();
            Photo.Thumbnail = entity.Thumbnail;
            Photo.SortIndex = entity.SortIndex;
            Photo.CreatedOn = entity.CreatedOn;
            Photo.CreatedBy = entity.CreatedBy;
            Photo.ModifiedOn = entity.ModifiedOn;
            Photo.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfphoto.Add(Photo);
            SubmitChanges();
            return Photo;

        }
        public void Update(XsiScrfphoto entity)
        {
            XsiScrfphoto Photo = XsiContext.Context.XsiScrfphoto.Find(entity.ItemId);
            XsiContext.Context.Entry(Photo).State = EntityState.Modified;

            //if (!entity.CategoryId.IsDefault())
            //    Photo.CategoryId = entity.CategoryId;

            
            if (!entity.IsActive.IsDefault())
                Photo.IsActive = entity.IsActive;

            if (!entity.IsAlbumCover.IsDefault())
                Photo.IsAlbumCover = entity.IsAlbumCover;

            if (!entity.Title.IsDefault())
                Photo.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                Photo.Description = entity.Description.Trim();

            if (!entity.Thumbnail.IsDefault())
                Photo.Thumbnail = entity.Thumbnail;

            if (!entity.SortIndex.IsDefault())
                Photo.SortIndex = entity.SortIndex;

            if (!entity.CreatedOn.IsDefault())
                Photo.CreatedOn = entity.CreatedOn;

            if (!entity.CreatedBy.IsDefault())
                Photo.CreatedBy = entity.CreatedBy;

            if (!entity.ModifiedOn.IsDefault())
                Photo.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                Photo.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiScrfphoto entity)
        {
            foreach (XsiScrfphoto Photo in Select(entity))
            {
                XsiScrfphoto aPhoto = XsiContext.Context.XsiScrfphoto.Find(Photo.ItemId);
                XsiContext.Context.XsiScrfphoto.Remove(aPhoto);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfphoto aPhoto = XsiContext.Context.XsiScrfphoto.Find(itemId);
            XsiContext.Context.XsiScrfphoto.Remove(aPhoto);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
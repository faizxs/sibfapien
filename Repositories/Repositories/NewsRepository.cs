﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class NewsRepository : INewsRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public NewsRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public NewsRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public NewsRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiNews GetById(long itemId)
        {
            return XsiContext.Context.XsiNews.Find(itemId);
        }
         
        public List<XsiNews> Select()
        {
            return XsiContext.Context.XsiNews.ToList();
        }
        public List<XsiNews> Select(XsiNews entity)
        {
            var predicate = PredicateBuilder.True<XsiNews>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
                        
            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated == entity.Dated);

            if (!entity.Detail.IsDefault())
                predicate = predicate.And(p => p.Detail.Contains(entity.Detail));

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Equals(entity.ImageName));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Equals(entity.FileName));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiNews.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiNews> SelectComeplete(XsiNews entity,long langId)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiNews>();
            var predicate = PredicateBuilder.True<XsiNews>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
                       
            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            //if (!entity.Title.IsDefault())
            //    predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (langId == 2 && !entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.TitleAr.IsDefault())
                {
                    string strSearchText = entity.TitleAr.ToLower();
                    Inner = Inner.Or(p => p.TitleAr.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }


            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated == entity.Dated);

            if (!entity.Detail.IsDefault())
                predicate = predicate.And(p => p.Detail.Contains(entity.Detail));

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Equals(entity.ImageName));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Equals(entity.FileName));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiNews.Include(p => p.CategoryId).AsExpandable().Where(predicate).ToList();
        }

        public XsiNews Add(XsiNews entity)
        {
            XsiNews News = new XsiNews();
            News.CategoryId = entity.CategoryId;
            News.WebsiteId = entity.WebsiteId;
            if (!entity.Title.IsDefault())
                News.Title = entity.Title.Trim();
            News.Dated = entity.Dated;
            if (!entity.Detail.IsDefault())
                News.Detail = entity.Detail.Trim();
            if (!entity.Overview.IsDefault())
                News.Overview = entity.Overview.Trim();
            News.IsActive = entity.IsActive;
            News.ImageName = entity.ImageName;
            News.FileName = entity.FileName;

            #region Ar
            News.DatedAr = entity.DatedAr;
            if (!entity.TitleAr.IsDefault())
                News.TitleAr = entity.TitleAr.Trim();
            if (!entity.OverviewAr.IsDefault())
                News.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.DetailAr.IsDefault())
                News.DetailAr = entity.DetailAr;
            if (!entity.ImageNameAr.IsDefault())
                News.ImageNameAr = entity.ImageNameAr;
            News.IsActiveAr = entity.IsActiveAr;
            News.ImageNameAr = entity.ImageName;
            News.FileNameAr = entity.FileName;
            #endregion Ar

            News.CreatedOn = entity.CreatedOn;
            News.CreatedBy = entity.CreatedBy;
            News.ModifiedOn = entity.ModifiedOn;
            News.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiNews.Add(News);
            SubmitChanges();
            return News;

        }
        public void Update(XsiNews entity)
        {
            XsiNews News = XsiContext.Context.XsiNews.Find(entity.ItemId);
            XsiContext.Context.Entry(News).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                News.Title = entity.Title.Trim();

            if (!entity.WebsiteId.IsDefault())
                News.WebsiteId = entity.WebsiteId;

            if (!entity.Dated.IsDefault())
                News.Dated = entity.Dated;

            if (!entity.Detail.IsDefault())
                News.Detail = entity.Detail.Trim();

            if (!entity.Overview.IsDefault())
                News.Overview = entity.Overview.Trim();

            if (!entity.IsActive.IsDefault())
                News.IsActive = entity.IsActive;

            if (!entity.ImageName.IsDefault())
                News.ImageName = entity.ImageName;

            if (!entity.FileName.IsDefault())
                News.FileName = entity.FileName;
            #region Ar
            if (!entity.DatedAr.IsDefault())
                News.DatedAr = entity.DatedAr;
            if (!entity.TitleAr.IsDefault())
                News.TitleAr = entity.TitleAr.Trim();
            if (!entity.OverviewAr.IsDefault())
                News.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.DetailAr.IsDefault())
                News.DetailAr = entity.DetailAr;
            if (!entity.ImageNameAr.IsDefault())
                News.ImageNameAr = entity.ImageNameAr;
            if (!entity.IsActiveAr.IsDefault())
                News.IsActiveAr = entity.IsActiveAr;
            if (!entity.FileNameAr.IsDefault())
                News.FileNameAr = entity.FileName;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                News.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                News.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiNews entity)
        {
            foreach (XsiNews News in Select(entity))
            {
                XsiNews aNews = XsiContext.Context.XsiNews.Find(News.ItemId);
                XsiContext.Context.XsiNews.Remove(aNews);
            }
        }
        public void Delete(long itemId)
        {
            XsiNews aNews = XsiContext.Context.XsiNews.Find(itemId);
            XsiContext.Context.XsiNews.Remove(aNews);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
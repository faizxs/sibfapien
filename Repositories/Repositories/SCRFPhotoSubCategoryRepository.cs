﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFPhotoSubCategoryRepository : ISCRFPhotoSubCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFPhotoSubCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFPhotoSubCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFPhotoSubCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfphotoSubCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfphotoSubCategory.Find(itemId);
        }
         
        public List<XsiScrfphotoSubCategory> Select()
        {
            return XsiContext.Context.XsiScrfphotoSubCategory.ToList();
        }
        public List<XsiScrfphotoSubCategory> Select(XsiScrfphotoSubCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfphotoSubCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiScrfphotoSubCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiScrfphotoSubCategory> SelectComeplete(XsiScrfphotoSubCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfphotoSubCategory>();
            var predicate = PredicateBuilder.True<XsiScrfphotoSubCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.CategoryId.IsDefault())
            {
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);
                XsiScrfphotoCategory MainEntity = XsiContext.Context.XsiScrfphotoCategory.Where(p => p.ItemId == entity.CategoryId).FirstOrDefault();

                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title;
                    var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                    Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                        || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                        || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                        || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                        );

                    isInner = true;
                }
                else
                {
                    if (!entity.Title.IsDefault())
                    {
                        string strSearchText = entity.Title.ToLower();
                        Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                        isInner = true;
                    }
                }
            }
            else
            {
                if (!entity.Title.IsDefault())
                    predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));
            }
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiScrfphotoSubCategory.Include(p => p.Category).AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfphotoSubCategory Add(XsiScrfphotoSubCategory entity)
        {
            XsiScrfphotoSubCategory PhotoSubCategory = new XsiScrfphotoSubCategory();
            
            PhotoSubCategory.CategoryId = entity.CategoryId;
            if (!entity.Title.IsDefault())
            PhotoSubCategory.Title = entity.Title.Trim();
            PhotoSubCategory.IsActive = entity.IsActive;
            if (!entity.TitleAr.IsDefault())
                PhotoSubCategory.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                PhotoSubCategory.IsActiveAr = entity.IsActiveAr;
            PhotoSubCategory.CreatedOn = entity.CreatedOn;
            PhotoSubCategory.CreatedBy = entity.CreatedBy;
            PhotoSubCategory.ModifiedOn = entity.ModifiedOn;
            PhotoSubCategory.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfphotoSubCategory.Add(PhotoSubCategory);
            SubmitChanges();
            return PhotoSubCategory;

        }
        public void Update(XsiScrfphotoSubCategory entity)
        {
            XsiScrfphotoSubCategory PhotoSubCategory = XsiContext.Context.XsiScrfphotoSubCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(PhotoSubCategory).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                PhotoSubCategory.Title = entity.Title.Trim();

            if (!entity.IsActive.IsDefault())
                PhotoSubCategory.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                PhotoSubCategory.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                PhotoSubCategory.IsActiveAr = entity.IsActiveAr;

            if (!entity.ModifiedOn.IsDefault())
                PhotoSubCategory.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                PhotoSubCategory.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiScrfphotoSubCategory entity)
        {
            foreach (XsiScrfphotoSubCategory PhotoSubCategory in Select(entity))
            {
                XsiScrfphotoSubCategory aPhotoSubCategory = XsiContext.Context.XsiScrfphotoSubCategory.Find(PhotoSubCategory.ItemId);
                XsiContext.Context.XsiScrfphotoSubCategory.Remove(aPhotoSubCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfphotoSubCategory aPhotoSubCategory = XsiContext.Context.XsiScrfphotoSubCategory.Find(itemId);
            XsiContext.Context.XsiScrfphotoSubCategory.Remove(aPhotoSubCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
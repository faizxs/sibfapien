﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFCityRepository : ISCRFCityRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFCityRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFCityRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFCityRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfcity GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfcity.Find(itemId);
        }

        public List<XsiScrfcity> Select()
        {
            return XsiContext.Context.XsiScrfcity.ToList();
        }
        public List<XsiScrfcity> Select(XsiScrfcity entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfcity>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiScrfcity.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiScrfcity> SelectComeplete(XsiScrfcity entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfcity>();
            var predicate = PredicateBuilder.True<XsiScrfcity>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.CountryId.IsDefault())
            {
                predicate = predicate.And(p => p.CountryId == entity.CountryId);
                XsiScrfcountry MainEntity = XsiContext.Context.XsiScrfcountry.Where(p => p.ItemId == entity.CountryId).FirstOrDefault();

                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title;
                    var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                    Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                        || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                        || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                        || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                        );

                    isInner = true;
                }
                else
                {
                    if (!entity.Title.IsDefault())
                    {
                        string strSearchText = entity.Title.ToLower();
                        Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                        isInner = true;
                    }
                }
            }
            else
            {
                if (!entity.Title.IsDefault())
                    predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));
            }
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiScrfcity.Include(p => p.Country).AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfcity Add(XsiScrfcity entity)
        {
            XsiScrfcity SCRFCity = new XsiScrfcity();
            SCRFCity.CountryId = entity.CountryId;
            if (!entity.Title.IsDefault())
            SCRFCity.Title = entity.Title.Trim();
            SCRFCity.IsActive = entity.IsActive;
            SCRFCity.CreatedOn = entity.CreatedOn;
            SCRFCity.CreatedBy = entity.CreatedBy;
            SCRFCity.ModifiedOn = entity.ModifiedOn;
            SCRFCity.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfcity.Add(SCRFCity);
            SubmitChanges();
            return SCRFCity;

        }
        public void Update(XsiScrfcity entity)
        {
            XsiScrfcity SCRFCity = XsiContext.Context.XsiScrfcity.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFCity).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                SCRFCity.Title = entity.Title.Trim();

            if (!entity.IsActive.IsDefault())
                SCRFCity.IsActive = entity.IsActive;

            if (!entity.ModifiedOn.IsDefault())
                SCRFCity.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                SCRFCity.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiScrfcity entity)
        {
            foreach (XsiScrfcity SCRFCity in Select(entity))
            {
                XsiScrfcity aSCRFCity = XsiContext.Context.XsiScrfcity.Find(SCRFCity.ItemId);
                XsiContext.Context.XsiScrfcity.Remove(aSCRFCity);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfcity aSCRFCity = XsiContext.Context.XsiScrfcity.Find(itemId);
            XsiContext.Context.XsiScrfcity.Remove(aSCRFCity);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
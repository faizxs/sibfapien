﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class HomepageBannerNewRepository : IHomepageBannerNewRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public HomepageBannerNewRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public HomepageBannerNewRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public HomepageBannerNewRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiHomepageBannerNew GetById(long itemId)
        {
            return XsiContext.Context.XsiHomepageBannerNew.Find(itemId);
        }
        public List<XsiHomepageBannerNew> Select()
        {
            return XsiContext.Context.XsiHomepageBannerNew.ToList();
        }
        public List<XsiHomepageBannerNew> Select(XsiHomepageBannerNew entity)
        {
            var predicate = PredicateBuilder.True<XsiHomepageBannerNew>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Cmstitle.IsDefault())
                predicate = predicate.And(p => p.Cmstitle.Contains(entity.Cmstitle));
            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.SortOrder.IsDefault())
                predicate = predicate.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.SubTitle.IsDefault())
                predicate = predicate.And(p => p.SubTitle.Contains(entity.SubTitle));
            if (!entity.HomeBanner.IsDefault())
                predicate = predicate.And(p => p.HomeBanner.Equals(entity.HomeBanner));
            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.IsFeatured.IsDefault())
                predicate = predicate.And(p => p.IsFeatured.Equals(entity.IsFeatured));
            if (!entity.Color.IsDefault())
                predicate = predicate.And(p => p.Color.Equals(entity.Color));
            return XsiContext.Context.XsiHomepageBannerNew.AsExpandable().Where(predicate).ToList();
        }
        public XsiHomepageBannerNew Add(XsiHomepageBannerNew entity)
        {
            XsiHomepageBannerNew HomepageBannerNew = new XsiHomepageBannerNew();
            if (!entity.Cmstitle.IsDefault())
                HomepageBannerNew.Cmstitle = entity.Cmstitle.Trim();
            if (!entity.Title.IsDefault())
                HomepageBannerNew.Title = entity.Title.Trim();
            if (!entity.SubTitle.IsDefault())
                HomepageBannerNew.SubTitle = entity.SubTitle.Trim();
            HomepageBannerNew.HomeBanner = entity.HomeBanner;
            if (!entity.Url.IsDefault())
                HomepageBannerNew.Url = entity.Url.Trim();
            #region Ar
            if (!entity.CmstitleAr.IsDefault())
                HomepageBannerNew.CmstitleAr = entity.CmstitleAr.Trim();
            if (!entity.TitleAr.IsDefault())
                HomepageBannerNew.TitleAr = entity.TitleAr.Trim();
            if (!entity.SubTitleAr.IsDefault())
                HomepageBannerNew.SubTitleAr = entity.SubTitleAr.Trim();
            if (!entity.HomeBannerAr.IsDefault())
                HomepageBannerNew.HomeBannerAr = entity.HomeBannerAr;
            if (!entity.Urlar.IsDefault())
                HomepageBannerNew.Urlar = entity.Urlar.Trim();
            if (!entity.IsActiveAr.IsDefault())
                HomepageBannerNew.IsActiveAr = entity.IsActiveAr;
            if (!entity.ColorAr.IsDefault())
                HomepageBannerNew.ColorAr = entity.ColorAr;
            #endregion Ar

            HomepageBannerNew.IsActive = entity.IsActive;
            HomepageBannerNew.IsFeatured = entity.IsFeatured;
            HomepageBannerNew.SortOrder = entity.SortOrder;
            HomepageBannerNew.Color = entity.Color;
            HomepageBannerNew.CreatedOn = entity.CreatedOn;
            HomepageBannerNew.CreatedBy = entity.CreatedBy;
            HomepageBannerNew.ModifiedOn = entity.ModifiedOn;
            HomepageBannerNew.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiHomepageBannerNew.Add(HomepageBannerNew);
            SubmitChanges();
            return HomepageBannerNew;

        }
        public void Update(XsiHomepageBannerNew entity)
        {
            XsiHomepageBannerNew HomepageBannerNew = XsiContext.Context.XsiHomepageBannerNew.Find(entity.ItemId);
            XsiContext.Context.Entry(HomepageBannerNew).State = EntityState.Modified;
            
            if (!entity.Cmstitle.IsDefault())
                HomepageBannerNew.Cmstitle = entity.Cmstitle.Trim();
            if (!entity.Title.IsDefault())
                HomepageBannerNew.Title = entity.Title.Trim();
            if (!entity.SubTitle.IsDefault())
                HomepageBannerNew.SubTitle = entity.SubTitle.Trim();
            if (!entity.HomeBanner.IsDefault())
                HomepageBannerNew.HomeBanner = entity.HomeBanner;
            if (!entity.Url.IsDefault())
                HomepageBannerNew.Url = entity.Url.Trim();
            if (!entity.IsActive.IsDefault())
                HomepageBannerNew.IsActive = entity.IsActive;
            if (!entity.IsFeatured.IsDefault())
                HomepageBannerNew.IsFeatured = entity.IsFeatured;
            if (!entity.Color.IsDefault())
                HomepageBannerNew.Color = entity.Color;
            if (!entity.SortOrder.IsDefault())
                HomepageBannerNew.SortOrder = entity.SortOrder;
            if (!entity.ModifiedOn.IsDefault())
                HomepageBannerNew.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                HomepageBannerNew.ModifiedBy = entity.ModifiedBy;

            #region Ar
            if (!entity.CmstitleAr.IsDefault())
                HomepageBannerNew.CmstitleAr = entity.CmstitleAr.Trim();
            if (!entity.TitleAr.IsDefault())
                HomepageBannerNew.TitleAr = entity.TitleAr.Trim();
            if (!entity.SubTitleAr.IsDefault())
                HomepageBannerNew.SubTitleAr = entity.SubTitleAr.Trim();
            if (!entity.HomeBannerAr.IsDefault())
                HomepageBannerNew.HomeBannerAr = entity.HomeBannerAr;
            if (!entity.Urlar.IsDefault())
                HomepageBannerNew.Urlar = entity.Urlar.Trim();
            if (!entity.IsActiveAr.IsDefault())
                HomepageBannerNew.IsActiveAr = entity.IsActiveAr; 
            if (!entity.ColorAr.IsDefault())
                HomepageBannerNew.ColorAr = entity.ColorAr;
            #endregion Ar
        }
        public void Delete(XsiHomepageBannerNew entity)
        {
            foreach (XsiHomepageBannerNew HomepageBannerNew in Select(entity))
            {
                XsiHomepageBannerNew aHomepageBannerNew = XsiContext.Context.XsiHomepageBannerNew.Find(HomepageBannerNew.ItemId);
                XsiContext.Context.XsiHomepageBannerNew.Remove(aHomepageBannerNew);
            }
        }
        public void Delete(long itemId)
        {
            XsiHomepageBannerNew aHomepageBannerNew = XsiContext.Context.XsiHomepageBannerNew.Find(itemId);
            XsiContext.Context.XsiHomepageBannerNew.Remove(aHomepageBannerNew);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
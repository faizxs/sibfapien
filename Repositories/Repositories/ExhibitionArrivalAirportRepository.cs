﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionArrivalAirportRepository : IExhibitionArrivalAirportRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionArrivalAirportRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionArrivalAirportRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionArrivalAirportRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionArrivalAirport GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionArrivalAirport.Find(itemId);
        }
        public List<XsiExhibitionArrivalAirport> Select()
        {
            return XsiContext.Context.XsiExhibitionArrivalAirport.ToList();
        }
        public List<XsiExhibitionArrivalAirport> Select(XsiExhibitionArrivalAirport entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionArrivalAirport>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiExhibitionArrivalAirport.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionArrivalAirport> SelectOtherLanguageCategory(XsiExhibitionArrivalAirport entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionArrivalAirport>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            return XsiContext.Context.XsiExhibitionArrivalAirport.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionArrivalAirport> SelectCurrentLanguageCategory(XsiExhibitionArrivalAirport entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionArrivalAirport>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            return XsiContext.Context.XsiExhibitionArrivalAirport.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionArrivalAirport> SelectComplete(XsiExhibitionArrivalAirport entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionArrivalAirport>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
            
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionArrivalAirport.AsExpandable().Where(predicate).ToList();
            //return XsiContext.Context.XsiExhibitionArrivalAirport.Include(p => p.XsiArrivalTerminal).AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionArrivalAirport> SelectOr(XsiExhibitionArrivalAirport entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionArrivalAirport>();
            var Outer = PredicateBuilder.True<XsiExhibitionArrivalAirport>();

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.TitleAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.TitleAr.Contains(searchKeys.str1) || p.TitleAr.Contains(searchKeys.str2) || p.TitleAr.Contains(searchKeys.str3) || p.TitleAr.Contains(searchKeys.str4) || p.TitleAr.Contains(searchKeys.str5)
                    || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                    || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                    || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsActiveAr.IsDefault())
                Outer = Outer.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionArrivalAirport.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiExhibitionArrivalAirport Add(XsiExhibitionArrivalAirport entity)
        {
            XsiExhibitionArrivalAirport ExhibitionArrivalAirport = new XsiExhibitionArrivalAirport();
            if (!entity.Title.IsDefault())
                ExhibitionArrivalAirport.Title = entity.Title.Trim();
            ExhibitionArrivalAirport.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                ExhibitionArrivalAirport.TitleAr = entity.TitleAr.Trim();
            ExhibitionArrivalAirport.IsActiveAr = entity.IsActiveAr;

            ExhibitionArrivalAirport.CreatedOn = entity.CreatedOn;
            ExhibitionArrivalAirport.CreatedBy = entity.CreatedBy;
            ExhibitionArrivalAirport.ModifiedOn = entity.ModifiedOn;
            ExhibitionArrivalAirport.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionArrivalAirport.Add(ExhibitionArrivalAirport);
            SubmitChanges();
            return ExhibitionArrivalAirport;

        }
        public void Update(XsiExhibitionArrivalAirport entity)
        {
            XsiExhibitionArrivalAirport ExhibitionArrivalAirport = XsiContext.Context.XsiExhibitionArrivalAirport.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionArrivalAirport).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionArrivalAirport.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                ExhibitionArrivalAirport.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                ExhibitionArrivalAirport.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                ExhibitionArrivalAirport.IsActiveAr = entity.IsActiveAr;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionArrivalAirport.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                ExhibitionArrivalAirport.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionArrivalAirport entity)
        {
            foreach (XsiExhibitionArrivalAirport ExhibitionArrivalAirport in Select(entity))
            {
                XsiExhibitionArrivalAirport aExhibitionArrivalAirport = XsiContext.Context.XsiExhibitionArrivalAirport.Find(ExhibitionArrivalAirport.ItemId);
                XsiContext.Context.XsiExhibitionArrivalAirport.Remove(aExhibitionArrivalAirport);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionArrivalAirport aExhibitionArrivalAirport = XsiContext.Context.XsiExhibitionArrivalAirport.Find(itemId);
            XsiContext.Context.XsiExhibitionArrivalAirport.Remove(aExhibitionArrivalAirport);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
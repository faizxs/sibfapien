﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionStaffGuestBookingDetailRepository : IExhibitionStaffGuestBookingDetailsRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionStaffGuestBookingDetailRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionStaffGuestBookingDetailRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionStaffGuestBookingDetailRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionStaffGuestBookingDetails GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionStaffGuestBookingDetails.Find(itemId);
        }
        public List<XsiExhibitionStaffGuestBookingDetails> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionStaffGuestBookingDetails> Select()
        {
            return XsiContext.Context.XsiExhibitionStaffGuestBookingDetails.ToList();
        }

        public List<XsiExhibitionStaffGuestBookingDetails> Select(XsiExhibitionStaffGuestBookingDetails entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionStaffGuestBookingDetails>();

            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.StaffGuestId.IsDefault())
                predicate = predicate.And(p => p.StaffGuestId == entity.StaffGuestId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            #region Flight Booking Details
            if (!entity.GuestCategory.IsDefault())
                predicate = predicate.And(p => p.GuestCategory.Equals(entity.GuestCategory));

            if (!entity.ContactDetails.IsDefault())
                predicate = predicate.And(p => p.ContactDetails.Contains(entity.ContactDetails));

            if (!entity.GuestPhoneNumber.IsDefault())
                predicate = predicate.And(p => p.GuestPhoneNumber.Contains(entity.GuestPhoneNumber));

            if (!entity.StaffInChargePhoneNumber.IsDefault())
                predicate = predicate.And(p => p.StaffInChargePhoneNumber.Contains(entity.StaffInChargePhoneNumber));

            if (!entity.ServiceType.IsDefault())
                predicate = predicate.And(p => p.ServiceType.Equals(entity.ServiceType));

            if (!entity.MarhabaReferenceNumber.IsDefault())
                predicate = predicate.And(p => p.MarhabaReferenceNumber.Equals(entity.MarhabaReferenceNumber));

            if (!entity.StartDate.IsDefault())
                predicate = predicate.And(p => p.StartDate == entity.StartDate);

            if (!entity.EndDate.IsDefault())
                predicate = predicate.And(p => p.EndDate == entity.EndDate);

            if (!entity.ArrivalDateTime.IsDefault())
                predicate = predicate.And(p => p.ArrivalDateTime == entity.ArrivalDateTime);

            if (!entity.ReturnDateTime.IsDefault())
                predicate = predicate.And(p => p.ReturnDateTime == entity.ReturnDateTime);

            if (!entity.PreferredTime.IsDefault())
                predicate = predicate.And(p => p.PreferredTime == entity.PreferredTime);

            if (!entity.PreferredAirline.IsDefault())
                predicate = predicate.And(p => p.PreferredAirline.Contains(entity.PreferredAirline));

            if (!entity.ClassSeat.IsDefault())
                predicate = predicate.And(p => p.ClassSeat.Equals(entity.ClassSeat));

            if (!entity.ArrivalDateTime.IsDefault())
                predicate = predicate.And(p => p.ArrivalDateTime.Contains(entity.ArrivalDateTime));

            if (!entity.ReturnDateTime.IsDefault())
                predicate = predicate.And(p => p.ReturnDateTime.Contains(entity.ReturnDateTime));

            if (!entity.ArrivalDate.IsDefault())
                predicate = predicate.And(p => p.ArrivalDate == entity.ArrivalDate);

            if (!entity.ReturnDate.IsDefault())
                predicate = predicate.And(p => p.ReturnDate == entity.ReturnDate);

            if (!entity.Terminal.IsDefault())
                predicate = predicate.And(p => p.Terminal.Equals(entity.Terminal));

            if (!entity.ClassSeatAllotted.IsDefault())
                predicate = predicate.And(p => p.ClassSeatAllotted.Equals(entity.ClassSeatAllotted));

            if (!entity.Passport.IsDefault())
                predicate = predicate.And(p => p.Passport.Contains(entity.Passport));

            if (!entity.PassportCopyTwo.IsDefault())
                predicate = predicate.And(p => p.PassportCopyTwo.Contains(entity.PassportCopyTwo));

            if (!entity.IsPassportModified.IsDefault())
                predicate = predicate.And(p => p.IsPassportModified.Contains(entity.IsPassportModified));

            if (!entity.FlightBookingType.IsDefault())
                predicate = predicate.And(p => p.FlightBookingType.Equals(entity.FlightBookingType));

            if (!entity.FlightDetails.IsDefault())
                predicate = predicate.And(p => p.FlightDetails.Equals(entity.FlightDetails));

            if (!entity.FlightNumber.IsDefault())
                predicate = predicate.And(p => p.FlightNumber.Contains(entity.FlightNumber));

            if (!entity.FlightTicket.IsDefault())
                predicate = predicate.And(p => p.FlightTicket.Contains(entity.FlightTicket));

            if (!entity.DestinationFrom.IsDefault())
                predicate = predicate.And(p => p.DestinationFrom == entity.DestinationFrom);

            if (!entity.DestinationTo.IsDefault())
                predicate = predicate.And(p => p.DestinationTo == entity.DestinationTo);

            if (!entity.OriginAirport.IsDefault())
                predicate = predicate.And(p => p.OriginAirport == entity.OriginAirport);

            if (!entity.DestinationAirport.IsDefault())
                predicate = predicate.And(p => p.DestinationAirport == entity.DestinationAirport);

            if (!entity.IsDirectOrTransitFlight.IsDefault())
                predicate = predicate.And(p => p.IsDirectOrTransitFlight.Equals(entity.IsDirectOrTransitFlight));

            if (!entity.ReturnClassSeat.IsDefault())
                predicate = predicate.And(p => p.ReturnClassSeat.Equals(entity.ReturnClassSeat));

            if (!entity.ReturnTerminal.IsDefault())
                predicate = predicate.And(p => p.ReturnTerminal.Equals(entity.ReturnTerminal));

            if (!entity.ReturnClassSeatAllotted.IsDefault())
                predicate = predicate.And(p => p.ReturnClassSeatAllotted.Equals(entity.ReturnClassSeatAllotted));

            if (!entity.ReturnFlightBookingType.IsDefault())
                predicate = predicate.And(p => p.ReturnFlightBookingType.Equals(entity.ReturnFlightBookingType));

            if (!entity.ReturnFlightStatus.IsDefault())
                predicate = predicate.And(p => p.ReturnFlightStatus.Equals(entity.ReturnFlightStatus));

            if (!entity.ReturnFlight.IsDefault())
                predicate = predicate.And(p => p.ReturnFlight.Contains(entity.ReturnFlight));

            if (!entity.ReturnFlightTicket.IsDefault())
                predicate = predicate.And(p => p.ReturnFlightTicket.Contains(entity.ReturnFlightTicket));

            if (!entity.ReturnFlightNumber.IsDefault())
                predicate = predicate.And(p => p.ReturnFlightNumber.Contains(entity.ReturnFlightNumber));

            if (!entity.ReturnFlightCheckInTime.IsDefault())
                predicate = predicate.And(p => p.ReturnFlightCheckInTime.Contains(entity.ReturnFlightCheckInTime));

            if (!entity.ReturnFlightCheckOutTime.IsDefault())
                predicate = predicate.And(p => p.ReturnFlightCheckOutTime.Contains(entity.ReturnFlightCheckOutTime));

            if (!entity.SuggestFlightAttachment.IsDefault())
                predicate = predicate.And(p => p.SuggestFlightAttachment.Contains(entity.SuggestFlightAttachment));
            #endregion

            #region Hotel
            if (!entity.HotelBookingStartDate.IsDefault())
                predicate = predicate.And(p => p.HotelBookingStartDate == entity.HotelBookingStartDate);

            if (!entity.HotelBookingEndDate.IsDefault())
                predicate = predicate.And(p => p.HotelBookingEndDate == entity.HotelBookingEndDate);

            if (!entity.HotelStartDate.IsDefault())
                predicate = predicate.And(p => p.HotelStartDate == entity.HotelStartDate);

            if (!entity.HotelEndDate.IsDefault())
                predicate = predicate.And(p => p.HotelEndDate == entity.HotelEndDate);

            if (!entity.RoomType.IsDefault())
                predicate = predicate.And(p => p.RoomType.Equals(entity.RoomType));

            if (!entity.SpecialRequest.IsDefault())
                predicate = predicate.And(p => p.SpecialRequest.Contains(entity.SpecialRequest));

            if (!entity.HotelStatus.IsDefault())
                predicate = predicate.And(p => p.HotelStatus.Equals(entity.HotelStatus));

            if (!entity.ReservationReferenceNumber.IsDefault())
                predicate = predicate.And(p => p.ReservationReferenceNumber.Contains(entity.ReservationReferenceNumber));

            if (!entity.HotelName.IsDefault())
                predicate = predicate.And(p => p.HotelName.Contains(entity.HotelName));

            if (!entity.HotelAddress.IsDefault())
                predicate = predicate.And(p => p.HotelAddress.Contains(entity.HotelAddress));

            if (!entity.HotelPhoneNumber.IsDefault())
                predicate = predicate.And(p => p.HotelPhoneNumber.Contains(entity.HotelPhoneNumber));

            if (!entity.HotelStartDate.IsDefault())
                predicate = predicate.And(p => p.HotelStartDate == entity.HotelStartDate);

            if (!entity.HotelEndDate.IsDefault())
                predicate = predicate.And(p => p.HotelEndDate == entity.HotelEndDate);

            if (!entity.CheckInTime.IsDefault())
                predicate = predicate.And(p => p.CheckInTime.Contains(entity.CheckInTime));

            if (!entity.CheckOutTime.IsDefault())
                predicate = predicate.And(p => p.CheckOutTime.Contains(entity.CheckOutTime));

            if (!entity.RoomTypeAllotted.IsDefault())
                predicate = predicate.And(p => p.RoomTypeAllotted.Equals(entity.RoomTypeAllotted));

            if (!entity.Notes.IsDefault())
                predicate = predicate.And(p => p.Notes.Contains(entity.Notes));

            if (!entity.ReservationCopy.IsDefault())
                predicate = predicate.And(p => p.ReservationCopy.Contains(entity.ReservationCopy));
            #endregion

            #region Transport
            if (!entity.TransportationType.IsDefault())
                predicate = predicate.And(p => p.TransportationType.Equals(entity.TransportationType));

            if (!entity.TransportStartDate.IsDefault())
                predicate = predicate.And(p => p.TransportStartDate == entity.TransportStartDate);

            if (!entity.TransportEndDate.IsDefault())
                predicate = predicate.And(p => p.TransportEndDate == entity.TransportEndDate);

            if (!entity.TransportContactPerson.IsDefault())
                predicate = predicate.And(p => p.TransportContactPerson.Contains(entity.TransportContactPerson));

            if (!entity.TransportContactNumber.IsDefault())
                predicate = predicate.And(p => p.TransportContactNumber.Contains(entity.TransportContactNumber));

            if (!entity.TransportationStatus.IsDefault())
                predicate = predicate.And(p => p.TransportationStatus.Equals(entity.TransportationStatus));

            if (!entity.TransportationTypeAllotted.IsDefault())
                predicate = predicate.And(p => p.TransportationTypeAllotted.Equals(entity.TransportationTypeAllotted));

            if (!entity.IsIndividualOrGroup.IsDefault())
                predicate = predicate.And(p => p.IsIndividualOrGroup.Equals(entity.IsIndividualOrGroup));

            if (!entity.NoofPeople.IsDefault())
                predicate = predicate.And(p => p.NoofPeople.Equals(entity.NoofPeople));

            if (!entity.PickupLocation.IsDefault())
                predicate = predicate.And(p => p.PickupLocation.Contains(entity.PickupLocation));

            #endregion

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiExhibitionStaffGuestBookingDetails.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionStaffGuestBookingDetails Add(XsiExhibitionStaffGuestBookingDetails entity)
        {
            XsiExhibitionStaffGuestBookingDetails detailsentity = new XsiExhibitionStaffGuestBookingDetails();

            detailsentity.StaffGuestId = entity.StaffGuestId;
            detailsentity.ExhibitionId = entity.ExhibitionId;

            #region Booking Details
            detailsentity.GuestCategory = entity.GuestCategory;
            detailsentity.ContactDetails = entity.ContactDetails;
            detailsentity.GuestPhoneNumber = entity.GuestPhoneNumber;
            detailsentity.StaffInChargePhoneNumber = entity.StaffInChargePhoneNumber;
            detailsentity.ServiceType = entity.ServiceType;
            detailsentity.MarhabaReferenceNumber = entity.MarhabaReferenceNumber;
            detailsentity.StartDate = entity.StartDate;
            detailsentity.EndDate = entity.EndDate;
            detailsentity.ArrivalDateTime = entity.ArrivalDateTime;
            detailsentity.ReturnDateTime = entity.ReturnDateTime;
            detailsentity.PreferredTime = entity.PreferredTime;
            detailsentity.PreferredAirline = entity.PreferredAirline;
            detailsentity.FlightBookingType = entity.FlightBookingType;
            detailsentity.ArrivalDateTime = entity.ArrivalDateTime;
            detailsentity.ReturnDateTime = entity.ReturnDateTime;
            detailsentity.ClassSeat = entity.ClassSeat;
            detailsentity.ArrivalDate = entity.ArrivalDate;
            detailsentity.ReturnDate = entity.ReturnDate;
            detailsentity.Terminal = entity.Terminal;
            detailsentity.ClassSeatAllotted = entity.ClassSeatAllotted;
            detailsentity.Passport = entity.Passport;
            detailsentity.PassportCopyTwo = entity.PassportCopyTwo;
            detailsentity.IsPassportModified = entity.IsPassportModified;

            if (!string.IsNullOrEmpty(entity.SuggestFlightAttachment))
                detailsentity.SuggestFlightAttachment = entity.SuggestFlightAttachment;

            detailsentity.FlightStatus = entity.FlightStatus;
            detailsentity.FlightDetails = entity.FlightDetails;
            detailsentity.FlightNumber = entity.FlightNumber;
            detailsentity.FlightTicket = entity.FlightTicket;
            detailsentity.DestinationFrom = entity.DestinationFrom;
            detailsentity.DestinationTo = entity.DestinationTo;
            detailsentity.OriginAirport = entity.OriginAirport;
            detailsentity.DestinationAirport = entity.DestinationAirport;
            detailsentity.IsDirectOrTransitFlight = entity.IsDirectOrTransitFlight;

            detailsentity.ReturnClassSeat = entity.ReturnClassSeat;
            detailsentity.ReturnTerminal = entity.ReturnTerminal;
            detailsentity.ReturnClassSeatAllotted = entity.ReturnClassSeatAllotted;
            detailsentity.ReturnFlightBookingType = entity.ReturnFlightBookingType;
            detailsentity.ReturnFlightStatus = entity.ReturnFlightStatus;
            detailsentity.ReturnFlight = entity.ReturnFlight;
            detailsentity.ReturnFlightTicket = entity.ReturnFlightTicket;
            detailsentity.ReturnFlightNumber = entity.ReturnFlightNumber;
            detailsentity.ReturnFlightCheckInTime = entity.ReturnFlightCheckInTime;
            detailsentity.ReturnFlightCheckOutTime = entity.ReturnFlightCheckOutTime;
            #endregion

            #region Hotel Details
            detailsentity.HotelBookingStartDate = entity.HotelBookingStartDate;
            detailsentity.HotelBookingEndDate = entity.HotelBookingEndDate;
            detailsentity.HotelStartDate = entity.HotelStartDate;
            detailsentity.HotelEndDate = entity.HotelEndDate;
            detailsentity.RoomType = entity.RoomType;
            detailsentity.SpecialRequest = entity.SpecialRequest;
            detailsentity.HotelStatus = entity.HotelStatus;
            detailsentity.ReservationReferenceNumber = entity.ReservationReferenceNumber;
            detailsentity.HotelName = entity.HotelName;
            detailsentity.HotelAddress = entity.HotelAddress;
            detailsentity.HotelPhoneNumber = entity.HotelPhoneNumber;
            detailsentity.HotelStartDate = entity.HotelStartDate;
            detailsentity.HotelEndDate = entity.HotelEndDate;
            detailsentity.CheckInTime = entity.CheckInTime;
            detailsentity.CheckOutTime = entity.CheckOutTime;
            detailsentity.RoomTypeAllotted = entity.RoomTypeAllotted;
            detailsentity.ReservationCopy = entity.ReservationCopy;
            #endregion

            #region Transport Details
            detailsentity.TransportationType = entity.TransportationType;
            detailsentity.TransportStartDate = entity.TransportStartDate;
            detailsentity.TransportEndDate = entity.TransportEndDate;
            detailsentity.TransportContactPerson = entity.TransportContactPerson;
            detailsentity.TransportContactNumber = entity.TransportContactNumber;
            detailsentity.TransportationStatus = entity.TransportationStatus;
            detailsentity.TransportationTypeAllotted = entity.TransportationTypeAllotted;
            detailsentity.IsIndividualOrGroup = entity.IsIndividualOrGroup;
            detailsentity.NoofPeople = entity.NoofPeople;
            detailsentity.PickupLocation = entity.PickupLocation;
            #endregion

            detailsentity.IsActive = entity.IsActive;
            detailsentity.CreatedOn = entity.CreatedOn;
            detailsentity.CreatedBy = entity.CreatedBy;
            detailsentity.ModifiedOn = entity.ModifiedOn;
            detailsentity.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionStaffGuestBookingDetails.Add(detailsentity);
            SubmitChanges();
            return detailsentity;

        }
        public void Update(XsiExhibitionStaffGuestBookingDetails entity)
        {
            XsiExhibitionStaffGuestBookingDetails detailsentity = XsiContext.Context.XsiExhibitionStaffGuestBookingDetails.Find(entity.ItemId);

            XsiContext.Context.Entry(detailsentity).State = EntityState.Modified;

            if (!entity.StaffGuestId.IsDefault())
                detailsentity.StaffGuestId = entity.StaffGuestId;

            if (!entity.ExhibitionId.IsDefault())
                detailsentity.ExhibitionId = entity.ExhibitionId;

            #region Booking Details
            if (!entity.GuestCategory.IsDefault())
                detailsentity.GuestCategory = entity.GuestCategory;

            if (!entity.ContactDetails.IsDefault())
                detailsentity.ContactDetails = entity.ContactDetails;

            if (!entity.GuestPhoneNumber.IsDefault())
                detailsentity.GuestPhoneNumber = entity.GuestPhoneNumber.Trim();

            if (!entity.StaffInChargePhoneNumber.IsDefault())
                detailsentity.StaffInChargePhoneNumber = entity.StaffInChargePhoneNumber.Trim();

            if (!entity.ServiceType.IsDefault())
                detailsentity.ServiceType = entity.ServiceType;

            if (!entity.MarhabaReferenceNumber.IsDefault())
                detailsentity.MarhabaReferenceNumber = entity.MarhabaReferenceNumber;

            if (!entity.StartDate.IsDefault())
                detailsentity.StartDate = entity.StartDate;

            if (!entity.EndDate.IsDefault())
                detailsentity.EndDate = entity.EndDate;

            if (!entity.ArrivalDateTime.IsDefault())
                detailsentity.ArrivalDateTime = entity.ArrivalDateTime;

            if (!entity.ReturnDateTime.IsDefault())
                detailsentity.ReturnDateTime = entity.ReturnDateTime;

            if (!entity.PreferredTime.IsDefault())
                detailsentity.PreferredTime = entity.PreferredTime;

            if (!entity.PreferredAirline.IsDefault())
                detailsentity.PreferredAirline = entity.PreferredAirline.Trim();

            if (!entity.ClassSeat.IsDefault())
                detailsentity.ClassSeat = entity.ClassSeat.Trim();

            if (!entity.ArrivalDateTime.IsDefault())
                detailsentity.ArrivalDateTime = entity.ArrivalDateTime.Trim();

            if (!entity.ReturnDateTime.IsDefault())
                detailsentity.ReturnDateTime = entity.ReturnDateTime.Trim();

            if (!entity.ArrivalDate.IsDefault())
                detailsentity.ArrivalDate = entity.ArrivalDate;

            if (!entity.ReturnDate.IsDefault())
                detailsentity.ReturnDate = entity.ReturnDate;

            if (!entity.Terminal.IsDefault())
                detailsentity.Terminal = entity.Terminal;

            if (!entity.ClassSeatAllotted.IsDefault())
                detailsentity.ClassSeatAllotted = entity.ClassSeatAllotted;

            if (!entity.Passport.IsDefault())
                detailsentity.Passport = entity.Passport.Trim();

            if (!entity.PassportCopyTwo.IsDefault())
                detailsentity.PassportCopyTwo = entity.PassportCopyTwo.Trim();

            if (!entity.IsPassportModified.IsDefault())
                detailsentity.IsPassportModified = entity.IsPassportModified.Trim();

            if (!entity.OriginAirport.IsDefault())
                detailsentity.OriginAirport = entity.OriginAirport;

            if (!entity.DestinationAirport.IsDefault())
                detailsentity.DestinationAirport = entity.DestinationAirport;

            if (!entity.IsDirectOrTransitFlight.IsDefault())
                detailsentity.IsDirectOrTransitFlight = entity.IsDirectOrTransitFlight;

            if (!entity.SuggestFlightAttachment.IsDefault())
                detailsentity.SuggestFlightAttachment = entity.SuggestFlightAttachment;

            if (!entity.FlightTicket.IsDefault())
                detailsentity.FlightTicket = entity.FlightTicket;

            if (!entity.FlightStatus.IsDefault())
                detailsentity.FlightStatus = entity.FlightStatus.Trim();

            if (!entity.FlightDetails.IsDefault())
                detailsentity.FlightDetails = entity.FlightDetails.Trim();

            if (!entity.FlightNumber.IsDefault())
                detailsentity.FlightNumber = entity.FlightNumber.Trim();

            if (!entity.DestinationFrom.IsDefault())
                detailsentity.DestinationFrom = entity.DestinationFrom;

            if (!entity.DestinationTo.IsDefault())
                detailsentity.DestinationTo = entity.DestinationTo;

            if (!entity.ReturnClassSeat.IsDefault())
                detailsentity.ReturnClassSeat = entity.ReturnClassSeat;

            if (!entity.ReturnTerminal.IsDefault())
                detailsentity.ReturnTerminal = entity.ReturnTerminal;

            if (!entity.ReturnClassSeatAllotted.IsDefault())
                detailsentity.ReturnClassSeatAllotted = entity.ReturnClassSeatAllotted;

            if (!entity.ReturnFlightBookingType.IsDefault())
                detailsentity.ReturnFlightBookingType = entity.ReturnFlightBookingType;

            if (!entity.ReturnFlightStatus.IsDefault())
                detailsentity.ReturnFlightStatus = entity.ReturnFlightStatus;

            if (!entity.ReturnFlight.IsDefault())
                detailsentity.ReturnFlight = entity.ReturnFlight;

            if (!entity.ReturnFlightTicket.IsDefault())
                detailsentity.ReturnFlightTicket = entity.ReturnFlightTicket;

            if (!entity.ReturnFlightNumber.IsDefault())
                detailsentity.ReturnFlightNumber = entity.ReturnFlightNumber;

            if (!entity.ReturnFlightCheckInTime.IsDefault())
                detailsentity.ReturnFlightCheckInTime = entity.ReturnFlightCheckInTime;

            if (!entity.ReturnFlightCheckOutTime.IsDefault())
                detailsentity.ReturnFlightCheckOutTime = entity.ReturnFlightCheckOutTime;

            #endregion

            #region Hotel Details
            if (!entity.HotelBookingStartDate.IsDefault())
                detailsentity.HotelBookingStartDate = entity.HotelBookingStartDate;

            if (!entity.HotelBookingEndDate.IsDefault())
                detailsentity.HotelBookingEndDate = entity.HotelBookingEndDate;

            if (!entity.HotelStartDate.IsDefault())
                detailsentity.HotelStartDate = entity.HotelStartDate;

            if (!entity.HotelEndDate.IsDefault())
                detailsentity.HotelEndDate = entity.HotelEndDate;

            if (!entity.RoomType.IsDefault())
                detailsentity.RoomType = entity.RoomType;

            if (!entity.SpecialRequest.IsDefault())
                detailsentity.SpecialRequest = entity.SpecialRequest;

            if (!entity.HotelStatus.IsDefault())
                detailsentity.HotelStatus = entity.HotelStatus;

            if (!entity.ReservationReferenceNumber.IsDefault())
                detailsentity.ReservationReferenceNumber = entity.ReservationReferenceNumber;

            if (!entity.HotelName.IsDefault())
                detailsentity.HotelName = entity.HotelName;

            if (!entity.HotelAddress.IsDefault())
                detailsentity.HotelAddress = entity.HotelAddress.Trim();

            if (!entity.HotelPhoneNumber.IsDefault())
                detailsentity.HotelPhoneNumber = entity.HotelPhoneNumber.Trim();

            if (!entity.HotelStartDate.IsDefault())
                detailsentity.HotelStartDate = entity.HotelStartDate;

            if (!entity.HotelEndDate.IsDefault())
                detailsentity.HotelEndDate = entity.HotelEndDate;

            if (!entity.CheckInTime.IsDefault())
                detailsentity.CheckInTime = entity.CheckInTime;
            if (!entity.CheckOutTime.IsDefault())
                detailsentity.CheckOutTime = entity.CheckOutTime;

            if (!entity.RoomTypeAllotted.IsDefault())
                detailsentity.RoomTypeAllotted = entity.RoomTypeAllotted;

            if (!entity.Notes.IsDefault())
                detailsentity.Notes = entity.Notes;

            if (!entity.ReservationCopy.IsDefault())
                detailsentity.ReservationCopy = entity.ReservationCopy;
            #endregion

            #region Transport
            if (!entity.TransportationType.IsDefault())
                detailsentity.TransportationType = entity.TransportationType.Trim();

            if (!entity.TransportStartDate.IsDefault())
                detailsentity.TransportStartDate = entity.TransportStartDate;

            if (!entity.TransportEndDate.IsDefault())
                detailsentity.TransportEndDate = entity.TransportEndDate;

            if (!entity.TransportContactPerson.IsDefault())
                detailsentity.TransportContactPerson = entity.TransportContactPerson.Trim();

            if (!entity.TransportContactNumber.IsDefault())
                detailsentity.TransportContactNumber = entity.TransportContactNumber.Trim();

            if (!entity.TransportationStatus.IsDefault())
                detailsentity.TransportationStatus = entity.TransportationStatus.Trim();

            if (!entity.NoofPeople.IsDefault())
                detailsentity.NoofPeople = entity.NoofPeople;

            if (!entity.IsIndividualOrGroup.IsDefault())
                detailsentity.IsIndividualOrGroup = entity.IsIndividualOrGroup;

            if (!entity.TransportationTypeAllotted.IsDefault())
                detailsentity.TransportationTypeAllotted = entity.TransportationTypeAllotted.Trim();

            if (!entity.PickupLocation.IsDefault())
                detailsentity.PickupLocation = entity.PickupLocation.Trim();
            #endregion

            if (!entity.IsActive.IsDefault())
                detailsentity.IsActive = entity.IsActive;

            if (!entity.ModifiedBy.IsDefault())
                detailsentity.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                detailsentity.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiExhibitionStaffGuestBookingDetails entity)
        {
            foreach (XsiExhibitionStaffGuestBookingDetails detailsentity in Select(entity))
            {
                XsiExhibitionStaffGuestBookingDetails aExhibitionStaffGuestParticipating = XsiContext.Context.XsiExhibitionStaffGuestBookingDetails.Find(detailsentity.ItemId);
                XsiContext.Context.XsiExhibitionStaffGuestBookingDetails.Remove(aExhibitionStaffGuestParticipating);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionStaffGuestBookingDetails detailsentity = XsiContext.Context.XsiExhibitionStaffGuestBookingDetails.Where(p => p.ItemId == itemId).OrderByDescending(o => o.ItemId).FirstOrDefault();
            XsiContext.Context.XsiExhibitionStaffGuestBookingDetails.Remove(detailsentity);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}

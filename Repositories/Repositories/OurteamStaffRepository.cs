﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class OurteamStaffRepository : IOurteamStaffRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public OurteamStaffRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public OurteamStaffRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public OurteamStaffRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiOurteamStaff GetById(long itemId)
        {
            return XsiContext.Context.XsiOurteamStaff.Find(itemId);
        }
        
        public List<XsiOurteamStaff> Select()
        {
            return XsiContext.Context.XsiOurteamStaff.ToList();
        }
        public List<XsiOurteamStaff> Select(XsiOurteamStaff entity)
        {
            var predicate = PredicateBuilder.True<XsiOurteamStaff>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
                        
            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.FirstName.IsDefault())
                predicate = predicate.And(p => p.FirstName.Contains(entity.FirstName));
            if (!entity.MiddleName.IsDefault())
                predicate = predicate.And(p => p.MiddleName.Contains(entity.MiddleName));
            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));
            if (!entity.Position.IsDefault())
                predicate = predicate.And(p => p.Position.Contains(entity.Position));
            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Contains(entity.ImageName));
            if (!entity.PhoneNo.IsDefault())
                predicate = predicate.And(p => p.PhoneNo.Contains(entity.PhoneNo));
            if (!entity.MobileNo.IsDefault())
                predicate = predicate.And(p => p.MobileNo.Contains(entity.MobileNo));
            if (!entity.Fax.IsDefault())
                predicate = predicate.And(p => p.Fax.Contains(entity.Fax));
            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Contains(entity.Email));
            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiOurteamStaff.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiOurteamStaff> SelectComeplete(XsiOurteamStaff entity)
        {
            var predicate = PredicateBuilder.True<XsiOurteamStaff>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.FirstName.IsDefault())
                predicate = predicate.And(p => p.FirstName.Contains(entity.FirstName));
            if (!entity.MiddleName.IsDefault())
                predicate = predicate.And(p => p.MiddleName.Contains(entity.MiddleName));
            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));
            if (!entity.Position.IsDefault())
                predicate = predicate.And(p => p.Position.Contains(entity.Position));
            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Contains(entity.ImageName));
            if (!entity.PhoneNo.IsDefault())
                predicate = predicate.And(p => p.PhoneNo.Contains(entity.PhoneNo));
            if (!entity.MobileNo.IsDefault())
                predicate = predicate.And(p => p.MobileNo.Contains(entity.MobileNo));
            if (!entity.Fax.IsDefault())
                predicate = predicate.And(p => p.Fax.Contains(entity.Fax));
            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Contains(entity.Email));
            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));


            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);
            if (!entity.Category.IsDefault())
            {
                 if (!entity.Category.IsActive.IsDefault())
                    predicate = predicate.And(p => p.Category.IsActive.Equals(entity.Category.IsActive));
            }
            return XsiContext.Context.XsiOurteamStaff.Include(p => p.Category).AsExpandable().Where(predicate).ToList();
        }

        public XsiOurteamStaff Add(XsiOurteamStaff entity)
        {
            XsiOurteamStaff OurteamStaff = new XsiOurteamStaff();
         
            OurteamStaff.CategoryId = entity.CategoryId;
            if (!entity.FirstName.IsDefault())
                OurteamStaff.FirstName = entity.FirstName.Trim();
            if (!entity.MiddleName.IsDefault())
                OurteamStaff.MiddleName = entity.MiddleName.Trim();
            if (!entity.LastName.IsDefault())
                OurteamStaff.LastName = entity.LastName.Trim();
            if (!entity.Position.IsDefault())
                OurteamStaff.Position = entity.Position.Trim();
            OurteamStaff.ImageName = entity.ImageName;
            if (!entity.PhoneNo.IsDefault())
                OurteamStaff.PhoneNo = entity.PhoneNo.Trim();
            if (!entity.MobileNo.IsDefault())
                OurteamStaff.MobileNo = entity.MobileNo.Trim();
            if (!entity.Fax.IsDefault())
                OurteamStaff.Fax = entity.Fax.Trim();
            if (!entity.Email.IsDefault())
                OurteamStaff.Email = entity.Email.Trim();
            if (!entity.Description.IsDefault())
                OurteamStaff.Description = entity.Description.Trim();
            OurteamStaff.IsActive = entity.IsActive;

            #region Ar
            if (!entity.FirstNameAr.IsDefault())
                OurteamStaff.FirstNameAr = entity.FirstNameAr.Trim();
            if (!entity.MiddleNameAr.IsDefault())
                OurteamStaff.MiddleNameAr = entity.MiddleNameAr.Trim();
            if (!entity.LastNameAr.IsDefault())
                OurteamStaff.LastNameAr = entity.LastNameAr.Trim();
            if (!entity.PositionAr.IsDefault())
                OurteamStaff.PositionAr = entity.PositionAr.Trim();
            OurteamStaff.ImageNameAr = entity.ImageNameAr;
            if (!entity.PhoneNoAr.IsDefault())
                OurteamStaff.PhoneNoAr = entity.PhoneNoAr.Trim();
            if (!entity.MobileNoAr.IsDefault())
                OurteamStaff.MobileNoAr = entity.MobileNoAr.Trim();
            if (!entity.FaxAr.IsDefault())
                OurteamStaff.FaxAr = entity.FaxAr.Trim();
            if (!entity.EmailAr.IsDefault())
                OurteamStaff.EmailAr = entity.EmailAr.Trim();
            if (!entity.DescriptionAr.IsDefault())
                OurteamStaff.DescriptionAr = entity.DescriptionAr.Trim();
            OurteamStaff.IsActiveAr = entity.IsActiveAr;
            #endregion Ar
            OurteamStaff.CreatedOn = entity.CreatedOn;
            OurteamStaff.CreatedBy = entity.CreatedBy;
            OurteamStaff.ModifiedOn = entity.ModifiedOn;
            OurteamStaff.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiOurteamStaff.Add(OurteamStaff);
            SubmitChanges();
            return OurteamStaff;

        }
        public void Update(XsiOurteamStaff entity)
        {
            XsiOurteamStaff OurteamStaff = XsiContext.Context.XsiOurteamStaff.Find(entity.ItemId);
            XsiContext.Context.Entry(OurteamStaff).State = EntityState.Modified;

            if (!entity.FirstName.IsDefault())
                OurteamStaff.FirstName = entity.FirstName.Trim();

            if (!entity.MiddleName.IsDefault())
                OurteamStaff.MiddleName = entity.MiddleName.Trim();
            if (!entity.LastName.IsDefault())
                OurteamStaff.LastName = entity.LastName.Trim();
            if (!entity.Position.IsDefault())
                OurteamStaff.Position = entity.Position.Trim();
            if (!entity.ImageName.IsDefault())
                OurteamStaff.ImageName = entity.ImageName;
            if (!entity.PhoneNo.IsDefault())
                OurteamStaff.PhoneNo = entity.PhoneNo.Trim();
            if (!entity.MobileNo.IsDefault())
                OurteamStaff.MobileNo = entity.MobileNo.Trim();
            if (!entity.Fax.IsDefault())
                OurteamStaff.Fax = entity.Fax.Trim();
            if (!entity.Email.IsDefault())
                OurteamStaff.Email = entity.Email.Trim();
            if (!entity.Description.IsDefault())
                OurteamStaff.Description = entity.Description.Trim();

            if (!entity.IsActive.IsDefault())
                OurteamStaff.IsActive = entity.IsActive;

            #region Ar
            if (!entity.FirstNameAr.IsDefault())
                OurteamStaff.FirstNameAr = entity.FirstNameAr.Trim();

            if (!entity.MiddleNameAr.IsDefault())
                OurteamStaff.MiddleNameAr = entity.MiddleNameAr.Trim();
            if (!entity.LastNameAr.IsDefault())
                OurteamStaff.LastNameAr = entity.LastNameAr.Trim();
            if (!entity.PositionAr.IsDefault())
                OurteamStaff.PositionAr = entity.PositionAr.Trim();
            if (!entity.ImageNameAr.IsDefault())
                OurteamStaff.ImageNameAr = entity.ImageNameAr;
            if (!entity.PhoneNoAr.IsDefault())
                OurteamStaff.PhoneNoAr = entity.PhoneNoAr.Trim();
            if (!entity.MobileNoAr.IsDefault())
                OurteamStaff.MobileNoAr = entity.MobileNoAr.Trim();
            if (!entity.FaxAr.IsDefault())
                OurteamStaff.FaxAr = entity.FaxAr.Trim();
            if (!entity.EmailAr.IsDefault())
                OurteamStaff.EmailAr = entity.EmailAr.Trim();
            if (!entity.DescriptionAr.IsDefault())
                OurteamStaff.DescriptionAr = entity.DescriptionAr.Trim();

            if (!entity.IsActiveAr.IsDefault())
                OurteamStaff.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                OurteamStaff.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                OurteamStaff.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiOurteamStaff entity)
        {
            foreach (XsiOurteamStaff OurteamStaff in Select(entity))
            {
                XsiOurteamStaff aOurteamStaff = XsiContext.Context.XsiOurteamStaff.Find(OurteamStaff.ItemId);
                XsiContext.Context.XsiOurteamStaff.Remove(aOurteamStaff);
            }
        }
        public void Delete(long itemId)
        {
            XsiOurteamStaff aOurteamStaff = XsiContext.Context.XsiOurteamStaff.Find(itemId);
            XsiContext.Context.XsiOurteamStaff.Remove(aOurteamStaff);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
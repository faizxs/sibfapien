﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;
namespace Xsi.Repositories
{
    public class ExhibitionCountryRepository : IExhibitionCountryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionCountryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionCountryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionCountryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionCountry GetById(long itemId)
        {
            if (CachingRepository.CachedExhibitionCountry == null)
                CachingRepository.CachedExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.ToList();
            return CachingRepository.CachedExhibitionCountry.AsQueryable().Where(i => i.CountryId == itemId).FirstOrDefault();
            // return XsiContext.Context.XsiExhibitionCountry.Find(itemId);
        }
        public List<XsiExhibitionCountry> Select()
        {
            if (CachingRepository.CachedExhibitionCountry == null)
                CachingRepository.CachedExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.ToList();
            return CachingRepository.CachedExhibitionCountry;

            // return XsiContext.Context.XsiExhibitionCountry.ToList();
        }
        public List<XsiExhibitionCountry> SelectOr(XsiExhibitionCountry entity)
        {
            if (CachingRepository.CachedExhibitionCountry == null)
                CachingRepository.CachedExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.ToList();

            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionCountry>();
            var Outer = PredicateBuilder.True<XsiExhibitionCountry>();

            if (!entity.CountryNameAr.IsDefault())
            {
                string strSearchText = entity.CountryNameAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.CountryNameAr.Contains(searchKeys.str1) || p.CountryNameAr.Contains(searchKeys.str2) || p.CountryNameAr.Contains(searchKeys.str3) || p.CountryNameAr.Contains(searchKeys.str4) || p.CountryNameAr.Contains(searchKeys.str5)
                    || p.CountryNameAr.Contains(searchKeys.str6) || p.CountryNameAr.Contains(searchKeys.str7) || p.CountryNameAr.Contains(searchKeys.str8) || p.CountryNameAr.Contains(searchKeys.str9) || p.CountryNameAr.Contains(searchKeys.str10)
                    || p.CountryNameAr.Contains(searchKeys.str11) || p.CountryNameAr.Contains(searchKeys.str12) || p.CountryNameAr.Contains(searchKeys.str13)
                    || p.CountryNameAr.Contains(searchKeys.str14) || p.CountryNameAr.Contains(searchKeys.str15) || p.CountryNameAr.Contains(searchKeys.str16) || p.CountryNameAr.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.CountryName.IsDefault())
                {
                    string strSearchText = entity.CountryName.ToLower();
                    Inner = Inner.Or(p => p.CountryName.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsVisaRequired.IsDefault())
                Outer = Outer.And(p => p.IsVisaRequired != null && p.IsVisaRequired.Equals(entity.IsVisaRequired));

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive != null && p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return CachingRepository.CachedExhibitionCountry.AsQueryable().Where(Outer.Expand()).ToList();

            // return XsiContext.Context.XsiExhibitionCountry.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiExhibitionCountry> Select(XsiExhibitionCountry entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionCountry>();
            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.IsVisaRequired.IsDefault())
                predicate = predicate.And(p => p.IsVisaRequired.Equals(entity.IsVisaRequired));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CountryName.IsDefault())
                predicate = predicate.And(p => p.CountryName.Contains(entity.CountryName));

            if (!entity.CountryNameAr.IsDefault())
                predicate = predicate.And(p => p.CountryNameAr.Contains(entity.CountryNameAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            if (CachingRepository.CachedExhibitionCountry == null)
                CachingRepository.CachedExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.ToList();

            return CachingRepository.CachedExhibitionCountry.AsQueryable().Where(predicate).ToList();
            //  return XsiContext.Context.XsiExhibitionCountry.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionCountry> SelectOtherLanguageCategory(XsiExhibitionCountry entity)
        {
            if (CachingRepository.CachedExhibitionCountry == null)
                CachingRepository.CachedExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.ToList();

            var predicate = PredicateBuilder.True<XsiExhibitionCountry>();
            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            // var CountryId = CachingRepository.CachedExhibitionCountry.AsQueryable().Where(predicate).Single().CountryId;
            //   predicate = PredicateBuilder.True<XsiExhibitionCountry>();

            return CachingRepository.CachedExhibitionCountry.AsQueryable().Where(predicate).ToList();
            // return XsiContext.Context.XsiExhibitionCountry.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionCountry> SelectCurrentLanguageCategory(XsiExhibitionCountry entity)
        {
            if (CachingRepository.CachedExhibitionCountry == null)
                CachingRepository.CachedExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.ToList();

            var predicate = PredicateBuilder.True<XsiExhibitionCountry>();
            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            //  var GroupId = XsiContext.Context.XsiExhibitionCountry.AsExpandable().Where(predicate).Single().CountryId;
            //predicate = PredicateBuilder.True<XsiExhibitionCountry>();

            return CachingRepository.CachedExhibitionCountry.AsQueryable().Where(predicate).ToList();
            // return XsiContext.Context.XsiExhibitionCountry.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionCountry> SelectComplete(XsiExhibitionCountry entity)
        {
            if (CachingRepository.CachedExhibitionCountry == null)
                CachingRepository.CachedExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.ToList();

            var predicate = PredicateBuilder.True<XsiExhibitionCountry>();
            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.IsVisaRequired.IsDefault())
                predicate = predicate.And(p => p.IsVisaRequired.Equals(entity.IsVisaRequired));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CountryName.IsDefault())
                predicate = predicate.And(p => p.CountryName.Contains(entity.CountryName));

            if (!entity.CountryNameAr.IsDefault())
                predicate = predicate.And(p => p.CountryNameAr.Contains(entity.CountryNameAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return CachingRepository.CachedExhibitionCountry.AsQueryable().Include(p => p.XsiExhibitionCity).AsExpandable().Where(predicate).ToList();
            // return XsiContext.Context.XsiExhibitionCountry.Include(p => p.XsiExhibitionCities).AsExpandable().Where(predicate).ToList();
        }
        public XsiExhibitionCountry Add(XsiExhibitionCountry entity)
        {
            if (CachingRepository.CachedExhibitionCountry == null)
                CachingRepository.CachedExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.ToList();

            XsiExhibitionCountry ExhibitionCountry = new XsiExhibitionCountry();
            ExhibitionCountry.IsVisaRequired = entity.IsVisaRequired;
            ExhibitionCountry.IsActive = entity.IsActive;
            if (!entity.CountryName.IsDefault())
                ExhibitionCountry.CountryName = entity.CountryName.Trim();
            if (!entity.CountryNameAr.IsDefault())
                ExhibitionCountry.CountryNameAr = entity.CountryNameAr.Trim();
            ExhibitionCountry.CreatedOn = entity.CreatedOn;
            ExhibitionCountry.CreatedBy = entity.CreatedBy;
            ExhibitionCountry.ModifiedOn = entity.ModifiedOn;
            ExhibitionCountry.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionCountry.Add(ExhibitionCountry);
            SubmitChanges();

            CachingRepository.CachedExhibitionCountry.Add(ExhibitionCountry);

            return ExhibitionCountry;

        }
        public void Update(XsiExhibitionCountry entity)
        {
            if (CachingRepository.CachedExhibitionCountry == null)
                CachingRepository.CachedExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.ToList();

            XsiExhibitionCountry ExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.Find(entity.CountryId);
            XsiContext.Context.Entry(ExhibitionCountry).State = EntityState.Modified;

            if (!entity.IsVisaRequired.IsDefault())
                ExhibitionCountry.IsVisaRequired = entity.IsVisaRequired;

            if (!entity.IsActive.IsDefault())
                ExhibitionCountry.IsActive = entity.IsActive;

            if (!entity.CountryName.IsDefault())
                ExhibitionCountry.CountryName = entity.CountryName.Trim();

            if (!entity.CountryNameAr.IsDefault())
                ExhibitionCountry.CountryNameAr = entity.CountryNameAr.Trim();

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionCountry.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionCountry.ModifiedBy = entity.ModifiedBy;
            CachingRepository.CachedExhibitionCountry.Remove(ExhibitionCountry);
            CachingRepository.CachedExhibitionCountry.Add(ExhibitionCountry);
        }
        public void Delete(XsiExhibitionCountry entity)
        {
            if (CachingRepository.CachedExhibitionCountry == null)
                CachingRepository.CachedExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.ToList();

            foreach (XsiExhibitionCountry ExhibitionCountry in Select(entity))
            {
                XsiExhibitionCountry aExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.Find(ExhibitionCountry.CountryId);
                XsiContext.Context.XsiExhibitionCountry.Remove(aExhibitionCountry);

                CachingRepository.CachedExhibitionCountry.Remove(entity);
            }
        }
        public void Delete(long itemId)
        {
            if (CachingRepository.CachedExhibitionCountry == null)
                CachingRepository.CachedExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.ToList();

            XsiExhibitionCountry aExhibitionCountry = XsiContext.Context.XsiExhibitionCountry.Find(itemId);
            XsiContext.Context.XsiExhibitionCountry.Remove(aExhibitionCountry);

            CachingRepository.CachedExhibitionCountry.Remove(aExhibitionCountry);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
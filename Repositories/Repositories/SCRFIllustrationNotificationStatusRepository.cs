﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFIllustrationNotificationRepository : ISCRFIllustrationNotificationRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFIllustrationNotificationRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFIllustrationNotificationRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFIllustrationNotificationRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfillustrationNotification GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfillustrationNotification.Find(itemId);
        }
        public List<XsiScrfillustrationNotification> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiScrfillustrationNotification> Select()
        {
            return XsiContext.Context.XsiScrfillustrationNotification.ToList();
        }
        public List<XsiScrfillustrationNotification> Select(XsiScrfillustrationNotification entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfillustrationNotification>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IllustrationId.IsDefault())
                predicate = predicate.And(p => p.IllustrationId == entity.IllustrationId);

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));


            return XsiContext.Context.XsiScrfillustrationNotification.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfillustrationNotification Add(XsiScrfillustrationNotification entity)
        {
            XsiScrfillustrationNotification SCRFIllustrationNotification = new XsiScrfillustrationNotification();
            SCRFIllustrationNotification.IllustrationId = entity.IllustrationId;
            if (!entity.Description.IsDefault())
            SCRFIllustrationNotification.Description = entity.Description.Trim();

            XsiContext.Context.XsiScrfillustrationNotification.Add(SCRFIllustrationNotification);
            SubmitChanges();
            return SCRFIllustrationNotification;

        }
        public void Update(XsiScrfillustrationNotification entity)
        {
            XsiScrfillustrationNotification SCRFIllustrationNotification = XsiContext.Context.XsiScrfillustrationNotification.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFIllustrationNotification).State = EntityState.Modified;

            if (!entity.IllustrationId.IsDefault())
                SCRFIllustrationNotification.IllustrationId = entity.IllustrationId;

            if (!entity.Description.IsDefault())
                SCRFIllustrationNotification.Description = entity.Description.Trim();
        }
        public void Delete(XsiScrfillustrationNotification entity)
        {
            foreach (XsiScrfillustrationNotification SCRFIllustrationNotification in Select(entity))
            {
                XsiScrfillustrationNotification aSCRFIllustrationNotification = XsiContext.Context.XsiScrfillustrationNotification.Find(SCRFIllustrationNotification.ItemId);
                XsiContext.Context.XsiScrfillustrationNotification.Remove(aSCRFIllustrationNotification);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfillustrationNotification aSCRFIllustrationNotification = XsiContext.Context.XsiScrfillustrationNotification.Find(itemId);
            XsiContext.Context.XsiScrfillustrationNotification.Remove(aSCRFIllustrationNotification);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class NewsletterBannerRepository : INewsletterBannerRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public NewsletterBannerRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public NewsletterBannerRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public NewsletterBannerRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiNewsletterBanner GetById(long itemId)
        {
            return XsiContext.Context.XsiNewsletterBanner.Find(itemId);
        }

        public List<XsiNewsletterBanner> Select()
        {
            return XsiContext.Context.XsiNewsletterBanner.ToList();
        }
        public List<XsiNewsletterBanner> Select(XsiNewsletterBanner entity)
        {
            var predicate = PredicateBuilder.True<XsiNewsletterBanner>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Contains(entity.ImageName));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            return XsiContext.Context.XsiNewsletterBanner.AsExpandable().Where(predicate).ToList();
        }

        public XsiNewsletterBanner Add(XsiNewsletterBanner entity)
        {
            XsiNewsletterBanner NewsletterBanner = new XsiNewsletterBanner();
            if (!entity.Title.IsDefault())
                NewsletterBanner.Title = entity.Title.Trim();
            if (!entity.Url.IsDefault())
                NewsletterBanner.Url = entity.Url.Trim();
            NewsletterBanner.ImageName = entity.ImageName;
            NewsletterBanner.IsActive = entity.IsActive;
            NewsletterBanner.CreatedOn = entity.CreatedOn;
            NewsletterBanner.CreatedBy = entity.CreatedBy;
            NewsletterBanner.ModifiedOn = entity.ModifiedOn;
            NewsletterBanner.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiNewsletterBanner.Add(NewsletterBanner);
            SubmitChanges();
            return NewsletterBanner;

        }
        public void Update(XsiNewsletterBanner entity)
        {
            XsiNewsletterBanner NewsletterBanner = XsiContext.Context.XsiNewsletterBanner.Find(entity.ItemId);
            XsiContext.Context.Entry(NewsletterBanner).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                NewsletterBanner.Title = entity.Title.Trim();
            if (!entity.Url.IsDefault())
                NewsletterBanner.Url = entity.Url.Trim();
            if (!entity.ImageName.IsDefault())
                NewsletterBanner.ImageName = entity.ImageName;
            if (!entity.IsActive.IsDefault())
                NewsletterBanner.IsActive = entity.IsActive;
            if (!entity.ModifiedOn.IsDefault())
                NewsletterBanner.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                NewsletterBanner.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiNewsletterBanner entity)
        {
            foreach (XsiNewsletterBanner NewsletterBanner in Select(entity))
            {
                XsiNewsletterBanner aNewsletterBanner = XsiContext.Context.XsiNewsletterBanner.Find(NewsletterBanner.ItemId);
                XsiContext.Context.XsiNewsletterBanner.Remove(aNewsletterBanner);
            }
        }
        public void Delete(long itemId)
        {
            XsiNewsletterBanner aNewsletterBanner = XsiContext.Context.XsiNewsletterBanner.Find(itemId);
            XsiContext.Context.XsiNewsletterBanner.Remove(aNewsletterBanner);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
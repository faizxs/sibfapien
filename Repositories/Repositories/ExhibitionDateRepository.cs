﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionDateRepository : IExhibitionDateRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionDateRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionDateRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionDateRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionDate GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionDate.Find(itemId);
        }
        public List<XsiExhibitionDate> GetByGroupId(long groupId)
        {
            return XsiContext.Context.XsiExhibitionDate.Where(p => p.GroupId == groupId).ToList();
        }

        public List<XsiExhibitionDate> Select()
        {
            return XsiContext.Context.XsiExhibitionDate.ToList();
        }
        public List<XsiExhibitionDate> Select(XsiExhibitionDate entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionDate>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.GroupId.IsDefault())
                predicate = predicate.And(p => p.GroupId == entity.GroupId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Equals(entity.Title));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Equals(entity.Description));

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy.Equals(entity.CreatedBy));

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy.Equals(entity.ModifiedBy));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn.Equals(entity.CreatedOn));

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn.Equals(entity.ModifiedOn));

            return XsiContext.Context.XsiExhibitionDate.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionDate Add(XsiExhibitionDate entity)
        {
            XsiExhibitionDate ExhibitionDate = new XsiExhibitionDate();
            ExhibitionDate.LanguageId = entity.LanguageId;
            ExhibitionDate.GroupId = entity.GroupId;
            if (!entity.Title.IsDefault())
                ExhibitionDate.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
            ExhibitionDate.Description = entity.Description.Trim();
            ExhibitionDate.WebsiteId = entity.WebsiteId;
            ExhibitionDate.IsActive = entity.IsActive;
            ExhibitionDate.CreatedBy = entity.CreatedBy;
            ExhibitionDate.CreatedOn = entity.CreatedOn;
            ExhibitionDate.ModifiedOn = entity.ModifiedOn;
            ExhibitionDate.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiExhibitionDate.Add(ExhibitionDate);
            SubmitChanges();
            return ExhibitionDate;

        }
        public void Update(XsiExhibitionDate entity)
        {
            XsiExhibitionDate ExhibitionDate = XsiContext.Context.XsiExhibitionDate.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionDate).State = EntityState.Modified;

            if (!entity.LanguageId.IsDefault())
                ExhibitionDate.LanguageId = entity.LanguageId;

            if (!entity.GroupId.IsDefault())
                ExhibitionDate.GroupId = entity.GroupId;

            if (!entity.Title.IsDefault())
                ExhibitionDate.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                ExhibitionDate.Description = entity.Description.Trim();

            if (!entity.WebsiteId.IsDefault())
                ExhibitionDate.WebsiteId = entity.WebsiteId;

            if (!entity.IsActive.IsDefault())
                ExhibitionDate.IsActive = entity.IsActive;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionDate.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionDate.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiExhibitionDate entity)
        {
            foreach (XsiExhibitionDate ExhibitionDate in Select(entity))
            {
                XsiExhibitionDate aExhibitionDate = XsiContext.Context.XsiExhibitionDate.Find(ExhibitionDate.ItemId);
                XsiContext.Context.XsiExhibitionDate.Remove(aExhibitionDate);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionDate aExhibitionDate = XsiContext.Context.XsiExhibitionDate.Find(itemId);
            XsiContext.Context.XsiExhibitionDate.Remove(aExhibitionDate);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
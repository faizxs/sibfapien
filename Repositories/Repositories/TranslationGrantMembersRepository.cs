﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class TranslationGrantMemberRepository : ITranslationGrantMemberRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public TranslationGrantMemberRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public TranslationGrantMemberRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public TranslationGrantMemberRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionTranslationGrantMembers GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionTranslationGrantMembers.Find(itemId);
        }

        public List<XsiExhibitionTranslationGrantMembers> Select()
        {
            return XsiContext.Context.XsiExhibitionTranslationGrantMembers.ToList();
        }
        public List<XsiExhibitionTranslationGrantMembers> Select(XsiExhibitionTranslationGrantMembers entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionTranslationGrantMembers>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);

            if (!entity.TranslationGrantId.IsDefault())
                predicate = predicate.And(p => p.TranslationGrantId == entity.TranslationGrantId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.TranslationType.IsDefault())
                predicate = predicate.And(p => p.TranslationType.Equals(entity.TranslationType));

            if (!entity.SellerName.IsDefault())
                predicate = predicate.And(p => p.SellerName.Contains(entity.SellerName));

            if (!entity.SellerJobTitle.IsDefault())
                predicate = predicate.And(p => p.SellerJobTitle.Contains(entity.SellerJobTitle));

            if (!entity.SellerCompanyName.IsDefault())
                predicate = predicate.And(p => p.SellerCompanyName.Contains(entity.SellerCompanyName));

            if (!entity.SellerAddress.IsDefault())
                predicate = predicate.And(p => p.SellerAddress.Contains(entity.SellerAddress));

            if (!entity.SellerTelephone.IsDefault())
                predicate = predicate.And(p => p.SellerTelephone.Contains(entity.SellerTelephone));

            if (!entity.SellerFax.IsDefault())
                predicate = predicate.And(p => p.SellerFax.Contains(entity.SellerFax));

            if (!entity.SellerEmail.IsDefault())
                predicate = predicate.And(p => p.SellerEmail.Equals(entity.SellerEmail));

            if (!entity.BuyerName.IsDefault())
                predicate = predicate.And(p => p.BuyerName.Contains(entity.BuyerName));

            if (!entity.BuyerJobTitle.IsDefault())
                predicate = predicate.And(p => p.BuyerJobTitle.Contains(entity.BuyerJobTitle));

            if (!entity.BuyerCompanyName.IsDefault())
                predicate = predicate.And(p => p.BuyerCompanyName.Contains(entity.BuyerCompanyName));

            if (!entity.BuyerAddress.IsDefault())
                predicate = predicate.And(p => p.BuyerAddress.Contains(entity.BuyerAddress));

            if (!entity.BuyerTelephone.IsDefault())
                predicate = predicate.And(p => p.BuyerTelephone.Contains(entity.BuyerTelephone));

            if (!entity.BuyerFax.IsDefault())
                predicate = predicate.And(p => p.BuyerFax.Contains(entity.BuyerFax));

            if (!entity.BuyerEmail.IsDefault())
                predicate = predicate.And(p => p.BuyerEmail.Contains(entity.BuyerEmail));

            if (!entity.BookTitleOriginal.IsDefault())
                predicate = predicate.And(p => p.BookTitleOriginal.Contains(entity.BookTitleOriginal));

            if (!entity.BookTitleEnglish.IsDefault())
                predicate = predicate.And(p => p.BookTitleEnglish.Contains(entity.BookTitleEnglish));

            if (!entity.BookTitleArabic.IsDefault())
                predicate = predicate.And(p => p.BookTitleArabic.Contains(entity.BookTitleArabic));

            if (!entity.Author.IsDefault())
                predicate = predicate.And(p => p.Author.Contains(entity.Author));

            if (!entity.Isbn.IsDefault())
                predicate = predicate.And(p => p.Isbn.Contains(entity.Isbn));

            if (!entity.Genre.IsDefault())
                predicate = predicate.And(p => p.Genre.Contains(entity.Genre));

            if (!entity.OriginalLanguageId.IsDefault())
                predicate = predicate.And(p => p.OriginalLanguageId == entity.OriginalLanguageId);

            if (!entity.ProposedLanguageId.IsDefault())
                predicate = predicate.And(p => p.ProposedLanguageId == entity.ProposedLanguageId);

            if (!entity.NoOfPagesOriginal.IsDefault())
                predicate = predicate.And(p => p.NoOfPagesOriginal.Contains(entity.NoOfPagesOriginal));

            if (!entity.ApplicationForm.IsDefault())
                predicate = predicate.And(p => p.ApplicationForm.Contains(entity.ApplicationForm));

            if (!entity.GrantLetter.IsDefault())
                predicate = predicate.And(p => p.GrantLetter.Contains(entity.GrantLetter));

            if (!entity.NoOfWords.IsDefault())
                predicate = predicate.And(p => p.NoOfWords.Contains(entity.NoOfWords));

            if (!entity.Contract.IsDefault())
                predicate = predicate.And(p => p.Contract.Contains(entity.Contract));

            if (!entity.TransferFormByAdmin.IsDefault())
                predicate = predicate.And(p => p.TransferFormByAdmin.Contains(entity.TransferFormByAdmin));

            if (!entity.SignedContract.IsDefault())
                predicate = predicate.And(p => p.SignedContract.Contains(entity.SignedContract));

            if (!entity.BankName.IsDefault())
                predicate = predicate.And(p => p.BankName.Contains(entity.BankName));

            if (!entity.BranchNameAddress.IsDefault())
                predicate = predicate.And(p => p.BranchNameAddress.Contains(entity.BranchNameAddress));

            if (!entity.AccNo.IsDefault())
                predicate = predicate.And(p => p.AccNo.Contains(entity.AccNo));

            if (!entity.Ibanno.IsDefault())
                predicate = predicate.And(p => p.Ibanno.Contains(entity.Ibanno));

            if (!entity.SwiftCode.IsDefault())
                predicate = predicate.And(p => p.SwiftCode.Contains(entity.SwiftCode));

            if (!entity.OriginalSoftCopy.IsDefault())
                predicate = predicate.And(p => p.OriginalSoftCopy.Contains(entity.OriginalSoftCopy));

            if (!entity.FirstPayment.IsDefault())
                predicate = predicate.And(p => p.FirstPayment.Contains(entity.FirstPayment));

            if (!entity.FirstPaymentDate.IsDefault())
                predicate = predicate.And(p => p.FirstPaymentDate == entity.FirstPaymentDate);

            if (!entity.Draft1.IsDefault())
                predicate = predicate.And(p => p.Draft1.Contains(entity.Draft1));

            if (!entity.SecondPayment.IsDefault())
                predicate = predicate.And(p => p.SecondPayment.Contains(entity.SecondPayment));

            if (!entity.SecondPaymentDate.IsDefault())
                predicate = predicate.And(p => p.SecondPaymentDate == entity.SecondPaymentDate);

            if (!entity.FinalPayment.IsDefault())
                predicate = predicate.And(p => p.FinalPayment.Contains(entity.FinalPayment));

            if (!entity.FinalPaymentDate.IsDefault())
                predicate = predicate.And(p => p.FinalPaymentDate == entity.FinalPaymentDate);

            if (!entity.OriginalBookStatus.IsDefault())
                predicate = predicate.And(p => p.OriginalBookStatus.Equals(entity.OriginalBookStatus));

            if (!entity.IsIbanorBoth.IsDefault())
                predicate = predicate.And(p => p.IsIbanorBoth.Equals(entity.IsIbanorBoth));

            if (!entity.GenreId.IsDefault())
                predicate = predicate.And(p => p.GenreId == entity.GenreId);

            if (!entity.AwardGrantValue.IsDefault())
                predicate = predicate.And(p => p.AwardGrantValue.Contains(entity.AwardGrantValue));

            if (!entity.IsHardCopy.IsDefault())
                predicate = predicate.And(p => p.IsHardCopy.Equals(entity.IsHardCopy));

            if (!entity.DateofShipment.IsDefault())
                predicate = predicate.And(p => p.DateofShipment == entity.DateofShipment);

            if (!entity.BuyerorSellerRights.IsDefault())
                predicate = predicate.And(p => p.BuyerorSellerRights.Contains(entity.BuyerorSellerRights));

            if (!entity.FinalContract.IsDefault())
                predicate = predicate.And(p => p.FinalContract.Contains(entity.FinalContract));

            if (!entity.TransferForm.IsDefault())
                predicate = predicate.And(p => p.TransferForm.Contains(entity.TransferForm));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiExhibitionTranslationGrantMembers.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionTranslationGrantMembers Add(XsiExhibitionTranslationGrantMembers entity)
        {
            XsiExhibitionTranslationGrantMembers TranslationGrantMember = new XsiExhibitionTranslationGrantMembers();
            TranslationGrantMember.MemberId = entity.MemberId;
            TranslationGrantMember.TranslationGrantId = entity.TranslationGrantId;
            TranslationGrantMember.IsActive = entity.IsActive;
            TranslationGrantMember.Status = entity.Status;
            TranslationGrantMember.TranslationType = entity.TranslationType;
            if (!entity.SellerName.IsDefault())
                TranslationGrantMember.SellerName = entity.SellerName.Trim();
            if (!entity.SellerFirstName.IsDefault())
                TranslationGrantMember.SellerFirstName = entity.SellerFirstName.Trim();
            if (!entity.SellerMiddleName.IsDefault())
                TranslationGrantMember.SellerMiddleName = entity.SellerMiddleName.Trim();
            if (!entity.SellerLastName.IsDefault())
                TranslationGrantMember.SellerLastName = entity.SellerLastName.Trim();
            if (!entity.SellerJobTitle.IsDefault())
                TranslationGrantMember.SellerJobTitle = entity.SellerJobTitle.Trim();
            if (!entity.SellerCompanyName.IsDefault())
                TranslationGrantMember.SellerCompanyName = entity.SellerCompanyName.Trim();
            if (!entity.SellerAddress.IsDefault())
                TranslationGrantMember.SellerAddress = entity.SellerAddress.Trim();
            if (!entity.SellerTelephone.IsDefault())
                TranslationGrantMember.SellerTelephone = entity.SellerTelephone.Trim();
            if (!entity.SellerFax.IsDefault())
                TranslationGrantMember.SellerFax = entity.SellerFax.Trim();
            if (!entity.SellerEmail.IsDefault())
                TranslationGrantMember.SellerEmail = entity.SellerEmail.Trim();
            if (!entity.BuyerName.IsDefault())
                TranslationGrantMember.BuyerName = entity.BuyerName.Trim();
            if (!entity.BuyerFirstName.IsDefault())
                TranslationGrantMember.BuyerFirstName = entity.BuyerFirstName.Trim();
            if (!entity.BuyerMiddleName.IsDefault())
                TranslationGrantMember.BuyerMiddleName = entity.BuyerMiddleName.Trim();
            if (!entity.BuyerLastName.IsDefault())
                TranslationGrantMember.BuyerLastName = entity.BuyerLastName.Trim();
            if (!entity.BuyerJobTitle.IsDefault())
                TranslationGrantMember.BuyerJobTitle = entity.BuyerJobTitle.Trim();
            if (!entity.BuyerCompanyName.IsDefault())
                TranslationGrantMember.BuyerCompanyName = entity.BuyerCompanyName.Trim();
            if (!entity.BuyerAddress.IsDefault())
                TranslationGrantMember.BuyerAddress = entity.BuyerAddress.Trim();
            if (!entity.BuyerTelephone.IsDefault())
                TranslationGrantMember.BuyerTelephone = entity.BuyerTelephone.Trim();
            if (!entity.BuyerFax.IsDefault())
                TranslationGrantMember.BuyerFax = entity.BuyerFax.Trim();
            if (!entity.BuyerEmail.IsDefault())
                TranslationGrantMember.BuyerEmail = entity.BuyerEmail.Trim();
            if (!entity.BookTitleOriginal.IsDefault())
                TranslationGrantMember.BookTitleOriginal = entity.BookTitleOriginal.Trim();
            if (!entity.BookTitleEnglish.IsDefault())
                TranslationGrantMember.BookTitleEnglish = entity.BookTitleEnglish.Trim();
            if (!entity.BookTitleArabic.IsDefault())
                TranslationGrantMember.BookTitleArabic = entity.BookTitleArabic.Trim();
            if (!entity.Author.IsDefault())
                TranslationGrantMember.Author = entity.Author.Trim();
            if (!entity.Isbn.IsDefault())
                TranslationGrantMember.Isbn = entity.Isbn.Trim();
            if (!entity.Genre.IsDefault())
                TranslationGrantMember.Genre = entity.Genre.Trim();
            if (!entity.OriginalLanguageId.IsDefault())
                TranslationGrantMember.OriginalLanguageId = entity.OriginalLanguageId;
            TranslationGrantMember.ProposedLanguageId = entity.ProposedLanguageId;
            if (!entity.NoOfPagesOriginal.IsDefault())
                TranslationGrantMember.NoOfPagesOriginal = entity.NoOfPagesOriginal.Trim();
            if (!entity.ApplicationForm.IsDefault())
                TranslationGrantMember.ApplicationForm = entity.ApplicationForm.Trim();
            if (!entity.GrantLetter.IsDefault())
                TranslationGrantMember.GrantLetter = entity.GrantLetter.Trim();
            if (!entity.NoOfWords.IsDefault())
                TranslationGrantMember.NoOfWords = entity.NoOfWords.Trim();
            if (entity.Contract.IsDefault())
                TranslationGrantMember.Contract = entity.Contract;
            if (entity.TransferFormByAdmin.IsDefault())
                TranslationGrantMember.TransferFormByAdmin = entity.TransferFormByAdmin;
            TranslationGrantMember.SignedContract = entity.SignedContract;
            if (!entity.BankName.IsDefault())
                TranslationGrantMember.BankName = entity.BankName.Trim();
            if (!entity.BranchNameAddress.IsDefault())
                TranslationGrantMember.BranchNameAddress = entity.BranchNameAddress.Trim();
            if (!entity.AccNo.IsDefault())
                TranslationGrantMember.AccNo = entity.AccNo.Trim();
            if (!entity.Ibanno.IsDefault())
                TranslationGrantMember.Ibanno = entity.Ibanno.Trim();
            if (!entity.SwiftCode.IsDefault())
                TranslationGrantMember.SwiftCode = entity.SwiftCode.Trim();
            TranslationGrantMember.OriginalSoftCopy = entity.OriginalSoftCopy;
            if (!entity.FirstPayment.IsDefault())
                TranslationGrantMember.FirstPayment = entity.FirstPayment.Trim();
            TranslationGrantMember.FirstPaymentDate = entity.FirstPaymentDate;
            TranslationGrantMember.FirstPaymentReceipt = entity.FirstPaymentReceipt;
            TranslationGrantMember.Draft1 = entity.Draft1;
            if (!entity.SecondPayment.IsDefault())
                TranslationGrantMember.SecondPayment = entity.SecondPayment.Trim();
            TranslationGrantMember.SecondPaymentDate = entity.SecondPaymentDate;
            TranslationGrantMember.SecondPaymentReceipt = entity.SecondPaymentReceipt;
            if (!entity.FinalPayment.IsDefault())
                TranslationGrantMember.FinalPayment = entity.FinalPayment.Trim();
            TranslationGrantMember.FinalPaymentDate = entity.FinalPaymentDate;
            TranslationGrantMember.FinalPaymentReceipt = entity.FinalPaymentReceipt;

            TranslationGrantMember.OriginalBookStatus = entity.OriginalBookStatus;
            TranslationGrantMember.IsIbanorBoth = entity.IsIbanorBoth;
            TranslationGrantMember.GenreId = entity.GenreId;
            if (!entity.AwardGrantValue.IsDefault())
                TranslationGrantMember.AwardGrantValue = entity.AwardGrantValue.Trim();
            TranslationGrantMember.IsHardCopy = entity.IsHardCopy;
            TranslationGrantMember.DateofShipment = entity.DateofShipment;
            if (!entity.BuyerorSellerRights.IsDefault())
                TranslationGrantMember.BuyerorSellerRights = entity.BuyerorSellerRights.Trim();
            TranslationGrantMember.FinalContract = entity.FinalContract;
            TranslationGrantMember.TransferForm = entity.TransferForm;
            TranslationGrantMember.CreatedOn = entity.CreatedOn;
            TranslationGrantMember.CreatedBy = entity.CreatedBy;
            TranslationGrantMember.ModifiedOn = entity.ModifiedOn;
            TranslationGrantMember.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionTranslationGrantMembers.Add(TranslationGrantMember);
            SubmitChanges();
            return TranslationGrantMember;

        }
        public void Update(XsiExhibitionTranslationGrantMembers entity)
        {
            XsiExhibitionTranslationGrantMembers TranslationGrantMember = XsiContext.Context.XsiExhibitionTranslationGrantMembers.Find(entity.ItemId);
            XsiContext.Context.Entry(TranslationGrantMember).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                TranslationGrantMember.IsActive = entity.IsActive;

            if (!entity.Status.IsDefault())
                TranslationGrantMember.Status = entity.Status;

            if (!entity.TranslationType.IsDefault())
                TranslationGrantMember.TranslationType = entity.TranslationType;

            if (!entity.SellerName.IsDefault())
                TranslationGrantMember.SellerName = entity.SellerName.Trim();

            if (!entity.SellerFirstName.IsDefault())
                TranslationGrantMember.SellerFirstName = entity.SellerFirstName.Trim();

            if (!entity.SellerMiddleName.IsDefault())
                TranslationGrantMember.SellerMiddleName = entity.SellerMiddleName.Trim();

            if (!entity.SellerLastName.IsDefault())
                TranslationGrantMember.SellerLastName = entity.SellerLastName.Trim();

            if (!entity.SellerJobTitle.IsDefault())
                TranslationGrantMember.SellerJobTitle = entity.SellerJobTitle.Trim();

            if (!entity.SellerCompanyName.IsDefault())
                TranslationGrantMember.SellerCompanyName = entity.SellerCompanyName.Trim();

            if (!entity.SellerAddress.IsDefault())
                TranslationGrantMember.SellerAddress = entity.SellerAddress.Trim();

            if (!entity.SellerTelephone.IsDefault())
                TranslationGrantMember.SellerTelephone = entity.SellerTelephone.Trim();

            if (!entity.SellerFax.IsDefault())
                TranslationGrantMember.SellerFax = entity.SellerFax.Trim();

            if (!entity.SellerEmail.IsDefault())
                TranslationGrantMember.SellerEmail = entity.SellerEmail.Trim();

            if (!entity.BuyerName.IsDefault())
                TranslationGrantMember.BuyerName = entity.BuyerName.Trim();

            if (!entity.BuyerFirstName.IsDefault())
                TranslationGrantMember.BuyerFirstName = entity.BuyerFirstName.Trim();

            if (!entity.BuyerMiddleName.IsDefault())
                TranslationGrantMember.BuyerMiddleName = entity.BuyerMiddleName.Trim();

            if (!entity.BuyerLastName.IsDefault())
                TranslationGrantMember.BuyerLastName = entity.BuyerLastName.Trim();

            if (!entity.BuyerJobTitle.IsDefault())
                TranslationGrantMember.BuyerJobTitle = entity.BuyerJobTitle.Trim();

            if (!entity.BuyerCompanyName.IsDefault())
                TranslationGrantMember.BuyerCompanyName = entity.BuyerCompanyName.Trim();

            if (!entity.BuyerAddress.IsDefault())
                TranslationGrantMember.BuyerAddress = entity.BuyerAddress.Trim();

            if (!entity.BuyerTelephone.IsDefault())
                TranslationGrantMember.BuyerTelephone = entity.BuyerTelephone.Trim();

            if (!entity.BuyerFax.IsDefault())
                TranslationGrantMember.BuyerFax = entity.BuyerFax.Trim();

            if (!entity.BuyerEmail.IsDefault())
                TranslationGrantMember.BuyerEmail = entity.BuyerEmail.Trim();

            if (!entity.BookTitleOriginal.IsDefault())
                TranslationGrantMember.BookTitleOriginal = entity.BookTitleOriginal.Trim();

            if (!entity.BookTitleEnglish.IsDefault())
                TranslationGrantMember.BookTitleEnglish = entity.BookTitleEnglish.Trim();

            if (!entity.BookTitleArabic.IsDefault())
                TranslationGrantMember.BookTitleArabic = entity.BookTitleArabic.Trim();

            if (!entity.Author.IsDefault())
                TranslationGrantMember.Author = entity.Author.Trim();

            if (!entity.Isbn.IsDefault())
                TranslationGrantMember.Isbn = entity.Isbn.Trim();

            if (!entity.Genre.IsDefault())
                TranslationGrantMember.Genre = entity.Genre.Trim();

            if (!entity.OriginalLanguageId.IsDefault())
                TranslationGrantMember.OriginalLanguageId = entity.OriginalLanguageId;

            if (!entity.ProposedLanguageId.IsDefault())
                TranslationGrantMember.ProposedLanguageId = entity.ProposedLanguageId;

            if (!entity.NoOfPagesOriginal.IsDefault())
                TranslationGrantMember.NoOfPagesOriginal = entity.NoOfPagesOriginal.Trim();

            if (!entity.ApplicationForm.IsDefault())
                TranslationGrantMember.ApplicationForm = entity.ApplicationForm;

            if (!entity.GrantLetter.IsDefault())
                TranslationGrantMember.GrantLetter = entity.GrantLetter;

            if (!entity.NoOfWords.IsDefault())
                TranslationGrantMember.NoOfWords = entity.NoOfWords.Trim();

            if (!entity.Contract.IsDefault())
                TranslationGrantMember.Contract = entity.Contract;

            if (!entity.TransferFormByAdmin.IsDefault())
                TranslationGrantMember.TransferFormByAdmin = entity.TransferFormByAdmin;

            if (!entity.SignedContract.IsDefault())
                TranslationGrantMember.SignedContract = entity.SignedContract;

            if (!entity.BankName.IsDefault())
                TranslationGrantMember.BankName = entity.BankName.Trim();

            if (!entity.BranchNameAddress.IsDefault())
                TranslationGrantMember.BranchNameAddress = entity.BranchNameAddress.Trim();

            if (!entity.AccNo.IsDefault())
                TranslationGrantMember.AccNo = entity.AccNo.Trim();

            if (!entity.Ibanno.IsDefault())
                TranslationGrantMember.Ibanno = entity.Ibanno.Trim();

            if (!entity.SwiftCode.IsDefault())
                TranslationGrantMember.SwiftCode = entity.SwiftCode.Trim();

            if (!entity.OriginalSoftCopy.IsDefault())
                TranslationGrantMember.OriginalSoftCopy = entity.OriginalSoftCopy;

            if (!entity.FirstPayment.IsDefault())
                TranslationGrantMember.FirstPayment = entity.FirstPayment.Trim();

            if (!entity.FirstPaymentDate.IsDefault())
                TranslationGrantMember.FirstPaymentDate = entity.FirstPaymentDate;

            if (!entity.FirstPaymentReceipt.IsDefault())
                TranslationGrantMember.FirstPaymentReceipt = entity.FirstPaymentReceipt;

            if (!entity.Draft1.IsDefault())
                TranslationGrantMember.Draft1 = entity.Draft1;

            if (!entity.SecondPayment.IsDefault())
                TranslationGrantMember.SecondPayment = entity.SecondPayment.Trim();

            if (!entity.SecondPaymentReceipt.IsDefault())
                TranslationGrantMember.SecondPaymentReceipt = entity.SecondPaymentReceipt;

            if (!entity.SecondPaymentDate.IsDefault())
                TranslationGrantMember.SecondPaymentDate = entity.SecondPaymentDate;

            if (!entity.FinalPayment.IsDefault())
                TranslationGrantMember.FinalPayment = entity.FinalPayment.Trim();

            if (!entity.FinalPaymentDate.IsDefault())
                TranslationGrantMember.FinalPaymentDate = entity.FinalPaymentDate;

            if (!entity.FinalPaymentReceipt.IsDefault())
                TranslationGrantMember.FinalPaymentReceipt = entity.FinalPaymentReceipt;

            if (!entity.OriginalBookStatus.IsDefault())
                TranslationGrantMember.OriginalBookStatus = entity.OriginalBookStatus;

            if (!entity.IsIbanorBoth.IsDefault())
                TranslationGrantMember.IsIbanorBoth = entity.IsIbanorBoth;

            if (!entity.GenreId.IsDefault())
                TranslationGrantMember.GenreId = entity.GenreId;

            if (!entity.AwardGrantValue.IsDefault())
                TranslationGrantMember.AwardGrantValue = entity.AwardGrantValue.Trim();

            if (!entity.IsHardCopy.IsDefault())
                TranslationGrantMember.IsHardCopy = entity.IsHardCopy;

            if (!entity.DateofShipment.IsDefault())
                TranslationGrantMember.DateofShipment = entity.DateofShipment;

            if (!entity.BuyerorSellerRights.IsDefault())
                TranslationGrantMember.BuyerorSellerRights = entity.BuyerorSellerRights.Trim();

            if (!entity.FinalContract.IsDefault())
                TranslationGrantMember.FinalContract = entity.FinalContract;

            if (!entity.TransferForm.IsDefault())
                TranslationGrantMember.TransferForm = entity.TransferForm;

            if (!entity.ModifiedOn.IsDefault())
                TranslationGrantMember.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                TranslationGrantMember.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiExhibitionTranslationGrantMembers entity)
        {
            foreach (XsiExhibitionTranslationGrantMembers TranslationGrantMember in Select(entity))
            {
                XsiExhibitionTranslationGrantMembers aTranslationGrantMember = XsiContext.Context.XsiExhibitionTranslationGrantMembers.Find(TranslationGrantMember.ItemId);
                XsiContext.Context.XsiExhibitionTranslationGrantMembers.Remove(aTranslationGrantMember);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionTranslationGrantMembers aTranslationGrantMember = XsiContext.Context.XsiExhibitionTranslationGrantMembers.Find(itemId);
            XsiContext.Context.XsiExhibitionTranslationGrantMembers.Remove(aTranslationGrantMember);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
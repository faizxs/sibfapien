﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionStaffGuestParticipatingStatusLogRepository : IExhibitionStaffGuestParticipatingStatusLogRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionStaffGuestParticipatingStatusLogRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionStaffGuestParticipatingStatusLogRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionStaffGuestParticipatingStatusLogRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionStaffGuestParticipatingStatusLogs GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionStaffGuestParticipatingStatusLogs.Find(itemId);
        }
        public List<XsiExhibitionStaffGuestParticipatingStatusLogs> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionStaffGuestParticipatingStatusLogs> Select()
        {
            return XsiContext.Context.XsiExhibitionStaffGuestParticipatingStatusLogs.ToList();
        }
        public List<XsiExhibitionStaffGuestParticipatingStatusLogs> Select(XsiExhibitionStaffGuestParticipatingStatusLogs entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionStaffGuestParticipatingStatusLogs>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            if (!entity.StaffGuestId.IsDefault())
                predicate = predicate.And(p => p.StaffGuestId == entity.StaffGuestId);

            if (!entity.AdminId.IsDefault())
                predicate = predicate.And(p => p.AdminId == entity.AdminId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);


            return XsiContext.Context.XsiExhibitionStaffGuestParticipatingStatusLogs.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionStaffGuestParticipatingStatusLogs Add(XsiExhibitionStaffGuestParticipatingStatusLogs entity)
        {
            XsiExhibitionStaffGuestParticipatingStatusLogs ExhibitionStaffGuestParticipatingStatusLog = new XsiExhibitionStaffGuestParticipatingStatusLogs();
            ExhibitionStaffGuestParticipatingStatusLog.ExhibitionId = entity.ExhibitionId;
            ExhibitionStaffGuestParticipatingStatusLog.StaffGuestId = entity.StaffGuestId;
            ExhibitionStaffGuestParticipatingStatusLog.AdminId = entity.AdminId;
            ExhibitionStaffGuestParticipatingStatusLog.Status = entity.Status;
            ExhibitionStaffGuestParticipatingStatusLog.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiExhibitionStaffGuestParticipatingStatusLogs.Add(ExhibitionStaffGuestParticipatingStatusLog);
            SubmitChanges();
            return ExhibitionStaffGuestParticipatingStatusLog;

        }
        public void Update(XsiExhibitionStaffGuestParticipatingStatusLogs entity)
        {
            XsiExhibitionStaffGuestParticipatingStatusLogs ExhibitionStaffGuestParticipatingStatusLog = XsiContext.Context.XsiExhibitionStaffGuestParticipatingStatusLogs.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionStaffGuestParticipatingStatusLog).State = EntityState.Modified;

            if (!entity.Status.IsDefault())
                ExhibitionStaffGuestParticipatingStatusLog.Status = entity.Status;
        }
        public void Delete(XsiExhibitionStaffGuestParticipatingStatusLogs entity)
        {
            foreach (XsiExhibitionStaffGuestParticipatingStatusLogs ExhibitionStaffGuestParticipatingStatusLog in Select(entity))
            {
                XsiExhibitionStaffGuestParticipatingStatusLogs aExhibitionStaffGuestParticipatingStatusLog = XsiContext.Context.XsiExhibitionStaffGuestParticipatingStatusLogs.Find(ExhibitionStaffGuestParticipatingStatusLog.ItemId);
                XsiContext.Context.XsiExhibitionStaffGuestParticipatingStatusLogs.Remove(aExhibitionStaffGuestParticipatingStatusLog);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionStaffGuestParticipatingStatusLogs aExhibitionStaffGuestParticipatingStatusLog = XsiContext.Context.XsiExhibitionStaffGuestParticipatingStatusLogs.Find(itemId);
            XsiContext.Context.XsiExhibitionStaffGuestParticipatingStatusLogs.Remove(aExhibitionStaffGuestParticipatingStatusLog);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
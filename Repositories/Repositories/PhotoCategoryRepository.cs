﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PhotoCategoryRepository : IPhotoCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PhotoCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PhotoCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PhotoCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiPhotoCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiPhotoCategory.Find(itemId);
        }

        public List<XsiPhotoCategory> Select()
        {
            return XsiContext.Context.XsiPhotoCategory.ToList();
        }
        public List<XsiPhotoCategory> Select(XsiPhotoCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiPhotoCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiPhotoCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiPhotoCategory> SelectOr(XsiPhotoCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiPhotoCategory>();
            var Outer = PredicateBuilder.True<XsiPhotoCategory>();

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.TitleAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.TitleAr.Contains(searchKeys.str1) || p.TitleAr.Contains(searchKeys.str2) || p.TitleAr.Contains(searchKeys.str3) || p.TitleAr.Contains(searchKeys.str4) || p.TitleAr.Contains(searchKeys.str5)
                    || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                    || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                    || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsActiveAr.IsDefault())
                Outer = Outer.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiPhotoCategory.AsExpandable().Where(Outer.Expand()).ToList();
        }
        /*public List<XsiPhotoCategory> SelectOtherLanguageCategory(XsiPhotoCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiPhotoCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            var GroupId = XsiContext.Context.XsiPhotoCategory.AsExpandable().Where(predicate).Single().GroupId.Value;
            predicate = PredicateBuilder.True<XsiPhotoCategory>();
            predicate = predicate.And(p => p.GroupId == GroupId);
            predicate = predicate.And(p => p.LanguageId != entity.LanguageId);
            return XsiContext.Context.XsiPhotoCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiPhotoCategory> SelectCurrentLanguageCategory(XsiPhotoCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiPhotoCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            var GroupId = XsiContext.Context.XsiPhotoCategory.AsExpandable().Where(predicate).Single().GroupId.Value;
            predicate = PredicateBuilder.True<XsiPhotoCategory>();
            predicate = predicate.And(p => p.GroupId == GroupId);
            predicate = predicate.And(p => p.LanguageId == entity.LanguageId);
            return XsiContext.Context.XsiPhotoCategory.AsExpandable().Where(predicate).ToList();
        }*/

        public XsiPhotoCategory Add(XsiPhotoCategory entity)
        {
            XsiPhotoCategory PhotoCategory = new XsiPhotoCategory();

            PhotoCategory.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                PhotoCategory.Title = entity.Title.Trim();

            PhotoCategory.IsActiveAr = entity.IsActiveAr;
            if (!entity.TitleAr.IsDefault())
                PhotoCategory.TitleAr = entity.TitleAr.Trim();

            PhotoCategory.CreatedOn = entity.CreatedOn;
            PhotoCategory.CreatedBy = entity.CreatedBy;
            PhotoCategory.ModifiedOn = entity.ModifiedOn;
            PhotoCategory.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiPhotoCategory.Add(PhotoCategory);
            SubmitChanges();
            return PhotoCategory;

        }
        public void Update(XsiPhotoCategory entity)
        {
            XsiPhotoCategory PhotoCategory = XsiContext.Context.XsiPhotoCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(PhotoCategory).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                PhotoCategory.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                PhotoCategory.Title = entity.Title.Trim();

            if (!entity.IsActiveAr.IsDefault())
                PhotoCategory.IsActiveAr = entity.IsActiveAr;

            if (!entity.TitleAr.IsDefault())
                PhotoCategory.TitleAr = entity.TitleAr.Trim();

            if (!entity.ModifiedOn.IsDefault())
                PhotoCategory.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                PhotoCategory.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiPhotoCategory entity)
        {
            foreach (XsiPhotoCategory PhotoCategory in Select(entity))
            {
                XsiPhotoCategory aPhotoCategory = XsiContext.Context.XsiPhotoCategory.Find(PhotoCategory.ItemId);
                XsiContext.Context.XsiPhotoCategory.Remove(aPhotoCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiPhotoCategory aPhotoCategory = XsiContext.Context.XsiPhotoCategory.Find(itemId);
            XsiContext.Context.XsiPhotoCategory.Remove(aPhotoCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PublisherNewsRepository : IPublisherNewsRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PublisherNewsRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PublisherNewsRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PublisherNewsRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiPublisherNews GetById(long itemId)
        {
            return XsiContext.Context.XsiPublisherNews.Find(itemId);
        }
        
        public List<XsiPublisherNews> Select()
        {
            return XsiContext.Context.XsiPublisherNews.ToList();
        }
        public List<XsiPublisherNews> Select(XsiPublisherNews entity)
        {
            var predicate = PredicateBuilder.True<XsiPublisherNews>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            
            if (!entity.PublisherNewsCategoryId.IsDefault())
                predicate = predicate.And(p => p.PublisherNewsCategoryId == entity.PublisherNewsCategoryId);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated == entity.Dated);

            if (!entity.Detail.IsDefault())
                predicate = predicate.And(p => p.Detail.Contains(entity.Detail));

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Equals(entity.ImageName));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Equals(entity.FileName));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiPublisherNews.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiPublisherNews> SelectComeplete(XsiPublisherNews entity,long langId)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiPublisherNews>();
            var predicate = PredicateBuilder.True<XsiPublisherNews>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.PublisherNewsCategoryId.IsDefault())
                predicate = predicate.And(p => p.PublisherNewsCategoryId == entity.PublisherNewsCategoryId);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            //if (!entity.Title.IsDefault())
            //    predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (langId == 2 && !entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }


            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated == entity.Dated);

            if (!entity.Detail.IsDefault())
                predicate = predicate.And(p => p.Detail.Contains(entity.Detail));

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Equals(entity.ImageName));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Equals(entity.FileName));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiPublisherNews.Include(p => p.PublisherNewsCategoryId).AsExpandable().Where(predicate).ToList();
        }

        public XsiPublisherNews Add(XsiPublisherNews entity)
        {
            XsiPublisherNews PublisherNews = new XsiPublisherNews();
      
            PublisherNews.PublisherNewsCategoryId = entity.PublisherNewsCategoryId;
            PublisherNews.WebsiteId = entity.WebsiteId;
            if (!entity.Title.IsDefault())
                PublisherNews.Title = entity.Title.Trim();
            PublisherNews.Dated = entity.Dated;
            if (!entity.Detail.IsDefault())
                PublisherNews.Detail = entity.Detail.Trim();
            if (!entity.Overview.IsDefault())
                PublisherNews.Overview = entity.Overview.Trim();
            PublisherNews.IsActive = entity.IsActive;
            PublisherNews.ImageName = entity.ImageName;
            PublisherNews.FileName = entity.FileName;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                PublisherNews.TitleAr = entity.TitleAr.Trim();
            PublisherNews.DatedAr = entity.DatedAr;
            if (!entity.DetailAr.IsDefault())
                PublisherNews.DetailAr = entity.DetailAr.Trim();
            if (!entity.OverviewAr.IsDefault())
                PublisherNews.OverviewAr = entity.OverviewAr.Trim();
            PublisherNews.IsActiveAr = entity.IsActiveAr;
            PublisherNews.ImageNameAr = entity.ImageNameAr;
            PublisherNews.FileNameAr = entity.FileNameAr;
            #endregion Ar

            PublisherNews.CreatedOn = entity.CreatedOn;
            PublisherNews.CreatedBy = entity.CreatedBy;
            PublisherNews.ModifiedOn = entity.ModifiedOn;
            PublisherNews.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiPublisherNews.Add(PublisherNews);
            SubmitChanges();
            return PublisherNews;

        }
        public void Update(XsiPublisherNews entity)
        {
            XsiPublisherNews PublisherNews = XsiContext.Context.XsiPublisherNews.Find(entity.ItemId);
            XsiContext.Context.Entry(PublisherNews).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                PublisherNews.Title = entity.Title.Trim();

            if (!entity.WebsiteId.IsDefault())
                PublisherNews.WebsiteId = entity.WebsiteId;

            if (!entity.Dated.IsDefault())
                PublisherNews.Dated = entity.Dated;

            if (!entity.Detail.IsDefault())
                PublisherNews.Detail = entity.Detail.Trim();

            if (!entity.Overview.IsDefault())
                PublisherNews.Overview = entity.Overview.Trim();

            if (!entity.IsActive.IsDefault())
                PublisherNews.IsActive = entity.IsActive;

            if (!entity.ImageName.IsDefault())
                PublisherNews.ImageName = entity.ImageName;

            if (!entity.FileName.IsDefault())
                PublisherNews.FileName = entity.FileName;
           
            #region Ar
            if (!entity.TitleAr.IsDefault())
                PublisherNews.TitleAr = entity.TitleAr.Trim();
             
            if (!entity.DatedAr.IsDefault())
                PublisherNews.DatedAr = entity.DatedAr;

            if (!entity.DetailAr.IsDefault())
                PublisherNews.DetailAr = entity.DetailAr.Trim();

            if (!entity.OverviewAr.IsDefault())
                PublisherNews.OverviewAr = entity.OverviewAr.Trim();

            if (!entity.IsActiveAr.IsDefault())
                PublisherNews.IsActiveAr = entity.IsActiveAr;

            if (!entity.ImageNameAr.IsDefault())
                PublisherNews.ImageNameAr = entity.ImageNameAr;

            if (!entity.FileNameAr.IsDefault())
                PublisherNews.FileNameAr = entity.FileNameAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                PublisherNews.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                PublisherNews.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiPublisherNews entity)
        {
            foreach (XsiPublisherNews PublisherNews in Select(entity))
            {
                XsiPublisherNews aPublisherNews = XsiContext.Context.XsiPublisherNews.Find(PublisherNews.ItemId);
                XsiContext.Context.XsiPublisherNews.Remove(aPublisherNews);
            }
        }
        public void Delete(long itemId)
        {
            XsiPublisherNews aPublisherNews = XsiContext.Context.XsiPublisherNews.Find(itemId);
            XsiContext.Context.XsiPublisherNews.Remove(aPublisherNews);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFIllustrationRegistrationRepository : ISCRFIllustrationRegistrationRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFIllustrationRegistrationRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFIllustrationRegistrationRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFIllustrationRegistrationRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfillustrationRegistration GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfillustrationRegistration.Find(itemId);
        }

        public List<XsiScrfillustrationRegistration> Select()
        {
            return XsiContext.Context.XsiScrfillustrationRegistration.ToList();
        }
        public List<XsiScrfillustrationRegistration> Select(XsiScrfillustrationRegistration entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfillustrationRegistration>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.IllustrationId.IsDefault())
                predicate = predicate.And(p => p.IllustrationId == entity.IllustrationId);

            if (!entity.FileNumber.IsDefault())
                predicate = predicate.And(p => p.FileNumber.Contains(entity.FileNumber));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.FirstName.IsDefault())
                predicate = predicate.And(p => p.FirstName.Contains(entity.FirstName));

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));

            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail.Contains(entity.Thumbnail));

            if (!entity.Passport.IsDefault())
                predicate = predicate.And(p => p.Passport.Contains(entity.Passport));

            if (!entity.Cv.IsDefault())
                predicate = predicate.And(p => p.Cv.Contains(entity.Cv));

            if (!entity.Dob.IsDefault())
                predicate = predicate.And(p => p.Dob == entity.Dob);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.CityId.IsDefault())
                predicate = predicate.And(p => p.CityId == entity.CityId);

            if (!entity.Others.IsDefault())
                predicate = predicate.And(p => p.Others.Contains(entity.Others));

            if (!entity.Phone.IsDefault())
                predicate = predicate.And(p => p.Phone.Contains(entity.Phone));

            if (!entity.Mobile.IsDefault())
                predicate = predicate.And(p => p.Mobile.Contains(entity.Mobile));

            if (!entity.Fax.IsDefault())
                predicate = predicate.And(p => p.Fax.Contains(entity.Fax));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Equals(entity.Email));

            if (!entity.Bio.IsDefault())
                predicate = predicate.And(p => p.Bio.Contains(entity.Bio));

            if (!entity.Address1.IsDefault())
                predicate = predicate.And(p => p.Address1.Contains(entity.Address1));

            if (!entity.Address2.IsDefault())
                predicate = predicate.And(p => p.Address2.Contains(entity.Address2));

            if (!entity.PostalCode.IsDefault())
                predicate = predicate.And(p => p.PostalCode.Contains(entity.PostalCode));

            if (!entity.Username.IsDefault())
                predicate = predicate.And(p => p.Username.Equals(entity.Username));

            if (!entity.Password.IsDefault())
                predicate = predicate.And(p => p.Password.Contains(entity.Password));

            if (!entity.NickName.IsDefault())
                predicate = predicate.And(p => p.NickName.Contains(entity.NickName));

            if (!entity.NickNameAr.IsDefault())
                predicate = predicate.And(p => p.NickNameAr.Contains(entity.NickNameAr));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Contains(entity.IsActive));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Contains(entity.Status));

            if (!entity.Notification.IsDefault())
                predicate = predicate.And(p => p.Notification.Contains(entity.Notification));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);


            return XsiContext.Context.XsiScrfillustrationRegistration.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfillustrationRegistration Add(XsiScrfillustrationRegistration entity)
        {
            XsiScrfillustrationRegistration SCRFIllustrationRegistration = new XsiScrfillustrationRegistration();
            SCRFIllustrationRegistration.LanguageId = entity.LanguageId;
            SCRFIllustrationRegistration.IllustrationId = entity.IllustrationId;
            if (!entity.FileNumber.IsDefault())
                SCRFIllustrationRegistration.FileNumber = entity.FileNumber.Trim();
            if (!entity.Title.IsDefault())
                SCRFIllustrationRegistration.Title = entity.Title.Trim();
            if (!entity.FirstName.IsDefault())
                SCRFIllustrationRegistration.FirstName = entity.FirstName.Trim();
            if (!entity.LastName.IsDefault())
                SCRFIllustrationRegistration.LastName = entity.LastName.Trim();
            if (!entity.FirstNameAr.IsDefault())
                SCRFIllustrationRegistration.FirstNameAr = entity.FirstNameAr.Trim();
            if (!entity.LastNameAr.IsDefault())
            SCRFIllustrationRegistration.LastNameAr = entity.LastNameAr.Trim();
            SCRFIllustrationRegistration.Thumbnail = entity.Thumbnail;
            SCRFIllustrationRegistration.Dob = entity.Dob;
            SCRFIllustrationRegistration.NationalityId = entity.NationalityId;
            SCRFIllustrationRegistration.CountryId = entity.CountryId;
            SCRFIllustrationRegistration.CityId = entity.CityId;
            if (!entity.Others.IsDefault())
                SCRFIllustrationRegistration.Others = entity.Others.Trim();
            if (!entity.Phone.IsDefault())
                SCRFIllustrationRegistration.Phone = entity.Phone.Trim();
            if (!entity.Mobile.IsDefault())
                SCRFIllustrationRegistration.Mobile = entity.Mobile.Trim();
            if (!entity.Email.IsDefault())
                SCRFIllustrationRegistration.Email = entity.Email.Trim();
            if (!entity.Fax.IsDefault())
                SCRFIllustrationRegistration.Fax = entity.Fax.Trim();
            if (!entity.Email.IsDefault())
                SCRFIllustrationRegistration.Email = entity.Email.Trim();
            if (!entity.Bio.IsDefault())
                SCRFIllustrationRegistration.Bio = entity.Bio.Trim();
            if (!entity.BioAr.IsDefault())
                SCRFIllustrationRegistration.BioAr = entity.BioAr.Trim();
            if (!entity.Address1.IsDefault())
                SCRFIllustrationRegistration.Address1 = entity.Address1.Trim();
            if (!entity.Address2.IsDefault())
                SCRFIllustrationRegistration.Address2 = entity.Address2.Trim();
            if (!entity.PostalCode.IsDefault())
                SCRFIllustrationRegistration.PostalCode = entity.PostalCode.Trim();
            if (!entity.Username.IsDefault())
                SCRFIllustrationRegistration.Username = entity.Username.Trim();
            if (!entity.Password.IsDefault())
                SCRFIllustrationRegistration.Password = entity.Password.Trim();
            if (!entity.NickName.IsDefault())
                SCRFIllustrationRegistration.NickName = entity.NickName.Trim();
            if (!entity.NickNameAr.IsDefault())
            SCRFIllustrationRegistration.NickNameAr = entity.NickNameAr.Trim();
            SCRFIllustrationRegistration.IsActive = entity.IsActive;
            SCRFIllustrationRegistration.Status = entity.Status;
            if (!entity.Notification.IsDefault())
            SCRFIllustrationRegistration.Notification = entity.Notification.Trim();
            SCRFIllustrationRegistration.CreatedOn = entity.CreatedOn;
            SCRFIllustrationRegistration.ModifiedOn = entity.ModifiedOn;
            XsiContext.Context.XsiScrfillustrationRegistration.Add(SCRFIllustrationRegistration);
            SubmitChanges();
            return SCRFIllustrationRegistration;
        }
        public void Update(XsiScrfillustrationRegistration entity)
        {
            XsiScrfillustrationRegistration SCRFIllustrationRegistration = XsiContext.Context.XsiScrfillustrationRegistration.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFIllustrationRegistration).State = EntityState.Modified;

            if (!entity.LanguageId.IsDefault())
                SCRFIllustrationRegistration.LanguageId = entity.LanguageId;

            if (!entity.IllustrationId.IsDefault())
                SCRFIllustrationRegistration.IllustrationId = entity.IllustrationId;

            if (!entity.Title.IsDefault())
                SCRFIllustrationRegistration.Title = entity.Title.Trim();

            if (!entity.FileNumber.IsDefault())
                SCRFIllustrationRegistration.FileNumber = entity.FileNumber.Trim();

            if (!entity.FirstName.IsDefault())
                SCRFIllustrationRegistration.FirstName = entity.FirstName.Trim();

            if (!entity.LastName.IsDefault())
                SCRFIllustrationRegistration.LastName = entity.LastName.Trim();

            if (!entity.FirstNameAr.IsDefault())
                SCRFIllustrationRegistration.FirstNameAr = entity.FirstNameAr.Trim();

            if (!entity.LastNameAr.IsDefault())
                SCRFIllustrationRegistration.LastNameAr = entity.LastNameAr.Trim();

            if (!entity.Thumbnail.IsDefault())
                SCRFIllustrationRegistration.Thumbnail = entity.Thumbnail;

            if (!entity.Dob.IsDefault())
                SCRFIllustrationRegistration.Dob = entity.Dob;

            if (!entity.NationalityId.IsDefault())
                SCRFIllustrationRegistration.NationalityId = entity.NationalityId;

            if (!entity.CountryId.IsDefault())
                SCRFIllustrationRegistration.CountryId = entity.CountryId;

            if (!entity.CityId.IsDefault())
                SCRFIllustrationRegistration.CityId = entity.CityId;

            if (!entity.Others.IsDefault())
                SCRFIllustrationRegistration.Others = entity.Others.Trim();

            if (!entity.Phone.IsDefault())
                SCRFIllustrationRegistration.Phone = entity.Phone.Trim();

            if (!entity.Mobile.IsDefault())
                SCRFIllustrationRegistration.Mobile = entity.Mobile.Trim();

            if (!entity.Fax.IsDefault())
                SCRFIllustrationRegistration.Fax = entity.Fax.Trim();

            if (!entity.Email.IsDefault())
                SCRFIllustrationRegistration.Email = entity.Email.Trim();

            if (!entity.Bio.IsDefault())
                SCRFIllustrationRegistration.Bio = entity.Bio.Trim();

            if (!entity.BioAr.IsDefault())
                SCRFIllustrationRegistration.BioAr = entity.BioAr.Trim();

            if (!entity.Address1.IsDefault())
                SCRFIllustrationRegistration.Address1 = entity.Address1.Trim();

            if (!entity.Address2.IsDefault())
                SCRFIllustrationRegistration.Address2 = entity.Address2.Trim();

            if (!entity.PostalCode.IsDefault())
                SCRFIllustrationRegistration.PostalCode = entity.PostalCode.Trim();

            if (!entity.Username.IsDefault())
                SCRFIllustrationRegistration.Username = entity.Username.Trim();

            if (!entity.Password.IsDefault())
                SCRFIllustrationRegistration.Password = entity.Password.Trim();

            if (!entity.NickName.IsDefault())
                SCRFIllustrationRegistration.NickName = entity.NickName.Trim();

            if (!entity.NickNameAr.IsDefault())
                SCRFIllustrationRegistration.NickNameAr = entity.NickNameAr.Trim();

            if (!entity.IsActive.IsDefault())
                SCRFIllustrationRegistration.IsActive = entity.IsActive;

            if (!entity.Status.IsDefault())
                SCRFIllustrationRegistration.Status = entity.Status;

            if (!entity.Notification.IsDefault())
                SCRFIllustrationRegistration.Notification = entity.Notification.Trim();

            if (!entity.ModifiedOn.IsDefault())
                SCRFIllustrationRegistration.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiScrfillustrationRegistration entity)
        {
            foreach (XsiScrfillustrationRegistration SCRFIllustrationRegistration in Select(entity))
            {
                XsiScrfillustrationRegistration aSCRFIllustrationRegistration = XsiContext.Context.XsiScrfillustrationRegistration.Find(SCRFIllustrationRegistration.ItemId);
                XsiContext.Context.XsiScrfillustrationRegistration.Remove(aSCRFIllustrationRegistration);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfillustrationRegistration aSCRFIllustrationRegistration = XsiContext.Context.XsiScrfillustrationRegistration.Find(itemId);
            XsiContext.Context.XsiScrfillustrationRegistration.Remove(aSCRFIllustrationRegistration);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
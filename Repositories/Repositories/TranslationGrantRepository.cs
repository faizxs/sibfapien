﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class TranslationGrantRepository : ITranslationGrantRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public TranslationGrantRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public TranslationGrantRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public TranslationGrantRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionTranslationGrant GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionTranslationGrant.Find(itemId);
        }

        public List<XsiExhibitionTranslationGrant> Select()
        {
            return XsiContext.Context.XsiExhibitionTranslationGrant.ToList();
        }
        public List<XsiExhibitionTranslationGrant> Select(XsiExhibitionTranslationGrant entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionTranslationGrant>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsArchive.IsDefault())
                predicate = predicate.And(p => p.IsArchive.Equals(entity.IsArchive));

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            if (!entity.Phase1StartDate.IsDefault())
                predicate = predicate.And(p => p.Phase1StartDate == entity.Phase1StartDate);

            if (!entity.Phase1EndDate.IsDefault())
                predicate = predicate.And(p => p.Phase1EndDate == entity.Phase1EndDate);

            if (!entity.Phase1Desc.IsDefault())
                predicate = predicate.And(p => p.Phase1Desc.Contains(entity.Phase1Desc));

            if (!entity.Phase2StartDate.IsDefault())
                predicate = predicate.And(p => p.Phase2StartDate == entity.Phase2StartDate);

            if (!entity.Phase2EndDate.IsDefault())
                predicate = predicate.And(p => p.Phase2EndDate == entity.Phase2EndDate);

            if (!entity.Phase2Desc.IsDefault())
                predicate = predicate.And(p => p.Phase2Desc.Contains(entity.Phase2Desc));

            if (!entity.Phase3StartDate.IsDefault())
                predicate = predicate.And(p => p.Phase3StartDate == entity.Phase3StartDate);

            if (!entity.Phase3EndDate.IsDefault())
                predicate = predicate.And(p => p.Phase3EndDate == entity.Phase3EndDate);

            if (!entity.Phase3Desc.IsDefault())
                predicate = predicate.And(p => p.Phase3Desc.Contains(entity.Phase3Desc));

            if (!entity.Phase4Desc.IsDefault())
                predicate = predicate.And(p => p.Phase4Desc.Contains(entity.Phase4Desc));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionTranslationGrant.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionTranslationGrant Add(XsiExhibitionTranslationGrant entity)
        {
            XsiExhibitionTranslationGrant TranslationGrant = new XsiExhibitionTranslationGrant();
            if (!entity.Title.IsDefault())
            TranslationGrant.Title = entity.Title.Trim();
            TranslationGrant.IsActive = entity.IsActive;
            TranslationGrant.IsArchive = entity.IsArchive;
            TranslationGrant.ExhibitionId = entity.ExhibitionId;
            TranslationGrant.Phase1StartDate = entity.Phase1StartDate;
            TranslationGrant.Phase1EndDate = entity.Phase1EndDate;
            if (!entity.Phase1Desc.IsDefault())
            TranslationGrant.Phase1Desc = entity.Phase1Desc.Trim();
            TranslationGrant.Phase2StartDate = entity.Phase2StartDate;
            TranslationGrant.Phase2EndDate = entity.Phase2EndDate;
            if (!entity.Phase2Desc.IsDefault())
            TranslationGrant.Phase2Desc = entity.Phase2Desc.Trim();
            TranslationGrant.Phase3StartDate = entity.Phase3StartDate;
            TranslationGrant.Phase3EndDate = entity.Phase3EndDate;
            if (!entity.Phase3Desc.IsDefault())
                TranslationGrant.Phase3Desc = entity.Phase3Desc.Trim();
            if (!entity.Phase4Desc.IsDefault())
            TranslationGrant.Phase4Desc = entity.Phase4Desc.Trim();
            TranslationGrant.CreatedOn = entity.CreatedOn;
            TranslationGrant.CreatedBy = entity.CreatedBy;
            TranslationGrant.ModifiedOn = entity.ModifiedOn;
            TranslationGrant.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionTranslationGrant.Add(TranslationGrant);
            SubmitChanges();
            return TranslationGrant;

        }
        public void Update(XsiExhibitionTranslationGrant entity)
        {
            XsiExhibitionTranslationGrant TranslationGrant = XsiContext.Context.XsiExhibitionTranslationGrant.Find(entity.ItemId);
            XsiContext.Context.Entry(TranslationGrant).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                TranslationGrant.Title = entity.Title.Trim();

            if (!entity.IsActive.IsDefault())
                TranslationGrant.IsActive = entity.IsActive;

            if (!entity.IsArchive.IsDefault())
                TranslationGrant.IsArchive = entity.IsArchive;

            if (!entity.ExhibitionId.IsDefault())
                TranslationGrant.ExhibitionId = entity.ExhibitionId;

            if (!entity.Phase1StartDate.IsDefault())
                TranslationGrant.Phase1StartDate = entity.Phase1StartDate;

            if (!entity.Phase1EndDate.IsDefault())
                TranslationGrant.Phase1EndDate = entity.Phase1EndDate;

            if (!entity.Phase1Desc.IsDefault())
                TranslationGrant.Phase1Desc = entity.Phase1Desc.Trim();

            if (!entity.Phase2StartDate.IsDefault())
                TranslationGrant.Phase2StartDate = entity.Phase2StartDate;

            if (!entity.Phase2EndDate.IsDefault())
                TranslationGrant.Phase2EndDate = entity.Phase2EndDate;

            if (!entity.Phase2Desc.IsDefault())
                TranslationGrant.Phase2Desc = entity.Phase2Desc.Trim();

            if (!entity.Phase3StartDate.IsDefault())
                TranslationGrant.Phase3StartDate = entity.Phase3StartDate;

            if (!entity.Phase3EndDate.IsDefault())
                TranslationGrant.Phase3EndDate = entity.Phase3EndDate;

            if (!entity.Phase3Desc.IsDefault())
                TranslationGrant.Phase3Desc = entity.Phase3Desc.Trim();

            if (!entity.Phase4Desc.IsDefault())
                TranslationGrant.Phase4Desc = entity.Phase4Desc.Trim();

            if (!entity.ModifiedOn.IsDefault())
                TranslationGrant.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                TranslationGrant.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiExhibitionTranslationGrant entity)
        {
            foreach (XsiExhibitionTranslationGrant TranslationGrant in Select(entity))
            {
                XsiExhibitionTranslationGrant aTranslationGrant = XsiContext.Context.XsiExhibitionTranslationGrant.Find(TranslationGrant.ItemId);
                XsiContext.Context.XsiExhibitionTranslationGrant.Remove(aTranslationGrant);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionTranslationGrant aTranslationGrant = XsiContext.Context.XsiExhibitionTranslationGrant.Find(itemId);
            XsiContext.Context.XsiExhibitionTranslationGrant.Remove(aTranslationGrant);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
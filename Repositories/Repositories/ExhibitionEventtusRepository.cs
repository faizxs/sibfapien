﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionEventtusRepository : IExhibitionEventtusRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionEventtusRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionEventtusRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionEventtusRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionEventtus GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionEventtus.Find(itemId);
        }

        public List<XsiExhibitionEventtus> Select()
        {
            return XsiContext.Context.XsiExhibitionEventtus.ToList();
        }
        public List<XsiExhibitionEventtus> Select(XsiExhibitionEventtus entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionEventtus>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.EventtusId.IsDefault())
                predicate = predicate.And(p => p.EventtusId.Equals(entity.EventtusId));

            if (!entity.Day.IsDefault())
                predicate = predicate.And(p => p.Day.Equals(entity.Day));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId==entity.WebsiteId);

            if (!entity.StartDate.IsDefault())
                predicate = predicate.And(p => p.StartDate == entity.StartDate);

            if (!entity.EndDate.IsDefault())
                predicate = predicate.And(p => p.EndDate == entity.EndDate);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionEventtus.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionEventtus Add(XsiExhibitionEventtus entity)
        {
            XsiExhibitionEventtus ExhibitionEventtus = new XsiExhibitionEventtus();

            ExhibitionEventtus.IsActive = entity.IsActive;

            if (!entity.EventtusId.IsDefault())
                ExhibitionEventtus.EventtusId = entity.EventtusId;

            if (!entity.Day.IsDefault())
                ExhibitionEventtus.Day = entity.Day;

            if (!entity.WebsiteId.IsDefault())
                ExhibitionEventtus.WebsiteId = entity.WebsiteId;

            if (!entity.Title.IsDefault())
                ExhibitionEventtus.Title = entity.Title;

            if (!entity.StartDate.IsDefault())
                ExhibitionEventtus.StartDate = entity.StartDate;

            if (!entity.EndDate.IsDefault())
                ExhibitionEventtus.EndDate = entity.EndDate;

            ExhibitionEventtus.CreatedOn = entity.CreatedOn;
            ExhibitionEventtus.CreatedBy = entity.CreatedBy;
            ExhibitionEventtus.ModifiedOn = entity.ModifiedOn;
            ExhibitionEventtus.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionEventtus.Add(ExhibitionEventtus);
            SubmitChanges();
            return ExhibitionEventtus;

        }
        public void Update(XsiExhibitionEventtus entity)
        {
            XsiExhibitionEventtus ExhibitionEventtus = XsiContext.Context.XsiExhibitionEventtus.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionEventtus).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                ExhibitionEventtus.IsActive = entity.IsActive;

            if (!entity.EventtusId.IsDefault())
                ExhibitionEventtus.EventtusId = entity.EventtusId;

            if (!entity.WebsiteId.IsDefault())
                ExhibitionEventtus.WebsiteId = entity.WebsiteId;

            if (!entity.Day.IsDefault())
                ExhibitionEventtus.Day = entity.Day;

            if (!entity.Title.IsDefault())
                ExhibitionEventtus.Day = entity.Title;

            if (!entity.StartDate.IsDefault())
                ExhibitionEventtus.StartDate = entity.StartDate;

            if (!entity.EndDate.IsDefault())
                ExhibitionEventtus.EndDate = entity.EndDate;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionEventtus.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionEventtus.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiExhibitionEventtus entity)
        {
            foreach (XsiExhibitionEventtus ExhibitionEventtus in Select(entity))
            {
                XsiExhibitionEventtus aExhibitionEventtus = XsiContext.Context.XsiExhibitionEventtus.Find(ExhibitionEventtus.ItemId);
                XsiContext.Context.XsiExhibitionEventtus.Remove(aExhibitionEventtus);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionEventtus aExhibitionEventtus = XsiContext.Context.XsiExhibitionEventtus.Find(itemId);
            XsiContext.Context.XsiExhibitionEventtus.Remove(aExhibitionEventtus);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
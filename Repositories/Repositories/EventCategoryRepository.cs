﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class EventCategoryRepository : IEventCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public EventCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public EventCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public EventCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiEventCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiEventCategory.Find(itemId);
        }
         
        public List<XsiEventCategory> Select()
        {
            return XsiContext.Context.XsiEventCategory.ToList();
        }
        public List<XsiEventCategory> Select(XsiEventCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiEventCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.EventtusEventCategoryId.IsDefault())
                predicate = predicate.And(p => p.EventtusEventCategoryId.Equals(entity.EventtusEventCategoryId));

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.EmailId.IsDefault())
                predicate = predicate.And(p => p.EmailId.Contains(entity.EmailId));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.ScrfDetails.IsDefault())
                predicate = predicate.And(p => p.ScrfDetails.Contains(entity.ScrfDetails));

            if (!entity.Details.IsDefault())
                predicate = predicate.And(p => p.Details.Contains(entity.Details));

            if (!entity.Color.IsDefault())
                predicate = predicate.And(p => p.Color.Contains(entity.Color));

            if (!entity.ScrfThumbnail.IsDefault())
                predicate = predicate.And(p => p.ScrfThumbnail.Contains(entity.ScrfThumbnail));

            if (!entity.ScrfColor.IsDefault())
                predicate = predicate.And(p => p.ScrfColor.Contains(entity.ScrfColor));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiEventCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiEventCategory> SelectOr(XsiEventCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiEventCategory>();
            var Outer = PredicateBuilder.True<XsiEventCategory>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            } 

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiEventCategory.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiEventCategory> SelectOtherLanguageCategory(XsiEventCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiEventCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            //var GroupId = XsiContext.Context.XsiEventCategory.AsExpandable().Where(predicate).Single().GroupId.Value;
            predicate = PredicateBuilder.True<XsiEventCategory>();
           
            return XsiContext.Context.XsiEventCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiEventCategory> SelectCurrentLanguageCategory(XsiEventCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiEventCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            //var GroupId = XsiContext.Context.XsiEventCategory.AsExpandable().Where(predicate).Single().GroupId.Value;
            predicate = PredicateBuilder.True<XsiEventCategory>();
           
            return XsiContext.Context.XsiEventCategory.AsExpandable().Where(predicate).ToList();
        }

        public XsiEventCategory Add(XsiEventCategory entity)
        {
            XsiEventCategory EventCategory = new XsiEventCategory();
             
            if (!entity.EventtusEventCategoryId.IsDefault())
                EventCategory.EventtusEventCategoryId = entity.EventtusEventCategoryId;

            EventCategory.WebsiteId = entity.WebsiteId;
            EventCategory.IsActive = entity.IsActive;
            EventCategory.EmailId = entity.EmailId;
            if (!entity.Title.IsDefault())
                EventCategory.Title = entity.Title.Trim();
            if (!entity.ScrfDetails.IsDefault())
                EventCategory.ScrfDetails = entity.ScrfDetails.Trim();
            if (!entity.Details.IsDefault())
                EventCategory.Details = entity.Details.Trim();
            EventCategory.Thumbnail = entity.Thumbnail;
            if (!entity.Color.IsDefault())
                EventCategory.Color = entity.Color.Trim();
            EventCategory.ScrfThumbnail = entity.ScrfThumbnail;
            if (!entity.ScrfColor.IsDefault())
                EventCategory.ScrfColor = entity.ScrfColor.Trim();

            #region Ar
            if (!entity.TitleAr.IsDefault())
                EventCategory.TitleAr = entity.TitleAr.Trim();
            if (!entity.ScrfDetailsAr.IsDefault())
                EventCategory.ScrfDetailsAr = entity.ScrfDetailsAr.Trim();
            if (!entity.DetailsAr.IsDefault())
                EventCategory.DetailsAr = entity.DetailsAr.Trim();
            if (!entity.ThumbnailAr.IsDefault())
                EventCategory.ThumbnailAr = entity.ThumbnailAr;
            if (!entity.ColorAr.IsDefault())
                EventCategory.ColorAr = entity.ColorAr.Trim();
            if (!entity.ScrfThumbnailAr.IsDefault())
                EventCategory.ScrfThumbnailAr = entity.ScrfThumbnailAr;
            if (!entity.ScrfColorAr.IsDefault())
                EventCategory.ScrfColorAr = entity.ScrfColorAr.Trim();
            #endregion Ar
            EventCategory.CreatedOn = entity.CreatedOn;
            EventCategory.CreatedBy = entity.CreatedBy;
            EventCategory.ModifiedOn = entity.ModifiedOn;
            EventCategory.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiEventCategory.Add(EventCategory);
            SubmitChanges();
            return EventCategory;

        }
        public void Update(XsiEventCategory entity)
        {
            XsiEventCategory EventCategory = XsiContext.Context.XsiEventCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(EventCategory).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                EventCategory.IsActive = entity.IsActive;

            if (!entity.WebsiteId.IsDefault())
                EventCategory.WebsiteId = entity.WebsiteId;

            if (!entity.EventtusEventCategoryId.IsDefault())
                EventCategory.EventtusEventCategoryId = entity.EventtusEventCategoryId;

            if (!entity.EmailId.IsDefault())
                EventCategory.EmailId = entity.EmailId.Trim();

            if (!entity.Title.IsDefault())
                EventCategory.Title = entity.Title.Trim();

            if (!entity.ScrfDetails.IsDefault())
                EventCategory.ScrfDetails = entity.ScrfDetails.Trim();

            if (!entity.Details.IsDefault())
                EventCategory.Details = entity.Details.Trim();

            if (!entity.Thumbnail.IsDefault())
                EventCategory.Thumbnail = entity.Thumbnail;

            if (!entity.Color.IsDefault())
                EventCategory.Color = entity.Color.Trim();

            if (!entity.ScrfThumbnail.IsDefault())
                EventCategory.ScrfThumbnail = entity.ScrfThumbnail;

            if (!entity.ScrfColor.IsDefault())
                EventCategory.ScrfColor = entity.ScrfColor.Trim();

            #region Ar
            if (!entity.TitleAr.IsDefault())
                EventCategory.TitleAr = entity.TitleAr.Trim();
            if (!entity.ScrfDetailsAr.IsDefault())
                EventCategory.ScrfDetailsAr = entity.ScrfDetailsAr.Trim();
            if (!entity.DetailsAr.IsDefault())
                EventCategory.DetailsAr = entity.DetailsAr.Trim();
            if (!entity.ThumbnailAr.IsDefault())
                EventCategory.ThumbnailAr = entity.ThumbnailAr;
            if (!entity.ColorAr.IsDefault())
                EventCategory.ColorAr = entity.ColorAr.Trim();
            if (!entity.ScrfThumbnailAr.IsDefault())
                EventCategory.ScrfThumbnailAr = entity.ScrfThumbnailAr;
            if (!entity.ScrfColorAr.IsDefault())
                EventCategory.ScrfColorAr = entity.ScrfColorAr.Trim();
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                EventCategory.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                EventCategory.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiEventCategory entity)
        {
            foreach (XsiEventCategory EventCategory in Select(entity))
            {
                XsiEventCategory aEventCategory = XsiContext.Context.XsiEventCategory.Find(EventCategory.ItemId);
                XsiContext.Context.XsiEventCategory.Remove(aEventCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiEventCategory aEventCategory = XsiContext.Context.XsiEventCategory.Find(itemId);
            XsiContext.Context.XsiEventCategory.Remove(aEventCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
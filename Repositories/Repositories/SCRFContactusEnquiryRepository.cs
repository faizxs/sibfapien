﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFContactusEnquiryRepository : ISCRFContactusEnquiryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFContactusEnquiryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFContactusEnquiryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFContactusEnquiryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfcontactusEnquiry GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfcontactusEnquiry.Find(itemId);
        }
        public List<XsiScrfcontactusEnquiry> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiScrfcontactusEnquiry> Select()
        {
            return XsiContext.Context.XsiScrfcontactusEnquiry.ToList();
        }
        public List<XsiScrfcontactusEnquiry> SelectOr(XsiScrfcontactusEnquiry entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfcontactusEnquiry>();
            var Outer = PredicateBuilder.True<XsiScrfcontactusEnquiry>();

            if (!entity.Subject.IsDefault())
            {
                string strSearchText = entity.Subject;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                if (!entity.LastName.IsDefault())
                {
                    Inner = Inner.Or(p => p.LastName.Contains(searchKeys.str1) || p.LastName.Contains(searchKeys.str2) || p.LastName.Contains(searchKeys.str3) || p.LastName.Contains(searchKeys.str4) || p.LastName.Contains(searchKeys.str5)
                         || p.LastName.Contains(searchKeys.str6) || p.LastName.Contains(searchKeys.str7) || p.LastName.Contains(searchKeys.str8) || p.LastName.Contains(searchKeys.str9) || p.LastName.Contains(searchKeys.str10)
                         || p.LastName.Contains(searchKeys.str11) || p.LastName.Contains(searchKeys.str12) || p.LastName.Contains(searchKeys.str13)
                         || p.LastName.Contains(searchKeys.str14) || p.LastName.Contains(searchKeys.str15) || p.LastName.Contains(searchKeys.str16) || p.LastName.Contains(searchKeys.str17)
                         );
                }

                if (!entity.MiddleName.IsDefault())
                {
                    Inner = Inner.Or(p => p.MiddleName.Contains(searchKeys.str1) || p.MiddleName.Contains(searchKeys.str2) || p.MiddleName.Contains(searchKeys.str3) || p.MiddleName.Contains(searchKeys.str4) || p.MiddleName.Contains(searchKeys.str5)
                        || p.MiddleName.Contains(searchKeys.str6) || p.MiddleName.Contains(searchKeys.str7) || p.MiddleName.Contains(searchKeys.str8) || p.MiddleName.Contains(searchKeys.str9) || p.MiddleName.Contains(searchKeys.str10)
                        || p.MiddleName.Contains(searchKeys.str11) || p.MiddleName.Contains(searchKeys.str12) || p.MiddleName.Contains(searchKeys.str13)
                        || p.MiddleName.Contains(searchKeys.str14) || p.MiddleName.Contains(searchKeys.str15) || p.MiddleName.Contains(searchKeys.str16) || p.MiddleName.Contains(searchKeys.str17)
                        );
                }

                if (!entity.FirstName.IsDefault())
                {
                    Inner = Inner.Or(p => (p.FirstName + " " + p.MiddleName + " " + p.LastName).Contains(searchKeys.strSearchText)
                    || p.FirstName.Contains(searchKeys.str1) || p.FirstName.Contains(searchKeys.str2) || p.FirstName.Contains(searchKeys.str3) || p.FirstName.Contains(searchKeys.str4) || p.FirstName.Contains(searchKeys.str5)
                        || p.FirstName.Contains(searchKeys.str6) || p.FirstName.Contains(searchKeys.str7) || p.FirstName.Contains(searchKeys.str8) || p.FirstName.Contains(searchKeys.str9) || p.FirstName.Contains(searchKeys.str10)
                        || p.FirstName.Contains(searchKeys.str11) || p.FirstName.Contains(searchKeys.str12) || p.FirstName.Contains(searchKeys.str13)
                        || p.FirstName.Contains(searchKeys.str14) || p.FirstName.Contains(searchKeys.str15) || p.FirstName.Contains(searchKeys.str16) || p.FirstName.Contains(searchKeys.str17)
                    );
                }

                Inner = Inner.Or(p => p.Subject.Contains(searchKeys.str1) || p.Subject.Contains(searchKeys.str2) || p.Subject.Contains(searchKeys.str3) || p.Subject.Contains(searchKeys.str4) || p.Subject.Contains(searchKeys.str5)
                    || p.Subject.Contains(searchKeys.str6) || p.Subject.Contains(searchKeys.str7) || p.Subject.Contains(searchKeys.str8) || p.Subject.Contains(searchKeys.str9) || p.Subject.Contains(searchKeys.str10)
                    || p.Subject.Contains(searchKeys.str11) || p.Subject.Contains(searchKeys.str12) || p.Subject.Contains(searchKeys.str13)
                    || p.Subject.Contains(searchKeys.str14) || p.Subject.Contains(searchKeys.str15) || p.Subject.Contains(searchKeys.str16) || p.Subject.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Subject.IsDefault())
                {
                    string strSearchText = entity.Subject.ToLower();

                    if (!entity.LastName.IsDefault())
                        Inner = Inner.Or(p => p.LastName.ToLower().Contains(strSearchText));

                    if (!entity.MiddleName.IsDefault())
                        Inner = Inner.Or(p => p.MiddleName.ToLower().Contains(strSearchText));

                    if (!entity.FirstName.IsDefault())
                        Inner = Inner.Or(p => (p.FirstName.ToLower() + " " + p.MiddleName.ToLower() + " " + p.LastName.ToLower()).Contains(strSearchText));

                    Inner = Inner.Or(p => p.Subject.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.Status.IsDefault())
                Outer = Outer.And(p => p.Status.Equals(entity.Status));

            if (!entity.FlagType.IsDefault())
                Outer = Outer.And(p => p.FlagType.Equals(entity.FlagType));

            if (!entity.CountryId.IsDefault())
                Outer = Outer.And(p => p.CountryId == entity.CountryId);

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiScrfcontactusEnquiry.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiScrfcontactusEnquiry> Select(XsiScrfcontactusEnquiry entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfcontactusEnquiry>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.FirstName.IsDefault())
                predicate = predicate.And(p => p.FirstName.Contains(entity.FirstName));

            if (!entity.MiddleName.IsDefault())
                predicate = predicate.And(p => p.MiddleName.Contains(entity.MiddleName));

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));

            if (!entity.Phone.IsDefault())
                predicate = predicate.And(p => p.Phone.Equals(entity.Phone));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Equals(entity.Email));

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.Subject.IsDefault())
                predicate = predicate.And(p => p.Subject.Contains(entity.Subject));

            if (!entity.Detail.IsDefault())
                predicate = predicate.And(p => p.Detail.Contains(entity.Detail));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.DateAdded.IsDefault())
                predicate = predicate.And(p => p.DateAdded == entity.DateAdded);

            if (!entity.DateClosed.IsDefault())
                predicate = predicate.And(p => p.DateClosed == entity.DateClosed);

            if (!entity.IsViewed.IsDefault())
                predicate = predicate.And(p => p.IsViewed.Equals(entity.IsViewed));

            if (!entity.FlagType.IsDefault())
                predicate = predicate.And(p => p.FlagType.Equals(entity.FlagType));

            return XsiContext.Context.XsiScrfcontactusEnquiry.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfcontactusEnquiry Add(XsiScrfcontactusEnquiry entity)
        {
            XsiScrfcontactusEnquiry SCRFContactusEnquiry = new XsiScrfcontactusEnquiry();
            SCRFContactusEnquiry.CategoryId = entity.CategoryId;
            if (!entity.FirstName.IsDefault())
                SCRFContactusEnquiry.FirstName = entity.FirstName.Trim();
            if (!entity.MiddleName.IsDefault())
                SCRFContactusEnquiry.MiddleName = entity.MiddleName.Trim();
            if (!entity.LastName.IsDefault())
                SCRFContactusEnquiry.LastName = entity.LastName.Trim();
            if (!entity.Phone.IsDefault())
                SCRFContactusEnquiry.Phone = entity.Phone.Trim();
            if (!entity.Email.IsDefault())
            SCRFContactusEnquiry.Email = entity.Email.Trim();
            SCRFContactusEnquiry.CountryId = entity.CountryId;
            if (!entity.Subject.IsDefault())
                SCRFContactusEnquiry.Subject = entity.Subject.Trim();
            if (!entity.Detail.IsDefault())
            SCRFContactusEnquiry.Detail = entity.Detail.Trim();
            SCRFContactusEnquiry.Status = entity.Status;
            SCRFContactusEnquiry.DateAdded = entity.DateAdded;
            SCRFContactusEnquiry.DateClosed = entity.DateClosed;
            SCRFContactusEnquiry.IsViewed = entity.IsViewed;
            SCRFContactusEnquiry.FlagType = entity.FlagType;

            XsiContext.Context.XsiScrfcontactusEnquiry.Add(SCRFContactusEnquiry);
            SubmitChanges();
            return SCRFContactusEnquiry;

        }
        public void Update(XsiScrfcontactusEnquiry entity)
        {
            XsiScrfcontactusEnquiry SCRFContactusEnquiry = XsiContext.Context.XsiScrfcontactusEnquiry.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFContactusEnquiry).State = EntityState.Modified;

            if (!entity.CategoryId.IsDefault())
                SCRFContactusEnquiry.CategoryId = entity.CategoryId;

            if (!entity.FirstName.IsDefault())
                SCRFContactusEnquiry.FirstName = entity.FirstName.Trim();

            if (!entity.MiddleName.IsDefault())
                SCRFContactusEnquiry.MiddleName = entity.MiddleName.Trim();

            if (!entity.LastName.IsDefault())
                SCRFContactusEnquiry.LastName = entity.LastName.Trim();

            if (!entity.Phone.IsDefault())
                SCRFContactusEnquiry.Phone = entity.Phone.Trim();

            if (!entity.Email.IsDefault())
                SCRFContactusEnquiry.Email = entity.Email.Trim();

            if (!entity.CountryId.IsDefault())
                SCRFContactusEnquiry.CountryId = entity.CountryId;

            if (!entity.Subject.IsDefault())
                SCRFContactusEnquiry.Subject = entity.Subject.Trim();

            if (!entity.Detail.IsDefault())
                SCRFContactusEnquiry.Detail = entity.Detail.Trim();

            if (!entity.Status.IsDefault())
                SCRFContactusEnquiry.Status = entity.Status;

            if (!entity.DateAdded.IsDefault())
                SCRFContactusEnquiry.DateAdded = entity.DateAdded;

            if (!entity.DateClosed.IsDefault())
                SCRFContactusEnquiry.DateClosed = entity.DateClosed;

            if (!entity.IsViewed.IsDefault())
                SCRFContactusEnquiry.IsViewed = entity.IsViewed;

            if (!entity.FlagType.IsDefault())
                SCRFContactusEnquiry.FlagType = entity.FlagType;
        }
        public void Delete(XsiScrfcontactusEnquiry entity)
        {
            foreach (XsiScrfcontactusEnquiry SCRFContactusEnquiry in Select(entity))
            {
                XsiScrfcontactusEnquiry aSCRFContactusEnquiry = XsiContext.Context.XsiScrfcontactusEnquiry.Find(SCRFContactusEnquiry.ItemId);
                XsiContext.Context.XsiScrfcontactusEnquiry.Remove(aSCRFContactusEnquiry);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfcontactusEnquiry aSCRFContactusEnquiry = XsiContext.Context.XsiScrfcontactusEnquiry.Find(itemId);
            XsiContext.Context.XsiScrfcontactusEnquiry.Remove(aSCRFContactusEnquiry);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
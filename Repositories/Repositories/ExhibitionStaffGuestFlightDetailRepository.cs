﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionStaffGuestFlightDetailRepository : IExhibitionStaffGuestFlightDetailsRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionStaffGuestFlightDetailRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionStaffGuestFlightDetailRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionStaffGuestFlightDetailRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionStaffGuestFlightDetails GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionStaffGuestFlightDetails.Find(itemId);
        }
        public List<XsiExhibitionStaffGuestFlightDetails> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionStaffGuestFlightDetails> Select()
        {
            return XsiContext.Context.XsiExhibitionStaffGuestFlightDetails.ToList();
        }

        public List<XsiExhibitionStaffGuestFlightDetails> Select(XsiExhibitionStaffGuestFlightDetails entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionStaffGuestFlightDetails>();

            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.StaffGuestId.IsDefault())
                predicate = predicate.And(p => p.StaffGuestId == entity.StaffGuestId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            #region Flight Details

            if (!entity.GuestCategory.IsDefault())
                predicate = predicate.And(p => p.GuestCategory.Equals(entity.GuestCategory));

            if (!entity.ContactDetails.IsDefault())
                predicate = predicate.And(p => p.ContactDetails.Contains(entity.ContactDetails));

            if (!entity.GuestPhoneNumber.IsDefault())
                predicate = predicate.And(p => p.GuestPhoneNumber.Contains(entity.GuestPhoneNumber));

            if (!entity.StaffInChargePhoneNumber.IsDefault())
                predicate = predicate.And(p => p.StaffInChargePhoneNumber.Contains(entity.StaffInChargePhoneNumber));

            if (!entity.ServiceType.IsDefault())
                predicate = predicate.And(p => p.ServiceType.Equals(entity.ServiceType));

            if (!entity.MarhabaReferenceNumber.IsDefault())
                predicate = predicate.And(p => p.MarhabaReferenceNumber.Equals(entity.MarhabaReferenceNumber));

            if (!entity.StartDate.IsDefault())
                predicate = predicate.And(p => p.StartDate == entity.StartDate);

            if (!entity.EndDate.IsDefault())
                predicate = predicate.And(p => p.EndDate == entity.EndDate);

            if (!entity.PreferredTime.IsDefault())
                predicate = predicate.And(p => p.PreferredTime == entity.PreferredTime);

            if (!entity.PreferredAirline.IsDefault())
                predicate = predicate.And(p => p.PreferredAirline.Contains(entity.PreferredAirline));

            if (!entity.ClassSeat.IsDefault())
                predicate = predicate.And(p => p.ClassSeat.Equals(entity.ClassSeat));

            if (!entity.FlightBookingType.IsDefault())
                predicate = predicate.And(p => p.FlightBookingType == entity.FlightBookingType);

            if (!entity.FlightDetails.IsDefault())
                predicate = predicate.And(p => p.FlightDetails.Equals(entity.FlightDetails));

            if (!entity.FlightNumber.IsDefault())
                predicate = predicate.And(p => p.FlightNumber.Contains(entity.FlightNumber));

            if (!entity.FlightCheckInTime.IsDefault())
                predicate = predicate.And(p => p.FlightCheckInTime.Contains(entity.FlightCheckInTime));

            if (!entity.FlightCheckOutTime.IsDefault())
                predicate = predicate.And(p => p.FlightCheckOutTime.Contains(entity.FlightCheckOutTime));

            if (!entity.ArrivalDate.IsDefault())
                predicate = predicate.And(p => p.ArrivalDate == entity.ArrivalDate);

            if (!entity.ReturnDate.IsDefault())
                predicate = predicate.And(p => p.ReturnDate == entity.ReturnDate);

            if (!entity.Terminal.IsDefault())
                predicate = predicate.And(p => p.Terminal.Equals(entity.Terminal));

            if (!entity.ClassSeatAllotted.IsDefault())
                predicate = predicate.And(p => p.ClassSeatAllotted.Equals(entity.ClassSeatAllotted));

            if (!entity.FlightTicket.IsDefault())
                predicate = predicate.And(p => p.FlightTicket.Contains(entity.FlightTicket));

            if (!entity.Passport.IsDefault())
                predicate = predicate.And(p => p.Passport.Contains(entity.Passport));

            if (!entity.PassportCopyTwo.IsDefault())
                predicate = predicate.And(p => p.PassportCopyTwo.Contains(entity.PassportCopyTwo));

            if (!entity.IsPassportModified.IsDefault())
                predicate = predicate.And(p => p.IsPassportModified.Contains(entity.IsPassportModified));

            #endregion

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionStaffGuestFlightDetails.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionStaffGuestFlightDetails Add(XsiExhibitionStaffGuestFlightDetails entity)
        {
            XsiExhibitionStaffGuestFlightDetails detailsentity = new XsiExhibitionStaffGuestFlightDetails();

            detailsentity.StaffGuestId = entity.StaffGuestId;
            detailsentity.ExhibitionId = entity.ExhibitionId;

            #region Flight Details
            detailsentity.GuestCategory = entity.GuestCategory;
            detailsentity.ContactDetails = entity.ContactDetails;
            detailsentity.GuestPhoneNumber = entity.GuestPhoneNumber;
            detailsentity.StaffInChargePhoneNumber = entity.StaffInChargePhoneNumber;
            detailsentity.ServiceType = entity.ServiceType;
            detailsentity.MarhabaReferenceNumber = entity.MarhabaReferenceNumber;
            detailsentity.StartDate = entity.StartDate;
            detailsentity.EndDate = entity.EndDate;
            detailsentity.PreferredTime = entity.PreferredTime;
            detailsentity.PreferredAirline = entity.PreferredAirline;
            detailsentity.ClassSeat = entity.ClassSeat;
            detailsentity.FlightBookingType = entity.FlightBookingType;
            detailsentity.FlightStatus = entity.FlightStatus;
            detailsentity.FlightDetails = entity.FlightDetails;
            detailsentity.FlightNumber = entity.FlightNumber;
            detailsentity.FlightCheckInTime = entity.FlightCheckInTime;
            detailsentity.FlightCheckOutTime = entity.FlightCheckOutTime;
            detailsentity.ArrivalDate = entity.ArrivalDate;
            detailsentity.ReturnDate = entity.ReturnDate;
            detailsentity.Terminal = entity.Terminal;
            detailsentity.ClassSeatAllotted = entity.ClassSeatAllotted;
            detailsentity.FlightTicket = entity.FlightTicket;
            detailsentity.IsPassportModified = entity.IsPassportModified;
            detailsentity.Passport = entity.Passport;
            detailsentity.PassportCopyTwo = entity.PassportCopyTwo;
            #endregion

            detailsentity.IsActive = entity.IsActive;
            detailsentity.CreatedOn = entity.CreatedOn;
            detailsentity.CreatedBy = entity.CreatedBy;
            detailsentity.ModifiedOn = entity.ModifiedOn;
            detailsentity.ModifiedBy = entity.ModifiedBy;



            XsiContext.Context.XsiExhibitionStaffGuestFlightDetails.Add(detailsentity);
            SubmitChanges();
            return detailsentity;

        }
        public void Update(XsiExhibitionStaffGuestFlightDetails entity)
        {
            XsiExhibitionStaffGuestFlightDetails detailsentity = XsiContext.Context.XsiExhibitionStaffGuestFlightDetails.Find(entity.ItemId);

            XsiContext.Context.Entry(detailsentity).State = EntityState.Modified;

            if (!entity.StaffGuestId.IsDefault())
                detailsentity.StaffGuestId = entity.StaffGuestId;

            if (!entity.ExhibitionId.IsDefault())
                detailsentity.ExhibitionId = entity.ExhibitionId;

            #region Flight Details
            if (!entity.GuestCategory.IsDefault())
                detailsentity.GuestCategory = entity.GuestCategory;

            if (!entity.ContactDetails.IsDefault())
                detailsentity.ContactDetails = entity.ContactDetails;

            if (!entity.GuestPhoneNumber.IsDefault())
                detailsentity.GuestPhoneNumber = entity.GuestPhoneNumber.Trim();

            if (!entity.StaffInChargePhoneNumber.IsDefault())
                detailsentity.StaffInChargePhoneNumber = entity.StaffInChargePhoneNumber.Trim();

            if (!entity.ServiceType.IsDefault())
                detailsentity.ServiceType = entity.ServiceType;

            if (!entity.MarhabaReferenceNumber.IsDefault())
                detailsentity.MarhabaReferenceNumber = entity.MarhabaReferenceNumber.Trim();

            if (!entity.StartDate.IsDefault())
                detailsentity.StartDate = entity.StartDate;

            if (!entity.EndDate.IsDefault())
                detailsentity.EndDate = entity.EndDate;

            if (!entity.PreferredTime.IsDefault())
                detailsentity.PreferredTime = entity.PreferredTime;

            if (!entity.PreferredAirline.IsDefault())
                detailsentity.PreferredAirline = entity.PreferredAirline.Trim();

            if (!entity.ClassSeat.IsDefault())
                detailsentity.ClassSeat = entity.ClassSeat.Trim();

            if (!entity.FlightBookingType.IsDefault())
                detailsentity.FlightBookingType = entity.FlightBookingType;

            if (!entity.FlightStatus.IsDefault())
                detailsentity.FlightStatus = entity.FlightStatus.Trim();

            if (!entity.FlightDetails.IsDefault())
                detailsentity.FlightDetails = entity.FlightDetails.Trim();

            if (!entity.FlightNumber.IsDefault())
                detailsentity.FlightNumber = entity.FlightNumber.Trim();

            if (!entity.ArrivalDate.IsDefault())
                detailsentity.ArrivalDate = entity.ArrivalDate;

            if (!entity.FlightCheckInTime.IsDefault())
                detailsentity.FlightCheckInTime = entity.FlightCheckInTime;

            if (!entity.FlightCheckOutTime.IsDefault())
                detailsentity.FlightCheckOutTime = entity.FlightCheckOutTime;

            if (!entity.ArrivalDateTime.IsDefault())
                detailsentity.ArrivalDateTime = entity.ArrivalDateTime;

            if (!entity.ReturnDateTime.IsDefault())
                detailsentity.ReturnDateTime = entity.ReturnDateTime;

            if (!entity.ReturnDate.IsDefault())
                detailsentity.ReturnDate = entity.ReturnDate;

            if (!entity.Terminal.IsDefault())
                detailsentity.Terminal = entity.Terminal;

            if (!entity.ClassSeatAllotted.IsDefault())
                detailsentity.ClassSeatAllotted = entity.ClassSeatAllotted;

            if (!entity.FlightTicket.IsDefault())
                detailsentity.FlightTicket = entity.FlightTicket;

            if (!entity.Passport.IsDefault())
                detailsentity.Passport = entity.Passport.Trim();

            if (!entity.PassportCopyTwo.IsDefault())
                detailsentity.PassportCopyTwo = entity.PassportCopyTwo.Trim();

            if (!entity.IsPassportModified.IsDefault())
                detailsentity.IsPassportModified = entity.IsPassportModified.Trim();
            #endregion

            if (!entity.IsActive.IsDefault())
                detailsentity.IsActive = entity.IsActive;

            if (!entity.ModifiedBy.IsDefault())
                detailsentity.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                detailsentity.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiExhibitionStaffGuestFlightDetails entity)
        {
            foreach (XsiExhibitionStaffGuestFlightDetails detailsentity in Select(entity))
            {
                XsiExhibitionStaffGuestFlightDetails aExhibitionStaffGuestParticipating = XsiContext.Context.XsiExhibitionStaffGuestFlightDetails.Find(detailsentity.ItemId);
                XsiContext.Context.XsiExhibitionStaffGuestFlightDetails.Remove(aExhibitionStaffGuestParticipating);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionStaffGuestFlightDetails detailsentity = XsiContext.Context.XsiExhibitionStaffGuestFlightDetails.Where(p => p.ItemId == itemId).OrderByDescending(o => o.ItemId).FirstOrDefault();
            XsiContext.Context.XsiExhibitionStaffGuestFlightDetails.Remove(detailsentity);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
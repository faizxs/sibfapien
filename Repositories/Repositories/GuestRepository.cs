﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class GuestRepository : IGuestRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public GuestRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public GuestRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public GuestRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiGuests GetById(long itemId)
        {
            return XsiContext.Context.XsiGuests.Find(itemId);
        }
          
        public List<XsiGuests> Select()
        {
            return XsiContext.Context.XsiGuests.ToList();
        }
        public List<XsiGuests> Select(XsiGuests entity)
        {
            var predicate = PredicateBuilder.True<XsiGuests>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.EventtusGuestId.IsDefault())
                predicate = predicate.And(p => p.EventtusGuestId.Equals(entity.EventtusGuestId));
             
            if (!entity.GuestName.IsDefault())
                predicate = predicate.And(p => p.GuestName.Contains(entity.GuestName));

            if (!entity.GuestTitle.IsDefault())
                predicate = predicate.And(p => p.GuestTitle.Contains(entity.GuestTitle));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.GuestPhoto.IsDefault())
                predicate = predicate.And(p => p.GuestPhoto.Contains(entity.GuestPhoto));

            //if (!entity.EventId.IsDefault())
            //    predicate = predicate.And(p => p.EventId == entity.EventId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiGuests.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiGuests> SelectOr(XsiGuests entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiGuests>();
            var Outer = PredicateBuilder.True<XsiGuests>();

            if (!entity.GuestName.IsDefault())
            {
                string strSearchText = entity.GuestName;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.GuestName.Contains(searchKeys.str1) || p.GuestName.Contains(searchKeys.str2) || p.GuestName.Contains(searchKeys.str3) || p.GuestName.Contains(searchKeys.str4) || p.GuestName.Contains(searchKeys.str5)
                    || p.GuestName.Contains(searchKeys.str6) || p.GuestName.Contains(searchKeys.str7) || p.GuestName.Contains(searchKeys.str8) || p.GuestName.Contains(searchKeys.str9) || p.GuestName.Contains(searchKeys.str10)
                    || p.GuestName.Contains(searchKeys.str11) || p.GuestName.Contains(searchKeys.str12) || p.GuestName.Contains(searchKeys.str13)
                    || p.GuestName.Contains(searchKeys.str14) || p.GuestName.Contains(searchKeys.str15) || p.GuestName.Contains(searchKeys.str16) || p.GuestName.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.GuestName.IsDefault())
                {
                    string strSearchText = entity.GuestName.ToLower();
                    Inner = Inner.Or(p => p.GuestName.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
             
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiGuests.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiGuests Add(XsiGuests entity)
        {
            XsiGuests Guest = new XsiGuests();
            Guest.EventtusGuestId = entity.EventtusGuestId;
            
            Guest.IsActive = entity.IsActive;
            if (!entity.GuestName.IsDefault())
                Guest.GuestName = entity.GuestName.Trim();
            if (!entity.GuestTitle.IsDefault())
                Guest.GuestTitle = entity.GuestTitle.Trim();
            if (!entity.Description.IsDefault())
                Guest.Description = entity.Description.Trim();
            if (!entity.GuestPhoto.IsDefault())
                Guest.GuestPhoto = entity.GuestPhoto.Trim();
            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                Guest.IsActiveAr = entity.IsActiveAr;
            if (!entity.GuestNameAr.IsDefault())
                Guest.GuestNameAr = entity.GuestNameAr.Trim();
            if (!entity.GuestTitleAr.IsDefault())
                Guest.GuestTitleAr = entity.GuestTitleAr.Trim();
            if (!entity.DescriptionAr.IsDefault())
                Guest.DescriptionAr = entity.DescriptionAr.Trim();
            if (!entity.GuestPhotoAr.IsDefault())
                Guest.GuestPhotoAr = entity.GuestPhotoAr.Trim();
            #endregion Ar

            //Guest.EventId = entity.EventId;
            Guest.CreatedOn = entity.CreatedOn;
            Guest.CreatedBy = entity.CreatedBy;
            Guest.ModifiedOn = entity.ModifiedOn;
            Guest.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiGuests.Add(Guest);
            SubmitChanges();
            return Guest;

        }
        public void Update(XsiGuests entity)
        {
            XsiGuests Guest = XsiContext.Context.XsiGuests.Find(entity.ItemId);
            XsiContext.Context.Entry(Guest).State = EntityState.Modified;

            if (!entity.EventtusGuestId.IsDefault())
                Guest.EventtusGuestId = entity.EventtusGuestId;

            if (!entity.IsActive.IsDefault())
                Guest.IsActive = entity.IsActive;
            if (!entity.GuestName.IsDefault())
                Guest.GuestName = entity.GuestName.Trim();
            if (!entity.GuestTitle.IsDefault())
                Guest.GuestTitle = entity.GuestTitle.Trim();
            if (!entity.Description.IsDefault())
                Guest.Description = entity.Description.Trim();
            if (!entity.GuestPhoto.IsDefault())
                Guest.GuestPhoto = entity.GuestPhoto.Trim();
            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                Guest.IsActiveAr = entity.IsActiveAr;
            if (!entity.GuestNameAr.IsDefault())
                Guest.GuestNameAr = entity.GuestNameAr.Trim();
            if (!entity.GuestTitleAr.IsDefault())
                Guest.GuestTitleAr = entity.GuestTitleAr.Trim();
            if (!entity.DescriptionAr.IsDefault())
                Guest.DescriptionAr = entity.DescriptionAr.Trim();
            if (!entity.GuestPhotoAr.IsDefault())
                Guest.GuestPhotoAr = entity.GuestPhotoAr.Trim();
            #endregion Ar

            //if (!entity.EventId.IsDefault())
            //    Guest.EventId = entity.EventId;
            if (!entity.ModifiedOn.IsDefault())
                Guest.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                Guest.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiGuests entity)
        {
            foreach (XsiGuests Guest in Select(entity))
            {
                XsiGuests aGuest = XsiContext.Context.XsiGuests.Find(Guest.ItemId);
                XsiContext.Context.XsiGuests.Remove(aGuest);
            }
        }
        public void Delete(long itemId)
        {
            XsiGuests aGuest = XsiContext.Context.XsiGuests.Find(itemId);
            XsiContext.Context.XsiGuests.Remove(aGuest);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SharjahAwardForTranslationRepository : ISharjahAwardForTranslationRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SharjahAwardForTranslationRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SharjahAwardForTranslationRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SharjahAwardForTranslationRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiSharjahAwardForTranslation GetById(long itemId)
        {
            return XsiContext.Context.XsiSharjahAwardForTranslation.Find(itemId);
        }
        public List<XsiSharjahAwardForTranslation> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiSharjahAwardForTranslation> Select()
        {
            return XsiContext.Context.XsiSharjahAwardForTranslation.ToList();
        }
        public List<XsiSharjahAwardForTranslation> Select(XsiSharjahAwardForTranslation entity)
        {
            var predicate = PredicateBuilder.True<XsiSharjahAwardForTranslation>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId.Equals(entity.ExhibitionId));
             
            if (!entity.AwardId.IsDefault())
                predicate = predicate.And(p => p.AwardId == entity.AwardId);

            if (!entity.TranslatedBookName.IsDefault())
                predicate = predicate.And(p => p.TranslatedBookName == entity.TranslatedBookName);

            if (!entity.PublishedYearOfFirstTranslation.IsDefault())
                predicate = predicate.And(p => p.PublishedYearOfFirstTranslation == entity.PublishedYearOfFirstTranslation);

            if (!entity.TranslatorName.IsDefault())
                predicate = predicate.And(p => p.TranslatorName == entity.TranslatorName);

            if (!entity.BookNameArabic.IsDefault())
                predicate = predicate.And(p => p.BookNameArabic.Equals(entity.BookNameArabic));

            if (!entity.PublishedYearOfFirstArabicTranslation.IsDefault())
                predicate = predicate.And(p => p.PublishedYearOfFirstArabicTranslation.Equals(entity.PublishedYearOfFirstArabicTranslation));

            if (!entity.AuthorName.IsDefault())
                predicate = predicate.And(p => p.AuthorName.Equals(entity.AuthorName));

            if (!entity.ForeignPublishName.IsDefault())
                predicate = predicate.And(p => p.ForeignPublishName.Equals(entity.ForeignPublishName));

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId.Equals(entity.CountryId));

            if (!entity.TranslationLanguageId.IsDefault())
                predicate = predicate.And(p => p.TranslationLanguageId.Equals(entity.TranslationLanguageId));

            if (!entity.CorrespondenceAddress.IsDefault())
                predicate = predicate.And(p => p.CorrespondenceAddress.Equals(entity.CorrespondenceAddress));

            if (!entity.Pobox.IsDefault())
                predicate = predicate.And(p => p.Pobox.Equals(entity.Pobox));

            if (!entity.Phone.IsDefault())
                predicate = predicate.And(p => p.Phone.Equals(entity.Phone));

            if (!entity.Fax.IsDefault())
                predicate = predicate.And(p => p.Fax.Equals(entity.Fax));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Equals(entity.Email));

            if (!entity.ArabPublishHouseName.IsDefault())
                predicate = predicate.And(p => p.ArabPublishHouseName.Equals(entity.ArabPublishHouseName));

            if (!entity.ArabPublishHouseCountryId.IsDefault())
                predicate = predicate.And(p => p.ArabPublishHouseCountryId.Equals(entity.ArabPublishHouseCountryId));

            if (!entity.ArabPublishHouseNameAdress.IsDefault())
                predicate = predicate.And(p => p.ArabPublishHouseNameAdress.Equals(entity.ArabPublishHouseNameAdress));

            if (!entity.ArabPublishHousePobox.IsDefault())
                predicate = predicate.And(p => p.ArabPublishHousePobox.Equals(entity.ArabPublishHousePobox));

            if (!entity.ArabPublishHousePhone.IsDefault())
                predicate = predicate.And(p => p.ArabPublishHousePhone.Equals(entity.ArabPublishHousePhone));

            if (!entity.ArabPublishHouseFax.IsDefault())
                predicate = predicate.And(p => p.ArabPublishHouseFax.Equals(entity.ArabPublishHouseFax));

            if (!entity.ArabPublishHouseEmail.IsDefault())
                predicate = predicate.And(p => p.ArabPublishHouseEmail.Equals(entity.ArabPublishHouseEmail));

            if (!entity.ArabPublishHouseNominationBody.IsDefault())
                predicate = predicate.And(p => p.ArabPublishHouseNominationBody.Equals(entity.ArabPublishHouseNominationBody));

            if (!entity.Date.IsDefault())
                predicate = predicate.And(p => p.Date.Equals(entity.Date));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.PassportCopy.IsDefault())
                predicate = predicate.And(p => p.PassportCopy.Equals(entity.PassportCopy));

            if (!entity.Comments.IsDefault())
                predicate = predicate.And(p => p.Comments.Equals(entity.Comments));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn.Equals(entity.CreatedOn));

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn.Equals(entity.ModifiedOn));

            return XsiContext.Context.XsiSharjahAwardForTranslation.AsExpandable().Where(predicate).ToList();
        }
        public XsiSharjahAwardForTranslation Add(XsiSharjahAwardForTranslation entity)
        {
            XsiSharjahAwardForTranslation SharjahAwardForTranslation = new XsiSharjahAwardForTranslation();
             
            if (!entity.Status.IsDefault())
                SharjahAwardForTranslation.Status = entity.Status;
            if (!entity.ExhibitionId.IsDefault())
                SharjahAwardForTranslation.ExhibitionId = entity.ExhibitionId;
            
            if (!entity.AwardId.IsDefault())
                SharjahAwardForTranslation.AwardId = entity.AwardId;
            if (!entity.TranslatedBookName.IsDefault())
                SharjahAwardForTranslation.TranslatedBookName = entity.TranslatedBookName.Trim();
            if (!entity.PublishedYearOfFirstTranslation.IsDefault())
                SharjahAwardForTranslation.PublishedYearOfFirstTranslation = entity.PublishedYearOfFirstTranslation.Trim();
            if (!entity.TranslatorName.IsDefault())
                SharjahAwardForTranslation.TranslatorName = entity.TranslatorName.Trim();
            if (!entity.BookNameArabic.IsDefault())
                SharjahAwardForTranslation.BookNameArabic = entity.BookNameArabic.Trim();
            if (!entity.PublishedYearOfFirstArabicTranslation.IsDefault())
                SharjahAwardForTranslation.PublishedYearOfFirstArabicTranslation = entity.PublishedYearOfFirstArabicTranslation;
            if (!entity.AuthorName.IsDefault())
                SharjahAwardForTranslation.AuthorName = entity.AuthorName;
            if (!entity.ForeignPublishName.IsDefault())
                SharjahAwardForTranslation.ForeignPublishName = entity.ForeignPublishName;
            if (!entity.CountryId.IsDefault())
                SharjahAwardForTranslation.CountryId = entity.CountryId;

            if (!entity.TranslationLanguageId.IsDefault())
                SharjahAwardForTranslation.TranslationLanguageId = entity.TranslationLanguageId;

            if (!entity.CorrespondenceAddress.IsDefault())
                SharjahAwardForTranslation.CorrespondenceAddress = entity.CorrespondenceAddress.Trim();

            if (!entity.Pobox.IsDefault())
                SharjahAwardForTranslation.Pobox = entity.Pobox.Trim();

            if (!entity.Phone.IsDefault())
                SharjahAwardForTranslation.Phone = entity.Phone.Trim();

            if (!entity.Fax.IsDefault())
                SharjahAwardForTranslation.Fax = entity.Fax.Trim();

            if (!entity.Email.IsDefault())
                SharjahAwardForTranslation.Email = entity.Email.Trim();

            if (!entity.ArabPublishHouseName.IsDefault())
                SharjahAwardForTranslation.ArabPublishHouseName = entity.ArabPublishHouseName.Trim();

            if (!entity.ArabPublishHouseCountryId.IsDefault())
                SharjahAwardForTranslation.ArabPublishHouseCountryId = entity.ArabPublishHouseCountryId;

            if (!entity.ArabPublishHouseNameAdress.IsDefault())
                SharjahAwardForTranslation.ArabPublishHouseNameAdress = entity.ArabPublishHouseNameAdress.Trim();

            if (!entity.ArabPublishHousePobox.IsDefault())
                SharjahAwardForTranslation.ArabPublishHousePobox = entity.ArabPublishHousePobox.Trim();

            if (!entity.ArabPublishHousePhone.IsDefault())
                SharjahAwardForTranslation.ArabPublishHousePhone = entity.ArabPublishHousePhone.Trim();

            if (!entity.ArabPublishHouseFax.IsDefault())
                SharjahAwardForTranslation.ArabPublishHouseFax = entity.ArabPublishHouseFax.Trim();

            if (!entity.ArabPublishHouseEmail.IsDefault())
                SharjahAwardForTranslation.ArabPublishHouseEmail = entity.ArabPublishHouseEmail.Trim();

            if (!entity.ArabPublishHouseNominationBody.IsDefault())
                SharjahAwardForTranslation.ArabPublishHouseNominationBody = entity.ArabPublishHouseNominationBody.Trim();

            if (!entity.Date.IsDefault())
                SharjahAwardForTranslation.Date = entity.Date;

            if (!entity.Comments.IsDefault())
                SharjahAwardForTranslation.Comments = entity.Comments;

            if (!entity.PassportCopy.IsDefault())
                SharjahAwardForTranslation.PassportCopy = entity.PassportCopy;
            if (!entity.IsActive.IsDefault())
                SharjahAwardForTranslation.IsActive = entity.IsActive;
            SharjahAwardForTranslation.CreatedOn = entity.CreatedOn;
            SharjahAwardForTranslation.ModifiedOn = entity.ModifiedOn;
            XsiContext.Context.XsiSharjahAwardForTranslation.Add(SharjahAwardForTranslation);
            SubmitChanges();
            return SharjahAwardForTranslation;

        }
        public void Update(XsiSharjahAwardForTranslation entity)
        {
            XsiSharjahAwardForTranslation SharjahAwardForTranslation = XsiContext.Context.XsiSharjahAwardForTranslation.Find(entity.ItemId);
            XsiContext.Context.Entry(SharjahAwardForTranslation).State = EntityState.Modified;

            if (!entity.Status.IsDefault())
                SharjahAwardForTranslation.Status = entity.Status;
            if (!entity.ExhibitionId.IsDefault())
                SharjahAwardForTranslation.ExhibitionId = entity.ExhibitionId;
            
            if (!entity.AwardId.IsDefault())
                SharjahAwardForTranslation.AwardId = entity.AwardId;
            if (!entity.TranslatedBookName.IsDefault())
                SharjahAwardForTranslation.TranslatedBookName = entity.TranslatedBookName.Trim();
            if (!entity.PublishedYearOfFirstTranslation.IsDefault())
                SharjahAwardForTranslation.PublishedYearOfFirstTranslation = entity.PublishedYearOfFirstTranslation.Trim();
            if (!entity.TranslatorName.IsDefault())
                SharjahAwardForTranslation.TranslatorName = entity.TranslatorName.Trim();
            if (!entity.BookNameArabic.IsDefault())
                SharjahAwardForTranslation.BookNameArabic = entity.BookNameArabic.Trim();
            if (!entity.PublishedYearOfFirstArabicTranslation.IsDefault())
                SharjahAwardForTranslation.PublishedYearOfFirstArabicTranslation = entity.PublishedYearOfFirstArabicTranslation;
            if (!entity.AuthorName.IsDefault())
                SharjahAwardForTranslation.AuthorName = entity.AuthorName;
            if (!entity.ForeignPublishName.IsDefault())
                SharjahAwardForTranslation.ForeignPublishName = entity.ForeignPublishName;
            if (!entity.CountryId.IsDefault())
                SharjahAwardForTranslation.CountryId = entity.CountryId;

            if (!entity.TranslationLanguageId.IsDefault())
                SharjahAwardForTranslation.TranslationLanguageId = entity.TranslationLanguageId;

            if (!entity.CorrespondenceAddress.IsDefault())
                SharjahAwardForTranslation.CorrespondenceAddress = entity.CorrespondenceAddress.Trim();

            if (!entity.Pobox.IsDefault())
                SharjahAwardForTranslation.Pobox = entity.Pobox.Trim();

            if (!entity.Phone.IsDefault())
                SharjahAwardForTranslation.Phone = entity.Phone.Trim();

            if (!entity.Fax.IsDefault())
                SharjahAwardForTranslation.Fax = entity.Fax.Trim();

            if (!entity.Email.IsDefault())
                SharjahAwardForTranslation.Email = entity.Email.Trim();

            if (!entity.ArabPublishHouseName.IsDefault())
                SharjahAwardForTranslation.ArabPublishHouseName = entity.ArabPublishHouseName.Trim();

            if (!entity.ArabPublishHouseCountryId.IsDefault())
                SharjahAwardForTranslation.ArabPublishHouseCountryId = entity.ArabPublishHouseCountryId;

            if (!entity.ArabPublishHouseNameAdress.IsDefault())
                SharjahAwardForTranslation.ArabPublishHouseNameAdress = entity.ArabPublishHouseNameAdress.Trim();

            if (!entity.ArabPublishHousePobox.IsDefault())
                SharjahAwardForTranslation.ArabPublishHousePobox = entity.ArabPublishHousePobox.Trim();

            if (!entity.ArabPublishHousePhone.IsDefault())
                SharjahAwardForTranslation.ArabPublishHousePhone = entity.ArabPublishHousePhone.Trim();

            if (!entity.ArabPublishHouseFax.IsDefault())
                SharjahAwardForTranslation.ArabPublishHouseFax = entity.ArabPublishHouseFax.Trim();

            if (!entity.ArabPublishHouseEmail.IsDefault())
                SharjahAwardForTranslation.ArabPublishHouseEmail = entity.ArabPublishHouseEmail.Trim();

            if (!entity.ArabPublishHouseNominationBody.IsDefault())
                SharjahAwardForTranslation.ArabPublishHouseNominationBody = entity.ArabPublishHouseNominationBody.Trim();

            if (!entity.Date.IsDefault())
                SharjahAwardForTranslation.Date = entity.Date;

            if (!entity.PassportCopy.IsDefault())
                SharjahAwardForTranslation.PassportCopy = entity.PassportCopy;

            if (!entity.Comments.IsDefault())
                SharjahAwardForTranslation.Comments = entity.Comments;

            if (!entity.IsActive.IsDefault())
                SharjahAwardForTranslation.IsActive = entity.IsActive;

            if (!entity.ModifiedOn.IsDefault())
                SharjahAwardForTranslation.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiSharjahAwardForTranslation entity)
        {
            foreach (XsiSharjahAwardForTranslation SharjahAwardForTranslation in Select(entity))
            {
                XsiSharjahAwardForTranslation aSharjahAwardForTranslation = XsiContext.Context.XsiSharjahAwardForTranslation.Find(SharjahAwardForTranslation.ItemId);
                XsiContext.Context.XsiSharjahAwardForTranslation.Remove(aSharjahAwardForTranslation);
            }
        }
        public void Delete(long itemId)
        {
            XsiSharjahAwardForTranslation aSharjahAwardForTranslation = XsiContext.Context.XsiSharjahAwardForTranslation.Find(itemId);
            XsiContext.Context.XsiSharjahAwardForTranslation.Remove(aSharjahAwardForTranslation);
        }
        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class VideoCategoryRepository : IVideoCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public VideoCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public VideoCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public VideoCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiVideoCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiVideoCategory.Find(itemId);
        }

        public List<XsiVideoCategory> Select()
        {
            return XsiContext.Context.XsiVideoCategory.ToList();
        }
        public List<XsiVideoCategory> Select(XsiVideoCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiVideoCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.DescriptionAr.IsDefault())
                predicate = predicate.And(p => p.DescriptionAr.Contains(entity.DescriptionAr));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiVideoCategory.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiVideoCategory> SelectOr(XsiVideoCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiVideoCategory>();
            var Outer = PredicateBuilder.True<XsiVideoCategory>();

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.TitleAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                    || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                    || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsActiveAr.IsDefault())
                Outer = Outer.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiVideoCategory.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiVideoCategory Add(XsiVideoCategory entity)
        {
            XsiVideoCategory VideoCategory = new XsiVideoCategory();
            if (!entity.Title.IsDefault())
                VideoCategory.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
            VideoCategory.Description = entity.Description.Trim();
            VideoCategory.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                VideoCategory.TitleAr = entity.TitleAr.Trim();
            if (!entity.DescriptionAr.IsDefault())
                VideoCategory.DescriptionAr = entity.DescriptionAr.Trim();
            VideoCategory.IsActiveAr = entity.IsActiveAr;

            VideoCategory.CreatedOn = entity.CreatedOn;
            VideoCategory.CreatedBy = entity.CreatedBy;
            VideoCategory.ModifiedOn = entity.ModifiedOn;
            VideoCategory.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiVideoCategory.Add(VideoCategory);
            SubmitChanges();
            return VideoCategory;

        }
        public void Update(XsiVideoCategory entity)
        {
            XsiVideoCategory VideoCategory = XsiContext.Context.XsiVideoCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(VideoCategory).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                VideoCategory.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                VideoCategory.Description = entity.Description.Trim();

            if (!entity.IsActive.IsDefault())
                VideoCategory.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                VideoCategory.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                VideoCategory.DescriptionAr = entity.DescriptionAr.Trim();

            if (!entity.IsActiveAr.IsDefault())
                VideoCategory.IsActiveAr = entity.IsActiveAr;

            if (!entity.ModifiedOn.IsDefault())
                VideoCategory.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                VideoCategory.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiVideoCategory entity)
        {
            foreach (XsiVideoCategory VideoCategory in Select(entity))
            {
                XsiVideoCategory aVideoCategory = XsiContext.Context.XsiVideoCategory.Find(VideoCategory.ItemId);
                XsiContext.Context.XsiVideoCategory.Remove(aVideoCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiVideoCategory aVideoCategory = XsiContext.Context.XsiVideoCategory.Find(itemId);
            XsiContext.Context.XsiVideoCategory.Remove(aVideoCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
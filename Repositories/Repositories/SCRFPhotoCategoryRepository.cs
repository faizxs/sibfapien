﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFPhotoCategoryRepository : ISCRFPhotoCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFPhotoCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFPhotoCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFPhotoCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfphotoCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfphotoCategory.Find(itemId);
        }
        
        public List<XsiScrfphotoCategory> Select()
        {
            return XsiContext.Context.XsiScrfphotoCategory.ToList();
        }
        public List<XsiScrfphotoCategory> Select(XsiScrfphotoCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfphotoCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiScrfphotoCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiScrfphotoCategory> SelectOtherLanguageCategory(XsiScrfphotoCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfphotoCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            //var GroupId = XsiContext.Context.XsiScrfphotoCategory.AsExpandable().Where(predicate).Single().GroupId.Value;
            predicate = PredicateBuilder.True<XsiScrfphotoCategory>();
            
            return XsiContext.Context.XsiScrfphotoCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiScrfphotoCategory> SelectOr(XsiScrfphotoCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfphotoCategory>();
            var Outer = PredicateBuilder.True<XsiScrfphotoCategory>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
             
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiScrfphotoCategory.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiScrfphotoCategory> SelectCurrentLanguageCategory(XsiScrfphotoCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfphotoCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            //var GroupId = XsiContext.Context.XsiScrfphotoCategory.AsExpandable().Where(predicate).Single().GroupId.Value;
            predicate = PredicateBuilder.True<XsiScrfphotoCategory>();
            
            return XsiContext.Context.XsiScrfphotoCategory.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfphotoCategory Add(XsiScrfphotoCategory entity)
        {
            XsiScrfphotoCategory PhotoCategory = new XsiScrfphotoCategory();
           
            PhotoCategory.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
            PhotoCategory.Title = entity.Title.Trim();
            if (!entity.IsActiveAr.IsDefault())
                PhotoCategory.IsActiveAr = entity.IsActiveAr;
            if (!entity.TitleAr.IsDefault())
                PhotoCategory.TitleAr = entity.TitleAr.Trim();
            PhotoCategory.CreatedOn = entity.CreatedOn;
            PhotoCategory.CreatedBy = entity.CreatedBy;
            PhotoCategory.ModifiedOn = entity.ModifiedOn;
            PhotoCategory.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfphotoCategory.Add(PhotoCategory);
            SubmitChanges();
            return PhotoCategory;

        }
        public void Update(XsiScrfphotoCategory entity)
        {
            XsiScrfphotoCategory PhotoCategory = XsiContext.Context.XsiScrfphotoCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(PhotoCategory).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                PhotoCategory.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                PhotoCategory.Title = entity.Title.Trim();

            if (!entity.IsActiveAr.IsDefault())
                PhotoCategory.IsActiveAr = entity.IsActiveAr;
            if (!entity.TitleAr.IsDefault())
                PhotoCategory.TitleAr = entity.TitleAr.Trim();

            if (!entity.ModifiedOn.IsDefault())
                PhotoCategory.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                PhotoCategory.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiScrfphotoCategory entity)
        {
            foreach (XsiScrfphotoCategory PhotoCategory in Select(entity))
            {
                XsiScrfphotoCategory aPhotoCategory = XsiContext.Context.XsiScrfphotoCategory.Find(PhotoCategory.ItemId);
                XsiContext.Context.XsiScrfphotoCategory.Remove(aPhotoCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfphotoCategory aPhotoCategory = XsiContext.Context.XsiScrfphotoCategory.Find(itemId);
            XsiContext.Context.XsiScrfphotoCategory.Remove(aPhotoCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
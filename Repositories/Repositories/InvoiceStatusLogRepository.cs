﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class InvoiceStatusLogRepository : IInvoiceStatusLogRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public InvoiceStatusLogRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public InvoiceStatusLogRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public InvoiceStatusLogRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiInvoiceStatusLog GetById(long itemId)
        {
            return XsiContext.Context.XsiInvoiceStatusLog.Find(itemId);
        }
        public List<XsiInvoiceStatusLog> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiInvoiceStatusLog> Select()
        {
            return XsiContext.Context.XsiInvoiceStatusLog.ToList();
        }
        public List<XsiInvoiceStatusLog> Select(XsiInvoiceStatusLog entity)
        {
            var predicate = PredicateBuilder.True<XsiInvoiceStatusLog>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.InvoiceId.IsDefault())
                predicate = predicate.And(p => p.InvoiceId == entity.InvoiceId);

            if (!entity.ExhibitorId.IsDefault())
                predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

            if (!entity.AdminId.IsDefault())
                predicate = predicate.And(p => p.AdminId == entity.AdminId);

            if (!entity.GalaxyTeamId.IsDefault())
                predicate = predicate.And(p => p.GalaxyTeamId == entity.GalaxyTeamId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);


            return XsiContext.Context.XsiInvoiceStatusLog.AsExpandable().Where(predicate).ToList();
        }

        public XsiInvoiceStatusLog Add(XsiInvoiceStatusLog entity)
        {
            XsiInvoiceStatusLog InvoiceStatusLog = new XsiInvoiceStatusLog();
            InvoiceStatusLog.ExhibitorId = entity.ExhibitorId;
            InvoiceStatusLog.AdminId = entity.AdminId;
            InvoiceStatusLog.InvoiceId = entity.InvoiceId;
            if (!entity.GalaxyTeamId.IsDefault())
                InvoiceStatusLog.GalaxyTeamId = entity.GalaxyTeamId;
            InvoiceStatusLog.Status = entity.Status;
            InvoiceStatusLog.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiInvoiceStatusLog.Add(InvoiceStatusLog);
            SubmitChanges();
            return InvoiceStatusLog;

        }
        public void Update(XsiInvoiceStatusLog entity)
        {
            XsiInvoiceStatusLog InvoiceStatusLog = XsiContext.Context.XsiInvoiceStatusLog.Find(entity.ItemId);
            XsiContext.Context.Entry(InvoiceStatusLog).State = EntityState.Modified;

            if (!entity.Status.IsDefault())
                InvoiceStatusLog.Status = entity.Status;
        }
        public void Delete(XsiInvoiceStatusLog entity)
        {
            foreach (XsiInvoiceStatusLog InvoiceStatusLog in Select(entity))
            {
                XsiInvoiceStatusLog aInvoiceStatusLog = XsiContext.Context.XsiInvoiceStatusLog.Find(InvoiceStatusLog.ItemId);
                XsiContext.Context.XsiInvoiceStatusLog.Remove(aInvoiceStatusLog);
            }
        }
        public void Delete(long itemId)
        {
            XsiInvoiceStatusLog aInvoiceStatusLog = XsiContext.Context.XsiInvoiceStatusLog.Find(itemId);
            XsiContext.Context.XsiInvoiceStatusLog.Remove(aInvoiceStatusLog);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
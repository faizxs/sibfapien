﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using LinqKit;
using System.Data;
using Entities.Models;

namespace Xsi.Repositories
{
    public class AdminUserLogsRepository : IAdminLogRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public AdminUserLogsRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public AdminUserLogsRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public AdminUserLogsRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdminLogs GetById(long itemId)
        {
            return XsiContext.Context.XsiAdminLogs.Find(itemId);
        }
        public List<XsiAdminLogs> GetByGroupId(long groupId)
        {
            return null;
        }
        public XsiAdminLogs GetLogLastLogin(long userId)
        {
            IList<XsiAdminLogs> list = XsiContext.Context.XsiAdminLogs.Where(p => p.UserId == userId).OrderByDescending(p => p.LoggedOn).ToList();
            if (list.Count > 1)
                return list.Skip(1).FirstOrDefault();
            else
                return list.FirstOrDefault();
        }

        public List<XsiAdminLogs> Select()
        {
            return XsiContext.Context.XsiAdminLogs.ToList();
        }
        public List<XsiAdminLogs> Select(XsiAdminLogs entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminLogs>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.UserId.IsDefault())
                predicate = predicate.And(p => p.UserId == entity.UserId);

            if (!entity.Username.IsDefault())
                predicate = predicate.And(p => p.Username == entity.Username);

            if (!entity.LoggedOn.IsDefault())
                predicate = predicate.And(p => p.LoggedOn == entity.LoggedOn);

            if (!entity.Ipaddress.IsDefault())
                predicate = predicate.And(p => p.Ipaddress == entity.Ipaddress);

            return XsiContext.Context.XsiAdminLogs.AsExpandable().Where(predicate).ToList();
        }

        public XsiAdminLogs Add(XsiAdminLogs entity)
        {
            XsiAdminLogs AdminLogs = new XsiAdminLogs();
            AdminLogs.UserId = entity.UserId;
            if (!entity.Username.IsDefault())
                AdminLogs.Username = entity.Username.Trim();
            AdminLogs.LoggedOn = entity.LoggedOn;
            if (!entity.Ipaddress.IsDefault())
                AdminLogs.Ipaddress = entity.Ipaddress.Trim();

            XsiContext.Context.XsiAdminLogs.Add(AdminLogs);

            SubmitChanges();

            return AdminLogs;
        }
        public void Update(XsiAdminLogs entity)
        {
            XsiAdminLogs AdminLogs = XsiContext.Context.XsiAdminLogs.Find(entity.ItemId);
            XsiContext.Context.Entry(AdminLogs).State = EntityState.Modified;

            if (!entity.UserId.IsDefault())
                AdminLogs.UserId = entity.UserId;

            if (!entity.Username.IsDefault())
                AdminLogs.Username = entity.Username.Trim();

            if (!entity.LoggedOn.IsDefault())
                AdminLogs.LoggedOn = entity.LoggedOn;

            if (!entity.Ipaddress.IsDefault())
                AdminLogs.Ipaddress = entity.Ipaddress.Trim();
        }
        public void Delete(XsiAdminLogs entity)
        {
            foreach (XsiAdminLogs AdminLog in Select(entity))
            {
                XsiAdminLogs alog = XsiContext.Context.XsiAdminLogs.Find(AdminLog.ItemId);
                XsiContext.Context.XsiAdminLogs.Remove(alog);
            }
        }
        public void Delete(long itemId)
        {
            XsiAdminLogs alog = XsiContext.Context.XsiAdminLogs.Find(itemId);
            XsiContext.Context.XsiAdminLogs.Remove(alog);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}

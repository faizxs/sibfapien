﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ReasonRepository : IReasonRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ReasonRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ReasonRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ReasonRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiReason GetById(long itemId)
        {
            return XsiContext.Context.XsiReason.Find(itemId);
        }
         
        public List<XsiReason> Select()
        {
            return XsiContext.Context.XsiReason.ToList();
        }
        public List<XsiReason> Select(XsiReason entity)
        {
            var predicate = PredicateBuilder.True<XsiReason>();
            if (!entity.ReasonId.IsDefault())
                predicate = predicate.And(p => p.ReasonId == entity.ReasonId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));
 
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiReason.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiReason> SelectOr(XsiReason entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiReason>();
            var Outer = PredicateBuilder.True<XsiReason>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
             
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiReason.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiReason Add(XsiReason entity)
        {
            XsiReason Reason = new XsiReason();
            
            if (!entity.Title.IsDefault())
            Reason.Title = entity.Title.Trim();
            Reason.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                Reason.TitleAr = entity.TitleAr.Trim();
            Reason.IsActiveAr = entity.IsActiveAr;
            #endregion Ar
            Reason.CreatedOn = entity.CreatedOn;
            Reason.CreatedBy = entity.CreatedBy;
            Reason.ModifiedOn = entity.ModifiedOn;
            Reason.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiReason.Add(Reason);
            SubmitChanges();
            return Reason;

        }
        public void Update(XsiReason entity)
        {
            XsiReason Reason = XsiContext.Context.XsiReason.Find(entity.ReasonId);
            XsiContext.Context.Entry(Reason).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                Reason.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                Reason.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                Reason.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                Reason.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                Reason.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                Reason.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiReason entity)
        {
            foreach (XsiReason Reason in Select(entity))
            {
                XsiReason aReason = XsiContext.Context.XsiReason.Find(Reason.ReasonId);
                XsiContext.Context.XsiReason.Remove(aReason);
            }
        }
        public void Delete(long itemId)
        {
            XsiReason aReason = XsiContext.Context.XsiReason.Find(itemId);
            XsiContext.Context.XsiReason.Remove(aReason);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
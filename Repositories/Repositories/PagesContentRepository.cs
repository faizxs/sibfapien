﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PagesContentRepository : IPagesContentRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PagesContentRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PagesContentRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PagesContentRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiPagesContentNew GetById(long itemId)
        {
            return XsiContext.Context.XsiPagesContentNew.Find(itemId);
        }

        public List<XsiPagesContentNew> Select()
        {
            return XsiContext.Context.XsiPagesContentNew.ToList();
        }

        public List<XsiPagesContentNew> SelectOr(XsiPagesContentNew entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiPagesContentNew>();
            var Outer = PredicateBuilder.True<XsiPagesContentNew>();

            if (!entity.PageTitle.IsDefault())
            {

                string strSearchText = entity.PageTitle;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.PageTitle.Contains(searchKeys.str1) || p.PageTitle.Contains(searchKeys.str2) || p.PageTitle.Contains(searchKeys.str3) || p.PageTitle.Contains(searchKeys.str4) || p.PageTitle.Contains(searchKeys.str5)
                     || p.PageTitle.Contains(searchKeys.str6) || p.PageTitle.Contains(searchKeys.str7) || p.PageTitle.Contains(searchKeys.str8) || p.PageTitle.Contains(searchKeys.str9) || p.PageTitle.Contains(searchKeys.str10)
                     || p.PageTitle.Contains(searchKeys.str11) || p.PageTitle.Contains(searchKeys.str12) || p.PageTitle.Contains(searchKeys.str13)
                     || p.PageTitle.Contains(searchKeys.str14) || p.PageTitle.Contains(searchKeys.str15) || p.PageTitle.Contains(searchKeys.str16) || p.PageTitle.Contains(searchKeys.str17)
                     );
                Inner = Inner.Or(p => p.PageSubTitle.Contains(searchKeys.str1) || p.PageSubTitle.Contains(searchKeys.str2) || p.PageSubTitle.Contains(searchKeys.str3) || p.PageSubTitle.Contains(searchKeys.str4) || p.PageSubTitle.Contains(searchKeys.str5)
                     || p.PageSubTitle.Contains(searchKeys.str6) || p.PageSubTitle.Contains(searchKeys.str7) || p.PageSubTitle.Contains(searchKeys.str8) || p.PageSubTitle.Contains(searchKeys.str9) || p.PageSubTitle.Contains(searchKeys.str10)
                     || p.PageSubTitle.Contains(searchKeys.str11) || p.PageSubTitle.Contains(searchKeys.str12) || p.PageSubTitle.Contains(searchKeys.str13)
                     || p.PageSubTitle.Contains(searchKeys.str14) || p.PageSubTitle.Contains(searchKeys.str15) || p.PageSubTitle.Contains(searchKeys.str16) || p.PageSubTitle.Contains(searchKeys.str17)
                     );
                Inner = Inner.Or(p => p.PageContent.Contains(searchKeys.str1) || p.PageContent.Contains(searchKeys.str2) || p.PageContent.Contains(searchKeys.str3) || p.PageContent.Contains(searchKeys.str4) || p.PageContent.Contains(searchKeys.str5)
                     || p.PageContent.Contains(searchKeys.str6) || p.PageContent.Contains(searchKeys.str7) || p.PageContent.Contains(searchKeys.str8) || p.PageContent.Contains(searchKeys.str9) || p.PageContent.Contains(searchKeys.str10)
                     || p.PageContent.Contains(searchKeys.str11) || p.PageContent.Contains(searchKeys.str12) || p.PageContent.Contains(searchKeys.str13)
                     || p.PageContent.Contains(searchKeys.str14) || p.PageContent.Contains(searchKeys.str15) || p.PageContent.Contains(searchKeys.str16) || p.PageContent.Contains(searchKeys.str17)
                     );
                isInner = true;
            }
            else
            {
                if (!entity.PageTitle.IsDefault())
                {
                    string strSearchText = entity.PageTitle.ToLower();

                    if (!entity.PageTitle.IsDefault())
                        Inner = Inner.Or(p => p.PageTitle.ToLower().Contains(strSearchText));

                    Inner = Inner.Or(p => p.PageTitle.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsDynamic.IsDefault())
                Outer = Outer.And(p => p.IsDynamic == entity.IsDynamic);

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiPagesContentNew.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiPagesContentNew> Select(XsiPagesContentNew entity)
        {
            var predicate = PredicateBuilder.True<XsiPagesContentNew>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.HierarchicalId.IsDefault())
                predicate = predicate.And(p => p.HierarchicalId == entity.HierarchicalId);

            if (!entity.PageTitle.IsDefault())
                predicate = predicate.And(p => p.PageTitle.Contains(entity.PageTitle));

            if (!entity.PageSubTitle.IsDefault())
                predicate = predicate.And(p => p.PageSubTitle.Contains(entity.PageSubTitle));

            if (!entity.PageContent.IsDefault())
                predicate = predicate.And(p => p.PageContent.Contains(entity.PageContent));

            if (!entity.PageSubContent.IsDefault())
                predicate = predicate.And(p => p.PageSubContent.Contains(entity.PageSubContent));

            if (!entity.PageSubSubContent.IsDefault())
                predicate = predicate.And(p => p.PageSubSubContent.Contains(entity.PageSubSubContent));

            if (!entity.HeaderImage.IsDefault())
                predicate = predicate.And(p => p.HeaderImage == entity.HeaderImage);

            if (!entity.InnerBanner.IsDefault())
                predicate = predicate.And(p => p.InnerBanner == entity.InnerBanner);

            if (!entity.ExplorerTitle.IsDefault())
                predicate = predicate.And(p => p.ExplorerTitle.Contains(entity.ExplorerTitle));

            if (!entity.PageMetaKeywords.IsDefault())
                predicate = predicate.And(p => p.PageMetaKeywords.Contains(entity.PageMetaKeywords));

            if (!entity.PageMetaDescription.IsDefault())
                predicate = predicate.And(p => p.PageMetaDescription.Contains(entity.PageMetaDescription));

            if (!entity.IsDynamic.IsDefault())
                predicate = predicate.And(p => p.IsDynamic == entity.IsDynamic);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive == entity.IsActive);

            if (!entity.RollbackXml.IsDefault())
                predicate = predicate.And(p => p.RollbackXml.Contains(entity.RollbackXml));


            return XsiContext.Context.XsiPagesContentNew.AsExpandable().Where(predicate).ToList();
        }
        /*public List<sp_FetchSIBFSearchResults_Result> SelectSearch(string searchString, long languageId)
        {
            return XsiContext.Context.sp_FetchSIBFSearchResults(searchString, languageId).AsQueryable().ToList();
        }*/

        public XsiPagesContentNew Add(XsiPagesContentNew entity)
        {
            XsiPagesContentNew PageContent = new XsiPagesContentNew();
            PageContent.HierarchicalId = entity.HierarchicalId;
            if (!entity.PageTitle.IsDefault())
                PageContent.PageTitle = entity.PageTitle.Trim();
            if (!entity.PageSubTitle.IsDefault())
                PageContent.PageSubTitle = entity.PageSubTitle.Trim();
            if (!entity.PageContent.IsDefault())
                PageContent.PageContent = entity.PageContent.Trim();
            if (!entity.PageSubContent.IsDefault())
                PageContent.PageSubContent = entity.PageSubContent.Trim();
            if (!entity.PageSubSubContent.IsDefault())
                PageContent.PageSubSubContent = entity.PageSubSubContent.Trim();
            PageContent.HeaderImage = entity.HeaderImage;
            PageContent.InnerBanner = entity.InnerBanner;
            if (!entity.ExplorerTitle.IsDefault())
                PageContent.ExplorerTitle = entity.ExplorerTitle.Trim();
            if (!entity.PageMetaKeywords.IsDefault())
                PageContent.PageMetaKeywords = entity.PageMetaKeywords.Trim();
            if (!entity.PageMetaDescription.IsDefault())
                PageContent.PageMetaDescription = entity.PageMetaDescription.Trim();
            PageContent.IsDynamic = entity.IsDynamic;
            PageContent.IsActive = entity.IsActive;
            PageContent.RollbackXml = entity.RollbackXml;

            XsiContext.Context.XsiPagesContentNew.Add(PageContent);
            SubmitChanges();
            return PageContent;

        }
        public void Update(XsiPagesContentNew entity)
        {
            XsiPagesContentNew PageContent = XsiContext.Context.XsiPagesContentNew.Find(entity.ItemId);
            XsiContext.Context.Entry(PageContent).State = EntityState.Modified;

            if (!entity.HierarchicalId.IsDefault())
                PageContent.HierarchicalId = entity.HierarchicalId;

            if (!entity.PageTitle.IsDefault())
                PageContent.PageTitle = entity.PageTitle.Trim();

            if (!entity.PageSubTitle.IsDefault())
                PageContent.PageSubTitle = entity.PageSubTitle.Trim();

            if (!entity.PageContent.IsDefault())
                PageContent.PageContent = entity.PageContent.Trim();

            if (!entity.PageSubContent.IsDefault())
                PageContent.PageSubContent = entity.PageSubContent.Trim();

            if (!entity.PageSubSubContent.IsDefault())
                PageContent.PageSubSubContent = entity.PageSubSubContent.Trim();

            if (!entity.HeaderImage.IsDefault())
                PageContent.HeaderImage = entity.HeaderImage;

            if (!entity.InnerBanner.IsDefault())
                PageContent.InnerBanner = entity.InnerBanner;

            if (!entity.ExplorerTitle.IsDefault())
                PageContent.ExplorerTitle = entity.ExplorerTitle.Trim();

            if (!entity.PageMetaKeywords.IsDefault())
                PageContent.PageMetaKeywords = entity.PageMetaKeywords.Trim();

            if (!entity.PageMetaDescription.IsDefault())
                PageContent.PageMetaDescription = entity.PageMetaDescription.Trim();

            if (!entity.IsDynamic.IsDefault())
                PageContent.IsDynamic = entity.IsDynamic;

            if (!entity.IsActive.IsDefault())
                PageContent.IsActive = entity.IsActive;

            if (!entity.RollbackXml.IsDefault())
                PageContent.RollbackXml = entity.RollbackXml;

        }
        public void Delete(XsiPagesContentNew entity)
        {
            foreach (XsiPagesContentNew PageContent in Select(entity))
            {
                XsiPagesContentNew aPageContent = XsiContext.Context.XsiPagesContentNew.Find(PageContent.ItemId);
                XsiContext.Context.XsiPagesContentNew.Remove(aPageContent);
            }
        }
        public void Delete(long itemId)
        {
            XsiPagesContentNew aPageContent = XsiContext.Context.XsiPagesContentNew.Find(itemId);
            XsiContext.Context.XsiPagesContentNew.Remove(aPageContent);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
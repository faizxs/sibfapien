﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class IconsForSponsorRepository : IIconsForSponsorRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public IconsForSponsorRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public IconsForSponsorRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public IconsForSponsorRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiIconsForSponsor GetById(long itemId)
        {
            return XsiContext.Context.XsiIconsForSponsor.Find(itemId);
        }
        public List<XsiIconsForSponsor> Select()
        {
            return XsiContext.Context.XsiIconsForSponsor.ToList();
        }
        public List<XsiIconsForSponsor> Select(XsiIconsForSponsor entity)
        {
            var predicate = PredicateBuilder.True<XsiIconsForSponsor>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Contains(entity.ImageName));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.OverviewAr.IsDefault())
                predicate = predicate.And(p => p.OverviewAr.Contains(entity.OverviewAr));

            if (!entity.ImageNameAr.IsDefault())
                predicate = predicate.And(p => p.ImageNameAr.Contains(entity.ImageNameAr));

            if (!entity.Urlar.IsDefault())
                predicate = predicate.And(p => p.Urlar.Contains(entity.Urlar));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            return XsiContext.Context.XsiIconsForSponsor.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiIconsForSponsor> SelectOr(XsiIconsForSponsor entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiIconsForSponsor>();
            var Outer = PredicateBuilder.True<XsiIconsForSponsor>();

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.TitleAr.Contains(searchKeys.str1) || p.TitleAr.Contains(searchKeys.str2) || p.TitleAr.Contains(searchKeys.str3) || p.TitleAr.Contains(searchKeys.str4) || p.TitleAr.Contains(searchKeys.str5)
                    || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                    || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                    || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.Overview.IsDefault())
                Outer = Outer.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.ImageName.IsDefault())
                Outer = Outer.And(p => p.ImageName.Contains(entity.ImageName));

            if (!entity.Url.IsDefault())
                Outer = Outer.And(p => p.Url.Contains(entity.Url));

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.OverviewAr.IsDefault())
                Outer = Outer.And(p => p.OverviewAr.Contains(entity.OverviewAr));

            if (!entity.ImageNameAr.IsDefault())
                Outer = Outer.And(p => p.ImageNameAr.Contains(entity.ImageNameAr));

            if (!entity.Urlar.IsDefault())
                Outer = Outer.And(p => p.Urlar.Contains(entity.Urlar));

            if (!entity.IsActiveAr.IsDefault())
                Outer = Outer.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiIconsForSponsor.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public XsiIconsForSponsor Add(XsiIconsForSponsor entity)
        {
            XsiIconsForSponsor IconsForSponsor = new XsiIconsForSponsor();
            if (!entity.Title.IsDefault())
                IconsForSponsor.Title = entity.Title.Trim();
            if (!entity.Overview.IsDefault())
                IconsForSponsor.Overview = entity.Overview.Trim();
            IconsForSponsor.ImageName = entity.ImageName;
            if (!entity.Url.IsDefault())
                IconsForSponsor.Url = entity.Url.Trim();
            IconsForSponsor.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                IconsForSponsor.TitleAr = entity.TitleAr.Trim();
            if (!entity.OverviewAr.IsDefault())
                IconsForSponsor.OverviewAr = entity.OverviewAr.Trim();
            IconsForSponsor.ImageNameAr = entity.ImageNameAr;
            if (!entity.Urlar.IsDefault())
                IconsForSponsor.Urlar = entity.Urlar.Trim();
            IconsForSponsor.IsActiveAr = entity.IsActiveAr;

            IconsForSponsor.CreatedOn = entity.CreatedOn;
            IconsForSponsor.CreatedBy = entity.CreatedBy;
            IconsForSponsor.ModifiedOn = entity.ModifiedOn;
            IconsForSponsor.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiIconsForSponsor.Add(IconsForSponsor);
            SubmitChanges();
            return IconsForSponsor;

        }
        public void Update(XsiIconsForSponsor entity)
        {
            XsiIconsForSponsor IconsForSponsor = XsiContext.Context.XsiIconsForSponsor.Find(entity.ItemId);
            XsiContext.Context.Entry(IconsForSponsor).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                IconsForSponsor.Title = entity.Title.Trim();
            if (!entity.Overview.IsDefault())
                IconsForSponsor.Overview = entity.Overview.Trim();
            if (!entity.ImageName.IsDefault())
                IconsForSponsor.ImageName = entity.ImageName;
            if (!entity.Url.IsDefault())
                IconsForSponsor.Url = entity.Url.Trim();
            if (!entity.IsActive.IsDefault())
                IconsForSponsor.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                IconsForSponsor.TitleAr = entity.TitleAr.Trim();
            if (!entity.OverviewAr.IsDefault())
                IconsForSponsor.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.ImageNameAr.IsDefault())
                IconsForSponsor.ImageNameAr = entity.ImageNameAr;
            if (!entity.Urlar.IsDefault())
                IconsForSponsor.Urlar = entity.Urlar.Trim();
            if (!entity.IsActiveAr.IsDefault())
                IconsForSponsor.IsActiveAr = entity.IsActiveAr;

            if (!entity.ModifiedOn.IsDefault())
                IconsForSponsor.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                IconsForSponsor.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiIconsForSponsor entity)
        {
            foreach (XsiIconsForSponsor IconsForSponsor in Select(entity))
            {
                XsiIconsForSponsor aIconsForSponsor = XsiContext.Context.XsiIconsForSponsor.Find(IconsForSponsor.ItemId);
                XsiContext.Context.XsiIconsForSponsor.Remove(aIconsForSponsor);
            }
        }
        public void Delete(long itemId)
        {
            XsiIconsForSponsor aIconsForSponsor = XsiContext.Context.XsiIconsForSponsor.Find(itemId);
            XsiContext.Context.XsiIconsForSponsor.Remove(aIconsForSponsor);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
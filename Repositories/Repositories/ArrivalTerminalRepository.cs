﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ArrivalTerminalRepository : IArrivalTerminalRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ArrivalTerminalRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ArrivalTerminalRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ArrivalTerminalRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiArrivalTerminal GetById(long itemId)
        {
            return XsiContext.Context.XsiArrivalTerminal.Find(itemId);
        }
       
        public List<XsiArrivalTerminal> Select()
        {
            return XsiContext.Context.XsiArrivalTerminal.ToList();
        }
        public List<XsiArrivalTerminal> Select(XsiArrivalTerminal entity)
        {
            var predicate = PredicateBuilder.True<XsiArrivalTerminal>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiArrivalTerminal.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiArrivalTerminal> SelectComeplete(XsiArrivalTerminal entity)
        {
            var predicate = PredicateBuilder.True<XsiArrivalTerminal>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiArrivalTerminal.Include(p => p.Category).AsExpandable().Where(predicate).ToList();
        }

        public XsiArrivalTerminal Add(XsiArrivalTerminal entity)
        {
            XsiArrivalTerminal ArrivalTerminal = new XsiArrivalTerminal();
            
            ArrivalTerminal.CategoryId = entity.CategoryId;
            if (!entity.Title.IsDefault())
                ArrivalTerminal.Title = entity.Title.Trim();
            ArrivalTerminal.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ArrivalTerminal.TitleAr = entity.TitleAr.Trim();
            ArrivalTerminal.IsActiveAr = entity.IsActiveAr;
            #endregion Ar
            ArrivalTerminal.CreatedOn = entity.CreatedOn;
            ArrivalTerminal.CreatedBy = entity.CreatedBy;
            ArrivalTerminal.ModifiedOn = entity.ModifiedOn;
            ArrivalTerminal.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiArrivalTerminal.Add(ArrivalTerminal);
            SubmitChanges();
            return ArrivalTerminal;

        }
        public void Update(XsiArrivalTerminal entity)
        {
            XsiArrivalTerminal ArrivalTerminal = XsiContext.Context.XsiArrivalTerminal.Find(entity.ItemId);
            XsiContext.Context.Entry(ArrivalTerminal).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ArrivalTerminal.Title = entity.Title.Trim();

            if (!entity.IsActive.IsDefault())
                ArrivalTerminal.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ArrivalTerminal.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                ArrivalTerminal.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                ArrivalTerminal.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ArrivalTerminal.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiArrivalTerminal entity)
        {
            foreach (XsiArrivalTerminal ArrivalTerminal in Select(entity))
            {
                XsiArrivalTerminal aArrivalTerminal = XsiContext.Context.XsiArrivalTerminal.Find(ArrivalTerminal.ItemId);
                XsiContext.Context.XsiArrivalTerminal.Remove(aArrivalTerminal);
            }
        }
        public void Delete(long itemId)
        {
            XsiArrivalTerminal aArrivalTerminal = XsiContext.Context.XsiArrivalTerminal.Find(itemId);
            XsiContext.Context.XsiArrivalTerminal.Remove(aArrivalTerminal);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
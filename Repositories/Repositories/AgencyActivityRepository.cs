﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class AgencyActivityRepository : IAgencyActivityRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public AgencyActivityRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public AgencyActivityRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public AgencyActivityRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiMemberExhibitionActivityYearly GetById(long itemId)
        {
            return XsiContext.Context.XsiMemberExhibitionActivityYearly.Find(itemId);
        }

        public List<XsiMemberExhibitionActivityYearly> Select()
        {
            return XsiContext.Context.XsiMemberExhibitionActivityYearly.ToList();
        }
        public List<XsiMemberExhibitionActivityYearly> Select(XsiMemberExhibitionActivityYearly entity)
        {
            var predicate = PredicateBuilder.True<XsiMemberExhibitionActivityYearly>();
            if (!entity.ActivityId.IsDefault())
                predicate = predicate.And(p => p.ActivityId == entity.ActivityId);

            if (!entity.MemberExhibitionYearlyId.IsDefault())
                predicate = predicate.And(p => p.MemberExhibitionYearlyId == entity.MemberExhibitionYearlyId);

            return XsiContext.Context.XsiMemberExhibitionActivityYearly.AsExpandable().Where(predicate).ToList();
        }

        public XsiMemberExhibitionActivityYearly Add(XsiMemberExhibitionActivityYearly entity)
        {
            XsiMemberExhibitionActivityYearly AgencyActivity = new XsiMemberExhibitionActivityYearly();
            AgencyActivity.ActivityId = entity.ActivityId;
            AgencyActivity.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;
            XsiContext.Context.XsiMemberExhibitionActivityYearly.Add(AgencyActivity);
            SubmitChanges();
            return AgencyActivity;
        }
        public void Update(XsiMemberExhibitionActivityYearly entity)
        {
            XsiMemberExhibitionActivityYearly AgencyActivity = XsiContext.Context.XsiMemberExhibitionActivityYearly.Find(entity.MemberExhibitionYearlyId);
            XsiContext.Context.Entry(AgencyActivity).State = EntityState.Modified;

            if (!entity.ActivityId.IsDefault())
                AgencyActivity.ActivityId = entity.ActivityId;

            if (!entity.MemberExhibitionYearlyId.IsDefault())
                AgencyActivity.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;
        }
        public void Delete(XsiMemberExhibitionActivityYearly entity)
        {
            foreach (XsiMemberExhibitionActivityYearly AgencyActivity in Select(entity))
            {
                XsiMemberExhibitionActivityYearly aAgencyActivity = XsiContext.Context.XsiMemberExhibitionActivityYearly.Find(AgencyActivity.MemberExhibitionYearlyId);
                XsiContext.Context.XsiMemberExhibitionActivityYearly.Remove(aAgencyActivity);
            }
        }
        public void Delete(long itemId)
        {
            XsiMemberExhibitionActivityYearly aAgencyActivity = XsiContext.Context.XsiMemberExhibitionActivityYearly.Find(itemId);
            XsiContext.Context.XsiMemberExhibitionActivityYearly.Remove(aAgencyActivity);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
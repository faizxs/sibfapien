﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFVideoAlbumRepository : ISCRFVideoAlbumRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFVideoAlbumRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFVideoAlbumRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFVideoAlbumRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfvideoAlbum GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfvideoAlbum.Find(itemId);
        }
        
        public List<XsiScrfvideoAlbum> Select()
        {
            return XsiContext.Context.XsiScrfvideoAlbum.ToList();
        }
        public List<XsiScrfvideoAlbum> Select(XsiScrfvideoAlbum entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfvideoAlbum>();
            var predicate = PredicateBuilder.True<XsiScrfvideoAlbum>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);
             
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated == entity.Dated);

            if (!entity.SortIndex.IsDefault())
                predicate = predicate.And(p => p.SortIndex == entity.SortIndex);

            if (!entity.DatedAr.IsDefault())
                predicate = predicate.And(p => p.DatedAr == entity.DatedAr);

            if (!entity.SortIndexAr.IsDefault())
                predicate = predicate.And(p => p.SortIndexAr == entity.SortIndexAr);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiScrfvideoAlbum.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfvideoAlbum Add(XsiScrfvideoAlbum entity)
        {
            XsiScrfvideoAlbum VideoAlbum = new XsiScrfvideoAlbum();
            VideoAlbum.CategoryId = entity.CategoryId;
            
            VideoAlbum.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                VideoAlbum.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
            VideoAlbum.Description = entity.Description.Trim();
            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                VideoAlbum.IsActiveAr = entity.IsActiveAr;

            if (!entity.TitleAr.IsDefault())
                VideoAlbum.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                VideoAlbum.DescriptionAr = entity.DescriptionAr.Trim();
            #endregion Ar

            VideoAlbum.Dated = entity.Dated;
            VideoAlbum.SortIndex = entity.SortIndex;
            VideoAlbum.DatedAr = entity.DatedAr;
            VideoAlbum.SortIndexAr = entity.SortIndexAr;
            VideoAlbum.CreatedOn = entity.CreatedOn;
            VideoAlbum.CreatedBy = entity.CreatedBy;
            VideoAlbum.ModifiedOn = entity.ModifiedOn;
            VideoAlbum.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfvideoAlbum.Add(VideoAlbum);
            SubmitChanges();
            return VideoAlbum;

        }
        public void Update(XsiScrfvideoAlbum entity)
        {
            XsiScrfvideoAlbum VideoAlbum = XsiContext.Context.XsiScrfvideoAlbum.Find(entity.ItemId);
            XsiContext.Context.Entry(VideoAlbum).State = EntityState.Modified;

            if (!entity.CategoryId.IsDefault())
                VideoAlbum.CategoryId = entity.CategoryId;
             
            if (!entity.IsActive.IsDefault())
                VideoAlbum.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                VideoAlbum.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                VideoAlbum.Description = entity.Description.Trim();
            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                VideoAlbum.IsActiveAr = entity.IsActiveAr;

            if (!entity.TitleAr.IsDefault())
                VideoAlbum.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                VideoAlbum.DescriptionAr = entity.DescriptionAr.Trim();
            #endregion Ar

            if (!entity.Dated.IsDefault())
                VideoAlbum.Dated = entity.Dated;

            if (!entity.SortIndex.IsDefault())
                VideoAlbum.SortIndex = entity.SortIndex;

            if (!entity.DatedAr.IsDefault())
                VideoAlbum.DatedAr = entity.DatedAr;

            if (!entity.SortIndexAr.IsDefault())
                VideoAlbum.SortIndexAr = entity.SortIndexAr;

            if (!entity.CreatedOn.IsDefault())
                VideoAlbum.CreatedOn = entity.CreatedOn;

            if (!entity.CreatedBy.IsDefault())
                VideoAlbum.CreatedBy = entity.CreatedBy;

            if (!entity.ModifiedOn.IsDefault())
                VideoAlbum.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                VideoAlbum.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiScrfvideoAlbum entity)
        {
            foreach (XsiScrfvideoAlbum VideoAlbum in Select(entity))
            {
                XsiScrfvideoAlbum aVideoAlbum = XsiContext.Context.XsiScrfvideoAlbum.Find(VideoAlbum.ItemId);
                XsiContext.Context.XsiScrfvideoAlbum.Remove(aVideoAlbum);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfvideoAlbum aVideoAlbum = XsiContext.Context.XsiScrfvideoAlbum.Find(itemId);
            XsiContext.Context.XsiScrfvideoAlbum.Remove(aVideoAlbum);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
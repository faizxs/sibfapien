﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class EventTypeRepository : IEventTypeRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public EventTypeRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public EventTypeRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public EventTypeRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiEventType GetById(long itemId)
        {
            return XsiContext.Context.XsiEventType.Find(itemId);
        }
        public List<XsiEventType> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiEventType> Select()
        {
            return XsiContext.Context.XsiEventType.ToList();
        }
        public List<XsiEventType> Select(XsiEventType entity)
        {
            var predicate = PredicateBuilder.True<XsiEventType>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Type.IsDefault())
                predicate = predicate.And(p => p.Type.Equals(entity.Type));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);


            return XsiContext.Context.XsiEventType.AsExpandable().Where(predicate).ToList();
        }

        public XsiEventType Add(XsiEventType entity)
        {
            XsiEventType EventType = new XsiEventType();
            EventType.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                EventType.Title = entity.Title.Trim();
            EventType.Type = entity.Type;
            EventType.CreatedOn = entity.CreatedOn;
            EventType.CreatedBy = entity.CreatedBy;

            XsiContext.Context.XsiEventType.Add(EventType);
            SubmitChanges();
            return EventType;

        }
        public void Update(XsiEventType entity)
        {
            XsiEventType EventType = XsiContext.Context.XsiEventType.Find(entity.ItemId);
            XsiContext.Context.Entry(EventType).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                EventType.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                EventType.Title = entity.Title.Trim();

            if (!entity.Type.IsDefault())
                EventType.Type = entity.Type;

            if (!entity.CreatedOn.IsDefault())
                EventType.CreatedOn = entity.CreatedOn;

            if (!entity.CreatedBy.IsDefault())
                EventType.CreatedBy = entity.CreatedBy;
        }
        public void Delete(XsiEventType entity)
        {
            foreach (XsiEventType EventType in Select(entity))
            {
                XsiEventType aEventType = XsiContext.Context.XsiEventType.Find(EventType.ItemId);
                XsiContext.Context.XsiEventType.Remove(aEventType);
            }
        }
        public void Delete(long itemId)
        {
            XsiEventType aEventType = XsiContext.Context.XsiEventType.Find(itemId);
            XsiContext.Context.XsiEventType.Remove(aEventType);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
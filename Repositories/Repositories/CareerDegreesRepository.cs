﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class CareerDegreesRepository : ICareerDegreesRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public CareerDegreesRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public CareerDegreesRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public CareerDegreesRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiCareerDegrees GetById(long itemId)
        {
            return XsiContext.Context.XsiCareerDegrees.Find(itemId);
        }
        
        public List<XsiCareerDegrees> Select()
        {
            return XsiContext.Context.XsiCareerDegrees.ToList();
        }
        public List<XsiCareerDegrees> SelectOr(XsiCareerDegrees entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiCareerDegrees>();
            var Outer = PredicateBuilder.True<XsiCareerDegrees>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiCareerDegrees.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiCareerDegrees> Select(XsiCareerDegrees entity)
        {
            var predicate = PredicateBuilder.True<XsiCareerDegrees>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

             
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiCareerDegrees.AsExpandable().Where(predicate).ToList();
        }

        public XsiCareerDegrees Add(XsiCareerDegrees entity)
        {
            XsiCareerDegrees CareerDegrees = new XsiCareerDegrees();
           
            if (!entity.Title.IsDefault())
                CareerDegrees.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                CareerDegrees.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerDegrees.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerDegrees.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            CareerDegrees.CreatedOn = entity.CreatedOn;
            CareerDegrees.CreatedBy = entity.CreatedBy;
            CareerDegrees.ModifiedOn = entity.ModifiedOn;
            CareerDegrees.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiCareerDegrees.Add(CareerDegrees);
            SubmitChanges();
            return CareerDegrees;

        }
        public void Update(XsiCareerDegrees entity)
        {
            XsiCareerDegrees CareerDegrees = XsiContext.Context.XsiCareerDegrees.Find(entity.ItemId);
            XsiContext.Context.Entry(CareerDegrees).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                CareerDegrees.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                CareerDegrees.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerDegrees.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerDegrees.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                CareerDegrees.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                CareerDegrees.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiCareerDegrees entity)
        {
            foreach (XsiCareerDegrees CareerDegrees in Select(entity))
            {
                XsiCareerDegrees aCareerDegrees = XsiContext.Context.XsiCareerDegrees.Find(CareerDegrees.ItemId);
                XsiContext.Context.XsiCareerDegrees.Remove(aCareerDegrees);
            }
        }
        public void Delete(long itemId)
        {
            XsiCareerDegrees aCareerDegrees = XsiContext.Context.XsiCareerDegrees.Find(itemId);
            XsiContext.Context.XsiCareerDegrees.Remove(aCareerDegrees);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
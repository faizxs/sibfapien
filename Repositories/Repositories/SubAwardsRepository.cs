﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SubAwardsRepository : ISubAwardsRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SubAwardsRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SubAwardsRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SubAwardsRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiSubAwards GetById(long itemId)
        {
            return XsiContext.Context.XsiSubAwards.Find(itemId);
        }
        
        public List<XsiSubAwards> Select()
        {
            return XsiContext.Context.XsiSubAwards.ToList();
        }
        public List<XsiSubAwards> Select(XsiSubAwards entity)
        {
            var predicate = PredicateBuilder.True<XsiSubAwards>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
 
            if (!entity.AwardId.IsDefault())
                predicate = predicate.And(p => p.AwardId == entity.AwardId);

            if (!entity.Conditions.IsDefault())
                predicate = predicate.And(p => p.Conditions.Contains(entity.Conditions));

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.Introduction.IsDefault())
                predicate = predicate.And(p => p.Introduction.Contains(entity.Introduction));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifieidBy.IsDefault())
                predicate = predicate.And(p => p.ModifieidBy == entity.ModifieidBy);

            if (!entity.Name.IsDefault())
                predicate = predicate.And(p => p.Name.Contains(entity.Name));

            if (!entity.Rrequirements.IsDefault())
                predicate = predicate.And(p => p.Rrequirements.Contains(entity.Rrequirements));

            return XsiContext.Context.XsiSubAwards.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiSubAwards> SelectComeplete(XsiSubAwards entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiSubAwards>();
            var predicate = PredicateBuilder.True<XsiSubAwards>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.AwardId.IsDefault())
            {
                predicate = predicate.And(p => p.AwardId == entity.AwardId);
                XsiAwards MainEntity = XsiContext.Context.XsiAwards.Where(p => p.ItemId == entity.AwardId).FirstOrDefault();

                if (!entity.Name.IsDefault())
                {
                    string strSearchText = entity.Name;
                    var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                    Inner = Inner.Or(p => p.Name.Contains(searchKeys.str1) || p.Name.Contains(searchKeys.str2) || p.Name.Contains(searchKeys.str3) || p.Name.Contains(searchKeys.str4) || p.Name.Contains(searchKeys.str5)
                        || p.Name.Contains(searchKeys.str6) || p.Name.Contains(searchKeys.str7) || p.Name.Contains(searchKeys.str8) || p.Name.Contains(searchKeys.str9) || p.Name.Contains(searchKeys.str10)
                        || p.Name.Contains(searchKeys.str11) || p.Name.Contains(searchKeys.str12) || p.Name.Contains(searchKeys.str13)
                        || p.Name.Contains(searchKeys.str14) || p.Name.Contains(searchKeys.str15) || p.Name.Contains(searchKeys.str16) || p.Name.Contains(searchKeys.str17)
                        );

                    isInner = true;
                }
                else
                {
                    if (!entity.Name.IsDefault())
                    {
                        string strSearchText = entity.Name.ToLower();
                        Inner = Inner.Or(p => p.Name.ToLower().Contains(strSearchText));
                        isInner = true;
                    }
                }
            }
            else
            {
                if (!entity.Name.IsDefault())
                    predicate = predicate.And(p => p.Name.ToLower().Contains(entity.Name));
            }
            if (!entity.Conditions.IsDefault())
                predicate = predicate.And(p => p.Conditions.Contains(entity.Conditions));

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.Introduction.IsDefault())
                predicate = predicate.And(p => p.Introduction.Contains(entity.Introduction));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifieidBy.IsDefault())
                predicate = predicate.And(p => p.ModifieidBy == entity.ModifieidBy);

            if (!entity.Rrequirements.IsDefault())
                predicate = predicate.And(p => p.Rrequirements.Contains(entity.Rrequirements));

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            //return XsiContext.Context.XsiSubAwards.Include(p => p.XsiAwards).AsExpandable().Where(predicate).ToList();
            return XsiContext.Context.XsiSubAwards.AsExpandable().Where(predicate).ToList();
        }

        public XsiSubAwards Add(XsiSubAwards entity)
        {
            XsiSubAwards SubAwards = new XsiSubAwards();
             
            SubAwards.AwardId = entity.AwardId;
            if (!entity.Conditions.IsDefault())
                SubAwards.Conditions = entity.Conditions.Trim();
            if (!entity.Introduction.IsDefault())
                SubAwards.Introduction = entity.Introduction.Trim();
            SubAwards.IsActive = entity.IsActive;
            SubAwards.ModifiedOn = entity.ModifiedOn;
            SubAwards.ModifieidBy = entity.ModifieidBy;
            SubAwards.Name = entity.Name;
            if (!entity.Rrequirements.IsDefault())
                SubAwards.Rrequirements = entity.Rrequirements.Trim();

            #region Ar
            if (!entity.ConditionsAr.IsDefault())
                SubAwards.ConditionsAr = entity.ConditionsAr.Trim();
            if (!entity.IntroductionAr.IsDefault())
                SubAwards.IntroductionAr = entity.IntroductionAr.Trim();
            SubAwards.IsActiveAr = entity.IsActiveAr;           
            SubAwards.NameAr = entity.NameAr;
            if (!entity.RrequirementsAr.IsDefault())
                SubAwards.RrequirementsAr = entity.RrequirementsAr.Trim();
            #endregion Ar
            SubAwards.CreatedBy = entity.CreatedBy;
            SubAwards.CreatedOn = entity.CreatedOn;
            
            XsiContext.Context.XsiSubAwards.Add(SubAwards);
            SubmitChanges();
            return SubAwards;

        }
        public void Update(XsiSubAwards entity)
        {
            XsiSubAwards SubAwards = XsiContext.Context.XsiSubAwards.Find(entity.ItemId);
            XsiContext.Context.Entry(SubAwards).State = EntityState.Modified;

            if (!entity.Conditions.IsDefault())
                SubAwards.Conditions = entity.Conditions.Trim();

            if (!entity.Introduction.IsDefault())
                SubAwards.Introduction = entity.Introduction.Trim();

            if (!entity.IsActive.IsDefault())
                SubAwards.IsActive = entity.IsActive;

            if (!entity.ModifiedOn.IsDefault())
                SubAwards.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifieidBy.IsDefault())
                SubAwards.ModifiedOn = entity.ModifiedOn;

            if (!entity.Name.IsDefault())
                SubAwards.Name = entity.Name.Trim();

            if (!entity.Rrequirements.IsDefault())
                SubAwards.Rrequirements = entity.Rrequirements.Trim();

            #region Ar
            if (!entity.ConditionsAr.IsDefault())
                SubAwards.ConditionsAr = entity.ConditionsAr.Trim();
            if (!entity.IntroductionAr.IsDefault())
                SubAwards.IntroductionAr = entity.IntroductionAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                SubAwards.IsActiveAr = entity.IsActiveAr;
            if (!entity.NameAr.IsDefault())
                SubAwards.NameAr = entity.NameAr;
            if (!entity.RrequirementsAr.IsDefault())
                SubAwards.RrequirementsAr = entity.RrequirementsAr.Trim();
            #endregion Ar
        }
        public void Delete(XsiSubAwards entity)
        {
            foreach (XsiSubAwards SubAwards in Select(entity))
            {
                XsiSubAwards aSubAwards = XsiContext.Context.XsiSubAwards.Find(SubAwards.ItemId);
                XsiContext.Context.XsiSubAwards.Remove(aSubAwards);
            }
        }
        public void Delete(long itemId)
        {
            XsiSubAwards aSubAwards = XsiContext.Context.XsiSubAwards.Find(itemId);
            XsiContext.Context.XsiSubAwards.Remove(aSubAwards);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
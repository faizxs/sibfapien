﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class EventSubCategoryRepository : IEventSubCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public EventSubCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public EventSubCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public EventSubCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiEventSubCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiEventSubCategory.Find(itemId);
        }
       
        public List<XsiEventSubCategory> Select()
        {
            return XsiContext.Context.XsiEventSubCategory.ToList();
        }
        public List<XsiEventSubCategory> Select(XsiEventSubCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiEventSubCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            
            if (!entity.EventCategoryId.IsDefault())
                predicate = predicate.And(p => p.EventCategoryId == entity.EventCategoryId);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);
              
            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiEventSubCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiEventSubCategory> SelectComeplete(XsiEventSubCategory entity)
        {

            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiEventSubCategory>();
            var predicate = PredicateBuilder.True<XsiEventSubCategory>();

            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.EventCategoryId.IsDefault())
                predicate = predicate.And(p => p.EventCategoryId == entity.EventCategoryId);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
            //if (!entity.Title.IsDefault())
            //    predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiEventSubCategory.Include(p => p.EventCategory).AsExpandable().Where(predicate).ToList();
        }

        public XsiEventSubCategory Add(XsiEventSubCategory entity)
        {
            XsiEventSubCategory EventSubCategory = new XsiEventSubCategory();
            
            EventSubCategory.EventCategoryId = entity.EventCategoryId;
            EventSubCategory.WebsiteId = entity.WebsiteId;
            
            if (!entity.Title.IsDefault())
                EventSubCategory.Title = entity.Title.Trim();
            EventSubCategory.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                EventSubCategory.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                EventSubCategory.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            EventSubCategory.CreatedOn = entity.CreatedOn;
            EventSubCategory.CreatedBy = entity.CreatedBy;
            EventSubCategory.ModifiedOn = entity.ModifiedOn;
            EventSubCategory.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiEventSubCategory.Add(EventSubCategory);
            SubmitChanges();
            return EventSubCategory;

        }
        public void Update(XsiEventSubCategory entity)
        {
            XsiEventSubCategory EventSubCategory = XsiContext.Context.XsiEventSubCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(EventSubCategory).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                EventSubCategory.Title = entity.Title.Trim();

            if (!entity.WebsiteId.IsDefault())
                EventSubCategory.WebsiteId = entity.WebsiteId;

            if (!entity.IsActive.IsDefault())
                EventSubCategory.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                EventSubCategory.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                EventSubCategory.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                EventSubCategory.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                EventSubCategory.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiEventSubCategory entity)
        {
            foreach (XsiEventSubCategory EventSubCategory in Select(entity))
            {
                XsiEventSubCategory aEventSubCategory = XsiContext.Context.XsiEventSubCategory.Find(EventSubCategory.ItemId);
                XsiContext.Context.XsiEventSubCategory.Remove(aEventSubCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiEventSubCategory aEventSubCategory = XsiContext.Context.XsiEventSubCategory.Find(itemId);
            XsiContext.Context.XsiEventSubCategory.Remove(aEventSubCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
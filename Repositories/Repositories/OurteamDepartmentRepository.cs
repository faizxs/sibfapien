﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class OurteamDepartmentRepository : IOurteamDepartmentRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public OurteamDepartmentRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public OurteamDepartmentRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public OurteamDepartmentRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiOurteamDepartment GetById(long itemId)
        {
            return XsiContext.Context.XsiOurteamDepartment.Find(itemId);
        }
        public List<XsiOurteamDepartment> Select()
        {
            return XsiContext.Context.XsiOurteamDepartment.ToList();
        }
        public List<XsiOurteamDepartment> SelectOr(XsiOurteamDepartment entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiOurteamDepartment>();
            var Outer = PredicateBuilder.True<XsiOurteamDepartment>();

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.TitleAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.TitleAr.Contains(searchKeys.str1) || p.TitleAr.Contains(searchKeys.str2) || p.TitleAr.Contains(searchKeys.str3) || p.TitleAr.Contains(searchKeys.str4) || p.TitleAr.Contains(searchKeys.str5)
                    || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                    || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                    || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsActiveAr.IsDefault())
                Outer = Outer.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiOurteamDepartment.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiOurteamDepartment> Select(XsiOurteamDepartment entity)
        {
            var predicate = PredicateBuilder.True<XsiOurteamDepartment>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiOurteamDepartment.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiOurteamDepartment> SelectOtherLanguageCategory(XsiOurteamDepartment entity)
        {
            var predicate = PredicateBuilder.True<XsiOurteamDepartment>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            var ItemId = XsiContext.Context.XsiOurteamDepartment.AsExpandable().Where(predicate).Single().ItemId;
            predicate = PredicateBuilder.True<XsiOurteamDepartment>();
            predicate = predicate.And(p => p.ItemId == ItemId);
            return XsiContext.Context.XsiOurteamDepartment.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiOurteamDepartment> SelectCurrentLanguageCategory(XsiOurteamDepartment entity)
        {
            var predicate = PredicateBuilder.True<XsiOurteamDepartment>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            var ItemId = XsiContext.Context.XsiOurteamDepartment.AsExpandable().Where(predicate).Single().ItemId;
            predicate = PredicateBuilder.True<XsiOurteamDepartment>();
            predicate = predicate.And(p => p.ItemId == ItemId);
            return XsiContext.Context.XsiOurteamDepartment.AsExpandable().Where(predicate).ToList();
        }

        public XsiOurteamDepartment Add(XsiOurteamDepartment entity)
        {
            XsiOurteamDepartment OurteamDepartment = new XsiOurteamDepartment();
            OurteamDepartment.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                OurteamDepartment.Title = entity.Title.Trim();

            OurteamDepartment.IsActiveAr = entity.IsActiveAr;
            if (!entity.TitleAr.IsDefault())
                OurteamDepartment.TitleAr = entity.TitleAr.Trim();

            OurteamDepartment.CreatedOn = entity.CreatedOn;
            OurteamDepartment.CreatedBy = entity.CreatedBy;
            OurteamDepartment.ModifiedOn = entity.ModifiedOn;
            OurteamDepartment.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiOurteamDepartment.Add(OurteamDepartment);
            SubmitChanges();
            return OurteamDepartment;

        }
        public void Update(XsiOurteamDepartment entity)
        {
            XsiOurteamDepartment OurteamDepartment = XsiContext.Context.XsiOurteamDepartment.Find(entity.ItemId);
            XsiContext.Context.Entry(OurteamDepartment).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                OurteamDepartment.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                OurteamDepartment.Title = entity.Title.Trim();

            if (!entity.IsActiveAr.IsDefault())
                OurteamDepartment.IsActiveAr = entity.IsActiveAr;

            if (!entity.TitleAr.IsDefault())
                OurteamDepartment.TitleAr = entity.TitleAr.Trim();

            if (!entity.ModifiedOn.IsDefault())
                OurteamDepartment.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                OurteamDepartment.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiOurteamDepartment entity)
        {
            foreach (XsiOurteamDepartment OurteamDepartment in Select(entity))
            {
                XsiOurteamDepartment aOurteamDepartment = XsiContext.Context.XsiOurteamDepartment.Find(OurteamDepartment.ItemId);
                XsiContext.Context.XsiOurteamDepartment.Remove(aOurteamDepartment);
            }
        }
        public void Delete(long itemId)
        {
            XsiOurteamDepartment aOurteamDepartment = XsiContext.Context.XsiOurteamDepartment.Find(itemId);
            XsiContext.Context.XsiOurteamDepartment.Remove(aOurteamDepartment);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
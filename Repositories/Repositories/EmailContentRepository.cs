﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class EmailContentRepository : IEmailContentRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public EmailContentRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public EmailContentRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public EmailContentRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiEmailContent GetById(long itemId)
        {
            return XsiContext.Context.XsiEmailContent.Find(itemId);
        }
         
        public List<XsiEmailContent> Select()
        {
            return XsiContext.Context.XsiEmailContent.ToList();
        }
        public List<XsiEmailContent> Select(XsiEmailContent entity)
        {
            var predicate = PredicateBuilder.True<XsiEmailContent>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Subject.IsDefault())
                predicate = predicate.And(p => p.Subject.Contains(entity.Subject));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Contains(entity.Email));

            if (!entity.Body.IsDefault())
                predicate = predicate.And(p => p.Body.Contains(entity.Body));

            if (!entity.RollbackXml.IsDefault())
                predicate = predicate.And(p => p.RollbackXml.Equals(entity.RollbackXml));


            return XsiContext.Context.XsiEmailContent.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiEmailContent> SelectOr(XsiEmailContent entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiEmailContent>();
            var Outer = PredicateBuilder.True<XsiEmailContent>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                     || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                     || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                     || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                     );
                Inner = Inner.Or(p => p.Subject.Contains(searchKeys.str1) || p.Subject.Contains(searchKeys.str2) || p.Subject.Contains(searchKeys.str3) || p.Subject.Contains(searchKeys.str4) || p.Subject.Contains(searchKeys.str5)
                    || p.Subject.Contains(searchKeys.str6) || p.Subject.Contains(searchKeys.str7) || p.Subject.Contains(searchKeys.str8) || p.Subject.Contains(searchKeys.str9) || p.Subject.Contains(searchKeys.str10)
                    || p.Subject.Contains(searchKeys.str11) || p.Subject.Contains(searchKeys.str12) || p.Subject.Contains(searchKeys.str13)
                    || p.Subject.Contains(searchKeys.str14) || p.Subject.Contains(searchKeys.str15) || p.Subject.Contains(searchKeys.str16) || p.Subject.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();

                    if (!entity.Subject.IsDefault())
                        Inner = Inner.Or(p => p.Subject.ToLower().Contains(strSearchText));

                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));

                    isInner = true;
                }
            }
             
            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiEmailContent.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public XsiEmailContent Add(XsiEmailContent entity)
        {
            XsiEmailContent EmailContent = new XsiEmailContent();
            
            if (!entity.Title.IsDefault())
                EmailContent.Title = entity.Title.Trim();
            if (!entity.Subject.IsDefault())
                EmailContent.Subject = entity.Subject.Trim();
            if (!entity.Email.IsDefault())
                EmailContent.Email = entity.Email.Trim();
            if (!entity.Body.IsDefault())
                EmailContent.Body = entity.Body.Trim();
            EmailContent.RollbackXml = entity.RollbackXml;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                EmailContent.TitleAr = entity.TitleAr.Trim();
            if (!entity.SubjectAr.IsDefault())
                EmailContent.SubjectAr = entity.SubjectAr.Trim();
            if (!entity.EmailAr.IsDefault())
                EmailContent.EmailAr = entity.EmailAr.Trim();
            if (!entity.BodyAr.IsDefault())
                EmailContent.BodyAr = entity.BodyAr.Trim();
            EmailContent.RollbackXmlAr = entity.RollbackXmlAr;
            #endregion Ar

            XsiContext.Context.XsiEmailContent.Add(EmailContent);
            SubmitChanges();
            return EmailContent;

        }
        public void Update(XsiEmailContent entity)
        {
            XsiEmailContent EmailContent = XsiContext.Context.XsiEmailContent.Find(entity.ItemId);
            XsiContext.Context.Entry(EmailContent).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                EmailContent.Title = entity.Title.Trim();

            if (!entity.Subject.IsDefault())
                EmailContent.Subject = entity.Subject.Trim();

            if (!entity.Email.IsDefault())
                EmailContent.Email = entity.Email.Trim();

            if (!entity.Body.IsDefault())
                EmailContent.Body = entity.Body.Trim();

            if (!entity.RollbackXml.IsDefault())
                EmailContent.RollbackXml = entity.RollbackXml;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                EmailContent.TitleAr = entity.TitleAr.Trim();
            if (!entity.SubjectAr.IsDefault())
                EmailContent.SubjectAr = entity.SubjectAr.Trim();
            if (!entity.EmailAr.IsDefault())
                EmailContent.EmailAr = entity.EmailAr.Trim();
            if (!entity.BodyAr.IsDefault())
                EmailContent.BodyAr = entity.BodyAr.Trim();
            if (!entity.RollbackXmlAr.IsDefault())
                EmailContent.RollbackXmlAr = entity.RollbackXmlAr;
            #endregion Ar
        }
        public void Delete(XsiEmailContent entity)
        {
            foreach (XsiEmailContent EmailContent in Select(entity))
            {
                XsiEmailContent aEmailContent = XsiContext.Context.XsiEmailContent.Find(EmailContent.ItemId);
                XsiContext.Context.XsiEmailContent.Remove(aEmailContent);
            }
        }
        public void Delete(long itemId)
        {
            XsiEmailContent aEmailContent = XsiContext.Context.XsiEmailContent.Find(itemId);
            XsiContext.Context.XsiEmailContent.Remove(aEmailContent);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
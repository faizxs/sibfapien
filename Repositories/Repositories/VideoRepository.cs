﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class VideoRepository : IVideoRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public VideoRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public VideoRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public VideoRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiVideo GetById(long itemId)
        {
            return XsiContext.Context.XsiVideo.Find(itemId);
        }

        public List<XsiVideo> Select()
        {
            return XsiContext.Context.XsiVideo.ToList();
        }
        public List<XsiVideo> Select(XsiVideo entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiVideo>();
            var predicate = PredicateBuilder.True<XsiVideo>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.VideoAlbumId.IsDefault())
                predicate = predicate.And(p => p.VideoAlbumId == entity.VideoAlbumId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsAlbumCover.IsDefault())
                predicate = predicate.And(p => p.IsAlbumCover.Equals(entity.IsAlbumCover));

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.TitleAr.Contains(searchKeys.str1) || p.TitleAr.Contains(searchKeys.str2) || p.TitleAr.Contains(searchKeys.str3) || p.TitleAr.Contains(searchKeys.str4) || p.TitleAr.Contains(searchKeys.str5)
                    || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                    || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                    || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail.Contains(entity.Thumbnail));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            if (!entity.SortIndex.IsDefault())
                predicate = predicate.And(p => p.SortIndex == entity.SortIndex);


            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.IsAlbumCoverAr.IsDefault())
                predicate = predicate.And(p => p.IsAlbumCoverAr.Equals(entity.IsAlbumCoverAr));

            if (!entity.DescriptionAr.IsDefault())
                predicate = predicate.And(p => p.DescriptionAr.Contains(entity.DescriptionAr));

            if (!entity.ThumbnailAr.IsDefault())
                predicate = predicate.And(p => p.ThumbnailAr.Contains(entity.ThumbnailAr));

            if (!entity.Urlar.IsDefault())
                predicate = predicate.And(p => p.Urlar.Contains(entity.Urlar));

            if (!entity.SortIndexAr.IsDefault())
                predicate = predicate.And(p => p.SortIndexAr == entity.SortIndexAr);


            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiVideo.AsExpandable().Where(predicate).ToList();
        }

        public XsiVideo Add(XsiVideo entity)
        {
            XsiVideo Video = new XsiVideo();
            Video.VideoAlbumId = entity.VideoAlbumId;
            Video.IsActive = entity.IsActive;
            Video.IsAlbumCover = entity.IsAlbumCover;
            if (!entity.Title.IsDefault())
                Video.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
            Video.Description = entity.Description.Trim();
            Video.Thumbnail = entity.Thumbnail;
            if (!entity.Url.IsDefault())
            Video.Url = entity.Url.Trim();
            Video.SortIndex = entity.SortIndex;

            Video.IsActiveAr = entity.IsActiveAr;
            Video.IsAlbumCoverAr = entity.IsAlbumCoverAr;
            if (!entity.TitleAr.IsDefault())
                Video.TitleAr = entity.TitleAr.Trim();
            if (!entity.DescriptionAr.IsDefault())
                Video.DescriptionAr = entity.DescriptionAr.Trim();
            Video.ThumbnailAr = entity.ThumbnailAr;
            if (!entity.Urlar.IsDefault())
                Video.Urlar = entity.Urlar.Trim();
            Video.SortIndexAr = entity.SortIndexAr;


            Video.CreatedOn = entity.CreatedOn;
            Video.CreatedBy = entity.CreatedBy;
            Video.ModifiedOn = entity.ModifiedOn;
            Video.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiVideo.Add(Video);
            SubmitChanges();
            return Video;

        }
        public void Update(XsiVideo entity)
        {
            XsiVideo Video = XsiContext.Context.XsiVideo.Find(entity.ItemId);
            XsiContext.Context.Entry(Video).State = EntityState.Modified;

            if (!entity.VideoAlbumId.IsDefault())
                Video.VideoAlbumId = entity.VideoAlbumId;

            if (!entity.IsActive.IsDefault())
                Video.IsActive = entity.IsActive;

            if (!entity.IsAlbumCover.IsDefault())
                Video.IsAlbumCover = entity.IsAlbumCover;

            if (!entity.Title.IsDefault())
                Video.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                Video.Description = entity.Description.Trim();

            if (!entity.Thumbnail.IsDefault())
                Video.Thumbnail = entity.Thumbnail;

            if (!entity.Url.IsDefault())
                Video.Url = entity.Url.Trim();

            if (!entity.SortIndex.IsDefault())
                Video.SortIndex = entity.SortIndex;

            if (!entity.IsActiveAr.IsDefault())
                Video.IsActiveAr = entity.IsActiveAr;

            if (!entity.IsAlbumCoverAr.IsDefault())
                Video.IsAlbumCoverAr = entity.IsAlbumCoverAr;

            if (!entity.TitleAr.IsDefault())
                Video.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                Video.DescriptionAr = entity.DescriptionAr.Trim();

            if (!entity.ThumbnailAr.IsDefault())
                Video.ThumbnailAr = entity.ThumbnailAr;

            if (!entity.Urlar.IsDefault())
                Video.Urlar = entity.Urlar.Trim();

            if (!entity.SortIndexAr.IsDefault())
                Video.SortIndexAr = entity.SortIndexAr;

            if (!entity.CreatedOn.IsDefault())
                Video.CreatedOn = entity.CreatedOn;

            if (!entity.CreatedBy.IsDefault())
                Video.CreatedBy = entity.CreatedBy;

            if (!entity.ModifiedOn.IsDefault())
                Video.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                Video.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiVideo entity)
        {
            foreach (XsiVideo Video in Select(entity))
            {
                XsiVideo aVideo = XsiContext.Context.XsiVideo.Find(Video.ItemId);
                XsiContext.Context.XsiVideo.Remove(aVideo);
            }
        }
        public void Delete(long itemId)
        {
            XsiVideo aVideo = XsiContext.Context.XsiVideo.Find(itemId);
            XsiContext.Context.XsiVideo.Remove(aVideo);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
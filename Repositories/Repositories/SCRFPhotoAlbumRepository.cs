﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFPhotoAlbumRepository : ISCRFPhotoAlbumRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFPhotoAlbumRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFPhotoAlbumRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFPhotoAlbumRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfphotoAlbum GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfphotoAlbum.Find(itemId);
        }
         
        public List<XsiScrfphotoAlbum> Select()
        {
            return XsiContext.Context.XsiScrfphotoAlbum.ToList();
        }
        public List<XsiScrfphotoAlbum> Select(XsiScrfphotoAlbum entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfphotoAlbum>();
            var predicate = PredicateBuilder.True<XsiScrfphotoAlbum>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.SubCategoryId.IsDefault())
                predicate = predicate.And(p => p.SubCategoryId == entity.SubCategoryId);
              
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated == entity.Dated);

            if (!entity.SortIndex.IsDefault())
                predicate = predicate.And(p => p.SortIndex == entity.SortIndex);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiScrfphotoAlbum.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfphotoAlbum Add(XsiScrfphotoAlbum entity)
        {
            XsiScrfphotoAlbum PhotoAlbum = new XsiScrfphotoAlbum();
            PhotoAlbum.SubCategoryId = entity.SubCategoryId;
           
            PhotoAlbum.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                PhotoAlbum.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
            PhotoAlbum.Description = entity.Description.Trim();
            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                PhotoAlbum.IsActiveAr = entity.IsActiveAr;

            if (!entity.TitleAr.IsDefault())
                PhotoAlbum.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                PhotoAlbum.DescriptionAr = entity.DescriptionAr.Trim();
            #endregion Ar

            PhotoAlbum.Dated = entity.Dated;
            PhotoAlbum.SortIndex = entity.SortIndex;
            PhotoAlbum.CreatedOn = entity.CreatedOn;
            PhotoAlbum.CreatedBy = entity.CreatedBy;
            PhotoAlbum.ModifiedOn = entity.ModifiedOn;
            PhotoAlbum.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfphotoAlbum.Add(PhotoAlbum);
            SubmitChanges();
            return PhotoAlbum;

        }
        public void Update(XsiScrfphotoAlbum entity)
        {
            XsiScrfphotoAlbum PhotoAlbum = XsiContext.Context.XsiScrfphotoAlbum.Find(entity.ItemId);
            XsiContext.Context.Entry(PhotoAlbum).State = EntityState.Modified;

            if (!entity.SubCategoryId.IsDefault())
                PhotoAlbum.SubCategoryId = entity.SubCategoryId;
             
            if (!entity.IsActive.IsDefault())
                PhotoAlbum.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                PhotoAlbum.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                PhotoAlbum.Description = entity.Description.Trim();

            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                PhotoAlbum.IsActiveAr = entity.IsActiveAr;

            if (!entity.TitleAr.IsDefault())
                PhotoAlbum.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                PhotoAlbum.DescriptionAr = entity.DescriptionAr.Trim();
            #endregion Ar

            if (!entity.Dated.IsDefault())
                PhotoAlbum.Dated = entity.Dated;

            if (!entity.SortIndex.IsDefault())
                PhotoAlbum.SortIndex = entity.SortIndex;

            if (!entity.CreatedOn.IsDefault())
                PhotoAlbum.CreatedOn = entity.CreatedOn;

            if (!entity.CreatedBy.IsDefault())
                PhotoAlbum.CreatedBy = entity.CreatedBy;

            if (!entity.ModifiedOn.IsDefault())
                PhotoAlbum.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                PhotoAlbum.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiScrfphotoAlbum entity)
        {
            foreach (XsiScrfphotoAlbum PhotoAlbum in Select(entity))
            {
                XsiScrfphotoAlbum aPhotoAlbum = XsiContext.Context.XsiScrfphotoAlbum.Find(PhotoAlbum.ItemId);
                XsiContext.Context.XsiScrfphotoAlbum.Remove(aPhotoAlbum);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfphotoAlbum aPhotoAlbum = XsiContext.Context.XsiScrfphotoAlbum.Find(itemId);
            XsiContext.Context.XsiScrfphotoAlbum.Remove(aPhotoAlbum);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
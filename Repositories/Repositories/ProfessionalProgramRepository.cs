﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ProfessionalProgramRepository : IProfessionalProgramRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ProfessionalProgramRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ProfessionalProgramRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ProfessionalProgramRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionProfessionalProgram GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionProfessionalProgram.Find(itemId);
        }
       
        public List<XsiExhibitionProfessionalProgram> Select()
        {
            return XsiContext.Context.XsiExhibitionProfessionalProgram.ToList();
        }
        public List<XsiExhibitionProfessionalProgram> Select(XsiExhibitionProfessionalProgram entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgram>();
            if (entity.ProfessionalProgramId>0)
                predicate = predicate.And(p => p.ProfessionalProgramId == entity.ProfessionalProgramId);

            if (!entity.Name.IsDefault())
                predicate = predicate.And(p => p.Name.Contains(entity.Name));

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsArchive.IsDefault())
                predicate = predicate.And(p => p.IsArchive.Equals(entity.IsArchive));

             
            //if (!entity..IsDefault())
            //    predicate = predicate.And(p => p.Step1.Contains(entity.Step1));

            //if (!entity.Step2a.IsDefault())
            //    predicate = predicate.And(p => p.Step2a.Contains(entity.Step2a));

            //if (!entity.Step2b.IsDefault())
            //    predicate = predicate.And(p => p.Step2b.Contains(entity.Step2b));

            //if (!entity.Step3a.IsDefault())
            //    predicate = predicate.And(p => p.Step3a.Contains(entity.Step3a));

            //if (!entity.Step3b.IsDefault())
            //    predicate = predicate.And(p => p.Step3b.Contains(entity.Step3b));

            //if (!entity.Step4.IsDefault())
            //    predicate = predicate.And(p => p.Step4.Contains(entity.Step4));

            if (!entity.TimeSlot.IsDefault())
                predicate = predicate.And(p => p.TimeSlot == entity.TimeSlot);

            if (!entity.AppointmentStartDate.IsDefault())
                predicate = predicate.And(p => p.AppointmentStartDate == entity.AppointmentStartDate);

            if (!entity.AppointmentEndDate.IsDefault())
                predicate = predicate.And(p => p.AppointmentEndDate == entity.AppointmentEndDate);

            if (!entity.StartDateDayOne.IsDefault())
                predicate = predicate.And(p => p.StartDateDayOne == entity.StartDateDayOne);

            if (!entity.EndDateDayOne.IsDefault())
                predicate = predicate.And(p => p.EndDateDayOne == entity.EndDateDayOne);

            if (!entity.BreakTimeOneDayOneStart.IsDefault())
                predicate = predicate.And(p => p.BreakTimeOneDayOneStart == entity.BreakTimeOneDayOneStart);

            if (!entity.BreakTimeOneDayOneEnd.IsDefault())
                predicate = predicate.And(p => p.BreakTimeOneDayOneEnd == entity.BreakTimeOneDayOneEnd);

            if (!entity.BreakTimeTwoDayOneStart.IsDefault())
                predicate = predicate.And(p => p.BreakTimeTwoDayOneStart == entity.BreakTimeTwoDayOneStart);

            if (!entity.BreakTimeTwoDayOneEnd.IsDefault())
                predicate = predicate.And(p => p.BreakTimeTwoDayOneEnd == entity.BreakTimeTwoDayOneEnd);

            if (!entity.StartDateDayTwo.IsDefault())
                predicate = predicate.And(p => p.StartDateDayTwo == entity.StartDateDayTwo);

            if (!entity.EndDateDayTwo.IsDefault())
                predicate = predicate.And(p => p.EndDateDayTwo == entity.EndDateDayTwo);

            if (!entity.BreakTimeOneDayTwoStart.IsDefault())
                predicate = predicate.And(p => p.BreakTimeOneDayTwoStart == entity.BreakTimeOneDayTwoStart);

            if (!entity.BreakTimeOneDayTwoEnd.IsDefault())
                predicate = predicate.And(p => p.BreakTimeOneDayTwoEnd == entity.BreakTimeOneDayTwoEnd);

            if (!entity.BreakTimeTwoDayTwoStart.IsDefault())
                predicate = predicate.And(p => p.BreakTimeTwoDayTwoStart == entity.BreakTimeTwoDayTwoStart);

            if (!entity.BreakTimeTwoDayTwoEnd.IsDefault())
                predicate = predicate.And(p => p.BreakTimeTwoDayTwoEnd == entity.BreakTimeTwoDayTwoEnd);


            if (!entity.RegistrationStartDate.IsDefault())
                predicate = predicate.And(p => p.RegistrationStartDate == entity.RegistrationStartDate);

            if (!entity.RegistrationEndDate.IsDefault())
                predicate = predicate.And(p => p.RegistrationEndDate == entity.RegistrationEndDate);

            if (!entity.Location.IsDefault())
                predicate = predicate.And(p => p.Location.Contains(entity.Location));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedById.IsDefault())
                predicate = predicate.And(p => p.CreatedById == entity.CreatedById);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedById.IsDefault())
                predicate = predicate.And(p => p.ModifiedById == entity.ModifiedById);


            return XsiContext.Context.XsiExhibitionProfessionalProgram.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionProfessionalProgram Add(XsiExhibitionProfessionalProgram entity)
        {
            XsiExhibitionProfessionalProgram ProfessionalProgram = new XsiExhibitionProfessionalProgram();
             
            ProfessionalProgram.ExhibitionId = entity.ExhibitionId;
            
            if (!entity.Name.IsDefault())
                ProfessionalProgram.Name = entity.Name.Trim();
            ProfessionalProgram.IsActive = entity.IsActive;
            ProfessionalProgram.IsArchive = entity.IsArchive;
             
            ProfessionalProgram.TimeSlot = entity.TimeSlot;
            ProfessionalProgram.AppointmentStartDate = entity.AppointmentStartDate;
            ProfessionalProgram.AppointmentEndDate = entity.AppointmentEndDate;
            ProfessionalProgram.StartDateDayOne = entity.StartDateDayOne;
            ProfessionalProgram.EndDateDayOne = entity.EndDateDayOne;
            ProfessionalProgram.BreakTimeOneDayOneStart = entity.BreakTimeOneDayOneStart;
            ProfessionalProgram.BreakTimeOneDayOneEnd = entity.BreakTimeOneDayOneEnd;
            ProfessionalProgram.BreakTimeTwoDayOneStart = entity.BreakTimeTwoDayOneStart;
            ProfessionalProgram.BreakTimeTwoDayOneEnd = entity.BreakTimeTwoDayOneEnd;
            ProfessionalProgram.StartDateDayTwo = entity.StartDateDayTwo;
            ProfessionalProgram.EndDateDayTwo = entity.EndDateDayTwo;
            ProfessionalProgram.BreakTimeOneDayTwoStart = entity.BreakTimeOneDayTwoStart;
            ProfessionalProgram.BreakTimeOneDayTwoEnd = entity.BreakTimeOneDayTwoEnd;
            ProfessionalProgram.BreakTimeTwoDayTwoStart = entity.BreakTimeTwoDayTwoStart;
            ProfessionalProgram.BreakTimeTwoDayTwoEnd = entity.BreakTimeTwoDayTwoEnd;
            ProfessionalProgram.RegistrationStartDate = entity.RegistrationStartDate;
            ProfessionalProgram.RegistrationEndDate = entity.RegistrationEndDate;
            if (!entity.Location.IsDefault())
                ProfessionalProgram.Location = entity.Location.Trim();
            ProfessionalProgram.CreatedOn = entity.CreatedOn;
            ProfessionalProgram.CreatedById = entity.CreatedById;
            ProfessionalProgram.ModifiedOn = entity.ModifiedOn;
            ProfessionalProgram.ModifiedById = entity.ModifiedById;

            XsiContext.Context.XsiExhibitionProfessionalProgram.Add(ProfessionalProgram);
            SubmitChanges();
            return ProfessionalProgram;

        }
        public void Update(XsiExhibitionProfessionalProgram entity)
        {
            XsiExhibitionProfessionalProgram ProfessionalProgram = XsiContext.Context.XsiExhibitionProfessionalProgram.Find(entity.ProfessionalProgramId);
            XsiContext.Context.Entry(ProfessionalProgram).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                ProfessionalProgram.IsActive = entity.IsActive;

            if (!entity.Name.IsDefault())
                ProfessionalProgram.Name = entity.Name.Trim();

            if (!entity.IsArchive.IsDefault())
                ProfessionalProgram.IsArchive = entity.IsArchive;
              
            if (!entity.TimeSlot.IsDefault())
                ProfessionalProgram.TimeSlot = entity.TimeSlot;


           // if (!entity.AppointmentStartDate.IsDefault())
                ProfessionalProgram.AppointmentStartDate = entity.AppointmentStartDate;

           // if (!entity.AppointmentEndDate.IsDefault())
                ProfessionalProgram.AppointmentEndDate = entity.AppointmentEndDate;

           // if (!entity.StartDateDayOne.IsDefault())
                ProfessionalProgram.StartDateDayOne = entity.StartDateDayOne;

         //   if (!entity.EndDateDayOne.IsDefault())
                ProfessionalProgram.EndDateDayOne = entity.EndDateDayOne;

          
            ProfessionalProgram.BreakTimeOneDayOneStart = entity.BreakTimeOneDayOneStart;
            ProfessionalProgram.BreakTimeOneDayOneEnd = entity.BreakTimeOneDayOneEnd;
            ProfessionalProgram.BreakTimeTwoDayOneStart = entity.BreakTimeTwoDayOneStart;
            ProfessionalProgram.BreakTimeTwoDayOneEnd = entity.BreakTimeTwoDayOneEnd;

        //    if (!entity.StartDateDayTwo.IsDefault())
                ProfessionalProgram.StartDateDayTwo = entity.StartDateDayTwo;

            //if (!entity.EndDateDayTwo.IsDefault())
                ProfessionalProgram.EndDateDayTwo = entity.EndDateDayTwo;

            ProfessionalProgram.BreakTimeOneDayTwoStart = entity.BreakTimeOneDayTwoStart;
            ProfessionalProgram.BreakTimeOneDayTwoEnd = entity.BreakTimeOneDayTwoEnd;
            ProfessionalProgram.BreakTimeTwoDayTwoStart = entity.BreakTimeTwoDayTwoStart;
            ProfessionalProgram.BreakTimeTwoDayTwoEnd = entity.BreakTimeTwoDayTwoEnd;

            if (!entity.RegistrationStartDate.IsDefault())
                ProfessionalProgram.RegistrationStartDate = entity.RegistrationStartDate;

            if (!entity.RegistrationEndDate.IsDefault())
                ProfessionalProgram.RegistrationEndDate = entity.RegistrationEndDate;

            if (!entity.Location.IsDefault())
                ProfessionalProgram.Location = entity.Location.Trim();

            if (!entity.ModifiedOn.IsDefault())
                ProfessionalProgram.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedById.IsDefault())
                ProfessionalProgram.ModifiedById = entity.ModifiedById;
        }
        public void Delete(XsiExhibitionProfessionalProgram entity)
        {
            foreach (XsiExhibitionProfessionalProgram ProfessionalProgram in Select(entity))
            {
                XsiExhibitionProfessionalProgram aProfessionalProgram = XsiContext.Context.XsiExhibitionProfessionalProgram.Find(ProfessionalProgram.ProfessionalProgramId);
                XsiContext.Context.XsiExhibitionProfessionalProgram.Remove(aProfessionalProgram);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionProfessionalProgram aProfessionalProgram = XsiContext.Context.XsiExhibitionProfessionalProgram.Find(itemId);
            XsiContext.Context.XsiExhibitionProfessionalProgram.Remove(aProfessionalProgram);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class NewsPhotoGalleryRepository : INewsPhotoGalleryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public NewsPhotoGalleryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public NewsPhotoGalleryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public NewsPhotoGalleryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiNewsPhotoGallery GetById(long itemId)
        {
            return XsiContext.Context.XsiNewsPhotoGallery.Find(itemId);
        }
        public List<XsiNewsPhotoGallery> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiNewsPhotoGallery> Select()
        {
            return XsiContext.Context.XsiNewsPhotoGallery.ToList();
        }
        public List<XsiNewsPhotoGallery> Select(XsiNewsPhotoGallery entity)
        {
            var predicate = PredicateBuilder.True<XsiNewsPhotoGallery>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Detail.IsDefault())
                predicate = predicate.And(p => p.Detail.Contains(entity.Detail));

            if (!entity.NewsId.IsDefault())
                predicate = predicate.And(p => p.NewsId == entity.NewsId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Equals(entity.ImageName));

            if (!entity.SortIndex.IsDefault())
                predicate = predicate.And(p => p.SortIndex == entity.SortIndex);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiNewsPhotoGallery.AsExpandable().Where(predicate).ToList();
        }

        public XsiNewsPhotoGallery Add(XsiNewsPhotoGallery entity)
        {
            XsiNewsPhotoGallery NewsPhotoGallery = new XsiNewsPhotoGallery();
            NewsPhotoGallery.NewsId = entity.NewsId;
            if (!entity.Title.IsDefault())
                NewsPhotoGallery.Title = entity.Title.Trim();
            if (!entity.Detail.IsDefault())
                NewsPhotoGallery.Detail = entity.Detail.Trim();
            NewsPhotoGallery.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                NewsPhotoGallery.TitleAr = entity.TitleAr.Trim();
            if (!entity.DetailAr.IsDefault())
                NewsPhotoGallery.DetailAr = entity.DetailAr.Trim();
            NewsPhotoGallery.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            NewsPhotoGallery.ImageName = entity.ImageName;
            NewsPhotoGallery.SortIndex = entity.SortIndex;
            NewsPhotoGallery.ModifiedOn = entity.ModifiedOn;
            NewsPhotoGallery.ModifiedBy = entity.ModifiedBy;
            NewsPhotoGallery.CreatedOn = entity.CreatedOn;
            NewsPhotoGallery.CreatedBy = entity.CreatedBy;

            XsiContext.Context.XsiNewsPhotoGallery.Add(NewsPhotoGallery);
            SubmitChanges();
            return NewsPhotoGallery;

        }
        public void Update(XsiNewsPhotoGallery entity)
        {
            XsiNewsPhotoGallery NewsPhotoGallery = XsiContext.Context.XsiNewsPhotoGallery.Find(entity.ItemId);
            XsiContext.Context.Entry(NewsPhotoGallery).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                NewsPhotoGallery.Title = entity.Title.Trim();

            if (!entity.Detail.IsDefault())
                NewsPhotoGallery.Detail = entity.Detail.Trim();

            if (!entity.IsActive.IsDefault())
                NewsPhotoGallery.IsActive = entity.IsActive;

            if (!entity.SortIndex.IsDefault())
                NewsPhotoGallery.SortIndex = entity.SortIndex;

            if (!entity.ImageName.IsDefault())
                NewsPhotoGallery.ImageName = entity.ImageName;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                NewsPhotoGallery.TitleAr = entity.TitleAr.Trim();
            if (!entity.DetailAr.IsDefault())
                NewsPhotoGallery.DetailAr = entity.DetailAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                NewsPhotoGallery.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                NewsPhotoGallery.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                NewsPhotoGallery.ModifiedBy = entity.ModifiedBy;

            if (!entity.NewsId.IsDefault())
                NewsPhotoGallery.NewsId = entity.NewsId;
        }
        public void Delete(XsiNewsPhotoGallery entity)
        {
            foreach (XsiNewsPhotoGallery NewsPhotoGallery in Select(entity))
            {
                XsiNewsPhotoGallery aNewsPhotoGallery = XsiContext.Context.XsiNewsPhotoGallery.Find(NewsPhotoGallery.ItemId);
                XsiContext.Context.XsiNewsPhotoGallery.Remove(aNewsPhotoGallery);
            }
        }
        public void Delete(long itemId)
        {
            XsiNewsPhotoGallery aNewsPhotoGallery = XsiContext.Context.XsiNewsPhotoGallery.Find(itemId);
            XsiContext.Context.XsiNewsPhotoGallery.Remove(aNewsPhotoGallery);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class HomePageLinkRepository : IHomePageLinkRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public HomePageLinkRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public HomePageLinkRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public HomePageLinkRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiHomePageLink GetById(long itemId)
        {
            return XsiContext.Context.XsiHomePageLink.Find(itemId);
        }
        
        public List<XsiHomePageLink> SelectOr(XsiHomePageLink entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiHomePageLink>();
            var Outer = PredicateBuilder.True<XsiHomePageLink>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
             
            if (!entity.PageUrl.IsDefault())
                Outer = Outer.And(p => p.PageUrl.Equals(entity.PageUrl));
             
            if (!entity.SortOrder.IsDefault())
                Outer = Outer.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.IsExternal.IsDefault())
                Outer = Outer.And(p => p.IsExternal.Equals(entity.IsExternal));

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiHomePageLink.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiHomePageLink> Select()
        {
            return XsiContext.Context.XsiHomePageLink.ToList();
        }
        public List<XsiHomePageLink> Select(XsiHomePageLink entity)
        {
            var predicate = PredicateBuilder.True<XsiHomePageLink>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
 
            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Equals(entity.Title));

            if (!entity.PageUrl.IsDefault())
                predicate = predicate.And(p => p.PageUrl.Equals(entity.PageUrl));

             
            if (!entity.SortOrder.IsDefault())
                predicate = predicate.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.IsExternal.IsDefault())
                predicate = predicate.And(p => p.IsExternal.Equals(entity.IsExternal));

            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail.Contains(entity.Thumbnail));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy.Equals(entity.CreatedBy));

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy.Equals(entity.ModifiedBy));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn.Equals(entity.CreatedOn));

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn.Equals(entity.ModifiedOn));

            return XsiContext.Context.XsiHomePageLink.AsExpandable().Where(predicate).ToList();
        }

        public XsiHomePageLink Add(XsiHomePageLink entity)
        {
            XsiHomePageLink HomePageLink = new XsiHomePageLink();
            
            if (!entity.Title.IsDefault())
                HomePageLink.Title = entity.Title.Trim();
            if (!entity.PageUrl.IsDefault())
                HomePageLink.PageUrl = entity.PageUrl.Trim();
            HomePageLink.Thumbnail = entity.Thumbnail;
            HomePageLink.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                HomePageLink.TitleAr = entity.TitleAr.Trim();
            if (!entity.PageUrlAr.IsDefault())
                HomePageLink.PageUrlAr = entity.PageUrlAr.Trim();
            HomePageLink.ThumbnailAr = entity.ThumbnailAr;
            HomePageLink.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            HomePageLink.SortOrder = entity.SortOrder;
            HomePageLink.IsExternal = entity.IsExternal; 
            HomePageLink.CreatedBy = entity.CreatedBy;
            HomePageLink.CreatedOn = entity.CreatedOn;
            HomePageLink.ModifiedOn = entity.ModifiedOn;
            HomePageLink.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiHomePageLink.Add(HomePageLink);
            SubmitChanges();
            return HomePageLink;

        }
        public void Update(XsiHomePageLink entity)
        {
            XsiHomePageLink HomePageLink = XsiContext.Context.XsiHomePageLink.Find(entity.ItemId);
            XsiContext.Context.Entry(HomePageLink).State = EntityState.Modified;

            
            if (!entity.Title.IsDefault())
                HomePageLink.Title = entity.Title.Trim();

            if (!entity.PageUrl.IsDefault())
                HomePageLink.PageUrl = entity.PageUrl.Trim();
 
            if (!entity.SortOrder.IsDefault())
                HomePageLink.SortOrder = entity.SortOrder;

            if (!entity.IsExternal.IsDefault())
                HomePageLink.IsExternal = entity.IsExternal;

            if (!entity.Thumbnail.IsDefault())
                HomePageLink.Thumbnail = entity.Thumbnail;

            if (!entity.IsActive.IsDefault())
                HomePageLink.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                HomePageLink.TitleAr = entity.TitleAr.Trim();
            if (!entity.PageUrlAr.IsDefault())
                HomePageLink.PageUrlAr = entity.PageUrlAr.Trim();
            if (!entity.ThumbnailAr.IsDefault())
                HomePageLink.ThumbnailAr = entity.ThumbnailAr;
            if (!entity.IsActiveAr.IsDefault())
                HomePageLink.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedBy.IsDefault())
                HomePageLink.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                HomePageLink.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiHomePageLink entity)
        {
            foreach (XsiHomePageLink HomePageLink in Select(entity))
            {
                XsiHomePageLink aHomePageLink = XsiContext.Context.XsiHomePageLink.Find(HomePageLink.ItemId);
                XsiContext.Context.XsiHomePageLink.Remove(aHomePageLink);
            }
        }
        public void Delete(long itemId)
        {
            XsiHomePageLink aHomePageLink = XsiContext.Context.XsiHomePageLink.Find(itemId);
            XsiContext.Context.XsiHomePageLink.Remove(aHomePageLink);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFActivitiesBannerRepository : ISCRFActivitiesBannerRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFActivitiesBannerRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFActivitiesBannerRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFActivitiesBannerRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfactivitiesBanner GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfactivitiesBanner.Find(itemId);
        }

        public List<XsiScrfactivitiesBanner> Select()
        {
            return XsiContext.Context.XsiScrfactivitiesBanner.ToList();
        }
        public List<XsiScrfactivitiesBanner> Select(XsiScrfactivitiesBanner entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfactivitiesBanner>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));
            if (!entity.HomeBanner.IsDefault())
                predicate = predicate.And(p => p.HomeBanner.Equals(entity.HomeBanner));
            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
            return XsiContext.Context.XsiScrfactivitiesBanner.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiScrfactivitiesBanner> SelectOr(XsiScrfactivitiesBanner entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfactivitiesBanner>();
            var Outer = PredicateBuilder.True<XsiScrfactivitiesBanner>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
            
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiScrfactivitiesBanner.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiScrfactivitiesBanner Add(XsiScrfactivitiesBanner entity)
        {
            XsiScrfactivitiesBanner SCRFActivitiesBanner = new XsiScrfactivitiesBanner();
            if (!entity.Title.IsDefault())
            SCRFActivitiesBanner.Title = entity.Title.Trim();
            SCRFActivitiesBanner.HomeBanner = entity.HomeBanner;
            SCRFActivitiesBanner.Url = entity.Url.Trim();
            SCRFActivitiesBanner.IsActive = entity.IsActive;
            SCRFActivitiesBanner.CreatedOn = entity.CreatedOn;
            SCRFActivitiesBanner.CreatedBy = entity.CreatedBy;
            SCRFActivitiesBanner.ModifiedOn = entity.ModifiedOn;
            SCRFActivitiesBanner.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfactivitiesBanner.Add(SCRFActivitiesBanner);
            SubmitChanges();
            return SCRFActivitiesBanner;

        }
        public void Update(XsiScrfactivitiesBanner entity)
        {
            XsiScrfactivitiesBanner SCRFActivitiesBanner = XsiContext.Context.XsiScrfactivitiesBanner.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFActivitiesBanner).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                SCRFActivitiesBanner.Title = entity.Title.Trim();
            if (!entity.HomeBanner.IsDefault())
                SCRFActivitiesBanner.HomeBanner = entity.HomeBanner;
            if (!entity.Url.IsDefault())
                SCRFActivitiesBanner.Url = entity.Url.Trim();
            if (!entity.IsActive.IsDefault())
                SCRFActivitiesBanner.IsActive = entity.IsActive;
           
            if (!entity.ModifiedOn.IsDefault())
                SCRFActivitiesBanner.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                SCRFActivitiesBanner.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiScrfactivitiesBanner entity)
        {
            foreach (XsiScrfactivitiesBanner SCRFActivitiesBanner in Select(entity))
            {
                XsiScrfactivitiesBanner aSCRFActivitiesBanner = XsiContext.Context.XsiScrfactivitiesBanner.Find(SCRFActivitiesBanner.ItemId);
                XsiContext.Context.XsiScrfactivitiesBanner.Remove(aSCRFActivitiesBanner);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfactivitiesBanner aSCRFActivitiesBanner = XsiContext.Context.XsiScrfactivitiesBanner.Find(itemId);
            XsiContext.Context.XsiScrfactivitiesBanner.Remove(aSCRFActivitiesBanner);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
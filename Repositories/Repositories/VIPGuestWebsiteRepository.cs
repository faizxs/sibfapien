﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class VIPGuestWebsiteRepository : IVIPGuestWebsiteRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public VIPGuestWebsiteRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public VIPGuestWebsiteRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public VIPGuestWebsiteRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiVipguestWebsite GetById(long itemId)
        {
            return XsiContext.Context.XsiVipguestWebsite.Find(itemId);
        }
        public List<XsiVipguestWebsite> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiVipguestWebsite> Select()
        {
            return XsiContext.Context.XsiVipguestWebsite.ToList();
        }
        public List<XsiVipguestWebsite> Select(XsiVipguestWebsite entity)
        {
            var predicate = PredicateBuilder.True<XsiVipguestWebsite>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.VipguestId.IsDefault())
                predicate = predicate.And(p => p.VipguestId == entity.VipguestId);

            return XsiContext.Context.XsiVipguestWebsite.AsExpandable().Where(predicate).ToList();
        }

        public List<long?> Select(long ID1, long ID2, long ID3)
        {
            var predicate = PredicateBuilder.True<XsiVipguestWebsite>();
            if (ID1 != -1)
                predicate = predicate.And(p => p.WebsiteId == ID1);

            if (ID2 != -1)
                predicate = predicate.And(p => p.WebsiteId == ID2);

            if (ID3 != -1)
                predicate = predicate.And(p => p.WebsiteId == ID3);

            return XsiContext.Context.XsiVipguestWebsite.AsExpandable().Where(predicate).Select(s => s.VipguestId).ToList();
        }

        public XsiVipguestWebsite Add(XsiVipguestWebsite entity)
        {
            XsiVipguestWebsite VIPGuestWebsite = new XsiVipguestWebsite();
            VIPGuestWebsite.VipguestId = entity.VipguestId;
            VIPGuestWebsite.WebsiteId = entity.WebsiteId;

            XsiContext.Context.XsiVipguestWebsite.Add(VIPGuestWebsite);
            SubmitChanges();
            return VIPGuestWebsite;

        }
        public void Update(XsiVipguestWebsite entity)
        {
            XsiVipguestWebsite VIPGuestWebsite = XsiContext.Context.XsiVipguestWebsite.Find(entity.ItemId);
            XsiContext.Context.Entry(VIPGuestWebsite).State = EntityState.Modified;

            if (!entity.VipguestId.IsDefault())
                VIPGuestWebsite.VipguestId = entity.VipguestId;

            if (!entity.WebsiteId.IsDefault())
                VIPGuestWebsite.WebsiteId = entity.WebsiteId;

        }
        public void Delete(XsiVipguestWebsite entity)
        {
            foreach (XsiVipguestWebsite VIPGuestWebsite in Select(entity))
            {
                XsiVipguestWebsite aVIPGuestWebsite = XsiContext.Context.XsiVipguestWebsite.Find(VIPGuestWebsite.ItemId);
                XsiContext.Context.XsiVipguestWebsite.Remove(aVIPGuestWebsite);
            }
        }
        public void Delete(long itemId)
        {
            XsiVipguestWebsite aVIPGuestWebsite = XsiContext.Context.XsiVipguestWebsite.Find(itemId);
            XsiContext.Context.XsiVipguestWebsite.Remove(aVIPGuestWebsite);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
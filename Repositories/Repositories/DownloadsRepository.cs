﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class DownloadsRepository : IDownloadsRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public DownloadsRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public DownloadsRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public DownloadsRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiDownloads GetById(long itemId)
        {
            return XsiContext.Context.XsiDownloads.Find(itemId);
        }

        public List<XsiDownloads> Select()
        {
            return XsiContext.Context.XsiDownloads.ToList();
        }
        public List<XsiDownloads> Select(XsiDownloads entity)
        {
            var predicate = PredicateBuilder.True<XsiDownloads>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.DownloadCategoryId.IsDefault())
                predicate = predicate.And(p => p.DownloadCategoryId == entity.DownloadCategoryId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated == (entity.Dated));

            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail.Equals(entity.Thumbnail));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Equals(entity.FileName));

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiDownloads.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiDownloads> SelectComeplete(XsiDownloads entity, long langId)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiDownloads>();
            var predicate = PredicateBuilder.True<XsiDownloads>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.DownloadCategoryId.IsDefault())
                predicate = predicate.And(p => p.DownloadCategoryId == entity.DownloadCategoryId);
            if (langId == 1)
            {
                if (!entity.Title.IsDefault())
                    predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title.ToLower()));
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    predicate = predicate.And(p => p.Title.Contains(entity.Title));
                    string strSearchText = entity.Title;
                    var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                    Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                        || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                        || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                        || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                        );

                    isInner = true;
                }
            }
            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated == (entity.Dated));

            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail.Equals(entity.Thumbnail));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Equals(entity.FileName));

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiDownloads.Include(p => p.DownloadCategory).AsExpandable().Where(predicate).ToList();
        }

        public XsiDownloads Add(XsiDownloads entity)
        {
            XsiDownloads Downloads = new XsiDownloads();

            Downloads.DownloadCategoryId = entity.DownloadCategoryId;
            Downloads.WebsiteId = entity.WebsiteId;
            Downloads.Dated = entity.Dated;

            if (!entity.Title.IsDefault())
                Downloads.Title = entity.Title.Trim();
            Downloads.Thumbnail = entity.Thumbnail;
            Downloads.FileName = entity.FileName;
            if (!entity.Overview.IsDefault())
                Downloads.Overview = entity.Overview.Trim();

            #region Ar
            if (!entity.TitleAr.IsDefault())
                Downloads.TitleAr = entity.TitleAr.Trim();
            Downloads.ThumbnailAr = entity.ThumbnailAr;
            Downloads.FileNameAr = entity.FileNameAr;
            if (!entity.OverviewAr.IsDefault())
                Downloads.OverviewAr = entity.OverviewAr.Trim();

            Downloads.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            Downloads.IsActive = entity.IsActive;
            Downloads.CreatedOn = entity.CreatedOn;
            Downloads.CreatedBy = entity.CreatedBy;
            Downloads.ModifiedOn = entity.ModifiedOn;
            Downloads.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiDownloads.Add(Downloads);
            SubmitChanges();
            return Downloads;

        }
        public void Update(XsiDownloads entity)
        {
            XsiDownloads Downloads = XsiContext.Context.XsiDownloads.Find(entity.ItemId);
            XsiContext.Context.Entry(Downloads).State = EntityState.Modified;

            if (!entity.Dated.IsDefault())
                Downloads.Dated = entity.Dated;

            if (!entity.Title.IsDefault())
                Downloads.Title = entity.Title.Trim();

            if (!entity.Thumbnail.IsDefault())
                Downloads.Thumbnail = entity.Thumbnail;

            if (!entity.FileName.IsDefault())
                Downloads.FileName = entity.FileName;

            if (!entity.Overview.IsDefault())
                Downloads.Overview = entity.Overview.Trim();

            if (!entity.WebsiteId.IsDefault())
                Downloads.WebsiteId = entity.WebsiteId;

            if (!entity.IsActive.IsDefault())
                Downloads.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                Downloads.TitleAr = entity.TitleAr.Trim();

            if (!entity.ThumbnailAr.IsDefault())
                Downloads.ThumbnailAr = entity.ThumbnailAr;

            if (!entity.FileNameAr.IsDefault())
                Downloads.FileNameAr = entity.FileNameAr;

            if (!entity.OverviewAr.IsDefault())
                Downloads.OverviewAr = entity.OverviewAr.Trim();

            if (!entity.IsActiveAr.IsDefault())
                Downloads.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                Downloads.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                Downloads.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiDownloads entity)
        {
            foreach (XsiDownloads Downloads in Select(entity))
            {
                XsiDownloads aDownloads = XsiContext.Context.XsiDownloads.Find(Downloads.ItemId);
                XsiContext.Context.XsiDownloads.Remove(aDownloads);
            }
        }
        public void Delete(long itemId)
        {
            XsiDownloads aDownloads = XsiContext.Context.XsiDownloads.Find(itemId);
            XsiContext.Context.XsiDownloads.Remove(aDownloads);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
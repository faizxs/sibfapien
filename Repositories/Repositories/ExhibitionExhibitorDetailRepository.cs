﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionExhibitorDetailRepository : IExhibitionExhibitorDetailRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionExhibitorDetailRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionExhibitorDetailRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionExhibitorDetailRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionExhibitorDetails GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionExhibitorDetails.Find(itemId);
        }
        public List<XsiExhibitionExhibitorDetails> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionExhibitorDetails> Select()
        {
            return XsiContext.Context.XsiExhibitionExhibitorDetails.ToList();
        }
        public List<XsiExhibitionExhibitorDetails> Select(XsiExhibitionExhibitorDetails entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionExhibitorDetails>();
            if (!entity.ExhibitorDetailsId.IsDefault())
                predicate = predicate.And(p => p.ExhibitorDetailsId == entity.ExhibitorDetailsId);
             

            if (!entity.BoothSectionId.IsDefault())
                predicate = predicate.And(p => p.BoothSectionId == entity.BoothSectionId);

            if (!entity.BoothSubSectionId.IsDefault())
                predicate = predicate.And(p => p.BoothSubSectionId == entity.BoothSubSectionId);

            if (!entity.MemberExhibitionYearlyId.IsDefault())
                predicate = predicate.And(p => p.MemberExhibitionYearlyId == entity.MemberExhibitionYearlyId);
             
            if (!entity.AllocatedSpace.IsDefault())
                predicate = predicate.And(p => p.AllocatedSpace.Equals(entity.AllocatedSpace));

            if (!entity.AllocatedAreaType.IsDefault())
                predicate = predicate.And(p => p.AllocatedAreaType.Equals(entity.AllocatedAreaType));

            if (!entity.Area.IsDefault())
                predicate = predicate.And(p => p.Area.Equals(entity.Area));

            if (!entity.RequiredAreaId.IsDefault())
                predicate = predicate.And(p => p.RequiredAreaId == entity.RequiredAreaId);

            if (!entity.RequiredAreaType.IsDefault())
                predicate = predicate.And(p => p.RequiredAreaType.Equals(entity.RequiredAreaType));

            if (!entity.CalculatedArea.IsDefault())
                predicate = predicate.And(p => p.CalculatedArea.Equals(entity.CalculatedArea));

            if (!entity.HallNumber.IsDefault())
                predicate = predicate.And(p => p.HallNumber.Equals(entity.HallNumber));

            if (!entity.StandNumberStart.IsDefault())
                predicate = predicate.And(p => p.StandNumberStart.Equals(entity.StandNumberStart));

            if (!entity.StandNumberEnd.IsDefault())
                predicate = predicate.And(p => p.StandNumberEnd.Equals(entity.StandNumberEnd));

            if (!entity.FirstLocation.IsDefault())
                predicate = predicate.And(p => p.FirstLocation.Equals(entity.FirstLocation));

            if (!entity.SecondLocation.IsDefault())
                predicate = predicate.And(p => p.SecondLocation.Equals(entity.SecondLocation));

            if (!entity.ThirdLocation.IsDefault())
                predicate = predicate.And(p => p.ThirdLocation.Equals(entity.ThirdLocation));

            if (!entity.FourthLocation.IsDefault())
                predicate = predicate.And(p => p.FourthLocation.Equals(entity.FourthLocation));

            if (!entity.StandCode.IsDefault())
                predicate = predicate.And(p => p.StandCode.Equals(entity.StandCode));

            if (!entity.UploadWarningLetter.IsDefault())
                predicate = predicate.And(p => p.UploadWarningLetter.Equals(entity.UploadWarningLetter));

            if (!entity.CancellationReason.IsDefault())
                predicate = predicate.And(p => p.CancellationReason.Contains(entity.CancellationReason));

            if (!entity.TradeLicence.IsDefault())
                predicate = predicate.And(p => p.TradeLicence.Equals(entity.TradeLicence));

            if (!entity.Brief.IsDefault())
                predicate = predicate.And(p => p.Brief.Contains(entity.Brief));

            if (!entity.BriefAr.IsDefault())
                predicate = predicate.And(p => p.BriefAr.Contains(entity.BriefAr));

            if (!entity.Apu.IsDefault())
                predicate = predicate.And(p => p.Apu.Equals(entity.Apu));

            if (!entity.BoothDetail.IsDefault())
                predicate = predicate.And(p => p.BoothDetail.Equals(entity.BoothDetail));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiExhibitionExhibitorDetails.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionExhibitorDetails Add(XsiExhibitionExhibitorDetails entity)
        {
            XsiExhibitionExhibitorDetails ExhibitorRegistrationDetails = new XsiExhibitionExhibitorDetails();

            ExhibitorRegistrationDetails.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;
            if (!entity.Apu.IsDefault())
                ExhibitorRegistrationDetails.Apu = entity.Apu.Trim();

            ExhibitorRegistrationDetails.BoothSectionId = entity.BoothSectionId;
            ExhibitorRegistrationDetails.BoothSubSectionId = entity.BoothSubSectionId;
            if (!entity.AllocatedSpace.IsDefault())
                ExhibitorRegistrationDetails.AllocatedSpace = entity.AllocatedSpace.Trim();
            ExhibitorRegistrationDetails.AllocatedAreaType = entity.AllocatedAreaType;
            if (!entity.Area.IsDefault())
                ExhibitorRegistrationDetails.Area = entity.Area.Trim();
            ExhibitorRegistrationDetails.RequiredAreaId = entity.RequiredAreaId;
            ExhibitorRegistrationDetails.RequiredAreaType = entity.RequiredAreaType;
            if (!entity.CalculatedArea.IsDefault())
                ExhibitorRegistrationDetails.CalculatedArea = entity.CalculatedArea.Trim();
            if (!entity.HallNumber.IsDefault())
                ExhibitorRegistrationDetails.HallNumber = entity.HallNumber.Trim();
            if (!entity.StandNumberStart.IsDefault())
                ExhibitorRegistrationDetails.StandNumberStart = entity.StandNumberStart.Trim();
            if (!entity.StandNumberEnd.IsDefault())
                ExhibitorRegistrationDetails.StandNumberEnd = entity.StandNumberEnd.Trim();
            if (!entity.FirstLocation.IsDefault())
                ExhibitorRegistrationDetails.FirstLocation = entity.FirstLocation.Trim();
            if (!entity.SecondLocation.IsDefault())
                ExhibitorRegistrationDetails.SecondLocation = entity.SecondLocation.Trim();
            if (!entity.ThirdLocation.IsDefault())
                ExhibitorRegistrationDetails.ThirdLocation = entity.ThirdLocation.Trim();
            if (!entity.FourthLocation.IsDefault())
                ExhibitorRegistrationDetails.FourthLocation = entity.FourthLocation.Trim();
            if (!entity.StandCode.IsDefault())
                ExhibitorRegistrationDetails.StandCode = entity.StandCode.Trim();
            ExhibitorRegistrationDetails.UploadWarningLetter = entity.UploadWarningLetter;
            if (!entity.CancellationReason.IsDefault())
                ExhibitorRegistrationDetails.CancellationReason = entity.CancellationReason.Trim();
            ExhibitorRegistrationDetails.TradeLicence = entity.TradeLicence;
            if (!entity.Brief.IsDefault())
                ExhibitorRegistrationDetails.Brief = entity.Brief.Trim();
            if (!entity.BriefAr.IsDefault())
                ExhibitorRegistrationDetails.BriefAr = entity.BriefAr.Trim();
            if (!entity.BoothDetail.IsDefault())
                ExhibitorRegistrationDetails.BoothDetail = entity.BoothDetail.Trim();

            ExhibitorRegistrationDetails.CreatedOn = entity.CreatedOn;
            ExhibitorRegistrationDetails.CreatedBy = entity.CreatedBy;
            ExhibitorRegistrationDetails.ModifiedOn = entity.ModifiedOn;
            ExhibitorRegistrationDetails.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionExhibitorDetails.Add(ExhibitorRegistrationDetails);
            SubmitChanges(); 
            return ExhibitorRegistrationDetails;

        }
        public void Update(XsiExhibitionExhibitorDetails entity)
        {
            XsiExhibitionExhibitorDetails ExhibitorRegistrationDetails = XsiContext.Context.XsiExhibitionExhibitorDetails.Find(entity.ExhibitorDetailsId);
            XsiContext.Context.Entry(ExhibitorRegistrationDetails).State = EntityState.Modified;


            if (!entity.BoothSectionId.IsDefault())
                ExhibitorRegistrationDetails.BoothSectionId = entity.BoothSectionId;

            if (!entity.BoothSubSectionId.IsDefault())
                ExhibitorRegistrationDetails.BoothSubSectionId = entity.BoothSubSectionId;

            if (!entity.AllocatedSpace.IsDefault())
                ExhibitorRegistrationDetails.AllocatedSpace = entity.AllocatedSpace.Trim();

            if (!entity.AllocatedAreaType.IsDefault())
                ExhibitorRegistrationDetails.AllocatedAreaType = entity.AllocatedAreaType;

            if (!entity.Area.IsDefault())
                ExhibitorRegistrationDetails.Area = entity.Area.Trim();

            if (!entity.RequiredAreaId.IsDefault())
                ExhibitorRegistrationDetails.RequiredAreaId = entity.RequiredAreaId;

            if (!entity.RequiredAreaType.IsDefault())
                ExhibitorRegistrationDetails.RequiredAreaType = entity.RequiredAreaType;

            if (!entity.CalculatedArea.IsDefault())
                ExhibitorRegistrationDetails.CalculatedArea = entity.CalculatedArea.Trim();

            if (!entity.HallNumber.IsDefault())
                ExhibitorRegistrationDetails.HallNumber = entity.HallNumber.Trim();

            if (!entity.StandNumberStart.IsDefault())
                ExhibitorRegistrationDetails.StandNumberStart = entity.StandNumberStart.Trim();

            if (!entity.StandNumberEnd.IsDefault())
                ExhibitorRegistrationDetails.StandNumberEnd = entity.StandNumberEnd.Trim();

            if (!entity.FirstLocation.IsDefault())
                ExhibitorRegistrationDetails.FirstLocation = entity.FirstLocation.Trim();

            if (!entity.SecondLocation.IsDefault())
                ExhibitorRegistrationDetails.SecondLocation = entity.SecondLocation.Trim();

            if (!entity.ThirdLocation.IsDefault())
                ExhibitorRegistrationDetails.ThirdLocation = entity.ThirdLocation.Trim();

            if (!entity.FourthLocation.IsDefault())
                ExhibitorRegistrationDetails.FourthLocation = entity.FourthLocation.Trim();

            if (!entity.StandCode.IsDefault())
                ExhibitorRegistrationDetails.StandCode = entity.StandCode.Trim();

            if (!entity.UploadWarningLetter.IsDefault())
                ExhibitorRegistrationDetails.UploadWarningLetter = entity.UploadWarningLetter;

            if (!entity.CancellationReason.IsDefault())
                ExhibitorRegistrationDetails.CancellationReason = entity.CancellationReason.Trim();


            if (!entity.TradeLicence.IsDefault())
                ExhibitorRegistrationDetails.TradeLicence = entity.TradeLicence;

            if (!entity.Apu.IsDefault())
                ExhibitorRegistrationDetails.Apu = entity.Apu.Trim();

            if (!entity.Brief.IsDefault())
                ExhibitorRegistrationDetails.Brief = entity.Brief.Trim();

            if (!entity.BriefAr.IsDefault())
                ExhibitorRegistrationDetails.BriefAr = entity.BriefAr.Trim();

            if (!entity.BoothDetail.IsDefault())
                ExhibitorRegistrationDetails.BoothDetail = entity.BoothDetail.Trim();
        }
        public void Delete(XsiExhibitionExhibitorDetails entity)
        { 
            foreach (XsiExhibitionExhibitorDetails ExhibitorRegistrationDetails in Select(entity))
            {
                var aExhibitorRegistration = XsiContext.Context.XsiExhibitionExhibitorDetails.Find(ExhibitorRegistrationDetails.ExhibitorDetailsId);
                XsiContext.Context.XsiExhibitionExhibitorDetails.Remove(aExhibitorRegistration);
            }
        }
        public void Delete(long itemId)
        {
            var aExhibitorRegistration = XsiContext.Context.XsiExhibitionExhibitorDetails.Find(itemId);
            XsiContext.Context.XsiExhibitionExhibitorDetails.Remove(aExhibitorRegistration);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
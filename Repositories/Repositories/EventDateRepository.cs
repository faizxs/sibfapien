﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class EventDateRepository : IEventDateRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public EventDateRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public EventDateRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public EventDateRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiEventDate GetById(long itemId)
        {
            return XsiContext.Context.XsiEventDate.Find(itemId);
        }

        public List<XsiEventDate> Select()
        {
            return XsiContext.Context.XsiEventDate.ToList();
        }
        public List<XsiEventDate> Select(XsiEventDate entity)
        {
            var predicate = PredicateBuilder.True<XsiEventDate>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.EventId.IsDefault())
                predicate = predicate.And(p => p.EventId == entity.EventId);

            if (!entity.StartDate.IsDefault())
                predicate = predicate.And(p => p.StartDate == entity.StartDate);

            if (!entity.EndDate.IsDefault())
                predicate = predicate.And(p => p.EndDate == entity.EndDate);

            if (!entity.EventusIden.IsDefault())
                predicate = predicate.And(p => p.EventusIden.Equals(entity.EventusIden));

            if (!entity.EventusIdar.IsDefault())
                predicate = predicate.And(p => p.EventusIdar.Equals(entity.EventusIdar));

            return XsiContext.Context.XsiEventDate.AsExpandable().Where(predicate).ToList();
        }

        public XsiEventDate Add(XsiEventDate entity)
        {
            XsiEventDate EventDate = new XsiEventDate();
            EventDate.EventId = entity.EventId;
            EventDate.StartDate = entity.StartDate;
            EventDate.EndDate = entity.EndDate;

            if (!entity.EventusIden.IsDefault())
                EventDate.EventusIden = entity.EventusIden;

            if (!entity.EventusIdar.IsDefault())
                EventDate.EventusIdar = entity.EventusIdar;

            XsiContext.Context.XsiEventDate.Add(EventDate);
            SubmitChanges();
            return EventDate;

        }
        public void Update(XsiEventDate entity)
        {
            XsiEventDate EventDate = XsiContext.Context.XsiEventDate.Find(entity.ItemId);
            XsiContext.Context.Entry(EventDate).State = EntityState.Modified;

            if (!entity.EventId.IsDefault())
                EventDate.EventId = entity.EventId;

            if (!entity.StartDate.IsDefault())
                EventDate.StartDate = entity.StartDate;

            if (!entity.EndDate.IsDefault())
                EventDate.EndDate = entity.EndDate;

            if (!entity.EventusIden.IsDefault())
                EventDate.EventusIden = entity.EventusIden;

            if (!entity.EventusIdar.IsDefault())
                EventDate.EventusIdar = entity.EventusIdar;

        }
        public void Delete(XsiEventDate entity)
        {
            foreach (XsiEventDate EventDate in Select(entity))
            {
                XsiEventDate aEventDate = XsiContext.Context.XsiEventDate.Find(EventDate.ItemId);
                XsiContext.Context.XsiEventDate.Remove(aEventDate);
            }
        }
        public void Delete(long itemId)
        {
            XsiEventDate aEventDate = XsiContext.Context.XsiEventDate.Find(itemId);
            XsiContext.Context.XsiEventDate.Remove(aEventDate);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFVideoRepository : ISCRFVideoRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFVideoRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFVideoRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFVideoRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfvideo GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfvideo.Find(itemId);
        }

        public List<XsiScrfvideo> Select()
        {
            return XsiContext.Context.XsiScrfvideo.ToList();
        }
        public List<XsiScrfvideo> Select(XsiScrfvideo entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfvideo>();
            var predicate = PredicateBuilder.True<XsiScrfvideo>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.AlbumId.IsDefault())
                predicate = predicate.And(p => p.AlbumId == entity.AlbumId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsAlbumCover.IsDefault())
                predicate = predicate.And(p => p.IsAlbumCover.Equals(entity.IsAlbumCover));

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail.Contains(entity.Thumbnail));

            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated == entity.Dated);

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            if (!entity.SortIndex.IsDefault())
                predicate = predicate.And(p => p.SortIndex == entity.SortIndex);

            if (!entity.SortIndexAr.IsDefault())
                predicate = predicate.And(p => p.SortIndexAr == entity.SortIndexAr);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiScrfvideo.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfvideo Add(XsiScrfvideo entity)
        {
            XsiScrfvideo Video = new XsiScrfvideo();
            Video.AlbumId = entity.AlbumId;
            Video.IsActive = entity.IsActive;
            Video.IsAlbumCover = entity.IsAlbumCover;
            if (!entity.Title.IsDefault())
                Video.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
                Video.Description = entity.Description.Trim();
            Video.Thumbnail = entity.Thumbnail;
            if (!entity.Url.IsDefault())
                Video.Url = entity.Url.Trim();
            Video.SortIndex = entity.SortIndex;
            Video.Dated = entity.Dated;
            Video.CreatedOn = entity.CreatedOn;
            Video.CreatedBy = entity.CreatedBy;
            Video.ModifiedOn = entity.ModifiedOn;
            Video.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfvideo.Add(Video);
            SubmitChanges();
            return Video;

        }
        public void Update(XsiScrfvideo entity)
        {
            XsiScrfvideo Video = XsiContext.Context.XsiScrfvideo.Find(entity.ItemId);
            XsiContext.Context.Entry(Video).State = EntityState.Modified;

            if (!entity.AlbumId.IsDefault())
                Video.AlbumId = entity.AlbumId;

            if (!entity.IsActive.IsDefault())
                Video.IsActive = entity.IsActive;

            if (!entity.IsAlbumCover.IsDefault())
                Video.IsAlbumCover = entity.IsAlbumCover;

            if (!entity.Title.IsDefault())
                Video.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                Video.Description = entity.Description.Trim();

            if (!entity.Thumbnail.IsDefault())
                Video.Thumbnail = entity.Thumbnail;

            if (!entity.Url.IsDefault())
                Video.Url = entity.Url.Trim();

            if (!entity.SortIndex.IsDefault())
                Video.SortIndex = entity.SortIndex;

            if (!entity.Dated.IsDefault())
                Video.Dated = entity.Dated;

            if (!entity.CreatedOn.IsDefault())
                Video.CreatedOn = entity.CreatedOn;

            if (!entity.CreatedBy.IsDefault())
                Video.CreatedBy = entity.CreatedBy;

            if (!entity.ModifiedOn.IsDefault())
                Video.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                Video.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiScrfvideo entity)
        {
            foreach (XsiScrfvideo Video in Select(entity))
            {
                XsiScrfvideo aVideo = XsiContext.Context.XsiScrfvideo.Find(Video.ItemId);
                XsiContext.Context.XsiScrfvideo.Remove(aVideo);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfvideo aVideo = XsiContext.Context.XsiScrfvideo.Find(itemId);
            XsiContext.Context.XsiScrfvideo.Remove(aVideo);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
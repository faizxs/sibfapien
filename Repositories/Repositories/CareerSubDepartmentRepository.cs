﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class CareerSubDepartmentRepository : ICareerSubDepartmentRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public CareerSubDepartmentRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public CareerSubDepartmentRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public CareerSubDepartmentRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiCareerSubDepartment GetById(long itemId)
        {
            return XsiContext.Context.XsiCareerSubDepartment.Find(itemId);
        }
         
        public List<XsiCareerSubDepartment> Select()
        {
            return XsiContext.Context.XsiCareerSubDepartment.ToList();
        }
        public List<XsiCareerSubDepartment> Select(XsiCareerSubDepartment entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiCareerSubDepartment>();
            var predicate = PredicateBuilder.True<XsiCareerSubDepartment>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
              
            if (!entity.DepartmentId.IsDefault())
                predicate = predicate.And(p => p.DepartmentId == entity.DepartmentId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiCareerSubDepartment.AsExpandable().Where(predicate).ToList();
        }

        public XsiCareerSubDepartment Add(XsiCareerSubDepartment entity)
        {
            XsiCareerSubDepartment CareerSubDepartment = new XsiCareerSubDepartment();
            
            CareerSubDepartment.DepartmentId = entity.DepartmentId;
            if (!entity.Title.IsDefault())
                CareerSubDepartment.Title = entity.Title.Trim();
            CareerSubDepartment.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerSubDepartment.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerSubDepartment.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            CareerSubDepartment.CreatedOn = entity.CreatedOn;
            CareerSubDepartment.CreatedBy = entity.CreatedBy;
            CareerSubDepartment.ModifiedOn = entity.ModifiedOn;
            CareerSubDepartment.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiCareerSubDepartment.Add(CareerSubDepartment);
            SubmitChanges();
            return CareerSubDepartment;

        }
        public void Update(XsiCareerSubDepartment entity)
        {
            XsiCareerSubDepartment CareerSubDepartment = XsiContext.Context.XsiCareerSubDepartment.Find(entity.ItemId);
            XsiContext.Context.Entry(CareerSubDepartment).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                CareerSubDepartment.Title = entity.Title.Trim();
            if (!entity.DepartmentId.IsDefault())
                CareerSubDepartment.DepartmentId = entity.DepartmentId;
            if (!entity.IsActive.IsDefault())
                CareerSubDepartment.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerSubDepartment.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerSubDepartment.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                CareerSubDepartment.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                CareerSubDepartment.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiCareerSubDepartment entity)
        {
            foreach (XsiCareerSubDepartment CareerSubDepartment in Select(entity))
            {
                XsiCareerSubDepartment aCareerSubDepartment = XsiContext.Context.XsiCareerSubDepartment.Find(CareerSubDepartment.ItemId);
                XsiContext.Context.XsiCareerSubDepartment.Remove(aCareerSubDepartment);
            }
        }
        public void Delete(long itemId)
        {
            XsiCareerSubDepartment aCareerSubDepartment = XsiContext.Context.XsiCareerSubDepartment.Find(itemId);
            XsiContext.Context.XsiCareerSubDepartment.Remove(aCareerSubDepartment);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
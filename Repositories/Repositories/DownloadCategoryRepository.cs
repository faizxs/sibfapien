﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class DownloadCategoryRepository : IDownloadCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public DownloadCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public DownloadCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public DownloadCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiDownloadCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiDownloadCategory.Find(itemId);
        }

        public List<XsiDownloadCategory> Select()
        {
            return XsiContext.Context.XsiDownloadCategory.ToList();
        }
        public List<XsiDownloadCategory> Select(XsiDownloadCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiDownloadCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiDownloadCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiDownloadCategory> SelectOtherLanguageCategory(XsiDownloadCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiDownloadCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
            predicate = PredicateBuilder.True<XsiDownloadCategory>();

            return XsiContext.Context.XsiDownloadCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiDownloadCategory> SelectCurrentLanguageCategory(XsiDownloadCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiDownloadCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            predicate = PredicateBuilder.True<XsiDownloadCategory>();
        
            return XsiContext.Context.XsiDownloadCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiDownloadCategory> SelectOr(XsiDownloadCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiDownloadCategory>();
            var Outer = PredicateBuilder.True<XsiDownloadCategory>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
             

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiDownloadCategory.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public XsiDownloadCategory Add(XsiDownloadCategory entity)
        {
            XsiDownloadCategory DownloadCategory = new XsiDownloadCategory();
            
            DownloadCategory.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                DownloadCategory.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
                DownloadCategory.Description = entity.Description.Trim();
           
            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                DownloadCategory.IsActiveAr = entity.IsActiveAr;

            if (!entity.TitleAr.IsDefault())
                DownloadCategory.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                DownloadCategory.DescriptionAr = entity.DescriptionAr.Trim();
            #endregion Ar

            DownloadCategory.CreatedOn = entity.CreatedOn;
            DownloadCategory.CreatedBy = entity.CreatedBy;
            DownloadCategory.ModifiedOn = entity.ModifiedOn;
            DownloadCategory.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiDownloadCategory.Add(DownloadCategory);
            SubmitChanges();
            return DownloadCategory;

        }
        public void Update(XsiDownloadCategory entity)
        {
            XsiDownloadCategory DownloadCategory = XsiContext.Context.XsiDownloadCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(DownloadCategory).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                DownloadCategory.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                DownloadCategory.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                DownloadCategory.Description = entity.Description.Trim();

            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                DownloadCategory.IsActiveAr = entity.IsActiveAr;

            if (!entity.TitleAr.IsDefault())
                DownloadCategory.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                DownloadCategory.DescriptionAr = entity.DescriptionAr.Trim();
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                DownloadCategory.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                DownloadCategory.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiDownloadCategory entity)
        {
            foreach (XsiDownloadCategory DownloadCategory in Select(entity))
            {
                XsiDownloadCategory aDownloadCategory = XsiContext.Context.XsiDownloadCategory.Find(DownloadCategory.ItemId);
                XsiContext.Context.XsiDownloadCategory.Remove(aDownloadCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiDownloadCategory aDownloadCategory = XsiContext.Context.XsiDownloadCategory.Find(itemId);
            XsiContext.Context.XsiDownloadCategory.Remove(aDownloadCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
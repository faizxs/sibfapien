﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using LinqKit;
using System.Data;
using Entities.Models;

namespace Xsi.Repositories
{
    public class AdminPermissionsRepository : IAdminPermissionRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public AdminPermissionsRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public AdminPermissionsRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public AdminPermissionsRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdminPermissions GetById(long itemId)
        {
            return XsiContext.Context.XsiAdminPermissions.Find(itemId);
        }
        public List<XsiAdminPermissions> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiAdminPermissions> Select()
        {
            return XsiContext.Context.XsiAdminPermissions.ToList();
        }
        public List<XsiAdminPermissions> Select(XsiAdminPermissions entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminPermissions>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.RoleId.IsDefault())
                predicate = predicate.And(p => p.RoleId == entity.RoleId);

            if (!entity.UserId.IsDefault())
                predicate = predicate.And(p => p.UserId == entity.UserId);

            if (!entity.PageId.IsDefault())
                predicate = predicate.And(p => p.PageId == entity.PageId);

            if (!entity.IsRead.IsDefault())
                predicate = predicate.And(p => p.IsRead == entity.IsRead);

            if (!entity.IsWrite.IsDefault())
                predicate = predicate.And(p => p.IsWrite == entity.IsWrite);

            if (!entity.IsDelete.IsDefault())
                predicate = predicate.And(p => p.IsDelete == entity.IsDelete);

            return XsiContext.Context.XsiAdminPermissions.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiAdminPermissions> SelectAdminPermissionAndUser(XsiAdminPermissions entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminPermissions>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.RoleId.IsDefault())
                predicate = predicate.And(p => p.RoleId == entity.RoleId);

            if (!entity.UserId.IsDefault())
                predicate = predicate.And(p => p.UserId == entity.UserId);

            if (!entity.PageId.IsDefault())
                predicate = predicate.And(p => p.PageId == entity.PageId);

            if (!entity.IsRead.IsDefault())
                predicate = predicate.And(p => p.IsRead == entity.IsRead);

            if (!entity.IsWrite.IsDefault())
                predicate = predicate.And(p => p.IsWrite == entity.IsWrite);

            if (!entity.IsDelete.IsDefault())
                predicate = predicate.And(p => p.IsDelete == entity.IsDelete);

            return XsiContext.Context.XsiAdminPermissions.Include(u => u.Role).AsExpandable().Where(predicate).ToList();
        }
        public List<XsiAdminPermissions> SelectAdminPermissionAndRole(XsiAdminPermissions entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminPermissions>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.RoleId.IsDefault())
                predicate = predicate.And(p => p.RoleId == entity.RoleId);

            if (!entity.UserId.IsDefault())
                predicate = predicate.And(p => p.UserId == entity.UserId);

            if (!entity.PageId.IsDefault())
                predicate = predicate.And(p => p.PageId == entity.PageId);

            if (!entity.IsRead.IsDefault())
                predicate = predicate.And(p => p.IsRead == entity.IsRead);

            if (!entity.IsWrite.IsDefault())
                predicate = predicate.And(p => p.IsWrite == entity.IsWrite);

            if (!entity.IsDelete.IsDefault())
                predicate = predicate.And(p => p.IsDelete == entity.IsDelete);

            return XsiContext.Context.XsiAdminPermissions.Include(r => r.Role).AsExpandable().Where(predicate).ToList();
        }
        public List<XsiAdminPermissions> SelectAdminPermissionAndPage(XsiAdminPermissions entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminPermissions>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.RoleId.IsDefault())
                predicate = predicate.And(p => p.RoleId == entity.RoleId);

            if (!entity.UserId.IsDefault())
                predicate = predicate.And(p => p.UserId == entity.UserId);

            if (!entity.PageId.IsDefault())
                predicate = predicate.And(p => p.PageId == entity.PageId);

            if (!entity.IsRead.IsDefault())
                predicate = predicate.And(p => p.IsRead == entity.IsRead);

            if (!entity.IsWrite.IsDefault())
                predicate = predicate.And(p => p.IsWrite == entity.IsWrite);

            if (!entity.IsDelete.IsDefault())
                predicate = predicate.And(p => p.IsDelete == entity.IsDelete);

            if (!entity.IsCreate.IsDefault())
                predicate = predicate.And(p => p.IsCreate == entity.IsCreate);

            return XsiContext.Context.XsiAdminPermissions.Include(p => p.Page).AsExpandable().Where(predicate).ToList();
        }

        public XsiAdminPermissions Add(XsiAdminPermissions entity)
        {
            XsiAdminPermissions AdminPermission = new XsiAdminPermissions();
            AdminPermission.RoleId = entity.RoleId;
            AdminPermission.UserId = entity.UserId;
            AdminPermission.PageId = entity.PageId;
            AdminPermission.IsRead = entity.IsRead;
            AdminPermission.IsWrite = entity.IsWrite;
            AdminPermission.IsDelete = entity.IsDelete;
            AdminPermission.IsCreate = entity.IsCreate;

            XsiContext.Context.XsiAdminPermissions.Add(AdminPermission);

            SubmitChanges();
            return AdminPermission;
        }
        public void Update(XsiAdminPermissions entity)
        {
            XsiAdminPermissions AdminPermission = XsiContext.Context.XsiAdminPermissions.Find(entity.ItemId);
            XsiContext.Context.Entry(AdminPermission).State = EntityState.Modified;

            if (!entity.RoleId.IsDefault())
                AdminPermission.RoleId = entity.RoleId;

            if (!entity.UserId.IsDefault())
                AdminPermission.UserId = entity.UserId;

            if (!entity.PageId.IsDefault())
                AdminPermission.PageId = entity.PageId;

            if (!entity.IsRead.IsDefault())
                AdminPermission.IsRead = entity.IsRead;

            if (!entity.IsWrite.IsDefault())
                AdminPermission.IsWrite = entity.IsWrite;

            if (!entity.IsDelete.IsDefault())
                AdminPermission.IsDelete = entity.IsDelete;

            if (!entity.IsCreate.IsDefault())
                AdminPermission.IsCreate = entity.IsCreate;
        }
        public void Delete(XsiAdminPermissions entity)
        {
            foreach (XsiAdminPermissions AdminPermission in Select(entity))
            {
                XsiAdminPermissions apermission = XsiContext.Context.XsiAdminPermissions.Find(AdminPermission.ItemId);
                XsiContext.Context.XsiAdminPermissions.Remove(apermission);
            }
        }
        public void Delete(long itemId)
        {
            XsiAdminPermissions apermission = XsiContext.Context.XsiAdminPermissions.Find(itemId);
            XsiContext.Context.XsiAdminPermissions.Remove(apermission);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}

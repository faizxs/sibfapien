﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class AdvertisementPageRepository : IAdvertisementPageRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public AdvertisementPageRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public AdvertisementPageRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public AdvertisementPageRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdvertisementPage GetById(long itemId)
        {
            return XsiContext.Context.XsiAdvertisementPage.Find(itemId);
        }
        public List<XsiAdvertisementPage> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiAdvertisementPage> Select()
        {
            return XsiContext.Context.XsiAdvertisementPage.ToList();
        }
        public List<XsiAdvertisementPage> Select(XsiAdvertisementPage entity)
        {
            var predicate = PredicateBuilder.True<XsiAdvertisementPage>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.AdvertisementId.IsDefault())
                predicate = predicate.And(p => p.AdvertisementId == entity.AdvertisementId);

            if (!entity.PageId.IsDefault())
                predicate = predicate.And(p => p.PageId == entity.PageId);


            return XsiContext.Context.XsiAdvertisementPage.AsExpandable().Where(predicate).ToList();
        }

        public XsiAdvertisementPage Add(XsiAdvertisementPage entity)
        {
            XsiAdvertisementPage AdvertisementPage = new XsiAdvertisementPage();
            AdvertisementPage.AdvertisementId = entity.AdvertisementId;
            AdvertisementPage.PageId = entity.PageId;

            XsiContext.Context.XsiAdvertisementPage.Add(AdvertisementPage);
            SubmitChanges();
            return AdvertisementPage;

        }
        public void Update(XsiAdvertisementPage entity)
        {
            XsiAdvertisementPage AdvertisementPage = XsiContext.Context.XsiAdvertisementPage.Find(entity.ItemId);
            XsiContext.Context.Entry(AdvertisementPage).State = EntityState.Modified;

            if (!entity.AdvertisementId.IsDefault())
                AdvertisementPage.AdvertisementId = entity.AdvertisementId;
            if (!entity.PageId.IsDefault())
                AdvertisementPage.PageId = entity.PageId;
        }
        public void Delete(XsiAdvertisementPage entity)
        {
            foreach (XsiAdvertisementPage AdvertisementPage in Select(entity))
            {
                XsiAdvertisementPage aAdvertisementPage = XsiContext.Context.XsiAdvertisementPage.Find(AdvertisementPage.ItemId);
                XsiContext.Context.XsiAdvertisementPage.Remove(aAdvertisementPage);
            }
        }
        public void Delete(long itemId)
        {
            XsiAdvertisementPage aAdvertisementPage = XsiContext.Context.XsiAdvertisementPage.Find(itemId);
            XsiContext.Context.XsiAdvertisementPage.Remove(aAdvertisementPage);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionRegionRepository : IExhibitionRegionRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionRegionRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionRegionRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionRegionRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionRegion GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionRegion.Find(itemId);
        }
      
        public List<XsiExhibitionRegion> Select()
        {
            return XsiContext.Context.XsiExhibitionRegion.ToList();
        }
        public List<XsiExhibitionRegion> SelectOr(XsiExhibitionRegion entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionRegion>();
            var Outer = PredicateBuilder.True<XsiExhibitionRegion>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionRegion.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiExhibitionRegion> Select(XsiExhibitionRegion entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionRegion>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiExhibitionRegion.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionRegion Add(XsiExhibitionRegion entity)
        {
            XsiExhibitionRegion ExhibitionRegion = new XsiExhibitionRegion();
           
            if (!entity.Title.IsDefault())
            ExhibitionRegion.Title = entity.Title.Trim();
            ExhibitionRegion.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionRegion.TitleAr = entity.TitleAr.Trim();
            ExhibitionRegion.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            ExhibitionRegion.CreatedOn = entity.CreatedOn;
            ExhibitionRegion.CreatedBy = entity.CreatedBy;
            ExhibitionRegion.ModifiedOn = entity.ModifiedOn;
            ExhibitionRegion.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionRegion.Add(ExhibitionRegion);
            SubmitChanges();
            return ExhibitionRegion;

        }
        public void Update(XsiExhibitionRegion entity)
        {
            XsiExhibitionRegion ExhibitionRegion = XsiContext.Context.XsiExhibitionRegion.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionRegion).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionRegion.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                ExhibitionRegion.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionRegion.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                ExhibitionRegion.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionRegion.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                ExhibitionRegion.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionRegion entity)
        {
            foreach (XsiExhibitionRegion ExhibitionRegion in Select(entity))
            {
                XsiExhibitionRegion aExhibitionRegion = XsiContext.Context.XsiExhibitionRegion.Find(ExhibitionRegion.ItemId);
                XsiContext.Context.XsiExhibitionRegion.Remove(aExhibitionRegion);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionRegion aExhibitionRegion = XsiContext.Context.XsiExhibitionRegion.Find(itemId);
            XsiContext.Context.XsiExhibitionRegion.Remove(aExhibitionRegion);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
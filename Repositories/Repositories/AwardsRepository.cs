﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class AwardRepository : IAwardsRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public AwardRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public AwardRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public AwardRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiAwards GetById(long itemId)
        {
            return XsiContext.Context.XsiAwards.Find(itemId);
        }

        public List<XsiAwards> Select()
        {
            return XsiContext.Context.XsiAwards.ToList();
        }
        public List<XsiAwards> SelectOr(XsiAwards entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiAwards>();
            var Outer = PredicateBuilder.True<XsiAwards>();

            if (!entity.Name.IsDefault())
            {
                string strSearchText = entity.Name;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Name.Contains(searchKeys.str1) || p.Name.Contains(searchKeys.str2) || p.Name.Contains(searchKeys.str3) || p.Name.Contains(searchKeys.str4) || p.Name.Contains(searchKeys.str5)
                    || p.Name.Contains(searchKeys.str6) || p.Name.Contains(searchKeys.str7) || p.Name.Contains(searchKeys.str8) || p.Name.Contains(searchKeys.str9) || p.Name.Contains(searchKeys.str10)
                    || p.Name.Contains(searchKeys.str11) || p.Name.Contains(searchKeys.str12) || p.Name.Contains(searchKeys.str13)
                    || p.Name.Contains(searchKeys.str14) || p.Name.Contains(searchKeys.str15) || p.Name.Contains(searchKeys.str16) || p.Name.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Name.IsDefault())
                {
                    string strSearchText = entity.Name.ToLower();
                    Inner = Inner.Or(p => p.Name.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.WebsiteId.IsDefault())
                Outer = Outer.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiAwards.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiAwards> Select(XsiAwards entity)
        {
            var predicate = PredicateBuilder.True<XsiAwards>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Category.IsDefault())
                predicate = predicate.And(p => p.Category == entity.Category);

            if (!entity.Conditions.IsDefault())
                predicate = predicate.And(p => p.Conditions.Contains(entity.Conditions));

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.Introduction.IsDefault())
                predicate = predicate.And(p => p.Introduction.Contains(entity.Introduction));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.Name.IsDefault())
                predicate = predicate.And(p => p.Name.Contains(entity.Name));

            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail.Contains(entity.Thumbnail));

            if (!entity.Objectives.IsDefault())
                predicate = predicate.And(p => p.Objectives.Contains(entity.Objectives));

            if (!entity.Requirements.IsDefault())
                predicate = predicate.And(p => p.Requirements.Contains(entity.Requirements));

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            return XsiContext.Context.XsiAwards.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiAwards> SelectOtherLanguageCategory(XsiAwards entity)
        {
            var predicate = PredicateBuilder.True<XsiAwards>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            predicate = PredicateBuilder.True<XsiAwards>();

            return XsiContext.Context.XsiAwards.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiAwards> SelectCurrentLanguageCategory(XsiAwards entity)
        {
            var predicate = PredicateBuilder.True<XsiAwards>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            predicate = PredicateBuilder.True<XsiAwards>(); 
            return XsiContext.Context.XsiAwards.AsExpandable().Where(predicate).ToList();
        }

        public XsiAwards Add(XsiAwards entity)
        {
            XsiAwards Award = new XsiAwards();
            if (!entity.Category.IsDefault())
                Award.Category = entity.Category.Trim();
            if (!entity.Conditions.IsDefault())
                Award.Conditions = entity.Conditions.Trim();
            
            if (!entity.Introduction.IsDefault())
                Award.Introduction = entity.Introduction.Trim();
            Award.IsActive = entity.IsActive;
            if (!entity.Name.IsDefault())
                Award.Name = entity.Name.Trim();
            if (!entity.Thumbnail.IsDefault())
                Award.Thumbnail = entity.Thumbnail.Trim();
            if (!entity.Objectives.IsDefault())
                Award.Objectives = entity.Objectives.Trim();
            if (!entity.Requirements.IsDefault())
                Award.Requirements = entity.Requirements.Trim();
          
            #region Ar
            if (!entity.CategoryAr.IsDefault())
                Award.CategoryAr = entity.CategoryAr.Trim();
            if (!entity.ConditionsAr.IsDefault())
                Award.ConditionsAr = entity.ConditionsAr.Trim();
            if (!entity.IntroductionAr.IsDefault())
                Award.IntroductionAr = entity.IntroductionAr.Trim();
            Award.IsActiveAr = entity.IsActiveAr;
            if (!entity.NameAr.IsDefault())
                Award.NameAr = entity.NameAr.Trim();
            if (!entity.ThumbnailAr.IsDefault())
                Award.ThumbnailAr = entity.ThumbnailAr.Trim();
            if (!entity.ObjectivesAr.IsDefault())
                Award.ObjectivesAr = entity.ObjectivesAr.Trim();
            if (!entity.RequirementsAr.IsDefault())
                Award.RequirementsAr = entity.RequirementsAr.Trim();
            #endregion Ar

            Award.WebsiteId = entity.WebsiteId;
            Award.CreatedBy = entity.CreatedBy;
            Award.CreatedOn = entity.CreatedOn;
            Award.ModifiedBy = entity.ModifiedBy;
            Award.ModifiedOn = entity.ModifiedOn;
            
            XsiContext.Context.XsiAwards.Add(Award);
            SubmitChanges();
            return Award;

        }
        public void Update(XsiAwards entity)
        {
            XsiAwards Award = XsiContext.Context.XsiAwards.Find(entity.ItemId);
            XsiContext.Context.Entry(Award).State = EntityState.Modified;

            if (!entity.Category.IsDefault())
                Award.Category = entity.Category.Trim();

            if (!entity.Conditions.IsDefault())
                Award.Conditions = entity.Conditions.Trim();

            if (!entity.Introduction.IsDefault())
                Award.Introduction = entity.Introduction.Trim();

            if (!entity.IsActive.IsDefault())
                Award.IsActive = entity.IsActive;

            if (!entity.ModifiedBy.IsDefault())
                Award.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                Award.ModifiedOn = entity.ModifiedOn;

            if (!entity.Name.IsDefault())
                Award.Name = entity.Name.Trim();

            if (!entity.Thumbnail.IsDefault())
                Award.Thumbnail = entity.Thumbnail.Trim();

            if (!entity.Objectives.IsDefault())
                Award.Objectives = entity.Objectives.Trim();

            if (!entity.Requirements.IsDefault())
                Award.Requirements = entity.Requirements.Trim();

            #region Ar
            if (!entity.CategoryAr.IsDefault())
                Award.CategoryAr = entity.CategoryAr.Trim();
            if (!entity.ConditionsAr.IsDefault())
                Award.ConditionsAr = entity.ConditionsAr.Trim();
            if (!entity.IntroductionAr.IsDefault())
                Award.IntroductionAr = entity.IntroductionAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                Award.IsActiveAr = entity.IsActiveAr;
            if (!entity.NameAr.IsDefault())
                Award.NameAr = entity.NameAr.Trim();
            if (!entity.ThumbnailAr.IsDefault())
                Award.ThumbnailAr = entity.ThumbnailAr.Trim();
            if (!entity.ObjectivesAr.IsDefault())
                Award.ObjectivesAr = entity.ObjectivesAr.Trim();
            if (!entity.RequirementsAr.IsDefault())
                Award.RequirementsAr = entity.RequirementsAr.Trim();
            #endregion Ar

            if (!entity.WebsiteId.IsDefault())
                Award.WebsiteId = entity.WebsiteId;
        }
        public void Delete(XsiAwards entity)
        {
            foreach (XsiAwards Award in Select(entity))
            {
                XsiAwards aAward = XsiContext.Context.XsiAwards.Find(Award.ItemId);
                XsiContext.Context.XsiAwards.Remove(aAward);
            }
        }
        public void Delete(long itemId)
        {
            XsiAwards aAward = XsiContext.Context.XsiAwards.Find(itemId);
            XsiContext.Context.XsiAwards.Remove(aAward);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionFurnitureItemOrderRepository : IExhibitionFurnitureItemOrdersRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionFurnitureItemOrderRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionFurnitureItemOrderRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionFurnitureItemOrderRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionFurnitureItemOrders GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionFurnitureItemOrders.Find(itemId);
        }

        public List<XsiExhibitionFurnitureItemOrders> Select()
        {
            return XsiContext.Context.XsiExhibitionFurnitureItemOrders.ToList();
        }

        public List<XsiExhibitionFurnitureItemOrders> Select(XsiExhibitionFurnitureItemOrders entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionFurnitureItemOrders>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.InvoiceNumber.IsDefault())
                predicate = predicate.And(p => p.InvoiceNumber.Contains(entity.InvoiceNumber));

            if (!entity.ExhibitorId.IsDefault())
                predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Equals(entity.FileName));

            if (!entity.BankName.IsDefault())
                predicate = predicate.And(p => p.BankName.Equals(entity.BankName));

            if (!entity.TransferDate.IsDefault())
                predicate = predicate.And(p => p.TransferDate == entity.TransferDate);

            if (!entity.ReceiptNumber.IsDefault())
                predicate = predicate.And(p => p.ReceiptNumber.Equals(entity.ReceiptNumber));

            if (!entity.UploadPaymentReceipt.IsDefault())
                predicate = predicate.And(p => p.UploadPaymentReceipt.Equals(entity.UploadPaymentReceipt));

            if (!entity.PaymentTypeId.IsDefault())
                predicate = predicate.And(p => p.PaymentTypeId == entity.PaymentTypeId);

            if (!entity.Discount.IsDefault())
                predicate = predicate.And(p => p.Discount == entity.Discount);

            if (!entity.Penalty.IsDefault())
                predicate = predicate.And(p => p.Penalty == entity.Penalty);

            if (!entity.DiscountPercent.IsDefault())
                predicate = predicate.And(p => p.DiscountPercent == entity.DiscountPercent);

            if (!entity.PenaltyPercent.IsDefault())
                predicate = predicate.And(p => p.PenaltyPercent == entity.PenaltyPercent);

            if (!entity.Vat.IsDefault())
                predicate = predicate.And(p => p.Vat == entity.Vat);

            if (!entity.SubTotal.IsDefault())
                predicate = predicate.And(p => p.SubTotal == entity.SubTotal);

            if (!entity.Total.IsDefault())
                predicate = predicate.And(p => p.Total == entity.Total);

            if (!entity.PaidAmount.IsDefault())
                predicate = predicate.And(p => p.PaidAmount == entity.PaidAmount);

            if (!entity.Notes.IsDefault())
                predicate = predicate.And(p => p.Notes.Contains(entity.Notes));

            if (!entity.DiscountNote.IsDefault())
                predicate = predicate.And(p => p.DiscountNote.Contains(entity.DiscountNote));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiExhibitionFurnitureItemOrders.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionFurnitureItemOrders Add(XsiExhibitionFurnitureItemOrders entity)
        {
            XsiExhibitionFurnitureItemOrders ExhibitionFurnitureItemOrder = new XsiExhibitionFurnitureItemOrders();

            if (!entity.InvoiceNumber.IsDefault())
                ExhibitionFurnitureItemOrder.InvoiceNumber = entity.InvoiceNumber.Trim();

            if (!entity.ExhibitorId.IsDefault())
                ExhibitionFurnitureItemOrder.ExhibitorId = entity.ExhibitorId;

            if (!entity.IsActive.IsDefault())
                ExhibitionFurnitureItemOrder.IsActive = entity.IsActive;

            if (!entity.Status.IsDefault())
                ExhibitionFurnitureItemOrder.Status = entity.Status;

            if (!entity.BankName.IsDefault())
                ExhibitionFurnitureItemOrder.BankName = entity.BankName;

            if (!entity.TransferDate.IsDefault())
                ExhibitionFurnitureItemOrder.TransferDate = entity.TransferDate;

            if (!entity.ReceiptNumber.IsDefault())
                ExhibitionFurnitureItemOrder.ReceiptNumber = entity.ReceiptNumber;

            if (!entity.UploadPaymentReceipt.IsDefault())
                ExhibitionFurnitureItemOrder.UploadPaymentReceipt = entity.UploadPaymentReceipt;

            if (!entity.PaymentTypeId.IsDefault())
                ExhibitionFurnitureItemOrder.PaymentTypeId = entity.PaymentTypeId;

            if (!entity.Discount.IsDefault())
                ExhibitionFurnitureItemOrder.Discount = entity.Discount;

            if (!entity.Penalty.IsDefault())
                ExhibitionFurnitureItemOrder.Penalty = entity.Penalty;

            if (!entity.DiscountPercent.IsDefault())
                ExhibitionFurnitureItemOrder.DiscountPercent = entity.DiscountPercent;

            if (!entity.PenaltyPercent.IsDefault())
                ExhibitionFurnitureItemOrder.PenaltyPercent = entity.PenaltyPercent;

            if (!entity.Vat.IsDefault())
                ExhibitionFurnitureItemOrder.Vat = entity.Vat;

            if (!entity.SubTotal.IsDefault())
                ExhibitionFurnitureItemOrder.SubTotal = entity.SubTotal;

            if (!entity.Total.IsDefault())
                ExhibitionFurnitureItemOrder.Total = entity.Total;

            if (!entity.PaidAmount.IsDefault())
                ExhibitionFurnitureItemOrder.PaidAmount = entity.PaidAmount;

            if (!entity.FileName.IsDefault())
                ExhibitionFurnitureItemOrder.FileName = entity.FileName;

            if (!entity.DiscountNote.IsDefault())
                ExhibitionFurnitureItemOrder.DiscountNote = entity.DiscountNote;

            if (!entity.Notes.IsDefault())
                ExhibitionFurnitureItemOrder.Notes = entity.Notes;

            ExhibitionFurnitureItemOrder.CreatedOn = entity.CreatedOn;
            ExhibitionFurnitureItemOrder.CreatedBy = entity.CreatedBy;
            ExhibitionFurnitureItemOrder.ModifiedOn = entity.ModifiedOn;
            ExhibitionFurnitureItemOrder.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiExhibitionFurnitureItemOrders.Add(ExhibitionFurnitureItemOrder);
            SubmitChanges();
            return ExhibitionFurnitureItemOrder;

        }
        public void Update(XsiExhibitionFurnitureItemOrders entity)
        {
            XsiExhibitionFurnitureItemOrders ExhibitionFurnitureItemOrder = XsiContext.Context.XsiExhibitionFurnitureItemOrders.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionFurnitureItemOrder).State = EntityState.Modified;

            if (!entity.InvoiceNumber.IsDefault())
                ExhibitionFurnitureItemOrder.InvoiceNumber = entity.InvoiceNumber.Trim();

            if (!entity.ExhibitorId.IsDefault())
                ExhibitionFurnitureItemOrder.ExhibitorId = entity.ExhibitorId;

            if (!entity.IsActive.IsDefault())
                ExhibitionFurnitureItemOrder.IsActive = entity.IsActive;

            if (!entity.Status.IsDefault())
                ExhibitionFurnitureItemOrder.Status = entity.Status;

            if (!entity.BankName.IsDefault())
                ExhibitionFurnitureItemOrder.BankName = entity.BankName;

            if (!entity.TransferDate.IsDefault())
                ExhibitionFurnitureItemOrder.TransferDate = entity.TransferDate;

            if (!entity.ReceiptNumber.IsDefault())
                ExhibitionFurnitureItemOrder.ReceiptNumber = entity.ReceiptNumber;

            if (!entity.UploadPaymentReceipt.IsDefault())
                ExhibitionFurnitureItemOrder.UploadPaymentReceipt = entity.UploadPaymentReceipt;

            if (!entity.PaymentTypeId.IsDefault())
                ExhibitionFurnitureItemOrder.PaymentTypeId = entity.PaymentTypeId;

            if (!entity.Discount.IsDefault())
                ExhibitionFurnitureItemOrder.Discount = entity.Discount;

            if (!entity.Penalty.IsDefault())
                ExhibitionFurnitureItemOrder.Penalty = entity.Penalty;

            if (!entity.DiscountPercent.IsDefault())
                ExhibitionFurnitureItemOrder.DiscountPercent = entity.DiscountPercent;

            if (!entity.PenaltyPercent.IsDefault())
                ExhibitionFurnitureItemOrder.PenaltyPercent = entity.PenaltyPercent;

            if (!entity.Vat.IsDefault())
                ExhibitionFurnitureItemOrder.Vat = entity.Vat;

            if (!entity.SubTotal.IsDefault())
                ExhibitionFurnitureItemOrder.SubTotal = entity.SubTotal;

            if (!entity.Total.IsDefault())
                ExhibitionFurnitureItemOrder.Total = entity.Total;

            if (!entity.PaidAmount.IsDefault())
                ExhibitionFurnitureItemOrder.PaidAmount = entity.PaidAmount;

            if (!entity.FileName.IsDefault())
                ExhibitionFurnitureItemOrder.FileName = entity.FileName;

            if (!entity.DiscountNote.IsDefault())
                ExhibitionFurnitureItemOrder.DiscountNote = entity.DiscountNote;

            if (!entity.Notes.IsDefault())
                ExhibitionFurnitureItemOrder.Notes = entity.Notes;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionFurnitureItemOrder.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionFurnitureItemOrder.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionFurnitureItemOrders entity)
        {
            foreach (XsiExhibitionFurnitureItemOrders ExhibitionFurnitureItemOrder in Select(entity))
            {
                XsiExhibitionFurnitureItemOrders aExhibitionFurnitureItemOrder = XsiContext.Context.XsiExhibitionFurnitureItemOrders.Find(ExhibitionFurnitureItemOrder.ItemId);
                XsiContext.Context.XsiExhibitionFurnitureItemOrders.Remove(aExhibitionFurnitureItemOrder);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionFurnitureItemOrders aExhibitionFurnitureItemOrder = XsiContext.Context.XsiExhibitionFurnitureItemOrders.Find(itemId);
            XsiContext.Context.XsiExhibitionFurnitureItemOrders.Remove(aExhibitionFurnitureItemOrder);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
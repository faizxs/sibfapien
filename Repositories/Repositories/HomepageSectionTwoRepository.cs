﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class HomepageSectionTwoRepository : IHomepageSectionTwoRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public HomepageSectionTwoRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public HomepageSectionTwoRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public HomepageSectionTwoRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiHomepageSectionTwo GetById(long itemId)
        {
            return XsiContext.Context.XsiHomepageSectionTwo.Find(itemId);
        }

        public List<XsiHomepageSectionTwo> Select()
        {
            return XsiContext.Context.XsiHomepageSectionTwo.ToList();
        }
        public List<XsiHomepageSectionTwo> Select(XsiHomepageSectionTwo entity)
        {
            var predicate = PredicateBuilder.True<XsiHomepageSectionTwo>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail.Equals(entity.Thumbnail));

            if (!entity.Color.IsDefault())
                predicate = predicate.And(p => p.Color.Contains(entity.Color));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiHomepageSectionTwo.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiHomepageSectionTwo> SelectComeplete(XsiHomepageSectionTwo entity, long langId)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiHomepageSectionTwo>();
            var predicate = PredicateBuilder.True<XsiHomepageSectionTwo>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);
            if (langId == 1)
            {
                if (!entity.Title.IsDefault())
                    predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title.ToLower()));
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    predicate = predicate.And(p => p.Title.Contains(entity.Title));
                    string strSearchText = entity.Title;
                    var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                    Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                        || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                        || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                        || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                        );

                    isInner = true;
                }
            }
            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail.Equals(entity.Thumbnail));

            if (!entity.Color.IsDefault())
                predicate = predicate.And(p => p.Color.Equals(entity.Color));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Equals(entity.Url));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            //return XsiContext.Context.XsiHomepageSectionTwo.Include(p => p.XsiHomepageSectionTwoCategory).AsExpandable().Where(predicate).ToList();
            return XsiContext.Context.XsiHomepageSectionTwo.AsExpandable().Where(predicate).ToList();
        }

        public XsiHomepageSectionTwo Add(XsiHomepageSectionTwo entity)
        {
            XsiHomepageSectionTwo HomepageSectionTwo = new XsiHomepageSectionTwo();
            HomepageSectionTwo.CategoryId = entity.CategoryId;
            if (!entity.Title.IsDefault())
                HomepageSectionTwo.Title = entity.Title.Trim();
            HomepageSectionTwo.Thumbnail = entity.Thumbnail;
            HomepageSectionTwo.Color = entity.Color;
            HomepageSectionTwo.Url = entity.Url;
            HomepageSectionTwo.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                HomepageSectionTwo.TitleAr = entity.TitleAr.Trim();
            HomepageSectionTwo.ThumbnailAr = entity.ThumbnailAr;
            HomepageSectionTwo.ColorAr = entity.ColorAr;
            HomepageSectionTwo.Urlar = entity.Urlar;
            HomepageSectionTwo.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            HomepageSectionTwo.CreatedOn = entity.CreatedOn;
            HomepageSectionTwo.CreatedBy = entity.CreatedBy;
            HomepageSectionTwo.ModifiedOn = entity.ModifiedOn;
            HomepageSectionTwo.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiHomepageSectionTwo.Add(HomepageSectionTwo);
            SubmitChanges();
            return HomepageSectionTwo;

        }
        public void Update(XsiHomepageSectionTwo entity)
        {
            XsiHomepageSectionTwo HomepageSectionTwo = XsiContext.Context.XsiHomepageSectionTwo.Find(entity.ItemId);
            XsiContext.Context.Entry(HomepageSectionTwo).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                HomepageSectionTwo.Title = entity.Title.Trim();

            if (!entity.CategoryId.IsDefault())
                HomepageSectionTwo.CategoryId = entity.CategoryId;

            if (!entity.Thumbnail.IsDefault())
                HomepageSectionTwo.Thumbnail = entity.Thumbnail;

            if (!entity.Color.IsDefault())
                HomepageSectionTwo.Color = entity.Color;

            if (!entity.Url.IsDefault())
                HomepageSectionTwo.Url = entity.Url;

            if (!entity.IsActive.IsDefault())
                HomepageSectionTwo.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                HomepageSectionTwo.TitleAr = entity.TitleAr.Trim();
             
            if (!entity.ThumbnailAr.IsDefault())
                HomepageSectionTwo.ThumbnailAr = entity.ThumbnailAr;

            if (!entity.ColorAr.IsDefault())
                HomepageSectionTwo.ColorAr = entity.ColorAr;

            if (!entity.Urlar.IsDefault())
                HomepageSectionTwo.Urlar = entity.Urlar;

            if (!entity.IsActiveAr.IsDefault())
                HomepageSectionTwo.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                HomepageSectionTwo.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                HomepageSectionTwo.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiHomepageSectionTwo entity)
        {
            foreach (XsiHomepageSectionTwo HomepageSectionTwo in Select(entity))
            {
                XsiHomepageSectionTwo aHomepageSectionTwo = XsiContext.Context.XsiHomepageSectionTwo.Find(HomepageSectionTwo.ItemId);
                XsiContext.Context.XsiHomepageSectionTwo.Remove(aHomepageSectionTwo);
            }
        }
        public void Delete(long itemId)
        {
            XsiHomepageSectionTwo aHomepageSectionTwo = XsiContext.Context.XsiHomepageSectionTwo.Find(itemId);
            XsiContext.Context.XsiHomepageSectionTwo.Remove(aHomepageSectionTwo);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
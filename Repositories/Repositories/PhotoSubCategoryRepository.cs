﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PhotoSubCategoryRepository : IPhotoSubCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PhotoSubCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PhotoSubCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PhotoSubCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiPhotoSubCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiPhotoSubCategory.Find(itemId);
        }

        public List<XsiPhotoSubCategory> Select()
        {
            return XsiContext.Context.XsiPhotoSubCategory.ToList();
        }
        public List<XsiPhotoSubCategory> Select(XsiPhotoSubCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiPhotoSubCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiPhotoSubCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiPhotoSubCategory> SelectComeplete(XsiPhotoSubCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiPhotoSubCategory>();
            var predicate = PredicateBuilder.True<XsiPhotoSubCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.CategoryId.IsDefault())
            {
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);
                XsiPhotoCategory MainEntity = XsiContext.Context.XsiPhotoCategory.Where(p => p.ItemId == entity.CategoryId).FirstOrDefault();

                if (!entity.TitleAr.IsDefault())
                {
                    string strSearchText = entity.TitleAr;
                    var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                    Inner = Inner.Or(p => p.TitleAr.Contains(searchKeys.str1) || p.TitleAr.Contains(searchKeys.str2) || p.TitleAr.Contains(searchKeys.str3) || p.TitleAr.Contains(searchKeys.str4) || p.TitleAr.Contains(searchKeys.str5)
                        || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                        || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                        || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                        );

                    isInner = true;
                }
                else
                {
                    if (!entity.Title.IsDefault())
                    {
                        string strSearchText = entity.Title.ToLower();
                        Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                        isInner = true;
                    }
                }
            }
            else
            {
                if (!entity.Title.IsDefault())
                    predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));
            }
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiPhotoSubCategory.Include(p => p.Category).AsExpandable().Where(predicate).ToList();
        }

        public XsiPhotoSubCategory Add(XsiPhotoSubCategory entity)
        {
            XsiPhotoSubCategory PhotoSubCategory = new XsiPhotoSubCategory();
            PhotoSubCategory.CategoryId = entity.CategoryId;
            if (!entity.Title.IsDefault())
                PhotoSubCategory.Title = entity.Title.Trim();
            PhotoSubCategory.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                PhotoSubCategory.TitleAr = entity.TitleAr.Trim();
            PhotoSubCategory.IsActiveAr = entity.IsActiveAr;

            PhotoSubCategory.CreatedOn = entity.CreatedOn;
            PhotoSubCategory.CreatedBy = entity.CreatedBy;
            PhotoSubCategory.ModifiedOn = entity.ModifiedOn;
            PhotoSubCategory.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiPhotoSubCategory.Add(PhotoSubCategory);
            SubmitChanges();
            return PhotoSubCategory;

        }
        public void Update(XsiPhotoSubCategory entity)
        {
            XsiPhotoSubCategory PhotoSubCategory = XsiContext.Context.XsiPhotoSubCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(PhotoSubCategory).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                PhotoSubCategory.Title = entity.Title.Trim();

            if (!entity.IsActive.IsDefault())
                PhotoSubCategory.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                PhotoSubCategory.TitleAr = entity.TitleAr.Trim();

            if (!entity.IsActiveAr.IsDefault())
                PhotoSubCategory.IsActiveAr = entity.IsActiveAr;

            if (!entity.ModifiedOn.IsDefault())
                PhotoSubCategory.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                PhotoSubCategory.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiPhotoSubCategory entity)
        {
            foreach (XsiPhotoSubCategory PhotoSubCategory in Select(entity))
            {
                XsiPhotoSubCategory aPhotoSubCategory = XsiContext.Context.XsiPhotoSubCategory.Find(PhotoSubCategory.ItemId);
                XsiContext.Context.XsiPhotoSubCategory.Remove(aPhotoSubCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiPhotoSubCategory aPhotoSubCategory = XsiContext.Context.XsiPhotoSubCategory.Find(itemId);
            XsiContext.Context.XsiPhotoSubCategory.Remove(aPhotoSubCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
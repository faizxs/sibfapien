﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFIllustratorRepository : ISCRFIllustratorRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFIllustratorRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFIllustratorRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFIllustratorRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfillustrator GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfillustrator.Find(itemId);
        }

        public List<XsiScrfillustrator> Select()
        {
            return XsiContext.Context.XsiScrfillustrator.ToList();
        }
        public List<XsiScrfillustrator> Select(XsiScrfillustrator entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfillustrator>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.StartDate.IsDefault())
                predicate = predicate.And(p => p.StartDate == (entity.StartDate));

            if (!entity.EndDate.IsDefault())
                predicate = predicate.And(p => p.EndDate == (entity.EndDate));

            if (!entity.Deadline.IsDefault())
                predicate = predicate.And(p => p.Deadline == (entity.Deadline));

            if (!entity.HardCopyDeadline.IsDefault())
                predicate = predicate.And(p => p.HardCopyDeadline == (entity.HardCopyDeadline));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.FirstPrize.IsDefault())
                predicate = predicate.And(p => p.FirstPrize.Contains(entity.FirstPrize));

            if (!entity.SecondPrize.IsDefault())
                predicate = predicate.And(p => p.SecondPrize.Contains(entity.SecondPrize));

            if (!entity.ThirdPrize.IsDefault())
                predicate = predicate.And(p => p.ThirdPrize.Contains(entity.ThirdPrize));

            if (!entity.FirstHonorableMention.IsDefault())
                predicate = predicate.And(p => p.FirstHonorableMention.Contains(entity.FirstHonorableMention));

            if (!entity.SecondHonorableMention.IsDefault())
                predicate = predicate.And(p => p.SecondHonorableMention.Contains(entity.SecondHonorableMention));

            if (!entity.ThirdHonorableMention.IsDefault())
                predicate = predicate.And(p => p.ThirdHonorableMention.Contains(entity.ThirdHonorableMention));

            if (!entity.FourthHonorableMention.IsDefault())
                predicate = predicate.And(p => p.FourthHonorableMention.Contains(entity.FourthHonorableMention));

            if (!entity.FifthHonorableMention.IsDefault())
                predicate = predicate.And(p => p.FifthHonorableMention.Contains(entity.FifthHonorableMention));

            if (!entity.SixthHonorableMention.IsDefault())
                predicate = predicate.And(p => p.SixthHonorableMention.Contains(entity.SixthHonorableMention));

            if (!entity.Notes.IsDefault())
                predicate = predicate.And(p => p.Notes.Contains(entity.Notes));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Contains(entity.IsActive));

            if (!entity.IsArchive.IsDefault())
                predicate = predicate.And(p => p.IsArchive.Contains(entity.IsArchive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiScrfillustrator.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfillustrator Add(XsiScrfillustrator entity)
        {
            XsiScrfillustrator SCRFIllustrator = new XsiScrfillustrator();
            if (!entity.Title.IsDefault())
                SCRFIllustrator.Title = entity.Title.Trim();
            if (!entity.Year.IsDefault())
                SCRFIllustrator.Year = entity.Year.Trim();
            SCRFIllustrator.StartDate = entity.StartDate;
            SCRFIllustrator.EndDate = entity.EndDate;
            SCRFIllustrator.Deadline = entity.Deadline;
            SCRFIllustrator.HardCopyDeadline = entity.HardCopyDeadline;
            if (!entity.Description.IsDefault())
                SCRFIllustrator.Description = entity.Description.Trim();
            if (!entity.FirstPrize.IsDefault())
                SCRFIllustrator.FirstPrize = entity.FirstPrize.Trim();
            if (!entity.SecondPrize.IsDefault())
                SCRFIllustrator.SecondPrize = entity.SecondPrize.Trim();
            if (!entity.ThirdPrize.IsDefault())
                SCRFIllustrator.ThirdPrize = entity.ThirdPrize.Trim();
            if (!entity.FirstHonorableMention.IsDefault())
                SCRFIllustrator.FirstHonorableMention = entity.FirstHonorableMention.Trim();
            if (!entity.SecondHonorableMention.IsDefault())
                SCRFIllustrator.SecondHonorableMention = entity.SecondHonorableMention.Trim();
            if (!entity.ThirdHonorableMention.IsDefault())
                SCRFIllustrator.ThirdHonorableMention = entity.ThirdHonorableMention.Trim();
            if (!entity.FourthHonorableMention.IsDefault())
                SCRFIllustrator.FourthHonorableMention = entity.FourthHonorableMention.Trim();
            if (!entity.FifthHonorableMention.IsDefault())
                SCRFIllustrator.FifthHonorableMention = entity.FifthHonorableMention.Trim();
            if (!entity.SixthHonorableMention.IsDefault())
                SCRFIllustrator.SixthHonorableMention = entity.SixthHonorableMention.Trim();
            if (!entity.Notes.IsDefault())
                SCRFIllustrator.Notes = entity.Notes.Trim();
            SCRFIllustrator.IsActive = entity.IsActive;
            SCRFIllustrator.IsArchive = entity.IsArchive;
            SCRFIllustrator.CreatedOn = entity.CreatedOn;
            SCRFIllustrator.CreatedBy = entity.CreatedBy;
            SCRFIllustrator.ModifiedOn = entity.ModifiedOn;
            SCRFIllustrator.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfillustrator.Add(SCRFIllustrator);
            SubmitChanges();
            return SCRFIllustrator;

        }
        public void Update(XsiScrfillustrator entity)
        {
            XsiScrfillustrator SCRFIllustrator = XsiContext.Context.XsiScrfillustrator.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFIllustrator).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                SCRFIllustrator.Title = entity.Title.Trim();

            if (!entity.Year.IsDefault())
                SCRFIllustrator.Year = entity.Year.Trim();

            if (!entity.StartDate.IsDefault())
                SCRFIllustrator.StartDate = entity.StartDate;

            if (!entity.EndDate.IsDefault())
                SCRFIllustrator.EndDate = entity.EndDate;

            if (!entity.Deadline.IsDefault())
                SCRFIllustrator.Deadline = entity.Deadline;

            if (!entity.HardCopyDeadline.IsDefault())
                SCRFIllustrator.HardCopyDeadline = entity.HardCopyDeadline;

            if (!entity.Description.IsDefault())
                SCRFIllustrator.Description = entity.Description.Trim();

            if (!entity.FirstPrize.IsDefault())
                SCRFIllustrator.FirstPrize = entity.FirstPrize.Trim();

            if (!entity.SecondPrize.IsDefault())
                SCRFIllustrator.SecondPrize = entity.SecondPrize.Trim();

            if (!entity.ThirdPrize.IsDefault())
                SCRFIllustrator.ThirdPrize = entity.ThirdPrize.Trim();

            if (!entity.FirstHonorableMention.IsDefault())
                SCRFIllustrator.FirstHonorableMention = entity.FirstHonorableMention.Trim();

            if (!entity.SecondHonorableMention.IsDefault())
                SCRFIllustrator.SecondHonorableMention = entity.SecondHonorableMention.Trim();

            if (!entity.ThirdHonorableMention.IsDefault())
                SCRFIllustrator.ThirdHonorableMention = entity.ThirdHonorableMention.Trim();

            if (!entity.FourthHonorableMention.IsDefault())
                SCRFIllustrator.FourthHonorableMention = entity.FourthHonorableMention.Trim();

            if (!entity.FifthHonorableMention.IsDefault())
                SCRFIllustrator.FifthHonorableMention = entity.FifthHonorableMention.Trim();

            if (!entity.SixthHonorableMention.IsDefault())
                SCRFIllustrator.SixthHonorableMention = entity.SixthHonorableMention.Trim();

            if (!entity.Notes.IsDefault())
                SCRFIllustrator.Notes = entity.Notes.Trim();

            if (!entity.IsActive.IsDefault())
                SCRFIllustrator.IsActive = entity.IsActive;

            if (!entity.IsArchive.IsDefault())
                SCRFIllustrator.IsArchive = entity.IsArchive;

            if (!entity.ModifiedOn.IsDefault())
                SCRFIllustrator.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                SCRFIllustrator.ModifiedBy = entity.ModifiedBy;


        }
        public void Delete(XsiScrfillustrator entity)
        {
            foreach (XsiScrfillustrator SCRFIllustrator in Select(entity))
            {
                XsiScrfillustrator aSCRFIllustrator = XsiContext.Context.XsiScrfillustrator.Find(SCRFIllustrator.ItemId);
                XsiContext.Context.XsiScrfillustrator.Remove(aSCRFIllustrator);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfillustrator aSCRFIllustrator = XsiContext.Context.XsiScrfillustrator.Find(itemId);
            XsiContext.Context.XsiScrfillustrator.Remove(aSCRFIllustrator);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class WebsiteRepository : IWebsiteRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public WebsiteRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public WebsiteRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public WebsiteRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiWebsites GetById(long itemId)
        {
            return XsiContext.Context.XsiWebsites.Find(itemId);
        }
        public List<XsiWebsites> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiWebsites> Select()
        {
            return XsiContext.Context.XsiWebsites.ToList();
        }
        public List<XsiWebsites> Select(XsiWebsites entity)
        {
            var predicate = PredicateBuilder.True<XsiWebsites>();
            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.Name.IsDefault())
                predicate = predicate.And(p => p.Name.Contains(entity.Name));


            return XsiContext.Context.XsiWebsites.AsExpandable().Where(predicate).ToList();
        }

        public XsiWebsites Add(XsiWebsites entity)
        {
            XsiWebsites Website = new XsiWebsites();
            if (!entity.Name.IsDefault())
                Website.Name = entity.Name.Trim();

            XsiContext.Context.XsiWebsites.Add(Website);
            SubmitChanges();
            return Website;

        }
        public void Update(XsiWebsites entity)
        {
            XsiWebsites Website = XsiContext.Context.XsiWebsites.Find(entity.WebsiteId);
            XsiContext.Context.Entry(Website).State = EntityState.Modified;

            if (!entity.Name.IsDefault())
                Website.Name = entity.Name.Trim();
        }
        public void Delete(XsiWebsites entity)
        {
            foreach (XsiWebsites Website in Select(entity))
            {
                XsiWebsites aWebsite = XsiContext.Context.XsiWebsites.Find(Website.WebsiteId);
                XsiContext.Context.XsiWebsites.Remove(aWebsite);
            }
        }
        public void Delete(long itemId)
        {
            XsiWebsites aWebsite = XsiContext.Context.XsiWebsites.Find(itemId);
            XsiContext.Context.XsiWebsites.Remove(aWebsite);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
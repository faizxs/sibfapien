﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class EReceiptRepository : IEReceiptRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public EReceiptRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public EReceiptRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public EReceiptRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiEreceipt GetById(long itemId)
        {
            return XsiContext.Context.XsiEreceipt.Find(itemId);
        }
        public List<XsiEreceipt> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiEreceipt> Select()
        {
            return XsiContext.Context.XsiEreceipt.ToList();
        }
        public List<XsiEreceipt> Select(XsiEreceipt entity)
        {
            var predicate = PredicateBuilder.True<XsiEreceipt>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ExhibitorId.IsDefault())
                predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

            if (!entity.InvoiceId.IsDefault())
                predicate = predicate.And(p => p.InvoiceId == entity.InvoiceId);

            if (!entity.OrderId.IsDefault())
                predicate = predicate.And(p => p.OrderId == entity.OrderId);

            if (!entity.EreceiptNumber.IsDefault())
                predicate = predicate.And(p => p.EreceiptNumber.Equals(entity.EreceiptNumber));

            if (!entity.IsPayLater.IsDefault())
                predicate = predicate.And(p => p.IsPayLater.Equals(entity.IsPayLater));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.AmountDueInAed.IsDefault())
                predicate = predicate.And(p => p.AmountDueInAed.Equals(entity.AmountDueInAed));

            if (!entity.PaymentReceivedInAed.IsDefault())
                predicate = predicate.And(p => p.PaymentReceivedInAed.Equals(entity.PaymentReceivedInAed));

            if (!entity.AmountDueInUsd.IsDefault())
                predicate = predicate.And(p => p.AmountDueInUsd.Equals(entity.AmountDueInUsd));

            if (!entity.PaymentReceivedInUsd.IsDefault())
                predicate = predicate.And(p => p.PaymentReceivedInUsd.Equals(entity.PaymentReceivedInUsd));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Equals(entity.FileName));

            if (!entity.PaymentType.IsDefault())
                predicate = predicate.And(p => p.PaymentType.Equals(entity.PaymentType));

            if (!entity.FileBankTransfer1.IsDefault())
                predicate = predicate.And(p => p.FileBankTransfer1.Equals(entity.FileBankTransfer1));

            if (!entity.FileBankTransfer2.IsDefault())
                predicate = predicate.And(p => p.FileBankTransfer2.Equals(entity.FileBankTransfer2));

            if (!entity.FileBankTransfer3.IsDefault())
                predicate = predicate.And(p => p.FileBankTransfer3.Equals(entity.FileBankTransfer3));

            if (!entity.BankName.IsDefault())
                predicate = predicate.And(p => p.BankName.Contains(entity.BankName));

            if (!entity.ReceiptOrChequeNumber.IsDefault())
                predicate = predicate.And(p => p.ReceiptOrChequeNumber.Equals(entity.ReceiptOrChequeNumber));

            if (!entity.Comments.IsDefault())
                predicate = predicate.And(p => p.Comments.Contains(entity.Comments));

            if (!entity.TransferDate.IsDefault())
                predicate = predicate.And(p => p.TransferDate == entity.TransferDate);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy.Equals(entity.CreatedBy));

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy.Equals(entity.ModifiedBy));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn.Equals(entity.CreatedOn));

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn.Equals(entity.ModifiedOn));

            return XsiContext.Context.XsiEreceipt.AsExpandable().Where(predicate).ToList();
        }

        public XsiEreceipt Add(XsiEreceipt entity)
        {
            XsiEreceipt EReceipt = new XsiEreceipt();
            EReceipt.ExhibitorId = entity.ExhibitorId;
            EReceipt.InvoiceId = entity.InvoiceId;
            EReceipt.OrderId = entity.OrderId;
            EReceipt.EreceiptNumber = entity.EreceiptNumber;
            EReceipt.IsPayLater = entity.IsPayLater;
            EReceipt.IsActive = entity.IsActive;
            EReceipt.Status = entity.Status;
            EReceipt.AmountDueInAed = entity.AmountDueInAed;
            EReceipt.PaymentReceivedInAed = entity.PaymentReceivedInAed;
            EReceipt.AmountDueInUsd = entity.AmountDueInUsd;
            EReceipt.PaymentReceivedInUsd = entity.PaymentReceivedInUsd;
            EReceipt.FileName = entity.FileName;
            EReceipt.PaymentType = entity.PaymentType;
            EReceipt.FileBankTransfer1 = entity.FileBankTransfer1;
            EReceipt.FileBankTransfer2 = entity.FileBankTransfer2;
            EReceipt.FileBankTransfer3 = entity.FileBankTransfer3;
            EReceipt.BankName = entity.BankName;
            EReceipt.ReceiptOrChequeNumber = entity.ReceiptOrChequeNumber;
            EReceipt.Comments = entity.Comments;
            EReceipt.TransferDate = entity.TransferDate;
            EReceipt.CreatedBy = entity.CreatedBy;
            EReceipt.CreatedOn = entity.CreatedOn;
            EReceipt.ModifiedOn = entity.ModifiedOn;
            EReceipt.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiEreceipt.Add(EReceipt);
            SubmitChanges();
            return EReceipt;

        }
        public void Update(XsiEreceipt entity)
        {
            XsiEreceipt EReceipt = XsiContext.Context.XsiEreceipt.Find(entity.ItemId);
            XsiContext.Context.Entry(EReceipt).State = EntityState.Modified;

            if (!entity.ExhibitorId.IsDefault())
                EReceipt.ExhibitorId = entity.ExhibitorId;

            if (!entity.InvoiceId.IsDefault())
                EReceipt.InvoiceId = entity.InvoiceId;

            if (!entity.OrderId.IsDefault())
                EReceipt.OrderId = entity.OrderId;

            if (!entity.EreceiptNumber.IsDefault())
                EReceipt.EreceiptNumber = entity.EreceiptNumber;

            if (!entity.IsPayLater.IsDefault())
                EReceipt.IsPayLater = entity.IsPayLater;

            if (!entity.IsActive.IsDefault())
                EReceipt.IsActive = entity.IsActive;

            if (!entity.Status.IsDefault())
                EReceipt.Status = entity.Status;

            if (!entity.AmountDueInAed.IsDefault())
                EReceipt.AmountDueInAed = entity.AmountDueInAed;

            if (!entity.PaymentReceivedInAed.IsDefault())
                EReceipt.PaymentReceivedInAed = entity.PaymentReceivedInAed;

            if (!entity.AmountDueInUsd.IsDefault())
                EReceipt.AmountDueInUsd = entity.AmountDueInUsd;

            if (!entity.PaymentReceivedInUsd.IsDefault())
                EReceipt.PaymentReceivedInUsd = entity.PaymentReceivedInUsd;

            if (!entity.FileName.IsDefault())
                EReceipt.FileName = entity.FileName;

            if (!entity.PaymentType.IsDefault())
                EReceipt.PaymentType = entity.PaymentType;

            if (!entity.FileBankTransfer1.IsDefault())
                EReceipt.FileBankTransfer1 = entity.FileBankTransfer1;

            if (!entity.FileBankTransfer2.IsDefault())
                EReceipt.FileBankTransfer2 = entity.FileBankTransfer2;

            if (!entity.FileBankTransfer3.IsDefault())
                EReceipt.FileBankTransfer3 = entity.FileBankTransfer3;

            if (!entity.BankName.IsDefault())
                EReceipt.BankName = entity.BankName;

            if (!entity.ReceiptOrChequeNumber.IsDefault())
                EReceipt.ReceiptOrChequeNumber = entity.ReceiptOrChequeNumber;

            if (!entity.Comments.IsDefault())
                EReceipt.Comments = entity.Comments;

            if (!entity.TransferDate.IsDefault())
                EReceipt.TransferDate = entity.TransferDate;

            if (!entity.ModifiedBy.IsDefault())
                EReceipt.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                EReceipt.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiEreceipt entity)
        {
            foreach (XsiEreceipt EReceipt in Select(entity))
            {
                XsiEreceipt aEReceipt = XsiContext.Context.XsiEreceipt.Find(EReceipt.ItemId);
                XsiContext.Context.XsiEreceipt.Remove(aEReceipt);
            }
        }
        public void Delete(long itemId)
        {
            XsiEreceipt aEReceipt = XsiContext.Context.XsiEreceipt.Find(itemId);
            XsiContext.Context.XsiEreceipt.Remove(aEReceipt);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository : IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs.Find(itemId);
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs> Select()
        {
            return XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs.ToList();
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs> Select(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            if (!entity.StaffGuestId.IsDefault())
                predicate = predicate.And(p => p.StaffGuestId == entity.StaffGuestId);

            if (!entity.AdminId.IsDefault())
                predicate = predicate.And(p => p.AdminId == entity.AdminId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);


            return XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs Add(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity)
        {
            XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs ExhibitionOtherEventsStaffGuestParticipatingStatusLog = new XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs();
            ExhibitionOtherEventsStaffGuestParticipatingStatusLog.ExhibitionId = entity.ExhibitionId;
            ExhibitionOtherEventsStaffGuestParticipatingStatusLog.StaffGuestId = entity.StaffGuestId;
            ExhibitionOtherEventsStaffGuestParticipatingStatusLog.AdminId = entity.AdminId;
            ExhibitionOtherEventsStaffGuestParticipatingStatusLog.Status = entity.Status;
            ExhibitionOtherEventsStaffGuestParticipatingStatusLog.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs.Add(ExhibitionOtherEventsStaffGuestParticipatingStatusLog);
            SubmitChanges();
            return ExhibitionOtherEventsStaffGuestParticipatingStatusLog;

        }
        public void Update(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity)
        {
            XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs ExhibitionOtherEventsStaffGuestParticipatingStatusLog = XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionOtherEventsStaffGuestParticipatingStatusLog).State = EntityState.Modified;

            if (!entity.Status.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipatingStatusLog.Status = entity.Status;
        }
        public void Delete(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity)
        {
            foreach (XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs ExhibitionOtherEventsStaffGuestParticipatingStatusLog in Select(entity))
            {
                XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs aExhibitionOtherEventsStaffGuestParticipatingStatusLog = XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs.Find(ExhibitionOtherEventsStaffGuestParticipatingStatusLog.ItemId);
                XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs.Remove(aExhibitionOtherEventsStaffGuestParticipatingStatusLog);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs aExhibitionOtherEventsStaffGuestParticipatingStatusLog = XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs.Find(itemId);
            XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs.Remove(aExhibitionOtherEventsStaffGuestParticipatingStatusLog);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
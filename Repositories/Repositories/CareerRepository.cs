﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class CareerRepository : ICareerRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public CareerRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public CareerRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public CareerRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiCareer GetById(long itemId)
        {
            return XsiContext.Context.XsiCareer.Find(itemId);
        }
        
        public List<XsiCareer> Select()
        {
            return XsiContext.Context.XsiCareer.ToList();
        }
        public List<XsiCareer> SelectOr(XsiCareer entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiCareer>();
            var Outer = PredicateBuilder.True<XsiCareer>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiCareer.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiCareer> Select(XsiCareer entity)
        {
            var predicate = PredicateBuilder.True<XsiCareer>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.JobId.IsDefault())
                predicate = predicate.And(p => p.JobId.Contains(entity.JobId));

            if (!entity.PostingDate.IsDefault())
                predicate = predicate.And(p => p.PostingDate == entity.PostingDate);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));
             
            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.Responsibilities.IsDefault())
                predicate = predicate.And(p => p.Responsibilities.Contains(entity.Responsibilities));

            if (!entity.Experience.IsDefault())
                predicate = predicate.And(p => p.Experience.Contains(entity.Experience));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
              
            if (!entity.Degree.IsDefault())
                predicate = predicate.And(p => p.Degree == entity.Degree);

            if (!entity.Department.IsDefault())
                predicate = predicate.And(p => p.Department == entity.Department);

            if (!entity.SubDepartment.IsDefault())
                predicate = predicate.And(p => p.SubDepartment == entity.SubDepartment);
              
            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiCareer.AsExpandable().Where(predicate).ToList();
        }

        public XsiCareer Add(XsiCareer entity)
        {
            XsiCareer Career = new XsiCareer();

            if (!entity.JobId.IsDefault())
                Career.JobId = entity.JobId.Trim();
            if (!entity.PostingDate.IsDefault())
                Career.PostingDate = entity.PostingDate;

            if (!entity.Title.IsDefault())
                Career.Title = entity.Title.Trim();           
            if (!entity.Overview.IsDefault())
                Career.Overview = entity.Overview.Trim();
            if (!entity.Description.IsDefault())
                Career.Description = entity.Description.Trim();
            if (!entity.Responsibilities.IsDefault())
                Career.Responsibilities = entity.Responsibilities.Trim();
            if (!entity.Experience.IsDefault())
                Career.Experience = entity.Experience.Trim();
            Career.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                Career.TitleAr = entity.TitleAr.Trim();
            if (!entity.OverviewAr.IsDefault())
                Career.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.DescriptionAr.IsDefault())
                Career.DescriptionAr = entity.DescriptionAr.Trim();
            if (!entity.ResponsibilitiesAr.IsDefault())
                Career.ResponsibilitiesAr = entity.ResponsibilitiesAr.Trim();
            if (!entity.ExperienceAr.IsDefault())
                Career.ExperienceAr = entity.ExperienceAr.Trim();
            Career.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.Degree.IsDefault())
                Career.Degree = entity.Degree;
            if (!entity.Department.IsDefault())
                Career.Department = entity.Department;
            if (!entity.SubDepartment.IsDefault())
                Career.SubDepartment = entity.SubDepartment;
          
            Career.CreatedOn = entity.CreatedOn;
            Career.CreatedBy = entity.CreatedBy;
            Career.ModifiedOn = entity.ModifiedOn;
            Career.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiCareer.Add(Career);
            SubmitChanges();
            return Career;

        }
        public void Update(XsiCareer entity)
        {
            XsiCareer Career = XsiContext.Context.XsiCareer.Find(entity.ItemId);
            XsiContext.Context.Entry(Career).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                Career.Title = entity.Title.Trim();
            if (!entity.JobId.IsDefault())
                Career.JobId = entity.JobId.Trim();
            if (!entity.PostingDate.IsDefault())
                Career.PostingDate = entity.PostingDate;
            if (!entity.Overview.IsDefault())
                Career.Overview = entity.Overview.Trim();
            if (!entity.Description.IsDefault())
                Career.Description = entity.Description.Trim();
            if (!entity.Responsibilities.IsDefault())
                Career.Responsibilities = entity.Responsibilities.Trim();
            if (!entity.Experience.IsDefault())
                Career.Experience = entity.Experience.Trim();
            if (!entity.Degree.IsDefault())
                Career.Degree = entity.Degree;
            if (!entity.Department.IsDefault())
                Career.Department = entity.Department;
            if (!entity.SubDepartment.IsDefault())
                Career.SubDepartment = entity.SubDepartment;
            if (!entity.IsActive.IsDefault())
                Career.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                Career.TitleAr = entity.TitleAr.Trim();
            if (!entity.OverviewAr.IsDefault())
                Career.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.DescriptionAr.IsDefault())
                Career.DescriptionAr = entity.DescriptionAr.Trim();
            if (!entity.ResponsibilitiesAr.IsDefault())
                Career.ResponsibilitiesAr = entity.ResponsibilitiesAr.Trim();
            if (!entity.ExperienceAr.IsDefault())
                Career.ExperienceAr = entity.ExperienceAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                Career.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                Career.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                Career.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiCareer entity)
        {
            foreach (XsiCareer Career in Select(entity))
            {
                XsiCareer aCareer = XsiContext.Context.XsiCareer.Find(Career.ItemId);
                XsiContext.Context.XsiCareer.Remove(aCareer);
            }
        }
        public void Delete(long itemId)
        {
            XsiCareer aCareer = XsiContext.Context.XsiCareer.Find(itemId);
            XsiContext.Context.XsiCareer.Remove(aCareer);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SIBFCrawlerRepository : ISIBFCrawlerRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SIBFCrawlerRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SIBFCrawlerRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SIBFCrawlerRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiSibfcrawler GetById(long itemId)
        {
            return XsiContext.Context.XsiSibfcrawler.Find(itemId);
        }

        public List<XsiSibfcrawler> Select()
        {
            return XsiContext.Context.XsiSibfcrawler.ToList();
        }
        public List<XsiSibfcrawler> Select(XsiSibfcrawler entity)
        {
            var predicate = PredicateBuilder.True<XsiSibfcrawler>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            return XsiContext.Context.XsiSibfcrawler.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiSibfcrawler> SelectOr(XsiSibfcrawler entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiSibfcrawler>();
            var predicate = PredicateBuilder.True<XsiSibfcrawler>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiSibfcrawler.AsExpandable().Where(predicate.Expand()).ToList();
        }
        public XsiSibfcrawler Add(XsiSibfcrawler entity)
        {
            XsiSibfcrawler SIBFCrawler = new XsiSibfcrawler();
            if (!entity.Title.IsDefault())
                SIBFCrawler.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
                SIBFCrawler.Description = entity.Description.Trim();
            if (!entity.Url.IsDefault())
                SIBFCrawler.Url = entity.Url.Trim();
            SIBFCrawler.IsActive = entity.IsActive;
            SIBFCrawler.CreatedOn = entity.CreatedOn;
            SIBFCrawler.CreatedBy = entity.CreatedBy;
            SIBFCrawler.ModifiedOn = entity.ModifiedOn;
            SIBFCrawler.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiSibfcrawler.Add(SIBFCrawler);
            SubmitChanges();
            return SIBFCrawler;

        }
        public void Update(XsiSibfcrawler entity)
        {
            XsiSibfcrawler SIBFCrawler = XsiContext.Context.XsiSibfcrawler.Find(entity.ItemId);
            XsiContext.Context.Entry(SIBFCrawler).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                SIBFCrawler.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
                SIBFCrawler.Description = entity.Description.Trim();
            if (!entity.Url.IsDefault())
                SIBFCrawler.Url = entity.Url.Trim();
            if (!entity.IsActive.IsDefault())
                SIBFCrawler.IsActive = entity.IsActive;
            if (!entity.ModifiedOn.IsDefault())
                SIBFCrawler.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                SIBFCrawler.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiSibfcrawler entity)
        {
            foreach (XsiSibfcrawler SIBFCrawler in Select(entity))
            {
                XsiSibfcrawler aSIBFCrawler = XsiContext.Context.XsiSibfcrawler.Find(SIBFCrawler.ItemId);
                XsiContext.Context.XsiSibfcrawler.Remove(aSIBFCrawler);
            }
        }
        public void Delete(long itemId)
        {
            XsiSibfcrawler aSIBFCrawler = XsiContext.Context.XsiSibfcrawler.Find(itemId);
            XsiContext.Context.XsiSibfcrawler.Remove(aSIBFCrawler);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
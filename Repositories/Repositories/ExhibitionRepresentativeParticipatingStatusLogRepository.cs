﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionRepresentativeParticipatingStatusLogRepository : IExhibitionRepresentativeParticipatingStatusLogRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionRepresentativeParticipatingStatusLogRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionRepresentativeParticipatingStatusLogRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionRepresentativeParticipatingStatusLogRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionRepresentativeParticipatingStatusLogs GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionRepresentativeParticipatingStatusLogs.Find(itemId);
        }
        public List<XsiExhibitionRepresentativeParticipatingStatusLogs> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionRepresentativeParticipatingStatusLogs> Select()
        {
            return XsiContext.Context.XsiExhibitionRepresentativeParticipatingStatusLogs.ToList();
        }
        public List<XsiExhibitionRepresentativeParticipatingStatusLogs> Select(XsiExhibitionRepresentativeParticipatingStatusLogs entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionRepresentativeParticipatingStatusLogs>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ExhibitorId.IsDefault())
                predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

            if (!entity.RepresentativeId.IsDefault())
                predicate = predicate.And(p => p.RepresentativeId == entity.RepresentativeId);

            if (!entity.AdminId.IsDefault())
                predicate = predicate.And(p => p.AdminId == entity.AdminId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);


            return XsiContext.Context.XsiExhibitionRepresentativeParticipatingStatusLogs.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionRepresentativeParticipatingStatusLogs Add(XsiExhibitionRepresentativeParticipatingStatusLogs entity)
        {
            XsiExhibitionRepresentativeParticipatingStatusLogs ExhibitionRepresentativeParticipatingStatusLog = new XsiExhibitionRepresentativeParticipatingStatusLogs();
            ExhibitionRepresentativeParticipatingStatusLog.ExhibitorId = entity.ExhibitorId;
            ExhibitionRepresentativeParticipatingStatusLog.RepresentativeId = entity.RepresentativeId;
            ExhibitionRepresentativeParticipatingStatusLog.AdminId = entity.AdminId;
            ExhibitionRepresentativeParticipatingStatusLog.Status = entity.Status;
            ExhibitionRepresentativeParticipatingStatusLog.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiExhibitionRepresentativeParticipatingStatusLogs.Add(ExhibitionRepresentativeParticipatingStatusLog);
            SubmitChanges();
            return ExhibitionRepresentativeParticipatingStatusLog;

        }
        public void Update(XsiExhibitionRepresentativeParticipatingStatusLogs entity)
        {
            XsiExhibitionRepresentativeParticipatingStatusLogs ExhibitionRepresentativeParticipatingStatusLog = XsiContext.Context.XsiExhibitionRepresentativeParticipatingStatusLogs.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionRepresentativeParticipatingStatusLog).State = EntityState.Modified;

            if (!entity.Status.IsDefault())
                ExhibitionRepresentativeParticipatingStatusLog.Status = entity.Status;
        }
        public void Delete(XsiExhibitionRepresentativeParticipatingStatusLogs entity)
        {
            foreach (XsiExhibitionRepresentativeParticipatingStatusLogs ExhibitionRepresentativeParticipatingStatusLog in Select(entity))
            {
                XsiExhibitionRepresentativeParticipatingStatusLogs aExhibitionRepresentativeParticipatingStatusLog = XsiContext.Context.XsiExhibitionRepresentativeParticipatingStatusLogs.Find(ExhibitionRepresentativeParticipatingStatusLog.ItemId);
                XsiContext.Context.XsiExhibitionRepresentativeParticipatingStatusLogs.Remove(aExhibitionRepresentativeParticipatingStatusLog);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionRepresentativeParticipatingStatusLogs aExhibitionRepresentativeParticipatingStatusLog = XsiContext.Context.XsiExhibitionRepresentativeParticipatingStatusLogs.Find(itemId);
            XsiContext.Context.XsiExhibitionRepresentativeParticipatingStatusLogs.Remove(aExhibitionRepresentativeParticipatingStatusLog);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
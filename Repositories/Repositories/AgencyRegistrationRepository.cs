﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class AgencyRegistrationRepository : IAgencyRegistrationRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public AgencyRegistrationRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public AgencyRegistrationRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public AgencyRegistrationRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionMemberApplicationYearly GetById(long MemberExhibitionYearlyId)
        {
            return XsiContext.Context.XsiExhibitionMemberApplicationYearly.Find(MemberExhibitionYearlyId);
        }

        public List<XsiExhibitionMemberApplicationYearly> Select()
        {
            return XsiContext.Context.XsiExhibitionMemberApplicationYearly.Where(p => p.MemberRoleId == 2).ToList();
        }
        public List<XsiExhibitionMemberApplicationYearly> Select(XsiExhibitionMemberApplicationYearly entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMemberApplicationYearly>();
            if (!entity.MemberExhibitionYearlyId.IsDefault())
                predicate = predicate.And(p => p.MemberExhibitionYearlyId == entity.MemberExhibitionYearlyId);

            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            if (!entity.ParentId.IsDefault())
                predicate = predicate.And(p => p.ParentId == entity.ParentId);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.CityId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.OtherCity.IsDefault())
                predicate = predicate.And(p => p.OtherCity.Contains(entity.OtherCity));

            if (!entity.LanguageUrl.IsDefault())
                predicate = predicate.And(p => p.LanguageUrl == entity.LanguageUrl);

            if (!entity.ExhibitionCategoryId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionCategoryId == entity.ExhibitionCategoryId);

            //if (!entity.IsRegisteredByExhibitor.IsDefault())
            //    predicate = predicate.And(p => p.IsRegisteredByExhibitor.Equals(entity.IsRegisteredByExhibitor));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsEditRequest.IsDefault())
                predicate = predicate.And(p => p.IsEditRequest.Equals(entity.IsEditRequest));

            if (!entity.IsStatusChanged.IsDefault())
                predicate = predicate.And(p => p.IsStatusChanged.Equals(entity.IsStatusChanged));

            //if (!entity.IsIncludedInInvoice.IsDefault())
            //    predicate = predicate.And(p => p.IsIncludedInInvoice.Equals(entity.IsIncludedInInvoice));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.PublisherNameAr.IsDefault())
                predicate = predicate.And(p => p.PublisherNameAr.Contains(entity.PublisherNameAr));

            if (!entity.PublisherNameEn.IsDefault())
                predicate = predicate.And(p => p.PublisherNameEn.Contains(entity.PublisherNameEn));

            if (!entity.ContactPersonName.IsDefault())
                predicate = predicate.And(p => p.ContactPersonName.Contains(entity.ContactPersonName));

            if (!entity.ContactPersonTitle.IsDefault())
                predicate = predicate.And(p => p.ContactPersonTitle.Contains(entity.ContactPersonTitle));

            if (!entity.ContactPersonNameAr.IsDefault())
                predicate = predicate.And(p => p.ContactPersonNameAr.Contains(entity.ContactPersonNameAr));

            if (!entity.ContactPersonTitleAr.IsDefault())
                predicate = predicate.And(p => p.ContactPersonTitleAr.Contains(entity.ContactPersonTitleAr));

            if (!entity.Phone.IsDefault())
                predicate = predicate.And(p => p.Phone.Contains(entity.Phone));

            if (!entity.TotalNumberOfTitles.IsDefault())
                predicate = predicate.And(p => p.TotalNumberOfTitles.Equals(entity.TotalNumberOfTitles));

            if (!entity.TotalNumberOfNewTitles.IsDefault())
                predicate = predicate.And(p => p.TotalNumberOfNewTitles.Equals(entity.TotalNumberOfNewTitles));

            if (!entity.FileNumber.IsDefault())
                predicate = predicate.And(p => p.FileNumber.Equals(entity.FileNumber));

            if (!entity.Mobile.IsDefault())
                predicate = predicate.And(p => p.Mobile.Equals(entity.Mobile));

            if (!entity.Website.IsDefault())
                predicate = predicate.And(p => p.Website.Equals(entity.Website));

            if (!entity.Fax.IsDefault())
                predicate = predicate.And(p => p.Fax.Equals(entity.Fax));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Contains(entity.Email));

            if (!entity.AuthorizationLetter.IsDefault())
                predicate = predicate.And(p => p.AuthorizationLetter.Contains(entity.AuthorizationLetter));

            if (!entity.ExactNameBoard.IsDefault())
                predicate = predicate.And(p => p.ExactNameBoard.Equals(entity.ExactNameBoard));

            if (!entity.UploadLocationMap.IsDefault())
                predicate = predicate.And(p => p.UploadLocationMap.Equals(entity.UploadLocationMap));

            if (!entity.Address.IsDefault())
                predicate = predicate.And(p => p.Address.Equals(entity.Address));

            if (!entity.Notes.IsDefault())
                predicate = predicate.And(p => p.Notes.Equals(entity.Notes));

            if (!entity.TempId.IsDefault())
                predicate = predicate.And(p => p.TempId.Equals(entity.TempId));

            //if (!entity.NoofParcel.IsDefault())
            //    predicate = predicate.And(p => p.NoofParcel.Contains(entity.NoofParcel));

            //if (!entity.InvoiceNumber.IsDefault())
            //    predicate = predicate.And(p => p.InvoiceNumber.Equals(entity.InvoiceNumber));

            if (!entity.IsSampleReceived.IsDefault())
                predicate = predicate.And(p => p.IsSampleReceived.Equals(entity.IsSampleReceived));

            if (!entity.SampleReceivedOn.IsDefault())
                predicate = predicate.And(p => p.SampleReceivedOn == entity.SampleReceivedOn);

            if (!entity.IsBooksUpload.IsDefault())
            {
                if (entity.IsBooksUpload == "Y")
                {
                    predicate = predicate.And(p => p.IsBooksUpload.Equals(entity.IsBooksUpload));
                }
                else
                    predicate = predicate.And(p => (p.IsBooksUpload != "Y" || p.IsBooksUpload == null));
            }

            if (!entity.BookFileName.IsDefault())
                predicate = predicate.And(p => p.BookFileName == entity.BookFileName);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiExhibitionMemberApplicationYearly.AsExpandable().Where(predicate).Where(p => p.MemberRoleId == 2).ToList();
        }
        public List<string> SelectAgencyName(XsiExhibitionMemberApplicationYearly entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMemberApplicationYearly>();
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));
            if (!entity.PublisherNameEn.IsDefault())
                predicate = predicate.And(p => p.PublisherNameEn.ToLower().Contains(entity.PublisherNameEn));
            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);
            return XsiContext.Context.XsiExhibitionMemberApplicationYearly.AsExpandable().Where(predicate).Where(p => p.MemberRoleId == 2)
                .Select(p => p.PublisherNameEn)
                .Distinct().Take(50).ToList();
        }
        public List<string> SelectAgencyNameAr(XsiExhibitionMemberApplicationYearly entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMemberApplicationYearly>();
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));
            if (!entity.PublisherNameAr.IsDefault())
                predicate = predicate.And(p => p.PublisherNameAr.Contains(entity.PublisherNameAr));
            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);
            return XsiContext.Context.XsiExhibitionMemberApplicationYearly.AsExpandable().Where(predicate).Where(p => p.MemberRoleId == 2)
                .Select(p => p.PublisherNameAr)
                .Distinct().Take(50).ToList();
        }
        public List<XsiExhibitionMemberApplicationYearly> SelectExact(XsiExhibitionMemberApplicationYearly entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMemberApplicationYearly>();
            if (!entity.MemberExhibitionYearlyId.IsDefault())
                predicate = predicate.And(p => p.MemberExhibitionYearlyId == entity.MemberExhibitionYearlyId);

            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            if (!entity.ParentId.IsDefault())
                predicate = predicate.And(p => p.ParentId == entity.ParentId);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.CityId.IsDefault())
                predicate = predicate.And(p => p.CityId == entity.CityId);

            if (!entity.OtherCity.IsDefault())
                predicate = predicate.And(p => p.OtherCity.Contains(entity.OtherCity));

            if (!entity.LanguageUrl.IsDefault())
                predicate = predicate.And(p => p.LanguageUrl == entity.LanguageUrl);

            if (!entity.ExhibitionCategoryId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionCategoryId == entity.ExhibitionCategoryId);

            //if (!entity.IsRegisteredByExhibitor.IsDefault())
            //    predicate = predicate.And(p => p.IsRegisteredByExhibitor.Equals(entity.IsRegisteredByExhibitor));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsEditRequest.IsDefault())
                predicate = predicate.And(p => p.IsEditRequest.Equals(entity.IsEditRequest));

            if (!entity.IsStatusChanged.IsDefault())
                predicate = predicate.And(p => p.IsStatusChanged.Equals(entity.IsStatusChanged));

            //if (!entity.IsIncludedInInvoice.IsDefault())
            //    predicate = predicate.And(p => p.IsIncludedInInvoice.Equals(entity.IsIncludedInInvoice));

            if (!entity.IsArabicPublisher.IsDefault())
                predicate = predicate.And(p => p.IsArabicPublisher.Equals(entity.IsArabicPublisher));

            if (!entity.IsArabicPublisher.IsDefault())
                predicate = predicate.And(p => p.IsArabicPublisher.Equals(entity.IsArabicPublisher));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.PublisherNameAr.IsDefault())
                predicate = predicate.And(p => p.PublisherNameAr.ToLower().Equals(entity.PublisherNameAr.ToLower()));

            if (!entity.PublisherNameEn.IsDefault())
                predicate = predicate.And(p => p.PublisherNameEn.ToLower().Equals(entity.PublisherNameEn.ToLower()));

            if (!entity.ContactPersonName.IsDefault())
                predicate = predicate.And(p => p.ContactPersonName.Contains(entity.ContactPersonName));

            if (!entity.ContactPersonTitle.IsDefault())
                predicate = predicate.And(p => p.ContactPersonTitle.Contains(entity.ContactPersonTitle));

            if (!entity.ContactPersonNameAr.IsDefault())
                predicate = predicate.And(p => p.ContactPersonNameAr.Contains(entity.ContactPersonNameAr));

            if (!entity.ContactPersonTitleAr.IsDefault())
                predicate = predicate.And(p => p.ContactPersonTitleAr.Contains(entity.ContactPersonTitleAr));

            if (!entity.Phone.IsDefault())
                predicate = predicate.And(p => p.Phone.Contains(entity.Phone));

            if (!entity.TotalNumberOfTitles.IsDefault())
                predicate = predicate.And(p => p.TotalNumberOfTitles.Equals(entity.TotalNumberOfTitles));

            if (!entity.TotalNumberOfNewTitles.IsDefault())
                predicate = predicate.And(p => p.TotalNumberOfNewTitles.Equals(entity.TotalNumberOfNewTitles));

            if (!entity.FileNumber.IsDefault())
                predicate = predicate.And(p => p.FileNumber.Equals(entity.FileNumber));

            if (!entity.Mobile.IsDefault())
                predicate = predicate.And(p => p.Mobile.Equals(entity.Mobile));

            if (!entity.Website.IsDefault())
                predicate = predicate.And(p => p.Website.Equals(entity.Website));

            if (!entity.Fax.IsDefault())
                predicate = predicate.And(p => p.Fax.Equals(entity.Fax));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Contains(entity.Email));

            if (!entity.ExactNameBoard.IsDefault())
                predicate = predicate.And(p => p.ExactNameBoard.Equals(entity.ExactNameBoard));

            if (!entity.UploadLocationMap.IsDefault())
                predicate = predicate.And(p => p.UploadLocationMap.Equals(entity.UploadLocationMap));

            if (!entity.Address.IsDefault())
                predicate = predicate.And(p => p.Address.Equals(entity.Address));

            if (!entity.Notes.IsDefault())
                predicate = predicate.And(p => p.Notes.Equals(entity.Notes));

            if (!entity.AuthorizationLetter.IsDefault())
                predicate = predicate.And(p => p.AuthorizationLetter.Equals(entity.AuthorizationLetter));

            if (!entity.TempId.IsDefault())
                predicate = predicate.And(p => p.TempId.Equals(entity.TempId));

            //if (!entity.NoofParcel.IsDefault())
            //    predicate = predicate.And(p => p.NoofParcel.Contains(entity.NoofParcel));

            //if (!entity.InvoiceNumber.IsDefault())
            //    predicate = predicate.And(p => p.InvoiceNumber.Equals(entity.InvoiceNumber));

            if (!entity.IsSampleReceived.IsDefault())
                predicate = predicate.And(p => p.IsSampleReceived.Equals(entity.IsSampleReceived));

            if (!entity.SampleReceivedOn.IsDefault())
                predicate = predicate.And(p => p.SampleReceivedOn == entity.SampleReceivedOn);

            if (!entity.IsBooksUpload.IsDefault())
            {
                if (entity.IsBooksUpload == "Y")
                {
                    predicate = predicate.And(p => p.IsBooksUpload.Equals(entity.IsBooksUpload));
                }
                else
                    predicate = predicate.And(p => (p.IsBooksUpload != "Y" || p.IsBooksUpload == null));
            }

            if (!entity.BookFileName.IsDefault())
                predicate = predicate.And(p => p.BookFileName == entity.BookFileName);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionMemberApplicationYearly.AsExpandable().Where(predicate).Where(p => p.MemberRoleId == 2).ToList();
        }

        public XsiExhibitionMemberApplicationYearly Add(XsiExhibitionMemberApplicationYearly entity)
        {
            XsiExhibitionMemberApplicationYearly AgencyRegistration = new XsiExhibitionMemberApplicationYearly();
            AgencyRegistration.MemberId = entity.MemberId;
            AgencyRegistration.ParentId = entity.ParentId;
            AgencyRegistration.ExhibitionId = entity.ExhibitionId;
            AgencyRegistration.CountryId = entity.CountryId;
            AgencyRegistration.CityId = entity.CityId;
            if (!entity.OtherCity.IsDefault())
                AgencyRegistration.OtherCity = entity.OtherCity.Trim();
            AgencyRegistration.ExhibitionCategoryId = entity.ExhibitionCategoryId;
            AgencyRegistration.LanguageUrl = entity.LanguageUrl;
            //AgencyRegistration.IsRegisteredByExhibitor = entity.IsRegisteredByExhibitor;
            AgencyRegistration.IsActive = entity.IsActive;
            AgencyRegistration.IsEditRequest = entity.IsEditRequest;
            AgencyRegistration.IsStatusChanged = entity.IsStatusChanged;
            //AgencyRegistration.IsIncludedInInvoice = entity.IsIncludedInInvoice;
            AgencyRegistration.Status = entity.Status;
            if (!entity.PublisherNameAr.IsDefault())
                AgencyRegistration.PublisherNameAr = entity.PublisherNameAr.Trim();
            if (!entity.PublisherNameEn.IsDefault())
                AgencyRegistration.PublisherNameEn = entity.PublisherNameEn.Trim();
            if (!entity.ContactPersonName.IsDefault())
                AgencyRegistration.ContactPersonName = entity.ContactPersonName.Trim();
            if (!entity.ContactPersonTitle.IsDefault())
                AgencyRegistration.ContactPersonTitle = entity.ContactPersonTitle.Trim();
            if (!entity.ContactPersonNameAr.IsDefault())
                AgencyRegistration.ContactPersonNameAr = entity.ContactPersonNameAr.Trim();
            if (!entity.ContactPersonTitleAr.IsDefault())
                AgencyRegistration.ContactPersonTitleAr = entity.ContactPersonTitleAr.Trim();
            if (!entity.Phone.IsDefault())
                AgencyRegistration.Phone = entity.Phone.Trim();
            if (!entity.Fax.IsDefault())
                AgencyRegistration.Fax = entity.Fax.Trim();
            if (!entity.Email.IsDefault())
                AgencyRegistration.Email = entity.Email.Trim();
            if (!entity.TotalNumberOfTitles.IsDefault())
                AgencyRegistration.TotalNumberOfTitles = entity.TotalNumberOfTitles.Trim();
            if (!entity.TotalNumberOfNewTitles.IsDefault())
                AgencyRegistration.TotalNumberOfNewTitles = entity.TotalNumberOfNewTitles.Trim();
            if (!entity.FileNumber.IsDefault())
                AgencyRegistration.FileNumber = entity.FileNumber.Trim();
            if (!entity.Mobile.IsDefault())
                AgencyRegistration.Mobile = entity.Mobile.Trim();
            if (!entity.Website.IsDefault())
                AgencyRegistration.Website = entity.Website.Trim();
            if (!entity.ExactNameBoard.IsDefault())
                AgencyRegistration.ExactNameBoard = entity.ExactNameBoard.Trim();
            AgencyRegistration.UploadLocationMap = entity.UploadLocationMap;
            if (!entity.Address.IsDefault())
                AgencyRegistration.Address = entity.Address.Trim();
            if (!entity.AuthorizationLetter.IsDefault())
                AgencyRegistration.AuthorizationLetter = entity.AuthorizationLetter.Trim();
            if (!entity.Notes.IsDefault())
                AgencyRegistration.Notes = entity.Notes.Trim();

            //if (!entity.NoofParcel.IsDefault())
            //    AgencyRegistration.NoofParcel = entity.NoofParcel.Trim();

            //if (!entity.InvoiceNumber.IsDefault())
            //    AgencyRegistration.InvoiceNumber = entity.InvoiceNumber.Trim();

            if (!entity.IsSampleReceived.IsDefault())
                AgencyRegistration.IsSampleReceived = entity.IsSampleReceived;

            if (!entity.SampleReceivedOn.IsDefault())
                AgencyRegistration.SampleReceivedOn = entity.SampleReceivedOn;

            if (!entity.IsBooksUpload.IsDefault())
                AgencyRegistration.IsBooksUpload = entity.IsBooksUpload;

            if (!entity.BookFileName.IsDefault())
                AgencyRegistration.BookFileName = entity.BookFileName;

            AgencyRegistration.TempId = entity.TempId;
            AgencyRegistration.CreatedOn = entity.CreatedOn;
            AgencyRegistration.CreatedBy = entity.CreatedBy;
            AgencyRegistration.ModifiedOn = entity.ModifiedOn;
            AgencyRegistration.ModifiedBy = entity.ModifiedBy;
            AgencyRegistration.MemberRoleId = 2;

            XsiContext.Context.XsiExhibitionMemberApplicationYearly.Add(AgencyRegistration);
            SubmitChanges();
            return AgencyRegistration;

        }
        public void Update(XsiExhibitionMemberApplicationYearly entity)
        {
            XsiExhibitionMemberApplicationYearly AgencyRegistration = XsiContext.Context.XsiExhibitionMemberApplicationYearly.Find(entity.MemberExhibitionYearlyId);
            XsiContext.Context.Entry(AgencyRegistration).State = EntityState.Modified;

            if (!entity.CountryId.IsDefault())
                AgencyRegistration.CountryId = entity.CountryId;

            if (!entity.CityId.IsDefault())
                AgencyRegistration.CityId = entity.CityId;

            if (!entity.OtherCity.IsDefault())
                AgencyRegistration.OtherCity = entity.OtherCity.Trim();

            if (!entity.ExhibitionCategoryId.IsDefault())
                AgencyRegistration.ExhibitionCategoryId = entity.ExhibitionCategoryId;

            if (!entity.ParentId.IsDefault())
                AgencyRegistration.ParentId = entity.ParentId;

            //if (!entity.IsRegisteredByExhibitor.IsDefault())
            //    AgencyRegistration.IsRegisteredByExhibitor = entity.IsRegisteredByExhibitor;

            if (!entity.IsActive.IsDefault())
                AgencyRegistration.IsActive = entity.IsActive;

            if (!entity.IsEditRequest.IsDefault())
                AgencyRegistration.IsEditRequest = entity.IsEditRequest;

            if (!entity.IsStatusChanged.IsDefault())
                AgencyRegistration.IsStatusChanged = entity.IsStatusChanged;

            //if (!entity.IsIncludedInInvoice.IsDefault())
            //    AgencyRegistration.IsIncludedInInvoice = entity.IsIncludedInInvoice;

            if (!entity.Status.IsDefault())
                AgencyRegistration.Status = entity.Status;

            if (!entity.PublisherNameAr.IsDefault())
                AgencyRegistration.PublisherNameAr = entity.PublisherNameAr.Trim();

            if (!entity.PublisherNameEn.IsDefault())
                AgencyRegistration.PublisherNameEn = entity.PublisherNameEn.Trim();

            if (!entity.ContactPersonName.IsDefault())
                AgencyRegistration.ContactPersonName = entity.ContactPersonName.Trim();

            if (!entity.ContactPersonTitle.IsDefault())
                AgencyRegistration.ContactPersonTitle = entity.ContactPersonTitle.Trim();

            if (!entity.ContactPersonNameAr.IsDefault())
                AgencyRegistration.ContactPersonNameAr = entity.ContactPersonNameAr.Trim();

            if (!entity.ContactPersonTitleAr.IsDefault())
                AgencyRegistration.ContactPersonTitleAr = entity.ContactPersonTitleAr.Trim();

            if (!entity.Phone.IsDefault())
                AgencyRegistration.Phone = entity.Phone.Trim();

            if (!entity.Fax.IsDefault())
                AgencyRegistration.Fax = entity.Fax.Trim();

            if (!entity.Email.IsDefault())
                AgencyRegistration.Email = entity.Email.Trim();

            if (!entity.TotalNumberOfTitles.IsDefault())
                AgencyRegistration.TotalNumberOfTitles = entity.TotalNumberOfTitles.Trim();

            if (!entity.TotalNumberOfNewTitles.IsDefault())
                AgencyRegistration.TotalNumberOfNewTitles = entity.TotalNumberOfNewTitles.Trim();

            if (!entity.FileNumber.IsDefault())
                AgencyRegistration.FileNumber = entity.FileNumber.Trim();

            if (!entity.Mobile.IsDefault())
                AgencyRegistration.Mobile = entity.Mobile.Trim();

            if (!entity.Website.IsDefault())
                AgencyRegistration.Website = entity.Website.Trim();

            if (!entity.ExactNameBoard.IsDefault())
                AgencyRegistration.ExactNameBoard = entity.ExactNameBoard.Trim();

            if (!entity.UploadLocationMap.IsDefault())
                AgencyRegistration.UploadLocationMap = entity.UploadLocationMap;

            if (!entity.Address.IsDefault())
                AgencyRegistration.Address = entity.Address.Trim();

            if (!entity.Notes.IsDefault())
                AgencyRegistration.Notes = entity.Notes.Trim();

            if (!entity.AuthorizationLetter.IsDefault())
                AgencyRegistration.AuthorizationLetter = entity.AuthorizationLetter.Trim();

            //if (!entity.NoofParcel.IsDefault())
            //    AgencyRegistration.NoofParcel = entity.NoofParcel.Trim();

            //if (!entity.InvoiceNumber.IsDefault())
            //    AgencyRegistration.InvoiceNumber = entity.InvoiceNumber.Trim();

            if (!entity.IsSampleReceived.IsDefault())
                AgencyRegistration.IsSampleReceived = entity.IsSampleReceived;

            if (!entity.SampleReceivedOn.IsDefault())
                AgencyRegistration.SampleReceivedOn = entity.SampleReceivedOn;

            if (!entity.IsBooksUpload.IsDefault())
                AgencyRegistration.IsBooksUpload = entity.IsBooksUpload;

            if (!entity.BookFileName.IsDefault())
                AgencyRegistration.BookFileName = entity.BookFileName;

            if (!entity.TempId.IsDefault())
                AgencyRegistration.TempId = entity.TempId;

            if (!entity.ModifiedBy.IsDefault())
                AgencyRegistration.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                AgencyRegistration.ModifiedOn = entity.ModifiedOn;
            AgencyRegistration.MemberRoleId = 2;

            //if (!entity.SIBF_ApplicantID.IsDefault())
            //    AgencyRegistration.SIBF_ApplicantID = entity.SIBF_ApplicantID;

        }
        public void Delete(XsiExhibitionMemberApplicationYearly entity)
        {
            foreach (XsiExhibitionMemberApplicationYearly AgencyRegistration in Select(entity))
            {
                XsiExhibitionMemberApplicationYearly aAgencyRegistration = XsiContext.Context.XsiExhibitionMemberApplicationYearly.Find(AgencyRegistration.MemberExhibitionYearlyId);
                XsiContext.Context.XsiExhibitionMemberApplicationYearly.Remove(aAgencyRegistration);
            }
        }
        public void Delete(long MemberExhibitionYearlyId)
        {
            XsiExhibitionMemberApplicationYearly aAgencyRegistration = XsiContext.Context.XsiExhibitionMemberApplicationYearly.Find(MemberExhibitionYearlyId);
            XsiContext.Context.XsiExhibitionMemberApplicationYearly.Remove(aAgencyRegistration);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
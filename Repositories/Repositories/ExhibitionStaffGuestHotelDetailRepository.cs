﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionStaffGuestHotelDetailRepository : IExhibitionStaffGuestHotelDetailsRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionStaffGuestHotelDetailRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionStaffGuestHotelDetailRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionStaffGuestHotelDetailRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionStaffGuestHotelDetails GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionStaffGuestHotelDetails.Find(itemId);
        }
        public List<XsiExhibitionStaffGuestHotelDetails> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionStaffGuestHotelDetails> Select()
        {
            return XsiContext.Context.XsiExhibitionStaffGuestHotelDetails.ToList();
        }

        public List<XsiExhibitionStaffGuestHotelDetails> Select(XsiExhibitionStaffGuestHotelDetails entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionStaffGuestHotelDetails>();

            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.StaffGuestId.IsDefault())
                predicate = predicate.And(p => p.StaffGuestId == entity.StaffGuestId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            #region Hotel
            if (!entity.HotelStartDate.IsDefault())
                predicate = predicate.And(p => p.HotelStartDate == entity.HotelStartDate);

            if (!entity.HotelEndDate.IsDefault())
                predicate = predicate.And(p => p.HotelEndDate == entity.HotelEndDate);

            if (!entity.RoomType.IsDefault())
                predicate = predicate.And(p => p.RoomType.Equals(entity.RoomType));

            if (!entity.SpecialRequest.IsDefault())
                predicate = predicate.And(p => p.SpecialRequest.Contains(entity.SpecialRequest));

            if (!entity.ReservationReferenceNumber.IsDefault())
                predicate = predicate.And(p => p.ReservationReferenceNumber.Contains(entity.ReservationReferenceNumber));

            if (!entity.HotelName.IsDefault())
                predicate = predicate.And(p => p.HotelName.Contains(entity.HotelName));

            if (!entity.HotelAddress.IsDefault())
                predicate = predicate.And(p => p.HotelAddress.Contains(entity.HotelAddress));

            if (!entity.HotelPhoneNumber.IsDefault())
                predicate = predicate.And(p => p.HotelPhoneNumber.Contains(entity.HotelPhoneNumber));

            if (!entity.HotelStartDate.IsDefault())
                predicate = predicate.And(p => p.HotelStartDate == entity.HotelStartDate);

            if (!entity.HotelEndDate.IsDefault())
                predicate = predicate.And(p => p.HotelEndDate == entity.HotelEndDate);

            if (!entity.CheckInTime.IsDefault())
                predicate = predicate.And(p => p.CheckInTime.Contains(entity.CheckInTime));
          
            if (!entity.CheckOutTime.IsDefault())
                predicate = predicate.And(p => p.CheckOutTime.Contains(entity.CheckOutTime));

            if (!entity.RoomTypeAllotted.IsDefault())
                predicate = predicate.And(p => p.RoomTypeAllotted.Equals(entity.RoomTypeAllotted));

            if (!entity.Notes.IsDefault())
                predicate = predicate.And(p => p.Notes.Contains(entity.Notes));

            if (!entity.ReservationCopy.IsDefault())
                predicate = predicate.And(p => p.ReservationCopy.Contains(entity.ReservationCopy));
            #endregion

            if (!entity.HotelStatus.IsDefault())
                predicate = predicate.And(p => p.HotelStatus.Equals(entity.HotelStatus));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionStaffGuestHotelDetails.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionStaffGuestHotelDetails Add(XsiExhibitionStaffGuestHotelDetails entity)
        {
            XsiExhibitionStaffGuestHotelDetails detailsentity = new XsiExhibitionStaffGuestHotelDetails();

            detailsentity.StaffGuestId = entity.StaffGuestId;
            detailsentity.ExhibitionId = entity.ExhibitionId;
         
            #region Hotel Details
            detailsentity.HotelBookingStartDate = entity.HotelBookingStartDate;
            detailsentity.HotelBookingEndDate = entity.HotelBookingEndDate;
            detailsentity.RoomType = entity.RoomType;
            detailsentity.SpecialRequest = entity.SpecialRequest;
            detailsentity.HotelStatus = entity.HotelStatus;
            detailsentity.ReservationReferenceNumber = entity.ReservationReferenceNumber;
            detailsentity.HotelName = entity.HotelName;
            detailsentity.HotelAddress = entity.HotelAddress;
            detailsentity.HotelPhoneNumber = entity.HotelPhoneNumber;
            detailsentity.HotelStartDate = entity.HotelStartDate;
            detailsentity.HotelEndDate = entity.HotelEndDate;
            detailsentity.CheckInTime = entity.CheckInTime;
            detailsentity.CheckOutTime = entity.CheckOutTime;
            detailsentity.RoomTypeAllotted = entity.RoomTypeAllotted;
            detailsentity.Notes = entity.Notes;
            detailsentity.ReservationCopy = entity.ReservationCopy;
            #endregion

            detailsentity.IsActive = entity.IsActive;
            detailsentity.CreatedOn = entity.CreatedOn;
            detailsentity.CreatedBy = entity.CreatedBy;
            detailsentity.ModifiedOn = entity.ModifiedOn;
            detailsentity.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionStaffGuestHotelDetails.Add(detailsentity);
            SubmitChanges();
            return detailsentity;

        }
        public void Update(XsiExhibitionStaffGuestHotelDetails entity)
        {
            XsiExhibitionStaffGuestHotelDetails detailsentity = XsiContext.Context.XsiExhibitionStaffGuestHotelDetails.Find(entity.ItemId); 

            XsiContext.Context.Entry(detailsentity).State = EntityState.Modified;

            if (!entity.StaffGuestId.IsDefault())
                detailsentity.StaffGuestId = entity.StaffGuestId;

            if (!entity.ExhibitionId.IsDefault())
                detailsentity.ExhibitionId = entity.ExhibitionId;

            #region Hotel Details

            if (!entity.HotelStartDate.IsDefault())
                detailsentity.HotelStartDate = entity.HotelStartDate;

            if (!entity.HotelEndDate.IsDefault())
                detailsentity.HotelEndDate = entity.HotelEndDate;

            if (!entity.RoomType.IsDefault())
                detailsentity.RoomType = entity.RoomType;

            if (!entity.SpecialRequest.IsDefault())
                detailsentity.SpecialRequest = entity.SpecialRequest;

            if (!entity.HotelStatus.IsDefault())
                detailsentity.HotelStatus = entity.HotelStatus;

            if (!entity.ReservationReferenceNumber.IsDefault())
                detailsentity.ReservationReferenceNumber = entity.ReservationReferenceNumber;

            if (!entity.HotelName.IsDefault())
                detailsentity.HotelName = entity.HotelName;

            if (!entity.HotelAddress.IsDefault())
                detailsentity.HotelAddress = entity.HotelAddress.Trim();

            if (!entity.HotelPhoneNumber.IsDefault())
                detailsentity.HotelPhoneNumber = entity.HotelPhoneNumber.Trim();

            if (!entity.HotelStartDate.IsDefault())
                detailsentity.HotelStartDate = entity.HotelStartDate;

            if (!entity.HotelEndDate.IsDefault())
                detailsentity.HotelEndDate = entity.HotelEndDate;

            if (!entity.CheckInTime.IsDefault())
                detailsentity.CheckInTime = entity.CheckInTime;

            if (!entity.CheckOutTime.IsDefault())
                detailsentity.CheckOutTime = entity.CheckOutTime;

            if (!entity.RoomTypeAllotted.IsDefault())
                detailsentity.RoomTypeAllotted = entity.RoomTypeAllotted;

            if (!entity.Notes.IsDefault())
                detailsentity.Notes = entity.Notes;

            if (!entity.ReservationCopy.IsDefault())
                detailsentity.ReservationCopy = entity.ReservationCopy;

            #endregion

            if (!entity.IsActive.IsDefault())
                detailsentity.IsActive = entity.IsActive;

            if (!entity.ModifiedBy.IsDefault())
                detailsentity.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                detailsentity.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiExhibitionStaffGuestHotelDetails entity)
        {
            foreach (XsiExhibitionStaffGuestHotelDetails detailsentity in Select(entity))
            {
                XsiExhibitionStaffGuestHotelDetails aExhibitionStaffGuestParticipating = XsiContext.Context.XsiExhibitionStaffGuestHotelDetails.Find(detailsentity.ItemId);
                XsiContext.Context.XsiExhibitionStaffGuestHotelDetails.Remove(aExhibitionStaffGuestParticipating);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionStaffGuestHotelDetails detailsentity = XsiContext.Context.XsiExhibitionStaffGuestHotelDetails.Where(p => p.ItemId == itemId).OrderByDescending(o => o.ItemId).FirstOrDefault();
            XsiContext.Context.XsiExhibitionStaffGuestHotelDetails.Remove(detailsentity);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
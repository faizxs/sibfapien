﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionCountryAirportRepository : IExhibitionCountryAirportRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionCountryAirportRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionCountryAirportRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionCountryAirportRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionCountryAirport GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionCountryAirport.Find(itemId);
        }
        public List<XsiExhibitionCountryAirport> Select()
        {
            return XsiContext.Context.XsiExhibitionCountryAirport.ToList();
        }
        public List<XsiExhibitionCountryAirport> Select(XsiExhibitionCountryAirport entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionCountryAirport>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.AirportName.IsDefault())
                predicate = predicate.And(p => p.AirportName.Contains(entity.AirportName));

            if (!entity.AirportNameAr.IsDefault())
                predicate = predicate.And(p => p.AirportNameAr.Contains(entity.AirportNameAr));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionCountryAirport.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionCountryAirport> SelectComplete(XsiExhibitionCountryAirport entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionCountryAirport>();
            var predicate = PredicateBuilder.True<XsiExhibitionCountryAirport>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.CountryId.IsDefault())
            {
                predicate = predicate.And(p => p.CountryId == entity.CountryId);
                XsiExhibitionCountry MainEntity = XsiContext.Context.XsiExhibitionCountry.Where(p => p.CountryId == entity.CountryId).FirstOrDefault();

                if (!entity.AirportNameAr.IsDefault())
                {
                    string strSearchText = entity.AirportNameAr;
                    var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                    Inner = Inner.Or(p => p.AirportNameAr.Contains(searchKeys.str1) || p.AirportNameAr.Contains(searchKeys.str2) || p.AirportNameAr.Contains(searchKeys.str3) || p.AirportNameAr.Contains(searchKeys.str4) || p.AirportNameAr.Contains(searchKeys.str5)
                        || p.AirportNameAr.Contains(searchKeys.str6) || p.AirportNameAr.Contains(searchKeys.str7) || p.AirportNameAr.Contains(searchKeys.str8) || p.AirportNameAr.Contains(searchKeys.str9) || p.AirportNameAr.Contains(searchKeys.str10)
                        || p.AirportNameAr.Contains(searchKeys.str11) || p.AirportNameAr.Contains(searchKeys.str12) || p.AirportNameAr.Contains(searchKeys.str13)
                        || p.AirportNameAr.Contains(searchKeys.str14) || p.AirportNameAr.Contains(searchKeys.str15) || p.AirportNameAr.Contains(searchKeys.str16) || p.AirportNameAr.Contains(searchKeys.str17)
                        );

                    isInner = true;
                }
                else
                {
                    if (!entity.AirportName.IsDefault())
                    {
                        string strSearchText = entity.AirportName.ToLower();
                        Inner = Inner.Or(p => p.AirportName.ToLower().Contains(strSearchText));
                        isInner = true;
                    }
                }
            }
            else
            {
                if (!entity.AirportName.IsDefault())
                    predicate = predicate.And(p => p.AirportName.ToLower().Contains(entity.AirportName));
            }

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionCountryAirport.Include(p => p.Country).AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionCountryAirport Add(XsiExhibitionCountryAirport entity)
        {
            XsiExhibitionCountryAirport ExhibitionCountryAirport = new XsiExhibitionCountryAirport();
            ExhibitionCountryAirport.CountryId = entity.CountryId;
            if (!entity.AirportName.IsDefault())
                ExhibitionCountryAirport.AirportName = entity.AirportName.Trim();
            if (!entity.AirportNameAr.IsDefault())
                ExhibitionCountryAirport.AirportNameAr = entity.AirportNameAr.Trim();
            ExhibitionCountryAirport.IsActive = entity.IsActive;
            ExhibitionCountryAirport.CreatedOn = entity.CreatedOn;
            ExhibitionCountryAirport.CreatedBy = entity.CreatedBy;
            ExhibitionCountryAirport.ModifiedOn = entity.ModifiedOn;
            ExhibitionCountryAirport.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionCountryAirport.Add(ExhibitionCountryAirport);
            SubmitChanges();
            return ExhibitionCountryAirport;

        }
        public void Update(XsiExhibitionCountryAirport entity)
        {
            XsiExhibitionCountryAirport ExhibitionCountryAirport = XsiContext.Context.XsiExhibitionCountryAirport.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionCountryAirport).State = EntityState.Modified;

            if (!entity.AirportName.IsDefault())
                ExhibitionCountryAirport.AirportName = entity.AirportName.Trim();

            if (!entity.AirportNameAr.IsDefault())
                ExhibitionCountryAirport.AirportNameAr = entity.AirportNameAr.Trim();

            if (!entity.IsActive.IsDefault())
                ExhibitionCountryAirport.IsActive = entity.IsActive;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionCountryAirport.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionCountryAirport.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionCountryAirport entity)
        {
            foreach (XsiExhibitionCountryAirport ExhibitionCountryAirport in Select(entity))
            {
                XsiExhibitionCountryAirport aExhibitionCountryAirport = XsiContext.Context.XsiExhibitionCountryAirport.Find(ExhibitionCountryAirport.ItemId);
                XsiContext.Context.XsiExhibitionCountryAirport.Remove(aExhibitionCountryAirport);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionCountryAirport aExhibitionCountryAirport = XsiContext.Context.XsiExhibitionCountryAirport.Find(itemId);
            XsiContext.Context.XsiExhibitionCountryAirport.Remove(aExhibitionCountryAirport);
        }
        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
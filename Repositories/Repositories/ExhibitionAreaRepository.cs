﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionAreaRepository : IExhibitionAreaRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionAreaRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionAreaRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionAreaRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionArea GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionArea.Find(itemId);
        }
        public List<XsiExhibitionArea> Select()
        {
            return XsiContext.Context.XsiExhibitionArea.ToList();
        }
        public List<XsiExhibitionArea> Select(XsiExhibitionArea entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionArea>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));
            
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiExhibitionArea.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionArea Add(XsiExhibitionArea entity)
        {
            XsiExhibitionArea ExhibitionArea = new XsiExhibitionArea();
            if (!entity.Title.IsDefault())
                ExhibitionArea.Title = entity.Title.Trim();
            ExhibitionArea.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionArea.TitleAr = entity.TitleAr.Trim();
            ExhibitionArea.IsActiveAr = entity.IsActiveAr;
            #endregion Ar
            ExhibitionArea.CreatedOn = entity.CreatedOn;
            ExhibitionArea.CreatedBy = entity.CreatedBy;
            ExhibitionArea.ModifiedOn = entity.ModifiedOn;
            ExhibitionArea.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionArea.Add(ExhibitionArea);
            SubmitChanges();
            return ExhibitionArea;

        }
        public void Update(XsiExhibitionArea entity)
        {
            XsiExhibitionArea ExhibitionArea = XsiContext.Context.XsiExhibitionArea.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionArea).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionArea.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                ExhibitionArea.IsActive = entity.IsActive;
           
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionArea.TitleAr = entity.TitleAr.Trim();
            ExhibitionArea.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionArea.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                ExhibitionArea.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionArea entity)
        {
            foreach (XsiExhibitionArea ExhibitionArea in Select(entity))
            {
                XsiExhibitionArea aExhibitionArea = XsiContext.Context.XsiExhibitionArea.Find(ExhibitionArea.ItemId);
                XsiContext.Context.XsiExhibitionArea.Remove(aExhibitionArea);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionArea aExhibitionArea = XsiContext.Context.XsiExhibitionArea.Find(itemId);
            XsiContext.Context.XsiExhibitionArea.Remove(aExhibitionArea);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
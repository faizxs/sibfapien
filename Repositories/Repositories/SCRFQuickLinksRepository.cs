﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFQuickLinksRepository : ISCRFQuickLinksRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFQuickLinksRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFQuickLinksRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFQuickLinksRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfquickLinks GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfquickLinks.Find(itemId);
        }

        public List<XsiScrfquickLinks> Select()
        {
            return XsiContext.Context.XsiScrfquickLinks.ToList();
        }
        public List<XsiScrfquickLinks> SelectOr(XsiScrfquickLinks entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfquickLinks>();
            var Outer = PredicateBuilder.True<XsiScrfquickLinks>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.PageUrl.IsDefault())
                Outer = Outer.And(p => p.PageUrl.Equals(entity.PageUrl));

            if (!entity.PageContentId.IsDefault())
                Outer = Outer.And(p => p.PageContentId == entity.PageContentId);

            if (!entity.SortOrder.IsDefault())
                Outer = Outer.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.IsExternal.IsDefault())
                Outer = Outer.And(p => p.IsExternal.Equals(entity.IsExternal));

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiScrfquickLinks.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiScrfquickLinks> Select(XsiScrfquickLinks entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfquickLinks>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Equals(entity.Title));

            if (!entity.PageUrl.IsDefault())
                predicate = predicate.And(p => p.PageUrl.Equals(entity.PageUrl));

            if (!entity.PageContentId.IsDefault())
                predicate = predicate.And(p => p.PageContentId == entity.PageContentId);

            if (!entity.SortOrder.IsDefault())
                predicate = predicate.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.IsExternal.IsDefault())
                predicate = predicate.And(p => p.IsExternal.Equals(entity.IsExternal));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy.Equals(entity.CreatedBy));

            if (!entity.ModifyBy.IsDefault())
                predicate = predicate.And(p => p.ModifyBy.Equals(entity.ModifyBy));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn.Equals(entity.CreatedOn));

            if (!entity.ModifyOn.IsDefault())
                predicate = predicate.And(p => p.ModifyOn.Equals(entity.ModifyOn));

            return XsiContext.Context.XsiScrfquickLinks.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfquickLinks Add(XsiScrfquickLinks entity)
        {
            XsiScrfquickLinks SCRFQuickLinks = new XsiScrfquickLinks();
            if (!entity.Title.IsDefault())
                SCRFQuickLinks.Title = entity.Title.Trim();
            if (!entity.PageUrl.IsDefault())
                SCRFQuickLinks.PageUrl = entity.PageUrl.Trim();
            SCRFQuickLinks.PageContentId = entity.PageContentId;
            SCRFQuickLinks.SortOrder = entity.SortOrder;
            SCRFQuickLinks.IsExternal = entity.IsExternal;
            SCRFQuickLinks.IsActive = entity.IsActive;
            SCRFQuickLinks.CreatedBy = entity.CreatedBy;
            SCRFQuickLinks.CreatedOn = entity.CreatedOn;
            SCRFQuickLinks.ModifyOn = entity.ModifyOn;
            SCRFQuickLinks.ModifyBy = entity.ModifyBy;
            XsiContext.Context.XsiScrfquickLinks.Add(SCRFQuickLinks);
            SubmitChanges();
            return SCRFQuickLinks;

        }
        public void Update(XsiScrfquickLinks entity)
        {
            XsiScrfquickLinks SCRFQuickLinks = XsiContext.Context.XsiScrfquickLinks.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFQuickLinks).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                SCRFQuickLinks.Title = entity.Title.Trim();

            if (!entity.PageUrl.IsDefault())
                SCRFQuickLinks.PageUrl = entity.PageUrl.Trim();

            if (!entity.PageContentId.IsDefault())
                SCRFQuickLinks.PageContentId = entity.PageContentId;

            if (!entity.SortOrder.IsDefault())
                SCRFQuickLinks.SortOrder = entity.SortOrder;

            if (!entity.IsExternal.IsDefault())
                SCRFQuickLinks.IsExternal = entity.IsExternal;

            if (!entity.IsActive.IsDefault())
                SCRFQuickLinks.IsActive = entity.IsActive;

            if (!entity.ModifyBy.IsDefault())
                SCRFQuickLinks.ModifyBy = entity.ModifyBy;

            if (!entity.ModifyOn.IsDefault())
                SCRFQuickLinks.ModifyOn = entity.ModifyOn;
        }
        public void Delete(XsiScrfquickLinks entity)
        {
            foreach (XsiScrfquickLinks SCRFQuickLinks in Select(entity))
            {
                XsiScrfquickLinks aSCRFQuickLinks = XsiContext.Context.XsiScrfquickLinks.Find(SCRFQuickLinks.ItemId);
                XsiContext.Context.XsiScrfquickLinks.Remove(aSCRFQuickLinks);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfquickLinks aSCRFQuickLinks = XsiContext.Context.XsiScrfquickLinks.Find(itemId);
            XsiContext.Context.XsiScrfquickLinks.Remove(aSCRFQuickLinks);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
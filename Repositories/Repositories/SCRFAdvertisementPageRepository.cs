﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFAdvertisementPageRepository : ISCRFAdvertisementPageRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFAdvertisementPageRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFAdvertisementPageRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFAdvertisementPageRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfadvertisementPage GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfadvertisementPage.Find(itemId);
        }
        public List<XsiScrfadvertisementPage> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiScrfadvertisementPage> Select()
        {
            return XsiContext.Context.XsiScrfadvertisementPage.ToList();
        }
        public List<XsiScrfadvertisementPage> Select(XsiScrfadvertisementPage entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfadvertisementPage>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.AdvertisementId.IsDefault())
                predicate = predicate.And(p => p.AdvertisementId == entity.AdvertisementId);

            if (!entity.PageId.IsDefault())
                predicate = predicate.And(p => p.PageId == entity.PageId);


            return XsiContext.Context.XsiScrfadvertisementPage.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfadvertisementPage Add(XsiScrfadvertisementPage entity)
        {
            XsiScrfadvertisementPage SCRFAdvertisementPage = new XsiScrfadvertisementPage();
            SCRFAdvertisementPage.AdvertisementId = entity.AdvertisementId;
            SCRFAdvertisementPage.PageId = entity.PageId;

            XsiContext.Context.XsiScrfadvertisementPage.Add(SCRFAdvertisementPage);
            SubmitChanges();
            return SCRFAdvertisementPage;

        }
        public void Update(XsiScrfadvertisementPage entity)
        {
            XsiScrfadvertisementPage SCRFAdvertisementPage = XsiContext.Context.XsiScrfadvertisementPage.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFAdvertisementPage).State = EntityState.Modified;

            if (!entity.AdvertisementId.IsDefault())
                SCRFAdvertisementPage.AdvertisementId = entity.AdvertisementId;
            if (!entity.PageId.IsDefault())
                SCRFAdvertisementPage.PageId = entity.PageId;
        }
        public void Delete(XsiScrfadvertisementPage entity)
        {
            foreach (XsiScrfadvertisementPage SCRFAdvertisementPage in Select(entity))
            {
                XsiScrfadvertisementPage aSCRFAdvertisementPage = XsiContext.Context.XsiScrfadvertisementPage.Find(SCRFAdvertisementPage.ItemId);
                XsiContext.Context.XsiScrfadvertisementPage.Remove(aSCRFAdvertisementPage);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfadvertisementPage aSCRFAdvertisementPage = XsiContext.Context.XsiScrfadvertisementPage.Find(itemId);
            XsiContext.Context.XsiScrfadvertisementPage.Remove(aSCRFAdvertisementPage);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
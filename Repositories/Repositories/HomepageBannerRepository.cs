﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class HomepageBannerRepository : IHomepageBannerRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public HomepageBannerRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public HomepageBannerRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public HomepageBannerRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiHomepageBanner GetById(long itemId)
        {
            return XsiContext.Context.XsiHomepageBanner.Find(itemId);
        }

        public List<XsiHomepageBanner> Select()
        {
            return XsiContext.Context.XsiHomepageBanner.ToList();
        }
        public List<XsiHomepageBanner> Select(XsiHomepageBanner entity)
        {
            var predicate = PredicateBuilder.True<XsiHomepageBanner>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Cmstitle.IsDefault())
                predicate = predicate.And(p => p.Cmstitle.Contains(entity.Cmstitle));
            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.SortOrder.IsDefault())
                predicate = predicate.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.SubTitle.IsDefault())
                predicate = predicate.And(p => p.SubTitle.Contains(entity.SubTitle));
            if (!entity.HomeBanner.IsDefault())
                predicate = predicate.And(p => p.HomeBanner.Equals(entity.HomeBanner));
            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.IsFeatured.IsDefault())
                predicate = predicate.And(p => p.IsFeatured.Equals(entity.IsFeatured));
            return XsiContext.Context.XsiHomepageBanner.AsExpandable().Where(predicate).ToList();
        }

        public XsiHomepageBanner Add(XsiHomepageBanner entity)
        {
            XsiHomepageBanner HomepageBanner = new XsiHomepageBanner();
            if (!entity.Cmstitle.IsDefault())
                HomepageBanner.Cmstitle = entity.Cmstitle.Trim();
            if (!entity.Title.IsDefault())
                HomepageBanner.Title = entity.Title.Trim();
            if (!entity.SubTitle.IsDefault())
                HomepageBanner.SubTitle = entity.SubTitle.Trim();
            HomepageBanner.HomeBanner = entity.HomeBanner;
            if (!entity.Url.IsDefault())
                HomepageBanner.Url = entity.Url.Trim();
            HomepageBanner.IsActive = entity.IsActive;
            HomepageBanner.IsFeatured = entity.IsFeatured;
            HomepageBanner.SortOrder = entity.SortOrder;
            HomepageBanner.CreatedOn = entity.CreatedOn;
            HomepageBanner.CreatedBy = entity.CreatedBy;
            HomepageBanner.ModifiedOn = entity.ModifiedOn;
            HomepageBanner.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiHomepageBanner.Add(HomepageBanner);
            SubmitChanges();
            return HomepageBanner;

        }
        public void Update(XsiHomepageBanner entity)
        {
            XsiHomepageBanner HomepageBanner = XsiContext.Context.XsiHomepageBanner.Find(entity.ItemId);
            XsiContext.Context.Entry(HomepageBanner).State = EntityState.Modified;

            if (!entity.Cmstitle.IsDefault())
                HomepageBanner.Cmstitle = entity.Cmstitle.Trim();
            if (!entity.Title.IsDefault())
                HomepageBanner.Title = entity.Title.Trim();
            if (!entity.SubTitle.IsDefault())
                HomepageBanner.SubTitle = entity.SubTitle.Trim();
            if (!entity.HomeBanner.IsDefault())
                HomepageBanner.HomeBanner = entity.HomeBanner;
            if (!entity.Url.IsDefault())
                HomepageBanner.Url = entity.Url.Trim();
            if (!entity.IsActive.IsDefault())
                HomepageBanner.IsActive = entity.IsActive;
            if (!entity.IsFeatured.IsDefault())
                HomepageBanner.IsFeatured = entity.IsFeatured;
            if (!entity.SortOrder.IsDefault())
                HomepageBanner.SortOrder = entity.SortOrder;
            if (!entity.ModifiedOn.IsDefault())
                HomepageBanner.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                HomepageBanner.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiHomepageBanner entity)
        {
            foreach (XsiHomepageBanner HomepageBanner in Select(entity))
            {
                XsiHomepageBanner aHomepageBanner = XsiContext.Context.XsiHomepageBanner.Find(HomepageBanner.ItemId);
                XsiContext.Context.XsiHomepageBanner.Remove(aHomepageBanner);
            }
        }
        public void Delete(long itemId)
        {
            XsiHomepageBanner aHomepageBanner = XsiContext.Context.XsiHomepageBanner.Find(itemId);
            XsiContext.Context.XsiHomepageBanner.Remove(aHomepageBanner);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
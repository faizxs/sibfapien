﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitorRepository : IExhibitorRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitorRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitorRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitorRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitor GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitor.Find(itemId);
        }
         

        public List<XsiExhibitor> Select()
        {
            return XsiContext.Context.XsiExhibitor.ToList();
        }
        public List<XsiExhibitor> SelectOr(XsiExhibitor entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitor>();
            var Outer = PredicateBuilder.True<XsiExhibitor>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
             

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitor.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiExhibitor> Select(XsiExhibitor entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitor>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Contains(entity.ImageName));
                        
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiExhibitor.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitor Add(XsiExhibitor entity)
        {
            XsiExhibitor Exhibitor = new XsiExhibitor();
             
            if (!entity.Title.IsDefault())
                Exhibitor.Title = entity.Title.Trim();
            if (!entity.Overview.IsDefault())
                Exhibitor.Overview = entity.Overview.Trim();
            if (!entity.Url.IsDefault())
                Exhibitor.Url = entity.Url;
            if (!entity.ImageName.IsDefault())
                Exhibitor.ImageName = entity.ImageName;
            Exhibitor.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                Exhibitor.TitleAr = entity.TitleAr.Trim();
            if (!entity.OverviewAr.IsDefault())
                Exhibitor.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.Urlar.IsDefault())
                Exhibitor.Urlar = entity.Urlar;
            if (!entity.ImageNameAr.IsDefault())
                Exhibitor.ImageNameAr = entity.ImageNameAr;
            Exhibitor.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            Exhibitor.CreatedOn = entity.CreatedOn;
            Exhibitor.CreatedBy = entity.CreatedBy;
            Exhibitor.ModifiedOn = entity.ModifiedOn;
            Exhibitor.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiExhibitor.Add(Exhibitor);
            SubmitChanges();
            return Exhibitor;
        }
        public void Update(XsiExhibitor entity)
        {
            XsiExhibitor Exhibitor = XsiContext.Context.XsiExhibitor.Find(entity.ItemId);
            XsiContext.Context.Entry(Exhibitor).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                Exhibitor.Title = entity.Title.Trim();
            if (!entity.Overview.IsDefault())
                Exhibitor.Overview = entity.Overview.Trim();
            if (!entity.Url.IsDefault())
                Exhibitor.Url = entity.Url;
            if (!entity.ImageName.IsDefault())
                Exhibitor.ImageName = entity.ImageName;
            if (!entity.IsActive.IsDefault())
                Exhibitor.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                Exhibitor.TitleAr = entity.TitleAr.Trim();
            if (!entity.OverviewAr.IsDefault())
                Exhibitor.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.Urlar.IsDefault())
                Exhibitor.Urlar = entity.Urlar;
            if (!entity.ImageNameAr.IsDefault())
                Exhibitor.ImageNameAr = entity.ImageNameAr;
            Exhibitor.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                Exhibitor.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                Exhibitor.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitor entity)
        {
            foreach (XsiExhibitor Exhibitor in Select(entity))
            {
                XsiExhibitor aExhibitor = XsiContext.Context.XsiExhibitor.Find(Exhibitor.ItemId);
                XsiContext.Context.XsiExhibitor.Remove(aExhibitor);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitor aExhibitor = XsiContext.Context.XsiExhibitor.Find(itemId);
            XsiContext.Context.XsiExhibitor.Remove(aExhibitor);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
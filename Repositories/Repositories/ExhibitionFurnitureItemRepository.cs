﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionFurnitureItemRepository : IExhibitionFurnitureItemRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionFurnitureItemRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionFurnitureItemRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionFurnitureItemRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionFurnitureItems GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionFurnitureItems.Find(itemId);
        }

        public List<XsiExhibitionFurnitureItems> Select()
        {
            return XsiContext.Context.XsiExhibitionFurnitureItems.ToList();
        }

        public List<XsiExhibitionFurnitureItems> Select(XsiExhibitionFurnitureItems entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionFurnitureItems>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail.Equals(entity.Thumbnail));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.DescriptionAr.IsDefault())
                predicate = predicate.And(p => p.DescriptionAr.Contains(entity.DescriptionAr));

            if (!entity.ThumbnailAr.IsDefault())
                predicate = predicate.And(p => p.ThumbnailAr.Equals(entity.ThumbnailAr));

            if (!entity.Price.IsDefault())
                predicate = predicate.And(p => p.Price == entity.Price);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionFurnitureItems.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionFurnitureItems Add(XsiExhibitionFurnitureItems entity)
        {
            XsiExhibitionFurnitureItems ExhibitionFurnitureItem = new XsiExhibitionFurnitureItems();

            if (!entity.Title.IsDefault())
                ExhibitionFurnitureItem.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                ExhibitionFurnitureItem.Description = entity.Description.Trim();

            if (!entity.Thumbnail.IsDefault())
                ExhibitionFurnitureItem.Thumbnail = entity.Thumbnail.Trim();

            if (!entity.TitleAr.IsDefault())
                ExhibitionFurnitureItem.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                ExhibitionFurnitureItem.DescriptionAr = entity.DescriptionAr.Trim();

            if (!entity.ThumbnailAr.IsDefault())
                ExhibitionFurnitureItem.ThumbnailAr = entity.ThumbnailAr.Trim();

            if (!entity.Price.IsDefault())
                ExhibitionFurnitureItem.Price = entity.Price;

            if (!entity.IsActive.IsDefault())
                ExhibitionFurnitureItem.IsActive = entity.IsActive;

            if (!entity.IsActiveAr.IsDefault())
                ExhibitionFurnitureItem.IsActiveAr = entity.IsActiveAr;

            ExhibitionFurnitureItem.CreatedOn = entity.CreatedOn;
            ExhibitionFurnitureItem.CreatedBy = entity.CreatedBy;
            ExhibitionFurnitureItem.ModifiedOn = entity.ModifiedOn;
            ExhibitionFurnitureItem.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiExhibitionFurnitureItems.Add(ExhibitionFurnitureItem);
            SubmitChanges();
            return ExhibitionFurnitureItem;

        }
        public void Update(XsiExhibitionFurnitureItems entity)
        {
            XsiExhibitionFurnitureItems ExhibitionFurnitureItem = XsiContext.Context.XsiExhibitionFurnitureItems.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionFurnitureItem).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionFurnitureItem.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                ExhibitionFurnitureItem.Description = entity.Description.Trim();

            if (!entity.Thumbnail.IsDefault())
                ExhibitionFurnitureItem.Thumbnail = entity.Thumbnail.Trim();

            if (!entity.TitleAr.IsDefault())
                ExhibitionFurnitureItem.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                ExhibitionFurnitureItem.DescriptionAr = entity.DescriptionAr.Trim();

            if (!entity.ThumbnailAr.IsDefault())
                ExhibitionFurnitureItem.ThumbnailAr = entity.ThumbnailAr.Trim();

            if (!entity.Price.IsDefault())
                ExhibitionFurnitureItem.Price = entity.Price;

            if (!entity.IsActive.IsDefault())
                ExhibitionFurnitureItem.IsActive = entity.IsActive;

            if (!entity.IsActiveAr.IsDefault())
                ExhibitionFurnitureItem.IsActiveAr = entity.IsActiveAr;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionFurnitureItem.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                ExhibitionFurnitureItem.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionFurnitureItems entity)
        {
            foreach (XsiExhibitionFurnitureItems ExhibitionFurnitureItem in Select(entity))
            {
                XsiExhibitionFurnitureItems aExhibitionFurnitureItem = XsiContext.Context.XsiExhibitionFurnitureItems.Find(ExhibitionFurnitureItem.ItemId);
                XsiContext.Context.XsiExhibitionFurnitureItems.Remove(aExhibitionFurnitureItem);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionFurnitureItems aExhibitionFurnitureItem = XsiContext.Context.XsiExhibitionFurnitureItems.Find(itemId);
            XsiContext.Context.XsiExhibitionFurnitureItems.Remove(aExhibitionFurnitureItem);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
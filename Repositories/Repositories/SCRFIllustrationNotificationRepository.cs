﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFIllustrationNotificationStatusRepository : ISCRFIllustrationNotificationStatusRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFIllustrationNotificationStatusRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFIllustrationNotificationStatusRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFIllustrationNotificationStatusRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfillustrationNotificationStatus GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfillustrationNotificationStatus.Find(itemId);
        }
        public List<XsiScrfillustrationNotificationStatus> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiScrfillustrationNotificationStatus> Select()
        {
            return XsiContext.Context.XsiScrfillustrationNotificationStatus.ToList();
        }
        public List<XsiScrfillustrationNotificationStatus> Select(XsiScrfillustrationNotificationStatus entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfillustrationNotificationStatus>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IllustrationNotificationId.IsDefault())
                predicate = predicate.And(p => p.IllustrationNotificationId == entity.IllustrationNotificationId);

            if (!entity.IllustratorId.IsDefault())
                predicate = predicate.And(p => p.IllustratorId == entity.IllustratorId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));


            return XsiContext.Context.XsiScrfillustrationNotificationStatus.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfillustrationNotificationStatus Add(XsiScrfillustrationNotificationStatus entity)
        {
            XsiScrfillustrationNotificationStatus SCRFIllustrationNotificationStatus = new XsiScrfillustrationNotificationStatus();
            SCRFIllustrationNotificationStatus.IllustrationNotificationId = entity.IllustrationNotificationId;
            SCRFIllustrationNotificationStatus.IllustratorId = entity.IllustratorId;

            XsiContext.Context.XsiScrfillustrationNotificationStatus.Add(SCRFIllustrationNotificationStatus);
            SubmitChanges();
            return SCRFIllustrationNotificationStatus;

        }
        public void Update(XsiScrfillustrationNotificationStatus entity)
        {
            XsiScrfillustrationNotificationStatus SCRFIllustrationNotificationStatus = XsiContext.Context.XsiScrfillustrationNotificationStatus.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFIllustrationNotificationStatus).State = EntityState.Modified;

            if (!entity.IllustrationNotificationId.IsDefault())
                SCRFIllustrationNotificationStatus.IllustrationNotificationId = entity.IllustrationNotificationId;

            if (!entity.IllustratorId.IsDefault())
                SCRFIllustrationNotificationStatus.IllustratorId = entity.IllustratorId;

            if (!entity.Status.IsDefault())
                SCRFIllustrationNotificationStatus.Status = entity.Status;
        }
        public void Delete(XsiScrfillustrationNotificationStatus entity)
        {
            foreach (XsiScrfillustrationNotificationStatus SCRFIllustrationNotificationStatus in Select(entity))
            {
                XsiScrfillustrationNotificationStatus aSCRFIllustrationNotificationStatus = XsiContext.Context.XsiScrfillustrationNotificationStatus.Find(SCRFIllustrationNotificationStatus.ItemId);
                XsiContext.Context.XsiScrfillustrationNotificationStatus.Remove(aSCRFIllustrationNotificationStatus);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfillustrationNotificationStatus aSCRFIllustrationNotificationStatus = XsiContext.Context.XsiScrfillustrationNotificationStatus.Find(itemId);
            XsiContext.Context.XsiScrfillustrationNotificationStatus.Remove(aSCRFIllustrationNotificationStatus);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
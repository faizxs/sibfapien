﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFEmailContentRepository : ISCRFEmailContentRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFEmailContentRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFEmailContentRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFEmailContentRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfemailContent GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfemailContent.Find(itemId);
        }
        
        public List<XsiScrfemailContent> Select()
        {
            return XsiContext.Context.XsiScrfemailContent.ToList();
        }
        public List<XsiScrfemailContent> Select(XsiScrfemailContent entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfemailContent>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Equals(entity.Title));

            if (!entity.Subject.IsDefault())
                predicate = predicate.And(p => p.Subject.Equals(entity.Subject));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Equals(entity.Email));

            if (!entity.Body.IsDefault())
                predicate = predicate.And(p => p.Body.Equals(entity.Body));

            if (!entity.RollbackXml.IsDefault())
                predicate = predicate.And(p => p.RollbackXml.Equals(entity.RollbackXml));


            return XsiContext.Context.XsiScrfemailContent.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiScrfemailContent> SelectOr(XsiScrfemailContent entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfemailContent>();
            var Outer = PredicateBuilder.True<XsiScrfemailContent>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                     || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                     || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                     || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                     );
                Inner = Inner.Or(p => p.Subject.Contains(searchKeys.str1) || p.Subject.Contains(searchKeys.str2) || p.Subject.Contains(searchKeys.str3) || p.Subject.Contains(searchKeys.str4) || p.Subject.Contains(searchKeys.str5)
                    || p.Subject.Contains(searchKeys.str6) || p.Subject.Contains(searchKeys.str7) || p.Subject.Contains(searchKeys.str8) || p.Subject.Contains(searchKeys.str9) || p.Subject.Contains(searchKeys.str10)
                    || p.Subject.Contains(searchKeys.str11) || p.Subject.Contains(searchKeys.str12) || p.Subject.Contains(searchKeys.str13)
                    || p.Subject.Contains(searchKeys.str14) || p.Subject.Contains(searchKeys.str15) || p.Subject.Contains(searchKeys.str16) || p.Subject.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();

                    if (!entity.Subject.IsDefault())
                        Inner = Inner.Or(p => p.Subject.ToLower().Contains(strSearchText));

                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));

                    isInner = true;
                }
            }
             
            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiScrfemailContent.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public XsiScrfemailContent Add(XsiScrfemailContent entity)
        {
            XsiScrfemailContent SCRFEmailContent = new XsiScrfemailContent();
            
            if (!entity.Title.IsDefault())
                SCRFEmailContent.Title = entity.Title.Trim();
            if (!entity.Email.IsDefault())
                SCRFEmailContent.Email = entity.Email.Trim();
            if (!entity.Subject.IsDefault())
                SCRFEmailContent.Subject = entity.Subject.Trim();
            if (!entity.Body.IsDefault())
                SCRFEmailContent.Body = entity.Body.Trim();
            SCRFEmailContent.RollbackXml = entity.RollbackXml;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                SCRFEmailContent.TitleAr = entity.TitleAr.Trim();
            if (!entity.EmailAr.IsDefault())
                SCRFEmailContent.EmailAr = entity.EmailAr.Trim();
            if (!entity.SubjectAr.IsDefault())
                SCRFEmailContent.SubjectAr = entity.SubjectAr.Trim();
            if (!entity.BodyAr.IsDefault())
                SCRFEmailContent.BodyAr = entity.BodyAr.Trim();
            if (!entity.RollbackXmlAr.IsDefault())
                SCRFEmailContent.RollbackXmlAr = entity.RollbackXmlAr;
            #endregion Ar

            XsiContext.Context.XsiScrfemailContent.Add(SCRFEmailContent);
            SubmitChanges();
            return SCRFEmailContent;

        }
        public void Update(XsiScrfemailContent entity)
        {
            XsiScrfemailContent SCRFEmailContent = XsiContext.Context.XsiScrfemailContent.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFEmailContent).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                SCRFEmailContent.Title = entity.Title.Trim();

            if (!entity.Subject.IsDefault())
                SCRFEmailContent.Subject = entity.Subject.Trim();

            if (!entity.Email.IsDefault())
                SCRFEmailContent.Email = entity.Email.Trim();

            if (!entity.Body.IsDefault())
                SCRFEmailContent.Body = entity.Body.Trim();

            if (!entity.RollbackXml.IsDefault())
                SCRFEmailContent.RollbackXml = entity.RollbackXml;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                SCRFEmailContent.TitleAr = entity.TitleAr.Trim();
            if (!entity.EmailAr.IsDefault())
                SCRFEmailContent.EmailAr = entity.EmailAr.Trim();
            if (!entity.SubjectAr.IsDefault())
                SCRFEmailContent.SubjectAr = entity.SubjectAr.Trim();
            if (!entity.BodyAr.IsDefault())
                SCRFEmailContent.BodyAr = entity.BodyAr.Trim();
            if (!entity.RollbackXmlAr.IsDefault())
                SCRFEmailContent.RollbackXmlAr = entity.RollbackXmlAr;
            #endregion Ar
        }
        public void Delete(XsiScrfemailContent entity)
        {
            foreach (XsiScrfemailContent SCRFEmailContent in Select(entity))
            {
                XsiScrfemailContent aSCRFEmailContent = XsiContext.Context.XsiScrfemailContent.Find(SCRFEmailContent.ItemId);
                XsiContext.Context.XsiScrfemailContent.Remove(aSCRFEmailContent);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfemailContent aSCRFEmailContent = XsiContext.Context.XsiScrfemailContent.Find(itemId);
            XsiContext.Context.XsiScrfemailContent.Remove(aSCRFEmailContent);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
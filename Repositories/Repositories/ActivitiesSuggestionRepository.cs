﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ActivitiesSuggestionRepository : IActivitiesSuggestionRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ActivitiesSuggestionRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ActivitiesSuggestionRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ActivitiesSuggestionRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiActivitiesSuggestion GetById(long itemId)
        {
            return XsiContext.Context.XsiActivitiesSuggestion.Find(itemId);
        }
        public List<XsiActivitiesSuggestion> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiActivitiesSuggestion> Select()
        {
            return XsiContext.Context.XsiActivitiesSuggestion.ToList();
        }
        public List<XsiActivitiesSuggestion> Select(XsiActivitiesSuggestion entity)
        {
            var predicate = PredicateBuilder.True<XsiActivitiesSuggestion>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            if (!entity.FirstName.IsDefault())
                predicate = predicate.And(p => p.FirstName.Contains(entity.FirstName));

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));

            if (!entity.Phone.IsDefault())
                predicate = predicate.And(p => p.Phone.Equals(entity.Phone));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Equals(entity.Email));

            if (!entity.Detail.IsDefault())
                predicate = predicate.And(p => p.Detail.Contains(entity.Detail));

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.ActivityCategory.IsDefault())
                predicate = predicate.And(p => p.ActivityCategory == entity.ActivityCategory);

            if (!entity.ActivityTitle.IsDefault())
                predicate = predicate.And(p => p.ActivityTitle.Contains(entity.ActivityTitle));

            if (!entity.ActivityDescription.IsDefault())
                predicate = predicate.And(p => p.ActivityDescription.Equals(entity.ActivityDescription));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiActivitiesSuggestion.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiActivitiesSuggestion> SelectOr(XsiActivitiesSuggestion entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiActivitiesSuggestion>();
            var Outer = PredicateBuilder.True<XsiActivitiesSuggestion>();

            if (!entity.LanguageId.IsDefault() && entity.LanguageId == 2 && !entity.FirstName.IsDefault())
            {
                string strSearchText = entity.FirstName;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.FirstName.Contains(searchKeys.str1) || p.FirstName.Contains(searchKeys.str2) || p.FirstName.Contains(searchKeys.str3) || p.FirstName.Contains(searchKeys.str4) || p.FirstName.Contains(searchKeys.str5)
                    || p.FirstName.Contains(searchKeys.str6) || p.FirstName.Contains(searchKeys.str7) || p.FirstName.Contains(searchKeys.str8) || p.FirstName.Contains(searchKeys.str9) || p.FirstName.Contains(searchKeys.str10)
                    || p.FirstName.Contains(searchKeys.str11) || p.FirstName.Contains(searchKeys.str12) || p.FirstName.Contains(searchKeys.str13)
                    || p.FirstName.Contains(searchKeys.str14) || p.FirstName.Contains(searchKeys.str15) || p.FirstName.Contains(searchKeys.str16) || p.FirstName.Contains(searchKeys.str17)
                    );
                Inner = Inner.Or(p => p.LastName.Contains(searchKeys.str1) || p.LastName.Contains(searchKeys.str2) || p.LastName.Contains(searchKeys.str3) || p.LastName.Contains(searchKeys.str4) || p.LastName.Contains(searchKeys.str5)
                    || p.LastName.Contains(searchKeys.str6) || p.LastName.Contains(searchKeys.str7) || p.LastName.Contains(searchKeys.str8) || p.LastName.Contains(searchKeys.str9) || p.LastName.Contains(searchKeys.str10)
                    || p.LastName.Contains(searchKeys.str11) || p.LastName.Contains(searchKeys.str12) || p.LastName.Contains(searchKeys.str13)
                    || p.LastName.Contains(searchKeys.str14) || p.LastName.Contains(searchKeys.str15) || p.LastName.Contains(searchKeys.str16) || p.LastName.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.FirstName.IsDefault())
                {
                    string strSearchText = entity.FirstName.ToLower();
                    Inner = Inner.Or(p => p.FirstName.ToLower().Contains(strSearchText));
                    Inner = Inner.Or(p => p.LastName.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.LanguageId.IsDefault())
                Outer = Outer.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.WebsiteId.IsDefault())
                Outer = Outer.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiActivitiesSuggestion.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public XsiActivitiesSuggestion Add(XsiActivitiesSuggestion entity)
        {
            XsiActivitiesSuggestion ActivitiesSuggestion = new XsiActivitiesSuggestion();
            ActivitiesSuggestion.LanguageId = entity.LanguageId;
            ActivitiesSuggestion.WebsiteId = entity.WebsiteId;
            ActivitiesSuggestion.ExhibitionId = entity.ExhibitionId;
            if (!entity.FirstName.IsDefault())
                ActivitiesSuggestion.FirstName = entity.FirstName.Trim();
            if (!entity.LastName.IsDefault())
                ActivitiesSuggestion.LastName = entity.LastName.Trim();
            if (!entity.Phone.IsDefault())
                ActivitiesSuggestion.Phone = entity.Phone.Trim();
            if (!entity.Email.IsDefault())
                ActivitiesSuggestion.Email = entity.Email.Trim();
            if (!entity.Detail.IsDefault())
                ActivitiesSuggestion.Detail = entity.Detail.Trim();
            ActivitiesSuggestion.IsActive = entity.IsActive;
            ActivitiesSuggestion.CountryId = entity.CountryId;
            ActivitiesSuggestion.ActivityCategory = entity.ActivityCategory;
            if (!entity.ActivityTitle.IsDefault())
                ActivitiesSuggestion.ActivityTitle = entity.ActivityTitle.Trim();
            if (!entity.ActivityDescription.IsDefault())
                ActivitiesSuggestion.ActivityDescription = entity.ActivityDescription.Trim();
            ActivitiesSuggestion.CreatedOn = entity.CreatedOn;
            ActivitiesSuggestion.ModifiedOn = entity.ModifiedOn;
            ActivitiesSuggestion.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiActivitiesSuggestion.Add(ActivitiesSuggestion);
            SubmitChanges();
            return ActivitiesSuggestion;

        }
        public void Update(XsiActivitiesSuggestion entity)
        {
            XsiActivitiesSuggestion ActivitiesSuggestion = XsiContext.Context.XsiActivitiesSuggestion.Find(entity.ItemId);
            XsiContext.Context.Entry(ActivitiesSuggestion).State = EntityState.Modified;

            if (!entity.LanguageId.IsDefault())
                ActivitiesSuggestion.LanguageId = entity.LanguageId;

            if (!entity.WebsiteId.IsDefault())
                ActivitiesSuggestion.WebsiteId = entity.WebsiteId;

            if (!entity.ExhibitionId.IsDefault())
                ActivitiesSuggestion.ExhibitionId = entity.ExhibitionId;

            if (!entity.FirstName.IsDefault())
                ActivitiesSuggestion.FirstName = entity.FirstName.Trim();

            if (!entity.LastName.IsDefault())
                ActivitiesSuggestion.LastName = entity.LastName.Trim();

            if (!entity.Phone.IsDefault())
                ActivitiesSuggestion.Phone = entity.Phone.Trim();

            if (!entity.Email.IsDefault())
                ActivitiesSuggestion.Email = entity.Email.Trim();

            if (!entity.Detail.IsDefault())
                ActivitiesSuggestion.Detail = entity.Detail.Trim();

            if (!entity.IsActive.IsDefault())
                ActivitiesSuggestion.IsActive = entity.IsActive;

            if (!entity.CountryId.IsDefault())
                ActivitiesSuggestion.CountryId = entity.CountryId;

            if (!entity.ActivityCategory.IsDefault())
                ActivitiesSuggestion.ActivityCategory = entity.ActivityCategory;

            if (!entity.ActivityTitle.IsDefault())
                ActivitiesSuggestion.ActivityTitle = entity.ActivityTitle.Trim();

            if (!entity.ActivityDescription.IsDefault())
                ActivitiesSuggestion.ActivityDescription = entity.ActivityDescription.Trim();

            if (!entity.CreatedOn.IsDefault())
                ActivitiesSuggestion.CreatedOn = entity.CreatedOn;

            if (!entity.ModifiedOn.IsDefault())
                ActivitiesSuggestion.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ActivitiesSuggestion.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiActivitiesSuggestion entity)
        {
            foreach (XsiActivitiesSuggestion ActivitiesSuggestion in Select(entity))
            {
                XsiActivitiesSuggestion aActivitiesSuggestion = XsiContext.Context.XsiActivitiesSuggestion.Find(ActivitiesSuggestion.ItemId);
                XsiContext.Context.XsiActivitiesSuggestion.Remove(aActivitiesSuggestion);
            }
        }
        public void Delete(long itemId)
        {
            XsiActivitiesSuggestion aActivitiesSuggestion = XsiContext.Context.XsiActivitiesSuggestion.Find(itemId);
            XsiContext.Context.XsiActivitiesSuggestion.Remove(aActivitiesSuggestion);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
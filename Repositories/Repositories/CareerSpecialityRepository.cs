﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class CareerSpecialityRepository : ICareerSpecialityRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public CareerSpecialityRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public CareerSpecialityRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public CareerSpecialityRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiCareerSpeciality GetById(long itemId)
        {
            return XsiContext.Context.XsiCareerSpeciality.Find(itemId);
        }

        public List<XsiCareerSpeciality> Select()
        {
            return XsiContext.Context.XsiCareerSpeciality.ToList();
        }
        public List<XsiCareerSpeciality> Select(XsiCareerSpeciality entity)
        {
            var predicate = PredicateBuilder.True<XsiCareerSpeciality>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiCareerSpeciality.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiCareerSpeciality> SelectOr(XsiCareerSpeciality entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiCareerSpeciality>();
            var Outer = PredicateBuilder.True<XsiCareerSpeciality>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiCareerSpeciality.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiCareerSpeciality Add(XsiCareerSpeciality entity)
        {
            XsiCareerSpeciality CareerSpeciality = new XsiCareerSpeciality();

            if (!entity.Title.IsDefault())
                CareerSpeciality.Title = entity.Title.Trim();
            CareerSpeciality.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerSpeciality.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerSpeciality.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            CareerSpeciality.CreatedOn = entity.CreatedOn;
            CareerSpeciality.CreatedBy = entity.CreatedBy;
            CareerSpeciality.ModifiedOn = entity.ModifiedOn;
            CareerSpeciality.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiCareerSpeciality.Add(CareerSpeciality);
            SubmitChanges();
            return CareerSpeciality;

        }
        public void Update(XsiCareerSpeciality entity)
        {
            XsiCareerSpeciality CareerSpeciality = XsiContext.Context.XsiCareerSpeciality.Find(entity.ItemId);
            XsiContext.Context.Entry(CareerSpeciality).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                CareerSpeciality.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                CareerSpeciality.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerSpeciality.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerSpeciality.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                CareerSpeciality.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                CareerSpeciality.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiCareerSpeciality entity)
        {
            foreach (XsiCareerSpeciality CareerSpeciality in Select(entity))
            {
                XsiCareerSpeciality aCareerSpeciality = XsiContext.Context.XsiCareerSpeciality.Find(CareerSpeciality.ItemId);
                XsiContext.Context.XsiCareerSpeciality.Remove(aCareerSpeciality);
            }
        }
        public void Delete(long itemId)
        {
            XsiCareerSpeciality aCareerSpeciality = XsiContext.Context.XsiCareerSpeciality.Find(itemId);
            XsiContext.Context.XsiCareerSpeciality.Remove(aCareerSpeciality);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionMemberStatusLogRepository : IExhibitionMemberStatusLogRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionMemberStatusLogRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionMemberStatusLogRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionMemberStatusLogRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionMemberStatusLog GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionMemberStatusLog.Find(itemId);
        }
        public List<XsiExhibitionMemberStatusLog> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionMemberStatusLog> Select()
        {
            return XsiContext.Context.XsiExhibitionMemberStatusLog.ToList();
        }
        public List<XsiExhibitionMemberStatusLog> Select(XsiExhibitionMemberStatusLog entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMemberStatusLog>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ExhibitionMemberId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionMemberId == entity.ExhibitionMemberId);

            if (!entity.AdminId.IsDefault())
                predicate = predicate.And(p => p.AdminId == entity.AdminId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);


            return XsiContext.Context.XsiExhibitionMemberStatusLog.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionMemberStatusLog Add(XsiExhibitionMemberStatusLog entity)
        {
            XsiExhibitionMemberStatusLog ExhibitionMemberStatusLog = new XsiExhibitionMemberStatusLog();
            ExhibitionMemberStatusLog.ExhibitionMemberId = entity.ExhibitionMemberId;
            ExhibitionMemberStatusLog.AdminId = entity.AdminId;
            ExhibitionMemberStatusLog.Status = entity.Status;
            ExhibitionMemberStatusLog.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiExhibitionMemberStatusLog.Add(ExhibitionMemberStatusLog);
            SubmitChanges();
            return ExhibitionMemberStatusLog;

        }
        public void Update(XsiExhibitionMemberStatusLog entity)
        {
            XsiExhibitionMemberStatusLog ExhibitionMemberStatusLog = XsiContext.Context.XsiExhibitionMemberStatusLog.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionMemberStatusLog).State = EntityState.Modified;

            if (!entity.Status.IsDefault())
                ExhibitionMemberStatusLog.Status = entity.Status;
        }
        public void Delete(XsiExhibitionMemberStatusLog entity)
        {
            foreach (XsiExhibitionMemberStatusLog ExhibitionMemberStatusLog in Select(entity))
            {
                XsiExhibitionMemberStatusLog aExhibitionMemberStatusLog = XsiContext.Context.XsiExhibitionMemberStatusLog.Find(ExhibitionMemberStatusLog.ItemId);
                XsiContext.Context.XsiExhibitionMemberStatusLog.Remove(aExhibitionMemberStatusLog);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionMemberStatusLog aExhibitionMemberStatusLog = XsiContext.Context.XsiExhibitionMemberStatusLog.Find(itemId);
            XsiContext.Context.XsiExhibitionMemberStatusLog.Remove(aExhibitionMemberStatusLog);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
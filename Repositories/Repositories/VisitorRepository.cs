﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class VisitorRepository : IVisitorRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public VisitorRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public VisitorRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public VisitorRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiVisitor GetById(long itemId)
        {
            return XsiContext.Context.XsiVisitor.Find(itemId);
        }
        

        public List<XsiVisitor> Select()
        {
            return XsiContext.Context.XsiVisitor.ToList();
        }
        public List<XsiVisitor> SelectOr(XsiVisitor entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiVisitor>();
            var Outer = PredicateBuilder.True<XsiVisitor>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

             
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiVisitor.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiVisitor> Select(XsiVisitor entity)
        {
            var predicate = PredicateBuilder.True<XsiVisitor>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Contains(entity.ImageName));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiVisitor.AsExpandable().Where(predicate).ToList();
        }

        public XsiVisitor Add(XsiVisitor entity)
        {
            XsiVisitor Visitor = new XsiVisitor();
           
            if (!entity.Title.IsDefault())
                Visitor.Title = entity.Title.Trim();
            if (!entity.Overview.IsDefault())
                Visitor.Overview = entity.Overview.Trim();
            if (!entity.Url.IsDefault())
                Visitor.Url = entity.Url;
            if (!entity.ImageName.IsDefault())
                Visitor.ImageName = entity.ImageName;
            Visitor.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                Visitor.TitleAr = entity.TitleAr.Trim();
            if (!entity.OverviewAr.IsDefault())
                Visitor.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.Urlar.IsDefault())
                Visitor.Urlar = entity.Urlar;
            if (!entity.ImageNameAr.IsDefault())
                Visitor.ImageNameAr = entity.ImageNameAr;
            Visitor.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            Visitor.CreatedOn = entity.CreatedOn;
            Visitor.CreatedBy = entity.CreatedBy;
            Visitor.ModifiedOn = entity.ModifiedOn;
            Visitor.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiVisitor.Add(Visitor);
            SubmitChanges();
            return Visitor;
        }
        public void Update(XsiVisitor entity)
        {
            XsiVisitor Visitor = XsiContext.Context.XsiVisitor.Find(entity.ItemId);
            XsiContext.Context.Entry(Visitor).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                Visitor.Title = entity.Title.Trim();
            if (!entity.Overview.IsDefault())
                Visitor.Overview = entity.Overview.Trim();
            if (!entity.Url.IsDefault())
                Visitor.Url = entity.Url;
            if (!entity.ImageName.IsDefault())
                Visitor.ImageName = entity.ImageName;
            if (!entity.IsActive.IsDefault())
                Visitor.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                Visitor.TitleAr = entity.TitleAr.Trim();
            if (!entity.OverviewAr.IsDefault())
                Visitor.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.Urlar.IsDefault())
                Visitor.Urlar = entity.Urlar;
            if (!entity.ImageNameAr.IsDefault())
                Visitor.ImageNameAr = entity.ImageNameAr;
            if (!entity.IsActiveAr.IsDefault())
                Visitor.IsActiveAr = entity.IsActive;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                Visitor.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                Visitor.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiVisitor entity)
        {
            foreach (XsiVisitor Visitor in Select(entity))
            {
                XsiVisitor aVisitor = XsiContext.Context.XsiVisitor.Find(Visitor.ItemId);
                XsiContext.Context.XsiVisitor.Remove(aVisitor);
            }
        }
        public void Delete(long itemId)
        {
            XsiVisitor aVisitor = XsiContext.Context.XsiVisitor.Find(itemId);
            XsiContext.Context.XsiVisitor.Remove(aVisitor);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
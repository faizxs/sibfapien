﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFTechniqueMaterialRepository : ISCRFTechniqueMaterialRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFTechniqueMaterialRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFTechniqueMaterialRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFTechniqueMaterialRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrftechniqueMaterial GetById(long itemId)
        {
            return XsiContext.Context.XsiScrftechniqueMaterial.Find(itemId);
        }
        public List<XsiScrftechniqueMaterial> Select()
        {
            return XsiContext.Context.XsiScrftechniqueMaterial.ToList();
        }
        public List<XsiScrftechniqueMaterial> Select(XsiScrftechniqueMaterial entity)
        {
            var predicate = PredicateBuilder.True<XsiScrftechniqueMaterial>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiScrftechniqueMaterial.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiScrftechniqueMaterial> SelectOtherLanguageCategory(XsiScrftechniqueMaterial entity)
        {
            var predicate = PredicateBuilder.True<XsiScrftechniqueMaterial>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

           // var GroupId = XsiContext.Context.XsiScrftechniqueMaterial.AsExpandable().Where(predicate).Single().GroupId.Value;
            predicate = PredicateBuilder.True<XsiScrftechniqueMaterial>();
            return XsiContext.Context.XsiScrftechniqueMaterial.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiScrftechniqueMaterial> SelectCurrentLanguageCategory(XsiScrftechniqueMaterial entity)
        {
            var predicate = PredicateBuilder.True<XsiScrftechniqueMaterial>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

           // var GroupId = XsiContext.Context.XsiScrftechniqueMaterial.AsExpandable().Where(predicate).Single().GroupId.Value;
            predicate = PredicateBuilder.True<XsiScrftechniqueMaterial>();
            return XsiContext.Context.XsiScrftechniqueMaterial.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrftechniqueMaterial Add(XsiScrftechniqueMaterial entity)
        {
            XsiScrftechniqueMaterial SCRFTechniqueMaterial = new XsiScrftechniqueMaterial();
            SCRFTechniqueMaterial.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
            SCRFTechniqueMaterial.Title = entity.Title.Trim();
            SCRFTechniqueMaterial.CreatedOn = entity.CreatedOn;
            SCRFTechniqueMaterial.CreatedBy = entity.CreatedBy;
            SCRFTechniqueMaterial.ModifiedOn = entity.ModifiedOn;
            SCRFTechniqueMaterial.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrftechniqueMaterial.Add(SCRFTechniqueMaterial);
            SubmitChanges();
            return SCRFTechniqueMaterial;

        }
        public void Update(XsiScrftechniqueMaterial entity)
        {
            XsiScrftechniqueMaterial SCRFTechniqueMaterial = XsiContext.Context.XsiScrftechniqueMaterial.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFTechniqueMaterial).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                SCRFTechniqueMaterial.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                SCRFTechniqueMaterial.Title = entity.Title.Trim();

            if (!entity.ModifiedOn.IsDefault())
                SCRFTechniqueMaterial.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                SCRFTechniqueMaterial.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiScrftechniqueMaterial entity)
        {
            foreach (XsiScrftechniqueMaterial SCRFTechniqueMaterial in Select(entity))
            {
                XsiScrftechniqueMaterial aSCRFTechniqueMaterial = XsiContext.Context.XsiScrftechniqueMaterial.Find(SCRFTechniqueMaterial.ItemId);
                XsiContext.Context.XsiScrftechniqueMaterial.Remove(aSCRFTechniqueMaterial);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrftechniqueMaterial aSCRFTechniqueMaterial = XsiContext.Context.XsiScrftechniqueMaterial.Find(itemId);
            XsiContext.Context.XsiScrftechniqueMaterial.Remove(aSCRFTechniqueMaterial);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
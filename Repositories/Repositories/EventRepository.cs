﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class EventRepository : IEventRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public EventRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public EventRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public EventRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiEvent GetById(long itemId)
        {
            return XsiContext.Context.XsiEvent.Find(itemId);
        }
        
        public List<XsiEvent> Select()
        {
            return XsiContext.Context.XsiEvent.ToList();
        }
        public List<XsiEvent> Select(XsiEvent entity)
        {
            //var temp = XsiContext.Context.XsiEvent.Select(p => new
            //{
            //    SubCategoryTitle = _XsiEntitiesContext.XsiEventSubCategory.Where(q => q.GroupId == p.CategoryId & q.LanguageId == 1).Select(q => q.Title).FirstOrDefault(),
            //    CategoryTitle = _XsiEntitiesContext.XsiEventSubCategory.Where(q => q.GroupId == p.CategoryId & q.LanguageId == 1).Select(q => q.XsiEventCategory.Title).FirstOrDefault(),
            //    p.Details
            //}).ToList();

            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiEvent>();

            var predicate = PredicateBuilder.True<XsiEvent>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
 
            if (!entity.EventSubCategoryId.IsDefault())
                predicate = predicate.And(p => p.EventSubCategoryId == entity.EventSubCategoryId);

            if (!entity.PhotoAlbumId.IsDefault())
                predicate = predicate.And(p => p.PhotoAlbumId == entity.PhotoAlbumId);

            if (!entity.VideoAlbumId.IsDefault())
                predicate = predicate.And(p => p.VideoAlbumId == entity.VideoAlbumId);

            if (!entity.ScrfphotoAlbumId.IsDefault())
                predicate = predicate.And(p => p.ScrfphotoAlbumId == entity.ScrfphotoAlbumId);

            if (!entity.ScrfvideoAlbumId.IsDefault())
                predicate = predicate.And(p => p.ScrfphotoAlbumId == entity.ScrfphotoAlbumId);

            if (!entity.Type.IsDefault())
                predicate = predicate.And(p => p.Type.Equals(entity.Type));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsFeatured.IsDefault())
                predicate = predicate.And(p => p.IsFeatured.Equals(entity.IsFeatured));

            //if (entity.LanguageId == 2)
            //{
            //    if (!entity.Title.IsDefault())
            //    {
            //        string strSearchText = entity.Title;
            //        var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

            //        Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
            //            || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
            //            || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
            //            || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
            //            );

            //        isInner = true;
            //    }
            //}
            //else
            //{
            //    if (!entity.Title.IsDefault())
            //        predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title.ToLower()));
            //}
            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title.ToLower()));
            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Contains(entity.FileName));

            if (!entity.EventThumbnailNew.IsDefault())
                predicate = predicate.And(p => p.EventThumbnailNew.Contains(entity.EventThumbnailNew));

            if (!entity.InnerTopImageOne.IsDefault())
                predicate = predicate.And(p => p.InnerTopImageOne.Contains(entity.InnerTopImageOne));

            if (!entity.InnerTopImageTwo.IsDefault())
                predicate = predicate.And(p => p.InnerTopImageTwo.Contains(entity.InnerTopImageTwo));

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.Details.IsDefault())
                predicate = predicate.And(p => p.Details.Contains(entity.Details));

            if (!entity.Guest.IsDefault())
                predicate = predicate.And(p => p.Guest.Contains(entity.Guest));

            if (!entity.StartDate.IsDefault())
                predicate = predicate.And(p => p.StartDate == entity.StartDate);

            if (!entity.EndDate.IsDefault())
                predicate = predicate.And(p => p.EndDate == entity.EndDate);

            if (!entity.Location.IsDefault())
                predicate = predicate.And(p => p.Location.Contains(entity.Location));

            if (!entity.LocationAr.IsDefault())
                predicate = predicate.And(p => p.LocationAr.Contains(entity.LocationAr));

            if (!entity.BookingLink.IsDefault())
                predicate = predicate.And(p => p.BookingLink.Contains(entity.BookingLink));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiEvent.AsExpandable().Where(predicate).ToList();
        }
        /*public List<GetEvents_Result> SelectEventsResult(long dayVal, long monthVal, long yearVal, string groupidsList, long languageId)
        {
            return XsiContext.Context.GetEvents(dayVal, monthVal, yearVal, groupidsList, languageId).ToList();
        }*/
        public XsiEvent Add(XsiEvent entity)
        {
            XsiEvent Event = new XsiEvent(); 
            Event.EventSubCategoryId = entity.EventSubCategoryId;
            Event.PhotoAlbumId = entity.PhotoAlbumId;
            Event.VideoAlbumId = entity.VideoAlbumId;
            Event.ScrfphotoAlbumId = entity.ScrfphotoAlbumId;
            Event.ScrfvideoAlbumId = entity.ScrfvideoAlbumId;
            Event.Type = entity.Type;
            Event.IsActive = entity.IsActive; 
            Event.IsFeatured = entity.IsFeatured;
            if (!entity.Title.IsDefault())
                Event.Title = entity.Title.Trim();
            Event.FileName = entity.FileName;
            Event.EventThumbnailNew = entity.EventThumbnailNew;
            Event.InnerTopImageOne = entity.InnerTopImageOne;
            Event.InnerTopImageTwo = entity.InnerTopImageTwo;
            if (!entity.Overview.IsDefault())
                Event.Overview = entity.Overview.Trim();
            if (!entity.Details.IsDefault())
                Event.Details = entity.Details.Trim();
            if (!entity.Guest.IsDefault())
                Event.Guest = entity.Guest.Trim();
            Event.StartDate = entity.StartDate;
            Event.EndDate = entity.EndDate;
            if (!entity.Location.IsDefault())
                Event.Location = entity.Location.Trim();
            #region Ar
            if (!entity.TitleAr.IsDefault())
                Event.TitleAr = entity.TitleAr.Trim();
            Event.FileNameAr = entity.FileNameAr;
            Event.EventThumbnailNewAr = entity.EventThumbnailNewAr;
            Event.InnerTopImageOneAr = entity.InnerTopImageOneAr;
            Event.InnerTopImageTwoAr = entity.InnerTopImageTwoAr;
            if (!entity.OverviewAr.IsDefault())
                Event.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.DetailsAr.IsDefault())
                Event.DetailsAr = entity.DetailsAr.Trim();
            if (!entity.GuestAr.IsDefault())
                Event.GuestAr = entity.GuestAr.Trim();
            if (!entity.LocationAr.IsDefault())
                Event.LocationAr = entity.LocationAr.Trim();
            #endregion Ar

            if (!entity.BookingLink.IsDefault())
                Event.BookingLink = entity.BookingLink.Trim();

            Event.CreatedOn = entity.CreatedOn;
            Event.CreatedBy = entity.CreatedBy;
            Event.ModifiedOn = entity.ModifiedOn;
            Event.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiEvent.Add(Event);
            SubmitChanges();
            return Event;

        }
        public void Update(XsiEvent entity)
        {
            XsiEvent Event = XsiContext.Context.XsiEvent.Find(entity.ItemId);
            XsiContext.Context.Entry(Event).State = EntityState.Modified;

            if (!entity.EventSubCategoryId.IsDefault())
                Event.EventSubCategoryId = entity.EventSubCategoryId;

            if (!entity.PhotoAlbumId.IsDefault())
                Event.PhotoAlbumId = entity.PhotoAlbumId;

            if (!entity.VideoAlbumId.IsDefault())
                Event.VideoAlbumId = entity.VideoAlbumId;

            if (!entity.ScrfphotoAlbumId.IsDefault())
                Event.ScrfphotoAlbumId = entity.ScrfphotoAlbumId;

            if (!entity.ScrfvideoAlbumId.IsDefault())
                Event.ScrfvideoAlbumId = entity.ScrfvideoAlbumId;

            if (!entity.Type.IsDefault())
                Event.Type = entity.Type;

            if (!entity.IsActive.IsDefault())
                Event.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                Event.Title = entity.Title.Trim();

            if (!entity.IsFeatured.IsDefault())
                Event.IsFeatured = entity.IsFeatured;

            if (!entity.FileName.IsDefault())
                Event.FileName = entity.FileName;

            if (!entity.EventThumbnailNew.IsDefault())
                Event.EventThumbnailNew = entity.EventThumbnailNew;

            if (!entity.InnerTopImageOne.IsDefault())
                Event.InnerTopImageOne = entity.InnerTopImageOne;

            if (!entity.InnerTopImageTwo.IsDefault())
                Event.InnerTopImageTwo = entity.InnerTopImageTwo;

            if (!entity.Overview.IsDefault())
                Event.Overview = entity.Overview.Trim();

            if (!entity.Details.IsDefault())
                Event.Details = entity.Details.Trim();

            if (!entity.Guest.IsDefault())
                Event.Guest = entity.Guest.Trim();

            if (!entity.StartDate.IsDefault())
                Event.StartDate = entity.StartDate;

            if (!entity.EndDate.IsDefault())
                Event.EndDate = entity.EndDate;

            if (!entity.Location.IsDefault())
                Event.Location = entity.Location.Trim();

            #region Ar
            if (!entity.TitleAr.IsDefault())
                Event.TitleAr = entity.TitleAr.Trim();
            if (!entity.FileNameAr.IsDefault())
                Event.FileNameAr = entity.FileNameAr;
            if (!entity.EventThumbnailNewAr.IsDefault())
                Event.EventThumbnailNewAr = entity.EventThumbnailNewAr;
            if (!entity.InnerTopImageOneAr.IsDefault())
                Event.InnerTopImageOneAr = entity.InnerTopImageOneAr;
            if (!entity.InnerTopImageTwoAr.IsDefault())
                Event.InnerTopImageTwoAr = entity.InnerTopImageTwoAr;
            if (!entity.OverviewAr.IsDefault())
                Event.OverviewAr = entity.OverviewAr.Trim();
            if (!entity.DetailsAr.IsDefault())
                Event.DetailsAr = entity.DetailsAr.Trim();
            if (!entity.GuestAr.IsDefault())
                Event.GuestAr = entity.GuestAr.Trim();
            if (!entity.LocationAr.IsDefault())
                Event.LocationAr = entity.LocationAr.Trim();
            #endregion Ar

            if (!entity.BookingLink.IsDefault())
                Event.BookingLink = entity.BookingLink.Trim();

            if (!entity.ModifiedOn.IsDefault())
                Event.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                Event.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiEvent entity)
        {
            foreach (XsiEvent Event in Select(entity))
            {
                XsiEvent aEvent = XsiContext.Context.XsiEvent.Find(Event.ItemId);
                XsiContext.Context.XsiEvent.Remove(aEvent);
            }
        }
        public void Delete(long itemId)
        {
            XsiEvent aEvent = XsiContext.Context.XsiEvent.Find(itemId);
            XsiContext.Context.XsiEvent.Remove(aEvent);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
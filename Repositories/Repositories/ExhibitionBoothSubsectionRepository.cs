﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionBoothSubsectionRepository : IExhibitionBoothSubsectionRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionBoothSubsectionRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionBoothSubsectionRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionBoothSubsectionRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionBoothSubsection GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionBoothSubsection.Find(itemId);
        }
        
        public List<XsiExhibitionBoothSubsection> Select()
        {
            return XsiContext.Context.XsiExhibitionBoothSubsection.ToList();
        }
        public List<XsiExhibitionBoothSubsection> Select(XsiExhibitionBoothSubsection entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBoothSubsection>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
 
            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Color.IsDefault())
                predicate = predicate.And(p => p.Color.Contains(entity.Color));

            if (!entity.WebsiteType.IsDefault())
                predicate = predicate.And(p => p.WebsiteType.Equals(entity.WebsiteType));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionBoothSubsection.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionBoothSubsection> SelectComeplete(XsiExhibitionBoothSubsection entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionBoothSubsection>();
            var predicate = PredicateBuilder.True<XsiExhibitionBoothSubsection>();

            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
             
            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.CategoryId.IsDefault())
            {
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);
                XsiExhibitionBooth MainEntity = XsiContext.Context.XsiExhibitionBooth.Where(p => p.ItemId == entity.CategoryId).FirstOrDefault();

                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title;
                    var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                    Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                        || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                        || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                        || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                        );

                    isInner = true;
                }
                else
                {
                    if (!entity.Title.IsDefault())
                    {
                        string strSearchText = entity.Title.ToLower();
                        Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                        isInner = true;
                    }
                }
            }
            else
            {
                if (!entity.Title.IsDefault())
                    predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));
            }
            if (!entity.WebsiteType.IsDefault())
                predicate = predicate.And(p => p.WebsiteType.Equals(entity.WebsiteType));

            if (!entity.Color.IsDefault())
                predicate = predicate.And(p => p.Color.Contains(entity.Color));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionBoothSubsection.Include(p => p.Category).AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionBoothSubsection Add(XsiExhibitionBoothSubsection entity)
        {
            XsiExhibitionBoothSubsection ExhibitionBoothSubsection = new XsiExhibitionBoothSubsection();
            
            ExhibitionBoothSubsection.CategoryId = entity.CategoryId;
            if (!entity.Title.IsDefault())
                ExhibitionBoothSubsection.Title = entity.Title.Trim();
            ExhibitionBoothSubsection.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionBoothSubsection.TitleAr = entity.TitleAr.Trim();
            ExhibitionBoothSubsection.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.Color.IsDefault())
            ExhibitionBoothSubsection.Color = entity.Color.Trim();
            ExhibitionBoothSubsection.WebsiteType = entity.WebsiteType; 
            ExhibitionBoothSubsection.CreatedOn = entity.CreatedOn;
            ExhibitionBoothSubsection.CreatedBy = entity.CreatedBy;
            ExhibitionBoothSubsection.ModifiedOn = entity.ModifiedOn;
            ExhibitionBoothSubsection.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionBoothSubsection.Add(ExhibitionBoothSubsection);
            SubmitChanges();
            return ExhibitionBoothSubsection;

        }
        public void Update(XsiExhibitionBoothSubsection entity)
        {
            XsiExhibitionBoothSubsection ExhibitionBoothSubsection = XsiContext.Context.XsiExhibitionBoothSubsection.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionBoothSubsection).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionBoothSubsection.Title = entity.Title.Trim();

            if (!entity.Color.IsDefault())
                ExhibitionBoothSubsection.Color = entity.Color.Trim();

            if (!entity.WebsiteType.IsDefault())
                ExhibitionBoothSubsection.WebsiteType = entity.WebsiteType;

            if (!entity.IsActive.IsDefault())
                ExhibitionBoothSubsection.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionBoothSubsection.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                ExhibitionBoothSubsection.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionBoothSubsection.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionBoothSubsection.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionBoothSubsection entity)
        {
            foreach (XsiExhibitionBoothSubsection ExhibitionBoothSubsection in Select(entity))
            {
                XsiExhibitionBoothSubsection aExhibitionBoothSubsection = XsiContext.Context.XsiExhibitionBoothSubsection.Find(ExhibitionBoothSubsection.ItemId);
                XsiContext.Context.XsiExhibitionBoothSubsection.Remove(aExhibitionBoothSubsection);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionBoothSubsection aExhibitionBoothSubsection = XsiContext.Context.XsiExhibitionBoothSubsection.Find(itemId);
            XsiContext.Context.XsiExhibitionBoothSubsection.Remove(aExhibitionBoothSubsection);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
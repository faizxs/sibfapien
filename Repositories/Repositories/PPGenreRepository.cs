﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PPGenreRepository : IPPGenreRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PPGenreRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PPGenreRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PPGenreRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionPpgenre GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionPpgenre.Find(itemId);
        }
        public List<XsiExhibitionPpgenre> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionPpgenre> Select()
        {
            return XsiContext.Context.XsiExhibitionPpgenre.ToList();
        }
        public List<XsiExhibitionPpgenre> Select(XsiExhibitionPpgenre entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionPpgenre>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.GenreId.IsDefault())
                predicate = predicate.And(p => p.GenreId == entity.GenreId);

            if (!entity.ProfessionalProgramRegistrationId.IsDefault())
                predicate = predicate.And(p => p.ProfessionalProgramRegistrationId == entity.ProfessionalProgramRegistrationId);

            return XsiContext.Context.XsiExhibitionPpgenre.AsExpandable().Where(predicate).ToList();
        }
        public XsiExhibitionPpgenre Add(XsiExhibitionPpgenre entity)
        {
            XsiExhibitionPpgenre PPGenre = new XsiExhibitionPpgenre();
            PPGenre.ProfessionalProgramRegistrationId = entity.ProfessionalProgramRegistrationId;
            PPGenre.GenreId = entity.GenreId;
            XsiContext.Context.XsiExhibitionPpgenre.Add(PPGenre);
            SubmitChanges();
            return PPGenre;

        }
        public void Update(XsiExhibitionPpgenre entity)
        {
            XsiExhibitionPpgenre PPGenre = XsiContext.Context.XsiExhibitionPpgenre.Find(entity.ItemId);
            XsiContext.Context.Entry(PPGenre).State = EntityState.Modified;

            if (!entity.GenreId.IsDefault())
                PPGenre.GenreId = entity.GenreId;

            if (!entity.ProfessionalProgramRegistrationId.IsDefault())
                PPGenre.ProfessionalProgramRegistrationId = entity.ProfessionalProgramRegistrationId;
        }
        public void Delete(XsiExhibitionPpgenre entity)
        {
            foreach (XsiExhibitionPpgenre PPGenre in Select(entity))
            {
                XsiExhibitionPpgenre aPPGenre = XsiContext.Context.XsiExhibitionPpgenre.Find(PPGenre.ItemId);
                XsiContext.Context.XsiExhibitionPpgenre.Remove(aPPGenre);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionPpgenre aPPGenre = XsiContext.Context.XsiExhibitionPpgenre.Find(itemId);
            XsiContext.Context.XsiExhibitionPpgenre.Remove(aPPGenre);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
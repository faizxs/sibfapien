﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFPageMenuRepository : ISCRFPageMenuRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFPageMenuRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFPageMenuRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFPageMenuRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfpageMenu GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfpageMenu.Find(itemId);
        }

        public List<XsiScrfpageMenu> Select()
        {
            return XsiContext.Context.XsiScrfpageMenu.ToList();
        }

        public List<XsiScrfpageMenu> SelectOr(XsiScrfpageMenu entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfpageMenu>();
            var Outer = PredicateBuilder.True<XsiScrfpageMenu>();

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                     || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                     || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                     || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                     );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();

                    if (!entity.Title.IsDefault())
                        Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));

                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.ParentId.IsDefault())
                Outer = Outer.And(p => p.ParentId == entity.ParentId);

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiScrfpageMenu.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiScrfpageMenu> Select(XsiScrfpageMenu entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfpageMenu>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ParentId.IsDefault())
                predicate = predicate.And(p => p.ParentId == entity.ParentId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Equals(entity.Title));

            if (!entity.TitleImage.IsDefault())
                predicate = predicate.And(p => p.TitleImage.Equals(entity.TitleImage));

            if (!entity.IsTitleImagePrimary.IsDefault())
                predicate = predicate.And(p => p.IsTitleImagePrimary.Equals(entity.IsTitleImagePrimary));

            if (!entity.PageUrl.IsDefault())
                predicate = predicate.And(p => p.PageUrl.Equals(entity.PageUrl));

            if (!entity.PageContentId.IsDefault())
                predicate = predicate.And(p => p.PageContentId == entity.PageContentId);

            if (!entity.SortOrder.IsDefault())
                predicate = predicate.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.IsExternal.IsDefault())
                predicate = predicate.And(p => p.IsExternal.Equals(entity.IsExternal));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            return XsiContext.Context.XsiScrfpageMenu.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfpageMenu Add(XsiScrfpageMenu entity)
        {
            XsiScrfpageMenu SCRFPageMenu = new XsiScrfpageMenu();
            SCRFPageMenu.ParentId = entity.ParentId;
            if (!entity.Title.IsDefault())
            SCRFPageMenu.Title = entity.Title.Trim();
            SCRFPageMenu.TitleImage = entity.TitleImage;
            SCRFPageMenu.IsTitleImagePrimary = entity.IsTitleImagePrimary;
            if (!entity.PageUrl.IsDefault())
            SCRFPageMenu.PageUrl = entity.PageUrl.Trim();
            SCRFPageMenu.PageContentId = entity.PageContentId;
            SCRFPageMenu.SortOrder = entity.SortOrder;
            SCRFPageMenu.IsExternal = entity.IsExternal;
            SCRFPageMenu.IsActive = entity.IsActive;

            XsiContext.Context.XsiScrfpageMenu.Add(SCRFPageMenu);
            SubmitChanges();
            return SCRFPageMenu;

        }
        public void Update(XsiScrfpageMenu entity)
        {
            XsiScrfpageMenu SCRFPageMenu = XsiContext.Context.XsiScrfpageMenu.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFPageMenu).State = EntityState.Modified;

            if (!entity.ParentId.IsDefault())
                SCRFPageMenu.ParentId = entity.ParentId;

            if (!entity.Title.IsDefault())
                SCRFPageMenu.Title = entity.Title.Trim();

            if (!entity.TitleImage.IsDefault())
                SCRFPageMenu.TitleImage = entity.TitleImage;

            if (!entity.IsTitleImagePrimary.IsDefault())
                SCRFPageMenu.IsTitleImagePrimary = entity.IsTitleImagePrimary;

            if (!entity.PageUrl.IsDefault())
                SCRFPageMenu.PageUrl = entity.PageUrl.Trim();

            if (!entity.PageContentId.IsDefault())
                SCRFPageMenu.PageContentId = entity.PageContentId;

            if (!entity.SortOrder.IsDefault())
                SCRFPageMenu.SortOrder = entity.SortOrder;

            if (!entity.IsExternal.IsDefault())
                SCRFPageMenu.IsExternal = entity.IsExternal;

            if (!entity.IsActive.IsDefault())
                SCRFPageMenu.IsActive = entity.IsActive;
        }
        public void Delete(XsiScrfpageMenu entity)
        {
            foreach (XsiScrfpageMenu SCRFPageMenu in Select(entity))
            {
                XsiScrfpageMenu aSCRFPageMenu = XsiContext.Context.XsiScrfpageMenu.Find(SCRFPageMenu.ItemId);
                XsiContext.Context.XsiScrfpageMenu.Remove(aSCRFPageMenu);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfpageMenu aSCRFPageMenu = XsiContext.Context.XsiScrfpageMenu.Find(itemId);
            XsiContext.Context.XsiScrfpageMenu.Remove(aSCRFPageMenu);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
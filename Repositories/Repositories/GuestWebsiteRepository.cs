﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class GuestWebsiteRepository : IGuestWebsiteRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public GuestWebsiteRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public GuestWebsiteRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public GuestWebsiteRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiGuestWebsite GetById(long itemId)
        {
            return XsiContext.Context.XsiGuestWebsite.Find(itemId);
        }
        public List<XsiGuestWebsite> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiGuestWebsite> Select()
        {
            return XsiContext.Context.XsiGuestWebsite.ToList();
        }
        public List<XsiGuestWebsite> Select(XsiGuestWebsite entity)
        {
            var predicate = PredicateBuilder.True<XsiGuestWebsite>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.GuestId.IsDefault())
                predicate = predicate.And(p => p.GuestId == entity.GuestId);

            return XsiContext.Context.XsiGuestWebsite.AsExpandable().Where(predicate).ToList();
        }

        public List<long?> Select(long ID1, long ID2, long ID3)
        {
            var predicate = PredicateBuilder.True<XsiGuestWebsite>();
            if (ID1 != -1)
                predicate = predicate.And(p => p.WebsiteId == ID1);

            if (ID2 != -1)
                predicate = predicate.And(p => p.WebsiteId == ID2);

            if (ID3 != -1)
                predicate = predicate.And(p => p.WebsiteId == ID3);

            return XsiContext.Context.XsiGuestWebsite.AsExpandable().Where(predicate).Select(s => s.GuestId).ToList();
        }

        public XsiGuestWebsite Add(XsiGuestWebsite entity)
        {
            XsiGuestWebsite GuestWebsite = new XsiGuestWebsite();
            GuestWebsite.GuestId = entity.GuestId;
            GuestWebsite.WebsiteId = entity.WebsiteId;

            XsiContext.Context.XsiGuestWebsite.Add(GuestWebsite);
            SubmitChanges();
            return GuestWebsite;

        }
        public void Update(XsiGuestWebsite entity)
        {
            XsiGuestWebsite GuestWebsite = XsiContext.Context.XsiGuestWebsite.Find(entity.ItemId);
            XsiContext.Context.Entry(GuestWebsite).State = EntityState.Modified;

            if (!entity.GuestId.IsDefault())
                GuestWebsite.GuestId = entity.GuestId;

            if (!entity.WebsiteId.IsDefault())
                GuestWebsite.WebsiteId = entity.WebsiteId;

        }
        public void Delete(XsiGuestWebsite entity)
        {
            foreach (XsiGuestWebsite GuestWebsite in Select(entity))
            {
                XsiGuestWebsite aGuestWebsite = XsiContext.Context.XsiGuestWebsite.Find(GuestWebsite.ItemId);
                XsiContext.Context.XsiGuestWebsite.Remove(aGuestWebsite);
            }
        }
        public void Delete(long itemId)
        {
            XsiGuestWebsite aGuestWebsite = XsiContext.Context.XsiGuestWebsite.Find(itemId);
            XsiContext.Context.XsiGuestWebsite.Remove(aGuestWebsite);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
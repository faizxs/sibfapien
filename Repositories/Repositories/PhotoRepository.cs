﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PhotoRepository : IPhotoRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PhotoRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PhotoRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PhotoRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiPhoto GetById(long itemId)
        {
            return XsiContext.Context.XsiPhoto.Find(itemId);
        }

        public List<XsiPhoto> Select()
        {
            return XsiContext.Context.XsiPhoto.ToList();
        }
        public List<XsiPhoto> Select(XsiPhoto entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiPhoto>();
            var predicate = PredicateBuilder.True<XsiPhoto>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.PhotoAlbumId.IsDefault())
                predicate = predicate.And(p => p.PhotoAlbumId == entity.PhotoAlbumId);
            
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsAlbumCover.IsDefault())
                predicate = predicate.And(p => p.IsAlbumCover.Equals(entity.IsAlbumCover));

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.TitleAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.TitleAr.Contains(searchKeys.str1) || p.TitleAr.Contains(searchKeys.str2) || p.TitleAr.Contains(searchKeys.str3) || p.TitleAr.Contains(searchKeys.str4) || p.TitleAr.Contains(searchKeys.str5)
                    || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                    || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                    || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail.Contains(entity.Thumbnail));

            if (!entity.SortIndex.IsDefault())
                predicate = predicate.And(p => p.SortIndex == entity.SortIndex);

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));
            
            if (!entity.DescriptionAr.IsDefault())
                predicate = predicate.And(p => p.DescriptionAr.Contains(entity.DescriptionAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiPhoto.AsExpandable().Where(predicate).ToList();
        }

        public XsiPhoto Add(XsiPhoto entity)
        {
            XsiPhoto Photo = new XsiPhoto();
            Photo.PhotoAlbumId = entity.PhotoAlbumId;

            Photo.IsActive = entity.IsActive;
            Photo.IsAlbumCover = entity.IsAlbumCover;
            if (!entity.Title.IsDefault())
                Photo.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
            Photo.Description = entity.Description.Trim();
            Photo.Thumbnail = entity.Thumbnail;

            Photo.IsActiveAr = entity.IsActiveAr;
            if (!entity.TitleAr.IsDefault())
                Photo.TitleAr = entity.TitleAr.Trim();
            if (!entity.DescriptionAr.IsDefault())
                Photo.DescriptionAr = entity.DescriptionAr.Trim();

            Photo.SortIndex = entity.SortIndex;
            Photo.CreatedOn = entity.CreatedOn;
            Photo.CreatedBy = entity.CreatedBy;
            Photo.ModifiedOn = entity.ModifiedOn;
            Photo.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiPhoto.Add(Photo);
            SubmitChanges();
            return Photo;

        }
        public void Update(XsiPhoto entity)
        {
            XsiPhoto Photo = XsiContext.Context.XsiPhoto.Find(entity.ItemId);
            XsiContext.Context.Entry(Photo).State = EntityState.Modified;

            if (!entity.PhotoAlbumId.IsDefault())
                Photo.PhotoAlbumId = entity.PhotoAlbumId;

            if (!entity.IsActive.IsDefault())
                Photo.IsActive = entity.IsActive;

            if (!entity.IsAlbumCover.IsDefault())
                Photo.IsAlbumCover = entity.IsAlbumCover;

            if (!entity.Title.IsDefault())
                Photo.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                Photo.Description = entity.Description.Trim();

            if (!entity.Thumbnail.IsDefault())
                Photo.Thumbnail = entity.Thumbnail;

            if (!entity.SortIndex.IsDefault())
                Photo.SortIndex = entity.SortIndex;

            if (!entity.IsActiveAr.IsDefault())
                Photo.IsActiveAr = entity.IsActiveAr;
            
            if (!entity.TitleAr.IsDefault())
                Photo.TitleAr = entity.TitleAr.Trim();

            if (!entity.DescriptionAr.IsDefault())
                Photo.DescriptionAr = entity.DescriptionAr.Trim();

            if (!entity.CreatedOn.IsDefault())
                Photo.CreatedOn = entity.CreatedOn;

            if (!entity.CreatedBy.IsDefault())
                Photo.CreatedBy = entity.CreatedBy;

            if (!entity.ModifiedOn.IsDefault())
                Photo.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                Photo.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiPhoto entity)
        {
            foreach (XsiPhoto Photo in Select(entity))
            {
                XsiPhoto aPhoto = XsiContext.Context.XsiPhoto.Find(Photo.ItemId);
                XsiContext.Context.XsiPhoto.Remove(aPhoto);
            }
        }
        public void Delete(long itemId)
        {
            XsiPhoto aPhoto = XsiContext.Context.XsiPhoto.Find(itemId);
            XsiContext.Context.XsiPhoto.Remove(aPhoto);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
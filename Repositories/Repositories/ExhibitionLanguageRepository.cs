﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionLanguageRepository : IExhibitionLanguageRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionLanguageRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionLanguageRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionLanguageRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionLanguage GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionLanguage.Find(itemId);
        }

        public List<XsiExhibitionLanguage> Select()
        {
            return XsiContext.Context.XsiExhibitionLanguage.ToList();
        }
        public List<XsiExhibitionLanguage> Select(XsiExhibitionLanguage entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionLanguage>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));
             
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionLanguage.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiExhibitionLanguage> SelectOr(XsiExhibitionLanguage entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionLanguage>();
            var Outer = PredicateBuilder.True<XsiExhibitionLanguage>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
             
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionLanguage.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiExhibitionLanguage Add(XsiExhibitionLanguage entity)
        {
            XsiExhibitionLanguage ExhibitionLanguage = new XsiExhibitionLanguage();
             
            if (!entity.Title.IsDefault())
            ExhibitionLanguage.Title = entity.Title.Trim();
            ExhibitionLanguage.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionLanguage.TitleAr = entity.TitleAr.Trim();
            ExhibitionLanguage.IsActiveAr = entity.IsActiveAr;
            #endregion Ar
            ExhibitionLanguage.CreatedOn = entity.CreatedOn;
            ExhibitionLanguage.CreatedBy = entity.CreatedBy;
            ExhibitionLanguage.ModifiedOn = entity.ModifiedOn;
            ExhibitionLanguage.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiExhibitionLanguage.Add(ExhibitionLanguage);
            SubmitChanges();
            return ExhibitionLanguage;

        }
        public void Update(XsiExhibitionLanguage entity)
        {
            XsiExhibitionLanguage ExhibitionLanguage = XsiContext.Context.XsiExhibitionLanguage.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionLanguage).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionLanguage.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                ExhibitionLanguage.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionLanguage.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                ExhibitionLanguage.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionLanguage.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                ExhibitionLanguage.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionLanguage entity)
        {
            foreach (XsiExhibitionLanguage ExhibitionLanguage in Select(entity))
            {
                XsiExhibitionLanguage aExhibitionLanguage = XsiContext.Context.XsiExhibitionLanguage.Find(ExhibitionLanguage.ItemId);
                XsiContext.Context.XsiExhibitionLanguage.Remove(aExhibitionLanguage);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionLanguage aExhibitionLanguage = XsiContext.Context.XsiExhibitionLanguage.Find(itemId);
            XsiContext.Context.XsiExhibitionLanguage.Remove(aExhibitionLanguage);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
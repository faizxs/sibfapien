﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PageMenuRepository : IPageMenuRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PageMenuRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PageMenuRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PageMenuRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiPageMenuNew GetById(long itemId)
        {
            return XsiContext.Context.XsiPageMenuNew.Find(itemId);
        }

        public List<XsiPageMenuNew> SelectOr(XsiPageMenuNew entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiPageMenuNew>();
            var Outer = PredicateBuilder.True<XsiPageMenuNew>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                     || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                     || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                     || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                     );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.ParentId.IsDefault())
                Outer = Outer.And(p => p.ParentId == entity.ParentId);

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiPageMenuNew.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public List<XsiPageMenuNew> Select()
        {
            if (CachingRepository.CachedMenu == null)
                CachingRepository.CachedMenu = XsiContext.Context.XsiPageMenuNew.ToList();

            return CachingRepository.CachedMenu.AsQueryable().ToList();
        }
        public List<XsiPageMenuNew> Select(XsiPageMenuNew entity)
        {
            var predicate = PredicateBuilder.True<XsiPageMenuNew>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ParentId.IsDefault())
                predicate = predicate.And(p => p.ParentId == entity.ParentId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Equals(entity.Title));

            if (!entity.TitleImage.IsDefault())
                predicate = predicate.And(p => p.TitleImage.Equals(entity.TitleImage));

            if (!entity.IsTitleImagePrimary.IsDefault())
                predicate = predicate.And(p => p.IsTitleImagePrimary.Equals(entity.IsTitleImagePrimary));

            if (!entity.PageUrl.IsDefault())
                predicate = predicate.And(p => p.PageUrl.Equals(entity.PageUrl));

            if (!entity.PageContentId.IsDefault())
                predicate = predicate.And(p => p.PageContentId == entity.PageContentId);

            if (!entity.SortOrder.IsDefault())
                predicate = predicate.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.IsExternal.IsDefault())
                predicate = predicate.And(p => p.IsExternal.Equals(entity.IsExternal));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (CachingRepository.CachedMenu == null)
                CachingRepository.CachedMenu = XsiContext.Context.XsiPageMenuNew.ToList();

            return CachingRepository.CachedMenu.AsQueryable().Where(predicate).ToList();
        }

        public XsiPageMenuNew Add(XsiPageMenuNew entity)
        {
            XsiPageMenuNew PageMenu = new XsiPageMenuNew();
            PageMenu.ParentId = entity.ParentId;
            if (!entity.Title.IsDefault())
                PageMenu.Title = entity.Title.Trim();
            PageMenu.TitleImage = entity.TitleImage;
            PageMenu.IsTitleImagePrimary = entity.IsTitleImagePrimary;
            if (!entity.PageUrl.IsDefault())
                PageMenu.PageUrl = entity.PageUrl.Trim();
            PageMenu.PageContentId = entity.PageContentId;
            PageMenu.SortOrder = entity.SortOrder;
            PageMenu.IsExternal = entity.IsExternal;
            PageMenu.IsActive = entity.IsActive;
            XsiContext.Context.XsiPageMenuNew.Add(PageMenu);
            SubmitChanges();
            if (CachingRepository.CachedMenu == null)
                CachingRepository.CachedMenu = XsiContext.Context.XsiPageMenuNew.ToList();
            else
                CachingRepository.CachedMenu.Add(PageMenu);
            return PageMenu;
        }
        public void Update(XsiPageMenuNew entity)
        {
            XsiPageMenuNew PageMenu = XsiContext.Context.XsiPageMenuNew.Find(entity.ItemId);
            XsiContext.Context.Entry(PageMenu).State = EntityState.Modified;

            if (!entity.ParentId.IsDefault())
                PageMenu.ParentId = entity.ParentId;

            if (!entity.Title.IsDefault())
                PageMenu.Title = entity.Title.Trim();

            if (!entity.TitleImage.IsDefault())
                PageMenu.TitleImage = entity.TitleImage;

            if (!entity.IsTitleImagePrimary.IsDefault())
                PageMenu.IsTitleImagePrimary = entity.IsTitleImagePrimary;

            if (!entity.PageUrl.IsDefault())
                PageMenu.PageUrl = entity.PageUrl.Trim();

            if (!entity.PageContentId.IsDefault())
                PageMenu.PageContentId = entity.PageContentId;

            if (!entity.SortOrder.IsDefault())
                PageMenu.SortOrder = entity.SortOrder;

            if (!entity.IsExternal.IsDefault())
                PageMenu.IsExternal = entity.IsExternal;

            if (!entity.IsActive.IsDefault())
                PageMenu.IsActive = entity.IsActive;
            if (CachingRepository.CachedMenu == null)
                CachingRepository.CachedMenu = XsiContext.Context.XsiPageMenuNew.ToList();
            else
            {
                #region update cache
                var ItemToModify = CachingRepository.CachedMenu.Where(p => p.ItemId == entity.ItemId).FirstOrDefault();

                if (!entity.ParentId.IsDefault())
                    ItemToModify.ParentId = entity.ParentId;

                if (!entity.Title.IsDefault())
                    ItemToModify.Title = entity.Title.Trim();

                if (!entity.TitleImage.IsDefault())
                    ItemToModify.TitleImage = entity.TitleImage;

                if (!entity.IsTitleImagePrimary.IsDefault())
                    ItemToModify.IsTitleImagePrimary = entity.IsTitleImagePrimary;

                if (!entity.PageUrl.IsDefault())
                    ItemToModify.PageUrl = entity.PageUrl.Trim();

                if (!entity.PageContentId.IsDefault())
                    ItemToModify.PageContentId = entity.PageContentId;

                if (!entity.SortOrder.IsDefault())
                    ItemToModify.SortOrder = entity.SortOrder;

                if (!entity.IsExternal.IsDefault())
                    ItemToModify.IsExternal = entity.IsExternal;

                if (!entity.IsActive.IsDefault())
                    ItemToModify.IsActive = entity.IsActive; 
                #endregion
            }
        }
        public void Delete(XsiPageMenuNew entity)
        {
            foreach (XsiPageMenuNew PageMenu in Select(entity))
            {
                XsiPageMenuNew aPageMenu = XsiContext.Context.XsiPageMenuNew.Find(PageMenu.ItemId);
                XsiContext.Context.XsiPageMenuNew.Remove(aPageMenu);
                if (CachingRepository.CachedMenu == null)
                    CachingRepository.CachedMenu = XsiContext.Context.XsiPageMenuNew.ToList();
                CachingRepository.CachedMenu.Remove(CachingRepository.CachedMenu.SingleOrDefault(x => x.ItemId == PageMenu.ItemId));
            }
        }
        public void Delete(long itemId)
        {
            XsiPageMenuNew aPageMenu = XsiContext.Context.XsiPageMenuNew.Find(itemId);
            XsiContext.Context.XsiPageMenuNew.Remove(aPageMenu);
            if (CachingRepository.CachedMenu == null)
                CachingRepository.CachedMenu = XsiContext.Context.XsiPageMenuNew.ToList();
            CachingRepository.CachedMenu.Remove(CachingRepository.CachedMenu.SingleOrDefault(x => x.ItemId == itemId));
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
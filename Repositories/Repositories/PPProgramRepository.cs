﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PPProgramRepository : IPPProgramRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PPProgramRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PPProgramRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PPProgramRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionPpprogram GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionPpprogram.Find(itemId);
        }
        public List<XsiExhibitionPpprogram> GetByGroupId(long groupId)
        {
            return null;
        }
        public List<XsiExhibitionPpprogram> Select()
        {
            return XsiContext.Context.XsiExhibitionPpprogram.ToList();
        }
        public List<XsiExhibitionPpprogram> Select(XsiExhibitionPpprogram entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionPpprogram>();
            if (!entity.ProfessionalProgramRegistrationId.IsDefault())
                predicate = predicate.And(p => p.ProfessionalProgramRegistrationId == entity.ProfessionalProgramRegistrationId);


            if (!entity.ProfessionalProgramRegistrationId.IsDefault())
                predicate = predicate.And(p => p.ProfessionalProgramRegistrationId == entity.ProfessionalProgramRegistrationId);

            return XsiContext.Context.XsiExhibitionPpprogram.AsExpandable().Where(predicate).ToList();
        }
        public XsiExhibitionPpprogram  Add(XsiExhibitionPpprogram entity)
        {
            XsiExhibitionPpprogram PPProgram = new XsiExhibitionPpprogram();
            PPProgram.ProfessionalProgramRegistrationId = entity.ProfessionalProgramRegistrationId;
            PPProgram.PpprogramId = entity.PpprogramId;
            XsiContext.Context.XsiExhibitionPpprogram.Add(PPProgram);
            SubmitChanges();
            return PPProgram;

        }
        public void Update(XsiExhibitionPpprogram entity)
        {
            XsiExhibitionPpprogram PPProgram = XsiContext.Context.XsiExhibitionPpprogram.Find(entity.ProfessionalProgramRegistrationId);
            XsiContext.Context.Entry(PPProgram).State = EntityState.Modified;

            if (!entity.PpprogramId.IsDefault())
                PPProgram.PpprogramId = entity.PpprogramId;

            if (!entity.ProfessionalProgramRegistrationId.IsDefault())
                PPProgram.ProfessionalProgramRegistrationId = entity.ProfessionalProgramRegistrationId;
        }
        public void Delete(XsiExhibitionPpprogram entity)
        {
            foreach (XsiExhibitionPpprogram PPProgram in Select(entity))
            {
                XsiExhibitionPpprogram aPPProgram = XsiContext.Context.XsiExhibitionPpprogram.Find(PPProgram.ProfessionalProgramRegistrationId);
                XsiContext.Context.XsiExhibitionPpprogram.Remove(aPPProgram);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionPpprogram aPPProgram = XsiContext.Context.XsiExhibitionPpprogram.Find(itemId);
            XsiContext.Context.XsiExhibitionPpprogram.Remove(aPPProgram);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFIconsForSponsorRepository : ISCRFIconsForSponsorRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFIconsForSponsorRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFIconsForSponsorRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFIconsForSponsorRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrficonsForSponsor GetById(long itemId)
        {
            return XsiContext.Context.XsiScrficonsForSponsor.Find(itemId);
        }

        public List<XsiScrficonsForSponsor> Select()
        {
            return XsiContext.Context.XsiScrficonsForSponsor.ToList();
        }
        public List<XsiScrficonsForSponsor> Select(XsiScrficonsForSponsor entity)
        {
            var predicate = PredicateBuilder.True<XsiScrficonsForSponsor>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Contains(entity.ImageName));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            return XsiContext.Context.XsiScrficonsForSponsor.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiScrficonsForSponsor> SelectOr(XsiScrficonsForSponsor entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrficonsForSponsor>();
            var Outer = PredicateBuilder.True<XsiScrficonsForSponsor>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.ImageName.IsDefault())
                Outer = Outer.And(p => p.ImageName.Contains(entity.ImageName));

            if (!entity.Url.IsDefault())
                Outer = Outer.And(p => p.Url.Contains(entity.Url));

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiScrficonsForSponsor.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiScrficonsForSponsor Add(XsiScrficonsForSponsor entity)
        {
            XsiScrficonsForSponsor SCRFIconsForSponsor = new XsiScrficonsForSponsor();
            if (!entity.Title.IsDefault())
            SCRFIconsForSponsor.Title = entity.Title.Trim();
            SCRFIconsForSponsor.ImageName = entity.ImageName;
            if (!entity.Url.IsDefault())
            SCRFIconsForSponsor.Url = entity.Url.Trim();
            SCRFIconsForSponsor.IsActive = entity.IsActive;
            SCRFIconsForSponsor.CreatedOn = entity.CreatedOn;
            SCRFIconsForSponsor.CreatedBy = entity.CreatedBy;
            SCRFIconsForSponsor.ModifiedOn = entity.ModifiedOn;
            SCRFIconsForSponsor.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrficonsForSponsor.Add(SCRFIconsForSponsor);
            SubmitChanges();
            return SCRFIconsForSponsor;

        }
        public void Update(XsiScrficonsForSponsor entity)
        {
            XsiScrficonsForSponsor SCRFIconsForSponsor = XsiContext.Context.XsiScrficonsForSponsor.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFIconsForSponsor).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                SCRFIconsForSponsor.Title = entity.Title.Trim();
            if (!entity.ImageName.IsDefault())
                SCRFIconsForSponsor.ImageName = entity.ImageName;
            if (!entity.Url.IsDefault())
                SCRFIconsForSponsor.Url = entity.Url.Trim();
            if (!entity.IsActive.IsDefault())
                SCRFIconsForSponsor.IsActive = entity.IsActive;
            if (!entity.ModifiedOn.IsDefault())
                SCRFIconsForSponsor.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                SCRFIconsForSponsor.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiScrficonsForSponsor entity)
        {
            foreach (XsiScrficonsForSponsor SCRFIconsForSponsor in Select(entity))
            {
                XsiScrficonsForSponsor aSCRFIconsForSponsor = XsiContext.Context.XsiScrficonsForSponsor.Find(SCRFIconsForSponsor.ItemId);
                XsiContext.Context.XsiScrficonsForSponsor.Remove(aSCRFIconsForSponsor);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrficonsForSponsor aSCRFIconsForSponsor = XsiContext.Context.XsiScrficonsForSponsor.Find(itemId);
            XsiContext.Context.XsiScrficonsForSponsor.Remove(aSCRFIconsForSponsor);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
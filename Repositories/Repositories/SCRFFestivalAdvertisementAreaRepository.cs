﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFFestivalAdvertisementAreaRepository : ISCRFFestivalAdvertisementAreaRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFFestivalAdvertisementAreaRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFFestivalAdvertisementAreaRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFFestivalAdvertisementAreaRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrffestivalAdvertisementArea GetById(long itemId)
        {
            return XsiContext.Context.XsiScrffestivalAdvertisementArea.Find(itemId);
        }

        public List<XsiScrffestivalAdvertisementArea> Select()
        {
            return XsiContext.Context.XsiScrffestivalAdvertisementArea.ToList();
        }
        public List<XsiScrffestivalAdvertisementArea> Select(XsiScrffestivalAdvertisementArea entity)
        {
            var predicate = PredicateBuilder.True<XsiScrffestivalAdvertisementArea>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Cmstitle.IsDefault())
                predicate = predicate.And(p => p.Cmstitle.Contains(entity.Cmstitle));
            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.SubTitle.IsDefault())
                predicate = predicate.And(p => p.SubTitle.Contains(entity.SubTitle));
            if (!entity.HomeBanner.IsDefault())
                predicate = predicate.And(p => p.HomeBanner.Equals(entity.HomeBanner));
            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.IsFeatured.IsDefault())
                predicate = predicate.And(p => p.IsFeatured.Equals(entity.IsFeatured));
            return XsiContext.Context.XsiScrffestivalAdvertisementArea.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiScrffestivalAdvertisementArea> SelectOr(XsiScrffestivalAdvertisementArea entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrffestivalAdvertisementArea>();
            var Outer = PredicateBuilder.True<XsiScrffestivalAdvertisementArea>();

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiScrffestivalAdvertisementArea.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiScrffestivalAdvertisementArea Add(XsiScrffestivalAdvertisementArea entity)
        {
            XsiScrffestivalAdvertisementArea FestivalAdvertisementArea = new XsiScrffestivalAdvertisementArea();
            if (!entity.Cmstitle.IsDefault())
                FestivalAdvertisementArea.Cmstitle = entity.Cmstitle.Trim();
            if (!entity.Title.IsDefault())
                FestivalAdvertisementArea.Title = entity.Title.Trim();
            if (!entity.SubTitle.IsDefault())
            FestivalAdvertisementArea.SubTitle = entity.SubTitle.Trim();
            FestivalAdvertisementArea.HomeBanner = entity.HomeBanner;
            if (!entity.Url.IsDefault())
            FestivalAdvertisementArea.Url = entity.Url.Trim();
            FestivalAdvertisementArea.IsActive = entity.IsActive;
            FestivalAdvertisementArea.IsFeatured = entity.IsFeatured;
            FestivalAdvertisementArea.SortOrder = entity.SortOrder;
            FestivalAdvertisementArea.CreatedOn = entity.CreatedOn;
            FestivalAdvertisementArea.CreatedBy = entity.CreatedBy;
            FestivalAdvertisementArea.ModifiedOn = entity.ModifiedOn;
            FestivalAdvertisementArea.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrffestivalAdvertisementArea.Add(FestivalAdvertisementArea);
            SubmitChanges();
            return FestivalAdvertisementArea;

        }
        public void Update(XsiScrffestivalAdvertisementArea entity)
        {
            XsiScrffestivalAdvertisementArea FestivalAdvertisementArea = XsiContext.Context.XsiScrffestivalAdvertisementArea.Find(entity.ItemId);
            XsiContext.Context.Entry(FestivalAdvertisementArea).State = EntityState.Modified;

            if (!entity.Cmstitle.IsDefault())
                FestivalAdvertisementArea.Cmstitle = entity.Cmstitle.Trim();
            if (!entity.Title.IsDefault())
                FestivalAdvertisementArea.Title = entity.Title.Trim();
            if (!entity.SubTitle.IsDefault())
                FestivalAdvertisementArea.SubTitle = entity.SubTitle.Trim();
            if (!entity.HomeBanner.IsDefault())
                FestivalAdvertisementArea.HomeBanner = entity.HomeBanner;
            if (!entity.Url.IsDefault())
                FestivalAdvertisementArea.Url = entity.Url.Trim();
            if (!entity.IsActive.IsDefault())
                FestivalAdvertisementArea.IsActive = entity.IsActive;
            if (!entity.IsFeatured.IsDefault())
                FestivalAdvertisementArea.IsFeatured = entity.IsFeatured;
            if (!entity.SortOrder.IsDefault())
                FestivalAdvertisementArea.SortOrder = entity.SortOrder;
            if (!entity.ModifiedOn.IsDefault())
                FestivalAdvertisementArea.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                FestivalAdvertisementArea.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiScrffestivalAdvertisementArea entity)
        {
            foreach (XsiScrffestivalAdvertisementArea FestivalAdvertisementArea in Select(entity))
            {
                XsiScrffestivalAdvertisementArea aFestivalAdvertisementArea = XsiContext.Context.XsiScrffestivalAdvertisementArea.Find(FestivalAdvertisementArea.ItemId);
                XsiContext.Context.XsiScrffestivalAdvertisementArea.Remove(aFestivalAdvertisementArea);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrffestivalAdvertisementArea aFestivalAdvertisementArea = XsiContext.Context.XsiScrffestivalAdvertisementArea.Find(itemId);
            XsiContext.Context.XsiScrffestivalAdvertisementArea.Remove(aFestivalAdvertisementArea);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class VIPGuestRepository : IVIPGuestRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public VIPGuestRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public VIPGuestRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public VIPGuestRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiVipguest GetById(long itemId)
        {
            return XsiContext.Context.XsiVipguest.Find(itemId);
        }

        public List<XsiVipguest> Select()
        {
            return XsiContext.Context.XsiVipguest.ToList();
        }
        public List<XsiVipguest> Select(XsiVipguest entity)
        {
            var predicate = PredicateBuilder.True<XsiVipguest>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.Name.IsDefault())
                predicate = predicate.And(p => p.Name.Contains(entity.Name));

            if (!entity.NameAr.IsDefault())
                predicate = predicate.And(p => p.NameAr.Contains(entity.NameAr));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));


            if (!entity.Image.IsDefault())
                predicate = predicate.And(p => p.Image.Contains(entity.Image));


            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiVipguest.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiVipguest> SelectOr(XsiVipguest entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiVipguest>();
            var Outer = PredicateBuilder.True<XsiVipguest>();

            if (!entity.Name.IsDefault())
            {
                string strSearchText = entity.Name.ToLower();
                Inner = Inner.Or(p => p.Name.ToLower().Contains(strSearchText));
                isInner = true;
            }

            if (!entity.NameAr.IsDefault())
            {
                string strSearchText = entity.NameAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.NameAr.Contains(searchKeys.str1) || p.NameAr.Contains(searchKeys.str2) || p.NameAr.Contains(searchKeys.str3) || p.NameAr.Contains(searchKeys.str4) || p.NameAr.Contains(searchKeys.str5)
                                      || p.NameAr.Contains(searchKeys.str6) || p.NameAr.Contains(searchKeys.str7) || p.NameAr.Contains(searchKeys.str8) || p.NameAr.Contains(searchKeys.str9) || p.NameAr.Contains(searchKeys.str10)
                                      || p.NameAr.Contains(searchKeys.str11) || p.NameAr.Contains(searchKeys.str12) || p.NameAr.Contains(searchKeys.str13)
                                      || p.NameAr.Contains(searchKeys.str14) || p.NameAr.Contains(searchKeys.str15) || p.NameAr.Contains(searchKeys.str16) || p.NameAr.Contains(searchKeys.str17)
                || p.NameAr.Contains(searchKeys.str18) || p.NameAr.Contains(searchKeys.str19) || p.NameAr.Contains(searchKeys.str20) || p.NameAr.Contains(searchKeys.str21)
                        || p.NameAr.Contains(searchKeys.str22) || p.NameAr.Contains(searchKeys.str23) || p.NameAr.Contains(searchKeys.str24) || p.NameAr.Contains(searchKeys.str25)
                        || p.NameAr.Contains(searchKeys.str26) || p.NameAr.Contains(searchKeys.str27)
                                      );

                isInner = true;
            }
            else
            {
                if (!entity.Name.IsDefault())
                {
                    string strSearchText = entity.Name.ToLower();
                    Inner = Inner.Or(p => p.Name.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiVipguest.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiVipguest Add(XsiVipguest entity)
        {
            XsiVipguest VIPGuest = new XsiVipguest();

            VIPGuest.IsActive = entity.IsActive;
            VIPGuest.WebsiteId = entity.WebsiteId;

            if (!entity.Name.IsDefault())
                VIPGuest.Name = entity.Name.Trim();
            if (!entity.Title.IsDefault())
                VIPGuest.Title = entity.Title.Trim();

            if (!entity.Image.IsDefault())
                VIPGuest.Image = entity.Image.Trim();
            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                VIPGuest.IsActiveAr = entity.IsActiveAr;
            if (!entity.NameAr.IsDefault())
                VIPGuest.NameAr = entity.NameAr.Trim();
            if (!entity.TitleAr.IsDefault())
                VIPGuest.TitleAr = entity.TitleAr.Trim();
            if (!entity.ImageAr.IsDefault())
                VIPGuest.ImageAr = entity.ImageAr.Trim();
            #endregion Ar

            //VIPGuest.EventId = entity.EventId;
            VIPGuest.CreatedOn = entity.CreatedOn;
            VIPGuest.CreatedBy = entity.CreatedBy;
            VIPGuest.ModifiedOn = entity.ModifiedOn;
            VIPGuest.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiVipguest.Add(VIPGuest);
            SubmitChanges();
            return VIPGuest;

        }
        public void Update(XsiVipguest entity)
        {
            XsiVipguest VIPGuest = XsiContext.Context.XsiVipguest.Find(entity.ItemId);
            XsiContext.Context.Entry(VIPGuest).State = EntityState.Modified;

            if (!entity.WebsiteId.IsDefault())
                VIPGuest.WebsiteId = entity.WebsiteId;
            if (!entity.IsActive.IsDefault())
                VIPGuest.IsActive = entity.IsActive;
            if (!entity.Name.IsDefault())
                VIPGuest.Name = entity.Name.Trim();
            if (!entity.Title.IsDefault())
                VIPGuest.Title = entity.Title.Trim();

            if (!entity.Image.IsDefault())
                VIPGuest.Image = entity.Image.Trim();
            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                VIPGuest.IsActiveAr = entity.IsActiveAr;
            if (!entity.NameAr.IsDefault())
                VIPGuest.NameAr = entity.NameAr.Trim();
            if (!entity.TitleAr.IsDefault())
                VIPGuest.TitleAr = entity.TitleAr.Trim();

            if (!entity.ImageAr.IsDefault())
                VIPGuest.ImageAr = entity.ImageAr.Trim();
            #endregion Ar

            //if (!entity.EventId.IsDefault())
            //    VIPGuest.EventId = entity.EventId;
            if (!entity.ModifiedOn.IsDefault())
                VIPGuest.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                VIPGuest.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiVipguest entity)
        {
            foreach (XsiVipguest VIPGuest in Select(entity))
            {
                XsiVipguest aVIPGuest = XsiContext.Context.XsiVipguest.Find(VIPGuest.ItemId);
                XsiContext.Context.XsiVipguest.Remove(aVIPGuest);
            }
        }
        public void Delete(long itemId)
        {
            XsiVipguest aVIPGuest = XsiContext.Context.XsiVipguest.Find(itemId);
            XsiContext.Context.XsiVipguest.Remove(aVIPGuest);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
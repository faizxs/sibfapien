﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionBookSubjectRepository : IExhibitionBookSubjectRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionBookSubjectRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionBookSubjectRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionBookSubjectRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionBookSubject GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionBookSubject.Find(itemId);
        }
         
        public List<XsiExhibitionBookSubject> Select()
        {
            return XsiContext.Context.XsiExhibitionBookSubject.ToList();
        }
        public List<XsiExhibitionBookSubject> Select(XsiExhibitionBookSubject entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBookSubject>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.Color.IsDefault())
                predicate = predicate.And(p => p.Color.Contains(entity.Color));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionBookSubject.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionBookSubject> SelectOr(XsiExhibitionBookSubject entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionBookSubject>();
            var Outer = PredicateBuilder.True<XsiExhibitionBookSubject>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
 
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionBookSubject.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiExhibitionBookSubject> SelectOtherLanguageCategory(XsiExhibitionBookSubject entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBookSubject>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            var itemId = XsiContext.Context.XsiExhibitionBookSubject.AsExpandable().Where(predicate).Single().ItemId;
            predicate = PredicateBuilder.True<XsiExhibitionBookSubject>();
          
            return XsiContext.Context.XsiExhibitionBookSubject.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionBookSubject> SelectCurrentLanguageCategory(XsiExhibitionBookSubject entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBookSubject>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            var GroupId = XsiContext.Context.XsiExhibitionBookSubject.AsExpandable().Where(predicate).Single().ItemId;
            //predicate = PredicateBuilder.True<XsiExhibitionBookSubject>();
           
            return XsiContext.Context.XsiExhibitionBookSubject.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionBookSubject Add(XsiExhibitionBookSubject entity)
        {
            XsiExhibitionBookSubject ExhibitionBookSubject = new XsiExhibitionBookSubject();
            
            ExhibitionBookSubject.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                ExhibitionBookSubject.Title = entity.Title.Trim();
            if (!entity.Color.IsDefault())
                ExhibitionBookSubject.Color = entity.Color.Trim();
           
            #region Ar
            ExhibitionBookSubject.IsActiveAr = entity.IsActiveAr;
            if (!entity.TitleAr.IsDefault())
                ExhibitionBookSubject.TitleAr = entity.TitleAr.Trim(); 
            #endregion Ar

            ExhibitionBookSubject.CreatedOn = entity.CreatedOn;
            ExhibitionBookSubject.CreatedBy = entity.CreatedBy;
            ExhibitionBookSubject.ModifiedOn = entity.ModifiedOn;
            ExhibitionBookSubject.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionBookSubject.Add(ExhibitionBookSubject);
            SubmitChanges();
            return ExhibitionBookSubject;

        }
        public void Update(XsiExhibitionBookSubject entity)
        {
            XsiExhibitionBookSubject ExhibitionBookSubject = XsiContext.Context.XsiExhibitionBookSubject.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionBookSubject).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                ExhibitionBookSubject.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                ExhibitionBookSubject.Title = entity.Title.Trim();

            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                ExhibitionBookSubject.IsActiveAr = entity.IsActiveAr;

            if (!entity.TitleAr.IsDefault())
                ExhibitionBookSubject.TitleAr = entity.TitleAr.Trim();
            #endregion Ar

            if (!entity.Color.IsDefault())
                ExhibitionBookSubject.Color = entity.Color.Trim();

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionBookSubject.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionBookSubject.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiExhibitionBookSubject entity)
        {
            foreach (XsiExhibitionBookSubject ExhibitionBookSubject in Select(entity))
            {
                XsiExhibitionBookSubject aExhibitionBookSubject = XsiContext.Context.XsiExhibitionBookSubject.Find(ExhibitionBookSubject.ItemId);
                XsiContext.Context.XsiExhibitionBookSubject.Remove(aExhibitionBookSubject);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionBookSubject aExhibitionBookSubject = XsiContext.Context.XsiExhibitionBookSubject.Find(itemId);
            XsiContext.Context.XsiExhibitionBookSubject.Remove(aExhibitionBookSubject);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
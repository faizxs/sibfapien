﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitorActivityRepository : IExhibitorActivityRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitorActivityRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitorActivityRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitorActivityRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiMemberExhibitionActivityYearly GetById(long itemId)
        {
            return XsiContext.Context.XsiMemberExhibitionActivityYearly.Where(i => i.MemberExhibitionYearlyId == itemId).FirstOrDefault();
        }
        public List<XsiMemberExhibitionActivityYearly> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiMemberExhibitionActivityYearly> Select()
        {
            return XsiContext.Context.XsiMemberExhibitionActivityYearly.ToList();
        }
        public List<XsiMemberExhibitionActivityYearly> Select(XsiMemberExhibitionActivityYearly entity)
        {
            var predicate = PredicateBuilder.True<XsiMemberExhibitionActivityYearly>();
            if (!entity.MemberExhibitionYearlyId.IsDefault())
                predicate = predicate.And(p => p.MemberExhibitionYearlyId == entity.MemberExhibitionYearlyId);
            //if (!entity..IsDefault())
            //    predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);
            //if (!entity.ActivityGroupId.IsDefault())
            //    predicate = predicate.And(p => p.ActivityGroupId == entity.ActivityGroupId);

            return XsiContext.Context.XsiMemberExhibitionActivityYearly.AsExpandable().Where(predicate).ToList();
        }

        public XsiMemberExhibitionActivityYearly Add(XsiMemberExhibitionActivityYearly entity)
        {
            XsiMemberExhibitionActivityYearly ExhibitorActivity = new XsiMemberExhibitionActivityYearly();
            ExhibitorActivity.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;
            ExhibitorActivity.ActivityId = entity.ActivityId;

            XsiContext.Context.XsiMemberExhibitionActivityYearly.Add(ExhibitorActivity);
            SubmitChanges();
            return ExhibitorActivity;

        }
        public void Update(XsiMemberExhibitionActivityYearly entity)
        {
            XsiMemberExhibitionActivityYearly ExhibitorActivity = XsiContext.Context.XsiMemberExhibitionActivityYearly.Find(entity.MemberExhibitionYearlyId);
            XsiContext.Context.Entry(ExhibitorActivity).State = EntityState.Modified;

            if (!entity.MemberExhibitionYearlyId.IsDefault())
                ExhibitorActivity.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;

            if (!entity.ActivityId.IsDefault())
                ExhibitorActivity.ActivityId = entity.ActivityId;
        }
        public void Delete(XsiMemberExhibitionActivityYearly entity)
        {
            foreach (XsiMemberExhibitionActivityYearly ExhibitorActivity in Select(entity))
            {
                XsiMemberExhibitionActivityYearly aExhibitorActivity = XsiContext.Context.XsiMemberExhibitionActivityYearly.Find(ExhibitorActivity.MemberExhibitionYearlyId);
                XsiContext.Context.XsiMemberExhibitionActivityYearly.Remove(aExhibitorActivity);
            }
        }
        public void Delete(long itemId)
        {
            XsiMemberExhibitionActivityYearly aExhibitorActivity = XsiContext.Context.XsiMemberExhibitionActivityYearly.Where(i=>i.MemberExhibitionYearlyId==itemId).FirstOrDefault();
            XsiContext.Context.XsiMemberExhibitionActivityYearly.Remove(aExhibitorActivity);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
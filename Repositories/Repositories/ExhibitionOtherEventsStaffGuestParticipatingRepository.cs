﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionOtherEventsStaffGuestParticipatingRepository : IExhibitionOtherEventsStaffGuestParticipatingRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionOtherEventsStaffGuestParticipatingRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionOtherEventsStaffGuestParticipatingRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionOtherEventsStaffGuestParticipatingRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionOtherEventsStaffGuestParticipating GetById(long itemId)
        {
            //return XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipating.Find(itemId);
            return null;
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipating> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionOtherEventsStaffGuestParticipating> Select()
        {
            return XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipating.ToList();
        }
        //public XsiExhibitionOtherEventsStaffGuestParticipating GetByStaffGuestId(long itemId)
        //{
        //    return XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipating.Include("XsiExhibitionOtherEventsMemberApplicationYearly").Where(p => p.StaffGuestId == itemId).FirstOrDefault();
        //}
        public List<XsiExhibitionOtherEventsStaffGuestParticipating> Select(XsiExhibitionOtherEventsStaffGuestParticipating entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionOtherEventsStaffGuestParticipating>();

            if (!entity.StaffGuestId.IsDefault())
                predicate = predicate.And(p => p.StaffGuestId == entity.StaffGuestId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsUaeresident.IsDefault())
                predicate = predicate.And(p => p.IsUaeresident.Equals(entity.IsUaeresident));

            if (!entity.Uidno.IsDefault())
                predicate = predicate.And(p => p.Uidno.Equals(entity.Uidno));

            if (!entity.CountryUidno.IsDefault())
                predicate = predicate.And(p => p.CountryUidno.Equals(entity.CountryUidno));

            if (!entity.IsTravelDetails.IsDefault())
                predicate = predicate.And(p => p.IsTravelDetails.Equals(entity.IsTravelDetails));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.IsEmailSent.IsDefault())
                predicate = predicate.And(p => p.IsEmailSent.Equals(entity.IsEmailSent));

            if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.NameEn.IsDefault())
                predicate = predicate.And(p => p.NameEn.Contains(entity.NameEn));

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));

            if (!entity.NameAr.IsDefault())
                predicate = predicate.And(p => p.NameAr.Contains(entity.NameAr));

            if (!entity.Profession.IsDefault())
                predicate = predicate.And(p => p.Profession.Contains(entity.Profession));

            /*if (!entity.DOB.IsDefault())
                predicate = predicate.And(p => p.DOB == entity.DOB);

            if (!entity.POB.IsDefault())
                predicate = predicate.And(p => p.POB == entity.POB);*/

            if (!entity.Telephone.IsDefault())
                predicate = predicate.And(p => p.Telephone.Contains(entity.Telephone));

            if (!entity.Mobile.IsDefault())
                predicate = predicate.And(p => p.Mobile.Contains(entity.Mobile));

            if (!entity.TravelDetails.IsDefault())
                predicate = predicate.And(p => p.TravelDetails.Contains(entity.TravelDetails));

            if (!entity.NeedVisa.IsDefault())
                predicate = predicate.And(p => p.NeedVisa.Equals(entity.NeedVisa));

            if (!entity.ArrivalDate.IsDefault())
                predicate = predicate.And(p => p.ArrivalDate == entity.ArrivalDate);

            if (!entity.ArrivalAirportId.IsDefault())
                predicate = predicate.And(p => p.ArrivalAirportId == entity.ArrivalAirportId);

            if (!entity.ArrivalTerminalId.IsDefault())
                predicate = predicate.And(p => p.ArrivalTerminalId == entity.ArrivalTerminalId);

            if (!entity.DepartureDate.IsDefault())
                predicate = predicate.And(p => p.DepartureDate == entity.DepartureDate);

            if (!entity.Passport.IsDefault())
                predicate = predicate.And(p => p.Passport.Contains(entity.Passport));

            if (!entity.PlaceOfIssue.IsDefault())
                predicate = predicate.And(p => p.PlaceOfIssue.Contains(entity.PlaceOfIssue));

            if (!entity.DateOfIssue.IsDefault())
                predicate = predicate.And(p => p.DateOfIssue == entity.DateOfIssue);

            if (!entity.ExpiryDate.IsDefault())
                predicate = predicate.And(p => p.ExpiryDate == entity.ExpiryDate);

            if (!entity.PassportCopy.IsDefault())
                predicate = predicate.And(p => p.PassportCopy.Contains(entity.PassportCopy));

            if (!entity.Photo.IsDefault())
                predicate = predicate.And(p => p.Photo.Contains(entity.Photo));

            if (!entity.FlightTickets.IsDefault())
                predicate = predicate.And(p => p.FlightTickets.Contains(entity.FlightTickets));

            if (!entity.VisaCopy.IsDefault())
                predicate = predicate.And(p => p.VisaCopy.Contains(entity.VisaCopy));

            if (!entity.Invoice.IsDefault())
                predicate = predicate.And(p => p.Invoice.Contains(entity.Invoice));

            if (!entity.Notes.IsDefault())
                predicate = predicate.And(p => p.Notes.Contains(entity.Notes));

            if (!entity.VisaApplicationForm.IsDefault())
                predicate = predicate.And(p => p.VisaApplicationForm.Contains(entity.VisaApplicationForm));

            if (!entity.NoofVisaMonths.IsDefault())
                predicate = predicate.And(p => p.NoofVisaMonths.Equals(entity.NoofVisaMonths));

            if (!entity.GuestType.IsDefault())
                predicate = predicate.And(p => p.GuestType.Equals(entity.GuestType));

            if (!entity.GuestCategory.IsDefault())
                predicate = predicate.And(p => p.GuestCategory.Equals(entity.GuestCategory));

            if (!entity.ActivityDate.IsDefault())
                predicate = predicate.And(p => p.ActivityDate == entity.ActivityDate);

            if (!entity.PaidVisa.IsDefault())
                predicate = predicate.And(p => p.PaidVisa.Contains(entity.PaidVisa));

            if (!entity.PassportCopyTwo.IsDefault())
                predicate = predicate.And(p => p.PassportCopyTwo.Contains(entity.PassportCopyTwo));

            if (!entity.VisaType.IsDefault())
                predicate = predicate.And(p => p.VisaType.Equals(entity.VisaType));

            if (!entity.HealthInsurance.IsDefault())
                predicate = predicate.And(p => p.HealthInsurance.Contains(entity.HealthInsurance));

            if (!entity.HealthInsuranceTwo.IsDefault())
                predicate = predicate.And(p => p.HealthInsuranceTwo.Contains(entity.HealthInsuranceTwo));

            //if (!entity.StaffGuestAdminNote.IsDefault())
            //    predicate = predicate.And(p => p.StaffGuestAdminNote.Contains(entity.StaffGuestAdminNote));

            if (!entity.IsVisitedUae.IsDefault())
                predicate = predicate.And(p => p.IsVisitedUae.Equals(entity.IsVisitedUae));

            if (!entity.RejectedStatus.IsDefault())
                predicate = predicate.And(p => p.RejectedStatus.Equals(entity.RejectedStatus));

            if (!entity.MainGuestId.IsDefault())
                predicate = predicate.And(p => p.MainGuestId == entity.MainGuestId);

            if (!entity.MainGuest.IsDefault())
                predicate = predicate.And(p => p.MainGuest.Contains(entity.MainGuest));

            if (!entity.IsCompanion.IsDefault())
                predicate = predicate.And(p => p.IsCompanion.Equals(entity.IsCompanion));

            if (!entity.CompanionDescription.IsDefault())
                predicate = predicate.And(p => p.CompanionDescription.Contains(entity.CompanionDescription));

            if (!entity.IsFromPcregistration.IsDefault())
            {
                if (entity.IsFromPcregistration == "Y" || entity.IsFromPcregistration == "N")
                    predicate = predicate.And(p => p.IsFromPcregistration.Equals(entity.IsFromPcregistration));
            }


            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipating.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionOtherEventsStaffGuestParticipating Add(XsiExhibitionOtherEventsStaffGuestParticipating entity)
        {
            XsiExhibitionOtherEventsStaffGuestParticipating ExhibitionOtherEventsStaffGuestParticipating = new XsiExhibitionOtherEventsStaffGuestParticipating();

            ExhibitionOtherEventsStaffGuestParticipating.StaffGuestId = entity.StaffGuestId;
            //StaffGuest.ExhibitorId = entity.ExhibitorId;
            ExhibitionOtherEventsStaffGuestParticipating.ExhibitionId = entity.ExhibitionId;
            ExhibitionOtherEventsStaffGuestParticipating.CountryId = entity.CountryId;
            ExhibitionOtherEventsStaffGuestParticipating.IsActive = entity.IsActive;
            ExhibitionOtherEventsStaffGuestParticipating.IsUaeresident = entity.IsUaeresident;
            if (!entity.Uidno.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Uidno = entity.Uidno.Trim();
            if (!entity.CountryUidno.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.CountryUidno = entity.CountryUidno.Trim();
            ExhibitionOtherEventsStaffGuestParticipating.IsTravelDetails = entity.IsTravelDetails;
            ExhibitionOtherEventsStaffGuestParticipating.Status = entity.Status;
            ExhibitionOtherEventsStaffGuestParticipating.IsEmailSent = entity.IsEmailSent;
            ExhibitionOtherEventsStaffGuestParticipating.LanguageId = entity.LanguageId;
            if (!entity.NameEn.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.NameEn = entity.NameEn.Trim();
            if (!entity.LastName.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.LastName = entity.LastName.Trim();
            if (!entity.NameAr.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.NameAr = entity.NameAr.Trim();
            if (!entity.Profession.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Profession = entity.Profession.Trim();
            /*StaffGuest.DOB = entity.DOB;
            if (!entity.POB.IsDefault())
                StaffGuest.POB = entity.POB.Trim();*/
            if (!entity.Telephone.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Telephone = entity.Telephone.Trim();
            if (!entity.Mobile.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Mobile = entity.Mobile.Trim();
            if (!entity.TravelDetails.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.TravelDetails = entity.TravelDetails.Trim();
            if (!entity.NeedVisa.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.NeedVisa = entity.NeedVisa.Trim();
            ExhibitionOtherEventsStaffGuestParticipating.ArrivalDate = entity.ArrivalDate;
            ExhibitionOtherEventsStaffGuestParticipating.ArrivalAirportId = entity.ArrivalAirportId;
            ExhibitionOtherEventsStaffGuestParticipating.ArrivalTerminalId = entity.ArrivalTerminalId;
            ExhibitionOtherEventsStaffGuestParticipating.DepartureDate = entity.DepartureDate;
            if (!entity.Passport.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Passport = entity.Passport.Trim();
            if (!entity.PlaceOfIssue.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.PlaceOfIssue = entity.PlaceOfIssue.Trim();
            ExhibitionOtherEventsStaffGuestParticipating.DateOfIssue = entity.DateOfIssue;
            ExhibitionOtherEventsStaffGuestParticipating.ExpiryDate = entity.ExpiryDate;
            ExhibitionOtherEventsStaffGuestParticipating.PassportCopy = entity.PassportCopy;
            ExhibitionOtherEventsStaffGuestParticipating.Photo = entity.Photo;
            ExhibitionOtherEventsStaffGuestParticipating.FlightTickets = entity.FlightTickets;
            ExhibitionOtherEventsStaffGuestParticipating.VisaCopy = entity.VisaCopy;
            if (!entity.Invoice.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Invoice = entity.Invoice.Trim();
            if (!entity.Notes.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Notes = entity.Notes.Trim();
            if (!entity.VisaApplicationForm.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.VisaApplicationForm = entity.VisaApplicationForm.Trim();

            #region Guest Details
            ExhibitionOtherEventsStaffGuestParticipating.NoofVisaMonths = entity.NoofVisaMonths;
            ExhibitionOtherEventsStaffGuestParticipating.GuestType = entity.GuestType;
            ExhibitionOtherEventsStaffGuestParticipating.GuestCategory = entity.GuestCategory;
            ExhibitionOtherEventsStaffGuestParticipating.ActivityDate = entity.ActivityDate;
            ExhibitionOtherEventsStaffGuestParticipating.PaidVisa = entity.PaidVisa;
            ExhibitionOtherEventsStaffGuestParticipating.RepresentativeAdminNote = entity.RepresentativeAdminNote;
            #endregion

            ExhibitionOtherEventsStaffGuestParticipating.PassportCopyTwo = entity.PassportCopyTwo;
            ExhibitionOtherEventsStaffGuestParticipating.VisaType = entity.VisaType;
            ExhibitionOtherEventsStaffGuestParticipating.HealthInsurance = entity.HealthInsurance;
            ExhibitionOtherEventsStaffGuestParticipating.HealthInsuranceTwo = entity.HealthInsuranceTwo;
            ExhibitionOtherEventsStaffGuestParticipating.AddedBy = entity.AddedBy;
            ExhibitionOtherEventsStaffGuestParticipating.IsVisitedUae = entity.IsVisitedUae;
            ExhibitionOtherEventsStaffGuestParticipating.RejectedStatus = entity.RejectedStatus;

            if (!entity.MainGuestId.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.MainGuestId = entity.MainGuestId;

            if (!entity.IsFromPcregistration.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.IsFromPcregistration = entity.IsFromPcregistration;

            ExhibitionOtherEventsStaffGuestParticipating.MainGuest = entity.MainGuest;
            ExhibitionOtherEventsStaffGuestParticipating.IsCompanion = entity.IsCompanion;
            ExhibitionOtherEventsStaffGuestParticipating.CompanionDescription = entity.CompanionDescription;
            ExhibitionOtherEventsStaffGuestParticipating.CreatedOn = entity.CreatedOn;
            ExhibitionOtherEventsStaffGuestParticipating.CreatedBy = entity.CreatedBy;
            ExhibitionOtherEventsStaffGuestParticipating.ModifiedOn = entity.ModifiedOn;
            ExhibitionOtherEventsStaffGuestParticipating.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipating.Add(ExhibitionOtherEventsStaffGuestParticipating);
            SubmitChanges();
            return ExhibitionOtherEventsStaffGuestParticipating;

        }
        public void Update(XsiExhibitionOtherEventsStaffGuestParticipating entity)
        {
            //XsiExhibitionOtherEventsStaffGuestParticipating ExhibitionOtherEventsStaffGuestParticipating = XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipating.Find(entity.StaffGuestId); 
            XsiExhibitionOtherEventsStaffGuestParticipating ExhibitionOtherEventsStaffGuestParticipating = XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipating.Where(p => p.StaffGuestId == entity.StaffGuestId).FirstOrDefault();

            XsiContext.Context.Entry(ExhibitionOtherEventsStaffGuestParticipating).State = EntityState.Modified;

            if (!entity.ExhibitionId.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.ExhibitionId = entity.ExhibitionId;

            if (!entity.CountryId.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.CountryId = entity.CountryId;

            if (!entity.IsActive.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.IsActive = entity.IsActive;

            if (!entity.IsUaeresident.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.IsUaeresident = entity.IsUaeresident;

            if (!entity.Uidno.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Uidno = entity.Uidno.Trim();

            if (!entity.CountryUidno.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.CountryUidno = entity.CountryUidno.Trim();

            if (!entity.IsTravelDetails.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.IsTravelDetails = entity.IsTravelDetails;

            if (!entity.Status.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Status = entity.Status;

            if (!entity.IsEmailSent.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.IsEmailSent = entity.IsEmailSent;

            if (!entity.LanguageId.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.LanguageId = entity.LanguageId;

            if (!entity.NameEn.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.NameEn = entity.NameEn.Trim();

            if (!entity.LastName.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.LastName = entity.LastName.Trim();

            if (!entity.NameAr.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.NameAr = entity.NameAr.Trim();

            if (!entity.Profession.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Profession = entity.Profession.Trim();

            /*if (!entity.DOB.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.DOB = entity.DOB;

            if (!entity.POB.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.POB = entity.POB.Trim();*/

            if (!entity.Telephone.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Telephone = entity.Telephone.Trim();

            if (!entity.Mobile.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Mobile = entity.Mobile.Trim();

            if (!entity.TravelDetails.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.TravelDetails = entity.TravelDetails.Trim();

            if (!entity.NeedVisa.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.NeedVisa = entity.NeedVisa;

            if (!entity.ArrivalDate.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.ArrivalDate = entity.ArrivalDate;

            if (!entity.ArrivalAirportId.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.ArrivalAirportId = entity.ArrivalAirportId;

            if (!entity.ArrivalTerminalId.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.ArrivalTerminalId = entity.ArrivalTerminalId;

            if (!entity.DepartureDate.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.DepartureDate = entity.DepartureDate;

            if (!entity.Passport.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Passport = entity.Passport.Trim();

            if (!entity.PlaceOfIssue.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.PlaceOfIssue = entity.PlaceOfIssue.Trim();

            if (!entity.DateOfIssue.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.DateOfIssue = entity.DateOfIssue;

            if (!entity.ExpiryDate.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.ExpiryDate = entity.ExpiryDate;

            if (!entity.PassportCopy.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.PassportCopy = entity.PassportCopy;

            if (!entity.Photo.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Photo = entity.Photo;

            if (!entity.FlightTickets.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.FlightTickets = entity.FlightTickets;

            if (!entity.VisaCopy.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.VisaCopy = entity.VisaCopy;

            if (!entity.Invoice.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Invoice = entity.Invoice.Trim();

            if (!entity.Notes.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.Notes = entity.Notes.Trim();

            if (!entity.VisaApplicationForm.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.VisaApplicationForm = entity.VisaApplicationForm.Trim();

            if (!entity.NoofVisaMonths.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.NoofVisaMonths = entity.NoofVisaMonths;

            if (!entity.GuestType.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.GuestType = entity.GuestType;

            if (!entity.GuestCategory.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.GuestCategory = entity.GuestCategory;

            if (!entity.ActivityDate.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.ActivityDate = entity.ActivityDate;

            if (!entity.PaidVisa.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.PaidVisa = entity.PaidVisa;

            if (!entity.RepresentativeAdminNote.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.RepresentativeAdminNote = entity.RepresentativeAdminNote.Trim();

            if (!entity.PassportCopyTwo.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.PassportCopyTwo = entity.PassportCopyTwo.Trim();

            if (!entity.VisaType.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.VisaType = entity.VisaType.Trim();

            if (!entity.HealthInsurance.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.HealthInsurance = entity.HealthInsurance.Trim();

            if (!entity.HealthInsuranceTwo.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.HealthInsuranceTwo = entity.HealthInsuranceTwo.Trim();

            if (!entity.IsVisitedUae.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.IsVisitedUae = entity.IsVisitedUae;

            if (!entity.RejectedStatus.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.RejectedStatus = entity.RejectedStatus;

            if (!entity.MainGuestId.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.MainGuestId = entity.MainGuestId;

            if (!entity.MainGuest.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.MainGuest = entity.MainGuest;

            if (!entity.IsCompanion.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.IsCompanion = entity.IsCompanion;

            if (!entity.CompanionDescription.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.CompanionDescription = entity.CompanionDescription;

            if (!entity.AddedBy.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.AddedBy = entity.AddedBy;

            if (!entity.StaffGuestId.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.StaffGuestId = entity.StaffGuestId;

            if (!entity.IsActive.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.IsActive = entity.IsActive;

            if (!entity.IsFromPcregistration.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.IsFromPcregistration = entity.IsFromPcregistration;

            if (!entity.CreatedBy.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.CreatedBy = entity.CreatedBy;

            if (!entity.CreatedOn.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.CreatedOn = entity.CreatedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionOtherEventsStaffGuestParticipating.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiExhibitionOtherEventsStaffGuestParticipating entity)
        {
            foreach (XsiExhibitionOtherEventsStaffGuestParticipating ExhibitionOtherEventsStaffGuestParticipating in Select(entity))
            {
                XsiExhibitionOtherEventsStaffGuestParticipating aExhibitionOtherEventsStaffGuestParticipating = XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipating.Find(ExhibitionOtherEventsStaffGuestParticipating.StaffGuestId);
                XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipating.Remove(aExhibitionOtherEventsStaffGuestParticipating);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionOtherEventsStaffGuestParticipating aExhibitionOtherEventsStaffGuestParticipating = XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipating.Where(p => p.StaffGuestId == itemId).OrderByDescending(o => o.StaffGuestId).FirstOrDefault();
            XsiContext.Context.XsiExhibitionOtherEventsStaffGuestParticipating.Remove(aExhibitionOtherEventsStaffGuestParticipating);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
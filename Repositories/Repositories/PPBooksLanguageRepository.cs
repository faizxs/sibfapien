﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PPBooksLanguageRepository : IPPBooksLanguageRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PPBooksLanguageRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PPBooksLanguageRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PPBooksLanguageRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionPpbooksLanguage GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionPpbooksLanguage.Find(itemId);
        }
        public List<XsiExhibitionPpbooksLanguage> GetByGroupId(long groupId)
        {
            return null;
        }
        public List<XsiExhibitionPpbooksLanguage> Select()
        {
            return XsiContext.Context.XsiExhibitionPpbooksLanguage.ToList();
        }
        public List<XsiExhibitionPpbooksLanguage> Select(XsiExhibitionPpbooksLanguage entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionPpbooksLanguage>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);


            if (!entity.PpbookId.IsDefault())
                predicate = predicate.And(p => p.PpbookId == entity.PpbookId);

            if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            return XsiContext.Context.XsiExhibitionPpbooksLanguage.AsExpandable().Where(predicate).ToList();
        }
        public XsiExhibitionPpbooksLanguage Add(XsiExhibitionPpbooksLanguage entity)
        {
            XsiExhibitionPpbooksLanguage PPBooksLanguage = new XsiExhibitionPpbooksLanguage();
            PPBooksLanguage.PpbookId = entity.PpbookId;
            PPBooksLanguage.LanguageId = entity.LanguageId;
            XsiContext.Context.XsiExhibitionPpbooksLanguage.Add(PPBooksLanguage);
            SubmitChanges();
            return PPBooksLanguage;

        }
        public void Update(XsiExhibitionPpbooksLanguage entity)
        {
            XsiExhibitionPpbooksLanguage PPBooksLanguage = XsiContext.Context.XsiExhibitionPpbooksLanguage.Find(entity.ItemId);
            XsiContext.Context.Entry(PPBooksLanguage).State = EntityState.Modified;

            if (!entity.PpbookId.IsDefault())
                PPBooksLanguage.PpbookId = entity.PpbookId;
            if (!entity.LanguageId.IsDefault())
                PPBooksLanguage.LanguageId = entity.LanguageId;
        }
        public void Delete(XsiExhibitionPpbooksLanguage entity)
        {
            foreach (XsiExhibitionPpbooksLanguage PPBooksLanguage in Select(entity))
            {
                XsiExhibitionPpbooksLanguage aPPBooksLanguage = XsiContext.Context.XsiExhibitionPpbooksLanguage.Find(PPBooksLanguage.ItemId);
                XsiContext.Context.XsiExhibitionPpbooksLanguage.Remove(aPPBooksLanguage);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionPpbooksLanguage aPPBooksLanguage = XsiContext.Context.XsiExhibitionPpbooksLanguage.Find(itemId);
            XsiContext.Context.XsiExhibitionPpbooksLanguage.Remove(aPPBooksLanguage);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionFurnitureItemOrderDetailRepository : IExhibitionFurnitureItemOrderDetailsRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionFurnitureItemOrderDetailRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionFurnitureItemOrderDetailRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionFurnitureItemOrderDetailRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionFurnitureItemOrderDetails GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionFurnitureItemOrderDetails.Find(itemId);
        }

        public List<XsiExhibitionFurnitureItemOrderDetails> Select()
        {
            return XsiContext.Context.XsiExhibitionFurnitureItemOrderDetails.ToList();
        }

        public List<XsiExhibitionFurnitureItemOrderDetails> Select(XsiExhibitionFurnitureItemOrderDetails entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionFurnitureItemOrderDetails>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.OrderId.IsDefault())
                predicate = predicate.And(p => p.OrderId == entity.OrderId);

            if (!entity.FurnitureItemId.IsDefault())
                predicate = predicate.And(p => p.FurnitureItemId == entity.FurnitureItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Quantity.IsDefault())
                predicate = predicate.And(p => p.Quantity == entity.Quantity);

            if (!entity.Price.IsDefault())
                predicate = predicate.And(p => p.Price == entity.Price);

            if (!entity.Total.IsDefault())
                predicate = predicate.And(p => p.Total == entity.Total);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionFurnitureItemOrderDetails.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionFurnitureItemOrderDetails Add(XsiExhibitionFurnitureItemOrderDetails entity)
        {
            XsiExhibitionFurnitureItemOrderDetails ExhibitionFurnitureItemOrderDetail = new XsiExhibitionFurnitureItemOrderDetails();

            if (!entity.OrderId.IsDefault())
                ExhibitionFurnitureItemOrderDetail.OrderId = entity.OrderId;

            if (!entity.FurnitureItemId.IsDefault())
                ExhibitionFurnitureItemOrderDetail.FurnitureItemId = entity.FurnitureItemId;

            if (!entity.IsActive.IsDefault())
                ExhibitionFurnitureItemOrderDetail.IsActive = entity.IsActive;

            if (!entity.Quantity.IsDefault())
                ExhibitionFurnitureItemOrderDetail.Quantity = entity.Quantity;

            if (!entity.Price.IsDefault())
                ExhibitionFurnitureItemOrderDetail.Price = entity.Price;

            if (!entity.Total.IsDefault())
                ExhibitionFurnitureItemOrderDetail.Total = entity.Total;

            ExhibitionFurnitureItemOrderDetail.CreatedOn = entity.CreatedOn;
            ExhibitionFurnitureItemOrderDetail.CreatedBy = entity.CreatedBy;
            ExhibitionFurnitureItemOrderDetail.ModifiedOn = entity.ModifiedOn;
            ExhibitionFurnitureItemOrderDetail.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiExhibitionFurnitureItemOrderDetails.Add(ExhibitionFurnitureItemOrderDetail);
            SubmitChanges();
            return ExhibitionFurnitureItemOrderDetail;

        }
        public void Update(XsiExhibitionFurnitureItemOrderDetails entity)
        {
            XsiExhibitionFurnitureItemOrderDetails ExhibitionFurnitureItemOrderDetail = XsiContext.Context.XsiExhibitionFurnitureItemOrderDetails.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionFurnitureItemOrderDetail).State = EntityState.Modified;

            if (!entity.OrderId.IsDefault())
                ExhibitionFurnitureItemOrderDetail.OrderId = entity.OrderId;

            if (!entity.FurnitureItemId.IsDefault())
                ExhibitionFurnitureItemOrderDetail.FurnitureItemId = entity.FurnitureItemId;

            if (!entity.IsActive.IsDefault())
                ExhibitionFurnitureItemOrderDetail.IsActive = entity.IsActive;

            if (!entity.Quantity.IsDefault())
                ExhibitionFurnitureItemOrderDetail.Quantity = entity.Quantity;

            if (!entity.Price.IsDefault())
                ExhibitionFurnitureItemOrderDetail.Price = entity.Price;

            if (!entity.Total.IsDefault())
                ExhibitionFurnitureItemOrderDetail.Total = entity.Total;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionFurnitureItemOrderDetail.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionFurnitureItemOrderDetail.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionFurnitureItemOrderDetails entity)
        {
            foreach (XsiExhibitionFurnitureItemOrderDetails ExhibitionFurnitureItemOrderDetail in Select(entity))
            {
                XsiExhibitionFurnitureItemOrderDetails aExhibitionFurnitureItemOrderDetail = XsiContext.Context.XsiExhibitionFurnitureItemOrderDetails.Find(ExhibitionFurnitureItemOrderDetail.ItemId);
                XsiContext.Context.XsiExhibitionFurnitureItemOrderDetails.Remove(aExhibitionFurnitureItemOrderDetail);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionFurnitureItemOrderDetails aExhibitionFurnitureItemOrderDetail = XsiContext.Context.XsiExhibitionFurnitureItemOrderDetails.Find(itemId);
            XsiContext.Context.XsiExhibitionFurnitureItemOrderDetails.Remove(aExhibitionFurnitureItemOrderDetail);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
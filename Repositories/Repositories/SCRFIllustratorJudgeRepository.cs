﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFIllustratorJudgeRepository : ISCRFIllustratorJudgeRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFIllustratorJudgeRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFIllustratorJudgeRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFIllustratorJudgeRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfillustratorJudge GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfillustratorJudge.Find(itemId);
        }

        public List<XsiScrfillustratorJudge> Select()
        {
            return XsiContext.Context.XsiScrfillustratorJudge.ToList();
        }
        public List<XsiScrfillustratorJudge> Select(XsiScrfillustratorJudge entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfillustratorJudge>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Contains(entity.IsActive));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Equals(entity.Email));

            if (!entity.Password.IsDefault())
                predicate = predicate.And(p => p.Password.Equals(entity.Password));

            if (!entity.FirstName.IsDefault())
                predicate = predicate.And(p => p.FirstName.Contains(entity.FirstName));

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));

            if (!entity.Avatar.IsDefault())
                predicate = predicate.And(p => p.Avatar.Contains(entity.Avatar));

            if (!entity.Dob.IsDefault())
                predicate = predicate.And(p => p.Dob == entity.Dob);

            if (!entity.Designation.IsDefault())
                predicate = predicate.And(p => p.Designation.Contains(entity.Designation));

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.SortOrder.IsDefault())
                predicate = predicate.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.LanguagePrefered.IsDefault())
                predicate = predicate.And(p => p.LanguagePrefered == entity.LanguagePrefered);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            return XsiContext.Context.XsiScrfillustratorJudge.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiScrfillustratorJudge> SelectOr(XsiScrfillustratorJudge entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfillustratorJudge>();
            var Outer = PredicateBuilder.True<XsiScrfillustratorJudge>();

            if (!entity.FirstName.IsDefault())
            {
                string strSearchText = entity.FirstName;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);
                Inner = Inner.Or(p => p.LastName.Contains(searchKeys.str1) || p.LastName.Contains(searchKeys.str2) || p.LastName.Contains(searchKeys.str3) || p.LastName.Contains(searchKeys.str4) || p.LastName.Contains(searchKeys.str5)
                    || p.LastName.Contains(searchKeys.str6) || p.LastName.Contains(searchKeys.str7) || p.LastName.Contains(searchKeys.str8) || p.LastName.Contains(searchKeys.str9) || p.LastName.Contains(searchKeys.str10)
                    || p.LastName.Contains(searchKeys.str11) || p.LastName.Contains(searchKeys.str12) || p.LastName.Contains(searchKeys.str13)
                    || p.LastName.Contains(searchKeys.str14) || p.LastName.Contains(searchKeys.str15) || p.LastName.Contains(searchKeys.str16) || p.LastName.Contains(searchKeys.str17)
                    );
                Inner = Inner.Or(p => (p.FirstName + " " + p.LastName).Contains(strSearchText) || p.FirstName.Contains(searchKeys.str1) || p.FirstName.Contains(searchKeys.str2) || p.FirstName.Contains(searchKeys.str3) || p.FirstName.Contains(searchKeys.str4) || p.FirstName.Contains(searchKeys.str5)
                    || p.FirstName.Contains(searchKeys.str6) || p.FirstName.Contains(searchKeys.str7) || p.FirstName.Contains(searchKeys.str8) || p.FirstName.Contains(searchKeys.str9) || p.FirstName.Contains(searchKeys.str10)
                    || p.FirstName.Contains(searchKeys.str11) || p.FirstName.Contains(searchKeys.str12) || p.FirstName.Contains(searchKeys.str13)
                    || p.FirstName.Contains(searchKeys.str14) || p.FirstName.Contains(searchKeys.str15) || p.FirstName.Contains(searchKeys.str16) || p.FirstName.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.FirstName.IsDefault())
                {
                    string strSearchText = entity.FirstName.ToLower();
                    Inner = Inner.Or(p => (p.FirstName + " " + p.LastName).Contains(strSearchText) || p.FirstName.ToLower().Contains(strSearchText) || p.LastName.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Contains(entity.IsActive));

            if (!entity.Email.IsDefault())
                Outer = Outer.And(p => p.Email.Equals(entity.Email));

            if (!entity.Password.IsDefault())
                Outer = Outer.And(p => p.Password.Equals(entity.Password));

            if (!entity.Avatar.IsDefault())
                Outer = Outer.And(p => p.Avatar.Contains(entity.Avatar));

            if (!entity.Dob.IsDefault())
                Outer = Outer.And(p => p.Dob == entity.Dob);

            if (!entity.Designation.IsDefault())
                Outer = Outer.And(p => p.Designation.Contains(entity.Designation));

            if (!entity.Overview.IsDefault())
                Outer = Outer.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.SortOrder.IsDefault())
                Outer = Outer.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.LanguagePrefered.IsDefault())
                Outer = Outer.And(p => p.LanguagePrefered == entity.LanguagePrefered);
            
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Email.IsDefault())
                Outer = Outer.And(p => p.Email.Equals(entity.Email));

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiScrfillustratorJudge.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiScrfillustratorJudge Add(XsiScrfillustratorJudge entity)
        {
            XsiScrfillustratorJudge SCRFIllustratorJudge = new XsiScrfillustratorJudge();
            SCRFIllustratorJudge.IsActive = entity.IsActive;
            if (!entity.Email.IsDefault())
                SCRFIllustratorJudge.Email = entity.Email.Trim();
            if (!entity.Password.IsDefault())
                SCRFIllustratorJudge.Password = entity.Password.Trim();
            if (!entity.FirstName.IsDefault())
                SCRFIllustratorJudge.FirstName = entity.FirstName.Trim();
            if (!entity.LastName.IsDefault())
            SCRFIllustratorJudge.LastName = entity.LastName.Trim();
            SCRFIllustratorJudge.Avatar = entity.Avatar;
            SCRFIllustratorJudge.Dob = entity.Dob;
            if (!entity.Designation.IsDefault())
                SCRFIllustratorJudge.Designation = entity.Designation.Trim();
            if (!entity.Overview.IsDefault())
            SCRFIllustratorJudge.Overview = entity.Overview.Trim();
            SCRFIllustratorJudge.SortOrder = entity.SortOrder;
            SCRFIllustratorJudge.LanguagePrefered = entity.LanguagePrefered;
            SCRFIllustratorJudge.CreatedOn = entity.CreatedOn;
            SCRFIllustratorJudge.CreatedBy = entity.CreatedBy;
            SCRFIllustratorJudge.ModifiedBy = entity.ModifiedBy;
            SCRFIllustratorJudge.ModifiedOn = entity.ModifiedOn;

            XsiContext.Context.XsiScrfillustratorJudge.Add(SCRFIllustratorJudge);
            SubmitChanges();
            return SCRFIllustratorJudge;

        }
        public void Update(XsiScrfillustratorJudge entity)
        {
            XsiScrfillustratorJudge SCRFIllustratorJudge = XsiContext.Context.XsiScrfillustratorJudge.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFIllustratorJudge).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                SCRFIllustratorJudge.IsActive = entity.IsActive;

            if (!entity.Email.IsDefault())
                SCRFIllustratorJudge.Email = entity.Email.Trim();

            if (!entity.Password.IsDefault())
                SCRFIllustratorJudge.Password = entity.Password.Trim();

            if (!entity.FirstName.IsDefault())
                SCRFIllustratorJudge.FirstName = entity.FirstName.Trim();

            if (!entity.LastName.IsDefault())
                SCRFIllustratorJudge.LastName = entity.LastName.Trim();

            if (!entity.Dob.IsDefault())
                SCRFIllustratorJudge.Dob = entity.Dob;

            if (!entity.Avatar.IsDefault())
                SCRFIllustratorJudge.Avatar = entity.Avatar;

            if (!entity.Designation.IsDefault())
                SCRFIllustratorJudge.Designation = entity.Designation.Trim();

            if (!entity.Overview.IsDefault())
                SCRFIllustratorJudge.Overview = entity.Overview.Trim();

            if (!entity.SortOrder.IsDefault())
                SCRFIllustratorJudge.SortOrder = entity.SortOrder;

            if (!entity.LanguagePrefered.IsDefault())
                SCRFIllustratorJudge.LanguagePrefered = entity.LanguagePrefered;

            if (!entity.NationalityId.IsDefault())
                SCRFIllustratorJudge.NationalityId = entity.NationalityId;

            if (!entity.CountryId.IsDefault())
                SCRFIllustratorJudge.CountryId = entity.CountryId;

            if (!entity.ModifiedBy.IsDefault())
                SCRFIllustratorJudge.ModifiedBy = entity.ModifiedBy;

            if (!entity.ModifiedOn.IsDefault())
                SCRFIllustratorJudge.ModifiedOn = entity.ModifiedOn;

        }
        public void Delete(XsiScrfillustratorJudge entity)
        {
            foreach (XsiScrfillustratorJudge SCRFIllustratorJudge in Select(entity))
            {
                XsiScrfillustratorJudge aSCRFIllustratorJudge = XsiContext.Context.XsiScrfillustratorJudge.Find(SCRFIllustratorJudge.ItemId);
                XsiContext.Context.XsiScrfillustratorJudge.Remove(aSCRFIllustratorJudge);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfillustratorJudge aSCRFIllustratorJudge = XsiContext.Context.XsiScrfillustratorJudge.Find(itemId);
            XsiContext.Context.XsiScrfillustratorJudge.Remove(aSCRFIllustratorJudge);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
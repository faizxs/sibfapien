﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class CareerDepartmentRepository : ICareerDepartmentRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public CareerDepartmentRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public CareerDepartmentRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public CareerDepartmentRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiCareerDepartment GetById(long itemId)
        {
            return XsiContext.Context.XsiCareerDepartment.Find(itemId);
        }
        
        public List<XsiCareerDepartment> Select()
        {
            return XsiContext.Context.XsiCareerDepartment.ToList();
        }
        public List<XsiCareerDepartment> Select(XsiCareerDepartment entity)
        {
            var predicate = PredicateBuilder.True<XsiCareerDepartment>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));
             
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiCareerDepartment.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiCareerDepartment> SelectOr(XsiCareerDepartment entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiCareerDepartment>();
            var Outer = PredicateBuilder.True<XsiCareerDepartment>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
             
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiCareerDepartment.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiCareerDepartment Add(XsiCareerDepartment entity)
        {
            XsiCareerDepartment CareerDepartment = new XsiCareerDepartment();
            
            if (!entity.Title.IsDefault())
            CareerDepartment.Title = entity.Title.Trim();
            CareerDepartment.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerDepartment.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerDepartment.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            CareerDepartment.CreatedOn = entity.CreatedOn;
            CareerDepartment.CreatedBy = entity.CreatedBy;
            CareerDepartment.ModifiedOn = entity.ModifiedOn;
            CareerDepartment.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiCareerDepartment.Add(CareerDepartment);
            SubmitChanges();
            return CareerDepartment;

        }
        public void Update(XsiCareerDepartment entity)
        {
            XsiCareerDepartment CareerDepartment = XsiContext.Context.XsiCareerDepartment.Find(entity.ItemId);
            XsiContext.Context.Entry(CareerDepartment).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                CareerDepartment.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                CareerDepartment.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerDepartment.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerDepartment.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                CareerDepartment.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                CareerDepartment.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiCareerDepartment entity)
        {
            foreach (XsiCareerDepartment CareerDepartment in Select(entity))
            {
                XsiCareerDepartment aCareerDepartment = XsiContext.Context.XsiCareerDepartment.Find(CareerDepartment.ItemId);
                XsiContext.Context.XsiCareerDepartment.Remove(aCareerDepartment);
            }
        }
        public void Delete(long itemId)
        {
            XsiCareerDepartment aCareerDepartment = XsiContext.Context.XsiCareerDepartment.Find(itemId);
            XsiContext.Context.XsiCareerDepartment.Remove(aCareerDepartment);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
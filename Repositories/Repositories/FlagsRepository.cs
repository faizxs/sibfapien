﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class FlagRepository : IFlagRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public FlagRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public FlagRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public FlagRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiFlags GetById(long itemId)
        {
            return XsiContext.Context.XsiFlags.Find(itemId);
        }

        public List<XsiFlags> Select()
        {
            return XsiContext.Context.XsiFlags.ToList();
        }
        public List<XsiFlags> Select(XsiFlags entity)
        {
            var predicate = PredicateBuilder.True<XsiFlags>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.FlagName.IsDefault())
                predicate = predicate.And(p => p.FlagName.Contains(entity.FlagName));

            if (!entity.FlagModuleIdentifier.IsDefault())
                predicate = predicate.And(p => p.FlagModuleIdentifier.Equals(entity.FlagModuleIdentifier));

            if (!entity.FlagType.IsDefault())
                predicate = predicate.And(p => p.FlagType.Equals(entity.FlagType));


            return XsiContext.Context.XsiFlags.AsExpandable().Where(predicate).ToList();
        }

        public XsiFlags Add(XsiFlags entity)
        {
            XsiFlags Flag = new XsiFlags();
            if (!entity.FlagName.IsDefault())
                Flag.FlagName = entity.FlagName.Trim();
            if (!entity.FlagModuleIdentifier.IsDefault())
            Flag.FlagModuleIdentifier = entity.FlagModuleIdentifier.Trim();
            Flag.FlagType = entity.FlagType;

            XsiContext.Context.XsiFlags.Add(Flag);
            SubmitChanges();
            return Flag;

        }
        public void Update(XsiFlags entity)
        {
            XsiFlags Flag = XsiContext.Context.XsiFlags.Find(entity.ItemId);
            XsiContext.Context.Entry(Flag).State = EntityState.Modified;

            if (!entity.FlagName.IsDefault())
                Flag.FlagName = entity.FlagName.Trim();

            if (!entity.FlagModuleIdentifier.IsDefault())
                Flag.FlagModuleIdentifier = entity.FlagModuleIdentifier.Trim();

            if (!entity.FlagType.IsDefault())
                Flag.FlagType = entity.FlagType;
        }
        public void Delete(XsiFlags entity)
        {
            foreach (XsiFlags Flag in Select(entity))
            {
                XsiFlags aFlag = XsiContext.Context.XsiFlags.Find(Flag.ItemId);
                XsiContext.Context.XsiFlags.Remove(aFlag);
            }
        }
        public void Delete(long itemId)
        {
            XsiFlags aFlag = XsiContext.Context.XsiFlags.Find(itemId);
            XsiContext.Context.XsiFlags.Remove(aFlag);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
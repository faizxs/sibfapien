﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class StaffGuestCategoryRepository : IStaffGuestCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public StaffGuestCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public StaffGuestCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public StaffGuestCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiStaffGuestCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiStaffGuestCategory.Find(itemId);
        }

        public List<XsiStaffGuestCategory> Select()
        {
            return XsiContext.Context.XsiStaffGuestCategory.ToList();
        }
        public List<XsiStaffGuestCategory> Select(XsiStaffGuestCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiStaffGuestCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.ToLower().Contains(entity.TitleAr));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiStaffGuestCategory.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiStaffGuestCategory> SelectOr(XsiStaffGuestCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiStaffGuestCategory>();
            var Outer = PredicateBuilder.True<XsiStaffGuestCategory>();

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.TitleAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.TitleAr.Contains(searchKeys.str1) || p.TitleAr.Contains(searchKeys.str2) || p.TitleAr.Contains(searchKeys.str3) || p.TitleAr.Contains(searchKeys.str4) || p.TitleAr.Contains(searchKeys.str5)
                    || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                    || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                    || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsActiveAr.IsDefault())
                Outer = Outer.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiStaffGuestCategory.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public XsiStaffGuestCategory Add(XsiStaffGuestCategory entity)
        {
            XsiStaffGuestCategory StaffGuestCategory = new XsiStaffGuestCategory();
            if (!entity.Title.IsDefault())
                StaffGuestCategory.Title = entity.Title.Trim();
            StaffGuestCategory.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                StaffGuestCategory.TitleAr = entity.TitleAr.Trim();
            StaffGuestCategory.IsActiveAr = entity.IsActiveAr;

            StaffGuestCategory.CreatedOn = entity.CreatedOn;
            StaffGuestCategory.CreatedBy = entity.CreatedBy;
            StaffGuestCategory.ModifiedOn = entity.ModifiedOn;
            StaffGuestCategory.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiStaffGuestCategory.Add(StaffGuestCategory);
            SubmitChanges();
            return StaffGuestCategory;

        }
        public void Update(XsiStaffGuestCategory entity)
        {
            XsiStaffGuestCategory StaffGuestCategory = XsiContext.Context.XsiStaffGuestCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(StaffGuestCategory).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                StaffGuestCategory.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                StaffGuestCategory.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                StaffGuestCategory.TitleAr = entity.TitleAr.Trim();

            if (!entity.IsActiveAr.IsDefault())
                StaffGuestCategory.IsActiveAr = entity.IsActiveAr;

            if (!entity.ModifiedOn.IsDefault())
                StaffGuestCategory.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                StaffGuestCategory.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiStaffGuestCategory entity)
        {
            foreach (XsiStaffGuestCategory StaffGuestCategory in Select(entity))
            {
                XsiStaffGuestCategory aStaffGuestCategory = XsiContext.Context.XsiStaffGuestCategory.Find(StaffGuestCategory.ItemId);
                XsiContext.Context.XsiStaffGuestCategory.Remove(aStaffGuestCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiStaffGuestCategory aStaffGuestCategory = XsiContext.Context.XsiStaffGuestCategory.Find(itemId);
            XsiContext.Context.XsiStaffGuestCategory.Remove(aStaffGuestCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
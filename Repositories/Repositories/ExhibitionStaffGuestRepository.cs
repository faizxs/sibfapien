﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionStaffGuestRepository : IExhibitionStaffGuestRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionStaffGuestRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionStaffGuestRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionStaffGuestRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionStaffGuest GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionStaffGuest.Find(itemId);
        }
          
        public List<XsiExhibitionStaffGuest> Select()
        {
            return XsiContext.Context.XsiExhibitionStaffGuest.ToList();
        }
        public List<XsiExhibitionStaffGuest> Select(XsiExhibitionStaffGuest entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionStaffGuest>();
            if (!entity.StaffGuestId.IsDefault())
                predicate = predicate.And(p => p.StaffGuestId == entity.StaffGuestId);

            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);

            if (!entity.NameEn.IsDefault())
                predicate = predicate.And(p => p.NameEn == entity.NameEn);

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName == entity.LastName);

            if (!entity.NameAr.IsDefault())
                predicate = predicate.And(p => p.NameAr == entity.NameAr);

            if (!entity.StaffGuestId.IsDefault())
                predicate = predicate.And(p => p.StaffGuestId == entity.StaffGuestId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);
             
             
            return XsiContext.Context.XsiExhibitionStaffGuest.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionStaffGuest Add(XsiExhibitionStaffGuest entity)
        {
            XsiExhibitionStaffGuest ExhibitionStaffGuest = new XsiExhibitionStaffGuest();
            if (!entity.NameEn.IsDefault())
                ExhibitionStaffGuest.NameEn = entity.NameEn.Trim();
            if (!entity.LastName.IsDefault())
                ExhibitionStaffGuest.LastName = entity.LastName.Trim();
            if (!entity.NameAr.IsDefault())
                ExhibitionStaffGuest.NameAr = entity.NameAr.Trim();
            if (entity.Dob != null)
                ExhibitionStaffGuest.Dob = entity.Dob;
            ExhibitionStaffGuest.PlaceOfBirth = entity.PlaceOfBirth;
            ExhibitionStaffGuest.MemberId = entity.MemberId;
            ExhibitionStaffGuest.StaffGuestId = entity.StaffGuestId;
            ExhibitionStaffGuest.IsActive = entity.IsActive;
            ExhibitionStaffGuest.CreatedBy = entity.CreatedBy;
            ExhibitionStaffGuest.CreatedOn = entity.CreatedOn;
            ExhibitionStaffGuest.ModifiedBy = entity.ModifiedBy;
            ExhibitionStaffGuest.ModifiedOn = entity.ModifiedOn;

            XsiContext.Context.XsiExhibitionStaffGuest.Add(ExhibitionStaffGuest);
            SubmitChanges();
            return ExhibitionStaffGuest;

        }
        public void Update(XsiExhibitionStaffGuest entity)
        {
            //XsiExhibitionStaffGuest ExhibitionStaffGuest = XsiContext.Context.XsiExhibitionStaffGuest.Find(entity.StaffGuestId);
            XsiExhibitionStaffGuest ExhibitionStaffGuest = XsiContext.Context.XsiExhibitionStaffGuest.Where(p=>p.StaffGuestId== entity.StaffGuestId).FirstOrDefault();
            XsiContext.Context.Entry(ExhibitionStaffGuest).State = EntityState.Modified;
            if (!entity.NameEn.IsDefault())
                ExhibitionStaffGuest.NameEn = entity.NameEn.Trim();

            if (!entity.LastName.IsDefault())
                ExhibitionStaffGuest.LastName = entity.LastName.Trim();

            if (!entity.NameAr.IsDefault())
                ExhibitionStaffGuest.NameAr = entity.NameAr.Trim();

            if (!entity.Dob.IsDefault())
                ExhibitionStaffGuest.Dob = entity.Dob;

            if (!entity.PlaceOfBirth.IsDefault())
                ExhibitionStaffGuest.PlaceOfBirth = entity.PlaceOfBirth;

            if (!entity.MemberId.IsDefault())
                ExhibitionStaffGuest.MemberId = entity.MemberId;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionStaffGuest.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionStaffGuest.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionStaffGuest entity)
        {
            foreach (XsiExhibitionStaffGuest ExhibitionStaffGuest in Select(entity))
            {
                XsiExhibitionStaffGuest aExhibitionStaffGuest = XsiContext.Context.XsiExhibitionStaffGuest.Find(ExhibitionStaffGuest.StaffGuestId);
                XsiContext.Context.XsiExhibitionStaffGuest.Remove(aExhibitionStaffGuest);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionStaffGuest aExhibitionStaffGuest = XsiContext.Context.XsiExhibitionStaffGuest.Find(itemId);
            XsiContext.Context.XsiExhibitionStaffGuest.Remove(aExhibitionStaffGuest);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
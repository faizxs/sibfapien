﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ActivitiesBannerRepository : IActivitiesBannerRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ActivitiesBannerRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ActivitiesBannerRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ActivitiesBannerRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiActivitiesBanner GetById(long itemId)
        {
            return XsiContext.Context.XsiActivitiesBanner.Find(itemId);
        }

        public List<XsiActivitiesBanner> Select()
        {
            return XsiContext.Context.XsiActivitiesBanner.ToList();
        }
        public List<XsiActivitiesBanner> Select(XsiActivitiesBanner entity)
        {
            var predicate = PredicateBuilder.True<XsiActivitiesBanner>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.SortOrder.IsDefault())
                predicate = predicate.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));
            if (!entity.HomeBanner.IsDefault())
                predicate = predicate.And(p => p.HomeBanner.Equals(entity.HomeBanner));
            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
            return XsiContext.Context.XsiActivitiesBanner.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiActivitiesBanner> SelectOr(XsiActivitiesBanner entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiActivitiesBanner>();
            var Outer = PredicateBuilder.True<XsiActivitiesBanner>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiActivitiesBanner.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiActivitiesBanner Add(XsiActivitiesBanner entity)
        {
            XsiActivitiesBanner ActivitiesBanner = new XsiActivitiesBanner();
            if (!entity.Title.IsDefault())
                ActivitiesBanner.Title = entity.Title.Trim();
            ActivitiesBanner.HomeBanner = entity.HomeBanner;
            if (!entity.Url.IsDefault())
                ActivitiesBanner.Url = entity.Url.Trim();
            ActivitiesBanner.IsActive = entity.IsActive;
            ActivitiesBanner.CreatedOn = entity.CreatedOn;
            ActivitiesBanner.CreatedBy = entity.CreatedBy;
            ActivitiesBanner.ModifiedOn = entity.ModifiedOn;
            ActivitiesBanner.ModifiedBy = entity.ModifiedBy;
            ActivitiesBanner.SortOrder = entity.SortOrder;

            XsiContext.Context.XsiActivitiesBanner.Add(ActivitiesBanner);
            SubmitChanges();
            return ActivitiesBanner;

        }
        public void Update(XsiActivitiesBanner entity)
        {
            XsiActivitiesBanner ActivitiesBanner = XsiContext.Context.XsiActivitiesBanner.Find(entity.ItemId);
            XsiContext.Context.Entry(ActivitiesBanner).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ActivitiesBanner.Title = entity.Title.Trim();
            if (!entity.HomeBanner.IsDefault())
                ActivitiesBanner.HomeBanner = entity.HomeBanner;
            if (!entity.Url.IsDefault())
                ActivitiesBanner.Url = entity.Url.Trim();
            if (!entity.IsActive.IsDefault())
                ActivitiesBanner.IsActive = entity.IsActive;

            if (!entity.ModifiedOn.IsDefault())
                ActivitiesBanner.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                ActivitiesBanner.ModifiedBy = entity.ModifiedBy;

            if (!entity.SortOrder.IsDefault())
                ActivitiesBanner.SortOrder = entity.SortOrder;
        }
        public void Delete(XsiActivitiesBanner entity)
        {
            foreach (XsiActivitiesBanner ActivitiesBanner in Select(entity))
            {
                XsiActivitiesBanner aActivitiesBanner = XsiContext.Context.XsiActivitiesBanner.Find(ActivitiesBanner.ItemId);
                XsiContext.Context.XsiActivitiesBanner.Remove(aActivitiesBanner);
            }
        }
        public void Delete(long itemId)
        {
            XsiActivitiesBanner aActivitiesBanner = XsiContext.Context.XsiActivitiesBanner.Find(itemId);
            XsiContext.Context.XsiActivitiesBanner.Remove(aActivitiesBanner);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
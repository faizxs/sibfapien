﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Xsi.Repositories
{
    public class ProfessionalProgramRegistrationRepository : IProfessionalProgramRegistrationRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ProfessionalProgramRegistrationRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ProfessionalProgramRegistrationRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ProfessionalProgramRegistrationRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionProfessionalProgramRegistration GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.Find(itemId);
        }

        public List<XsiExhibitionProfessionalProgramRegistration> Select()
        {
            return XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.ToList();
        }
        public List<XsiExhibitionProfessionalProgramRegistration> Select(XsiExhibitionProfessionalProgramRegistration entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgramRegistration>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);

            if (!entity.ProfessionalProgramId.IsDefault())
                predicate = predicate.And(p => p.ProfessionalProgramId == entity.ProfessionalProgramId);

            if (!entity.TableId.IsDefault())
                predicate = predicate.And(p => p.TableId == entity.TableId);

            if (!entity.ProfessionalProgramId.IsDefault())
                predicate = predicate.And(p => p.ProfessionalProgramId == entity.ProfessionalProgramId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.CityId.IsDefault())
                predicate = predicate.And(p => p.CityId == entity.CityId);

            if (!entity.OtherCity.IsDefault())
                predicate = predicate.And(p => p.OtherCity.Contains(entity.OtherCity));

            if (!entity.StepId.IsDefault())
                predicate = predicate.And(p => p.StepId == entity.StepId);

            if (!entity.FirstName.IsDefault())
                predicate = predicate.And(p => p.FirstName.Contains(entity.FirstName));

            if (!entity.FirstNameAr.IsDefault())
                predicate = predicate.And(p => p.FirstNameAr.Contains(entity.FirstNameAr));

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));

            if (!entity.LastNameAr.IsDefault())
                predicate = predicate.And(p => p.LastNameAr.Contains(entity.LastNameAr));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Contains(entity.Email));

            if (!entity.PhoneNumber.IsDefault())
                predicate = predicate.And(p => p.PhoneNumber.Contains(entity.PhoneNumber));

            if (!entity.MobileNumber.IsDefault())
                predicate = predicate.And(p => p.MobileNumber.Contains(entity.MobileNumber));

            if (!entity.CompanyName.IsDefault())
                predicate = predicate.And(p => p.CompanyName.Contains(entity.CompanyName));

            if (!entity.CompanyNameAr.IsDefault())
                predicate = predicate.And(p => p.CompanyNameAr.Contains(entity.CompanyNameAr));

            if (!entity.JobTitle.IsDefault())
                predicate = predicate.And(p => p.JobTitle.Contains(entity.JobTitle));

            if (!entity.JobTitleAr.IsDefault())
                predicate = predicate.And(p => p.JobTitleAr.Contains(entity.JobTitleAr));

            if (!entity.WorkProfile.IsDefault())
                predicate = predicate.And(p => p.WorkProfile.Contains(entity.WorkProfile));

            if (!entity.WorkProfileAr.IsDefault())
                predicate = predicate.And(p => p.WorkProfileAr.Contains(entity.WorkProfileAr));

            if (!entity.Website.IsDefault())
                predicate = predicate.And(p => p.Website.Contains(entity.Website));

            if (!entity.GenreId.IsDefault())
                predicate = predicate.And(p => p.GenreId == entity.GenreId);

            if (!entity.TradeType.IsDefault())
                predicate = predicate.And(p => p.TradeType.Equals(entity.TradeType));

            if (!entity.IsEbooks.IsDefault())
                predicate = predicate.And(p => p.IsEbooks.Equals(entity.IsEbooks));

            if (!entity.ExamplesOfTranslatedBooks.IsDefault())
                predicate = predicate.And(p => p.ExamplesOfTranslatedBooks.Contains(entity.ExamplesOfTranslatedBooks));

            if (!entity.IsInterested.IsDefault())
                predicate = predicate.And(p => p.IsInterested.Equals(entity.IsInterested));

            if (!entity.InterestedInfo.IsDefault())
                predicate = predicate.And(p => p.InterestedInfo.Contains(entity.InterestedInfo));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Contains(entity.FileName));

            if (!entity.FileName1.IsDefault())
                predicate = predicate.And(p => p.FileName1.Contains(entity.FileName1));

            if (!entity.IsAttended.IsDefault())
                predicate = predicate.And(p => p.IsAttended.Equals(entity.IsAttended));

            if (!entity.HowManyTimes.IsDefault())
                predicate = predicate.And(p => p.HowManyTimes.Equals(entity.HowManyTimes));

            if (!entity.BooksUrl.IsDefault())
                predicate = predicate.And(p => p.BooksUrl.Equals(entity.BooksUrl));

            if (!entity.ZipCode.IsDefault())
                predicate = predicate.And(p => p.ZipCode.Equals(entity.ZipCode));

            if (!entity.ReasonToAttend.IsDefault())
                predicate = predicate.And(p => p.ReasonToAttend.Contains(entity.ReasonToAttend));

            if (!entity.SocialLinkUrl.IsDefault())
                predicate = predicate.And(p => p.SocialLinkUrl.Contains(entity.SocialLinkUrl));

            if (!entity.ReasonToAttendAgain.IsDefault())
                predicate = predicate.And(p => p.ReasonToAttendAgain.Contains(entity.ReasonToAttendAgain));

            if (!entity.IsAttendedTg.IsDefault())
                predicate = predicate.And(p => p.IsAttendedTg.Equals(entity.IsAttendedTg));

            if (!entity.NoofPendingTg.IsDefault())
                predicate = predicate.And(p => p.NoofPendingTg.Contains(entity.NoofPendingTg));

            if (!entity.NoofPublishedTg.IsDefault())
                predicate = predicate.And(p => p.NoofPublishedTg.Contains(entity.NoofPublishedTg));

            if (!entity.TgreasonForNotAttend.IsDefault())
                predicate = predicate.And(p => p.TgreasonForNotAttend.Contains(entity.TgreasonForNotAttend));

            if (!entity.ProgramOther.IsDefault())
                predicate = predicate.And(p => p.ProgramOther.Contains(entity.ProgramOther));

            if (!entity.UnSoldRightsBooks.IsDefault())
                predicate = predicate.And(p => p.UnSoldRightsBooks.Contains(entity.UnSoldRightsBooks));

            if (!entity.AcquireRightsToPublished.IsDefault())
                predicate = predicate.And(p => p.AcquireRightsToPublished.Contains(entity.AcquireRightsToPublished));

            if (!entity.IsAlreadyBoughtRights.IsDefault())
                predicate = predicate.And(p => p.IsAlreadyBoughtRights.Equals(entity.IsAlreadyBoughtRights));

            if (!entity.KeyPublisherTitlesOrBusinessRegion.IsDefault())
                predicate = predicate.And(p => p.KeyPublisherTitlesOrBusinessRegion.Contains(entity.KeyPublisherTitlesOrBusinessRegion));

            if (!entity.CatalogueMainLanguageOfOriginalTitle.IsDefault())
                predicate = predicate.And(p => p.CatalogueMainLanguageOfOriginalTitle.Contains(entity.CatalogueMainLanguageOfOriginalTitle));

            if (!entity.MainTerritories.IsDefault())
                predicate = predicate.And(p => p.MainTerritories.Contains(entity.MainTerritories));

            if (!entity.SuggestionToSupportApp.IsDefault())
                predicate = predicate.And(p => p.SuggestionToSupportApp.Contains(entity.SuggestionToSupportApp));

            if (!entity.RoundTableTopics.IsDefault())
                predicate = predicate.And(p => p.RoundTableTopics.Contains(entity.RoundTableTopics));

            if (!entity.SpeakOnRoundTable.IsDefault())
                predicate = predicate.And(p => p.SpeakOnRoundTable.Contains(entity.SpeakOnRoundTable));

            if (!entity.RecommendPublishers.IsDefault())
                predicate = predicate.And(p => p.RecommendPublishers.Contains(entity.RecommendPublishers));

            if (!entity.Qrcode.IsDefault())
                predicate = predicate.And(p => p.Qrcode.Contains(entity.Qrcode));

            if (!entity.Type.IsDefault())
                predicate = predicate.And(p => p.Type.Equals(entity.Type));

            if (!entity.Qrcode.IsDefault())
                predicate = predicate.And(p => p.Qrcode.Contains(entity.Qrcode));

            if (!entity.IsEditRequest.IsDefault())
                predicate = predicate.And(p => p.IsEditRequest.Equals(entity.IsEditRequest));

            if (!entity.IsHopitalityPackage.IsDefault())
                predicate = predicate.And(p => p.IsHopitalityPackage.Equals(entity.IsHopitalityPackage));

            if (!entity.CatalogUrl.IsDefault())
                predicate = predicate.And(p => p.CatalogUrl.Contains(entity.CatalogUrl));

            if (!entity.CreatedById.IsDefault())
                predicate = predicate.And(p => p.CreatedById == entity.CreatedById);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.ModifiedById.IsDefault())
                predicate = predicate.And(p => p.ModifiedById == entity.ModifiedById);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            return XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionProfessionalProgramRegistration> SelectSuggestedProfessionals(XsiExhibitionProfessionalProgramRegistration entity)
        {
            var predicatePPSlotRegistration = PredicateBuilder.True<XsiExhibitionProfessionalProgramSlots>();
            //if (!entity.ItemId.IsDefault())
            // predicatePPSlotRegistration = predicatePPSlotRegistration.And(p => p.RequestId == entity.ItemId);
            var PPInvitedIdList = new HashSet<long>(XsiContext.Context.XsiExhibitionProfessionalProgramSlots.AsExpandable().Where(predicatePPSlotRegistration).Select(p => p.ItemId));

            var predicate = PredicateBuilder.True<XsiExhibitionPpgenre>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ProfessionalProgramRegistrationId == entity.ItemId);
            var GenreIdList = new HashSet<long?>(XsiContext.Context.XsiExhibitionPpgenre.AsExpandable().Where(predicate).Select(p => p.GenreId));

            var predicate1 = PredicateBuilder.True<XsiExhibitionPpgenre>();
            if (GenreIdList.Count > 0)
                predicate1 = predicate1.And(p => GenreIdList.Contains(p.GenreId));
            if (!entity.ItemId.IsDefault())
                predicate1 = predicate1.And(p => p.ProfessionalProgramRegistrationId != entity.ItemId);
            if (PPInvitedIdList.Count > 0)
                predicate1 = predicate1.And(p => !PPInvitedIdList.Contains(p.ProfessionalProgramRegistrationId.Value));
            var PPRegisteredListItemIds = new HashSet<long>(XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.AsExpandable().Where(i => i.Status == "A" && i.IsActive == "Y").Select(p => p.ItemId));

            if (PPRegisteredListItemIds.Count > 0)
                predicate1 = predicate1.And(p => PPRegisteredListItemIds.Contains(p.ItemId));
            var PPRegistrationIdList = new HashSet<long?>(XsiContext.Context.XsiExhibitionPpgenre.AsExpandable().Where(predicate1).Select(p => p.ProfessionalProgramRegistrationId).Distinct());

            var predicatePPRegistration = PredicateBuilder.True<XsiExhibitionProfessionalProgramRegistration>();
            predicatePPRegistration = predicatePPRegistration.And(p => PPRegistrationIdList.Contains(p.ItemId));
            if (!entity.ProfessionalProgramId.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.ProfessionalProgramId == entity.ProfessionalProgramId);
            if (!entity.IsActive.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.Status.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.Status.Equals(entity.Status));
           
            return XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.AsExpandable().Where(predicatePPRegistration).ToList();
        }
        public List<XsiExhibitionProfessionalProgramRegistration> SelectOtherProfessionals(List<long?> genreIDList, List<long> suggestedIDList, XsiExhibitionProfessionalProgramRegistration entity)
        {
            var predicate1 = PredicateBuilder.True<XsiExhibitionPpgenre>();
            if (genreIDList.Count > 0)
                predicate1 = predicate1.And(p => genreIDList.Contains(p.GenreId));
            if (suggestedIDList.Count > 0)
                predicate1 = predicate1.And(p => !suggestedIDList.Contains(p.ProfessionalProgramRegistrationId.Value));
            if (!entity.ItemId.IsDefault())
                predicate1 = predicate1.And(p => p.ProfessionalProgramRegistrationId != entity.ItemId);
            var PPRegistrationIdList = new HashSet<long?>(XsiContext.Context.XsiExhibitionPpgenre.AsExpandable().Where(predicate1).Select(p => p.ProfessionalProgramRegistrationId).Distinct());

            var predicatePPRegistration = PredicateBuilder.True<XsiExhibitionProfessionalProgramRegistration>();
            predicatePPRegistration = predicatePPRegistration.And(p => PPRegistrationIdList.Contains(p.ItemId));
            if (!entity.ProfessionalProgramId.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.ProfessionalProgramId == entity.ProfessionalProgramId);
            if (!entity.IsActive.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.Status.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.Status.Equals(entity.Status));

            var predicateOr = PredicateBuilder.False<XsiExhibitionProfessionalProgramRegistration>();
            if (!entity.FirstName.IsDefault())
                predicateOr = predicateOr.Or(p => p.FirstName.Contains(entity.FirstName));
            if (!entity.LastName.IsDefault())
                predicateOr = predicateOr.Or(p => p.LastName.Contains(entity.LastName));
            if (!entity.FirstNameAr.IsDefault())
                predicateOr = predicateOr.Or(p => p.FirstNameAr.Contains(entity.FirstNameAr));
            if (!entity.LastNameAr.IsDefault())
                predicateOr = predicateOr.Or(p => p.LastNameAr.Contains(entity.LastNameAr));
            if (!entity.FirstName.IsDefault() || !entity.LastName.IsDefault() || !entity.FirstNameAr.IsDefault() || !entity.LastNameAr.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(predicateOr.Expand());

            return XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.AsExpandable().Where(predicatePPRegistration).ToList();
        }
        public List<XsiExhibitionProfessionalProgramRegistration> SelectOtherProfessionalsSearch(List<long?> genreIDList, List<long> suggestedIDList, XsiExhibitionProfessionalProgramRegistration entity, string name)
        {
            var predicate1 = PredicateBuilder.True<XsiExhibitionPpgenre>();
            if (genreIDList != null && genreIDList.Count > 0)
                predicate1 = predicate1.And(p => genreIDList.Contains(p.GenreId));
            if (suggestedIDList != null && suggestedIDList.Count > 0)
                predicate1 = predicate1.And(p => !suggestedIDList.Contains(p.ProfessionalProgramRegistrationId.Value));
            if (!entity.ItemId.IsDefault())
                predicate1 = predicate1.And(p => p.ProfessionalProgramRegistrationId != entity.ItemId);

            var PPRegistrationIdList = new HashSet<long?>(XsiContext.Context.XsiExhibitionPpgenre.AsExpandable().Where(predicate1).Select(p => p.ProfessionalProgramRegistrationId).Distinct());

            var predicatePPRegistration = PredicateBuilder.True<XsiExhibitionProfessionalProgramRegistration>();
            predicatePPRegistration = predicatePPRegistration.And(p => PPRegistrationIdList.Contains(p.ItemId));
            if (!entity.ProfessionalProgramId.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.ProfessionalProgramId == entity.ProfessionalProgramId);
            if (!entity.IsActive.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.Status.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.Status.Equals(entity.Status));


            var predicateOr = PredicateBuilder.False<XsiExhibitionProfessionalProgramRegistration>();
            if (!string.IsNullOrEmpty(name) && !name.IsDefault())
            {
                predicateOr = predicateOr.Or(p => (p.FirstName + " " + p.LastName).Contains(name));
                predicateOr = predicateOr.Or(p => (p.FirstNameAr + " " + p.LastNameAr).Contains(name));
                predicatePPRegistration = predicatePPRegistration.And(predicateOr.Expand());
            }

            return XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.AsExpandable().Where(predicatePPRegistration).ToList();
        }
        public List<XsiExhibitionProfessionalProgramRegistration> SelectOtherProfessionals(XsiExhibitionProfessionalProgramRegistration entity)
        {
            var predicatePPRegistration = PredicateBuilder.True<XsiExhibitionProfessionalProgramRegistration>();
            if (!entity.ProfessionalProgramId.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.ProfessionalProgramId == entity.ProfessionalProgramId);
            if (!entity.IsActive.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.Status.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.Status.Equals(entity.Status));
            if (!entity.ItemId.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.ItemId != entity.ItemId);

            var predicateOr = PredicateBuilder.False<XsiExhibitionProfessionalProgramRegistration>();
            if (!entity.FirstName.IsDefault())
                predicateOr = predicateOr.Or(p => p.FirstName.Contains(entity.FirstName));
            if (!entity.LastName.IsDefault())
                predicateOr = predicateOr.Or(p => p.LastName.Contains(entity.LastName));
            if (!entity.FirstNameAr.IsDefault())
                predicateOr = predicateOr.Or(p => p.FirstNameAr.Contains(entity.FirstNameAr));
            if (!entity.LastNameAr.IsDefault())
                predicateOr = predicateOr.Or(p => p.LastNameAr.Contains(entity.LastNameAr));
            if (!entity.FirstName.IsDefault() || !entity.LastName.IsDefault() || !entity.FirstNameAr.IsDefault() || !entity.LastNameAr.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(predicateOr.Expand());

            return XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.AsExpandable().Where(predicatePPRegistration).ToList();
        }
        public List<XsiExhibitionProfessionalProgramRegistration> SelectOtherProfessionalsSearch(XsiExhibitionProfessionalProgramRegistration entity, string name)
        {
            var predicatePPRegistration = PredicateBuilder.True<XsiExhibitionProfessionalProgramRegistration>();
            if (!entity.ProfessionalProgramId.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.ProfessionalProgramId == entity.ProfessionalProgramId);
            if (!entity.IsActive.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.Status.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.Status.Equals(entity.Status));
            if (!entity.ItemId.IsDefault())
                predicatePPRegistration = predicatePPRegistration.And(p => p.ItemId != entity.ItemId);


            var predicateOr = PredicateBuilder.False<XsiExhibitionProfessionalProgramRegistration>();
            if (!name.IsDefault())
            {
                predicateOr = predicateOr.Or(p => (p.FirstName + " " + p.LastName).Contains(name));
                predicateOr = predicateOr.Or(p => (p.FirstNameAr + " " + p.LastNameAr).Contains(name));
                predicatePPRegistration = predicatePPRegistration.And(predicateOr.Expand());
            }

            return XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.AsExpandable().Where(predicatePPRegistration).ToList();
        }
        public List<XsiExhibitionProfessionalProgramRegistration> SelectPPRegistrationCMS(XsiExhibitionProfessionalProgramRegistration entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgramRegistration>();
            var predicateOr = PredicateBuilder.False<XsiExhibitionProfessionalProgramRegistration>();

            if (!entity.ProfessionalProgramId.IsDefault())
                predicate = predicate.And(p => p.ProfessionalProgramId == entity.ProfessionalProgramId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.Type.IsDefault())
                predicate = predicate.And(p => p.Type.Equals(entity.Type));

            if (!entity.IsEditRequest.IsDefault())
                predicate = predicate.And(p => p.IsEditRequest.Equals(entity.IsEditRequest));

            if (!entity.FirstName.IsDefault())
                predicate = predicate.And(p => (p.FirstName + " " + p.LastName).Contains(entity.FirstName));

            return XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.AsExpandable().Where(predicate).ToList();
        }

        /* public List<GetApporvedMeetUps_Result> SelectApprovedMeetups(long professionalProgramID)
         {
             return XsiContext.Context.GetApporvedMeetUps(professionalProgramID).ToList();
         }
         public List<GetApporvedMeetUpsByMember_Result> SelectApprovedMeetupsByMember(long professionalProgramID, long profesionalProgramRegistrationID)
         {
             return XsiContext.Context.GetApporvedMeetUpsByMember(professionalProgramID, profesionalProgramRegistrationID).ToList();
         }*/
        public XsiExhibitionProfessionalProgramRegistration Add(XsiExhibitionProfessionalProgramRegistration entity)
        {
            XsiExhibitionProfessionalProgramRegistration ProfessionalProgramRegistration = new XsiExhibitionProfessionalProgramRegistration();
            ProfessionalProgramRegistration.MemberId = entity.MemberId;
            ProfessionalProgramRegistration.ProfessionalProgramId = entity.ProfessionalProgramId;
            ProfessionalProgramRegistration.TableId = entity.TableId;
            ProfessionalProgramRegistration.IsActive = entity.IsActive;
            ProfessionalProgramRegistration.Status = entity.Status;
            ProfessionalProgramRegistration.LanguageId = entity.LanguageId;
            ProfessionalProgramRegistration.CountryId = entity.CountryId;
            ProfessionalProgramRegistration.CityId = entity.CityId;
            if (!entity.OtherCity.IsDefault())
                ProfessionalProgramRegistration.OtherCity = entity.OtherCity.Trim();
            ProfessionalProgramRegistration.StepId = entity.StepId;
            if (!entity.Title.IsDefault())
                ProfessionalProgramRegistration.Title = entity.Title.Trim();
            if (!entity.FirstName.IsDefault())
                ProfessionalProgramRegistration.FirstName = entity.FirstName.Trim();
            if (!entity.FirstNameAr.IsDefault())
                ProfessionalProgramRegistration.FirstNameAr = entity.FirstNameAr.Trim();
            if (!entity.LastName.IsDefault())
                ProfessionalProgramRegistration.LastName = entity.LastName.Trim();
            if (!entity.LastNameAr.IsDefault())
                ProfessionalProgramRegistration.LastNameAr = entity.LastNameAr.Trim();
            if (!entity.Email.IsDefault())
                ProfessionalProgramRegistration.Email = entity.Email.Trim();
            if (!entity.PhoneNumber.IsDefault())
                ProfessionalProgramRegistration.PhoneNumber = entity.PhoneNumber.Trim();
            if (!entity.MobileNumber.IsDefault())
                ProfessionalProgramRegistration.MobileNumber = entity.MobileNumber.Trim();
            if (!entity.CompanyName.IsDefault())
                ProfessionalProgramRegistration.CompanyName = entity.CompanyName.Trim();
            if (!entity.CompanyNameAr.IsDefault())
                ProfessionalProgramRegistration.CompanyNameAr = entity.CompanyNameAr.Trim();
            if (!entity.JobTitle.IsDefault())
                ProfessionalProgramRegistration.JobTitle = entity.JobTitle.Trim();
            if (!entity.JobTitleAr.IsDefault())
                ProfessionalProgramRegistration.JobTitleAr = entity.JobTitleAr.Trim();
            if (!entity.WorkProfile.IsDefault())
                ProfessionalProgramRegistration.WorkProfile = entity.WorkProfile.Trim();
            if (!entity.WorkProfileAr.IsDefault())
                ProfessionalProgramRegistration.WorkProfileAr = entity.WorkProfileAr.Trim();
            if (!entity.Website.IsDefault())
                ProfessionalProgramRegistration.Website = entity.Website.Trim();

            ProfessionalProgramRegistration.GenreId = entity.GenreId;
            if (!entity.OtherGenre.IsDefault())
                ProfessionalProgramRegistration.OtherGenre = entity.OtherGenre.Trim();
            ProfessionalProgramRegistration.TradeType = entity.TradeType;
            ProfessionalProgramRegistration.IsEbooks = entity.IsEbooks;
            if (!entity.ExamplesOfTranslatedBooks.IsDefault())
                ProfessionalProgramRegistration.ExamplesOfTranslatedBooks = entity.ExamplesOfTranslatedBooks.Trim();
            ProfessionalProgramRegistration.IsInterested = entity.IsInterested;
            ProfessionalProgramRegistration.InterestedInfo = entity.InterestedInfo;
            ProfessionalProgramRegistration.FileName = entity.FileName;
            ProfessionalProgramRegistration.FileName1 = entity.FileName1;
            ProfessionalProgramRegistration.IsAttended = entity.IsAttended;
            if (!entity.HowManyTimes.IsDefault())
                ProfessionalProgramRegistration.HowManyTimes = entity.HowManyTimes.Trim();
            if (!entity.BooksUrl.IsDefault())
                ProfessionalProgramRegistration.BooksUrl = entity.BooksUrl.Trim();
            if (!entity.ZipCode.IsDefault())
                ProfessionalProgramRegistration.ZipCode = entity.ZipCode.Trim();
            if (!entity.ReasonToAttend.IsDefault())
                ProfessionalProgramRegistration.ReasonToAttend = entity.ReasonToAttend.Trim();
            if (!entity.SocialLinkUrl.IsDefault())
                ProfessionalProgramRegistration.SocialLinkUrl = entity.SocialLinkUrl.Trim();
            if (!entity.ReasonToAttendAgain.IsDefault())
                ProfessionalProgramRegistration.ReasonToAttendAgain = entity.ReasonToAttendAgain.Trim();
            if (!entity.IsAttendedTg.IsDefault())
                ProfessionalProgramRegistration.IsAttendedTg = entity.IsAttendedTg.Trim();
            if (!entity.NoofPendingTg.IsDefault())
                ProfessionalProgramRegistration.NoofPendingTg = entity.NoofPendingTg.Trim();
            if (!entity.NoofPublishedTg.IsDefault())
                ProfessionalProgramRegistration.NoofPublishedTg = entity.NoofPublishedTg.Trim();
            if (!entity.TgreasonForNotAttend.IsDefault())
                ProfessionalProgramRegistration.TgreasonForNotAttend = entity.TgreasonForNotAttend.Trim();
            if (!entity.ProgramOther.IsDefault())
                ProfessionalProgramRegistration.ProgramOther = entity.ProgramOther.Trim();
            if (!entity.UnSoldRightsBooks.IsDefault())
                ProfessionalProgramRegistration.UnSoldRightsBooks = entity.UnSoldRightsBooks.Trim();
            if (!entity.AcquireRightsToPublished.IsDefault())
                ProfessionalProgramRegistration.AcquireRightsToPublished = entity.AcquireRightsToPublished.Trim();
            if (!entity.IsAlreadyBoughtRights.IsDefault())
                ProfessionalProgramRegistration.IsAlreadyBoughtRights = entity.IsAlreadyBoughtRights.Trim();
            if (!entity.KeyPublisherTitlesOrBusinessRegion.IsDefault())
                ProfessionalProgramRegistration.KeyPublisherTitlesOrBusinessRegion = entity.KeyPublisherTitlesOrBusinessRegion.Trim();
            if (!entity.CatalogueMainLanguageOfOriginalTitle.IsDefault())
                ProfessionalProgramRegistration.CatalogueMainLanguageOfOriginalTitle = entity.CatalogueMainLanguageOfOriginalTitle.Trim();
            if (!entity.MainTerritories.IsDefault())
                ProfessionalProgramRegistration.MainTerritories = entity.MainTerritories.Trim();
            if (!entity.SuggestionToSupportApp.IsDefault())
                ProfessionalProgramRegistration.SuggestionToSupportApp = entity.SuggestionToSupportApp.Trim();
            if (!entity.RoundTableTopics.IsDefault())
                ProfessionalProgramRegistration.RoundTableTopics = entity.RoundTableTopics.Trim();
            if (!entity.SpeakOnRoundTable.IsDefault())
                ProfessionalProgramRegistration.SpeakOnRoundTable = entity.SpeakOnRoundTable.Trim();
            if (!entity.RecommendPublishers.IsDefault())
                ProfessionalProgramRegistration.RecommendPublishers = entity.RecommendPublishers.Trim();
            ProfessionalProgramRegistration.Type = entity.Type;
            if (!entity.IsEditRequest.IsDefault())
                ProfessionalProgramRegistration.IsEditRequest = entity.IsEditRequest;
            if (!entity.CatalogUrl.IsDefault())
                ProfessionalProgramRegistration.CatalogUrl = entity.CatalogUrl.Trim();
            if (!entity.Qrcode.IsDefault())
                ProfessionalProgramRegistration.Qrcode = entity.Qrcode; 
            if (!entity.IsHopitalityPackage.IsDefault())
                ProfessionalProgramRegistration.IsHopitalityPackage = entity.IsHopitalityPackage;
            ProfessionalProgramRegistration.CreatedById = entity.CreatedById;
            ProfessionalProgramRegistration.CreatedOn = entity.CreatedOn;
            ProfessionalProgramRegistration.ModifiedById = entity.ModifiedById;
            ProfessionalProgramRegistration.ModifiedOn = entity.ModifiedOn;

            XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.Add(ProfessionalProgramRegistration);
            SubmitChanges();
            return ProfessionalProgramRegistration;
        }
        public void Update(XsiExhibitionProfessionalProgramRegistration entity)
        {
            XsiExhibitionProfessionalProgramRegistration ProfessionalProgramRegistration = XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.Find(entity.ItemId);
            XsiContext.Context.Entry(ProfessionalProgramRegistration).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                ProfessionalProgramRegistration.IsActive = entity.IsActive;
            if (!entity.Status.IsDefault())
                ProfessionalProgramRegistration.Status = entity.Status;
            if (!entity.CountryId.IsDefault())
                ProfessionalProgramRegistration.CountryId = entity.CountryId;
            if (!entity.CityId.IsDefault())
                ProfessionalProgramRegistration.CityId = entity.CityId;
            if (!entity.OtherCity.IsDefault())
                ProfessionalProgramRegistration.OtherCity = entity.OtherCity.Trim();
            if (!entity.TableId.IsDefault())
                ProfessionalProgramRegistration.TableId = entity.TableId;
            if (!entity.StepId.IsDefault())
                ProfessionalProgramRegistration.StepId = entity.StepId;
            if (!entity.Title.IsDefault())
                ProfessionalProgramRegistration.Title = entity.Title.Trim();
            if (!entity.FirstName.IsDefault())
                ProfessionalProgramRegistration.FirstName = entity.FirstName.Trim();
            if (!entity.FirstNameAr.IsDefault())
                ProfessionalProgramRegistration.FirstNameAr = entity.FirstNameAr.Trim();
            if (!entity.LastName.IsDefault())
                ProfessionalProgramRegistration.LastName = entity.LastName.Trim();
            if (!entity.LastNameAr.IsDefault())
                ProfessionalProgramRegistration.LastNameAr = entity.LastNameAr.Trim();
            if (!entity.Email.IsDefault())
                ProfessionalProgramRegistration.Email = entity.Email.Trim();
            if (!entity.PhoneNumber.IsDefault())
                ProfessionalProgramRegistration.PhoneNumber = entity.PhoneNumber.Trim();
            if (!entity.MobileNumber.IsDefault())
                ProfessionalProgramRegistration.MobileNumber = entity.MobileNumber.Trim();
            if (!entity.CompanyName.IsDefault())
                ProfessionalProgramRegistration.CompanyName = entity.CompanyName.Trim();
            if (!entity.CompanyNameAr.IsDefault())
                ProfessionalProgramRegistration.CompanyNameAr = entity.CompanyNameAr.Trim();
            if (!entity.JobTitle.IsDefault())
                ProfessionalProgramRegistration.JobTitle = entity.JobTitle.Trim();
            if (!entity.JobTitleAr.IsDefault())
                ProfessionalProgramRegistration.JobTitleAr = entity.JobTitleAr.Trim();
            if (!entity.WorkProfile.IsDefault())
                ProfessionalProgramRegistration.WorkProfile = entity.WorkProfile.Trim();
            if (!entity.WorkProfileAr.IsDefault())
                ProfessionalProgramRegistration.WorkProfileAr = entity.WorkProfileAr.Trim();
            if (!entity.Website.IsDefault())
                ProfessionalProgramRegistration.Website = entity.Website.Trim();
            if (!entity.GenreId.IsDefault())
                ProfessionalProgramRegistration.GenreId = entity.GenreId;
            if (!entity.OtherGenre.IsDefault())
                ProfessionalProgramRegistration.OtherGenre = entity.OtherGenre.Trim();
            if (!entity.TradeType.IsDefault())
                ProfessionalProgramRegistration.TradeType = entity.TradeType;
            if (!entity.IsEbooks.IsDefault())
                ProfessionalProgramRegistration.IsEbooks = entity.IsEbooks;
            if (!entity.ExamplesOfTranslatedBooks.IsDefault())
                ProfessionalProgramRegistration.ExamplesOfTranslatedBooks = entity.ExamplesOfTranslatedBooks.Trim();
            if (!entity.IsInterested.IsDefault())
                ProfessionalProgramRegistration.IsInterested = entity.IsInterested;
            if (!entity.InterestedInfo.IsDefault())
                ProfessionalProgramRegistration.InterestedInfo = entity.InterestedInfo;
            if (!entity.FileName.IsDefault())
                ProfessionalProgramRegistration.FileName = entity.FileName;
            if (!entity.FileName1.IsDefault())
                ProfessionalProgramRegistration.FileName1 = entity.FileName1;
            if (!entity.IsAttended.IsDefault())
                ProfessionalProgramRegistration.IsAttended = entity.IsAttended;
            if (!entity.HowManyTimes.IsDefault())
                ProfessionalProgramRegistration.HowManyTimes = entity.HowManyTimes.Trim();
            if (!entity.BooksUrl.IsDefault())
                ProfessionalProgramRegistration.BooksUrl = entity.BooksUrl.Trim();
            if (!entity.ZipCode.IsDefault())
                ProfessionalProgramRegistration.ZipCode = entity.ZipCode.Trim();
            if (!entity.ReasonToAttend.IsDefault())
                ProfessionalProgramRegistration.ReasonToAttend = entity.ReasonToAttend.Trim();
            if (!entity.SocialLinkUrl.IsDefault())
                ProfessionalProgramRegistration.SocialLinkUrl = entity.SocialLinkUrl.Trim();
            if (!entity.ReasonToAttendAgain.IsDefault())
                ProfessionalProgramRegistration.ReasonToAttendAgain = entity.ReasonToAttendAgain.Trim();
            if (!entity.IsAttendedTg.IsDefault())
                ProfessionalProgramRegistration.IsAttendedTg = entity.IsAttendedTg.Trim();
            if (!entity.NoofPendingTg.IsDefault())
                ProfessionalProgramRegistration.NoofPendingTg = entity.NoofPendingTg.Trim();
            if (!entity.NoofPublishedTg.IsDefault())
                ProfessionalProgramRegistration.NoofPublishedTg = entity.NoofPublishedTg.Trim();
            if (!entity.TgreasonForNotAttend.IsDefault())
                ProfessionalProgramRegistration.TgreasonForNotAttend = entity.TgreasonForNotAttend.Trim();
            if (!entity.ProgramOther.IsDefault())
                ProfessionalProgramRegistration.ProgramOther = entity.ProgramOther.Trim();
            if (!entity.UnSoldRightsBooks.IsDefault())
                ProfessionalProgramRegistration.UnSoldRightsBooks = entity.UnSoldRightsBooks.Trim();
            if (!entity.AcquireRightsToPublished.IsDefault())
                ProfessionalProgramRegistration.AcquireRightsToPublished = entity.AcquireRightsToPublished.Trim();
            if (!entity.IsAlreadyBoughtRights.IsDefault())
                ProfessionalProgramRegistration.IsAlreadyBoughtRights = entity.IsAlreadyBoughtRights.Trim();
            if (!entity.KeyPublisherTitlesOrBusinessRegion.IsDefault())
                ProfessionalProgramRegistration.KeyPublisherTitlesOrBusinessRegion = entity.KeyPublisherTitlesOrBusinessRegion.Trim();
            if (!entity.CatalogueMainLanguageOfOriginalTitle.IsDefault())
                ProfessionalProgramRegistration.CatalogueMainLanguageOfOriginalTitle = entity.CatalogueMainLanguageOfOriginalTitle.Trim();
            if (!entity.MainTerritories.IsDefault())
                ProfessionalProgramRegistration.MainTerritories = entity.MainTerritories.Trim();
            if (!entity.SuggestionToSupportApp.IsDefault())
                ProfessionalProgramRegistration.SuggestionToSupportApp = entity.SuggestionToSupportApp.Trim();
            if (!entity.RoundTableTopics.IsDefault())
                ProfessionalProgramRegistration.RoundTableTopics = entity.RoundTableTopics.Trim();
            if (!entity.SpeakOnRoundTable.IsDefault())
                ProfessionalProgramRegistration.SpeakOnRoundTable = entity.SpeakOnRoundTable.Trim();
            if (!entity.RecommendPublishers.IsDefault())
                ProfessionalProgramRegistration.RecommendPublishers = entity.RecommendPublishers.Trim();
            if (!entity.Type.IsDefault())
                ProfessionalProgramRegistration.Type = entity.Type;
            if (!entity.IsHopitalityPackage.IsDefault())
                ProfessionalProgramRegistration.IsHopitalityPackage = entity.IsHopitalityPackage;
            if (!entity.Qrcode.IsDefault())
                ProfessionalProgramRegistration.Qrcode = entity.Qrcode;
            if (!entity.IsEditRequest.IsDefault())
                ProfessionalProgramRegistration.IsEditRequest = entity.IsEditRequest;
            if (!entity.CatalogUrl.IsDefault())
                ProfessionalProgramRegistration.CatalogUrl = entity.CatalogUrl;
            if (!entity.ModifiedById.IsDefault())
                ProfessionalProgramRegistration.ModifiedById = entity.ModifiedById;
            if (!entity.ModifiedOn.IsDefault())
                ProfessionalProgramRegistration.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiExhibitionProfessionalProgramRegistration entity)
        {
            foreach (XsiExhibitionProfessionalProgramRegistration ProfessionalProgramRegistration in Select(entity))
            {
                XsiExhibitionProfessionalProgramRegistration aProfessionalProgramRegistration = XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.Find(ProfessionalProgramRegistration.ItemId);
                XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.Remove(aProfessionalProgramRegistration);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionProfessionalProgramRegistration aProfessionalProgramRegistration = XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.Find(itemId);
            XsiContext.Context.XsiExhibitionProfessionalProgramRegistration.Remove(aProfessionalProgramRegistration);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
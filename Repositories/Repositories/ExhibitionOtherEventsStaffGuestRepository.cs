﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionOtherEventsStaffGuestRepository : IExhibitionOtherEventsStaffGuestRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionOtherEventsStaffGuestRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionOtherEventsStaffGuestRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionOtherEventsStaffGuestRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionOtherEventsStaffGuest GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionOtherEventsStaffGuest.Find(itemId);
        }
          
        public List<XsiExhibitionOtherEventsStaffGuest> Select()
        {
            return XsiContext.Context.XsiExhibitionOtherEventsStaffGuest.ToList();
        }
        public List<XsiExhibitionOtherEventsStaffGuest> Select(XsiExhibitionOtherEventsStaffGuest entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionOtherEventsStaffGuest>();
            if (!entity.StaffGuestId.IsDefault())
                predicate = predicate.And(p => p.StaffGuestId == entity.StaffGuestId);

            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);

            if (!entity.StaffGuestId.IsDefault())
                predicate = predicate.And(p => p.StaffGuestId == entity.StaffGuestId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);
            
            //return XsiContext.Context.XsiExhibitionOtherEventsStaffGuest.Include("XsiExhibitionOtherEventsStaffGuestParticipating").AsExpandable().Where(predicate).ToList();

            return XsiContext.Context.XsiExhibitionOtherEventsStaffGuest.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionOtherEventsStaffGuest Add(XsiExhibitionOtherEventsStaffGuest entity)
        {
            XsiExhibitionOtherEventsStaffGuest ExhibitionOtherEventsStaffGuest = new XsiExhibitionOtherEventsStaffGuest();
            ExhibitionOtherEventsStaffGuest.MemberId = entity.MemberId;
            ExhibitionOtherEventsStaffGuest.StaffGuestId = entity.StaffGuestId;
            ExhibitionOtherEventsStaffGuest.NameEn = entity.NameEn;
            ExhibitionOtherEventsStaffGuest.LastName = entity.LastName;
            ExhibitionOtherEventsStaffGuest.NameAr = entity.NameAr;
            ExhibitionOtherEventsStaffGuest.IsActive = entity.IsActive;
            ExhibitionOtherEventsStaffGuest.CreatedBy = entity.CreatedBy;
            ExhibitionOtherEventsStaffGuest.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiExhibitionOtherEventsStaffGuest.Add(ExhibitionOtherEventsStaffGuest);
            SubmitChanges();
            return ExhibitionOtherEventsStaffGuest;

        }
        public void Update(XsiExhibitionOtherEventsStaffGuest entity)
        {
            //XsiExhibitionOtherEventsStaffGuest ExhibitionOtherEventsStaffGuest = XsiContext.Context.XsiExhibitionOtherEventsStaffGuest.Find(entity.StaffGuestId);
            XsiExhibitionOtherEventsStaffGuest ExhibitionOtherEventsStaffGuest = XsiContext.Context.XsiExhibitionOtherEventsStaffGuest.Where(p=>p.StaffGuestId== entity.StaffGuestId).FirstOrDefault();
            XsiContext.Context.Entry(ExhibitionOtherEventsStaffGuest).State = EntityState.Modified;

            if (!entity.MemberId.IsDefault())
                ExhibitionOtherEventsStaffGuest.MemberId = entity.MemberId;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionOtherEventsStaffGuest.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionOtherEventsStaffGuest.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionOtherEventsStaffGuest entity)
        {
            foreach (XsiExhibitionOtherEventsStaffGuest ExhibitionOtherEventsStaffGuest in Select(entity))
            {
                XsiExhibitionOtherEventsStaffGuest aExhibitionOtherEventsStaffGuest = XsiContext.Context.XsiExhibitionOtherEventsStaffGuest.Find(ExhibitionOtherEventsStaffGuest.StaffGuestId);
                XsiContext.Context.XsiExhibitionOtherEventsStaffGuest.Remove(aExhibitionOtherEventsStaffGuest);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionOtherEventsStaffGuest aExhibitionOtherEventsStaffGuest = XsiContext.Context.XsiExhibitionOtherEventsStaffGuest.Find(itemId);
            XsiContext.Context.XsiExhibitionOtherEventsStaffGuest.Remove(aExhibitionOtherEventsStaffGuest);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
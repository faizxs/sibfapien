﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ProfessionalProgramBookRepository : IProfessionalProgramBookRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ProfessionalProgramBookRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ProfessionalProgramBookRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ProfessionalProgramBookRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionProfessionalProgramBook GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionProfessionalProgramBook.Find(itemId);
        }
        public List<XsiExhibitionProfessionalProgramBook> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionProfessionalProgramBook> Select()
        {
            return XsiContext.Context.XsiExhibitionProfessionalProgramBook.ToList();
        }
        public List<XsiExhibitionProfessionalProgramBook> Select(XsiExhibitionProfessionalProgramBook entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgramBook>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ProfessionalProgramRegistrationId.IsDefault())
                predicate = predicate.And(p => p.ProfessionalProgramRegistrationId == entity.ProfessionalProgramRegistrationId);

            if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.AuthorName.IsDefault())
                predicate = predicate.And(p => p.AuthorName.Contains(entity.AuthorName));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Equals(entity.Title));

            if (!entity.GenreId.IsDefault())
                predicate = predicate.And(p => p.GenreId == entity.GenreId);

            if (!entity.OtherGenre.IsDefault())
                predicate = predicate.And(p => p.OtherGenre == entity.OtherGenre);

            if (!entity.Synopsis.IsDefault())
                predicate = predicate.And(p => p.Synopsis.Contains(entity.Synopsis));

            if (!entity.IsRightsAvailable.IsDefault())
                predicate = predicate.And(p => p.IsRightsAvailable.Equals(entity.IsRightsAvailable));

            if (!entity.IsOwnRights.IsDefault())
                predicate = predicate.And(p => p.IsOwnRights.Equals(entity.IsOwnRights));

            if (!entity.RightsOwner.IsDefault())
                predicate = predicate.And(p => p.RightsOwner.Contains(entity.RightsOwner));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Contains(entity.FileName));

            if (!entity.CreatedById.IsDefault())
                predicate = predicate.And(p => p.CreatedById == entity.CreatedById);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.ModifiedById.IsDefault())
                predicate = predicate.And(p => p.ModifiedById == entity.ModifiedById);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);
            return XsiContext.Context.XsiExhibitionProfessionalProgramBook.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionProfessionalProgramBook Add(XsiExhibitionProfessionalProgramBook entity)
        {
            XsiExhibitionProfessionalProgramBook ProfessionalProgramBook = new XsiExhibitionProfessionalProgramBook();
            ProfessionalProgramBook.ProfessionalProgramRegistrationId = entity.ProfessionalProgramRegistrationId;
            ProfessionalProgramBook.LanguageId = entity.LanguageId;
            ProfessionalProgramBook.IsActive = entity.IsActive;
            if (!entity.AuthorName.IsDefault())
                ProfessionalProgramBook.AuthorName = entity.AuthorName.Trim();
            if (!entity.Title.IsDefault())
            ProfessionalProgramBook.Title = entity.Title.Trim();
            ProfessionalProgramBook.GenreId = entity.GenreId;
            if (!entity.OtherGenre.IsDefault())
                ProfessionalProgramBook.OtherGenre = entity.OtherGenre.Trim();
            if (!entity.Synopsis.IsDefault())
            ProfessionalProgramBook.Synopsis = entity.Synopsis.Trim();
            ProfessionalProgramBook.IsRightsAvailable = entity.IsRightsAvailable;
            ProfessionalProgramBook.IsOwnRights = entity.IsOwnRights;
            if (!entity.RightsOwner.IsDefault())
            ProfessionalProgramBook.RightsOwner = entity.RightsOwner.Trim();
            ProfessionalProgramBook.FileName = entity.FileName;
            ProfessionalProgramBook.CreatedOn = entity.CreatedOn;
            ProfessionalProgramBook.CreatedById = entity.CreatedById;
            ProfessionalProgramBook.ModifiedOn = entity.ModifiedOn;
            ProfessionalProgramBook.ModifiedById = entity.ModifiedById;

            XsiContext.Context.XsiExhibitionProfessionalProgramBook.Add(ProfessionalProgramBook);
            SubmitChanges();
            return ProfessionalProgramBook;

        }
        public void Update(XsiExhibitionProfessionalProgramBook entity)
        {
            XsiExhibitionProfessionalProgramBook ProfessionalProgramBook = XsiContext.Context.XsiExhibitionProfessionalProgramBook.Find(entity.ItemId);
            XsiContext.Context.Entry(ProfessionalProgramBook).State = EntityState.Modified;


            if (!entity.IsActive.IsDefault())
                ProfessionalProgramBook.IsActive = entity.IsActive;
            if (!entity.AuthorName.IsDefault())
                ProfessionalProgramBook.AuthorName = entity.AuthorName.Trim();
            if (!entity.Title.IsDefault())
                ProfessionalProgramBook.Title = entity.Title.Trim();
            if (!entity.GenreId.IsDefault())
                ProfessionalProgramBook.GenreId = entity.GenreId;
            if (!entity.OtherGenre.IsDefault())
                ProfessionalProgramBook.OtherGenre = entity.OtherGenre.Trim();
            if (!entity.Synopsis.IsDefault())
                ProfessionalProgramBook.Synopsis = entity.Synopsis.Trim();
            if (!entity.IsRightsAvailable.IsDefault())
                ProfessionalProgramBook.IsRightsAvailable = entity.IsRightsAvailable;
            if (!entity.IsOwnRights.IsDefault())
                ProfessionalProgramBook.IsOwnRights = entity.IsOwnRights;
            if (!entity.RightsOwner.IsDefault())
                ProfessionalProgramBook.RightsOwner = entity.RightsOwner.Trim();
            if (!entity.FileName.IsDefault())
                ProfessionalProgramBook.FileName = entity.FileName;
            if (!entity.ModifiedOn.IsDefault())
                ProfessionalProgramBook.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedById.IsDefault())
                ProfessionalProgramBook.ModifiedById = entity.ModifiedById;
        }
        public void Delete(XsiExhibitionProfessionalProgramBook entity)
        {
            foreach (XsiExhibitionProfessionalProgramBook ProfessionalProgramBook in Select(entity))
            {
                XsiExhibitionProfessionalProgramBook aProfessionalProgramBook = XsiContext.Context.XsiExhibitionProfessionalProgramBook.Find(ProfessionalProgramBook.ItemId);
                XsiContext.Context.XsiExhibitionProfessionalProgramBook.Remove(aProfessionalProgramBook);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionProfessionalProgramBook aProfessionalProgramBook = XsiContext.Context.XsiExhibitionProfessionalProgramBook.Find(itemId);
            XsiContext.Context.XsiExhibitionProfessionalProgramBook.Remove(aProfessionalProgramBook);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
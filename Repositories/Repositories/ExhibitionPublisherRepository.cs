﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionPublisherRepository : IExhibitionPublisherRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionPublisherRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionPublisherRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionPublisherRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionBookPublisher GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionBookPublisher.Find(itemId);
        }
        public List<XsiExhibitionBookPublisher> GetByGroupId(long groupId)
        {
            return null;
        }
        public List<XsiExhibitionBookPublisher> Select()
        {
            return XsiContext.Context.XsiExhibitionBookPublisher.ToList();
        }
        public List<XsiExhibitionBookPublisher> Select(XsiExhibitionBookPublisher entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBookPublisher>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.ToLower().Contains(entity.TitleAr));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            //if (!entity.SIBF_PublisherId.IsDefault())
            //    predicate = predicate.And(p => p.SIBF_PublisherId == entity.SIBF_PublisherId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            return XsiContext.Context.XsiExhibitionBookPublisher.AsExpandable().Where(predicate).ToList();
        }
        public Array SelectUI(XsiExhibitionBookPublisher entity, long exhibitionGroupID)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBookPublisher>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.ToLower().Contains(entity.TitleAr));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            //if (!entity.SIBF_PublisherId.IsDefault())
            //    predicate = predicate.And(p => p.SIBF_PublisherId == entity.SIBF_PublisherId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));
            #region Useful Commented Code
            //if (CachingRepository.CachedPublishers == null)
            //    CachingRepository.CachedPublishers = XsiContext.Context.XsiExhibitionBookPublisher.ToList();
            //if (CachingRepository.CachedBooksParticipating == null)
            //    CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBooksParticipatings.ToList();
            //if (CachingRepository.CachedBooks == null)
            //    CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();

            //var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.ExhibitionGroupId == exhibitionGroupID && p.IsActive == "Y" && p.BookId != null).Select(s => s.BookId.Value).Distinct());
            //var PublisherIDs = new HashSet<long>(CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.ItemId) && p.PublisherId != null).Select(s => s.PublisherId.Value).Distinct());
            //var SearchLists = CachingRepository.CachedPublishers.Where(p => PublisherIDs.Contains(p.ItemId)).ToList();
            //return SearchLists.AsQueryable().AsExpandable().Where(predicate).Take(30).Select(s => new { ItemId = s.ItemId, Title = s.Title, TitleAr = s.TitleAr }).ToArray();
            #endregion Useful Commented Code
            return null;
        }
        public List<XsiExhibitionBookPublisher> SelectSCRFUI(XsiExhibitionBookPublisher entity, long exhibitionGroupID)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBookPublisher>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.ToLower().Contains(entity.TitleAr));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            //if (!entity.SIBF_PublisherId.IsDefault())
            //    predicate = predicate.And(p => p.SIBF_PublisherId == entity.SIBF_PublisherId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));
            #region Useful Commented Code
            //if (CachingRepository.CachedPublishers == null)
            //    CachingRepository.CachedPublishers = XsiContext.Context.XsiExhibitionBookPublisher.ToList();
            //if (CachingRepository.CachedBooksParticipating == null)
            //    CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiBooksParticipatings.ToList();
            //if (CachingRepository.CachedBooks == null)
            //    CachingRepository.CachedBooks = XsiContext.Context.XsiBooks.ToList();

            //var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.ExhibitionGroupId == exhibitionGroupID && p.IsActive == "Y" && p.BookId != null).Select(s => s.BookId.Value).Distinct());
            //var PublisherIDs = new HashSet<long>(CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.ItemId) && p.PublisherId != null).Select(s => s.PublisherId.Value).Distinct());
            //var SearchLists = CachingRepository.CachedPublishers.Where(p => PublisherIDs.Contains(p.ItemId)).ToList();
            //return SearchLists.AsQueryable().AsExpandable().Where(predicate).Take(30).Select(s => new XsiExhibitionBookPublisher { ItemId = s.ItemId, Title = s.Title, TitleAr = s.TitleAr }).ToList();
            #endregion Useful Commented Code
            return null;
        }
        public XsiExhibitionBookPublisher Add(XsiExhibitionBookPublisher entity)
        {
            XsiExhibitionBookPublisher ExhibitionPublisher = new XsiExhibitionBookPublisher();

            if (!entity.Title.IsDefault())
                ExhibitionPublisher.Title = entity.Title.Trim();
            ExhibitionPublisher.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionPublisher.TitleAr = entity.TitleAr.Trim();
            ExhibitionPublisher.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            ExhibitionPublisher.CreatedOn = entity.CreatedOn;
            ExhibitionPublisher.CreatedBy = entity.CreatedBy;
            ExhibitionPublisher.ModifiedOn = entity.ModifiedOn;
            ExhibitionPublisher.ModifiedBy = entity.ModifiedBy;
            ExhibitionPublisher.Status = entity.Status;

            XsiContext.Context.XsiExhibitionBookPublisher.Add(ExhibitionPublisher);
            SubmitChanges();
            if (CachingRepository.CachedPublishers == null)
                CachingRepository.CachedPublishers = XsiContext.Context.XsiExhibitionBookPublisher.ToList();
            else
                CachingRepository.CachedPublishers.Add(ExhibitionPublisher);
            return ExhibitionPublisher;

        }
        public void Update(XsiExhibitionBookPublisher entity)
        {
            XsiExhibitionBookPublisher ExhibitionPublisher = XsiContext.Context.XsiExhibitionBookPublisher.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionPublisher).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionPublisher.Title = entity.Title.Trim();          
            if (!entity.IsActive.IsDefault())
                ExhibitionPublisher.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionPublisher.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                ExhibitionPublisher.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionPublisher.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                ExhibitionPublisher.ModifiedBy = entity.ModifiedBy;
            if (!entity.Status.IsDefault())
                ExhibitionPublisher.Status = entity.Status;
            if (CachingRepository.CachedPublishers == null)
                CachingRepository.CachedPublishers = XsiContext.Context.XsiExhibitionBookPublisher.ToList();
            else
            {
                #region Update Cache
                var ItemToModify = CachingRepository.CachedPublishers.Where(p => p.ItemId == entity.ItemId).FirstOrDefault();
                if (!entity.Title.IsDefault())
                    ItemToModify.Title = entity.Title.Trim();
                if (!entity.TitleAr.IsDefault())
                    ItemToModify.TitleAr = entity.TitleAr.Trim();
                if (!entity.IsActive.IsDefault())
                    ItemToModify.IsActive = entity.IsActive;
                if (!entity.ModifiedOn.IsDefault())
                    ItemToModify.ModifiedOn = entity.ModifiedOn;
                if (!entity.ModifiedBy.IsDefault())
                    ItemToModify.ModifiedBy = entity.ModifiedBy;
                if (!entity.Status.IsDefault())
                    ItemToModify.Status = entity.Status;
                #endregion
            }
        }
        public void Delete(XsiExhibitionBookPublisher entity)
        {
            foreach (XsiExhibitionBookPublisher ExhibitionPublisher in Select(entity))
            {
                XsiExhibitionBookPublisher aExhibitionPublisher = XsiContext.Context.XsiExhibitionBookPublisher.Find(ExhibitionPublisher.ItemId);
                XsiContext.Context.XsiExhibitionBookPublisher.Remove(aExhibitionPublisher);
                if (CachingRepository.CachedPublishers == null)
                    CachingRepository.CachedPublishers = XsiContext.Context.XsiExhibitionBookPublisher.ToList();
                CachingRepository.CachedPublishers.Remove(CachingRepository.CachedPublishers.SingleOrDefault(x => x.ItemId == ExhibitionPublisher.ItemId));
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionBookPublisher aExhibitionPublisher = XsiContext.Context.XsiExhibitionBookPublisher.Find(itemId);
            XsiContext.Context.XsiExhibitionBookPublisher.Remove(aExhibitionPublisher);
            if (CachingRepository.CachedPublishers == null)
                CachingRepository.CachedPublishers = XsiContext.Context.XsiExhibitionBookPublisher.ToList();
            CachingRepository.CachedPublishers.Remove(CachingRepository.CachedPublishers.SingleOrDefault(x => x.ItemId == itemId));
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
        //Custom
        public long GetNameCount(long itemID, string title, string titlear)
        {
            return XsiContext.Context.XsiExhibitionBookPublisher.Where(p => p.ItemId != itemID && (p.Title == title || p.TitleAr == titlear)).Count();
        }
    }
}
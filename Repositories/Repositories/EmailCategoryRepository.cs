﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class EmailCategoryRepository : IEmailCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public EmailCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public EmailCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public EmailCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiEmailCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiEmailCategory.Find(itemId);
        }
        public List<XsiEmailCategory> Select()
        {
            return XsiContext.Context.XsiEmailCategory.ToList();
        }
        public List<XsiEmailCategory> Select(XsiEmailCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiEmailCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.CategoryId.IsDefault())
                predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.EmailId.IsDefault())
                predicate = predicate.And(p => p.EmailId.Contains(entity.EmailId));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.Contains(entity.TitleAr));

            if (!entity.IsActiveAr.IsDefault())
                predicate = predicate.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);



            return XsiContext.Context.XsiEmailCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiEmailCategory> SelectOr(XsiEmailCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiEmailCategory>();
            var Outer = PredicateBuilder.True<XsiEmailCategory>();

            if (!entity.TitleAr.IsDefault())
            {
                string strSearchText = entity.TitleAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.TitleAr.Contains(searchKeys.str1) || p.TitleAr.Contains(searchKeys.str2) || p.TitleAr.Contains(searchKeys.str3) || p.TitleAr.Contains(searchKeys.str4) || p.TitleAr.Contains(searchKeys.str5)
                    || p.TitleAr.Contains(searchKeys.str6) || p.TitleAr.Contains(searchKeys.str7) || p.TitleAr.Contains(searchKeys.str8) || p.TitleAr.Contains(searchKeys.str9) || p.TitleAr.Contains(searchKeys.str10)
                    || p.TitleAr.Contains(searchKeys.str11) || p.TitleAr.Contains(searchKeys.str12) || p.TitleAr.Contains(searchKeys.str13)
                    || p.TitleAr.Contains(searchKeys.str14) || p.TitleAr.Contains(searchKeys.str15) || p.TitleAr.Contains(searchKeys.str16) || p.TitleAr.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.CategoryId.IsDefault())
                Outer = Outer.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsActiveAr.IsDefault())
                Outer = Outer.And(p => p.IsActiveAr.Equals(entity.IsActiveAr));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiEmailCategory.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiEmailCategory Add(XsiEmailCategory entity)
        {
            XsiEmailCategory EmailCategory = new XsiEmailCategory();
            EmailCategory.WebsiteId = entity.WebsiteId;
            EmailCategory.CategoryId = entity.CategoryId;
            if (!entity.Title.IsDefault())
                EmailCategory.Title = entity.Title.Trim();
            if (!entity.EmailId.IsDefault())
                EmailCategory.EmailId = entity.EmailId.Trim();
            EmailCategory.IsActive = entity.IsActive;

            if (!entity.TitleAr.IsDefault())
                EmailCategory.TitleAr = entity.TitleAr.Trim();
            EmailCategory.IsActiveAr = entity.IsActiveAr;

            EmailCategory.CreatedOn = entity.CreatedOn;
            EmailCategory.CreatedBy = entity.CreatedBy;
            EmailCategory.ModifiedOn = entity.ModifiedOn;
            EmailCategory.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiEmailCategory.Add(EmailCategory);
            SubmitChanges();
            return EmailCategory;

        }
        public void Update(XsiEmailCategory entity)
        {
            XsiEmailCategory EmailCategory = XsiContext.Context.XsiEmailCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(EmailCategory).State = EntityState.Modified;

            if (!entity.WebsiteId.IsDefault())
                EmailCategory.WebsiteId = entity.WebsiteId;

            if (!entity.CategoryId.IsDefault())
                EmailCategory.CategoryId = entity.CategoryId;

            if (!entity.Title.IsDefault())
                EmailCategory.Title = entity.Title.Trim();

            if (!entity.EmailId.IsDefault())
                EmailCategory.EmailId = entity.EmailId.Trim();

            if (!entity.IsActive.IsDefault())
                EmailCategory.IsActive = entity.IsActive;
            
            if (!entity.TitleAr.IsDefault())
                EmailCategory.TitleAr = entity.TitleAr.Trim();

            if (!entity.IsActiveAr.IsDefault())
                EmailCategory.IsActiveAr = entity.IsActiveAr;

            if (!entity.ModifiedOn.IsDefault())
                EmailCategory.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                EmailCategory.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiEmailCategory entity)
        {
            foreach (XsiEmailCategory EmailCategory in Select(entity))
            {
                XsiEmailCategory aEmailCategory = XsiContext.Context.XsiEmailCategory.Find(EmailCategory.ItemId);
                XsiContext.Context.XsiEmailCategory.Remove(aEmailCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiEmailCategory aEmailCategory = XsiContext.Context.XsiEmailCategory.Find(itemId);
            XsiContext.Context.XsiEmailCategory.Remove(aEmailCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
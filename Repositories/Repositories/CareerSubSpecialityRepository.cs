﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Xsi.Repositories
{
    public class CareerSubSpecialityRepository : ICareerSubSpecialityRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public CareerSubSpecialityRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public CareerSubSpecialityRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public CareerSubSpecialityRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiCareerSubSpeciality GetById(long itemId)
        {
            return XsiContext.Context.XsiCareerSubSpeciality.Find(itemId);
        }
        
        public List<XsiCareerSubSpeciality> Select()
        {
            return XsiContext.Context.XsiCareerSubSpeciality.ToList();
        }
        public List<XsiCareerSubSpeciality> Select(XsiCareerSubSpeciality entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiCareerSubSpeciality>();
            var predicate = PredicateBuilder.True<XsiCareerSubSpeciality>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
             
            if (!entity.SpecialityId.IsDefault())
                predicate = predicate.And(p => p.SpecialityId == entity.SpecialityId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiCareerSubSpeciality.AsExpandable().Where(predicate).ToList();
        }

        public XsiCareerSubSpeciality Add(XsiCareerSubSpeciality entity)
        {
            XsiCareerSubSpeciality CareerSubSpeciality = new XsiCareerSubSpeciality();
            
            CareerSubSpeciality.SpecialityId = entity.SpecialityId;
            if (!entity.Title.IsDefault())
                CareerSubSpeciality.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                CareerSubSpeciality.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerSubSpeciality.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerSubSpeciality.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            CareerSubSpeciality.CreatedOn = entity.CreatedOn;
            CareerSubSpeciality.CreatedBy = entity.CreatedBy;
            CareerSubSpeciality.ModifiedOn = entity.ModifiedOn;
            CareerSubSpeciality.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiCareerSubSpeciality.Add(CareerSubSpeciality);
            SubmitChanges();
            return CareerSubSpeciality;

        }
        public void Update(XsiCareerSubSpeciality entity)
        {
            XsiCareerSubSpeciality CareerSubSpeciality = XsiContext.Context.XsiCareerSubSpeciality.Find(entity.ItemId);
            XsiContext.Context.Entry(CareerSubSpeciality).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                CareerSubSpeciality.Title = entity.Title.Trim();
            if (!entity.SpecialityId.IsDefault())
                CareerSubSpeciality.SpecialityId = entity.SpecialityId;
            if (!entity.IsActive.IsDefault())
                CareerSubSpeciality.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerSubSpeciality.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerSubSpeciality.IsActiveAr = entity.IsActiveAr;
            #endregion Ar
             
            if (!entity.ModifiedOn.IsDefault())
                CareerSubSpeciality.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                CareerSubSpeciality.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiCareerSubSpeciality entity)
        {
            foreach (XsiCareerSubSpeciality CareerSubSpeciality in Select(entity))
            {
                XsiCareerSubSpeciality aCareerSubSpeciality = XsiContext.Context.XsiCareerSubSpeciality.Find(CareerSubSpeciality.ItemId);
                XsiContext.Context.XsiCareerSubSpeciality.Remove(aCareerSubSpeciality);
            }
        }
        public void Delete(long itemId)
        {
            XsiCareerSubSpeciality aCareerSubSpeciality = XsiContext.Context.XsiCareerSubSpeciality.Find(itemId);
            XsiContext.Context.XsiCareerSubSpeciality.Remove(aCareerSubSpeciality);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
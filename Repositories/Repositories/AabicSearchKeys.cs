﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
  
    public class AabicSearchKeys
    {
        public string strSearchText { get; set; }
        public string str1 { get; set; }
        public string str2 { get; set; }
        public string str3 { get; set; }
        public string str4 { get; set; }
        public string str5 { get; set; }
        public string str6 { get; set; }
        public string str7 { get; set; }
        public string str8 { get; set; }
        public string str9 { get; set; }
        public string str10 { get; set; }
        public string str11 { get; set; }
        public string str12 { get; set; }
        public string str13 { get; set; }
        public string str14 { get; set; }
        public string str15 { get; set; }
        public string str16 { get; set; }
        public string str17 { get; set; }
        public string str18 { get; set; }
        public string str19 { get; set; }
        public string str20 { get; set; }
        public string str21 { get; set; }
        public string str22 { get; set; }

        public string str23 { get; set; }
        public string str24 { get; set; }
        public string str25 { get; set; }
        public string str26 { get; set; }
        public string str27 { get; set; }
        public static AabicSearchKeys GetSearchKeys(string strSearchText)
        {
            AabicSearchKeys searchKeys = new AabicSearchKeys();
            searchKeys.strSearchText = strSearchText;
            searchKeys.str1 = strSearchText;
            searchKeys.str2 = strSearchText.Replace("أ", "إ");
            searchKeys.str3 = strSearchText.Replace("أ", "ا");
            searchKeys.str4 = strSearchText.Replace("إ", "أ");
            searchKeys.str5 = strSearchText.Replace("إ", "ا");
            searchKeys.str6 = strSearchText.Replace("ا", "أ");
            searchKeys.str7 = strSearchText.Replace("ا", "إ");

            searchKeys.str8 = strSearchText.Replace("ا", "إ");
            searchKeys.str9 = strSearchText.Replace("ا", "أ");

            searchKeys.str10 = strSearchText.Replace("إ", "أ");
            searchKeys.str11 = strSearchText.Replace("إ", "ا");

            searchKeys.str12 = strSearchText.Replace("إ", "أ");
            searchKeys.str13 = strSearchText.Replace("إ", "ا");

            searchKeys.str14 = strSearchText.Replace("ه", "ة");
            searchKeys.str15 = strSearchText.Replace("ة", "ه");

            searchKeys.str16 = strSearchText.Replace("ى", "ي");
            searchKeys.str17 = strSearchText.Replace("ي", "ى");

            searchKeys.str18 = strSearchText.Replace("ا", "ء");
            searchKeys.str19 = strSearchText.Replace("ا", "آ");
            searchKeys.str20 = strSearchText.Replace("ا", "ؤ");
            searchKeys.str21 = strSearchText.Replace("ا", "ئ");
            searchKeys.str22 = strSearchText.Replace("ا", "ا");

            searchKeys.str23 = strSearchText.Replace("ي", "ي");

            searchKeys.str24 = strSearchText.Replace("ة", "ة");
            searchKeys.str25 = strSearchText.Replace("ة", "ه");

            searchKeys.str26 = strSearchText.Replace("و", "و");
            searchKeys.str27 = strSearchText.Replace("و", "ؤ");

            return searchKeys;
        }
    }
   
}
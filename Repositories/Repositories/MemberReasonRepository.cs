﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class MemberReasonRepository : IMemberReasonRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public MemberReasonRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public MemberReasonRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public MemberReasonRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiMemberReason GetById(long itemId)
        {
            return XsiContext.Context.XsiMemberReason.Find(itemId);
        }

       
        public List<XsiMemberReason> Select()
        {
            return XsiContext.Context.XsiMemberReason.ToList();
        }
        public List<XsiMemberReason> Select(XsiMemberReason entity)
        {
            var predicate = PredicateBuilder.True<XsiMemberReason>();
            if (!entity.ReasonId.IsDefault())
                predicate = predicate.And(p => p.ReasonId == entity.ReasonId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);

            if (!entity.ReasonId.IsDefault())
                predicate = predicate.And(p => p.ReasonId == entity.ReasonId);

            return XsiContext.Context.XsiMemberReason.AsExpandable().Where(predicate).ToList();
        }

        public XsiMemberReason Add(XsiMemberReason entity)
        {
            XsiMemberReason MemberReason = new XsiMemberReason();
            if (!entity.Title.IsDefault())
            MemberReason.Title = entity.Title.Trim();
            MemberReason.MemberId = entity.MemberId;
            MemberReason.ReasonId = entity.ReasonId;
            XsiContext.Context.XsiMemberReason.Add(MemberReason);
            SubmitChanges();
            return MemberReason;

        }
        public void Update(XsiMemberReason entity)
        {
            XsiMemberReason MemberReason = XsiContext.Context.XsiMemberReason.Find(entity.ReasonId);
            XsiContext.Context.Entry(MemberReason).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                MemberReason.Title = entity.Title.Trim();
            if (!entity.MemberId.IsDefault())
                MemberReason.MemberId = entity.MemberId;
            if (!entity.ReasonId.IsDefault())
                MemberReason.ReasonId = entity.ReasonId;
        }
        public void Delete(XsiMemberReason entity)
        {
            foreach (XsiMemberReason MemberReason in Select(entity))
            {
                XsiMemberReason aMemberReason = XsiContext.Context.XsiMemberReason.Find(MemberReason.ReasonId);
                XsiContext.Context.XsiMemberReason.Remove(aMemberReason);
            }
        }
        public void Delete(long itemId)
        {
            XsiMemberReason aMemberReason = XsiContext.Context.XsiMemberReason.Find(itemId);
            XsiContext.Context.XsiMemberReason.Remove(aMemberReason);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
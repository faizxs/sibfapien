﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class NewsletterSubscriberRepository : INewsletterSubscriberRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public NewsletterSubscriberRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public NewsletterSubscriberRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public NewsletterSubscriberRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiNewsletterSubscribers GetById(long itemId)
        {
            return XsiContext.Context.XsiNewsletterSubscribers.Find(itemId);
        }
       
        public List<XsiNewsletterSubscribers> Select()
        {
            return XsiContext.Context.XsiNewsletterSubscribers.ToList();
        }
        public List<XsiNewsletterSubscribers> Select(XsiNewsletterSubscribers entity)
        {
            var predicate = PredicateBuilder.True<XsiNewsletterSubscribers>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.IsSubscribe.IsDefault())
                predicate = predicate.And(p => p.IsSubscribe.Equals(entity.IsSubscribe));

            //if (!entity.LanguageId.IsDefault())
            //    predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.Name.IsDefault())
                predicate = predicate.And(p => p.Name.Contains(entity.Name));

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Contains(entity.Email));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn.Equals(entity.CreatedOn));

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn.Equals(entity.ModifiedOn));

            if (!entity.ModifiedById.IsDefault())
                predicate = predicate.And(p => p.ModifiedById == entity.ModifiedById);


            return XsiContext.Context.XsiNewsletterSubscribers.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiNewsletterSubscribers> SelectOr(XsiNewsletterSubscribers entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiNewsletterSubscribers>();
            var Outer = PredicateBuilder.True<XsiNewsletterSubscribers>();

            if (!entity.Name.IsDefault())
            {
                string strSearchText = entity.Name;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Name.Contains(searchKeys.str1) || p.Name.Contains(searchKeys.str2) || p.Name.Contains(searchKeys.str3) || p.Name.Contains(searchKeys.str4) || p.Name.Contains(searchKeys.str5)
                    || p.Name.Contains(searchKeys.str6) || p.Name.Contains(searchKeys.str7) || p.Name.Contains(searchKeys.str8) || p.Name.Contains(searchKeys.str9) || p.Name.Contains(searchKeys.str10)
                    || p.Name.Contains(searchKeys.str11) || p.Name.Contains(searchKeys.str12) || p.Name.Contains(searchKeys.str13)
                    || p.Name.Contains(searchKeys.str14) || p.Name.Contains(searchKeys.str15) || p.Name.Contains(searchKeys.str16) || p.Name.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Name.IsDefault())
                {
                    string strSearchText = entity.Name.ToLower();
                    Inner = Inner.Or(p => p.Name.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            //if (!entity.LanguageId.IsDefault())
            //    Outer = Outer.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsSubscribe.IsDefault())
                Outer = Outer.And(p => p.IsSubscribe.Equals(entity.IsSubscribe));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiNewsletterSubscribers.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public XsiNewsletterSubscribers Add(XsiNewsletterSubscribers entity)
        {
            XsiNewsletterSubscribers NewsletterSubscriber = new XsiNewsletterSubscribers();
            if (!entity.Name.IsDefault())
                NewsletterSubscriber.Name = entity.Name.Trim();
            if (!entity.Email.IsDefault())
            NewsletterSubscriber.Email = entity.Email.Trim(); 
            NewsletterSubscriber.IsActive = entity.IsActive;
            NewsletterSubscriber.CountryId = entity.CountryId;
          
            #region Ar
            if (!entity.NameAr.IsDefault())
                NewsletterSubscriber.NameAr = entity.NameAr.Trim();
            //if (!entity.EmailAr.IsDefault())
            //    NewsletterSubscriber.EmailAr = entity.EmailAr.Trim();
            NewsletterSubscriber.IsActiveAr = entity.IsActiveAr; 
            #endregion Ar

            NewsletterSubscriber.CreatedOn = entity.CreatedOn;
            NewsletterSubscriber.ModifiedOn = entity.ModifiedOn;
            NewsletterSubscriber.ModifiedById = entity.ModifiedById;

            XsiContext.Context.XsiNewsletterSubscribers.Add(NewsletterSubscriber);
            SubmitChanges();
            return NewsletterSubscriber;

        }
        public void Update(XsiNewsletterSubscribers entity)
        {
            XsiNewsletterSubscribers NewsletterSubscriber = XsiContext.Context.XsiNewsletterSubscribers.Find(entity.ItemId);
            XsiContext.Context.Entry(NewsletterSubscriber).State = EntityState.Modified;

            if (!entity.Name.IsDefault())
                NewsletterSubscriber.Name = entity.Name.Trim();

            if (!entity.Email.IsDefault())
                NewsletterSubscriber.Email = entity.Email.Trim();

            if (!entity.CountryId.IsDefault())
                NewsletterSubscriber.CountryId = entity.CountryId;

            if (!entity.IsActive.IsDefault())
                NewsletterSubscriber.IsActive = entity.IsActive;
            if (!entity.IsSubscribe.IsDefault())
                NewsletterSubscriber.IsSubscribe = entity.IsSubscribe;

            #region Ar
            if (!entity.NameAr.IsDefault())
                NewsletterSubscriber.NameAr = entity.NameAr.Trim();
            //if (!entity.EmailAr.IsDefault())
            //    NewsletterSubscriber.EmailAr = entity.EmailAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                NewsletterSubscriber.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                NewsletterSubscriber.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedById.IsDefault())
                NewsletterSubscriber.ModifiedById = entity.ModifiedById;

        }
        public void Delete(XsiNewsletterSubscribers entity)
        {
            foreach (XsiNewsletterSubscribers NewsletterSubscriber in Select(entity))
            {
                XsiNewsletterSubscribers aNewsletterSubscriber = XsiContext.Context.XsiNewsletterSubscribers.Find(NewsletterSubscriber.ItemId);
                XsiContext.Context.XsiNewsletterSubscribers.Remove(aNewsletterSubscriber);
            }
        }
        public void Delete(long itemId)
        {
            XsiNewsletterSubscribers aNewsletterSubscriber = XsiContext.Context.XsiNewsletterSubscribers.Find(itemId);
            XsiContext.Context.XsiNewsletterSubscribers.Remove(aNewsletterSubscriber);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class CareerVisaStatusRepository : ICareerVisaStatusRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public CareerVisaStatusRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public CareerVisaStatusRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public CareerVisaStatusRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiCareerVisaStatus GetById(long itemId)
        {
            return XsiContext.Context.XsiCareerVisaStatus.Find(itemId);
        }

        public List<XsiCareerVisaStatus> Select()
        {
            return XsiContext.Context.XsiCareerVisaStatus.ToList();
        }
        public List<XsiCareerVisaStatus> SelectOr(XsiCareerVisaStatus entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiCareerVisaStatus>();
            var Outer = PredicateBuilder.True<XsiCareerVisaStatus>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }


            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiCareerVisaStatus.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiCareerVisaStatus> Select(XsiCareerVisaStatus entity)
        {
            var predicate = PredicateBuilder.True<XsiCareerVisaStatus>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiCareerVisaStatus.AsExpandable().Where(predicate).ToList();
        }

        public XsiCareerVisaStatus Add(XsiCareerVisaStatus entity)
        {
            XsiCareerVisaStatus CareerVisaStatus = new XsiCareerVisaStatus();

            if (!entity.Title.IsDefault())
                CareerVisaStatus.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                CareerVisaStatus.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerVisaStatus.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerVisaStatus.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            CareerVisaStatus.CreatedOn = entity.CreatedOn;
            CareerVisaStatus.CreatedBy = entity.CreatedBy;
            CareerVisaStatus.ModifiedOn = entity.ModifiedOn;
            CareerVisaStatus.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiCareerVisaStatus.Add(CareerVisaStatus);
            SubmitChanges();
            return CareerVisaStatus;

        }
        public void Update(XsiCareerVisaStatus entity)
        {
            XsiCareerVisaStatus CareerVisaStatus = XsiContext.Context.XsiCareerVisaStatus.Find(entity.ItemId);
            XsiContext.Context.Entry(CareerVisaStatus).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                CareerVisaStatus.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                CareerVisaStatus.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerVisaStatus.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerVisaStatus.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                CareerVisaStatus.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                CareerVisaStatus.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiCareerVisaStatus entity)
        {
            foreach (XsiCareerVisaStatus CareerVisaStatus in Select(entity))
            {
                XsiCareerVisaStatus aCareerVisaStatus = XsiContext.Context.XsiCareerVisaStatus.Find(CareerVisaStatus.ItemId);
                XsiContext.Context.XsiCareerVisaStatus.Remove(aCareerVisaStatus);
            }
        }
        public void Delete(long itemId)
        {
            XsiCareerVisaStatus aCareerVisaStatus = XsiContext.Context.XsiCareerVisaStatus.Find(itemId);
            XsiContext.Context.XsiCareerVisaStatus.Remove(aCareerVisaStatus);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ProfessionalProgramSlotsRepository : IProfessionalProgramSlotsRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ProfessionalProgramSlotsRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ProfessionalProgramSlotsRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ProfessionalProgramSlotsRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionProfessionalProgramSlots GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionProfessionalProgramSlots.Find(itemId);
        }
        public List<XsiExhibitionProfessionalProgramSlots> GetByGroupId(long groupId)
        {
            return XsiContext.Context.XsiExhibitionProfessionalProgramSlots.Where(p => p.ProfessionalProgramId == groupId).ToList();
        }

        public List<XsiExhibitionProfessionalProgramSlots> Select()
        {
            return XsiContext.Context.XsiExhibitionProfessionalProgramSlots.ToList();
        }
        public List<XsiExhibitionProfessionalProgramSlots> Select(XsiExhibitionProfessionalProgramSlots entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgramSlots>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ProfessionalProgramId.IsDefault())
                predicate = predicate.And(p => p.ProfessionalProgramId == entity.ProfessionalProgramId);

            if (!entity.IsDelete.IsDefault())
                predicate = predicate.And(p => p.IsDelete.Equals(entity.IsDelete));

            if (!entity.IsBreak.IsDefault())
                predicate = predicate.And(p => p.IsBreak.Equals(entity.IsBreak));

            if (!entity.DayId.IsDefault())
                predicate = predicate.And(p => p.DayId == entity.DayId);

            if (!entity.BreakTitle.IsDefault())
                predicate = predicate.And(p => p.BreakTitle.Contains(entity.BreakTitle));

            if (!entity.BreakTitleAr.IsDefault())
                predicate = predicate.And(p => p.BreakTitleAr.Contains(entity.BreakTitleAr));

            if (!entity.StartTime.IsDefault())
                predicate = predicate.And(p => p.StartTime == entity.StartTime);

            if (!entity.EndTime.IsDefault())
                predicate = predicate.And(p => p.EndTime == entity.EndTime);

            return XsiContext.Context.XsiExhibitionProfessionalProgramSlots.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionProfessionalProgramSlots Add(XsiExhibitionProfessionalProgramSlots entity)
        {
            XsiExhibitionProfessionalProgramSlots ProfessionalProgramSlots = new XsiExhibitionProfessionalProgramSlots();
            ProfessionalProgramSlots.ProfessionalProgramId = entity.ProfessionalProgramId;
            ProfessionalProgramSlots.IsBreak = entity.IsBreak;
            ProfessionalProgramSlots.IsDelete = entity.IsDelete;
            ProfessionalProgramSlots.DayId = entity.DayId;
            if (!entity.BreakTitle.IsDefault())
                ProfessionalProgramSlots.BreakTitle = entity.BreakTitle.Trim();
            if (!entity.BreakTitleAr.IsDefault())
                ProfessionalProgramSlots.BreakTitleAr = entity.BreakTitleAr.Trim();
            ProfessionalProgramSlots.StartTime = entity.StartTime;
            ProfessionalProgramSlots.EndTime = entity.EndTime;
            XsiContext.Context.XsiExhibitionProfessionalProgramSlots.Add(ProfessionalProgramSlots);
            SubmitChanges();
            return ProfessionalProgramSlots;

        }
        public void Update(XsiExhibitionProfessionalProgramSlots entity)
        {
            XsiExhibitionProfessionalProgramSlots ProfessionalProgramSlots = XsiContext.Context.XsiExhibitionProfessionalProgramSlots.Find(entity.ItemId);
            XsiContext.Context.Entry(ProfessionalProgramSlots).State = EntityState.Modified;

            if (!entity.ProfessionalProgramId.IsDefault())
                ProfessionalProgramSlots.ProfessionalProgramId = entity.ProfessionalProgramId;
            if (!entity.IsBreak.IsDefault())
                ProfessionalProgramSlots.IsBreak = entity.IsBreak;
            if (!entity.BreakTitle.IsDefault())
                ProfessionalProgramSlots.BreakTitle = entity.BreakTitle.Trim();
            if (!entity.BreakTitleAr.IsDefault())
                ProfessionalProgramSlots.BreakTitleAr = entity.BreakTitleAr.Trim();
            if (!entity.IsDelete.IsDefault())
                ProfessionalProgramSlots.IsDelete = entity.IsDelete;
            if (!entity.StartTime.IsDefault())
                ProfessionalProgramSlots.StartTime = entity.StartTime;
            if (!entity.EndTime.IsDefault())
                ProfessionalProgramSlots.EndTime = entity.EndTime;
            if (!entity.DayId.IsDefault())
                ProfessionalProgramSlots.DayId = entity.DayId;
        }
        public void Delete(XsiExhibitionProfessionalProgramSlots entity)
        {
            foreach (XsiExhibitionProfessionalProgramSlots ProfessionalProgramSlots in Select(entity))
            {
                XsiExhibitionProfessionalProgramSlots aProfessionalProgramSlots = XsiContext.Context.XsiExhibitionProfessionalProgramSlots.Find(ProfessionalProgramSlots.ItemId);
                XsiContext.Context.XsiExhibitionProfessionalProgramSlots.Remove(aProfessionalProgramSlots);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionProfessionalProgramSlots aProfessionalProgramSlots = XsiContext.Context.XsiExhibitionProfessionalProgramSlots.Find(itemId);
            XsiContext.Context.XsiExhibitionProfessionalProgramSlots.Remove(aProfessionalProgramSlots);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
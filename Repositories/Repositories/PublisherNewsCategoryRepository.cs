﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PublisherNewsCategoryRepository : IPublisherNewsCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PublisherNewsCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PublisherNewsCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PublisherNewsCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiPublisherNewsCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiPublisherNewsCategory.Find(itemId);
        }
         
        public List<XsiPublisherNewsCategory> Select()
        {
            return XsiContext.Context.XsiPublisherNewsCategory.ToList();
        }
        public List<XsiPublisherNewsCategory> SelectOr(XsiPublisherNewsCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiPublisherNewsCategory>();
            var Outer = PredicateBuilder.True<XsiPublisherNewsCategory>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
               
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

             
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiPublisherNewsCategory.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiPublisherNewsCategory> Select(XsiPublisherNewsCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiPublisherNewsCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);
                        
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);


            return XsiContext.Context.XsiPublisherNewsCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiPublisherNewsCategory> SelectOtherLanguageCategory(XsiPublisherNewsCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiPublisherNewsCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

           // var GroupId = XsiContext.Context.XsiPublisherNewsCategory.AsExpandable().Where(predicate).Single().GroupId.Value;
            predicate = PredicateBuilder.True<XsiPublisherNewsCategory>();
          
            return XsiContext.Context.XsiPublisherNewsCategory.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiPublisherNewsCategory> SelectCurrentLanguageCategory(XsiPublisherNewsCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiPublisherNewsCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            //var GroupId = XsiContext.Context.XsiPublisherNewsCategory.AsExpandable().Where(predicate).Single().GroupId.Value;
            predicate = PredicateBuilder.True<XsiPublisherNewsCategory>();
          
            return XsiContext.Context.XsiPublisherNewsCategory.AsExpandable().Where(predicate).ToList();
        }

        public XsiPublisherNewsCategory Add(XsiPublisherNewsCategory entity)
        {
            XsiPublisherNewsCategory PublisherNewsCategory = new XsiPublisherNewsCategory();
           
            PublisherNewsCategory.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                PublisherNewsCategory.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
                PublisherNewsCategory.Description = entity.Description.Trim();
            PublisherNewsCategory.CreatedOn = entity.CreatedOn;
            PublisherNewsCategory.CreatedBy = entity.CreatedBy;

            XsiContext.Context.XsiPublisherNewsCategory.Add(PublisherNewsCategory);
            SubmitChanges();
            return PublisherNewsCategory;

        }
        public void Update(XsiPublisherNewsCategory entity)
        {
            XsiPublisherNewsCategory PublisherNewsCategory = XsiContext.Context.XsiPublisherNewsCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(PublisherNewsCategory).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                PublisherNewsCategory.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                PublisherNewsCategory.Title = entity.Title.Trim();

            if (!entity.Description.IsDefault())
                PublisherNewsCategory.Description = entity.Description.Trim();

        }
        public void Delete(XsiPublisherNewsCategory entity)
        {
            foreach (XsiPublisherNewsCategory PublisherNewsCategory in Select(entity))
            {
                XsiPublisherNewsCategory aPublisherNewsCategory = XsiContext.Context.XsiPublisherNewsCategory.Find(PublisherNewsCategory.ItemId);
                XsiContext.Context.XsiPublisherNewsCategory.Remove(aPublisherNewsCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiPublisherNewsCategory aPublisherNewsCategory = XsiContext.Context.XsiPublisherNewsCategory.Find(itemId);
            XsiContext.Context.XsiPublisherNewsCategory.Remove(aPublisherNewsCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
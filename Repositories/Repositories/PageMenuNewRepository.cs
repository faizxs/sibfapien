﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PageMenuNewRepository : IPageMenuNewRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PageMenuNewRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PageMenuNewRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PageMenuNewRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiPageMenuNew GetById(long itemId)
        {
            return XsiContext.Context.XsiPageMenuNew.Find(itemId);
        }

        public List<XsiPageMenuNew> SelectOr(XsiPageMenuNew entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiPageMenuNew>();
            var Outer = PredicateBuilder.True<XsiPageMenuNew>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                     || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                     || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                     || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                     );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.ParentId.IsDefault())
                Outer = Outer.And(p => p.ParentId == entity.ParentId);

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiPageMenuNew.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public List<XsiPageMenuNew> Select()
        {
            //if (CachingRepository.CachedMenuNew == null)
            //    CachingRepository.CachedMenuNew = XsiContext.Context.XsiPageMenuNew.ToList();

            //return CachingRepository.CachedMenuNew.AsQueryable().ToList();
            return XsiContext.Context.XsiPageMenuNew.ToList();
        }
        public List<XsiPageMenuNew> Select(XsiPageMenuNew entity)
        {
            var predicate = PredicateBuilder.True<XsiPageMenuNew>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.ParentId.IsDefault())
                predicate = predicate.And(p => p.ParentId == entity.ParentId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Equals(entity.Title));

            if (!entity.TitleImage.IsDefault())
                predicate = predicate.And(p => p.TitleImage.Equals(entity.TitleImage));

            if (!entity.IsTitleImagePrimary.IsDefault())
                predicate = predicate.And(p => p.IsTitleImagePrimary.Equals(entity.IsTitleImagePrimary));

            if (!entity.PageUrl.IsDefault())
                predicate = predicate.And(p => p.PageUrl.Equals(entity.PageUrl));

            if (!entity.SortOrder.IsDefault())
                predicate = predicate.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.IsExternal.IsDefault())
                predicate = predicate.And(p => p.IsExternal.Equals(entity.IsExternal));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            //if (CachingRepository.CachedMenuNew == null)
            //    CachingRepository.CachedMenuNew = XsiContext.Context.XsiPageMenuNew.ToList();

            //return CachingRepository.CachedMenuNew.AsQueryable().Where(predicate).ToList();
            return XsiContext.Context.XsiPageMenuNew.AsExpandable().Where(predicate).ToList();
        }

        public XsiPageMenuNew Add(XsiPageMenuNew entity)
        {
            XsiPageMenuNew PageMenuNew = new XsiPageMenuNew();

            PageMenuNew.ParentId = entity.ParentId;
            if (!entity.Title.IsDefault())
                PageMenuNew.Title = entity.Title.Trim();
            PageMenuNew.TitleImage = entity.TitleImage;
            PageMenuNew.IsTitleImagePrimary = entity.IsTitleImagePrimary;
            if (!entity.PageUrl.IsDefault())
                PageMenuNew.PageUrl = entity.PageUrl.Trim();
            PageMenuNew.SortOrder = entity.SortOrder;
            PageMenuNew.IsExternal = entity.IsExternal;
            PageMenuNew.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                PageMenuNew.TitleAr = entity.TitleAr.Trim();
            PageMenuNew.TitleImageAr = entity.TitleImageAr;
            PageMenuNew.IsTitleImagePrimaryAr = entity.IsTitleImagePrimaryAr;
            if (!entity.PageUrlAr.IsDefault())
                PageMenuNew.PageUrlAr = entity.PageUrlAr.Trim();
            PageMenuNew.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            XsiContext.Context.XsiPageMenuNew.Add(PageMenuNew);
            SubmitChanges();
            //if (CachingRepository.CachedMenuNew == null)
            //    CachingRepository.CachedMenuNew = XsiContext.Context.XsiPageMenuNew.ToList();
            //else
            //    CachingRepository.CachedMenuNew.Add(PageMenuNew);
            return PageMenuNew;
        }
        public void Update(XsiPageMenuNew entity)
        {
            XsiPageMenuNew PageMenuNew = XsiContext.Context.XsiPageMenuNew.Find(entity.ItemId);
            XsiContext.Context.Entry(PageMenuNew).State = EntityState.Modified;

            if (!entity.ParentId.IsDefault())
                PageMenuNew.ParentId = entity.ParentId;

            if (!entity.Title.IsDefault())
                PageMenuNew.Title = entity.Title.Trim();

            if (!entity.TitleImage.IsDefault())
                PageMenuNew.TitleImage = entity.TitleImage;

            if (!entity.IsTitleImagePrimary.IsDefault())
                PageMenuNew.IsTitleImagePrimary = entity.IsTitleImagePrimary;

            if (!entity.PageUrl.IsDefault())
                PageMenuNew.PageUrl = entity.PageUrl.Trim();

            if (!entity.IsActive.IsDefault())
                PageMenuNew.IsActive = entity.IsActive;

            if (!entity.SortOrder.IsDefault())
                PageMenuNew.SortOrder = entity.SortOrder;

            if (!entity.IsExternal.IsDefault())
                PageMenuNew.IsExternal = entity.IsExternal;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                PageMenuNew.TitleAr = entity.TitleAr.Trim();

            if (!entity.TitleImageAr.IsDefault())
                PageMenuNew.TitleImageAr = entity.TitleImageAr;

            if (!entity.IsTitleImagePrimaryAr.IsDefault())
                PageMenuNew.IsTitleImagePrimaryAr = entity.IsTitleImagePrimaryAr;

            if (!entity.PageUrlAr.IsDefault())
                PageMenuNew.PageUrlAr = entity.PageUrlAr.Trim();

            if (!entity.IsActiveAr.IsDefault())
                PageMenuNew.IsActiveAr = entity.IsActiveAr;
            #endregion Ar
            //if (CachingRepository.CachedMenuNew == null)
            //    CachingRepository.CachedMenuNew = XsiContext.Context.XsiPageMenuNew.ToList();
            //else
            //{
            // #region update cache
            //var ItemToModify = CachingRepository.CachedMenuNew.Where(p => p.ItemId == entity.ItemId).FirstOrDefault();
            //if (!entity.LanguageId.IsDefault())
            //    ItemToModify.LanguageId = entity.LanguageId;

            //if (!entity.GroupId.IsDefault())
            //    ItemToModify.GroupId = entity.GroupId;

            //if (!entity.ParentId.IsDefault())
            //    ItemToModify.ParentId = entity.ParentId;

            //if (!entity.Title.IsDefault())
            //    ItemToModify.Title = entity.Title.Trim();

            //if (!entity.TitleImage.IsDefault())
            //    ItemToModify.TitleImage = entity.TitleImage;

            //if (!entity.IsTitleImagePrimary.IsDefault())
            //    ItemToModify.IsTitleImagePrimary = entity.IsTitleImagePrimary;

            //if (!entity.PageUrl.IsDefault())
            //    ItemToModify.PageUrl = entity.PageUrl.Trim();

            //if (!entity.PageContentGroupId.IsDefault())
            //    ItemToModify.PageContentGroupId = entity.PageContentGroupId;

            //if (!entity.SortOrder.IsDefault())
            //    ItemToModify.SortOrder = entity.SortOrder;

            //if (!entity.IsExternal.IsDefault())
            //    ItemToModify.IsExternal = entity.IsExternal;

            //if (!entity.IsActive.IsDefault())
            //    ItemToModify.IsActive = entity.IsActive;
            // #endregion
            //}
        }
        public void Delete(XsiPageMenuNew entity)
        {
            foreach (XsiPageMenuNew PageMenuNew in Select(entity))
            {
                XsiPageMenuNew aPageMenuNew = XsiContext.Context.XsiPageMenuNew.Find(PageMenuNew.ItemId);
                XsiContext.Context.XsiPageMenuNew.Remove(aPageMenuNew);
                //if (CachingRepository.CachedMenuNew == null)
                //    CachingRepository.CachedMenuNew = XsiContext.Context.XsiPageMenuNew.ToList();
                //CachingRepository.CachedMenuNew.Remove(CachingRepository.CachedMenuNew.SingleOrDefault(x => x.ItemId == PageMenuNew.ItemId));
            }
        }
        public void Delete(long itemId)
        {
            XsiPageMenuNew aPageMenuNew = XsiContext.Context.XsiPageMenuNew.Find(itemId);
            XsiContext.Context.XsiPageMenuNew.Remove(aPageMenuNew);
            //if (CachingRepository.CachedMenuNew == null)
            //    CachingRepository.CachedMenuNew = XsiContext.Context.XsiPageMenuNew.ToList();
            //CachingRepository.CachedMenuNew.Remove(CachingRepository.CachedMenuNew.SingleOrDefault(x => x.ItemId == itemId));
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class AgencyStatusLogRepository : IAgencyStatusLogRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public AgencyStatusLogRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public AgencyStatusLogRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public AgencyStatusLogRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionMemberApplicationYearlyLogs GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Find(itemId);
        }

        public List<XsiExhibitionMemberApplicationYearlyLogs> Select()
        {
            return XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.ToList();
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> Select(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMemberApplicationYearlyLogs>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.MemberExhibitionYearlyId.IsDefault())
                predicate = predicate.And(p => p.MemberExhibitionYearlyId == entity.MemberExhibitionYearlyId);

            if (!entity.AdminId.IsDefault())
                predicate = predicate.And(p => p.AdminId == entity.AdminId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);


            return XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionMemberApplicationYearlyLogs Add(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            XsiExhibitionMemberApplicationYearlyLogs AgencyStatusLog = new XsiExhibitionMemberApplicationYearlyLogs();
            AgencyStatusLog.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;
            AgencyStatusLog.AdminId = entity.AdminId;
            AgencyStatusLog.Status = entity.Status;
            AgencyStatusLog.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Add(AgencyStatusLog);
            SubmitChanges();
            return AgencyStatusLog;

        }
        public void Update(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            XsiExhibitionMemberApplicationYearlyLogs AgencyStatusLog = XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Find(entity.ItemId);
            XsiContext.Context.Entry(AgencyStatusLog).State = EntityState.Modified;

            if (!entity.Status.IsDefault())
                AgencyStatusLog.Status = entity.Status;
        }
        public void Delete(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            foreach (XsiExhibitionMemberApplicationYearlyLogs AgencyStatusLog in Select(entity))
            {
                XsiExhibitionMemberApplicationYearlyLogs aAgencyStatusLog = XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Find(AgencyStatusLog.ItemId);
                XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Remove(aAgencyStatusLog);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionMemberApplicationYearlyLogs aAgencyStatusLog = XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Find(itemId);
            XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Remove(aAgencyStatusLog);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
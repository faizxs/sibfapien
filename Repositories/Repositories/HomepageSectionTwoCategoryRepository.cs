﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class HomepageSectionTwoCategoryRepository : IHomepageSectionTwoCategoryRepository
    { 
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public HomepageSectionTwoCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public HomepageSectionTwoCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public HomepageSectionTwoCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiHomepageSectionTwoCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiHomepageSectionTwoCategory.Find(itemId);
        }
        public List<XsiHomepageSectionTwoCategory> Select()
        {
            return XsiContext.Context.XsiHomepageSectionTwoCategory.ToList();
        }
        public List<XsiHomepageSectionTwoCategory> Select(XsiHomepageSectionTwoCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiHomepageSectionTwoCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiHomepageSectionTwoCategory.AsExpandable().Where(predicate).ToList();
        }

        public XsiHomepageSectionTwoCategory Add(XsiHomepageSectionTwoCategory entity)
        {
            XsiHomepageSectionTwoCategory HomepageSectionTwoCategory = new XsiHomepageSectionTwoCategory();
            HomepageSectionTwoCategory.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                HomepageSectionTwoCategory.Title = entity.Title.Trim();
            if (!entity.Url.IsDefault())
                HomepageSectionTwoCategory.Url = entity.Url.Trim();

            #region Ar
            HomepageSectionTwoCategory.IsActiveAr = entity.IsActive;
            if (!entity.TitleAr.IsDefault())
                HomepageSectionTwoCategory.TitleAr = entity.TitleAr.Trim();
            if (!entity.Urlar.IsDefault())
                HomepageSectionTwoCategory.Urlar = entity.Urlar.Trim();
            #endregion Ar

            HomepageSectionTwoCategory.CreatedOn = entity.CreatedOn;
            HomepageSectionTwoCategory.CreatedBy = entity.CreatedBy;
            HomepageSectionTwoCategory.ModifiedOn = entity.ModifiedOn;
            HomepageSectionTwoCategory.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiHomepageSectionTwoCategory.Add(HomepageSectionTwoCategory);
            SubmitChanges();
            return HomepageSectionTwoCategory;

        }
        public void Update(XsiHomepageSectionTwoCategory entity)
        {
            XsiHomepageSectionTwoCategory HomepageSectionTwoCategory = XsiContext.Context.XsiHomepageSectionTwoCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(HomepageSectionTwoCategory).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                HomepageSectionTwoCategory.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                HomepageSectionTwoCategory.Title = entity.Title.Trim();

            if (!entity.Url.IsDefault())
                HomepageSectionTwoCategory.Url = entity.Url.Trim();

            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                HomepageSectionTwoCategory.IsActiveAr = entity.IsActiveAr;

            if (!entity.TitleAr.IsDefault())
                HomepageSectionTwoCategory.TitleAr = entity.TitleAr.Trim();

            if (!entity.Urlar.IsDefault())
                HomepageSectionTwoCategory.Urlar = entity.Urlar.Trim();
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                HomepageSectionTwoCategory.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                HomepageSectionTwoCategory.ModifiedBy = entity.ModifiedBy;

        }
        public void Delete(XsiHomepageSectionTwoCategory entity)
        {
            foreach (XsiHomepageSectionTwoCategory HomepageSectionTwoCategory in Select(entity))
            {
                XsiHomepageSectionTwoCategory aHomepageSectionTwoCategory = XsiContext.Context.XsiHomepageSectionTwoCategory.Find(HomepageSectionTwoCategory.ItemId);
                XsiContext.Context.XsiHomepageSectionTwoCategory.Remove(aHomepageSectionTwoCategory);
            }
        }
        public void Delete(long itemId)
        {
            XsiHomepageSectionTwoCategory aHomepageSectionTwoCategory = XsiContext.Context.XsiHomepageSectionTwoCategory.Find(itemId);
            XsiContext.Context.XsiHomepageSectionTwoCategory.Remove(aHomepageSectionTwoCategory);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PublisherNewsPhotoGalleryRepository : IPublisherNewsPhotoGalleryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PublisherNewsPhotoGalleryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PublisherNewsPhotoGalleryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PublisherNewsPhotoGalleryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiPublisherNewsPhotoGallery GetById(long itemId)
        {
            return XsiContext.Context.XsiPublisherNewsPhotoGallery.Find(itemId);
        }
        public List<XsiPublisherNewsPhotoGallery> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiPublisherNewsPhotoGallery> Select()
        {
            return XsiContext.Context.XsiPublisherNewsPhotoGallery.ToList();
        }
        public List<XsiPublisherNewsPhotoGallery> Select(XsiPublisherNewsPhotoGallery entity)
        {
            var predicate = PredicateBuilder.True<XsiPublisherNewsPhotoGallery>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Detail.IsDefault())
                predicate = predicate.And(p => p.Detail.Contains(entity.Detail));

            //if (!entity.CategoryId.IsDefault())
            //    predicate = predicate.And(p => p.CategoryId == entity.CategoryId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.ImageName.IsDefault())
                predicate = predicate.And(p => p.ImageName.Equals(entity.ImageName));

            if (!entity.SortIndex.IsDefault())
                predicate = predicate.And(p => p.SortIndex == entity.SortIndex);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiPublisherNewsPhotoGallery.AsExpandable().Where(predicate).ToList();
        }

        public XsiPublisherNewsPhotoGallery Add(XsiPublisherNewsPhotoGallery entity)
        {
            XsiPublisherNewsPhotoGallery PublisherNewsPhotoGallery = new XsiPublisherNewsPhotoGallery();
            //PublisherNewsPhotoGallery.CategoryId = entity.CategoryId;
            if (!entity.Title.IsDefault())
                PublisherNewsPhotoGallery.Title = entity.Title.Trim();
            if (!entity.Detail.IsDefault())
                PublisherNewsPhotoGallery.Detail = entity.Detail.Trim();
            PublisherNewsPhotoGallery.IsActive = entity.IsActive;
            PublisherNewsPhotoGallery.ImageName = entity.ImageName;
            PublisherNewsPhotoGallery.SortIndex = entity.SortIndex;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                PublisherNewsPhotoGallery.TitleAr = entity.TitleAr.Trim();
            if (!entity.DetailAr.IsDefault())
                PublisherNewsPhotoGallery.DetailAr = entity.DetailAr.Trim();
            PublisherNewsPhotoGallery.IsActiveAr = entity.IsActiveAr;
            PublisherNewsPhotoGallery.ImageNameAr = entity.ImageNameAr;
            PublisherNewsPhotoGallery.SortIndexAr = entity.SortIndexAr;
            #endregion Ar

            PublisherNewsPhotoGallery.ModifiedOn = entity.ModifiedOn;
            PublisherNewsPhotoGallery.ModifiedBy = entity.ModifiedBy;
            PublisherNewsPhotoGallery.CreatedOn = entity.CreatedOn;
            PublisherNewsPhotoGallery.CreatedBy = entity.CreatedBy;

            XsiContext.Context.XsiPublisherNewsPhotoGallery.Add(PublisherNewsPhotoGallery);
            SubmitChanges();
            return PublisherNewsPhotoGallery;

        }
        public void Update(XsiPublisherNewsPhotoGallery entity)
        {
            XsiPublisherNewsPhotoGallery PublisherNewsPhotoGallery = XsiContext.Context.XsiPublisherNewsPhotoGallery.Find(entity.ItemId);
            XsiContext.Context.Entry(PublisherNewsPhotoGallery).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                PublisherNewsPhotoGallery.Title = entity.Title.Trim();

            if (!entity.Detail.IsDefault())
                PublisherNewsPhotoGallery.Detail = entity.Detail.Trim();

            if (!entity.IsActive.IsDefault())
                PublisherNewsPhotoGallery.IsActive = entity.IsActive;

            if (!entity.SortIndex.IsDefault())
                PublisherNewsPhotoGallery.SortIndex = entity.SortIndex;

            if (!entity.ImageName.IsDefault())
                PublisherNewsPhotoGallery.ImageName = entity.ImageName;

            if (!entity.ModifiedOn.IsDefault())
                PublisherNewsPhotoGallery.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                PublisherNewsPhotoGallery.ModifiedBy = entity.ModifiedBy;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                PublisherNewsPhotoGallery.TitleAr = entity.TitleAr.Trim();
            if (!entity.DetailAr.IsDefault())
                PublisherNewsPhotoGallery.DetailAr = entity.DetailAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                PublisherNewsPhotoGallery.IsActiveAr = entity.IsActiveAr;
            if (!entity.ImageNameAr.IsDefault())
                PublisherNewsPhotoGallery.ImageNameAr = entity.ImageNameAr;
            if (!entity.SortIndexAr.IsDefault())
                PublisherNewsPhotoGallery.SortIndexAr = entity.SortIndexAr;
            #endregion Ar
            //if (!entity.CategoryId.IsDefault())
            //    PublisherNewsPhotoGallery.CategoryId = entity.CategoryId;
        }
        public void Delete(XsiPublisherNewsPhotoGallery entity)
        {
            foreach (XsiPublisherNewsPhotoGallery PublisherNewsPhotoGallery in Select(entity))
            {
                XsiPublisherNewsPhotoGallery aPublisherNewsPhotoGallery = XsiContext.Context.XsiPublisherNewsPhotoGallery.Find(PublisherNewsPhotoGallery.ItemId);
                XsiContext.Context.XsiPublisherNewsPhotoGallery.Remove(aPublisherNewsPhotoGallery);
            }
        }
        public void Delete(long itemId)
        {
            XsiPublisherNewsPhotoGallery aPublisherNewsPhotoGallery = XsiContext.Context.XsiPublisherNewsPhotoGallery.Find(itemId);
            XsiContext.Context.XsiPublisherNewsPhotoGallery.Remove(aPublisherNewsPhotoGallery);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class CareerYOBRepository : ICareerYOBRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public CareerYOBRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public CareerYOBRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public CareerYOBRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiCareerYob GetById(long itemId)
        {
            return XsiContext.Context.XsiCareerYob.Find(itemId);
        }

        public List<XsiCareerYob> Select()
        {
            return XsiContext.Context.XsiCareerYob.ToList();
        }
        public List<XsiCareerYob> Select(XsiCareerYob entity)
        {
            var predicate = PredicateBuilder.True<XsiCareerYob>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiCareerYob.AsExpandable().Where(predicate).ToList();
        }

        public XsiCareerYob Add(XsiCareerYob entity)
        {
            XsiCareerYob CareerYOB = new XsiCareerYob();

            if (!entity.Title.IsDefault())
                CareerYOB.Title = entity.Title.Trim();
            CareerYOB.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerYOB.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerYOB.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            CareerYOB.CreatedOn = entity.CreatedOn;
            CareerYOB.CreatedBy = entity.CreatedBy;
            CareerYOB.ModifiedOn = entity.ModifiedOn;
            CareerYOB.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiCareerYob.Add(CareerYOB);
            SubmitChanges();
            return CareerYOB;

        }
        public void Update(XsiCareerYob entity)
        {
            XsiCareerYob CareerYOB = XsiContext.Context.XsiCareerYob.Find(entity.ItemId);
            XsiContext.Context.Entry(CareerYOB).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                CareerYOB.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                CareerYOB.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                CareerYOB.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                CareerYOB.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                CareerYOB.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                CareerYOB.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiCareerYob entity)
        {
            foreach (XsiCareerYob CareerYOB in Select(entity))
            {
                XsiCareerYob aCareerYOB = XsiContext.Context.XsiCareerYob.Find(CareerYOB.ItemId);
                XsiContext.Context.XsiCareerYob.Remove(aCareerYOB);
            }
        }
        public void Delete(long itemId)
        {
            XsiCareerYob aCareerYOB = XsiContext.Context.XsiCareerYob.Find(itemId);
            XsiContext.Context.XsiCareerYob.Remove(aCareerYOB);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
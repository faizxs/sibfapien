﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class PPSourceRepository : IPPSourceRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public PPSourceRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public PPSourceRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public PPSourceRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionProfessionalProgramSource GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionProfessionalProgramSource.Find(itemId);
        }
        
        public List<XsiExhibitionProfessionalProgramSource> Select()
        {
            return XsiContext.Context.XsiExhibitionProfessionalProgramSource.ToList();
        }
        public List<XsiExhibitionProfessionalProgramSource> Select(XsiExhibitionProfessionalProgramSource entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgramSource>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionProfessionalProgramSource.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionProfessionalProgramSource Add(XsiExhibitionProfessionalProgramSource entity)
        {
            XsiExhibitionProfessionalProgramSource PPSource = new XsiExhibitionProfessionalProgramSource();

            if (!entity.Title.IsDefault())
                PPSource.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                PPSource.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                PPSource.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                PPSource.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            PPSource.CreatedOn = entity.CreatedOn;
            PPSource.CreatedBy = entity.CreatedBy;
            PPSource.ModifiedOn = entity.ModifiedOn;
            PPSource.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiExhibitionProfessionalProgramSource.Add(PPSource);
            SubmitChanges();
            return PPSource;

        }
        public void Update(XsiExhibitionProfessionalProgramSource entity)
        {
            XsiExhibitionProfessionalProgramSource PPSource = XsiContext.Context.XsiExhibitionProfessionalProgramSource.Find(entity.ItemId);
            XsiContext.Context.Entry(PPSource).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                PPSource.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                PPSource.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                PPSource.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                PPSource.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                PPSource.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                PPSource.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionProfessionalProgramSource entity)
        {
            foreach (XsiExhibitionProfessionalProgramSource PPSource in Select(entity))
            {
                XsiExhibitionProfessionalProgramSource aPPSource = XsiContext.Context.XsiExhibitionProfessionalProgramSource.Find(PPSource.ItemId);
                XsiContext.Context.XsiExhibitionProfessionalProgramSource.Remove(aPPSource);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionProfessionalProgramSource aPPSource = XsiContext.Context.XsiExhibitionProfessionalProgramSource.Find(itemId);
            XsiContext.Context.XsiExhibitionProfessionalProgramSource.Remove(aPPSource);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFIllustrationSubmissionRepository : ISCRFIllustrationSubmissionRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFIllustrationSubmissionRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFIllustrationSubmissionRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFIllustrationSubmissionRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfillustrationSubmission GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfillustrationSubmission.Find(itemId);
        }

        public List<XsiScrfillustrationSubmission> Select()
        {
            return XsiContext.Context.XsiScrfillustrationSubmission.ToList();
        }
        public List<XsiScrfillustrationSubmission> Select(XsiScrfillustrationSubmission entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfillustrationSubmission>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IllustratorId.IsDefault())
                predicate = predicate.And(p => p.IllustratorId == entity.IllustratorId);

            if (!entity.UserPhotoId.IsDefault())
                predicate = predicate.And(p => p.UserPhotoId.Contains(entity.UserPhotoId));

            if (!entity.FileOriginial.IsDefault())
                predicate = predicate.And(p => p.FileOriginial.Contains(entity.FileOriginial));

            if (!entity.FilePreview.IsDefault())
                predicate = predicate.And(p => p.FilePreview.Contains(entity.FilePreview));

            if (!entity.FileFullView.IsDefault())
                predicate = predicate.And(p => p.FileFullView.Contains(entity.FileFullView));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Description.IsDefault())
                predicate = predicate.And(p => p.Description.Contains(entity.Description));

            if (!entity.CreateDate.IsDefault())
                predicate = predicate.And(p => p.CreateDate == entity.CreateDate);

            if (!entity.TechniqueOrMaterials.IsDefault())
                predicate = predicate.And(p => p.TechniqueOrMaterials == entity.TechniqueOrMaterials);

            if (!entity.SizeWidth.IsDefault())
                predicate = predicate.And(p => p.SizeWidth.Contains(entity.SizeWidth));

            if (!entity.SizeHeight.IsDefault())
                predicate = predicate.And(p => p.SizeHeight.Contains(entity.SizeHeight));

            if (!entity.Price.IsDefault())
                predicate = predicate.And(p => p.Price == entity.Price);

            if (!entity.IsForSale.IsDefault())
                predicate = predicate.And(p => p.IsForSale.Equals(entity.IsForSale));

            if (!entity.BookTitleEn.IsDefault())
                predicate = predicate.And(p => p.BookTitleEn.Contains(entity.BookTitleEn));

            if (!entity.BookTitleAr.IsDefault())
                predicate = predicate.And(p => p.BookTitleAr.Contains(entity.BookTitleAr));

            if (!entity.PublisherNameEn.IsDefault())
                predicate = predicate.And(p => p.PublisherNameEn.Contains(entity.PublisherNameEn));

            if (!entity.PublisherNameAr.IsDefault())
                predicate = predicate.And(p => p.PublisherNameAr.Contains(entity.PublisherNameAr));

            if (!entity.Address.IsDefault())
                predicate = predicate.And(p => p.Address.Contains(entity.Address));

            if (!entity.PublishYear.IsDefault())
                predicate = predicate.And(p => p.PublishYear.Contains(entity.PublishYear));

            if (!entity.IsPublished.IsDefault())
                predicate = predicate.And(p => p.IsPublished.Equals(entity.IsPublished));

            if (!entity.UsersRated.IsDefault())
                predicate = predicate.And(p => p.UsersRated == entity.UsersRated);

            if (!entity.Rating.IsDefault())
                predicate = predicate.And(p => p.Rating == entity.Rating);

            if (!entity.WinnerPosition.IsDefault())
                predicate = predicate.And(p => p.WinnerPosition == entity.WinnerPosition);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.IllustrationPrice.IsDefault())
                predicate = predicate.And(p => p.IllustrationPrice==entity.IllustrationPrice);

            if (!entity.CreateDateYear.IsDefault())
                predicate = predicate.And(p => p.CreateDateYear.Contains(entity.CreateDateYear));

            if (!entity.BookBrief.IsDefault())
                predicate = predicate.And(p => p.BookBrief.Contains(entity.BookBrief));

            if (!entity.BookBriefAr.IsDefault())
                predicate = predicate.And(p => p.BookBriefAr.Contains(entity.BookBriefAr));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            return XsiContext.Context.XsiScrfillustrationSubmission.AsExpandable().Where(predicate).ToList();
        }

        public XsiScrfillustrationSubmission Add(XsiScrfillustrationSubmission entity)
        {
            XsiScrfillustrationSubmission SCRFIllustrationSubmission = new XsiScrfillustrationSubmission();
            SCRFIllustrationSubmission.IsActive = entity.IsActive;
            SCRFIllustrationSubmission.IllustratorId = entity.IllustratorId;
            SCRFIllustrationSubmission.UserPhotoId = entity.UserPhotoId;
            SCRFIllustrationSubmission.FileOriginial = entity.FileOriginial;
            SCRFIllustrationSubmission.FilePreview = entity.FilePreview;
            SCRFIllustrationSubmission.FileFullView = entity.FileFullView;
            if (!entity.Title.IsDefault())
                SCRFIllustrationSubmission.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
                SCRFIllustrationSubmission.Description = entity.Description.Trim();
            SCRFIllustrationSubmission.CreateDate = entity.CreateDate;
            if (!entity.TechniqueOrMaterials.IsDefault())
                SCRFIllustrationSubmission.TechniqueOrMaterials = entity.TechniqueOrMaterials.Trim();
            if (!entity.SizeWidth.IsDefault())
                SCRFIllustrationSubmission.SizeWidth = entity.SizeWidth.Trim();
            if (!entity.SizeHeight.IsDefault())
                SCRFIllustrationSubmission.SizeHeight = entity.SizeHeight.Trim();
            if (!entity.Price.IsDefault())
                SCRFIllustrationSubmission.Price = entity.Price;
            SCRFIllustrationSubmission.IsForSale = entity.IsForSale;
            if (!entity.BookTitleEn.IsDefault())
                SCRFIllustrationSubmission.BookTitleEn = entity.BookTitleEn.Trim();
            if (!entity.BookTitleAr.IsDefault())
                SCRFIllustrationSubmission.BookTitleAr = entity.BookTitleAr.Trim();
            if (!entity.PublisherNameEn.IsDefault())
                SCRFIllustrationSubmission.PublisherNameEn = entity.PublisherNameEn.Trim();
            if (!entity.PublisherNameAr.IsDefault())
                SCRFIllustrationSubmission.PublisherNameAr = entity.PublisherNameAr.Trim();
            if (!entity.Address.IsDefault())
                SCRFIllustrationSubmission.Address = entity.Address.Trim();
            if (!entity.PublishYear.IsDefault())
                SCRFIllustrationSubmission.PublishYear = entity.PublishYear.Trim();
            SCRFIllustrationSubmission.IsPublished = entity.IsPublished;
            SCRFIllustrationSubmission.UsersRated = entity.UsersRated;
            SCRFIllustrationSubmission.Rating = entity.Rating;
            SCRFIllustrationSubmission.WinnerPosition = entity.WinnerPosition;
            SCRFIllustrationSubmission.Status = entity.Status;
            if (!entity.IllustrationPrice.IsDefault())
                SCRFIllustrationSubmission.IllustrationPrice = entity.IllustrationPrice;
            if (!entity.CreateDateYear.IsDefault())
                SCRFIllustrationSubmission.CreateDateYear = entity.CreateDateYear.Trim();
            if (!entity.BookBrief.IsDefault())
                SCRFIllustrationSubmission.BookBrief = entity.BookBrief.Trim();
            if (!entity.BookBriefAr.IsDefault())
                SCRFIllustrationSubmission.BookBriefAr = entity.BookBriefAr.Trim();
            SCRFIllustrationSubmission.CreatedOn = entity.CreatedOn;
            SCRFIllustrationSubmission.ModifiedOn = entity.ModifiedOn;

            XsiContext.Context.XsiScrfillustrationSubmission.Add(SCRFIllustrationSubmission);
            SubmitChanges();
            return SCRFIllustrationSubmission;

        }
        public void Update(XsiScrfillustrationSubmission entity)
        {
            XsiScrfillustrationSubmission SCRFIllustrationSubmission = XsiContext.Context.XsiScrfillustrationSubmission.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFIllustrationSubmission).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                SCRFIllustrationSubmission.IsActive = entity.IsActive;

            if (!entity.IllustratorId.IsDefault())
                SCRFIllustrationSubmission.IllustratorId = entity.IllustratorId;
            if (!entity.UserPhotoId.IsDefault())
                SCRFIllustrationSubmission.UserPhotoId = entity.UserPhotoId;
            if (!entity.FileOriginial.IsDefault())
                SCRFIllustrationSubmission.FileOriginial = entity.FileOriginial;
            if (!entity.FilePreview.IsDefault())
                SCRFIllustrationSubmission.FilePreview = entity.FilePreview;
            if (!entity.FileFullView.IsDefault())
                SCRFIllustrationSubmission.FileFullView = entity.FileFullView;
            if (!entity.Title.IsDefault())
                SCRFIllustrationSubmission.Title = entity.Title.Trim();
            if (!entity.Description.IsDefault())
                SCRFIllustrationSubmission.Description = entity.Description.Trim();
            if (!entity.CreateDate.IsDefault())
                SCRFIllustrationSubmission.CreateDate = entity.CreateDate;
            if (!entity.TechniqueOrMaterials.IsDefault())
                SCRFIllustrationSubmission.TechniqueOrMaterials = entity.TechniqueOrMaterials.Trim();
            if (!entity.SizeWidth.IsDefault())
                SCRFIllustrationSubmission.SizeWidth = entity.SizeWidth.Trim();
            if (!entity.SizeHeight.IsDefault())
                SCRFIllustrationSubmission.SizeHeight = entity.SizeHeight.Trim();
            if (!entity.Price.IsDefault())
                SCRFIllustrationSubmission.Price = entity.Price;
            if (!entity.IsForSale.IsDefault())
                SCRFIllustrationSubmission.IsForSale = entity.IsForSale;
            if (!entity.BookTitleEn.IsDefault())
                SCRFIllustrationSubmission.BookTitleEn = entity.BookTitleEn.Trim();
            if (!entity.BookTitleAr.IsDefault())
                SCRFIllustrationSubmission.BookTitleAr = entity.BookTitleAr.Trim();
            if (!entity.PublisherNameEn.IsDefault())
                SCRFIllustrationSubmission.PublisherNameEn = entity.PublisherNameEn.Trim();
            if (!entity.PublisherNameAr.IsDefault())
                SCRFIllustrationSubmission.PublisherNameAr = entity.PublisherNameAr.Trim();
            if (!entity.Address.IsDefault())
                SCRFIllustrationSubmission.Address = entity.Address.Trim();
            if (!entity.PublishYear.IsDefault())
                SCRFIllustrationSubmission.PublishYear = entity.PublishYear.Trim();
            if (!entity.IsPublished.IsDefault())
                SCRFIllustrationSubmission.IsPublished = entity.IsPublished;
            if (!entity.UsersRated.IsDefault())
                SCRFIllustrationSubmission.UsersRated = entity.UsersRated;
            if (!entity.Rating.IsDefault())
                SCRFIllustrationSubmission.Rating = entity.Rating;
            if (!entity.WinnerPosition.IsDefault())
                SCRFIllustrationSubmission.WinnerPosition = entity.WinnerPosition;
            if (!entity.IllustrationPrice.IsDefault())
                SCRFIllustrationSubmission.IllustrationPrice = entity.IllustrationPrice;
            if (!entity.CreateDateYear.IsDefault())
                SCRFIllustrationSubmission.CreateDateYear = entity.CreateDateYear.Trim();
            if (!entity.BookBrief.IsDefault())
                SCRFIllustrationSubmission.BookBrief = entity.BookBrief.Trim();
            if (!entity.BookBriefAr.IsDefault())
                SCRFIllustrationSubmission.BookBriefAr = entity.BookBriefAr.Trim();
            if (!entity.Status.IsDefault())
                SCRFIllustrationSubmission.Status = entity.Status;
            if (!entity.ModifiedOn.IsDefault())
                SCRFIllustrationSubmission.ModifiedOn = entity.ModifiedOn;
        }



        public void Delete(XsiScrfillustrationSubmission entity)
        {
            foreach (XsiScrfillustrationSubmission SCRFIllustrationSubmission in Select(entity))
            {
                XsiScrfillustrationSubmission aSCRFIllustrationSubmission = XsiContext.Context.XsiScrfillustrationSubmission.Find(SCRFIllustrationSubmission.ItemId);
                XsiContext.Context.XsiScrfillustrationSubmission.Remove(aSCRFIllustrationSubmission);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfillustrationSubmission aSCRFIllustrationSubmission = XsiContext.Context.XsiScrfillustrationSubmission.Find(itemId);
            XsiContext.Context.XsiScrfillustrationSubmission.Remove(aSCRFIllustrationSubmission);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
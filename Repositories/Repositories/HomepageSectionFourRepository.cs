﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class HomepageSectionFourRepository : IHomepageSectionFourRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public HomepageSectionFourRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public HomepageSectionFourRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public HomepageSectionFourRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiHomepageSectionFour GetById(long itemId)
        {
            return XsiContext.Context.XsiHomepageSectionFour.Find(itemId);
        }
        public List<XsiHomepageSectionFour> Select()
        {
            return XsiContext.Context.XsiHomepageSectionFour.ToList();
        }
        public List<XsiHomepageSectionFour> Select(XsiHomepageSectionFour entity)
        {
            var predicate = PredicateBuilder.True<XsiHomepageSectionFour>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.ViewAllUrl.IsDefault())
                predicate = predicate.And(p => p.ViewAllUrl.Contains(entity.ViewAllUrl));

            if (!entity.SubSectionOneTitle.IsDefault())
                predicate = predicate.And(p => p.SubSectionOneTitle.Contains(entity.SubSectionOneTitle));

            if (!entity.SubSectionOneOverview.IsDefault())
                predicate = predicate.And(p => p.SubSectionOneOverview.Contains(entity.SubSectionOneOverview));

            if (!entity.SubSectionOneImage.IsDefault())
                predicate = predicate.And(p => p.SubSectionOneImage.Contains(entity.SubSectionOneImage));

            if (!entity.SubSectionOneUrl.IsDefault())
                predicate = predicate.And(p => p.SubSectionOneUrl.Contains(entity.SubSectionOneUrl));

            if (!entity.SubSectionTwoTitle.IsDefault())
                predicate = predicate.And(p => p.SubSectionTwoTitle.Contains(entity.SubSectionTwoTitle));

            if (!entity.SubSectionTwoOverview.IsDefault())
                predicate = predicate.And(p => p.SubSectionTwoOverview.Contains(entity.SubSectionTwoOverview));

            if (!entity.SubSectionTwoImage.IsDefault())
                predicate = predicate.And(p => p.SubSectionTwoImage.Contains(entity.SubSectionTwoImage));

            if (!entity.SubSectionTwoUrl.IsDefault())
                predicate = predicate.And(p => p.SubSectionTwoUrl.Contains(entity.SubSectionTwoUrl));

            if (!entity.SubSectionThreeTitle.IsDefault())
                predicate = predicate.And(p => p.SubSectionThreeTitle.Contains(entity.SubSectionThreeTitle));

            if (!entity.SubSectionThreeOverview.IsDefault())
                predicate = predicate.And(p => p.SubSectionThreeOverview.Contains(entity.SubSectionThreeOverview));

            if (!entity.SubSectionThreeImage.IsDefault())
                predicate = predicate.And(p => p.SubSectionThreeImage.Contains(entity.SubSectionThreeImage));

            if (!entity.SubSectionThreeUrl.IsDefault())
                predicate = predicate.And(p => p.SubSectionThreeUrl.Contains(entity.SubSectionThreeUrl));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiHomepageSectionFour.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiHomepageSectionFour> SelectComeplete(XsiHomepageSectionFour entity, long langId)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiHomepageSectionFour>();
            var predicate = PredicateBuilder.True<XsiHomepageSectionFour>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (langId == 1)
            {
                if (!entity.Title.IsDefault())
                    predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title.ToLower()));
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    predicate = predicate.And(p => p.Title.Contains(entity.Title));
                    string strSearchText = entity.Title;
                    var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                    Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                        || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                        || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                        || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                        );

                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            //return XsiContext.Context.XsiHomepageSectionFoures.Include(p => p.XsiHomepageSectionFourCategory).AsExpandable().Where(predicate).ToList();
            return XsiContext.Context.XsiHomepageSectionFour.AsExpandable().Where(predicate).ToList();
        }

        public XsiHomepageSectionFour Add(XsiHomepageSectionFour entity)
        {
            XsiHomepageSectionFour HomepageSectionFour = new XsiHomepageSectionFour();
            if (!entity.Title.IsDefault())
                HomepageSectionFour.Title = entity.Title.Trim();
            HomepageSectionFour.ViewAllUrl = entity.ViewAllUrl;
            HomepageSectionFour.SubSectionOneTitle = entity.SubSectionOneTitle;
            HomepageSectionFour.SubSectionOneOverview = entity.SubSectionOneOverview;
            HomepageSectionFour.SubSectionOneImage = entity.SubSectionOneImage;
            HomepageSectionFour.SubSectionOneUrl = entity.SubSectionOneUrl;

            HomepageSectionFour.SubSectionTwoTitle = entity.SubSectionTwoTitle;
            HomepageSectionFour.SubSectionTwoOverview = entity.SubSectionTwoOverview;
            HomepageSectionFour.SubSectionTwoImage = entity.SubSectionTwoImage;
            HomepageSectionFour.SubSectionTwoUrl = entity.SubSectionTwoUrl;

            HomepageSectionFour.SubSectionThreeTitle = entity.SubSectionThreeTitle;
            HomepageSectionFour.SubSectionThreeOverview = entity.SubSectionThreeOverview;
            HomepageSectionFour.SubSectionThreeImage = entity.SubSectionThreeImage;
            HomepageSectionFour.SubSectionThreeUrl = entity.SubSectionThreeUrl;

            HomepageSectionFour.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                HomepageSectionFour.TitleAr = entity.TitleAr.Trim();
            HomepageSectionFour.ViewAllUrlar = entity.ViewAllUrlar;
            HomepageSectionFour.SubSectionOneTitleAr = entity.SubSectionOneTitleAr;
            HomepageSectionFour.SubSectionOneOverviewAr = entity.SubSectionOneOverviewAr;
            HomepageSectionFour.SubSectionOneImageAr = entity.SubSectionOneImageAr;
            HomepageSectionFour.SubSectionOneUrlar = entity.SubSectionOneUrlar;
            
            HomepageSectionFour.SubSectionTwoTitleAr = entity.SubSectionTwoTitleAr;
            HomepageSectionFour.SubSectionTwoOverviewAr = entity.SubSectionTwoOverviewAr;
            HomepageSectionFour.SubSectionTwoImageAr = entity.SubSectionTwoImageAr;
            HomepageSectionFour.SubSectionTwoUrlar = entity.SubSectionTwoUrlar;

            HomepageSectionFour.SubSectionThreeTitleAr = entity.SubSectionThreeTitleAr;
            HomepageSectionFour.SubSectionThreeOverviewAr = entity.SubSectionThreeOverviewAr;
            HomepageSectionFour.SubSectionThreeImageAr = entity.SubSectionThreeImageAr;
            HomepageSectionFour.SubSectionThreeUrlar = entity.SubSectionThreeUrlar;

            HomepageSectionFour.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            HomepageSectionFour.CreatedOn = entity.CreatedOn;
            HomepageSectionFour.CreatedBy = entity.CreatedBy;
            HomepageSectionFour.ModifiedOn = entity.ModifiedOn;
            HomepageSectionFour.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiHomepageSectionFour.Add(HomepageSectionFour);
            SubmitChanges();
            return HomepageSectionFour;

        }
        public void Update(XsiHomepageSectionFour entity)
        {
            XsiHomepageSectionFour HomepageSectionFour = XsiContext.Context.XsiHomepageSectionFour.Find(entity.ItemId);
            XsiContext.Context.Entry(HomepageSectionFour).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                HomepageSectionFour.Title = entity.Title.Trim();

            if (!entity.ViewAllUrl.IsDefault())
                HomepageSectionFour.ViewAllUrl = entity.ViewAllUrl;

            if (!entity.SubSectionOneTitle.IsDefault())
                HomepageSectionFour.SubSectionOneTitle = entity.SubSectionOneTitle;

            if (!entity.SubSectionOneOverview.IsDefault())
                HomepageSectionFour.SubSectionOneOverview = entity.SubSectionOneOverview;

            if (!entity.SubSectionOneImage.IsDefault())
                HomepageSectionFour.SubSectionOneImage = entity.SubSectionOneImage;

            if (!entity.SubSectionOneUrl.IsDefault())
                HomepageSectionFour.SubSectionOneUrl = entity.SubSectionOneUrl;

            if (!entity.SubSectionTwoTitle.IsDefault())
                HomepageSectionFour.SubSectionTwoTitle = entity.SubSectionTwoTitle;

            if (!entity.SubSectionTwoOverview.IsDefault())
                HomepageSectionFour.SubSectionTwoOverview = entity.SubSectionTwoOverview;

            if (!entity.SubSectionTwoImage.IsDefault())
                HomepageSectionFour.SubSectionTwoImage = entity.SubSectionTwoImage;

            if (!entity.SubSectionTwoUrl.IsDefault())
                HomepageSectionFour.SubSectionTwoUrl = entity.SubSectionTwoUrl;

            if (!entity.SubSectionThreeTitle.IsDefault())
                HomepageSectionFour.SubSectionThreeTitle = entity.SubSectionThreeTitle;

            if (!entity.SubSectionThreeOverview.IsDefault())
                HomepageSectionFour.SubSectionThreeOverview = entity.SubSectionThreeOverview;

            if (!entity.SubSectionThreeImage.IsDefault())
                HomepageSectionFour.SubSectionThreeImage = entity.SubSectionThreeImage;

            if (!entity.SubSectionThreeUrl.IsDefault())
                HomepageSectionFour.SubSectionThreeUrl = entity.SubSectionThreeUrl;

            if (!entity.IsActive.IsDefault())
                HomepageSectionFour.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                HomepageSectionFour.TitleAr = entity.TitleAr.Trim();

            if (!entity.ViewAllUrlar.IsDefault())
                HomepageSectionFour.ViewAllUrlar = entity.ViewAllUrlar;

            if (!entity.SubSectionOneTitleAr.IsDefault())
                HomepageSectionFour.SubSectionOneTitleAr = entity.SubSectionOneTitleAr;

            if (!entity.SubSectionOneOverviewAr.IsDefault())
                HomepageSectionFour.SubSectionOneOverviewAr = entity.SubSectionOneOverviewAr;

            if (!entity.SubSectionOneImageAr.IsDefault())
                HomepageSectionFour.SubSectionOneImageAr = entity.SubSectionOneImageAr;

            if (!entity.SubSectionOneUrlar.IsDefault())
                HomepageSectionFour.SubSectionOneUrlar = entity.SubSectionOneUrlar;

            if (!entity.SubSectionTwoTitleAr.IsDefault())
                HomepageSectionFour.SubSectionTwoTitleAr = entity.SubSectionTwoTitleAr;

            if (!entity.SubSectionTwoOverviewAr.IsDefault())
                HomepageSectionFour.SubSectionTwoOverviewAr = entity.SubSectionTwoOverviewAr;

            if (!entity.SubSectionTwoImageAr.IsDefault())
                HomepageSectionFour.SubSectionTwoImageAr = entity.SubSectionTwoImageAr;

            if (!entity.SubSectionTwoUrlar.IsDefault())
                HomepageSectionFour.SubSectionTwoUrlar = entity.SubSectionTwoUrlar;

            if (!entity.SubSectionThreeTitleAr.IsDefault())
                HomepageSectionFour.SubSectionThreeTitleAr = entity.SubSectionThreeTitleAr;

            if (!entity.SubSectionThreeOverviewAr.IsDefault())
                HomepageSectionFour.SubSectionThreeOverviewAr = entity.SubSectionThreeOverviewAr;

            if (!entity.SubSectionThreeImageAr.IsDefault())
                HomepageSectionFour.SubSectionThreeImageAr = entity.SubSectionThreeImageAr;

            if (!entity.SubSectionThreeUrlar.IsDefault())
                HomepageSectionFour.SubSectionThreeUrlar = entity.SubSectionThreeUrlar;

            if (!entity.IsActiveAr.IsDefault())
                HomepageSectionFour.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                HomepageSectionFour.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                HomepageSectionFour.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiHomepageSectionFour entity)
        {
            foreach (XsiHomepageSectionFour HomepageSectionFour in Select(entity))
            {
                XsiHomepageSectionFour aHomepageSectionFour = XsiContext.Context.XsiHomepageSectionFour.Find(HomepageSectionFour.ItemId);
                XsiContext.Context.XsiHomepageSectionFour.Remove(aHomepageSectionFour);
            }
        }
        public void Delete(long itemId)
        {
            XsiHomepageSectionFour aHomepageSectionFour = XsiContext.Context.XsiHomepageSectionFour.Find(itemId);
            XsiContext.Context.XsiHomepageSectionFour.Remove(aHomepageSectionFour);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
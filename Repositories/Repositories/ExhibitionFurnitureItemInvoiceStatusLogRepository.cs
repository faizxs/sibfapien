﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionFurnitureItemInvoiceStatusLogRepository : IExhibitionFurnitureItemInvoiceStatusLogRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionFurnitureItemInvoiceStatusLogRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionFurnitureItemInvoiceStatusLogRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionFurnitureItemInvoiceStatusLogRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionFurnitureItemStatusLogs GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionFurnitureItemStatusLogs.Find(itemId);
        }
        public List<XsiExhibitionFurnitureItemStatusLogs> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionFurnitureItemStatusLogs> Select()
        {
            return XsiContext.Context.XsiExhibitionFurnitureItemStatusLogs.ToList();
        }
        public List<XsiExhibitionFurnitureItemStatusLogs> Select(XsiExhibitionFurnitureItemStatusLogs entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionFurnitureItemStatusLogs>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.InvoiceId.IsDefault())
                predicate = predicate.And(p => p.InvoiceId == entity.InvoiceId);

            if (!entity.ExhibitorId.IsDefault())
                predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);
         
            if (!entity.AdminId.IsDefault())
                predicate = predicate.And(p => p.AdminId == entity.AdminId);

            if (!entity.GalaxyTeamId.IsDefault())
                predicate = predicate.And(p => p.GalaxyTeamId == entity.GalaxyTeamId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);


            return XsiContext.Context.XsiExhibitionFurnitureItemStatusLogs.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionFurnitureItemStatusLogs Add(XsiExhibitionFurnitureItemStatusLogs entity)
        {
            XsiExhibitionFurnitureItemStatusLogs InvoiceStatusLog = new XsiExhibitionFurnitureItemStatusLogs();
            InvoiceStatusLog.ExhibitorId = entity.ExhibitorId;
            InvoiceStatusLog.InvoiceId = entity.InvoiceId;
            if (!entity.GalaxyTeamId.IsDefault())
                InvoiceStatusLog.GalaxyTeamId = entity.GalaxyTeamId;
            InvoiceStatusLog.AdminId = entity.AdminId;
            InvoiceStatusLog.Status = entity.Status;
            InvoiceStatusLog.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiExhibitionFurnitureItemStatusLogs.Add(InvoiceStatusLog);
            SubmitChanges();
            return InvoiceStatusLog;

        }
        public void Update(XsiExhibitionFurnitureItemStatusLogs entity)
        {
            XsiExhibitionFurnitureItemStatusLogs InvoiceStatusLog = XsiContext.Context.XsiExhibitionFurnitureItemStatusLogs.Find(entity.ItemId);
            XsiContext.Context.Entry(InvoiceStatusLog).State = EntityState.Modified;

            if (!entity.Status.IsDefault())
                InvoiceStatusLog.Status = entity.Status;
        }
        public void Delete(XsiExhibitionFurnitureItemStatusLogs entity)
        {
            foreach (XsiExhibitionFurnitureItemStatusLogs InvoiceStatusLog in Select(entity))
            {
                XsiExhibitionFurnitureItemStatusLogs aInvoiceStatusLog = XsiContext.Context.XsiExhibitionFurnitureItemStatusLogs.Find(InvoiceStatusLog.ItemId);
                XsiContext.Context.XsiExhibitionFurnitureItemStatusLogs.Remove(aInvoiceStatusLog);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionFurnitureItemStatusLogs aInvoiceStatusLog = XsiContext.Context.XsiExhibitionFurnitureItemStatusLogs.Find(itemId);
            XsiContext.Context.XsiExhibitionFurnitureItemStatusLogs.Remove(aInvoiceStatusLog);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
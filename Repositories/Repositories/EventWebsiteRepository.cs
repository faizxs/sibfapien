﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class EventWebsiteRepository : IEventWebsiteRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public EventWebsiteRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public EventWebsiteRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public EventWebsiteRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiEventWebsite GetById(long itemId)
        {
            return XsiContext.Context.XsiEventWebsite.Find(itemId);
        }
        public List<XsiEventWebsite> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiEventWebsite> Select()
        {
            return XsiContext.Context.XsiEventWebsite.ToList();
        }
        public List<XsiEventWebsite> Select(XsiEventWebsite entity)
        {
            var predicate = PredicateBuilder.True<XsiEventWebsite>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.EventId.IsDefault())
                predicate = predicate.And(p => p.EventId == entity.EventId);


            return XsiContext.Context.XsiEventWebsite.AsExpandable().Where(predicate).ToList();
        }

        public XsiEventWebsite Add(XsiEventWebsite entity)
        {
            XsiEventWebsite EventWebsite = new XsiEventWebsite();
            EventWebsite.EventId = entity.EventId;
            EventWebsite.WebsiteId = entity.WebsiteId;

            XsiContext.Context.XsiEventWebsite.Add(EventWebsite);
            SubmitChanges();
            return EventWebsite;

        }
        public void Update(XsiEventWebsite entity)
        {
            XsiEventWebsite EventWebsite = XsiContext.Context.XsiEventWebsite.Find(entity.ItemId);
            XsiContext.Context.Entry(EventWebsite).State = EntityState.Modified;

            if (!entity.EventId.IsDefault())
                EventWebsite.EventId = entity.EventId;

            if (!entity.WebsiteId.IsDefault())
                EventWebsite.WebsiteId = entity.WebsiteId;
        }
        public void Delete(XsiEventWebsite entity)
        {
            foreach (XsiEventWebsite EventWebsite in Select(entity))
            {
                XsiEventWebsite aEventWebsite = XsiContext.Context.XsiEventWebsite.Find(EventWebsite.ItemId);
                XsiContext.Context.XsiEventWebsite.Remove(aEventWebsite);
            }
        }
        public void Delete(long itemId)
        {
            XsiEventWebsite aEventWebsite = XsiContext.Context.XsiEventWebsite.Find(itemId);
            XsiContext.Context.XsiEventWebsite.Remove(aEventWebsite);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;
using System.Web;

namespace Xsi.Repositories
{
    public class ExhibitionBookRepository : IExhibitionBookRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionBookRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionBookRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionBookRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionBooks GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionBooks.Find(itemId);
        }

        public List<XsiExhibitionBooks> Select()
        {
            return XsiContext.Context.XsiExhibitionBooks.ToList();
        }
        public List<XsiExhibitionBooks> Select(XsiExhibitionBooks entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBooks>();
            if (!entity.BookId.IsDefault())
                predicate = predicate.And(p => p.BookId == entity.BookId);
            #region UsefulCommented
            //if (!entity.EventusIDEn.IsDefault())
            //    predicate = predicate.And(p => p.EventusIDEn.Equals(entity.EventusIDEn));

            //if (!entity.EventusIDAr.IsDefault())
            //    predicate = predicate.And(p => p.EventusIDAr.Equals(entity.EventusIDAr));

            //if (!entity.AuthorId.IsDefault())
            //    predicate = predicate.And(p => p.AuthorId == entity.AuthorId);

            //if (!entity.PublisherId.IsDefault())
            //    predicate = predicate.And(p => p.PublisherId == entity.PublisherId);

            //if (!entity.ExhibitorId.IsDefault())
            //    predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

            //if (!entity.AgencyId.IsDefault())
            //    predicate = predicate.And(p => p.AgencyId == entity.AgencyId);

            //if (!entity.ExhibitionGroupId.IsDefault())
            //    predicate = predicate.And(p => p.ExhibitionGroupId == entity.ExhibitionGroupId);

            //if (!entity.SIBF_BookID.IsDefault())
            //    predicate = predicate.And(p => p.SIBF_BookID == entity.SIBF_BookID);
            #endregion UsefulCommented

            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsEnable.IsDefault())
                predicate = predicate.And(p => p.IsEnable.Equals(entity.IsEnable));

            if (!entity.ExhibitionCurrencyId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionCurrencyId == entity.ExhibitionCurrencyId);

            if (!entity.ExhibitionSubjectId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionSubjectId == entity.ExhibitionSubjectId);

            if (!entity.ExhibitionSubsubjectId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionSubsubjectId == entity.ExhibitionSubsubjectId);

            if (!entity.BookTypeId.IsDefault())
                predicate = predicate.And(p => p.BookTypeId == entity.BookTypeId);

            if (!entity.IsNew.IsDefault())
                predicate = predicate.And(p => p.IsNew.Equals(entity.IsNew));

            if (!entity.Thumbnail.IsDefault())
                predicate = predicate.And(p => p.Thumbnail == entity.Thumbnail);

            if (!entity.BookDetails.IsDefault())
                predicate = predicate.And(p => p.BookDetails.Contains(entity.BookDetails));

            if (!entity.BookDetailsAr.IsDefault())
                predicate = predicate.And(p => p.BookDetailsAr.Contains(entity.BookDetailsAr));

            if (!entity.IsAvailableOnline.IsDefault())
                predicate = predicate.And(p => p.IsAvailableOnline.Equals(entity.IsAvailableOnline));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr.ToLower().Contains(entity.TitleAr));

            if (!entity.TitleEn.IsDefault())
                predicate = predicate.And(p => p.TitleEn.ToLower().Contains(entity.TitleEn));

            if (!entity.AuthorName.IsDefault())
                predicate = predicate.And(p => p.AuthorName.ToLower().Contains(entity.AuthorName));

            if (!entity.AuthorNameAr.IsDefault())
                predicate = predicate.And(p => p.AuthorNameAr.ToLower().Contains(entity.AuthorNameAr));

            if (!entity.BookPublisherName.IsDefault())
                predicate = predicate.And(p => p.BookPublisherName.ToLower().Contains(entity.BookPublisherName));

            if (!entity.BookPublisherNameAr.IsDefault())
                predicate = predicate.And(p => p.BookPublisherNameAr.ToLower().Contains(entity.BookPublisherNameAr));

            if (!entity.Isbn.IsDefault())
                predicate = predicate.And(p => p.Isbn.Contains(entity.Isbn));

            if (!entity.IssueYear.IsDefault())
                predicate = predicate.And(p => p.IssueYear.Contains(entity.IssueYear));

            if (!entity.Price.IsDefault())
                predicate = predicate.And(p => p.Price == entity.Price);

            if (!entity.PriceBeforeDiscount.IsDefault())
                predicate = predicate.And(p => p.PriceBeforeDiscount == entity.PriceBeforeDiscount);

            if (!entity.IsBestSeller.IsDefault())
                predicate = predicate.And(p => p.IsBestSeller.Equals(entity.IsBestSeller));

            if (!entity.ClickCount.IsDefault())
                predicate = predicate.And(p => p.ClickCount == entity.ClickCount);

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            return CachingRepository.CachedBooks.AsQueryable().Where(predicate).ToList();
        }
        public List<XsiExhibitionBookForUi> SelectBooksForUI(XsiExhibitionBooks entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBookParticipating>();


            //if (!entity.ExhibitionCurrencyId.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.ExhibitionCurrencyId.Equals(entity.ExhibitionCurrencyId));

            //if (!entity.AuthorId.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.AuthorId == entity.AuthorId);

            //if (!entity.PublisherId.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.PublisherId == entity.PublisherId);

            //if (!entity.ExhibitorId.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.ExhibitorId == entity.ExhibitorId);

            //if (!entity.AgencyId.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.AgencyId == entity.AgencyId);

            //if (!entity.MemberId.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.MemberId == entity.MemberId);


            //if (!entity.IsActive.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.IsActive.Equals(entity.IsActive));

            //if (!entity.IsEnable.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.IsEnable.Equals(entity.IsEnable));

            //if (!entity.ExhibitionSubjectId.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.ExhibitionSubjectId.Equals(entity.ExhibitionSubjectId));

            //if (!entity.ExhibitionSubsubjectId.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.ExhibitionSubsubjectId.Equals(entity.ExhibitionSubsubjectId));

            //if (!entity.BookTypeId.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.BookTypeId == entity.BookTypeId);

            //if (!entity.IsNew.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.IsNew.Equals(entity.IsNew));

            //if (!entity.TitleAr.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.TitleAr != null && p.XsiExhibitionBooks.TitleAr.Contains(entity.TitleAr));

            //if (!entity.TitleEn.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.TitleEn != null && p.XsiExhibitionBooks.TitleEn.Contains(entity.TitleEn));

            //if (!entity.ISBN.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.ISBN != null && p.XsiExhibitionBooks.ISBN.Contains(entity.ISBN));

            //if (!entity.IssueYear.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.IssueYear != null && p.XsiExhibitionBooks.IssueYear.Contains(entity.IssueYear));

            //if (!entity.Price.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.Price != null && p.XsiExhibitionBooks.Price == entity.Price);

            //if (!entity.IsBestSeller.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.IsBestSeller.Equals(entity.IsBestSeller));

            //if (!entity.PriceBeforeDiscount.IsDefault())
            //    predicate = predicate.And(p => p.XsiExhibitionBooks.PriceBeforeDiscount == entity.PriceBeforeDiscount);

            //predicate = predicate.And(p => p.IsActive == "Y");

            //////////////////////////////////////////////////////////////////////////////

            //if (CachingRepository.CachedBooksParticipating == null)
            //    CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBooksParticipatings.ToList();


            //var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.ExhibitionGroupId == entity.ExhibitionGroupId && p.IsActive == "Y" && p.BookId != null && (ExhibitorIDs.Contains(p.ExhibitorId.Value) || AgencyIDs.Contains(p.AgencyId.Value))).Select(s => s.BookId.Value).Distinct());
            //if (CachingRepository.CachedBooks == null)
            //    CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //// above 1.5 items in books, 84,000 to be search in contains.
            //var SearchLists = CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.ItemId)).ToList();

            //return SearchLists.AsQueryable().Where(predicate).Select(p =>
            // new XsiExhibitionBookForUi
            // {
            //     ItemId = p.ItemId,
            //     Thumbnail = p.Thumbnail,
            //     TitleAr = p.TitleAr,
            //     TitleEn = p.TitleEn,
            //     Price = p.Price,
            //     AuthorId = p.AuthorId,
            //     IsBestSeller = p.IsBestSeller,
            //     BookTypeId = p.BookTypeId,
            //     ExhibitionCurrencyId = p.ExhibitionCurrencyId,
            // }
            // ).ToList();
            //////////////////////////////////////////////////////////////////////////////



            return XsiContext.Context.XsiExhibitionBookParticipating/*.Include(p => p.XsiExhibitionBooks)*/.AsExpandable()
                /*.Where(predicate)*/.Select(p =>
                new XsiExhibitionBookForUi
                {
                    //BookId = p.XsiExhibitionBooks.BookId,
                    //EventusIDEn = p.XsiExhibitionBooks.EventusIDEn,
                    //EventusIDAr = p.XsiExhibitionBooks.EventusIDAr,
                    //Thumbnail = p.XsiExhibitionBooks.Thumbnail,
                    //TitleAr = p.XsiExhibitionBooks.TitleAr,
                    //TitleEn = p.XsiExhibitionBooks.TitleEn,
                    //Price = p.XsiExhibitionBooks.Price.ToString(),
                    ////AuthorId = p.XsiExhibitionBooks.AuthorId,
                    //BookTypeId = p.XsiExhibitionBooks.BookTypeId,

                    //ISBN = p.XsiExhibitionBooks.ISBN,
                    //IsNew = p.XsiExhibitionBooks.IsNew,
                    //IssueYear = p.XsiExhibitionBooks.IssueYear,
                    //PriceBeforeDiscount = p.XsiExhibitionBooks.PriceBeforeDiscount,
                    //ExhibitionSubjectId = p.XsiExhibitionBooks.ExhibitionSubjectId,
                    //ExhibitionSubsubjectId = p.XsiExhibitionBooks.ExhibitionSubsubjectId,
                    //ExhibitionCurrencyId = p.XsiExhibitionBooks.ExhibitionCurrencyId,
                    //IsBestSeller = p.XsiExhibitionBooks.IsBestSeller,
                    //PublisherId = p.XsiExhibitionBooks.PublisherId,
                    //ExhibitorId = p.XsiExhibitionBooks.ExhibitorId,
                    //AgencyId = p.XsiExhibitionBooks.AgencyId,
                }
                ).ToList();
        }
        public List<XsiExhibitionBookForUi> SelectBooksUIAnd(XsiExhibitionBooks entity, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            //using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //{
            var predicate = PredicateBuilder.True<XsiExhibitionBooks>();
            var predicateOrSubject = PredicateBuilder.False<XsiExhibitionBooks>();
            var predicateOrSubsubject = PredicateBuilder.False<XsiExhibitionBooks>();
            var predicateOrBookType = PredicateBuilder.False<XsiExhibitionBooks>();

            if (!entity.ExhibitionCurrencyId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionCurrencyId == entity.ExhibitionCurrencyId);

            if (!entity.BookPublisherId.IsDefault())
                predicate = predicate.And(p => p.BookPublisherId == entity.BookPublisherId);

            //if (!entity.AuthorId.IsDefault())
            //    predicate = predicate.And(p => p.AuthorId == entity.AuthorId);

            //if (!entity.ExhibitorId.IsDefault())
            //    predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

            //if (!entity.AgencyId.IsDefault())
            //    predicate = predicate.And(p => p.AgencyId == entity.AgencyId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsEnable.IsDefault())
                predicate = predicate.And(p => p.IsEnable.Equals(entity.IsEnable));

            if (!entity.IsBestSeller.IsDefault())
                predicate = predicate.And(p => p.IsBestSeller.Equals(entity.IsBestSeller));

            if (!string.IsNullOrEmpty(subjectIDs))
            {
                string[] strArray = subjectIDs.Split(',');
                foreach (string str in strArray)
                    predicateOrSubject = predicateOrSubject.Or(p => p.ExhibitionSubjectId == Convert.ToInt64(str));
                predicate = predicate.And(predicateOrSubject);
            }

            if (!string.IsNullOrEmpty(subsubjectIDs))
            {
                string[] strArray = subsubjectIDs.Split(',');
                foreach (string str in strArray)
                    predicateOrSubsubject = predicateOrSubsubject.Or(p => p.ExhibitionSubsubjectId == Convert.ToInt64(str));
                predicate = predicate.And(predicateOrSubsubject);
            }

            if (!string.IsNullOrEmpty(bookTypeIDs))
            {
                string[] strArray = bookTypeIDs.Split(',');
                foreach (string str in strArray)
                    predicateOrBookType = predicateOrBookType.Or(p => p.BookTypeId == Convert.ToInt64(str));
                predicate = predicate.And(predicateOrBookType);
            }

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr != null && p.TitleAr.Contains(entity.TitleAr));

            if (!entity.TitleEn.IsDefault())
                predicate = predicate.And(p => p.TitleEn != null && p.TitleEn.ToLower().Contains(entity.TitleEn.ToLower()));

            if (!entity.AuthorNameAr.IsDefault())
                predicate = predicate.And(p => p.AuthorNameAr != null && p.AuthorNameAr.Contains(entity.AuthorNameAr));

            if (!entity.AuthorName.IsDefault())
                predicate = predicate.And(p => p.AuthorName != null && p.AuthorName.ToLower().Contains(entity.AuthorName.ToLower()));

            if (!entity.BookPublisherName.IsDefault())
                predicate = predicate.And(p => p.BookPublisherName != null && p.BookPublisherName.Contains(entity.BookPublisherName));

            if (!entity.BookPublisherNameAr.IsDefault())
                predicate = predicate.And(p => p.BookPublisherNameAr != null && p.BookPublisherNameAr.ToLower().Contains(entity.BookPublisherNameAr.ToLower()));

            if (!entity.Isbn.IsDefault())
                predicate = predicate.And(p => p.Isbn != null && p.Isbn.Contains(entity.Isbn));

            if (!entity.IssueYear.IsDefault())
                predicate = predicate.And(p => p.IssueYear != null && p.IssueYear.Contains(entity.IssueYear));

            if (priceFrom != 0)
                predicate = predicate.And(p => Convert.ToDouble(p.Price) >= priceFrom);

            if (priceTo != 0)
                predicate = predicate.And(p => Convert.ToDouble(p.Price) <= priceTo);

            if (CachingRepository.CachedBooksParticipating == null)
            {
                //XsiContext.Context.Query("select 1 A, 2 B union all select 3, 4");
                CachingRepository.CachedBooksParticipating = (IList<XsiExhibitionBookParticipating>)XsiContext.Context.XsiExhibitionBookParticipating.ToList();
            }
            //else
            //{
            //    var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooksParticipating");
            //    if (CachingRepository.CachedBooksParticipating.Count() != count)
            //        CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBooksParticipatings.ToList();
            //}

            //HashSet<long> ExhibitorIDs = new HashSet<long>(XsiContext.Context.GetApprovedExhibitorIds(websiteId).Select(s => s.Value));
            //HashSet<long> AgencyIDs = new HashSet<long>(XsiContext.Context.GetApproveAgencyIds(websiteId).Select(s => s.Value));

            var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p =>/* p.ExhibitionGroupId == entity.ExhibitionGroupId &&*/ p.IsActive == "Y" && p.BookId > 0
            /*&& (ExhibitorIDs.Contains(p.ExhibitorId.Value) || AgencyIDs.Contains(p.AgencyId.Value))*/).Select(s => s.BookId).Distinct());
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.Take(100000).ToList();
            //else
            //{
            //    var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooks");
            //    if (CachingRepository.CachedBooks.Count() != count)
            //        CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //}
            // above 1.5 items in books, 84,000 to be search in contains.
            var SearchLists = CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.BookId)).ToList();

            return SearchLists.AsQueryable().Where(predicate).Select(p =>
             new XsiExhibitionBookForUi
             {
                 BookId = p.BookId,
                 Thumbnail = p.Thumbnail,
                 //EventusIDEn = p.EventusIDEn,
                 //EventusIDAr = p.EventusIDAr,
                 TitleAr = p.TitleAr,
                 TitleEn = p.TitleEn,
                 Price = p.Price.ToString(),
                 AuthorName = p.AuthorName,
                 PublisherName = p.BookPublisherName,
                 //AuthorId = p.AuthorId,
                 // PublisherId = p.BookPublisherId,
                 IsBestSeller = p.IsBestSeller,
                 BookTypeId = p.BookTypeId,
                 ExhibitionCurrencyId = p.ExhibitionCurrencyId,
                 ExhibitionSubjectId = p.ExhibitionSubjectId,
                 ExhibitionSubsubjectId = p.ExhibitionSubsubjectId,
                 Isbn = p.Isbn,
                 IssueYear = p.IssueYear,
                 //ExhibitorId = p.ExhibitorId
             }
             ).ToList();
            //}
        }
        public List<XsiExhibitionBookForUi> SelectBooksUIAnd1(XsiExhibitionBooks entity, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            //using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //{
            var predicate = PredicateBuilder.True<XsiExhibitionBooks>();
            var predicateOrSubject = PredicateBuilder.False<XsiExhibitionBooks>();
            var predicateOrSubsubject = PredicateBuilder.False<XsiExhibitionBooks>();
            var predicateOrBookType = PredicateBuilder.False<XsiExhibitionBooks>();

            if (!entity.ExhibitionCurrencyId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionCurrencyId == entity.ExhibitionCurrencyId);

            if (!entity.BookPublisherId.IsDefault())
                predicate = predicate.And(p => p.BookPublisherId == entity.BookPublisherId);

            //if (!entity.AuthorId.IsDefault())
            //    predicate = predicate.And(p => p.AuthorId == entity.AuthorId);

            //if (!entity.ExhibitorId.IsDefault())
            //    predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

            //if (!entity.AgencyId.IsDefault())
            //    predicate = predicate.And(p => p.AgencyId == entity.AgencyId);


            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsEnable.IsDefault())
                predicate = predicate.And(p => p.IsEnable.Equals(entity.IsEnable));

            if (!entity.IsBestSeller.IsDefault())
                predicate = predicate.And(p => p.IsBestSeller.Equals(entity.IsBestSeller));

            if (!entity.ExhibitionSubjectId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionSubjectId.Equals(entity.ExhibitionSubjectId));

            if (!entity.ExhibitionSubsubjectId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionSubsubjectId.Equals(entity.ExhibitionSubsubjectId));

            if (!entity.BookTypeId.IsDefault())
                predicate = predicate.And(p => p.BookTypeId.Equals(entity.BookTypeId));

            //if (!string.IsNullOrEmpty(subjectIDs))
            //{
            //    string[] strArray = subjectIDs.Split(',');
            //    foreach (string str in strArray)
            //        predicateOrSubject = predicateOrSubject.Or(p => p.ExhibitionSubjectId == Convert.ToInt64(str));
            //    predicate = predicate.And(predicateOrSubject);
            //}

            //if (!string.IsNullOrEmpty(subsubjectIDs))
            //{
            //    string[] strArray = subsubjectIDs.Split(',');
            //    foreach (string str in strArray)
            //        predicateOrSubsubject = predicateOrSubsubject.Or(p => p.ExhibitionSubsubjectId == Convert.ToInt64(str));
            //    predicate = predicate.And(predicateOrSubsubject);
            //}

            //if (!string.IsNullOrEmpty(bookTypeIDs))
            //{
            //    string[] strArray = bookTypeIDs.Split(',');
            //    foreach (string str in strArray)
            //        predicateOrBookType = predicateOrBookType.Or(p => p.BookTypeId == Convert.ToInt64(str));
            //    predicate = predicate.And(predicateOrBookType);
            //}

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr != null && p.TitleAr.Contains(entity.TitleAr));

            if (!entity.TitleEn.IsDefault())
                predicate = predicate.And(p => p.TitleEn != null && p.TitleEn.ToLower().Contains(entity.TitleEn.ToLower()));

            if (!entity.Isbn.IsDefault())
                predicate = predicate.And(p => p.Isbn != null && p.Isbn.Contains(entity.Isbn));

            if (!entity.IssueYear.IsDefault())
                predicate = predicate.And(p => p.IssueYear != null && p.IssueYear.Contains(entity.IssueYear));

            if (priceFrom != 0)
                predicate = predicate.And(p => Convert.ToDouble(p.Price) >= priceFrom);

            if (priceTo != 0)
                predicate = predicate.And(p => Convert.ToDouble(p.Price) <= priceTo);

            if (CachingRepository.CachedBooksParticipating == null)
            {

                //XsiContext.Context.Query("select 1 A, 2 B union all select 3, 4");
                //below commented line useful
                //CachingRepository.CachedBooksParticipating = db.Query<XsiExhibitionBooksParticipating>("select * from XsiExhibitionBooksParticipating").ToList();//XsiContext.Context.XsiExhibitionBooksParticipatings.ToList();
            }
            //else
            //{
            //    var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooksParticipating");
            //    if (CachingRepository.CachedBooksParticipating.Count() != count)
            //        CachingRepository.CachedBooksParticipating = db.Query<XsiExhibitionBooksParticipating>("select * from XsiExhibitionBooksParticipating").ToList();
            //}

            //HashSet<long> ExhibitorIDs = new HashSet<long>(XsiContext.Context.GetApprovedExhibitorIds(websiteId).Select(s => s.Value));
            //HashSet<long> AgencyIDs = new HashSet<long>(XsiContext.Context.GetApproveAgencyIds(websiteId).Select(s => s.Value));
            //HashSet<long> ExhibitorIDs = new HashSet<long>(db.Query<long>("GetApprovedExhibitorIds", new { WebsiteId = websiteId }, commandType: CommandType.StoredProcedure));
            //HashSet<long> AgencyIDs = new HashSet<long>(db.Query<long>("GetApproveAgencyIds", new { WebsiteId = websiteId }, commandType: CommandType.StoredProcedure));

            var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.IsActive == "Y" && p.BookId > 0
            /*&& (ExhibitorIDs.Contains(p.ExhibitorId) || AgencyIDs.Contains(p.AgencyId.Value))*/).Select(s => s.BookId).Distinct());
            //if (CachingRepository.CachedBooks == null)
            //    CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.Take(100000).ToList();
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //else
            //{
            //    var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooks");
            //    if (CachingRepository.CachedBooks.Count() != count)
            //        CachingRepository.CachedBooks = db.Query<XsiExhibitionBooks>("select * from XsiExhibitionBooks").ToList();
            //}
            // above 1.5 items in books, 84,000 to be search in contains.
            var SearchLists = CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.BookId)).ToList();

            return SearchLists.AsQueryable().Where(predicate).Select(p =>
             new XsiExhibitionBookForUi
             {
                 BookId = p.BookId,
                 //EventusIDEn = p.EventusIDEn,
                 //EventusIDAr = p.EventusIDAr,
                 Thumbnail = p.Thumbnail,
                 TitleAr = p.TitleAr,
                 TitleEn = p.TitleEn,
                 Price = p.Price.ToString(),
                 AuthorName = p.AuthorName,
                 PublisherName = p.BookPublisherName,
                 //PublisherId = p.BookPublisherId,
                 IsBestSeller = p.IsBestSeller,
                 BookTypeId = p.BookTypeId,
                 ExhibitionCurrencyId = p.ExhibitionCurrencyId,
                 ExhibitionSubjectId = p.ExhibitionSubjectId,
                 ExhibitionSubsubjectId = p.ExhibitionSubsubjectId,
                 Isbn = p.Isbn,
                 IssueYear = p.IssueYear,
                 //ExhibitorId = p.ExhibitorId
             }
             ).ToList();
            // }
        }
        public List<XsiExhibitionBookForUi> SelectBooksUIOr(XsiExhibitionBooks entity, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBooks>();
            var predicateOr = PredicateBuilder.False<XsiExhibitionBooks>();
            var predicateOrSubject = PredicateBuilder.False<XsiExhibitionBooks>();
            var predicateOrSubsubject = PredicateBuilder.False<XsiExhibitionBooks>();
            var predicateOrBookType = PredicateBuilder.False<XsiExhibitionBooks>();

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsEnable.IsDefault())
                predicate = predicate.And(p => p.IsEnable.Equals(entity.IsEnable));

            if (!entity.IsBestSeller.IsDefault())
                predicate = predicate.And(p => p.IsBestSeller.Equals(entity.IsBestSeller));

            if (!string.IsNullOrEmpty(subjectIDs))
            {
                string[] strArray = subjectIDs.Split(',');
                foreach (string str in strArray)
                    predicateOrSubject = predicateOrSubject.Or(p => p.ExhibitionSubjectId == Convert.ToInt64(str));
                predicate = predicate.And(predicateOrSubject);
            }

            if (!string.IsNullOrEmpty(subsubjectIDs))
            {
                string[] strArray = subsubjectIDs.Split(',');
                foreach (string str in strArray)
                    predicateOrSubsubject = predicateOrSubsubject.Or(p => p.ExhibitionSubsubjectId == Convert.ToInt64(str));
                predicate = predicate.And(predicateOrSubsubject);
            }

            if (!string.IsNullOrEmpty(bookTypeIDs))
            {
                string[] strArray = bookTypeIDs.Split(',');
                foreach (string str in strArray)
                    predicateOrBookType = predicateOrBookType.Or(p => p.BookTypeId == Convert.ToInt64(str));
                predicate = predicate.And(predicateOrBookType);
            }

            if (!entity.ExhibitionCurrencyId.IsDefault())
            {
                predicateOr = predicateOr.Or(p => p.ExhibitionCurrencyId == entity.ExhibitionCurrencyId);
                predicate = predicate.And(predicateOr);
            }

            if (!entity.BookPublisherId.IsDefault())
            {
                predicateOr = predicateOr.Or(p => p.BookPublisherId == entity.BookPublisherId);
                predicate = predicate.And(predicateOr);
            }

            //if (!entity.ExhibitorId.IsDefault())
            //{
            //    predicateOr = predicateOr.Or(p => p.ExhibitorId == entity.ExhibitorId);
            //    predicate = predicate.And(predicateOr);
            //}

            //if (!entity.AgencyId.IsDefault())
            //{
            //    predicateOr = predicateOr.Or(p => p.AgencyId == entity.AgencyId);
            //    predicate = predicate.And(predicateOr);
            //}

            if (!entity.TitleAr.IsDefault())
            {
                predicateOr = predicateOr.Or(p => p.TitleAr != null && p.TitleAr.Contains(entity.TitleAr));
                predicate = predicate.And(predicateOr);
            }

            if (!entity.TitleEn.IsDefault())
            {
                predicateOr = predicateOr.Or(p => p.TitleEn != null && p.TitleEn.ToLower().Contains(entity.TitleEn.ToLower()));
                predicate = predicate.And(predicateOr);
            }

            if (!entity.Isbn.IsDefault())
            {
                predicateOr = predicateOr.Or(p => p.Isbn != null && p.Isbn.Contains(entity.Isbn));
                predicate = predicate.And(predicateOr);
            }

            if (!entity.IssueYear.IsDefault())
            {
                predicateOr = predicateOr.Or(p => p.IssueYear != null && p.IssueYear.Contains(entity.IssueYear));
                predicate = predicate.And(predicateOr);
            }

            if (priceFrom != 0)
            {
                predicateOr = predicateOr.Or(p => Convert.ToDouble(p.Price) >= priceFrom);
                predicate = predicate.And(predicateOr);
            }

            if (priceTo != 0)
            {
                predicateOr = predicateOr.Or(p => Convert.ToDouble(p.Price) <= priceTo);
                predicate = predicate.And(predicateOr);
            }

            if (CachingRepository.CachedBooksParticipating == null)
                CachingRepository.CachedBooksParticipating = (IList<XsiExhibitionBookParticipating>)XsiContext.Context.XsiExhibitionBookParticipating.ToList();
            //else
            //{
            //    using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //    {
            //        var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooksParticipating");
            //        if (CachingRepository.CachedBooksParticipating.Count() != count)
            //            CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBooksParticipatings.ToList();
            //    }
            //}

            //HashSet<long> ExhibitorIDs = new HashSet<long>(XsiContext.Context.GetApprovedExhibitorIds(websiteId).Select(s => s.Value));
            //HashSet<long> AgencyIDs = new HashSet<long>(XsiContext.Context.GetApproveAgencyIds(websiteId).Select(s => s.Value));

            var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.IsActive == "Y" && p.BookId > 0
            /*&& (ExhibitorIDs.Contains(p.ExhibitorId.Value) || AgencyIDs.Contains(p.AgencyId.Value))*/).Select(s => s.BookId).Distinct());
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //else
            //{
            //    using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //    {
            //        var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooks");
            //        if (CachingRepository.CachedBooks.Count() != count)
            //            CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //    }
            //}
            // above 1.5 items in books, 84,000 to be search in contains.
            var SearchLists = CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.BookId)).ToList();

            return SearchLists.AsQueryable().Where(predicate).Select(p =>
             new XsiExhibitionBookForUi
             {
                 BookId = p.BookId,

                 //EventusIDEn = p.EventusIDEn,
                 //EventusIDAr = p.EventusIDAr,
                 Thumbnail = p.Thumbnail,
                 TitleAr = p.TitleAr,
                 TitleEn = p.TitleEn,
                 Price = p.Price.ToString(),
                 AuthorName = p.AuthorName,
                 PublisherName = p.BookPublisherName,
                 IsBestSeller = p.IsBestSeller,
                 BookTypeId = p.BookTypeId,
                 ExhibitionCurrencyId = p.ExhibitionCurrencyId,
             }
             ).ToList();
        }
        public List<XsiExhibitionBookForUi> SelectBooksUITopSearchable(XsiExhibitionBooks entity, long websiteId)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBooks>();

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsEnable.IsDefault())
                predicate = predicate.And(p => p.IsEnable.Equals(entity.IsEnable));

            if (CachingRepository.CachedBooksParticipating == null)
                CachingRepository.CachedBooksParticipating = (IList<XsiExhibitionBookParticipating>)XsiContext.Context.XsiExhibitionBookParticipating.ToList();
            //else
            //{
            //    using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //    {
            //        var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooksParticipating");
            //        if (CachingRepository.CachedBooksParticipating.Count() != count)
            //            CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBooksParticipatings.ToList();
            //    }
            //}

            //HashSet<long> ExhibitorIDs = new HashSet<long>(XsiContext.Context.GetApprovedExhibitorIds(websiteId).Select(s => s.Value));
            //HashSet<long> AgencyIDs = new HashSet<long>(XsiContext.Context.GetApproveAgencyIds(websiteId).Select(s => s.Value));

            var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.IsActive == "Y" && p.BookId > 0
            /*&& (ExhibitorIDs.Contains(p.ExhibitorId.Value) || AgencyIDs.Contains(p.AgencyId.Value))*/).Select(s => s.BookId).Distinct());
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //else
            //{
            //    using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //    {
            //        var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooks");
            //        if (CachingRepository.CachedBooks.Count() != count)
            //            CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //    }
            //}
            // above 1.5 items in books, 84,000 to be search in contains.
            var SearchLists = CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.BookId)).OrderByDescending(o => o.ClickCount).Take(6).ToList();

            return SearchLists.AsQueryable().Where(predicate).Select(p =>
             new XsiExhibitionBookForUi
             {
                 BookId = p.BookId,
                 Thumbnail = p.Thumbnail,
                 TitleAr = p.TitleAr,
                 TitleEn = p.TitleEn,
                 Price = p.Price.ToString(),
                 AuthorName = p.AuthorName,
                 PublisherName = p.BookPublisherName,
                 //PublisherId = p.BookPublisherId,
                 IsBestSeller = p.IsBestSeller,
                 BookTypeId = p.BookTypeId,
                 ExhibitionCurrencyId = p.ExhibitionCurrencyId,
             }
             ).ToList();
        }
        public List<XsiExhibitionBookForUi> SelectBooksTitle(XsiExhibitionBooks entity, long websiteId)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBooks>();

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.IsEnable.IsDefault())
                predicate = predicate.And(p => p.IsEnable.Equals(entity.IsEnable));

            if (!entity.IsBestSeller.IsDefault())
                predicate = predicate.And(p => p.IsBestSeller.Equals(entity.IsBestSeller));

            if (!entity.TitleAr.IsDefault())
                predicate = predicate.And(p => p.TitleAr != null && p.TitleAr.Contains(entity.TitleAr));

            if (!entity.TitleEn.IsDefault())
                predicate = predicate.And(p => p.TitleEn != null && p.TitleEn.ToLower().Contains(entity.TitleEn.ToLower()));

            if (CachingRepository.CachedBooksParticipating == null)
                CachingRepository.CachedBooksParticipating = (IList<XsiExhibitionBookParticipating>)XsiContext.Context.XsiExhibitionBookParticipating.ToList();
            //else
            //{
            //    using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //    {
            //        var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooksParticipating");
            //        if (CachingRepository.CachedBooksParticipating.Count() != count)
            //            CachingRepository.CachedBooksParticipating = XsiContext.Context.XsiExhibitionBooksParticipatings.ToList();
            //    }
            //}

            //HashSet<long> ExhibitorIDs = new HashSet<long>(XsiContext.Context.GetApprovedExhibitorIds(websiteId).Select(s => s.Value));
            //HashSet<long> AgencyIDs = new HashSet<long>(XsiContext.Context.GetApproveAgencyIds(websiteId).Select(s => s.Value));

            var BookIDs = new HashSet<long>(CachingRepository.CachedBooksParticipating.Where(p => p.IsActive == "Y" && p.BookId > 0
            /*&& (ExhibitorIDs.Contains(p.ExhibitorId.Value) || AgencyIDs.Contains(p.AgencyId.Value))*/).Select(s => s.BookId).Distinct());
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //else
            //{
            //    using (System.Data.IDbConnection db = new System.Data.SqlClient.SqlConnection(XsiContext.Context.Database.Connection.ConnectionString))
            //    {
            //        var count = db.QuerySingle<int>("select count(*) from XsiExhibitionBooks");
            //        if (CachingRepository.CachedBooks.Count() != count)
            //            CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //    }
            //}
            // above 1.5 items in books, 84,000 to be search in contains.
            var SearchLists = CachingRepository.CachedBooks.Where(p => BookIDs.Contains(p.BookId)).ToList();

            return SearchLists.AsQueryable().Where(predicate).Take(30).Select(p =>
             new XsiExhibitionBookForUi
             {
                 BookId = p.BookId,
                 TitleAr = p.TitleAr,
                 TitleEn = p.TitleEn,
             }
             ).ToList();
        }

        public XsiExhibitionBooks Add(XsiExhibitionBooks entity)
        {
            XsiExhibitionBooks Book = new XsiExhibitionBooks();
            //if (!entity.EventusIDEn.IsDefault())
            //    Book.EventusIDEn = entity.EventusIDEn.Trim();
            //if (!entity.EventusIDAr.IsDefault())
            //    Book.EventusIDAr = entity.EventusIDAr.Trim();

            //Book.BookPublisherId = entity.BookPublisherId;
            Book.MemberId = entity.MemberId;
            //Book.ExhibitorId = entity.ExhibitorId;
            //Book.AgencyId = entity.AgencyId; 
            Book.ExhibitionSubjectId = entity.ExhibitionSubjectId;
            Book.ExhibitionSubsubjectId = entity.ExhibitionSubsubjectId;
            Book.ExhibitionCurrencyId = entity.ExhibitionCurrencyId;
            Book.BookLanguageId = entity.BookLanguageId;
            Book.BookTypeId = entity.BookTypeId;
            Book.IsActive = entity.IsActive;
            Book.IsEnable = entity.IsEnable;
            Book.IsNew = entity.IsNew;
            if (!entity.BookDetails.IsDefault())
                Book.BookDetails = entity.BookDetails.Trim();
            if (!entity.BookDetailsAr.IsDefault())
                Book.BookDetailsAr = entity.BookDetailsAr.Trim();
            Book.IsAvailableOnline = entity.IsAvailableOnline;
            if (!entity.TitleAr.IsDefault())
                Book.TitleAr = entity.TitleAr.Trim();
            if (!entity.TitleEn.IsDefault())
                Book.TitleEn = entity.TitleEn.Trim();

            if (!entity.AuthorNameAr.IsDefault())
                Book.AuthorNameAr = entity.AuthorNameAr.Trim();
            if (!entity.AuthorName.IsDefault())
                Book.AuthorName = entity.AuthorName.Trim();


            if (!entity.BookPublisherNameAr.IsDefault())
                Book.BookPublisherNameAr = entity.BookPublisherNameAr.Trim();
            if (!entity.BookPublisherName.IsDefault())
                Book.BookPublisherName = entity.BookPublisherName.Trim();

            Book.Thumbnail = entity.Thumbnail;
            if (!entity.Isbn.IsDefault())
                Book.Isbn = entity.Isbn.Trim();
            if (!entity.IssueYear.IsDefault())
                Book.IssueYear = entity.IssueYear.Trim();
            if (!entity.Price.IsDefault())
                Book.Price = entity.Price;
            if (!entity.PriceBeforeDiscount.IsDefault())
                Book.PriceBeforeDiscount = entity.PriceBeforeDiscount;
            Book.IsBestSeller = entity.IsBestSeller;
            Book.ClickCount = entity.ClickCount;
            Book.CreatedOn = entity.CreatedOn;
            Book.CreatedBy = entity.CreatedBy;
            Book.ModifiedOn = entity.ModifiedOn;
            Book.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionBooks.Add(Book);
            SubmitChanges();

            return Book;
        }
        public void Update(XsiExhibitionBooks entity)
        {
            XsiExhibitionBooks Book = XsiContext.Context.XsiExhibitionBooks.Find(entity.BookId);
            XsiContext.Context.Entry(Book).State = EntityState.Modified;

            //if (!entity.EventusIDEn.IsDefault())
            //    Book.EventusIDEn = entity.EventusIDEn.Trim();
            //if (!entity.EventusIDAr.IsDefault())
            //    Book.EventusIDAr = entity.EventusIDAr.Trim();

            if (!entity.IsActive.IsDefault())
                Book.IsActive = entity.IsActive;
            if (!entity.IsEnable.IsDefault())
                Book.IsEnable = entity.IsEnable;
            if (!entity.IsNew.IsDefault())
                Book.IsNew = entity.IsNew;
            if (!entity.BookDetails.IsDefault())
                Book.BookDetails = entity.BookDetails.Trim();
            if (!entity.BookDetailsAr.IsDefault())
                Book.BookDetailsAr = entity.BookDetailsAr.Trim();
            if (!entity.IsAvailableOnline.IsDefault())
                Book.IsAvailableOnline = entity.IsAvailableOnline;
            if (!entity.TitleAr.IsDefault())
                Book.TitleAr = entity.TitleAr.Trim();
            if (!entity.TitleEn.IsDefault())
                Book.TitleEn = entity.TitleEn.Trim();
            if (!entity.Thumbnail.IsDefault())
                Book.Thumbnail = entity.Thumbnail;

            //if (!entity.ExhibitorId.IsDefault())
            //    Book.ExhibitorId = entity.ExhibitorId;
            //if (!entity.AgencyId.IsDefault())
            //    Book.AgencyId = entity.AgencyId;
            //if (!entity.BookPublisherId.IsDefault())
            //    Book.BookPublisherId = entity.BookPublisherId;
            if (!entity.MemberId.IsDefault())
                Book.MemberId = entity.MemberId;

            if (!entity.ExhibitionSubsubjectId.IsDefault())
                Book.ExhibitionSubsubjectId = entity.ExhibitionSubsubjectId;
            if (!entity.ExhibitionSubjectId.IsDefault())
                Book.ExhibitionSubjectId = entity.ExhibitionSubjectId;

            if (!entity.ExhibitionCurrencyId.IsDefault())
                Book.ExhibitionCurrencyId = entity.ExhibitionCurrencyId;
            if (!entity.BookLanguageId.IsDefault())
                Book.BookLanguageId = entity.BookLanguageId;
            if (!entity.BookTypeId.IsDefault())
                Book.BookTypeId = entity.BookTypeId;
            if (!entity.Isbn.IsDefault())
                Book.Isbn = entity.Isbn.Trim();
            if (!entity.IssueYear.IsDefault())
                Book.IssueYear = entity.IssueYear.Trim();
            if (!entity.Price.IsDefault())
                Book.Price = entity.Price;
            if (!entity.PriceBeforeDiscount.IsDefault())
                Book.PriceBeforeDiscount = entity.PriceBeforeDiscount;
            if (!entity.IsBestSeller.IsDefault())
                Book.IsBestSeller = entity.IsBestSeller;
            if (!entity.ClickCount.IsDefault())
                Book.ClickCount = entity.ClickCount;
            if (!entity.ModifiedOn.IsDefault())
                Book.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                Book.ModifiedBy = entity.ModifiedBy;

        }
        /*public void UpdatePublisherId(long? oldPublisherID, long? newPublisherID)
        {
            long resultCount = XsiContext.Context.UpdatePublisherInBooks(oldPublisherID, newPublisherID, 1);
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            else
            {
                var ItemToModify = CachingRepository.CachedBooks.Where(p => p.BookPublisherId == oldPublisherID);
                foreach (XsiExhibitionBooks row in ItemToModify)
                    row.BookPublisherId = newPublisherID;
            }
        }
        public void UpdateAuthorId(long? oldAuthorID, long? newAuthorID)
        {
            long resultCount = XsiContext.Context.UpdateAuthorInBooks(oldAuthorID, newAuthorID, 1);
            if (CachingRepository.CachedBooks == null)
                CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            else
            {
                //var ItemToModify = CachingRepository.CachedBooks.Where(p => p.AuthorId == oldAuthorID);
                //foreach (XsiExhibitionBooks row in ItemToModify)
                //    row.AuthorId = newAuthorID;
            }
        }*/
        public void Delete(XsiExhibitionBooks entity)
        {
            foreach (XsiExhibitionBooks Book in Select(entity))
            {
                XsiExhibitionBooks aBook = XsiContext.Context.XsiExhibitionBooks.Find(Book.BookId);
                XsiContext.Context.XsiExhibitionBooks.Remove(aBook);
                //if (CachingRepository.CachedBooks == null)
                //    CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
                //CachingRepository.CachedBooks.Remove(CachingRepository.CachedBooks.SingleOrDefault(x => x.BookId == Book.BookId));
            }
        }
        public void Delete(long bookId)
        {
            XsiExhibitionBooks aBook = XsiContext.Context.XsiExhibitionBooks.Find(bookId);
            XsiContext.Context.XsiExhibitionBooks.Remove(aBook);
          
            //if (CachingRepository.CachedBooks == null)
            //    CachingRepository.CachedBooks = XsiContext.Context.XsiExhibitionBooks.ToList();
            //CachingRepository.CachedBooks.Remove(CachingRepository.CachedBooks.SingleOrDefault(x => x.BookId == bookId));
        }

        public void AddAuthor(XsiExhibitionBookAuthor entity)
        {
            XsiExhibitionBookAuthor Book = new XsiExhibitionBookAuthor();
            Book.BookId = entity.BookId;
            Book.AuthorId = entity.AuthorId;
            XsiContext.Context.XsiExhibitionBookAuthor.Add(Book);
            SubmitChanges();
        }
        public void DeleteBookAuthor(long bookId)
        {
            List<XsiExhibitionBookAuthor> aBook = XsiContext.Context.XsiExhibitionBookAuthor.Where(x => x.BookId == bookId).ToList();
            foreach (var item in aBook)
            {
                XsiContext.Context.XsiExhibitionBookAuthor.Remove(item);
            }
        }

        public void AddBookParticipating(XsiExhibitionBookParticipating entity)
        {
            XsiExhibitionBookParticipating Book = new XsiExhibitionBookParticipating();
            Book.BookId = entity.BookId;
            Book.ExhibitorId = entity.ExhibitorId;
            Book.AgencyId = entity.AgencyId;
            Book.Price = entity.Price;
            Book.IsActive = entity.IsActive;
            Book.PriceBeforeDiscount = entity.PriceBeforeDiscount;
            Book.CreatedOn = entity.CreatedOn;
            Book.CreatedBy = entity.CreatedBy;
            Book.ModifiedOn = entity.ModifiedOn;
            Book.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiExhibitionBookParticipating.Add(Book);
            SubmitChanges();
        }

        public void UpdateBookParticipating(XsiExhibitionBookParticipating entity)
        {
            XsiExhibitionBookParticipating Book = XsiContext.Context.XsiExhibitionBookParticipating.Where(x => x.BookId == entity.BookId && x.ExhibitorId == entity.ExhibitorId).FirstOrDefault();
            XsiContext.Context.Entry(Book).State = EntityState.Modified;
            if (!entity.IsActive.IsDefault())
                Book.IsActive = entity.IsActive;
            if (!entity.BookId.IsDefault())
                Book.BookId = entity.BookId;
            if (!entity.ExhibitorId.IsDefault())
                Book.ExhibitorId = entity.ExhibitorId;
            if (!entity.AgencyId.IsDefault())
                Book.AgencyId = entity.AgencyId;
            if (!entity.Price.IsDefault())
                Book.Price = entity.Price;
            if (!entity.PriceBeforeDiscount.IsDefault())
                Book.PriceBeforeDiscount = entity.PriceBeforeDiscount;
            if (!entity.ModifiedOn.IsDefault())
                Book.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                Book.ModifiedBy = entity.ModifiedBy;
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }


        /*public void UpdateAuthorInBooks(long oldAuthorId, long newAuthorId, long AdminUserId)
        {
            XsiContext.Context.UpdateAuthorInBooks(oldAuthorId, newAuthorId, AdminUserId);
        }

        public void UpdatePublisherInBooks(long oldPublisherId, long newPublisherId, long AdminUserId)
        {
            XsiContext.Context.UpdatePublisherInBooks(oldPublisherId, newPublisherId, AdminUserId);
        }*/

        #endregion
    }
}
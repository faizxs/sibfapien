﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class AdvertisementBannerRepository : IAdvertisementBannerRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public AdvertisementBannerRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public AdvertisementBannerRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public AdvertisementBannerRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdvertisementBanner GetById(long itemId)
        {
            return XsiContext.Context.XsiAdvertisementBanner.Find(itemId);
        }

        public List<XsiAdvertisementBanner> Select()
        {
            return XsiContext.Context.XsiAdvertisementBanner.ToList();
        }
        public List<XsiAdvertisementBanner> SelectOr(XsiAdvertisementBanner entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiAdvertisementBanner>();
            var Outer = PredicateBuilder.True<XsiAdvertisementBanner>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
               
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiAdvertisementBanner.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiAdvertisementBanner> Select(XsiAdvertisementBanner entity)
        {
            var predicate = PredicateBuilder.True<XsiAdvertisementBanner>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.SortOrder.IsDefault())
                predicate = predicate.And(p => p.SortOrder == entity.SortOrder);

            if (!entity.CompanyName.IsDefault())
                predicate = predicate.And(p => p.CompanyName.Contains(entity.CompanyName));

            if (!entity.ContactPerson.IsDefault())
                predicate = predicate.And(p => p.ContactPerson.Contains(entity.ContactPerson));

            if (!entity.PhoneNo.IsDefault())
                predicate = predicate.And(p => p.PhoneNo.Equals(entity.PhoneNo));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Equals(entity.Email));

            if (!entity.HomeBanner.IsDefault())
                predicate = predicate.And(p => p.HomeBanner.Equals(entity.HomeBanner));

            if (!entity.SubpageBanner.IsDefault())
                predicate = predicate.And(p => p.SubpageBanner.Equals(entity.SubpageBanner));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Contains(entity.Url));

            if (!entity.StartDate.IsDefault())
                predicate = predicate.And(p => p.StartDate == entity.StartDate);

            if (!entity.EndDate.IsDefault())
                predicate = predicate.And(p => p.EndDate == entity.EndDate);

            if (!entity.PageIds.IsDefault())
                predicate = predicate.And(p => p.PageIds.Equals(entity.PageIds));

            return XsiContext.Context.XsiAdvertisementBanner.AsExpandable().Where(predicate).ToList();
        }

        public XsiAdvertisementBanner Add(XsiAdvertisementBanner entity)
        {
            XsiAdvertisementBanner AdvertisementBanner = new XsiAdvertisementBanner();
            if (!entity.Title.IsDefault())
            AdvertisementBanner.Title = entity.Title.Trim();
            AdvertisementBanner.IsActive = entity.IsActive;
            if (!entity.CompanyName.IsDefault())
            AdvertisementBanner.CompanyName = entity.CompanyName.Trim();
            if (!entity.ContactPerson.IsDefault())
            AdvertisementBanner.ContactPerson = entity.ContactPerson.Trim();
            if (!entity.PhoneNo.IsDefault())
            AdvertisementBanner.PhoneNo = entity.PhoneNo.Trim();
            if (!entity.Email.IsDefault())
            AdvertisementBanner.Email = entity.Email.Trim();
            AdvertisementBanner.HomeBanner = entity.HomeBanner;
            AdvertisementBanner.SubpageBanner = entity.SubpageBanner;
            if (!entity.Url.IsDefault())
            AdvertisementBanner.Url = entity.Url.Trim();
            AdvertisementBanner.StartDate = entity.StartDate;
            AdvertisementBanner.EndDate = entity.EndDate;
            AdvertisementBanner.PageIds = entity.PageIds;
            AdvertisementBanner.CreatedOn = entity.CreatedOn;
            AdvertisementBanner.CreatedBy = entity.CreatedBy;
            AdvertisementBanner.ModifiedOn = entity.ModifiedOn;
            AdvertisementBanner.ModifiedBy = entity.ModifiedBy;
            AdvertisementBanner.SortOrder = entity.SortOrder;

            XsiContext.Context.XsiAdvertisementBanner.Add(AdvertisementBanner);
            SubmitChanges();
            return AdvertisementBanner;

        }
        public void Update(XsiAdvertisementBanner entity)
        {
            XsiAdvertisementBanner AdvertisementBanner = XsiContext.Context.XsiAdvertisementBanner.Find(entity.ItemId);
            XsiContext.Context.Entry(AdvertisementBanner).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                AdvertisementBanner.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                AdvertisementBanner.IsActive = entity.IsActive;
            if (!entity.CompanyName.IsDefault())
                AdvertisementBanner.CompanyName = entity.CompanyName.Trim();
            if (!entity.ContactPerson.IsDefault())
                AdvertisementBanner.ContactPerson = entity.ContactPerson.Trim();
            if (!entity.PhoneNo.IsDefault())
                AdvertisementBanner.PhoneNo = entity.PhoneNo.Trim();
            if (!entity.Email.IsDefault())
                AdvertisementBanner.Email = entity.Email.Trim();
            if (!entity.HomeBanner.IsDefault())
                AdvertisementBanner.HomeBanner = entity.HomeBanner;
            if (!entity.SubpageBanner.IsDefault())
                AdvertisementBanner.SubpageBanner = entity.SubpageBanner;
            if (!entity.Url.IsDefault())
                AdvertisementBanner.Url = entity.Url.Trim();
            if (!entity.StartDate.IsDefault())
                AdvertisementBanner.StartDate = entity.StartDate;
            if (!entity.EndDate.IsDefault())
                AdvertisementBanner.EndDate = entity.EndDate;
            if (!entity.PageIds.IsDefault())
                AdvertisementBanner.PageIds = entity.PageIds;
            if (!entity.ModifiedOn.IsDefault())
                AdvertisementBanner.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                AdvertisementBanner.ModifiedBy = entity.ModifiedBy;
            if (!entity.SortOrder.IsDefault())
                AdvertisementBanner.SortOrder = entity.SortOrder;
        }
        public void Delete(XsiAdvertisementBanner entity)
        {
            foreach (XsiAdvertisementBanner AdvertisementBanner in Select(entity))
            {
                XsiAdvertisementBanner aAdvertisementBanner = XsiContext.Context.XsiAdvertisementBanner.Find(AdvertisementBanner.ItemId);
                XsiContext.Context.XsiAdvertisementBanner.Remove(aAdvertisementBanner);
            }
        }
        public void Delete(long itemId)
        {
            XsiAdvertisementBanner aAdvertisementBanner = XsiContext.Context.XsiAdvertisementBanner.Find(itemId);
            XsiContext.Context.XsiAdvertisementBanner.Remove(aAdvertisementBanner);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
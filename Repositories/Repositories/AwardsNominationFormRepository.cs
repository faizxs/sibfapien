﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class AwardsNominationFormRepository : IAwardsNominationFormRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public AwardsNominationFormRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public AwardsNominationFormRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public AwardsNominationFormRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiAwardsNominationForms GetById(long itemId)
        {
            return XsiContext.Context.XsiAwardsNominationForms.Find(itemId);
        }
        public List<XsiAwardsNominationForms> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiAwardsNominationForms> Select()
        {
            return XsiContext.Context.XsiAwardsNominationForms.ToList();
        }
        public List<XsiAwardsNominationForms> Select(XsiAwardsNominationForms entity)
        {
            var predicate = PredicateBuilder.True<XsiAwardsNominationForms>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

           
            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));
              
            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.PublishedYear.IsDefault())
                predicate = predicate.And(p => p.PublishedYear.Contains(entity.PublishedYear));

            if (!entity.Isbn.IsDefault())
                predicate = predicate.And(p => p.Isbn.Contains(entity.Isbn));

            if (!entity.NominationType.IsDefault())
                predicate = predicate.And(p => p.NominationType.Contains(entity.NominationType));

            if (!entity.AuthorName.IsDefault())
                predicate = predicate.And(p => p.AuthorName.Contains(entity.AuthorName));

            if (!entity.AuthorNumber.IsDefault())
                predicate = predicate.And(p => p.AuthorNumber.Contains(entity.AuthorNumber));

            if (!entity.AuthorEmail.IsDefault())
                predicate = predicate.And(p => p.AuthorEmail.Contains(entity.AuthorEmail));

            if (!entity.AuthorNationalityId.IsDefault())
                predicate = predicate.And(p => p.AuthorNationalityId == entity.AuthorNationalityId);

            if (!entity.Publisher.IsDefault())
                predicate = predicate.And(p => p.Publisher.Contains(entity.Publisher));

            if (!entity.PublisherNumber.IsDefault())
                predicate = predicate.And(p => p.PublisherNumber.Contains(entity.PublisherNumber));

            if (!entity.PublisherEmail.IsDefault())
                predicate = predicate.And(p => p.PublisherEmail.Contains(entity.PublisherEmail));

            if (!entity.PublisherCountryId.IsDefault())
                predicate = predicate.And(p => p.PublisherCountryId == entity.PublisherCountryId);

            if (!entity.OtherCity.IsDefault())
                predicate = predicate.And(p => p.OtherCity.Contains(entity.OtherCity));

            if (!entity.CityId.IsDefault())
                predicate = predicate.And(p => p.CityId == entity.CityId);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.PublishersSpecialization.IsDefault())
                predicate = predicate.And(p => p.PublishersSpecialization.Contains(entity.PublishersSpecialization));

            if (!entity.EstablishedYear.IsDefault())
                predicate = predicate.And(p => p.EstablishedYear.Contains(entity.EstablishedYear));

            if (!entity.MailAddress.IsDefault())
                predicate = predicate.And(p => p.MailAddress.Contains(entity.MailAddress));

            if (!entity.Pobox.IsDefault())
                predicate = predicate.And(p => p.Pobox.Contains(entity.Pobox));

            if (!entity.Phone.IsDefault())
                predicate = predicate.And(p => p.Phone.Contains(entity.Phone));

            if (!entity.Fax.IsDefault())
                predicate = predicate.And(p => p.Fax.Contains(entity.Fax));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Contains(entity.Email));

            if (!entity.OwnerOfPubHouse.IsDefault())
                predicate = predicate.And(p => p.OwnerOfPubHouse.Contains(entity.OwnerOfPubHouse));

            if (!entity.Date.IsDefault())
                predicate = predicate.And(p => p.Date == entity.Date);

            if (!entity.PostalCode.IsDefault())
                predicate = predicate.And(p => p.PostalCode.Contains(entity.PostalCode));

            if (!entity.FirstParticipatedYear.IsDefault())
                predicate = predicate.And(p => p.FirstParticipatedYear.Contains(entity.FirstParticipatedYear));

            if (!entity.NoOfPublications.IsDefault())
                predicate = predicate.And(p => p.NoOfPublications == entity.NoOfPublications);

            if (!entity.NoOfPublicationsInCurrentYear.IsDefault())
                predicate = predicate.And(p => p.NoOfPublicationsInCurrentYear == entity.NoOfPublicationsInCurrentYear);

            if (!entity.GeneralManagerOfPubHouse.IsDefault())
                predicate = predicate.And(p => p.GeneralManagerOfPubHouse.Contains(entity.GeneralManagerOfPubHouse));

            if (!entity.Comments.IsDefault())
                predicate = predicate.And(p => p.Comments.Contains(entity.Comments));

            if (!entity.PassportCopy.IsDefault())
                predicate = predicate.And(p => p.PassportCopy.Contains(entity.PassportCopy));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Contains(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            return XsiContext.Context.XsiAwardsNominationForms.AsExpandable().Where(predicate).ToList();
        }

        public XsiAwardsNominationForms Add(XsiAwardsNominationForms entity)
        {
            XsiAwardsNominationForms AwardsNominationForm = new XsiAwardsNominationForms();
         
            AwardsNominationForm.Status = entity.Status;
            if (!entity.ExhibitionId.IsDefault())
                AwardsNominationForm.ExhibitionId = entity.ExhibitionId;
            AwardsNominationForm.AwardId = entity.AwardId;
            AwardsNominationForm.SubAwardId = entity.SubAwardId;

            if (!entity.Title.IsDefault())
                AwardsNominationForm.Title = entity.Title.Trim();
            if (!entity.PublishedYear.IsDefault())
                AwardsNominationForm.PublishedYear = entity.PublishedYear.Trim();
            if (!entity.Isbn.IsDefault())
                AwardsNominationForm.Isbn = entity.Isbn.Trim();
            if (!entity.NominationType.IsDefault())
                AwardsNominationForm.NominationType = entity.NominationType.Trim();
            if (!entity.AuthorName.IsDefault())
                AwardsNominationForm.AuthorName = entity.AuthorName.Trim();
            if (!entity.AuthorNumber.IsDefault())
                AwardsNominationForm.AuthorNumber = entity.AuthorNumber.Trim();
            if (!entity.AuthorEmail.IsDefault())
                AwardsNominationForm.AuthorEmail = entity.AuthorEmail.Trim();
            if (!entity.AuthorNationalityId.IsDefault())
                AwardsNominationForm.AuthorNationalityId = entity.AuthorNationalityId;
            if (!entity.Publisher.IsDefault())
                AwardsNominationForm.Publisher = entity.Publisher.Trim();

            if (!entity.PublisherNumber.IsDefault())
                AwardsNominationForm.PublisherNumber = entity.PublisherNumber.Trim();

            if (!entity.PublisherEmail.IsDefault())
                AwardsNominationForm.PublisherEmail = entity.PublisherEmail.Trim();

            if (!entity.PublisherCountryId.IsDefault())
                AwardsNominationForm.PublisherCountryId = entity.PublisherCountryId;

            if (!entity.OtherCity.IsDefault())
                AwardsNominationForm.OtherCity = entity.OtherCity.Trim();
            AwardsNominationForm.CityId = entity.CityId;
            AwardsNominationForm.CountryId = entity.CountryId;
            if (!entity.PublishersSpecialization.IsDefault())
                AwardsNominationForm.PublishersSpecialization = entity.PublishersSpecialization.Trim();
            if (!entity.EstablishedYear.IsDefault())
                AwardsNominationForm.EstablishedYear = entity.EstablishedYear.Trim();
            if (!entity.MailAddress.IsDefault())
                AwardsNominationForm.MailAddress = entity.MailAddress.Trim();
            if (!entity.Pobox.IsDefault())
                AwardsNominationForm.Pobox = entity.Pobox.Trim();
            if (!entity.Phone.IsDefault())
                AwardsNominationForm.Phone = entity.Phone.Trim();
            if (!entity.Fax.IsDefault())
                AwardsNominationForm.Fax = entity.Fax.Trim();
            if (!entity.Email.IsDefault())
                AwardsNominationForm.Email = entity.Email.Trim();
            if (!entity.OwnerOfPubHouse.IsDefault())
                AwardsNominationForm.OwnerOfPubHouse = entity.OwnerOfPubHouse.Trim();
            AwardsNominationForm.Date = entity.Date;
            if (!entity.PostalCode.IsDefault())
                AwardsNominationForm.PostalCode = entity.PostalCode.Trim();
            if (!entity.FirstParticipatedYear.IsDefault())
                AwardsNominationForm.FirstParticipatedYear = entity.FirstParticipatedYear.Trim();
            AwardsNominationForm.NoOfPublications = entity.NoOfPublications;
            AwardsNominationForm.NoOfPublicationsInCurrentYear = entity.NoOfPublicationsInCurrentYear;
            if (!entity.GeneralManagerOfPubHouse.IsDefault())
                AwardsNominationForm.GeneralManagerOfPubHouse = entity.GeneralManagerOfPubHouse.Trim();
            if (!entity.Comments.IsDefault())
                AwardsNominationForm.Comments = entity.Comments.Trim();
            AwardsNominationForm.PassportCopy = entity.PassportCopy;
            AwardsNominationForm.IsActive = entity.IsActive;
            AwardsNominationForm.CreatedOn = entity.CreatedOn;
            AwardsNominationForm.ModifiedOn = entity.ModifiedOn;

            XsiContext.Context.XsiAwardsNominationForms.Add(AwardsNominationForm);
            SubmitChanges();
            return AwardsNominationForm;

        }
        public void Update(XsiAwardsNominationForms entity)
        {
            XsiAwardsNominationForms AwardsNominationForm = XsiContext.Context.XsiAwardsNominationForms.Find(entity.ItemId);
            XsiContext.Context.Entry(AwardsNominationForm).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                AwardsNominationForm.Title = entity.Title.Trim();

            if (!entity.Status.IsDefault())
                AwardsNominationForm.Status = entity.Status;

            if (!entity.ExhibitionId.IsDefault())
                AwardsNominationForm.ExhibitionId = entity.ExhibitionId;

            if (!entity.AwardId.IsDefault())
                AwardsNominationForm.AwardId = entity.AwardId;

            if (!entity.SubAwardId.IsDefault())
                AwardsNominationForm.SubAwardId = entity.SubAwardId;

            if (!entity.PublishedYear.IsDefault())
                AwardsNominationForm.PublishedYear = entity.PublishedYear.Trim();

            if (!entity.Isbn.IsDefault())
                AwardsNominationForm.Isbn = entity.Isbn.Trim();

            if (!entity.NominationType.IsDefault())
                AwardsNominationForm.NominationType = entity.NominationType.Trim();

            if (!entity.AuthorName.IsDefault())
                AwardsNominationForm.AuthorName = entity.AuthorName.Trim();

            if (!entity.AuthorNumber.IsDefault())
                AwardsNominationForm.AuthorNumber = entity.AuthorNumber.Trim();

            if (!entity.AuthorEmail.IsDefault())
                AwardsNominationForm.AuthorEmail = entity.AuthorEmail.Trim();

            if (!entity.AuthorNationalityId.IsDefault())
                AwardsNominationForm.AuthorNationalityId = entity.AuthorNationalityId;

            if (!entity.Publisher.IsDefault())
                AwardsNominationForm.Publisher = entity.Publisher.Trim();

            if (!entity.PublisherNumber.IsDefault())
                AwardsNominationForm.PublisherNumber = entity.PublisherNumber.Trim();

            if (!entity.PublisherEmail.IsDefault())
                AwardsNominationForm.PublisherEmail = entity.PublisherEmail.Trim();

            if (!entity.PublisherCountryId.IsDefault())
                AwardsNominationForm.PublisherCountryId = entity.PublisherCountryId;

            if (!entity.OtherCity.IsDefault())
                AwardsNominationForm.OtherCity = entity.OtherCity.Trim();

            if (!entity.CityId.IsDefault())
                AwardsNominationForm.CityId = entity.CityId;

            if (!entity.CountryId.IsDefault())
                AwardsNominationForm.CountryId = entity.CountryId;

            if (!entity.PublishersSpecialization.IsDefault())
                AwardsNominationForm.PublishersSpecialization = entity.PublishersSpecialization.Trim();

            if (!entity.EstablishedYear.IsDefault())
                AwardsNominationForm.EstablishedYear = entity.EstablishedYear.Trim();
            if (!entity.MailAddress.IsDefault())
                AwardsNominationForm.MailAddress = entity.MailAddress.Trim();
            if (!entity.Pobox.IsDefault())
                AwardsNominationForm.Pobox = entity.Pobox.Trim();
            if (!entity.Phone.IsDefault())
                AwardsNominationForm.Phone = entity.Phone.Trim();
            if (!entity.Fax.IsDefault())
                AwardsNominationForm.Fax = entity.Fax.Trim();
            if (!entity.Email.IsDefault())
                AwardsNominationForm.Email = entity.Email.Trim();
            if (!entity.OwnerOfPubHouse.IsDefault())
                AwardsNominationForm.OwnerOfPubHouse = entity.OwnerOfPubHouse.Trim();
            if (!entity.Date.IsDefault())
                AwardsNominationForm.Date = entity.Date;
            if (!entity.PostalCode.IsDefault())
                AwardsNominationForm.PostalCode = entity.PostalCode.Trim();
            if (!entity.FirstParticipatedYear.IsDefault())
                AwardsNominationForm.FirstParticipatedYear = entity.FirstParticipatedYear.Trim();
            if (!entity.NoOfPublications.IsDefault())
                AwardsNominationForm.NoOfPublications = entity.NoOfPublications;
            if (!entity.NoOfPublicationsInCurrentYear.IsDefault())
                AwardsNominationForm.NoOfPublicationsInCurrentYear = entity.NoOfPublicationsInCurrentYear;
            if (!entity.GeneralManagerOfPubHouse.IsDefault())
                AwardsNominationForm.GeneralManagerOfPubHouse = entity.GeneralManagerOfPubHouse.Trim();
            if (!entity.Comments.IsDefault())
                AwardsNominationForm.Comments = entity.Comments.Trim();
            if (!entity.PassportCopy.IsDefault())
                AwardsNominationForm.PassportCopy = entity.PassportCopy;
            if (!entity.IsActive.IsDefault())
                AwardsNominationForm.IsActive = entity.IsActive;
            if (!entity.ModifiedOn.IsDefault())
                AwardsNominationForm.ModifiedOn = entity.ModifiedOn;
        }
        public void Delete(XsiAwardsNominationForms entity)
        {
            foreach (XsiAwardsNominationForms AwardsNominationForm in Select(entity))
            {
                XsiAwardsNominationForms aAwardsNominationForm = XsiContext.Context.XsiAwardsNominationForms.Find(AwardsNominationForm.ItemId);
                XsiContext.Context.XsiAwardsNominationForms.Remove(aAwardsNominationForm);
            }
        }
        public void Delete(long itemId)
        {
            XsiAwardsNominationForms aAwardsNominationForm = XsiContext.Context.XsiAwardsNominationForms.Find(itemId);
            XsiContext.Context.XsiAwardsNominationForms.Remove(aAwardsNominationForm);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
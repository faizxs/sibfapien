﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionProfessionalProgramRegistrationStatusLogRepository : IExhibitionProfessionalProgramRegistrationStatusLogRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionProfessionalProgramRegistrationStatusLogRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionProfessionalProgramRegistrationStatusLogRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionProfessionalProgramRegistrationStatusLogRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionProfessionalProgramRegistrationStatusLogs GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionProfessionalProgramRegistrationStatusLogs.Find(itemId);
        }
        public List<XsiExhibitionProfessionalProgramRegistrationStatusLogs> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionProfessionalProgramRegistrationStatusLogs> Select()
        {
            return XsiContext.Context.XsiExhibitionProfessionalProgramRegistrationStatusLogs.ToList();
        }
        public List<XsiExhibitionProfessionalProgramRegistrationStatusLogs> Select(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionProfessionalProgramRegistrationStatusLogs>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.RegistrationId.IsDefault())
                predicate = predicate.And(p => p.RegistrationId == entity.RegistrationId);

            if (!entity.AdminId.IsDefault())
                predicate = predicate.And(p => p.AdminId == entity.AdminId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);


            return XsiContext.Context.XsiExhibitionProfessionalProgramRegistrationStatusLogs.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionProfessionalProgramRegistrationStatusLogs Add(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity)
        {
            XsiExhibitionProfessionalProgramRegistrationStatusLogs ExhibitionProfessionalProgramRegistrationStatusLog = new XsiExhibitionProfessionalProgramRegistrationStatusLogs();
            ExhibitionProfessionalProgramRegistrationStatusLog.RegistrationId = entity.RegistrationId;
            ExhibitionProfessionalProgramRegistrationStatusLog.AdminId = entity.AdminId;
            ExhibitionProfessionalProgramRegistrationStatusLog.Status = entity.Status;
            ExhibitionProfessionalProgramRegistrationStatusLog.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiExhibitionProfessionalProgramRegistrationStatusLogs.Add(ExhibitionProfessionalProgramRegistrationStatusLog);
            SubmitChanges();
            return ExhibitionProfessionalProgramRegistrationStatusLog;

        }
        public void Update(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity)
        {
            XsiExhibitionProfessionalProgramRegistrationStatusLogs ExhibitionProfessionalProgramRegistrationStatusLog = XsiContext.Context.XsiExhibitionProfessionalProgramRegistrationStatusLogs.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionProfessionalProgramRegistrationStatusLog).State = EntityState.Modified;

            if (!entity.Status.IsDefault())
                ExhibitionProfessionalProgramRegistrationStatusLog.Status = entity.Status;
        }
        public void Delete(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity)
        {
            foreach (XsiExhibitionProfessionalProgramRegistrationStatusLogs ExhibitionProfessionalProgramRegistrationStatusLog in Select(entity))
            {
                XsiExhibitionProfessionalProgramRegistrationStatusLogs aExhibitionProfessionalProgramRegistrationStatusLog = XsiContext.Context.XsiExhibitionProfessionalProgramRegistrationStatusLogs.Find(ExhibitionProfessionalProgramRegistrationStatusLog.ItemId);
                XsiContext.Context.XsiExhibitionProfessionalProgramRegistrationStatusLogs.Remove(aExhibitionProfessionalProgramRegistrationStatusLog);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionProfessionalProgramRegistrationStatusLogs aExhibitionProfessionalProgramRegistrationStatusLog = XsiContext.Context.XsiExhibitionProfessionalProgramRegistrationStatusLogs.Find(itemId);
            XsiContext.Context.XsiExhibitionProfessionalProgramRegistrationStatusLogs.Remove(aExhibitionProfessionalProgramRegistrationStatusLog);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class SCRFPressReleaseRepository : ISCRFPressReleaseRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public SCRFPressReleaseRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public SCRFPressReleaseRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public SCRFPressReleaseRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfpressRelease GetById(long itemId)
        {
            return XsiContext.Context.XsiScrfpressRelease.Find(itemId);
        }

        public List<XsiScrfpressRelease> Select()
        {
            return XsiContext.Context.XsiScrfpressRelease.ToList();
        }
        public List<XsiScrfpressRelease> Select(XsiScrfpressRelease entity)
        {
            var predicate = PredicateBuilder.True<XsiScrfpressRelease>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated == entity.Dated);

            if (!entity.Source.IsDefault())
                predicate = predicate.And(p => p.Source.Contains(entity.Source));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName == entity.FileName);

            if (!entity.WebsiteId.IsDefault())
                predicate = predicate.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.Overview.IsDefault())
                predicate = predicate.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiScrfpressRelease.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiScrfpressRelease> SelectOr(XsiScrfpressRelease entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiScrfpressRelease>();
            var Outer = PredicateBuilder.True<XsiScrfpressRelease>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.Dated.IsDefault())
                Outer = Outer.And(p => p.Dated == entity.Dated);

            if (!entity.Source.IsDefault())
                Outer = Outer.And(p => p.Source.Contains(entity.Source));

            if (!entity.FileName.IsDefault())
                Outer = Outer.And(p => p.FileName == entity.FileName);

            if (!entity.WebsiteId.IsDefault())
                Outer = Outer.And(p => p.WebsiteId == entity.WebsiteId);

            if (!entity.Overview.IsDefault())
                Outer = Outer.And(p => p.Overview.Contains(entity.Overview));

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                Outer = Outer.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                Outer = Outer.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.CreatedBy.IsDefault())
                Outer = Outer.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedBy.IsDefault())
                Outer = Outer.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiScrfpressRelease.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiScrfpressRelease Add(XsiScrfpressRelease entity)
        {
            XsiScrfpressRelease SCRFPressRelease = new XsiScrfpressRelease();
            if (!entity.Title.IsDefault())
            SCRFPressRelease.Title = entity.Title.Trim();
            SCRFPressRelease.Dated = entity.Dated;
            if (!entity.Source.IsDefault())
            SCRFPressRelease.Source = entity.Source.Trim();
            SCRFPressRelease.FileName = entity.FileName;
            if (!entity.Overview.IsDefault())
            SCRFPressRelease.Overview = entity.Overview.Trim();
            SCRFPressRelease.WebsiteId = entity.WebsiteId;
            SCRFPressRelease.IsActive = entity.IsActive;
            SCRFPressRelease.CreatedOn = entity.CreatedOn;
            SCRFPressRelease.CreatedBy = entity.CreatedBy;
            SCRFPressRelease.ModifiedOn = entity.ModifiedOn;
            SCRFPressRelease.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiScrfpressRelease.Add(SCRFPressRelease);
            SubmitChanges();
            return SCRFPressRelease;

        }
        public void Update(XsiScrfpressRelease entity)
        {
            XsiScrfpressRelease SCRFPressRelease = XsiContext.Context.XsiScrfpressRelease.Find(entity.ItemId);
            XsiContext.Context.Entry(SCRFPressRelease).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                SCRFPressRelease.Title = entity.Title.Trim();

            if (!entity.Dated.IsDefault())
                SCRFPressRelease.Dated = entity.Dated;

            if (!entity.Source.IsDefault())
                SCRFPressRelease.Source = entity.Source.Trim();

            if (!entity.FileName.IsDefault())
                SCRFPressRelease.FileName = entity.FileName;

            if (!entity.Overview.IsDefault())
                SCRFPressRelease.Overview = entity.Overview.Trim();

            if (!entity.WebsiteId.IsDefault())
                SCRFPressRelease.WebsiteId = entity.WebsiteId;

            if (!entity.IsActive.IsDefault())
                SCRFPressRelease.IsActive = entity.IsActive;

            if (!entity.ModifiedOn.IsDefault())
                SCRFPressRelease.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                SCRFPressRelease.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiScrfpressRelease entity)
        {
            foreach (XsiScrfpressRelease SCRFPressRelease in Select(entity))
            {
                XsiScrfpressRelease aSCRFPressRelease = XsiContext.Context.XsiScrfpressRelease.Find(SCRFPressRelease.ItemId);
                XsiContext.Context.XsiScrfpressRelease.Remove(aSCRFPressRelease);
            }
        }
        public void Delete(long itemId)
        {
            XsiScrfpressRelease aSCRFPressRelease = XsiContext.Context.XsiScrfpressRelease.Find(itemId);
            XsiContext.Context.XsiScrfpressRelease.Remove(aSCRFPressRelease);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class HomepageOrganiserRepository : IHomepageOrganiserRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public HomepageOrganiserRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public HomepageOrganiserRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public HomepageOrganiserRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiHomepageOrganiser GetById(long itemId)
        {
            return XsiContext.Context.XsiHomepageOrganiser.Find(itemId);
        }

        public List<XsiHomepageOrganiser> Select()
        {
            return XsiContext.Context.XsiHomepageOrganiser.ToList();
        }
        public List<XsiHomepageOrganiser> Select(XsiHomepageOrganiser entity)
        {
            var predicate = PredicateBuilder.True<XsiHomepageOrganiser>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiHomepageOrganiser.AsExpandable().Where(predicate).ToList();
        }

        public XsiHomepageOrganiser Add(XsiHomepageOrganiser entity)
        {
            XsiHomepageOrganiser HomepageOrganiser = new XsiHomepageOrganiser();
            if (!entity.IsActive.IsDefault())
                HomepageOrganiser.IsActive = entity.IsActive;
            if (!entity.Title.IsDefault())
                HomepageOrganiser.Title = entity.Title.Trim();

            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                HomepageOrganiser.IsActiveAr = entity.IsActiveAr;
            if (!entity.TitleAr.IsDefault())
                HomepageOrganiser.TitleAr = entity.TitleAr.Trim();
            #endregion Ar

            HomepageOrganiser.CreatedOn = entity.CreatedOn;
            HomepageOrganiser.CreatedBy = entity.CreatedBy;
            HomepageOrganiser.ModifiedOn = entity.ModifiedOn;
            HomepageOrganiser.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiHomepageOrganiser.Add(HomepageOrganiser);
            SubmitChanges();
            return HomepageOrganiser;
        }

        public void Update(XsiHomepageOrganiser entity)
        {
            XsiHomepageOrganiser HomepageOrganiser = XsiContext.Context.XsiHomepageOrganiser.Find(entity.ItemId);
            XsiContext.Context.Entry(HomepageOrganiser).State = EntityState.Modified;

            if (!entity.IsActive.IsDefault())
                HomepageOrganiser.IsActive = entity.IsActive;

            if (!entity.Title.IsDefault())
                HomepageOrganiser.Title = entity.Title.Trim();

            #region Ar
            if (!entity.IsActiveAr.IsDefault())
                HomepageOrganiser.IsActiveAr = entity.IsActiveAr;

            if (!entity.TitleAr.IsDefault())
                HomepageOrganiser.TitleAr = entity.TitleAr.Trim();
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                HomepageOrganiser.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                HomepageOrganiser.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiHomepageOrganiser entity)
        {
            foreach (XsiHomepageOrganiser HomepageOrganiser in Select(entity))
            {
                XsiHomepageOrganiser aHomepageOrganiser = XsiContext.Context.XsiHomepageOrganiser.Find(HomepageOrganiser.ItemId);
                XsiContext.Context.XsiHomepageOrganiser.Remove(aHomepageOrganiser);
            }
        }
        public void Delete(long itemId)
        {
            XsiHomepageOrganiser aHomepageOrganiser = XsiContext.Context.XsiHomepageOrganiser.Find(itemId);
            XsiContext.Context.XsiHomepageOrganiser.Remove(aHomepageOrganiser);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionCurrencyRepository : IExhibitionCurrencyRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionCurrencyRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionCurrencyRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionCurrencyRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionCurrency GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionCurrency.Find(itemId);
        }

        public List<XsiExhibitionCurrency> Select()
        {
            return XsiContext.Context.XsiExhibitionCurrency.ToList();
        }
        public List<XsiExhibitionCurrency> Select(XsiExhibitionCurrency entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionCurrency>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.ToLower().Contains(entity.Title));

            if (!entity.CurrencyCode.IsDefault())
                predicate = predicate.And(p => p.CurrencyCode.Contains(entity.CurrencyCode));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionCurrency.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiExhibitionCurrency> SelectOr(XsiExhibitionCurrency entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionCurrency>();
            var Outer = PredicateBuilder.True<XsiExhibitionCurrency>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionCurrency.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public XsiExhibitionCurrency Add(XsiExhibitionCurrency entity)
        {
            XsiExhibitionCurrency ExhibitionCurrency = new XsiExhibitionCurrency();

            if (!entity.Title.IsDefault())
                ExhibitionCurrency.Title = entity.Title.Trim();
            if (!entity.CurrencyCode.IsDefault())
                ExhibitionCurrency.CurrencyCode = entity.CurrencyCode.Trim();
            ExhibitionCurrency.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionCurrency.TitleAr = entity.TitleAr.Trim();
            ExhibitionCurrency.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            ExhibitionCurrency.CreatedOn = entity.CreatedOn;
            ExhibitionCurrency.CreatedBy = entity.CreatedBy;
            ExhibitionCurrency.ModifiedOn = entity.ModifiedOn;
            ExhibitionCurrency.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionCurrency.Add(ExhibitionCurrency);
            SubmitChanges();
            return ExhibitionCurrency;

        }
        public void Update(XsiExhibitionCurrency entity)
        {
            XsiExhibitionCurrency ExhibitionCurrency = XsiContext.Context.XsiExhibitionCurrency.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionCurrency).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionCurrency.Title = entity.Title.Trim();
            if (!entity.CurrencyCode.IsDefault())
                ExhibitionCurrency.CurrencyCode = entity.CurrencyCode.Trim();
            if (!entity.IsActive.IsDefault())
                ExhibitionCurrency.IsActive = entity.IsActive;
            
            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionCurrency.TitleAr = entity.TitleAr.Trim(); 
            if (!entity.IsActiveAr.IsDefault())
                ExhibitionCurrency.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionCurrency.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                ExhibitionCurrency.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionCurrency entity)
        {
            foreach (XsiExhibitionCurrency ExhibitionCurrency in Select(entity))
            {
                XsiExhibitionCurrency aExhibitionCurrency = XsiContext.Context.XsiExhibitionCurrency.Find(ExhibitionCurrency.ItemId);
                XsiContext.Context.XsiExhibitionCurrency.Remove(aExhibitionCurrency);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionCurrency aExhibitionCurrency = XsiContext.Context.XsiExhibitionCurrency.Find(itemId);
            XsiContext.Context.XsiExhibitionCurrency.Remove(aExhibitionCurrency);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class CareerApplicantsRepository : ICareerApplicantsRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public CareerApplicantsRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public CareerApplicantsRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public CareerApplicantsRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiCareerApplicants GetById(long itemId)
        {
            return XsiContext.Context.XsiCareerApplicants.Find(itemId);
        }
        public List<XsiCareerApplicants> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiCareerApplicants> Select()
        {
            return XsiContext.Context.XsiCareerApplicants.ToList();
        }
        public List<XsiCareerApplicants> SelectOr(XsiCareerApplicants entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiCareerApplicants>();
            var Outer = PredicateBuilder.True<XsiCareerApplicants>();

            if (!entity.LanguageId.IsDefault() && entity.LanguageId == 2 && !entity.FirstName.IsDefault())
            {
                string strSearchText = entity.FirstName;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);
                Inner = Inner.Or(p => p.LastName.Contains(searchKeys.str1) || p.LastName.Contains(searchKeys.str2) || p.LastName.Contains(searchKeys.str3) || p.LastName.Contains(searchKeys.str4) || p.LastName.Contains(searchKeys.str5)
                    || p.LastName.Contains(searchKeys.str6) || p.LastName.Contains(searchKeys.str7) || p.LastName.Contains(searchKeys.str8) || p.LastName.Contains(searchKeys.str9) || p.LastName.Contains(searchKeys.str10)
                    || p.LastName.Contains(searchKeys.str11) || p.LastName.Contains(searchKeys.str12) || p.LastName.Contains(searchKeys.str13)
                    || p.LastName.Contains(searchKeys.str14) || p.LastName.Contains(searchKeys.str15) || p.LastName.Contains(searchKeys.str16) || p.LastName.Contains(searchKeys.str17)
                    );
                Inner = Inner.Or(p => (p.FirstName + " " + p.LastName).Contains(strSearchText) || p.FirstName.Contains(searchKeys.str1) || p.FirstName.Contains(searchKeys.str2) || p.FirstName.Contains(searchKeys.str3) || p.FirstName.Contains(searchKeys.str4) || p.FirstName.Contains(searchKeys.str5)
                    || p.FirstName.Contains(searchKeys.str6) || p.FirstName.Contains(searchKeys.str7) || p.FirstName.Contains(searchKeys.str8) || p.FirstName.Contains(searchKeys.str9) || p.FirstName.Contains(searchKeys.str10)
                    || p.FirstName.Contains(searchKeys.str11) || p.FirstName.Contains(searchKeys.str12) || p.FirstName.Contains(searchKeys.str13)
                    || p.FirstName.Contains(searchKeys.str14) || p.FirstName.Contains(searchKeys.str15) || p.FirstName.Contains(searchKeys.str16) || p.FirstName.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.FirstName.IsDefault())
                {
                    string strSearchText = entity.FirstName.ToLower();
                    Inner = Inner.Or(p => (p.FirstName + " " + p.LastName).Contains(strSearchText) || p.FirstName.ToLower().Contains(strSearchText) || p.LastName.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.LanguageId.IsDefault())
                Outer = Outer.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Status.IsDefault())
                Outer = Outer.And(p => p.Status.Equals(entity.Status));
            
            if (!entity.Email.IsDefault())
                Outer = Outer.And(p => p.Email.Equals(entity.Email));

            if (!entity.ContactNo.IsDefault())
                Outer = Outer.And(p => p.ContactNo.Contains(entity.ContactNo));

            if (!entity.YearofBirth.IsDefault())
                Outer = Outer.And(p => p.YearofBirth.Contains(entity.YearofBirth));

            if (!entity.VisaStatus.IsDefault())
                Outer = Outer.And(p => p.VisaStatus.Equals(entity.VisaStatus));

            if (!entity.Gender.IsDefault())
                Outer = Outer.And(p => p.Gender.Equals(entity.Gender));

            if (!entity.Nationality.IsDefault())
                Outer = Outer.And(p => p.Nationality == entity.Nationality);

            if (!entity.Residence.IsDefault())
                Outer = Outer.And(p => p.Residence == entity.Residence);

            if (!entity.Degree.IsDefault())
                Outer = Outer.And(p => p.Degree == entity.Degree);

            if (!entity.Speciality.IsDefault())
                Outer = Outer.And(p => p.Speciality == entity.Speciality);

            if (!entity.SubSpeciality.IsDefault())
                Outer = Outer.And(p => p.SubSpeciality == entity.SubSpeciality);

            if (!entity.Cv.IsDefault())
                Outer = Outer.And(p => p.Cv.Equals(entity.Cv));

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Status.IsDefault())
                Outer = Outer.And(p => p.Status.Equals(entity.Status));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            if (!entity.CareerId.IsDefault())
                Outer = Outer.And(p => p.CareerId == entity.CareerId);

            return XsiContext.Context.XsiCareerApplicants.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiCareerApplicants> Select(XsiCareerApplicants entity)
        {
            var predicate = PredicateBuilder.True<XsiCareerApplicants>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.LanguageId.IsDefault())
                predicate = predicate.And(p => p.LanguageId == entity.LanguageId);

            if (!entity.CareerId.IsDefault())
                predicate = predicate.And(p => p.CareerId == entity.CareerId);

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));

            if (!entity.FirstName.IsDefault())
                predicate = predicate.And(p => (p.FirstName + " " + p.LastName).Contains(entity.FirstName));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Equals(entity.Email));

            if (!entity.ContactNo.IsDefault())
                predicate = predicate.And(p => p.ContactNo.Contains(entity.ContactNo));

            if (!entity.YearofBirth.IsDefault())
                predicate = predicate.And(p => p.YearofBirth.Contains(entity.YearofBirth));

            if (!entity.VisaStatus.IsDefault())
                predicate = predicate.And(p => p.VisaStatus.Equals(entity.VisaStatus));

            if (!entity.Gender.IsDefault())
                predicate = predicate.And(p => p.Gender.Equals(entity.Gender));

            if (!entity.Nationality.IsDefault())
                predicate = predicate.And(p => p.Nationality == entity.Nationality);

            if (!entity.Residence.IsDefault())
                predicate = predicate.And(p => p.Residence == entity.Residence);

            if (!entity.Degree.IsDefault())
                predicate = predicate.And(p => p.Degree == entity.Degree);

            if (!entity.Speciality.IsDefault())
                predicate = predicate.And(p => p.Speciality == entity.Speciality);

            if (!entity.SubSpeciality.IsDefault())
                predicate = predicate.And(p => p.SubSpeciality == entity.SubSpeciality);

            if (!entity.Cv.IsDefault())
                predicate = predicate.And(p => p.Cv.Equals(entity.Cv));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn.Equals(entity.CreatedOn));

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn.Equals(entity.ModifiedOn));

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiCareerApplicants.AsExpandable().Where(predicate).ToList();
        }
        public XsiCareerApplicants Add(XsiCareerApplicants entity)
        {
            XsiCareerApplicants CareerApplicant = new XsiCareerApplicants();
            CareerApplicant.LanguageId = entity.LanguageId;
            if (!entity.CareerId.IsDefault())
                CareerApplicant.CareerId = entity.CareerId;
            if (!entity.FirstName.IsDefault())
                CareerApplicant.FirstName = entity.FirstName.Trim();
            if (!entity.LastName.IsDefault())
                CareerApplicant.LastName = entity.LastName.Trim();
            if (!entity.Email.IsDefault())
                CareerApplicant.Email = entity.Email.Trim();
            if (!entity.ContactNo.IsDefault())
                CareerApplicant.ContactNo = entity.ContactNo.Trim();
            if (!entity.YearofBirth.IsDefault())
                CareerApplicant.YearofBirth = entity.YearofBirth.Trim();
            if (!entity.VisaStatus.IsDefault())
                CareerApplicant.VisaStatus = entity.VisaStatus.Trim();
            if (!entity.Gender.IsDefault())
                CareerApplicant.Gender = entity.Gender.Trim();
            if (!entity.Nationality.IsDefault())
                CareerApplicant.Nationality = entity.Nationality;
            if (!entity.Residence.IsDefault())
                CareerApplicant.Residence = entity.Residence;
            if (!entity.Degree.IsDefault())
                CareerApplicant.Degree = entity.Degree;
            if (!entity.Speciality.IsDefault())
                CareerApplicant.Speciality = entity.Speciality;
            if (!entity.SubSpeciality.IsDefault())
                CareerApplicant.SubSpeciality = entity.SubSpeciality;
            if (!entity.Cv.IsDefault())
                CareerApplicant.Cv = entity.Cv.Trim();
            if (!entity.Status.IsDefault())
                CareerApplicant.Status = entity.Status;
            if (!entity.IsActive.IsDefault())
                CareerApplicant.IsActive = entity.IsActive;
            CareerApplicant.CreatedOn = entity.CreatedOn;
            CareerApplicant.ModifiedOn = entity.ModifiedOn;
            CareerApplicant.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiCareerApplicants.Add(CareerApplicant);
            SubmitChanges();
            return CareerApplicant;

        }
        public void Update(XsiCareerApplicants entity)
        {
            XsiCareerApplicants CareerApplicant = XsiContext.Context.XsiCareerApplicants.Find(entity.ItemId);
            XsiContext.Context.Entry(CareerApplicant).State = EntityState.Modified;
            if (!entity.CareerId.IsDefault())
                CareerApplicant.CareerId = entity.CareerId;
            if (!entity.FirstName.IsDefault())
                CareerApplicant.FirstName = entity.FirstName.Trim();
            if (!entity.LastName.IsDefault())
                CareerApplicant.LastName = entity.LastName.Trim();
            if (!entity.Email.IsDefault())
                CareerApplicant.Email = entity.Email.Trim();
            if (!entity.ContactNo.IsDefault())
                CareerApplicant.ContactNo = entity.ContactNo.Trim();
            if (!entity.YearofBirth.IsDefault())
                CareerApplicant.YearofBirth = entity.YearofBirth.Trim();
            if (!entity.VisaStatus.IsDefault())
                CareerApplicant.VisaStatus = entity.VisaStatus.Trim();
            if (!entity.Gender.IsDefault())
                CareerApplicant.Gender = entity.Gender.Trim();
            if (!entity.Nationality.IsDefault())
                CareerApplicant.Nationality = entity.Nationality;
            if (!entity.Residence.IsDefault())
                CareerApplicant.Residence = entity.Residence;
            if (!entity.Degree.IsDefault())
                CareerApplicant.Degree = entity.Degree;
            if (!entity.Speciality.IsDefault())
                CareerApplicant.Speciality = entity.Speciality;
            if (!entity.SubSpeciality.IsDefault())
                CareerApplicant.SubSpeciality = entity.SubSpeciality;
            if (!entity.Cv.IsDefault())
                CareerApplicant.Cv = entity.Cv.Trim();
            if (!entity.Status.IsDefault())
                CareerApplicant.Status = entity.Status;
            if (!entity.IsActive.IsDefault())
                CareerApplicant.IsActive = entity.IsActive;
            if (!entity.ModifiedOn.IsDefault())
                CareerApplicant.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                CareerApplicant.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiCareerApplicants entity)
        {
            foreach (XsiCareerApplicants CareerApplicant in Select(entity))
            {
                XsiCareerApplicants aCareerApplicant = XsiContext.Context.XsiCareerApplicants.Find(CareerApplicant.ItemId);
                XsiContext.Context.XsiCareerApplicants.Remove(aCareerApplicant);
            }
        }
        public void Delete(long itemId)
        {
            XsiCareerApplicants aCareerApplicant = XsiContext.Context.XsiCareerApplicants.Find(itemId);
            XsiContext.Context.XsiCareerApplicants.Remove(aCareerApplicant);
        }
        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
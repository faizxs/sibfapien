﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionCityRepository : IExhibitionCityRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionCityRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionCityRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionCityRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionCity GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionCity.Find(itemId);
        }
        public List<XsiExhibitionCity> Select()
        {
            return XsiContext.Context.XsiExhibitionCity.ToList();
        }
        public List<XsiExhibitionCity> Select(XsiExhibitionCity entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionCity>();
            if (!entity.CityId.IsDefault())
                predicate = predicate.And(p => p.CityId == entity.CityId);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);


            if (!entity.CityName.IsDefault())
                predicate = predicate.And(p => p.CityName.Contains(entity.CityName));

            if (!entity.CityNameAr.IsDefault())
                predicate = predicate.And(p => p.CityNameAr.Contains(entity.CityNameAr));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionCity.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionCity> SelectComeplete(XsiExhibitionCity entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionCity>();
            var predicate = PredicateBuilder.True<XsiExhibitionCity>();
            if (!entity.CityId.IsDefault())
                predicate = predicate.And(p => p.CityId == entity.CityId);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.CountryId.IsDefault())
            {
                predicate = predicate.And(p => p.CountryId == entity.CountryId);
                XsiExhibitionCountry MainEntity = XsiContext.Context.XsiExhibitionCountry.Where(p => p.CountryId == entity.CountryId).FirstOrDefault();

                if (!entity.CityNameAr.IsDefault())
                {
                    string strSearchText = entity.CityNameAr;
                    var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                    Inner = Inner.Or(p => p.CityNameAr.Contains(searchKeys.str1) || p.CityNameAr.Contains(searchKeys.str2) || p.CityNameAr.Contains(searchKeys.str3) || p.CityNameAr.Contains(searchKeys.str4) || p.CityNameAr.Contains(searchKeys.str5)
                        || p.CityNameAr.Contains(searchKeys.str6) || p.CityNameAr.Contains(searchKeys.str7) || p.CityNameAr.Contains(searchKeys.str8) || p.CityNameAr.Contains(searchKeys.str9) || p.CityNameAr.Contains(searchKeys.str10)
                        || p.CityNameAr.Contains(searchKeys.str11) || p.CityNameAr.Contains(searchKeys.str12) || p.CityNameAr.Contains(searchKeys.str13)
                        || p.CityNameAr.Contains(searchKeys.str14) || p.CityNameAr.Contains(searchKeys.str15) || p.CityNameAr.Contains(searchKeys.str16) || p.CityNameAr.Contains(searchKeys.str17)
                        );

                    isInner = true;
                }
                else
                {
                    if (!entity.CityName.IsDefault())
                    {
                        string strSearchText = entity.CityName.ToLower();
                        Inner = Inner.Or(p => p.CityName.ToLower().Contains(strSearchText));
                        isInner = true;
                    }
                }
            }
            else
            {
                if (!entity.CityName.IsDefault())
                    predicate = predicate.And(p => p.CityName.ToLower().Contains(entity.CityName));
            }

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            if (isInner)
                predicate = predicate.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionCity.Include(p => p.Country).AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionCity Add(XsiExhibitionCity entity)
        {
            XsiExhibitionCity ExhibitionCity = new XsiExhibitionCity();
            ExhibitionCity.CountryId = entity.CountryId;
            if (!entity.CityName.IsDefault())
                ExhibitionCity.CityName = entity.CityName.Trim();
            if (!entity.CityNameAr.IsDefault())
                ExhibitionCity.CityNameAr = entity.CityNameAr.Trim();
            ExhibitionCity.IsActive = entity.IsActive;
            ExhibitionCity.CreatedOn = entity.CreatedOn;
            ExhibitionCity.CreatedBy = entity.CreatedBy;
            ExhibitionCity.ModifiedOn = entity.ModifiedOn;
            ExhibitionCity.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionCity.Add(ExhibitionCity);
            SubmitChanges();
            return ExhibitionCity;

        }
        public void Update(XsiExhibitionCity entity)
        {
            XsiExhibitionCity ExhibitionCity = XsiContext.Context.XsiExhibitionCity.Find(entity.CityId);
            XsiContext.Context.Entry(ExhibitionCity).State = EntityState.Modified;

            if (!entity.CityName.IsDefault())
                ExhibitionCity.CityName = entity.CityName.Trim();

            if (!entity.CityNameAr.IsDefault())
                ExhibitionCity.CityNameAr = entity.CityNameAr.Trim();

            if (!entity.IsActive.IsDefault())
                ExhibitionCity.IsActive = entity.IsActive;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionCity.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionCity.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionCity entity)
        {
            foreach (XsiExhibitionCity ExhibitionCity in Select(entity))
            {
                XsiExhibitionCity aExhibitionCity = XsiContext.Context.XsiExhibitionCity.Find(ExhibitionCity.CityId);
                XsiContext.Context.XsiExhibitionCity.Remove(aExhibitionCity);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionCity aExhibitionCity = XsiContext.Context.XsiExhibitionCity.Find(itemId);
            XsiContext.Context.XsiExhibitionCity.Remove(aExhibitionCity);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
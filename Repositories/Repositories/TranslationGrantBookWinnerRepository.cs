﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class TranslationGrantBookWinnerRepository : ITranslationGrantBookWinnerRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public TranslationGrantBookWinnerRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public TranslationGrantBookWinnerRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public TranslationGrantBookWinnerRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionTranslationGrantBookWinner GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionTranslationGrantBookWinner.Find(itemId);
        }
        public List<XsiExhibitionTranslationGrantBookWinner> GetByGroupId(long groupId)
        {
            // return XsiContext.Context.XsiExhibitionTranslationGrantBookWinner.Where(p => p.GroupId == groupId).ToList();
            return null;
        }

        public List<XsiExhibitionTranslationGrantBookWinner> Select()
        {
            return XsiContext.Context.XsiExhibitionTranslationGrantBookWinner.ToList();
        }
        public List<XsiExhibitionTranslationGrantBookWinner> SelectOr(XsiExhibitionTranslationGrantBookWinner entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionTranslationGrantBookWinner>();
            var Outer = PredicateBuilder.True<XsiExhibitionTranslationGrantBookWinner>();

            if (!entity.NameEn.IsDefault())
            {
                string strSearchText = entity.NameAr;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.NameAr.Contains(searchKeys.str1) || p.NameAr.Contains(searchKeys.str2) || p.NameAr.Contains(searchKeys.str3) || p.NameAr.Contains(searchKeys.str4) || p.NameAr.Contains(searchKeys.str5)
                    || p.NameAr.Contains(searchKeys.str6) || p.NameAr.Contains(searchKeys.str7) || p.NameAr.Contains(searchKeys.str8) || p.NameAr.Contains(searchKeys.str9) || p.NameAr.Contains(searchKeys.str10)
                    || p.NameAr.Contains(searchKeys.str11) || p.NameAr.Contains(searchKeys.str12) || p.NameAr.Contains(searchKeys.str13)
                    || p.NameAr.Contains(searchKeys.str14) || p.NameAr.Contains(searchKeys.str15) || p.NameAr.Contains(searchKeys.str16) || p.NameAr.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.NameEn.IsDefault())
                {
                    string strSearchText = entity.NameEn.ToLower();
                    Inner = Inner.Or(p => p.NameEn.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.TranslationGrantRegistrationId.IsDefault())
                Outer = Outer.And(p => p.TranslationGrantRegistrationId == entity.TranslationGrantRegistrationId);

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionTranslationGrantBookWinner.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiExhibitionTranslationGrantBookWinner> Select(XsiExhibitionTranslationGrantBookWinner entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionTranslationGrantBookWinner>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.TranslationGrantRegistrationId.IsDefault())
                predicate = predicate.And(p => p.TranslationGrantRegistrationId == entity.TranslationGrantRegistrationId);

            if (!entity.Edition.IsDefault())
                predicate = predicate.And(p => p.Edition == entity.Edition);

            if (!entity.NameEn.IsDefault())
                predicate = predicate.And(p => p.NameEn.Contains(entity.NameEn));

            if (!entity.NameAr.IsDefault())
                predicate = predicate.And(p => p.NameAr.Contains(entity.NameAr));

            if (!entity.BookTitleEn.IsDefault())
                predicate = predicate.And(p => p.BookTitleEn.Contains(entity.BookTitleEn));

            if (!entity.BookTitleAr.IsDefault())
                predicate = predicate.And(p => p.BookTitleAr.Contains(entity.BookTitleAr));

            if (!entity.Category.IsDefault())
                predicate = predicate.And(p => p.Category == entity.Category);

            if (!entity.Year.IsDefault())
                predicate = predicate.And(p => p.Year == entity.Year);

            if (!entity.BookLanguage.IsDefault())
                predicate = predicate.And(p => p.BookLanguage == entity.BookLanguage);

            if (!entity.TranslatedTo.IsDefault())
                predicate = predicate.And(p => p.TranslatedTo == entity.TranslatedTo);

            if (!entity.BookThumbnail.IsDefault())
                predicate = predicate.And(p => p.BookThumbnail == entity.BookThumbnail);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionTranslationGrantBookWinner.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionTranslationGrantBookWinner Add(XsiExhibitionTranslationGrantBookWinner entity)
        {
            XsiExhibitionTranslationGrantBookWinner TranslationGrantBookWinner = new XsiExhibitionTranslationGrantBookWinner();

            if (!entity.TranslationGrantRegistrationId.IsDefault())
                TranslationGrantBookWinner.TranslationGrantRegistrationId = entity.TranslationGrantRegistrationId;

            if (!entity.Edition.IsDefault())
                TranslationGrantBookWinner.Edition = entity.Edition;

            if (!entity.NameEn.IsDefault())
                TranslationGrantBookWinner.NameEn = entity.NameEn.Trim();

            if (!entity.NameAr.IsDefault())
                TranslationGrantBookWinner.NameAr = entity.NameAr.Trim();

            if (!entity.BookTitleEn.IsDefault())
                TranslationGrantBookWinner.BookTitleEn = entity.BookTitleEn.Trim();

            if (!entity.BookTitleAr.IsDefault())
                TranslationGrantBookWinner.BookTitleAr = entity.BookTitleAr.Trim();

            if (!entity.Category.IsDefault())
                TranslationGrantBookWinner.Category = entity.Category;

            if (!entity.Year.IsDefault())
                TranslationGrantBookWinner.Year = entity.Year;

            if (!entity.BookLanguage.IsDefault())
                TranslationGrantBookWinner.BookLanguage = entity.BookLanguage;

            if (!entity.TranslatedTo.IsDefault())
                TranslationGrantBookWinner.TranslatedTo = entity.TranslatedTo;

            if (!entity.BookThumbnail.IsDefault())
                TranslationGrantBookWinner.BookThumbnail = entity.BookThumbnail.Trim();

            TranslationGrantBookWinner.IsActive = entity.IsActive;
            TranslationGrantBookWinner.CreatedOn = entity.CreatedOn;
            TranslationGrantBookWinner.CreatedBy = entity.CreatedBy;
            TranslationGrantBookWinner.ModifiedOn = entity.ModifiedOn;
            TranslationGrantBookWinner.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiExhibitionTranslationGrantBookWinner.Add(TranslationGrantBookWinner);
            SubmitChanges();
            return TranslationGrantBookWinner;

        }
        public void Update(XsiExhibitionTranslationGrantBookWinner entity)
        {
            XsiExhibitionTranslationGrantBookWinner TranslationGrantBookWinner = XsiContext.Context.XsiExhibitionTranslationGrantBookWinner.Find(entity.ItemId);
            XsiContext.Context.Entry(TranslationGrantBookWinner).State = EntityState.Modified;

            if (!entity.TranslationGrantRegistrationId.IsDefault())
                TranslationGrantBookWinner.TranslationGrantRegistrationId = entity.TranslationGrantRegistrationId;

            if (!entity.Edition.IsDefault())
                TranslationGrantBookWinner.Edition = entity.Edition;

            if (!entity.NameEn.IsDefault())
                TranslationGrantBookWinner.NameEn = entity.NameEn.Trim();

            if (!entity.NameAr.IsDefault())
                TranslationGrantBookWinner.NameAr = entity.NameAr.Trim();

            if (!entity.BookTitleEn.IsDefault())
                TranslationGrantBookWinner.BookTitleEn = entity.BookTitleEn.Trim();

            if (!entity.BookTitleAr.IsDefault())
                TranslationGrantBookWinner.BookTitleAr = entity.BookTitleAr.Trim();

            if (!entity.Category.IsDefault())
                TranslationGrantBookWinner.Category = entity.Category;

            if (!entity.Year.IsDefault())
                TranslationGrantBookWinner.Year = entity.Year;

            if (!entity.BookLanguage.IsDefault())
                TranslationGrantBookWinner.BookLanguage = entity.BookLanguage;

            if (!entity.TranslatedTo.IsDefault())
                TranslationGrantBookWinner.TranslatedTo = entity.TranslatedTo;

            if (!entity.BookThumbnail.IsDefault())
                TranslationGrantBookWinner.BookThumbnail = entity.BookThumbnail.Trim();

            if (!entity.IsActive.IsDefault())
                TranslationGrantBookWinner.IsActive = entity.IsActive;
            if (!entity.ModifiedOn.IsDefault())
                TranslationGrantBookWinner.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                TranslationGrantBookWinner.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionTranslationGrantBookWinner entity)
        {
            foreach (XsiExhibitionTranslationGrantBookWinner TranslationGrantBookWinner in Select(entity))
            {
                XsiExhibitionTranslationGrantBookWinner aTranslationGrantBookWinner = XsiContext.Context.XsiExhibitionTranslationGrantBookWinner.Find(TranslationGrantBookWinner.ItemId);
                XsiContext.Context.XsiExhibitionTranslationGrantBookWinner.Remove(aTranslationGrantBookWinner);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionTranslationGrantBookWinner aTranslationGrantBookWinner = XsiContext.Context.XsiExhibitionTranslationGrantBookWinner.Find(itemId);
            XsiContext.Context.XsiExhibitionTranslationGrantBookWinner.Remove(aTranslationGrantBookWinner);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
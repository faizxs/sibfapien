﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class TranslationGrantCategoryRepository : ITranslationGrantCategoryRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public TranslationGrantCategoryRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public TranslationGrantCategoryRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public TranslationGrantCategoryRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionTranslationGrantCategory GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionTranslationGrantCategory.Find(itemId);
        }

        public List<XsiExhibitionTranslationGrantCategory> Select()
        {
            return XsiContext.Context.XsiExhibitionTranslationGrantCategory.ToList();
        }
        public List<XsiExhibitionTranslationGrantCategory> SelectOr(XsiExhibitionTranslationGrantCategory entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionTranslationGrantCategory>();
            var Outer = PredicateBuilder.True<XsiExhibitionTranslationGrantCategory>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }

            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionTranslationGrantCategory.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiExhibitionTranslationGrantCategory> Select(XsiExhibitionTranslationGrantCategory entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionTranslationGrantCategory>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionTranslationGrantCategory.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionTranslationGrantCategory Add(XsiExhibitionTranslationGrantCategory entity)
        {
            XsiExhibitionTranslationGrantCategory TranslationGrantCategorys = new XsiExhibitionTranslationGrantCategory();
            if (!entity.Title.IsDefault())
                TranslationGrantCategorys.Title = entity.Title.Trim();
            TranslationGrantCategorys.IsActive = entity.IsActive;
            TranslationGrantCategorys.CreatedOn = entity.CreatedOn;
            TranslationGrantCategorys.CreatedBy = entity.CreatedBy;
            TranslationGrantCategorys.ModifiedOn = entity.ModifiedOn;
            TranslationGrantCategorys.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiExhibitionTranslationGrantCategory.Add(TranslationGrantCategorys);
            SubmitChanges();
            return TranslationGrantCategorys;

        }
        public void Update(XsiExhibitionTranslationGrantCategory entity)
        {
            XsiExhibitionTranslationGrantCategory TranslationGrantCategorys = XsiContext.Context.XsiExhibitionTranslationGrantCategory.Find(entity.ItemId);
            XsiContext.Context.Entry(TranslationGrantCategorys).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                TranslationGrantCategorys.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                TranslationGrantCategorys.IsActive = entity.IsActive;
            if (!entity.ModifiedOn.IsDefault())
                TranslationGrantCategorys.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                TranslationGrantCategorys.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionTranslationGrantCategory entity)
        {
            foreach (XsiExhibitionTranslationGrantCategory TranslationGrantCategorys in Select(entity))
            {
                XsiExhibitionTranslationGrantCategory aTranslationGrantCategorys = XsiContext.Context.XsiExhibitionTranslationGrantCategory.Find(TranslationGrantCategorys.ItemId);
                XsiContext.Context.XsiExhibitionTranslationGrantCategory.Remove(aTranslationGrantCategorys);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionTranslationGrantCategory aTranslationGrantCategorys = XsiContext.Context.XsiExhibitionTranslationGrantCategory.Find(itemId);
            XsiContext.Context.XsiExhibitionTranslationGrantCategory.Remove(aTranslationGrantCategorys);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
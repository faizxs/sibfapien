﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionShipmentRepository : IExhibitionShipmentRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionShipmentRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionShipmentRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionShipmentRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionShipment GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionShipment.Find(itemId);
        }
        
        public List<XsiExhibitionShipment> Select()
        {
            return XsiContext.Context.XsiExhibitionShipment.ToList();
        }
        public List<XsiExhibitionShipment> Select(XsiExhibitionShipment entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionShipment>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.LanguageUrl.IsDefault())
                predicate = predicate.And(p => p.LanguageUrl == entity.LanguageUrl);

            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);

            if (!entity.ExhibitorId.IsDefault())
                predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

            if (!entity.AgencyId.IsDefault())
                predicate = predicate.And(p => p.AgencyId == entity.AgencyId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);

            if (!entity.IsAgency.IsDefault())
                predicate = predicate.And(p => p.IsAgency.Equals(entity.IsAgency));

            if (!entity.ShipmentCount.IsDefault())
                predicate = predicate.And(p => p.ShipmentCount == entity.ShipmentCount);

            if (!entity.ShipmentType.IsDefault())
                predicate = predicate.And(p => p.ShipmentType == entity.ShipmentType);

            if (!entity.ShipmentCompanyName.IsDefault())
                predicate = predicate.And(p => p.ShipmentCompanyName.Contains(entity.ShipmentCompanyName));

            if (!entity.BillofLading.IsDefault())
                predicate = predicate.And(p => p.BillofLading.Contains(entity.BillofLading));

            if (!entity.ShipmentNumber.IsDefault())
                predicate = predicate.And(p => p.ShipmentNumber.Contains(entity.ShipmentNumber));

            if (!entity.ShipmentCountryFrom.IsDefault())
                predicate = predicate.And(p => p.ShipmentCountryFrom == entity.ShipmentCountryFrom);

            if (!entity.ShipmentCountryTo.IsDefault())
                predicate = predicate.And(p => p.ShipmentCountryTo == entity.ShipmentCountryTo);

            if (!entity.ReceivedShipmentLocationType.IsDefault())
                predicate = predicate.And(p => p.ReceivedShipmentLocationType == entity.ReceivedShipmentLocationType);

            if (!entity.ReceivingCompanyName.IsDefault())
                predicate = predicate.And(p => p.ReceivingCompanyName.Contains(entity.ReceivingCompanyName));

            if (!entity.QrfileName.IsDefault())
                predicate = predicate.And(p => p.QrfileName == entity.QrfileName);

            if (!entity.NoofBooks.IsDefault())
                predicate = predicate.And(p => p.NoofBooks == entity.NoofBooks);

            if (!entity.BooksFile.IsDefault())
                predicate = predicate.And(p => p.BooksFile.Equals(entity.BooksFile));

            if (!entity.IsLocalWarehouse.IsDefault())
                predicate = predicate.And(p => p.IsLocalWarehouse.Equals(entity.IsLocalWarehouse));

            if (!entity.WshipmentCount.IsDefault())
                predicate = predicate.And(p => p.WshipmentCount == entity.WshipmentCount);

            if (!entity.WshipmentCompanyName.IsDefault())
                predicate = predicate.And(p => p.WshipmentCompanyName.Contains(entity.WshipmentCompanyName));

            if (!entity.WshipmentNumber.IsDefault())
                predicate = predicate.And(p => p.WshipmentNumber.Contains(entity.WshipmentNumber));

            if (!entity.WnoofBooks.IsDefault())
                predicate = predicate.And(p => p.WnoofBooks == entity.WnoofBooks);

            if (!entity.WbooksFile.IsDefault())
                predicate = predicate.And(p => p.WbooksFile.Equals(entity.WbooksFile));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiExhibitionShipment.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionShipment Add(XsiExhibitionShipment entity)
        {
            XsiExhibitionShipment ExhibitionShipment = new XsiExhibitionShipment();

            if (!entity.MemberId.IsDefault())
                ExhibitionShipment.MemberId = entity.MemberId;

            if (!entity.ExhibitorId.IsDefault())
                ExhibitionShipment.ExhibitorId = entity.ExhibitorId;

            if (!entity.AgencyId.IsDefault())
                ExhibitionShipment.AgencyId = entity.AgencyId;

            if (!entity.ExhibitionId.IsDefault())
                ExhibitionShipment.ExhibitionId = entity.ExhibitionId;

            if (!entity.LanguageUrl.IsDefault())
                ExhibitionShipment.LanguageUrl = entity.LanguageUrl;
           
            if (!entity.IsAgency.IsDefault())
                ExhibitionShipment.IsAgency = entity.IsAgency;
            if (!entity.ShipmentCount.IsDefault())
                ExhibitionShipment.ShipmentCount = entity.ShipmentCount;
            if (!entity.ShipmentType.IsDefault())
                ExhibitionShipment.ShipmentType = entity.ShipmentType;
            if (!entity.ShipmentCompanyName.IsDefault())
                ExhibitionShipment.ShipmentCompanyName = entity.ShipmentCompanyName;
            if (!entity.BillofLading.IsDefault())
                ExhibitionShipment.BillofLading = entity.BillofLading;
            if (!entity.ShipmentNumber.IsDefault())
                ExhibitionShipment.ShipmentNumber = entity.ShipmentNumber;
            if (!entity.ShipmentCountryFrom.IsDefault())
                ExhibitionShipment.ShipmentCountryFrom = entity.ShipmentCountryFrom;
            if (!entity.ShipmentCountryTo.IsDefault())
                ExhibitionShipment.ShipmentCountryTo = entity.ShipmentCountryTo;
            if (!entity.ReceivedShipmentLocationType.IsDefault())
                ExhibitionShipment.ReceivedShipmentLocationType = entity.ReceivedShipmentLocationType;
            if (!entity.ReceivingCompanyName.IsDefault())
                ExhibitionShipment.ReceivingCompanyName = entity.ReceivingCompanyName;
            if (!entity.QrfileName.IsDefault())
                ExhibitionShipment.QrfileName = entity.QrfileName;

            if (!entity.NoofBooks.IsDefault())
                ExhibitionShipment.NoofBooks = entity.NoofBooks;

            if (!entity.BooksFile.IsDefault())
                ExhibitionShipment.BooksFile = entity.BooksFile;

            if (!entity.IsLocalWarehouse.IsDefault())
                ExhibitionShipment.IsLocalWarehouse = entity.IsLocalWarehouse;

            if (!entity.WshipmentCount.IsDefault())
                ExhibitionShipment.WshipmentCount = entity.WshipmentCount;

            if (!entity.WshipmentCompanyName.IsDefault())
                ExhibitionShipment.WshipmentCompanyName = entity.WshipmentCompanyName;

            if (!entity.WshipmentNumber.IsDefault())
                ExhibitionShipment.WshipmentNumber = entity.WshipmentNumber;

            if (!entity.WnoofBooks.IsDefault())
                ExhibitionShipment.WnoofBooks = entity.WnoofBooks;

            if (!entity.WbooksFile.IsDefault())
                ExhibitionShipment.WbooksFile = entity.WbooksFile;

            if (!entity.IsActive.IsDefault())
                ExhibitionShipment.IsActive = entity.IsActive;

            if (!entity.Status.IsDefault())
                ExhibitionShipment.Status = entity.Status;

            ExhibitionShipment.CreatedOn = entity.CreatedOn;
            ExhibitionShipment.CreatedBy = entity.CreatedBy;
            ExhibitionShipment.ModifiedOn = entity.ModifiedOn;
            ExhibitionShipment.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiExhibitionShipment.Add(ExhibitionShipment);
            SubmitChanges();
            return ExhibitionShipment;

        }
        public void Update(XsiExhibitionShipment entity)
        {
            XsiExhibitionShipment ExhibitionShipment = XsiContext.Context.XsiExhibitionShipment.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionShipment).State = EntityState.Modified;

            if (!entity.MemberId.IsDefault())
                ExhibitionShipment.MemberId = entity.MemberId;

            if (!entity.ExhibitorId.IsDefault())
                ExhibitionShipment.ExhibitorId = entity.ExhibitorId;

            if (!entity.AgencyId.IsDefault())
                ExhibitionShipment.AgencyId = entity.AgencyId;

            if (!entity.ExhibitionId.IsDefault())
                ExhibitionShipment.ExhibitionId = entity.ExhibitionId;

            if (!entity.ShipmentCount.IsDefault())
                ExhibitionShipment.ShipmentCount = entity.ShipmentCount;

            if (!entity.ShipmentType.IsDefault())
                ExhibitionShipment.ShipmentType = entity.ShipmentType;

            if (!entity.ShipmentCompanyName.IsDefault())
                ExhibitionShipment.ShipmentCompanyName = entity.ShipmentCompanyName;

            if (!entity.BillofLading.IsDefault())
                ExhibitionShipment.BillofLading = entity.BillofLading;

            if (!entity.ShipmentNumber.IsDefault())
                ExhibitionShipment.ShipmentNumber = entity.ShipmentNumber;

            if (!entity.ShipmentCountryFrom.IsDefault())
                ExhibitionShipment.ShipmentCountryFrom = entity.ShipmentCountryFrom;

            if (!entity.ShipmentCountryTo.IsDefault())
                ExhibitionShipment.ShipmentCountryTo = entity.ShipmentCountryTo;

            if (!entity.ReceivedShipmentLocationType.IsDefault())
                ExhibitionShipment.ReceivedShipmentLocationType = entity.ReceivedShipmentLocationType;

            if (!entity.ReceivingCompanyName.IsDefault())
                ExhibitionShipment.ReceivingCompanyName = entity.ReceivingCompanyName;

            if (!entity.QrfileName.IsDefault())
                ExhibitionShipment.QrfileName = entity.QrfileName;

            if (!entity.IsActive.IsDefault())
                ExhibitionShipment.IsActive = entity.IsActive;

            if (!entity.IsAgency.IsDefault())
                ExhibitionShipment.IsAgency = entity.IsAgency;

            if (!entity.NoofBooks.IsDefault())
                ExhibitionShipment.NoofBooks = entity.NoofBooks;

            if (!entity.BooksFile.IsDefault())
                ExhibitionShipment.BooksFile = entity.BooksFile;

            if (!entity.IsLocalWarehouse.IsDefault())
                ExhibitionShipment.IsLocalWarehouse = entity.IsLocalWarehouse;

            if (!entity.WshipmentCount.IsDefault())
                ExhibitionShipment.WshipmentCount = entity.WshipmentCount;

            if (!entity.WshipmentCompanyName.IsDefault())
                ExhibitionShipment.WshipmentCompanyName = entity.WshipmentCompanyName;

            if (!entity.WshipmentNumber.IsDefault())
                ExhibitionShipment.WshipmentNumber = entity.WshipmentNumber;

            if (!entity.WnoofBooks.IsDefault())
                ExhibitionShipment.WnoofBooks = entity.WnoofBooks;

            if (!entity.WbooksFile.IsDefault())
                ExhibitionShipment.WbooksFile = entity.WbooksFile;

            if (!entity.Status.IsDefault())
                ExhibitionShipment.Status = entity.Status;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionShipment.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitionShipment.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionShipment entity)
        {
            foreach (XsiExhibitionShipment ExhibitionShipment in Select(entity))
            {
                XsiExhibitionShipment aExhibitionShipment = XsiContext.Context.XsiExhibitionShipment.Find(ExhibitionShipment.ItemId);
                XsiContext.Context.XsiExhibitionShipment.Remove(aExhibitionShipment);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionShipment aExhibitionShipment = XsiContext.Context.XsiExhibitionShipment.Find(itemId);
            XsiContext.Context.XsiExhibitionShipment.Remove(aExhibitionShipment);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
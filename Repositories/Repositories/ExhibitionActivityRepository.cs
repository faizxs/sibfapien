﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitionActivityRepository : IExhibitionActivityRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionActivityRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionActivityRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionActivityRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionActivity GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionActivity.Find(itemId);
        }
         
        public List<XsiExhibitionActivity> Select()
        {
            return XsiContext.Context.XsiExhibitionActivity.ToList();
        }
        public List<XsiExhibitionActivity> SelectOr(XsiExhibitionActivity entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionActivity>();
            var Outer = PredicateBuilder.True<XsiExhibitionActivity>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
             
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionActivity.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiExhibitionActivity> Select(XsiExhibitionActivity entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionActivity>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Color.IsDefault())
                predicate = predicate.And(p => p.Color.Contains(entity.Color));
 
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);



            return XsiContext.Context.XsiExhibitionActivity.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiExhibitionActivity> GetByItemIds(long[] ItemidArray)
        {
            return XsiContext.Context.XsiExhibitionActivity.Where(p => ItemidArray.Contains(p.ItemId)).ToList();
        }

        public XsiExhibitionActivity Add(XsiExhibitionActivity entity)
        {
            XsiExhibitionActivity ExhibitionActivity = new XsiExhibitionActivity();
            
            if (!entity.Title.IsDefault())
                ExhibitionActivity.Title = entity.Title.Trim();
            if (!entity.Color.IsDefault())
                ExhibitionActivity.Color = entity.Color.Trim();
            ExhibitionActivity.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionActivity.TitleAr = entity.TitleAr.Trim(); 
            ExhibitionActivity.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            ExhibitionActivity.CreatedOn = entity.CreatedOn;
            ExhibitionActivity.CreatedBy = entity.CreatedBy;
            ExhibitionActivity.ModifiedOn = entity.ModifiedOn;
            ExhibitionActivity.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionActivity.Add(ExhibitionActivity);
            SubmitChanges();
            return ExhibitionActivity;

        }
        public void Update(XsiExhibitionActivity entity)
        {
            XsiExhibitionActivity ExhibitionActivity = XsiContext.Context.XsiExhibitionActivity.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionActivity).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionActivity.Title = entity.Title.Trim();
            if (!entity.Color.IsDefault())
                ExhibitionActivity.Color = entity.Color.Trim();
            if (!entity.IsActive.IsDefault())
                ExhibitionActivity.IsActive = entity.IsActive;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionActivity.TitleAr = entity.TitleAr.Trim(); 
            if (!entity.IsActiveAr.IsDefault())
                ExhibitionActivity.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                ExhibitionActivity.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                ExhibitionActivity.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionActivity entity)
        {
            foreach (XsiExhibitionActivity ExhibitionActivity in Select(entity))
            {
                XsiExhibitionActivity aExhibitionActivity = XsiContext.Context.XsiExhibitionActivity.Find(ExhibitionActivity.ItemId);
                XsiContext.Context.XsiExhibitionActivity.Remove(aExhibitionActivity);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionActivity aExhibitionActivity = XsiContext.Context.XsiExhibitionActivity.Find(itemId);
            XsiContext.Context.XsiExhibitionActivity.Remove(aExhibitionActivity);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
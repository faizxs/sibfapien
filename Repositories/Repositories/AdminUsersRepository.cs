﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using LinqKit;
using System.Data;
using Entities.Models;

namespace Xsi.Repositories
{
    public class AdminUsersRepository : IAdminUserRepository
    {
        #region Fields
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public AdminUsersRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public AdminUsersRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public AdminUsersRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdminUsers GetById(long itemId)
        {
            return XsiContext.Context.XsiAdminUsers.Find(itemId);
        }
        public List<XsiAdminUsers> GetByGroupId(long groupId)
        {
            return null;
        }
        public XsiAdminUsers GetByUserName(string userName)
        {
            return XsiContext.Context.XsiAdminUsers.Where(p => p.UserName == userName).FirstOrDefault();
        }

        public List<XsiAdminUsers> Select()
        {
            return XsiContext.Context.XsiAdminUsers.ToList();
        }
        public List<XsiAdminUsers> Select(XsiAdminUsers entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminUsers>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.RoleId.IsDefault())
                predicate = predicate.And(p => p.RoleId == entity.RoleId);

            if (!entity.FirstName.IsDefault())
                predicate = predicate.And(p => p.FirstName.Contains(entity.FirstName));

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email == entity.Email);

            if (!entity.UserName.IsDefault())
                predicate = predicate.And(p => p.UserName == entity.UserName);

            if (!entity.Password.IsDefault())
                predicate = predicate.And(p => p.Password == entity.Password);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive == entity.IsActive);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.IsSuperAdmin.IsDefault())
                predicate = predicate.And(p => p.IsSuperAdmin == entity.IsSuperAdmin);

            return XsiContext.Context.XsiAdminUsers.AsExpandable().Where(predicate).ToList();
        }
        public List<XsiAdminUsers> SelectAdminUserAndLogs(XsiAdminUsers entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminUsers>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.RoleId.IsDefault())
                predicate = predicate.And(p => p.RoleId == entity.RoleId);

            if (!entity.FirstName.IsDefault())
                predicate = predicate.And(p => p.FirstName.Contains(entity.FirstName));

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email == entity.Email);

            if (!entity.UserName.IsDefault())
                predicate = predicate.And(p => p.UserName == entity.UserName);

            if (!entity.Password.IsDefault())
                predicate = predicate.And(p => p.Password == entity.Password);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive == entity.IsActive);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.IsSuperAdmin.IsDefault())
                predicate = predicate.And(p => p.IsSuperAdmin == entity.IsSuperAdmin);

            return XsiContext.Context.XsiAdminUsers.Include(u => u.XsiAdminLogs).AsExpandable().Where(predicate).ToList();
        }
        public List<XsiAdminUsers> SelectAdminUserAndRoles(XsiAdminUsers entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminUsers>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.RoleId.IsDefault())
                predicate = predicate.And(p => p.RoleId == entity.RoleId);

            if (!entity.FirstName.IsDefault())
                predicate = predicate.And(p => p.FirstName.Contains(entity.FirstName));

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email == entity.Email);

            if (!entity.UserName.IsDefault())
                predicate = predicate.And(p => p.UserName == entity.UserName);

            if (!entity.Password.IsDefault())
                predicate = predicate.And(p => p.Password == entity.Password);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive == entity.IsActive);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.IsSuperAdmin.IsDefault())
                predicate = predicate.And(p => p.IsSuperAdmin == entity.IsSuperAdmin);

            return XsiContext.Context.XsiAdminUsers.Include(u => u.Role).AsExpandable().Where(predicate).ToList();

        }
        public List<XsiAdminUsers> SelectAdminUserAndPermissions(XsiAdminUsers entity)
        {
            var predicate = PredicateBuilder.True<XsiAdminUsers>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.RoleId.IsDefault())
                predicate = predicate.And(p => p.RoleId == entity.RoleId);

            if (!entity.FirstName.IsDefault())
                predicate = predicate.And(p => p.FirstName.Contains(entity.FirstName));

            if (!entity.LastName.IsDefault())
                predicate = predicate.And(p => p.LastName.Contains(entity.LastName));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email == entity.Email);

            if (!entity.UserName.IsDefault())
                predicate = predicate.And(p => p.UserName == entity.UserName);

            if (!entity.Password.IsDefault())
                predicate = predicate.And(p => p.Password == entity.Password);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive == entity.IsActive);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.IsSuperAdmin.IsDefault())
                predicate = predicate.And(p => p.IsSuperAdmin == entity.IsSuperAdmin);

            return XsiContext.Context.XsiAdminUsers.Include(u => u.Role).AsExpandable().Where(predicate).ToList();
        }

        public XsiAdminUsers Add(XsiAdminUsers entity)
        {
            XsiAdminUsers AdminUser = new XsiAdminUsers();
            AdminUser.RoleId = entity.RoleId;
            if (!entity.FirstName.IsDefault())
                AdminUser.FirstName = entity.FirstName.Trim();
            if (!entity.LastName.IsDefault())
                AdminUser.LastName = entity.LastName.Trim();
            if (!entity.Email.IsDefault())
                AdminUser.Email = entity.Email.Trim();
            if (!entity.UserName.IsDefault())
                AdminUser.UserName = entity.UserName.Trim();
            if (!entity.Password.IsDefault())
            AdminUser.Password = entity.Password.Trim();
            AdminUser.IsActive = entity.IsActive;
            AdminUser.CountryId = entity.CountryId;
            AdminUser.IsSuperAdmin = entity.IsSuperAdmin;
            AdminUser.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiAdminUsers.Add(AdminUser);

            SubmitChanges();

            return AdminUser;
        }
        public void Update(XsiAdminUsers entity)
        {
            XsiAdminUsers AdminUsers = XsiContext.Context.XsiAdminUsers.Find(entity.ItemId);
            XsiContext.Context.Entry(AdminUsers).State = EntityState.Modified;

            if (!entity.RoleId.IsDefault())
                AdminUsers.RoleId = entity.RoleId;

            if (!entity.FirstName.IsDefault())
                AdminUsers.FirstName = entity.FirstName.Trim();

            if (!entity.LastName.IsDefault())
                AdminUsers.LastName = entity.LastName.Trim();

            if (!entity.Email.IsDefault())
                AdminUsers.Email = entity.Email.Trim();

            if (!entity.UserName.IsDefault())
                AdminUsers.UserName = entity.UserName.Trim();

            if (!entity.Password.IsDefault())
                AdminUsers.Password = entity.Password.Trim();

            if (!entity.IsActive.IsDefault())
                AdminUsers.IsActive = entity.IsActive;

            if (!entity.CountryId.IsDefault())
                AdminUsers.CountryId = entity.CountryId;

            if (!entity.IsSuperAdmin.IsDefault())
                AdminUsers.IsSuperAdmin = entity.IsSuperAdmin;

            if (!entity.CreatedOn.IsDefault())
                AdminUsers.CreatedOn = entity.CreatedOn;
        }
        public void Delete(XsiAdminUsers entity)
        {
            foreach (XsiAdminUsers AdminUsers in Select(entity))
            {
                XsiAdminUsers user = XsiContext.Context.XsiAdminUsers.Find(AdminUsers.ItemId);
                XsiContext.Context.XsiAdminUsers.Remove(user);
            }
        }
        public void Delete(long itemId)
        {
            XsiAdminUsers user = XsiContext.Context.XsiAdminUsers.Find(itemId);
            XsiContext.Context.XsiAdminUsers.Remove(user);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class BooksParticipatingRepository : IBooksParticipatingRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public BooksParticipatingRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public BooksParticipatingRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public BooksParticipatingRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionBookParticipating GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionBookParticipating.Find(itemId);
        }
        public List<XsiExhibitionBookParticipating> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionBookParticipating> Select()
        {
            return XsiContext.Context.XsiExhibitionBookParticipating.ToList();
        }
        public List<XsiExhibitionBookParticipating> Select(XsiExhibitionBookParticipating entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBookParticipating>();
            if (!entity.BookId.IsDefault())
                predicate = predicate.And(p => p.BookId == entity.BookId);

            if (!entity.ExhibitorId.IsDefault())
                predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);

            if (entity.AgencyId != null && entity.AgencyId > 0)
                predicate = predicate.And(p => p.AgencyId == entity.AgencyId);

            if (!entity.Price.IsDefault())
                predicate = predicate.And(p => p.Price.Equals(entity.Price));

            if (!entity.PriceBeforeDiscount.IsDefault())
                predicate = predicate.And(p => p.PriceBeforeDiscount.Equals(entity.PriceBeforeDiscount));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            return XsiContext.Context.XsiExhibitionBookParticipating.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionBookParticipating Add(XsiExhibitionBookParticipating entity)
        {
            XsiExhibitionBookParticipating BooksParticipating = new XsiExhibitionBookParticipating();

            BooksParticipating.BookId = entity.BookId;
            if (entity.AgencyId != null && entity.AgencyId > 0)
                BooksParticipating.AgencyId = entity.AgencyId;
            BooksParticipating.ExhibitorId = entity.ExhibitorId;

            BooksParticipating.Price = entity.Price;
            BooksParticipating.PriceBeforeDiscount = entity.PriceBeforeDiscount;
            BooksParticipating.IsActive = entity.IsActive;
            BooksParticipating.CreatedOn = entity.CreatedOn;
            BooksParticipating.CreatedBy = entity.CreatedBy;
            BooksParticipating.ModifiedOn = entity.ModifiedOn;
            BooksParticipating.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionBookParticipating.Add(BooksParticipating);
            SubmitChanges();

            return BooksParticipating;

        }
        public void Update(XsiExhibitionBookParticipating entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionBookParticipating>();
            if (!entity.BookId.IsDefault())
                predicate = predicate.And(p => p.BookId == entity.BookId);
            if (!entity.ExhibitorId.IsDefault())
                predicate = predicate.And(p => p.ExhibitorId == entity.ExhibitorId);
            if (!entity.AgencyId.IsDefault() && entity.AgencyId > 0)
                predicate = predicate.And(p => p.AgencyId == entity.AgencyId);

            XsiExhibitionBookParticipating BooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.AsExpandable().Where(predicate).FirstOrDefault();
            XsiContext.Context.Entry(BooksParticipating).State = EntityState.Modified;

            if (!entity.BookId.IsDefault())
                BooksParticipating.BookId = entity.BookId;

            if (!entity.ExhibitorId.IsDefault())
                BooksParticipating.ExhibitorId = entity.ExhibitorId;

            if (!entity.AgencyId.IsDefault() && entity.AgencyId > 0)
                BooksParticipating.AgencyId = entity.AgencyId;

            if (!entity.Price.IsDefault())
                BooksParticipating.Price = entity.Price;

            if (!entity.PriceBeforeDiscount.IsDefault())
                BooksParticipating.PriceBeforeDiscount = entity.PriceBeforeDiscount;

            if (!entity.IsActive.IsDefault())
                BooksParticipating.IsActive = entity.IsActive;

            if (!entity.ModifiedOn.IsDefault())
                BooksParticipating.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                BooksParticipating.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiExhibitionBookParticipating entity)
        {
            foreach (XsiExhibitionBookParticipating BooksParticipating in Select(entity))
            {
                XsiExhibitionBookParticipating aBooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.Find(BooksParticipating.ItemId);
                XsiContext.Context.XsiExhibitionBookParticipating.Remove(aBooksParticipating);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionBookParticipating aBooksParticipating = XsiContext.Context.XsiExhibitionBookParticipating.Find(itemId);
            XsiContext.Context.XsiExhibitionBookParticipating.Remove(aBooksParticipating);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
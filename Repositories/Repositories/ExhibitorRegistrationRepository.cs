﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitorRegistrationRepository : IExhibitorRegistrationRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitorRegistrationRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitorRegistrationRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitorRegistrationRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public List<GetExhibitorsAndCountriesSCRF_Result> GetExhibitorsForAutoComplete(string keyword, long langId, long exhibitionId)
        {
            if (!string.IsNullOrEmpty(keyword))
            {
                if (langId == 1)
                {
                    return XsiContext.Context.XsiExhibitionMemberApplicationYearly.Where(p => p.MemberRoleId == 1 && p.ExhibitionId == exhibitionId && (p.Status == "A" || p.Status == "I") && p.PublisherNameEn.Contains(keyword))
                        .Join(XsiContext.Context.XsiExhibitionCountry, ema => ema.CountryId, c => c.CountryId, (ema, c) =>
                               new GetExhibitorsAndCountriesSCRF_Result
                               {
                                   ItemId = ema.MemberExhibitionYearlyId,
                                   PublisherNameEnglish = ema.PublisherNameEn,
                                   PublisherNameArabic = ema.PublisherNameAr,
                                   CountryName = c.CountryName,
                                   CountryNameAr = c.CountryNameAr
                               })
                        .ToList();
                }
                else
                {
                    return XsiContext.Context.XsiExhibitionMemberApplicationYearly.Where(p => p.MemberRoleId == 1 && p.ExhibitionId == exhibitionId && (p.Status == "A" || p.Status == "I") && p.PublisherNameAr.Contains(keyword))
                        .Join(XsiContext.Context.XsiExhibitionCountry, ema => ema.CountryId, c => c.CountryId, (ema, c) =>
                               new GetExhibitorsAndCountriesSCRF_Result
                               {
                                   ItemId = ema.MemberExhibitionYearlyId,
                                   PublisherNameEnglish = ema.PublisherNameEn,
                                   PublisherNameArabic = ema.PublisherNameAr,
                                   CountryName = c.CountryName,
                                   CountryNameAr = c.CountryNameAr
                               })
                        .ToList();
                }
            }
            else
            {
                if (langId == 1)
                {
                    return XsiContext.Context.XsiExhibitionMemberApplicationYearly.Where(p => p.MemberRoleId == 1 && p.ExhibitionId == exhibitionId && (p.Status == "A" || p.Status == "I"))
                        .Join(XsiContext.Context.XsiExhibitionCountry, ema => ema.CountryId, c => c.CountryId, (ema, c) =>
                               new GetExhibitorsAndCountriesSCRF_Result
                               {
                                   ItemId = ema.MemberExhibitionYearlyId,
                                   PublisherNameEnglish = ema.PublisherNameEn,
                                   PublisherNameArabic = ema.PublisherNameAr,
                                   CountryName = c.CountryName,
                                   CountryNameAr = c.CountryNameAr
                               })
                        .ToList();
                }
                else
                {

                    return XsiContext.Context.XsiExhibitionMemberApplicationYearly.Where(p => p.MemberRoleId == 1 && p.ExhibitionId == exhibitionId && (p.Status == "A" || p.Status == "I"))
                        .Join(XsiContext.Context.XsiExhibitionCountry, ema => ema.CountryId, c => c.CountryId, (ema, c) =>
                               new GetExhibitorsAndCountriesSCRF_Result
                               {
                                   ItemId = ema.MemberExhibitionYearlyId,
                                   PublisherNameEnglish = ema.PublisherNameEn,
                                   PublisherNameArabic = ema.PublisherNameAr,
                                   CountryName = c.CountryName,
                                   CountryNameAr = c.CountryNameAr
                               })
                        .ToList();
                }
            }

        }
        public XsiExhibitionMemberApplicationYearly GetById(long MemberExhibitionYearlyId)
        {
            return XsiContext.Context.XsiExhibitionMemberApplicationYearly.Find(MemberExhibitionYearlyId);
        }
        public XsiExhibitionMemberApplicationYearly GetByIdAll(long MemberExhibitionYearlyId)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMemberApplicationYearly>();
            if (!MemberExhibitionYearlyId.IsDefault())
                predicate = predicate.And(p => p.MemberExhibitionYearlyId == MemberExhibitionYearlyId);
            //return XsiContext.Context.XsiExhibitionMemberApplicationYearly.Include(p => p.XsiExhibition).Include(p => p.XsiExhibition.XsiWebsites)
            //    .AsExpandable().Where(predicate).FirstOrDefault();
            return null;
        }

        public List<XsiExhibitionMemberApplicationYearly> Select()
        {
            return XsiContext.Context.XsiExhibitionMemberApplicationYearly.ToList();
        }
        public List<XsiExhibitionMemberApplicationYearly> Select(XsiExhibitionMemberApplicationYearly entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMemberApplicationYearly>();
            if (!entity.MemberExhibitionYearlyId.IsDefault())
                predicate = predicate.And(p => p.MemberExhibitionYearlyId == entity.MemberExhibitionYearlyId);

            if (!entity.ExhibitionId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionId == entity.ExhibitionId);
            
            if (!entity.MemberId.IsDefault())
                predicate = predicate.And(p => p.MemberId == entity.MemberId);

            if (!entity.MemberRoleId.IsDefault())
                predicate = predicate.And(p => p.MemberRoleId == entity.MemberRoleId);

            if (!entity.ParentId.IsDefault())
                predicate = predicate.And(p => p.ParentId == entity.ParentId);

            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);

            if (!entity.CityId.IsDefault())
                predicate = predicate.And(p => p.CityId == entity.CityId);

            if (!entity.OtherCity.IsDefault())
                predicate = predicate.And(p => p.OtherCity.Equals(entity.OtherCity));

            if (!entity.ExhibitionCategoryId.IsDefault())
                predicate = predicate.And(p => p.ExhibitionCategoryId == entity.ExhibitionCategoryId);

            if (!entity.LanguageUrl.IsDefault())
                predicate = predicate.And(p => p.LanguageUrl == entity.LanguageUrl);

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.PublisherNameEn.IsDefault())
                predicate = predicate.And(p => p.PublisherNameEn.Equals(entity.PublisherNameEn));

            if (!entity.PublisherNameAr.IsDefault())
                predicate = predicate.And(p => p.PublisherNameAr.Equals(entity.PublisherNameAr));

            if (!entity.ContactPersonName.IsDefault())
                predicate = predicate.And(p => p.ContactPersonName.Contains(entity.ContactPersonName));

            if (!entity.ContactPersonNameAr.IsDefault())
                predicate = predicate.And(p => p.ContactPersonNameAr.Contains(entity.ContactPersonNameAr));

            if (!entity.ContactPersonTitle.IsDefault())
                predicate = predicate.And(p => p.ContactPersonTitle.Contains(entity.ContactPersonTitle));

            if (!entity.ContactPersonTitleAr.IsDefault())
                predicate = predicate.And(p => p.ContactPersonTitleAr.Contains(entity.ContactPersonTitleAr));

            if (!entity.Address.IsDefault())
                predicate = predicate.And(p => p.Address.Contains(entity.Address));

            if (!entity.Phone.IsDefault())
                predicate = predicate.And(p => p.Phone.Equals(entity.Phone));

            if (!entity.Fax.IsDefault())
                predicate = predicate.And(p => p.Fax.Equals(entity.Fax));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Contains(entity.Email));

            if (!entity.TotalNumberOfTitles.IsDefault())
                predicate = predicate.And(p => p.TotalNumberOfTitles.Contains(entity.TotalNumberOfTitles));

            if (!entity.TotalNumberOfNewTitles.IsDefault())
                predicate = predicate.And(p => p.TotalNumberOfNewTitles.Equals(entity.TotalNumberOfNewTitles));

            if (!entity.IsFirstTime.IsDefault())
                predicate = predicate.And(p => p.IsFirstTime.Equals(entity.IsFirstTime));

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.IsStatusChanged.IsDefault())
                predicate = predicate.And(p => p.IsStatusChanged.Equals(entity.IsStatusChanged));
           
            if (!entity.FileNumber.IsDefault())
                predicate = predicate.And(p => p.FileNumber.Equals(entity.FileNumber));

            if (!entity.Mobile.IsDefault())
                predicate = predicate.And(p => p.Mobile.Equals(entity.Mobile));

            if (!entity.Website.IsDefault())
                predicate = predicate.And(p => p.Website.Equals(entity.Website));

            if (!entity.ExactNameBoard.IsDefault())
                predicate = predicate.And(p => p.ExactNameBoard.Equals(entity.ExactNameBoard));

            if (!entity.UploadLocationMap.IsDefault())
                predicate = predicate.And(p => p.UploadLocationMap.Equals(entity.UploadLocationMap));

            if (!entity.Notes.IsDefault())
                predicate = predicate.And(p => p.Notes.Contains(entity.Notes));

            if (!entity.TempId.IsDefault())
                predicate = predicate.And(p => p.TempId.Contains(entity.TempId));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName.Equals(entity.FileName));

            if (!entity.IsEditRequest.IsDefault())
                predicate = predicate.And(p => p.IsEditRequest.Equals(entity.IsEditRequest));

            if (!entity.IsEditRequestEmailSent.IsDefault())
                predicate = predicate.And(p => p.IsEditRequestEmailSent.Equals(entity.IsEditRequestEmailSent));

            if (!entity.IsArabicPublisher.IsDefault())
                predicate = predicate.And(p => p.IsArabicPublisher.Equals(entity.IsArabicPublisher));

            if (!entity.IsInternalOrExternal.IsDefault())
                predicate = predicate.And(p => p.IsInternalOrExternal.Equals(entity.IsInternalOrExternal));

            if (!entity.Trn.IsDefault())
                predicate = predicate.And(p => p.Trn.Contains(entity.Trn));

            //if (!entity.AllocatedSpace.IsDefault())
            //    predicate = predicate.And(p => p.AllocatedSpace.Equals(entity.AllocatedSpace));

            //if (!entity.AllocatedAreaType.IsDefault())
            //    predicate = predicate.And(p => p.AllocatedAreaType.Equals(entity.AllocatedAreaType));

            //if (!entity.Area.IsDefault())
            //    predicate = predicate.And(p => p.Area.Equals(entity.Area));

            //if (!entity.RequiredAreaId.IsDefault())
            //    predicate = predicate.And(p => p.RequiredAreaId == entity.RequiredAreaId);

            //if (!entity.RequiredAreaType.IsDefault())
            //    predicate = predicate.And(p => p.RequiredAreaType.Equals(entity.RequiredAreaType));

            //if (!entity.CalculatedArea.IsDefault())
            //    predicate = predicate.And(p => p.CalculatedArea.Equals(entity.CalculatedArea));

            //if (!entity.HallNumber.IsDefault())
            //    predicate = predicate.And(p => p.HallNumber.Equals(entity.HallNumber));

            //if (!entity.StandNumberStart.IsDefault())
            //    predicate = predicate.And(p => p.StandNumberStart.Equals(entity.StandNumberStart));

            //if (!entity.StandNumberEnd.IsDefault())
            //    predicate = predicate.And(p => p.StandNumberEnd.Equals(entity.StandNumberEnd));

            //if (!entity.FirstLocation.IsDefault())
            //    predicate = predicate.And(p => p.FirstLocation.Equals(entity.FirstLocation));

            //if (!entity.SecondLocation.IsDefault())
            //    predicate = predicate.And(p => p.SecondLocation.Equals(entity.SecondLocation));

            //if (!entity.ThirdLocation.IsDefault())
            //    predicate = predicate.And(p => p.ThirdLocation.Equals(entity.ThirdLocation));

            //if (!entity.FourthLocation.IsDefault())
            //    predicate = predicate.And(p => p.FourthLocation.Equals(entity.FourthLocation));

            //if (!entity.StandCode.IsDefault())
            //    predicate = predicate.And(p => p.StandCode.Equals(entity.StandCode));


            //if (!entity.UploadWarningLetter.IsDefault())
            //    predicate = predicate.And(p => p.UploadWarningLetter.Equals(entity.UploadWarningLetter));

            //if (!entity.CancellationReason.IsDefault())
            //    predicate = predicate.And(p => p.CancellationReason.Contains(entity.CancellationReason));

            //if (!entity.TradeLicence.IsDefault())
            //    predicate = predicate.And(p => p.TradeLicence.Equals(entity.TradeLicence));

            //if (!entity.Brief.IsDefault())
            //    predicate = predicate.And(p => p.Brief.Contains(entity.Brief));

            //if (!entity.BriefAr.IsDefault())
            //    predicate = predicate.And(p => p.BriefAr.Contains(entity.BriefAr));

            //if (!entity.APU.IsDefault())
            //    predicate = predicate.And(p => p.APU.Equals(entity.APU));

            if (!entity.WifiVoucherCode.IsDefault())
                predicate = predicate.And(p => p.WifiVoucherCode.Equals(entity.WifiVoucherCode));

            if (!entity.IsRegisteredByExhibitor.IsDefault())
                predicate = predicate.And(p => p.IsRegisteredByExhibitor.Equals(entity.IsRegisteredByExhibitor));

            if (!entity.IsIncludedInInvoice.IsDefault())
                predicate = predicate.And(p => p.IsIncludedInInvoice.Equals(entity.IsIncludedInInvoice));

            if (!entity.AuthorizationLetter.IsDefault())
                predicate = predicate.And(p => p.AuthorizationLetter.Equals(entity.AuthorizationLetter));

            if (!entity.ContractCms.IsDefault())
                predicate = predicate.And(p => p.ContractCms.Contains(entity.ContractCms));

            if (!entity.ContractUser.IsDefault())
                predicate = predicate.And(p => p.ContractUser.Contains(entity.ContractUser));

            if (!entity.IsIncludedInInvoice.IsDefault())
                predicate = predicate.And(p => p.IsIncludedInInvoice == entity.IsIncludedInInvoice);

            if (!entity.AcknowledgementFileName1.IsDefault())
                predicate = predicate.And(p => p.AcknowledgementFileName1.Contains(entity.AcknowledgementFileName1));

            if (!entity.AcknowledgementFileName2.IsDefault())
                predicate = predicate.And(p => p.AcknowledgementFileName2.Contains(entity.AcknowledgementFileName2));

            if (!entity.RevenueToken.IsDefault())
                predicate = predicate.And(p => p.RevenueToken.Equals(entity.RevenueToken));

            if (!entity.RevenueTokenCreatedOn.IsDefault())
                predicate = predicate.And(p => p.RevenueTokenCreatedOn == entity.RevenueTokenCreatedOn);

            if (!entity.IsNotesRead.IsDefault())
                predicate = predicate.And(p => p.IsNotesRead.Equals(entity.IsNotesRead));

            if (!entity.IsAllowApprove.IsDefault())
                predicate = predicate.And(p => p.IsAllowApprove.Equals(entity.IsAllowApprove));

            if (!entity.IsSampleReceived.IsDefault())
                predicate = predicate.And(p => p.IsSampleReceived.Equals(entity.IsSampleReceived));

            if (!entity.SampleReceivedOn.IsDefault())
                predicate = predicate.And(p => p.SampleReceivedOn == entity.SampleReceivedOn);

            if (!entity.IsBooksUpload.IsDefault())
                predicate = predicate.And(p => p.IsBooksUpload.Equals(entity.IsBooksUpload));

            if (!entity.BookFileName.IsDefault())
                predicate = predicate.And(p => p.BookFileName.Contains(entity.BookFileName));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);

            //if (!entity..IsDefault())
            //    predicate = predicate.And(p => p.SIBF_ApplicantID == entity.SIBF_ApplicantID);

            //return XsiContext.Context.XsiExhibitionMemberApplicationYearly.Include(p => p.XsiExhibition).AsExpandable().Where(predicate).ToList();
            return XsiContext.Context.XsiExhibitionMemberApplicationYearly.AsExpandable().Where(predicate).ToList();
        }
        public List<string> Select(XsiExhibitionMemberApplicationYearly entity, List<long> exhibitionIDs)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMemberApplicationYearly>();
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => (p.Status.Equals(entity.Status) || p.Status.Equals("I")));
            if (!entity.PublisherNameEn.IsDefault())
                predicate = predicate.And(p => p.PublisherNameEn.ToLower().Contains(entity.PublisherNameEn));
            if (exhibitionIDs.Count > 0)
                predicate = predicate.And(p => exhibitionIDs.Contains(p.ExhibitionId.Value));
            return XsiContext.Context.XsiExhibitionMemberApplicationYearly.AsExpandable().Where(predicate).Select(p => p.PublisherNameEn).Distinct().Take(30).ToList();
        }
        public List<string> SelectAr(XsiExhibitionMemberApplicationYearly entity, List<long> exhibitionIDs)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMemberApplicationYearly>();
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));
            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => (p.Status.Equals(entity.Status) || p.Status.Equals("I")));
            if (!entity.PublisherNameAr.IsDefault())
                predicate = predicate.And(p => p.PublisherNameAr.Contains(entity.PublisherNameAr));
            if (exhibitionIDs.Count > 0)
                predicate = predicate.And(p => exhibitionIDs.Contains(p.ExhibitionId.Value));
            return XsiContext.Context.XsiExhibitionMemberApplicationYearly.AsExpandable().Where(predicate).Select(p => p.PublisherNameAr).Distinct().Take(30).ToList();
        }
        /*public List<GetExhibitors_Agencies_Result> SelectExhibitorAgency(string whereExhibitor, string whereAgency, string languageId)
        {
            return XsiContext.Context.GetExhibitors_Agencies(whereExhibitor, whereAgency, languageId).AsQueryable().ToList();
        }
        public List<GetExhibitors_Agencies_Result> SelectExhibitorAgencyAnd(string whereExhibitor, string whereAgency, GetExhibitors_Agencies_Result entity, List<long?> categoryIDs, string languageId)
        {
            var predicate = PredicateBuilder.True<GetExhibitors_Agencies_Result>();
            if (!entity.CountryId.IsDefault())
                predicate = predicate.And(p => p.CountryId == entity.CountryId);
            if (!entity.BoothSectionId.IsDefault())
                predicate = predicate.And(p => p.BoothSectionId == entity.BoothSectionId);
            if (!entity.BoothSubSectionId.IsDefault())
                predicate = predicate.And(p => p.BoothSubSectionId == entity.BoothSubSectionId);
            if (!entity.publishernameen.IsDefault())
                predicate = predicate.And(p => p.publishernameen.ToLower().Contains(entity.publishernameen.ToLower()));
            if (!entity.publishernamear.IsDefault())
                predicate = predicate.And(p => p.publishernamear.Contains(entity.publishernamear));
            if (categoryIDs.Count > 0)
                predicate = predicate.And(p => categoryIDs.Contains(p.ExhibitionCategoryId));

            return XsiContext.Context.GetExhibitors_Agencies(whereExhibitor, whereAgency, languageId).AsQueryable().Where(predicate).ToList();
        }
        public List<GetExhibitors_Agencies_Result> SelectExhibitorAgencyOr(string whereExhibitor, string whereAgency, GetExhibitors_Agencies_Result entity, List<long?> categoryIDs, string languageId)
        {
            var predicate = PredicateBuilder.False<GetExhibitors_Agencies_Result>();
            if (!entity.CountryId.IsDefault())
                predicate = predicate.Or(p => p.CountryId == entity.CountryId);
            if (!entity.BoothSectionId.IsDefault())
                predicate = predicate.Or(p => p.BoothSectionId == entity.BoothSectionId);
            if (!entity.BoothSubSectionId.IsDefault())
                predicate = predicate.Or(p => p.BoothSubSectionId == entity.BoothSubSectionId);
            if (!entity.publishernameen.IsDefault())
                predicate = predicate.Or(p => p.publishernameen.ToLower().Contains(entity.publishernameen.ToLower()));
            if (!entity.publishernamear.IsDefault())
                predicate = predicate.Or(p => p.publishernamear.Contains(entity.publishernamear));
            if (categoryIDs.Count > 0)
                predicate = predicate.Or(p => categoryIDs.Contains(p.ExhibitionCategoryId));

            return XsiContext.Context.GetExhibitors_Agencies(whereExhibitor, whereAgency, languageId).AsQueryable().Where(predicate).ToList();
        }
        /*public List<GetExhibitors_AgenciesToExport_Result> SelectExhibitorAgencyToExport(string exhibitorIds, string agencyIds, string languageId)
        {
            return XsiContext.Context.GetExhibitors_AgenciesToExport(exhibitorIds, agencyIds, languageId).AsQueryable().ToList();
        }
        public List<GetExhibitorsAndCountries_Result> SelectExhibitorsAndCountries()
        {
            return XsiContext.Context.GetExhibitorsAndCountries().AsQueryable().ToList();
        }
        public List<GetExhibitorsAndCountriesSCRF_Result> SelectExhibitorsAndCountriesSCRF()
        {
            return XsiContext.Context.GetExhibitorsAndCountriesSCRF().AsQueryable().ToList();
        }*/
        /*public long? SelectRequestedAreaSum()
        {
            var Result = XsiContext.Context.GetRequestedAreaSum().FirstOrDefault();
            if (Result != null)
                return Result.Value;
            return null;
        }
        public List<GetExhibitorRegistrationList_Result> SelectExhibitorList(string languageId)
        { 
            return XsiContext.Context.GetExhibitorRegistrationList(languageId).AsQueryable().ToList();
        }*/

        public XsiExhibitionMemberApplicationYearly Add(XsiExhibitionMemberApplicationYearly entity)
        {
            XsiExhibitionMemberApplicationYearly ExhibitorRegistration = new XsiExhibitionMemberApplicationYearly();
            //if (!entity.EventusIDEn.IsDefault())
            //    ExhibitorRegistration.EventusIDEn = entity.EventusIDEn.Trim();
            //if (!entity.EventusIDAr.IsDefault())
            //    ExhibitorRegistration.EventusIDAr = entity.EventusIDAr.Trim();

            ExhibitorRegistration.ExhibitionId = entity.ExhibitionId;
            ExhibitorRegistration.MemberId = entity.MemberId;
            ExhibitorRegistration.MemberRoleId = entity.MemberRoleId;
            ExhibitorRegistration.CountryId = entity.CountryId;
            ExhibitorRegistration.CityId = entity.CityId;
            if (!entity.OtherCity.IsDefault())
                ExhibitorRegistration.OtherCity = entity.OtherCity.Trim();
            ExhibitorRegistration.ExhibitionCategoryId = entity.ExhibitionCategoryId;

            ExhibitorRegistration.LanguageUrl = entity.LanguageUrl;
            ExhibitorRegistration.IsActive = entity.IsActive;
            if (!entity.PublisherNameEn.IsDefault())
                ExhibitorRegistration.PublisherNameEn = entity.PublisherNameEn.Trim();
            if (!entity.PublisherNameAr.IsDefault())
                ExhibitorRegistration.PublisherNameAr = entity.PublisherNameAr.Trim();
            if (!entity.ContactPersonName.IsDefault())
                ExhibitorRegistration.ContactPersonName = entity.ContactPersonName.Trim();
            if (!entity.ContactPersonNameAr.IsDefault())
                ExhibitorRegistration.ContactPersonNameAr = entity.ContactPersonNameAr.Trim();
            if (!entity.ContactPersonTitle.IsDefault())
                ExhibitorRegistration.ContactPersonTitle = entity.ContactPersonTitle.Trim();
            if (!entity.ContactPersonTitleAr.IsDefault())
                ExhibitorRegistration.ContactPersonTitleAr = entity.ContactPersonTitleAr.Trim();
            if (!entity.Address.IsDefault())
                ExhibitorRegistration.Address = entity.Address.Trim();
            if (!entity.Phone.IsDefault())
                ExhibitorRegistration.Phone = entity.Phone.Trim();
            if (!entity.Fax.IsDefault())
                ExhibitorRegistration.Fax = entity.Fax.Trim();
            if (!entity.Email.IsDefault())
                ExhibitorRegistration.Email = entity.Email.Trim();
            if (!entity.TotalNumberOfTitles.IsDefault())
                ExhibitorRegistration.TotalNumberOfTitles = entity.TotalNumberOfTitles.Trim();
            if (!entity.TotalNumberOfNewTitles.IsDefault())
                ExhibitorRegistration.TotalNumberOfNewTitles = entity.TotalNumberOfNewTitles.Trim();
            ExhibitorRegistration.IsFirstTime = entity.IsFirstTime;
            ExhibitorRegistration.Status = entity.Status;
            ExhibitorRegistration.IsStatusChanged = entity.IsStatusChanged;
            if (!entity.FileNumber.IsDefault())
                ExhibitorRegistration.FileNumber = entity.FileNumber.Trim();
            if (!entity.Mobile.IsDefault())
                ExhibitorRegistration.Mobile = entity.Mobile.Trim();
            if (!entity.Website.IsDefault())
                ExhibitorRegistration.Website = entity.Website.Trim();
            if (!entity.ExactNameBoard.IsDefault())
                ExhibitorRegistration.ExactNameBoard = entity.ExactNameBoard.Trim();
            ExhibitorRegistration.UploadLocationMap = entity.UploadLocationMap;
           
            if (!entity.Notes.IsDefault())
                ExhibitorRegistration.Notes = entity.Notes.Trim();
           
            if (!entity.TempId.IsDefault())
                ExhibitorRegistration.TempId = entity.TempId;

            if (!entity.FileName.IsDefault())
                ExhibitorRegistration.FileName = entity.FileName.Trim();
            if (!entity.IsEditRequest.IsDefault())
                ExhibitorRegistration.IsEditRequest = entity.IsEditRequest;
            if (!entity.IsEditRequestEmailSent.IsDefault())
                ExhibitorRegistration.IsEditRequestEmailSent = entity.IsEditRequestEmailSent;
            if (!entity.IsArabicPublisher.IsDefault())
                ExhibitorRegistration.IsArabicPublisher = entity.IsArabicPublisher;
            if (!entity.IsInternalOrExternal.IsDefault())
                ExhibitorRegistration.IsInternalOrExternal = entity.IsInternalOrExternal;

            if (!entity.Trn.IsDefault())
                ExhibitorRegistration.Trn = entity.Trn;
            if (!entity.WifiVoucherCode.IsDefault())
                ExhibitorRegistration.WifiVoucherCode = entity.WifiVoucherCode;
            if (!entity.IsStatusChanged.IsDefault())
                ExhibitorRegistration.IsStatusChanged = entity.IsStatusChanged;
            if (!entity.IsRegisteredByExhibitor.IsDefault())
                ExhibitorRegistration.IsRegisteredByExhibitor = entity.IsRegisteredByExhibitor;
            if (!entity.IsIncludedInInvoice.IsDefault())
                ExhibitorRegistration.IsIncludedInInvoice = entity.IsIncludedInInvoice;

            if (!entity.AuthorizationLetter.IsDefault())
                ExhibitorRegistration.AuthorizationLetter = entity.AuthorizationLetter;

            if (!entity.ContractCms.IsDefault())
                ExhibitorRegistration.ContractCms = entity.ContractCms;

            if (!entity.ContractUser.IsDefault())
                ExhibitorRegistration.ContractUser = entity.ContractUser;

            if (!entity.IsSampleReceived.IsDefault())
                ExhibitorRegistration.IsSampleReceived = entity.IsSampleReceived;

            if (!entity.SampleReceivedOn.IsDefault())
                ExhibitorRegistration.SampleReceivedOn = entity.SampleReceivedOn;

            if (!entity.IsBooksUpload.IsDefault())
                ExhibitorRegistration.IsBooksUpload = entity.IsBooksUpload;

            if (!entity.BookFileName.IsDefault())
                ExhibitorRegistration.BookFileName = entity.BookFileName;

            if (!entity.IsIncludedInInvoice.IsDefault())
                ExhibitorRegistration.IsIncludedInInvoice = entity.IsIncludedInInvoice.Trim();

            if (!entity.AcknowledgementFileName1.IsDefault())
                ExhibitorRegistration.AcknowledgementFileName1 = entity.AcknowledgementFileName1;

            if (!entity.AcknowledgementFileName2.IsDefault())
                ExhibitorRegistration.AcknowledgementFileName2 = entity.AcknowledgementFileName2;

            if (!entity.RevenueToken.IsDefault())
                ExhibitorRegistration.RevenueToken = entity.RevenueToken;

            if (!entity.RevenueTokenCreatedOn.IsDefault())
                ExhibitorRegistration.RevenueTokenCreatedOn = entity.RevenueTokenCreatedOn;

            if (!entity.IsNotesRead.IsDefault())
                ExhibitorRegistration.IsNotesRead = entity.IsNotesRead;

            if (!entity.IsAllowApprove.IsDefault())
                ExhibitorRegistration.IsAllowApprove = entity.IsAllowApprove;

            ExhibitorRegistration.CreatedOn = entity.CreatedOn;
            ExhibitorRegistration.CreatedBy = entity.CreatedBy;
            ExhibitorRegistration.ModifiedOn = entity.ModifiedOn;
            ExhibitorRegistration.ModifiedBy = entity.ModifiedBy;

            XsiContext.Context.XsiExhibitionMemberApplicationYearly.Add(ExhibitorRegistration);
            SubmitChanges();
            return ExhibitorRegistration;

        }
        public void Update(XsiExhibitionMemberApplicationYearly entity)
        {
            XsiExhibitionMemberApplicationYearly ExhibitorRegistration = XsiContext.Context.XsiExhibitionMemberApplicationYearly.Find(entity.MemberExhibitionYearlyId);
            XsiContext.Context.Entry(ExhibitorRegistration).State = EntityState.Modified;

            if (!entity.CountryId.IsDefault())
                ExhibitorRegistration.CountryId = entity.CountryId;

            if (!entity.CityId.IsDefault())
                ExhibitorRegistration.CityId = entity.CityId;

            if (!entity.OtherCity.IsDefault())
                ExhibitorRegistration.OtherCity = entity.OtherCity.Trim();

            if (!entity.ExhibitionCategoryId.IsDefault())
                ExhibitorRegistration.ExhibitionCategoryId = entity.ExhibitionCategoryId;

            if (!entity.IsActive.IsDefault())
                ExhibitorRegistration.IsActive = entity.IsActive;

            if (!entity.PublisherNameEn.IsDefault())
                ExhibitorRegistration.PublisherNameEn = entity.PublisherNameEn.Trim();

            if (!entity.PublisherNameAr.IsDefault())
                ExhibitorRegistration.PublisherNameAr = entity.PublisherNameAr.Trim();

            if (!entity.PublisherNameEn.IsDefault())
                ExhibitorRegistration.PublisherNameEn = entity.PublisherNameEn.Trim();

            if (!entity.ContactPersonName.IsDefault())
                ExhibitorRegistration.ContactPersonName = entity.ContactPersonName.Trim();

            if (!entity.ContactPersonNameAr.IsDefault())
                ExhibitorRegistration.ContactPersonNameAr = entity.ContactPersonNameAr.Trim();

            if (!entity.ContactPersonTitle.IsDefault())
                ExhibitorRegistration.ContactPersonTitle = entity.ContactPersonTitle.Trim();

            if (!entity.ContactPersonTitleAr.IsDefault())
                ExhibitorRegistration.ContactPersonTitleAr = entity.ContactPersonTitleAr.Trim();

            if (!entity.Address.IsDefault())
                ExhibitorRegistration.Address = entity.Address.Trim();

            if (!entity.Phone.IsDefault())
                ExhibitorRegistration.Phone = entity.Phone.Trim();

            if (!entity.Fax.IsDefault())
                ExhibitorRegistration.Fax = entity.Fax.Trim();

            if (!entity.Email.IsDefault())
                ExhibitorRegistration.Email = entity.Email.Trim();

            if (!entity.TotalNumberOfTitles.IsDefault())
                ExhibitorRegistration.TotalNumberOfTitles = entity.TotalNumberOfTitles.Trim();

            if (!entity.TotalNumberOfNewTitles.IsDefault())
                ExhibitorRegistration.TotalNumberOfNewTitles = entity.TotalNumberOfNewTitles.Trim();

            if (!entity.IsFirstTime.IsDefault())
                ExhibitorRegistration.IsFirstTime = entity.IsFirstTime;

            if (!entity.Status.IsDefault())
                ExhibitorRegistration.Status = entity.Status;

            if (!entity.IsStatusChanged.IsDefault())
                ExhibitorRegistration.IsStatusChanged = entity.IsStatusChanged;

            if (!entity.FileNumber.IsDefault())
                ExhibitorRegistration.FileNumber = entity.FileNumber.Trim();

            if (!entity.Mobile.IsDefault())
                ExhibitorRegistration.Mobile = entity.Mobile.Trim();

            if (!entity.Website.IsDefault())
                ExhibitorRegistration.Website = entity.Website.Trim();

            if (!entity.ExactNameBoard.IsDefault())
                ExhibitorRegistration.ExactNameBoard = entity.ExactNameBoard.Trim();

            if (!entity.UploadLocationMap.IsDefault())
                ExhibitorRegistration.UploadLocationMap = entity.UploadLocationMap;

            if (!entity.Notes.IsDefault())
                ExhibitorRegistration.Notes = entity.Notes.Trim();

            if (!entity.TempId.IsDefault())
                ExhibitorRegistration.TempId = entity.TempId;

            if (!entity.FileName.IsDefault())
                ExhibitorRegistration.FileName = entity.FileName;

            if (!entity.IsEditRequest.IsDefault())
                ExhibitorRegistration.IsEditRequest = entity.IsEditRequest;

            if (!entity.IsEditRequestEmailSent.IsDefault())
                ExhibitorRegistration.IsEditRequestEmailSent = entity.IsEditRequestEmailSent;

            if (!entity.IsArabicPublisher.IsDefault())
                ExhibitorRegistration.IsArabicPublisher = entity.IsArabicPublisher;

            if (!entity.IsInternalOrExternal.IsDefault())
                ExhibitorRegistration.IsInternalOrExternal = entity.IsInternalOrExternal;

            if (!entity.Trn.IsDefault())
                ExhibitorRegistration.Trn = entity.Trn;

            if (!entity.WifiVoucherCode.IsDefault())
                ExhibitorRegistration.WifiVoucherCode = entity.WifiVoucherCode;

            if (!entity.IsRegisteredByExhibitor.IsDefault())
                ExhibitorRegistration.IsRegisteredByExhibitor = entity.IsRegisteredByExhibitor;

            if (!entity.IsIncludedInInvoice.IsDefault())
                ExhibitorRegistration.IsIncludedInInvoice = entity.IsIncludedInInvoice;

            if (!entity.AuthorizationLetter.IsDefault())
                ExhibitorRegistration.AuthorizationLetter = entity.AuthorizationLetter;

            if (!entity.ContractCms.IsDefault())
                ExhibitorRegistration.ContractCms = entity.ContractCms;

            if (!entity.ContractUser.IsDefault())
                ExhibitorRegistration.ContractUser = entity.ContractUser;

            if (!entity.IsSampleReceived.IsDefault())
                ExhibitorRegistration.IsSampleReceived = entity.IsSampleReceived;

            if (!entity.SampleReceivedOn.IsDefault())
                ExhibitorRegistration.SampleReceivedOn = entity.SampleReceivedOn;

            if (!entity.IsBooksUpload.IsDefault())
                ExhibitorRegistration.IsBooksUpload = entity.IsBooksUpload;

            if (!entity.BookFileName.IsDefault())
                ExhibitorRegistration.BookFileName = entity.BookFileName;

            if (!entity.IsIncludedInInvoice.IsDefault())
                ExhibitorRegistration.IsIncludedInInvoice = entity.IsIncludedInInvoice;

            if (!entity.AcknowledgementFileName1.IsDefault())
                ExhibitorRegistration.AcknowledgementFileName1 = entity.AcknowledgementFileName1;

            if (!entity.AcknowledgementFileName2.IsDefault())
                ExhibitorRegistration.AcknowledgementFileName2 = entity.AcknowledgementFileName2;

            if (!entity.RevenueToken.IsDefault())
                ExhibitorRegistration.RevenueToken = entity.RevenueToken;

            if (!entity.RevenueTokenCreatedOn.IsDefault())
                ExhibitorRegistration.RevenueTokenCreatedOn = entity.RevenueTokenCreatedOn;

            if (!entity.IsNotesRead.IsDefault())
                ExhibitorRegistration.IsNotesRead = entity.IsNotesRead;

            if (!entity.IsAllowApprove.IsDefault())
                ExhibitorRegistration.IsAllowApprove = entity.IsAllowApprove;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitorRegistration.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitorRegistration.ModifiedBy = entity.ModifiedBy;

            //if (!entity.SIBF_ApplicantID.IsDefault())
            //    ExhibitorRegistration.SIBF_ApplicantID = entity.SIBF_ApplicantID;
        }
        public void Delete(XsiExhibitionMemberApplicationYearly entity)
        {
            foreach (XsiExhibitionMemberApplicationYearly ExhibitorRegistration in Select(entity))
            {
                XsiExhibitionMemberApplicationYearly aExhibitorRegistration = XsiContext.Context.XsiExhibitionMemberApplicationYearly.Find(ExhibitorRegistration.MemberExhibitionYearlyId);
                XsiContext.Context.XsiExhibitionMemberApplicationYearly.Remove(aExhibitorRegistration);
            }
        }
        public void Delete(long MemberExhibitionYearlyId)
        {
            XsiExhibitionMemberApplicationYearly aExhibitorRegistration = XsiContext.Context.XsiExhibitionMemberApplicationYearly.Find(MemberExhibitionYearlyId);
            XsiContext.Context.XsiExhibitionMemberApplicationYearly.Remove(aExhibitorRegistration);
        }

        #region Details
        public XsiExhibitionExhibitorDetails GetDetailsById(long MemberExhibitionYearlyId)
        {
            return XsiContext.Context.XsiExhibitionExhibitorDetails.Where(x => x.MemberExhibitionYearlyId == MemberExhibitionYearlyId).FirstOrDefault();
        }
        public XsiExhibitionExhibitorDetails Add(XsiExhibitionExhibitorDetails entity)
        {
            XsiExhibitionExhibitorDetails ExhibitorRegistrationDetails = new XsiExhibitionExhibitorDetails();
            if (!entity.MemberExhibitionYearlyId.IsDefault())
                ExhibitorRegistrationDetails.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId.Value;
            ExhibitorRegistrationDetails.BoothSectionId = entity.BoothSectionId;
            ExhibitorRegistrationDetails.BoothSubSectionId = entity.BoothSubSectionId;
            if (!entity.AllocatedSpace.IsDefault())
                ExhibitorRegistrationDetails.AllocatedSpace = entity.AllocatedSpace.Trim();
            ExhibitorRegistrationDetails.AllocatedAreaType = entity.AllocatedAreaType;
            if (!entity.Area.IsDefault())
                ExhibitorRegistrationDetails.Area = entity.Area.Trim();
            ExhibitorRegistrationDetails.RequiredAreaId = entity.RequiredAreaId;
            ExhibitorRegistrationDetails.RequiredAreaType = entity.RequiredAreaType;
            if (!entity.CalculatedArea.IsDefault())
                ExhibitorRegistrationDetails.CalculatedArea = entity.CalculatedArea.Trim();
            if (!entity.HallNumber.IsDefault())
                ExhibitorRegistrationDetails.HallNumber = entity.HallNumber.Trim();
            if (!entity.StandNumberStart.IsDefault())
                ExhibitorRegistrationDetails.StandNumberStart = entity.StandNumberStart.Trim();
            if (!entity.StandNumberEnd.IsDefault())
                ExhibitorRegistrationDetails.StandNumberEnd = entity.StandNumberEnd.Trim();
            if (!entity.FirstLocation.IsDefault())
                ExhibitorRegistrationDetails.FirstLocation = entity.FirstLocation.Trim();
            if (!entity.SecondLocation.IsDefault())
                ExhibitorRegistrationDetails.SecondLocation = entity.SecondLocation.Trim();
            if (!entity.ThirdLocation.IsDefault())
                ExhibitorRegistrationDetails.ThirdLocation = entity.ThirdLocation.Trim();
            if (!entity.FourthLocation.IsDefault())
                ExhibitorRegistrationDetails.FourthLocation = entity.FourthLocation.Trim();
            if (!entity.StandCode.IsDefault())
                ExhibitorRegistrationDetails.StandCode = entity.StandCode.Trim();
            ExhibitorRegistrationDetails.UploadWarningLetter = entity.UploadWarningLetter;
            if (!entity.CancellationReason.IsDefault())
                ExhibitorRegistrationDetails.CancellationReason = entity.CancellationReason.Trim();
            if (!entity.Apu.IsDefault())
                ExhibitorRegistrationDetails.Apu = entity.Apu.Trim();
            ExhibitorRegistrationDetails.FileName = entity.FileName;
            ExhibitorRegistrationDetails.TradeLicence = entity.TradeLicence;
            ExhibitorRegistrationDetails.StandPhoto = entity.StandPhoto;
            ExhibitorRegistrationDetails.StandLayoutByMeter = entity.StandLayoutByMeter;
            ExhibitorRegistrationDetails.RequiredPowerCapacity = entity.RequiredPowerCapacity;
            if (!entity.Brief.IsDefault())
                ExhibitorRegistrationDetails.Brief = entity.Brief.Trim();
            if (!entity.BriefAr.IsDefault())
                ExhibitorRegistrationDetails.BriefAr = entity.BriefAr.Trim();
            ExhibitorRegistrationDetails.IsInternalOrExternal = entity.IsInternalOrExternal;

            ExhibitorRegistrationDetails.BoothDetail = entity.BoothDetail;

            ExhibitorRegistrationDetails.CreatedOn = entity.CreatedOn;
            ExhibitorRegistrationDetails.CreatedBy = entity.CreatedBy;
            ExhibitorRegistrationDetails.ModifiedOn = entity.ModifiedOn;
            ExhibitorRegistrationDetails.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiExhibitionExhibitorDetails.Add(ExhibitorRegistrationDetails);
            SubmitChanges();
            return ExhibitorRegistrationDetails;

        }
        public void Update(XsiExhibitionExhibitorDetails entity)
        {
            XsiExhibitionExhibitorDetails ExhibitorRegistrationDetails = XsiContext.Context.XsiExhibitionExhibitorDetails.Find(entity.ExhibitorDetailsId);
            XsiContext.Context.Entry(ExhibitorRegistrationDetails).State = EntityState.Modified;

            if (!entity.BoothSectionId.IsDefault())
                ExhibitorRegistrationDetails.BoothSectionId = entity.BoothSectionId;

            if (!entity.BoothSubSectionId.IsDefault())
                ExhibitorRegistrationDetails.BoothSubSectionId = entity.BoothSubSectionId;

            if (!entity.AllocatedSpace.IsDefault())
                ExhibitorRegistrationDetails.AllocatedSpace = entity.AllocatedSpace.Trim();

            if (!entity.AllocatedAreaType.IsDefault())
                ExhibitorRegistrationDetails.AllocatedAreaType = entity.AllocatedAreaType;

            if (!entity.Area.IsDefault())
                ExhibitorRegistrationDetails.Area = entity.Area.Trim();

            if (!entity.RequiredAreaId.IsDefault())
                ExhibitorRegistrationDetails.RequiredAreaId = entity.RequiredAreaId;

            if (!entity.RequiredAreaType.IsDefault())
                ExhibitorRegistrationDetails.RequiredAreaType = entity.RequiredAreaType;

            if (!entity.CalculatedArea.IsDefault())
                ExhibitorRegistrationDetails.CalculatedArea = entity.CalculatedArea.Trim();

            if (!entity.HallNumber.IsDefault())
                ExhibitorRegistrationDetails.HallNumber = entity.HallNumber.Trim();

            if (!entity.StandNumberStart.IsDefault())
                ExhibitorRegistrationDetails.StandNumberStart = entity.StandNumberStart.Trim();

            if (!entity.StandNumberEnd.IsDefault())
                ExhibitorRegistrationDetails.StandNumberEnd = entity.StandNumberEnd.Trim();

            if (!entity.FirstLocation.IsDefault())
                ExhibitorRegistrationDetails.FirstLocation = entity.FirstLocation.Trim();

            if (!entity.SecondLocation.IsDefault())
                ExhibitorRegistrationDetails.SecondLocation = entity.SecondLocation.Trim();

            if (!entity.ThirdLocation.IsDefault())
                ExhibitorRegistrationDetails.ThirdLocation = entity.ThirdLocation.Trim();

            if (!entity.FourthLocation.IsDefault())
                ExhibitorRegistrationDetails.FourthLocation = entity.FourthLocation.Trim();

            if (!entity.StandCode.IsDefault())
                ExhibitorRegistrationDetails.StandCode = entity.StandCode.Trim();

            if (!entity.UploadWarningLetter.IsDefault())
                ExhibitorRegistrationDetails.UploadWarningLetter = entity.UploadWarningLetter;

            if (!entity.CancellationReason.IsDefault())
                ExhibitorRegistrationDetails.CancellationReason = entity.CancellationReason.Trim();

            if (!entity.FileName.IsDefault())
                ExhibitorRegistrationDetails.FileName = entity.FileName;

            if (!entity.TradeLicence.IsDefault())
                ExhibitorRegistrationDetails.TradeLicence = entity.TradeLicence;

            if (!entity.StandPhoto.IsDefault())
                ExhibitorRegistrationDetails.StandPhoto = entity.StandPhoto;

            if (!entity.StandLayoutByMeter.IsDefault())
                ExhibitorRegistrationDetails.StandLayoutByMeter = entity.StandLayoutByMeter;

            if (!entity.RequiredPowerCapacity.IsDefault())
                ExhibitorRegistrationDetails.RequiredPowerCapacity = entity.RequiredPowerCapacity;

            if (!entity.Apu.IsDefault())
                ExhibitorRegistrationDetails.Apu = entity.Apu.Trim();

            if (!entity.Brief.IsDefault())
                ExhibitorRegistrationDetails.Brief = entity.Brief.Trim();

            if (!entity.BriefAr.IsDefault())
                ExhibitorRegistrationDetails.BriefAr = entity.BriefAr.Trim();

            if (!entity.IsInternalOrExternal.IsDefault())
                ExhibitorRegistrationDetails.IsInternalOrExternal = entity.IsInternalOrExternal;

            if (!entity.BoothDetail.IsDefault())
                ExhibitorRegistrationDetails.BoothDetail = entity.BoothDetail;

            if (!entity.ModifiedOn.IsDefault())
                ExhibitorRegistrationDetails.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedBy.IsDefault())
                ExhibitorRegistrationDetails.ModifiedBy = entity.ModifiedBy;

            //if (!entity.SIBF_ApplicantID.IsDefault())
            //    ExhibitorRegistration.SIBF_ApplicantID = entity.SIBF_ApplicantID;
        }
        #endregion

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
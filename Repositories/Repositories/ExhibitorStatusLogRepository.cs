﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class ExhibitorStatusLogRepository : IExhibitorStatusLogRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitorStatusLogRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitorStatusLogRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitorStatusLogRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionMemberApplicationYearlyLogs GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Find(itemId);
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetByGroupId(long groupId)
        {
            return null;
        }

        public List<XsiExhibitionMemberApplicationYearlyLogs> Select()
        {
            return XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.ToList();
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> Select(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionMemberApplicationYearlyLogs>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.MemberExhibitionYearlyId.IsDefault())
                predicate = predicate.And(p => p.MemberExhibitionYearlyId == entity.MemberExhibitionYearlyId);

            if (!entity.AdminId.IsDefault())
                predicate = predicate.And(p => p.AdminId == entity.AdminId);

            if (!entity.Status.IsDefault())
                predicate = predicate.And(p => p.Status.Equals(entity.Status));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);


            return XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.AsExpandable().Where(predicate).ToList();
        }

        public XsiExhibitionMemberApplicationYearlyLogs Add(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            XsiExhibitionMemberApplicationYearlyLogs ExhibitorStatusLog = new XsiExhibitionMemberApplicationYearlyLogs();
            ExhibitorStatusLog.MemberExhibitionYearlyId = entity.MemberExhibitionYearlyId;
            ExhibitorStatusLog.AdminId = entity.AdminId;
            ExhibitorStatusLog.Status = entity.Status;
            ExhibitorStatusLog.CreatedOn = entity.CreatedOn;

            XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Add(ExhibitorStatusLog);
            SubmitChanges();
            return ExhibitorStatusLog;

        }
        public void Update(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            XsiExhibitionMemberApplicationYearlyLogs ExhibitorStatusLog = XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitorStatusLog).State = EntityState.Modified;

            if (!entity.Status.IsDefault())
                ExhibitorStatusLog.Status = entity.Status;
        }
        public void Delete(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            foreach (XsiExhibitionMemberApplicationYearlyLogs ExhibitorStatusLog in Select(entity))
            {
                XsiExhibitionMemberApplicationYearlyLogs aExhibitorStatusLog = XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Find(ExhibitorStatusLog.ItemId);
                XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Remove(aExhibitorStatusLog);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionMemberApplicationYearlyLogs aExhibitorStatusLog = XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Find(itemId);
            XsiContext.Context.XsiExhibitionMemberApplicationYearlyLogs.Remove(aExhibitorStatusLog);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class GenreRepository : IGenreRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public GenreRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public GenreRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public GenreRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiGenre GetById(long itemId)
        {
            return XsiContext.Context.XsiGenre.Find(itemId);
        }
        
        public List<XsiGenre> Select()
        {
            return XsiContext.Context.XsiGenre.ToList();
        }
        public List<XsiGenre> SelectOr(XsiGenre entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiGenre>();
            var Outer = PredicateBuilder.True<XsiGenre>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                    || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                    || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                    || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                    );

                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();
                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));
                    isInner = true;
                }
            }
             
            if (!entity.IsActive.IsDefault())
                Outer = Outer.And(p => p.IsActive.Equals(entity.IsActive));

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiGenre.AsExpandable().Where(Outer.Expand()).ToList();
        }
        public List<XsiGenre> Select(XsiGenre entity)
        {
            var predicate = PredicateBuilder.True<XsiGenre>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));
             
            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn == entity.CreatedOn);

            if (!entity.CreatedBy.IsDefault())
                predicate = predicate.And(p => p.CreatedBy == entity.CreatedBy);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn == entity.ModifiedOn);

            if (!entity.ModifiedBy.IsDefault())
                predicate = predicate.And(p => p.ModifiedBy == entity.ModifiedBy);


            return XsiContext.Context.XsiGenre.AsExpandable().Where(predicate).ToList();
        }

        public XsiGenre Add(XsiGenre entity)
        {
            XsiGenre Genre = new XsiGenre();
            
            if (!entity.Title.IsDefault())
            Genre.Title = entity.Title.Trim();
            Genre.IsActive = entity.IsActive;
            #region Ar
            if (!entity.TitleAr.IsDefault())
                Genre.TitleAr = entity.TitleAr.Trim();
            Genre.IsActiveAr = entity.IsActiveAr;
            #endregion Ar
            Genre.CreatedOn = entity.CreatedOn;
            Genre.CreatedBy = entity.CreatedBy;
            Genre.ModifiedOn = entity.ModifiedOn;
            Genre.ModifiedBy = entity.ModifiedBy;
            XsiContext.Context.XsiGenre.Add(Genre);
            SubmitChanges();
            return Genre;

        }
        public void Update(XsiGenre entity)
        {
            XsiGenre Genre = XsiContext.Context.XsiGenre.Find(entity.ItemId);
            XsiContext.Context.Entry(Genre).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                Genre.Title = entity.Title.Trim();
            if (!entity.IsActive.IsDefault())
                Genre.IsActive = entity.IsActive;
         
            #region Ar
            if (!entity.TitleAr.IsDefault())
                Genre.TitleAr = entity.TitleAr.Trim();
            if (!entity.IsActiveAr.IsDefault())
                Genre.IsActiveAr = entity.IsActiveAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                Genre.ModifiedOn = entity.ModifiedOn;
            if (!entity.ModifiedBy.IsDefault())
                Genre.ModifiedBy = entity.ModifiedBy;
        }
        public void Delete(XsiGenre entity)
        {
            foreach (XsiGenre Genre in Select(entity))
            {
                XsiGenre aGenre = XsiContext.Context.XsiGenre.Find(Genre.ItemId);
                XsiContext.Context.XsiGenre.Remove(aGenre);
            }
        }
        public void Delete(long itemId)
        {
            XsiGenre aGenre = XsiContext.Context.XsiGenre.Find(itemId);
            XsiContext.Context.XsiGenre.Remove(aGenre);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
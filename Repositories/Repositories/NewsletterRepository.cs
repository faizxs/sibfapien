﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using LinqKit;
using Entities.Models;

namespace Xsi.Repositories
{
    public class NewsletterRepository : INewsletterRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public NewsletterRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public NewsletterRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public NewsletterRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiNewsletter GetById(long itemId)
        {
            return XsiContext.Context.XsiNewsletter.Find(itemId);
        }
       
        public List<XsiNewsletter> Select()
        {
            return XsiContext.Context.XsiNewsletter.ToList();
        }
        public List<XsiNewsletter> Select(XsiNewsletter entity)
        {
            var predicate = PredicateBuilder.True<XsiNewsletter>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

         
            if (!entity.IssueNumber.IsDefault())
                predicate = predicate.And(p => p.IssueNumber.Contains(entity.IssueNumber));

            if (!entity.Url.IsDefault())
                predicate = predicate.And(p => p.Url.Equals(entity.Url));

            if (!entity.IsActive.IsDefault())
                predicate = predicate.And(p => p.IsActive.Equals(entity.IsActive));

            if (!entity.CreatedOn.IsDefault())
                predicate = predicate.And(p => p.CreatedOn.Equals(entity.CreatedOn));

            if (!entity.CreatedById.IsDefault())
                predicate = predicate.And(p => p.CreatedById == entity.CreatedById);

            if (!entity.ModifiedOn.IsDefault())
                predicate = predicate.And(p => p.ModifiedOn.Equals(entity.ModifiedOn));

            if (!entity.ModifiedById.IsDefault())
                predicate = predicate.And(p => p.ModifiedById == entity.ModifiedById);

            if (!entity.Dated.IsDefault())
                predicate = predicate.And(p => p.Dated.Equals(entity.Dated));

            if (!entity.FileName.IsDefault())
                predicate = predicate.And(p => p.FileName == entity.FileName);

            return XsiContext.Context.XsiNewsletter.AsExpandable().Where(predicate).ToList();
        }

        public XsiNewsletter Add(XsiNewsletter entity)
        {
            XsiNewsletter Newsletter = new XsiNewsletter();
             
            if (!entity.IssueNumber.IsDefault())
                Newsletter.IssueNumber = entity.IssueNumber.Trim();
            if (!entity.Url.IsDefault())
                Newsletter.Url = entity.Url.Trim();
            Newsletter.IsActive = entity.IsActive;
            Newsletter.Dated = entity.Dated;
            Newsletter.FileName = entity.FileName;
            #region Ar           
            if (!entity.IssueNumberAr.IsDefault())
                Newsletter.IssueNumberAr = entity.IssueNumberAr.Trim();
            if (!entity.Urlar.IsDefault())
                Newsletter.Urlar = entity.Urlar.Trim();
            if (!entity.IsActiveAr.IsDefault())
                Newsletter.IsActiveAr = entity.IsActiveAr;
            if (!entity.DatedAr.IsDefault())
                Newsletter.DatedAr = entity.DatedAr;
            if (!entity.FileNameAr.IsDefault())
                Newsletter.FileNameAr = entity.FileNameAr;
            #endregion Ar

            Newsletter.CreatedOn = entity.CreatedOn;
            Newsletter.CreatedById = entity.CreatedById;
            Newsletter.ModifiedOn = entity.ModifiedOn;
            Newsletter.ModifiedById = entity.ModifiedById;
            
            XsiContext.Context.XsiNewsletter.Add(Newsletter);
            SubmitChanges();
            return Newsletter;

        }
        public void Update(XsiNewsletter entity)
        {
            XsiNewsletter Newsletter = XsiContext.Context.XsiNewsletter.Find(entity.ItemId);
            XsiContext.Context.Entry(Newsletter).State = EntityState.Modified;

            if (!entity.IssueNumber.IsDefault())
                Newsletter.IssueNumber = entity.IssueNumber.Trim();

            if (!entity.Url.IsDefault())
                Newsletter.Url = entity.Url.Trim();

            if (!entity.IsActive.IsDefault())
                Newsletter.IsActive = entity.IsActive;

            if (!entity.Dated.IsDefault())
                Newsletter.Dated = entity.Dated;

            if (!entity.FileName.IsDefault())
                Newsletter.FileName = entity.FileName;

            #region Ar      
            if (!entity.IssueNumberAr.IsDefault())
                Newsletter.IssueNumberAr = entity.IssueNumberAr.Trim();
            if (!entity.Urlar.IsDefault())
                Newsletter.Urlar = entity.Urlar.Trim();
            if (!entity.IsActiveAr.IsDefault())
                Newsletter.IsActiveAr = entity.IsActiveAr;
            if (!entity.DatedAr.IsDefault())
                Newsletter.DatedAr = entity.DatedAr;
            if (!entity.FileNameAr.IsDefault())
                Newsletter.FileNameAr = entity.FileNameAr;
            #endregion Ar

            if (!entity.ModifiedOn.IsDefault())
                Newsletter.ModifiedOn = entity.ModifiedOn;

            if (!entity.ModifiedById.IsDefault())
                Newsletter.ModifiedById = entity.ModifiedById;
             
        }
        public void Delete(XsiNewsletter entity)
        {
            foreach (XsiNewsletter Newsletter in Select(entity))
            {
                XsiNewsletter aNewsletter = XsiContext.Context.XsiNewsletter.Find(Newsletter.ItemId);
                XsiContext.Context.XsiNewsletter.Remove(aNewsletter);
            }
        }
        public void Delete(long itemId)
        {
            XsiNewsletter aNewsletter = XsiContext.Context.XsiNewsletter.Find(itemId);
            XsiContext.Context.XsiNewsletter.Remove(aNewsletter);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
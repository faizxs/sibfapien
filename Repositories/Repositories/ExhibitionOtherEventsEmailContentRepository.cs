﻿using Entities.Models;
using LinqKit;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
namespace Xsi.Repositories
{
    public class ExhibitionOtherEventsEmailContentRepository : IExhibitionOtherEventsEmailContentRepository
    {
        #region Feilds
        XsiDbContext XsiContext;
        #endregion
        #region Constructor
        public ExhibitionOtherEventsEmailContentRepository()
        {
            XsiContext = new XsiDbContext();
        }
        public ExhibitionOtherEventsEmailContentRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                XsiContext = new XsiDbContext();
            else
                XsiContext = new XsiDbContext(connectionString);
        }
        public ExhibitionOtherEventsEmailContentRepository(XsiDbContext context)
        {
            XsiContext = context;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiContext != null)
                this.XsiContext.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionOtherEventsEmailContent GetById(long itemId)
        {
            return XsiContext.Context.XsiExhibitionOtherEventsEmailContent.Find(itemId);
        }

        public List<XsiExhibitionOtherEventsEmailContent> Select()
        {
            return XsiContext.Context.XsiExhibitionOtherEventsEmailContent.ToList();
        }
        public List<XsiExhibitionOtherEventsEmailContent> Select(XsiExhibitionOtherEventsEmailContent entity)
        {
            var predicate = PredicateBuilder.True<XsiExhibitionOtherEventsEmailContent>();
            if (!entity.ItemId.IsDefault())
                predicate = predicate.And(p => p.ItemId == entity.ItemId);

            if (!entity.Title.IsDefault())
                predicate = predicate.And(p => p.Title.Contains(entity.Title));

            if (!entity.Subject.IsDefault())
                predicate = predicate.And(p => p.Subject.Contains(entity.Subject));

            if (!entity.Email.IsDefault())
                predicate = predicate.And(p => p.Email.Contains(entity.Email));

            if (!entity.Body.IsDefault())
                predicate = predicate.And(p => p.Body.Contains(entity.Body));

            if (!entity.RollbackXml.IsDefault())
                predicate = predicate.And(p => p.RollbackXml.Equals(entity.RollbackXml));


            return XsiContext.Context.XsiExhibitionOtherEventsEmailContent.AsExpandable().Where(predicate).ToList();
        }

        public List<XsiExhibitionOtherEventsEmailContent> SelectOr(XsiExhibitionOtherEventsEmailContent entity)
        {
            bool isInner = false;
            var Inner = PredicateBuilder.False<XsiExhibitionOtherEventsEmailContent>();
            var Outer = PredicateBuilder.True<XsiExhibitionOtherEventsEmailContent>();

            if (!entity.Title.IsDefault())
            {
                string strSearchText = entity.Title;
                var searchKeys = AabicSearchKeys.GetSearchKeys(strSearchText);

                Inner = Inner.Or(p => p.Title.Contains(searchKeys.str1) || p.Title.Contains(searchKeys.str2) || p.Title.Contains(searchKeys.str3) || p.Title.Contains(searchKeys.str4) || p.Title.Contains(searchKeys.str5)
                     || p.Title.Contains(searchKeys.str6) || p.Title.Contains(searchKeys.str7) || p.Title.Contains(searchKeys.str8) || p.Title.Contains(searchKeys.str9) || p.Title.Contains(searchKeys.str10)
                     || p.Title.Contains(searchKeys.str11) || p.Title.Contains(searchKeys.str12) || p.Title.Contains(searchKeys.str13)
                     || p.Title.Contains(searchKeys.str14) || p.Title.Contains(searchKeys.str15) || p.Title.Contains(searchKeys.str16) || p.Title.Contains(searchKeys.str17)
                     || p.Title.Contains(searchKeys.str18) || p.Title.Contains(searchKeys.str19) || p.Title.Contains(searchKeys.str20) || p.Title.Contains(searchKeys.str21)
                        || p.Title.Contains(searchKeys.str22) || p.Title.Contains(searchKeys.str23) || p.Title.Contains(searchKeys.str24) || p.Title.Contains(searchKeys.str25)
                        || p.Title.Contains(searchKeys.str26) || p.Title.Contains(searchKeys.str27)
                     );
                Inner = Inner.Or(p => p.Subject.Contains(searchKeys.str1) || p.Subject.Contains(searchKeys.str2) || p.Subject.Contains(searchKeys.str3) || p.Subject.Contains(searchKeys.str4) || p.Subject.Contains(searchKeys.str5)
                    || p.Subject.Contains(searchKeys.str6) || p.Subject.Contains(searchKeys.str7) || p.Subject.Contains(searchKeys.str8) || p.Subject.Contains(searchKeys.str9) || p.Subject.Contains(searchKeys.str10)
                    || p.Subject.Contains(searchKeys.str11) || p.Subject.Contains(searchKeys.str12) || p.Subject.Contains(searchKeys.str13)
                    || p.Subject.Contains(searchKeys.str14) || p.Subject.Contains(searchKeys.str15) || p.Subject.Contains(searchKeys.str16) || p.Subject.Contains(searchKeys.str17)
                    || p.Subject.Contains(searchKeys.str18) || p.Subject.Contains(searchKeys.str19) || p.Subject.Contains(searchKeys.str20) || p.Subject.Contains(searchKeys.str21)
                        || p.Subject.Contains(searchKeys.str22) || p.Subject.Contains(searchKeys.str23) || p.Subject.Contains(searchKeys.str24) || p.Subject.Contains(searchKeys.str25)
                        || p.Subject.Contains(searchKeys.str26) || p.Subject.Contains(searchKeys.str27)
                    );
                isInner = true;
            }
            else
            {
                if (!entity.Title.IsDefault())
                {
                    string strSearchText = entity.Title.ToLower();

                    if (!entity.Subject.IsDefault())
                        Inner = Inner.Or(p => p.Subject.ToLower().Contains(strSearchText));

                    Inner = Inner.Or(p => p.Title.ToLower().Contains(strSearchText));

                    isInner = true;
                }
            }

            if (isInner)
                Outer = Outer.And(Inner.Expand());

            return XsiContext.Context.XsiExhibitionOtherEventsEmailContent.AsExpandable().Where(Outer.Expand()).ToList();
        }

        public XsiExhibitionOtherEventsEmailContent Add(XsiExhibitionOtherEventsEmailContent entity)
        {
            XsiExhibitionOtherEventsEmailContent ExhibitionOtherEventsEmailContent = new XsiExhibitionOtherEventsEmailContent();

            if (!entity.Title.IsDefault())
                ExhibitionOtherEventsEmailContent.Title = entity.Title.Trim();
            if (!entity.Subject.IsDefault())
                ExhibitionOtherEventsEmailContent.Subject = entity.Subject.Trim();
            if (!entity.Email.IsDefault())
                ExhibitionOtherEventsEmailContent.Email = entity.Email.Trim();
            if (!entity.Body.IsDefault())
                ExhibitionOtherEventsEmailContent.Body = entity.Body.Trim();
            ExhibitionOtherEventsEmailContent.RollbackXml = entity.RollbackXml;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionOtherEventsEmailContent.TitleAr = entity.TitleAr.Trim();
            if (!entity.SubjectAr.IsDefault())
                ExhibitionOtherEventsEmailContent.SubjectAr = entity.SubjectAr.Trim();
            if (!entity.EmailAr.IsDefault())
                ExhibitionOtherEventsEmailContent.EmailAr = entity.EmailAr.Trim();
            if (!entity.BodyAr.IsDefault())
                ExhibitionOtherEventsEmailContent.BodyAr = entity.BodyAr.Trim();
            ExhibitionOtherEventsEmailContent.RollbackXmlAr = entity.RollbackXmlAr;
            #endregion Ar

            XsiContext.Context.XsiExhibitionOtherEventsEmailContent.Add(ExhibitionOtherEventsEmailContent);
            SubmitChanges();
            return ExhibitionOtherEventsEmailContent;

        }
        public void Update(XsiExhibitionOtherEventsEmailContent entity)
        {
            XsiExhibitionOtherEventsEmailContent ExhibitionOtherEventsEmailContent = XsiContext.Context.XsiExhibitionOtherEventsEmailContent.Find(entity.ItemId);
            XsiContext.Context.Entry(ExhibitionOtherEventsEmailContent).State = EntityState.Modified;

            if (!entity.Title.IsDefault())
                ExhibitionOtherEventsEmailContent.Title = entity.Title.Trim();

            if (!entity.Subject.IsDefault())
                ExhibitionOtherEventsEmailContent.Subject = entity.Subject.Trim();

            if (!entity.Email.IsDefault())
                ExhibitionOtherEventsEmailContent.Email = entity.Email.Trim();

            if (!entity.Body.IsDefault())
                ExhibitionOtherEventsEmailContent.Body = entity.Body.Trim();

            if (!entity.RollbackXml.IsDefault())
                ExhibitionOtherEventsEmailContent.RollbackXml = entity.RollbackXml;

            #region Ar
            if (!entity.TitleAr.IsDefault())
                ExhibitionOtherEventsEmailContent.TitleAr = entity.TitleAr.Trim();
            if (!entity.SubjectAr.IsDefault())
                ExhibitionOtherEventsEmailContent.SubjectAr = entity.SubjectAr.Trim();
            if (!entity.EmailAr.IsDefault())
                ExhibitionOtherEventsEmailContent.EmailAr = entity.EmailAr.Trim();
            if (!entity.BodyAr.IsDefault())
                ExhibitionOtherEventsEmailContent.BodyAr = entity.BodyAr.Trim();
            if (!entity.RollbackXmlAr.IsDefault())
                ExhibitionOtherEventsEmailContent.RollbackXmlAr = entity.RollbackXmlAr;
            #endregion Ar
        }
        public void Delete(XsiExhibitionOtherEventsEmailContent entity)
        {
            foreach (XsiExhibitionOtherEventsEmailContent ExhibitionOtherEventsEmailContent in Select(entity))
            {
                XsiExhibitionOtherEventsEmailContent aExhibitionOtherEventsEmailContent = XsiContext.Context.XsiExhibitionOtherEventsEmailContent.Find(ExhibitionOtherEventsEmailContent.ItemId);
                XsiContext.Context.XsiExhibitionOtherEventsEmailContent.Remove(aExhibitionOtherEventsEmailContent);
            }
        }
        public void Delete(long itemId)
        {
            XsiExhibitionOtherEventsEmailContent aExhibitionOtherEventsEmailContent = XsiContext.Context.XsiExhibitionOtherEventsEmailContent.Find(itemId);
            XsiContext.Context.XsiExhibitionOtherEventsEmailContent.Remove(aExhibitionOtherEventsEmailContent);
        }

        public int SubmitChanges()
        {
            return XsiContext.Save();
        }
        #endregion
    }
}
﻿using System;
using Entities.Models;

namespace Xsi.Repositories
{
    public class XsiDbContext : IDisposable
    {
        readonly sibfnewdbContext _XsiEntitiesContext;
        internal sibfnewdbContext Context { get { return _XsiEntitiesContext; } }

        public XsiDbContext(string connectionString)
        {
            _XsiEntitiesContext = new sibfnewdbContext();
        }

        public XsiDbContext()
        {
            _XsiEntitiesContext = new sibfnewdbContext();
        }

        public int Save()
        {
            return _XsiEntitiesContext.SaveChanges();
        }
        public void Dispose()
        {
            if (_XsiEntitiesContext != null)
                _XsiEntitiesContext.Dispose();
        }
    }
}

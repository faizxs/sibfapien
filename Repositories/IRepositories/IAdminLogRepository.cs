﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IAdminLogRepository : IBaseEntityRepository<XsiAdminLogs>
    {
        XsiAdminLogs GetLogLastLogin(long userId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IPagesContentRepository : IBaseEntityRepository<XsiPagesContentNew>
    {
        //List<sp_FetchSIBFSearchResults_Result> SelectSearch(string searchString, long languageId);
        List<XsiPagesContentNew> SelectOr(XsiPagesContentNew entity);
    }
}

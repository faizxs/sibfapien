﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISCRFEmailContentRepository : IBaseEntityRepository<XsiScrfemailContent>
    {
        List<XsiScrfemailContent> SelectOr(XsiScrfemailContent entity);
    }
}

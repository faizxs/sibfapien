﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IVideoCategoryRepository : IBaseEntityRepository<XsiVideoCategory>
    {
        List<XsiVideoCategory> SelectOr(XsiVideoCategory entity);
    }
}

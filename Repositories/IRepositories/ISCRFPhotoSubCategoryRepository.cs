﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISCRFPhotoSubCategoryRepository : IBaseEntityRepository<XsiScrfphotoSubCategory>
    {
        List<XsiScrfphotoSubCategory> SelectComeplete(XsiScrfphotoSubCategory entity);
    }
}

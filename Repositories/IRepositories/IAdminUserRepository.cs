﻿using System.Collections.Generic;
using Entities.Models;
using System.Linq;

namespace Xsi.Repositories
{
    public interface IAdminUserRepository : IBaseEntityRepository<XsiAdminUsers>
    {
        XsiAdminUsers GetByUserName(string userName);
        List<XsiAdminUsers> SelectAdminUserAndLogs(XsiAdminUsers entity);
        List<XsiAdminUsers> SelectAdminUserAndRoles(XsiAdminUsers entity);
        List<XsiAdminUsers> SelectAdminUserAndPermissions(XsiAdminUsers entity);
    }
}

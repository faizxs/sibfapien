﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xsi.Repositories
{
    public interface IPPProgramRepository : IBaseEntityRepository<XsiExhibitionPpprogram>
    {
    }
}

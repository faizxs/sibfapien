﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionCountryAirportRepository : IBaseEntityRepository<XsiExhibitionCountryAirport>
    {
        List<XsiExhibitionCountryAirport> SelectComplete(XsiExhibitionCountryAirport entity);
    }
}

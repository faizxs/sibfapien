﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IAgencyRegistrationRepository : IBaseEntityRepository<XsiExhibitionMemberApplicationYearly>
    {
        List<XsiExhibitionMemberApplicationYearly> SelectExact(XsiExhibitionMemberApplicationYearly entity);
        List<string> SelectAgencyName(XsiExhibitionMemberApplicationYearly entity);
        List<string> SelectAgencyNameAr(XsiExhibitionMemberApplicationYearly entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionBookTypeRepository : IBaseEntityRepository<XsiExhibitionBookType>
    {
        List<XsiExhibitionBookType> SelectOr(XsiExhibitionBookType entity);
    }
}

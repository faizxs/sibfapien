﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionCurrencyRepository : IBaseEntityRepository<XsiExhibitionCurrency>
    {
        List<XsiExhibitionCurrency> SelectOr(XsiExhibitionCurrency entity);
    }
}

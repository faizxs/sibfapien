﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ITranslationGrantBookWinnerRepository : IBaseEntityRepository<XsiExhibitionTranslationGrantBookWinner>
    {
        List<XsiExhibitionTranslationGrantBookWinner> SelectOr(XsiExhibitionTranslationGrantBookWinner entity);
    }
}

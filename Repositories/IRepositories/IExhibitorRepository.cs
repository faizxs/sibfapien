﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitorRepository : IBaseEntityRepository<XsiExhibitor>
    {
        List<XsiExhibitor> SelectOr(XsiExhibitor entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IVIPGuestWebsiteRepository : IBaseEntityRepository<XsiVipguestWebsite>
    {
        List<long?> Select(long ID1, long ID2, long ID3);
    }
}

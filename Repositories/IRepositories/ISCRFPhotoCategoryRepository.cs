﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISCRFPhotoCategoryRepository : IBaseEntityRepository<XsiScrfphotoCategory>
    {
        List<XsiScrfphotoCategory> SelectOr(XsiScrfphotoCategory entity);
        List<XsiScrfphotoCategory> SelectOtherLanguageCategory(XsiScrfphotoCategory entity);
        List<XsiScrfphotoCategory> SelectCurrentLanguageCategory(XsiScrfphotoCategory entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionCountryRepository : IBaseEntityRepository<XsiExhibitionCountry>
    {
        List<XsiExhibitionCountry> SelectOr(XsiExhibitionCountry entity);
        List<XsiExhibitionCountry> SelectOtherLanguageCategory(XsiExhibitionCountry entity);
        List<XsiExhibitionCountry> SelectCurrentLanguageCategory(XsiExhibitionCountry entity);
        List<XsiExhibitionCountry> SelectComplete(XsiExhibitionCountry entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IAdminPermissionRepository : IBaseEntityRepository<XsiAdminPermissions>
    {
        List<XsiAdminPermissions> SelectAdminPermissionAndUser(XsiAdminPermissions entity);
        List<XsiAdminPermissions> SelectAdminPermissionAndRole(XsiAdminPermissions entity);
        List<XsiAdminPermissions> SelectAdminPermissionAndPage(XsiAdminPermissions entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IVisitorRepository : IBaseEntityRepository<XsiVisitor>
    {
        List<XsiVisitor> SelectOr(XsiVisitor entity);
    }
}

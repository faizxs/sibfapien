﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IEmailContentRepository : IBaseEntityRepository<XsiEmailContent>
    {
        List<XsiEmailContent> SelectOr(XsiEmailContent entity);
    }
}

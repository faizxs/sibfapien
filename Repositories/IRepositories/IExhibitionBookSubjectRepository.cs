﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionBookSubjectRepository : IBaseEntityRepository<XsiExhibitionBookSubject>
    {
        List<XsiExhibitionBookSubject> SelectOr(XsiExhibitionBookSubject entity);
        List<XsiExhibitionBookSubject> SelectOtherLanguageCategory(XsiExhibitionBookSubject entity);
        List<XsiExhibitionBookSubject> SelectCurrentLanguageCategory(XsiExhibitionBookSubject entity);
    }
}

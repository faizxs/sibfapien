﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISchoolRegistrationRepository : IBaseEntityRepository<XsiSchoolRegistration>
    {
        List<XsiSchoolRegistration> SelectOr(XsiSchoolRegistration entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IAdminRoleRepository : IBaseEntityRepository<XsiAdminRoles>
    {
        List<XsiAdminRoles> SelectAdminRoleAndPermissions(XsiAdminRoles entity);
        List<XsiAdminRoles> SelectAdminRoleAndUsers(XsiAdminRoles entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ITranslationGrantCategoryRepository : IBaseEntityRepository<XsiExhibitionTranslationGrantCategory>
    {
        List<XsiExhibitionTranslationGrantCategory> SelectOr(XsiExhibitionTranslationGrantCategory entity);
    }
}

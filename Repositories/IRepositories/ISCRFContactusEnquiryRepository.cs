﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISCRFContactusEnquiryRepository : IBaseEntityRepository<XsiScrfcontactusEnquiry>
    {
        List<XsiScrfcontactusEnquiry> SelectOr(XsiScrfcontactusEnquiry entity);
    }
}

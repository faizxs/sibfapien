﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IEmailCategoryRepository : IBaseEntityRepository<XsiEmailCategory>
    {
        List<XsiEmailCategory> SelectOr(XsiEmailCategory entity);
    }
}

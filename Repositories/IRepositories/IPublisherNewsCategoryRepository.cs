﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IPublisherNewsCategoryRepository : IBaseEntityRepository<XsiPublisherNewsCategory>
    {
        List<XsiPublisherNewsCategory> SelectOr(XsiPublisherNewsCategory entity);
        List<XsiPublisherNewsCategory> SelectOtherLanguageCategory(XsiPublisherNewsCategory entity);
        List<XsiPublisherNewsCategory> SelectCurrentLanguageCategory(XsiPublisherNewsCategory entity);
    }
}

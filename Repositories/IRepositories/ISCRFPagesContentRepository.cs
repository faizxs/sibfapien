﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISCRFPagesContentRepository : IBaseEntityRepository<XsiScrfpagesContent>
    {
        List<XsiScrfpagesContent> SelectOr(XsiScrfpagesContent entity);
    }
}

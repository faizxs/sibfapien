﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IRestaurantRepository : IBaseEntityRepository<XsiExhibitionMemberApplicationYearly>
    {
        //List<GetExhibitors_Agencies_Result> SelectExhibitorAgency(string whereExhibitor, string whereAgency, string languageId);
        //List<GetExhibitors_AgenciesToExport_Result> SelectExhibitorAgencyToExport(string exhibitorsIds, string agencyIds, string languageid);
        //List<GetExhibitorsAndCountries_Result> SelectExhibitorsAndCountries();
        //List<GetExhibitors_Agencies_Result> SelectExhibitorAgencyAnd(string whereExhibitor, string whereAgency, GetExhibitors_Agencies_Result entity, List<long?> categoryIDs, string languageId);
        //List<GetExhibitors_Agencies_Result> SelectExhibitorAgencyOr(string whereExhibitor, string whereAgency, GetExhibitors_Agencies_Result entity, List<long?> categoryIDs, string languageId);
        //List<GetExhibitorRegistrationList_Result> SelectExhibitorList(string languageId);
        List<string> Select(XsiExhibitionMemberApplicationYearly entity, List<long> exhibitionIDs);
        List<string> SelectAr(XsiExhibitionMemberApplicationYearly entity, List<long> exhibitionIDs);
        List<GetExhibitorsAndCountriesSCRF_Result> GetExhibitorsForAutoComplete(string keyword, long langId, long exhibitionId);
        XsiExhibitionMemberApplicationYearly GetByIdAll(long itemId);
        XsiExhibitionExhibitorDetails GetDetailsById(long MemberExhibitionYearlyId);
        XsiExhibitionExhibitorDetails Add(XsiExhibitionExhibitorDetails entity);
        void Update(XsiExhibitionExhibitorDetails entity);
    }
}

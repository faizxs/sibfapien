﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IAwardsRepository : IBaseEntityRepository<XsiAwards>
    {
        List<XsiAwards> SelectOr(XsiAwards entity);
        List<XsiAwards> SelectOtherLanguageCategory(XsiAwards entity);
        List<XsiAwards> SelectCurrentLanguageCategory(XsiAwards entity);
    }
}

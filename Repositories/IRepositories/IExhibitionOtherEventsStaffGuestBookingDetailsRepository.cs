﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionOtherEventsStaffGuestBookingDetailsRepository : IBaseEntityRepository<XsiExhibitionOtherEventsStaffGuestBookingDetails>
    {
        List<XsiSgbdto> SelectBookingListForMember(long memberid,long exhibitionid, string bookingtype);
    }
}

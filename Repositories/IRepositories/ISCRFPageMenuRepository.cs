﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISCRFPageMenuRepository : IBaseEntityRepository<XsiScrfpageMenu>
    {
        List<XsiScrfpageMenu> SelectOr(XsiScrfpageMenu entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionPublisherRepository : IBaseEntityRepository<XsiExhibitionBookPublisher>
    {
        long GetNameCount(long itemID, string title, string titlear);
        Array SelectUI(XsiExhibitionBookPublisher entity, long exhibitionGroupID);
        List<XsiExhibitionBookPublisher> SelectSCRFUI(XsiExhibitionBookPublisher entity, long exhibitionGroupID);
    }
}

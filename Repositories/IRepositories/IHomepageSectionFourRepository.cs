﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IHomepageSectionFourRepository : IBaseEntityRepository<XsiHomepageSectionFour>
    {
        List<XsiHomepageSectionFour> SelectComeplete(XsiHomepageSectionFour entity, long langId);
    }
}

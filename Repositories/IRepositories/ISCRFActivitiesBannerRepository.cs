﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISCRFActivitiesBannerRepository : IBaseEntityRepository<XsiScrfactivitiesBanner>
    {
        List<XsiScrfactivitiesBanner> SelectOr(XsiScrfactivitiesBanner entity);
    }
}

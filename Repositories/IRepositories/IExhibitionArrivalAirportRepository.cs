﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionArrivalAirportRepository : IBaseEntityRepository<XsiExhibitionArrivalAirport>
    {
        List<XsiExhibitionArrivalAirport> SelectOr(XsiExhibitionArrivalAirport entity);
        List<XsiExhibitionArrivalAirport> SelectOtherLanguageCategory(XsiExhibitionArrivalAirport entity);
        List<XsiExhibitionArrivalAirport> SelectCurrentLanguageCategory(XsiExhibitionArrivalAirport entity);
        List<XsiExhibitionArrivalAirport> SelectComplete(XsiExhibitionArrivalAirport entity);
    }
}

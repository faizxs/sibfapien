﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IProfessionalProgramRegistrationRepository : IBaseEntityRepository<XsiExhibitionProfessionalProgramRegistration>
    {
        List<XsiExhibitionProfessionalProgramRegistration> SelectSuggestedProfessionals(XsiExhibitionProfessionalProgramRegistration entity);
        List<XsiExhibitionProfessionalProgramRegistration> SelectOtherProfessionals(List<long?> genreGroupIDList, List<long> suggestedIDList, XsiExhibitionProfessionalProgramRegistration entity);
        List<XsiExhibitionProfessionalProgramRegistration> SelectOtherProfessionalsSearch(List<long?> genreGroupIDList, List<long> suggestedIDList, XsiExhibitionProfessionalProgramRegistration entity, string name);
        List<XsiExhibitionProfessionalProgramRegistration> SelectOtherProfessionals(XsiExhibitionProfessionalProgramRegistration entity);
        List<XsiExhibitionProfessionalProgramRegistration> SelectOtherProfessionalsSearch(XsiExhibitionProfessionalProgramRegistration entity, string name);
        //List<GetApporvedMeetUps_Result> SelectApprovedMeetups(long professionalProgramGroupID);
       // List<GetApporvedMeetUpsByMember_Result> SelectApprovedMeetupsByMember(long professionalProgramGroupID, long profesionalProgramRegistrationID);
        List<XsiExhibitionProfessionalProgramRegistration> SelectPPRegistrationCMS(XsiExhibitionProfessionalProgramRegistration entity);
    }
}

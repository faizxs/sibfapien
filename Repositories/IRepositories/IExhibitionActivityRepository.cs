﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionActivityRepository : IBaseEntityRepository<XsiExhibitionActivity>
    {
        List<XsiExhibitionActivity> SelectOr(XsiExhibitionActivity entity);
        List<XsiExhibitionActivity> GetByItemIds(long[] ItemidArray);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionStaffGuestParticipatingRepository : IBaseEntityRepository<XsiExhibitionStaffGuestParticipating>
    {
        XsiExhibitionStaffGuestParticipating GetByStaffGuestId(long itemId);
    }
}

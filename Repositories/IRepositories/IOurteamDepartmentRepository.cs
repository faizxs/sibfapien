﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IOurteamDepartmentRepository : IBaseEntityRepository<XsiOurteamDepartment>
    {
        List<XsiOurteamDepartment> SelectOr(XsiOurteamDepartment entity);
        List<XsiOurteamDepartment> SelectOtherLanguageCategory(XsiOurteamDepartment entity);
        List<XsiOurteamDepartment> SelectCurrentLanguageCategory(XsiOurteamDepartment entity);
    }
}

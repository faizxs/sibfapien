﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IEventRepository : IBaseEntityRepository<XsiEvent>
    {
       // List<GetEvents_Result> SelectEventsResult(long dayVal, long monthVal, long yearVal, string groupidsList, long languageId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IStaffGuestCategoryRepository : IBaseEntityRepository<XsiStaffGuestCategory>
    {
        List<XsiStaffGuestCategory> SelectOr(XsiStaffGuestCategory entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IHomePageLinkRepository : IBaseEntityRepository<XsiHomePageLink>
    {
        List<XsiHomePageLink> SelectOr(XsiHomePageLink entity);
    }
}

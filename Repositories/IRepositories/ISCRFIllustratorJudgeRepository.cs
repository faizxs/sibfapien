﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISCRFIllustratorJudgeRepository : IBaseEntityRepository<XsiScrfillustratorJudge>
    {
        List<XsiScrfillustratorJudge> SelectOr(XsiScrfillustratorJudge entity);
    }
}

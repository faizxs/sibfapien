﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IAnnouncementRepository : IBaseEntityRepository<XsiAnnouncement>
    {
        List<XsiAnnouncement> SelectOr(XsiAnnouncement entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IEventCategoryRepository : IBaseEntityRepository<XsiEventCategory>
    {
        List<XsiEventCategory> SelectOr(XsiEventCategory entity);
        List<XsiEventCategory> SelectOtherLanguageCategory(XsiEventCategory entity);
        List<XsiEventCategory> SelectCurrentLanguageCategory(XsiEventCategory entity);
    }
}

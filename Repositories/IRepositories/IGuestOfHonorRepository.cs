﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IGuestOfHonorRepository : IBaseEntityRepository<XsiGuestOfHonor>
    {
        List<XsiGuestOfHonor> SelectOr(XsiGuestOfHonor entity);
    }
}

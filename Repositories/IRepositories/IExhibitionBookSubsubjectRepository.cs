﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionBookSubsubjectRepository : IBaseEntityRepository<XsiExhibitionBookSubsubject>
    {
        List<XsiExhibitionBookSubsubject> SelectComeplete(XsiExhibitionBookSubsubject entity);
    }
}

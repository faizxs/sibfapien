﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IDownloadCategoryRepository : IBaseEntityRepository<XsiDownloadCategory>
    {
        List<XsiDownloadCategory> SelectOr(XsiDownloadCategory entity);
        List<XsiDownloadCategory> SelectOtherLanguageCategory(XsiDownloadCategory entity);
        List<XsiDownloadCategory> SelectCurrentLanguageCategory(XsiDownloadCategory entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface INewsCategoryRepository : IBaseEntityRepository<XsiNewsCategory>
    {
        List<XsiNewsCategory> SelectOr(XsiNewsCategory entity);
        List<XsiNewsCategory> SelectOtherLanguageCategory(XsiNewsCategory entity);
        List<XsiNewsCategory> SelectCurrentLanguageCategory(XsiNewsCategory entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IGenreRepository : IBaseEntityRepository<XsiGenre>
    {
        List<XsiGenre> SelectOr(XsiGenre entity);
    }
}

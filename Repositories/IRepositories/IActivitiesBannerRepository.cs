﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IActivitiesBannerRepository : IBaseEntityRepository<XsiActivitiesBanner>
    {
        List<XsiActivitiesBanner> SelectOr(XsiActivitiesBanner entity);
    }
}

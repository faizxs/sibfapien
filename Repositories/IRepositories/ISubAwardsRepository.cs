﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISubAwardsRepository : IBaseEntityRepository<XsiSubAwards>
    {
        List<XsiSubAwards> SelectComeplete(XsiSubAwards entity);
    }
}

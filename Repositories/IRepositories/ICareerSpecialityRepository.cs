﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ICareerSpecialityRepository : IBaseEntityRepository<XsiCareerSpeciality>
    {
        List<XsiCareerSpeciality> SelectOr(XsiCareerSpeciality entity);
    }
}

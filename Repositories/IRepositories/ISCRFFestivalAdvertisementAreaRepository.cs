﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISCRFFestivalAdvertisementAreaRepository : IBaseEntityRepository<XsiScrffestivalAdvertisementArea>
    {
        List<XsiScrffestivalAdvertisementArea> SelectOr(XsiScrffestivalAdvertisementArea entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IHomepageSectionTwoRepository : IBaseEntityRepository<XsiHomepageSectionTwo>
    {
        List<XsiHomepageSectionTwo> SelectComeplete(XsiHomepageSectionTwo entity,long langId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IAdminPageRepository : IBaseEntityRepository<XsiAdminPages>
    {
        List<XsiAdminPages> SelectAdminPageAndPermissions(XsiAdminPages entity);
    }
}

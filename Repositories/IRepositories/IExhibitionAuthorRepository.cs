﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionAuthorRepository : IBaseEntityRepository<XsiExhibitionAuthor>
    {
        long GetNameCount(long itemID, string title, string titlear);
        Array SelectUI(XsiExhibitionAuthor entity, long exhibitionGroupID);
        Array SelectUIForCMS(XsiExhibitionAuthor entity);
        List<XsiExhibitionAuthor> SelectSCRFUI(XsiExhibitionAuthor entity, long exhibitionGroupID);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ICareerRepository : IBaseEntityRepository<XsiCareer>
    {
        List<XsiCareer> SelectOr(XsiCareer entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IPublisherNewsRepository : IBaseEntityRepository<XsiPublisherNews>
    {
        List<XsiPublisherNews> SelectComeplete(XsiPublisherNews entity,long langId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IActivitiesSuggestionRepository : IBaseEntityRepository<XsiActivitiesSuggestion>
    {
        List<XsiActivitiesSuggestion> SelectOr(XsiActivitiesSuggestion entity);
    }
}

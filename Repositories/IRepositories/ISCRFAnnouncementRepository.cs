﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISCRFAnnouncementRepository : IBaseEntityRepository<XsiScrfannouncement>
    {
        List<XsiScrfannouncement> SelectOr(XsiScrfannouncement entity);
    }
}

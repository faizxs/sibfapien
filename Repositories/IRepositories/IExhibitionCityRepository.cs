﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionCityRepository : IBaseEntityRepository<XsiExhibitionCity>
    {
        List<XsiExhibitionCity> SelectComeplete(XsiExhibitionCity entity);
    }
}

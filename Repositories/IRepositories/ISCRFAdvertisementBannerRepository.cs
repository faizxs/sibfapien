﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISCRFAdvertisementBannerRepository : IBaseEntityRepository<XsiScrfadvertisementBanner>
    {
        List<XsiScrfadvertisementBanner> SelectOr(XsiScrfadvertisementBanner entity);
    }
}

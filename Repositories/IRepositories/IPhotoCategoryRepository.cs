﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IPhotoCategoryRepository : IBaseEntityRepository<XsiPhotoCategory>
    {
        List<XsiPhotoCategory> SelectOr(XsiPhotoCategory entity);
       // List<XsiPhotoCategory> SelectOtherLanguageCategory(XsiPhotoCategory entity);
       // List<XsiPhotoCategory> SelectCurrentLanguageCategory(XsiPhotoCategory entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ISCRFCountryRepository : IBaseEntityRepository<XsiScrfcountry>
    {
        List<XsiScrfcountry> SelectOr(XsiScrfcountry entity);
        List<XsiScrfcountry> SelectOtherLanguageCategory(XsiScrfcountry entity);
        List<XsiScrfcountry> SelectCurrentLanguageCategory(XsiScrfcountry entity);
        List<XsiScrfcountry> SelectComplete(XsiScrfcountry entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionBookRepository : IBaseEntityRepository<XsiExhibitionBooks>
    {
        List<XsiExhibitionBookForUi> SelectBooksForUI(XsiExhibitionBooks entity);
        List<XsiExhibitionBookForUi> SelectBooksUIAnd(XsiExhibitionBooks entity, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId);
        List<XsiExhibitionBookForUi> SelectBooksUIOr(XsiExhibitionBooks entity, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId);
        List<XsiExhibitionBookForUi> SelectBooksTitle(XsiExhibitionBooks entity, long websiteId);
       // void UpdatePublisherId(long? oldPublisherID, long? newPublisherID);
        //void UpdateAuthorId(long? oldAuthorID, long? newAuthorID);
        List<XsiExhibitionBookForUi> SelectBooksUITopSearchable(XsiExhibitionBooks entity, long websiteId);

        List<XsiExhibitionBookForUi> SelectBooksUIAnd1(XsiExhibitionBooks entity, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId);

        // void UpdateAuthorInBooks(long oldAuthorId, long newAuthorId, long AdminUserId);
        //void UpdatePublisherInBooks(long oldPublisherId, long newPublisherId, long AdminUserId);
        void AddAuthor(XsiExhibitionBookAuthor entity);
        void DeleteBookAuthor(long bookId);
        void AddBookParticipating(XsiExhibitionBookParticipating entity);
        void UpdateBookParticipating(XsiExhibitionBookParticipating entity);
    }
}

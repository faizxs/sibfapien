﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionBoothRepository : IBaseEntityRepository<XsiExhibitionBooth>
    {
        List<XsiExhibitionBooth> SelectOr(XsiExhibitionBooth entity);
        List<XsiExhibitionBooth> SelectOtherLanguageCategory(XsiExhibitionBooth entity);
        List<XsiExhibitionBooth> SelectCurrentLanguageCategory(XsiExhibitionBooth entity);
        List<XsiExhibitionBooth> SelectComplete(XsiExhibitionBooth entity);
    }
}

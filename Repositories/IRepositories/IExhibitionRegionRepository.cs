﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface IExhibitionRegionRepository : IBaseEntityRepository<XsiExhibitionRegion>
    {
        List<XsiExhibitionRegion> SelectOr(XsiExhibitionRegion entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ICareerDepartmentRepository : IBaseEntityRepository<XsiCareerDepartment>
    {
        List<XsiCareerDepartment> SelectOr(XsiCareerDepartment entity);
    }
}

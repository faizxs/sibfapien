﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace Xsi.Repositories
{
    public interface ICareerApplicantsRepository : IBaseEntityRepository<XsiCareerApplicants>
    {
        List<XsiCareerApplicants> SelectOr(XsiCareerApplicants entity);
    }
}

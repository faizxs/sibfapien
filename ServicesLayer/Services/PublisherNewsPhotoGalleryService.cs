﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PublisherNewsPhotoGalleryService : IDisposable
    {
        #region Feilds
        PublisherNewsPhotoGalleryBL PublisherNewsPhotoGalleryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PublisherNewsPhotoGalleryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return PublisherNewsPhotoGalleryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PublisherNewsPhotoGalleryService()
        {
            PublisherNewsPhotoGalleryBL = new PublisherNewsPhotoGalleryBL();
        }
        public PublisherNewsPhotoGalleryService(string connectionString)
        {
            PublisherNewsPhotoGalleryBL = new PublisherNewsPhotoGalleryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PublisherNewsPhotoGalleryBL != null)
                this.PublisherNewsPhotoGalleryBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return PublisherNewsPhotoGalleryBL.GetNextGroupId();
        }

        public long GetNextSortIndex()
        {
            return PublisherNewsPhotoGalleryBL.GetNextSortIndex();
        }
        public XsiPublisherNewsPhotoGallery GetPublisherNewsPhotoGalleryByItemId(long itemId)
        {
            return PublisherNewsPhotoGalleryBL.GetPublisherNewsPhotoGalleryByItemId(itemId);
        }
         
        public List<XsiPublisherNewsPhotoGallery> GetPublisherNewsPhotoGallery()
        {
            return PublisherNewsPhotoGalleryBL.GetPublisherNewsPhotoGallery();
        }
        public List<XsiPublisherNewsPhotoGallery> GetPublisherNewsPhotoGallery(EnumSortlistBy sortListBy)
        {
            return PublisherNewsPhotoGalleryBL.GetPublisherNewsPhotoGallery(sortListBy);
        }
        public List<XsiPublisherNewsPhotoGallery> GetPublisherNewsPhotoGallery(XsiPublisherNewsPhotoGallery entity)
        {
            return PublisherNewsPhotoGalleryBL.GetPublisherNewsPhotoGallery(entity);
        }
        public List<XsiPublisherNewsPhotoGallery> GetPublisherNewsPhotoGallery(XsiPublisherNewsPhotoGallery entity, EnumSortlistBy sortListBy)
        {
            return PublisherNewsPhotoGalleryBL.GetPublisherNewsPhotoGallery(entity, sortListBy);
        }
        public XsiPublisherNewsPhotoGallery GetPublisherNewsPhotoGallery(long groupId, long languageId)
        {
            return PublisherNewsPhotoGalleryBL.GetPublisherNewsPhotoGallery(groupId, languageId);
        }

        public EnumResultType InsertPublisherNewsPhotoGallery(XsiPublisherNewsPhotoGallery entity)
        {
            return PublisherNewsPhotoGalleryBL.InsertPublisherNewsPhotoGallery(entity);
        }
        public EnumResultType UpdatePublisherNewsPhotoGallery(XsiPublisherNewsPhotoGallery entity)
        {
            return PublisherNewsPhotoGalleryBL.UpdatePublisherNewsPhotoGallery(entity);
        }
        public EnumResultType UpdatePublisherNewsPhotoGalleryStatus(long itemId)
        {
            return PublisherNewsPhotoGalleryBL.UpdatePublisherNewsPhotoGalleryStatus(itemId);
        }
        public EnumResultType DeletePublisherNewsPhotoGallery(XsiPublisherNewsPhotoGallery entity)
        {
            return PublisherNewsPhotoGalleryBL.DeletePublisherNewsPhotoGallery(entity);
        }
        public EnumResultType DeletePublisherNewsPhotoGallery(long itemId)
        {
            return PublisherNewsPhotoGalleryBL.DeletePublisherNewsPhotoGallery(itemId);
        }
        #endregion
    }
}
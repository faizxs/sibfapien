﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class NewsCategoryService : IDisposable
    {
        #region Feilds
        NewsCategoryBL NewsCategoryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return NewsCategoryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return NewsCategoryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public NewsCategoryService()
        {
            NewsCategoryBL = new NewsCategoryBL();
        }
        public NewsCategoryService(string connectionString)
        {
            NewsCategoryBL = new NewsCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.NewsCategoryBL != null)
                this.NewsCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiNewsCategory GetNewsCategoryByItemId(long itemId)
        {
            return NewsCategoryBL.GetNewsCategoryByItemId(itemId);
        }
       
        public List<XsiNewsCategory> GetNewsCategory()
        {
            return NewsCategoryBL.GetNewsCategory();
        }
        public List<XsiNewsCategory> GetNewsCategory(EnumSortlistBy sortListBy)
        {
            return NewsCategoryBL.GetNewsCategory(sortListBy);
        }
        public List<XsiNewsCategory> GetNewsCategory(XsiNewsCategory entity)
        {
            return NewsCategoryBL.GetNewsCategory(entity);
        }
        public List<XsiNewsCategory> GetNewsCategory(XsiNewsCategory entity, EnumSortlistBy sortListBy)
        {
            return NewsCategoryBL.GetNewsCategory(entity, sortListBy);
        }
        public List<XsiNewsCategory> GetNewsCategoryOr(XsiNewsCategory entity, EnumSortlistBy sortListBy)
        {
            return NewsCategoryBL.GetNewsCategoryOr(entity, sortListBy);
        }
        
        public List<XsiNewsCategory> GetOtherLanguageNewsCategory(XsiNewsCategory entity)
        {
            return NewsCategoryBL.GetOtherLanguageNewsCategory(entity);
        }
        public List<XsiNewsCategory> GetCurrentLanguageNewsCategory(XsiNewsCategory entity)
        {
            return NewsCategoryBL.GetCurrentLanguageNewsCategory(entity);
        }

        public EnumResultType InsertNewsCategory(XsiNewsCategory entity)
        {
            return NewsCategoryBL.InsertNewsCategory(entity);
        }
        public EnumResultType UpdateNewsCategory(XsiNewsCategory entity)
        {
            return NewsCategoryBL.UpdateNewsCategory(entity);
        }
        public EnumResultType UpdateNewsCategoryStatus(long itemId)
        {
            return NewsCategoryBL.UpdateNewsCategoryStatus(itemId);
        }
        public EnumResultType DeleteNewsCategory(XsiNewsCategory entity)
        {
            return NewsCategoryBL.DeleteNewsCategory(entity);
        }
        public EnumResultType DeleteNewsCategory(long itemId)
        {
            return NewsCategoryBL.DeleteNewsCategory(itemId);
        }
        #endregion
    }
}
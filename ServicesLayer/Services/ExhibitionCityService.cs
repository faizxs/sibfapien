﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionCityService : IDisposable
    {
        #region Feilds
        ExhibitionCityBL ExhibitionCityBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return ExhibitionCityBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public ExhibitionCityService()
        {
            ExhibitionCityBL = new ExhibitionCityBL();
        }
        public ExhibitionCityService(string connectionString)
        {
            ExhibitionCityBL = new ExhibitionCityBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionCityBL != null)
                this.ExhibitionCityBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionCity GetExhibitionCityByItemId(long itemId)
        {
            return ExhibitionCityBL.GetExhibitionCityByItemId(itemId);
        }

        public List<XsiExhibitionCity> GetExhibitionCity()
        {
            return ExhibitionCityBL.GetExhibitionCity();
        }
        public List<XsiExhibitionCity> GetExhibitionCity(EnumSortlistBy sortListBy)
        {
            return ExhibitionCityBL.GetExhibitionCity(sortListBy);
        }
        public List<XsiExhibitionCity> GetExhibitionCity(XsiExhibitionCity entity)
        {
            return ExhibitionCityBL.GetExhibitionCity(entity);
        }
        public List<XsiExhibitionCity> GetExhibitionCity(XsiExhibitionCity entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionCityBL.GetExhibitionCity(entity, sortListBy);
        }
        public List<XsiExhibitionCity> GetCompleteExhibitionCity(XsiExhibitionCity entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionCityBL.GetCompleteExhibitionCity(entity, sortListBy);
        }

        public EnumResultType InsertExhibitionCity(XsiExhibitionCity entity)
        {
            return ExhibitionCityBL.InsertExhibitionCity(entity);
        }
        public EnumResultType UpdateExhibitionCity(XsiExhibitionCity entity)
        {
            return ExhibitionCityBL.UpdateExhibitionCity(entity);
        }
        public EnumResultType UpdateExhibitionCityStatus(long itemId)
        {
            return ExhibitionCityBL.UpdateExhibitionCityStatus(itemId);
        }
        public EnumResultType DeleteExhibitionCity(XsiExhibitionCity entity)
        {
            return ExhibitionCityBL.DeleteExhibitionCity(entity);
        }
        public EnumResultType DeleteExhibitionCity(long itemId)
        {
            return ExhibitionCityBL.DeleteExhibitionCity(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionOtherEventsStaffGuestParticipatingStatusLogService : IDisposable
    {
        #region Fields
        ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionOtherEventsStaffGuestParticipatingStatusLogService()
        {
            ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL = new ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL();
        }
        public ExhibitionOtherEventsStaffGuestParticipatingStatusLogService(string connectionString)
        {
            ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL = new ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL != null)
                this.ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.GetNextGroupId();
        }
        public XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs GetExhibitionOtherEventsStaffGuestParticipatingStatusLogByItemId(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.GetExhibitionOtherEventsStaffGuestParticipatingStatusLogByItemId(itemId);
        }
         
        public List<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs> GetExhibitionOtherEventsStaffGuestParticipatingStatusLog()
        {
            return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.GetExhibitionOtherEventsStaffGuestParticipatingStatusLog();
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs> GetExhibitionOtherEventsStaffGuestParticipatingStatusLog(EnumSortlistBy sortListBy)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.GetExhibitionOtherEventsStaffGuestParticipatingStatusLog(sortListBy);
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs> GetExhibitionOtherEventsStaffGuestParticipatingStatusLog(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.GetExhibitionOtherEventsStaffGuestParticipatingStatusLog(entity);
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs> GetExhibitionOtherEventsStaffGuestParticipatingStatusLog(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.GetExhibitionOtherEventsStaffGuestParticipatingStatusLog(entity, sortListBy);
        }
        public XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs GetExhibitionOtherEventsStaffGuestParticipatingStatusLog(long groupId, long languageId)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.GetExhibitionOtherEventsStaffGuestParticipatingStatusLog(groupId, languageId);
        }

        public EnumResultType InsertExhibitionOtherEventsStaffGuestParticipatingStatusLog(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.InsertExhibitionOtherEventsStaffGuestParticipatingStatusLog(entity);
        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuestParticipatingStatusLog(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.UpdateExhibitionOtherEventsStaffGuestParticipatingStatusLog(entity);
        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuestParticipatingStatusLogStatus(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.UpdateExhibitionOtherEventsStaffGuestParticipatingStatusLogStatus(itemId);
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuestParticipatingStatusLog(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.DeleteExhibitionOtherEventsStaffGuestParticipatingStatusLog(entity);
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuestParticipatingStatusLog(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL.DeleteExhibitionOtherEventsStaffGuestParticipatingStatusLog(itemId);
        }
        #endregion
    }
}
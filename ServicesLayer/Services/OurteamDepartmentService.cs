﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class OurteamDepartmentService : IDisposable
    {
        #region Feilds
        OurteamDepartmentBL OurteamDepartmentBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return OurteamDepartmentBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public OurteamDepartmentService()
        {
            OurteamDepartmentBL = new OurteamDepartmentBL();
        }
        public OurteamDepartmentService(string connectionString)
        {
            OurteamDepartmentBL = new OurteamDepartmentBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.OurteamDepartmentBL != null)
                this.OurteamDepartmentBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiOurteamDepartment GetOurteamDepartmentByItemId(long itemId)
        {
            return OurteamDepartmentBL.GetOurteamDepartmentByItemId(itemId);
        }

        public List<XsiOurteamDepartment> GetOurteamDepartment()
        {
            return OurteamDepartmentBL.GetOurteamDepartment();
        }
        public List<XsiOurteamDepartment> GetOurteamDepartment(EnumSortlistBy sortListBy)
        {
            return OurteamDepartmentBL.GetOurteamDepartment(sortListBy);
        }
        public List<XsiOurteamDepartment> GetOurteamDepartment(XsiOurteamDepartment entity)
        {
            return OurteamDepartmentBL.GetOurteamDepartment(entity);
        }
        public List<XsiOurteamDepartment> GetOurteamDepartment(XsiOurteamDepartment entity, EnumSortlistBy sortListBy)
        {
            return OurteamDepartmentBL.GetOurteamDepartment(entity, sortListBy);
        }
        public List<XsiOurteamDepartment> GetOurteamDepartmentOr(XsiOurteamDepartment entity, EnumSortlistBy sortListBy)
        {
            return OurteamDepartmentBL.GetOurteamDepartmentOr(entity, sortListBy);
        }
        public List<XsiOurteamDepartment> GetOtherLanguageOurteamDepartment(XsiOurteamDepartment entity)
        {
            return OurteamDepartmentBL.GetOtherLanguageOurteamDepartment(entity);
        }
        public List<XsiOurteamDepartment> GetCurrentLanguageOurteamDepartment(XsiOurteamDepartment entity)
        {
            return OurteamDepartmentBL.GetCurrentLanguageOurteamDepartment(entity);
        }

        public EnumResultType InsertOurteamDepartment(XsiOurteamDepartment entity)
        {
            return OurteamDepartmentBL.InsertOurteamDepartment(entity);
        }
        public EnumResultType UpdateOurteamDepartment(XsiOurteamDepartment entity)
        {
            return OurteamDepartmentBL.UpdateOurteamDepartment(entity);
        }
        public EnumResultType UpdateOurteamDepartmentStatus(long itemId, long langId)
        {
            return OurteamDepartmentBL.UpdateOurteamDepartmentStatus(itemId, langId);
        }
        public EnumResultType DeleteOurteamDepartment(XsiOurteamDepartment entity)
        {
            return OurteamDepartmentBL.DeleteOurteamDepartment(entity);
        }
        public EnumResultType DeleteOurteamDepartment(long itemId)
        {
            return OurteamDepartmentBL.DeleteOurteamDepartment(itemId);
        }
        #endregion
    }
}
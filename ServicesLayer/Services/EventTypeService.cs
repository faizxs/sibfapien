﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class EventTypeService : IDisposable
    {
        #region Feilds
        EventTypeBL EventTypeBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return EventTypeBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return EventTypeBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public EventTypeService()
        {
            EventTypeBL = new EventTypeBL();
        }
        public EventTypeService(string connectionString)
        {
            EventTypeBL = new EventTypeBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventTypeBL != null)
                this.EventTypeBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return EventTypeBL.GetNextGroupId();
        }
        public XsiEventType GetEventTypeByItemId(long itemId)
        {
            return EventTypeBL.GetEventTypeByItemId(itemId);
        }
         
        public List<XsiEventType> GetEventType()
        {
            return EventTypeBL.GetEventType();
        }
        public List<XsiEventType> GetEventType(EnumSortlistBy sortListBy)
        {
            return EventTypeBL.GetEventType(sortListBy);
        }
        public List<XsiEventType> GetEventType(XsiEventType entity)
        {
            return EventTypeBL.GetEventType(entity);
        }
        public List<XsiEventType> GetEventType(XsiEventType entity, EnumSortlistBy sortListBy)
        {
            return EventTypeBL.GetEventType(entity, sortListBy);
        }
        public XsiEventType GetEventType(long groupId, long languageId)
        {
            return EventTypeBL.GetEventType(groupId, languageId);
        }

        public EnumResultType InsertEventType(XsiEventType entity)
        {
            return EventTypeBL.InsertEventType(entity);
        }
        public EnumResultType UpdateEventType(XsiEventType entity)
        {
            return EventTypeBL.UpdateEventType(entity);
        }
        public EnumResultType UpdateEventTypeStatus(long itemId)
        {
            return EventTypeBL.UpdateEventTypeStatus(itemId);
        }
        public EnumResultType DeleteEventType(XsiEventType entity)
        {
            return EventTypeBL.DeleteEventType(entity);
        }
        public EnumResultType DeleteEventType(long itemId)
        {
            return EventTypeBL.DeleteEventType(itemId);
        }
        #endregion
    }
}
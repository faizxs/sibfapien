﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class AnnouncementService : IDisposable
    {
        #region Feilds
        AnnouncementBL AnnouncementBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return AnnouncementBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return AnnouncementBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public AnnouncementService()
        {
            AnnouncementBL = new AnnouncementBL();
        }
        public AnnouncementService(string connectionString)
        {
            AnnouncementBL = new AnnouncementBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AnnouncementBL != null)
                this.AnnouncementBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiAnnouncement GetAnnouncementByItemId(long itemId)
        {
            return AnnouncementBL.GetAnnouncementByItemId(itemId);
        }

        public List<XsiAnnouncement> GetAnnouncement()
        {
            return AnnouncementBL.GetAnnouncement();
        }
        public List<XsiAnnouncement> GetAnnouncement(EnumSortlistBy sortListBy)
        {
            return AnnouncementBL.GetAnnouncement(sortListBy);
        }
        public List<XsiAnnouncement> GetAnnouncement(XsiAnnouncement entity)
        {
            return AnnouncementBL.GetAnnouncement(entity);
        }
        public List<XsiAnnouncement> GetAnnouncement(XsiAnnouncement entity, EnumSortlistBy sortListBy)
        {
            return AnnouncementBL.GetAnnouncement(entity, sortListBy);
        }
        public List<XsiAnnouncement> GetAnnouncementOr(XsiAnnouncement entity, EnumSortlistBy sortListBy)
        {
            return AnnouncementBL.GetAnnouncementOr(entity, sortListBy);
        }

        public EnumResultType InsertAnnouncement(XsiAnnouncement entity)
        {
            return AnnouncementBL.InsertAnnouncement(entity);
        }
        public EnumResultType UpdateAnnouncement(XsiAnnouncement entity)
        {
            return AnnouncementBL.UpdateAnnouncement(entity);
        }
       
        public EnumResultType UpdateAnnouncementStatus(long itemId)
        {
            return AnnouncementBL.UpdateAnnouncementStatus(itemId);
        }
        public EnumResultType DeleteAnnouncement(XsiAnnouncement entity)
        {
            return AnnouncementBL.DeleteAnnouncement(entity);
        }
        public EnumResultType DeleteAnnouncement(long itemId)
        {
            return AnnouncementBL.DeleteAnnouncement(itemId);
        }
        #endregion
    }
}
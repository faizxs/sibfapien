﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PageContentService : IDisposable
    {
        #region Feilds
        PageContentBL XsiPageContentBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return XsiPageContentBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return XsiPageContentBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PageContentService()
        {
            XsiPageContentBL = new PageContentBL();
        }
        public PageContentService(string connectionString)
        {
            XsiPageContentBL = new PageContentBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiPageContentBL != null)
                this.XsiPageContentBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiPagesContentNew GetPageContentByItemId(long itemId)
        {
            return XsiPageContentBL.GetPageContentByItemId(itemId);
        }
        
        public List<XsiPagesContentNew> GetPageContent()
        {
            return XsiPageContentBL.GetPageContent();
        }
        public List<XsiPagesContentNew> GetPageContent(EnumSortlistBy sortListBy)
        {
            return XsiPageContentBL.GetPageContent(sortListBy);
        }
        public List<XsiPagesContentNew> GetPageContent(XsiPagesContentNew entity)
        {
            return XsiPageContentBL.GetPageContent(entity);
        }

        public List<XsiPagesContentNew> GetPageContentOr(XsiPagesContentNew entity, EnumSortlistBy sortListBy)
        {
            return XsiPageContentBL.GetPageContentOr(entity, sortListBy);
        }
        public List<XsiPagesContentNew> GetPageContent(XsiPagesContentNew entity, EnumSortlistBy sortListBy)
        {
            return XsiPageContentBL.GetPageContent(entity, sortListBy);
        }
        //public XsiPagesContentNew GetPageContent(long groupId, long languageId)
        //{
        //    return XsiPageContentBL.GetPageContent(groupId, languageId);
        //}
        //public List<sp_FetchSIBFSearchResults_Result> GetSearchContent(string searchString, long languageId)
        //{
        //    return XsiPageContentBL.GetSearchContent(searchString, languageId);
        //}
        
        public EnumResultType InsertPageContent(XsiPagesContentNew entity)
        {
            return XsiPageContentBL.InsertPageContent(entity);
        }
        public EnumResultType UpdatePageContent(XsiPagesContentNew entity)
        {
            return XsiPageContentBL.UpdatePageContent(entity);
        }
        public EnumResultType RollbackPageContent(long itemId)
        {
            return XsiPageContentBL.RollbackPageContent(itemId);
        }
        public EnumResultType UpdatePageContentStatus(long itemId)
        {
            return XsiPageContentBL.UpdatePageContentStatus(itemId);
        }
        public EnumResultType DeletePageContent(XsiPagesContentNew entity)
        {
            return XsiPageContentBL.DeletePageContent(entity);
        }
        public EnumResultType DeletePageContent(long itemId)
        {
            return XsiPageContentBL.DeletePageContent(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionFurnitureItemInvoiceStatusLogService : IDisposable
    {
        #region Fields
        ExhibitionFurnitureItemInvoiceStatusLogBL ExhibitionFurnitureItemInvoiceStatusLogBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionFurnitureItemInvoiceStatusLogBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionFurnitureItemInvoiceStatusLogBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionFurnitureItemInvoiceStatusLogService()
        {
            ExhibitionFurnitureItemInvoiceStatusLogBL = new ExhibitionFurnitureItemInvoiceStatusLogBL();
        }
        public ExhibitionFurnitureItemInvoiceStatusLogService(string connectionString)
        {
            ExhibitionFurnitureItemInvoiceStatusLogBL = new ExhibitionFurnitureItemInvoiceStatusLogBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionFurnitureItemInvoiceStatusLogBL != null)
                this.ExhibitionFurnitureItemInvoiceStatusLogBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ExhibitionFurnitureItemInvoiceStatusLogBL.GetNextGroupId();
        }
        public XsiExhibitionFurnitureItemStatusLogs GetExhibitionFurnitureItemInvoiceStatusLogByItemId(long itemId)
        {
            return ExhibitionFurnitureItemInvoiceStatusLogBL.GetExhibitionFurnitureItemInvoiceStatusLogByItemId(itemId);
        }

        public List<XsiExhibitionFurnitureItemStatusLogs> GetExhibitionFurnitureItemInvoiceStatusLog()
        {
            return ExhibitionFurnitureItemInvoiceStatusLogBL.GetExhibitionFurnitureItemInvoiceStatusLog();
        }
        public List<XsiExhibitionFurnitureItemStatusLogs> GetExhibitionFurnitureItemInvoiceStatusLog(EnumSortlistBy sortListBy)
        {
            return ExhibitionFurnitureItemInvoiceStatusLogBL.GetExhibitionFurnitureItemInvoiceStatusLog(sortListBy);
        }
        public List<XsiExhibitionFurnitureItemStatusLogs> GetExhibitionFurnitureItemInvoiceStatusLog(XsiExhibitionFurnitureItemStatusLogs entity)
        {
            return ExhibitionFurnitureItemInvoiceStatusLogBL.GetExhibitionFurnitureItemInvoiceStatusLog(entity);
        }
        public List<XsiExhibitionFurnitureItemStatusLogs> GetExhibitionFurnitureItemInvoiceStatusLog(XsiExhibitionFurnitureItemStatusLogs entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionFurnitureItemInvoiceStatusLogBL.GetExhibitionFurnitureItemInvoiceStatusLog(entity, sortListBy);
        }
        public XsiExhibitionFurnitureItemStatusLogs GetExhibitionFurnitureItemInvoiceStatusLog(long groupId, long languageId)
        {
            return ExhibitionFurnitureItemInvoiceStatusLogBL.GetExhibitionFurnitureItemInvoiceStatusLog(groupId, languageId);
        }

        public EnumResultType InsertExhibitionFurnitureItemInvoiceStatusLog(XsiExhibitionFurnitureItemStatusLogs entity)
        {
            return ExhibitionFurnitureItemInvoiceStatusLogBL.InsertExhibitionFurnitureItemInvoiceStatusLog(entity);
        }
        public EnumResultType UpdateExhibitionFurnitureItemInvoiceStatusLog(XsiExhibitionFurnitureItemStatusLogs entity)
        {
            return ExhibitionFurnitureItemInvoiceStatusLogBL.UpdateExhibitionFurnitureItemInvoiceStatusLog(entity);
        }
        public EnumResultType UpdateExhibitionFurnitureItemInvoiceStatusLogStatus(long itemId)
        {
            return ExhibitionFurnitureItemInvoiceStatusLogBL.UpdateExhibitionFurnitureItemInvoiceStatusLogStatus(itemId);
        }
        public EnumResultType DeleteExhibitionFurnitureItemInvoiceStatusLog(XsiExhibitionFurnitureItemStatusLogs entity)
        {
            return ExhibitionFurnitureItemInvoiceStatusLogBL.DeleteExhibitionFurnitureItemInvoiceStatusLog(entity);
        }
        public EnumResultType DeleteExhibitionFurnitureItemInvoiceStatusLog(long itemId)
        {
            return ExhibitionFurnitureItemInvoiceStatusLogBL.DeleteExhibitionFurnitureItemInvoiceStatusLog(itemId);
        }
        #endregion
    }
}
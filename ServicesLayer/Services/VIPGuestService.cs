﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class VIPGuestService : IDisposable
    {
        #region Feilds
        VIPGuestBL VIPGuestBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return VIPGuestBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return VIPGuestBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public VIPGuestService()
        {
            VIPGuestBL = new VIPGuestBL();
        }
        public VIPGuestService(string connectionString)
        {
            VIPGuestBL = new VIPGuestBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.VIPGuestBL != null)
                this.VIPGuestBL.Dispose();
        }
        #endregion
        #region Methods

        public XsiVipguest GetVIPGuestByItemId(long itemId)
        {
            return VIPGuestBL.GetVIPGuestByItemId(itemId);
        }

        public List<XsiVipguest> GetVIPGuest()
        {
            return VIPGuestBL.GetVIPGuest();
        }
        public List<XsiVipguest> GetVIPGuest(EnumSortlistBy sortListBy)
        {
            return VIPGuestBL.GetVIPGuest(sortListBy);
        }
        public List<XsiVipguest> GetVIPGuest(XsiVipguest entity)
        {
            return VIPGuestBL.GetVIPGuest(entity);
        }
        public List<XsiVipguest> GetVIPGuest(XsiVipguest entity, EnumSortlistBy sortListBy)
        {
            return VIPGuestBL.GetVIPGuest(entity, sortListBy);
        }
        public List<XsiVipguest> GetVIPGuestOr(XsiVipguest entity, EnumSortlistBy sortListBy)
        {
            return VIPGuestBL.GetVIPGuestOr(entity, sortListBy);
        }

        public EnumResultType InsertVIPGuest(XsiVipguest entity)
        {
            return VIPGuestBL.InsertVIPGuest(entity);
        }
        public EnumResultType UpdateVIPGuest(XsiVipguest entity)
        {
            return VIPGuestBL.UpdateVIPGuest(entity);
        }
        public EnumResultType UpdateVIPGuestStatus(long itemId, long langId)
        {
            return VIPGuestBL.UpdateVIPGuestStatus(itemId, langId);
        }
        public EnumResultType DeleteVIPGuest(XsiVipguest entity)
        {
            return VIPGuestBL.DeleteVIPGuest(entity);
        }
        public EnumResultType DeleteVIPGuest(long itemId)
        {
            return VIPGuestBL.DeleteVIPGuest(itemId);
        }
        #endregion
    }
}
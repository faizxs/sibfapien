﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFActivitiesBannerService : IDisposable
    {
        #region Feilds
        SCRFActivitiesBannerBL SCRFActivitiesBannerBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFActivitiesBannerBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFActivitiesBannerBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFActivitiesBannerService()
        {
            SCRFActivitiesBannerBL = new SCRFActivitiesBannerBL();
        }
        public SCRFActivitiesBannerService(string connectionString)
        {
            SCRFActivitiesBannerBL = new SCRFActivitiesBannerBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFActivitiesBannerBL != null)
                this.SCRFActivitiesBannerBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfactivitiesBanner GetSCRFActivitiesBannerByItemId(long itemId)
        {
            return SCRFActivitiesBannerBL.GetSCRFActivitiesBannerByItemId(itemId);
        }

        public List<XsiScrfactivitiesBanner> GetSCRFActivitiesBanner()
        {
            return SCRFActivitiesBannerBL.GetSCRFActivitiesBanner();
        }
        public List<XsiScrfactivitiesBanner> GetSCRFActivitiesBanner(EnumSortlistBy sortListBy)
        {
            return SCRFActivitiesBannerBL.GetSCRFActivitiesBanner(sortListBy);
        }
        public List<XsiScrfactivitiesBanner> GetSCRFActivitiesBanner(XsiScrfactivitiesBanner entity)
        {
            return SCRFActivitiesBannerBL.GetSCRFActivitiesBanner(entity);
        }
        public List<XsiScrfactivitiesBanner> GetSCRFActivitiesBanner(XsiScrfactivitiesBanner entity, EnumSortlistBy sortListBy)
        {
            return SCRFActivitiesBannerBL.GetSCRFActivitiesBanner(entity, sortListBy);
        }
        public List<XsiScrfactivitiesBanner> GetSCRFActivitiesBannerOr(XsiScrfactivitiesBanner entity, EnumSortlistBy sortListBy)
        {
            return SCRFActivitiesBannerBL.GetSCRFActivitiesBannerOr(entity, sortListBy);
        }

        public EnumResultType InsertSCRFActivitiesBanner(XsiScrfactivitiesBanner entity)
        {
            return SCRFActivitiesBannerBL.InsertSCRFActivitiesBanner(entity);
        }
        public EnumResultType UpdateSCRFActivitiesBanner(XsiScrfactivitiesBanner entity)
        {
            return SCRFActivitiesBannerBL.UpdateSCRFActivitiesBanner(entity);
        }
        public EnumResultType UpdateSCRFActivitiesBannerStatus(long itemId)
        {
            return SCRFActivitiesBannerBL.UpdateSCRFActivitiesBannerStatus(itemId);
        }
        public EnumResultType DeleteSCRFActivitiesBanner(XsiScrfactivitiesBanner entity)
        {
            return SCRFActivitiesBannerBL.DeleteSCRFActivitiesBanner(entity);
        }
        public EnumResultType DeleteSCRFActivitiesBanner(long itemId)
        {
            return SCRFActivitiesBannerBL.DeleteSCRFActivitiesBanner(itemId);
        }

        //Custom 
        public long GetHomeBannerInfo(long languageid)
        {
            return SCRFActivitiesBannerBL.GetHomeBannerInfo(languageid);
        }
        #endregion
    }
}
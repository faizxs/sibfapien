﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class DownloadsService : IDisposable
    {
        #region Feilds
        DownloadsBL DownloadsBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return DownloadsBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return DownloadsBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public DownloadsService()
        {
            DownloadsBL = new DownloadsBL();
        }
        public DownloadsService(string connectionString)
        {
            DownloadsBL = new DownloadsBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.DownloadsBL != null)
                this.DownloadsBL.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiDownloads GetDownloadsByItemId(long itemId)
        {
            return DownloadsBL.GetDownloadsByItemId(itemId);
        }
       
        public List<XsiDownloads> GetDownloads()
        {
            return DownloadsBL.GetDownloads();
        }
        public List<XsiDownloads> GetDownloads(EnumSortlistBy sortListBy)
        {
            return DownloadsBL.GetDownloads(sortListBy);
        }
        public List<XsiDownloads> GetDownloads(XsiDownloads entity)
        {
            return DownloadsBL.GetDownloads(entity);
        }
        public List<XsiDownloads> GetDownloads(XsiDownloads entity, EnumSortlistBy sortListBy)
        {
            return DownloadsBL.GetDownloads(entity, sortListBy);
        }
        public List<XsiDownloads> GetCompleteDownloads(XsiDownloads entity, EnumSortlistBy sortListBy, long langId)
        {
            return DownloadsBL.GetCompleteDownloads(entity, sortListBy, langId);
        }
        public XsiDownloads GetDownloads(long groupId, long categoryId)
        {
            return DownloadsBL.GetDownloads(groupId, categoryId);
        }

        public EnumResultType InsertDownloads(XsiDownloads entity)
        {
            return DownloadsBL.InsertDownloads(entity);
        }
        public EnumResultType UpdateDownloads(XsiDownloads entity)
        {
            return DownloadsBL.UpdateDownloads(entity);
        }
        public EnumResultType UpdateDownloadsStatus(long itemId)
        {
            return DownloadsBL.UpdateDownloadsStatus(itemId);
        }
        public EnumResultType DeleteDownloads(XsiDownloads entity)
        {
            return DownloadsBL.DeleteDownloads(entity);
        }
        public EnumResultType DeleteDownloads(long itemId)
        {
            return DownloadsBL.DeleteDownloads(itemId);
        }
        #endregion
    }
}
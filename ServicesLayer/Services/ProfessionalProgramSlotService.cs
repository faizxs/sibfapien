﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ProfessionalProgramSlotService : IDisposable
    {
        #region Feilds
        ProfessionalProgramSlotsBL ProfessionalProgramSlotBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ProfessionalProgramSlotBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ProfessionalProgramSlotBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ProfessionalProgramSlotService()
        {
            ProfessionalProgramSlotBL = new ProfessionalProgramSlotsBL();
        }
        public ProfessionalProgramSlotService(string connectionString)
        {
            ProfessionalProgramSlotBL = new ProfessionalProgramSlotsBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ProfessionalProgramSlotBL != null)
                this.ProfessionalProgramSlotBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionProfessionalProgramSlots GetProfessionalProgramSlotByItemId(long itemId)
        {
            return ProfessionalProgramSlotBL.GetProfessionalProgramSlotsByItemId(itemId);
        }

        public List<XsiExhibitionProfessionalProgramSlots> GetProfessionalProgramSlot()
        {
            return ProfessionalProgramSlotBL.GetProfessionalProgramSlots();
        }
        public List<XsiExhibitionProfessionalProgramSlots> GetProfessionalProgramSlot(EnumSortlistBy sortListBy)
        {
            return ProfessionalProgramSlotBL.GetProfessionalProgramSlots(sortListBy);
        }
        public List<XsiExhibitionProfessionalProgramSlots> GetProfessionalProgramSlot(XsiExhibitionProfessionalProgramSlots entity)
        {
            return ProfessionalProgramSlotBL.GetProfessionalProgramSlots(entity);
        }
        public List<XsiExhibitionProfessionalProgramSlots> GetProfessionalProgramSlot(XsiExhibitionProfessionalProgramSlots entity, EnumSortlistBy sortListBy)
        {
            return ProfessionalProgramSlotBL.GetProfessionalProgramSlots(entity, sortListBy);
        }

        public EnumResultType InsertProfessionalProgramSlot(XsiExhibitionProfessionalProgramSlots entity)
        {
            return ProfessionalProgramSlotBL.InsertProfessionalProgramSlots(entity);
        }
        public EnumResultType UpdateProfessionalProgramSlot(XsiExhibitionProfessionalProgramSlots entity)
        {
            return ProfessionalProgramSlotBL.UpdateProfessionalProgramSlots(entity);
        }
        public EnumResultType DeleteProfessionalProgramSlot(XsiExhibitionProfessionalProgramSlots entity)
        {
            return ProfessionalProgramSlotBL.DeleteProfessionalProgramSlots(entity);
        }
        public EnumResultType DeleteProfessionalProgramSlot(long itemId)
        {
            return ProfessionalProgramSlotBL.DeleteProfessionalProgramSlots(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionStaffGuestParticipatingStatusLogService : IDisposable
    {
        #region Fields
        ExhibitionStaffGuestParticipatingStatusLogBL ExhibitionStaffGuestParticipatingStatusLogBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionStaffGuestParticipatingStatusLogBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionStaffGuestParticipatingStatusLogBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestParticipatingStatusLogService()
        {
            ExhibitionStaffGuestParticipatingStatusLogBL = new ExhibitionStaffGuestParticipatingStatusLogBL();
        }
        public ExhibitionStaffGuestParticipatingStatusLogService(string connectionString)
        {
            ExhibitionStaffGuestParticipatingStatusLogBL = new ExhibitionStaffGuestParticipatingStatusLogBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionStaffGuestParticipatingStatusLogBL != null)
                this.ExhibitionStaffGuestParticipatingStatusLogBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ExhibitionStaffGuestParticipatingStatusLogBL.GetNextGroupId();
        }
        public XsiExhibitionStaffGuestParticipatingStatusLogs GetExhibitionStaffGuestParticipatingStatusLogByItemId(long itemId)
        {
            return ExhibitionStaffGuestParticipatingStatusLogBL.GetExhibitionStaffGuestParticipatingStatusLogByItemId(itemId);
        }
         
        public List<XsiExhibitionStaffGuestParticipatingStatusLogs> GetExhibitionStaffGuestParticipatingStatusLog()
        {
            return ExhibitionStaffGuestParticipatingStatusLogBL.GetExhibitionStaffGuestParticipatingStatusLog();
        }
        public List<XsiExhibitionStaffGuestParticipatingStatusLogs> GetExhibitionStaffGuestParticipatingStatusLog(EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestParticipatingStatusLogBL.GetExhibitionStaffGuestParticipatingStatusLog(sortListBy);
        }
        public List<XsiExhibitionStaffGuestParticipatingStatusLogs> GetExhibitionStaffGuestParticipatingStatusLog(XsiExhibitionStaffGuestParticipatingStatusLogs entity)
        {
            return ExhibitionStaffGuestParticipatingStatusLogBL.GetExhibitionStaffGuestParticipatingStatusLog(entity);
        }
        public List<XsiExhibitionStaffGuestParticipatingStatusLogs> GetExhibitionStaffGuestParticipatingStatusLog(XsiExhibitionStaffGuestParticipatingStatusLogs entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestParticipatingStatusLogBL.GetExhibitionStaffGuestParticipatingStatusLog(entity, sortListBy);
        }
        public XsiExhibitionStaffGuestParticipatingStatusLogs GetExhibitionStaffGuestParticipatingStatusLog(long groupId, long languageId)
        {
            return ExhibitionStaffGuestParticipatingStatusLogBL.GetExhibitionStaffGuestParticipatingStatusLog(groupId, languageId);
        }

        public EnumResultType InsertExhibitionStaffGuestParticipatingStatusLog(XsiExhibitionStaffGuestParticipatingStatusLogs entity)
        {
            return ExhibitionStaffGuestParticipatingStatusLogBL.InsertExhibitionStaffGuestParticipatingStatusLog(entity);
        }
        public EnumResultType UpdateExhibitionStaffGuestParticipatingStatusLog(XsiExhibitionStaffGuestParticipatingStatusLogs entity)
        {
            return ExhibitionStaffGuestParticipatingStatusLogBL.UpdateExhibitionStaffGuestParticipatingStatusLog(entity);
        }
        public EnumResultType UpdateExhibitionStaffGuestParticipatingStatusLogStatus(long itemId)
        {
            return ExhibitionStaffGuestParticipatingStatusLogBL.UpdateExhibitionStaffGuestParticipatingStatusLogStatus(itemId);
        }
        public EnumResultType DeleteExhibitionStaffGuestParticipatingStatusLog(XsiExhibitionStaffGuestParticipatingStatusLogs entity)
        {
            return ExhibitionStaffGuestParticipatingStatusLogBL.DeleteExhibitionStaffGuestParticipatingStatusLog(entity);
        }
        public EnumResultType DeleteExhibitionStaffGuestParticipatingStatusLog(long itemId)
        {
            return ExhibitionStaffGuestParticipatingStatusLogBL.DeleteExhibitionStaffGuestParticipatingStatusLog(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class NewsletterSubscriberService : IDisposable
    {
        #region Feilds
        NewsletterSubscriberBL NewsletterSubscriberBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return NewsletterSubscriberBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return NewsletterSubscriberBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public NewsletterSubscriberService()
        {
            NewsletterSubscriberBL = new NewsletterSubscriberBL();
        }
        public NewsletterSubscriberService(string connectionString)
        {
            NewsletterSubscriberBL = new NewsletterSubscriberBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.NewsletterSubscriberBL != null)
                this.NewsletterSubscriberBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiNewsletterSubscribers GetNewsletterSubscriberByItemId(long itemId)
        {
            return NewsletterSubscriberBL.GetNewsletterSubscriberByItemId(itemId);
        }
         
        public List<XsiNewsletterSubscribers> GetNewsletterSubscriber()
        {
            return NewsletterSubscriberBL.GetNewsletterSubscriber();
        }
        public List<XsiNewsletterSubscribers> GetNewsletterSubscriber(EnumSortlistBy sortListBy)
        {
            return NewsletterSubscriberBL.GetNewsletterSubscriber(sortListBy);
        }
        public List<XsiNewsletterSubscribers> GetNewsletterSubscriber(XsiNewsletterSubscribers entity)
        {
            return NewsletterSubscriberBL.GetNewsletterSubscriber(entity);
        }
        public List<XsiNewsletterSubscribers> GetNewsletterSubscriber(XsiNewsletterSubscribers entity, EnumSortlistBy sortListBy)
        {
            return NewsletterSubscriberBL.GetNewsletterSubscriber(entity, sortListBy);
        }
        public List<XsiNewsletterSubscribers> GetNewsletterSubscriberOr(XsiNewsletterSubscribers entity, EnumSortlistBy sortListBy)
        {
            return NewsletterSubscriberBL.GetNewsletterSubscriberOr(entity, sortListBy);
        }

        public EnumResultType InsertNewsletterSubscriber(XsiNewsletterSubscribers entity)
        {
            return NewsletterSubscriberBL.InsertNewsletterSubscriber(entity);
        }
        public EnumResultType UpdateNewsletterSubscriber(XsiNewsletterSubscribers entity)
        {
            return NewsletterSubscriberBL.UpdateNewsletterSubscriber(entity);
        }
        public EnumResultType UpdateNewsletterSubscriberStatus(long itemId)
        {
            return NewsletterSubscriberBL.UpdateNewsletterSubscriberStatus(itemId);
        }
        public EnumResultType DeleteNewsletterSubscriber(XsiNewsletterSubscribers entity)
        {
            return NewsletterSubscriberBL.DeleteNewsletterSubscriber(entity);
        }
        public EnumResultType DeleteNewsletterSubscriber(long itemId)
        {
            return NewsletterSubscriberBL.DeleteNewsletterSubscriber(itemId);
        }
        #endregion
    }
}
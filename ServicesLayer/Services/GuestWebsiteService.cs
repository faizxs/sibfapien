﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class GuestWebsiteService : IDisposable
    {
        #region Feilds
        GuestWebsiteBL GuestWebsiteBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return GuestWebsiteBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return GuestWebsiteBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public GuestWebsiteService()
        {
            GuestWebsiteBL = new GuestWebsiteBL();
        }
        public GuestWebsiteService(string connectionString)
        {
            GuestWebsiteBL = new GuestWebsiteBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.GuestWebsiteBL != null)
                this.GuestWebsiteBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return GuestWebsiteBL.GetNextGroupId();
        }
        public XsiGuestWebsite GetGuestWebsiteByItemId(long itemId)
        {
            return GuestWebsiteBL.GetGuestWebsiteByItemId(itemId);
        }
         
        public List<XsiGuestWebsite> GetGuestWebsite()
        {
            return GuestWebsiteBL.GetGuestWebsite();
        }
        public List<XsiGuestWebsite> GetGuestWebsite(EnumSortlistBy sortListBy)
        {
            return GuestWebsiteBL.GetGuestWebsite(sortListBy);
        }
        public List<XsiGuestWebsite> GetGuestWebsite(XsiGuestWebsite entity)
        {
            return GuestWebsiteBL.GetGuestWebsite(entity);
        }
        public List<XsiGuestWebsite> GetGuestWebsite(XsiGuestWebsite entity, EnumSortlistBy sortListBy)
        {
            return GuestWebsiteBL.GetGuestWebsite(entity, sortListBy);
        }
        public XsiGuestWebsite GetGuestWebsite(long groupId, long languageId)
        {
            return GuestWebsiteBL.GetGuestWebsite(groupId, languageId);
        }

        public List<long?> GetGuestId(long ID1, long ID2, long ID3)
        {
            return GuestWebsiteBL.GetGuestId(ID1, ID2, ID3);
        }

        public EnumResultType InsertGuestWebsite(XsiGuestWebsite entity)
        {
            return GuestWebsiteBL.InsertGuestWebsite(entity);
        }
        public EnumResultType UpdateGuestWebsite(XsiGuestWebsite entity)
        {
            return GuestWebsiteBL.UpdateGuestWebsite(entity);
        }
        public EnumResultType UpdateGuestWebsiteStatus(long itemId)
        {
            return GuestWebsiteBL.UpdateGuestWebsiteStatus(itemId);
        }
        public EnumResultType DeleteGuestWebsite(XsiGuestWebsite entity)
        {
            return GuestWebsiteBL.DeleteGuestWebsite(entity);
        }
        public EnumResultType DeleteGuestWebsite(long itemId)
        {
            return GuestWebsiteBL.DeleteGuestWebsite(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFPhotoSubCategoryService : IDisposable
    {
        #region Feilds
        SCRFPhotoSubCategoryBL SCRFPhotoSubCategoryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFPhotoSubCategoryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFPhotoSubCategoryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFPhotoSubCategoryService()
        {
            SCRFPhotoSubCategoryBL = new SCRFPhotoSubCategoryBL();
        }
        public SCRFPhotoSubCategoryService(string connectionString)
        {
            SCRFPhotoSubCategoryBL = new SCRFPhotoSubCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFPhotoSubCategoryBL != null)
                this.SCRFPhotoSubCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiScrfphotoSubCategory GetSCRFPhotoSubCategoryByItemId(long itemId)
        {
            return SCRFPhotoSubCategoryBL.GetSCRFPhotoSubCategoryByItemId(itemId);
        }
        
        public List<XsiScrfphotoSubCategory> GetSCRFPhotoSubCategory()
        {
            return SCRFPhotoSubCategoryBL.GetSCRFPhotoSubCategory();
        }
        public List<XsiScrfphotoSubCategory> GetSCRFPhotoSubCategory(EnumSortlistBy sortListBy)
        {
            return SCRFPhotoSubCategoryBL.GetSCRFPhotoSubCategory(sortListBy);
        }
        public List<XsiScrfphotoSubCategory> GetSCRFPhotoSubCategory(XsiScrfphotoSubCategory entity)
        {
            return SCRFPhotoSubCategoryBL.GetSCRFPhotoSubCategory(entity);
        }
        public List<XsiScrfphotoSubCategory> GetSCRFPhotoSubCategory(XsiScrfphotoSubCategory entity, EnumSortlistBy sortListBy)
        {
            return SCRFPhotoSubCategoryBL.GetSCRFPhotoSubCategory(entity, sortListBy);
        }
        public List<XsiScrfphotoSubCategory> GetCompleteSCRFPhotoSubCategory(XsiScrfphotoSubCategory entity, EnumSortlistBy sortListBy)
        {
            return SCRFPhotoSubCategoryBL.GetCompleteSCRFPhotoSubCategory(entity, sortListBy);
        }
         
        public EnumResultType InsertSCRFPhotoSubCategory(XsiScrfphotoSubCategory entity)
        {
            return SCRFPhotoSubCategoryBL.InsertSCRFPhotoSubCategory(entity);
        }
        public EnumResultType UpdateSCRFPhotoSubCategory(XsiScrfphotoSubCategory entity)
        {
            return SCRFPhotoSubCategoryBL.UpdateSCRFPhotoSubCategory(entity);
        }
        public EnumResultType UpdateSCRFPhotoSubCategoryStatus(long itemId)
        {
            return SCRFPhotoSubCategoryBL.UpdateSCRFPhotoSubCategoryStatus(itemId);
        }
        public EnumResultType DeleteSCRFPhotoSubCategory(XsiScrfphotoSubCategory entity)
        {
            return SCRFPhotoSubCategoryBL.DeleteSCRFPhotoSubCategory(entity);
        }
        public EnumResultType DeleteSCRFPhotoSubCategory(long itemId)
        {
            return SCRFPhotoSubCategoryBL.DeleteSCRFPhotoSubCategory(itemId);
        }
        #endregion
    }
}
﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionLanguageService : IDisposable
    {
        #region Feilds
        ExhibitionLanguageBL ExhibitionLanguageBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionLanguageBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionLanguageBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionLanguageService()
        {
            ExhibitionLanguageBL = new ExhibitionLanguageBL();
        }
        public ExhibitionLanguageService(string connectionString)
        {
            ExhibitionLanguageBL = new ExhibitionLanguageBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionLanguageBL != null)
                this.ExhibitionLanguageBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionLanguage GetExhibitionLanguageByItemId(long itemId)
        {
            return ExhibitionLanguageBL.GetExhibitionLanguageByItemId(itemId);
        }
        
        public List<XsiExhibitionLanguage> GetExhibitionLanguage()
        {
            return ExhibitionLanguageBL.GetExhibitionLanguage();
        }
        public List<XsiExhibitionLanguage> GetExhibitionLanguage(EnumSortlistBy sortListBy)
        {
            return ExhibitionLanguageBL.GetExhibitionLanguage(sortListBy);
        }
        public List<XsiExhibitionLanguage> GetExhibitionLanguage(XsiExhibitionLanguage entity)
        {
            return ExhibitionLanguageBL.GetExhibitionLanguage(entity);
        }
        public List<XsiExhibitionLanguage> GetExhibitionLanguage(XsiExhibitionLanguage entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionLanguageBL.GetExhibitionLanguage(entity, sortListBy);
        }

        public List<XsiExhibitionLanguage> GetExhibitionLanguageOr(XsiExhibitionLanguage entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionLanguageBL.GetExhibitionLanguageOr(entity, sortListBy);
        }
     
        public EnumResultType InsertExhibitionLanguage(XsiExhibitionLanguage entity)
        {
            return ExhibitionLanguageBL.InsertExhibitionLanguage(entity);
        }
        public EnumResultType UpdateExhibitionLanguage(XsiExhibitionLanguage entity)
        {
            return ExhibitionLanguageBL.UpdateExhibitionLanguage(entity);
        }
        public EnumResultType UpdateExhibitionLanguageStatus(long itemId)
        {
            return ExhibitionLanguageBL.UpdateExhibitionLanguageStatus(itemId);
        }
        public EnumResultType DeleteExhibitionLanguage(XsiExhibitionLanguage entity)
        {
            return ExhibitionLanguageBL.DeleteExhibitionLanguage(entity);
        }
        public EnumResultType DeleteExhibitionLanguage(long itemId)
        {
            return ExhibitionLanguageBL.DeleteExhibitionLanguage(itemId);
        }
        #endregion
    }
}
﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class CareerService : IDisposable
    {
        #region Feilds
        CareerBL CareerBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return CareerBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return CareerBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public CareerService()
        {
            CareerBL = new CareerBL();
        }
        public CareerService(string connectionString)
        {
            CareerBL = new CareerBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerBL != null)
                this.CareerBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiCareer GetCareerByItemId(long itemId)
        {
            return CareerBL.GetCareerByItemId(itemId);
        }
          
        public List<XsiCareer> GetCareer()
        {
            return CareerBL.GetCareer();
        }
        public List<XsiCareer> GetCareer(EnumSortlistBy sortListBy)
        {
            return CareerBL.GetCareer(sortListBy);
        }
        public List<XsiCareer> GetCareer(XsiCareer entity)
        {
            return CareerBL.GetCareer(entity);
        }
        public List<XsiCareer> GetCareer(XsiCareer entity, EnumSortlistBy sortListBy)
        {
            return CareerBL.GetCareer(entity, sortListBy);
        }
        public List<XsiCareer> GetCareerOr(XsiCareer entity, EnumSortlistBy sortListBy)
        {
            return CareerBL.GetCareerOr(entity, sortListBy);
        }
         
        public EnumResultType InsertCareer(XsiCareer entity)
        {
            return CareerBL.InsertCareer(entity);
        }
        public EnumResultType UpdateCareer(XsiCareer entity)
        {
            return CareerBL.UpdateCareer(entity);
        }
        public EnumResultType UpdateCareerStatus(long itemId)
        {
            return CareerBL.UpdateCareerStatus(itemId);
        }
        public EnumResultType DeleteCareer(XsiCareer entity)
        {
            return CareerBL.DeleteCareer(entity);
        }
        public EnumResultType DeleteCareer(long itemId)
        {
            return CareerBL.DeleteCareer(itemId);
        }
        #endregion
    }
}
﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class NewsPhotoGalleryService : IDisposable
    {
        #region Feilds
        NewsPhotoGalleryBL NewsPhotoGalleryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return NewsPhotoGalleryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return NewsPhotoGalleryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public NewsPhotoGalleryService()
        {
            NewsPhotoGalleryBL = new NewsPhotoGalleryBL();
        }
        public NewsPhotoGalleryService(string connectionString)
        {
            NewsPhotoGalleryBL = new NewsPhotoGalleryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.NewsPhotoGalleryBL != null)
                this.NewsPhotoGalleryBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return NewsPhotoGalleryBL.GetNextGroupId();
        }

        public long GetNextSortIndex()
        {
            return NewsPhotoGalleryBL.GetNextSortIndex();
        }
        public XsiNewsPhotoGallery GetNewsPhotoGalleryByItemId(long itemId)
        {
            return NewsPhotoGalleryBL.GetNewsPhotoGalleryByItemId(itemId);
        }
         
        public List<XsiNewsPhotoGallery> GetNewsPhotoGallery()
        {
            return NewsPhotoGalleryBL.GetNewsPhotoGallery();
        }
        public List<XsiNewsPhotoGallery> GetNewsPhotoGallery(EnumSortlistBy sortListBy)
        {
            return NewsPhotoGalleryBL.GetNewsPhotoGallery(sortListBy);
        }
        public List<XsiNewsPhotoGallery> GetNewsPhotoGallery(XsiNewsPhotoGallery entity)
        {
            return NewsPhotoGalleryBL.GetNewsPhotoGallery(entity);
        }
        public List<XsiNewsPhotoGallery> GetNewsPhotoGallery(XsiNewsPhotoGallery entity, EnumSortlistBy sortListBy)
        {
            return NewsPhotoGalleryBL.GetNewsPhotoGallery(entity, sortListBy);
        }
        public XsiNewsPhotoGallery GetNewsPhotoGallery(long groupId, long languageId)
        {
            return NewsPhotoGalleryBL.GetNewsPhotoGallery(groupId, languageId);
        }

        public EnumResultType InsertNewsPhotoGallery(XsiNewsPhotoGallery entity)
        {
            return NewsPhotoGalleryBL.InsertNewsPhotoGallery(entity);
        }
        public EnumResultType UpdateNewsPhotoGallery(XsiNewsPhotoGallery entity)
        {
            return NewsPhotoGalleryBL.UpdateNewsPhotoGallery(entity);
        }
        public EnumResultType UpdateNewsPhotoGalleryStatus(long itemId)
        {
            return NewsPhotoGalleryBL.UpdateNewsPhotoGalleryStatus(itemId);
        }
        public EnumResultType DeleteNewsPhotoGallery(XsiNewsPhotoGallery entity)
        {
            return NewsPhotoGalleryBL.DeleteNewsPhotoGallery(entity);
        }
        public EnumResultType DeleteNewsPhotoGallery(long itemId)
        {
            return NewsPhotoGalleryBL.DeleteNewsPhotoGallery(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFCityService : IDisposable
    {
        #region Feilds
        SCRFCityBL SCRFCityBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFCityBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFCityBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFCityService()
        {
            SCRFCityBL = new SCRFCityBL();
        }
        public SCRFCityService(string connectionString)
        {
            SCRFCityBL = new SCRFCityBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFCityBL != null)
                this.SCRFCityBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfcity GetSCRFCityByItemId(long itemId)
        {
            return SCRFCityBL.GetSCRFCityByItemId(itemId);
        }

        public List<XsiScrfcity> GetSCRFCity()
        {
            return SCRFCityBL.GetSCRFCity();
        }
        public List<XsiScrfcity> GetSCRFCity(EnumSortlistBy sortListBy)
        {
            return SCRFCityBL.GetSCRFCity(sortListBy);
        }
        public List<XsiScrfcity> GetSCRFCity(XsiScrfcity entity)
        {
            return SCRFCityBL.GetSCRFCity(entity);
        }
        public List<XsiScrfcity> GetSCRFCity(XsiScrfcity entity, EnumSortlistBy sortListBy)
        {
            return SCRFCityBL.GetSCRFCity(entity, sortListBy);
        }
        public List<XsiScrfcity> GetCompleteSCRFCity(XsiScrfcity entity, EnumSortlistBy sortListBy)
        {
            return SCRFCityBL.GetCompleteSCRFCity(entity, sortListBy);
        }

        public EnumResultType InsertSCRFCity(XsiScrfcity entity)
        {
            return SCRFCityBL.InsertSCRFCity(entity);
        }
        public EnumResultType UpdateSCRFCity(XsiScrfcity entity)
        {
            return SCRFCityBL.UpdateSCRFCity(entity);
        }
        public EnumResultType UpdateSCRFCityStatus(long itemId)
        {
            return SCRFCityBL.UpdateSCRFCityStatus(itemId);
        }
        public EnumResultType DeleteSCRFCity(XsiScrfcity entity)
        {
            return SCRFCityBL.DeleteSCRFCity(entity);
        }
        public EnumResultType DeleteSCRFCity(long itemId)
        {
            return SCRFCityBL.DeleteSCRFCity(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionStaffGuestFlightDetailsService : IDisposable
    {
        #region Feilds
        ExhibitionStaffGuestFlightDetailsBL ExhibitionStaffGuestFlightDetailsBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionStaffGuestFlightDetailsBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionStaffGuestFlightDetailsBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestFlightDetailsService()
        {
            ExhibitionStaffGuestFlightDetailsBL = new ExhibitionStaffGuestFlightDetailsBL();
        }
        public ExhibitionStaffGuestFlightDetailsService(string connectionString)
        {
            ExhibitionStaffGuestFlightDetailsBL = new ExhibitionStaffGuestFlightDetailsBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionStaffGuestFlightDetailsBL != null)
                this.ExhibitionStaffGuestFlightDetailsBL.Dispose();
        }
        #endregion
        #region Methods 
        public XsiExhibitionStaffGuestFlightDetails GetExhibitionStaffGuestFlightDetailsByItemId(long itemId)
        {
            return ExhibitionStaffGuestFlightDetailsBL.GetExhibitionStaffGuestFlightDetailsByItemId(itemId);
        }

        public List<XsiExhibitionStaffGuestFlightDetails> GetExhibitionStaffGuestFlightDetails()
        {
            return ExhibitionStaffGuestFlightDetailsBL.GetExhibitionStaffGuestFlightDetails();
        }
        public List<XsiExhibitionStaffGuestFlightDetails> GetExhibitionStaffGuestFlightDetails(EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestFlightDetailsBL.GetExhibitionStaffGuestFlightDetails(sortListBy);
        }
        public List<XsiExhibitionStaffGuestFlightDetails> GetExhibitionStaffGuestFlightDetails(XsiExhibitionStaffGuestFlightDetails entity)
        {
            return ExhibitionStaffGuestFlightDetailsBL.GetExhibitionStaffGuestFlightDetails(entity);
        }
        public List<XsiExhibitionStaffGuestFlightDetails> GetExhibitionStaffGuestFlightDetails(XsiExhibitionStaffGuestFlightDetails entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestFlightDetailsBL.GetExhibitionStaffGuestFlightDetails(entity, sortListBy);
        }
        public XsiExhibitionStaffGuestFlightDetails GetExhibitionStaffGuestFlightDetails(long groupId, long languageId)
        {
            return ExhibitionStaffGuestFlightDetailsBL.GetExhibitionStaffGuestFlightDetails(groupId, languageId);
        }

        public EnumResultType InsertExhibitionStaffGuestFlightDetails(XsiExhibitionStaffGuestFlightDetails entity)
        {
            return ExhibitionStaffGuestFlightDetailsBL.InsertExhibitionStaffGuestFlightDetails(entity);
        }
        public EnumResultType UpdateExhibitionStaffGuestFlightDetails(XsiExhibitionStaffGuestFlightDetails entity)
        {
            return ExhibitionStaffGuestFlightDetailsBL.UpdateExhibitionStaffGuestFlightDetails(entity);
        }
        public EnumResultType RollbackExhibitionStaffGuestFlightDetails(long itemId)
        {
            return ExhibitionStaffGuestFlightDetailsBL.RollbackExhibitionStaffGuestFlightDetails(itemId);
        }
        public EnumResultType UpdateExhibitionStaffGuestFlightDetailsStatus(long itemId)
        {
            return ExhibitionStaffGuestFlightDetailsBL.UpdateExhibitionStaffGuestFlightDetailsStatus(itemId);
        }
        public EnumResultType DeleteExhibitionStaffGuestFlightDetails(XsiExhibitionStaffGuestFlightDetails entity)
        {
            return ExhibitionStaffGuestFlightDetailsBL.DeleteExhibitionStaffGuestFlightDetails(entity);
        }
        public EnumResultType DeleteExhibitionStaffGuestFlightDetails(long itemId)
        {
            return ExhibitionStaffGuestFlightDetailsBL.DeleteExhibitionStaffGuestFlightDetails(itemId);
        }
        #endregion
    }
}
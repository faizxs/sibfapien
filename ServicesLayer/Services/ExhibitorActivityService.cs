﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitorActivityService : IDisposable
    {
        #region Feilds
        ExhibitorActivityBL ExhibitorActivityBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitorActivityBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitorActivityBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitorActivityService()
        {
            ExhibitorActivityBL = new ExhibitorActivityBL();
        }
        public ExhibitorActivityService(string connectionString)
        {
            ExhibitorActivityBL = new ExhibitorActivityBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitorActivityBL != null)
                this.ExhibitorActivityBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ExhibitorActivityBL.GetNextGroupId();
        }
        public XsiMemberExhibitionActivityYearly GetExhibitorActivityByItemId(long itemId)
        {
            return ExhibitorActivityBL.GetExhibitorActivityByItemId(itemId);
        }
         

        public List<XsiMemberExhibitionActivityYearly> GetExhibitorActivity()
        {
            return ExhibitorActivityBL.GetExhibitorActivity();
        }
        public List<XsiMemberExhibitionActivityYearly> GetExhibitorActivity(EnumSortlistBy sortListBy)
        {
            return ExhibitorActivityBL.GetExhibitorActivity(sortListBy);
        }
        public List<XsiMemberExhibitionActivityYearly> GetExhibitorActivity(XsiMemberExhibitionActivityYearly entity)
        {
            return ExhibitorActivityBL.GetExhibitorActivity(entity);
        }
        public List<XsiMemberExhibitionActivityYearly> GetExhibitorActivity(XsiMemberExhibitionActivityYearly entity, EnumSortlistBy sortListBy)
        {
            return ExhibitorActivityBL.GetExhibitorActivity(entity, sortListBy);
        }
      

        public EnumResultType InsertExhibitorActivity(XsiMemberExhibitionActivityYearly entity)
        {
            return ExhibitorActivityBL.InsertExhibitorActivity(entity);
        }
        public EnumResultType UpdateExhibitorActivity(XsiMemberExhibitionActivityYearly entity)
        {
            return ExhibitorActivityBL.UpdateExhibitorActivity(entity);
        }
      
        public EnumResultType DeleteExhibitorActivity(XsiMemberExhibitionActivityYearly entity)
        {
            return ExhibitorActivityBL.DeleteExhibitorActivity(entity);
        }
        public EnumResultType DeleteExhibitorActivity(long itemId)
        {
            return ExhibitorActivityBL.DeleteExhibitorActivity(itemId);
        }
        #endregion
    }
}
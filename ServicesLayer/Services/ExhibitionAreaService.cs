﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionAreaService : IDisposable
    {
        #region Feilds
        ExhibitionAreaBL ExhibitionAreaBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return ExhibitionAreaBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public ExhibitionAreaService()
        {
            ExhibitionAreaBL = new ExhibitionAreaBL();
        }
        public ExhibitionAreaService(string connectionString)
        {
            ExhibitionAreaBL = new ExhibitionAreaBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionAreaBL != null)
                this.ExhibitionAreaBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionArea GetExhibitionAreaByItemId(long itemId)
        {
            return ExhibitionAreaBL.GetExhibitionAreaByItemId(itemId);
        }
        public List<XsiExhibitionArea> GetExhibitionArea()
        {
            return ExhibitionAreaBL.GetExhibitionArea();
        }
        public List<XsiExhibitionArea> GetExhibitionArea(EnumSortlistBy sortListBy)
        {
            return ExhibitionAreaBL.GetExhibitionArea(sortListBy);
        }
        public List<XsiExhibitionArea> GetExhibitionArea(XsiExhibitionArea entity)
        {
            return ExhibitionAreaBL.GetExhibitionArea(entity);
        }
        public List<XsiExhibitionArea> GetExhibitionArea(XsiExhibitionArea entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionAreaBL.GetExhibitionArea(entity, sortListBy);
        }
        public EnumResultType InsertExhibitionArea(XsiExhibitionArea entity)
        {
            return ExhibitionAreaBL.InsertExhibitionArea(entity);
        }
        public EnumResultType UpdateExhibitionArea(XsiExhibitionArea entity)
        {
            return ExhibitionAreaBL.UpdateExhibitionArea(entity);
        }
        public EnumResultType UpdateExhibitionAreaStatus(long itemId)
        {
            return ExhibitionAreaBL.UpdateExhibitionAreaStatus(itemId);
        }
        public EnumResultType DeleteExhibitionArea(XsiExhibitionArea entity)
        {
            return ExhibitionAreaBL.DeleteExhibitionArea(entity);
        }
        public EnumResultType DeleteExhibitionArea(long itemId)
        {
            return ExhibitionAreaBL.DeleteExhibitionArea(itemId);
        }
        #endregion
    }
}
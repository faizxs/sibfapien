﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PublisherNewsService : IDisposable
    {
        #region Feilds
        PublisherNewsBL PublisherNewsBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PublisherNewsBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return PublisherNewsBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PublisherNewsService()
        {
            PublisherNewsBL = new PublisherNewsBL();
        }
        public PublisherNewsService(string connectionString)
        {
            PublisherNewsBL = new PublisherNewsBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PublisherNewsBL != null)
                this.PublisherNewsBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiPublisherNews GetPublisherNewsByItemId(long itemId)
        {
            return PublisherNewsBL.GetPublisherNewsByItemId(itemId);
        }
        

        public List<XsiPublisherNews> GetPublisherNews()
        {
            return PublisherNewsBL.GetPublisherNews();
        }
        public List<XsiPublisherNews> GetPublisherNews(EnumSortlistBy sortListBy)
        {
            return PublisherNewsBL.GetPublisherNews(sortListBy);
        }
        public List<XsiPublisherNews> GetPublisherNews(XsiPublisherNews entity)
        {
            return PublisherNewsBL.GetPublisherNews(entity);
        }
        public List<XsiPublisherNews> GetPublisherNews(XsiPublisherNews entity, EnumSortlistBy sortListBy)
        {
            return PublisherNewsBL.GetPublisherNews(entity, sortListBy);
        }
        public List<XsiPublisherNews> GetCompletePublisherNews(XsiPublisherNews entity, EnumSortlistBy sortListBy,long langId)
        {
            return PublisherNewsBL.GetCompletePublisherNews(entity, sortListBy, langId);
        }
        public XsiPublisherNews GetPublisherNews(long itemId, long categoryId)
        {
            return PublisherNewsBL.GetPublisherNews(itemId, categoryId);
        }

        public EnumResultType InsertPublisherNews(XsiPublisherNews entity)
        {
            return PublisherNewsBL.InsertPublisherNews(entity);
        }
        public EnumResultType UpdatePublisherNews(XsiPublisherNews entity)
        {
            return PublisherNewsBL.UpdatePublisherNews(entity);
        }
        public EnumResultType UpdatePublisherNewsStatus(long itemId)
        {
            return PublisherNewsBL.UpdatePublisherNewsStatus(itemId);
        }
        public EnumResultType DeletePublisherNews(XsiPublisherNews entity)
        {
            return PublisherNewsBL.DeletePublisherNews(entity);
        }
        public EnumResultType DeletePublisherNews(long itemId)
        {
            return PublisherNewsBL.DeletePublisherNews(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class AdvertisementPageService : IDisposable
    {
        #region Feilds
        AdvertisementPageBL AdvertisementPageBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return AdvertisementPageBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return AdvertisementPageBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public AdvertisementPageService()
        {
            AdvertisementPageBL = new AdvertisementPageBL();
        }
        public AdvertisementPageService(string connectionString)
        {
            AdvertisementPageBL = new AdvertisementPageBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AdvertisementPageBL != null)
                this.AdvertisementPageBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return AdvertisementPageBL.GetNextGroupId();
        }
        public XsiAdvertisementPage GetAdvertisementPageByItemId(long itemId)
        {
            return AdvertisementPageBL.GetAdvertisementPageByItemId(itemId);
        }
        public List<XsiAdvertisementPage> GetAdvertisementPage()
        {
            return AdvertisementPageBL.GetAdvertisementPage();
        }
        public List<XsiAdvertisementPage> GetAdvertisementPage(EnumSortlistBy sortListBy)
        {
            return AdvertisementPageBL.GetAdvertisementPage(sortListBy);
        }
        public List<XsiAdvertisementPage> GetAdvertisementPage(XsiAdvertisementPage entity)
        {
            return AdvertisementPageBL.GetAdvertisementPage(entity);
        }
        public List<XsiAdvertisementPage> GetAdvertisementPage(XsiAdvertisementPage entity, EnumSortlistBy sortListBy)
        {
            return AdvertisementPageBL.GetAdvertisementPage(entity, sortListBy);
        }
        public XsiAdvertisementPage GetAdvertisementPage(long groupId, long languageId)
        {
            return AdvertisementPageBL.GetAdvertisementPage(groupId, languageId);
        }

        public EnumResultType InsertAdvertisementPage(XsiAdvertisementPage entity)
        {
            return AdvertisementPageBL.InsertAdvertisementPage(entity);
        }
        public EnumResultType UpdateAdvertisementPage(XsiAdvertisementPage entity)
        {
            return AdvertisementPageBL.UpdateAdvertisementPage(entity);
        }
        public EnumResultType RollbackAdvertisementPage(long itemId)
        {
            return AdvertisementPageBL.RollbackAdvertisementPage(itemId);
        }
        public EnumResultType UpdateAdvertisementPageStatus(long itemId)
        {
            return AdvertisementPageBL.UpdateAdvertisementPageStatus(itemId);
        }
        public EnumResultType DeleteAdvertisementPage(XsiAdvertisementPage entity)
        {
            return AdvertisementPageBL.DeleteAdvertisementPage(entity);
        }
        public EnumResultType DeleteAdvertisementPage(long itemId)
        {
            return AdvertisementPageBL.DeleteAdvertisementPage(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFHomepageBannerService : IDisposable
    {
        #region Feilds
        SCRFHomepageBannerBL SCRFHomepageBannerBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFHomepageBannerBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFHomepageBannerBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFHomepageBannerService()
        {
            SCRFHomepageBannerBL = new SCRFHomepageBannerBL();
        }
        public SCRFHomepageBannerService(string connectionString)
        {
            SCRFHomepageBannerBL = new SCRFHomepageBannerBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFHomepageBannerBL != null)
                this.SCRFHomepageBannerBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfhomepageBanner GetSCRFHomepageBannerByItemId(long itemId)
        {
            return SCRFHomepageBannerBL.GetSCRFHomepageBannerByItemId(itemId);
        }

        public List<XsiScrfhomepageBanner> GetSCRFHomepageBanner()
        {
            return SCRFHomepageBannerBL.GetSCRFHomepageBanner();
        }
        public List<XsiScrfhomepageBanner> GetSCRFHomepageBanner(EnumSortlistBy sortListBy)
        {
            return SCRFHomepageBannerBL.GetSCRFHomepageBanner(sortListBy);
        }
        public List<XsiScrfhomepageBanner> GetSCRFHomepageBanner(XsiScrfhomepageBanner entity)
        {
            return SCRFHomepageBannerBL.GetSCRFHomepageBanner(entity);
        }
        public List<XsiScrfhomepageBanner> GetSCRFHomepageBanner(XsiScrfhomepageBanner entity, EnumSortlistBy sortListBy)
        {
            return SCRFHomepageBannerBL.GetSCRFHomepageBanner(entity, sortListBy);
        }

        public EnumResultType InsertSCRFHomepageBanner(XsiScrfhomepageBanner entity)
        {
            return SCRFHomepageBannerBL.InsertSCRFHomepageBanner(entity);
        }
        public EnumResultType UpdateSCRFHomepageBanner(XsiScrfhomepageBanner entity)
        {
            return SCRFHomepageBannerBL.UpdateSCRFHomepageBanner(entity);
        }
        public EnumResultType UpdateSCRFHomepageBannerStatus(long itemId)
        {
            return SCRFHomepageBannerBL.UpdateSCRFHomepageBannerStatus(itemId);
        }
        public EnumResultType DeleteSCRFHomepageBanner(XsiScrfhomepageBanner entity)
        {
            return SCRFHomepageBannerBL.DeleteSCRFHomepageBanner(entity);
        }
        public EnumResultType DeleteSCRFHomepageBanner(long itemId)
        {
            return SCRFHomepageBannerBL.DeleteSCRFHomepageBanner(itemId);
        }

        //Custom 
        public long GetHomeBannerInfo(long languageid)
        {
            return SCRFHomepageBannerBL.GetHomeBannerInfo(languageid);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class NewsletterService : IDisposable
    {
        #region Feilds
        NewsletterBL NewsletterBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return NewsletterBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return NewsletterBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public NewsletterService()
        {
            NewsletterBL = new NewsletterBL();
        }
        public NewsletterService(string connectionString)
        {
            NewsletterBL = new NewsletterBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.NewsletterBL != null)
                this.NewsletterBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiNewsletter GetNewsletterByItemId(long itemId)
        {
            return NewsletterBL.GetNewsletterByItemId(itemId);
        }
         
        public List<XsiNewsletter> GetNewsletter()
        {
            return NewsletterBL.GetNewsletter();
        }
        public List<XsiNewsletter> GetNewsletter(EnumSortlistBy sortListBy)
        {
            return NewsletterBL.GetNewsletter(sortListBy);
        }
        public List<XsiNewsletter> GetNewsletter(XsiNewsletter entity)
        {
            return NewsletterBL.GetNewsletter(entity);
        }
        public List<XsiNewsletter> GetNewsletter(XsiNewsletter entity, EnumSortlistBy sortListBy)
        {
            return NewsletterBL.GetNewsletter(entity, sortListBy);
        }
         
        public EnumResultType InsertNewsletter(XsiNewsletter entity)
        {
            return NewsletterBL.InsertNewsletter(entity);
        }
        public EnumResultType UpdateNewsletter(XsiNewsletter entity)
        {
            return NewsletterBL.UpdateNewsletter(entity);
        }
        public EnumResultType UpdateNewsletterStatus(long itemId)
        {
            return NewsletterBL.UpdateNewsletterStatus(itemId);
        }
        public EnumResultType DeleteNewsletter(XsiNewsletter entity)
        {
            return NewsletterBL.DeleteNewsletter(entity);
        }
        public EnumResultType DeleteNewsletter(long itemId)
        {
            return NewsletterBL.DeleteNewsletter(itemId);
        }
        #endregion
    }
}
﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionRepresentativeParticipatingNoVisaService : IDisposable
    {
        #region Feilds
        ExhibitionRepresentativeParticipatingNoVisaBL ExhibitionRepresentativeParticipatingNoVisaBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionRepresentativeParticipatingNoVisaBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionRepresentativeParticipatingNoVisaBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionRepresentativeParticipatingNoVisaService()
        {
            ExhibitionRepresentativeParticipatingNoVisaBL = new ExhibitionRepresentativeParticipatingNoVisaBL();
        }
        public ExhibitionRepresentativeParticipatingNoVisaService(string connectionString)
        {
            ExhibitionRepresentativeParticipatingNoVisaBL = new ExhibitionRepresentativeParticipatingNoVisaBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionRepresentativeParticipatingNoVisaBL != null)
                this.ExhibitionRepresentativeParticipatingNoVisaBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.GetNextGroupId();
        }
        public XsiExhibitionRepresentativeParticipatingNoVisa GetExhibitionRepresentativeParticipatingNoVisaByItemId(long itemId)
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.GetExhibitionRepresentativeParticipatingNoVisaByItemId(itemId);
        }

        public XsiExhibitionRepresentativeParticipatingNoVisa GetByRepresentativeId(long itemId)
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.GetByRepresentativeId(itemId); 
        }


        public List<XsiExhibitionRepresentativeParticipatingNoVisa> GetExhibitionRepresentativeParticipatingNoVisa()
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.GetExhibitionRepresentativeParticipatingNoVisa();
        }
        public List<XsiExhibitionRepresentativeParticipatingNoVisa> GetExhibitionRepresentativeParticipatingNoVisa(EnumSortlistBy sortListBy)
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.GetExhibitionRepresentativeParticipatingNoVisa(sortListBy);
        }
        public List<XsiExhibitionRepresentativeParticipatingNoVisa> GetExhibitionRepresentativeParticipatingNoVisa(XsiExhibitionRepresentativeParticipatingNoVisa entity)
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.GetExhibitionRepresentativeParticipatingNoVisa(entity);
        }
        public List<XsiExhibitionRepresentativeParticipatingNoVisa> GetExhibitionRepresentativeParticipatingNoVisa(XsiExhibitionRepresentativeParticipatingNoVisa entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.GetExhibitionRepresentativeParticipatingNoVisa(entity, sortListBy);
        }
        public XsiExhibitionRepresentativeParticipatingNoVisa GetExhibitionRepresentativeParticipatingNoVisa(long groupId, long languageId)
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.GetExhibitionRepresentativeParticipatingNoVisa(groupId, languageId);
        }

        public EnumResultType InsertExhibitionRepresentativeParticipatingNoVisa(XsiExhibitionRepresentativeParticipatingNoVisa entity)
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.InsertExhibitionRepresentativeParticipatingNoVisa(entity);
        }
        public EnumResultType UpdateExhibitionRepresentativeParticipatingNoVisa(XsiExhibitionRepresentativeParticipatingNoVisa entity)
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.UpdateExhibitionRepresentativeParticipatingNoVisa(entity);
        }
        public EnumResultType RollbackExhibitionRepresentativeParticipatingNoVisa(long itemId)
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.RollbackExhibitionRepresentativeParticipatingNoVisa(itemId);
        }
        public EnumResultType UpdateExhibitionRepresentativeParticipatingNoVisaStatus(long itemId)
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.UpdateExhibitionRepresentativeParticipatingNoVisaStatus(itemId);
        }
        public EnumResultType DeleteExhibitionRepresentativeParticipatingNoVisa(XsiExhibitionRepresentativeParticipatingNoVisa entity)
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.DeleteExhibitionRepresentativeParticipatingNoVisa(entity);
        }
        public EnumResultType DeleteExhibitionRepresentativeParticipatingNoVisa(long itemId)
        {
            return ExhibitionRepresentativeParticipatingNoVisaBL.DeleteExhibitionRepresentativeParticipatingNoVisa(itemId);
        }
        #endregion
    }
}
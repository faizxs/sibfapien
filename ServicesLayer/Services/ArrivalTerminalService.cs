﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ArrivalTerminalService : IDisposable
    {
        #region Feilds
        ArrivalTerminalBL ArrivalTerminalBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ArrivalTerminalBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ArrivalTerminalBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ArrivalTerminalService()
        {
            ArrivalTerminalBL = new ArrivalTerminalBL();
        }
        public ArrivalTerminalService(string connectionString)
        {
            ArrivalTerminalBL = new ArrivalTerminalBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ArrivalTerminalBL != null)
                this.ArrivalTerminalBL.Dispose();
        }
        #endregion
        #region Methods

        public XsiArrivalTerminal GetArrivalTerminalByItemId(long itemId)
        {
            return ArrivalTerminalBL.GetArrivalTerminalByItemId(itemId);
        }
        public List<XsiArrivalTerminal> GetArrivalTerminal()
        {
            return ArrivalTerminalBL.GetArrivalTerminal();
        }
        public List<XsiArrivalTerminal> GetArrivalTerminal(EnumSortlistBy sortListBy)
        {
            return ArrivalTerminalBL.GetArrivalTerminal(sortListBy);
        }
        public List<XsiArrivalTerminal> GetArrivalTerminal(XsiArrivalTerminal entity)
        {
            return ArrivalTerminalBL.GetArrivalTerminal(entity);
        }
        public List<XsiArrivalTerminal> GetArrivalTerminal(XsiArrivalTerminal entity, EnumSortlistBy sortListBy)
        {
            return ArrivalTerminalBL.GetArrivalTerminal(entity, sortListBy);
        }
        public List<XsiArrivalTerminal> GetCompleteArrivalTerminal(XsiArrivalTerminal entity, EnumSortlistBy sortListBy)
        {
            return ArrivalTerminalBL.GetCompleteArrivalTerminal(entity, sortListBy);
        }
        public XsiArrivalTerminal GetArrivalTerminal(long groupId, long categoryId)
        {
            return ArrivalTerminalBL.GetArrivalTerminal(groupId, categoryId);
        }

        public EnumResultType InsertArrivalTerminal(XsiArrivalTerminal entity)
        {
            return ArrivalTerminalBL.InsertArrivalTerminal(entity);
        }
        public EnumResultType UpdateArrivalTerminal(XsiArrivalTerminal entity)
        {
            return ArrivalTerminalBL.UpdateArrivalTerminal(entity);
        }
        public EnumResultType UpdateArrivalTerminalStatus(long itemId)
        {
            return ArrivalTerminalBL.UpdateArrivalTerminalStatus(itemId);
        }
        public EnumResultType DeleteArrivalTerminal(XsiArrivalTerminal entity)
        {
            return ArrivalTerminalBL.DeleteArrivalTerminal(entity);
        }
        public EnumResultType DeleteArrivalTerminal(long itemId)
        {
            return ArrivalTerminalBL.DeleteArrivalTerminal(itemId);
        }
        #endregion
    }
}
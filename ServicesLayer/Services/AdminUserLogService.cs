﻿using System;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;
using System.Linq;

namespace Xsi.ServicesLayer
{
    public class AdminUserLogService : IDisposable
    {
        #region Feilds
        AdminUserLogBL XsiAdminUserLogBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return XsiAdminUserLogBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return XsiAdminUserLogBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public AdminUserLogService()
        {
            XsiAdminUserLogBL = new AdminUserLogBL();
        }
        public AdminUserLogService(string connectionString)
        {
            XsiAdminUserLogBL = new AdminUserLogBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiAdminUserLogBL != null)
                this.XsiAdminUserLogBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdminLogs GetLogByItemId(long itemId)
        {
            return XsiAdminUserLogBL.GetLogByItemId(itemId);
        }
        public XsiAdminLogs GetLogLastLogin(long userId)
        {
            return XsiAdminUserLogBL.GetLogLastLogin(userId);
        }
        public List<XsiAdminLogs> GetLog(EnumSortlistBy sortListBy = EnumSortlistBy.ByItemIdDesc)
        {
            return XsiAdminUserLogBL.GetLog(sortListBy);
        }
        public List<XsiAdminLogs> GetLog(XsiAdminLogs entity)
        {
            return XsiAdminUserLogBL.GetLog(entity);
        }
        
        public EnumResultType InsertLog(XsiAdminLogs entity)
        {
            return XsiAdminUserLogBL.InsertLog(entity);
        }
        public EnumResultType UpdateLog(XsiAdminLogs entity)
        {
            return XsiAdminUserLogBL.UpdateLog(entity);
        }
        public EnumResultType DeleteLog(XsiAdminLogs entity)
        {
            return XsiAdminUserLogBL.DeleteLog(entity);
        }
        public EnumResultType DeleteLog(long itemId)
        {
            return XsiAdminUserLogBL.DeleteLog(itemId);
        }

        public void DeleteSixMonthsOldLogs()
        {
            XsiAdminUserLogBL.DeleteSixMonthsOldLog();
        }
        #endregion
    }
}
﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFAwardsNominationFormService : IDisposable
    {
        #region Feilds
        SCRFAwardsNominationFormBL SCRFAwardsNominationFormBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFAwardsNominationFormBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFAwardsNominationFormBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFAwardsNominationFormService()
        {
            SCRFAwardsNominationFormBL = new SCRFAwardsNominationFormBL();
        }
        public SCRFAwardsNominationFormService(string connectionString)
        {
            SCRFAwardsNominationFormBL = new SCRFAwardsNominationFormBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFAwardsNominationFormBL != null)
                this.SCRFAwardsNominationFormBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return SCRFAwardsNominationFormBL.GetNextGroupId();
        }
        public XsiScrfawardNominationForm GetSCRFAwardsNominationFormByItemId(long itemId)
        {
            return SCRFAwardsNominationFormBL.GetSCRFAwardsNominationFormByItemId(itemId);
        }
         
        public List<XsiScrfawardNominationForm> GetSCRFAwardsNominationForm()
        {
            return SCRFAwardsNominationFormBL.GetSCRFAwardsNominationForm();
        }
        public List<XsiScrfawardNominationForm> GetSCRFAwardsNominationForm(EnumSortlistBy sortListBy)
        {
            return SCRFAwardsNominationFormBL.GetSCRFAwardsNominationForm(sortListBy);
        }
        public List<XsiScrfawardNominationForm> GetSCRFAwardsNominationForm(XsiScrfawardNominationForm entity)
        {
            return SCRFAwardsNominationFormBL.GetSCRFAwardsNominationForm(entity);
        }
        public List<XsiScrfawardNominationForm> GetSCRFAwardsNominationForm(XsiScrfawardNominationForm entity, EnumSortlistBy sortListBy)
        {
            return SCRFAwardsNominationFormBL.GetSCRFAwardsNominationForm(entity, sortListBy);
        }
        public XsiScrfawardNominationForm GetSCRFAwardsNominationForm(long groupId, long languageId)
        {
            return SCRFAwardsNominationFormBL.GetSCRFAwardsNominationForm(groupId, languageId);
        }

        public EnumResultType InsertSCRFAwardsNominationForm(XsiScrfawardNominationForm entity)
        {
            return SCRFAwardsNominationFormBL.InsertSCRFAwardsNominationForm(entity);
        }
        public EnumResultType UpdateSCRFAwardsNominationForm(XsiScrfawardNominationForm entity)
        {
            return SCRFAwardsNominationFormBL.UpdateSCRFAwardsNominationForm(entity);
        }

        public EnumResultType UpdateSCRFAwardsNominationFormStatus(long itemId)
        {
            return SCRFAwardsNominationFormBL.UpdateSCRFAwardsNominationFormStatus(itemId);
        }
        public EnumResultType DeleteSCRFAwardsNominationForm(XsiScrfawardNominationForm entity)
        {
            return SCRFAwardsNominationFormBL.DeleteSCRFAwardsNominationForm(entity);
        }
        public EnumResultType DeleteSCRFAwardsNominationForm(long itemId)
        {
            return SCRFAwardsNominationFormBL.DeleteSCRFAwardsNominationForm(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class EmailCategoryService : IDisposable
    {
        #region Feilds
        EmailCategoryBL EmailCategoryBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return EmailCategoryBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public EmailCategoryService()
        {
            EmailCategoryBL = new EmailCategoryBL();
        }
        public EmailCategoryService(string connectionString)
        {
            EmailCategoryBL = new EmailCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EmailCategoryBL != null)
                this.EmailCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiEmailCategory GetEmailCategoryByItemId(long itemId)
        {
            return EmailCategoryBL.GetEmailCategoryByItemId(itemId);
        }

        public List<XsiEmailCategory> GetEmailCategory()
        {
            return EmailCategoryBL.GetEmailCategory();
        }
        public List<XsiEmailCategory> GetEmailCategory(EnumSortlistBy sortListBy)
        {
            return EmailCategoryBL.GetEmailCategory(sortListBy);
        }
        public List<XsiEmailCategory> GetEmailCategory(XsiEmailCategory entity)
        {
            return EmailCategoryBL.GetEmailCategory(entity);
        }
        public List<XsiEmailCategory> GetEmailCategory(XsiEmailCategory entity, EnumSortlistBy sortListBy)
        {
            return EmailCategoryBL.GetEmailCategory(entity, sortListBy);
        }
        public List<XsiEmailCategory> GetEmailCategoryOr(XsiEmailCategory entity, EnumSortlistBy sortListBy)
        {
            return EmailCategoryBL.GetEmailCategoryOr(entity, sortListBy);
        }

        public EnumResultType InsertEmailCategory(XsiEmailCategory entity)
        {
            return EmailCategoryBL.InsertEmailCategory(entity);
        }
        public EnumResultType UpdateEmailCategory(XsiEmailCategory entity)
        {
            return EmailCategoryBL.UpdateEmailCategory(entity);
        }
        public EnumResultType UpdateEmailCategoryStatus(long itemId, long langId)
        {
            return EmailCategoryBL.UpdateEmailCategoryStatus(itemId,langId);
        }
        public EnumResultType DeleteEmailCategory(XsiEmailCategory entity)
        {
            return EmailCategoryBL.DeleteEmailCategory(entity);
        }
        public EnumResultType DeleteEmailCategory(long itemId)
        {
            return EmailCategoryBL.DeleteEmailCategory(itemId);
        }
        #endregion
    }
}
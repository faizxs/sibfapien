﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFAdvertisementBannerService : IDisposable
    {
        #region Feilds
        SCRFAdvertisementBannerBL SCRFAdvertisementBannerBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFAdvertisementBannerBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFAdvertisementBannerBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFAdvertisementBannerService()
        {
            SCRFAdvertisementBannerBL = new SCRFAdvertisementBannerBL();
        }
        public SCRFAdvertisementBannerService(string connectionString)
        {
            SCRFAdvertisementBannerBL = new SCRFAdvertisementBannerBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFAdvertisementBannerBL != null)
                this.SCRFAdvertisementBannerBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfadvertisementBanner GetSCRFAdvertisementBannerByItemId(long itemId)
        {
            return SCRFAdvertisementBannerBL.GetSCRFAdvertisementBannerByItemId(itemId);
        }

        public List<XsiScrfadvertisementBanner> GetSCRFAdvertisementBanner()
        {
            return SCRFAdvertisementBannerBL.GetSCRFAdvertisementBanner();
        }
        public List<XsiScrfadvertisementBanner> GetSCRFAdvertisementBanner(EnumSortlistBy sortListBy)
        {
            return SCRFAdvertisementBannerBL.GetSCRFAdvertisementBanner(sortListBy);
        }
        public List<XsiScrfadvertisementBanner> GetSCRFAdvertisementBanner(XsiScrfadvertisementBanner entity)
        {
            return SCRFAdvertisementBannerBL.GetSCRFAdvertisementBanner(entity);
        }
        public List<XsiScrfadvertisementBanner> GetSCRFAdvertisementBanner(XsiScrfadvertisementBanner entity, EnumSortlistBy sortListBy)
        {
            return SCRFAdvertisementBannerBL.GetSCRFAdvertisementBanner(entity, sortListBy);
        }
        public List<XsiScrfadvertisementBanner> GetSCRFAdvertisementBannerOr(XsiScrfadvertisementBanner entity, EnumSortlistBy sortListBy)
        {
            return SCRFAdvertisementBannerBL.GetSCRFAdvertisementBannerOr(entity, sortListBy);
        }

        public EnumResultType InsertSCRFAdvertisementBanner(XsiScrfadvertisementBanner entity)
        {
            return SCRFAdvertisementBannerBL.InsertSCRFAdvertisementBanner(entity);
        }
        public EnumResultType UpdateSCRFAdvertisementBanner(XsiScrfadvertisementBanner entity)
        {
            return SCRFAdvertisementBannerBL.UpdateSCRFAdvertisementBanner(entity);
        }
        public EnumResultType RollbackSCRFAdvertisementBanner(long itemId)
        {
            return SCRFAdvertisementBannerBL.RollbackSCRFAdvertisementBanner(itemId);
        }
        public EnumResultType UpdateSCRFAdvertisementBannerStatus(long itemId)
        {
            return SCRFAdvertisementBannerBL.UpdateSCRFAdvertisementBannerStatus(itemId);
        }
        public EnumResultType DeleteSCRFAdvertisementBanner(XsiScrfadvertisementBanner entity)
        {
            return SCRFAdvertisementBannerBL.DeleteSCRFAdvertisementBanner(entity);
        }
        public EnumResultType DeleteSCRFAdvertisementBanner(long itemId)
        {
            return SCRFAdvertisementBannerBL.DeleteSCRFAdvertisementBanner(itemId);
        }
        #endregion
    }
}
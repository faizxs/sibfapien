﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionMemberApplicationYearlyPreApprovedService : IDisposable
    {
        #region Feilds
        ExhibitionMemberApplicationYearlyPreApprovedBL ExhibitionMemberApplicationYearlyPreApprovedBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionMemberApplicationYearlyPreApprovedBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionMemberApplicationYearlyPreApprovedBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionMemberApplicationYearlyPreApprovedService()
        {
            ExhibitionMemberApplicationYearlyPreApprovedBL = new ExhibitionMemberApplicationYearlyPreApprovedBL();
        }
        public ExhibitionMemberApplicationYearlyPreApprovedService(string connectionString)
        {
            ExhibitionMemberApplicationYearlyPreApprovedBL = new ExhibitionMemberApplicationYearlyPreApprovedBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionMemberApplicationYearlyPreApprovedBL != null)
                this.ExhibitionMemberApplicationYearlyPreApprovedBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionMemberApplicationYearlyPreApproved GetExhibitionMemberApplicationYearlyPreApprovedByItemId(long itemId)
        {
            return ExhibitionMemberApplicationYearlyPreApprovedBL.GetExhibitionMemberApplicationYearlyPreApprovedByItemId(itemId);
        }
         
        public List<XsiExhibitionMemberApplicationYearlyPreApproved> GetExhibitionMemberApplicationYearlyPreApproved()
        {
            return ExhibitionMemberApplicationYearlyPreApprovedBL.GetExhibitionMemberApplicationYearlyPreApproved();
        }
        public List<XsiExhibitionMemberApplicationYearlyPreApproved> GetExhibitionMemberApplicationYearlyPreApproved(EnumSortlistBy sortListBy)
        {
            return ExhibitionMemberApplicationYearlyPreApprovedBL.GetExhibitionMemberApplicationYearlyPreApproved(sortListBy);
        }
        public List<XsiExhibitionMemberApplicationYearlyPreApproved> GetExhibitionMemberApplicationYearlyPreApproved(XsiExhibitionMemberApplicationYearlyPreApproved entity)
        {
            return ExhibitionMemberApplicationYearlyPreApprovedBL.GetExhibitionMemberApplicationYearlyPreApproved(entity);
        }
        public List<XsiExhibitionMemberApplicationYearlyPreApproved> GetExhibitionMemberApplicationYearlyPreApproved(XsiExhibitionMemberApplicationYearlyPreApproved entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionMemberApplicationYearlyPreApprovedBL.GetExhibitionMemberApplicationYearlyPreApproved(entity, sortListBy);
        }
        public EnumResultType InsertExhibitionMemberApplicationYearlyPreApproved(XsiExhibitionMemberApplicationYearlyPreApproved entity)
        {
            return ExhibitionMemberApplicationYearlyPreApprovedBL.InsertExhibitionMemberApplicationYearlyPreApproved(entity);
        }
        public EnumResultType UpdateExhibitionMemberApplicationYearlyPreApproved(XsiExhibitionMemberApplicationYearlyPreApproved entity)
        {
            return ExhibitionMemberApplicationYearlyPreApprovedBL.UpdateExhibitionMemberApplicationYearlyPreApproved(entity);
        }
        public EnumResultType UpdateExhibitionMemberApplicationYearlyPreApprovedStatus(long itemId, long langId)
        {
            return ExhibitionMemberApplicationYearlyPreApprovedBL.UpdateExhibitionMemberApplicationYearlyPreApprovedStatus(itemId, langId);
        }
        public EnumResultType DeleteExhibitionMemberApplicationYearlyPreApproved(XsiExhibitionMemberApplicationYearlyPreApproved entity)
        {
            return ExhibitionMemberApplicationYearlyPreApprovedBL.DeleteExhibitionMemberApplicationYearlyPreApproved(entity);
        }
        public EnumResultType DeleteExhibitionMemberApplicationYearlyPreApproved(long itemId)
        {
            return ExhibitionMemberApplicationYearlyPreApprovedBL.DeleteExhibitionMemberApplicationYearlyPreApproved(itemId);
        }
        #endregion
    }
}
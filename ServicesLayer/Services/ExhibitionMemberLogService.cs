﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionMemberLogService : IDisposable
    {
        #region Feilds
        ExhibitionMemberLogBL ExhibitionMemberLogBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionMemberLogBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionMemberLogBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionMemberLogService()
        {
            ExhibitionMemberLogBL = new ExhibitionMemberLogBL();
        }
        public ExhibitionMemberLogService(string connectionString)
        {
            ExhibitionMemberLogBL = new ExhibitionMemberLogBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionMemberLogBL != null)
                this.ExhibitionMemberLogBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ExhibitionMemberLogBL.GetNextGroupId();
        }
        public XsiExhibitionMemberLog GetExhibitionMemberLogByItemId(long itemId)
        {
            return ExhibitionMemberLogBL.GetExhibitionMemberLogByItemId(itemId);
        }
         
        public List<XsiExhibitionMemberLog> GetExhibitionMemberLog()
        {
            return ExhibitionMemberLogBL.GetExhibitionMemberLog();
        }
        public List<XsiExhibitionMemberLog> GetExhibitionMemberLog(EnumSortlistBy sortListBy)
        {
            return ExhibitionMemberLogBL.GetExhibitionMemberLog(sortListBy);
        }
        public List<XsiExhibitionMemberLog> GetExhibitionMemberLog(XsiExhibitionMemberLog entity)
        {
            return ExhibitionMemberLogBL.GetExhibitionMemberLog(entity);
        }
        public List<XsiExhibitionMemberLog> GetExhibitionMemberLog(XsiExhibitionMemberLog entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionMemberLogBL.GetExhibitionMemberLog(entity, sortListBy);
        }
        public XsiExhibitionMemberLog GetExhibitionMemberLog(long groupId, long languageId)
        {
            return ExhibitionMemberLogBL.GetExhibitionMemberLog(groupId, languageId);
        }

        public EnumResultType InsertExhibitionMemberLog(XsiExhibitionMemberLog entity)
        {
            return ExhibitionMemberLogBL.InsertExhibitionMemberLog(entity);
        }
        public EnumResultType UpdateExhibitionMemberLog(XsiExhibitionMemberLog entity)
        {
            return ExhibitionMemberLogBL.UpdateExhibitionMemberLog(entity);
        }
        public EnumResultType UpdateExhibitionMemberLogStatus(long itemId)
        {
            return ExhibitionMemberLogBL.UpdateExhibitionMemberLogStatus(itemId);
        }
        public EnumResultType DeleteExhibitionMemberLog(XsiExhibitionMemberLog entity)
        {
            return ExhibitionMemberLogBL.DeleteExhibitionMemberLog(entity);
        }
        public EnumResultType DeleteExhibitionMemberLog(long itemId)
        {
            return ExhibitionMemberLogBL.DeleteExhibitionMemberLog(itemId);
        }
        #endregion
    }
}
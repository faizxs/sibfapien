﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionShipmentAgencyService : IDisposable
    {
        #region Feilds
        ExhibitionShipmentAgencyBL ExhibitionShipmentAgencyBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionShipmentAgencyBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionShipmentAgencyBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionShipmentAgencyService()
        {
            ExhibitionShipmentAgencyBL = new ExhibitionShipmentAgencyBL();
        }
        public ExhibitionShipmentAgencyService(string connectionString)
        {
            ExhibitionShipmentAgencyBL = new ExhibitionShipmentAgencyBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionShipmentAgencyBL != null)
                this.ExhibitionShipmentAgencyBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ExhibitionShipmentAgencyBL.GetNextGroupId();
        }
        public XsiExhibitionShipmentAgency GetExhibitionShipmentAgencyByItemId(long itemId)
        {
            return ExhibitionShipmentAgencyBL.GetExhibitionShipmentAgencyByItemId(itemId);
        }
       
        public List<XsiExhibitionShipmentAgency> GetExhibitionShipmentAgency()
        {
            return ExhibitionShipmentAgencyBL.GetExhibitionShipmentAgency();
        }
        public List<XsiExhibitionShipmentAgency> GetExhibitionShipmentAgency(EnumSortlistBy sortListBy)
        {
            return ExhibitionShipmentAgencyBL.GetExhibitionShipmentAgency(sortListBy);
        }
        public List<XsiExhibitionShipmentAgency> GetExhibitionShipmentAgency(XsiExhibitionShipmentAgency entity)
        {
            return ExhibitionShipmentAgencyBL.GetExhibitionShipmentAgency(entity);
        }
        public List<XsiExhibitionShipmentAgency> GetExhibitionShipmentAgency(XsiExhibitionShipmentAgency entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionShipmentAgencyBL.GetExhibitionShipmentAgency(entity, sortListBy);
        }
        public XsiExhibitionShipmentAgency GetExhibitionShipmentAgency(long groupId, long languageId)
        {
            return ExhibitionShipmentAgencyBL.GetExhibitionShipmentAgency(groupId, languageId);
        }

        public EnumResultType InsertExhibitionShipmentAgency(XsiExhibitionShipmentAgency entity)
        {
            return ExhibitionShipmentAgencyBL.InsertExhibitionShipmentAgency(entity);
        }
        public EnumResultType UpdateExhibitionShipmentAgency(XsiExhibitionShipmentAgency entity)
        {
            return ExhibitionShipmentAgencyBL.UpdateExhibitionShipmentAgency(entity);
        }
        public EnumResultType UpdateExhibitionShipmentAgencyStatus(long itemId)
        {
            return ExhibitionShipmentAgencyBL.UpdateExhibitionShipmentAgencyStatus(itemId);
        }
        public EnumResultType DeleteExhibitionShipmentAgency(XsiExhibitionShipmentAgency entity)
        {
            return ExhibitionShipmentAgencyBL.DeleteExhibitionShipmentAgency(entity);
        }
        public EnumResultType DeleteExhibitionShipmentAgency(long itemId)
        {
            return ExhibitionShipmentAgencyBL.DeleteExhibitionShipmentAgency(itemId);
        }
        #endregion
    }
}
﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class CareerSubDepartmentService : IDisposable
    {
        #region Feilds
        CareerSubDepartmentBL CareerSubDepartmentBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return CareerSubDepartmentBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return CareerSubDepartmentBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public CareerSubDepartmentService()
        {
            CareerSubDepartmentBL = new CareerSubDepartmentBL();
        }
        public CareerSubDepartmentService(string connectionString)
        {
            CareerSubDepartmentBL = new CareerSubDepartmentBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerSubDepartmentBL != null)
                this.CareerSubDepartmentBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiCareerSubDepartment GetCareerSubDepartmentByItemId(long itemId)
        {
            return CareerSubDepartmentBL.GetCareerSubDepartmentByItemId(itemId);
        }
         
        public List<XsiCareerSubDepartment> GetCareerSubDepartment()
        {
            return CareerSubDepartmentBL.GetCareerSubDepartment();
        }
        public List<XsiCareerSubDepartment> GetCareerSubDepartment(EnumSortlistBy sortListBy)
        {
            return CareerSubDepartmentBL.GetCareerSubDepartment(sortListBy);
        }
        public List<XsiCareerSubDepartment> GetCareerSubDepartment(XsiCareerSubDepartment entity)
        {
            return CareerSubDepartmentBL.GetCareerSubDepartment(entity);
        }
        public List<XsiCareerSubDepartment> GetCareerSubDepartment(XsiCareerSubDepartment entity, EnumSortlistBy sortListBy)
        {
            return CareerSubDepartmentBL.GetCareerSubDepartment(entity, sortListBy);
        }
        
        public EnumResultType InsertCareerSubDepartment(XsiCareerSubDepartment entity)
        {
            return CareerSubDepartmentBL.InsertCareerSubDepartment(entity);
        }
        public EnumResultType UpdateCareerSubDepartment(XsiCareerSubDepartment entity)
        {
            return CareerSubDepartmentBL.UpdateCareerSubDepartment(entity);
        }
        public EnumResultType UpdateCareerSubDepartmentStatus(long itemId)
        {
            return CareerSubDepartmentBL.UpdateCareerSubDepartmentStatus(itemId);
        }
        public EnumResultType DeleteCareerSubDepartment(XsiCareerSubDepartment entity)
        {
            return CareerSubDepartmentBL.DeleteCareerSubDepartment(entity);
        }
        public EnumResultType DeleteCareerSubDepartment(long itemId)
        {
            return CareerSubDepartmentBL.DeleteCareerSubDepartment(itemId);
        }
        #endregion
    }
}
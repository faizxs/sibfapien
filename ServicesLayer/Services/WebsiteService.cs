﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class WebsiteService : IDisposable
    {
        #region Feilds
        WebsiteBL WebsiteBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return WebsiteBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return WebsiteBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public WebsiteService()
        {
            WebsiteBL = new WebsiteBL();
        }
        public WebsiteService(string connectionString)
        {
            WebsiteBL = new WebsiteBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.WebsiteBL != null)
                this.WebsiteBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiWebsites GetWebsiteByItemId(long itemId)
        {
            return WebsiteBL.GetWebsiteByItemId(itemId);
        }
        
        public List<XsiWebsites> GetWebsite()
        {
            return WebsiteBL.GetWebsite();
        }
        public List<XsiWebsites> GetWebsite(EnumSortlistBy sortListBy)
        {
            return WebsiteBL.GetWebsite(sortListBy);
        }
        public List<XsiWebsites> GetWebsite(XsiWebsites entity)
        {
            return WebsiteBL.GetWebsite(entity);
        }
        public List<XsiWebsites> GetWebsite(XsiWebsites entity, EnumSortlistBy sortListBy)
        {
            return WebsiteBL.GetWebsite(entity, sortListBy);
        }

        public EnumResultType InsertWebsite(XsiWebsites entity)
        {
            return WebsiteBL.InsertWebsite(entity);
        }
        public EnumResultType UpdateWebsite(XsiWebsites entity)
        {
            return WebsiteBL.UpdateWebsite(entity);
        }
        public EnumResultType DeleteWebsite(XsiWebsites entity)
        {
            return WebsiteBL.DeleteWebsite(entity);
        }
        public EnumResultType DeleteWebsite(long itemId)
        {
            return WebsiteBL.DeleteWebsite(itemId);
        }
        #endregion
    }
}
﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class CareerDepartmentService : IDisposable
    {
        #region Feilds
        CareerDepartmentBL CareerDepartmentBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return CareerDepartmentBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return CareerDepartmentBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public CareerDepartmentService()
        {
            CareerDepartmentBL = new CareerDepartmentBL();
        }
        public CareerDepartmentService(string connectionString)
        {
            CareerDepartmentBL = new CareerDepartmentBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerDepartmentBL != null)
                this.CareerDepartmentBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiCareerDepartment GetCareerDepartmentByItemId(long itemId)
        {
            return CareerDepartmentBL.GetCareerDepartmentByItemId(itemId);
        }
         
        public List<XsiCareerDepartment> GetCareerDepartment()
        {
            return CareerDepartmentBL.GetCareerDepartment();
        }
        public List<XsiCareerDepartment> GetCareerDepartment(EnumSortlistBy sortListBy)
        {
            return CareerDepartmentBL.GetCareerDepartment(sortListBy);
        }
        public List<XsiCareerDepartment> GetCareerDepartment(XsiCareerDepartment entity)
        {
            return CareerDepartmentBL.GetCareerDepartment(entity);
        }
        public List<XsiCareerDepartment> GetCareerDepartment(XsiCareerDepartment entity, EnumSortlistBy sortListBy)
        {
            return CareerDepartmentBL.GetCareerDepartment(entity, sortListBy);
        }
        public List<XsiCareerDepartment> GetCareerDepartmentOr(XsiCareerDepartment entity, EnumSortlistBy sortListBy)
        {
            return CareerDepartmentBL.GetCareerDepartmentOr(entity, sortListBy);
        }
         
        public EnumResultType InsertCareerDepartment(XsiCareerDepartment entity)
        {
            return CareerDepartmentBL.InsertCareerDepartment(entity);
        }
        public EnumResultType UpdateCareerDepartment(XsiCareerDepartment entity)
        {
            return CareerDepartmentBL.UpdateCareerDepartment(entity);
        }
        public EnumResultType UpdateCareerDepartmentStatus(long itemId)
        {
            return CareerDepartmentBL.UpdateCareerDepartmentStatus(itemId);
        }
        public EnumResultType DeleteCareerDepartment(XsiCareerDepartment entity)
        {
            return CareerDepartmentBL.DeleteCareerDepartment(entity);
        }
        public EnumResultType DeleteCareerDepartment(long itemId)
        {
            return CareerDepartmentBL.DeleteCareerDepartment(itemId);
        }
        #endregion
    }
}
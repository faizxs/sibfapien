﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionRepresentativeParticipatingService : IDisposable
    {
        #region Feilds
        ExhibitionRepresentativeParticipatingBL ExhibitionRepresentativeParticipatingBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionRepresentativeParticipatingBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionRepresentativeParticipatingBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionRepresentativeParticipatingService()
        {
            ExhibitionRepresentativeParticipatingBL = new ExhibitionRepresentativeParticipatingBL();
        }
        public ExhibitionRepresentativeParticipatingService(string connectionString)
        {
            ExhibitionRepresentativeParticipatingBL = new ExhibitionRepresentativeParticipatingBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionRepresentativeParticipatingBL != null)
                this.ExhibitionRepresentativeParticipatingBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ExhibitionRepresentativeParticipatingBL.GetNextGroupId();
        }
        public XsiExhibitionRepresentativeParticipating GetExhibitionRepresentativeParticipatingByItemId(long itemId)
        {
            return ExhibitionRepresentativeParticipatingBL.GetExhibitionRepresentativeParticipatingByItemId(itemId);
        }

        public XsiExhibitionRepresentativeParticipating GetByRepresentativeId(long itemId)
        {
            return ExhibitionRepresentativeParticipatingBL.GetByRepresentativeId(itemId); 
        }


        public List<XsiExhibitionRepresentativeParticipating> GetExhibitionRepresentativeParticipating()
        {
            return ExhibitionRepresentativeParticipatingBL.GetExhibitionRepresentativeParticipating();
        }
        public List<XsiExhibitionRepresentativeParticipating> GetExhibitionRepresentativeParticipating(EnumSortlistBy sortListBy)
        {
            return ExhibitionRepresentativeParticipatingBL.GetExhibitionRepresentativeParticipating(sortListBy);
        }
        public List<XsiExhibitionRepresentativeParticipating> GetExhibitionRepresentativeParticipating(XsiExhibitionRepresentativeParticipating entity)
        {
            return ExhibitionRepresentativeParticipatingBL.GetExhibitionRepresentativeParticipating(entity);
        }
        public List<XsiExhibitionRepresentativeParticipating> GetExhibitionRepresentativeParticipating(XsiExhibitionRepresentativeParticipating entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionRepresentativeParticipatingBL.GetExhibitionRepresentativeParticipating(entity, sortListBy);
        }
        public XsiExhibitionRepresentativeParticipating GetExhibitionRepresentativeParticipating(long groupId, long languageId)
        {
            return ExhibitionRepresentativeParticipatingBL.GetExhibitionRepresentativeParticipating(groupId, languageId);
        }

        public EnumResultType InsertExhibitionRepresentativeParticipating(XsiExhibitionRepresentativeParticipating entity)
        {
            return ExhibitionRepresentativeParticipatingBL.InsertExhibitionRepresentativeParticipating(entity);
        }
        public EnumResultType UpdateExhibitionRepresentativeParticipating(XsiExhibitionRepresentativeParticipating entity)
        {
            return ExhibitionRepresentativeParticipatingBL.UpdateExhibitionRepresentativeParticipating(entity);
        }
        public EnumResultType RollbackExhibitionRepresentativeParticipating(long itemId)
        {
            return ExhibitionRepresentativeParticipatingBL.RollbackExhibitionRepresentativeParticipating(itemId);
        }
        public EnumResultType UpdateExhibitionRepresentativeParticipatingStatus(long itemId)
        {
            return ExhibitionRepresentativeParticipatingBL.UpdateExhibitionRepresentativeParticipatingStatus(itemId);
        }
        public EnumResultType DeleteExhibitionRepresentativeParticipating(XsiExhibitionRepresentativeParticipating entity)
        {
            return ExhibitionRepresentativeParticipatingBL.DeleteExhibitionRepresentativeParticipating(entity);
        }
        public EnumResultType DeleteExhibitionRepresentativeParticipating(long itemId)
        {
            return ExhibitionRepresentativeParticipatingBL.DeleteExhibitionRepresentativeParticipating(itemId);
        }
        #endregion
    }
}
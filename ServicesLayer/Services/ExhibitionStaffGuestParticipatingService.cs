﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionStaffGuestParticipatingService : IDisposable
    {
        #region Feilds
        ExhibitionStaffGuestParticipatingBL ExhibitionStaffGuestParticipatingBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionStaffGuestParticipatingBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionStaffGuestParticipatingBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestParticipatingService()
        {
            ExhibitionStaffGuestParticipatingBL = new ExhibitionStaffGuestParticipatingBL();
        }
        public ExhibitionStaffGuestParticipatingService(string connectionString)
        {
            ExhibitionStaffGuestParticipatingBL = new ExhibitionStaffGuestParticipatingBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionStaffGuestParticipatingBL != null)
                this.ExhibitionStaffGuestParticipatingBL.Dispose();
        }
        #endregion
        #region Methods 
        public XsiExhibitionStaffGuestParticipating GetExhibitionStaffGuestParticipatingByItemId(long itemId)
        {
            return ExhibitionStaffGuestParticipatingBL.GetExhibitionStaffGuestParticipatingByItemId(itemId);
        }

        public XsiExhibitionStaffGuestParticipating GetByStaffGuestId(long itemId)
        {
            return ExhibitionStaffGuestParticipatingBL.GetByStaffGuestId(itemId); 
        }


        public List<XsiExhibitionStaffGuestParticipating> GetExhibitionStaffGuestParticipating()
        {
            return ExhibitionStaffGuestParticipatingBL.GetExhibitionStaffGuestParticipating();
        }
        public List<XsiExhibitionStaffGuestParticipating> GetExhibitionStaffGuestParticipating(EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestParticipatingBL.GetExhibitionStaffGuestParticipating(sortListBy);
        }
        public List<XsiExhibitionStaffGuestParticipating> GetExhibitionStaffGuestParticipating(XsiExhibitionStaffGuestParticipating entity)
        {
            return ExhibitionStaffGuestParticipatingBL.GetExhibitionStaffGuestParticipating(entity);
        }
        public List<XsiExhibitionStaffGuestParticipating> GetExhibitionStaffGuestParticipating(XsiExhibitionStaffGuestParticipating entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestParticipatingBL.GetExhibitionStaffGuestParticipating(entity, sortListBy);
        }
        public XsiExhibitionStaffGuestParticipating GetExhibitionStaffGuestParticipating(long groupId, long languageId)
        {
            return ExhibitionStaffGuestParticipatingBL.GetExhibitionStaffGuestParticipating(groupId, languageId);
        }

        public EnumResultType InsertExhibitionStaffGuestParticipating(XsiExhibitionStaffGuestParticipating entity)
        {
            return ExhibitionStaffGuestParticipatingBL.InsertExhibitionStaffGuestParticipating(entity);
        }
        public EnumResultType UpdateExhibitionStaffGuestParticipating(XsiExhibitionStaffGuestParticipating entity)
        {
            return ExhibitionStaffGuestParticipatingBL.UpdateExhibitionStaffGuestParticipating(entity);
        }
        public EnumResultType RollbackExhibitionStaffGuestParticipating(long itemId)
        {
            return ExhibitionStaffGuestParticipatingBL.RollbackExhibitionStaffGuestParticipating(itemId);
        }
        public EnumResultType UpdateExhibitionStaffGuestParticipatingStatus(long itemId)
        {
            return ExhibitionStaffGuestParticipatingBL.UpdateExhibitionStaffGuestParticipatingStatus(itemId);
        }
        public EnumResultType DeleteExhibitionStaffGuestParticipating(XsiExhibitionStaffGuestParticipating entity)
        {
            return ExhibitionStaffGuestParticipatingBL.DeleteExhibitionStaffGuestParticipating(entity);
        }
        public EnumResultType DeleteExhibitionStaffGuestParticipating(long itemId)
        {
            return ExhibitionStaffGuestParticipatingBL.DeleteExhibitionStaffGuestParticipating(itemId);
        }
        #endregion
    }
}
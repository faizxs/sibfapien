﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class RepresentativeAgreementService : IDisposable
    {
        #region Feilds
        RepresentativeAgreementBL RepresentativeAgreementBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return RepresentativeAgreementBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return RepresentativeAgreementBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public RepresentativeAgreementService()
        {
            RepresentativeAgreementBL = new RepresentativeAgreementBL();
        }
        public RepresentativeAgreementService(string connectionString)
        {
            RepresentativeAgreementBL = new RepresentativeAgreementBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.RepresentativeAgreementBL != null)
                this.RepresentativeAgreementBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiRepresentativeAgreement GetRepresentativeAgreementByItemId(long itemId)
        {
            return RepresentativeAgreementBL.GetRepresentativeAgreementByItemId(itemId);
        }
         
        public List<XsiRepresentativeAgreement> GetRepresentativeAgreement()
        {
            return RepresentativeAgreementBL.GetRepresentativeAgreement();
        }
        public List<XsiRepresentativeAgreement> GetRepresentativeAgreement(EnumSortlistBy sortListBy)
        {
            return RepresentativeAgreementBL.GetRepresentativeAgreement(sortListBy);
        }
        public List<XsiRepresentativeAgreement> GetRepresentativeAgreement(XsiRepresentativeAgreement entity)
        {
            return RepresentativeAgreementBL.GetRepresentativeAgreement(entity);
        }
        public List<XsiRepresentativeAgreement> GetRepresentativeAgreement(XsiRepresentativeAgreement entity, EnumSortlistBy sortListBy)
        {
            return RepresentativeAgreementBL.GetRepresentativeAgreement(entity, sortListBy);
        }
        public EnumResultType InsertRepresentativeAgreement(XsiRepresentativeAgreement entity)
        {
            return RepresentativeAgreementBL.InsertRepresentativeAgreement(entity);
        }
        public EnumResultType UpdateRepresentativeAgreement(XsiRepresentativeAgreement entity)
        {
            return RepresentativeAgreementBL.UpdateRepresentativeAgreement(entity);
        }
        public EnumResultType UpdateRepresentativeAgreementStatus(long itemId)
        {
            return RepresentativeAgreementBL.UpdateRepresentativeAgreementStatus(itemId);
        }
        public EnumResultType DeleteRepresentativeAgreement(XsiRepresentativeAgreement entity)
        {
            return RepresentativeAgreementBL.DeleteRepresentativeAgreement(entity);
        }
        public EnumResultType DeleteRepresentativeAgreement(long itemId)
        {
            return RepresentativeAgreementBL.DeleteRepresentativeAgreement(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ActivitiesBannerService : IDisposable
    {
        #region Feilds
        ActivitiesBannerBL ActivitiesBannerBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ActivitiesBannerBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ActivitiesBannerBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ActivitiesBannerService()
        {
            ActivitiesBannerBL = new ActivitiesBannerBL();
        }
        public ActivitiesBannerService(string connectionString)
        {
            ActivitiesBannerBL = new ActivitiesBannerBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ActivitiesBannerBL != null)
                this.ActivitiesBannerBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiActivitiesBanner GetActivitiesBannerByItemId(long itemId)
        {
            return ActivitiesBannerBL.GetActivitiesBannerByItemId(itemId);
        }

        public List<XsiActivitiesBanner> GetActivitiesBanner()
        {
            return ActivitiesBannerBL.GetActivitiesBanner();
        }
        public List<XsiActivitiesBanner> GetActivitiesBanner(EnumSortlistBy sortListBy)
        {
            return ActivitiesBannerBL.GetActivitiesBanner(sortListBy);
        }
        public List<XsiActivitiesBanner> GetActivitiesBanner(XsiActivitiesBanner entity)
        {
            return ActivitiesBannerBL.GetActivitiesBanner(entity);
        }
        public List<XsiActivitiesBanner> GetActivitiesBanner(XsiActivitiesBanner entity, EnumSortlistBy sortListBy)
        {
            return ActivitiesBannerBL.GetActivitiesBanner(entity, sortListBy);
        }
        public List<XsiActivitiesBanner> GetActivitiesBannerOr(XsiActivitiesBanner entity, EnumSortlistBy sortListBy)
        {
            return ActivitiesBannerBL.GetActivitiesBannerOr(entity, sortListBy);
        }

        public EnumResultType InsertActivitiesBanner(XsiActivitiesBanner entity)
        {
            return ActivitiesBannerBL.InsertActivitiesBanner(entity);
        }
        public EnumResultType UpdateActivitiesBanner(XsiActivitiesBanner entity)
        {
            return ActivitiesBannerBL.UpdateActivitiesBanner(entity);
        }
        public EnumResultType UpdateActivitiesBannerStatus(long itemId)
        {
            return ActivitiesBannerBL.UpdateActivitiesBannerStatus(itemId);
        }
        public EnumResultType DeleteActivitiesBanner(XsiActivitiesBanner entity)
        {
            return ActivitiesBannerBL.DeleteActivitiesBanner(entity);
        }
        public EnumResultType DeleteActivitiesBanner(long itemId)
        {
            return ActivitiesBannerBL.DeleteActivitiesBanner(itemId);
        }
        //Custom 
        public long GetHomeBannerInfo(long languageid)
        {
            return ActivitiesBannerBL.GetHomeBannerInfo(languageid);
        }
        #endregion
    }
}
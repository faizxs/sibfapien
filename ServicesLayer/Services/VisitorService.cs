﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class VisitorService : IDisposable
    {
        #region Feilds
        VisitorBL VisitorBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return VisitorBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return VisitorBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public VisitorService()
        {
            VisitorBL = new VisitorBL();
        }
        public VisitorService(string connectionString)
        {
            VisitorBL = new VisitorBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.VisitorBL != null)
                this.VisitorBL.Dispose();
        }
        #endregion
        #region Methods
        //public long GetNextGroupId()
        //{
        //    return VisitorBL.GetNextGroupId();
        //}
        public XsiVisitor GetVisitorByItemId(long itemId)
        {
            return VisitorBL.GetVisitorByItemId(itemId);
        }
        //public List<XsiVisitor> GetVisitorByGroupId(long groupId)
        //{
        //    return VisitorBL.GetVisitorByGroupId(groupId);
        //}

        public List<XsiVisitor> GetVisitor()
        {
            return VisitorBL.GetVisitor();
        }
        public List<XsiVisitor> GetVisitor(EnumSortlistBy sortListBy)
        {
            return VisitorBL.GetVisitor(sortListBy);
        }
        public List<XsiVisitor> GetVisitor(XsiVisitor entity)
        {
            return VisitorBL.GetVisitor(entity);
        }
        public List<XsiVisitor> GetVisitor(XsiVisitor entity, EnumSortlistBy sortListBy)
        {
            return VisitorBL.GetVisitor(entity, sortListBy);
        }
        public List<XsiVisitor> GetVisitorOr(XsiVisitor entity, EnumSortlistBy sortListBy)
        {
            return VisitorBL.GetVisitorOr(entity, sortListBy);
        }
        //public XsiVisitor GetVisitor(long groupId, long languageId)
        //{
        //    return VisitorBL.GetVisitor(groupId, languageId);
        //}

        public EnumResultType InsertVisitor(XsiVisitor entity)
        {
            return VisitorBL.InsertVisitor(entity);
        }
        public EnumResultType UpdateVisitor(XsiVisitor entity)
        {
            return VisitorBL.UpdateVisitor(entity);
        }
        public EnumResultType UpdateVisitorStatus(long itemId)
        {
            return VisitorBL.UpdateVisitorStatus(itemId);
        }
        public EnumResultType DeleteVisitor(XsiVisitor entity)
        {
            return VisitorBL.DeleteVisitor(entity);
        }
        public EnumResultType DeleteVisitor(long itemId)
        {
            return VisitorBL.DeleteVisitor(itemId);
        }
        #endregion
    }
}
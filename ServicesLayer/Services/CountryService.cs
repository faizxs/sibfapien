﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class CountryService : IDisposable
    {
        #region Feilds
        CountryBL XsiCountryBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return XsiCountryBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public CountryService()
        {
            XsiCountryBL = new CountryBL();
        }
        public CountryService(string connectionString)
        {
            XsiCountryBL = new CountryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiCountryBL != null)
                this.XsiCountryBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiCountry GetCountryByItemId(long itemId)
        {
            return XsiCountryBL.GetCountryByItemId(itemId);
        }

        public List<XsiCountry> GetCountry()
        {
            return XsiCountryBL.GetCountry();
        }
        public List<XsiCountry> GetCountry(EnumSortlistBy sortListBy)
        {
            return XsiCountryBL.GetCountry(sortListBy);
        }
        public List<XsiCountry> GetCountry(XsiCountry entity)
        {
            return XsiCountryBL.GetCountry(entity);
        }
        public List<XsiCountry> GetCountry(XsiCountry entity, EnumSortlistBy sortListBy)
        {
            return XsiCountryBL.GetCountry(entity, sortListBy);
        }

        public EnumResultType InsertCountry(XsiCountry entity)
        {
            return XsiCountryBL.InsertCountry(entity);
        }
        public EnumResultType UpdateCountry(XsiCountry entity)
        {
            return XsiCountryBL.UpdateCountry(entity);
        }
        public EnumResultType UpdateCountryStatus(long itemId)
        {
            return XsiCountryBL.UpdateCountryStatus(itemId);
        }
        public EnumResultType DeleteCountry(XsiCountry entity)
        {
            return XsiCountryBL.DeleteCountry(entity);
        }
        public EnumResultType DeleteCountry(long itemId)
        {
            return XsiCountryBL.DeleteCountry(itemId);
        }
        #endregion
    }
}
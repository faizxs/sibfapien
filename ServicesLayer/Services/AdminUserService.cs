﻿using System;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;
using System.Linq;

namespace Xsi.ServicesLayer
{
    public class AdminUserService : IDisposable
    {
        #region Feilds
        AdminUserBL XsiAdminUserBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return XsiAdminUserBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return XsiAdminUserBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public AdminUserService()
        {
            XsiAdminUserBL = new AdminUserBL();
        }
        public AdminUserService(string connectionString)
        {
            XsiAdminUserBL = new AdminUserBL(connectionString);
        }        
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiAdminUserBL != null)
                this.XsiAdminUserBL.Dispose();
        }
        #endregion
        #region Methods
        public bool IsLoginAllowed(string userName, string password)
        {
            return XsiAdminUserBL.IsValidAdmin(userName, password);
        }
        public bool IsSuperAdmin(long itemId)
        {
            return XsiAdminUserBL.IsSuperAdmin(itemId);
        }
        public bool IsAdmin(long itemId)
        {
            return XsiAdminUserBL.IsAdmin(itemId);
        }
        public XsiAdminUsers GetAdminUserByItemId(long itemId)
        {
            return XsiAdminUserBL.GetAdminUserByItemId(itemId);
        }
        public XsiAdminUsers GetAdminUserByUsername(string userName)
        {
            return XsiAdminUserBL.GetUserByUsername(userName);
        }

        public List<XsiAdminUsers> GetAdminUser(EnumSortlistBy sortListBy = EnumSortlistBy.ByItemIdDesc)
        {
            return XsiAdminUserBL.GetAdminUser(sortListBy);
        }
        public List<XsiAdminUsers> GetAdminUser(XsiAdminUsers user)
        {
            return XsiAdminUserBL.GetAdminUser(user);
        }

        public List<XsiAdminUsers> GetUsersAndRoles(XsiAdminUsers user)
        {
            return XsiAdminUserBL.GetUsersAndRoles(user);
        }
        public List<XsiAdminUsers> GetUsersAndLogs(XsiAdminUsers user)
        {
            return XsiAdminUserBL.GetUsersAndLogs(user);
        }
        public List<XsiAdminUsers> GetUsersAndPermissions(XsiAdminUsers user)
        {
            return XsiAdminUserBL.GetUsersAndPermissions(user);
        }

        public EnumResultType InsertAdminUser(XsiAdminUsers user)
        {
            return XsiAdminUserBL.InsertAdminUser(user);
        }
        public EnumResultType UpdateAdminUser(XsiAdminUsers user)
        {
            return XsiAdminUserBL.UpdateAdminUser(user);
        }
        public EnumResultType UpdateAdminUserStatus(long itemId)
        {
            return XsiAdminUserBL.UpdateAdminUserStatus(itemId);
        }
        public EnumResultType DeleteAdminUser(XsiAdminUsers user)
        {
            return XsiAdminUserBL.DeleteAdminUser(user);
        }
        public EnumResultType DeleteAdminUser(long itemId)
        {
            return XsiAdminUserBL.DeleteAdminUser(itemId);
        }
        #endregion
    }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PhotoService : IDisposable
    {
        #region Feilds
        PhotoBL PhotoBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PhotoBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return PhotoBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PhotoService()
        {
            PhotoBL = new PhotoBL();
        }
        public PhotoService(string connectionString)
        {
            PhotoBL = new PhotoBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PhotoBL != null)
                this.PhotoBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextSortIndex()
        {
            return PhotoBL.GetNextSortIndex();
        }
        public XsiPhoto GetPhotoByItemId(long itemId)
        {
            return PhotoBL.GetPhotoByItemId(itemId);
        }
        public List<XsiPhoto> GetPhoto()
        {
            return PhotoBL.GetPhoto();
        }
        public List<XsiPhoto> GetPhoto(EnumSortlistBy sortListBy)
        {
            return PhotoBL.GetPhoto(sortListBy);
        }
        public List<XsiPhoto> GetPhoto(XsiPhoto entity)
        {
            return PhotoBL.GetPhoto(entity);
        }
        public List<XsiPhoto> GetPhoto(XsiPhoto entity, EnumSortlistBy sortListBy)
        {
            return PhotoBL.GetPhoto(entity, sortListBy);
        }

        public EnumResultType InsertPhoto(XsiPhoto entity)
        {
            return PhotoBL.InsertPhoto(entity);
        }
        public EnumResultType UpdatePhoto(XsiPhoto entity)
        {
            return PhotoBL.UpdatePhoto(entity);
        }
        public EnumResultType UpdatePhotoStatus(long itemId, long langId)
        {
            return PhotoBL.UpdatePhotoStatus(itemId, langId);
        }

        public EnumResultType UpdateAlbumCover(long itemId)
        {
            return PhotoBL.UpdateAlbumCover(itemId);
        }
        public EnumResultType DeletePhoto(XsiPhoto entity)
        {
            return PhotoBL.DeletePhoto(entity);
        }
        public EnumResultType DeletePhoto(long itemId)
        {
            return PhotoBL.DeletePhoto(itemId);
        }
        #endregion
    }
}
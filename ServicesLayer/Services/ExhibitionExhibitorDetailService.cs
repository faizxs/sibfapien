﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionExhibitorDetailService : IDisposable
    {
        #region Feilds
        ExhibitionExhibitorDetailBL ExhibitionExhibitorDetailBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionExhibitorDetailBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionExhibitorDetailBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionExhibitorDetailService()
        {
            ExhibitionExhibitorDetailBL = new ExhibitionExhibitorDetailBL();
        }
        public ExhibitionExhibitorDetailService(string connectionString)
        {
            ExhibitionExhibitorDetailBL = new ExhibitionExhibitorDetailBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionExhibitorDetailBL != null)
                this.ExhibitionExhibitorDetailBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionExhibitorDetails GetExhibitorRegistrationDetailsByItemId(long itemId)
        {
            return ExhibitionExhibitorDetailBL.GetExhibitorRegistrationDetailsByItemId(itemId);
        }

        public List<XsiExhibitionExhibitorDetails> GetExhibitorRegistrationDetails()
        {
            return ExhibitionExhibitorDetailBL.GetExhibitorRegistrationDetails();
        }
        public List<XsiExhibitionExhibitorDetails> GetExhibitorRegistrationDetails(EnumSortlistBy sortListBy)
        {
            return ExhibitionExhibitorDetailBL.GetExhibitorRegistrationDetails(sortListBy);
        }
        public List<XsiExhibitionExhibitorDetails> GetExhibitorRegistrationDetails(XsiExhibitionExhibitorDetails entity)
        {
            return ExhibitionExhibitorDetailBL.GetExhibitorRegistrationDetails(entity);
        }
        public List<XsiExhibitionExhibitorDetails> GetExhibitorRegistrationDetails(XsiExhibitionExhibitorDetails entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionExhibitorDetailBL.GetExhibitorRegistrationDetails(entity, sortListBy);
        }


        public EnumResultType InsertExhibitorRegistration(XsiExhibitionExhibitorDetails entity)
        {
            return ExhibitionExhibitorDetailBL.InsertExhibitorRegistration(entity);
        }
        public EnumResultType UpdateExhibitorRegistration(XsiExhibitionExhibitorDetails entity)
        {
            return ExhibitionExhibitorDetailBL.UpdateExhibitorRegistration(entity);
        }

        public EnumResultType DeleteExhibitorRegistration(XsiExhibitionExhibitorDetails entity)
        {
            return ExhibitionExhibitorDetailBL.DeleteExhibitorRegistration(entity);
        }
        public EnumResultType DeleteExhibitorDetailsRegistration(long ExhibitorDetailsId)
        {
            return ExhibitionExhibitorDetailBL.DeleteExhibitorDetailsRegistration(ExhibitorDetailsId);
        }
        #endregion
    }
}
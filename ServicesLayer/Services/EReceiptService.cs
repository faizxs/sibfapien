﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class EReceiptService : IDisposable
    {
        #region Feilds
        EReceiptBL EReceiptBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return EReceiptBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return EReceiptBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public EReceiptService()
        {
            EReceiptBL = new EReceiptBL();
        }
        public EReceiptService(string connectionString)
        {
            EReceiptBL = new EReceiptBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EReceiptBL != null)
                this.EReceiptBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return EReceiptBL.GetNextGroupId();
        }
        public XsiEreceipt GetEReceiptByItemId(long itemId)
        {
            return EReceiptBL.GetEReceiptByItemId(itemId);
        }

        public List<XsiEreceipt> GetEReceipt()
        {
            return EReceiptBL.GetEReceipt();
        }
        public List<XsiEreceipt> GetEReceipt(EnumSortlistBy sortListBy)
        {
            return EReceiptBL.GetEReceipt(sortListBy);
        }
        public List<XsiEreceipt> GetEReceipt(XsiEreceipt entity)
        {
            return EReceiptBL.GetEReceipt(entity);
        }
        public List<XsiEreceipt> GetEReceipt(XsiEreceipt entity, EnumSortlistBy sortListBy)
        {
            return EReceiptBL.GetEReceipt(entity, sortListBy);
        }
        public XsiEreceipt GetEReceipt(long groupId, long languageId)
        {
            return EReceiptBL.GetEReceipt(groupId, languageId);
        }

        public EnumResultType InsertEReceipt(XsiEreceipt entity)
        {
            return EReceiptBL.InsertEReceipt(entity);
        }
        public EnumResultType UpdateEReceipt(XsiEreceipt entity)
        {
            return EReceiptBL.UpdateEReceipt(entity);
        }
        public EnumResultType UpdateEReceiptStatus(long itemId)
        {
            return EReceiptBL.UpdateEReceiptStatus(itemId);
        }
        public EnumResultType DeleteEReceipt(XsiEreceipt entity)
        {
            return EReceiptBL.DeleteEReceipt(entity);
        }
        public EnumResultType DeleteEReceipt(long itemId)
        {
            return EReceiptBL.DeleteEReceipt(itemId);
        }
        #endregion
    }
}
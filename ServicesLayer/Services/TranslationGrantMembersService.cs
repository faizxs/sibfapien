﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class TranslationGrantMemberService : IDisposable
    {
        #region Feilds
        TranslationGrantMemberBL TranslationGrantMemberBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return TranslationGrantMemberBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return TranslationGrantMemberBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public TranslationGrantMemberService()
        {
            TranslationGrantMemberBL = new TranslationGrantMemberBL();
        }
        public TranslationGrantMemberService(string connectionString)
        {
            TranslationGrantMemberBL = new TranslationGrantMemberBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.TranslationGrantMemberBL != null)
                this.TranslationGrantMemberBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionTranslationGrantMembers GetTranslationGrantMemberByItemId(long itemId)
        {
            return TranslationGrantMemberBL.GetTranslationGrantMemberByItemId(itemId);
        }

        public List<XsiExhibitionTranslationGrantMembers> GetTranslationGrantMember()
        {
            return TranslationGrantMemberBL.GetTranslationGrantMember();
        }
        public List<XsiExhibitionTranslationGrantMembers> GetTranslationGrantMember(EnumSortlistBy sortListBy)
        {
            return TranslationGrantMemberBL.GetTranslationGrantMember(sortListBy);
        }
        public List<XsiExhibitionTranslationGrantMembers> GetTranslationGrantMember(XsiExhibitionTranslationGrantMembers entity)
        {
            return TranslationGrantMemberBL.GetTranslationGrantMember(entity);
        }
        public List<XsiExhibitionTranslationGrantMembers> GetTranslationGrantMember(XsiExhibitionTranslationGrantMembers entity, EnumSortlistBy sortListBy)
        {
            return TranslationGrantMemberBL.GetTranslationGrantMember(entity, sortListBy);
        }

        public EnumResultType InsertTranslationGrantMember(XsiExhibitionTranslationGrantMembers entity)
        {
            return TranslationGrantMemberBL.InsertTranslationGrantMember(entity);
        }
        public EnumResultType UpdateTranslationGrantMember(XsiExhibitionTranslationGrantMembers entity)
        {
            return TranslationGrantMemberBL.UpdateTranslationGrantMember(entity);
        }
        public EnumResultType UpdateTranslationGrantMemberStatus(long itemId)
        {
            return TranslationGrantMemberBL.UpdateTranslationGrantMemberStatus(itemId);
        }
        public EnumResultType DeleteTranslationGrantMember(XsiExhibitionTranslationGrantMembers entity)
        {
            return TranslationGrantMemberBL.DeleteTranslationGrantMember(entity);
        }
        public EnumResultType DeleteTranslationGrantMember(long itemId)
        {
            return TranslationGrantMemberBL.DeleteTranslationGrantMember(itemId);
        }
        #endregion
    }
}
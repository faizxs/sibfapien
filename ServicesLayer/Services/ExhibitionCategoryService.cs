﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionCategoryService : IDisposable
    {
        #region Feilds
        ExhibitionCategoryBL ExhibitionCategoryBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return ExhibitionCategoryBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public ExhibitionCategoryService()
        {
            ExhibitionCategoryBL = new ExhibitionCategoryBL();
        }
        public ExhibitionCategoryService(string connectionString)
        {
            ExhibitionCategoryBL = new ExhibitionCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionCategoryBL != null)
                this.ExhibitionCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionCategory GetExhibitionCategoryByItemId(long itemId)
        {
            return ExhibitionCategoryBL.GetExhibitionCategoryByItemId(itemId);
        }

        public List<XsiExhibitionCategory> GetExhibitionCategory()
        {
            return ExhibitionCategoryBL.GetExhibitionCategory();
        }
        public List<XsiExhibitionCategory> GetExhibitionCategory(EnumSortlistBy sortListBy)
        {
            return ExhibitionCategoryBL.GetExhibitionCategory(sortListBy);
        }
        public List<XsiExhibitionCategory> GetExhibitionCategory(XsiExhibitionCategory entity)
        {
            return ExhibitionCategoryBL.GetExhibitionCategory(entity);
        }
        public List<XsiExhibitionCategory> GetExhibitionCategory(XsiExhibitionCategory entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionCategoryBL.GetExhibitionCategory(entity, sortListBy);
        }
        public List<XsiExhibitionCategory> GetExhibitionCategoryOr(XsiExhibitionCategory entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionCategoryBL.GetExhibitionCategoryOr(entity, sortListBy);
        }

        public EnumResultType InsertExhibitionCategory(XsiExhibitionCategory entity)
        {
            return ExhibitionCategoryBL.InsertExhibitionCategory(entity);
        }
        public EnumResultType UpdateExhibitionCategory(XsiExhibitionCategory entity)
        {
            return ExhibitionCategoryBL.UpdateExhibitionCategory(entity);
        }
        public EnumResultType UpdateExhibitionCategoryStatus(long itemId, long langId)
        {
            return ExhibitionCategoryBL.UpdateExhibitionCategoryStatus(itemId, langId);
        }
        public EnumResultType DeleteExhibitionCategory(XsiExhibitionCategory entity)
        {
            return ExhibitionCategoryBL.DeleteExhibitionCategory(entity);
        }
        public EnumResultType DeleteExhibitionCategory(long itemId)
        {
            return ExhibitionCategoryBL.DeleteExhibitionCategory(itemId);
        }
        #endregion
    }
}
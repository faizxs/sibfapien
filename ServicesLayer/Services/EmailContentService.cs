﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class EmailContentService : IDisposable
    {
        #region Feilds
        EmailContentBL EmailContentBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return EmailContentBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return EmailContentBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public EmailContentService()
        {
            EmailContentBL = new EmailContentBL();
        }
        public EmailContentService(string connectionString)
        {
            EmailContentBL = new EmailContentBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EmailContentBL != null)
                this.EmailContentBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiEmailContent GetEmailContentByItemId(long itemId)
        {
            return EmailContentBL.GetEmailContentByItemId(itemId);
        }
        
        public List<XsiEmailContent> GetEmailContent()
        {
            return EmailContentBL.GetEmailContent();
        }
        public List<XsiEmailContent> GetEmailContent(EnumSortlistBy sortListBy)
        {
            return EmailContentBL.GetEmailContent(sortListBy);
        }
        public List<XsiEmailContent> GetEmailContent(XsiEmailContent entity)
        {
            return EmailContentBL.GetEmailContent(entity);
        }
        public List<XsiEmailContent> GetEmailContent(XsiEmailContent entity, EnumSortlistBy sortListBy)
        {
            return EmailContentBL.GetEmailContent(entity, sortListBy);
        }
        public List<XsiEmailContent> GetEmailContentOr(XsiEmailContent entity, EnumSortlistBy sortListBy)
        {
            return EmailContentBL.GetEmailContentOr(entity, sortListBy);
        }
        
        public EnumResultType InsertEmailContent(XsiEmailContent entity)
        {
            return EmailContentBL.InsertEmailContent(entity);
        }
        public EnumResultType UpdateEmailContent(XsiEmailContent entity)
        {
            return EmailContentBL.UpdateEmailContent(entity);
        }
        public EnumResultType RollbackEmailContent(long itemId)
        {
            return EmailContentBL.RollbackEmailContent(itemId);
        }
        public EnumResultType DeleteEmailContent(XsiEmailContent entity)
        {
            return EmailContentBL.DeleteEmailContent(entity);
        }
        public EnumResultType DeleteEmailContent(long itemId)
        {
            return EmailContentBL.DeleteEmailContent(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class EventService : IDisposable
    {
        #region Feilds
        EventBL EventBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return EventBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return EventBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public EventService()
        {
            EventBL = new EventBL();
        }
        public EventService(string connectionString)
        {
            EventBL = new EventBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventBL != null)
                this.EventBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiEvent GetEventByItemId(long itemId)
        {
            return EventBL.GetEventByItemId(itemId);
        }
         
        public List<XsiEvent> GetEvent()
        {
            return EventBL.GetEvent();
        }
        public List<XsiEvent> GetEvent(EnumSortlistBy sortListBy)
        {
            return EventBL.GetEvent(sortListBy);
        }
        public List<XsiEvent> GetEvent(XsiEvent entity)
        {
            return EventBL.GetEvent(entity);
        }
        public List<XsiEvent> GetEvent(XsiEvent entity, EnumSortlistBy sortListBy)
        {
            return EventBL.GetEvent(entity, sortListBy);
        }
        
        //public List<GetEvents_Result> GetEvent(long dayVal, long monthVal, long yearVal, string groupidsList, long languageId)
        //{
        //    return EventBL.GetEvent(dayVal, monthVal, yearVal, groupidsList, languageId);
        //}
        public EnumResultType InsertEvent(XsiEvent entity)
        {
            return EventBL.InsertEvent(entity);
        }
        public EnumResultType UpdateEvent(XsiEvent entity)
        {
            return EventBL.UpdateEvent(entity);
        }
        //public EnumResultType UpdatePublishorUnpublishAllByGroupId(long groupId, bool ispublish)
        //{
        //    return EventBL.UpdatePublishorUnpublishAllByGroupId(groupId, ispublish);
        //}
        public EnumResultType UpdateEventStatus(long itemId)
        {
            return EventBL.UpdateEventStatus(itemId);
        }
        public EnumResultType DeleteEvent(XsiEvent entity)
        {
            return EventBL.DeleteEvent(entity);
        }
        public EnumResultType DeleteEvent(long itemId)
        {
            return EventBL.DeleteEvent(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class AwardsService : IDisposable
    {
        #region Feilds
        AwardsBL AwardsBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return AwardsBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return AwardsBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public AwardsService()
        {
            AwardsBL = new AwardsBL();
        }
        public AwardsService(string connectionString)
        {
            AwardsBL = new AwardsBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AwardsBL != null)
                this.AwardsBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiAwards GetAwardsByItemId(long itemId)
        {
            return AwardsBL.GetAwardsByItemId(itemId);
        }
        
        public List<XsiAwards> GetAwards()
        {
            return AwardsBL.GetAwards();
        }
        public List<XsiAwards> GetAwards(EnumSortlistBy sortListBy)
        {
            return AwardsBL.GetAwards(sortListBy);
        }
        public List<XsiAwards> GetAwards(XsiAwards entity)
        {
            return AwardsBL.GetAwards(entity);
        }
        public List<XsiAwards> GetAwards(XsiAwards entity, EnumSortlistBy sortListBy)
        {
            return AwardsBL.GetAwards(entity, sortListBy);
        }
        public List<XsiAwards> GetAwardsOr(XsiAwards entity, EnumSortlistBy sortListBy)
        {
            return AwardsBL.GetAwardsOr(entity, sortListBy);
        }
        
        public List<XsiAwards> GetOtherLanguageAwards(XsiAwards entity)
        {
            return AwardsBL.GetOtherLanguageAwards(entity);
        }
        public List<XsiAwards> GetCurrentLanguageAwards(XsiAwards entity)
        {
            return AwardsBL.GetCurrentLanguageAwards(entity);
        }

        public EnumResultType InsertAwards(XsiAwards entity)
        {
            return AwardsBL.InsertAwards(entity);
        }
        public EnumResultType UpdateAwards(XsiAwards entity)
        {
            return AwardsBL.UpdateAwards(entity);
        }
        public EnumResultType UpdateAwardsStatus(long itemId)
        {
            return AwardsBL.UpdateAwardsStatus(itemId);
        }
        public EnumResultType DeleteAwards(XsiAwards entity)
        {
            return AwardsBL.DeleteAwards(entity);
        }
        public EnumResultType DeleteAwards(long itemId)
        {
            return AwardsBL.DeleteAwards(itemId);
        }
        #endregion
    }
}
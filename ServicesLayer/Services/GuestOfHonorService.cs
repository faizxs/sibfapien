﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class GuestOfHonorService : IDisposable
    {
        #region Feilds
        GuestOfHonorBL GuestOfHonorBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return GuestOfHonorBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return GuestOfHonorBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public GuestOfHonorService()
        {
            GuestOfHonorBL = new GuestOfHonorBL();
        }
        public GuestOfHonorService(string connectionString)
        {
            GuestOfHonorBL = new GuestOfHonorBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.GuestOfHonorBL != null)
                this.GuestOfHonorBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiGuestOfHonor GetGuestOfHonorByItemId(long itemId)
        {
            return GuestOfHonorBL.GetGuestOfHonorByItemId(itemId);
        }

        public List<XsiGuestOfHonor> GetGuestOfHonor()
        {
            return GuestOfHonorBL.GetGuestOfHonor();
        }
        public List<XsiGuestOfHonor> GetGuestOfHonor(EnumSortlistBy sortListBy)
        {
            return GuestOfHonorBL.GetGuestOfHonor(sortListBy);
        }
        public List<XsiGuestOfHonor> GetGuestOfHonor(XsiGuestOfHonor entity)
        {
            return GuestOfHonorBL.GetGuestOfHonor(entity);
        }
        public List<XsiGuestOfHonor> GetGuestOfHonor(XsiGuestOfHonor entity, EnumSortlistBy sortListBy)
        {
            return GuestOfHonorBL.GetGuestOfHonor(entity, sortListBy);
        }
        public List<XsiGuestOfHonor> GetGuestOfHonorOr(XsiGuestOfHonor entity, EnumSortlistBy sortListBy)
        {
            return GuestOfHonorBL.GetGuestOfHonorOr(entity, sortListBy);
        }

        public EnumResultType InsertGuestOfHonor(XsiGuestOfHonor entity)
        {
            return GuestOfHonorBL.InsertGuestOfHonor(entity);
        }
        public EnumResultType UpdateGuestOfHonor(XsiGuestOfHonor entity)
        {
            return GuestOfHonorBL.UpdateGuestOfHonor(entity);
        }
        public EnumResultType UpdateGuestOfHonorStatus(long itemId)
        {
            return GuestOfHonorBL.UpdateGuestOfHonorStatus(itemId);
        }
        public EnumResultType DeleteGuestOfHonor(XsiGuestOfHonor entity)
        {
            return GuestOfHonorBL.DeleteGuestOfHonor(entity);
        }
        public EnumResultType DeleteGuestOfHonor(long itemId)
        {
            return GuestOfHonorBL.DeleteGuestOfHonor(itemId);
        }
        #endregion
    }
}
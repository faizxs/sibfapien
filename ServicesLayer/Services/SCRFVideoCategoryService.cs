﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFVideoCategoryService : IDisposable
    {
        #region Feilds
        SCRFVideoCategoryBL SCRFVideoCategoryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFVideoCategoryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFVideoCategoryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFVideoCategoryService()
        {
            SCRFVideoCategoryBL = new SCRFVideoCategoryBL();
        }
        public SCRFVideoCategoryService(string connectionString)
        {
            SCRFVideoCategoryBL = new SCRFVideoCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFVideoCategoryBL != null)
                this.SCRFVideoCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiScrfvideoCategory GetSCRFVideoCategoryByItemId(long itemId)
        {
            return SCRFVideoCategoryBL.GetSCRFVideoCategoryByItemId(itemId);
        }
         
        public List<XsiScrfvideoCategory> GetSCRFVideoCategory()
        {
            return SCRFVideoCategoryBL.GetSCRFVideoCategory();
        }
        public List<XsiScrfvideoCategory> GetSCRFVideoCategory(EnumSortlistBy sortListBy)
        {
            return SCRFVideoCategoryBL.GetSCRFVideoCategory(sortListBy);
        }
        public List<XsiScrfvideoCategory> GetSCRFVideoCategory(XsiScrfvideoCategory entity)
        {
            return SCRFVideoCategoryBL.GetSCRFVideoCategory(entity);
        }
        public List<XsiScrfvideoCategory> GetSCRFVideoCategory(XsiScrfvideoCategory entity, EnumSortlistBy sortListBy)
        {
            return SCRFVideoCategoryBL.GetSCRFVideoCategory(entity, sortListBy);
        }
        public List<XsiScrfvideoCategory> GetSCRFVideoCategoryOr(XsiScrfvideoCategory entity, EnumSortlistBy sortListBy)
        {
            return SCRFVideoCategoryBL.GetSCRFVideoCategoryOr(entity, sortListBy);
        }
         
        public EnumResultType InsertSCRFVideoCategory(XsiScrfvideoCategory entity)
        {
            return SCRFVideoCategoryBL.InsertSCRFVideoCategory(entity);
        }
        public EnumResultType UpdateSCRFVideoCategory(XsiScrfvideoCategory entity)
        {
            return SCRFVideoCategoryBL.UpdateSCRFVideoCategory(entity);
        }
        public EnumResultType UpdateSCRFVideoCategoryStatus(long itemId)
        {
            return SCRFVideoCategoryBL.UpdateSCRFVideoCategoryStatus(itemId);
        }
        public EnumResultType DeleteSCRFVideoCategory(XsiScrfvideoCategory entity)
        {
            return SCRFVideoCategoryBL.DeleteSCRFVideoCategory(entity);
        }
        public EnumResultType DeleteSCRFVideoCategory(long itemId)
        {
            return SCRFVideoCategoryBL.DeleteSCRFVideoCategory(itemId);
        }
        #endregion
    }
}
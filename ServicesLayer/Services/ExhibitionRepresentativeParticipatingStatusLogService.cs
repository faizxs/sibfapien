﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionRepresentativeParticipatingStatusLogsService : IDisposable
    {
        #region Fields
        ExhibitionRepresentativeParticpatingStatusLogBL ExhibitionRepresentativeParticpatingStatusLogBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionRepresentativeParticpatingStatusLogBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionRepresentativeParticpatingStatusLogBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionRepresentativeParticipatingStatusLogsService()
        {
            ExhibitionRepresentativeParticpatingStatusLogBL = new ExhibitionRepresentativeParticpatingStatusLogBL();
        }
        public ExhibitionRepresentativeParticipatingStatusLogsService(string connectionString)
        {
            ExhibitionRepresentativeParticpatingStatusLogBL = new ExhibitionRepresentativeParticpatingStatusLogBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionRepresentativeParticpatingStatusLogBL != null)
                this.ExhibitionRepresentativeParticpatingStatusLogBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ExhibitionRepresentativeParticpatingStatusLogBL.GetNextGroupId();
        }
        public XsiExhibitionRepresentativeParticipatingStatusLogs GetExhibitionRepresentativeParticipatingStatusLogsByItemId(long itemId)
        {
            return ExhibitionRepresentativeParticpatingStatusLogBL.GetExhibitionRepresentativeParticpatingStatusLogByItemId(itemId);
        }
        

        public List<XsiExhibitionRepresentativeParticipatingStatusLogs> GetExhibitionRepresentativeParticipatingStatusLogs()
        {
            return ExhibitionRepresentativeParticpatingStatusLogBL.GetExhibitionRepresentativeParticpatingStatusLog();
        }
        public List<XsiExhibitionRepresentativeParticipatingStatusLogs> GetExhibitionRepresentativeParticipatingStatusLogs(EnumSortlistBy sortListBy)
        {
            return ExhibitionRepresentativeParticpatingStatusLogBL.GetExhibitionRepresentativeParticpatingStatusLog(sortListBy);
        }
        public List<XsiExhibitionRepresentativeParticipatingStatusLogs> GetExhibitionRepresentativeParticipatingStatusLogs(XsiExhibitionRepresentativeParticipatingStatusLogs entity)
        {
            return ExhibitionRepresentativeParticpatingStatusLogBL.GetExhibitionRepresentativeParticpatingStatusLog(entity);
        }
        public List<XsiExhibitionRepresentativeParticipatingStatusLogs> GetExhibitionRepresentativeParticipatingStatusLogs(XsiExhibitionRepresentativeParticipatingStatusLogs entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionRepresentativeParticpatingStatusLogBL.GetExhibitionRepresentativeParticpatingStatusLog(entity, sortListBy);
        }
        public XsiExhibitionRepresentativeParticipatingStatusLogs GetExhibitionRepresentativeParticipatingStatusLogs(long groupId, long languageId)
        {
            return ExhibitionRepresentativeParticpatingStatusLogBL.GetExhibitionRepresentativeParticpatingStatusLog(groupId, languageId);
        }

        public EnumResultType InsertExhibitionRepresentativeParticipatingStatusLogs(XsiExhibitionRepresentativeParticipatingStatusLogs entity)
        {
            return ExhibitionRepresentativeParticpatingStatusLogBL.InsertExhibitionRepresentativeParticpatingStatusLog(entity);
        }
        public EnumResultType UpdateExhibitionRepresentativeParticipatingStatusLogs(XsiExhibitionRepresentativeParticipatingStatusLogs entity)
        {
            return ExhibitionRepresentativeParticpatingStatusLogBL.UpdateExhibitionRepresentativeParticpatingStatusLog(entity);
        }
        public EnumResultType UpdateExhibitionRepresentativeParticipatingStatusLogsStatus(long itemId)
        {
            return ExhibitionRepresentativeParticpatingStatusLogBL.UpdateExhibitionRepresentativeParticpatingStatusLogStatus(itemId);
        }
        public EnumResultType DeleteExhibitionRepresentativeParticipatingStatusLogs(XsiExhibitionRepresentativeParticipatingStatusLogs entity)
        {
            return ExhibitionRepresentativeParticpatingStatusLogBL.DeleteExhibitionRepresentativeParticpatingStatusLog(entity);
        }
        public EnumResultType DeleteExhibitionRepresentativeParticipatingStatusLogs(long itemId)
        {
            return ExhibitionRepresentativeParticpatingStatusLogBL.DeleteExhibitionRepresentativeParticpatingStatusLog(itemId);
        }
        #endregion
    }
}
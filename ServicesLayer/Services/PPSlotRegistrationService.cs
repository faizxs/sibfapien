﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PPSlotRegistrationService : IDisposable
    {
        #region Feilds
        PPSlotRegistrationBL PPSlotRegistrationBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PPSlotRegistrationBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return PPSlotRegistrationBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PPSlotRegistrationService()
        {
            PPSlotRegistrationBL = new PPSlotRegistrationBL();
        }
        public PPSlotRegistrationService(string connectionString)
        {
            PPSlotRegistrationBL = new PPSlotRegistrationBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PPSlotRegistrationBL != null)
                this.PPSlotRegistrationBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionPpslotInvitation GetPPSlotRegistrationByItemId(long itemId)
        {
            return PPSlotRegistrationBL.GetPPSlotRegistrationByItemId(itemId);
        }

        public List<XsiExhibitionPpslotInvitation> GetPPSlotRegistration()
        {
            return PPSlotRegistrationBL.GetPPSlotRegistration();
        }
        public List<XsiExhibitionPpslotInvitation> GetPPSlotRegistration(EnumSortlistBy sortListBy)
        {
            return PPSlotRegistrationBL.GetPPSlotRegistration(sortListBy);
        }
        public List<XsiExhibitionPpslotInvitation> GetPPSlotRegistration(XsiExhibitionPpslotInvitation entity)
        {
            return PPSlotRegistrationBL.GetPPSlotRegistration(entity);
        }
        public List<XsiExhibitionPpslotInvitation> GetPPSlotRegistration(XsiExhibitionPpslotInvitation entity, EnumSortlistBy sortListBy)
        {
            return PPSlotRegistrationBL.GetPPSlotRegistration(entity, sortListBy);
        }
        public XsiExhibitionPpslotInvitation GetPPSlotRegistration(long groupId, long languageId)
        {
            return PPSlotRegistrationBL.GetPPSlotRegistration(groupId, languageId);
        }

        public EnumResultType InsertPPSlotRegistration(XsiExhibitionPpslotInvitation entity)
        {
            return PPSlotRegistrationBL.InsertPPSlotRegistration(entity);
        }
        public EnumResultType UpdatePPSlotRegistration(XsiExhibitionPpslotInvitation entity)
        {
            return PPSlotRegistrationBL.UpdatePPSlotRegistration(entity);
        }
        public EnumResultType RollbackPPSlotRegistration(long itemId)
        {
            return PPSlotRegistrationBL.RollbackPPSlotRegistration(itemId);
        }
        //public EnumResultType UpdatePPSlotRegistrationStatus(long itemId)
        //{
        //    return PPSlotRegistrationBL.UpdatePPSlotRegistrationStatus(itemId);
        //}
        public EnumResultType DeletePPSlotRegistration(XsiExhibitionPpslotInvitation entity)
        {
            return PPSlotRegistrationBL.DeletePPSlotRegistration(entity);
        }
        public EnumResultType DeletePPSlotRegistration(long itemId)
        {
            return PPSlotRegistrationBL.DeletePPSlotRegistration(itemId);
        }
        #endregion
    }
}
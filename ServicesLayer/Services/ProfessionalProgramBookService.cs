﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ProfessionalProgramBookService : IDisposable
    {
        #region Feilds
        ProfessionalProgramBookBL ProfessionalProgramBookBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ProfessionalProgramBookBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ProfessionalProgramBookBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ProfessionalProgramBookService()
        {
            ProfessionalProgramBookBL = new ProfessionalProgramBookBL();
        }
        public ProfessionalProgramBookService(string connectionString)
        {
            ProfessionalProgramBookBL = new ProfessionalProgramBookBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ProfessionalProgramBookBL != null)
                this.ProfessionalProgramBookBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ProfessionalProgramBookBL.GetNextGroupId();
        }
        public XsiExhibitionProfessionalProgramBook GetProfessionalProgramBookByItemId(long itemId)
        {
            return ProfessionalProgramBookBL.GetProfessionalProgramBookByItemId(itemId);
        }

        public List<XsiExhibitionProfessionalProgramBook> GetProfessionalProgramBook()
        {
            return ProfessionalProgramBookBL.GetProfessionalProgramBook();
        }
        public List<XsiExhibitionProfessionalProgramBook> GetProfessionalProgramBook(EnumSortlistBy sortListBy)
        {
            return ProfessionalProgramBookBL.GetProfessionalProgramBook(sortListBy);
        }
        public List<XsiExhibitionProfessionalProgramBook> GetProfessionalProgramBook(XsiExhibitionProfessionalProgramBook entity)
        {
            return ProfessionalProgramBookBL.GetProfessionalProgramBook(entity);
        }
        public List<XsiExhibitionProfessionalProgramBook> GetProfessionalProgramBook(XsiExhibitionProfessionalProgramBook entity, EnumSortlistBy sortListBy)
        {
            return ProfessionalProgramBookBL.GetProfessionalProgramBook(entity, sortListBy);
        }
        public XsiExhibitionProfessionalProgramBook GetProfessionalProgramBook(long groupId, long languageId)
        {
            return ProfessionalProgramBookBL.GetProfessionalProgramBook(groupId, languageId);
        }

        public EnumResultType InsertProfessionalProgramBook(XsiExhibitionProfessionalProgramBook entity)
        {
            return ProfessionalProgramBookBL.InsertProfessionalProgramBook(entity);
        }
        public EnumResultType UpdateProfessionalProgramBook(XsiExhibitionProfessionalProgramBook entity)
        {
            return ProfessionalProgramBookBL.UpdateProfessionalProgramBook(entity);
        }
        public EnumResultType UpdateProfessionalProgramBookStatus(long itemId)
        {
            return ProfessionalProgramBookBL.UpdateProfessionalProgramBookStatus(itemId);
        }
        public EnumResultType DeleteProfessionalProgramBook(XsiExhibitionProfessionalProgramBook entity)
        {
            return ProfessionalProgramBookBL.DeleteProfessionalProgramBook(entity);
        }
        public EnumResultType DeleteProfessionalProgramBook(long itemId)
        {
            return ProfessionalProgramBookBL.DeleteProfessionalProgramBook(itemId);
        }
        #endregion
    }
}
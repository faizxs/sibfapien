﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class EventDateService : IDisposable
    {
        #region Feilds
        EventDateBL EventDateBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return EventDateBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return EventDateBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public EventDateService()
        {
            EventDateBL = new EventDateBL();
        }
        public EventDateService(string connectionString)
        {
            EventDateBL = new EventDateBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventDateBL != null)
                this.EventDateBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return EventDateBL.GetNextGroupId();
        }
        public XsiEventDate GetEventDateByItemId(long itemId)
        {
            return EventDateBL.GetEventDateByItemId(itemId);
        }
        
        public List<XsiEventDate> GetEventDate()
        {
            return EventDateBL.GetEventDate();
        }
        public List<XsiEventDate> GetEventDate(EnumSortlistBy sortListBy)
        {
            return EventDateBL.GetEventDate(sortListBy);
        }
        public List<XsiEventDate> GetEventDate(XsiEventDate entity)
        {
            return EventDateBL.GetEventDate(entity);
        }
        public List<XsiEventDate> GetEventDate(XsiEventDate entity, EnumSortlistBy sortListBy)
        {
            return EventDateBL.GetEventDate(entity, sortListBy);
        }
        public XsiEventDate GetEventDate(long groupId, long languageId)
        {
            return EventDateBL.GetEventDate(groupId, languageId);
        }

        public EnumResultType InsertEventDate(XsiEventDate entity)
        {
            return EventDateBL.InsertEventDate(entity);
        }
        public EnumResultType UpdateEventDate(XsiEventDate entity)
        {
            return EventDateBL.UpdateEventDate(entity);
        }
        public EnumResultType UpdateEventDateStatus(long itemId)
        {
            return EventDateBL.UpdateEventDateStatus(itemId);
        }
        public EnumResultType DeleteEventDate(XsiEventDate entity)
        {
            return EventDateBL.DeleteEventDate(entity);
        }
        public EnumResultType DeleteEventDate(long itemId)
        {
            return EventDateBL.DeleteEventDate(itemId);
        }
        #endregion
    }
}
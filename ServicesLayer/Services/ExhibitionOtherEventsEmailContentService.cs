﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionOtherEventsEmailContentService : IDisposable
    {
        #region Feilds
        ExhibitionOtherEventsEmailContentBL ExhibitionOtherEventsEmailContentBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionOtherEventsEmailContentBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionOtherEventsEmailContentBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionOtherEventsEmailContentService()
        {
            ExhibitionOtherEventsEmailContentBL = new ExhibitionOtherEventsEmailContentBL();
        }
        public ExhibitionOtherEventsEmailContentService(string connectionString)
        {
            ExhibitionOtherEventsEmailContentBL = new ExhibitionOtherEventsEmailContentBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionOtherEventsEmailContentBL != null)
                this.ExhibitionOtherEventsEmailContentBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionOtherEventsEmailContent GetExhibitionOtherEventsEmailContentByItemId(long itemId)
        {
            return ExhibitionOtherEventsEmailContentBL.GetExhibitionOtherEventsEmailContentByItemId(itemId);
        }
        
        public List<XsiExhibitionOtherEventsEmailContent> GetExhibitionOtherEventsEmailContent()
        {
            return ExhibitionOtherEventsEmailContentBL.GetExhibitionOtherEventsEmailContent();
        }
        public List<XsiExhibitionOtherEventsEmailContent> GetExhibitionOtherEventsEmailContent(EnumSortlistBy sortListBy)
        {
            return ExhibitionOtherEventsEmailContentBL.GetExhibitionOtherEventsEmailContent(sortListBy);
        }
        public List<XsiExhibitionOtherEventsEmailContent> GetExhibitionOtherEventsEmailContent(XsiExhibitionOtherEventsEmailContent entity)
        {
            return ExhibitionOtherEventsEmailContentBL.GetExhibitionOtherEventsEmailContent(entity);
        }
        public List<XsiExhibitionOtherEventsEmailContent> GetExhibitionOtherEventsEmailContent(XsiExhibitionOtherEventsEmailContent entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionOtherEventsEmailContentBL.GetExhibitionOtherEventsEmailContent(entity, sortListBy);
        }
        public List<XsiExhibitionOtherEventsEmailContent> GetExhibitionOtherEventsEmailContentOr(XsiExhibitionOtherEventsEmailContent entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionOtherEventsEmailContentBL.GetExhibitionOtherEventsEmailContentOr(entity, sortListBy);
        }
        
        public EnumResultType InsertExhibitionOtherEventsEmailContent(XsiExhibitionOtherEventsEmailContent entity)
        {
            return ExhibitionOtherEventsEmailContentBL.InsertExhibitionOtherEventsEmailContent(entity);
        }
        public EnumResultType UpdateExhibitionOtherEventsEmailContent(XsiExhibitionOtherEventsEmailContent entity)
        {
            return ExhibitionOtherEventsEmailContentBL.UpdateExhibitionOtherEventsEmailContent(entity);
        }
        public EnumResultType RollbackExhibitionOtherEventsEmailContent(long itemId)
        {
            return ExhibitionOtherEventsEmailContentBL.RollbackExhibitionOtherEventsEmailContent(itemId);
        }
        public EnumResultType DeleteExhibitionOtherEventsEmailContent(XsiExhibitionOtherEventsEmailContent entity)
        {
            return ExhibitionOtherEventsEmailContentBL.DeleteExhibitionOtherEventsEmailContent(entity);
        }
        public EnumResultType DeleteExhibitionOtherEventsEmailContent(long itemId)
        {
            return ExhibitionOtherEventsEmailContentBL.DeleteExhibitionOtherEventsEmailContent(itemId);
        }
        #endregion
    }
}
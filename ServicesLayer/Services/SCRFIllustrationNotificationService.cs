﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFIllustrationNotificationService : IDisposable
    {
        #region Feilds
        SCRFIllustrationNotificationBL SCRFIllustrationNotificationBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFIllustrationNotificationBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFIllustrationNotificationBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFIllustrationNotificationService()
        {
            SCRFIllustrationNotificationBL = new SCRFIllustrationNotificationBL();
        }
        public SCRFIllustrationNotificationService(string connectionString)
        {
            SCRFIllustrationNotificationBL = new SCRFIllustrationNotificationBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIllustrationNotificationBL != null)
                this.SCRFIllustrationNotificationBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return SCRFIllustrationNotificationBL.GetNextGroupId();
        }
        public XsiScrfillustrationNotification GetSCRFIllustrationNotificationByItemId(long itemId)
        {
            return SCRFIllustrationNotificationBL.GetSCRFIllustrationNotificationByItemId(itemId);
        }

        public List<XsiScrfillustrationNotification> GetSCRFIllustrationNotification()
        {
            return SCRFIllustrationNotificationBL.GetSCRFIllustrationNotification();
        }
        public List<XsiScrfillustrationNotification> GetSCRFIllustrationNotification(EnumSortlistBy sortListBy)
        {
            return SCRFIllustrationNotificationBL.GetSCRFIllustrationNotification(sortListBy);
        }
        public List<XsiScrfillustrationNotification> GetSCRFIllustrationNotification(XsiScrfillustrationNotification entity)
        {
            return SCRFIllustrationNotificationBL.GetSCRFIllustrationNotification(entity);
        }
        public List<XsiScrfillustrationNotification> GetSCRFIllustrationNotification(XsiScrfillustrationNotification entity, EnumSortlistBy sortListBy)
        {
            return SCRFIllustrationNotificationBL.GetSCRFIllustrationNotification(entity, sortListBy);
        }
        public XsiScrfillustrationNotification GetSCRFIllustrationNotification(long groupId, long languageId)
        {
            return SCRFIllustrationNotificationBL.GetSCRFIllustrationNotification(groupId, languageId);
        }

        public EnumResultType InsertSCRFIllustrationNotification(XsiScrfillustrationNotification entity)
        {
            return SCRFIllustrationNotificationBL.InsertSCRFIllustrationNotification(entity);
        }
        public EnumResultType UpdateSCRFIllustrationNotification(XsiScrfillustrationNotification entity)
        {
            return SCRFIllustrationNotificationBL.UpdateSCRFIllustrationNotification(entity);
        }
       
        public EnumResultType DeleteSCRFIllustrationNotification(XsiScrfillustrationNotification entity)
        {
            return SCRFIllustrationNotificationBL.DeleteSCRFIllustrationNotification(entity);
        }
        public EnumResultType DeleteSCRFIllustrationNotification(long itemId)
        {
            return SCRFIllustrationNotificationBL.DeleteSCRFIllustrationNotification(itemId);
        }

        #endregion
    }
}
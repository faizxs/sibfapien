﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class InvoiceStatusLogService : IDisposable
    {
        #region Feilds
        InvoiceStatusLogBL InvoiceStatusLogBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return InvoiceStatusLogBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return InvoiceStatusLogBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public InvoiceStatusLogService()
        {
            InvoiceStatusLogBL = new InvoiceStatusLogBL();
        }
        public InvoiceStatusLogService(string connectionString)
        {
            InvoiceStatusLogBL = new InvoiceStatusLogBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.InvoiceStatusLogBL != null)
                this.InvoiceStatusLogBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return InvoiceStatusLogBL.GetNextGroupId();
        }
        public XsiInvoiceStatusLog GetInvoiceStatusLogByItemId(long itemId)
        {
            return InvoiceStatusLogBL.GetInvoiceStatusLogByItemId(itemId);
        }
         
        public List<XsiInvoiceStatusLog> GetInvoiceStatusLog()
        {
            return InvoiceStatusLogBL.GetInvoiceStatusLog();
        }
        public List<XsiInvoiceStatusLog> GetInvoiceStatusLog(EnumSortlistBy sortListBy)
        {
            return InvoiceStatusLogBL.GetInvoiceStatusLog(sortListBy);
        }
        public List<XsiInvoiceStatusLog> GetInvoiceStatusLog(XsiInvoiceStatusLog entity)
        {
            return InvoiceStatusLogBL.GetInvoiceStatusLog(entity);
        }
        public List<XsiInvoiceStatusLog> GetInvoiceStatusLog(XsiInvoiceStatusLog entity, EnumSortlistBy sortListBy)
        {
            return InvoiceStatusLogBL.GetInvoiceStatusLog(entity, sortListBy);
        }
        public XsiInvoiceStatusLog GetInvoiceStatusLog(long groupId, long languageId)
        {
            return InvoiceStatusLogBL.GetInvoiceStatusLog(groupId, languageId);
        }

        public EnumResultType InsertInvoiceStatusLog(XsiInvoiceStatusLog entity)
        {
            return InvoiceStatusLogBL.InsertInvoiceStatusLog(entity);
        }
        public EnumResultType UpdateInvoiceStatusLog(XsiInvoiceStatusLog entity)
        {
            return InvoiceStatusLogBL.UpdateInvoiceStatusLog(entity);
        }
        public EnumResultType UpdateInvoiceStatusLogStatus(long itemId)
        {
            return InvoiceStatusLogBL.UpdateInvoiceStatusLogStatus(itemId);
        }
        public EnumResultType DeleteInvoiceStatusLog(XsiInvoiceStatusLog entity)
        {
            return InvoiceStatusLogBL.DeleteInvoiceStatusLog(entity);
        }
        public EnumResultType DeleteInvoiceStatusLog(long itemId)
        {
            return InvoiceStatusLogBL.DeleteInvoiceStatusLog(itemId);
        }
        #endregion
    }
}
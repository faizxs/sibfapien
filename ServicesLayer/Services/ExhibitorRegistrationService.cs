﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitorRegistrationService : IDisposable
    {
        #region Feilds
        ExhibitorRegistrationBL ExhibitorRegistrationBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitorRegistrationBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitorRegistrationBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitorRegistrationService()
        {
            ExhibitorRegistrationBL = new ExhibitorRegistrationBL();
        }
        public ExhibitorRegistrationService(string connectionString)
        {
            ExhibitorRegistrationBL = new ExhibitorRegistrationBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitorRegistrationBL != null)
                this.ExhibitorRegistrationBL.Dispose();
        }
        #endregion
        #region Methods
        public List<GetExhibitorsAndCountriesSCRF_Result> GetExhibitorsForAutoComplete(string keyword, long langId, long exhibitionId)
        {
            return ExhibitorRegistrationBL.GetExhibitorsForAutoComplete(keyword, langId, exhibitionId);
        }
        public XsiExhibitionMemberApplicationYearly GetExhibitorRegistrationByItemId(long itemId)
        {
            return ExhibitorRegistrationBL.GetExhibitorRegistrationByItemId(itemId);
        }
        public XsiExhibitionMemberApplicationYearly GetExhibitorRegistrationByItemIdAll(long itemId)
        {
            return ExhibitorRegistrationBL.GetExhibitorRegistrationByItemIdAll(itemId);
        }

        public List<XsiExhibitionMemberApplicationYearly> GetExhibitorRegistration()
        {
            return ExhibitorRegistrationBL.GetExhibitorRegistration();
        }
        public List<XsiExhibitionMemberApplicationYearly> GetExhibitorRegistration(EnumSortlistBy sortListBy)
        {
            return ExhibitorRegistrationBL.GetExhibitorRegistration(sortListBy);
        }
        public List<XsiExhibitionMemberApplicationYearly> GetExhibitorRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            return ExhibitorRegistrationBL.GetExhibitorRegistration(entity);
        }
        public List<XsiExhibitionMemberApplicationYearly> GetExhibitorRegistration(XsiExhibitionMemberApplicationYearly entity, EnumSortlistBy sortListBy)
        {
            return ExhibitorRegistrationBL.GetExhibitorRegistration(entity, sortListBy);
        }
        public List<string> GetExhibitorRegistration(XsiExhibitionMemberApplicationYearly entity, List<long> exhibitionIDs)
        {
            return ExhibitorRegistrationBL.GetExhibitorRegistration(entity, exhibitionIDs);
        }
        public List<string> GetExhibitorRegistrationAr(XsiExhibitionMemberApplicationYearly entity, List<long> exhibitionIDs)
        {
            return ExhibitorRegistrationBL.GetExhibitorRegistrationAr(entity, exhibitionIDs);
        }
        /*public List<GetExhibitors_Agencies_Result> GetExhibitorAndAgency(string whereExhibitor, string whereAgency, string languageId)
        {
            return ExhibitorRegistrationBL.GetExhibitorAndAgency(whereExhibitor, whereAgency,languageId);
        }
        public List<GetExhibitors_Agencies_Result> GetExhibitorAndAgencyAnd(string whereExhibitor, string whereAgency, GetExhibitors_Agencies_Result entity, List<long?> categoryIDs, string languageId)
        {
            return ExhibitorRegistrationBL.GetExhibitorAndAgencyAnd(whereExhibitor, whereAgency, entity, categoryIDs,languageId);
        }
        public List<GetExhibitors_Agencies_Result> GetExhibitorAndAgencyOr(string whereExhibitor, string whereAgency, GetExhibitors_Agencies_Result entity, List<long?> categoryIDs, string languageId)
        {
            return ExhibitorRegistrationBL.GetExhibitorAndAgencyOr(whereExhibitor, whereAgency, entity, categoryIDs,languageId);
        }
        public List<GetExhibitors_AgenciesToExport_Result> GetExhibitorAndAgencyToExport(string exhibitorIds, string agencyIds, string languageId)
        {
            return ExhibitorRegistrationBL.GetExhibitorAndAgencyToExport(exhibitorIds, agencyIds, languageId);
        }
        public List<GetExhibitorsAndCountries_Result> GetExhibitorsAndCountries()
        {
            return ExhibitorRegistrationBL.GetExhibitorsAndCountries();
        }
        public List<GetExhibitorsAndCountriesSCRF_Result> GetExhibitorsAndCountriesSCRF()
        {
            return ExhibitorRegistrationBL.GetExhibitorsAndCountriesSCRF();
        }
        public long? GetRequestedAreaSum()
        {
            return ExhibitorRegistrationBL.GetRequestedAreaSum();
        }

        public List<GetExhibitorRegistrationList_Result> GetExhibitorList(string languageId) {
            return ExhibitorRegistrationBL.GetExhibitorList(languageId);
        }*/
        public EnumResultType InsertExhibitorRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            return ExhibitorRegistrationBL.InsertExhibitorRegistration(entity);
        }
        public EnumResultType UpdateExhibitorRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            return ExhibitorRegistrationBL.UpdateExhibitorRegistration(entity);
        }
        
        public EnumResultType UpdateExhibitorRegistrationStatus(long itemId)
        {
            return ExhibitorRegistrationBL.UpdateExhibitorRegistrationStatus(itemId);
        }
        public EnumResultType DeleteExhibitorRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            return ExhibitorRegistrationBL.DeleteExhibitorRegistration(entity);
        }
        public EnumResultType DeleteExhibitorRegistration(long itemId)
        {
            return ExhibitorRegistrationBL.DeleteExhibitorRegistration(itemId);
        }
        public XsiExhibitionExhibitorDetails GetDetailsById(long MemberExhibitionYearlyId)
        {
            return ExhibitorRegistrationBL.GetDetailsById(MemberExhibitionYearlyId);
        }
        public EnumResultType InsertExhibitorDetailsRegistration(XsiExhibitionExhibitorDetails entity)
        {
            return ExhibitorRegistrationBL.InsertExhibitorDetailRegistration(entity);
        }
        public EnumResultType UpdateExhibitorRegistrationDetails(XsiExhibitionExhibitorDetails entity)
        {
            return ExhibitorRegistrationBL.UpdateExhibitorRegistrationDetails(entity);
        }
        #endregion
    }
}
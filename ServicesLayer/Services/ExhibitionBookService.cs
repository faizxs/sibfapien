﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;
using System.Web;

namespace Xsi.ServicesLayer
{
    public class ExhibitionBookService : IDisposable
    {
        #region Feilds
        ExhibitionBookBL ExhibitionBookBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionBookBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionBookBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionBookService()
        {
            ExhibitionBookBL = new ExhibitionBookBL();
        }
        public ExhibitionBookService(string connectionString)
        {
            ExhibitionBookBL = new ExhibitionBookBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionBookBL != null)
                this.ExhibitionBookBL.Dispose();
        }
        #endregion
        #region Methods
       
        public long GetNextClickCount(long itemId)
        {
            return ExhibitionBookBL.GetNextClickCount(itemId);
        }
        public XsiExhibitionBooks GetBookByItemId(long itemId)
        {
            return ExhibitionBookBL.GetBookByItemId(itemId);
        }
          
        public List<XsiExhibitionBooks> GetBook()
        {
            return ExhibitionBookBL.GetBook();
        }
        public List<XsiExhibitionBooks> GetBook(EnumSortlistBy sortListBy)
        {
            return ExhibitionBookBL.GetBook(sortListBy);
        }
        public List<XsiExhibitionBooks> GetBook(XsiExhibitionBooks entity)
        {
            return ExhibitionBookBL.GetBook(entity);
        }
        public List<XsiExhibitionBookForUi> GetBookForUI(XsiExhibitionBooks entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBookBL.GetBookForUI(entity, sortListBy);
        }
        public List<XsiExhibitionBookForUi> GetBookUIAnd(XsiExhibitionBooks entity, EnumSortlistBy sortListBy, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            return ExhibitionBookBL.GetBookUIAnd(entity, sortListBy, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId);
        }
        public List<XsiExhibitionBookForUi> GetBookUIOr(XsiExhibitionBooks entity, EnumSortlistBy sortListBy, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            return ExhibitionBookBL.GetBookUIOr(entity, sortListBy, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId);
        }
        public List<XsiExhibitionBookForUi> GetBookUITopSearchable(XsiExhibitionBooks entity, long websiteId)
        {
            return ExhibitionBookBL.GetBookUITopSearchable(entity, websiteId);
        }
        public Array GetBooksTitle(XsiExhibitionBooks entity, long websiteId, EnumSortlistBy sortListBy)
        {
            return ExhibitionBookBL.GetBooksTitle(entity, websiteId, sortListBy);
        }
        public List<XsiExhibitionBookForUi> GetBooksSCRFTitle(XsiExhibitionBooks entity, long websiteId, EnumSortlistBy sortListBy)
        {
            return ExhibitionBookBL.GetBooksSCRFTitle(entity, websiteId, sortListBy);
        }
        public List<XsiExhibitionBooks> GetBook(XsiExhibitionBooks entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBookBL.GetBook(entity, sortListBy).ToList();
        }
        
        //public List<GetBooksToExport_Result> GetBooksToExport(string bookIds, string languageId)
        //{
        //    return ExhibitionBookBL.GetBooksToExport(bookIds, languageId).ToList();
        //}

        public EnumResultType InsertBook(XsiExhibitionBooks entity)
        {
            return ExhibitionBookBL.InsertBook(entity);
        }
        public EnumResultType UpdateBook(XsiExhibitionBooks entity)
        {
            return ExhibitionBookBL.UpdateBook(entity);
        }
        //public void UpdatePublisherId(long? oldPublisherID, long? newPublisherID)
        //{
        //    ExhibitionBookBL.UpdatePublisherId(oldPublisherID, newPublisherID);
        //}
        //public void UpdateAuthorId(long? oldAuthorID, long? newAuthorID)
        //{
        //    ExhibitionBookBL.UpdateAuthorId(oldAuthorID, newAuthorID);
        //}
        public EnumResultType UpdateBookStatus(long itemId)
        {
            return ExhibitionBookBL.UpdateBookStatus(itemId);
        }
        public EnumResultType DeleteBook(XsiExhibitionBooks entity)
        {
            return ExhibitionBookBL.DeleteBook(entity);
        }
        public EnumResultType DeleteBook(long itemId)
        {
            return ExhibitionBookBL.DeleteBook(itemId);
        }

        public List<XsiExhibitionBookForUi> GetBookUIAnd1(XsiExhibitionBooks entity, EnumSortlistBy sortListBy, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            return ExhibitionBookBL.GetBookUIAnd1(entity, sortListBy, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId);
        }

        //public void UpdateAuthorInBooks(long oldAuthorId, long newAuthorId, long AdminUserId)
        //{
        //    ExhibitionBookBL.UpdateAuthorInBooks(oldAuthorId, newAuthorId, AdminUserId);
        //}

        //public void UpdatePublisherInBooks(long oldPublisherId, long newPublisherId, long AdminUserId)
        //{
        //    ExhibitionBookBL.UpdatePublisherInBooks(oldPublisherId, newPublisherId, AdminUserId);
        //}
        public void InsertBookAuthor(XsiExhibitionBookAuthor entity)
        {
            ExhibitionBookBL.InsertBook(entity);
        }
        public void DeleteBookAuthor(long bookId)
        {
            ExhibitionBookBL.DeleteBookAuthor(bookId);
        }
        public void AddBooksParticipating(XsiExhibitionBookParticipating entity)
        {
            ExhibitionBookBL.AddBooksParticipating(entity);
        }
        public void UpdateBookParticipating(XsiExhibitionBookParticipating entity)
        {
            ExhibitionBookBL.UpdateBookParticipating(entity);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionService : IDisposable
    {
        #region Feilds
        ExhibitionBL ExhibitionBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionService()
        {
            ExhibitionBL = new ExhibitionBL();
        }
        public ExhibitionService(string connectionString)
        {
            ExhibitionBL = new ExhibitionBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionBL != null)
                this.ExhibitionBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibition GetExhibitionByItemId(long itemId)
        {
            return ExhibitionBL.GetExhibitionByItemId(itemId);
        }
        
        public List<XsiExhibition> GetExhibition()
        {
            return ExhibitionBL.GetExhibition();
        }
        public List<XsiExhibition> GetExhibition(EnumSortlistBy sortListBy)
        {
            return ExhibitionBL.GetExhibition(sortListBy);
        }
        public List<XsiExhibition> GetExhibition(XsiExhibition entity)
        {
            return ExhibitionBL.GetExhibition(entity);
        }
        public List<XsiExhibition> GetExhibition(XsiExhibition entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBL.GetExhibition(entity, sortListBy);
        }
         
        public EnumResultType InsertExhibition(XsiExhibition entity)
        {
            return ExhibitionBL.InsertExhibition(entity);
        }
        public EnumResultType UpdateExhibition(XsiExhibition entity)
        {
            return ExhibitionBL.UpdateExhibition(entity);
        }
        public EnumResultType UpdateExhibitionStatus(long itemId)
        {
            return ExhibitionBL.UpdateExhibitionStatus(itemId);
        }
        public EnumResultType UpdateExhibitionArchiveStatus(long itemId)
        {
            return ExhibitionBL.UpdateExhibitionArchiveStatus(itemId);
        }
        public EnumResultType DeleteExhibition(XsiExhibition entity)
        {
            return ExhibitionBL.DeleteExhibition(entity);
        }
        public EnumResultType DeleteExhibition(long itemId)
        {
            return ExhibitionBL.DeleteExhibition(itemId);
        }
        #endregion
    }
}
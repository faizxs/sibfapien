﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionCountryService : IDisposable
    {
        #region Feilds
        ExhibitionCountryBL ExhibitionCountryBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return ExhibitionCountryBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public ExhibitionCountryService()
        {
            ExhibitionCountryBL = new ExhibitionCountryBL();
        }
        public ExhibitionCountryService(string connectionString)
        {
            ExhibitionCountryBL = new ExhibitionCountryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionCountryBL != null)
                this.ExhibitionCountryBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionCountry GetExhibitionCountryByItemId(long itemId)
        {
            return ExhibitionCountryBL.GetExhibitionCountryByItemId(itemId);
        }
        public List<XsiExhibitionCountry> GetExhibitionCountry()
        {
            return ExhibitionCountryBL.GetExhibitionCountry();
        }
        public List<XsiExhibitionCountry> GetExhibitionCountry(EnumSortlistBy sortListBy)
        {
            return ExhibitionCountryBL.GetExhibitionCountry(sortListBy);
        }
        public List<XsiExhibitionCountry> GetExhibitionCountry(XsiExhibitionCountry entity)
        {
            return ExhibitionCountryBL.GetExhibitionCountry(entity);
        }
        public List<XsiExhibitionCountry> GetExhibitionCountry(XsiExhibitionCountry entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionCountryBL.GetExhibitionCountry(entity, sortListBy);
        }
        public List<XsiExhibitionCountry> GetExhibitionCountryOr(XsiExhibitionCountry entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionCountryBL.GetExhibitionCountryOr(entity, sortListBy);
        }
        public List<XsiExhibitionCountry> GetOtherLanguageExhibitionCountry(XsiExhibitionCountry entity)
        {
            return ExhibitionCountryBL.GetOtherLanguageExhibitionCountry(entity);
        }
        public List<XsiExhibitionCountry> GetCurrentLanguageExhibitionCountry(XsiExhibitionCountry entity)
        {
            return ExhibitionCountryBL.GetCurrentLanguageExhibitionCountry(entity);
        }
        public List<XsiExhibitionCountry> GetCompleteExhibitionCountry(XsiExhibitionCountry entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionCountryBL.GetCompleteExhibitionCountry(entity, sortListBy);
        }

        public EnumResultType InsertExhibitionCountry(XsiExhibitionCountry entity)
        {
            return ExhibitionCountryBL.InsertExhibitionCountry(entity);
        }
        public EnumResultType UpdateExhibitionCountry(XsiExhibitionCountry entity)
        {
            return ExhibitionCountryBL.UpdateExhibitionCountry(entity);
        }
        public EnumResultType UpdateExhibitionCountryStatus(long itemId)
        {
            return ExhibitionCountryBL.UpdateExhibitionCountryStatus(itemId);
        }
        public EnumResultType DeleteExhibitionCountry(XsiExhibitionCountry entity)
        {
            return ExhibitionCountryBL.DeleteExhibitionCountry(entity);
        }
        public EnumResultType DeleteExhibitionCountry(long itemId)
        {
            return ExhibitionCountryBL.DeleteExhibitionCountry(itemId);
        }
        #endregion
    }
}
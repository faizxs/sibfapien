﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionStaffGuestService : IDisposable
    {
        #region Feilds
        ExhibitionStaffGuestBL ExhibitionStaffGuestBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionStaffGuestBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionStaffGuestBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestService()
        {
            ExhibitionStaffGuestBL = new ExhibitionStaffGuestBL();
        }
        public ExhibitionStaffGuestService(string connectionString)
        {
            ExhibitionStaffGuestBL = new ExhibitionStaffGuestBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionStaffGuestBL != null)
                this.ExhibitionStaffGuestBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionStaffGuest GetStaffGuestByItemId(long itemId)
        {
            return ExhibitionStaffGuestBL.GetExhibitionStaffGuestByItemId(itemId);
        } 
        public List<XsiExhibitionStaffGuest> GetExhibitionStaffGuest()
        {
            return ExhibitionStaffGuestBL.GetExhibitionStaffGuest();
        }
        public List<XsiExhibitionStaffGuest> GetExhibitionStaffGuest(EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestBL.GetExhibitionStaffGuest(sortListBy);
        }
        public List<XsiExhibitionStaffGuest> GetExhibitionStaffGuest(XsiExhibitionStaffGuest entity)
        {
            return ExhibitionStaffGuestBL.GetExhibitionStaffGuest(entity);
        }
        public List<XsiExhibitionStaffGuest> GetExhibitionStaffGuest(XsiExhibitionStaffGuest entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestBL.GetExhibitionStaffGuest(entity, sortListBy);
        }
        public EnumResultType InsertExhibitionStaffGuest(XsiExhibitionStaffGuest entity)
        {
            return ExhibitionStaffGuestBL.InsertExhibitionStaffGuest(entity);
        }
        public EnumResultType UpdateExhibitionStaffGuest(XsiExhibitionStaffGuest entity)
        {
            return ExhibitionStaffGuestBL.UpdateExhibitionStaffGuest(entity);
        }
        public EnumResultType UpdateStaffGuestStatus(long itemId)
        {
            return ExhibitionStaffGuestBL.UpdateExhibitionStaffGuestStatus(itemId);
        }
        public EnumResultType DeleteExhibitionStaffGuest(XsiExhibitionStaffGuest entity)
        {
            return ExhibitionStaffGuestBL.DeleteExhibitionStaffGuest(entity);
        }
        public EnumResultType DeleteExhibitionStaffGuest(long itemId)
        {
            return ExhibitionStaffGuestBL.DeleteExhibitionStaffGuest(itemId);
        }
        #endregion
    }
}
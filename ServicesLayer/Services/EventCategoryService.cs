﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class EventCategoryService : IDisposable
    {
        #region Feilds
        EventCategoryBL EventCategoryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return EventCategoryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return EventCategoryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public EventCategoryService()
        {
            EventCategoryBL = new EventCategoryBL();
        }
        public EventCategoryService(string connectionString)
        {
            EventCategoryBL = new EventCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventCategoryBL != null)
                this.EventCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiEventCategory GetEventCategoryByItemId(long itemId)
        {
            return EventCategoryBL.GetEventCategoryByItemId(itemId);
        }
        
        public List<XsiEventCategory> GetEventCategory()
        {
            return EventCategoryBL.GetEventCategory();
        }
        public List<XsiEventCategory> GetEventCategory(EnumSortlistBy sortListBy)
        {
            return EventCategoryBL.GetEventCategory(sortListBy);
        }
        public List<XsiEventCategory> GetEventCategory(XsiEventCategory entity)
        {
            return EventCategoryBL.GetEventCategory(entity);
        }
        public List<XsiEventCategory> GetEventCategory(XsiEventCategory entity, EnumSortlistBy sortListBy)
        {
            return EventCategoryBL.GetEventCategory(entity, sortListBy);
        }
        public List<XsiEventCategory> GetEventCategoryOr(XsiEventCategory entity, EnumSortlistBy sortListBy)
        {
            return EventCategoryBL.GetEventCategoryOr(entity, sortListBy);
        }
         
        public List<XsiEventCategory> GetOtherLanguageEventCategory(XsiEventCategory entity)
        {
            return EventCategoryBL.GetOtherLanguageEventCategory(entity);
        }
        public List<XsiEventCategory> GetCurrentLanguageEventCategory(XsiEventCategory entity)
        {
            return EventCategoryBL.GetCurrentLanguageEventCategory(entity);
        }

        public EnumResultType InsertEventCategory(XsiEventCategory entity)
        {
            return EventCategoryBL.InsertEventCategory(entity);
        }
        public EnumResultType UpdateEventCategory(XsiEventCategory entity)
        {
            return EventCategoryBL.UpdateEventCategory(entity);
        }
        public EnumResultType UpdateEventCategoryStatus(long itemId)
        {
            return EventCategoryBL.UpdateEventCategoryStatus(itemId);
        }
        public EnumResultType DeleteEventCategory(XsiEventCategory entity)
        {
            return EventCategoryBL.DeleteEventCategory(entity);
        }
        public EnumResultType DeleteEventCategory(long itemId)
        {
            return EventCategoryBL.DeleteEventCategory(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class FlagService : IDisposable
    {
        #region Feilds
        FlagBL FlagBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return FlagBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public FlagService()
        {
            FlagBL = new FlagBL();
        }
        public FlagService(string connectionString)
        {
            FlagBL = new FlagBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.FlagBL != null)
                this.FlagBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiFlags GetFlagByItemId(long itemId)
        {
            return FlagBL.GetFlagByItemId(itemId);
        }

        public List<XsiFlags> GetFlag()
        {
            return FlagBL.GetFlag();
        }
        public List<XsiFlags> GetFlag(EnumSortlistBy sortListBy)
        {
            return FlagBL.GetFlag(sortListBy);
        }
        public List<XsiFlags> GetFlag(XsiFlags entity)
        {
            return FlagBL.GetFlag(entity);
        }
        public List<XsiFlags> GetFlag(XsiFlags entity, EnumSortlistBy sortListBy)
        {
            return FlagBL.GetFlag(entity, sortListBy);
        }

        public EnumResultType InsertFlag(XsiFlags entity)
        {
            return FlagBL.InsertFlag(entity);
        }
        public EnumResultType UpdateFlag(XsiFlags entity)
        {
            return FlagBL.UpdateFlag(entity);
        }
        public EnumResultType DeleteFlag(XsiFlags entity)
        {
            return FlagBL.DeleteFlag(entity);
        }
        public EnumResultType DeleteFlag(long itemId)
        {
            return FlagBL.DeleteFlag(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;
using System.Linq;

namespace Xsi.ServicesLayer
{
    public class AdminUserRoleService : IDisposable
    {
        #region Feilds
        AdminUserRoleBL XsiAdminUserRoleBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return XsiAdminUserRoleBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return XsiAdminUserRoleBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public AdminUserRoleService()
        {
            XsiAdminUserRoleBL = new AdminUserRoleBL();
        }
        public AdminUserRoleService(string connectionString)
        {
            XsiAdminUserRoleBL = new AdminUserRoleBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiAdminUserRoleBL != null)
                this.XsiAdminUserRoleBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdminRoles GetAdminUserRoleByItemId(long itemId)
        {
            return XsiAdminUserRoleBL.GetAdminUserRoleByItemId(itemId);
        }
        public List<XsiAdminRoles> GetNonSuperAdminUserRole(EnumSortlistBy sortListBy = EnumSortlistBy.ByItemIdDesc)
        {
            return XsiAdminUserRoleBL.GetNonSuperAdminUserRole(sortListBy);
        }
        public List<XsiAdminRoles> GetAdminUserRole(EnumSortlistBy sortListBy = EnumSortlistBy.ByItemIdDesc)
        {
            return XsiAdminUserRoleBL.GetAdminUserRole(sortListBy);
        }
        public List<XsiAdminRoles> GetAdminUserRole(XsiAdminRoles entity)
        {
            return XsiAdminUserRoleBL.GetAdminUserRole(entity);
        }
        public List<XsiAdminRoles> GetAdminUserRoleAndUser(XsiAdminRoles entity)
        {
            return XsiAdminUserRoleBL.GetAdminUserRoleAndUser(entity);
        }
        public List<XsiAdminRoles> GetAdminUserRoleAndPermission(XsiAdminRoles entity)
        {
            return XsiAdminUserRoleBL.GetAdminUserRoleAndPermission(entity);
        }
        public EnumResultType InsertAdminUserRole(XsiAdminRoles entity)
        {
            return XsiAdminUserRoleBL.InsertAdminUserRole(entity);
        }
        public EnumResultType UpdateAdminUserRole(XsiAdminRoles entity)
        {
            return XsiAdminUserRoleBL.UpdateAdminUserRole(entity);
        }
        public EnumResultType DeleteAdminUserRole(XsiAdminRoles entity)
        {
            return XsiAdminUserRoleBL.DeleteAdminUserRole(entity);
        }
        public EnumResultType DeleteAdminUserRole(long itemId)
        {
            return XsiAdminUserRoleBL.DeleteAdminUserRole(itemId);
        }
        #endregion
    }
}
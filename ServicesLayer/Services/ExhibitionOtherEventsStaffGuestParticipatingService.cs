﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionOtherEventsStaffGuestParticipatingService : IDisposable
    {
        #region Feilds
        ExhibitionOtherEventsStaffGuestParticipatingBL ExhibitionOtherEventsStaffGuestParticipatingBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionOtherEventsStaffGuestParticipatingBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionOtherEventsStaffGuestParticipatingBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionOtherEventsStaffGuestParticipatingService()
        {
            ExhibitionOtherEventsStaffGuestParticipatingBL = new ExhibitionOtherEventsStaffGuestParticipatingBL();
        }
        public ExhibitionOtherEventsStaffGuestParticipatingService(string connectionString)
        {
            ExhibitionOtherEventsStaffGuestParticipatingBL = new ExhibitionOtherEventsStaffGuestParticipatingBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionOtherEventsStaffGuestParticipatingBL != null)
                this.ExhibitionOtherEventsStaffGuestParticipatingBL.Dispose();
        }
        #endregion
        #region Methods 
        public XsiExhibitionOtherEventsStaffGuestParticipating GetExhibitionOtherEventsStaffGuestParticipatingByItemId(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingBL.GetExhibitionOtherEventsStaffGuestParticipatingByItemId(itemId);
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipating> GetExhibitionOtherEventsStaffGuestParticipating()
        {
            return ExhibitionOtherEventsStaffGuestParticipatingBL.GetExhibitionOtherEventsStaffGuestParticipating();
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipating> GetExhibitionOtherEventsStaffGuestParticipating(EnumSortlistBy sortListBy)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingBL.GetExhibitionOtherEventsStaffGuestParticipating(sortListBy);
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipating> GetExhibitionOtherEventsStaffGuestParticipating(XsiExhibitionOtherEventsStaffGuestParticipating entity)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingBL.GetExhibitionOtherEventsStaffGuestParticipating(entity);
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipating> GetExhibitionOtherEventsStaffGuestParticipating(XsiExhibitionOtherEventsStaffGuestParticipating entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingBL.GetExhibitionOtherEventsStaffGuestParticipating(entity, sortListBy);
        }
        public XsiExhibitionOtherEventsStaffGuestParticipating GetExhibitionOtherEventsStaffGuestParticipating(long groupId, long languageId)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingBL.GetExhibitionOtherEventsStaffGuestParticipating(groupId, languageId);
        }

        public EnumResultType InsertExhibitionOtherEventsStaffGuestParticipating(XsiExhibitionOtherEventsStaffGuestParticipating entity)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingBL.InsertExhibitionOtherEventsStaffGuestParticipating(entity);
        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuestParticipating(XsiExhibitionOtherEventsStaffGuestParticipating entity)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingBL.UpdateExhibitionOtherEventsStaffGuestParticipating(entity);
        }
        public EnumResultType RollbackExhibitionOtherEventsStaffGuestParticipating(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingBL.RollbackExhibitionOtherEventsStaffGuestParticipating(itemId);
        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuestParticipatingStatus(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingBL.UpdateExhibitionOtherEventsStaffGuestParticipatingStatus(itemId);
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuestParticipating(XsiExhibitionOtherEventsStaffGuestParticipating entity)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingBL.DeleteExhibitionOtherEventsStaffGuestParticipating(entity);
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuestParticipating(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestParticipatingBL.DeleteExhibitionOtherEventsStaffGuestParticipating(itemId);
        }
        #endregion
    }
}
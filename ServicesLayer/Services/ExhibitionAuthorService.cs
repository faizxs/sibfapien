﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionAuthorService : IDisposable
    {
        #region Feilds
        ExhibitionAuthorBL ExhibitionAuthorBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionAuthorBL.XsiItemdId; }
        }
        #endregion
        #region Constructor
        public ExhibitionAuthorService()
        {
            ExhibitionAuthorBL = new ExhibitionAuthorBL();
        }
        public ExhibitionAuthorService(string connectionString)
        {
            ExhibitionAuthorBL = new ExhibitionAuthorBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionAuthorBL != null)
                this.ExhibitionAuthorBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionAuthor GetExhibitionAuthorByItemId(long itemId)
        {
            return ExhibitionAuthorBL.GetExhibitionAuthorByItemId(itemId);
        }
       
        public List<XsiExhibitionAuthor> GetExhibitionAuthor()
        {
            return ExhibitionAuthorBL.GetExhibitionAuthor();
        }
        public List<XsiExhibitionAuthor> GetExhibitionAuthor(EnumSortlistBy sortListBy)
        {
            return ExhibitionAuthorBL.GetExhibitionAuthor(sortListBy);
        }
        public List<XsiExhibitionAuthor> GetExhibitionAuthor(XsiExhibitionAuthor entity)
        {
            return ExhibitionAuthorBL.GetExhibitionAuthor(entity);
        }

        public Array GetExhibitionAuthorForUI(XsiExhibitionAuthor entity, long exhibitionGroupID)
        {
            return ExhibitionAuthorBL.GetExhibitionAuthorForUI(entity, exhibitionGroupID);
        }
        public Array GetExhibitionAuthorUIForCMS(XsiExhibitionAuthor entity)
        {
            return ExhibitionAuthorBL.GetExhibitionAuthorUIForCMS(entity);
        }
        public List<XsiExhibitionAuthor> GetExhibitionAuthorForSCRFUI(XsiExhibitionAuthor entity, long exhibitionGroupID)
        {
            return ExhibitionAuthorBL.GetExhibitionAuthorForSCRFUI(entity, exhibitionGroupID);
        }
        public List<XsiExhibitionAuthor> GetExhibitionAuthor(XsiExhibitionAuthor entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionAuthorBL.GetExhibitionAuthor(entity, sortListBy);
        }

        public EnumResultType InsertExhibitionAuthor(XsiExhibitionAuthor entity)
        {
            return ExhibitionAuthorBL.InsertExhibitionAuthor(entity);
        }
        public EnumResultType UpdateExhibitionAuthor(XsiExhibitionAuthor entity)
        {
            return ExhibitionAuthorBL.UpdateExhibitionAuthor(entity);
        }
        public EnumResultType UpdateExhibitionAuthorStatus(long itemId)
        {
            return ExhibitionAuthorBL.UpdateExhibitionAuthorStatus(itemId);
        }
        public EnumResultType DeleteExhibitionAuthor(XsiExhibitionAuthor entity)
        {
            return ExhibitionAuthorBL.DeleteExhibitionAuthor(entity);
        }
        public EnumResultType DeleteExhibitionAuthor(long itemId)
        {
            return ExhibitionAuthorBL.DeleteExhibitionAuthor(itemId);
        }
        #endregion
        //Custom
        public long GetNameCount(long itemID, string title, string titlear)
        {
            return ExhibitionAuthorBL.GetNameCount(itemID, title, titlear);
        }
    }
}
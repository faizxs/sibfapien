﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PPSourceService : IDisposable
    {
        #region Feilds
        PPSourceBL PPSourceBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PPSourceBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return PPSourceBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PPSourceService()
        {
            PPSourceBL = new PPSourceBL();
        }
        public PPSourceService(string connectionString)
        {
            PPSourceBL = new PPSourceBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PPSourceBL != null)
                this.PPSourceBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionProfessionalProgramSource GetPPSourceByItemId(long itemId)
        {
            return PPSourceBL.GetPPSourceByItemId(itemId);
        }
        
        public List<XsiExhibitionProfessionalProgramSource> GetPPSource()
        {
            return PPSourceBL.GetPPSource();
        }
        public List<XsiExhibitionProfessionalProgramSource> GetPPSource(EnumSortlistBy sortListBy)
        {
            return PPSourceBL.GetPPSource(sortListBy);
        }
        public List<XsiExhibitionProfessionalProgramSource> GetPPSource(XsiExhibitionProfessionalProgramSource entity)
        {
            return PPSourceBL.GetPPSource(entity);
        }
        public List<XsiExhibitionProfessionalProgramSource> GetPPSource(XsiExhibitionProfessionalProgramSource entity, EnumSortlistBy sortListBy)
        {
            return PPSourceBL.GetPPSource(entity, sortListBy);
        }
         
        public EnumResultType InsertPPSource(XsiExhibitionProfessionalProgramSource entity)
        {
            return PPSourceBL.InsertPPSource(entity);
        }
        public EnumResultType UpdatePPSource(XsiExhibitionProfessionalProgramSource entity)
        {
            return PPSourceBL.UpdatePPSource(entity);
        }
        public EnumResultType UpdatePPSourceStatus(long itemId)
        {
            return PPSourceBL.UpdatePPSourceStatus(itemId);
        }
        public EnumResultType DeletePPSource(XsiExhibitionProfessionalProgramSource entity)
        {
            return PPSourceBL.DeletePPSource(entity);
        }
        public EnumResultType DeletePPSource(long itemId)
        {
            return PPSourceBL.DeletePPSource(itemId);
        }
        #endregion
    }
}
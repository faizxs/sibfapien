﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class HomepageSectionFourService : IDisposable
    {
        #region Feilds
        HomepageSectionFourBL HomepageSectionFourBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return HomepageSectionFourBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return HomepageSectionFourBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public HomepageSectionFourService()
        {
            HomepageSectionFourBL = new HomepageSectionFourBL();
        }
        public HomepageSectionFourService(string connectionString)
        {
            HomepageSectionFourBL = new HomepageSectionFourBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomepageSectionFourBL != null)
                this.HomepageSectionFourBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiHomepageSectionFour GetHomepageSectionFourByItemId(long itemId)
        {
            return HomepageSectionFourBL.GetHomepageSectionFourByItemId(itemId);
        }
        public List<XsiHomepageSectionFour> GetHomepageSectionFour()
        {
            return HomepageSectionFourBL.GetHomepageSectionFour();
        }
        public List<XsiHomepageSectionFour> GetHomepageSectionFour(EnumSortlistBy sortListBy)
        {
            return HomepageSectionFourBL.GetHomepageSectionFour(sortListBy);
        }
        public List<XsiHomepageSectionFour> GetHomepageSectionFour(XsiHomepageSectionFour entity)
        {
            return HomepageSectionFourBL.GetHomepageSectionFour(entity);
        }
        public List<XsiHomepageSectionFour> GetHomepageSectionFour(XsiHomepageSectionFour entity, EnumSortlistBy sortListBy)
        {
            return HomepageSectionFourBL.GetHomepageSectionFour(entity, sortListBy);
        }
        public List<XsiHomepageSectionFour> GetCompleteHomepageSectionFour(XsiHomepageSectionFour entity, EnumSortlistBy sortListBy, long langId)
        {
            return HomepageSectionFourBL.GetCompleteHomepageSectionFour(entity, sortListBy, langId);
        }
        public XsiHomepageSectionFour GetHomepageSectionFour(long groupId)
        {
            return HomepageSectionFourBL.GetHomepageSectionFour(groupId);
        }

        public EnumResultType InsertHomepageSectionFour(XsiHomepageSectionFour entity)
        {
            return HomepageSectionFourBL.InsertHomepageSectionFour(entity);
        }
        public EnumResultType UpdateHomepageSectionFour(XsiHomepageSectionFour entity)
        {
            return HomepageSectionFourBL.UpdateHomepageSectionFour(entity);
        }
        public EnumResultType UpdateHomepageSectionFourStatus(long itemId)
        {
            return HomepageSectionFourBL.UpdateHomepageSectionFourStatus(itemId);
        }
        public EnumResultType DeleteHomepageSectionFour(XsiHomepageSectionFour entity)
        {
            return HomepageSectionFourBL.DeleteHomepageSectionFour(entity);
        }
        public EnumResultType DeleteHomepageSectionFour(long itemId)
        {
            return HomepageSectionFourBL.DeleteHomepageSectionFour(itemId);
        }
        #endregion
    }
}
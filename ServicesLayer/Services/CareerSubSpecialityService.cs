﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class CareerSubSpecialityService : IDisposable
    {
        #region Feilds
        CareerSubSpecialityBL CareerSubSpecialityBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return CareerSubSpecialityBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return CareerSubSpecialityBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public CareerSubSpecialityService()
        {
            CareerSubSpecialityBL = new CareerSubSpecialityBL();
        }
        public CareerSubSpecialityService(string connectionString)
        {
            CareerSubSpecialityBL = new CareerSubSpecialityBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerSubSpecialityBL != null)
                this.CareerSubSpecialityBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiCareerSubSpeciality GetCareerSubSpecialityByItemId(long itemId)
        {
            return CareerSubSpecialityBL.GetCareerSubSpecialityByItemId(itemId);
        }
        
        public List<XsiCareerSubSpeciality> GetCareerSubSpeciality()
        {
            return CareerSubSpecialityBL.GetCareerSubSpeciality();
        }
        public List<XsiCareerSubSpeciality> GetCareerSubSpeciality(EnumSortlistBy sortListBy)
        {
            return CareerSubSpecialityBL.GetCareerSubSpeciality(sortListBy);
        }
        public List<XsiCareerSubSpeciality> GetCareerSubSpeciality(XsiCareerSubSpeciality entity)
        {
            return CareerSubSpecialityBL.GetCareerSubSpeciality(entity);
        }
        public List<XsiCareerSubSpeciality> GetCareerSubSpeciality(XsiCareerSubSpeciality entity, EnumSortlistBy sortListBy)
        {
            return CareerSubSpecialityBL.GetCareerSubSpeciality(entity, sortListBy);
        }
          
        public EnumResultType InsertCareerSubSpeciality(XsiCareerSubSpeciality entity)
        {
            return CareerSubSpecialityBL.InsertCareerSubSpeciality(entity);
        }
        public EnumResultType UpdateCareerSubSpeciality(XsiCareerSubSpeciality entity)
        {
            return CareerSubSpecialityBL.UpdateCareerSubSpeciality(entity);
        }
        public EnumResultType UpdateCareerSubSpecialityStatus(long itemId)
        {
            return CareerSubSpecialityBL.UpdateCareerSubSpecialityStatus(itemId);
        }
        public EnumResultType DeleteCareerSubSpeciality(XsiCareerSubSpeciality entity)
        {
            return CareerSubSpecialityBL.DeleteCareerSubSpeciality(entity);
        }
        public EnumResultType DeleteCareerSubSpeciality(long itemId)
        {
            return CareerSubSpecialityBL.DeleteCareerSubSpeciality(itemId);
        }
        #endregion
    }
}
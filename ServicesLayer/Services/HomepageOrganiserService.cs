﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class HomepageOrganiserService : IDisposable
    {
        #region Feilds
        HomepageOrganiserBL HomepageOrganiserBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return HomepageOrganiserBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return HomepageOrganiserBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public HomepageOrganiserService()
        {
            HomepageOrganiserBL = new HomepageOrganiserBL();
        }
        public HomepageOrganiserService(string connectionString)
        {
            HomepageOrganiserBL = new HomepageOrganiserBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomepageOrganiserBL != null)
                this.HomepageOrganiserBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiHomepageOrganiser GetHomepageOrganiserByItemId(long itemId)
        {
            return HomepageOrganiserBL.GetHomepageOrganiserByItemId(itemId);
        }

        public List<XsiHomepageOrganiser> GetHomepageOrganiser()
        {
            return HomepageOrganiserBL.GetHomepageOrganiser();
        }
        public List<XsiHomepageOrganiser> GetHomepageOrganiser(EnumSortlistBy sortListBy)
        {
            return HomepageOrganiserBL.GetHomepageOrganiser(sortListBy);
        }
        public List<XsiHomepageOrganiser> GetHomepageOrganiser(XsiHomepageOrganiser entity)
        {
            return HomepageOrganiserBL.GetHomepageOrganiser(entity);
        }
        public List<XsiHomepageOrganiser> GetHomepageOrganiser(XsiHomepageOrganiser entity, EnumSortlistBy sortListBy)
        {
            return HomepageOrganiserBL.GetHomepageOrganiser(entity, sortListBy);
        }
        public XsiHomepageOrganiser GetHomepageOrganiser(long Id)
        {
            return HomepageOrganiserBL.GetHomepageOrganiser(Id);
        }

        public EnumResultType InsertHomepageOrganiser(XsiHomepageOrganiser entity)
        {
            return HomepageOrganiserBL.InsertHomepageOrganiser(entity);
        }
        public EnumResultType UpdateHomepageOrganiser(XsiHomepageOrganiser entity)
        {
            return HomepageOrganiserBL.UpdateHomepageOrganiser(entity);
        }
        public EnumResultType UpdateHomepageOrganiserStatus(long itemId)
        {
            return HomepageOrganiserBL.UpdateHomepageOrganiserStatus(itemId);
        }
        public EnumResultType DeleteHomepageOrganiser(XsiHomepageOrganiser entity)
        {
            return HomepageOrganiserBL.DeleteHomepageOrganiser(entity);
        }
        public EnumResultType DeleteHomepageOrganiser(long itemId)
        {
            return HomepageOrganiserBL.DeleteHomepageOrganiser(itemId);
        }
        public EnumResultType UpdateHomepageOrganiserSortOrder(long groupId, long order)
        {
            return HomepageOrganiserBL.UpdateHomepageOrganiserSortOrder(groupId, order);
        }

        //Custom 
        public long GetHomeBannerInfo()
        {
            return HomepageOrganiserBL.GetHomeBannerInfo();
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFAdvertisementPageService : IDisposable
    {
        #region Feilds
        SCRFAdvertisementPageBL SCRFAdvertisementPageBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFAdvertisementPageBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFAdvertisementPageBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFAdvertisementPageService()
        {
            SCRFAdvertisementPageBL = new SCRFAdvertisementPageBL();
        }
        public SCRFAdvertisementPageService(string connectionString)
        {
            SCRFAdvertisementPageBL = new SCRFAdvertisementPageBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFAdvertisementPageBL != null)
                this.SCRFAdvertisementPageBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return SCRFAdvertisementPageBL.GetNextGroupId();
        }
        public XsiScrfadvertisementPage GetSCRFAdvertisementPageByItemId(long itemId)
        {
            return SCRFAdvertisementPageBL.GetSCRFAdvertisementPageByItemId(itemId);
        }

        public List<XsiScrfadvertisementPage> GetSCRFAdvertisementPage()
        {
            return SCRFAdvertisementPageBL.GetSCRFAdvertisementPage();
        }
        public List<XsiScrfadvertisementPage> GetSCRFAdvertisementPage(EnumSortlistBy sortListBy)
        {
            return SCRFAdvertisementPageBL.GetSCRFAdvertisementPage(sortListBy);
        }
        public List<XsiScrfadvertisementPage> GetSCRFAdvertisementPage(XsiScrfadvertisementPage entity)
        {
            return SCRFAdvertisementPageBL.GetSCRFAdvertisementPage(entity);
        }
        public List<XsiScrfadvertisementPage> GetSCRFAdvertisementPage(XsiScrfadvertisementPage entity, EnumSortlistBy sortListBy)
        {
            return SCRFAdvertisementPageBL.GetSCRFAdvertisementPage(entity, sortListBy);
        }
        public XsiScrfadvertisementPage GetSCRFAdvertisementPage(long groupId, long languageId)
        {
            return SCRFAdvertisementPageBL.GetSCRFAdvertisementPage(groupId, languageId);
        }

        public EnumResultType InsertSCRFAdvertisementPage(XsiScrfadvertisementPage entity)
        {
            return SCRFAdvertisementPageBL.InsertSCRFAdvertisementPage(entity);
        }
        public EnumResultType UpdateSCRFAdvertisementPage(XsiScrfadvertisementPage entity)
        {
            return SCRFAdvertisementPageBL.UpdateSCRFAdvertisementPage(entity);
        }
        public EnumResultType RollbackSCRFAdvertisementPage(long itemId)
        {
            return SCRFAdvertisementPageBL.RollbackSCRFAdvertisementPage(itemId);
        }
        public EnumResultType UpdateSCRFAdvertisementPageStatus(long itemId)
        {
            return SCRFAdvertisementPageBL.UpdateSCRFAdvertisementPageStatus(itemId);
        }
        public EnumResultType DeleteSCRFAdvertisementPage(XsiScrfadvertisementPage entity)
        {
            return SCRFAdvertisementPageBL.DeleteSCRFAdvertisementPage(entity);
        }
        public EnumResultType DeleteSCRFAdvertisementPage(long itemId)
        {
            return SCRFAdvertisementPageBL.DeleteSCRFAdvertisementPage(itemId);
        }
        #endregion
    }
}
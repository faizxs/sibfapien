﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionDateService : IDisposable
    {
        #region Feilds
        ExhibitionDateBL ExhibitionDateBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionDateBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionDateBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionDateService()
        {
            ExhibitionDateBL = new ExhibitionDateBL();
        }
        public ExhibitionDateService(string connectionString)
        {
            ExhibitionDateBL = new ExhibitionDateBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionDateBL != null)
                this.ExhibitionDateBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ExhibitionDateBL.GetNextGroupId();
        }
        public XsiExhibitionDate GetExhibitionDateByItemId(long itemId)
        {
            return ExhibitionDateBL.GetExhibitionDateByItemId(itemId);
        }
        public List<XsiExhibitionDate> GetExhibitionDate()
        {
            return ExhibitionDateBL.GetExhibitionDate();
        }
        public List<XsiExhibitionDate> GetExhibitionDate(EnumSortlistBy sortListBy)
        {
            return ExhibitionDateBL.GetExhibitionDate(sortListBy);
        }
        public List<XsiExhibitionDate> GetExhibitionDate(XsiExhibitionDate entity)
        {
            return ExhibitionDateBL.GetExhibitionDate(entity);
        }
        public List<XsiExhibitionDate> GetExhibitionDate(XsiExhibitionDate entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionDateBL.GetExhibitionDate(entity, sortListBy);
        }
        public XsiExhibitionDate GetExhibitionDate(long groupId, long languageId)
        {
            return ExhibitionDateBL.GetExhibitionDate(groupId, languageId);
        }
        public EnumResultType InsertExhibitionDate(XsiExhibitionDate entity)
        {
            return ExhibitionDateBL.InsertExhibitionDate(entity);
        }
        public EnumResultType UpdateExhibitionDate(XsiExhibitionDate entity)
        {
            return ExhibitionDateBL.UpdateExhibitionDate(entity);
        }
        public EnumResultType UpdateExhibitionDateStatus(long itemId)
        {
            return ExhibitionDateBL.UpdateExhibitionDateStatus(itemId);
        }
        public EnumResultType UpdateExhibitionDateSortOrder(long groupId, long order)
        {
            return ExhibitionDateBL.UpdateExhibitionDateSortOrder(groupId, order);
        }
        public EnumResultType DeleteExhibitionDate(XsiExhibitionDate entity)
        {
            return ExhibitionDateBL.DeleteExhibitionDate(entity);
        }
        public EnumResultType DeleteExhibitionDate(long itemId)
        {
            return ExhibitionDateBL.DeleteExhibitionDate(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFIllustrationSubmissionCommentService : IDisposable
    {
        #region Feilds
        SCRFIllustrationSubmissionCommentBL SCRFIllustrationSubmissionCommentBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFIllustrationSubmissionCommentBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFIllustrationSubmissionCommentBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFIllustrationSubmissionCommentService()
        {
            SCRFIllustrationSubmissionCommentBL = new SCRFIllustrationSubmissionCommentBL();
        }
        public SCRFIllustrationSubmissionCommentService(string connectionString)
        {
            SCRFIllustrationSubmissionCommentBL = new SCRFIllustrationSubmissionCommentBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIllustrationSubmissionCommentBL != null)
                this.SCRFIllustrationSubmissionCommentBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return SCRFIllustrationSubmissionCommentBL.GetNextGroupId();
        }
        public XsiScrfillustrationSubmissionComments GetSCRFIllustrationSubmissionCommentByItemId(long itemId)
        {
            return SCRFIllustrationSubmissionCommentBL.GetSCRFIllustrationSubmissionCommentByItemId(itemId);
        }

        public List<XsiScrfillustrationSubmissionComments> GetSCRFIllustrationSubmissionComment()
        {
            return SCRFIllustrationSubmissionCommentBL.GetSCRFIllustrationSubmissionComment();
        }
        public List<XsiScrfillustrationSubmissionComments> GetSCRFIllustrationSubmissionComment(EnumSortlistBy sortListBy)
        {
            return SCRFIllustrationSubmissionCommentBL.GetSCRFIllustrationSubmissionComment(sortListBy);
        }
        public List<XsiScrfillustrationSubmissionComments> GetSCRFIllustrationSubmissionComment(XsiScrfillustrationSubmissionComments entity)
        {
            return SCRFIllustrationSubmissionCommentBL.GetSCRFIllustrationSubmissionComment(entity);
        }
        public List<XsiScrfillustrationSubmissionComments> GetSCRFIllustrationSubmissionComment(XsiScrfillustrationSubmissionComments entity, EnumSortlistBy sortListBy)
        {
            return SCRFIllustrationSubmissionCommentBL.GetSCRFIllustrationSubmissionComment(entity, sortListBy);
        }

        public EnumResultType InsertSCRFIllustrationSubmissionComment(XsiScrfillustrationSubmissionComments entity)
        {
            return SCRFIllustrationSubmissionCommentBL.InsertSCRFIllustrationSubmissionComment(entity);
        }
        public EnumResultType UpdateSCRFIllustrationSubmissionComment(XsiScrfillustrationSubmissionComments entity)
        {
            return SCRFIllustrationSubmissionCommentBL.UpdateSCRFIllustrationSubmissionComment(entity);
        }
        public EnumResultType UpdateSCRFIllustrationSubmissionCommentStatus(long itemId)
        {
            return SCRFIllustrationSubmissionCommentBL.UpdateSCRFIllustrationSubmissionCommentStatus(itemId);
        }
        public EnumResultType DeleteSCRFIllustrationSubmissionComment(XsiScrfillustrationSubmissionComments entity)
        {
            return SCRFIllustrationSubmissionCommentBL.DeleteSCRFIllustrationSubmissionComment(entity);
        }
        public EnumResultType DeleteSCRFIllustrationSubmissionComment(long itemId)
        {
            return SCRFIllustrationSubmissionCommentBL.DeleteSCRFIllustrationSubmissionComment(itemId);
        }
        #endregion
    }
}
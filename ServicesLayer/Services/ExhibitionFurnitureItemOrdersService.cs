﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionFurnitureItemOrdersService : IDisposable
    {
        #region Fields
        ExhibitionFurnitureItemOrdersBL ExhibitionFurnitureItemOrdersBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionFurnitureItemOrdersBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionFurnitureItemOrdersBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionFurnitureItemOrdersService()
        {
            ExhibitionFurnitureItemOrdersBL = new ExhibitionFurnitureItemOrdersBL();
        }
        public ExhibitionFurnitureItemOrdersService(string connectionString)
        {
            ExhibitionFurnitureItemOrdersBL = new ExhibitionFurnitureItemOrdersBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionFurnitureItemOrdersBL != null)
                this.ExhibitionFurnitureItemOrdersBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionFurnitureItemOrders GetExhibitionFurnitureItemOrdersByItemId(long itemId)
        {
            return ExhibitionFurnitureItemOrdersBL.GetExhibitionFurnitureItemOrdersByItemId(itemId);
        }
          
        public List<XsiExhibitionFurnitureItemOrders> GetExhibitionFurnitureItemOrders()
        {
            return ExhibitionFurnitureItemOrdersBL.GetExhibitionFurnitureItemOrders();
        }
        public List<XsiExhibitionFurnitureItemOrders> GetExhibitionFurnitureItemOrders(EnumSortlistBy sortListBy)
        {
            return ExhibitionFurnitureItemOrdersBL.GetExhibitionFurnitureItemOrders(sortListBy);
        }
        public List<XsiExhibitionFurnitureItemOrders> GetExhibitionFurnitureItemOrders(XsiExhibitionFurnitureItemOrders entity)
        {
            return ExhibitionFurnitureItemOrdersBL.GetExhibitionFurnitureItemOrders(entity);
        }
        public List<XsiExhibitionFurnitureItemOrders> GetExhibitionFurnitureItemOrders(XsiExhibitionFurnitureItemOrders entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionFurnitureItemOrdersBL.GetExhibitionFurnitureItemOrders(entity, sortListBy);
        }
         
        public EnumResultType InsertExhibitionFurnitureItemOrders(XsiExhibitionFurnitureItemOrders entity)
        {
            return ExhibitionFurnitureItemOrdersBL.InsertExhibitionFurnitureItemOrders(entity);
        }
        public EnumResultType UpdateExhibitionFurnitureItemOrders(XsiExhibitionFurnitureItemOrders entity)
        {
            return ExhibitionFurnitureItemOrdersBL.UpdateExhibitionFurnitureItemOrders(entity);
        }
        public EnumResultType UpdateExhibitionFurnitureItemOrdersStatus(long itemId, long langId)
        {
            return ExhibitionFurnitureItemOrdersBL.UpdateExhibitionFurnitureItemOrdersStatus(itemId, langId);
        }
        public EnumResultType DeleteExhibitionFurnitureItemOrders(XsiExhibitionFurnitureItemOrders entity)
        {
            return ExhibitionFurnitureItemOrdersBL.DeleteExhibitionFurnitureItemOrders(entity);
        }
        public EnumResultType DeleteExhibitionFurnitureItemOrders(long itemId)
        {
            return ExhibitionFurnitureItemOrdersBL.DeleteExhibitionFurnitureItemOrders(itemId);
        }
        #endregion
    }
}
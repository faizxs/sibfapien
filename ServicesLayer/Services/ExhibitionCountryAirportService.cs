﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionCountryAirportService : IDisposable
    {
        #region Feilds
        ExhibitionCountryAirportBL ExhibitionCountryAirportBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return ExhibitionCountryAirportBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public ExhibitionCountryAirportService()
        {
            ExhibitionCountryAirportBL = new ExhibitionCountryAirportBL();
        }
        public ExhibitionCountryAirportService(string connectionString)
        {
            ExhibitionCountryAirportBL = new ExhibitionCountryAirportBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionCountryAirportBL != null)
                this.ExhibitionCountryAirportBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionCountryAirport GetExhibitionCountryAirportByItemId(long itemId)
        {
            return ExhibitionCountryAirportBL.GetExhibitionCountryAirportByItemId(itemId);
        }

        public List<XsiExhibitionCountryAirport> GetExhibitionCountryAirport()
        {
            return ExhibitionCountryAirportBL.GetExhibitionCountryAirport();
        }
        public List<XsiExhibitionCountryAirport> GetExhibitionCountryAirport(EnumSortlistBy sortListBy)
        {
            return ExhibitionCountryAirportBL.GetExhibitionCountryAirport(sortListBy);
        }
        public List<XsiExhibitionCountryAirport> GetExhibitionCountryAirport(XsiExhibitionCountryAirport entity)
        {
            return ExhibitionCountryAirportBL.GetExhibitionCountryAirport(entity);
        }
        public List<XsiExhibitionCountryAirport> GetExhibitionCountryAirport(XsiExhibitionCountryAirport entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionCountryAirportBL.GetExhibitionCountryAirport(entity, sortListBy);
        }
        public List<XsiExhibitionCountryAirport> GetCompleteExhibitionCountryAirport(XsiExhibitionCountryAirport entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionCountryAirportBL.GetCompleteExhibitionCountryAirport(entity, sortListBy);
        }

        public EnumResultType InsertExhibitionCountryAirport(XsiExhibitionCountryAirport entity)
        {
            return ExhibitionCountryAirportBL.InsertExhibitionCountryAirport(entity);
        }
        public EnumResultType UpdateExhibitionCountryAirport(XsiExhibitionCountryAirport entity)
        {
            return ExhibitionCountryAirportBL.UpdateExhibitionCountryAirport(entity);
        }
        public EnumResultType UpdateExhibitionCountryAirportStatus(long itemId)
        {
            return ExhibitionCountryAirportBL.UpdateExhibitionCountryAirportStatus(itemId);
        }
        public EnumResultType DeleteExhibitionCountryAirport(XsiExhibitionCountryAirport entity)
        {
            return ExhibitionCountryAirportBL.DeleteExhibitionCountryAirport(entity);
        }
        public EnumResultType DeleteExhibitionCountryAirport(long itemId)
        {
            return ExhibitionCountryAirportBL.DeleteExhibitionCountryAirport(itemId);
        }
        #endregion
    }
}
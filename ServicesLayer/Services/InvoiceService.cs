﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class InvoiceService : IDisposable
    {
        #region Feilds
        InvoiceBL InvoiceBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return InvoiceBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return InvoiceBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public InvoiceService()
        {
            InvoiceBL = new InvoiceBL();
        }
        public InvoiceService(string connectionString)
        {
            InvoiceBL = new InvoiceBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.InvoiceBL != null)
                this.InvoiceBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return InvoiceBL.GetNextGroupId();
        }
        public XsiInvoice GetInvoiceByItemId(long itemId)
        {
            return InvoiceBL.GetInvoiceByItemId(itemId);
        }
     
        public List<XsiInvoice> GetInvoice()
        {
            return InvoiceBL.GetInvoice();
        }
        public List<XsiInvoice> GetInvoice(EnumSortlistBy sortListBy)
        {
            return InvoiceBL.GetInvoice(sortListBy);
        }
        public List<XsiInvoice> GetInvoice(XsiInvoice entity)
        {
            return InvoiceBL.GetInvoice(entity);
        }
        public List<XsiInvoice> GetInvoice(XsiInvoice entity, EnumSortlistBy sortListBy)
        {
            return InvoiceBL.GetInvoice(entity, sortListBy);
        }
        public XsiInvoice GetInvoice(long groupId, long languageId)
        {
            return InvoiceBL.GetInvoice(groupId, languageId);
        }

        public EnumResultType InsertInvoice(XsiInvoice entity)
        {
            return InvoiceBL.InsertInvoice(entity);
        }
        public EnumResultType UpdateInvoice(XsiInvoice entity)
        {
            return InvoiceBL.UpdateInvoice(entity);
        }
        public EnumResultType UpdateInvoiceStatus(long itemId)
        {
            return InvoiceBL.UpdateInvoiceStatus(itemId);
        }
        public EnumResultType DeleteInvoice(XsiInvoice entity)
        {
            return InvoiceBL.DeleteInvoice(entity);
        }
        public EnumResultType DeleteInvoice(long itemId)
        {
            return InvoiceBL.DeleteInvoice(itemId);
        }
        #endregion
    }
}
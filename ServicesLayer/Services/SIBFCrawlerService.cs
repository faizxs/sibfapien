﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SIBFCrawlerService : IDisposable
    {
        #region Feilds
        SIBFCrawlerBL SIBFCrawlerBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SIBFCrawlerBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SIBFCrawlerBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SIBFCrawlerService()
        {
            SIBFCrawlerBL = new SIBFCrawlerBL();
        }
        public SIBFCrawlerService(string connectionString)
        {
            SIBFCrawlerBL = new SIBFCrawlerBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SIBFCrawlerBL != null)
                this.SIBFCrawlerBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiSibfcrawler GetSIBFCrawlerByItemId(long itemId)
        {
            return SIBFCrawlerBL.GetSIBFCrawlerByItemId(itemId);
        }

        public List<XsiSibfcrawler> GetSIBFCrawler()
        {
            return SIBFCrawlerBL.GetSIBFCrawler();
        }
        public List<XsiSibfcrawler> GetSIBFCrawler(EnumSortlistBy sortListBy)
        {
            return SIBFCrawlerBL.GetSIBFCrawler(sortListBy);
        }
        public List<XsiSibfcrawler> GetSIBFCrawler(XsiSibfcrawler entity)
        {
            return SIBFCrawlerBL.GetSIBFCrawler(entity);
        }
        public List<XsiSibfcrawler> GetSIBFCrawlerOr(XsiSibfcrawler entity, EnumSortlistBy sortListBy)
        {
            return SIBFCrawlerBL.GetSIBFCrawlerOr(entity, sortListBy);
        }
        public List<XsiSibfcrawler> GetSIBFCrawler(XsiSibfcrawler entity, EnumSortlistBy sortListBy)
        {
            return SIBFCrawlerBL.GetSIBFCrawler(entity, sortListBy);
        }

        public EnumResultType InsertSIBFCrawler(XsiSibfcrawler entity)
        {
            return SIBFCrawlerBL.InsertSIBFCrawler(entity);
        }
        public EnumResultType UpdateSIBFCrawler(XsiSibfcrawler entity)
        {
            return SIBFCrawlerBL.UpdateSIBFCrawler(entity);
        }
        public EnumResultType UpdateSIBFCrawlerStatus(long itemId)
        {
            return SIBFCrawlerBL.UpdateSIBFCrawlerStatus(itemId);
        }
        public EnumResultType DeleteSIBFCrawler(XsiSibfcrawler entity)
        {
            return SIBFCrawlerBL.DeleteSIBFCrawler(entity);
        }
        public EnumResultType DeleteSIBFCrawler(long itemId)
        {
            return SIBFCrawlerBL.DeleteSIBFCrawler(itemId);
        }
        #endregion
    }
}
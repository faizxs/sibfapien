﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class HomePageLinkService : IDisposable
    {
        #region Feilds
        HomePageLinkBL HomePageLinkBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return HomePageLinkBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return HomePageLinkBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public HomePageLinkService()
        {
            HomePageLinkBL = new HomePageLinkBL();
        }
        public HomePageLinkService(string connectionString)
        {
            HomePageLinkBL = new HomePageLinkBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomePageLinkBL != null)
                this.HomePageLinkBL.Dispose();
        }
        #endregion
        #region Methods
      
        public XsiHomePageLink GetHomePageLinkByItemId(long itemId)
        {
            return HomePageLinkBL.GetHomePageLinkByItemId(itemId);
        }
         
        public List<XsiHomePageLink> GetHomePageLink()
        {
            return HomePageLinkBL.GetHomePageLink();
        }
        public List<XsiHomePageLink> GetHomePageLink(EnumSortlistBy sortListBy)
        {
            return HomePageLinkBL.GetHomePageLink(sortListBy);
        }
        public List<XsiHomePageLink> GetHomePageLink(XsiHomePageLink entity)
        {
            return HomePageLinkBL.GetHomePageLink(entity);
        }
        public List<XsiHomePageLink> GetHomePageLink(XsiHomePageLink entity, EnumSortlistBy sortListBy)
        {
            return HomePageLinkBL.GetHomePageLink(entity, sortListBy);
        }
        public List<XsiHomePageLink> GetHomePageLinkOr(XsiHomePageLink entity, EnumSortlistBy sortListBy)
        {
            return HomePageLinkBL.GetHomePageLinkOr(entity, sortListBy);
        }
         
        public EnumResultType InsertHomePageLink(XsiHomePageLink entity)
        {
            return HomePageLinkBL.InsertHomePageLink(entity);
        }
        public EnumResultType UpdateHomePageLink(XsiHomePageLink entity)
        {
            return HomePageLinkBL.UpdateHomePageLink(entity);
        }
        public EnumResultType UpdateHomePageLinkStatus(long itemId)
        {
            return HomePageLinkBL.UpdateHomePageLinkStatus(itemId);
        }
        public EnumResultType UpdateHomePageLinkSortOrder(long groupId, long order)
        {
            return HomePageLinkBL.UpdateHomePageLinkSortOrder(groupId, order);
        }
        public EnumResultType DeleteHomePageLink(XsiHomePageLink entity)
        {
            return HomePageLinkBL.DeleteHomePageLink(entity);
        }
        public EnumResultType DeleteHomePageLink(long itemId)
        {
            return HomePageLinkBL.DeleteHomePageLink(itemId);
        }
        #endregion
    }
}
﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ReasonService : IDisposable
    {
        #region Feilds
        ReasonBL ReasonBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ReasonBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ReasonBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ReasonService()
        {
            ReasonBL = new ReasonBL();
        }
        public ReasonService(string connectionString)
        {
            ReasonBL = new ReasonBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ReasonBL != null)
                this.ReasonBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiReason GetReasonByItemId(long itemId)
        {
            return ReasonBL.GetReasonByItemId(itemId);
        }
        
        public List<XsiReason> GetReason()
        {
            return ReasonBL.GetReason();
        }
        public List<XsiReason> GetReason(EnumSortlistBy sortListBy)
        {
            return ReasonBL.GetReason(sortListBy);
        }
        public List<XsiReason> GetReason(XsiReason entity)
        {
            return ReasonBL.GetReason(entity);
        }
        public List<XsiReason> GetReason(XsiReason entity, EnumSortlistBy sortListBy)
        {
            return ReasonBL.GetReason(entity, sortListBy);
        }
        public List<XsiReason> GetReasonOr(XsiReason entity, EnumSortlistBy sortListBy)
        {
            return ReasonBL.GetReasonOr(entity, sortListBy);
        }
         
        public EnumResultType InsertReason(XsiReason entity)
        {
            return ReasonBL.InsertReason(entity);
        }
        public EnumResultType UpdateReason(XsiReason entity)
        {
            return ReasonBL.UpdateReason(entity);
        }
        public EnumResultType UpdateReasonStatus(long itemId)
        {
            return ReasonBL.UpdateReasonStatus(itemId);
        }
        public EnumResultType DeleteReason(XsiReason entity)
        {
            return ReasonBL.DeleteReason(entity);
        }
        public EnumResultType DeleteReason(long itemId)
        {
            return ReasonBL.DeleteReason(itemId);
        }
        #endregion
    }
}
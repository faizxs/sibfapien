﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFAnnouncementService : IDisposable
    {
        #region Feilds
        SCRFAnnouncementBL SCRFAnnouncementBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFAnnouncementBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFAnnouncementBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFAnnouncementService()
        {
            SCRFAnnouncementBL = new SCRFAnnouncementBL();
        }
        public SCRFAnnouncementService(string connectionString)
        {
            SCRFAnnouncementBL = new SCRFAnnouncementBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFAnnouncementBL != null)
                this.SCRFAnnouncementBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfannouncement GetSCRFAnnouncementByItemId(long itemId)
        {
            return SCRFAnnouncementBL.GetSCRFAnnouncementByItemId(itemId);
        }

        public List<XsiScrfannouncement> GetSCRFAnnouncement()
        {
            return SCRFAnnouncementBL.GetSCRFAnnouncement();
        }
        public List<XsiScrfannouncement> GetSCRFAnnouncement(EnumSortlistBy sortListBy)
        {
            return SCRFAnnouncementBL.GetSCRFAnnouncement(sortListBy);
        }
        public List<XsiScrfannouncement> GetSCRFAnnouncement(XsiScrfannouncement entity)
        {
            return SCRFAnnouncementBL.GetSCRFAnnouncement(entity);
        }
        public List<XsiScrfannouncement> GetSCRFAnnouncement(XsiScrfannouncement entity, EnumSortlistBy sortListBy)
        {
            return SCRFAnnouncementBL.GetSCRFAnnouncement(entity, sortListBy);
        }
        public List<XsiScrfannouncement> GetSCRFAnnouncementOr(XsiScrfannouncement entity, EnumSortlistBy sortListBy)
        {
            return SCRFAnnouncementBL.GetSCRFAnnouncementOr(entity, sortListBy);
        }

        public EnumResultType InsertSCRFAnnouncement(XsiScrfannouncement entity)
        {
            return SCRFAnnouncementBL.InsertSCRFAnnouncement(entity);
        }
        public EnumResultType UpdateSCRFAnnouncement(XsiScrfannouncement entity)
        {
            return SCRFAnnouncementBL.UpdateSCRFAnnouncement(entity);
        }
       
        public EnumResultType UpdateSCRFAnnouncementStatus(long itemId)
        {
            return SCRFAnnouncementBL.UpdateSCRFAnnouncementStatus(itemId);
        }
        public EnumResultType DeleteSCRFAnnouncement(XsiScrfannouncement entity)
        {
            return SCRFAnnouncementBL.DeleteSCRFAnnouncement(entity);
        }
        public EnumResultType DeleteSCRFAnnouncement(long itemId)
        {
            return SCRFAnnouncementBL.DeleteSCRFAnnouncement(itemId);
        }
        #endregion
    }
}
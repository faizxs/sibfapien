﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class AdvertisementBannerService : IDisposable
    {
        #region Feilds
        AdvertisementBannerBL AdvertisementBannerBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return AdvertisementBannerBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return AdvertisementBannerBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public AdvertisementBannerService()
        {
            AdvertisementBannerBL = new AdvertisementBannerBL();
        }
        public AdvertisementBannerService(string connectionString)
        {
            AdvertisementBannerBL = new AdvertisementBannerBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AdvertisementBannerBL != null)
                this.AdvertisementBannerBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdvertisementBanner GetAdvertisementBannerByItemId(long itemId)
        {
            return AdvertisementBannerBL.GetAdvertisementBannerByItemId(itemId);
        }

        public List<XsiAdvertisementBanner> GetAdvertisementBanner()
        {
            return AdvertisementBannerBL.GetAdvertisementBanner();
        }
        public List<XsiAdvertisementBanner> GetAdvertisementBanner(EnumSortlistBy sortListBy)
        {
            return AdvertisementBannerBL.GetAdvertisementBanner(sortListBy);
        }
        public List<XsiAdvertisementBanner> GetAdvertisementBanner(XsiAdvertisementBanner entity)
        {
            return AdvertisementBannerBL.GetAdvertisementBanner(entity);
        }
        public List<XsiAdvertisementBanner> GetAdvertisementBanner(XsiAdvertisementBanner entity, EnumSortlistBy sortListBy)
        {
            return AdvertisementBannerBL.GetAdvertisementBanner(entity, sortListBy);
        }
        public List<XsiAdvertisementBanner> GetAdvertisementBannerOr(XsiAdvertisementBanner entity, EnumSortlistBy sortListBy)
        {
            return AdvertisementBannerBL.GetAdvertisementBannerOr(entity, sortListBy);
        }

        public EnumResultType InsertAdvertisementBanner(XsiAdvertisementBanner entity)
        {
            return AdvertisementBannerBL.InsertAdvertisementBanner(entity);
        }
        public EnumResultType UpdateAdvertisementBanner(XsiAdvertisementBanner entity)
        {
            return AdvertisementBannerBL.UpdateAdvertisementBanner(entity);
        }
        public EnumResultType RollbackAdvertisementBanner(long itemId)
        {
            return AdvertisementBannerBL.RollbackAdvertisementBanner(itemId);
        }
        public EnumResultType UpdateAdvertisementBannerStatus(long itemId)
        {
            return AdvertisementBannerBL.UpdateAdvertisementBannerStatus(itemId);
        }
        public EnumResultType DeleteAdvertisementBanner(XsiAdvertisementBanner entity)
        {
            return AdvertisementBannerBL.DeleteAdvertisementBanner(entity);
        }
        public EnumResultType DeleteAdvertisementBanner(long itemId)
        {
            return AdvertisementBannerBL.DeleteAdvertisementBanner(itemId);
        }
        public EnumResultType UpdateAdvertisementBannerSortOrder(long groupId, long order)
        {
            return AdvertisementBannerBL.UpdateAdvertisementBannerSortOrder(groupId, order);
        }
        #endregion
    }
}
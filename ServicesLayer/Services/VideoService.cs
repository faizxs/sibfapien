﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class VideoService : IDisposable
    {
        #region Feilds
        VideoBL VideoBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return VideoBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return VideoBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public VideoService()
        {
            VideoBL = new VideoBL();
        }
        public VideoService(string connectionString)
        {
            VideoBL = new VideoBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.VideoBL != null)
                this.VideoBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextSortIndex()
        {
            return VideoBL.GetNextSortIndex();
        }
        public XsiVideo GetVideoByItemId(long itemId)
        {
            return VideoBL.GetVideoByItemId(itemId);
        }

        public List<XsiVideo> GetVideo()
        {
            return VideoBL.GetVideo();
        }
        public List<XsiVideo> GetVideo(EnumSortlistBy sortListBy)
        {
            return VideoBL.GetVideo(sortListBy);
        }
        public List<XsiVideo> GetVideo(XsiVideo entity)
        {
            return VideoBL.GetVideo(entity);
        }
        public List<XsiVideo> GetVideo(XsiVideo entity, EnumSortlistBy sortListBy)
        {
            return VideoBL.GetVideo(entity, sortListBy);
        }
        public EnumResultType InsertVideo(XsiVideo entity)
        {
            return VideoBL.InsertVideo(entity);
        }
        public EnumResultType UpdateVideo(XsiVideo entity)
        {
            return VideoBL.UpdateVideo(entity);
        }
        public EnumResultType UpdateVideoStatus(long itemId, long langId)
        {
            return VideoBL.UpdateVideoStatus(itemId, langId);
        }

        public EnumResultType UpdateAlbumCover(long itemId)
        {
            return VideoBL.UpdateAlbumCover(itemId);
        }
        public EnumResultType DeleteVideo(XsiVideo entity)
        {
            return VideoBL.DeleteVideo(entity);
        }
        public EnumResultType DeleteVideo(long itemId)
        {
            return VideoBL.DeleteVideo(itemId);
        }
        #endregion
    }
}
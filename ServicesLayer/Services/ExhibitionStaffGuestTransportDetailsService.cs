﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionStaffGuestTransportDetailsService : IDisposable
    {
        #region Feilds
        ExhibitionStaffGuestTransportDetailsBL ExhibitionStaffGuestTransportDetailsBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionStaffGuestTransportDetailsBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionStaffGuestTransportDetailsBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestTransportDetailsService()
        {
            ExhibitionStaffGuestTransportDetailsBL = new ExhibitionStaffGuestTransportDetailsBL();
        }
        public ExhibitionStaffGuestTransportDetailsService(string connectionString)
        {
            ExhibitionStaffGuestTransportDetailsBL = new ExhibitionStaffGuestTransportDetailsBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionStaffGuestTransportDetailsBL != null)
                this.ExhibitionStaffGuestTransportDetailsBL.Dispose();
        }
        #endregion
        #region Methods 
        public XsiExhibitionStaffGuestTransportDetails GetExhibitionStaffGuestTransportDetailsByItemId(long itemId)
        {
            return ExhibitionStaffGuestTransportDetailsBL.GetExhibitionStaffGuestTransportDetailsByItemId(itemId);
        }

        public List<XsiExhibitionStaffGuestTransportDetails> GetExhibitionStaffGuestTransportDetails()
        {
            return ExhibitionStaffGuestTransportDetailsBL.GetExhibitionStaffGuestTransportDetails();
        }
        public List<XsiExhibitionStaffGuestTransportDetails> GetExhibitionStaffGuestTransportDetails(EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestTransportDetailsBL.GetExhibitionStaffGuestTransportDetails(sortListBy);
        }
        public List<XsiExhibitionStaffGuestTransportDetails> GetExhibitionStaffGuestTransportDetails(XsiExhibitionStaffGuestTransportDetails entity)
        {
            return ExhibitionStaffGuestTransportDetailsBL.GetExhibitionStaffGuestTransportDetails(entity);
        }
        public List<XsiExhibitionStaffGuestTransportDetails> GetExhibitionStaffGuestTransportDetails(XsiExhibitionStaffGuestTransportDetails entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestTransportDetailsBL.GetExhibitionStaffGuestTransportDetails(entity, sortListBy);
        }
        public XsiExhibitionStaffGuestTransportDetails GetExhibitionStaffGuestTransportDetails(long groupId, long languageId)
        {
            return ExhibitionStaffGuestTransportDetailsBL.GetExhibitionStaffGuestTransportDetails(groupId, languageId);
        }

        public EnumResultType InsertExhibitionStaffGuestTransportDetails(XsiExhibitionStaffGuestTransportDetails entity)
        {
            return ExhibitionStaffGuestTransportDetailsBL.InsertExhibitionStaffGuestTransportDetails(entity);
        }
        public EnumResultType UpdateExhibitionStaffGuestTransportDetails(XsiExhibitionStaffGuestTransportDetails entity)
        {
            return ExhibitionStaffGuestTransportDetailsBL.UpdateExhibitionStaffGuestTransportDetails(entity);
        }
        public EnumResultType RollbackExhibitionStaffGuestTransportDetails(long itemId)
        {
            return ExhibitionStaffGuestTransportDetailsBL.RollbackExhibitionStaffGuestTransportDetails(itemId);
        }
        public EnumResultType UpdateExhibitionStaffGuestTransportDetailsStatus(long itemId)
        {
            return ExhibitionStaffGuestTransportDetailsBL.UpdateExhibitionStaffGuestTransportDetailsStatus(itemId);
        }
        public EnumResultType DeleteExhibitionStaffGuestTransportDetails(XsiExhibitionStaffGuestTransportDetails entity)
        {
            return ExhibitionStaffGuestTransportDetailsBL.DeleteExhibitionStaffGuestTransportDetails(entity);
        }
        public EnumResultType DeleteExhibitionStaffGuestTransportDetails(long itemId)
        {
            return ExhibitionStaffGuestTransportDetailsBL.DeleteExhibitionStaffGuestTransportDetails(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFWinnersService : IDisposable
    {
        #region Feilds
        SCRFWinnersBL SCRFWinnersBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFWinnersBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFWinnersBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFWinnersService()
        {
            SCRFWinnersBL = new SCRFWinnersBL();
        }
        public SCRFWinnersService(string connectionString)
        {
            SCRFWinnersBL = new SCRFWinnersBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFWinnersBL != null)
                this.SCRFWinnersBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return SCRFWinnersBL.GetNextGroupId();
        }
        public XsiScrfwinners GetSCRFWinnersByItemId(long itemId)
        {
            return SCRFWinnersBL.GetSCRFWinnersByItemId(itemId);
        }
        
        public List<XsiScrfwinners> GetSCRFWinners()
        {
            return SCRFWinnersBL.GetSCRFWinners();
        }
        public List<XsiScrfwinners> GetSCRFWinners(EnumSortlistBy sortListBy)
        {
            return SCRFWinnersBL.GetSCRFWinners(sortListBy);
        }
        public List<XsiScrfwinners> GetSCRFWinners(XsiScrfwinners entity)
        {
            return SCRFWinnersBL.GetSCRFWinners(entity);
        }
        public List<XsiScrfwinners> GetSCRFWinners(XsiScrfwinners entity, EnumSortlistBy sortListBy)
        {
            return SCRFWinnersBL.GetSCRFWinners(entity, sortListBy);
        }
        public XsiScrfwinners GetSCRFWinners(long groupId, long languageId)
        {
            return SCRFWinnersBL.GetSCRFWinners(groupId, languageId);
        }

        public EnumResultType InsertSCRFWinners(XsiScrfwinners entity)
        {
            return SCRFWinnersBL.InsertSCRFWinners(entity);
        }
        public EnumResultType UpdateSCRFWinners(XsiScrfwinners entity)
        {
            return SCRFWinnersBL.UpdateSCRFWinners(entity);
        }
        public EnumResultType UpdateSCRFWinnersStatus(long itemId)
        {
            return SCRFWinnersBL.UpdateSCRFWinnersStatus(itemId);
        }
        public EnumResultType DeleteSCRFWinners(XsiScrfwinners entity)
        {
            return SCRFWinnersBL.DeleteSCRFWinners(entity);
        }
        public EnumResultType DeleteSCRFWinners(long itemId)
        {
            return SCRFWinnersBL.DeleteSCRFWinners(itemId);
        }
        #endregion
    }
}
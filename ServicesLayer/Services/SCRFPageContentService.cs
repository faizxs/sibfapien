﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFPageContentService : IDisposable
    {
        #region Feilds
        SCRFPageContentBL XsiSCRFPageContentBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return XsiSCRFPageContentBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return XsiSCRFPageContentBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFPageContentService()
        {
            XsiSCRFPageContentBL = new SCRFPageContentBL();
        }
        public SCRFPageContentService(string connectionString)
        {
            XsiSCRFPageContentBL = new SCRFPageContentBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiSCRFPageContentBL != null)
                this.XsiSCRFPageContentBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfpagesContent GetSCRFPageContentByItemId(long itemId)
        {
            return XsiSCRFPageContentBL.GetSCRFPageContentByItemId(itemId);
        }
        
        public List<XsiScrfpagesContent> GetSCRFPageContent()
        {
            return XsiSCRFPageContentBL.GetSCRFPageContent();
        }
        public List<XsiScrfpagesContent> GetSCRFPageContent(EnumSortlistBy sortListBy)
        {
            return XsiSCRFPageContentBL.GetSCRFPageContent(sortListBy);
        }
        public List<XsiScrfpagesContent> GetSCRFPageContent(XsiScrfpagesContent entity)
        {
            return XsiSCRFPageContentBL.GetSCRFPageContent(entity);
        }

        public List<XsiScrfpagesContent> GetSCRFPageContentOr(XsiScrfpagesContent entity, EnumSortlistBy sortListBy)
        {
            return XsiSCRFPageContentBL.GetSCRFPageContentOr(entity, sortListBy);
        }
        public List<XsiScrfpagesContent> GetSCRFPageContent(XsiScrfpagesContent entity, EnumSortlistBy sortListBy)
        {
            return XsiSCRFPageContentBL.GetSCRFPageContent(entity, sortListBy);
        }
        
        public EnumResultType InsertSCRFPageContent(XsiScrfpagesContent entity)
        {
            return XsiSCRFPageContentBL.InsertSCRFPageContent(entity);
        }
        public EnumResultType UpdateSCRFPageContent(XsiScrfpagesContent entity)
        {
            return XsiSCRFPageContentBL.UpdateSCRFPageContent(entity);
        }
        public EnumResultType RollbackSCRFPageContent(long itemId)
        {
            return XsiSCRFPageContentBL.RollbackSCRFPageContent(itemId);
        }
        public EnumResultType UpdateSCRFPageContentStatus(long itemId)
        {
            return XsiSCRFPageContentBL.UpdateSCRFPageContentStatus(itemId);
        }
        public EnumResultType DeleteSCRFPageContent(XsiScrfpagesContent entity)
        {
            return XsiSCRFPageContentBL.DeleteSCRFPageContent(entity);
        }
        public EnumResultType DeleteSCRFPageContent(long itemId)
        {
            return XsiSCRFPageContentBL.DeleteSCRFPageContent(itemId);
        }
        #endregion
    }
}
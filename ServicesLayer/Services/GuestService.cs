﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class GuestService : IDisposable
    {
        #region Feilds
        GuestBL GuestBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return GuestBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return GuestBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public GuestService()
        {
            GuestBL = new GuestBL();
        }
        public GuestService(string connectionString)
        {
            GuestBL = new GuestBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.GuestBL != null)
                this.GuestBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiGuests GetGuestByItemId(long itemId)
        {
            return GuestBL.GetGuestByItemId(itemId);
        }
         
        public List<XsiGuests> GetGuest()
        {
            return GuestBL.GetGuest();
        }
        public List<XsiGuests> GetGuest(EnumSortlistBy sortListBy)
        {
            return GuestBL.GetGuest(sortListBy);
        }
        public List<XsiGuests> GetGuest(XsiGuests entity)
        {
            return GuestBL.GetGuest(entity);
        }
        public List<XsiGuests> GetGuest(XsiGuests entity, EnumSortlistBy sortListBy)
        {
            return GuestBL.GetGuest(entity, sortListBy);
        }
        public List<XsiGuests> GetGuestOr(XsiGuests entity, EnumSortlistBy sortListBy)
        {
            return GuestBL.GetGuestOr(entity, sortListBy);
        }
         
        public EnumResultType InsertGuest(XsiGuests entity)
        {
            return GuestBL.InsertGuest(entity);
        }
        public EnumResultType UpdateGuest(XsiGuests entity)
        {
            return GuestBL.UpdateGuest(entity);
        }
        public EnumResultType UpdateGuestStatus(long itemId)
        {
            return GuestBL.UpdateGuestStatus(itemId);
        }
        public EnumResultType DeleteGuest(XsiGuests entity)
        {
            return GuestBL.DeleteGuest(entity);
        }
        public EnumResultType DeleteGuest(long itemId)
        {
            return GuestBL.DeleteGuest(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFVideoAlbumService : IDisposable
    {
        #region Feilds
        SCRFVideoAlbumBL SCRFVideoAlbumBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFVideoAlbumBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFVideoAlbumBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFVideoAlbumService()
        {
            SCRFVideoAlbumBL = new SCRFVideoAlbumBL();
        }
        public SCRFVideoAlbumService(string connectionString)
        {
            SCRFVideoAlbumBL = new SCRFVideoAlbumBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFVideoAlbumBL != null)
                this.SCRFVideoAlbumBL.Dispose();
        }
        #endregion
        #region Methods
        
        public long GetNextSortIndex()
        {
            return SCRFVideoAlbumBL.GetNextSortIndex();
        }
        public XsiScrfvideoAlbum GetSCRFVideoAlbumByItemId(long itemId)
        {
            return SCRFVideoAlbumBL.GetSCRFVideoAlbumByItemId(itemId);
        }
         
        public List<XsiScrfvideoAlbum> GetSCRFVideoAlbum()
        {
            return SCRFVideoAlbumBL.GetSCRFVideoAlbum();
        }
        public List<XsiScrfvideoAlbum> GetSCRFVideoAlbum(EnumSortlistBy sortListBy)
        {
            return SCRFVideoAlbumBL.GetSCRFVideoAlbum(sortListBy);
        }
        public List<XsiScrfvideoAlbum> GetSCRFVideoAlbum(XsiScrfvideoAlbum entity)
        {
            return SCRFVideoAlbumBL.GetSCRFVideoAlbum(entity);
        }
        public List<XsiScrfvideoAlbum> GetSCRFVideoAlbum(XsiScrfvideoAlbum entity, EnumSortlistBy sortListBy)
        {
            return SCRFVideoAlbumBL.GetSCRFVideoAlbum(entity, sortListBy);
        }
        
        public EnumResultType InsertSCRFVideoAlbum(XsiScrfvideoAlbum entity)
        {
            return SCRFVideoAlbumBL.InsertSCRFVideoAlbum(entity);
        }
        public EnumResultType UpdateSCRFVideoAlbum(XsiScrfvideoAlbum entity)
        {
            return SCRFVideoAlbumBL.UpdateSCRFVideoAlbum(entity);
        }
        public EnumResultType UpdateSCRFVideoAlbumStatus(long itemId)
        {
            return SCRFVideoAlbumBL.UpdateSCRFVideoAlbumStatus(itemId);
        }
        public EnumResultType DeleteSCRFVideoAlbum(XsiScrfvideoAlbum entity)
        {
            return SCRFVideoAlbumBL.DeleteSCRFVideoAlbum(entity);
        }
        public EnumResultType DeleteSCRFVideoAlbum(long itemId)
        {
            return SCRFVideoAlbumBL.DeleteSCRFVideoAlbum(itemId);
        }
        #endregion
    }
}
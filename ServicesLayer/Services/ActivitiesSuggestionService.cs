﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ActivitiesSuggestionService : IDisposable
    {
        #region Feilds
        ActivitiesSuggestionBL ActivitiesSuggestionBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ActivitiesSuggestionBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ActivitiesSuggestionBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ActivitiesSuggestionService()
        {
            ActivitiesSuggestionBL = new ActivitiesSuggestionBL();
        }
        public ActivitiesSuggestionService(string connectionString)
        {
            ActivitiesSuggestionBL = new ActivitiesSuggestionBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ActivitiesSuggestionBL != null)
                this.ActivitiesSuggestionBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiActivitiesSuggestion GetActivitiesSuggestionByItemId(long itemId)
        {
            return ActivitiesSuggestionBL.GetActivitiesSuggestionByItemId(itemId);
        }

        public List<XsiActivitiesSuggestion> GetActivitiesSuggestion()
        {
            return ActivitiesSuggestionBL.GetActivitiesSuggestion();
        }
        public List<XsiActivitiesSuggestion> GetActivitiesSuggestion(EnumSortlistBy sortListBy)
        {
            return ActivitiesSuggestionBL.GetActivitiesSuggestion(sortListBy);
        }
        public List<XsiActivitiesSuggestion> GetActivitiesSuggestion(XsiActivitiesSuggestion entity)
        {
            return ActivitiesSuggestionBL.GetActivitiesSuggestion(entity);
        }
        public List<XsiActivitiesSuggestion> GetActivitiesSuggestion(XsiActivitiesSuggestion entity, EnumSortlistBy sortListBy)
        {
            return ActivitiesSuggestionBL.GetActivitiesSuggestion(entity, sortListBy);
        }
        public List<XsiActivitiesSuggestion> GetActivitiesSuggestionOr(XsiActivitiesSuggestion entity, EnumSortlistBy sortListBy)
        {
            return ActivitiesSuggestionBL.GetActivitiesSuggestionOr(entity, sortListBy);
        }

        public EnumResultType InsertActivitiesSuggestion(XsiActivitiesSuggestion entity)
        {
            return ActivitiesSuggestionBL.InsertActivitiesSuggestion(entity);
        }
        public EnumResultType UpdateActivitiesSuggestion(XsiActivitiesSuggestion entity)
        {
            return ActivitiesSuggestionBL.UpdateActivitiesSuggestion(entity);
        }
        public EnumResultType UpdateActivitiesSuggestionStatus(long itemId)
        {
            return ActivitiesSuggestionBL.UpdateActivitiesSuggestionStatus(itemId);
        }
        public EnumResultType DeleteActivitiesSuggestion(XsiActivitiesSuggestion entity)
        {
            return ActivitiesSuggestionBL.DeleteActivitiesSuggestion(entity);
        }
        public EnumResultType DeleteActivitiesSuggestion(long itemId)
        {
            return ActivitiesSuggestionBL.DeleteActivitiesSuggestion(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PageMenuNewService : IDisposable
    {
        #region Feilds
        PageMenuNewBL PageMenuNewBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PageMenuNewBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return PageMenuNewBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PageMenuNewService()
        {
            PageMenuNewBL = new PageMenuNewBL();
        }
        public PageMenuNewService(string connectionString)
        {
            PageMenuNewBL = new PageMenuNewBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PageMenuNewBL != null)
                this.PageMenuNewBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiPageMenuNew GetPageMenuNewByItemId(long itemId)
        {
            return PageMenuNewBL.GetPageMenuNewByItemId(itemId);
        }
         
        public List<XsiPageMenuNew> GetPageMenuNew()
        {
            return PageMenuNewBL.GetPageMenuNew();
        }
        public List<XsiPageMenuNew> GetPageMenuNew(EnumSortlistBy sortListBy)
        {
            return PageMenuNewBL.GetPageMenuNew(sortListBy);
        }
        public List<XsiPageMenuNew> GetPageMenuNew(XsiPageMenuNew entity)
        {
            return PageMenuNewBL.GetPageMenuNew(entity);
        }
        public List<XsiPageMenuNew> GetPageMenuNewOr(XsiPageMenuNew entity, EnumSortlistBy sortListBy)
        {
            return PageMenuNewBL.GetPageMenuNewOr(entity, sortListBy);
        }
        public List<XsiPageMenuNew> GetPageMenuNew(XsiPageMenuNew entity, EnumSortlistBy sortListBy)
        {
            return PageMenuNewBL.GetPageMenuNew(entity, sortListBy);
        }
        
        public List<XsiPageMenuNew> GetParents(long itemId)
        {
            return PageMenuNewBL.GetParents(itemId);
        }

        public EnumResultType InsertPageMenuNew(XsiPageMenuNew entity)
        {
            return PageMenuNewBL.InsertPageMenuNew(entity);
        }
        public EnumResultType UpdatePageMenuNew(XsiPageMenuNew entity)
        {
            return PageMenuNewBL.UpdatePageMenuNew(entity);
        }
        public EnumResultType UpdatePageMenuNewStatus(long itemId)
        {
            return PageMenuNewBL.UpdatePageMenuNewStatus(itemId);
        }
        public EnumResultType UpdatePageMenuNewSortOrder(long groupId, long order)
        {
            return PageMenuNewBL.UpdatePageMenuNewSortOrder(groupId, order);
        }
        public EnumResultType DeletePageMenuNew(XsiPageMenuNew entity)
        {
            return PageMenuNewBL.DeletePageMenuNew(entity);
        }
        public EnumResultType DeletePageMenuNew(long itemId)
        {
            return PageMenuNewBL.DeletePageMenuNew(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class EventWebsiteService : IDisposable
    {
        #region Feilds
        EventWebsiteBL EventWebsiteBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return EventWebsiteBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return EventWebsiteBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public EventWebsiteService()
        {
            EventWebsiteBL = new EventWebsiteBL();
        }
        public EventWebsiteService(string connectionString)
        {
            EventWebsiteBL = new EventWebsiteBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventWebsiteBL != null)
                this.EventWebsiteBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return EventWebsiteBL.GetNextGroupId();
        }
        public XsiEventWebsite GetEventWebsiteByItemId(long itemId)
        {
            return EventWebsiteBL.GetEventWebsiteByItemId(itemId);
        }
         
        public List<XsiEventWebsite> GetEventWebsite()
        {
            return EventWebsiteBL.GetEventWebsite();
        }
        public List<XsiEventWebsite> GetEventWebsite(EnumSortlistBy sortListBy)
        {
            return EventWebsiteBL.GetEventWebsite(sortListBy);
        }
        public List<XsiEventWebsite> GetEventWebsite(XsiEventWebsite entity)
        {
            return EventWebsiteBL.GetEventWebsite(entity);
        }
        public List<XsiEventWebsite> GetEventWebsite(XsiEventWebsite entity, EnumSortlistBy sortListBy)
        {
            return EventWebsiteBL.GetEventWebsite(entity, sortListBy);
        }
        public XsiEventWebsite GetEventWebsite(long groupId, long languageId)
        {
            return EventWebsiteBL.GetEventWebsite(groupId, languageId);
        }

        public EnumResultType InsertEventWebsite(XsiEventWebsite entity)
        {
            return EventWebsiteBL.InsertEventWebsite(entity);
        }
        public EnumResultType UpdateEventWebsite(XsiEventWebsite entity)
        {
            return EventWebsiteBL.UpdateEventWebsite(entity);
        }
        public EnumResultType UpdateEventWebsiteStatus(long itemId)
        {
            return EventWebsiteBL.UpdateEventWebsiteStatus(itemId);
        }
        public EnumResultType DeleteEventWebsite(XsiEventWebsite entity)
        {
            return EventWebsiteBL.DeleteEventWebsite(entity);
        }
        public EnumResultType DeleteEventWebsite(long itemId)
        {
            return EventWebsiteBL.DeleteEventWebsite(itemId);
        }
        #endregion
    }
}
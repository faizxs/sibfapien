﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PageContentNewService : IDisposable
    {
        #region Feilds
        PageContentNewBL XsiPageContentNewBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return XsiPageContentNewBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return XsiPageContentNewBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PageContentNewService()
        {
            XsiPageContentNewBL = new PageContentNewBL();
        }
        public PageContentNewService(string connectionString)
        {
            XsiPageContentNewBL = new PageContentNewBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiPageContentNewBL != null)
                this.XsiPageContentNewBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiPagesContentNew GetPageContentNewByItemId(long itemId)
        {
            return XsiPageContentNewBL.GetPageContentNewByItemId(itemId);
        }
         
        public List<XsiPagesContentNew> GetPageContentNew()
        {
            return XsiPageContentNewBL.GetPageContentNew();
        }
        public List<XsiPagesContentNew> GetPageContentNew(EnumSortlistBy sortListBy)
        {
            return XsiPageContentNewBL.GetPageContentNew(sortListBy);
        }
        public List<XsiPagesContentNew> GetPageContentNew(XsiPagesContentNew entity)
        {
            return XsiPageContentNewBL.GetPageContentNew(entity);
        }

        public List<XsiPagesContentNew> GetPageContentNewOr(XsiPagesContentNew entity, EnumSortlistBy sortListBy)
        {
            return XsiPageContentNewBL.GetPageContentNewOr(entity, sortListBy);
        }
        public List<XsiPagesContentNew> GetPageContentNew(XsiPagesContentNew entity, EnumSortlistBy sortListBy)
        {
            return XsiPageContentNewBL.GetPageContentNew(entity, sortListBy);
        }
        //public XsiPagesContentNew GetPageContentNew(long groupId, long languageId)
        //{
        //    return XsiPageContentNewBL.GetPageContentNew(groupId, languageId);
        //}
        //public List<sp_FetchSIBFSearchResults_Result> GetSearchContent(string searchString, long languageId)
        //{
        //    return XsiPageContentNewBL.GetSearchContent(searchString, languageId);
        //}

        public EnumResultType InsertPageContentNew(XsiPagesContentNew entity)
        {
            return XsiPageContentNewBL.InsertPageContentNew(entity);
        }
        public EnumResultType UpdatePageContentNew(XsiPagesContentNew entity)
        {
            return XsiPageContentNewBL.UpdatePageContentNew(entity);
        }
        public EnumResultType RollbackPageContentNew(long itemId)
        {
            return XsiPageContentNewBL.RollbackPageContentNew(itemId);
        }
        public EnumResultType UpdatePageContentNewStatus(long itemId)
        {
            return XsiPageContentNewBL.UpdatePageContentNewStatus(itemId);
        }
        public EnumResultType DeletePageContentNew(XsiPagesContentNew entity)
        {
            return XsiPageContentNewBL.DeletePageContentNew(entity);
        }
        public EnumResultType DeletePageContentNew(long itemId)
        {
            return XsiPageContentNewBL.DeletePageContentNew(itemId);
        }
        #endregion
    }
}
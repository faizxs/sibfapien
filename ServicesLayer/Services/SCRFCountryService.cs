﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFCountryService : IDisposable
    {
        #region Feilds
        SCRFCountryBL SCRFCountryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFCountryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFCountryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFCountryService()
        {
            SCRFCountryBL = new SCRFCountryBL();
        }
        public SCRFCountryService(string connectionString)
        {
            SCRFCountryBL = new SCRFCountryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFCountryBL != null)
                this.SCRFCountryBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfcountry GetSCRFCountryByItemId(long itemId)
        {
            return SCRFCountryBL.GetSCRFCountryByItemId(itemId);
        }

        public List<XsiScrfcountry> GetSCRFCountry()
        {
            return SCRFCountryBL.GetSCRFCountry();
        }
        public List<XsiScrfcountry> GetSCRFCountry(EnumSortlistBy sortListBy)
        {
            return SCRFCountryBL.GetSCRFCountry(sortListBy);
        }
        public List<XsiScrfcountry> GetSCRFCountry(XsiScrfcountry entity)
        {
            return SCRFCountryBL.GetSCRFCountry(entity);
        }
        public List<XsiScrfcountry> GetSCRFCountry(XsiScrfcountry entity, EnumSortlistBy sortListBy)
        {
            return SCRFCountryBL.GetSCRFCountry(entity, sortListBy);
        }
        public List<XsiScrfcountry> GetSCRFCountryOr(XsiScrfcountry entity, EnumSortlistBy sortListBy)
        {
            return SCRFCountryBL.GetSCRFCountryOr(entity, sortListBy);
        }
        public List<XsiScrfcountry> GetOtherLanguageSCRFCountry(XsiScrfcountry entity)
        {
            return SCRFCountryBL.GetOtherLanguageSCRFCountry(entity);
        }
        public List<XsiScrfcountry> GetCurrentLanguageSCRFCountry(XsiScrfcountry entity)
        {
            return SCRFCountryBL.GetCurrentLanguageSCRFCountry(entity);
        }
        public List<XsiScrfcountry> GetCompleteSCRFCountry(XsiScrfcountry entity, EnumSortlistBy sortListBy)
        {
            return SCRFCountryBL.GetCompleteSCRFCountry(entity, sortListBy);
        }

        public EnumResultType InsertSCRFCountry(XsiScrfcountry entity)
        {
            return SCRFCountryBL.InsertSCRFCountry(entity);
        }
        public EnumResultType UpdateSCRFCountry(XsiScrfcountry entity)
        {
            return SCRFCountryBL.UpdateSCRFCountry(entity);
        }
        public EnumResultType UpdateSCRFCountryStatus(long itemId)
        {
            return SCRFCountryBL.UpdateSCRFCountryStatus(itemId);
        }
        public EnumResultType DeleteSCRFCountry(XsiScrfcountry entity)
        {
            return SCRFCountryBL.DeleteSCRFCountry(entity);
        }
        public EnumResultType DeleteSCRFCountry(long itemId)
        {
            return SCRFCountryBL.DeleteSCRFCountry(itemId);
        }
        #endregion
    }
}
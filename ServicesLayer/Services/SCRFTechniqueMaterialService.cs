﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFTechniqueMaterialService : IDisposable
    {
        #region Feilds
        SCRFTechniqueMaterialBL SCRFTechniqueMaterialBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFTechniqueMaterialBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFTechniqueMaterialBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFTechniqueMaterialService()
        {
            SCRFTechniqueMaterialBL = new SCRFTechniqueMaterialBL();
        }
        public SCRFTechniqueMaterialService(string connectionString)
        {
            SCRFTechniqueMaterialBL = new SCRFTechniqueMaterialBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFTechniqueMaterialBL != null)
                this.SCRFTechniqueMaterialBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrftechniqueMaterial GetSCRFTechniqueMaterialByItemId(long itemId)
        {
            return SCRFTechniqueMaterialBL.GetSCRFTechniqueMaterialByItemId(itemId);
        }

        public List<XsiScrftechniqueMaterial> GetSCRFTechniqueMaterial()
        {
            return SCRFTechniqueMaterialBL.GetSCRFTechniqueMaterial();
        }
        public List<XsiScrftechniqueMaterial> GetSCRFTechniqueMaterial(EnumSortlistBy sortListBy)
        {
            return SCRFTechniqueMaterialBL.GetSCRFTechniqueMaterial(sortListBy);
        }
        public List<XsiScrftechniqueMaterial> GetSCRFTechniqueMaterial(XsiScrftechniqueMaterial entity)
        {
            return SCRFTechniqueMaterialBL.GetSCRFTechniqueMaterial(entity);
        }
        public List<XsiScrftechniqueMaterial> GetSCRFTechniqueMaterial(XsiScrftechniqueMaterial entity, EnumSortlistBy sortListBy)
        {
            return SCRFTechniqueMaterialBL.GetSCRFTechniqueMaterial(entity, sortListBy);
        }

        public EnumResultType InsertSCRFTechniqueMaterial(XsiScrftechniqueMaterial entity)
        {
            return SCRFTechniqueMaterialBL.InsertSCRFTechniqueMaterial(entity);
        }
        public EnumResultType UpdateSCRFTechniqueMaterial(XsiScrftechniqueMaterial entity)
        {
            return SCRFTechniqueMaterialBL.UpdateSCRFTechniqueMaterial(entity);
        }
        public EnumResultType UpdateSCRFTechniqueMaterialStatus(long itemId)
        {
            return SCRFTechniqueMaterialBL.UpdateSCRFTechniqueMaterialStatus(itemId);
        }
        public EnumResultType DeleteSCRFTechniqueMaterial(XsiScrftechniqueMaterial entity)
        {
            return SCRFTechniqueMaterialBL.DeleteSCRFTechniqueMaterial(entity);
        }
        public EnumResultType DeleteSCRFTechniqueMaterial(long itemId)
        {
            return SCRFTechniqueMaterialBL.DeleteSCRFTechniqueMaterial(itemId);
        }
        #endregion
    }
}
﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class StaffGuestTypeService : IDisposable
    {
        #region Feilds
        StaffGuestTypeBL StaffGuestTypeBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return StaffGuestTypeBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public StaffGuestTypeService()
        {
            StaffGuestTypeBL = new StaffGuestTypeBL();
        }
        public StaffGuestTypeService(string connectionString)
        {
            StaffGuestTypeBL = new StaffGuestTypeBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.StaffGuestTypeBL != null)
                this.StaffGuestTypeBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiStaffGuestType GetStaffGuestTypeByItemId(long itemId)
        {
            return StaffGuestTypeBL.GetStaffGuestTypeByItemId(itemId);
        }
        public List<XsiStaffGuestType> GetStaffGuestType()
        {
            return StaffGuestTypeBL.GetStaffGuestType();
        }
        public List<XsiStaffGuestType> GetStaffGuestType(EnumSortlistBy sortListBy)
        {
            return StaffGuestTypeBL.GetStaffGuestType(sortListBy);
        }
        public List<XsiStaffGuestType> GetStaffGuestType(XsiStaffGuestType entity)
        {
            return StaffGuestTypeBL.GetStaffGuestType(entity);
        }
        public List<XsiStaffGuestType> GetStaffGuestType(XsiStaffGuestType entity, EnumSortlistBy sortListBy)
        {
            return StaffGuestTypeBL.GetStaffGuestType(entity, sortListBy);
        }
        public List<XsiStaffGuestType> GetStaffGuestTypeOr(XsiStaffGuestType entity, EnumSortlistBy sortListBy)
        {
            return StaffGuestTypeBL.GetStaffGuestTypeOr(entity, sortListBy);
        }

        public EnumResultType InsertStaffGuestType(XsiStaffGuestType entity)
        {
            return StaffGuestTypeBL.InsertStaffGuestType(entity);
        }
        public EnumResultType UpdateStaffGuestType(XsiStaffGuestType entity)
        {
            return StaffGuestTypeBL.UpdateStaffGuestType(entity);
        }
        public EnumResultType UpdateStaffGuestTypeStatus(long itemId, long langId)
        {
            return StaffGuestTypeBL.UpdateStaffGuestTypeStatus(itemId, langId);
        }
        public EnumResultType DeleteStaffGuestType(XsiStaffGuestType entity)
        {
            return StaffGuestTypeBL.DeleteStaffGuestType(entity);
        }
        public EnumResultType DeleteStaffGuestType(long itemId)
        {
            return StaffGuestTypeBL.DeleteStaffGuestType(itemId);
        }
        #endregion
    }
}
﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitorService : IDisposable
    {
        #region Feilds
        ExhibitorBL ExhibitorBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitorBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitorBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitorService()
        {
            ExhibitorBL = new ExhibitorBL();
        }
        public ExhibitorService(string connectionString)
        {
            ExhibitorBL = new ExhibitorBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitorBL != null)
                this.ExhibitorBL.Dispose();
        }
        #endregion
        #region Methods
        //public long GetNextGroupId()
        //{
        //    return ExhibitorBL.GetNextGroupId();
        //}
        public XsiExhibitor GetExhibitorByItemId(long itemId)
        {
            return ExhibitorBL.GetExhibitorByItemId(itemId);
        }
        //public List<XsiExhibitor> GetExhibitorByItemId(long Id)
        //{
        //    return ExhibitorBL.GetExhibitorByItemId(Id);
        //}

        public List<XsiExhibitor> GetExhibitor()
        {
            return ExhibitorBL.GetExhibitor();
        }
        public List<XsiExhibitor> GetExhibitor(EnumSortlistBy sortListBy)
        {
            return ExhibitorBL.GetExhibitor(sortListBy);
        }
        public List<XsiExhibitor> GetExhibitor(XsiExhibitor entity)
        {
            return ExhibitorBL.GetExhibitor(entity);
        }
        public List<XsiExhibitor> GetExhibitor(XsiExhibitor entity, EnumSortlistBy sortListBy)
        {
            return ExhibitorBL.GetExhibitor(entity, sortListBy);
        }
        public List<XsiExhibitor> GetExhibitorOr(XsiExhibitor entity, EnumSortlistBy sortListBy)
        {
            return ExhibitorBL.GetExhibitorOr(entity, sortListBy);
        }
        //public XsiExhibitor GetExhibitor(long groupId, long languageId)
        //{
        //    return ExhibitorBL.GetExhibitor(groupId, languageId);
        //}

        public EnumResultType InsertExhibitor(XsiExhibitor entity)
        {
            return ExhibitorBL.InsertExhibitor(entity);
        }
        public EnumResultType UpdateExhibitor(XsiExhibitor entity)
        {
            return ExhibitorBL.UpdateExhibitor(entity);
        }
        public EnumResultType UpdateExhibitorStatus(long itemId)
        {
            return ExhibitorBL.UpdateExhibitorStatus(itemId);
        }
        public EnumResultType DeleteExhibitor(XsiExhibitor entity)
        {
            return ExhibitorBL.DeleteExhibitor(entity);
        }
        public EnumResultType DeleteExhibitor(long itemId)
        {
            return ExhibitorBL.DeleteExhibitor(itemId);
        }
        #endregion
    }
}
﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class CareerVisaStatusService : IDisposable
    {
        #region Feilds
        CareerVisaStatusBL CareerVisaStatusBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return CareerVisaStatusBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return CareerVisaStatusBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public CareerVisaStatusService()
        {
            CareerVisaStatusBL = new CareerVisaStatusBL();
        }
        public CareerVisaStatusService(string connectionString)
        {
            CareerVisaStatusBL = new CareerVisaStatusBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerVisaStatusBL != null)
                this.CareerVisaStatusBL.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiCareerVisaStatus GetCareerVisaStatusByItemId(long itemId)
        {
            return CareerVisaStatusBL.GetCareerVisaStatusByItemId(itemId);
        }
       
        public List<XsiCareerVisaStatus> GetCareerVisaStatus()
        {
            return CareerVisaStatusBL.GetCareerVisaStatus();
        }
        public List<XsiCareerVisaStatus> GetCareerVisaStatus(EnumSortlistBy sortListBy)
        {
            return CareerVisaStatusBL.GetCareerVisaStatus(sortListBy);
        }
        public List<XsiCareerVisaStatus> GetCareerVisaStatus(XsiCareerVisaStatus entity)
        {
            return CareerVisaStatusBL.GetCareerVisaStatus(entity);
        }
        public List<XsiCareerVisaStatus> GetCareerVisaStatus(XsiCareerVisaStatus entity, EnumSortlistBy sortListBy)
        {
            return CareerVisaStatusBL.GetCareerVisaStatus(entity, sortListBy);
        }
        public List<XsiCareerVisaStatus> GetCareerVisaStatusOr(XsiCareerVisaStatus entity, EnumSortlistBy sortListBy)
        {
            return CareerVisaStatusBL.GetCareerVisaStatusOr(entity, sortListBy);
        }
         
        public EnumResultType InsertCareerVisaStatus(XsiCareerVisaStatus entity)
        {
            return CareerVisaStatusBL.InsertCareerVisaStatus(entity);
        }
        public EnumResultType UpdateCareerVisaStatus(XsiCareerVisaStatus entity)
        {
            return CareerVisaStatusBL.UpdateCareerVisaStatus(entity);
        }
        public EnumResultType UpdateCareerVisaStatusStatus(long itemId)
        {
            return CareerVisaStatusBL.UpdateCareerVisaStatusStatus(itemId);
        }
        public EnumResultType DeleteCareerVisaStatus(XsiCareerVisaStatus entity)
        {
            return CareerVisaStatusBL.DeleteCareerVisaStatus(entity);
        }
        public EnumResultType DeleteCareerVisaStatus(long itemId)
        {
            return CareerVisaStatusBL.DeleteCareerVisaStatus(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class AwardsNominationFormService : IDisposable
    {
        #region Fields
        AwardsNominationFormBL AwardsNominationFormBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return AwardsNominationFormBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return AwardsNominationFormBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public AwardsNominationFormService()
        {
            AwardsNominationFormBL = new AwardsNominationFormBL();
        }
        public AwardsNominationFormService(string connectionString)
        {
            AwardsNominationFormBL = new AwardsNominationFormBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AwardsNominationFormBL != null)
                this.AwardsNominationFormBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return AwardsNominationFormBL.GetNextGroupId();
        }
        public XsiAwardsNominationForms GetAwardsNominationFormByItemId(long itemId)
        {
            return AwardsNominationFormBL.GetAwardsNominationFormByItemId(itemId);
        }
         
        public List<XsiAwardsNominationForms> GetAwardsNominationForm()
        {
            return AwardsNominationFormBL.GetAwardsNominationForm();
        }
        public List<XsiAwardsNominationForms> GetAwardsNominationForm(EnumSortlistBy sortListBy)
        {
            return AwardsNominationFormBL.GetAwardsNominationForm(sortListBy);
        }
        public List<XsiAwardsNominationForms> GetAwardsNominationForm(XsiAwardsNominationForms entity)
        {
            return AwardsNominationFormBL.GetAwardsNominationForm(entity);
        }
        public List<XsiAwardsNominationForms> GetAwardsNominationForm(XsiAwardsNominationForms entity, EnumSortlistBy sortListBy)
        {
            return AwardsNominationFormBL.GetAwardsNominationForm(entity, sortListBy);
        }
        public XsiAwardsNominationForms GetAwardsNominationForm(long groupId, long languageId)
        {
            return AwardsNominationFormBL.GetAwardsNominationForm(groupId, languageId);
        }

        public EnumResultType InsertAwardsNominationForm(XsiAwardsNominationForms entity)
        {
            return AwardsNominationFormBL.InsertAwardsNominationForm(entity);
        }
        public EnumResultType UpdateAwardsNominationForm(XsiAwardsNominationForms entity)
        {
            return AwardsNominationFormBL.UpdateAwardsNominationForm(entity);
        }

        public EnumResultType UpdateAwardsNominationFormStatus(long itemId)
        {
            return AwardsNominationFormBL.UpdateAwardsNominationFormStatus(itemId);
        }
        public EnumResultType DeleteAwardsNominationForm(XsiAwardsNominationForms entity)
        {
            return AwardsNominationFormBL.DeleteAwardsNominationForm(entity);
        }
        public EnumResultType DeleteAwardsNominationForm(long itemId)
        {
            return AwardsNominationFormBL.DeleteAwardsNominationForm(itemId);
        }
        #endregion
    }
}
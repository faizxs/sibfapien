﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionBookTypeService : IDisposable
    {
        #region Feilds
        ExhibitionBookTypeBL ExhibitionBookTypeBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionBookTypeBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionBookTypeBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionBookTypeService()
        {
            ExhibitionBookTypeBL = new ExhibitionBookTypeBL();
        }
        public ExhibitionBookTypeService(string connectionString)
        {
            ExhibitionBookTypeBL = new ExhibitionBookTypeBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionBookTypeBL != null)
                this.ExhibitionBookTypeBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionBookType GetExhibitionBookTypeByItemId(long itemId)
        {
            return ExhibitionBookTypeBL.GetExhibitionBookTypeByItemId(itemId);
        }
      
        public List<XsiExhibitionBookType> GetExhibitionBookType()
        {
            return ExhibitionBookTypeBL.GetExhibitionBookType();
        }
        public List<XsiExhibitionBookType> GetExhibitionBookType(EnumSortlistBy sortListBy)
        {
            return ExhibitionBookTypeBL.GetExhibitionBookType(sortListBy);
        }
        public List<XsiExhibitionBookType> GetExhibitionBookType(XsiExhibitionBookType entity)
        {
            return ExhibitionBookTypeBL.GetExhibitionBookType(entity);
        }
        public List<XsiExhibitionBookType> GetExhibitionBookType(XsiExhibitionBookType entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBookTypeBL.GetExhibitionBookType(entity, sortListBy);
        }

        public List<XsiExhibitionBookType> GetExhibitionBookTypeOr(XsiExhibitionBookType entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBookTypeBL.GetExhibitionBookTypeOr(entity, sortListBy);
        }
       
        public EnumResultType InsertExhibitionBookType(XsiExhibitionBookType entity)
        {
            return ExhibitionBookTypeBL.InsertExhibitionBookType(entity);
        }
        public EnumResultType UpdateExhibitionBookType(XsiExhibitionBookType entity)
        {
            return ExhibitionBookTypeBL.UpdateExhibitionBookType(entity);
        }
        public EnumResultType UpdateExhibitionBookTypeStatus(long itemId)
        {
            return ExhibitionBookTypeBL.UpdateExhibitionBookTypeStatus(itemId);
        }
        public EnumResultType DeleteExhibitionBookType(XsiExhibitionBookType entity)
        {
            return ExhibitionBookTypeBL.DeleteExhibitionBookType(entity);
        }
        public EnumResultType DeleteExhibitionBookType(long itemId)
        {
            return ExhibitionBookTypeBL.DeleteExhibitionBookType(itemId);
        }
        #endregion
    }
}
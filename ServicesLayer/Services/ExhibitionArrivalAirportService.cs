﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionArrivalAirportService : IDisposable
    {
        #region Feilds
        ExhibitionArrivalAirportBL ExhibitionArrivalAirportBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return ExhibitionArrivalAirportBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public ExhibitionArrivalAirportService()
        {
            ExhibitionArrivalAirportBL = new ExhibitionArrivalAirportBL();
        }
        public ExhibitionArrivalAirportService(string connectionString)
        {
            ExhibitionArrivalAirportBL = new ExhibitionArrivalAirportBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionArrivalAirportBL != null)
                this.ExhibitionArrivalAirportBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionArrivalAirport GetExhibitionArrivalAirportByItemId(long itemId)
        {
            return ExhibitionArrivalAirportBL.GetExhibitionArrivalAirportByItemId(itemId);
        }

        public List<XsiExhibitionArrivalAirport> GetExhibitionArrivalAirport()
        {
            return ExhibitionArrivalAirportBL.GetExhibitionArrivalAirport();
        }
        public List<XsiExhibitionArrivalAirport> GetExhibitionArrivalAirport(EnumSortlistBy sortListBy)
        {
            return ExhibitionArrivalAirportBL.GetExhibitionArrivalAirport(sortListBy);
        }
        public List<XsiExhibitionArrivalAirport> GetExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity)
        {
            return ExhibitionArrivalAirportBL.GetExhibitionArrivalAirport(entity);
        }
        public List<XsiExhibitionArrivalAirport> GetExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionArrivalAirportBL.GetExhibitionArrivalAirport(entity, sortListBy);
        }
        public List<XsiExhibitionArrivalAirport> GetExhibitionArrivalAirportOr(XsiExhibitionArrivalAirport entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionArrivalAirportBL.GetExhibitionArrivalAirportOr(entity, sortListBy);
        }
        public List<XsiExhibitionArrivalAirport> GetOtherLanguageExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity)
        {
            return ExhibitionArrivalAirportBL.GetOtherLanguageExhibitionArrivalAirport(entity);
        }
        public List<XsiExhibitionArrivalAirport> GetCurrentLanguageExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity)
        {
            return ExhibitionArrivalAirportBL.GetCurrentLanguageExhibitionArrivalAirport(entity);
        }
        public List<XsiExhibitionArrivalAirport> GetCompleteExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionArrivalAirportBL.GetCompleteExhibitionArrivalAirport(entity, sortListBy);
        }

        public EnumResultType InsertExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity)
        {
            return ExhibitionArrivalAirportBL.InsertExhibitionArrivalAirport(entity);
        }
        public EnumResultType UpdateExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity)
        {
            return ExhibitionArrivalAirportBL.UpdateExhibitionArrivalAirport(entity);
        }
        public EnumResultType UpdateExhibitionArrivalAirportStatus(long itemId, long langId)
        {
            return ExhibitionArrivalAirportBL.UpdateExhibitionArrivalAirportStatus(itemId, langId);
        }
        public EnumResultType DeleteExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity)
        {
            return ExhibitionArrivalAirportBL.DeleteExhibitionArrivalAirport(entity);
        }
        public EnumResultType DeleteExhibitionArrivalAirport(long itemId)
        {
            return ExhibitionArrivalAirportBL.DeleteExhibitionArrivalAirport(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ProfessionalProgramService : IDisposable
    {
        #region Feilds
        ProfessionalProgramBL ProfessionalProgramBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ProfessionalProgramBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ProfessionalProgramBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ProfessionalProgramService()
        {
            ProfessionalProgramBL = new ProfessionalProgramBL();
        }
        public ProfessionalProgramService(string connectionString)
        {
            ProfessionalProgramBL = new ProfessionalProgramBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ProfessionalProgramBL != null)
                this.ProfessionalProgramBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionProfessionalProgram GetProfessionalProgramByItemId(long itemId)
        {
            return ProfessionalProgramBL.GetProfessionalProgramByItemId(itemId);
        }
         
        public List<XsiExhibitionProfessionalProgram> GetProfessionalProgram()
        {
            return ProfessionalProgramBL.GetProfessionalProgram();
        }
        public List<XsiExhibitionProfessionalProgram> GetProfessionalProgram(EnumSortlistBy sortListBy)
        {
            return ProfessionalProgramBL.GetProfessionalProgram(sortListBy);
        }
        public List<XsiExhibitionProfessionalProgram> GetProfessionalProgram(XsiExhibitionProfessionalProgram entity)
        {
            return ProfessionalProgramBL.GetProfessionalProgram(entity);
        }
        public List<XsiExhibitionProfessionalProgram> GetProfessionalProgram(XsiExhibitionProfessionalProgram entity, EnumSortlistBy sortListBy)
        {
            return ProfessionalProgramBL.GetProfessionalProgram(entity, sortListBy);
        }
         
        public EnumResultType InsertProfessionalProgram(XsiExhibitionProfessionalProgram entity)
        {
            return ProfessionalProgramBL.InsertProfessionalProgram(entity);
        }
        public EnumResultType UpdateProfessionalProgram(XsiExhibitionProfessionalProgram entity)
        {
            return ProfessionalProgramBL.UpdateProfessionalProgram(entity);
        }
        public EnumResultType UpdateProfessionalProgramStatus(long itemId)
        {
            return ProfessionalProgramBL.UpdateProfessionalProgramStatus(itemId);
        }
        public EnumResultType DeleteProfessionalProgram(XsiExhibitionProfessionalProgram entity)
        {
            return ProfessionalProgramBL.DeleteProfessionalProgram(entity);
        }
        public EnumResultType DeleteProfessionalProgram(long itemId)
        {
            return ProfessionalProgramBL.DeleteProfessionalProgram(itemId);
        }
        #endregion
    }
}
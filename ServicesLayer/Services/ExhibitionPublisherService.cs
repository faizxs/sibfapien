﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionPublisherService : IDisposable
    {
        #region Feilds
        ExhibitionPublisherBL ExhibitionPublisherBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionPublisherBL.XsiItemdId; }
        }
        #endregion
        #region Constructor
        public ExhibitionPublisherService()
        {
            ExhibitionPublisherBL = new ExhibitionPublisherBL();
        }
        public ExhibitionPublisherService(string connectionString)
        {
            ExhibitionPublisherBL = new ExhibitionPublisherBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionPublisherBL != null)
                this.ExhibitionPublisherBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionBookPublisher GetExhibitionPublisherByItemId(long itemId)
        {
            return ExhibitionPublisherBL.GetExhibitionPublisherByItemId(itemId);
        }
        
        public List<XsiExhibitionBookPublisher> GetExhibitionPublisher()
        {
            return ExhibitionPublisherBL.GetExhibitionPublisher();
        }
        public List<XsiExhibitionBookPublisher> GetExhibitionPublisher(EnumSortlistBy sortListBy)
        {
            return ExhibitionPublisherBL.GetExhibitionPublisher(sortListBy);
        }
        public List<XsiExhibitionBookPublisher> GetExhibitionPublisher(XsiExhibitionBookPublisher entity)
        {
            return ExhibitionPublisherBL.GetExhibitionPublisher(entity);
        }

        public Array GetExhibitionPublisherForUI(XsiExhibitionBookPublisher entity, long exhibitionGroupID)
        {
            return ExhibitionPublisherBL.GetExhibitionPublisherForUI(entity, exhibitionGroupID);
        }
        public List<XsiExhibitionBookPublisher> GetExhibitionPublisherForSCRFUI(XsiExhibitionBookPublisher entity, long exhibitionGroupID)
        {
            return ExhibitionPublisherBL.GetExhibitionPublisherForSCRFUI(entity, exhibitionGroupID);
        }
        public List<XsiExhibitionBookPublisher> GetExhibitionPublisher(XsiExhibitionBookPublisher entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionPublisherBL.GetExhibitionPublisher(entity, sortListBy);
        }

        public EnumResultType InsertExhibitionPublisher(XsiExhibitionBookPublisher entity)
        {
            return ExhibitionPublisherBL.InsertExhibitionPublisher(entity);
        }
        public EnumResultType UpdateExhibitionPublisher(XsiExhibitionBookPublisher entity)
        {
            return ExhibitionPublisherBL.UpdateExhibitionPublisher(entity);
        }
        public EnumResultType UpdateExhibitionPublisherStatus(long itemId)
        {
            return ExhibitionPublisherBL.UpdateExhibitionPublisherStatus(itemId);
        }
        public EnumResultType DeleteExhibitionPublisher(XsiExhibitionBookPublisher entity)
        {
            return ExhibitionPublisherBL.DeleteExhibitionPublisher(entity);
        }
        public EnumResultType DeleteExhibitionPublisher(long itemId)
        {
            return ExhibitionPublisherBL.DeleteExhibitionPublisher(itemId);
        }
        #endregion
        //Custom
        public long GetNameCount(long itemID, string title, string titlear)
        {
            return ExhibitionPublisherBL.GetNameCount(itemID, title, titlear);
        }
    }
}
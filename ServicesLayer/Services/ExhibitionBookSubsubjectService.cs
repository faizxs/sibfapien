﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionBookSubsubjectService : IDisposable
    {
        #region Feilds
        ExhibitionBookSubsubjectBL ExhibitionBookSubsubjectBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionBookSubsubjectBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionBookSubsubjectBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionBookSubsubjectService()
        {
            ExhibitionBookSubsubjectBL = new ExhibitionBookSubsubjectBL();
        }
        public ExhibitionBookSubsubjectService(string connectionString)
        {
            ExhibitionBookSubsubjectBL = new ExhibitionBookSubsubjectBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionBookSubsubjectBL != null)
                this.ExhibitionBookSubsubjectBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionBookSubsubject GetExhibitionBookSubsubjectByItemId(long itemId)
        {
            return ExhibitionBookSubsubjectBL.GetExhibitionBookSubsubjectByItemId(itemId);
        }
         
        public List<XsiExhibitionBookSubsubject> GetExhibitionBookSubsubject()
        {
            return ExhibitionBookSubsubjectBL.GetExhibitionBookSubsubject();
        }
        public List<XsiExhibitionBookSubsubject> GetExhibitionBookSubsubject(EnumSortlistBy sortListBy)
        {
            return ExhibitionBookSubsubjectBL.GetExhibitionBookSubsubject(sortListBy);
        }
        public List<XsiExhibitionBookSubsubject> GetExhibitionBookSubsubject(XsiExhibitionBookSubsubject entity)
        {
            return ExhibitionBookSubsubjectBL.GetExhibitionBookSubsubject(entity);
        }
        public List<XsiExhibitionBookSubsubject> GetExhibitionBookSubsubject(XsiExhibitionBookSubsubject entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBookSubsubjectBL.GetExhibitionBookSubsubject(entity, sortListBy);
        }
        public List<XsiExhibitionBookSubsubject> GetCompleteExhibitionBookSubsubject(XsiExhibitionBookSubsubject entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBookSubsubjectBL.GetCompleteExhibitionBookSubsubject(entity, sortListBy);
        }
        
        public EnumResultType InsertExhibitionBookSubsubject(XsiExhibitionBookSubsubject entity)
        {
            return ExhibitionBookSubsubjectBL.InsertExhibitionBookSubsubject(entity);
        }
        public EnumResultType UpdateExhibitionBookSubsubject(XsiExhibitionBookSubsubject entity)
        {
            return ExhibitionBookSubsubjectBL.UpdateExhibitionBookSubsubject(entity);
        }
        public EnumResultType UpdateExhibitionBookSubsubjectStatus(long itemId)
        {
            return ExhibitionBookSubsubjectBL.UpdateExhibitionBookSubsubjectStatus(itemId);
        }
        public EnumResultType DeleteExhibitionBookSubsubject(XsiExhibitionBookSubsubject entity)
        {
            return ExhibitionBookSubsubjectBL.DeleteExhibitionBookSubsubject(entity);
        }
        public EnumResultType DeleteExhibitionBookSubsubject(long itemId)
        {
            return ExhibitionBookSubsubjectBL.DeleteExhibitionBookSubsubject(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class AgencyRegistrationService : IDisposable
    {
        #region Feilds
        AgencyRegistrationBL AgencyRegistrationBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return AgencyRegistrationBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return AgencyRegistrationBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public AgencyRegistrationService()
        {
            AgencyRegistrationBL = new AgencyRegistrationBL();
        }
        public AgencyRegistrationService(string connectionString)
        {
            AgencyRegistrationBL = new AgencyRegistrationBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AgencyRegistrationBL != null)
                this.AgencyRegistrationBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionMemberApplicationYearly GetAgencyRegistrationByItemId(long itemId)
        {
            return AgencyRegistrationBL.GetAgencyRegistrationByItemId(itemId);
        }
         
        public List<XsiExhibitionMemberApplicationYearly> GetAgencyRegistration()
        {
            return AgencyRegistrationBL.GetAgencyRegistration();
        }
        public List<XsiExhibitionMemberApplicationYearly> GetAgencyRegistration(EnumSortlistBy sortListBy)
        {
            return AgencyRegistrationBL.GetAgencyRegistration(sortListBy);
        }
        public List<XsiExhibitionMemberApplicationYearly> GetAgencyRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            return AgencyRegistrationBL.GetAgencyRegistration(entity);
        }
        public List<XsiExhibitionMemberApplicationYearly> GetAgencyRegistration(XsiExhibitionMemberApplicationYearly entity, EnumSortlistBy sortListBy)
        {
            return AgencyRegistrationBL.GetAgencyRegistration(entity, sortListBy);
        }
        public List<XsiExhibitionMemberApplicationYearly> GetAgencyRegistrationExact(XsiExhibitionMemberApplicationYearly entity)
        {
            return AgencyRegistrationBL.GetAgencyRegistrationExact(entity);
        }

        public List<string> GetAgencyRegistrationAgencyName(XsiExhibitionMemberApplicationYearly entity)
        {
            return AgencyRegistrationBL.GetAgencyRegistrationAgencyName(entity);
        }
        public List<string> GetAgencyRegistrationAgencyNameAr(XsiExhibitionMemberApplicationYearly entity)
        {
            return AgencyRegistrationBL.GetAgencyRegistrationAgencyNameAr(entity);
        }
        public EnumResultType InsertAgencyRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            return AgencyRegistrationBL.InsertAgencyRegistration(entity);
        }
        public EnumResultType UpdateAgencyRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            return AgencyRegistrationBL.UpdateAgencyRegistration(entity);
        }
        public EnumResultType UpdateAgencyRegistrationStatus(long itemId)
        {
            return AgencyRegistrationBL.UpdateAgencyRegistrationStatus(itemId);
        }
        public EnumResultType DeleteAgencyRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            return AgencyRegistrationBL.DeleteAgencyRegistration(entity);
        }
        public EnumResultType DeleteAgencyRegistration(long itemId)
        {
            return AgencyRegistrationBL.DeleteAgencyRegistration(itemId);
        }
        #endregion
    }
}
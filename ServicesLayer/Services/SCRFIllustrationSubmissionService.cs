﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFIllustrationSubmissionService : IDisposable
    {
        #region Feilds
        SCRFIllustrationSubmissionBL SCRFIllustrationSubmissionBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFIllustrationSubmissionBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFIllustrationSubmissionBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFIllustrationSubmissionService()
        {
            SCRFIllustrationSubmissionBL = new SCRFIllustrationSubmissionBL();
        }
        public SCRFIllustrationSubmissionService(string connectionString)
        {
            SCRFIllustrationSubmissionBL = new SCRFIllustrationSubmissionBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIllustrationSubmissionBL != null)
                this.SCRFIllustrationSubmissionBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfillustrationSubmission GetSCRFIllustrationSubmissionByItemId(long itemId)
        {
            return SCRFIllustrationSubmissionBL.GetSCRFIllustrationSubmissionByItemId(itemId);
        }

        public List<XsiScrfillustrationSubmission> GetSCRFIllustrationSubmission()
        {
            return SCRFIllustrationSubmissionBL.GetSCRFIllustrationSubmission();
        }
        public List<XsiScrfillustrationSubmission> GetSCRFIllustrationSubmission(EnumSortlistBy sortListBy)
        {
            return SCRFIllustrationSubmissionBL.GetSCRFIllustrationSubmission(sortListBy);
        }
        public List<XsiScrfillustrationSubmission> GetSCRFIllustrationSubmission(XsiScrfillustrationSubmission entity)
        {
            return SCRFIllustrationSubmissionBL.GetSCRFIllustrationSubmission(entity);
        }
        public List<XsiScrfillustrationSubmission> GetSCRFIllustrationSubmission(XsiScrfillustrationSubmission entity, EnumSortlistBy sortListBy)
        {
            return SCRFIllustrationSubmissionBL.GetSCRFIllustrationSubmission(entity, sortListBy);
        }

        public EnumResultType InsertSCRFIllustrationSubmission(XsiScrfillustrationSubmission entity)
        {
            return SCRFIllustrationSubmissionBL.InsertSCRFIllustrationSubmission(entity);
        }
        public EnumResultType UpdateSCRFIllustrationSubmission(XsiScrfillustrationSubmission entity)
        {
            return SCRFIllustrationSubmissionBL.UpdateSCRFIllustrationSubmission(entity);
        }
        public EnumResultType UpdateSCRFIllustrationSubmissionStatus(long itemId)
        {
            return SCRFIllustrationSubmissionBL.UpdateSCRFIllustrationSubmissionStatus(itemId);
        }
        public EnumResultType DeleteSCRFIllustrationSubmission(XsiScrfillustrationSubmission entity)
        {
            return SCRFIllustrationSubmissionBL.DeleteSCRFIllustrationSubmission(entity);
        }
        public EnumResultType DeleteSCRFIllustrationSubmission(long itemId)
        {
            return SCRFIllustrationSubmissionBL.DeleteSCRFIllustrationSubmission(itemId);
        }

        //Custom 
        public long GetHomeBannerInfo(long languageid)
        {
            return SCRFIllustrationSubmissionBL.GetHomeBannerInfo(languageid);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;
using System.Web;

namespace Xsi.ServicesLayer
{
    public class BookService : IDisposable
    {
        #region Feilds
        BookBL BookBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return BookBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return BookBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public BookService()
        {
            BookBL = new BookBL();
        }
        public BookService(string connectionString)
        {
            BookBL = new BookBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.BookBL != null)
                this.BookBL.Dispose();
        }
        #endregion
        #region Methods
       
        public long GetNextClickCount(long itemId)
        {
            return BookBL.GetNextClickCount(itemId);
        }
        public XsiExhibitionBooks GetBookByItemId(long itemId)
        {
            return BookBL.GetBookByItemId(itemId);
        }
        
        public List<XsiExhibitionBooks> GetBook()
        {
            return BookBL.GetBook();
        }
        public List<XsiExhibitionBooks> GetBook(EnumSortlistBy sortListBy)
        {
            return BookBL.GetBook(sortListBy);
        }
        public List<XsiExhibitionBooks> GetBook(XsiExhibitionBooks entity)
        {
            return BookBL.GetBook(entity);
        }

        public List<XsiExhibitionBooks> GetXsiExhibitionBooksByExhibitorId(long ehibitorId)
        {
            return BookBL.GetXsiExhibitionBooksByExhibitorId(ehibitorId);
        }
       
        public List<XsiExhibitionBookForUi> GetBookForUI(XsiExhibitionBooks entity, EnumSortlistBy sortListBy)
        {
            return BookBL.GetBookForUI(entity, sortListBy);
        }
        public List<XsiExhibitionBookForUi> GetBookUIAnd(XsiExhibitionBooks entity, EnumSortlistBy sortListBy, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            return BookBL.GetBookUIAnd(entity, sortListBy, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId);
        }
        public List<XsiExhibitionBookForUi> GetBookUIOr(XsiExhibitionBooks entity, EnumSortlistBy sortListBy, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            return BookBL.GetBookUIOr(entity, sortListBy, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId);
        }
        public List<XsiExhibitionBookForUi> GetBookUITopSearchable(XsiExhibitionBooks entity, long websiteId)
        {
            return BookBL.GetBookUITopSearchable(entity, websiteId);
        }
        public Array GetBooksTitle(XsiExhibitionBooks entity, long websiteId, EnumSortlistBy sortListBy)
        {
            return BookBL.GetBooksTitle(entity, websiteId, sortListBy);
        }
        public List<XsiExhibitionBookForUi> GetBooksSCRFTitle(XsiExhibitionBooks entity, long websiteId, EnumSortlistBy sortListBy)
        {
            return BookBL.GetBooksSCRFTitle(entity, websiteId, sortListBy);
        }
        public List<XsiExhibitionBooks> GetBook(XsiExhibitionBooks entity, EnumSortlistBy sortListBy)
        {
            return BookBL.GetBook(entity, sortListBy).ToList();
        }
         
        public List<GetBooksToExport_Result> GetBooksToExport(string bookIds, string languageId)
        {
            return BookBL.GetBooksToExport(bookIds, languageId).ToList();
        }

        public EnumResultType InsertBook(XsiExhibitionBooks entity)
        {
            return BookBL.InsertBook(entity);
        }
        public EnumResultType UpdateBook(XsiExhibitionBooks entity)
        {
            return BookBL.UpdateBook(entity);
        }
        public void UpdatePublisherId(long? oldPublisherID, long? newPublisherID)
        {
            BookBL.UpdatePublisherId(oldPublisherID, newPublisherID);
        }
        public void UpdateAuthorId(long? oldAuthorID, long? newAuthorID)
        {
            BookBL.UpdateAuthorId(oldAuthorID, newAuthorID);
        }
        public EnumResultType UpdateBookStatus(long itemId)
        {
            return BookBL.UpdateBookStatus(itemId);
        }
        public EnumResultType DeleteBook(XsiExhibitionBooks entity)
        {
            return BookBL.DeleteBook(entity);
        }
        public EnumResultType DeleteBook(long itemId)
        {
            return BookBL.DeleteBook(itemId);
        }

        public List<XsiExhibitionBookForUi> GetBookUIAnd1(XsiExhibitionBooks entity, EnumSortlistBy sortListBy, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            return BookBL.GetBookUIAnd1(entity, sortListBy, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId);
        }

        public void UpdateAuthorInBooks(long oldAuthorId, long newAuthorId, long AdminUserId)
        {
            BookBL.UpdateAuthorInBooks(oldAuthorId, newAuthorId, AdminUserId);
        }

        public void UpdatePublisherInBooks(long oldPublisherId, long newPublisherId, long AdminUserId)
        {
            BookBL.UpdatePublisherInBooks(oldPublisherId, newPublisherId, AdminUserId);
        }
        #endregion
    }
}
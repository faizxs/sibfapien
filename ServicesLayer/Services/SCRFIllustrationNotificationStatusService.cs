﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFIllustrationNotificationStatusService : IDisposable
    {
        #region Feilds
        SCRFIllustrationNotificationStatusBL SCRFIllustrationNotificationStatusBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFIllustrationNotificationStatusBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFIllustrationNotificationStatusBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFIllustrationNotificationStatusService()
        {
            SCRFIllustrationNotificationStatusBL = new SCRFIllustrationNotificationStatusBL();
        }
        public SCRFIllustrationNotificationStatusService(string connectionString)
        {
            SCRFIllustrationNotificationStatusBL = new SCRFIllustrationNotificationStatusBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIllustrationNotificationStatusBL != null)
                this.SCRFIllustrationNotificationStatusBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return SCRFIllustrationNotificationStatusBL.GetNextGroupId();
        }
        public XsiScrfillustrationNotificationStatus GetSCRFIllustrationNotificationStatusByItemId(long itemId)
        {
            return SCRFIllustrationNotificationStatusBL.GetSCRFIllustrationNotificationStatusByItemId(itemId);
        }

        public List<XsiScrfillustrationNotificationStatus> GetSCRFIllustrationNotificationStatus()
        {
            return SCRFIllustrationNotificationStatusBL.GetSCRFIllustrationNotificationStatus();
        }
        public List<XsiScrfillustrationNotificationStatus> GetSCRFIllustrationNotificationStatus(EnumSortlistBy sortListBy)
        {
            return SCRFIllustrationNotificationStatusBL.GetSCRFIllustrationNotificationStatus(sortListBy);
        }
        public List<XsiScrfillustrationNotificationStatus> GetSCRFIllustrationNotificationStatus(XsiScrfillustrationNotificationStatus entity)
        {
            return SCRFIllustrationNotificationStatusBL.GetSCRFIllustrationNotificationStatus(entity);
        }
        public List<XsiScrfillustrationNotificationStatus> GetSCRFIllustrationNotificationStatus(XsiScrfillustrationNotificationStatus entity, EnumSortlistBy sortListBy)
        {
            return SCRFIllustrationNotificationStatusBL.GetSCRFIllustrationNotificationStatus(entity, sortListBy);
        }
        public XsiScrfillustrationNotificationStatus GetSCRFIllustrationNotificationStatus(long groupId, long languageId)
        {
            return SCRFIllustrationNotificationStatusBL.GetSCRFIllustrationNotificationStatus(groupId, languageId);
        }

        public EnumResultType InsertSCRFIllustrationNotificationStatus(XsiScrfillustrationNotificationStatus entity)
        {
            return SCRFIllustrationNotificationStatusBL.InsertSCRFIllustrationNotificationStatus(entity);
        }
        public EnumResultType UpdateSCRFIllustrationNotificationStatus(XsiScrfillustrationNotificationStatus entity)
        {
            return SCRFIllustrationNotificationStatusBL.UpdateSCRFIllustrationNotificationStatus(entity);
        }
       
        public EnumResultType DeleteSCRFIllustrationNotificationStatus(XsiScrfillustrationNotificationStatus entity)
        {
            return SCRFIllustrationNotificationStatusBL.DeleteSCRFIllustrationNotificationStatus(entity);
        }
        public EnumResultType DeleteSCRFIllustrationNotificationStatus(long itemId)
        {
            return SCRFIllustrationNotificationStatusBL.DeleteSCRFIllustrationNotificationStatus(itemId);
        }

        #endregion
    }
}
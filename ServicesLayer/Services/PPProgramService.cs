﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PPProgramService : IDisposable
    {
        #region Feilds
        PPProgramBL PPProgramBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PPProgramBL.XsiItemdId; }
        }
        #endregion
        #region Constructor
        public PPProgramService()
        {
            PPProgramBL = new PPProgramBL();
        }
        public PPProgramService(string connectionString)
        {
            PPProgramBL = new PPProgramBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PPProgramBL != null)
                this.PPProgramBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionPpprogram GetPPProgramByItemId(long itemId)
        {
            return PPProgramBL.GetPPProgramByItemId(itemId);
        }
        public List<XsiExhibitionPpprogram> GetPPProgram()
        {
            return PPProgramBL.GetPPProgram();
        }
        public List<XsiExhibitionPpprogram> GetPPProgram(EnumSortlistBy sortListBy)
        {
            return PPProgramBL.GetPPProgram(sortListBy);
        }
        public List<XsiExhibitionPpprogram> GetPPProgram(XsiExhibitionPpprogram entity)
        {
            return PPProgramBL.GetPPProgram(entity);
        }
        public List<XsiExhibitionPpprogram> GetPPProgram(XsiExhibitionPpprogram entity, EnumSortlistBy sortListBy)
        {
            return PPProgramBL.GetPPProgram(entity, sortListBy);
        }
        public EnumResultType InsertPPProgram(XsiExhibitionPpprogram entity)
        {
            return PPProgramBL.InsertPPProgram(entity);
        }
        public EnumResultType UpdatePPProgram(XsiExhibitionPpprogram entity)
        {
            return PPProgramBL.UpdatePPProgram(entity);
        }

        public EnumResultType DeletePPProgram(XsiExhibitionPpprogram entity)
        {
            return PPProgramBL.DeletePPProgram(entity);
        }
        public EnumResultType DeletePPProgram(long itemId)
        {
            return PPProgramBL.DeletePPProgram(itemId);
        }
        #endregion
    }
}
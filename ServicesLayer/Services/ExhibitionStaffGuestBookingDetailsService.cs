﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionStaffGuestBookingDetailsService : IDisposable
    {
        #region Feilds
        ExhibitionStaffGuestBookingDetailsBL ExhibitionStaffGuestBookingDetailsBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionStaffGuestBookingDetailsBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionStaffGuestBookingDetailsBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestBookingDetailsService()
        {
            ExhibitionStaffGuestBookingDetailsBL = new ExhibitionStaffGuestBookingDetailsBL();
        }
        public ExhibitionStaffGuestBookingDetailsService(string connectionString)
        {
            ExhibitionStaffGuestBookingDetailsBL = new ExhibitionStaffGuestBookingDetailsBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionStaffGuestBookingDetailsBL != null)
                this.ExhibitionStaffGuestBookingDetailsBL.Dispose();
        }
        #endregion
        #region Methods 
        public XsiExhibitionStaffGuestBookingDetails GetExhibitionStaffGuestBookingDetailsByItemId(long itemId)
        {
            return ExhibitionStaffGuestBookingDetailsBL.GetExhibitionStaffGuestBookingDetailsByItemId(itemId);
        }

        public List<XsiExhibitionStaffGuestBookingDetails> GetExhibitionStaffGuestBookingDetails()
        {
            return ExhibitionStaffGuestBookingDetailsBL.GetExhibitionStaffGuestBookingDetails();
        }
      
        public List<XsiExhibitionStaffGuestBookingDetails> GetExhibitionStaffGuestBookingDetails(EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestBookingDetailsBL.GetExhibitionStaffGuestBookingDetails(sortListBy);
        }
        public List<XsiExhibitionStaffGuestBookingDetails> GetExhibitionStaffGuestBookingDetails(XsiExhibitionStaffGuestBookingDetails entity)
        {
            return ExhibitionStaffGuestBookingDetailsBL.GetExhibitionStaffGuestBookingDetails(entity);
        }
        public List<XsiExhibitionStaffGuestBookingDetails> GetExhibitionStaffGuestBookingDetails(XsiExhibitionStaffGuestBookingDetails entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestBookingDetailsBL.GetExhibitionStaffGuestBookingDetails(entity, sortListBy);
        }
        public XsiExhibitionStaffGuestBookingDetails GetExhibitionStaffGuestBookingDetails(long groupId, long languageId)
        {
            return ExhibitionStaffGuestBookingDetailsBL.GetExhibitionStaffGuestBookingDetails(groupId, languageId);
        }

        public EnumResultType InsertExhibitionStaffGuestBookingDetails(XsiExhibitionStaffGuestBookingDetails entity)
        {
            return ExhibitionStaffGuestBookingDetailsBL.InsertExhibitionStaffGuestBookingDetails(entity);
        }
        public EnumResultType UpdateExhibitionStaffGuestBookingDetails(XsiExhibitionStaffGuestBookingDetails entity)
        {
            return ExhibitionStaffGuestBookingDetailsBL.UpdateExhibitionStaffGuestBookingDetails(entity);
        }
        public EnumResultType RollbackExhibitionStaffGuestBookingDetails(long itemId)
        {
            return ExhibitionStaffGuestBookingDetailsBL.RollbackExhibitionStaffGuestBookingDetails(itemId);
        }
        public EnumResultType UpdateExhibitionStaffGuestBookingDetailsStatus(long itemId)
        {
            return ExhibitionStaffGuestBookingDetailsBL.UpdateExhibitionStaffGuestBookingDetailsStatus(itemId);
        }
        public EnumResultType DeleteExhibitionStaffGuestBookingDetails(XsiExhibitionStaffGuestBookingDetails entity)
        {
            return ExhibitionStaffGuestBookingDetailsBL.DeleteExhibitionStaffGuestBookingDetails(entity);
        }
        public EnumResultType DeleteExhibitionStaffGuestBookingDetails(long itemId)
        {
            return ExhibitionStaffGuestBookingDetailsBL.DeleteExhibitionStaffGuestBookingDetails(itemId);
        }
        #endregion
    }
}
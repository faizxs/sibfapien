﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFPressReleaseService : IDisposable
    {
        #region Feilds
        SCRFPressReleaseBL SCRFPressReleaseBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFPressReleaseBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFPressReleaseBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFPressReleaseService()
        {
            SCRFPressReleaseBL = new SCRFPressReleaseBL();
        }
        public SCRFPressReleaseService(string connectionString)
        {
            SCRFPressReleaseBL = new SCRFPressReleaseBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFPressReleaseBL != null)
                this.SCRFPressReleaseBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfpressRelease GetSCRFPressReleaseByItemId(long itemId)
        {
            return SCRFPressReleaseBL.GetSCRFPressReleaseByItemId(itemId);
        }

        public List<XsiScrfpressRelease> GetSCRFPressRelease()
        {
            return SCRFPressReleaseBL.GetSCRFPressRelease();
        }
        public List<XsiScrfpressRelease> GetSCRFPressRelease(EnumSortlistBy sortListBy)
        {
            return SCRFPressReleaseBL.GetSCRFPressRelease(sortListBy);
        }
        public List<XsiScrfpressRelease> GetSCRFPressRelease(XsiScrfpressRelease entity)
        {
            return SCRFPressReleaseBL.GetSCRFPressRelease(entity);
        }
        public List<XsiScrfpressRelease> GetSCRFPressRelease(XsiScrfpressRelease entity, EnumSortlistBy sortListBy)
        {
            return SCRFPressReleaseBL.GetSCRFPressRelease(entity, sortListBy);
        }
        public List<XsiScrfpressRelease> GetSCRFPressReleaseOr(XsiScrfpressRelease entity, EnumSortlistBy sortListBy)
        {
            return SCRFPressReleaseBL.GetSCRFPressReleaseOr(entity, sortListBy);
        }

        public EnumResultType InsertSCRFPressRelease(XsiScrfpressRelease entity)
        {
            return SCRFPressReleaseBL.InsertSCRFPressRelease(entity);
        }
        public EnumResultType UpdateSCRFPressRelease(XsiScrfpressRelease entity)
        {
            return SCRFPressReleaseBL.UpdateSCRFPressRelease(entity);
        }

        public EnumResultType UpdateSCRFPressReleaseStatus(long itemId)
        {
            return SCRFPressReleaseBL.UpdateSCRFPressReleaseStatus(itemId);
        }
        public EnumResultType DeleteSCRFPressRelease(XsiScrfpressRelease entity)
        {
            return SCRFPressReleaseBL.DeleteSCRFPressRelease(entity);
        }
        public EnumResultType DeleteSCRFPressRelease(long itemId)
        {
            return SCRFPressReleaseBL.DeleteSCRFPressRelease(itemId);
        }
        #endregion
    }
}
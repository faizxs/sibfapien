﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SubAwardsService : IDisposable
    {
        #region Feilds
        SubAwardsBL SubAwardsBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SubAwardsBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SubAwardsBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SubAwardsService()
        {
            SubAwardsBL = new SubAwardsBL();
        }
        public SubAwardsService(string connectionString)
        {
            SubAwardsBL = new SubAwardsBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SubAwardsBL != null)
                this.SubAwardsBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiSubAwards GetSubAwardsByItemId(long itemId)
        {
            return SubAwardsBL.GetSubAwardsByItemId(itemId);
        }
         
        public List<XsiSubAwards> GetSubAwards()
        {
            return SubAwardsBL.GetSubAwards();
        }
        public List<XsiSubAwards> GetSubAwards(EnumSortlistBy sortListBy)
        {
            return SubAwardsBL.GetSubAwards(sortListBy);
        }
        public List<XsiSubAwards> GetSubAwards(XsiSubAwards entity)
        {
            return SubAwardsBL.GetSubAwards(entity);
        }
        public List<XsiSubAwards> GetSubAwards(XsiSubAwards entity, EnumSortlistBy sortListBy)
        {
            return SubAwardsBL.GetSubAwards(entity, sortListBy);
        }
        public List<XsiSubAwards> GetCompleteSubAwards(XsiSubAwards entity, EnumSortlistBy sortListBy)
        {
            return SubAwardsBL.GetCompleteSubAwards(entity, sortListBy);
        }
         
        public EnumResultType InsertSubAwards(XsiSubAwards entity)
        {
            return SubAwardsBL.InsertSubAwards(entity);
        }
        public EnumResultType UpdateSubAwards(XsiSubAwards entity)
        {
            return SubAwardsBL.UpdateSubAwards(entity);
        }
        public EnumResultType UpdateSubAwardsStatus(long itemId)
        {
            return SubAwardsBL.UpdateSubAwardsStatus(itemId);
        }
        public EnumResultType DeleteSubAwards(XsiSubAwards entity)
        {
            return SubAwardsBL.DeleteSubAwards(entity);
        }
        public EnumResultType DeleteSubAwards(long itemId)
        {
            return SubAwardsBL.DeleteSubAwards(itemId);
        }
        #endregion
    }
}
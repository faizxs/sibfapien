﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFIllustrationRegistrationService : IDisposable
    {
        #region Feilds
        SCRFIllustrationRegistrationBL SCRFIllustrationRegistrationBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFIllustrationRegistrationBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFIllustrationRegistrationBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFIllustrationRegistrationService()
        {
            SCRFIllustrationRegistrationBL = new SCRFIllustrationRegistrationBL();
        }
        public SCRFIllustrationRegistrationService(string connectionString)
        {
            SCRFIllustrationRegistrationBL = new SCRFIllustrationRegistrationBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIllustrationRegistrationBL != null)
                this.SCRFIllustrationRegistrationBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfillustrationRegistration GetSCRFIllustrationRegistrationByItemId(long itemId)
        {
            return SCRFIllustrationRegistrationBL.GetSCRFIllustrationRegistrationByItemId(itemId);
        }
        public XsiScrfillustrationRegistration GetSCRFIllustrationRegistrationByEmail(string email)
        {
            return SCRFIllustrationRegistrationBL.GetSCRFIllustrationRegistrationByEmail(email);
        }

        public List<XsiScrfillustrationRegistration> GetSCRFIllustrationRegistration()
        {
            return SCRFIllustrationRegistrationBL.GetSCRFIllustrationRegistration();
        }
        public List<XsiScrfillustrationRegistration> GetSCRFIllustrationRegistration(EnumSortlistBy sortListBy)
        {
            return SCRFIllustrationRegistrationBL.GetSCRFIllustrationRegistration(sortListBy);
        }
        public List<XsiScrfillustrationRegistration> GetSCRFIllustrationRegistration(XsiScrfillustrationRegistration entity)
        {
            return SCRFIllustrationRegistrationBL.GetSCRFIllustrationRegistration(entity);
        }
        public List<XsiScrfillustrationRegistration> GetSCRFIllustrationRegistration(XsiScrfillustrationRegistration entity, EnumSortlistBy sortListBy)
        {
            return SCRFIllustrationRegistrationBL.GetSCRFIllustrationRegistration(entity, sortListBy);
        }

        public EnumResultType InsertSCRFIllustrationRegistration(XsiScrfillustrationRegistration entity)
        {
            return SCRFIllustrationRegistrationBL.InsertSCRFIllustrationRegistration(entity);
        }
        public EnumResultType UpdateSCRFIllustrationRegistration(XsiScrfillustrationRegistration entity)
        {
            return SCRFIllustrationRegistrationBL.UpdateSCRFIllustrationRegistration(entity);
        }
        public EnumResultType UpdateSCRFIllustrationRegistrationStatus(long itemId)
        {
            return SCRFIllustrationRegistrationBL.UpdateSCRFIllustrationRegistrationStatus(itemId);
        }
        public EnumResultType DeleteSCRFIllustrationRegistration(XsiScrfillustrationRegistration entity)
        {
            return SCRFIllustrationRegistrationBL.DeleteSCRFIllustrationRegistration(entity);
        }
        public EnumResultType DeleteSCRFIllustrationRegistration(long itemId)
        {
            return SCRFIllustrationRegistrationBL.DeleteSCRFIllustrationRegistration(itemId);
        }
        #endregion
    }
}
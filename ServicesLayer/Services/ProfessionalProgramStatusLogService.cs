﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionProfessionalProgramRegistrationStatusLogsService : IDisposable
    {
        #region Fields
        ProfessionalRegistrationStatusLogBL ProfessionalRegistrationStatusLogBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ProfessionalRegistrationStatusLogBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ProfessionalRegistrationStatusLogBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionProfessionalProgramRegistrationStatusLogsService()
        {
            ProfessionalRegistrationStatusLogBL = new ProfessionalRegistrationStatusLogBL();
        }
        public ExhibitionProfessionalProgramRegistrationStatusLogsService(string connectionString)
        {
            ProfessionalRegistrationStatusLogBL = new ProfessionalRegistrationStatusLogBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ProfessionalRegistrationStatusLogBL != null)
                this.ProfessionalRegistrationStatusLogBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ProfessionalRegistrationStatusLogBL.GetNextGroupId();
        }
        public XsiExhibitionProfessionalProgramRegistrationStatusLogs GetProfessionalProgramRegistrationStatusLogsByItemId(long itemId)
        {
            return ProfessionalRegistrationStatusLogBL.GetProfessionalRegistrationStatusLogByItemId(itemId);
        }
         
        public List<XsiExhibitionProfessionalProgramRegistrationStatusLogs> GetProfessionalProgramRegistrationStatusLogs()
        {
            return ProfessionalRegistrationStatusLogBL.GetProfessionalRegistrationStatusLog();
        }
        public List<XsiExhibitionProfessionalProgramRegistrationStatusLogs> GetProfessionalProgramRegistrationStatusLogs(EnumSortlistBy sortListBy)
        {
            return ProfessionalRegistrationStatusLogBL.GetProfessionalRegistrationStatusLog(sortListBy);
        }
        public List<XsiExhibitionProfessionalProgramRegistrationStatusLogs> GetProfessionalProgramRegistrationStatusLogs(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity)
        {
            return ProfessionalRegistrationStatusLogBL.GetProfessionalRegistrationStatusLog(entity);
        }
        public List<XsiExhibitionProfessionalProgramRegistrationStatusLogs> GetProfessionalProgramRegistrationStatusLogs(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity, EnumSortlistBy sortListBy)
        {
            return ProfessionalRegistrationStatusLogBL.GetProfessionalRegistrationStatusLog(entity, sortListBy);
        }
        public XsiExhibitionProfessionalProgramRegistrationStatusLogs GetProfessionalProgramRegistrationStatusLogs(long groupId, long languageId)
        {
            return ProfessionalRegistrationStatusLogBL.GetProfessionalRegistrationStatusLog(groupId, languageId);
        }

        public EnumResultType InsertExhibitionProfessionalProgramRegistrationStatusLogs(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity)
        {
            return ProfessionalRegistrationStatusLogBL.InsertProfessionalRegistrationStatusLog(entity);
        }
        public EnumResultType UpdateExhibitionProfessionalProgramRegistrationStatusLogs(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity)
        {
            return ProfessionalRegistrationStatusLogBL.UpdateProfessionalRegistrationStatusLog(entity);
        }
        public EnumResultType UpdateExhibitionProfessionalProgramRegistrationStatusLogsStatus(long itemId)
        {
            return ProfessionalRegistrationStatusLogBL.UpdateProfessionalRegistrationStatusLogStatus(itemId);
        }
        public EnumResultType DeleteExhibitionProfessionalProgramRegistrationStatusLogs(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity)
        {
            return ProfessionalRegistrationStatusLogBL.DeleteProfessionalRegistrationStatusLog(entity);
        }
        public EnumResultType DeleteExhibitionProfessionalProgramRegistrationStatusLogs(long itemId)
        {
            return ProfessionalRegistrationStatusLogBL.DeleteProfessionalRegistrationStatusLog(itemId);
        }
        #endregion
    }
}
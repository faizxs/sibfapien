﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class TranslationGrantCategoryService : IDisposable
    {
        #region Feilds
        TranslationGrantCategoryBL TranslationGrantCategoryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return TranslationGrantCategoryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return TranslationGrantCategoryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public TranslationGrantCategoryService()
        {
            TranslationGrantCategoryBL = new TranslationGrantCategoryBL();
        }
        public TranslationGrantCategoryService(string connectionString)
        {
            TranslationGrantCategoryBL = new TranslationGrantCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.TranslationGrantCategoryBL != null)
                this.TranslationGrantCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionTranslationGrantCategory GetTranslationGrantCategoryByItemId(long itemId)
        {
            return TranslationGrantCategoryBL.GetTranslationGrantCategoryByItemId(itemId);
        }

        public List<XsiExhibitionTranslationGrantCategory> GetTranslationGrantCategory()
        {
            return TranslationGrantCategoryBL.GetTranslationGrantCategory();
        }
        public List<XsiExhibitionTranslationGrantCategory> GetTranslationGrantCategory(EnumSortlistBy sortListBy)
        {
            return TranslationGrantCategoryBL.GetTranslationGrantCategory(sortListBy);
        }
        public List<XsiExhibitionTranslationGrantCategory> GetTranslationGrantCategory(XsiExhibitionTranslationGrantCategory entity)
        {
            return TranslationGrantCategoryBL.GetTranslationGrantCategory(entity);
        }
        public List<XsiExhibitionTranslationGrantCategory> GetTranslationGrantCategory(XsiExhibitionTranslationGrantCategory entity, EnumSortlistBy sortListBy)
        {
            return TranslationGrantCategoryBL.GetTranslationGrantCategory(entity, sortListBy);
        }
        public List<XsiExhibitionTranslationGrantCategory> GetTranslationGrantCategoryOr(XsiExhibitionTranslationGrantCategory entity, EnumSortlistBy sortListBy)
        {
            return TranslationGrantCategoryBL.GetTranslationGrantCategoryOr(entity, sortListBy);
        }

        public EnumResultType InsertTranslationGrantCategory(XsiExhibitionTranslationGrantCategory entity)
        {
            return TranslationGrantCategoryBL.InsertTranslationGrantCategory(entity);
        }
        public EnumResultType UpdateTranslationGrantCategory(XsiExhibitionTranslationGrantCategory entity)
        {
            return TranslationGrantCategoryBL.UpdateTranslationGrantCategory(entity);
        }
        public EnumResultType UpdateTranslationGrantCategoryStatus(long itemId)
        {
            return TranslationGrantCategoryBL.UpdateTranslationGrantCategoryStatus(itemId);
        }
        public EnumResultType DeleteTranslationGrantCategory(XsiExhibitionTranslationGrantCategory entity)
        {
            return TranslationGrantCategoryBL.DeleteTranslationGrantCategory(entity);
        }
        public EnumResultType DeleteTranslationGrantCategory(long itemId)
        {
            return TranslationGrantCategoryBL.DeleteTranslationGrantCategory(itemId);
        }
        #endregion
    }
}
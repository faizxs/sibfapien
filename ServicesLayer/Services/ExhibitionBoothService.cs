﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionBoothService : IDisposable
    {
        #region Feilds
        ExhibitionBoothBL ExhibitionBoothBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionBoothBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionBoothBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionBoothService()
        {
            ExhibitionBoothBL = new ExhibitionBoothBL();
        }
        public ExhibitionBoothService(string connectionString)
        {
            ExhibitionBoothBL = new ExhibitionBoothBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionBoothBL != null)
                this.ExhibitionBoothBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionBooth GetExhibitionBoothByItemId(long itemId)
        {
            return ExhibitionBoothBL.GetExhibitionBoothByItemId(itemId);
        }
        
        public List<XsiExhibitionBooth> GetExhibitionBooth()
        {
            return ExhibitionBoothBL.GetExhibitionBooth();
        }
        public List<XsiExhibitionBooth> GetExhibitionBooth(EnumSortlistBy sortListBy)
        {
            return ExhibitionBoothBL.GetExhibitionBooth(sortListBy);
        }
        public List<XsiExhibitionBooth> GetExhibitionBooth(XsiExhibitionBooth entity)
        {
            return ExhibitionBoothBL.GetExhibitionBooth(entity);
        }
        public List<XsiExhibitionBooth> GetExhibitionBooth(XsiExhibitionBooth entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBoothBL.GetExhibitionBooth(entity, sortListBy);
        }
         
        public List<XsiExhibitionBooth> GetOtherLanguageExhibitionBooth(XsiExhibitionBooth entity)
        {
            return ExhibitionBoothBL.GetOtherLanguageExhibitionBooth(entity);
        }
        public List<XsiExhibitionBooth> GetCurrentLanguageExhibitionBooth(XsiExhibitionBooth entity)
        {
            return ExhibitionBoothBL.GetCurrentLanguageExhibitionBooth(entity);
        }
        public List<XsiExhibitionBooth> GetCompleteExhibitionBooth(XsiExhibitionBooth entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBoothBL.GetCompleteExhibitionBooth(entity, sortListBy);
        }

        public List<XsiExhibitionBooth> GetExhibitionBoothOr(XsiExhibitionBooth entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBoothBL.GetExhibitionBoothOr(entity, sortListBy);
        }

        public EnumResultType InsertExhibitionBooth(XsiExhibitionBooth entity)
        {
            return ExhibitionBoothBL.InsertExhibitionBooth(entity);
        }
        public EnumResultType UpdateExhibitionBooth(XsiExhibitionBooth entity)
        {
            return ExhibitionBoothBL.UpdateExhibitionBooth(entity);
        }
        public EnumResultType UpdateExhibitionBoothStatus(long itemId)
        {
            return ExhibitionBoothBL.UpdateExhibitionBoothStatus(itemId);
        }
        public EnumResultType DeleteExhibitionBooth(XsiExhibitionBooth entity)
        {
            return ExhibitionBoothBL.DeleteExhibitionBooth(entity);
        }
        public EnumResultType DeleteExhibitionBooth(long itemId)
        {
            return ExhibitionBoothBL.DeleteExhibitionBooth(itemId);
        }
        #endregion
    }
}
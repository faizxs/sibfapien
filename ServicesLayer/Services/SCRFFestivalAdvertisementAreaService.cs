﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFFestivalAdvertisementAreaService : IDisposable
    {
        #region Feilds
        SCRFFestivalAdvertisementAreaBL SCRFFestivalAdvertisementAreaBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFFestivalAdvertisementAreaBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFFestivalAdvertisementAreaBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFFestivalAdvertisementAreaService()
        {
            SCRFFestivalAdvertisementAreaBL = new SCRFFestivalAdvertisementAreaBL();
        }
        public SCRFFestivalAdvertisementAreaService(string connectionString)
        {
            SCRFFestivalAdvertisementAreaBL = new SCRFFestivalAdvertisementAreaBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFFestivalAdvertisementAreaBL != null)
                this.SCRFFestivalAdvertisementAreaBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrffestivalAdvertisementArea GetSCRFFestivalAdvertisementAreaByItemId(long itemId)
        {
            return SCRFFestivalAdvertisementAreaBL.GetSCRFFestivalAdvertisementAreaByItemId(itemId);
        }

        public List<XsiScrffestivalAdvertisementArea> GetSCRFFestivalAdvertisementArea()
        {
            return SCRFFestivalAdvertisementAreaBL.GetSCRFFestivalAdvertisementArea();
        }
        public List<XsiScrffestivalAdvertisementArea> GetSCRFFestivalAdvertisementArea(EnumSortlistBy sortListBy)
        {
            return SCRFFestivalAdvertisementAreaBL.GetSCRFFestivalAdvertisementArea(sortListBy);
        }
        public List<XsiScrffestivalAdvertisementArea> GetSCRFFestivalAdvertisementArea(XsiScrffestivalAdvertisementArea entity)
        {
            return SCRFFestivalAdvertisementAreaBL.GetSCRFFestivalAdvertisementArea(entity);
        }
        public List<XsiScrffestivalAdvertisementArea> GetSCRFFestivalAdvertisementArea(XsiScrffestivalAdvertisementArea entity, EnumSortlistBy sortListBy)
        {
            return SCRFFestivalAdvertisementAreaBL.GetSCRFFestivalAdvertisementArea(entity, sortListBy);
        }
        public List<XsiScrffestivalAdvertisementArea> GetSCRFFestivalAdvertisementAreaOr(XsiScrffestivalAdvertisementArea entity, EnumSortlistBy sortListBy)
        {
            return SCRFFestivalAdvertisementAreaBL.GetSCRFFestivalAdvertisementAreaOr(entity, sortListBy);
        }

        public EnumResultType InsertSCRFFestivalAdvertisementArea(XsiScrffestivalAdvertisementArea entity)
        {
            return SCRFFestivalAdvertisementAreaBL.InsertSCRFFestivalAdvertisementArea(entity);
        }
        public EnumResultType UpdateSCRFFestivalAdvertisementArea(XsiScrffestivalAdvertisementArea entity)
        {
            return SCRFFestivalAdvertisementAreaBL.UpdateSCRFFestivalAdvertisementArea(entity);
        }
        public EnumResultType UpdateSCRFFestivalAdvertisementAreaStatus(long itemId)
        {
            return SCRFFestivalAdvertisementAreaBL.UpdateSCRFFestivalAdvertisementAreaStatus(itemId);
        }
        public EnumResultType DeleteSCRFFestivalAdvertisementArea(XsiScrffestivalAdvertisementArea entity)
        {
            return SCRFFestivalAdvertisementAreaBL.DeleteSCRFFestivalAdvertisementArea(entity);
        }
        public EnumResultType DeleteSCRFFestivalAdvertisementArea(long itemId)
        {
            return SCRFFestivalAdvertisementAreaBL.DeleteSCRFFestivalAdvertisementArea(itemId);
        }

        //Custom 
        public long GetHomeBannerInfo(long languageid)
        {
            return SCRFFestivalAdvertisementAreaBL.GetHomeBannerInfo(languageid);
        }
        #endregion
    }
}
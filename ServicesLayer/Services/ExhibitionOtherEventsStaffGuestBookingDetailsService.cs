﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;
using Entities.Models;

namespace Xsi.ServicesLayer
{
    public class ExhibitionOtherEventsStaffGuestBookingDetailsService : IDisposable
    {
        #region Feilds
        ExhibitionOtherEventsStaffGuestBookingDetailsBL ExhibitionOtherEventsStaffGuestBookingDetailsBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionOtherEventsStaffGuestBookingDetailsBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionOtherEventsStaffGuestBookingDetailsBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionOtherEventsStaffGuestBookingDetailsService()
        {
            ExhibitionOtherEventsStaffGuestBookingDetailsBL = new ExhibitionOtherEventsStaffGuestBookingDetailsBL();
        }
        public ExhibitionOtherEventsStaffGuestBookingDetailsService(string connectionString)
        {
            ExhibitionOtherEventsStaffGuestBookingDetailsBL = new ExhibitionOtherEventsStaffGuestBookingDetailsBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionOtherEventsStaffGuestBookingDetailsBL != null)
                this.ExhibitionOtherEventsStaffGuestBookingDetailsBL.Dispose();
        }
        #endregion
        #region Methods 
        public XsiExhibitionOtherEventsStaffGuestBookingDetails GetExhibitionOtherEventsStaffGuestBookingDetailsByItemId(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestBookingDetailsBL.GetExhibitionOtherEventsStaffGuestBookingDetailsByItemId(itemId);
        }
        public List<XsiSgbdto> GetExhibitionOtherEventsStaffGuestBookingListForMember(long memberid, long ExhibitionOtherEventsid, string bookingtype)
        {
            return ExhibitionOtherEventsStaffGuestBookingDetailsBL.GetExhibitionOtherEventsStaffGuestBookingListForMember(memberid, ExhibitionOtherEventsid, bookingtype);
        }

        public List<XsiExhibitionOtherEventsStaffGuestBookingDetails> GetExhibitionOtherEventsStaffGuestBookingDetails()
        {
            return ExhibitionOtherEventsStaffGuestBookingDetailsBL.GetExhibitionOtherEventsStaffGuestBookingDetails();
        }

        public List<XsiExhibitionOtherEventsStaffGuestBookingDetails> GetExhibitionOtherEventsStaffGuestBookingDetails(EnumSortlistBy sortListBy)
        {
            return ExhibitionOtherEventsStaffGuestBookingDetailsBL.GetExhibitionOtherEventsStaffGuestBookingDetails(sortListBy);
        }
        public List<XsiExhibitionOtherEventsStaffGuestBookingDetails> GetExhibitionOtherEventsStaffGuestBookingDetails(XsiExhibitionOtherEventsStaffGuestBookingDetails entity)
        {
            return ExhibitionOtherEventsStaffGuestBookingDetailsBL.GetExhibitionOtherEventsStaffGuestBookingDetails(entity);
        }
        public List<XsiExhibitionOtherEventsStaffGuestBookingDetails> GetExhibitionOtherEventsStaffGuestBookingDetails(XsiExhibitionOtherEventsStaffGuestBookingDetails entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionOtherEventsStaffGuestBookingDetailsBL.GetExhibitionOtherEventsStaffGuestBookingDetails(entity, sortListBy);
        }
        public XsiExhibitionOtherEventsStaffGuestBookingDetails GetExhibitionOtherEventsStaffGuestBookingDetails(long groupId, long languageId)
        {
            return ExhibitionOtherEventsStaffGuestBookingDetailsBL.GetExhibitionOtherEventsStaffGuestBookingDetails(groupId, languageId);
        }

        public EnumResultType InsertExhibitionOtherEventsStaffGuestBookingDetails(XsiExhibitionOtherEventsStaffGuestBookingDetails entity)
        {
            return ExhibitionOtherEventsStaffGuestBookingDetailsBL.InsertExhibitionOtherEventsStaffGuestBookingDetails(entity);
        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuestBookingDetails(XsiExhibitionOtherEventsStaffGuestBookingDetails entity)
        {
            return ExhibitionOtherEventsStaffGuestBookingDetailsBL.UpdateExhibitionOtherEventsStaffGuestBookingDetails(entity);
        }
        public EnumResultType RollbackExhibitionOtherEventsStaffGuestBookingDetails(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestBookingDetailsBL.RollbackExhibitionOtherEventsStaffGuestBookingDetails(itemId);
        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuestBookingDetailsStatus(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestBookingDetailsBL.UpdateExhibitionOtherEventsStaffGuestBookingDetailsStatus(itemId);
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuestBookingDetails(XsiExhibitionOtherEventsStaffGuestBookingDetails entity)
        {
            return ExhibitionOtherEventsStaffGuestBookingDetailsBL.DeleteExhibitionOtherEventsStaffGuestBookingDetails(entity);
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuestBookingDetails(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestBookingDetailsBL.DeleteExhibitionOtherEventsStaffGuestBookingDetails(itemId);
        }
        #endregion
    }
}
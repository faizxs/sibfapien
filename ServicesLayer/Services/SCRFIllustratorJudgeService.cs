﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFIllustratorJudgeService : IDisposable
    {
        #region Feilds
        SCRFIllustratorJudgeBL SCRFIllustratorJudgeBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFIllustratorJudgeBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFIllustratorJudgeBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFIllustratorJudgeService()
        {
            SCRFIllustratorJudgeBL = new SCRFIllustratorJudgeBL();
        }
        public SCRFIllustratorJudgeService(string connectionString)
        {
            SCRFIllustratorJudgeBL = new SCRFIllustratorJudgeBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIllustratorJudgeBL != null)
                this.SCRFIllustratorJudgeBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfillustratorJudge GetSCRFIllustratorJudgeByItemId(long itemId)
        {
            return SCRFIllustratorJudgeBL.GetSCRFIllustratorJudgeByItemId(itemId);
        }
        public XsiScrfillustratorJudge GetSCRFIllustratorJudgeByEmail(string email)
        {
            return SCRFIllustratorJudgeBL.GetSCRFIllustratorJudgeByEmail(email);
        }

        public List<XsiScrfillustratorJudge> GetSCRFIllustratorJudge()
        {
            return SCRFIllustratorJudgeBL.GetSCRFIllustratorJudge();
        }
        public List<XsiScrfillustratorJudge> GetSCRFIllustratorJudge(EnumSortlistBy sortListBy)
        {
            return SCRFIllustratorJudgeBL.GetSCRFIllustratorJudge(sortListBy);
        }
        public List<XsiScrfillustratorJudge> GetSCRFIllustratorJudge(XsiScrfillustratorJudge entity)
        {
            return SCRFIllustratorJudgeBL.GetSCRFIllustratorJudge(entity);
        }
        public List<XsiScrfillustratorJudge> GetSCRFIllustratorJudge(XsiScrfillustratorJudge entity, EnumSortlistBy sortListBy)
        {
            return SCRFIllustratorJudgeBL.GetSCRFIllustratorJudge(entity, sortListBy);
        }
        public List<XsiScrfillustratorJudge> GetSCRFIllustratorJudgeOr(XsiScrfillustratorJudge entity, EnumSortlistBy sortListBy)
        {
            return SCRFIllustratorJudgeBL.GetSCRFIllustratorJudgeOr(entity, sortListBy);
        }

        public EnumResultType InsertSCRFIllustratorJudge(XsiScrfillustratorJudge entity)
        {
            return SCRFIllustratorJudgeBL.InsertSCRFIllustratorJudge(entity);
        }
        public EnumResultType UpdateSCRFIllustratorJudge(XsiScrfillustratorJudge entity)
        {
            return SCRFIllustratorJudgeBL.UpdateSCRFIllustratorJudge(entity);
        }
        public EnumResultType UpdateSCRFIllustratorJudgeStatus(long itemId)
        {
            return SCRFIllustratorJudgeBL.UpdateSCRFIllustratorJudgeStatus(itemId);
        }
        public EnumResultType UpdateSCRFIllustratorJudgeStatusByGroupId(long itemId)
        {
            return SCRFIllustratorJudgeBL.UpdateSCRFIllustratorJudgeStatusByGroupId(itemId);
        }
        public EnumResultType UpdatePublishorUnpublishAll(long itemId, bool ispublish)
        {
            return SCRFIllustratorJudgeBL.UpdatePublishorUnpublishAll(itemId, ispublish);
        }
        public EnumResultType UpdatePublishorUnpublishAllByGroupId(long groupId, bool ispublish)
        {
            return SCRFIllustratorJudgeBL.UpdatePublishorUnpublishAllByGroupId(groupId, ispublish);
        }
        public EnumResultType DeleteSCRFIllustratorJudge(XsiScrfillustratorJudge entity)
        {
            return SCRFIllustratorJudgeBL.DeleteSCRFIllustratorJudge(entity);
        }
        public EnumResultType DeleteSCRFIllustratorJudge(long itemId)
        {
            return SCRFIllustratorJudgeBL.DeleteSCRFIllustratorJudge(itemId);
        }
        #endregion
    }
}
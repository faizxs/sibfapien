﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class IconsForSponsorService : IDisposable
    {
        #region Feilds
        IconsForSponsorBL IconsForSponsorBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return IconsForSponsorBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public IconsForSponsorService()
        {
            IconsForSponsorBL = new IconsForSponsorBL();
        }
        public IconsForSponsorService(string connectionString)
        {
            IconsForSponsorBL = new IconsForSponsorBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IconsForSponsorBL != null)
                this.IconsForSponsorBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiIconsForSponsor GetIconsForSponsorByItemId(long itemId)
        {
            return IconsForSponsorBL.GetIconsForSponsorByItemId(itemId);
        }
        public List<XsiIconsForSponsor> GetIconsForSponsor()
        {
            return IconsForSponsorBL.GetIconsForSponsor();
        }
        public List<XsiIconsForSponsor> GetIconsForSponsor(EnumSortlistBy sortListBy)
        {
            return IconsForSponsorBL.GetIconsForSponsor(sortListBy);
        }
        public List<XsiIconsForSponsor> GetIconsForSponsor(XsiIconsForSponsor entity)
        {
            return IconsForSponsorBL.GetIconsForSponsor(entity);
        }
        public List<XsiIconsForSponsor> GetIconsForSponsor(XsiIconsForSponsor entity, EnumSortlistBy sortListBy)
        {
            return IconsForSponsorBL.GetIconsForSponsor(entity, sortListBy);
        }
        public List<XsiIconsForSponsor> GetIconsForSponsorOr(XsiIconsForSponsor entity, EnumSortlistBy sortListBy)
        {
            return IconsForSponsorBL.GetIconsForSponsorOr(entity, sortListBy);
        }
        public EnumResultType InsertIconsForSponsor(XsiIconsForSponsor entity)
        {
            return IconsForSponsorBL.InsertIconsForSponsor(entity);
        }
        public EnumResultType UpdateIconsForSponsor(XsiIconsForSponsor entity)
        {
            return IconsForSponsorBL.UpdateIconsForSponsor(entity);
        }
        public EnumResultType UpdateIconsForSponsorStatus(long itemId, long langId)
        {
            return IconsForSponsorBL.UpdateIconsForSponsorStatus(itemId, langId);
        }
        public EnumResultType DeleteIconsForSponsor(XsiIconsForSponsor entity)
        {
            return IconsForSponsorBL.DeleteIconsForSponsor(entity);
        }
        public EnumResultType DeleteIconsForSponsor(long itemId)
        {
            return IconsForSponsorBL.DeleteIconsForSponsor(itemId);
        }
        #endregion
    }
}
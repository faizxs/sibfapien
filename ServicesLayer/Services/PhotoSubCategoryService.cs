﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PhotoSubCategoryService : IDisposable
    {
        #region Feilds
        PhotoSubCategoryBL PhotoSubCategoryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PhotoSubCategoryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return PhotoSubCategoryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PhotoSubCategoryService()
        {
            PhotoSubCategoryBL = new PhotoSubCategoryBL();
        }
        public PhotoSubCategoryService(string connectionString)
        {
            PhotoSubCategoryBL = new PhotoSubCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PhotoSubCategoryBL != null)
                this.PhotoSubCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiPhotoSubCategory GetPhotoSubCategoryByItemId(long itemId)
        {
            return PhotoSubCategoryBL.GetPhotoSubCategoryByItemId(itemId);
        }

        public List<XsiPhotoSubCategory> GetPhotoSubCategory()
        {
            return PhotoSubCategoryBL.GetPhotoSubCategory();
        }
        public List<XsiPhotoSubCategory> GetPhotoSubCategory(EnumSortlistBy sortListBy)
        {
            return PhotoSubCategoryBL.GetPhotoSubCategory(sortListBy);
        }
        public List<XsiPhotoSubCategory> GetPhotoSubCategory(XsiPhotoSubCategory entity)
        {
            return PhotoSubCategoryBL.GetPhotoSubCategory(entity);
        }
        public List<XsiPhotoSubCategory> GetPhotoSubCategory(XsiPhotoSubCategory entity, EnumSortlistBy sortListBy)
        {
            return PhotoSubCategoryBL.GetPhotoSubCategory(entity, sortListBy);
        }
        public List<XsiPhotoSubCategory> GetCompletePhotoSubCategory(XsiPhotoSubCategory entity, EnumSortlistBy sortListBy)
        {
            return PhotoSubCategoryBL.GetCompletePhotoSubCategory(entity, sortListBy);
        }
        public XsiPhotoSubCategory GetPhotoSubCategory(long groupId, long categoryId)
        {
            return PhotoSubCategoryBL.GetPhotoSubCategory(groupId, categoryId);
        }

        public EnumResultType InsertPhotoSubCategory(XsiPhotoSubCategory entity)
        {
            return PhotoSubCategoryBL.InsertPhotoSubCategory(entity);
        }
        public EnumResultType UpdatePhotoSubCategory(XsiPhotoSubCategory entity)
        {
            return PhotoSubCategoryBL.UpdatePhotoSubCategory(entity);
        }
        public EnumResultType UpdatePhotoSubCategoryStatus(long itemId, long langId)
        {
            return PhotoSubCategoryBL.UpdatePhotoSubCategoryStatus(itemId, langId);
        }
        public EnumResultType DeletePhotoSubCategory(XsiPhotoSubCategory entity)
        {
            return PhotoSubCategoryBL.DeletePhotoSubCategory(entity);
        }
        public EnumResultType DeletePhotoSubCategory(long itemId)
        {
            return PhotoSubCategoryBL.DeletePhotoSubCategory(itemId);
        }
        #endregion
    }
}
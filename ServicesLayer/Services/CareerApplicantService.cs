﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class CareerApplicantService : IDisposable
    {
        #region Feilds
        CareerApplicantBL CareerApplicantBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return CareerApplicantBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return CareerApplicantBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public CareerApplicantService()
        {
            CareerApplicantBL = new CareerApplicantBL();
        }
        public CareerApplicantService(string connectionString)
        {
            CareerApplicantBL = new CareerApplicantBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerApplicantBL != null)
                this.CareerApplicantBL.Dispose();
        }
        #endregion
        #region Methods

        public XsiCareerApplicants GetCareerApplicantByItemId(long itemId)
        {
            return CareerApplicantBL.GetCareerApplicantByItemId(itemId);
        }
         
        public List<XsiCareerApplicants> GetCareerApplicant()
        {
            return CareerApplicantBL.GetCareerApplicant();
        }
        public List<XsiCareerApplicants> GetCareerApplicant(EnumSortlistBy sortListBy)
        {
            return CareerApplicantBL.GetCareerApplicant(sortListBy);
        }
        public List<XsiCareerApplicants> GetCareerApplicant(XsiCareerApplicants entity)
        {
            return CareerApplicantBL.GetCareerApplicant(entity);
        }
        public List<XsiCareerApplicants> GetCareerApplicant(XsiCareerApplicants entity, EnumSortlistBy sortListBy)
        {
            return CareerApplicantBL.GetCareerApplicant(entity, sortListBy);
        }
        public List<XsiCareerApplicants> GetCareerApplicantOr(XsiCareerApplicants entity, EnumSortlistBy sortListBy)
        {
            return CareerApplicantBL.GetCareerApplicantOr(entity, sortListBy);
        }
        public EnumResultType InsertCareerApplicant(XsiCareerApplicants entity)
        {
            return CareerApplicantBL.InsertCareerApplicant(entity);
        }
        public EnumResultType UpdateCareerApplicant(XsiCareerApplicants entity)
        {
            return CareerApplicantBL.UpdateCareerApplicant(entity);
        }
        public EnumResultType UpdateCareerApplicantStatus(long itemId)
        {
            return CareerApplicantBL.UpdateCareerApplicantStatus(itemId);
        }
        public EnumResultType DeleteCareerApplicant(XsiCareerApplicants entity)
        {
            return CareerApplicantBL.DeleteCareerApplicant(entity);
        }
        public EnumResultType DeleteCareerApplicant(long itemId)
        {
            return CareerApplicantBL.DeleteCareerApplicant(itemId);
        }
        #endregion
    }
}
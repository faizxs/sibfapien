﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class NewsService : IDisposable
    {
        #region Feilds
        NewsBL NewsBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return NewsBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return NewsBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public NewsService()
        {
            NewsBL = new NewsBL();
        }
        public NewsService(string connectionString)
        {
            NewsBL = new NewsBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.NewsBL != null)
                this.NewsBL.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiNews GetNewsByItemId(long itemId)
        {
            return NewsBL.GetNewsByItemId(itemId);
        } 

        public List<XsiNews> GetNews()
        {
            return NewsBL.GetNews();
        }
        public List<XsiNews> GetNews(EnumSortlistBy sortListBy)
        {
            return NewsBL.GetNews(sortListBy);
        }
        public List<XsiNews> GetNews(XsiNews entity)
        {
            return NewsBL.GetNews(entity);
        }
        public List<XsiNews> GetNews(XsiNews entity, EnumSortlistBy sortListBy)
        {
            return NewsBL.GetNews(entity, sortListBy);
        }
        public List<XsiNews> GetCompleteNews(XsiNews entity, EnumSortlistBy sortListBy,long langId)
        {
            return NewsBL.GetCompleteNews(entity, sortListBy, langId);
        }
        public XsiNews GetNews(long itemId, long categoryId)
        {
            return NewsBL.GetNews(itemId,categoryId);
        }

        public EnumResultType InsertNews(XsiNews entity)
        {
            return NewsBL.InsertNews(entity);
        }
        public EnumResultType UpdateNews(XsiNews entity)
        {
            return NewsBL.UpdateNews(entity);
        }
        public EnumResultType UpdateNewsStatus(long itemId)
        {
            return NewsBL.UpdateNewsStatus(itemId);
        }
        public EnumResultType DeleteNews(XsiNews entity)
        {
            return NewsBL.DeleteNews(entity);
        }
        public EnumResultType DeleteNews(long itemId)
        {
            return NewsBL.DeleteNews(itemId);
        }
        #endregion
    }
}
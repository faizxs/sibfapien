﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionMemberService : IDisposable
    {
        #region Feilds
        ExhibitionMemberBL ExhibitionMemberBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionMemberBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionMemberBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionMemberService()
        {
            ExhibitionMemberBL = new ExhibitionMemberBL();
        }
        public ExhibitionMemberService(string connectionString)
        {
            ExhibitionMemberBL = new ExhibitionMemberBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionMemberBL != null)
                this.ExhibitionMemberBL.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionMember GetExhibitionMemberByItemId(long itemId)
        {
            return ExhibitionMemberBL.GetExhibitionMemberByItemId(itemId);
        }
        
        public XsiExhibitionMember GetMemberByEmail(string email)
        {
            return ExhibitionMemberBL.GetMemberByEmail(email);
        }
        public XsiExhibitionMember GetMemberByUsername(string username)
        {
            return ExhibitionMemberBL.GetMemberByUsername(username);
        }
        public List<XsiExhibitionMember> GetExhibitionMember()
        {
            return ExhibitionMemberBL.GetExhibitionMember();
        }
        public List<XsiExhibitionMember> GetExhibitionMember(EnumSortlistBy sortListBy)
        {
            return ExhibitionMemberBL.GetExhibitionMember(sortListBy);
        }
        public List<XsiExhibitionMember> GetExhibitionMember(XsiExhibitionMember entity)
        {
            return ExhibitionMemberBL.GetExhibitionMember(entity);
        }
        public List<XsiExhibitionMember> GetExhibitionMember(XsiExhibitionMember entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionMemberBL.GetExhibitionMember(entity, sortListBy);
        }
        public List<XsiExhibitionMember> GetExhibitionMemberOr(XsiExhibitionMember entity)
        {
            return ExhibitionMemberBL.GetExhibitionMemberOr(entity);
        }
        public List<XsiExhibitionMember> GetExhibitionMemberOr(XsiExhibitionMember entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionMemberBL.GetExhibitionMemberOr(entity, sortListBy);
        }
        public EnumResultType InsertExhibitionMember(XsiExhibitionMember entity)
        {
            return ExhibitionMemberBL.InsertExhibitionMember(entity);
        }
        public EnumResultType UpdateExhibitionMember(XsiExhibitionMember entity)
        {
            return ExhibitionMemberBL.UpdateExhibitionMember(entity);
        }
        public EnumResultType UpdateExhibitionMemberStatus(long itemId)
        {
            return ExhibitionMemberBL.UpdateExhibitionMemberStatus(itemId);
        }
        public EnumResultType DeleteExhibitionMember(XsiExhibitionMember entity)
        {
            return ExhibitionMemberBL.DeleteExhibitionMember(entity);
        }
        public EnumResultType DeleteExhibitionMember(long itemId)
        {
            return ExhibitionMemberBL.DeleteExhibitionMember(itemId);
        }
        public EnumResultType DeleteAnonymousExhibitionMemberRegistration(long itemId)
        {
            return ExhibitionMemberBL.DeleteAnonymousExhibitionMemberRegistration(itemId);
        }
        #endregion
    }
}
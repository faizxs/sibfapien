﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionActivityService : IDisposable
    {
        #region Feilds
        ExhibitionActivityBL ExhibitionActivityBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionActivityBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionActivityBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionActivityService()
        {
            ExhibitionActivityBL = new ExhibitionActivityBL();
        }
        public ExhibitionActivityService(string connectionString)
        {
            ExhibitionActivityBL = new ExhibitionActivityBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionActivityBL != null)
                this.ExhibitionActivityBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionActivity GetExhibitionActivityByItemId(long itemId)
        {
            return ExhibitionActivityBL.GetExhibitionActivityByItemId(itemId);
        }
       
        public List<XsiExhibitionActivity> GetExhibitionActivityByItemIds(long[] ItemidArray)
        {
            return ExhibitionActivityBL.GetExhibitionActivityByItemIds(ItemidArray);
        }

        public List<XsiExhibitionActivity> GetExhibitionActivity()
        {
            return ExhibitionActivityBL.GetExhibitionActivity();
        }
        public List<XsiExhibitionActivity> GetExhibitionActivity(EnumSortlistBy sortListBy)
        {
            return ExhibitionActivityBL.GetExhibitionActivity(sortListBy);
        }
        public List<XsiExhibitionActivity> GetExhibitionActivity(XsiExhibitionActivity entity)
        {
            return ExhibitionActivityBL.GetExhibitionActivity(entity);
        }
        public List<XsiExhibitionActivity> GetExhibitionActivity(XsiExhibitionActivity entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionActivityBL.GetExhibitionActivity(entity, sortListBy);
        }
        public List<XsiExhibitionActivity> GetExhibitionActivityOr(XsiExhibitionActivity entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionActivityBL.GetExhibitionActivityOr(entity, sortListBy);
        }
         
        public EnumResultType InsertExhibitionActivity(XsiExhibitionActivity entity)
        {
            return ExhibitionActivityBL.InsertExhibitionActivity(entity);
        }
        public EnumResultType UpdateExhibitionActivity(XsiExhibitionActivity entity)
        {
            return ExhibitionActivityBL.UpdateExhibitionActivity(entity);
        }
        public EnumResultType UpdateExhibitionActivityStatus(long itemId)
        {
            return ExhibitionActivityBL.UpdateExhibitionActivityStatus(itemId);
        }
        public EnumResultType DeleteExhibitionActivity(XsiExhibitionActivity entity)
        {
            return ExhibitionActivityBL.DeleteExhibitionActivity(entity);
        }
        public EnumResultType DeleteExhibitionActivity(long itemId)
        {
            return ExhibitionActivityBL.DeleteExhibitionActivity(itemId);
        }
        #endregion
    }
}
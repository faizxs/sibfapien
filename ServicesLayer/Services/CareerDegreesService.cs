﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class CareerDegreesService : IDisposable
    {
        #region Feilds
        CareerDegreeBL CareerDegreesBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return CareerDegreesBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return CareerDegreesBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public CareerDegreesService()
        {
            CareerDegreesBL = new CareerDegreeBL();
        }
        public CareerDegreesService(string connectionString)
        {
            CareerDegreesBL = new CareerDegreeBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerDegreesBL != null)
                this.CareerDegreesBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiCareerDegrees GetCareerDegreeByItemId(long itemId)
        {
            return CareerDegreesBL.GetCareerDegreeByItemId(itemId);
        }
         
        public List<XsiCareerDegrees> GetCareerDegree()
        {
            return CareerDegreesBL.GetCareerDegree();
        }
        public List<XsiCareerDegrees> GetCareerDegree(EnumSortlistBy sortListBy)
        {
            return CareerDegreesBL.GetCareerDegree(sortListBy);
        }
        public List<XsiCareerDegrees> GetCareerDegree(XsiCareerDegrees entity)
        {
            return CareerDegreesBL.GetCareerDegree(entity);
        }
        public List<XsiCareerDegrees> GetCareerDegree(XsiCareerDegrees entity, EnumSortlistBy sortListBy)
        {
            return CareerDegreesBL.GetCareerDegree(entity, sortListBy);
        }
        public List<XsiCareerDegrees> GetCareerDegreeOr(XsiCareerDegrees entity, EnumSortlistBy sortListBy)
        {
            return CareerDegreesBL.GetCareerDegreeOr(entity, sortListBy);
        }
        
        public EnumResultType InsertCareerDegrees(XsiCareerDegrees entity)
        {
            return CareerDegreesBL.InsertCareerDegree(entity);
        }
        public EnumResultType UpdateCareerDegrees(XsiCareerDegrees entity)
        {
            return CareerDegreesBL.UpdateCareerDegree(entity);
        }
        public EnumResultType UpdateCareerDegreesStatus(long itemId)
        {
            return CareerDegreesBL.UpdateCareerDegreeStatus(itemId);
        }
        public EnumResultType DeleteCareerDegrees(XsiCareerDegrees entity)
        {
            return CareerDegreesBL.DeleteCareerDegree(entity);
        }
        public EnumResultType DeleteCareerDegrees(long itemId)
        {
            return CareerDegreesBL.DeleteCareerDegree(itemId);
        }
        #endregion
    }
}
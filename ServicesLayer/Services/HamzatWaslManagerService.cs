﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class HamzatWaslService : IDisposable
    {
        #region Feilds
        HamzatWaslBL HamzatWaslBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return HamzatWaslBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return HamzatWaslBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public HamzatWaslService()
        {
            HamzatWaslBL = new HamzatWaslBL();
        }
        public HamzatWaslService(string connectionString)
        {
            HamzatWaslBL = new HamzatWaslBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HamzatWaslBL != null)
                this.HamzatWaslBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiHamzatWasl GetHamzatWaslByItemId(long itemId)
        {
            return HamzatWaslBL.GetHamzatWaslByItemId(itemId);
        }

        public List<XsiHamzatWasl> GetHamzatWasl()
        {
            return HamzatWaslBL.GetHamzatWasl();
        }
        public List<XsiHamzatWasl> GetHamzatWasl(EnumSortlistBy sortListBy)
        {
            return HamzatWaslBL.GetHamzatWasl(sortListBy);
        }
        public List<XsiHamzatWasl> GetHamzatWasl(XsiHamzatWasl entity)
        {
            return HamzatWaslBL.GetHamzatWasl(entity);
        }
        public List<XsiHamzatWasl> GetHamzatWasl(XsiHamzatWasl entity, EnumSortlistBy sortListBy)
        {
            return HamzatWaslBL.GetHamzatWasl(entity, sortListBy);
        }
        public List<XsiHamzatWasl> GetHamzatWaslOr(XsiHamzatWasl entity, EnumSortlistBy sortListBy)
        {
            return HamzatWaslBL.GetHamzatWaslOr(entity, sortListBy);
        }

        public EnumResultType InsertHamzatWasl(XsiHamzatWasl entity)
        {
            return HamzatWaslBL.InsertHamzatWasl(entity);
        }
        public EnumResultType UpdateHamzatWasl(XsiHamzatWasl entity)
        {
            return HamzatWaslBL.UpdateHamzatWasl(entity);
        }

        public EnumResultType UpdateHamzatWaslStatus(long itemId)
        {
            return HamzatWaslBL.UpdateHamzatWaslStatus(itemId);
        }
        public EnumResultType DeleteHamzatWasl(XsiHamzatWasl entity)
        {
            return HamzatWaslBL.DeleteHamzatWasl(entity);
        }
        public EnumResultType DeleteHamzatWasl(long itemId)
        {
            return HamzatWaslBL.DeleteHamzatWasl(itemId);
        }
        #endregion
    }
}
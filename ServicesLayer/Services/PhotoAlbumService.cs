﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PhotoAlbumService : IDisposable
    {
        #region Feilds
        PhotoAlbumBL PhotoAlbumBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PhotoAlbumBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return PhotoAlbumBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PhotoAlbumService()
        {
            PhotoAlbumBL = new PhotoAlbumBL();
        }
        public PhotoAlbumService(string connectionString)
        {
            PhotoAlbumBL = new PhotoAlbumBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PhotoAlbumBL != null)
                this.PhotoAlbumBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextSortIndex()
        {
            return PhotoAlbumBL.GetNextSortIndex();
        }
        public XsiPhotoAlbum GetPhotoAlbumByItemId(long itemId)
        {
            return PhotoAlbumBL.GetPhotoAlbumByItemId(itemId);
        }

        public List<XsiPhotoAlbum> GetPhotoAlbum()
        {
            return PhotoAlbumBL.GetPhotoAlbum();
        }
        public List<XsiPhotoAlbum> GetPhotoAlbum(EnumSortlistBy sortListBy)
        {
            return PhotoAlbumBL.GetPhotoAlbum(sortListBy);
        }
        public List<XsiPhotoAlbum> GetPhotoAlbum(XsiPhotoAlbum entity)
        {
            return PhotoAlbumBL.GetPhotoAlbum(entity);
        }
        public List<XsiPhotoAlbum> GetPhotoAlbum(XsiPhotoAlbum entity, EnumSortlistBy sortListBy)
        {
            return PhotoAlbumBL.GetPhotoAlbum(entity, sortListBy);
        }

        public EnumResultType InsertPhotoAlbum(XsiPhotoAlbum entity)
        {
            return PhotoAlbumBL.InsertPhotoAlbum(entity);
        }
        public EnumResultType UpdatePhotoAlbum(XsiPhotoAlbum entity)
        {
            return PhotoAlbumBL.UpdatePhotoAlbum(entity);
        }
        public EnumResultType UpdatePhotoAlbumStatus(long itemId, long langId)
        {
            return PhotoAlbumBL.UpdatePhotoAlbumStatus(itemId, langId);
        }
        public EnumResultType DeletePhotoAlbum(XsiPhotoAlbum entity)
        {
            return PhotoAlbumBL.DeletePhotoAlbum(entity);
        }
        public EnumResultType DeletePhotoAlbum(long itemId)
        {
            return PhotoAlbumBL.DeletePhotoAlbum(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ContactusEnquiryService : IDisposable
    {
        #region Feilds
        ContactusEnquiryBL ContactusEnquiryBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return ContactusEnquiryBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public ContactusEnquiryService()
        {
            ContactusEnquiryBL = new ContactusEnquiryBL();
        }
        public ContactusEnquiryService(string connectionString)
        {
            ContactusEnquiryBL = new ContactusEnquiryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ContactusEnquiryBL != null)
                this.ContactusEnquiryBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiContactusEnquiry GetContactusEnquiryByItemId(long itemId)
        {
            return ContactusEnquiryBL.GetContactusEnquiryByItemId(itemId);
        }

        public List<XsiContactusEnquiry> GetContactusEnquiry()
        {
            return ContactusEnquiryBL.GetContactusEnquiry();
        }

        public List<XsiContactusEnquiry> GetContactusEnquiryOr(XsiContactusEnquiry entity)
        {
            return ContactusEnquiryBL.GetContactusEnquiryOr(entity);
        }

        public List<XsiContactusEnquiry> GetContactusEnquiryOr(XsiContactusEnquiry entity, EnumSortlistBy sortListBy)
        {
            return ContactusEnquiryBL.GetContactusEnquiryOr(entity, sortListBy);
        }

        public List<XsiContactusEnquiry> GetContactusEnquiry(EnumSortlistBy sortListBy)
        {
            return ContactusEnquiryBL.GetContactusEnquiry(sortListBy);
        }
        public List<XsiContactusEnquiry> GetContactusEnquiry(XsiContactusEnquiry entity)
        {
            return ContactusEnquiryBL.GetContactusEnquiry(entity);
        }
        public List<XsiContactusEnquiry> GetContactusEnquiry(XsiContactusEnquiry entity, EnumSortlistBy sortListBy)
        {
            return ContactusEnquiryBL.GetContactusEnquiry(entity, sortListBy);
        }

        public EnumResultType InsertContactusEnquiry(XsiContactusEnquiry entity)
        {
            return ContactusEnquiryBL.InsertContactusEnquiry(entity);
        }
        public EnumResultType UpdateContactusEnquiry(XsiContactusEnquiry entity)
        {
            return ContactusEnquiryBL.UpdateContactusEnquiry(entity);
        }
        public EnumResultType UpdateContactusEnquiryStatus(long itemId)
        {
            return ContactusEnquiryBL.UpdateContactusEnquiryStatus(itemId);
        }
        public EnumResultType UpdateContactusEnquiryFlag(long itemId, string flag)
        {
            return ContactusEnquiryBL.UpdateContactusEnquiryFlag(itemId, flag);
        }
        public EnumResultType DeleteContactusEnquiry(XsiContactusEnquiry entity)
        {
            return ContactusEnquiryBL.DeleteContactusEnquiry(entity);
        }
        public EnumResultType DeleteContactusEnquiry(long itemId)
        {
            return ContactusEnquiryBL.DeleteContactusEnquiry(itemId);
        }
        #endregion
    }
}
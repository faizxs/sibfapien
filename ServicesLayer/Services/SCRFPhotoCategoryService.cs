﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFPhotoCategoryService : IDisposable
    {
        #region Feilds
        SCRFPhotoCategoryBL SCRFPhotoCategoryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFPhotoCategoryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFPhotoCategoryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFPhotoCategoryService()
        {
            SCRFPhotoCategoryBL = new SCRFPhotoCategoryBL();
        }
        public SCRFPhotoCategoryService(string connectionString)
        {
            SCRFPhotoCategoryBL = new SCRFPhotoCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFPhotoCategoryBL != null)
                this.SCRFPhotoCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiScrfphotoCategory GetSCRFPhotoCategoryByItemId(long itemId)
        {
            return SCRFPhotoCategoryBL.GetSCRFPhotoCategoryByItemId(itemId);
        }
        
        public List<XsiScrfphotoCategory> GetSCRFPhotoCategory()
        {
            return SCRFPhotoCategoryBL.GetSCRFPhotoCategory();
        }
        public List<XsiScrfphotoCategory> GetSCRFPhotoCategory(EnumSortlistBy sortListBy)
        {
            return SCRFPhotoCategoryBL.GetSCRFPhotoCategory(sortListBy);
        }
        public List<XsiScrfphotoCategory> GetSCRFPhotoCategory(XsiScrfphotoCategory entity)
        {
            return SCRFPhotoCategoryBL.GetSCRFPhotoCategory(entity);
        }
        public List<XsiScrfphotoCategory> GetSCRFPhotoCategory(XsiScrfphotoCategory entity, EnumSortlistBy sortListBy)
        {
            return SCRFPhotoCategoryBL.GetSCRFPhotoCategory(entity, sortListBy);
        }
        public List<XsiScrfphotoCategory> GetSCRFPhotoCategoryOr(XsiScrfphotoCategory entity, EnumSortlistBy sortListBy)
        {
            return SCRFPhotoCategoryBL.GetSCRFPhotoCategoryOr(entity, sortListBy);
        }
         
        public List<XsiScrfphotoCategory> GetOtherLanguagePhotoCategory(XsiScrfphotoCategory entity)
        {
            return SCRFPhotoCategoryBL.GetOtherLanguageSCRFPhotoCategory(entity);
        }
        public List<XsiScrfphotoCategory> GetCurrentLanguageSCRFPhotoCategory(XsiScrfphotoCategory entity)
        {
            return SCRFPhotoCategoryBL.GetCurrentLanguageSCRFPhotoCategory(entity);
        }

        public EnumResultType InsertSCRFPhotoCategory(XsiScrfphotoCategory entity)
        {
            return SCRFPhotoCategoryBL.InsertSCRFPhotoCategory(entity);
        }
        public EnumResultType UpdateSCRFPhotoCategory(XsiScrfphotoCategory entity)
        {
            return SCRFPhotoCategoryBL.UpdateSCRFPhotoCategory(entity);
        }
        public EnumResultType UpdateSCRFPhotoCategoryStatus(long itemId)
        {
            return SCRFPhotoCategoryBL.UpdateSCRFPhotoCategoryStatus(itemId);
        }
        public EnumResultType DeleteSCRFPhotoCategory(XsiScrfphotoCategory entity)
        {
            return SCRFPhotoCategoryBL.DeleteSCRFPhotoCategory(entity);
        }
        public EnumResultType DeleteSCRFPhotoCategory(long itemId)
        {
            return SCRFPhotoCategoryBL.DeleteSCRFPhotoCategory(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class DiscountService : IDisposable
    {
        #region Feilds
        DiscountBL DiscountBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return DiscountBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return DiscountBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public DiscountService()
        {
            DiscountBL = new DiscountBL();
        }
        public DiscountService(string connectionString)
        {
            DiscountBL = new DiscountBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.DiscountBL != null)
                this.DiscountBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return DiscountBL.GetNextGroupId();
        }
        public XsiDiscount GetDiscountByItemId(long itemId)
        {
            return DiscountBL.GetDiscountByItemId(itemId);
        }
         
        public List<XsiDiscount> GetDiscount()
        {
            return DiscountBL.GetDiscount();
        }
        public List<XsiDiscount> GetDiscount(EnumSortlistBy sortListBy)
        {
            return DiscountBL.GetDiscount(sortListBy);
        }
        public List<XsiDiscount> GetDiscount(XsiDiscount entity)
        {
            return DiscountBL.GetDiscount(entity);
        }
        public List<XsiDiscount> GetDiscount(XsiDiscount entity, EnumSortlistBy sortListBy)
        {
            return DiscountBL.GetDiscount(entity, sortListBy);
        }
        public XsiDiscount GetDiscount(long groupId, long languageId)
        {
            return DiscountBL.GetDiscount(groupId, languageId);
        }

        public EnumResultType InsertDiscount(XsiDiscount entity)
        {
            return DiscountBL.InsertDiscount(entity);
        }
        public EnumResultType UpdateDiscount(XsiDiscount entity)
        {
            return DiscountBL.UpdateDiscount(entity);
        }
        public EnumResultType RollbackDiscount(long itemId)
        {
            return DiscountBL.RollbackDiscount(itemId);
        }
        public EnumResultType UpdateDiscountStatus(long itemId)
        {
            return DiscountBL.UpdateDiscountStatus(itemId);
        }
        public EnumResultType DeleteDiscount(XsiDiscount entity)
        {
            return DiscountBL.DeleteDiscount(entity);
        }
        public EnumResultType DeleteDiscount(long itemId)
        {
            return DiscountBL.DeleteDiscount(itemId);
        }
        #endregion
    }
}
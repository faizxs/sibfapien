﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionFurnitureItemService : IDisposable
    {
        #region Fields
        ExhibitionFurnitureItemBL ExhibitionFurnitureItemBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionFurnitureItemBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionFurnitureItemBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionFurnitureItemService()
        {
            ExhibitionFurnitureItemBL = new ExhibitionFurnitureItemBL();
        }
        public ExhibitionFurnitureItemService(string connectionString)
        {
            ExhibitionFurnitureItemBL = new ExhibitionFurnitureItemBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionFurnitureItemBL != null)
                this.ExhibitionFurnitureItemBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionFurnitureItems GetExhibitionFurnitureItemByItemId(long itemId)
        {
            return ExhibitionFurnitureItemBL.GetExhibitionFurnitureItemByItemId(itemId);
        }
          
        public List<XsiExhibitionFurnitureItems> GetExhibitionFurnitureItem()
        {
            return ExhibitionFurnitureItemBL.GetExhibitionFurnitureItem();
        }
        public List<XsiExhibitionFurnitureItems> GetExhibitionFurnitureItem(EnumSortlistBy sortListBy)
        {
            return ExhibitionFurnitureItemBL.GetExhibitionFurnitureItem(sortListBy);
        }
        public List<XsiExhibitionFurnitureItems> GetExhibitionFurnitureItem(XsiExhibitionFurnitureItems entity)
        {
            return ExhibitionFurnitureItemBL.GetExhibitionFurnitureItem(entity);
        }
        public List<XsiExhibitionFurnitureItems> GetExhibitionFurnitureItem(XsiExhibitionFurnitureItems entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionFurnitureItemBL.GetExhibitionFurnitureItem(entity, sortListBy);
        }
         
        public EnumResultType InsertExhibitionFurnitureItem(XsiExhibitionFurnitureItems entity)
        {
            return ExhibitionFurnitureItemBL.InsertExhibitionFurnitureItem(entity);
        }
        public EnumResultType UpdateExhibitionFurnitureItem(XsiExhibitionFurnitureItems entity)
        {
            return ExhibitionFurnitureItemBL.UpdateExhibitionFurnitureItem(entity);
        }
        public EnumResultType UpdateExhibitionFurnitureItemStatus(long itemId, long langId)
        {
            return ExhibitionFurnitureItemBL.UpdateExhibitionFurnitureItemStatus(itemId, langId);
        }
        public EnumResultType DeleteExhibitionFurnitureItem(XsiExhibitionFurnitureItems entity)
        {
            return ExhibitionFurnitureItemBL.DeleteExhibitionFurnitureItem(entity);
        }
        public EnumResultType DeleteExhibitionFurnitureItem(long itemId)
        {
            return ExhibitionFurnitureItemBL.DeleteExhibitionFurnitureItem(itemId);
        }
        #endregion
    }
}
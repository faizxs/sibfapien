﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionShipmentService : IDisposable
    {
        #region Feilds
        ExhibitionShipmentBL ExhibitionShipmentBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionShipmentBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionShipmentBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionShipmentService()
        {
            ExhibitionShipmentBL = new ExhibitionShipmentBL();
        }
        public ExhibitionShipmentService(string connectionString)
        {
            ExhibitionShipmentBL = new ExhibitionShipmentBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionShipmentBL != null)
                this.ExhibitionShipmentBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionShipment GetExhibitionShipmentByItemId(long itemId)
        {
            return ExhibitionShipmentBL.GetExhibitionShipmentByItemId(itemId);
        }
         
        public List<XsiExhibitionShipment> GetExhibitionShipment()
        {
            return ExhibitionShipmentBL.GetExhibitionShipment();
        }
        public List<XsiExhibitionShipment> GetExhibitionShipment(EnumSortlistBy sortListBy)
        {
            return ExhibitionShipmentBL.GetExhibitionShipment(sortListBy);
        }
        public List<XsiExhibitionShipment> GetExhibitionShipment(XsiExhibitionShipment entity)
        {
            return ExhibitionShipmentBL.GetExhibitionShipment(entity);
        }
        public List<XsiExhibitionShipment> GetExhibitionShipment(XsiExhibitionShipment entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionShipmentBL.GetExhibitionShipment(entity, sortListBy);
        }
        public EnumResultType InsertExhibitionShipment(XsiExhibitionShipment entity)
        {
            return ExhibitionShipmentBL.InsertExhibitionShipment(entity);
        }
        public EnumResultType UpdateExhibitionShipment(XsiExhibitionShipment entity)
        {
            return ExhibitionShipmentBL.UpdateExhibitionShipment(entity);
        }
        public EnumResultType UpdateExhibitionShipmentStatus(long itemId)
        {
            return ExhibitionShipmentBL.UpdateExhibitionShipmentStatus(itemId);
        }
        public EnumResultType DeleteExhibitionShipment(XsiExhibitionShipment entity)
        {
            return ExhibitionShipmentBL.DeleteExhibitionShipment(entity);
        }
        public EnumResultType DeleteExhibitionShipment(long itemId)
        {
            return ExhibitionShipmentBL.DeleteExhibitionShipment(itemId);
        }
        #endregion
    }
}
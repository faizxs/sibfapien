﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class WinnersService : IDisposable
    {
        #region Feilds
        WinnersBL WinnersBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return WinnersBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return WinnersBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public WinnersService()
        {
            WinnersBL = new WinnersBL();
        }
        public WinnersService(string connectionString)
        {
            WinnersBL = new WinnersBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.WinnersBL != null)
                this.WinnersBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiAwardWinners GetWinnersByItemId(long itemId)
        {
            return WinnersBL.GetWinnersByItemId(itemId);
        }
         
        public List<XsiAwardWinners> GetWinners()
        {
            return WinnersBL.GetWinners();
        }
        public List<XsiAwardWinners> GetWinners(EnumSortlistBy sortListBy)
        {
            return WinnersBL.GetWinners(sortListBy);
        }
        public List<XsiAwardWinners> GetWinners(XsiAwardWinners entity)
        {
            return WinnersBL.GetWinners(entity);
        }
        public List<XsiAwardWinners> GetWinners(XsiAwardWinners entity, EnumSortlistBy sortListBy)
        {
            return WinnersBL.GetWinners(entity, sortListBy);
        }
        public XsiAwardWinners GetWinners(long groupId, long languageId)
        {
            return WinnersBL.GetWinners(groupId, languageId);
        }

        public EnumResultType InsertWinners(XsiAwardWinners entity)
        {
            return WinnersBL.InsertWinners(entity);
        }
        public EnumResultType UpdateWinners(XsiAwardWinners entity)
        {
            return WinnersBL.UpdateWinners(entity);
        }
        public EnumResultType UpdateWinnersStatus(long itemId)
        {
            return WinnersBL.UpdateWinnersStatus(itemId);
        }
        public EnumResultType DeleteWinners(XsiAwardWinners entity)
        {
            return WinnersBL.DeleteWinners(entity);
        }
        public EnumResultType DeleteWinners(long itemId)
        {
            return WinnersBL.DeleteWinners(itemId);
        }
        #endregion
    }
}
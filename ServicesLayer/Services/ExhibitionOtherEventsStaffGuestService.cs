﻿using System;
using System.Linq;
using System.Collections.Generic;
using Xsi.BusinessLogicLayer;
using Entities.Models;

namespace Xsi.ServicesLayer
{
    public class ExhibitionOtherEventsStaffGuestService : IDisposable
    {
        #region Feilds
        ExhibitionOtherEventsStaffGuestBL ExhibitionOtherEventsStaffGuestBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionOtherEventsStaffGuestBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionOtherEventsStaffGuestBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionOtherEventsStaffGuestService()
        {
            ExhibitionOtherEventsStaffGuestBL = new ExhibitionOtherEventsStaffGuestBL();
        }
        public ExhibitionOtherEventsStaffGuestService(string connectionString)
        {
            ExhibitionOtherEventsStaffGuestBL = new ExhibitionOtherEventsStaffGuestBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionOtherEventsStaffGuestBL != null)
                this.ExhibitionOtherEventsStaffGuestBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionOtherEventsStaffGuest GetStaffGuestByItemId(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestBL.GetExhibitionOtherEventsStaffGuestByItemId(itemId);
        } 
        public List<XsiExhibitionOtherEventsStaffGuest> GetExhibitionOtherEventsStaffGuest()
        {
            return ExhibitionOtherEventsStaffGuestBL.GetExhibitionOtherEventsStaffGuest();
        }
        public List<XsiExhibitionOtherEventsStaffGuest> GetExhibitionOtherEventsStaffGuest(EnumSortlistBy sortListBy)
        {
            return ExhibitionOtherEventsStaffGuestBL.GetExhibitionOtherEventsStaffGuest(sortListBy);
        }
        public List<XsiExhibitionOtherEventsStaffGuest> GetExhibitionOtherEventsStaffGuest(XsiExhibitionOtherEventsStaffGuest entity)
        {
            return ExhibitionOtherEventsStaffGuestBL.GetExhibitionOtherEventsStaffGuest(entity);
        }
        public List<XsiExhibitionOtherEventsStaffGuest> GetExhibitionOtherEventsStaffGuest(XsiExhibitionOtherEventsStaffGuest entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionOtherEventsStaffGuestBL.GetExhibitionOtherEventsStaffGuest(entity, sortListBy);
        }
        public EnumResultType InsertExhibitionOtherEventsStaffGuest(XsiExhibitionOtherEventsStaffGuest entity)
        {
            return ExhibitionOtherEventsStaffGuestBL.InsertExhibitionOtherEventsStaffGuest(entity);
        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuest(XsiExhibitionOtherEventsStaffGuest entity)
        {
            return ExhibitionOtherEventsStaffGuestBL.UpdateExhibitionOtherEventsStaffGuest(entity);
        }
        public EnumResultType UpdateStaffGuestStatus(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestBL.UpdateExhibitionOtherEventsStaffGuestStatus(itemId);
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuest(XsiExhibitionOtherEventsStaffGuest entity)
        {
            return ExhibitionOtherEventsStaffGuestBL.DeleteExhibitionOtherEventsStaffGuest(entity);
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuest(long itemId)
        {
            return ExhibitionOtherEventsStaffGuestBL.DeleteExhibitionOtherEventsStaffGuest(itemId);
        }
        #endregion
    }
}
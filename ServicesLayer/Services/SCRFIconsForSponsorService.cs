﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFIconsForSponsorService : IDisposable
    {
        #region Feilds
        SCRFIconsForSponsorBL SCRFIconsForSponsorBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFIconsForSponsorBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFIconsForSponsorBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFIconsForSponsorService()
        {
            SCRFIconsForSponsorBL = new SCRFIconsForSponsorBL();
        }
        public SCRFIconsForSponsorService(string connectionString)
        {
            SCRFIconsForSponsorBL = new SCRFIconsForSponsorBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIconsForSponsorBL != null)
                this.SCRFIconsForSponsorBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrficonsForSponsor GetSCRFIconsForSponsorByItemId(long itemId)
        {
            return SCRFIconsForSponsorBL.GetSCRFIconsForSponsorByItemId(itemId);
        }

        public List<XsiScrficonsForSponsor> GetSCRFIconsForSponsor()
        {
            return SCRFIconsForSponsorBL.GetSCRFIconsForSponsor();
        }
        public List<XsiScrficonsForSponsor> GetSCRFIconsForSponsor(EnumSortlistBy sortListBy)
        {
            return SCRFIconsForSponsorBL.GetSCRFIconsForSponsor(sortListBy);
        }
        public List<XsiScrficonsForSponsor> GetSCRFIconsForSponsor(XsiScrficonsForSponsor entity)
        {
            return SCRFIconsForSponsorBL.GetSCRFIconsForSponsor(entity);
        }
        public List<XsiScrficonsForSponsor> GetSCRFIconsForSponsor(XsiScrficonsForSponsor entity, EnumSortlistBy sortListBy)
        {
            return SCRFIconsForSponsorBL.GetSCRFIconsForSponsor(entity, sortListBy);
        }
        public List<XsiScrficonsForSponsor> GetSCRFIconsForSponsorOr(XsiScrficonsForSponsor entity, EnumSortlistBy sortListBy)
        {
            return SCRFIconsForSponsorBL.GetSCRFIconsForSponsorOr(entity, sortListBy);
        }

        public EnumResultType InsertSCRFIconsForSponsor(XsiScrficonsForSponsor entity)
        {
            return SCRFIconsForSponsorBL.InsertSCRFIconsForSponsor(entity);
        }
        public EnumResultType UpdateSCRFIconsForSponsor(XsiScrficonsForSponsor entity)
        {
            return SCRFIconsForSponsorBL.UpdateSCRFIconsForSponsor(entity);
        }
        public EnumResultType UpdateSCRFIconsForSponsorStatus(long itemId)
        {
            return SCRFIconsForSponsorBL.UpdateSCRFIconsForSponsorStatus(itemId);
        }
        public EnumResultType DeleteSCRFIconsForSponsor(XsiScrficonsForSponsor entity)
        {
            return SCRFIconsForSponsorBL.DeleteSCRFIconsForSponsor(entity);
        }
        public EnumResultType DeleteSCRFIconsForSponsor(long itemId)
        {
            return SCRFIconsForSponsorBL.DeleteSCRFIconsForSponsor(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SchoolRegistrationService : IDisposable
    {
        #region Feilds
        SchoolRegistrationBL SchoolRegistrationBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SchoolRegistrationBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SchoolRegistrationBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SchoolRegistrationService()
        {
            SchoolRegistrationBL = new SchoolRegistrationBL();
        }
        public SchoolRegistrationService(string connectionString)
        {
            SchoolRegistrationBL = new SchoolRegistrationBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SchoolRegistrationBL != null)
                this.SchoolRegistrationBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiSchoolRegistration GetSchoolRegistrationByItemId(long itemId)
        {
            return SchoolRegistrationBL.GetSchoolRegistrationByItemId(itemId);
        }
         
        public List<XsiSchoolRegistration> GetSchoolRegistration()
        {
            return SchoolRegistrationBL.GetSchoolRegistration();
        }
        public List<XsiSchoolRegistration> GetSchoolRegistration(EnumSortlistBy sortListBy)
        {
            return SchoolRegistrationBL.GetSchoolRegistration(sortListBy);
        }
        public List<XsiSchoolRegistration> GetSchoolRegistration(XsiSchoolRegistration entity)
        {
            return SchoolRegistrationBL.GetSchoolRegistration(entity);
        }
        public List<XsiSchoolRegistration> GetSchoolRegistration(XsiSchoolRegistration entity, EnumSortlistBy sortListBy)
        {
            return SchoolRegistrationBL.GetSchoolRegistration(entity, sortListBy);
        }
        public List<XsiSchoolRegistration> GetSchoolRegistrationOr(XsiSchoolRegistration entity, EnumSortlistBy sortListBy)
        {
            return SchoolRegistrationBL.GetSchoolRegistrationOr(entity, sortListBy);
        }
        public EnumResultType InsertSchoolRegistration(XsiSchoolRegistration entity)
        {
            return SchoolRegistrationBL.InsertSchoolRegistration(entity);
        }
        public EnumResultType UpdateSchoolRegistration(XsiSchoolRegistration entity)
        {
            return SchoolRegistrationBL.UpdateSchoolRegistration(entity);
        }
        public EnumResultType UpdateSchoolRegistrationStatus(long itemId)
        {
            return SchoolRegistrationBL.UpdateSchoolRegistrationStatus(itemId);
        }
        public EnumResultType UpdateSchoolRegistrationFlag(long itemId, string flag)
        {
            return SchoolRegistrationBL.UpdateSchoolRegistrationFlag(itemId, flag);
        }
        public EnumResultType DeleteSchoolRegistration(XsiSchoolRegistration entity)
        {
            return SchoolRegistrationBL.DeleteSchoolRegistration(entity);
        }
        public EnumResultType DeleteSchoolRegistration(long itemId)
        {
            return SchoolRegistrationBL.DeleteSchoolRegistration(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PPGenreService : IDisposable
    {
        #region Feilds
        PPGenreBL PPGenreBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PPGenreBL.XsiItemdId; }
        }
        #endregion
        #region Constructor
        public PPGenreService()
        {
            PPGenreBL = new PPGenreBL();
        }
        public PPGenreService(string connectionString)
        {
            PPGenreBL = new PPGenreBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PPGenreBL != null)
                this.PPGenreBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionPpgenre GetPPGenreByItemId(long itemId)
        {
            return PPGenreBL.GetPPGenreByItemId(itemId);
        }
        public List<XsiExhibitionPpgenre> GetPPGenre()
        {
            return PPGenreBL.GetPPGenre();
        }
        public List<XsiExhibitionPpgenre> GetPPGenre(EnumSortlistBy sortListBy)
        {
            return PPGenreBL.GetPPGenre(sortListBy);
        }
        public List<XsiExhibitionPpgenre> GetPPGenre(XsiExhibitionPpgenre entity)
        {
            return PPGenreBL.GetPPGenre(entity);
        }
        public List<XsiExhibitionPpgenre> GetPPGenre(XsiExhibitionPpgenre entity, EnumSortlistBy sortListBy)
        {
            return PPGenreBL.GetPPGenre(entity, sortListBy);
        }

        public EnumResultType InsertPPGenre(XsiExhibitionPpgenre entity)
        {
            return PPGenreBL.InsertPPGenre(entity);
        }
        public EnumResultType UpdatePPGenre(XsiExhibitionPpgenre entity)
        {
            return PPGenreBL.UpdatePPGenre(entity);
        }

        public EnumResultType DeletePPGenre(XsiExhibitionPpgenre entity)
        {
            return PPGenreBL.DeletePPGenre(entity);
        }
        public EnumResultType DeletePPGenre(long itemId)
        {
            return PPGenreBL.DeletePPGenre(itemId);
        }
        #endregion
    }
}
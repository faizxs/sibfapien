﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PageMenuService : IDisposable
    {
        #region Feilds
        PageMenuBL PageMenuBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PageMenuBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return PageMenuBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PageMenuService()
        {
            PageMenuBL = new PageMenuBL();
        }
        public PageMenuService(string connectionString)
        {
            PageMenuBL = new PageMenuBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PageMenuBL != null)
                this.PageMenuBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiPageMenuNew GetPageMenuByItemId(long itemId)
        {
            return PageMenuBL.GetPageMenuByItemId(itemId);
        }

        public List<XsiPageMenuNew> GetPageMenu()
        {
            return PageMenuBL.GetPageMenu();
        }
        public List<XsiPageMenuNew> GetPageMenu(EnumSortlistBy sortListBy)
        {
            return PageMenuBL.GetPageMenu(sortListBy);
        }
        public List<XsiPageMenuNew> GetPageMenu(XsiPageMenuNew entity)
        {
            return PageMenuBL.GetPageMenu(entity);
        }
        public List<XsiPageMenuNew> GetPageMenuOr(XsiPageMenuNew entity, EnumSortlistBy sortListBy)
        {
            return PageMenuBL.GetPageMenuOr(entity, sortListBy);
        }
        public List<XsiPageMenuNew> GetPageMenu(XsiPageMenuNew entity, EnumSortlistBy sortListBy)
        {
            return PageMenuBL.GetPageMenu(entity, sortListBy);
        }
        public List<XsiPageMenuNew> GetParents(long groupId, long languageId)
        {
            return PageMenuBL.GetParents(groupId, languageId);
        }

        public EnumResultType InsertPageMenu(XsiPageMenuNew entity)
        {
            return PageMenuBL.InsertPageMenu(entity);
        }
        public EnumResultType UpdatePageMenu(XsiPageMenuNew entity)
        {
            return PageMenuBL.UpdatePageMenu(entity);
        }
        public EnumResultType UpdatePageMenuStatus(long itemId)
        {
            return PageMenuBL.UpdatePageMenuStatus(itemId);
        }
        public EnumResultType UpdatePageMenuSortOrder(long groupId, long order)
        {
            return PageMenuBL.UpdatePageMenuSortOrder(groupId, order);
        }
        public EnumResultType DeletePageMenu(XsiPageMenuNew entity)
        {
            return PageMenuBL.DeletePageMenu(entity);
        }
        public EnumResultType DeletePageMenu(long itemId)
        {
            return PageMenuBL.DeletePageMenu(itemId);
        }
        #endregion
    }
}
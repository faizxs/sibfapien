﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionRegionService : IDisposable
    {
        #region Feilds
        ExhibitionRegionBL ExhibitionRegionBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionRegionBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionRegionBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionRegionService()
        {
            ExhibitionRegionBL = new ExhibitionRegionBL();
        }
        public ExhibitionRegionService(string connectionString)
        {
            ExhibitionRegionBL = new ExhibitionRegionBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionRegionBL != null)
                this.ExhibitionRegionBL.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiExhibitionRegion GetExhibitionRegionByItemId(long itemId)
        {
            return ExhibitionRegionBL.GetExhibitionRegionByItemId(itemId);
        }
       
        public List<XsiExhibitionRegion> GetExhibitionRegion()
        {
            return ExhibitionRegionBL.GetExhibitionRegion();
        }
        public List<XsiExhibitionRegion> GetExhibitionRegion(EnumSortlistBy sortListBy)
        {
            return ExhibitionRegionBL.GetExhibitionRegion(sortListBy);
        }
        public List<XsiExhibitionRegion> GetExhibitionRegion(XsiExhibitionRegion entity)
        {
            return ExhibitionRegionBL.GetExhibitionRegion(entity);
        }
        public List<XsiExhibitionRegion> GetExhibitionRegion(XsiExhibitionRegion entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionRegionBL.GetExhibitionRegion(entity, sortListBy);
        }
        public List<XsiExhibitionRegion> GetExhibitionRegionOr(XsiExhibitionRegion entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionRegionBL.GetExhibitionRegionOr(entity, sortListBy);
        }
      
        public EnumResultType InsertExhibitionRegion(XsiExhibitionRegion entity)
        {
            return ExhibitionRegionBL.InsertExhibitionRegion(entity);
        }
        public EnumResultType UpdateExhibitionRegion(XsiExhibitionRegion entity)
        {
            return ExhibitionRegionBL.UpdateExhibitionRegion(entity);
        }
        public EnumResultType UpdateExhibitionRegionStatus(long itemId)
        {
            return ExhibitionRegionBL.UpdateExhibitionRegionStatus(itemId);
        }
        public EnumResultType DeleteExhibitionRegion(XsiExhibitionRegion entity)
        {
            return ExhibitionRegionBL.DeleteExhibitionRegion(entity);
        }
        public EnumResultType DeleteExhibitionRegion(long itemId)
        {
            return ExhibitionRegionBL.DeleteExhibitionRegion(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionMemberStatusLogService : IDisposable
    {
        #region Feilds
        ExhibitionMemberStatusLogBL ExhibitionMemberStatusLogBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionMemberStatusLogBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionMemberStatusLogBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionMemberStatusLogService()
        {
            ExhibitionMemberStatusLogBL = new ExhibitionMemberStatusLogBL();
        }
        public ExhibitionMemberStatusLogService(string connectionString)
        {
            ExhibitionMemberStatusLogBL = new ExhibitionMemberStatusLogBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionMemberStatusLogBL != null)
                this.ExhibitionMemberStatusLogBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ExhibitionMemberStatusLogBL.GetNextGroupId();
        }
        public XsiExhibitionMemberStatusLog GetExhibitionMemberStatusLogByItemId(long itemId)
        {
            return ExhibitionMemberStatusLogBL.GetExhibitionMemberStatusLogByItemId(itemId);
        }
        

        public List<XsiExhibitionMemberStatusLog> GetExhibitionMemberStatusLog()
        {
            return ExhibitionMemberStatusLogBL.GetExhibitionMemberStatusLog();
        }
        public List<XsiExhibitionMemberStatusLog> GetExhibitionMemberStatusLog(EnumSortlistBy sortListBy)
        {
            return ExhibitionMemberStatusLogBL.GetExhibitionMemberStatusLog(sortListBy);
        }
        public List<XsiExhibitionMemberStatusLog> GetExhibitionMemberStatusLog(XsiExhibitionMemberStatusLog entity)
        {
            return ExhibitionMemberStatusLogBL.GetExhibitionMemberStatusLog(entity);
        }
        public List<XsiExhibitionMemberStatusLog> GetExhibitionMemberStatusLog(XsiExhibitionMemberStatusLog entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionMemberStatusLogBL.GetExhibitionMemberStatusLog(entity, sortListBy);
        }
        public XsiExhibitionMemberStatusLog GetExhibitionMemberStatusLog(long groupId, long languageId)
        {
            return ExhibitionMemberStatusLogBL.GetExhibitionMemberStatusLog(groupId, languageId);
        }

        public EnumResultType InsertExhibitionMemberStatusLog(XsiExhibitionMemberStatusLog entity)
        {
            return ExhibitionMemberStatusLogBL.InsertExhibitionMemberStatusLog(entity);
        }
        public EnumResultType UpdateExhibitionMemberStatusLog(XsiExhibitionMemberStatusLog entity)
        {
            return ExhibitionMemberStatusLogBL.UpdateExhibitionMemberStatusLog(entity);
        }
        public EnumResultType UpdateExhibitionMemberStatusLogStatus(long itemId)
        {
            return ExhibitionMemberStatusLogBL.UpdateExhibitionMemberStatusLogStatus(itemId);
        }
        public EnumResultType DeleteExhibitionMemberStatusLog(XsiExhibitionMemberStatusLog entity)
        {
            return ExhibitionMemberStatusLogBL.DeleteExhibitionMemberStatusLog(entity);
        }
        public EnumResultType DeleteExhibitionMemberStatusLog(long itemId)
        {
            return ExhibitionMemberStatusLogBL.DeleteExhibitionMemberStatusLog(itemId);
        }
        #endregion
    }
}
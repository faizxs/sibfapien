﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class GenreService : IDisposable
    {
        #region Feilds
        GenreBL GenreBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return GenreBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return GenreBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public GenreService()
        {
            GenreBL = new GenreBL();
        }
        public GenreService(string connectionString)
        {
            GenreBL = new GenreBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.GenreBL != null)
                this.GenreBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiGenre GetGenreByItemId(long itemId)
        {
            return GenreBL.GetGenreByItemId(itemId);
        }
        
        public List<XsiGenre> GetGenre()
        {
            return GenreBL.GetGenre();
        }
        public List<XsiGenre> GetGenre(EnumSortlistBy sortListBy)
        {
            return GenreBL.GetGenre(sortListBy);
        }
        public List<XsiGenre> GetGenre(XsiGenre entity)
        {
            return GenreBL.GetGenre(entity);
        }
        public List<XsiGenre> GetGenre(XsiGenre entity, EnumSortlistBy sortListBy)
        {
            return GenreBL.GetGenre(entity, sortListBy);
        }

        public List<XsiGenre> GetGenreOr(XsiGenre entity, EnumSortlistBy sortListBy)
        {
            return GenreBL.GetGenreOr(entity, sortListBy);
        }
        
        public EnumResultType InsertGenre(XsiGenre entity)
        {
            return GenreBL.InsertGenre(entity);
        }
        public EnumResultType UpdateGenre(XsiGenre entity)
        {
            return GenreBL.UpdateGenre(entity);
        }
        public EnumResultType UpdateGenreStatus(long itemId)
        {
            return GenreBL.UpdateGenreStatus(itemId);
        }
        public EnumResultType DeleteGenre(XsiGenre entity)
        {
            return GenreBL.DeleteGenre(entity);
        }
        public EnumResultType DeleteGenre(long itemId)
        {
            return GenreBL.DeleteGenre(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class TranslationGrantMemberStatusLogService : IDisposable
    {
        #region Feilds
        TranslationGrantMemberStatusLogBL TranslationGrantMemberStatusLogBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return TranslationGrantMemberStatusLogBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return TranslationGrantMemberStatusLogBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public TranslationGrantMemberStatusLogService()
        {
            TranslationGrantMemberStatusLogBL = new TranslationGrantMemberStatusLogBL();
        }
        public TranslationGrantMemberStatusLogService(string connectionString)
        {
            TranslationGrantMemberStatusLogBL = new TranslationGrantMemberStatusLogBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.TranslationGrantMemberStatusLogBL != null)
                this.TranslationGrantMemberStatusLogBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return TranslationGrantMemberStatusLogBL.GetNextGroupId();
        }
        public XsiExhibitionTranslationGrantMemberStatusLog GetTranslationGrantMemberStatusLogByItemId(long itemId)
        {
            return TranslationGrantMemberStatusLogBL.GetTranslationGrantMemberStatusLogByItemId(itemId);
        }

        public List<XsiExhibitionTranslationGrantMemberStatusLog> GetTranslationGrantMemberStatusLog()
        {
            return TranslationGrantMemberStatusLogBL.GetTranslationGrantMemberStatusLog();
        }
        public List<XsiExhibitionTranslationGrantMemberStatusLog> GetTranslationGrantMemberStatusLog(EnumSortlistBy sortListBy)
        {
            return TranslationGrantMemberStatusLogBL.GetTranslationGrantMemberStatusLog(sortListBy);
        }
        public List<XsiExhibitionTranslationGrantMemberStatusLog> GetTranslationGrantMemberStatusLog(XsiExhibitionTranslationGrantMemberStatusLog entity)
        {
            return TranslationGrantMemberStatusLogBL.GetTranslationGrantMemberStatusLog(entity);
        }
        public List<XsiExhibitionTranslationGrantMemberStatusLog> GetTranslationGrantMemberStatusLog(XsiExhibitionTranslationGrantMemberStatusLog entity, EnumSortlistBy sortListBy)
        {
            return TranslationGrantMemberStatusLogBL.GetTranslationGrantMemberStatusLog(entity, sortListBy);
        }
        public XsiExhibitionTranslationGrantMemberStatusLog GetTranslationGrantMemberStatusLog(long groupId, long languageId)
        {
            return TranslationGrantMemberStatusLogBL.GetTranslationGrantMemberStatusLog(groupId, languageId);
        }

        public EnumResultType InsertTranslationGrantMemberStatusLog(XsiExhibitionTranslationGrantMemberStatusLog entity)
        {
            return TranslationGrantMemberStatusLogBL.InsertTranslationGrantMemberStatusLog(entity);
        }
        public EnumResultType UpdateTranslationGrantMemberStatusLog(XsiExhibitionTranslationGrantMemberStatusLog entity)
        {
            return TranslationGrantMemberStatusLogBL.UpdateTranslationGrantMemberStatusLog(entity);
        }
        public EnumResultType UpdateTranslationGrantMemberStatusLogStatus(long itemId)
        {
            return TranslationGrantMemberStatusLogBL.UpdateTranslationGrantMemberStatusLogStatus(itemId);
        }
        public EnumResultType DeleteTranslationGrantMemberStatusLog(XsiExhibitionTranslationGrantMemberStatusLog entity)
        {
            return TranslationGrantMemberStatusLogBL.DeleteTranslationGrantMemberStatusLog(entity);
        }
        public EnumResultType DeleteTranslationGrantMemberStatusLog(long itemId)
        {
            return TranslationGrantMemberStatusLogBL.DeleteTranslationGrantMemberStatusLog(itemId);
        }
        #endregion
    }
}
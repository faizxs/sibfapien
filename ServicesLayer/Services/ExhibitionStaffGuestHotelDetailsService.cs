﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionStaffGuestHotelDetailsService : IDisposable
    {
        #region Feilds
        ExhibitionStaffGuestHotelDetailsBL ExhibitionStaffGuestHotelDetailsBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionStaffGuestHotelDetailsBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionStaffGuestHotelDetailsBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestHotelDetailsService()
        {
            ExhibitionStaffGuestHotelDetailsBL = new ExhibitionStaffGuestHotelDetailsBL();
        }
        public ExhibitionStaffGuestHotelDetailsService(string connectionString)
        {
            ExhibitionStaffGuestHotelDetailsBL = new ExhibitionStaffGuestHotelDetailsBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionStaffGuestHotelDetailsBL != null)
                this.ExhibitionStaffGuestHotelDetailsBL.Dispose();
        }
        #endregion
        #region Methods 
        public XsiExhibitionStaffGuestHotelDetails GetExhibitionStaffGuestHotelDetailsByItemId(long itemId)
        {
            return ExhibitionStaffGuestHotelDetailsBL.GetExhibitionStaffGuestHotelDetailsByItemId(itemId);
        }

        public List<XsiExhibitionStaffGuestHotelDetails> GetExhibitionStaffGuestHotelDetails()
        {
            return ExhibitionStaffGuestHotelDetailsBL.GetExhibitionStaffGuestHotelDetails();
        }
        public List<XsiExhibitionStaffGuestHotelDetails> GetExhibitionStaffGuestHotelDetails(EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestHotelDetailsBL.GetExhibitionStaffGuestHotelDetails(sortListBy);
        }
        public List<XsiExhibitionStaffGuestHotelDetails> GetExhibitionStaffGuestHotelDetails(XsiExhibitionStaffGuestHotelDetails entity)
        {
            return ExhibitionStaffGuestHotelDetailsBL.GetExhibitionStaffGuestHotelDetails(entity);
        }
        public List<XsiExhibitionStaffGuestHotelDetails> GetExhibitionStaffGuestHotelDetails(XsiExhibitionStaffGuestHotelDetails entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionStaffGuestHotelDetailsBL.GetExhibitionStaffGuestHotelDetails(entity, sortListBy);
        }
        public XsiExhibitionStaffGuestHotelDetails GetExhibitionStaffGuestHotelDetails(long groupId, long languageId)
        {
            return ExhibitionStaffGuestHotelDetailsBL.GetExhibitionStaffGuestHotelDetails(groupId, languageId);
        }

        public EnumResultType InsertExhibitionStaffGuestHotelDetails(XsiExhibitionStaffGuestHotelDetails entity)
        {
            return ExhibitionStaffGuestHotelDetailsBL.InsertExhibitionStaffGuestHotelDetails(entity);
        }
        public EnumResultType UpdateExhibitionStaffGuestHotelDetails(XsiExhibitionStaffGuestHotelDetails entity)
        {
            return ExhibitionStaffGuestHotelDetailsBL.UpdateExhibitionStaffGuestHotelDetails(entity);
        }
        public EnumResultType RollbackExhibitionStaffGuestHotelDetails(long itemId)
        {
            return ExhibitionStaffGuestHotelDetailsBL.RollbackExhibitionStaffGuestHotelDetails(itemId);
        }
        public EnumResultType UpdateExhibitionStaffGuestHotelDetailsStatus(long itemId)
        {
            return ExhibitionStaffGuestHotelDetailsBL.UpdateExhibitionStaffGuestHotelDetailsStatus(itemId);
        }
        public EnumResultType DeleteExhibitionStaffGuestHotelDetails(XsiExhibitionStaffGuestHotelDetails entity)
        {
            return ExhibitionStaffGuestHotelDetailsBL.DeleteExhibitionStaffGuestHotelDetails(entity);
        }
        public EnumResultType DeleteExhibitionStaffGuestHotelDetails(long itemId)
        {
            return ExhibitionStaffGuestHotelDetailsBL.DeleteExhibitionStaffGuestHotelDetails(itemId);
        }
        #endregion
    }
}
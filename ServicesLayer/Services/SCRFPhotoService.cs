﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFPhotoService : IDisposable
    {
        #region Feilds
        SCRFPhotoBL SCRFPhotoBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFPhotoBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFPhotoBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFPhotoService()
        {
            SCRFPhotoBL = new SCRFPhotoBL();
        }
        public SCRFPhotoService(string connectionString)
        {
            SCRFPhotoBL = new SCRFPhotoBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFPhotoBL != null)
                this.SCRFPhotoBL.Dispose();
        }
        #endregion
        #region Methods
        
        public long GetNextSortIndex()
        {
            return SCRFPhotoBL.GetNextSortIndex();
        }
        public XsiScrfphoto GetSCRFPhotoByItemId(long itemId)
        {
            return SCRFPhotoBL.GetSCRFPhotoByItemId(itemId);
        }
         
        public List<XsiScrfphoto> GetSCRFPhoto()
        {
            return SCRFPhotoBL.GetSCRFPhoto();
        }
        public List<XsiScrfphoto> GetSCRFPhoto(EnumSortlistBy sortListBy)
        {
            return SCRFPhotoBL.GetSCRFPhoto(sortListBy);
        }
        public List<XsiScrfphoto> GetSCRFPhoto(XsiScrfphoto entity)
        {
            return SCRFPhotoBL.GetSCRFPhoto(entity);
        }
        public List<XsiScrfphoto> GetSCRFPhoto(XsiScrfphoto entity, EnumSortlistBy sortListBy)
        {
            return SCRFPhotoBL.GetSCRFPhoto(entity, sortListBy);
        }
         
        public EnumResultType InsertSCRFPhoto(XsiScrfphoto entity)
        {
            return SCRFPhotoBL.InsertSCRFPhoto(entity);
        }
        public EnumResultType UpdateSCRFPhoto(XsiScrfphoto entity)
        {
            return SCRFPhotoBL.UpdateSCRFPhoto(entity);
        }
        public EnumResultType UpdateSCRFPhotoStatus(long itemId)
        {
            return SCRFPhotoBL.UpdateSCRFPhotoStatus(itemId);
        }

        public EnumResultType UpdateSCRFAlbumCover(long itemId)
        {
            return SCRFPhotoBL.UpdateSCRFAlbumCover(itemId);
        }
        public EnumResultType DeleteSCRFPhoto(XsiScrfphoto entity)
        {
            return SCRFPhotoBL.DeleteSCRFPhoto(entity);
        }
        public EnumResultType DeleteSCRFPhoto(long itemId)
        {
            return SCRFPhotoBL.DeleteSCRFPhoto(itemId);
        }
        #endregion
    }
}
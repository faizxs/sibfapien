﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFQuickLinksService : IDisposable
    {
        #region Feilds
        SCRFQuickLinksBL SCRFQuickLinksBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFQuickLinksBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFQuickLinksBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFQuickLinksService()
        {
            SCRFQuickLinksBL = new SCRFQuickLinksBL();
        }
        public SCRFQuickLinksService(string connectionString)
        {
            SCRFQuickLinksBL = new SCRFQuickLinksBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFQuickLinksBL != null)
                this.SCRFQuickLinksBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfquickLinks GetSCRFQuickLinksByItemId(long itemId)
        {
            return SCRFQuickLinksBL.GetSCRFQuickLinksByItemId(itemId);
        }

        public List<XsiScrfquickLinks> GetSCRFQuickLinks()
        {
            return SCRFQuickLinksBL.GetSCRFQuickLinks();
        }
        public List<XsiScrfquickLinks> GetSCRFQuickLinks(EnumSortlistBy sortListBy)
        {
            return SCRFQuickLinksBL.GetSCRFQuickLinks(sortListBy);
        }
        public List<XsiScrfquickLinks> GetSCRFQuickLinks(XsiScrfquickLinks entity)
        {
            return SCRFQuickLinksBL.GetSCRFQuickLinks(entity);
        }
        public List<XsiScrfquickLinks> GetSCRFQuickLinks(XsiScrfquickLinks entity, EnumSortlistBy sortListBy)
        {
            return SCRFQuickLinksBL.GetSCRFQuickLinks(entity, sortListBy);
        }
        public List<XsiScrfquickLinks> GetSCRFQuickLinksOr(XsiScrfquickLinks entity, EnumSortlistBy sortListBy)
        {
            return SCRFQuickLinksBL.GetSCRFQuickLinksOr(entity, sortListBy);
        }
        public EnumResultType InsertSCRFQuickLinks(XsiScrfquickLinks entity)
        {
            return SCRFQuickLinksBL.InsertSCRFQuickLinks(entity);
        }
        public EnumResultType UpdateSCRFQuickLinks(XsiScrfquickLinks entity)
        {
            return SCRFQuickLinksBL.UpdateSCRFQuickLinks(entity);
        }
        public EnumResultType UpdateSCRFQuickLinksStatus(long itemId)
        {
            return SCRFQuickLinksBL.UpdateSCRFQuickLinksStatus(itemId);
        }
        public EnumResultType UpdateSCRFQuickLinksSortOrder(long groupId, long order)
        {
            return SCRFQuickLinksBL.UpdateSCRFQuickLinksSortOrder(groupId, order);
        }
        public EnumResultType DeleteSCRFQuickLinks(XsiScrfquickLinks entity)
        {
            return SCRFQuickLinksBL.DeleteSCRFQuickLinks(entity);
        }
        public EnumResultType DeleteSCRFQuickLinks(long itemId)
        {
            return SCRFQuickLinksBL.DeleteSCRFQuickLinks(itemId);
        }
        #endregion
    }
}
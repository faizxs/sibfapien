﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class RestaurantRegistrationService : IDisposable
    {
        #region Feilds
        RestaurantRegistrationBL RestaurantRegistrationBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return RestaurantRegistrationBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return RestaurantRegistrationBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public RestaurantRegistrationService()
        {
            RestaurantRegistrationBL = new RestaurantRegistrationBL();
        }
        public RestaurantRegistrationService(string connectionString)
        {
            RestaurantRegistrationBL = new RestaurantRegistrationBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.RestaurantRegistrationBL != null)
                this.RestaurantRegistrationBL.Dispose();
        }
        #endregion
        #region Methods
        public List<GetExhibitorsAndCountriesSCRF_Result> GetExhibitorsForAutoComplete(string keyword, long langId, long exhibitionId)
        {
            return RestaurantRegistrationBL.GetExhibitorsForAutoComplete(keyword, langId, exhibitionId);
        }
        public XsiExhibitionMemberApplicationYearly GetRestaurantRegistrationByItemId(long itemId)
        {
            return RestaurantRegistrationBL.GetRestaurantRegistrationByItemId(itemId);
        }
        public XsiExhibitionMemberApplicationYearly GetRestaurantRegistrationByItemIdAll(long itemId)
        {
            return RestaurantRegistrationBL.GetRestaurantRegistrationByItemIdAll(itemId);
        }

        public List<XsiExhibitionMemberApplicationYearly> GetRestaurantRegistration()
        {
            return RestaurantRegistrationBL.GetRestaurantRegistration();
        }
        public List<XsiExhibitionMemberApplicationYearly> GetRestaurantRegistration(EnumSortlistBy sortListBy)
        {
            return RestaurantRegistrationBL.GetRestaurantRegistration(sortListBy);
        }
        public List<XsiExhibitionMemberApplicationYearly> GetRestaurantRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            return RestaurantRegistrationBL.GetRestaurantRegistration(entity);
        }
        public List<XsiExhibitionMemberApplicationYearly> GetRestaurantRegistration(XsiExhibitionMemberApplicationYearly entity, EnumSortlistBy sortListBy)
        {
            return RestaurantRegistrationBL.GetRestaurantRegistration(entity, sortListBy);
        }
        public List<string> GetRestaurantRegistration(XsiExhibitionMemberApplicationYearly entity, List<long> exhibitionIDs)
        {
            return RestaurantRegistrationBL.GetRestaurantRegistration(entity, exhibitionIDs);
        }
        public List<string> GetRestaurantRegistrationAr(XsiExhibitionMemberApplicationYearly entity, List<long> exhibitionIDs)
        {
            return RestaurantRegistrationBL.GetRestaurantRegistrationAr(entity, exhibitionIDs);
        }
        /*public List<GetExhibitors_Agencies_Result> GetExhibitorAndAgency(string whereExhibitor, string whereAgency, string languageId)
        {
            return RestaurantRegistrationBL.GetExhibitorAndAgency(whereExhibitor, whereAgency,languageId);
        }
        public List<GetExhibitors_Agencies_Result> GetExhibitorAndAgencyAnd(string whereExhibitor, string whereAgency, GetExhibitors_Agencies_Result entity, List<long?> categoryIDs, string languageId)
        {
            return RestaurantRegistrationBL.GetExhibitorAndAgencyAnd(whereExhibitor, whereAgency, entity, categoryIDs,languageId);
        }
        public List<GetExhibitors_Agencies_Result> GetExhibitorAndAgencyOr(string whereExhibitor, string whereAgency, GetExhibitors_Agencies_Result entity, List<long?> categoryIDs, string languageId)
        {
            return RestaurantRegistrationBL.GetExhibitorAndAgencyOr(whereExhibitor, whereAgency, entity, categoryIDs,languageId);
        }
        public List<GetExhibitors_AgenciesToExport_Result> GetExhibitorAndAgencyToExport(string exhibitorIds, string agencyIds, string languageId)
        {
            return RestaurantRegistrationBL.GetExhibitorAndAgencyToExport(exhibitorIds, agencyIds, languageId);
        }
        public List<GetExhibitorsAndCountries_Result> GetExhibitorsAndCountries()
        {
            return RestaurantRegistrationBL.GetExhibitorsAndCountries();
        }
        public List<GetExhibitorsAndCountriesSCRF_Result> GetExhibitorsAndCountriesSCRF()
        {
            return RestaurantRegistrationBL.GetExhibitorsAndCountriesSCRF();
        }
        public long? GetRequestedAreaSum()
        {
            return RestaurantRegistrationBL.GetRequestedAreaSum();
        }

        public List<GetRestaurantRegistrationList_Result> GetExhibitorList(string languageId) {
            return RestaurantRegistrationBL.GetExhibitorList(languageId);
        }*/
        public EnumResultType InsertRestaurantRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            return RestaurantRegistrationBL.InsertRestaurantRegistration(entity);
        }
        public EnumResultType UpdateRestaurantRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            return RestaurantRegistrationBL.UpdateRestaurantRegistration(entity);
        }
        
        public EnumResultType UpdateRestaurantRegistrationStatus(long itemId)
        {
            return RestaurantRegistrationBL.UpdateRestaurantRegistrationStatus(itemId);
        }
        public EnumResultType DeleteRestaurantRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            return RestaurantRegistrationBL.DeleteRestaurantRegistration(entity);
        }
        public EnumResultType DeleteRestaurantRegistration(long itemId)
        {
            return RestaurantRegistrationBL.DeleteRestaurantRegistration(itemId);
        }
        public XsiExhibitionExhibitorDetails GetDetailsById(long MemberExhibitionYearlyId)
        {
            return RestaurantRegistrationBL.GetDetailsById(MemberExhibitionYearlyId);
        }
        public EnumResultType InsertExhibitorDetailsRegistration(XsiExhibitionExhibitorDetails entity)
        {
            return RestaurantRegistrationBL.InsertExhibitorDetailRegistration(entity);
        }
        public EnumResultType UpdateRestaurantRegistrationDetails(XsiExhibitionExhibitorDetails entity)
        {
            return RestaurantRegistrationBL.UpdateRestaurantRegistrationDetails(entity);
        }
        #endregion
    }
}
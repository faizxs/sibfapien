﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionRepresentativeService : IDisposable
    {
        #region Feilds
        ExhibitionRepresentativeBL ExhibitionRepresentativeBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionRepresentativeBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionRepresentativeBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionRepresentativeService()
        {
            ExhibitionRepresentativeBL = new ExhibitionRepresentativeBL();
        }
        public ExhibitionRepresentativeService(string connectionString)
        {
            ExhibitionRepresentativeBL = new ExhibitionRepresentativeBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionRepresentativeBL != null)
                this.ExhibitionRepresentativeBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionRepresentative GetRepresentativeByItemId(long itemId)
        {
            return ExhibitionRepresentativeBL.GetExhibitionRepresentativeByItemId(itemId);
        } 
        public List<XsiExhibitionRepresentative> GetExhibitionRepresentative()
        {
            return ExhibitionRepresentativeBL.GetExhibitionRepresentative();
        }
        public List<XsiExhibitionRepresentative> GetExhibitionRepresentative(EnumSortlistBy sortListBy)
        {
            return ExhibitionRepresentativeBL.GetExhibitionRepresentative(sortListBy);
        }
        public List<XsiExhibitionRepresentative> GetExhibitionRepresentative(XsiExhibitionRepresentative entity)
        {
            return ExhibitionRepresentativeBL.GetExhibitionRepresentative(entity);
        }
        public List<XsiExhibitionRepresentative> GetExhibitionRepresentative(XsiExhibitionRepresentative entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionRepresentativeBL.GetExhibitionRepresentative(entity, sortListBy);
        }
        public EnumResultType InsertExhibitionRepresentative(XsiExhibitionRepresentative entity)
        {
            return ExhibitionRepresentativeBL.InsertExhibitionRepresentative(entity);
        }
        public EnumResultType UpdateExhibitionRepresentative(XsiExhibitionRepresentative entity)
        {
            return ExhibitionRepresentativeBL.UpdateExhibitionRepresentative(entity);
        }
        public EnumResultType UpdateRepresentativeStatus(long itemId)
        {
            return ExhibitionRepresentativeBL.UpdateExhibitionRepresentativeStatus(itemId);
        }
        public EnumResultType DeleteExhibitionRepresentative(XsiExhibitionRepresentative entity)
        {
            return ExhibitionRepresentativeBL.DeleteExhibitionRepresentative(entity);
        }
        public EnumResultType DeleteExhibitionRepresentative(long itemId)
        {
            return ExhibitionRepresentativeBL.DeleteExhibitionRepresentative(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class DownloadCategoryService : IDisposable
    {
        #region Feilds
        DownloadCategoryBL DownloadCategoryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return DownloadCategoryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return DownloadCategoryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public DownloadCategoryService()
        {
            DownloadCategoryBL = new DownloadCategoryBL();
        }
        public DownloadCategoryService(string connectionString)
        {
            DownloadCategoryBL = new DownloadCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.DownloadCategoryBL != null)
                this.DownloadCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiDownloadCategory GetDownloadCategoryByItemId(long itemId)
        {
            return DownloadCategoryBL.GetDownloadCategoryByItemId(itemId);
        }
         
        public List<XsiDownloadCategory> GetDownloadCategory()
        {
            return DownloadCategoryBL.GetDownloadCategory();
        }
        public List<XsiDownloadCategory> GetDownloadCategory(EnumSortlistBy sortListBy)
        {
            return DownloadCategoryBL.GetDownloadCategory(sortListBy);
        }
        public List<XsiDownloadCategory> GetDownloadCategory(XsiDownloadCategory entity)
        {
            return DownloadCategoryBL.GetDownloadCategory(entity);
        }
        public List<XsiDownloadCategory> GetDownloadCategory(XsiDownloadCategory entity, EnumSortlistBy sortListBy)
        {
            return DownloadCategoryBL.GetDownloadCategory(entity, sortListBy);
        }
        public List<XsiDownloadCategory> GetDownloadCategoryOr(XsiDownloadCategory entity, EnumSortlistBy sortListBy)
        {
            return DownloadCategoryBL.GetDownloadCategoryOr(entity, sortListBy);
        }
         
        public List<XsiDownloadCategory> GetOtherLanguageDownloadCategory(XsiDownloadCategory entity)
        {
            return DownloadCategoryBL.GetOtherLanguageDownloadCategory(entity);
        }
        public List<XsiDownloadCategory> GetCurrentLanguageDownloadCategory(XsiDownloadCategory entity)
        {
            return DownloadCategoryBL.GetCurrentLanguageDownloadCategory(entity);
        }

        public EnumResultType InsertDownloadCategory(XsiDownloadCategory entity)
        {
            return DownloadCategoryBL.InsertDownloadCategory(entity);
        }
        public EnumResultType UpdateDownloadCategory(XsiDownloadCategory entity)
        {
            return DownloadCategoryBL.UpdateDownloadCategory(entity);
        }
        public EnumResultType UpdateDownloadCategoryStatus(long itemId)
        {
            return DownloadCategoryBL.UpdateDownloadCategoryStatus(itemId);
        }
        public EnumResultType DeleteDownloadCategory(XsiDownloadCategory entity)
        {
            return DownloadCategoryBL.DeleteDownloadCategory(entity);
        }
        public EnumResultType DeleteDownloadCategory(long itemId)
        {
            return DownloadCategoryBL.DeleteDownloadCategory(itemId);
        }
        #endregion
    }
}
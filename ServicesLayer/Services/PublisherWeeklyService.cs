﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PublisherWeeklyService : IDisposable
    {
        #region Feilds
        PublisherWeeklyBL PublisherWeeklyBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PublisherWeeklyBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return PublisherWeeklyBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PublisherWeeklyService()
        {
            PublisherWeeklyBL = new PublisherWeeklyBL();
        }
        public PublisherWeeklyService(string connectionString)
        {
            PublisherWeeklyBL = new PublisherWeeklyBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PublisherWeeklyBL != null)
                this.PublisherWeeklyBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiPublisherWeekly GetPublisherWeeklyByItemId(long itemId)
        {
            return PublisherWeeklyBL.GetPublisherWeeklyByItemId(itemId);
        }
         
        public List<XsiPublisherWeekly> GetPublisherWeekly()
        {
            return PublisherWeeklyBL.GetPublisherWeekly();
        }
        public List<XsiPublisherWeekly> GetPublisherWeekly(EnumSortlistBy sortListBy)
        {
            return PublisherWeeklyBL.GetPublisherWeekly(sortListBy);
        }
        public List<XsiPublisherWeekly> GetPublisherWeekly(XsiPublisherWeekly entity)
        {
            return PublisherWeeklyBL.GetPublisherWeekly(entity);
        }
        public List<XsiPublisherWeekly> GetPublisherWeekly(XsiPublisherWeekly entity, EnumSortlistBy sortListBy)
        {
            return PublisherWeeklyBL.GetPublisherWeekly(entity, sortListBy);
        }
        public List<XsiPublisherWeekly> GetPublisherWeeklyOr(XsiPublisherWeekly entity, EnumSortlistBy sortListBy)
        {
            return PublisherWeeklyBL.GetPublisherWeeklyOr(entity, sortListBy);
        }
        
        public EnumResultType InsertPublisherWeekly(XsiPublisherWeekly entity)
        {
            return PublisherWeeklyBL.InsertPublisherWeekly(entity);
        }
        public EnumResultType UpdatePublisherWeekly(XsiPublisherWeekly entity)
        {
            return PublisherWeeklyBL.UpdatePublisherWeekly(entity);
        }
        public EnumResultType UpdatePublisherWeeklyStatus(long itemId)
        {
            return PublisherWeeklyBL.UpdatePublisherWeeklytatus(itemId);
        }
        public EnumResultType DeletePublisherWeekly(XsiPublisherWeekly entity)
        {
            return PublisherWeeklyBL.DeletePublisherWeekly(entity);
        }
        public EnumResultType DeletePublisherWeekly(long itemId)
        {
            return PublisherWeeklyBL.DeletePublisherWeekly(itemId);
        }
        #endregion
    }
}
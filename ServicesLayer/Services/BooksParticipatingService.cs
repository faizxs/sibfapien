﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class BooksParticipatingService : IDisposable
    {
        #region Feilds
        BooksParticipatingBL BooksParticipatingBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return BooksParticipatingBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return BooksParticipatingBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public BooksParticipatingService()
        {
            BooksParticipatingBL = new BooksParticipatingBL();
        }
        public BooksParticipatingService(string connectionString)
        {
            BooksParticipatingBL = new BooksParticipatingBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.BooksParticipatingBL != null)
                this.BooksParticipatingBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionBookParticipating GetBooksParticipatingByItemId(long itemId)
        {
            return BooksParticipatingBL.GetBooksParticipatingByItemId(itemId);
        }
       
        public List<XsiExhibitionBookParticipating> GetBooksParticipating()
        {
            return BooksParticipatingBL.GetBooksParticipating();
        }
        public List<XsiExhibitionBookParticipating> GetBooksParticipating(EnumSortlistBy sortListBy)
        {
            return BooksParticipatingBL.GetBooksParticipating(sortListBy);
        }
        public List<XsiExhibitionBookParticipating> GetBooksParticipating(XsiExhibitionBookParticipating entity)
        {
            return BooksParticipatingBL.GetBooksParticipating(entity);
        }
        public List<XsiExhibitionBookParticipating> GetBooksParticipating(XsiExhibitionBookParticipating entity, EnumSortlistBy sortListBy)
        {
            return BooksParticipatingBL.GetBooksParticipating(entity, sortListBy);
        }
        public XsiExhibitionBookParticipating GetBooksParticipating(long groupId, long languageId)
        {
            return BooksParticipatingBL.GetBooksParticipating(groupId, languageId);
        }

        public EnumResultType InsertBooksParticipating(XsiExhibitionBookParticipating entity)
        {
            return BooksParticipatingBL.InsertBooksParticipating(entity);
        }
        public EnumResultType UpdateBooksParticipating(XsiExhibitionBookParticipating entity)
        {
            return BooksParticipatingBL.UpdateBooksParticipating(entity);
        }
        public EnumResultType RollbackBooksParticipating(long itemId)
        {
            return BooksParticipatingBL.RollbackBooksParticipating(itemId);
        }
        public EnumResultType UpdateBooksParticipatingStatus(long itemId)
        {
            return BooksParticipatingBL.UpdateBooksParticipatingStatus(itemId);
        }
        public EnumResultType DeleteBooksParticipating(XsiExhibitionBookParticipating entity)
        {
            return BooksParticipatingBL.DeleteBooksParticipating(entity);
        }
        public EnumResultType DeleteBooksParticipating(long itemId)
        {
            return BooksParticipatingBL.DeleteBooksParticipating(itemId);
        }
        #endregion
    }
}
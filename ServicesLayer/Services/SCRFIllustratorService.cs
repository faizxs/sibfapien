﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFIllustratorService : IDisposable
    {
        #region Feilds
        SCRFIllustratorBL SCRFIllustratorBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFIllustratorBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFIllustratorBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFIllustratorService()
        {
            SCRFIllustratorBL = new SCRFIllustratorBL();
        }
        public SCRFIllustratorService(string connectionString)
        {
            SCRFIllustratorBL = new SCRFIllustratorBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIllustratorBL != null)
                this.SCRFIllustratorBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfillustrator GetSCRFIllustratorByItemId(long itemId)
        {
            return SCRFIllustratorBL.GetSCRFIllustratorByItemId(itemId);
        }

        public List<XsiScrfillustrator> GetSCRFIllustrator()
        {
            return SCRFIllustratorBL.GetSCRFIllustrator();
        }
        public List<XsiScrfillustrator> GetSCRFIllustrator(EnumSortlistBy sortListBy)
        {
            return SCRFIllustratorBL.GetSCRFIllustrator(sortListBy);
        }
        public List<XsiScrfillustrator> GetSCRFIllustrator(XsiScrfillustrator entity)
        {
            return SCRFIllustratorBL.GetSCRFIllustrator(entity);
        }
        public List<XsiScrfillustrator> GetSCRFIllustrator(XsiScrfillustrator entity, EnumSortlistBy sortListBy)
        {
            return SCRFIllustratorBL.GetSCRFIllustrator(entity, sortListBy);
        }

        public EnumResultType InsertSCRFIllustrator(XsiScrfillustrator entity)
        {
            return SCRFIllustratorBL.InsertSCRFIllustrator(entity);
        }
        public EnumResultType UpdateSCRFIllustrator(XsiScrfillustrator entity)
        {
            return SCRFIllustratorBL.UpdateSCRFIllustrator(entity);
        }
      
        public EnumResultType UpdateSCRFIllustratorStatus(long itemId)
        {
            return SCRFIllustratorBL.UpdateSCRFIllustratorStatus(itemId);
        }
        public EnumResultType DeleteSCRFIllustrator(XsiScrfillustrator entity)
        {
            return SCRFIllustratorBL.DeleteSCRFIllustrator(entity);
        }
        public EnumResultType DeleteSCRFIllustrator(long itemId)
        {
            return SCRFIllustratorBL.DeleteSCRFIllustrator(itemId);
        }
        #endregion
    }
}
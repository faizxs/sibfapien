﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class CareerYOBService : IDisposable
    {
        #region Feilds
        CareerYOBBL CareerYOBBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return CareerYOBBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return CareerYOBBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public CareerYOBService()
        {
            CareerYOBBL = new CareerYOBBL();
        }
        public CareerYOBService(string connectionString)
        {
            CareerYOBBL = new CareerYOBBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerYOBBL != null)
                this.CareerYOBBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiCareerYob GetCareerYOBByItemId(long itemId)
        {
            return CareerYOBBL.GetCareerYOBByItemId(itemId);
        }
         
        public List<XsiCareerYob> GetCareerYOB()
        {
            return CareerYOBBL.GetCareerYOB();
        }
        public List<XsiCareerYob> GetCareerYOB(EnumSortlistBy sortListBy)
        {
            return CareerYOBBL.GetCareerYOB(sortListBy);
        }
        public List<XsiCareerYob> GetCareerYOB(XsiCareerYob entity)
        {
            return CareerYOBBL.GetCareerYOB(entity);
        }
        public List<XsiCareerYob> GetCareerYOB(XsiCareerYob entity, EnumSortlistBy sortListBy)
        {
            return CareerYOBBL.GetCareerYOB(entity, sortListBy);
        }
          
        public EnumResultType InsertCareerYOB(XsiCareerYob entity)
        {
            return CareerYOBBL.InsertCareerYOB(entity);
        }
        public EnumResultType UpdateCareerYOB(XsiCareerYob entity)
        {
            return CareerYOBBL.UpdateCareerYOB(entity);
        }
        public EnumResultType UpdateCareerYOBStatus(long itemId)
        {
            return CareerYOBBL.UpdateCareerYOBStatus(itemId);
        }
        public EnumResultType DeleteCareerYOB(XsiCareerYob entity)
        {
            return CareerYOBBL.DeleteCareerYOB(entity);
        }
        public EnumResultType DeleteCareerYOB(long itemId)
        {
            return CareerYOBBL.DeleteCareerYOB(itemId);
        }
        #endregion
    }
}
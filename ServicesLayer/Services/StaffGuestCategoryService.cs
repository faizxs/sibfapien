﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class StaffGuestCategoryService : IDisposable
    {
        #region Feilds
        StaffGuestCategoryBL StaffGuestCategoryBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return StaffGuestCategoryBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public StaffGuestCategoryService()
        {
            StaffGuestCategoryBL = new StaffGuestCategoryBL();
        }
        public StaffGuestCategoryService(string connectionString)
        {
            StaffGuestCategoryBL = new StaffGuestCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.StaffGuestCategoryBL != null)
                this.StaffGuestCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiStaffGuestCategory GetStaffGuestCategoryByItemId(long itemId)
        {
            return StaffGuestCategoryBL.GetStaffGuestCategoryByItemId(itemId);
        }

        public List<XsiStaffGuestCategory> GetStaffGuestCategory()
        {
            return StaffGuestCategoryBL.GetStaffGuestCategory();
        }
        public List<XsiStaffGuestCategory> GetStaffGuestCategory(EnumSortlistBy sortListBy)
        {
            return StaffGuestCategoryBL.GetStaffGuestCategory(sortListBy);
        }
        public List<XsiStaffGuestCategory> GetStaffGuestCategory(XsiStaffGuestCategory entity)
        {
            return StaffGuestCategoryBL.GetStaffGuestCategory(entity);
        }
        public List<XsiStaffGuestCategory> GetStaffGuestCategory(XsiStaffGuestCategory entity, EnumSortlistBy sortListBy)
        {
            return StaffGuestCategoryBL.GetStaffGuestCategory(entity, sortListBy);
        }
        public List<XsiStaffGuestCategory> GetStaffGuestCategoryOr(XsiStaffGuestCategory entity, EnumSortlistBy sortListBy)
        {
            return StaffGuestCategoryBL.GetStaffGuestCategoryOr(entity, sortListBy);
        }

        public EnumResultType InsertStaffGuestCategory(XsiStaffGuestCategory entity)
        {
            return StaffGuestCategoryBL.InsertStaffGuestCategory(entity);
        }
        public EnumResultType UpdateStaffGuestCategory(XsiStaffGuestCategory entity)
        {
            return StaffGuestCategoryBL.UpdateStaffGuestCategory(entity);
        }
        public EnumResultType UpdateStaffGuestCategoryStatus(long itemId, long langId)
        {
            return StaffGuestCategoryBL.UpdateStaffGuestCategoryStatus(itemId, langId);
        }
        public EnumResultType DeleteStaffGuestCategory(XsiStaffGuestCategory entity)
        {
            return StaffGuestCategoryBL.DeleteStaffGuestCategory(entity);
        }
        public EnumResultType DeleteStaffGuestCategory(long itemId)
        {
            return StaffGuestCategoryBL.DeleteStaffGuestCategory(itemId);
        }
        #endregion
    }
}
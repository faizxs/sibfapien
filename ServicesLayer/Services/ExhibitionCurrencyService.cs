﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionCurrencyService : IDisposable
    {
        #region Feilds
        ExhibitionCurrencyBL ExhibitionCurrencyBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionCurrencyBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionCurrencyBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionCurrencyService()
        {
            ExhibitionCurrencyBL = new ExhibitionCurrencyBL();
        }
        public ExhibitionCurrencyService(string connectionString)
        {
            ExhibitionCurrencyBL = new ExhibitionCurrencyBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionCurrencyBL != null)
                this.ExhibitionCurrencyBL.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiExhibitionCurrency GetExhibitionCurrencyByItemId(long itemId)
        {
            return ExhibitionCurrencyBL.GetExhibitionCurrencyByItemId(itemId);
        }
       
        public List<XsiExhibitionCurrency> GetExhibitionCurrency()
        {
            return ExhibitionCurrencyBL.GetExhibitionCurrency();
        }
        public List<XsiExhibitionCurrency> GetExhibitionCurrency(EnumSortlistBy sortListBy)
        {
            return ExhibitionCurrencyBL.GetExhibitionCurrency(sortListBy);
        }
        public List<XsiExhibitionCurrency> GetExhibitionCurrency(XsiExhibitionCurrency entity)
        {
            return ExhibitionCurrencyBL.GetExhibitionCurrency(entity);
        }
        public List<XsiExhibitionCurrency> GetExhibitionCurrency(XsiExhibitionCurrency entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionCurrencyBL.GetExhibitionCurrency(entity, sortListBy);
        }

        public List<XsiExhibitionCurrency> GetExhibitionCurrencyOr(XsiExhibitionCurrency entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionCurrencyBL.GetExhibitionCurrencyOr(entity, sortListBy);
        }
        

        public EnumResultType InsertExhibitionCurrency(XsiExhibitionCurrency entity)
        {
            return ExhibitionCurrencyBL.InsertExhibitionCurrency(entity);
        }
        public EnumResultType UpdateExhibitionCurrency(XsiExhibitionCurrency entity)
        {
            return ExhibitionCurrencyBL.UpdateExhibitionCurrency(entity);
        }
        public EnumResultType UpdateExhibitionCurrencyStatus(long itemId)
        {
            return ExhibitionCurrencyBL.UpdateExhibitionCurrencyStatus(itemId);
        }
        public EnumResultType DeleteExhibitionCurrency(XsiExhibitionCurrency entity)
        {
            return ExhibitionCurrencyBL.DeleteExhibitionCurrency(entity);
        }
        public EnumResultType DeleteExhibitionCurrency(long itemId)
        {
            return ExhibitionCurrencyBL.DeleteExhibitionCurrency(itemId);
        }
        #endregion
    }
}
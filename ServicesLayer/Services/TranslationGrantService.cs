﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class TranslationGrantService : IDisposable
    {
        #region Feilds
        TranslationGrantBL TranslationGrantBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return TranslationGrantBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return TranslationGrantBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public TranslationGrantService()
        {
            TranslationGrantBL = new TranslationGrantBL();
        }
        public TranslationGrantService(string connectionString)
        {
            TranslationGrantBL = new TranslationGrantBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.TranslationGrantBL != null)
                this.TranslationGrantBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionTranslationGrant GetTranslationGrantByItemId(long itemId)
        {
            return TranslationGrantBL.GetTranslationGrantByItemId(itemId);
        }

        public List<XsiExhibitionTranslationGrant> GetTranslationGrant()
        {
            return TranslationGrantBL.GetTranslationGrant();
        }
        public List<XsiExhibitionTranslationGrant> GetTranslationGrant(EnumSortlistBy sortListBy)
        {
            return TranslationGrantBL.GetTranslationGrant(sortListBy);
        }
        public List<XsiExhibitionTranslationGrant> GetTranslationGrant(XsiExhibitionTranslationGrant entity)
        {
            return TranslationGrantBL.GetTranslationGrant(entity);
        }
        public List<XsiExhibitionTranslationGrant> GetTranslationGrant(XsiExhibitionTranslationGrant entity, EnumSortlistBy sortListBy)
        {
            return TranslationGrantBL.GetTranslationGrant(entity, sortListBy);
        }

        public EnumResultType InsertTranslationGrant(XsiExhibitionTranslationGrant entity)
        {
            return TranslationGrantBL.InsertTranslationGrant(entity);
        }
        public EnumResultType UpdateTranslationGrant(XsiExhibitionTranslationGrant entity)
        {
            return TranslationGrantBL.UpdateTranslationGrant(entity);
        }
        public EnumResultType UpdateTranslationGrantStatus(long itemId)
        {
            return TranslationGrantBL.UpdateTranslationGrantStatus(itemId);
        }
        public EnumResultType DeleteTranslationGrant(XsiExhibitionTranslationGrant entity)
        {
            return TranslationGrantBL.DeleteTranslationGrant(entity);
        }
        public EnumResultType DeleteTranslationGrant(long itemId)
        {
            return TranslationGrantBL.DeleteTranslationGrant(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionFurnitureItemOrderDetailsService : IDisposable
    {
        #region Fields
        ExhibitionFurnitureItemOrderDetailsBL ExhibitionFurnitureItemOrderDetailsBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionFurnitureItemOrderDetailsBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionFurnitureItemOrderDetailsBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionFurnitureItemOrderDetailsService()
        {
            ExhibitionFurnitureItemOrderDetailsBL = new ExhibitionFurnitureItemOrderDetailsBL();
        }
        public ExhibitionFurnitureItemOrderDetailsService(string connectionString)
        {
            ExhibitionFurnitureItemOrderDetailsBL = new ExhibitionFurnitureItemOrderDetailsBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionFurnitureItemOrderDetailsBL != null)
                this.ExhibitionFurnitureItemOrderDetailsBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionFurnitureItemOrderDetails GetExhibitionFurnitureItemOrderDetailsByItemId(long itemId)
        {
            return ExhibitionFurnitureItemOrderDetailsBL.GetExhibitionFurnitureItemOrderDetailsByItemId(itemId);
        }
          
        public List<XsiExhibitionFurnitureItemOrderDetails> GetExhibitionFurnitureItemOrderDetails()
        {
            return ExhibitionFurnitureItemOrderDetailsBL.GetExhibitionFurnitureItemOrderDetails();
        }
        public List<XsiExhibitionFurnitureItemOrderDetails> GetExhibitionFurnitureItemOrderDetails(EnumSortlistBy sortListBy)
        {
            return ExhibitionFurnitureItemOrderDetailsBL.GetExhibitionFurnitureItemOrderDetails(sortListBy);
        }
        public List<XsiExhibitionFurnitureItemOrderDetails> GetExhibitionFurnitureItemOrderDetails(XsiExhibitionFurnitureItemOrderDetails entity)
        {
            return ExhibitionFurnitureItemOrderDetailsBL.GetExhibitionFurnitureItemOrderDetails(entity);
        }
        public List<XsiExhibitionFurnitureItemOrderDetails> GetExhibitionFurnitureItemOrderDetails(XsiExhibitionFurnitureItemOrderDetails entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionFurnitureItemOrderDetailsBL.GetExhibitionFurnitureItemOrderDetails(entity, sortListBy);
        }
         
        public EnumResultType InsertExhibitionFurnitureItemOrderDetails(XsiExhibitionFurnitureItemOrderDetails entity)
        {
            return ExhibitionFurnitureItemOrderDetailsBL.InsertExhibitionFurnitureItemOrderDetails(entity);
        }
        public EnumResultType UpdateExhibitionFurnitureItemOrderDetails(XsiExhibitionFurnitureItemOrderDetails entity)
        {
            return ExhibitionFurnitureItemOrderDetailsBL.UpdateExhibitionFurnitureItemOrderDetails(entity);
        }
        public EnumResultType UpdateExhibitionFurnitureItemOrderDetailsStatus(long itemId, long langId)
        {
            return ExhibitionFurnitureItemOrderDetailsBL.UpdateExhibitionFurnitureItemOrderDetailsStatus(itemId, langId);
        }
        public EnumResultType DeleteExhibitionFurnitureItemOrderDetails(XsiExhibitionFurnitureItemOrderDetails entity)
        {
            return ExhibitionFurnitureItemOrderDetailsBL.DeleteExhibitionFurnitureItemOrderDetails(entity);
        }
        public EnumResultType DeleteExhibitionFurnitureItemOrderDetails(long itemId)
        {
            return ExhibitionFurnitureItemOrderDetailsBL.DeleteExhibitionFurnitureItemOrderDetails(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class EventGuestService : IDisposable
    {
        #region Feilds
        EventGuestBL EventGuestBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return EventGuestBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return EventGuestBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public EventGuestService()
        {
            EventGuestBL = new EventGuestBL();
        }
        public EventGuestService(string connectionString)
        {
            EventGuestBL = new EventGuestBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventGuestBL != null)
                this.EventGuestBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return EventGuestBL.GetNextGroupId();
        }
        public XsiEventGuest GetEventGuestByItemId(long itemId)
        {
            return EventGuestBL.GetEventGuestByItemId(itemId);
        }
       
        public List<XsiEventGuest> GetEventGuest()
        {
            return EventGuestBL.GetEventGuest();
        }
        public List<XsiEventGuest> GetEventGuest(EnumSortlistBy sortListBy)
        {
            return EventGuestBL.GetEventGuest(sortListBy);
        }
        public List<XsiEventGuest> GetEventGuest(XsiEventGuest entity)
        {
            return EventGuestBL.GetEventGuest(entity);
        }
        public List<XsiEventGuest> GetEventGuest(XsiEventGuest entity, EnumSortlistBy sortListBy)
        {
            return EventGuestBL.GetEventGuest(entity, sortListBy);
        }
        public XsiEventGuest GetEventGuest(long groupId, long languageId)
        {
            return EventGuestBL.GetEventGuest(groupId, languageId);
        }

        public EnumResultType InsertEventGuest(XsiEventGuest entity)
        {
            return EventGuestBL.InsertEventGuest(entity);
        }
        public EnumResultType UpdateEventGuest(XsiEventGuest entity)
        {
            return EventGuestBL.UpdateEventGuest(entity);
        }
        public EnumResultType UpdateEventGuestStatus(long itemId)
        {
            return EventGuestBL.UpdateEventGuestStatus(itemId);
        }
        public EnumResultType DeleteEventGuest(XsiEventGuest entity)
        {
            return EventGuestBL.DeleteEventGuest(entity);
        }
        public EnumResultType DeleteEventGuest(long itemId)
        {
            return EventGuestBL.DeleteEventGuest(itemId);
        }

        //public GetEvents_Result GetEventbyGuest(long guestId)
        //{
        //    return EventGuestBL.GetEventbyGuest(guestId);
        //}
        //public List<GetEvents_Result> GetEventbyGuestList(long guestId)
        //{
        //    return EventGuestBL.GetEventbyGuestList(guestId);
        //}
        //public List<GetEvents_Result> GetActivityList(string type,long guestGid)
        //{
        //    return EventGuestBL.GetActivityList(type, guestGid);
        //}
        #endregion
    }
}
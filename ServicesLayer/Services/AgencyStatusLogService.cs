﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class AgencyStatusLogService : IDisposable
    {
        #region Feilds
        AgencyStatusLogBL AgencyStatusLogBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return AgencyStatusLogBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return AgencyStatusLogBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public AgencyStatusLogService()
        {
            AgencyStatusLogBL = new AgencyStatusLogBL();
        }
        public AgencyStatusLogService(string connectionString)
        {
            AgencyStatusLogBL = new AgencyStatusLogBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AgencyStatusLogBL != null)
                this.AgencyStatusLogBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionMemberApplicationYearlyLogs GetAgencyStatusLogByItemId(long itemId)
        {
            return AgencyStatusLogBL.GetAgencyStatusLogByItemId(itemId);
        }

        public List<XsiExhibitionMemberApplicationYearlyLogs> GetAgencyStatusLog()
        {
            return AgencyStatusLogBL.GetAgencyStatusLog();
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetAgencyStatusLog(EnumSortlistBy sortListBy)
        {
            return AgencyStatusLogBL.GetAgencyStatusLog(sortListBy);
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetAgencyStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            return AgencyStatusLogBL.GetAgencyStatusLog(entity);
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetAgencyStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity, EnumSortlistBy sortListBy)
        {
            return AgencyStatusLogBL.GetAgencyStatusLog(entity, sortListBy);
        }
        public XsiExhibitionMemberApplicationYearlyLogs GetAgencyStatusLog(long groupId, long languageId)
        {
            return AgencyStatusLogBL.GetAgencyStatusLog(groupId, languageId);
        }

        public EnumResultType InsertAgencyStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            return AgencyStatusLogBL.InsertAgencyStatusLog(entity);
        }
        public EnumResultType UpdateAgencyStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            return AgencyStatusLogBL.UpdateAgencyStatusLog(entity);
        }
        public EnumResultType UpdateAgencyStatusLogStatus(long itemId)
        {
            return AgencyStatusLogBL.UpdateAgencyStatusLogStatus(itemId);
        }
        public EnumResultType DeleteAgencyStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            return AgencyStatusLogBL.DeleteAgencyStatusLog(entity);
        }
        public EnumResultType DeleteAgencyStatusLog(long itemId)
        {
            return AgencyStatusLogBL.DeleteAgencyStatusLog(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class NewsletterBannerService : IDisposable
    {
        #region Feilds
        NewsletterBannerBL NewsletterBannerBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return NewsletterBannerBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return NewsletterBannerBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public NewsletterBannerService()
        {
            NewsletterBannerBL = new NewsletterBannerBL();
        }
        public NewsletterBannerService(string connectionString)
        {
            NewsletterBannerBL = new NewsletterBannerBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.NewsletterBannerBL != null)
                this.NewsletterBannerBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiNewsletterBanner GetNewsletterBannerByItemId(long itemId)
        {
            return NewsletterBannerBL.GetNewsletterBannerByItemId(itemId);
        }

        public List<XsiNewsletterBanner> GetNewsletterBanner()
        {
            return NewsletterBannerBL.GetNewsletterBanner();
        }
        public List<XsiNewsletterBanner> GetNewsletterBanner(EnumSortlistBy sortListBy)
        {
            return NewsletterBannerBL.GetNewsletterBanner(sortListBy);
        }
        public List<XsiNewsletterBanner> GetNewsletterBanner(XsiNewsletterBanner entity)
        {
            return NewsletterBannerBL.GetNewsletterBanner(entity);
        }
        public List<XsiNewsletterBanner> GetNewsletterBanner(XsiNewsletterBanner entity, EnumSortlistBy sortListBy)
        {
            return NewsletterBannerBL.GetNewsletterBanner(entity, sortListBy);
        }

        public EnumResultType InsertNewsletterBanner(XsiNewsletterBanner entity)
        {
            return NewsletterBannerBL.InsertNewsletterBanner(entity);
        }
        public EnumResultType UpdateNewsletterBanner(XsiNewsletterBanner entity)
        {
            return NewsletterBannerBL.UpdateNewsletterBanner(entity);
        }
        public EnumResultType UpdateNewsletterBannerStatus(long itemId)
        {
            return NewsletterBannerBL.UpdateNewsletterBannerStatus(itemId);
        }
        public EnumResultType DeleteNewsletterBanner(XsiNewsletterBanner entity)
        {
            return NewsletterBannerBL.DeleteNewsletterBanner(entity);
        }
        public EnumResultType DeleteNewsletterBanner(long itemId)
        {
            return NewsletterBannerBL.DeleteNewsletterBanner(itemId);
        }
        #endregion
    }
}
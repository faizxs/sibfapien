﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SharjahAwardForTranslationService : IDisposable
    {
        #region Feilds
        SharjahAwardForTranslationBL SharjahAwardForTranslationBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SharjahAwardForTranslationBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SharjahAwardForTranslationBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SharjahAwardForTranslationService()
        {
            SharjahAwardForTranslationBL = new SharjahAwardForTranslationBL();
        }
        public SharjahAwardForTranslationService(string connectionString)
        {
            SharjahAwardForTranslationBL = new SharjahAwardForTranslationBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SharjahAwardForTranslationBL != null)
                this.SharjahAwardForTranslationBL.Dispose();
        }
        #endregion
        #region Methods

        public XsiSharjahAwardForTranslation GetSharjahAwardForTranslationByItemId(long itemId)
        {
            return SharjahAwardForTranslationBL.GetSharjahAwardForTranslationByItemId(itemId);
        }
        
        public List<XsiSharjahAwardForTranslation> GetSharjahAwardForTranslation()
        {
            return SharjahAwardForTranslationBL.GetSharjahAwardForTranslation();
        }
        public List<XsiSharjahAwardForTranslation> GetSharjahAwardForTranslation(EnumSortlistBy sortListBy)
        {
            return SharjahAwardForTranslationBL.GetSharjahAwardForTranslation(sortListBy);
        }
        public List<XsiSharjahAwardForTranslation> GetSharjahAwardForTranslation(XsiSharjahAwardForTranslation entity)
        {
            return SharjahAwardForTranslationBL.GetSharjahAwardForTranslation(entity);
        }
        public List<XsiSharjahAwardForTranslation> GetSharjahAwardForTranslation(XsiSharjahAwardForTranslation entity, EnumSortlistBy sortListBy)
        {
            return SharjahAwardForTranslationBL.GetSharjahAwardForTranslation(entity, sortListBy);
        }
        public EnumResultType InsertSharjahAwardForTranslation(XsiSharjahAwardForTranslation entity)
        {
            return SharjahAwardForTranslationBL.InsertSharjahAwardForTranslation(entity);
        }
        public EnumResultType UpdateSharjahAwardForTranslation(XsiSharjahAwardForTranslation entity)
        {
            return SharjahAwardForTranslationBL.UpdateSharjahAwardForTranslation(entity);
        }
        public EnumResultType UpdateSharjahAwardForTranslationStatus(long itemId)
        {
            return SharjahAwardForTranslationBL.UpdateSharjahAwardForTranslationStatus(itemId);
        }
        public EnumResultType DeleteSharjahAwardForTranslation(XsiSharjahAwardForTranslation entity)
        {
            return SharjahAwardForTranslationBL.DeleteSharjahAwardForTranslation(entity);
        }
        public EnumResultType DeleteSharjahAwardForTranslation(long itemId)
        {
            return SharjahAwardForTranslationBL.DeleteSharjahAwardForTranslation(itemId);
        }
        #endregion
    }
}
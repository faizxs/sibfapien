﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class LanguageService : IDisposable
    {
        #region Feilds
        LanguageBL XsiLanguageBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return XsiLanguageBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return XsiLanguageBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public LanguageService()
        {
            XsiLanguageBL = new LanguageBL();
        }
        public LanguageService(string connectionString)
        {
            XsiLanguageBL = new LanguageBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiLanguageBL != null)
                this.XsiLanguageBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiLanguages GetLanguageByItemId(long itemId)
        {
            return XsiLanguageBL.GetLanguageByItemId(itemId);
        }
        public XsiLanguages GetDefaultLanguage()
        {
            return XsiLanguageBL.GetDefaultLanguage();
        }
        public bool IsLanguageDirectionLeft(long itemId)
        {
            return XsiLanguageBL.IsLanguageDirectionLeft(itemId);
        }

        public List<XsiLanguages> GetLanguage()
        {
            return XsiLanguageBL.GetLanguage(EnumSortlistBy.ByItemIdAsc);
        }
        public List<XsiLanguages> GetLanguage(EnumSortlistBy sortListBy = EnumSortlistBy.ByItemIdDesc)
        {
            return XsiLanguageBL.GetLanguage(sortListBy);
        }
        public List<XsiLanguages> GetLanguage(XsiLanguages entity)
        {
            return XsiLanguageBL.GetLanguage(entity);
        }
        public List<XsiLanguages> GetLanguage(XsiLanguages entity, EnumSortlistBy sortListBy)
        {
            return XsiLanguageBL.GetLanguage(entity, sortListBy);
        }
        public List<XsiLanguages> GetActiveLanguages()
        {
            return XsiLanguageBL.GetActiveLanguages();
        }

        public EnumResultType InsertLanguage(XsiLanguages entity)
        {
            return XsiLanguageBL.InsertLanguage(entity);
        }
        public EnumResultType UpdateLanguage(XsiLanguages entity)
        {
            return XsiLanguageBL.UpdateLanguage(entity);
        }
        public EnumResultType UpdateLanguageStatus(long itemId)
        {
            return XsiLanguageBL.UpdateLanguageStatus(itemId);
        }
        public EnumResultType UpdateDefaultLanguage(long itemId)
        {
            return XsiLanguageBL.UpdateDefaultLanguage(itemId);
        }
        public EnumResultType DeleteLanguage(XsiLanguages entity)
        {
            return XsiLanguageBL.DeleteLanguage(entity);
        }
        public EnumResultType DeleteLanguage(long itemId)
        {
            return XsiLanguageBL.DeleteLanguage(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;
using System.Linq;

namespace Xsi.ServicesLayer
{
    public class AdminPageService : IDisposable
    {
        #region Feilds
        AdminPageBL XsiAdminPageBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return XsiAdminPageBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return XsiAdminPageBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public AdminPageService()
        {
            XsiAdminPageBL = new AdminPageBL();
        }
        public AdminPageService(string connectionString)
        {
            XsiAdminPageBL = new AdminPageBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiAdminPageBL != null)
                this.XsiAdminPageBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdminPages GetAdminPageByItemId(long itemId)
        {
            return XsiAdminPageBL.GetAdminPageByItemId(itemId);
        }
        public List<XsiAdminPages> GetAdminPage(EnumSortlistBy sortListBy = EnumSortlistBy.ByItemIdDesc)
        {
            return XsiAdminPageBL.GetAdminPage(sortListBy);
        }
        public List<XsiAdminPages> GetAdminPage(XsiAdminPages entity)
        {
            return XsiAdminPageBL.GetAdminPage(entity);
        }
        public List<XsiAdminPages> GetAdminPageAndPermission(XsiAdminPages entity)
        {
            return XsiAdminPageBL.GetAdminPageAndPermission(entity);
        }
        public EnumResultType InsertAdminPage(XsiAdminPages entity)
        {
            return XsiAdminPageBL.InsertAdminPage(entity);
        }
        public EnumResultType UpdateAdminPage(XsiAdminPages entity)
        {
            return XsiAdminPageBL.UpdateAdminPage(entity);
        }
        public EnumResultType DeleteAdminPage(XsiAdminPages entity)
        {
            return XsiAdminPageBL.DeleteAdminPage(entity);
        }
        public EnumResultType DeleteAdminPage(long itemId)
        {
            return XsiAdminPageBL.DeleteAdminPage(itemId);
        }
        #endregion
    }
}
﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class CareerSpecialityService : IDisposable
    {
        #region Feilds
        CareerSpecialityBL CareerSpecialityBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return CareerSpecialityBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return CareerSpecialityBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public CareerSpecialityService()
        {
            CareerSpecialityBL = new CareerSpecialityBL();
        }
        public CareerSpecialityService(string connectionString)
        {
            CareerSpecialityBL = new CareerSpecialityBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerSpecialityBL != null)
                this.CareerSpecialityBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiCareerSpeciality GetCareerSpecialityByItemId(long itemId)
        {
            return CareerSpecialityBL.GetCareerSpecialityByItemId(itemId);
        }
        
        public List<XsiCareerSpeciality> GetCareerSpeciality()
        {
            return CareerSpecialityBL.GetCareerSpeciality();
        }
        public List<XsiCareerSpeciality> GetCareerSpeciality(EnumSortlistBy sortListBy)
        {
            return CareerSpecialityBL.GetCareerSpeciality(sortListBy);
        }
        public List<XsiCareerSpeciality> GetCareerSpeciality(XsiCareerSpeciality entity)
        {
            return CareerSpecialityBL.GetCareerSpeciality(entity);
        }
        public List<XsiCareerSpeciality> GetCareerSpeciality(XsiCareerSpeciality entity, EnumSortlistBy sortListBy)
        {
            return CareerSpecialityBL.GetCareerSpeciality(entity, sortListBy);
        }
        public List<XsiCareerSpeciality> GetCareerSpecialityOr(XsiCareerSpeciality entity, EnumSortlistBy sortListBy)
        {
            return CareerSpecialityBL.GetCareerSpecialityOr(entity, sortListBy);
        }
         
        public EnumResultType InsertCareerSpeciality(XsiCareerSpeciality entity)
        {
            return CareerSpecialityBL.InsertCareerSpeciality(entity);
        }
        public EnumResultType UpdateCareerSpeciality(XsiCareerSpeciality entity)
        {
            return CareerSpecialityBL.UpdateCareerSpeciality(entity);
        }
        public EnumResultType UpdateCareerSpecialityStatus(long itemId)
        {
            return CareerSpecialityBL.UpdateCareerSpecialityStatus(itemId);
        }
        public EnumResultType DeleteCareerSpeciality(XsiCareerSpeciality entity)
        {
            return CareerSpecialityBL.DeleteCareerSpeciality(entity);
        }
        public EnumResultType DeleteCareerSpeciality(long itemId)
        {
            return CareerSpecialityBL.DeleteCareerSpeciality(itemId);
        }
        #endregion
    }
}
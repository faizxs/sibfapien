﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitorStatusLogService : IDisposable
    {
        #region Feilds
        ExhibitorStatusLogBL ExhibitorStatusLogBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitorStatusLogBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitorStatusLogBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitorStatusLogService()
        {
            ExhibitorStatusLogBL = new ExhibitorStatusLogBL();
        }
        public ExhibitorStatusLogService(string connectionString)
        {
            ExhibitorStatusLogBL = new ExhibitorStatusLogBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitorStatusLogBL != null)
                this.ExhibitorStatusLogBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ExhibitorStatusLogBL.GetNextGroupId();
        }
        public XsiExhibitionMemberApplicationYearlyLogs GetExhibitorStatusLogByItemId(long itemId)
        {
            return ExhibitorStatusLogBL.GetExhibitorStatusLogByItemId(itemId);
        }
         
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetExhibitorStatusLog()
        {
            return ExhibitorStatusLogBL.GetExhibitorStatusLog();
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetExhibitorStatusLog(EnumSortlistBy sortListBy)
        {
            return ExhibitorStatusLogBL.GetExhibitorStatusLog(sortListBy);
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetExhibitorStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            return ExhibitorStatusLogBL.GetExhibitorStatusLog(entity);
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetExhibitorStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity, EnumSortlistBy sortListBy)
        {
            return ExhibitorStatusLogBL.GetExhibitorStatusLog(entity, sortListBy);
        }
        public XsiExhibitionMemberApplicationYearlyLogs GetExhibitorStatusLog(long groupId, long languageId)
        {
            return ExhibitorStatusLogBL.GetExhibitorStatusLog(groupId, languageId);
        }

        public EnumResultType InsertExhibitorStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            return ExhibitorStatusLogBL.InsertExhibitorStatusLog(entity);
        }
        public EnumResultType UpdateExhibitorStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            return ExhibitorStatusLogBL.UpdateExhibitorStatusLog(entity);
        }
        public EnumResultType UpdateExhibitorStatusLogStatus(long itemId)
        {
            return ExhibitorStatusLogBL.UpdateExhibitorStatusLogStatus(itemId);
        }
        public EnumResultType DeleteExhibitorStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            return ExhibitorStatusLogBL.DeleteExhibitorStatusLog(entity);
        }
        public EnumResultType DeleteExhibitorStatusLog(long itemId)
        {
            return ExhibitorStatusLogBL.DeleteExhibitorStatusLog(itemId);
        }
        #endregion
    }
}
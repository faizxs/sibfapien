﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class OurteamStaffService : IDisposable
    {
        #region Feilds
        OurteamStaffBL OurteamStaffBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return OurteamStaffBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return OurteamStaffBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public OurteamStaffService()
        {
            OurteamStaffBL = new OurteamStaffBL();
        }
        public OurteamStaffService(string connectionString)
        {
            OurteamStaffBL = new OurteamStaffBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.OurteamStaffBL != null)
                this.OurteamStaffBL.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiOurteamStaff GetOurteamStaffByItemId(long itemId)
        {
            return OurteamStaffBL.GetOurteamStaffByItemId(itemId);
        }
        
        public List<XsiOurteamStaff> GetOurteamStaff()
        {
            return OurteamStaffBL.GetOurteamStaff();
        }
        public List<XsiOurteamStaff> GetOurteamStaff(EnumSortlistBy sortListBy)
        {
            return OurteamStaffBL.GetOurteamStaff(sortListBy);
        }
        public List<XsiOurteamStaff> GetOurteamStaff(XsiOurteamStaff entity)
        {
            return OurteamStaffBL.GetOurteamStaff(entity);
        }
        public List<XsiOurteamStaff> GetOurteamStaff(XsiOurteamStaff entity, EnumSortlistBy sortListBy)
        {
            return OurteamStaffBL.GetOurteamStaff(entity, sortListBy);
        }
        public List<XsiOurteamStaff> GetCompleteOurteamStaff(XsiOurteamStaff entity, EnumSortlistBy sortListBy)
        {
            return OurteamStaffBL.GetCompleteOurteamStaff(entity, sortListBy);
        }
       
        public EnumResultType InsertOurteamStaff(XsiOurteamStaff entity)
        {
            return OurteamStaffBL.InsertOurteamStaff(entity);
        }
        public EnumResultType UpdateOurteamStaff(XsiOurteamStaff entity)
        {
            return OurteamStaffBL.UpdateOurteamStaff(entity);
        }
        public EnumResultType UpdateOurteamStaffStatus(long itemId)
        {
            return OurteamStaffBL.UpdateOurteamStaffStatus(itemId);
        }
        public EnumResultType DeleteOurteamStaff(XsiOurteamStaff entity)
        {
            return OurteamStaffBL.DeleteOurteamStaff(entity);
        }
        public EnumResultType DeleteOurteamStaff(long itemId)
        {
            return OurteamStaffBL.DeleteOurteamStaff(itemId);
        }
        #endregion
    }
}
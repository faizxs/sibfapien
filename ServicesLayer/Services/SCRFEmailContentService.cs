﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFEmailContentService : IDisposable
    {
        #region Feilds
        SCRFEmailContentBL SCRFEmailContentBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFEmailContentBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFEmailContentBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFEmailContentService()
        {
            SCRFEmailContentBL = new SCRFEmailContentBL();
        }
        public SCRFEmailContentService(string connectionString)
        {
            SCRFEmailContentBL = new SCRFEmailContentBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFEmailContentBL != null)
                this.SCRFEmailContentBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiScrfemailContent GetSCRFEmailContentByItemId(long itemId)
        {
            return SCRFEmailContentBL.GetSCRFEmailContentByItemId(itemId);
        }
         
        public List<XsiScrfemailContent> GetSCRFEmailContent()
        {
            return SCRFEmailContentBL.GetSCRFEmailContent();
        }
        public List<XsiScrfemailContent> GetSCRFEmailContent(EnumSortlistBy sortListBy)
        {
            return SCRFEmailContentBL.GetSCRFEmailContent(sortListBy);
        }
        public List<XsiScrfemailContent> GetSCRFEmailContent(XsiScrfemailContent entity)
        {
            return SCRFEmailContentBL.GetSCRFEmailContent(entity);
        }
        public List<XsiScrfemailContent> GetSCRFEmailContent(XsiScrfemailContent entity, EnumSortlistBy sortListBy)
        {
            return SCRFEmailContentBL.GetSCRFEmailContent(entity, sortListBy);
        }
        public List<XsiScrfemailContent> GetSCRFEmailContentOr(XsiScrfemailContent entity, EnumSortlistBy sortListBy)
        {
            return SCRFEmailContentBL.GetSCRFEmailContentOr(entity, sortListBy);
        }
        
        public EnumResultType InsertSCRFEmailContent(XsiScrfemailContent entity)
        {
            return SCRFEmailContentBL.InsertSCRFEmailContent(entity);
        }
        public EnumResultType UpdateSCRFEmailContent(XsiScrfemailContent entity)
        {
            return SCRFEmailContentBL.UpdateSCRFEmailContent(entity);
        }
        public EnumResultType RollbackSCRFEmailContent(long itemId)
        {
            return SCRFEmailContentBL.RollbackSCRFEmailContent(itemId);
        }
        public EnumResultType DeleteSCRFEmailContent(XsiScrfemailContent entity)
        {
            return SCRFEmailContentBL.DeleteSCRFEmailContent(entity);
        }
        public EnumResultType DeleteSCRFEmailContent(long itemId)
        {
            return SCRFEmailContentBL.DeleteSCRFEmailContent(itemId);
        }
        #endregion
    }
}
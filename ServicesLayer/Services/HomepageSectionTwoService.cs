﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class HomepageSectionTwoService : IDisposable
    {
        #region Feilds
        HomepageSectionTwoBL HomepageSectionTwoBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return HomepageSectionTwoBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return HomepageSectionTwoBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public HomepageSectionTwoService()
        {
            HomepageSectionTwoBL = new HomepageSectionTwoBL();
        }
        public HomepageSectionTwoService(string connectionString)
        {
            HomepageSectionTwoBL = new HomepageSectionTwoBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomepageSectionTwoBL != null)
                this.HomepageSectionTwoBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiHomepageSectionTwo GetHomepageSectionTwoByItemId(long itemId)
        {
            return HomepageSectionTwoBL.GetHomepageSectionTwoByItemId(itemId);
        }

        public List<XsiHomepageSectionTwo> GetHomepageSectionTwo()
        {
            return HomepageSectionTwoBL.GetHomepageSectionTwo();
        }
        public List<XsiHomepageSectionTwo> GetHomepageSectionTwo(EnumSortlistBy sortListBy)
        {
            return HomepageSectionTwoBL.GetHomepageSectionTwo(sortListBy);
        }
        public List<XsiHomepageSectionTwo> GetHomepageSectionTwo(XsiHomepageSectionTwo entity)
        {
            return HomepageSectionTwoBL.GetHomepageSectionTwo(entity);
        }
        public List<XsiHomepageSectionTwo> GetHomepageSectionTwo(XsiHomepageSectionTwo entity, EnumSortlistBy sortListBy)
        {
            return HomepageSectionTwoBL.GetHomepageSectionTwo(entity, sortListBy);
        }
        public List<XsiHomepageSectionTwo> GetCompleteHomepageSectionTwo(XsiHomepageSectionTwo entity, EnumSortlistBy sortListBy, long langId)
        {
            return HomepageSectionTwoBL.GetCompleteHomepageSectionTwo(entity, sortListBy, langId);
        }
        public XsiHomepageSectionTwo GetHomepageSectionTwo(long groupId, long categoryId)
        {
            return HomepageSectionTwoBL.GetHomepageSectionTwo(groupId, categoryId);
        }

        public EnumResultType InsertHomepageSectionTwo(XsiHomepageSectionTwo entity)
        {
            return HomepageSectionTwoBL.InsertHomepageSectionTwo(entity);
        }
        public EnumResultType UpdateHomepageSectionTwo(XsiHomepageSectionTwo entity)
        {
            return HomepageSectionTwoBL.UpdateHomepageSectionTwo(entity);
        }
        public EnumResultType UpdateHomepageSectionTwoStatus(long itemId)
        {
            return HomepageSectionTwoBL.UpdateHomepageSectionTwoStatus(itemId);
        }
        public EnumResultType DeleteHomepageSectionTwo(XsiHomepageSectionTwo entity)
        {
            return HomepageSectionTwoBL.DeleteHomepageSectionTwo(entity);
        }
        public EnumResultType DeleteHomepageSectionTwo(long itemId)
        {
            return HomepageSectionTwoBL.DeleteHomepageSectionTwo(itemId);
        }
        #endregion
    }
}
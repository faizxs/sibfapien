﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PhotoCategoryService : IDisposable
    {
        #region Feilds
        PhotoCategoryBL PhotoCategoryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PhotoCategoryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return PhotoCategoryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PhotoCategoryService()
        {
            PhotoCategoryBL = new PhotoCategoryBL();
        }
        public PhotoCategoryService(string connectionString)
        {
            PhotoCategoryBL = new PhotoCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PhotoCategoryBL != null)
                this.PhotoCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiPhotoCategory GetPhotoCategoryByItemId(long itemId)
        {
            return PhotoCategoryBL.GetPhotoCategoryByItemId(itemId);
        }

        public List<XsiPhotoCategory> GetPhotoCategory()
        {
            return PhotoCategoryBL.GetPhotoCategory();
        }
        public List<XsiPhotoCategory> GetPhotoCategory(EnumSortlistBy sortListBy)
        {
            return PhotoCategoryBL.GetPhotoCategory(sortListBy);
        }
        public List<XsiPhotoCategory> GetPhotoCategory(XsiPhotoCategory entity)
        {
            return PhotoCategoryBL.GetPhotoCategory(entity);
        }
        public List<XsiPhotoCategory> GetPhotoCategory(XsiPhotoCategory entity, EnumSortlistBy sortListBy)
        {
            return PhotoCategoryBL.GetPhotoCategory(entity, sortListBy);
        }
        public List<XsiPhotoCategory> GetPhotoCategoryOr(XsiPhotoCategory entity, EnumSortlistBy sortListBy)
        {
            return PhotoCategoryBL.GetPhotoCategoryOr(entity, sortListBy);
        }
       /* public List<XsiPhotoCategory> GetOtherLanguagePhotoCategory(XsiPhotoCategory entity)
        {
            return PhotoCategoryBL.GetOtherLanguagePhotoCategory(entity);
        }
        public List<XsiPhotoCategory> GetCurrentLanguagePhotoCategory(XsiPhotoCategory entity)
        {
            return PhotoCategoryBL.GetCurrentLanguagePhotoCategory(entity);
        }*/

        public EnumResultType InsertPhotoCategory(XsiPhotoCategory entity)
        {
            return PhotoCategoryBL.InsertPhotoCategory(entity);
        }
        public EnumResultType UpdatePhotoCategory(XsiPhotoCategory entity)
        {
            return PhotoCategoryBL.UpdatePhotoCategory(entity);
        }
        public EnumResultType UpdatePhotoCategoryStatus(long itemId, long langId)
        {
            return PhotoCategoryBL.UpdatePhotoCategoryStatus(itemId, langId);
        }
        public EnumResultType DeletePhotoCategory(XsiPhotoCategory entity)
        {
            return PhotoCategoryBL.DeletePhotoCategory(entity);
        }
        public EnumResultType DeletePhotoCategory(long itemId)
        {
            return PhotoCategoryBL.DeletePhotoCategory(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class HomepageBannerService : IDisposable
    {
        #region Feilds
        HomepageBannerBL HomepageBannerBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return HomepageBannerBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return HomepageBannerBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public HomepageBannerService()
        {
            HomepageBannerBL = new HomepageBannerBL();
        }
        public HomepageBannerService(string connectionString)
        {
            HomepageBannerBL = new HomepageBannerBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomepageBannerBL != null)
                this.HomepageBannerBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiHomepageBanner GetHomepageBannerByItemId(long itemId)
        {
            return HomepageBannerBL.GetHomepageBannerByItemId(itemId);
        }

        public List<XsiHomepageBanner> GetHomepageBanner()
        {
            return HomepageBannerBL.GetHomepageBanner();
        }
        public List<XsiHomepageBanner> GetHomepageBanner(EnumSortlistBy sortListBy)
        {
            return HomepageBannerBL.GetHomepageBanner(sortListBy);
        }
        public List<XsiHomepageBanner> GetHomepageBanner(XsiHomepageBanner entity)
        {
            return HomepageBannerBL.GetHomepageBanner(entity);
        }
        public List<XsiHomepageBanner> GetHomepageBanner(XsiHomepageBanner entity, EnumSortlistBy sortListBy)
        {
            return HomepageBannerBL.GetHomepageBanner(entity, sortListBy);
        }
        public EnumResultType InsertHomepageBanner(XsiHomepageBanner entity)
        {
            return HomepageBannerBL.InsertHomepageBanner(entity);
        }
        public EnumResultType UpdateHomepageBanner(XsiHomepageBanner entity)
        {
            return HomepageBannerBL.UpdateHomepageBanner(entity);
        }
        public EnumResultType UpdateHomepageBannerStatus(long itemId)
        {
            return HomepageBannerBL.UpdateHomepageBannerStatus(itemId);
        }
        public EnumResultType DeleteHomepageBanner(XsiHomepageBanner entity)
        {
            return HomepageBannerBL.DeleteHomepageBanner(entity);
        }
        public EnumResultType DeleteHomepageBanner(long itemId)
        {
            return HomepageBannerBL.DeleteHomepageBanner(itemId);
        }

        //Custom 
        public long GetHomeBannerInfo(long languageid)
        {
            return HomepageBannerBL.GetHomeBannerInfo(languageid);
        }
        #endregion
    }
}
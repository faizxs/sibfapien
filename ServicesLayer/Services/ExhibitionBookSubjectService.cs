﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionBookSubjectService : IDisposable
    {
        #region Feilds
        ExhibitionBookSubjectBL ExhibitionBookSubjectBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionBookSubjectBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionBookSubjectBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionBookSubjectService()
        {
            ExhibitionBookSubjectBL = new ExhibitionBookSubjectBL();
        }
        public ExhibitionBookSubjectService(string connectionString)
        {
            ExhibitionBookSubjectBL = new ExhibitionBookSubjectBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionBookSubjectBL != null)
                this.ExhibitionBookSubjectBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionBookSubject GetExhibitionBookSubjectByItemId(long itemId)
        {
            return ExhibitionBookSubjectBL.GetExhibitionBookSubjectByItemId(itemId);
        }
         
        public List<XsiExhibitionBookSubject> GetExhibitionBookSubject()
        {
            return ExhibitionBookSubjectBL.GetExhibitionBookSubject();
        }
        public List<XsiExhibitionBookSubject> GetExhibitionBookSubject(EnumSortlistBy sortListBy)
        {
            return ExhibitionBookSubjectBL.GetExhibitionBookSubject(sortListBy);
        }
        public List<XsiExhibitionBookSubject> GetExhibitionBookSubject(XsiExhibitionBookSubject entity)
        {
            return ExhibitionBookSubjectBL.GetExhibitionBookSubject(entity);
        }
        public List<XsiExhibitionBookSubject> GetExhibitionBookSubject(XsiExhibitionBookSubject entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBookSubjectBL.GetExhibitionBookSubject(entity, sortListBy);
        }
        public List<XsiExhibitionBookSubject> GetExhibitionBookSubjectOr(XsiExhibitionBookSubject entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBookSubjectBL.GetExhibitionBookSubjectOr(entity, sortListBy);
        }
        
        public List<XsiExhibitionBookSubject> GetOtherLanguageExhibitionBookSubject(XsiExhibitionBookSubject entity)
        {
            return ExhibitionBookSubjectBL.GetOtherLanguageExhibitionBookSubject(entity);
        }
        public List<XsiExhibitionBookSubject> GetCurrentLanguageExhibitionBookSubject(XsiExhibitionBookSubject entity)
        {
            return ExhibitionBookSubjectBL.GetCurrentLanguageExhibitionBookSubject(entity);
        }

        public EnumResultType InsertExhibitionBookSubject(XsiExhibitionBookSubject entity)
        {
            return ExhibitionBookSubjectBL.InsertExhibitionBookSubject(entity);
        }
        public EnumResultType UpdateExhibitionBookSubject(XsiExhibitionBookSubject entity)
        {
            return ExhibitionBookSubjectBL.UpdateExhibitionBookSubject(entity);
        }
        public EnumResultType UpdateExhibitionBookSubjectStatus(long itemId)
        {
            return ExhibitionBookSubjectBL.UpdateExhibitionBookSubjectStatus(itemId);
        }
        public EnumResultType DeleteExhibitionBookSubject(XsiExhibitionBookSubject entity)
        {
            return ExhibitionBookSubjectBL.DeleteExhibitionBookSubject(entity);
        }
        public EnumResultType DeleteExhibitionBookSubject(long itemId)
        {
            return ExhibitionBookSubjectBL.DeleteExhibitionBookSubject(itemId);
        }
        #endregion
    }
}
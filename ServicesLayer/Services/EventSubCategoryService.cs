﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class EventSubCategoryService : IDisposable
    {
        #region Feilds
        EventSubCategoryBL EventSubCategoryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return EventSubCategoryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return EventSubCategoryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public EventSubCategoryService()
        {
            EventSubCategoryBL = new EventSubCategoryBL();
        }
        public EventSubCategoryService(string connectionString)
        {
            EventSubCategoryBL = new EventSubCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventSubCategoryBL != null)
                this.EventSubCategoryBL.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiEventSubCategory GetEventSubCategoryByItemId(long itemId)
        {
            return EventSubCategoryBL.GetEventSubCategoryByItemId(itemId);
        }       
        public List<XsiEventSubCategory> GetEventSubCategory()
        {
            return EventSubCategoryBL.GetEventSubCategory();
        }
        public List<XsiEventSubCategory> GetEventSubCategory(EnumSortlistBy sortListBy)
        {
            return EventSubCategoryBL.GetEventSubCategory(sortListBy);
        }
        public List<XsiEventSubCategory> GetEventSubCategory(XsiEventSubCategory entity)
        {
            return EventSubCategoryBL.GetEventSubCategory(entity);
        }
        public List<XsiEventSubCategory> GetEventSubCategory(XsiEventSubCategory entity, EnumSortlistBy sortListBy)
        {
            return EventSubCategoryBL.GetEventSubCategory(entity, sortListBy);
        }
        public List<XsiEventSubCategory> GetCompleteEventSubCategory(XsiEventSubCategory entity, EnumSortlistBy sortListBy)
        {
            return EventSubCategoryBL.GetCompleteEventSubCategory(entity, sortListBy);
        }
       
        public EnumResultType InsertEventSubCategory(XsiEventSubCategory entity)
        {
            return EventSubCategoryBL.InsertEventSubCategory(entity);
        }
        public EnumResultType UpdateEventSubCategory(XsiEventSubCategory entity)
        {
            return EventSubCategoryBL.UpdateEventSubCategory(entity);
        }
        public EnumResultType UpdateEventSubCategoryStatus(long itemId)
        {
            return EventSubCategoryBL.UpdateEventSubCategoryStatus(itemId);
        }
        public EnumResultType DeleteEventSubCategory(XsiEventSubCategory entity)
        {
            return EventSubCategoryBL.DeleteEventSubCategory(entity);
        }
        public EnumResultType DeleteEventSubCategory(long itemId)
        {
            return EventSubCategoryBL.DeleteEventSubCategory(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionBoothSubsectionService : IDisposable
    {
        #region Feilds
        ExhibitionBoothSubsectionBL ExhibitionBoothSubsectionBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionBoothSubsectionBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionBoothSubsectionBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionBoothSubsectionService()
        {
            ExhibitionBoothSubsectionBL = new ExhibitionBoothSubsectionBL();
        }
        public ExhibitionBoothSubsectionService(string connectionString)
        {
            ExhibitionBoothSubsectionBL = new ExhibitionBoothSubsectionBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionBoothSubsectionBL != null)
                this.ExhibitionBoothSubsectionBL.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionBoothSubsection GetExhibitionBoothSubsectionByItemId(long itemId)
        {
            return ExhibitionBoothSubsectionBL.GetExhibitionBoothSubsectionByItemId(itemId);
        }
        
        public List<XsiExhibitionBoothSubsection> GetExhibitionBoothSubsection()
        {
            return ExhibitionBoothSubsectionBL.GetExhibitionBoothSubsection();
        }
        public List<XsiExhibitionBoothSubsection> GetExhibitionBoothSubsection(EnumSortlistBy sortListBy)
        {
            return ExhibitionBoothSubsectionBL.GetExhibitionBoothSubsection(sortListBy);
        }
        public List<XsiExhibitionBoothSubsection> GetExhibitionBoothSubsection(XsiExhibitionBoothSubsection entity)
        {
            return ExhibitionBoothSubsectionBL.GetExhibitionBoothSubsection(entity);
        }
        public List<XsiExhibitionBoothSubsection> GetExhibitionBoothSubsection(XsiExhibitionBoothSubsection entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBoothSubsectionBL.GetExhibitionBoothSubsection(entity, sortListBy);
        }
        public List<XsiExhibitionBoothSubsection> GetCompleteExhibitionBoothSubsection(XsiExhibitionBoothSubsection entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionBoothSubsectionBL.GetCompleteExhibitionBoothSubsection(entity, sortListBy);
        }
        
        public EnumResultType InsertExhibitionBoothSubsection(XsiExhibitionBoothSubsection entity)
        {
            return ExhibitionBoothSubsectionBL.InsertExhibitionBoothSubsection(entity);
        }
        public EnumResultType UpdateExhibitionBoothSubsection(XsiExhibitionBoothSubsection entity)
        {
            return ExhibitionBoothSubsectionBL.UpdateExhibitionBoothSubsection(entity);
        }
        public EnumResultType UpdateExhibitionBoothSubsectionStatus(long itemId)
        {
            return ExhibitionBoothSubsectionBL.UpdateExhibitionBoothSubsectionStatus(itemId);
        }
        public EnumResultType DeleteExhibitionBoothSubsection(XsiExhibitionBoothSubsection entity)
        {
            return ExhibitionBoothSubsectionBL.DeleteExhibitionBoothSubsection(entity);
        }
        public EnumResultType DeleteExhibitionBoothSubsection(long itemId)
        {
            return ExhibitionBoothSubsectionBL.DeleteExhibitionBoothSubsection(itemId);
        }
        #endregion
    }
}
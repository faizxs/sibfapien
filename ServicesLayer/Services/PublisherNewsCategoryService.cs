﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PublisherNewsCategoryService : IDisposable
    {
        #region Feilds
        PublisherNewsCategoryBL PublisherNewsCategoryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PublisherNewsCategoryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return PublisherNewsCategoryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public PublisherNewsCategoryService()
        {
            PublisherNewsCategoryBL = new PublisherNewsCategoryBL();
        }
        public PublisherNewsCategoryService(string connectionString)
        {
            PublisherNewsCategoryBL = new PublisherNewsCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PublisherNewsCategoryBL != null)
                this.PublisherNewsCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiPublisherNewsCategory GetPublisherNewsCategoryByItemId(long itemId)
        {
            return PublisherNewsCategoryBL.GetPublisherNewsCategoryByItemId(itemId);
        }
         

        public List<XsiPublisherNewsCategory> GetPublisherNewsCategory()
        {
            return PublisherNewsCategoryBL.GetPublisherNewsCategory();
        }
        public List<XsiPublisherNewsCategory> GetPublisherNewsCategory(EnumSortlistBy sortListBy)
        {
            return PublisherNewsCategoryBL.GetPublisherNewsCategory(sortListBy);
        }
        public List<XsiPublisherNewsCategory> GetPublisherNewsCategory(XsiPublisherNewsCategory entity)
        {
            return PublisherNewsCategoryBL.GetPublisherNewsCategory(entity);
        }
        public List<XsiPublisherNewsCategory> GetPublisherNewsCategory(XsiPublisherNewsCategory entity, EnumSortlistBy sortListBy)
        {
            return PublisherNewsCategoryBL.GetPublisherNewsCategory(entity, sortListBy);
        }
        public List<XsiPublisherNewsCategory> GetPublisherNewsCategoryOr(XsiPublisherNewsCategory entity, EnumSortlistBy sortListBy)
        {
            return PublisherNewsCategoryBL.GetPublisherNewsCategoryOr(entity, sortListBy);
        }
        //public XsiPublisherNewsCategory GetPublisherNewsCategory(long groupId, long languageId)
        //{
        //    return PublisherNewsCategoryBL.GetPublisherNewsCategory(groupId, languageId);
        //}
        public List<XsiPublisherNewsCategory> GetOtherLanguagePublisherNewsCategory(XsiPublisherNewsCategory entity)
        {
            return PublisherNewsCategoryBL.GetOtherLanguagePublisherNewsCategory(entity);
        }
        public List<XsiPublisherNewsCategory> GetCurrentLanguagePublisherNewsCategory(XsiPublisherNewsCategory entity)
        {
            return PublisherNewsCategoryBL.GetCurrentLanguagePublisherNewsCategory(entity);
        }

        public EnumResultType InsertPublisherNewsCategory(XsiPublisherNewsCategory entity)
        {
            return PublisherNewsCategoryBL.InsertPublisherNewsCategory(entity);
        }
        public EnumResultType UpdatePublisherNewsCategory(XsiPublisherNewsCategory entity)
        {
            return PublisherNewsCategoryBL.UpdatePublisherNewsCategory(entity);
        }
        public EnumResultType UpdatePublisherNewsCategoryStatus(long itemId)
        {
            return PublisherNewsCategoryBL.UpdatePublisherNewsCategoryStatus(itemId);
        }
        public EnumResultType DeletePublisherNewsCategory(XsiPublisherNewsCategory entity)
        {
            return PublisherNewsCategoryBL.DeletePublisherNewsCategory(entity);
        }
        public EnumResultType DeletePublisherNewsCategory(long itemId)
        {
            return PublisherNewsCategoryBL.DeletePublisherNewsCategory(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class QuickLinksService : IDisposable
    {
        #region Feilds
        QuickLinksBL QuickLinksBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return QuickLinksBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return QuickLinksBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public QuickLinksService()
        {
            QuickLinksBL = new QuickLinksBL();
        }
        public QuickLinksService(string connectionString)
        {
            QuickLinksBL = new QuickLinksBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.QuickLinksBL != null)
                this.QuickLinksBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiQuickLinks GetQuickLinksByItemId(long itemId)
        {
            return QuickLinksBL.GetQuickLinksByItemId(itemId);
        }

        public List<XsiQuickLinks> GetQuickLinks()
        {
            return QuickLinksBL.GetQuickLinks();
        }
        public List<XsiQuickLinks> GetQuickLinks(EnumSortlistBy sortListBy)
        {
            return QuickLinksBL.GetQuickLinks(sortListBy);
        }
        public List<XsiQuickLinks> GetQuickLinks(XsiQuickLinks entity)
        {
            return QuickLinksBL.GetQuickLinks(entity);
        }
        public List<XsiQuickLinks> GetQuickLinks(XsiQuickLinks entity, EnumSortlistBy sortListBy)
        {
            return QuickLinksBL.GetQuickLinks(entity, sortListBy);
        }
        public List<XsiQuickLinks> GetQuickLinksOr(XsiQuickLinks entity, EnumSortlistBy sortListBy)
        {
            return QuickLinksBL.GetQuickLinksOr(entity, sortListBy);
        }
        public EnumResultType InsertQuickLinks(XsiQuickLinks entity)
        {
            return QuickLinksBL.InsertQuickLinks(entity);
        }
        public EnumResultType UpdateQuickLinks(XsiQuickLinks entity)
        {
            return QuickLinksBL.UpdateQuickLinks(entity);
        }
        public EnumResultType UpdateQuickLinksStatus(long itemId)
        {
            return QuickLinksBL.UpdateQuickLinksStatus(itemId);
        }
        public EnumResultType UpdateQuickLinksSortOrder(long groupId, long order)
        {
            return QuickLinksBL.UpdateQuickLinksSortOrder(groupId, order);
        }
        public EnumResultType DeleteQuickLinks(XsiQuickLinks entity)
        {
            return QuickLinksBL.DeleteQuickLinks(entity);
        }
        public EnumResultType DeleteQuickLinks(long itemId)
        {
            return QuickLinksBL.DeleteQuickLinks(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class HomepageBannerNewService : IDisposable
    {
        #region Feilds
        HomepageBannerNewBL HomepageBannerNewBL;
        #endregion
        #region Properties
        public long XsiItemId
        {
            get { return HomepageBannerNewBL.XsiItemId; }
        }
        #endregion
        #region Constructor
        public HomepageBannerNewService()
        {
            HomepageBannerNewBL = new HomepageBannerNewBL();
        }
        public HomepageBannerNewService(string connectionString)
        {
            HomepageBannerNewBL = new HomepageBannerNewBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomepageBannerNewBL != null)
                this.HomepageBannerNewBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiHomepageBannerNew GetHomepageBannerNewByItemId(long itemId)
        {
            return HomepageBannerNewBL.GetHomepageBannerNewByItemId(itemId);
        }

        public List<XsiHomepageBannerNew> GetHomepageBannerNew()
        {
            return HomepageBannerNewBL.GetHomepageBannerNew();
        }
        public List<XsiHomepageBannerNew> GetHomepageBannerNew(EnumSortlistBy sortListBy)
        {
            return HomepageBannerNewBL.GetHomepageBannerNew(sortListBy);
        }
        public List<XsiHomepageBannerNew> GetHomepageBannerNew(XsiHomepageBannerNew entity)
        {
            return HomepageBannerNewBL.GetHomepageBannerNew(entity);
        }
        public List<XsiHomepageBannerNew> GetHomepageBannerNew(XsiHomepageBannerNew entity, EnumSortlistBy sortListBy)
        {
            return HomepageBannerNewBL.GetHomepageBannerNew(entity, sortListBy);
        }
        public EnumResultType InsertHomepageBannerNew(XsiHomepageBannerNew entity)
        {
            return HomepageBannerNewBL.InsertHomepageBannerNew(entity);
        }
        public EnumResultType UpdateHomepageBannerNew(XsiHomepageBannerNew entity)
        {
            return HomepageBannerNewBL.UpdateHomepageBannerNew(entity);
        }
        public EnumResultType UpdateHomepageBannerNewStatus(long itemId)
        {
            return HomepageBannerNewBL.UpdateHomepageBannerNewStatus(itemId);
        }
        public EnumResultType DeleteHomepageBannerNew(XsiHomepageBannerNew entity)
        {
            return HomepageBannerNewBL.DeleteHomepageBannerNew(entity);
        }
        public EnumResultType DeleteHomepageBannerNew(long itemId)
        {
            return HomepageBannerNewBL.DeleteHomepageBannerNew(itemId);
        }
        public EnumResultType UpdateHomepageBannerNewSortOrder(long groupId, long order)
        {
            return HomepageBannerNewBL.UpdateHomepageBannerNewSortOrder(groupId, order);
        }

        //Custom 
        public long GetHomeBannerInfo()
        {
            return HomepageBannerNewBL.GetHomeBannerInfo();
        }
        #endregion
    }
}
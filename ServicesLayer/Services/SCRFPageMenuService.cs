﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFPageMenuService : IDisposable
    {
        #region Feilds
        SCRFPageMenuBL SCRFPageMenuBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFPageMenuBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFPageMenuBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFPageMenuService()
        {
            SCRFPageMenuBL = new SCRFPageMenuBL();
        }
        public SCRFPageMenuService(string connectionString)
        {
            SCRFPageMenuBL = new SCRFPageMenuBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFPageMenuBL != null)
                this.SCRFPageMenuBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfpageMenu GetSCRFPageMenuByItemId(long itemId)
        {
            return SCRFPageMenuBL.GetSCRFPageMenuByItemId(itemId);
        }

        public List<XsiScrfpageMenu> GetSCRFPageMenu()
        {
            return SCRFPageMenuBL.GetSCRFPageMenu();
        }
        public List<XsiScrfpageMenu> GetSCRFPageMenu(EnumSortlistBy sortListBy)
        {
            return SCRFPageMenuBL.GetSCRFPageMenu(sortListBy);
        }
        public List<XsiScrfpageMenu> GetSCRFPageMenu(XsiScrfpageMenu entity)
        {
            return SCRFPageMenuBL.GetSCRFPageMenu(entity);
        }
        public List<XsiScrfpageMenu> GetSCRFPageMenu(XsiScrfpageMenu entity, EnumSortlistBy sortListBy)
        {
            return SCRFPageMenuBL.GetSCRFPageMenu(entity, sortListBy);
        }
        public List<XsiScrfpageMenu> GetSCRFPageMenuOr(XsiScrfpageMenu entity, EnumSortlistBy sortListBy)
        {
            return SCRFPageMenuBL.GetSCRFPageMenuOr(entity, sortListBy);
        }

        public EnumResultType InsertSCRFPageMenu(XsiScrfpageMenu entity)
        {
            return SCRFPageMenuBL.InsertSCRFPageMenu(entity);
        }
        public EnumResultType UpdateSCRFPageMenu(XsiScrfpageMenu entity)
        {
            return SCRFPageMenuBL.UpdateSCRFPageMenu(entity);
        }
        public EnumResultType UpdateSCRFPageMenuStatus(long itemId)
        {
            return SCRFPageMenuBL.UpdateSCRFPageMenuStatus(itemId);
        }
        public EnumResultType UpdateSCRFPageMenuSortOrder(long groupId, long order)
        {
            return SCRFPageMenuBL.UpdateSCRFPageMenuSortOrder(groupId, order);
        }
        public EnumResultType DeleteSCRFPageMenu(XsiScrfpageMenu entity)
        {
            return SCRFPageMenuBL.DeleteSCRFPageMenu(entity);
        }
        public EnumResultType DeleteSCRFPageMenu(long itemId)
        {
            return SCRFPageMenuBL.DeleteSCRFPageMenu(itemId);
        }
        #endregion
    }
}
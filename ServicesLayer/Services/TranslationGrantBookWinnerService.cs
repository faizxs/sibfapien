﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class TranslationGrantBookWinnerService : IDisposable
    {
        #region Feilds
        TranslationGrantBookWinnerBL TranslationGrantBookWinnerBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return TranslationGrantBookWinnerBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return TranslationGrantBookWinnerBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public TranslationGrantBookWinnerService()
        {
            TranslationGrantBookWinnerBL = new TranslationGrantBookWinnerBL();
        }
        public TranslationGrantBookWinnerService(string connectionString)
        {
            TranslationGrantBookWinnerBL = new TranslationGrantBookWinnerBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.TranslationGrantBookWinnerBL != null)
                this.TranslationGrantBookWinnerBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionTranslationGrantBookWinner GetTranslationGrantBookWinnerByItemId(long itemId)
        {
            return TranslationGrantBookWinnerBL.GetTranslationGrantBookWinnerByItemId(itemId);
        }

        public List<XsiExhibitionTranslationGrantBookWinner> GetTranslationGrantBookWinner()
        {
            return TranslationGrantBookWinnerBL.GetTranslationGrantBookWinner();
        }
        public List<XsiExhibitionTranslationGrantBookWinner> GetTranslationGrantBookWinner(EnumSortlistBy sortListBy)
        {
            return TranslationGrantBookWinnerBL.GetTranslationGrantBookWinner(sortListBy);
        }
        public List<XsiExhibitionTranslationGrantBookWinner> GetTranslationGrantBookWinner(XsiExhibitionTranslationGrantBookWinner entity)
        {
            return TranslationGrantBookWinnerBL.GetTranslationGrantBookWinner(entity);
        }
        public List<XsiExhibitionTranslationGrantBookWinner> GetTranslationGrantBookWinner(XsiExhibitionTranslationGrantBookWinner entity, EnumSortlistBy sortListBy)
        {
            return TranslationGrantBookWinnerBL.GetTranslationGrantBookWinner(entity, sortListBy);
        }

        public EnumResultType InsertTranslationGrantBookWinner(XsiExhibitionTranslationGrantBookWinner entity)
        {
            return TranslationGrantBookWinnerBL.InsertTranslationGrantBookWinner(entity);
        }
        public EnumResultType UpdateTranslationGrantBookWinner(XsiExhibitionTranslationGrantBookWinner entity)
        {
            return TranslationGrantBookWinnerBL.UpdateTranslationGrantBookWinner(entity);
        }
        public EnumResultType UpdateTranslationGrantBookWinnerStatus(long itemId)
        {
            return TranslationGrantBookWinnerBL.UpdateTranslationGrantBookWinnerStatus(itemId);
        }
        public EnumResultType DeleteTranslationGrantBookWinner(XsiExhibitionTranslationGrantBookWinner entity)
        {
            return TranslationGrantBookWinnerBL.DeleteTranslationGrantBookWinner(entity);
        }
        public EnumResultType DeleteTranslationGrantBookWinner(long itemId)
        {
            return TranslationGrantBookWinnerBL.DeleteTranslationGrantBookWinner(itemId);
        }
        #endregion
    }
}
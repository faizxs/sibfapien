﻿
using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class MemberReasonService : IDisposable
    {
        #region Feilds
        MemberReasonBL MemberReasonBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return MemberReasonBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return MemberReasonBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public MemberReasonService()
        {
            MemberReasonBL = new MemberReasonBL();
        }
        public MemberReasonService(string connectionString)
        {
            MemberReasonBL = new MemberReasonBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.MemberReasonBL != null)
                this.MemberReasonBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiMemberReason GetMemberReasonByItemId(long itemId)
        {
            return MemberReasonBL.GetMemberReasonByItemId(itemId);
        }
        public List<XsiMemberReason> GetMemberReason()
        {
            return MemberReasonBL.GetMemberReason();
        }
        public List<XsiMemberReason> GetMemberReason(EnumSortlistBy sortListBy)
        {
            return MemberReasonBL.GetMemberReason(sortListBy);
        }
        public List<XsiMemberReason> GetMemberReason(XsiMemberReason entity)
        {
            return MemberReasonBL.GetMemberReason(entity);
        }
        public List<XsiMemberReason> GetMemberReason(XsiMemberReason entity, EnumSortlistBy sortListBy)
        {
            return MemberReasonBL.GetMemberReason(entity, sortListBy);
        }
        public EnumResultType InsertMemberReason(XsiMemberReason entity)
        {
            return MemberReasonBL.InsertMemberReason(entity);
        }
        public EnumResultType UpdateMemberReason(XsiMemberReason entity)
        {
            return MemberReasonBL.UpdateMemberReason(entity);
        }
        public EnumResultType DeleteMemberReason(XsiMemberReason entity)
        {
            return MemberReasonBL.DeleteMemberReason(entity);
        }
        public EnumResultType DeleteMemberReason(long itemId)
        {
            return MemberReasonBL.DeleteMemberReason(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionEventtusService : IDisposable
    {
        #region Feilds
        ExhibitionEventtusBL ExhibitionEventtusBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionEventtusBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionEventtusBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionEventtusService()
        {
            ExhibitionEventtusBL = new ExhibitionEventtusBL();
        }
        public ExhibitionEventtusService(string connectionString)
        {
            ExhibitionEventtusBL = new ExhibitionEventtusBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionEventtusBL != null)
                this.ExhibitionEventtusBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            return ExhibitionEventtusBL.GetNextGroupId();
        }
        public XsiExhibitionEventtus GetExhibitionEventtusByItemId(long itemId)
        {
            return ExhibitionEventtusBL.GetExhibitionEventtusByItemId(itemId);
        }
        public List<XsiExhibitionEventtus> GetExhibitionEventtusByGroupId(long groupId)
        {
            return ExhibitionEventtusBL.GetExhibitionEventtusByGroupId(groupId);
        }

        public List<XsiExhibitionEventtus> GetExhibitionEventtus()
        {
            return ExhibitionEventtusBL.GetExhibitionEventtus();
        }
        public List<XsiExhibitionEventtus> GetExhibitionEventtus(EnumSortlistBy sortListBy)
        {
            return ExhibitionEventtusBL.GetExhibitionEventtus(sortListBy);
        }
        public List<XsiExhibitionEventtus> GetExhibitionEventtus(XsiExhibitionEventtus entity)
        {
            return ExhibitionEventtusBL.GetExhibitionEventtus(entity);
        }
        public List<XsiExhibitionEventtus> GetExhibitionEventtus(XsiExhibitionEventtus entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionEventtusBL.GetExhibitionEventtus(entity, sortListBy);
        }
        public List<XsiExhibitionEventtus> GetExhibitionEventtusOr(XsiExhibitionEventtus entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionEventtusBL.GetExhibitionEventtusOr(entity, sortListBy);
        }
        public XsiExhibitionEventtus GetExhibitionEventtus(long groupId, long languageId)
        {
            return ExhibitionEventtusBL.GetExhibitionEventtus(groupId, languageId);
        }
        public EnumResultType InsertExhibitionEventtus(XsiExhibitionEventtus entity)
        {
            return ExhibitionEventtusBL.InsertExhibitionEventtus(entity);
        }
        public EnumResultType UpdateExhibitionEventtus(XsiExhibitionEventtus entity)
        {
            return ExhibitionEventtusBL.UpdateExhibitionEventtus(entity);
        }
        public EnumResultType UpdateExhibitionEventtusStatus(long itemId)
        {
            return ExhibitionEventtusBL.UpdateExhibitionEventtusStatus(itemId);
        }
        public EnumResultType DeleteExhibitionEventtus(XsiExhibitionEventtus entity)
        {
            return ExhibitionEventtusBL.DeleteExhibitionEventtus(entity);
        }
        public EnumResultType DeleteExhibitionEventtus(long itemId)
        {
            return ExhibitionEventtusBL.DeleteExhibitionEventtus(itemId);
        }
        #endregion
    }
}
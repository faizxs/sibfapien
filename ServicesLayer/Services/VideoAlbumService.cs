﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class VideoAlbumService : IDisposable
    {
        #region Feilds
        VideoAlbumBL VideoAlbumBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return VideoAlbumBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return VideoAlbumBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public VideoAlbumService()
        {
            VideoAlbumBL = new VideoAlbumBL();
        }
        public VideoAlbumService(string connectionString)
        {
            VideoAlbumBL = new VideoAlbumBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.VideoAlbumBL != null)
                this.VideoAlbumBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextSortIndex()
        {
            return VideoAlbumBL.GetNextSortIndex();
        }
        public XsiVideoAlbum GetVideoAlbumByItemId(long itemId)
        {
            return VideoAlbumBL.GetVideoAlbumByItemId(itemId);
        }

        public List<XsiVideoAlbum> GetVideoAlbum()
        {
            return VideoAlbumBL.GetVideoAlbum();
        }
        public List<XsiVideoAlbum> GetVideoAlbum(EnumSortlistBy sortListBy)
        {
            return VideoAlbumBL.GetVideoAlbum(sortListBy);
        }
        public List<XsiVideoAlbum> GetVideoAlbum(XsiVideoAlbum entity)
        {
            return VideoAlbumBL.GetVideoAlbum(entity);
        }
        public List<XsiVideoAlbum> GetVideoAlbum(XsiVideoAlbum entity, EnumSortlistBy sortListBy)
        {
            return VideoAlbumBL.GetVideoAlbum(entity, sortListBy);
        }

        public EnumResultType InsertVideoAlbum(XsiVideoAlbum entity)
        {
            return VideoAlbumBL.InsertVideoAlbum(entity);
        }
        public EnumResultType UpdateVideoAlbum(XsiVideoAlbum entity)
        {
            return VideoAlbumBL.UpdateVideoAlbum(entity);
        }
        public EnumResultType UpdateVideoAlbumStatus(long itemId, long langId)
        {
            return VideoAlbumBL.UpdateVideoAlbumStatus(itemId, langId);
        }
        public EnumResultType DeleteVideoAlbum(XsiVideoAlbum entity)
        {
            return VideoAlbumBL.DeleteVideoAlbum(entity);
        }
        public EnumResultType DeleteVideoAlbum(long itemId)
        {
            return VideoAlbumBL.DeleteVideoAlbum(itemId);
        }
        #endregion
    }
}
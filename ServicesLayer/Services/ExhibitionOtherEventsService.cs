﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ExhibitionOtherEventsService : IDisposable
    {
        #region Feilds
        ExhibitionOtherEventsBL ExhibitionOtherEventsBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ExhibitionOtherEventsBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ExhibitionOtherEventsBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ExhibitionOtherEventsService()
        {
            ExhibitionOtherEventsBL = new ExhibitionOtherEventsBL();
        }
        public ExhibitionOtherEventsService(string connectionString)
        {
            ExhibitionOtherEventsBL = new ExhibitionOtherEventsBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionOtherEventsBL != null)
                this.ExhibitionOtherEventsBL.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionOtherEvents GetExhibitionOtherEventsByItemId(long itemId)
        {
            return ExhibitionOtherEventsBL.GetExhibitionOtherEventsByItemId(itemId);
        }

        public List<XsiExhibitionOtherEvents> GetExhibitionOtherEvents()
        {
            return ExhibitionOtherEventsBL.GetExhibitionOtherEvents();
        }
        public List<ExhibitionDashboard> GetExhibitionDashboard()
        {
            return ExhibitionOtherEventsBL.GetExhibitionDashboard();
        }
        public List<XsiExhibitionOtherEvents> GetExhibitionOtherEvents(EnumSortlistBy sortListBy)
        {
            return ExhibitionOtherEventsBL.GetExhibitionOtherEvents(sortListBy);
        }
        public List<XsiExhibitionOtherEvents> GetExhibitionOtherEvents(XsiExhibitionOtherEvents entity)
        {
            return ExhibitionOtherEventsBL.GetExhibitionOtherEvents(entity);
        }
        public List<XsiExhibitionOtherEvents> GetExhibitionOtherEvents(XsiExhibitionOtherEvents entity, EnumSortlistBy sortListBy)
        {
            return ExhibitionOtherEventsBL.GetExhibitionOtherEvents(entity, sortListBy);
        }

        public EnumResultType InsertExhibitionOtherEvents(XsiExhibitionOtherEvents entity)
        {
            return ExhibitionOtherEventsBL.InsertExhibitionOtherEvents(entity);
        }
        public EnumResultType UpdateExhibitionOtherEvents(XsiExhibitionOtherEvents entity)
        {
            return ExhibitionOtherEventsBL.UpdateExhibitionOtherEvents(entity);
        }
        public EnumResultType UpdateExhibitionOtherEventsStatus(long itemId, long langId)
        {
            return ExhibitionOtherEventsBL.UpdateExhibitionOtherEventsStatus(itemId, langId);
        }
        public EnumResultType UpdateExhibitionOtherEventsArchiveStatus(long itemId)
        {
            return ExhibitionOtherEventsBL.UpdateExhibitionOtherEventsArchiveStatus(itemId);
        }
        public EnumResultType DeleteExhibitionOtherEvents(XsiExhibitionOtherEvents entity)
        {
            return ExhibitionOtherEventsBL.DeleteExhibitionOtherEvents(entity);
        }
        public EnumResultType DeleteExhibitionOtherEvents(long itemId)
        {
            return ExhibitionOtherEventsBL.DeleteExhibitionOtherEvents(itemId);
        }
        #endregion
    }
}
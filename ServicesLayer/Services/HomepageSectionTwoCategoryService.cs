﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class HomepageSectionTwoCategoryService : IDisposable
    {
        #region Feilds
        HomepageSectionTwoCategoryBL HomepageSectionTwoCategoryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return HomepageSectionTwoCategoryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return HomepageSectionTwoCategoryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public HomepageSectionTwoCategoryService()
        {
            HomepageSectionTwoCategoryBL = new HomepageSectionTwoCategoryBL();
        }
        public HomepageSectionTwoCategoryService(string connectionString)
        {
            HomepageSectionTwoCategoryBL = new HomepageSectionTwoCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomepageSectionTwoCategoryBL != null)
                this.HomepageSectionTwoCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiHomepageSectionTwoCategory GetHomepageSectionTwoCategoryByItemId(long itemId)
        {
            return HomepageSectionTwoCategoryBL.GetHomepageSectionTwoCategoryByItemId(itemId);
        }
        public List<XsiHomepageSectionTwoCategory> GetHomepageSectionTwoCategory()
        {
            return HomepageSectionTwoCategoryBL.GetHomepageSectionTwoCategory();
        }
        public List<XsiHomepageSectionTwoCategory> GetHomepageSectionTwoCategory(EnumSortlistBy sortListBy)
        {
            return HomepageSectionTwoCategoryBL.GetHomepageSectionTwoCategory(sortListBy);
        }
        public List<XsiHomepageSectionTwoCategory> GetHomepageSectionTwoCategory(XsiHomepageSectionTwoCategory entity)
        {
            return HomepageSectionTwoCategoryBL.GetHomepageSectionTwoCategory(entity);
        }
        public List<XsiHomepageSectionTwoCategory> GetHomepageSectionTwoCategory(XsiHomepageSectionTwoCategory entity, EnumSortlistBy sortListBy)
        {
            return HomepageSectionTwoCategoryBL.GetHomepageSectionTwoCategory(entity, sortListBy);
        }
        
        public XsiHomepageSectionTwoCategory GetHomepageSectionTwoCategory(long Id)
        {
            return HomepageSectionTwoCategoryBL.GetHomepageSectionTwoCategory(Id);
        }

        public EnumResultType InsertHomepageSectionTwoCategory(XsiHomepageSectionTwoCategory entity)
        {
            return HomepageSectionTwoCategoryBL.InsertHomepageSectionTwoCategory(entity);
        }
        public EnumResultType UpdateHomepageSectionTwoCategory(XsiHomepageSectionTwoCategory entity)
        {
            return HomepageSectionTwoCategoryBL.UpdateHomepageSectionTwoCategory(entity);
        }
        public EnumResultType UpdateHomepageSectionTwoCategoryStatus(long itemId)
        {
            return HomepageSectionTwoCategoryBL.UpdateHomepageSectionTwoCategoryStatus(itemId);
        }
        public EnumResultType DeleteHomepageSectionTwoCategory(XsiHomepageSectionTwoCategory entity)
        {
            return HomepageSectionTwoCategoryBL.DeleteHomepageSectionTwoCategory(entity);
        }
        public EnumResultType DeleteHomepageSectionTwoCategory(long itemId)
        {
            return HomepageSectionTwoCategoryBL.DeleteHomepageSectionTwoCategory(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFPhotoAlbumService : IDisposable
    {
        #region Feilds
        SCRFPhotoAlbumBL SCRFPhotoAlbumBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFPhotoAlbumBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFPhotoAlbumBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFPhotoAlbumService()
        {
            SCRFPhotoAlbumBL = new SCRFPhotoAlbumBL();
        }
        public SCRFPhotoAlbumService(string connectionString)
        {
            SCRFPhotoAlbumBL = new SCRFPhotoAlbumBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFPhotoAlbumBL != null)
                this.SCRFPhotoAlbumBL.Dispose();
        }
        #endregion
        #region Methods
         
        public long GetNextSortIndex()
        {
            return SCRFPhotoAlbumBL.GetNextSortIndex();
        }
        public XsiScrfphotoAlbum GetSCRFPhotoAlbumByItemId(long itemId)
        {
            return SCRFPhotoAlbumBL.GetSCRFPhotoAlbumByItemId(itemId);
        }
         
        public List<XsiScrfphotoAlbum> GetSCRFPhotoAlbum()
        {
            return SCRFPhotoAlbumBL.GetSCRFPhotoAlbum();
        }
        public List<XsiScrfphotoAlbum> GetSCRFPhotoAlbum(EnumSortlistBy sortListBy)
        {
            return SCRFPhotoAlbumBL.GetSCRFPhotoAlbum(sortListBy);
        }
        public List<XsiScrfphotoAlbum> GetSCRFPhotoAlbum(XsiScrfphotoAlbum entity)
        {
            return SCRFPhotoAlbumBL.GetSCRFPhotoAlbum(entity);
        }
        public List<XsiScrfphotoAlbum> GetSCRFPhotoAlbum(XsiScrfphotoAlbum entity, EnumSortlistBy sortListBy)
        {
            return SCRFPhotoAlbumBL.GetSCRFPhotoAlbum(entity, sortListBy);
        }
        
        public EnumResultType InsertSCRFPhotoAlbum(XsiScrfphotoAlbum entity)
        {
            return SCRFPhotoAlbumBL.InsertSCRFPhotoAlbum(entity);
        }
        public EnumResultType UpdateSCRFPhotoAlbum(XsiScrfphotoAlbum entity)
        {
            return SCRFPhotoAlbumBL.UpdateSCRFPhotoAlbum(entity);
        }
        public EnumResultType UpdateSCRFPhotoAlbumStatus(long itemId)
        {
            return SCRFPhotoAlbumBL.UpdateSCRFPhotoAlbumStatus(itemId);
        }
        public EnumResultType DeleteSCRFPhotoAlbum(XsiScrfphotoAlbum entity)
        {
            return SCRFPhotoAlbumBL.DeleteSCRFPhotoAlbum(entity);
        }
        public EnumResultType DeleteSCRFPhotoAlbum(long itemId)
        {
            return SCRFPhotoAlbumBL.DeleteSCRFPhotoAlbum(itemId);
        }
        #endregion
    }
}
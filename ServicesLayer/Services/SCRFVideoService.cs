﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFVideoService : IDisposable
    {
        #region Feilds
        SCRFVideoBL SCRFVideoBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFVideoBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFVideoBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFVideoService()
        {
            SCRFVideoBL = new SCRFVideoBL();
        }
        public SCRFVideoService(string connectionString)
        {
            SCRFVideoBL = new SCRFVideoBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFVideoBL != null)
                this.SCRFVideoBL.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextSortIndex()
        {
            return SCRFVideoBL.GetNextSortIndex();
        }
        public XsiScrfvideo GetSCRFVideoByItemId(long itemId)
        {
            return SCRFVideoBL.GetSCRFVideoByItemId(itemId);
        }

        public List<XsiScrfvideo> GetSCRFVideo()
        {
            return SCRFVideoBL.GetSCRFVideo();
        }
        public List<XsiScrfvideo> GetSCRFVideo(EnumSortlistBy sortListBy)
        {
            return SCRFVideoBL.GetSCRFVideo(sortListBy);
        }
        public List<XsiScrfvideo> GetSCRFVideo(XsiScrfvideo entity)
        {
            return SCRFVideoBL.GetSCRFVideo(entity);
        }
        public List<XsiScrfvideo> GetSCRFVideo(XsiScrfvideo entity, EnumSortlistBy sortListBy)
        {
            return SCRFVideoBL.GetSCRFVideo(entity, sortListBy);
        }

        public EnumResultType InsertSCRFVideo(XsiScrfvideo entity)
        {
            return SCRFVideoBL.InsertSCRFVideo(entity);
        }
        public EnumResultType UpdateSCRFVideo(XsiScrfvideo entity)
        {
            return SCRFVideoBL.UpdateSCRFVideo(entity);
        }
        public EnumResultType UpdateSCRFVideoStatus(long itemId)
        {
            return SCRFVideoBL.UpdateSCRFVideoStatus(itemId);
        }

        public EnumResultType UpdateSCRFAlbumCover(long itemId)
        {
            return SCRFVideoBL.UpdateSCRFAlbumCover(itemId);
        }
        public EnumResultType DeleteSCRFVideo(XsiScrfvideo entity)
        {
            return SCRFVideoBL.DeleteSCRFVideo(entity);
        }
        public EnumResultType DeleteSCRFVideo(long itemId)
        {
            return SCRFVideoBL.DeleteSCRFVideo(itemId);
        }
        #endregion
    }
}
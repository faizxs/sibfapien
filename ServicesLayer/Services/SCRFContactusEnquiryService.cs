﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class SCRFContactusEnquiryService : IDisposable
    {
        #region Feilds
        SCRFContactusEnquiryBL SCRFContactusEnquiryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return SCRFContactusEnquiryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return SCRFContactusEnquiryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public SCRFContactusEnquiryService()
        {
            SCRFContactusEnquiryBL = new SCRFContactusEnquiryBL();
        }
        public SCRFContactusEnquiryService(string connectionString)
        {
            SCRFContactusEnquiryBL = new SCRFContactusEnquiryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFContactusEnquiryBL != null)
                this.SCRFContactusEnquiryBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfcontactusEnquiry GetSCRFContactusEnquiryByItemId(long itemId)
        {
            return SCRFContactusEnquiryBL.GetSCRFContactusEnquiryByItemId(itemId);
        }

        public List<XsiScrfcontactusEnquiry> GetSCRFContactusEnquiry()
        {
            return SCRFContactusEnquiryBL.GetSCRFContactusEnquiry();
        }
        public List<XsiScrfcontactusEnquiry> GetSCRFContactusEnquiry(EnumSortlistBy sortListBy)
        {
            return SCRFContactusEnquiryBL.GetSCRFContactusEnquiry(sortListBy);
        }
        public List<XsiScrfcontactusEnquiry> GetSCRFContactusEnquiry(XsiScrfcontactusEnquiry entity)
        {
            return SCRFContactusEnquiryBL.GetSCRFContactusEnquiry(entity);
        }
        public List<XsiScrfcontactusEnquiry> GetSCRFContactusEnquiry(XsiScrfcontactusEnquiry entity, EnumSortlistBy sortListBy)
        {
            return SCRFContactusEnquiryBL.GetSCRFContactusEnquiry(entity, sortListBy);
        }
        public List<XsiScrfcontactusEnquiry> GetSCRFContactusEnquiryOr(XsiScrfcontactusEnquiry entity, EnumSortlistBy sortListBy)
        {
            return SCRFContactusEnquiryBL.GetSCRFContactusEnquiryOr(entity, sortListBy);
        }
        public EnumResultType InsertSCRFContactusEnquiry(XsiScrfcontactusEnquiry entity)
        {
            return SCRFContactusEnquiryBL.InsertSCRFContactusEnquiry(entity);
        }
        public EnumResultType UpdateSCRFContactusEnquiry(XsiScrfcontactusEnquiry entity)
        {
            return SCRFContactusEnquiryBL.UpdateSCRFContactusEnquiry(entity);
        }
        public EnumResultType UpdateSCRFContactusEnquiryStatus(long itemId)
        {
            return SCRFContactusEnquiryBL.UpdateSCRFContactusEnquiryStatus(itemId);
        }
        public EnumResultType UpdateSCRFContactusEnquiryFlag(long itemId, string flag)
        {
            return SCRFContactusEnquiryBL.UpdateSCRFContactusEnquiryFlag(itemId, flag);
        }
        public EnumResultType DeleteSCRFContactusEnquiry(XsiScrfcontactusEnquiry entity)
        {
            return SCRFContactusEnquiryBL.DeleteSCRFContactusEnquiry(entity);
        }
        public EnumResultType DeleteSCRFContactusEnquiry(long itemId)
        {
            return SCRFContactusEnquiryBL.DeleteSCRFContactusEnquiry(itemId);
        }
        #endregion
    }
}
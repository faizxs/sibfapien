﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class PPBooksLanguageService : IDisposable
    {
        #region Feilds
        PPBooksLanguageBL PPBooksLanguageBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return PPBooksLanguageBL.XsiItemdId; }
        }
        #endregion
        #region Constructor
        public PPBooksLanguageService()
        {
            PPBooksLanguageBL = new PPBooksLanguageBL();
        }
        public PPBooksLanguageService(string connectionString)
        {
            PPBooksLanguageBL = new PPBooksLanguageBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PPBooksLanguageBL != null)
                this.PPBooksLanguageBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionPpbooksLanguage GetPPBooksLanguageByItemId(long itemId)
        {
            return PPBooksLanguageBL.GetPPBooksLanguageByItemId(itemId);
        }
        public List<XsiExhibitionPpbooksLanguage> GetPPBooksLanguage()
        {
            return PPBooksLanguageBL.GetPPBooksLanguage();
        }
        public List<XsiExhibitionPpbooksLanguage> GetPPBooksLanguage(EnumSortlistBy sortListBy)
        {
            return PPBooksLanguageBL.GetPPBooksLanguage(sortListBy);
        }
        public List<XsiExhibitionPpbooksLanguage> GetPPBooksLanguage(XsiExhibitionPpbooksLanguage entity)
        {
            return PPBooksLanguageBL.GetPPBooksLanguage(entity);
        }
        public List<XsiExhibitionPpbooksLanguage> GetPPBooksLanguage(XsiExhibitionPpbooksLanguage entity, EnumSortlistBy sortListBy)
        {
            return PPBooksLanguageBL.GetPPBooksLanguage(entity, sortListBy);
        }
        public EnumResultType InsertPPBooksLanguage(XsiExhibitionPpbooksLanguage entity)
        {
            return PPBooksLanguageBL.InsertPPBooksLanguage(entity);
        }
        public EnumResultType UpdatePPBooksLanguage(XsiExhibitionPpbooksLanguage entity)
        {
            return PPBooksLanguageBL.UpdatePPBooksLanguage(entity);
        }

        public EnumResultType DeletePPBooksLanguage(XsiExhibitionPpbooksLanguage entity)
        {
            return PPBooksLanguageBL.DeletePPBooksLanguage(entity);
        }
        public EnumResultType DeletePPBooksLanguage(long itemId)
        {
            return PPBooksLanguageBL.DeletePPBooksLanguage(itemId);
        }
        #endregion
    }
}
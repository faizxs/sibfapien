﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class ProfessionalProgramRegistrationService : IDisposable
    {
        #region Feilds
        ProfessionalProgramRegistrationBL ProfessionalProgramRegistrationBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return ProfessionalProgramRegistrationBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return ProfessionalProgramRegistrationBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public ProfessionalProgramRegistrationService()
        {
            ProfessionalProgramRegistrationBL = new ProfessionalProgramRegistrationBL();
        }
        public ProfessionalProgramRegistrationService(string connectionString)
        {
            ProfessionalProgramRegistrationBL = new ProfessionalProgramRegistrationBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ProfessionalProgramRegistrationBL != null)
                this.ProfessionalProgramRegistrationBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionProfessionalProgramRegistration GetProfessionalProgramRegistrationByItemId(long itemId)
        {
            return ProfessionalProgramRegistrationBL.GetProfessionalProgramRegistrationByItemId(itemId);
        }

        public List<XsiExhibitionProfessionalProgramRegistration> GetProfessionalProgramRegistration()
        {
            return ProfessionalProgramRegistrationBL.GetProfessionalProgramRegistration();
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetProfessionalProgramRegistration(EnumSortlistBy sortListBy)
        {
            return ProfessionalProgramRegistrationBL.GetProfessionalProgramRegistration(sortListBy);
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetProfessionalProgramRegistration(XsiExhibitionProfessionalProgramRegistration entity)
        {
            return ProfessionalProgramRegistrationBL.GetProfessionalProgramRegistration(entity);
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetProfessionalProgramRegistration(XsiExhibitionProfessionalProgramRegistration entity, EnumSortlistBy sortListBy)
        {
            return ProfessionalProgramRegistrationBL.GetProfessionalProgramRegistration(entity, sortListBy);
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetProfessionalProgramRegistrationCMS(XsiExhibitionProfessionalProgramRegistration entity, EnumSortlistBy sortListBy)
        {
            return ProfessionalProgramRegistrationBL.GetProfessionalProgramRegistrationCMS(entity, sortListBy);
        }
        public XsiExhibitionProfessionalProgramRegistration GetProfessionalProgramRegistration(long groupId, long languageId)
        {
            return ProfessionalProgramRegistrationBL.GetProfessionalProgramRegistration(groupId, languageId);
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetSuggestedProfessionalProgramRegistration(XsiExhibitionProfessionalProgramRegistration entity)
        {
            return ProfessionalProgramRegistrationBL.GetSuggestedProfessionalProgramRegistration(entity);
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetOtherProfessionals(List<long?> genreGroupIDList, List<long> suggestedIDList, XsiExhibitionProfessionalProgramRegistration entity)
        {
            return ProfessionalProgramRegistrationBL.GetOtherProfessionals(genreGroupIDList, suggestedIDList, entity);
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetOtherProfessionalsSearch(List<long?> genreGroupIDList, List<long> suggestedIDList, XsiExhibitionProfessionalProgramRegistration entity,string name)
        {
            return ProfessionalProgramRegistrationBL.GetOtherProfessionalsSearch(genreGroupIDList, suggestedIDList, entity, name);
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetOtherProfessionals(XsiExhibitionProfessionalProgramRegistration entity)
        {
            return ProfessionalProgramRegistrationBL.GetOtherProfessionals(entity);
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetOtherProfessionalsSearch(XsiExhibitionProfessionalProgramRegistration entity, string name)
        {
            return ProfessionalProgramRegistrationBL.GetOtherProfessionalsSearch(entity, name);
        }
        //public List<GetApporvedMeetUps_Result> GetApprovedMeetUps(long professionalProgramGroupID)
        //{
        //    return ProfessionalProgramRegistrationBL.GetApprovedMeetUps(professionalProgramGroupID);
        //}
        //public List<GetApporvedMeetUpsByMember_Result> GetApprovedMeetUpsByMember(long professionalProgramGroupID, long profesionalProgramRegistrationID)
        //{
        //    return ProfessionalProgramRegistrationBL.GetApprovedMeetUpsByMember(professionalProgramGroupID, profesionalProgramRegistrationID);
        //}

        public EnumResultType InsertProfessionalProgramRegistration(XsiExhibitionProfessionalProgramRegistration entity)
        {
            return ProfessionalProgramRegistrationBL.InsertProfessionalProgramRegistration(entity);
        }
        public EnumResultType UpdateProfessionalProgramRegistration(XsiExhibitionProfessionalProgramRegistration entity)
        {
            return ProfessionalProgramRegistrationBL.UpdateProfessionalProgramRegistration(entity);
        }
        public EnumResultType UpdateProfessionalProgramRegistrationStatus(long itemId)
        {
            return ProfessionalProgramRegistrationBL.UpdateProfessionalProgramRegistrationStatus(itemId);
        }
        public EnumResultType DeleteProfessionalProgramRegistration(XsiExhibitionProfessionalProgramRegistration entity)
        {
            return ProfessionalProgramRegistrationBL.DeleteProfessionalProgramRegistration(entity);
        }
        public EnumResultType DeleteProfessionalProgramRegistration(long itemId)
        {
            return ProfessionalProgramRegistrationBL.DeleteProfessionalProgramRegistration(itemId);
        }
        #endregion
    }
}
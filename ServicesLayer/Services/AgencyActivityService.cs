﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class AgencyActivityService : IDisposable
    {
        #region Feilds
        AgencyActivityBL AgencyActivityBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return AgencyActivityBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return AgencyActivityBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public AgencyActivityService()
        {
            AgencyActivityBL = new AgencyActivityBL();
        }
        public AgencyActivityService(string connectionString)
        {
            AgencyActivityBL = new AgencyActivityBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AgencyActivityBL != null)
                this.AgencyActivityBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiMemberExhibitionActivityYearly GetAgencyActivityByItemId(long itemId)
        {
            return AgencyActivityBL.GetAgencyActivityByItemId(itemId);
        }

        public List<XsiMemberExhibitionActivityYearly> GetAgencyActivity()
        {
            return AgencyActivityBL.GetAgencyActivity();
        }
        public List<XsiMemberExhibitionActivityYearly> GetAgencyActivity(EnumSortlistBy sortListBy)
        {
            return AgencyActivityBL.GetAgencyActivity(sortListBy);
        }
        public List<XsiMemberExhibitionActivityYearly> GetAgencyActivity(XsiMemberExhibitionActivityYearly entity)
        {
            return AgencyActivityBL.GetAgencyActivity(entity);
        }
        public List<XsiMemberExhibitionActivityYearly> GetAgencyActivity(XsiMemberExhibitionActivityYearly entity, EnumSortlistBy sortListBy)
        {
            return AgencyActivityBL.GetAgencyActivity(entity, sortListBy);
        }

        public EnumResultType InsertAgencyActivity(XsiMemberExhibitionActivityYearly entity)
        {
            return AgencyActivityBL.InsertAgencyActivity(entity);
        }
        public EnumResultType UpdateAgencyActivity(XsiMemberExhibitionActivityYearly entity)
        {
            return AgencyActivityBL.UpdateAgencyActivity(entity);
        }
        public EnumResultType DeleteAgencyActivity(XsiMemberExhibitionActivityYearly entity)
        {
            return AgencyActivityBL.DeleteAgencyActivity(entity);
        }
        public EnumResultType DeleteAgencyActivity(long itemId)
        {
            return AgencyActivityBL.DeleteAgencyActivity(itemId);
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;
using System.Linq;

namespace Xsi.ServicesLayer
{
    public class AdminUserPermissionService : IDisposable
    {
        #region Feilds
        AdminUserPermissionBL XsiAdminUserPermissionBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return XsiAdminUserPermissionBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return XsiAdminUserPermissionBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public AdminUserPermissionService()
        {
            XsiAdminUserPermissionBL = new AdminUserPermissionBL();
        }
        public AdminUserPermissionService(string connectionString)
        {
            XsiAdminUserPermissionBL = new AdminUserPermissionBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiAdminUserPermissionBL != null)
                this.XsiAdminUserPermissionBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdminPermissions GetPermissionByItemId(long itemId)
        {
            return XsiAdminUserPermissionBL.GetPermissionByItemId(itemId);
        }
        public List<XsiAdminPermissions> GetPermission(EnumSortlistBy sortListBy = EnumSortlistBy.ByItemIdDesc)
        {
            return XsiAdminUserPermissionBL.GetPermission(sortListBy);
        }
        public List<XsiAdminPermissions> GetPermission(XsiAdminPermissions entity)
        {
            return XsiAdminUserPermissionBL.GetPermission(entity);
        }
        public List<XsiAdminPermissions> GetPermissionAndUser(XsiAdminPermissions entity)
        {
            return XsiAdminUserPermissionBL.GetPermissionAndUser(entity);
        }
        public List<XsiAdminPermissions> GetPermissionAndRole(XsiAdminPermissions entity)
        {
            return XsiAdminUserPermissionBL.GetPermissionAndRole(entity);
        }
        public List<XsiAdminPermissions> GetPermissionAndPage(XsiAdminPermissions entity)
        {
            return XsiAdminUserPermissionBL.GetPermissionAndPage(entity);
        }
        public EnumResultType InsertPermission(XsiAdminPermissions entity)
        {
            return XsiAdminUserPermissionBL.InsertPermission(entity);
        }
        public EnumResultType UpdatePermission(XsiAdminPermissions entity)
        {
            return XsiAdminUserPermissionBL.UpdatePermission(entity);
        }
        public EnumResultType DeletePermission(XsiAdminPermissions entity)
        {
            return XsiAdminUserPermissionBL.DeletePermission(entity);
        }
        public EnumResultType DeletePermission(long itemId)
        {
            return XsiAdminUserPermissionBL.DeletePermission(itemId);
        }
        #endregion
    }
}
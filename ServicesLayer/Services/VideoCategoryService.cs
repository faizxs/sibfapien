﻿using System;
using System.Linq;
using System.Collections.Generic;
using Entities.Models;
using Xsi.BusinessLogicLayer;

namespace Xsi.ServicesLayer
{
    public class VideoCategoryService : IDisposable
    {
        #region Feilds
        VideoCategoryBL VideoCategoryBL;
        #endregion
        #region Properties
        public long XsiItemdId
        {
            get { return VideoCategoryBL.XsiItemdId; }
        }
        public long XsiGroupId
        {
            get { return VideoCategoryBL.XsiGroupId; }
        }
        #endregion
        #region Constructor
        public VideoCategoryService()
        {
            VideoCategoryBL = new VideoCategoryBL();
        }
        public VideoCategoryService(string connectionString)
        {
            VideoCategoryBL = new VideoCategoryBL(connectionString);
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.VideoCategoryBL != null)
                this.VideoCategoryBL.Dispose();
        }
        #endregion
        #region Methods
        public XsiVideoCategory GetVideoCategoryByItemId(long itemId)
        {
            return VideoCategoryBL.GetVideoCategoryByItemId(itemId);
        }

        public List<XsiVideoCategory> GetVideoCategory()
        {
            return VideoCategoryBL.GetVideoCategory();
        }
        public List<XsiVideoCategory> GetVideoCategory(EnumSortlistBy sortListBy)
        {
            return VideoCategoryBL.GetVideoCategory(sortListBy);
        }
        public List<XsiVideoCategory> GetVideoCategory(XsiVideoCategory entity)
        {
            return VideoCategoryBL.GetVideoCategory(entity);
        }
        public List<XsiVideoCategory> GetVideoCategory(XsiVideoCategory entity, EnumSortlistBy sortListBy)
        {
            return VideoCategoryBL.GetVideoCategory(entity, sortListBy);
        }
        public List<XsiVideoCategory> GetVideoCategoryOr(XsiVideoCategory entity, EnumSortlistBy sortListBy)
        {
            return VideoCategoryBL.GetVideoCategoryOr(entity, sortListBy);
        }

        public EnumResultType InsertVideoCategory(XsiVideoCategory entity)
        {
            return VideoCategoryBL.InsertVideoCategory(entity);
        }
        public EnumResultType UpdateVideoCategory(XsiVideoCategory entity)
        {
            return VideoCategoryBL.UpdateVideoCategory(entity);
        }
        public EnumResultType UpdateVideoCategoryStatus(long itemId, long langId)
        {
            return VideoCategoryBL.UpdateVideoCategoryStatus(itemId, langId);
        }
        public EnumResultType DeleteVideoCategory(XsiVideoCategory entity)
        {
            return VideoCategoryBL.DeleteVideoCategory(entity);
        }
        public EnumResultType DeleteVideoCategory(long itemId)
        {
            return VideoCategoryBL.DeleteVideoCategory(itemId);
        }
        #endregion
    }
}
﻿
public enum EnumResultType : int
{
    Success,
    Failed,
    Found,
    NotFound,
    NotModified,
    AlreadyExists,
    Invalid,
    ValueInUse,
    Exception,
    EmailNotVerified,
    EmailSent,
    PublishedRecords,
    Error
}

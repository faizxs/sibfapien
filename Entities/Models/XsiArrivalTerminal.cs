﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiArrivalTerminal
    {
        public XsiArrivalTerminal()
        {
            XsiExhibitionRepresentativeParticipating = new HashSet<XsiExhibitionRepresentativeParticipating>();
            XsiExhibitionRepresentativeParticipatingNoVisa = new HashSet<XsiExhibitionRepresentativeParticipatingNoVisa>();
            XsiExhibitionStaffGuestParticipating = new HashSet<XsiExhibitionStaffGuestParticipating>();
        }

        public long ItemId { get; set; }
        public long? CategoryId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string Title { get; set; }
        public string TitleAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionArrivalAirport Category { get; set; }
        public virtual ICollection<XsiExhibitionRepresentativeParticipating> XsiExhibitionRepresentativeParticipating { get; set; }
        public virtual ICollection<XsiExhibitionRepresentativeParticipatingNoVisa> XsiExhibitionRepresentativeParticipatingNoVisa { get; set; }
        public virtual ICollection<XsiExhibitionStaffGuestParticipating> XsiExhibitionStaffGuestParticipating { get; set; }
    }
}

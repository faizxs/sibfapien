﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionTranslationGrantMembers
    {
        public XsiExhibitionTranslationGrantMembers()
        {
            XsiExhibitionTranslationGrantBookWinner = new HashSet<XsiExhibitionTranslationGrantBookWinner>();
        }

        public long ItemId { get; set; }
        public long? MemberId { get; set; }
        public long? TranslationGrantId { get; set; }
        public string IsActive { get; set; }
        public string Status { get; set; }
        public string TranslationType { get; set; }
        public string SellerName { get; set; }
        public string SellerFirstName { get; set; }
        public string SellerMiddleName { get; set; }
        public string SellerLastName { get; set; }
        public string SellerJobTitle { get; set; }
        public string SellerCompanyName { get; set; }
        public string SellerAddress { get; set; }
        public string SellerTelephone { get; set; }
        public string SellerFax { get; set; }
        public string SellerEmail { get; set; }
        public string BuyerName { get; set; }
        public string BuyerFirstName { get; set; }
        public string BuyerMiddleName { get; set; }
        public string BuyerLastName { get; set; }
        public string BuyerJobTitle { get; set; }
        public string BuyerCompanyName { get; set; }
        public string BuyerAddress { get; set; }
        public string BuyerTelephone { get; set; }
        public string BuyerFax { get; set; }
        public string BuyerEmail { get; set; }
        public string BookTitleOriginal { get; set; }
        public string BookTitleEnglish { get; set; }
        public string BookTitleArabic { get; set; }
        public string Author { get; set; }
        public string Isbn { get; set; }
        public string Genre { get; set; }
        public long? OriginalLanguageId { get; set; }
        public long? ProposedLanguageId { get; set; }
        public string NoOfPagesOriginal { get; set; }
        public string ApplicationForm { get; set; }
        public string GrantLetter { get; set; }
        public string NoOfWords { get; set; }
        public string Contract { get; set; }
        public string SignedContract { get; set; }
        public string BankName { get; set; }
        public string BranchNameAddress { get; set; }
        public string AccNo { get; set; }
        public string Ibanno { get; set; }
        public string SwiftCode { get; set; }
        public string OriginalSoftCopy { get; set; }
        public string FirstPayment { get; set; }
        public DateTime? FirstPaymentDate { get; set; }
        public string FirstPaymentReceipt { get; set; }
        public string Draft1 { get; set; }
        public string SecondPayment { get; set; }
        public DateTime? SecondPaymentDate { get; set; }
        public string SecondPaymentReceipt { get; set; }
        public string FinalPayment { get; set; }
        public DateTime? FinalPaymentDate { get; set; }
        public string FinalPaymentReceipt { get; set; }
        public string OriginalBookStatus { get; set; }
        public string IsIbanorBoth { get; set; }
        public long? GenreId { get; set; }
        public string AwardGrantValue { get; set; }
        public string IsHardCopy { get; set; }
        public DateTime? DateofShipment { get; set; }
        public string BuyerorSellerRights { get; set; }
        public string FinalContract { get; set; }
        public string TransferFormByAdmin { get; set; }
        public string TransferForm { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionLanguage OriginalLanguage { get; set; }
        public virtual XsiExhibitionLanguage ProposedLanguage { get; set; }
        public virtual XsiExhibitionTranslationGrant TranslationGrant { get; set; }
        public virtual ICollection<XsiExhibitionTranslationGrantBookWinner> XsiExhibitionTranslationGrantBookWinner { get; set; }
    }
}

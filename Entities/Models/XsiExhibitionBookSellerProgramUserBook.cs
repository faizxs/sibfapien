﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBookSellerProgramUserBook
    {
        public long ItemId { get; set; }
        public long? BookSellerRegistrationId { get; set; }
        public long? LanguageId { get; set; }
        public string IsActive { get; set; }
        public string AuthorName { get; set; }
        public string Title { get; set; }
        public long? GenreId { get; set; }
        public string OtherGenre { get; set; }
        public string Synopsis { get; set; }
        public string IsRightsAvailable { get; set; }
        public string IsOwnRights { get; set; }
        public string RightsOwner { get; set; }
        public string FileName { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedById { get; set; }

        public virtual XsiExhibitionBookSellerProgramRegistration BookSellerRegistration { get; set; }
    }
}

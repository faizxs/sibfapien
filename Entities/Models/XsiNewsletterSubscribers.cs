﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiNewsletterSubscribers
    {
        public long ItemId { get; set; }
        public long? LanguageId { get; set; }
        public long? CountryId { get; set; }
        public string IsActive { get; set; }
        public string IsSubscribe { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string NameAr { get; set; }
        public string IsActiveAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedById { get; set; }

        public virtual XsiExhibitionCountry Country { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfhomepageSection
    {
        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string DescriptionOne { get; set; }
        public string DescriptionTwo { get; set; }
        public string Thumbnail { get; set; }
        public string Url { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public string DescriptionOneAr { get; set; }
        public string DescriptionTwoAr { get; set; }
        public string ThumbnailAr { get; set; }
        public string Urlar { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBookAuthor
    {
        public long BookId { get; set; }
        public long AuthorId { get; set; }
    }
}

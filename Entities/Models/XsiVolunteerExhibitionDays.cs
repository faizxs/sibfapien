﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiVolunteerExhibitionDays
    {
        public long VolunteerPerioIdId { get; set; }
        public long ExhibitionDaysId { get; set; }
    }
}

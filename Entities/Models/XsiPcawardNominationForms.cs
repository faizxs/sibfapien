﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiPcawardNominationForms
    {
        public long ItemId { get; set; }
        public string Status { get; set; }
        public long? AwardDetailId { get; set; }
        public string ParticipantName { get; set; }
        public string CompanyName { get; set; }
        public long? CountryId { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Phone { get; set; }
        public string ReasonToWinAward { get; set; }
        public string NameOne { get; set; }
        public string CompanyNameOne { get; set; }
        public string EmailOne { get; set; }
        public string Testimonial { get; set; }
        public string FileName { get; set; }
        public string NameTwo { get; set; }
        public string CompanyNameTwo { get; set; }
        public string EmailTwo { get; set; }
        public string TestimonialTwo { get; set; }
        public string FileNameTwo { get; set; }
        public string PassportCopy { get; set; }
        public string Overview { get; set; }
        public string Thumbnail { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiPcawardDetails AwardDetail { get; set; }
    }
}

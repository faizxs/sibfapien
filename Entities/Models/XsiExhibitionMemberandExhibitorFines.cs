﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionMemberandExhibitorFines
    {
        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public long? ExhibitionId { get; set; }
        public long? MemberId { get; set; }
        public long? ExhibitorId { get; set; }
        public long? FineCategoryId { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionFinesCategory FineCategory { get; set; }
        public virtual XsiExhibitionMember Member { get; set; }
    }
}

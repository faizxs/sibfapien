﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionShipmentAgency
    {
        public long ItemId { get; set; }
        public long? ExhibitionShipmentId { get; set; }
        public long? AgencyId { get; set; }

        public virtual XsiExhibitionShipment ExhibitionShipment { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrficonsForSponsor
    {
        public long ItemId { get; set; }
        public string Title { get; set; }
        public string ImageName { get; set; }
        public string Url { get; set; }
        public string IsActive { get; set; }
        public string TitleAr { get; set; }
        public string ImageNameAr { get; set; }
        public string Urlar { get; set; }
        public string IsActiveAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

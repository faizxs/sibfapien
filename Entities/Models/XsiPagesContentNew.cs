﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiPagesContentNew
    {
        public XsiPagesContentNew()
        {
            XsiPageMenuNew = new HashSet<XsiPageMenuNew>();
        }

        public long ItemId { get; set; }
        public long? HierarchicalId { get; set; }
        public string PageTitle { get; set; }
        public string PageSubTitle { get; set; }
        public string PageContent { get; set; }
        public string PageSubContent { get; set; }
        public string PageSubSubContent { get; set; }
        public string HeaderImage { get; set; }
        public string TitleGray { get; set; }
        public string InnerBanner { get; set; }
        public string ExplorerTitle { get; set; }
        public string PageMetaKeywords { get; set; }
        public string PageMetaDescription { get; set; }
        public string PageTitleAr { get; set; }
        public string PageSubTitleAr { get; set; }
        public string PageContentAr { get; set; }
        public string PageSubContentAr { get; set; }
        public string PageSubSubContentAr { get; set; }
        public string HeaderImageAr { get; set; }
        public string TitleGrayAr { get; set; }
        public string InnerBannerAr { get; set; }
        public string ExplorerTitleAr { get; set; }
        public string PageMetaKeywordsAr { get; set; }
        public string PageMetaDescriptionAr { get; set; }
        public string IsDynamic { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string RollbackXml { get; set; }

        public virtual ICollection<XsiPageMenuNew> XsiPageMenuNew { get; set; }
    }
}

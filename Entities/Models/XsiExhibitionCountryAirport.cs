﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionCountryAirport
    {
        public long ItemId { get; set; }
        public long CountryId { get; set; }
        public string IsActive { get; set; }
        public string AirportCode { get; set; }
        public string AirportName { get; set; }
        public string AirportNameAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionCountry Country { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiPcawardDetails
    {
        public XsiPcawardDetails()
        {
            XsiPcawardNominationForms = new HashSet<XsiPcawardNominationForms>();
        }

        public long ItemId { get; set; }
        public long? AwardId { get; set; }
        public long? ExhibitionId { get; set; }
        public DateTime? RegistrationStartDate { get; set; }
        public DateTime? RegistrationEndDate { get; set; }
        public string AwardCycle { get; set; }
        public string AwardCycleAr { get; set; }
        public string FirstPrize { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiPcawards Award { get; set; }
        public virtual ICollection<XsiPcawardNominationForms> XsiPcawardNominationForms { get; set; }
    }
}

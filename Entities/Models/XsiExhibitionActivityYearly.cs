﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionActivityYearly
    {
        public long ExhibitionId { get; set; }
        public long ExhibitionActivityId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionMemberLog
    {
        public long ItemId { get; set; }
        public long? ExhibitionMemberId { get; set; }
        public long? AdminUserId { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}

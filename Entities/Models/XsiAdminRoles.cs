﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiAdminRoles
    {
        public XsiAdminRoles()
        {
            XsiAdminDashboardIcons = new HashSet<XsiAdminDashboardIcons>();
            XsiAdminPermissions = new HashSet<XsiAdminPermissions>();
            XsiAdminUsers = new HashSet<XsiAdminUsers>();
        }

        public long ItemId { get; set; }
        public string Role { get; set; }

        public virtual ICollection<XsiAdminDashboardIcons> XsiAdminDashboardIcons { get; set; }
        public virtual ICollection<XsiAdminPermissions> XsiAdminPermissions { get; set; }
        public virtual ICollection<XsiAdminUsers> XsiAdminUsers { get; set; }
    }
}

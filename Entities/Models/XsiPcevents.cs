﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiPcevents
    {
        public XsiPcevents()
        {
            XsiPcmemberEvents = new HashSet<XsiPcmemberEvents>();
        }

        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string Title { get; set; }
        public string GuestName { get; set; }
        public DateTime? EventDate { get; set; }
        public string Time { get; set; }
        public long? SeatCapacity { get; set; }
        public string Overview { get; set; }
        public string Description { get; set; }
        public string TitleAr { get; set; }
        public string GuestNameAr { get; set; }
        public string OverviewAr { get; set; }
        public string DescriptionAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<XsiPcmemberEvents> XsiPcmemberEvents { get; set; }
    }
}

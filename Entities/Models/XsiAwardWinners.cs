﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiAwardWinners
    {
        public long ItemId { get; set; }
        public long? NominationFormId { get; set; }
        public long? ExhibitionId { get; set; }
        public long? UserRegistrationNumber { get; set; }
        public long? AwardId { get; set; }
        public long? SubAwardId { get; set; }
        public string FirstNameEn { get; set; }
        public string LastNameEn { get; set; }
        public string FirstNameAr { get; set; }
        public string LastNameAr { get; set; }
        public string OverviewEn { get; set; }
        public string OverviewAr { get; set; }
        public string Image { get; set; }
        public string AwardsUrl { get; set; }
        public string BookThumbnail { get; set; }
        public string PublisherThumbnail { get; set; }
        public string BookTitleEn { get; set; }
        public string BookTitleAr { get; set; }
        public string PublisherEn { get; set; }
        public string PublisherAr { get; set; }
        public string BookOverviewEn { get; set; }
        public string BookOverviewAr { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifieidBy { get; set; }

        public virtual XsiExhibition Exhibition { get; set; }
        public virtual XsiAwardsNominationForms NominationForm { get; set; }
    }
}

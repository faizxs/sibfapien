﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiCmspageMenu
    {
        public long ItemId { get; set; }
        public long? ParentId { get; set; }
        public long? PageId { get; set; }
        public string Title { get; set; }
        public string PageUrl { get; set; }
        public string PageUrlAr { get; set; }
        public string TitleAr { get; set; }
        public long? SortOrder { get; set; }
        public string IsExternal { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

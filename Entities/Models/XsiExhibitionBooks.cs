﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBooks
    {
        public XsiExhibitionBooks()
        {
            XsiExhibitionBookParticipating = new HashSet<XsiExhibitionBookParticipating>();
        }

        public long BookId { get; set; }
        public long? MemberId { get; set; }
        public long? BookPublisherId { get; set; }
        public long? BookLanguageId { get; set; }
        public long? ExhibitionSubjectId { get; set; }
        public long? ExhibitionSubsubjectId { get; set; }
        public long? ExhibitionCurrencyId { get; set; }
        public long? BookTypeId { get; set; }
        public string Thumbnail { get; set; }
        public string IsAvailableOnline { get; set; }
        public string IsNew { get; set; }
        public string IsActive { get; set; }
        public string IsEnable { get; set; }
        public string TitleEn { get; set; }
        public string TitleAr { get; set; }
        public string AuthorName { get; set; }
        public string AuthorNameAr { get; set; }
        public string BookPublisherName { get; set; }
        public string BookPublisherNameAr { get; set; }
        public string Isbn { get; set; }
        public string IssueYear { get; set; }
        public string Price { get; set; }
        public string PriceBeforeDiscount { get; set; }
        public string BookDetails { get; set; }
        public string BookDetailsAr { get; set; }
        public string IsBestSeller { get; set; }
        public long? ClickCount { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionLanguage BookLanguage { get; set; }
        public virtual XsiExhibitionBookPublisher BookPublisher { get; set; }
        public virtual XsiExhibitionBookType BookType { get; set; }
        public virtual XsiExhibitionBookSubject ExhibitionSubject { get; set; }
        public virtual XsiExhibitionBookSubsubject ExhibitionSubsubject { get; set; }
        public virtual XsiExhibitionMember Member { get; set; }
        public virtual ICollection<XsiExhibitionBookParticipating> XsiExhibitionBookParticipating { get; set; }
    }
}

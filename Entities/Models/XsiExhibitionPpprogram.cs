﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionPpprogram
    {
        public long ItemId { get; set; }
        public long? ProfessionalProgramRegistrationId { get; set; }
        public long? PpprogramId { get; set; }
    }
}

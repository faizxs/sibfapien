﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiMemberReason
    {
        public long MemberReasonId { get; set; }
        public long? MemberId { get; set; }
        public long? ReasonId { get; set; }
        public string Title { get; set; }

        public virtual XsiExhibitionMember Member { get; set; }
        public virtual XsiReason Reason { get; set; }
    }
}

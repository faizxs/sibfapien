﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfillustrationSubmissionStatusLogs
    {
        public long ItemId { get; set; }
        public long SubmissionId { get; set; }
        public long? AdminId { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}

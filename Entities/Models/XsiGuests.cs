﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiGuests
    {
        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string EventtusGuestId { get; set; }
        public long? ExhibitionId { get; set; }
        public long? WebsiteId { get; set; }
        public string GuestName { get; set; }
        public string GuestTitle { get; set; }
        public string Description { get; set; }
        public string GuestPhoto { get; set; }
        public string GuestNameAr { get; set; }
        public string GuestTitleAr { get; set; }
        public string DescriptionAr { get; set; }
        public string GuestPhotoAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

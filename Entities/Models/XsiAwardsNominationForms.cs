﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiAwardsNominationForms
    {
        public XsiAwardsNominationForms()
        {
            XsiAwardWinners = new HashSet<XsiAwardWinners>();
        }

        public long ItemId { get; set; }
        public string Status { get; set; }
        public long? ExhibitionId { get; set; }
        public long? AwardId { get; set; }
        public long? SubAwardId { get; set; }
        public string Title { get; set; }
        public string PublishedYear { get; set; }
        public string Isbn { get; set; }
        public string NominationType { get; set; }
        public string AuthorName { get; set; }
        public string AuthorNumber { get; set; }
        public string AuthorEmail { get; set; }
        public long? AuthorNationalityId { get; set; }
        public string Publisher { get; set; }
        public string PublisherNumber { get; set; }
        public string PublisherEmail { get; set; }
        public long? PublisherCountryId { get; set; }
        public string OtherCity { get; set; }
        public long? CityId { get; set; }
        public long? CountryId { get; set; }
        public string PublishersSpecialization { get; set; }
        public string EstablishedYear { get; set; }
        public string MailAddress { get; set; }
        public string Pobox { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string OwnerOfPubHouse { get; set; }
        public DateTime? Date { get; set; }
        public string PostalCode { get; set; }
        public string FirstParticipatedYear { get; set; }
        public long? NoOfPublications { get; set; }
        public long? NoOfPublicationsInCurrentYear { get; set; }
        public string GeneralManagerOfPubHouse { get; set; }
        public string Comments { get; set; }
        public string PassportCopy { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiAwards Award { get; set; }
        public virtual XsiExhibition Exhibition { get; set; }
        public virtual XsiSubAwards SubAward { get; set; }
        public virtual ICollection<XsiAwardWinners> XsiAwardWinners { get; set; }
    }
}

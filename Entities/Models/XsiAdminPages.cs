﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiAdminPages
    {
        public XsiAdminPages()
        {
            XsiAdminPermissions = new HashSet<XsiAdminPermissions>();
        }

        public long ItemId { get; set; }
        public string PageName { get; set; }
        public string PageUrl { get; set; }
        public string Image { get; set; }
        public string IsActive { get; set; }

        public virtual ICollection<XsiAdminPermissions> XsiAdminPermissions { get; set; }
    }
}

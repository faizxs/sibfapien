﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiLanguages
    {
        public long ItemId { get; set; }
        public string Name { get; set; }
        public string Direction { get; set; }
        public string Culture { get; set; }
        public string IsActive { get; set; }
        public string IsDefault { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}

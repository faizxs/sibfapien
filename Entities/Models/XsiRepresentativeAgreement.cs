﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiRepresentativeAgreement
    {
        public long ItemId { get; set; }
        public long? ExhibitorId { get; set; }
        public string Agreement { get; set; }
        public string SignedAgreement { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionPpgenre
    {
        public long ItemId { get; set; }
        public long? ProfessionalProgramRegistrationId { get; set; }
        public long? GenreId { get; set; }
    }
}

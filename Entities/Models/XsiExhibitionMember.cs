﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionMember
    {
        public XsiExhibitionMember()
        {
            XsiExhibitionBooks = new HashSet<XsiExhibitionBooks>();
            XsiExhibitionMemberApplicationYearly = new HashSet<XsiExhibitionMemberApplicationYearly>();
            XsiExhibitionMemberandExhibitorFines = new HashSet<XsiExhibitionMemberandExhibitorFines>();
            XsiExhibitionOtherEventsStaffGuest = new HashSet<XsiExhibitionOtherEventsStaffGuest>();
            XsiExhibitionProfessionalProgramRegistration = new HashSet<XsiExhibitionProfessionalProgramRegistration>();
            XsiExhibitionRepresentative = new HashSet<XsiExhibitionRepresentative>();
            XsiExhibitionStaffGuest = new HashSet<XsiExhibitionStaffGuest>();
            XsiMemberReason = new HashSet<XsiMemberReason>();
            XsiPcmemberEvents = new HashSet<XsiPcmemberEvents>();
        }

        public long MemberId { get; set; }
        public long? LanguageUrl { get; set; }
        public long? WebsiteId { get; set; }
        public long? CityId { get; set; }
        public string OtherCity { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string FirstNameAr { get; set; }
        public string LastNameAr { get; set; }
        public string FileNumber { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string EmailNew { get; set; }
        public string Website { get; set; }
        public string Pobox { get; set; }
        public string Address { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string IsVerified { get; set; }
        public string IsEmailSent { get; set; }
        public string Status { get; set; }
        public string EditRequest { get; set; }
        public string Brief { get; set; }
        public string BriefAr { get; set; }
        public string IsSibfdepartment { get; set; }
        public long? SibfdepartmentId { get; set; }
        public string IsLibraryMember { get; set; }
        public string JobTitle { get; set; }
        public string JobTitleAr { get; set; }
        public string ContactPerson { get; set; }
        public string CompanyName { get; set; }
        public string CompanyNameAr { get; set; }
        public string FirstNameEmail { get; set; }
        public string LastNameEmail { get; set; }
        public string IsFoodSupplier { get; set; }
        public string IsPcorTg { get; set; }
        public string IsActivateForTg { get; set; }
        public string IsNewPasswordChange { get; set; }
        public string IsBooksRequired { get; set; }
        public string AdminEmailOtp { get; set; }
        public DateTime? AdminEmailOtpexpiresAt { get; set; }
        public DateTime? ForgotPasswordExpiresAt { get; set; }
        public string CompanyFieldofWork { get; set; }
        public string CompanyFieldofWorkAr { get; set; }
        public string Notes { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedById { get; set; }

        public virtual XsiExhibitionCity City { get; set; }
        public virtual ICollection<XsiExhibitionBooks> XsiExhibitionBooks { get; set; }
        public virtual ICollection<XsiExhibitionMemberApplicationYearly> XsiExhibitionMemberApplicationYearly { get; set; }
        public virtual ICollection<XsiExhibitionMemberandExhibitorFines> XsiExhibitionMemberandExhibitorFines { get; set; }
        public virtual ICollection<XsiExhibitionOtherEventsStaffGuest> XsiExhibitionOtherEventsStaffGuest { get; set; }
        public virtual ICollection<XsiExhibitionProfessionalProgramRegistration> XsiExhibitionProfessionalProgramRegistration { get; set; }
        public virtual ICollection<XsiExhibitionRepresentative> XsiExhibitionRepresentative { get; set; }
        public virtual ICollection<XsiExhibitionStaffGuest> XsiExhibitionStaffGuest { get; set; }
        public virtual ICollection<XsiMemberReason> XsiMemberReason { get; set; }
        public virtual ICollection<XsiPcmemberEvents> XsiPcmemberEvents { get; set; }
    }
}

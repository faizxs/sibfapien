﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiGuestWebsite
    {
        public long ItemId { get; set; }
        public long? WebsiteId { get; set; }
        public long? GuestId { get; set; }
    }
}

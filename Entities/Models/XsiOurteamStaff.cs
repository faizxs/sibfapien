﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiOurteamStaff
    {
        public long ItemId { get; set; }
        public long? CategoryId { get; set; }
        public string IsActive { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string ImageName { get; set; }
        public string PhoneNo { get; set; }
        public string MobileNo { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string IsActiveAr { get; set; }
        public string FirstNameAr { get; set; }
        public string MiddleNameAr { get; set; }
        public string LastNameAr { get; set; }
        public string PositionAr { get; set; }
        public string ImageNameAr { get; set; }
        public string PhoneNoAr { get; set; }
        public string MobileNoAr { get; set; }
        public string FaxAr { get; set; }
        public string EmailAr { get; set; }
        public string DescriptionAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiOurteamDepartment Category { get; set; }
    }
}

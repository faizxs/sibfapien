﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiInvoice
    {
        public XsiInvoice()
        {
            XsiEreceipt = new HashSet<XsiEreceipt>();
        }

        public long ItemId { get; set; }
        public long? ExhibitorId { get; set; }
        public string InvoiceNumber { get; set; }
        public string FileName { get; set; }
        public string AllocatedSpace { get; set; }
        public string ParticipationFee { get; set; }
        public string StandFee { get; set; }
        public string KnowledgeAndResearchFee { get; set; }
        public string AgencyFee { get; set; }
        public string RepresentativeFee { get; set; }
        public string DueFromLastYearCredit { get; set; }
        public string DueFromLastYearDebit { get; set; }
        public string DueCurrentYear { get; set; }
        public string DebitedCurrentYear { get; set; }
        public string Discount { get; set; }
        public string DiscountNote { get; set; }
        public string Penalty { get; set; }
        public string Vat { get; set; }
        public string SubTotalAed { get; set; }
        public string SubTotalUsd { get; set; }
        public string TotalAed { get; set; }
        public string TotalUsd { get; set; }
        public string PaidAmount { get; set; }
        public string PaidAmountUsd { get; set; }
        public string Status { get; set; }
        public string IsPaymentInAed { get; set; }
        public string IsActive { get; set; }
        public string IsChequeReceived { get; set; }
        public string IsNewPending { get; set; }
        public string BankName { get; set; }
        public DateTime? TransferDate { get; set; }
        public string ReceiptNumber { get; set; }
        public string UploadePaymentReceipt { get; set; }
        public long? PaymentTypeId { get; set; }
        public string IsStatusChanged { get; set; }
        public string Notes { get; set; }
        public string GeneralNotes { get; set; }
        public string ExemptionType { get; set; }
        public string IsRegistrationInvoice { get; set; }
        public string DiscountPercent { get; set; }
        public string PenaltyPercent { get; set; }
        public string RemainingAgencies { get; set; }
        public string RemainingRepresentatives { get; set; }
        public string PgtransactionId { get; set; }
        public string Pgstatus { get; set; }
        public string InvoiceNumberFromUser { get; set; }
        public string NoofParcel { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedById { get; set; }

        public virtual XsiExhibitionMemberApplicationYearly Exhibitor { get; set; }
        public virtual ICollection<XsiEreceipt> XsiEreceipt { get; set; }
    }
}

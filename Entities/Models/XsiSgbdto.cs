﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiSgbdto
    {
        public long ItemId { get; set; }
        public long? StaffGuestId { get; set; }
        public long? MemberId { get; set; }
        public long? ExhibitionId { get; set; }
        public string NameEn { get; set; }
        public string LastName { get; set; }
        public string NameAr { get; set; }
        public string StaffGuestStatus { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}

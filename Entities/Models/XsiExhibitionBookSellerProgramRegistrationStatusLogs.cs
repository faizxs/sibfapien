﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBookSellerProgramRegistrationStatusLogs
    {
        public long ItemId { get; set; }
        public long RegistrationId { get; set; }
        public long? AdminId { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedOn { get; set; }

        public virtual XsiExhibitionBookSellerProgramRegistration Registration { get; set; }
    }
}

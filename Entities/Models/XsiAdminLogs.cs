﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiAdminLogs
    {
        public long ItemId { get; set; }
        public long? UserId { get; set; }
        public string Username { get; set; }
        public DateTime? LoggedOn { get; set; }
        public string Ipaddress { get; set; }

        public virtual XsiAdminUsers User { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionExhibitorDetails
    {
        public long ExhibitorDetailsId { get; set; }
        public long? MemberExhibitionYearlyId { get; set; }
        public long? BoothSectionId { get; set; }
        public long? BoothSubSectionId { get; set; }
        public string IsInternalOrExternal { get; set; }
        public string AllocatedSpace { get; set; }
        public string AllocatedAreaType { get; set; }
        public string Area { get; set; }
        public long? RequiredAreaId { get; set; }
        public string RequiredAreaType { get; set; }
        public string CalculatedArea { get; set; }
        public string HallNumber { get; set; }
        public string StandNumberStart { get; set; }
        public string StandNumberEnd { get; set; }
        public string StandCode { get; set; }
        public string FeatureId { get; set; }
        public string FirstLocation { get; set; }
        public string SecondLocation { get; set; }
        public string ThirdLocation { get; set; }
        public string FourthLocation { get; set; }
        public string FifthLocation { get; set; }
        public string SixthLocation { get; set; }
        public string UploadWarningLetter { get; set; }
        public string CancellationReason { get; set; }
        public string FileName { get; set; }
        public string TradeLicence { get; set; }
        public string Brief { get; set; }
        public string BriefAr { get; set; }
        public string Apu { get; set; }
        public string StandPhoto { get; set; }
        public string StandLayoutByMeter { get; set; }
        public string RequiredPowerCapacity { get; set; }
        public string BoothDetail { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionBooth BoothSection { get; set; }
        public virtual XsiExhibitionBoothSubsection BoothSubSection { get; set; }
        public virtual XsiExhibitionMemberApplicationYearly MemberExhibitionYearly { get; set; }
    }
}

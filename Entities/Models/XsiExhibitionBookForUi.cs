﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBookForUi
    {
        public long BookId { get; set; }
        public long? AuthorId { get; set; }
        public long? BookTypeId { get; set; }
        public string TitleEn { get; set; }
        public string TitleAr { get; set; }
        public string Thumbnail { get; set; }
        public string EventusIden { get; set; }
        public string EventusIdar { get; set; }
        public string Price { get; set; }
        public string AuthorName { get; set; }
        public string PublisherName { get; set; }
        public string BookType { get; set; }
        public string IsBestSeller { get; set; }
        public string Isbn { get; set; }
        public string IsNew { get; set; }
        public string IssueYear { get; set; }
        public string PriceBeforeDiscount { get; set; }
        public long? ExhibitionLanguageId { get; set; }
        public long? ExhibitionSubjectId { get; set; }
        public long? ExhibitionSubsubjectId { get; set; }
        public long? PublisherId { get; set; }
        public long? ExhibitionCurrencyId { get; set; }
        public long? ExhibitorId { get; set; }
        public long? AgencyId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

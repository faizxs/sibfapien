﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionRepresentative
    {
        public XsiExhibitionRepresentative()
        {
            XsiExhibitionRepresentativeParticipating = new HashSet<XsiExhibitionRepresentativeParticipating>();
            XsiExhibitionRepresentativeParticipatingNoVisa = new HashSet<XsiExhibitionRepresentativeParticipatingNoVisa>();
        }

        public long RepresentativeId { get; set; }
        public long? MemberId { get; set; }
        public string IsActive { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public string IsEmailSent { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public DateTime? Dob { get; set; }
        public string PlaceOfBirth { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionMember Member { get; set; }
        public virtual ICollection<XsiExhibitionRepresentativeParticipating> XsiExhibitionRepresentativeParticipating { get; set; }
        public virtual ICollection<XsiExhibitionRepresentativeParticipatingNoVisa> XsiExhibitionRepresentativeParticipatingNoVisa { get; set; }
    }
}

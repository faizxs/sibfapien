﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionPpslotInvitation
    {
        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public long? RequestId { get; set; }
        public long? ResponseId { get; set; }
        public long? SlotId { get; set; }
        public string Status { get; set; }
        public string IsViewed { get; set; }
        public string Message { get; set; }
        public string InviteUrl { get; set; }
        public string IsInviteUrlclicked { get; set; }
        public long? CreatedById { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}

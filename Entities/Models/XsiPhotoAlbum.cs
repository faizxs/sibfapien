﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiPhotoAlbum
    {
        public XsiPhotoAlbum()
        {
            XsiPhoto = new HashSet<XsiPhoto>();
        }

        public long ItemId { get; set; }
        public long? SubCategoryId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? Dated { get; set; }
        public long? SortIndex { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public string DescriptionAr { get; set; }
        public DateTime? DatedAr { get; set; }
        public long? SortIndexAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiPhotoSubCategory SubCategory { get; set; }
        public virtual ICollection<XsiPhoto> XsiPhoto { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiHomepageSubBanner
    {
        public long ItemId { get; set; }
        public string Cmstitle { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string HomeBanner { get; set; }
        public string Url { get; set; }
        public string IsActive { get; set; }
        public string IsFeatured { get; set; }
        public string Color { get; set; }
        public long? SortOrder { get; set; }
        public string CmstitleAr { get; set; }
        public string TitleAr { get; set; }
        public string SubTitleAr { get; set; }
        public string HomeBannerAr { get; set; }
        public string Urlar { get; set; }
        public string IsActiveAr { get; set; }
        public string ColorAr { get; set; }
        public string Title2 { get; set; }
        public string SubTitle2 { get; set; }
        public string HomeBanner2 { get; set; }
        public string Url2 { get; set; }
        public string IsActive2 { get; set; }
        public string IsFeatured2 { get; set; }
        public string Color2 { get; set; }
        public long? SortOrder2 { get; set; }
        public string CmstitleAr2 { get; set; }
        public string TitleAr2 { get; set; }
        public string SubTitleAr2 { get; set; }
        public string HomeBannerAr2 { get; set; }
        public string Urlar2 { get; set; }
        public string IsActiveAr2 { get; set; }
        public string ColorAr2 { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiBookSellerMemberStatusLog
    {
        public long ItemId { get; set; }
        public long? BookSellerMemberId { get; set; }
        public long? AdminId { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedOn { get; set; }

        public virtual XsiBookSellerMember BookSellerMember { get; set; }
    }
}

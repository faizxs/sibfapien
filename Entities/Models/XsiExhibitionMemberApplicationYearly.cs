﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionMemberApplicationYearly
    {
        public XsiExhibitionMemberApplicationYearly()
        {
            XsiDiscount = new HashSet<XsiDiscount>();
            XsiEreceipt = new HashSet<XsiEreceipt>();
            XsiExhibitionAgencyDetails = new HashSet<XsiExhibitionAgencyDetails>();
            XsiExhibitionBookParticipatingAgency = new HashSet<XsiExhibitionBookParticipating>();
            XsiExhibitionBookParticipatingExhibitor = new HashSet<XsiExhibitionBookParticipating>();
            XsiExhibitionExhibitorDetails = new HashSet<XsiExhibitionExhibitorDetails>();
            XsiExhibitionFurnitureItemOrders = new HashSet<XsiExhibitionFurnitureItemOrders>();
            XsiExhibitionFurnitureItemStatusLogs = new HashSet<XsiExhibitionFurnitureItemStatusLogs>();
            XsiExhibitionPosdevice = new HashSet<XsiExhibitionPosdevice>();
            XsiExhibitionRepresentativeParticipating = new HashSet<XsiExhibitionRepresentativeParticipating>();
            XsiExhibitionRepresentativeParticipatingNoVisa = new HashSet<XsiExhibitionRepresentativeParticipatingNoVisa>();
            XsiExhibitionShipment = new HashSet<XsiExhibitionShipment>();
            XsiInvoice = new HashSet<XsiInvoice>();
        }

        public long MemberExhibitionYearlyId { get; set; }
        public long? ExhibitionId { get; set; }
        public long? MemberId { get; set; }
        public long? MemberRoleId { get; set; }
        public long? OldItemId { get; set; }
        public long? ParentId { get; set; }
        public long? CountryId { get; set; }
        public long? CityId { get; set; }
        public string OtherCity { get; set; }
        public long? ExhibitionCategoryId { get; set; }
        public long? LanguageUrl { get; set; }
        public string IsActive { get; set; }
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonNameAr { get; set; }
        public string ContactPersonTitle { get; set; }
        public string ContactPersonTitleAr { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string TotalNumberOfTitles { get; set; }
        public string TotalNumberOfNewTitles { get; set; }
        public string IsFirstTime { get; set; }
        public string Status { get; set; }
        public string IsStatusChanged { get; set; }
        public string FileNumber { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
        public string ExactNameBoard { get; set; }
        public string UploadLocationMap { get; set; }
        public string Notes { get; set; }
        public string TempId { get; set; }
        public string FileName { get; set; }
        public string IsEditRequest { get; set; }
        public string IsEditRequestEmailSent { get; set; }
        public string IsArabicPublisher { get; set; }
        public string IsIncludedInInvoice { get; set; }
        public string IsInternalOrExternal { get; set; }
        public string Trn { get; set; }
        public string WifiVoucherCode { get; set; }
        public string IsRegisteredByExhibitor { get; set; }
        public string AuthorizationLetter { get; set; }
        public string ContractCms { get; set; }
        public string ContractUser { get; set; }
        public string IsSampleReceived { get; set; }
        public DateTime? SampleReceivedOn { get; set; }
        public string AcknowledgementFileName1 { get; set; }
        public string AcknowledgementFileName2 { get; set; }
        public string RevenueToken { get; set; }
        public DateTime? RevenueTokenCreatedOn { get; set; }
        public string IsNotesRead { get; set; }
        public string IsBooksUpload { get; set; }
        public string BookFileName { get; set; }
        public string IsAllowApprove { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionCountry Country { get; set; }
        public virtual XsiExhibition Exhibition { get; set; }
        public virtual XsiExhibitionCategory ExhibitionCategory { get; set; }
        public virtual XsiExhibitionMember Member { get; set; }
        public virtual XsiExhibitionApplication MemberRole { get; set; }
        public virtual ICollection<XsiDiscount> XsiDiscount { get; set; }
        public virtual ICollection<XsiEreceipt> XsiEreceipt { get; set; }
        public virtual ICollection<XsiExhibitionAgencyDetails> XsiExhibitionAgencyDetails { get; set; }
        public virtual ICollection<XsiExhibitionBookParticipating> XsiExhibitionBookParticipatingAgency { get; set; }
        public virtual ICollection<XsiExhibitionBookParticipating> XsiExhibitionBookParticipatingExhibitor { get; set; }
        public virtual ICollection<XsiExhibitionExhibitorDetails> XsiExhibitionExhibitorDetails { get; set; }
        public virtual ICollection<XsiExhibitionFurnitureItemOrders> XsiExhibitionFurnitureItemOrders { get; set; }
        public virtual ICollection<XsiExhibitionFurnitureItemStatusLogs> XsiExhibitionFurnitureItemStatusLogs { get; set; }
        public virtual ICollection<XsiExhibitionPosdevice> XsiExhibitionPosdevice { get; set; }
        public virtual ICollection<XsiExhibitionRepresentativeParticipating> XsiExhibitionRepresentativeParticipating { get; set; }
        public virtual ICollection<XsiExhibitionRepresentativeParticipatingNoVisa> XsiExhibitionRepresentativeParticipatingNoVisa { get; set; }
        public virtual ICollection<XsiExhibitionShipment> XsiExhibitionShipment { get; set; }
        public virtual ICollection<XsiInvoice> XsiInvoice { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfadvertisementBanner
    {
        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string CompanyName { get; set; }
        public string ContactPerson { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string HomeBanner { get; set; }
        public string SubpageBanner { get; set; }
        public string Url { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string PageIds { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public string CompanyNameAr { get; set; }
        public string ContactPersonAr { get; set; }
        public string HomeBannerAr { get; set; }
        public string SubpageBannerAr { get; set; }
        public string Urlar { get; set; }
        public string PageIdsAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfquickLinks
    {
        public long ItemId { get; set; }
        public string Title { get; set; }
        public string PageUrl { get; set; }
        public string TitleAr { get; set; }
        public string PageUrlAr { get; set; }
        public long? PageContentId { get; set; }
        public long? SortOrder { get; set; }
        public string IsExternal { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifyBy { get; set; }
        public DateTime? ModifyOn { get; set; }
    }
}

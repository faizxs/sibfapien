﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionOtherEventsStaffGuestBookingDetails
    {
        public long ItemId { get; set; }
        public long? StaffGuestId { get; set; }
        public long? ExhibitionId { get; set; }
        public string GuestCategory { get; set; }
        public string ContactDetails { get; set; }
        public string GuestPhoneNumber { get; set; }
        public string StaffInChargePhoneNumber { get; set; }
        public string ServiceType { get; set; }
        public string MarhabaReferenceNumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public long? PreferredTime { get; set; }
        public string PreferredAirline { get; set; }
        public string ClassSeat { get; set; }
        public int? FlightBookingType { get; set; }
        public string FlightStatus { get; set; }
        public string FlightDetails { get; set; }
        public string FlightNumber { get; set; }
        public string FlightCheckInTime { get; set; }
        public string FlightCheckOutTime { get; set; }
        public string ArrivalDateTime { get; set; }
        public string ReturnDateTime { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public long? DestinationFrom { get; set; }
        public long? DestinationTo { get; set; }
        public long? OriginAirport { get; set; }
        public long? DestinationAirport { get; set; }
        public string Terminal { get; set; }
        public string ClassSeatAllotted { get; set; }
        public string FlightTicket { get; set; }
        public string ReturnClassSeat { get; set; }
        public string ReturnTerminal { get; set; }
        public string ReturnClassSeatAllotted { get; set; }
        public int? ReturnFlightBookingType { get; set; }
        public string ReturnFlightStatus { get; set; }
        public string ReturnFlight { get; set; }
        public string ReturnFlightTicket { get; set; }
        public string ReturnFlightNumber { get; set; }
        public string ReturnFlightCheckInTime { get; set; }
        public string ReturnFlightCheckOutTime { get; set; }
        public string AirlineRemarks { get; set; }
        public string Passport { get; set; }
        public string PassportCopyTwo { get; set; }
        public string IsPassportModified { get; set; }
        public string IsDirectOrTransitFlight { get; set; }
        public string SuggestFlightAttachment { get; set; }
        public DateTime? HotelBookingStartDate { get; set; }
        public DateTime? HotelBookingEndDate { get; set; }
        public string RoomType { get; set; }
        public string SpecialRequest { get; set; }
        public string HotelStatus { get; set; }
        public string ReservationReferenceNumber { get; set; }
        public string HotelName { get; set; }
        public string HotelAddress { get; set; }
        public string HotelPhoneNumber { get; set; }
        public DateTime? HotelStartDate { get; set; }
        public DateTime? HotelEndDate { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public string RoomTypeAllotted { get; set; }
        public string Notes { get; set; }
        public string ReservationCopy { get; set; }
        public string TransportationType { get; set; }
        public DateTime? TransportStartDate { get; set; }
        public DateTime? TransportEndDate { get; set; }
        public string TransportContactPerson { get; set; }
        public string TransportContactNumber { get; set; }
        public string TransportationStatus { get; set; }
        public string TransportationTypeAllotted { get; set; }
        public string IsIndividualOrGroup { get; set; }
        public int? NoofPeople { get; set; }
        public string PickupLocation { get; set; }
        public string IsActive { get; set; }
        public DateTime? FlightCreatedOn { get; set; }
        public DateTime? HotelCreatedOn { get; set; }
        public DateTime? TransportCreatedOn { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionOtherEvents Exhibition { get; set; }
    }
}

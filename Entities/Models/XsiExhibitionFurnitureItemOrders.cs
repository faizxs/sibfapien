﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionFurnitureItemOrders
    {
        public XsiExhibitionFurnitureItemOrders()
        {
            XsiExhibitionFurnitureItemOrderDetails = new HashSet<XsiExhibitionFurnitureItemOrderDetails>();
            XsiExhibitionFurnitureItemStatusLogs = new HashSet<XsiExhibitionFurnitureItemStatusLogs>();
        }

        public long ItemId { get; set; }
        public string InvoiceNumber { get; set; }
        public long? ExhibitorId { get; set; }
        public string Status { get; set; }
        public string FileName { get; set; }
        public string BankName { get; set; }
        public DateTime? TransferDate { get; set; }
        public string ReceiptNumber { get; set; }
        public string UploadPaymentReceipt { get; set; }
        public long? PaymentTypeId { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Penalty { get; set; }
        public decimal? DiscountPercent { get; set; }
        public decimal? PenaltyPercent { get; set; }
        public decimal? Vat { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? Total { get; set; }
        public decimal? PaidAmount { get; set; }
        public string DiscountNote { get; set; }
        public string Notes { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionMemberApplicationYearly Exhibitor { get; set; }
        public virtual ICollection<XsiExhibitionFurnitureItemOrderDetails> XsiExhibitionFurnitureItemOrderDetails { get; set; }
        public virtual ICollection<XsiExhibitionFurnitureItemStatusLogs> XsiExhibitionFurnitureItemStatusLogs { get; set; }
    }
}

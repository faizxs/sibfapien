﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfpoetryAwardCandidateNominationNew
    {
        public long ItemId { get; set; }
        public long? PoetryAwardNominationId { get; set; }
        public string StudentName { get; set; }
        public string StudentGrade { get; set; }
        public string ParentPhoneNumber { get; set; }
        public string Email { get; set; }
        public string SchoolName { get; set; }
        public string TeacherPhoneNumber { get; set; }
        public string TeacherEmail { get; set; }
        public string CategoryType { get; set; }
        public string Upload { get; set; }
        public string Status { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiScrfpoetryAwardNominationNew PoetryAwardNomination { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfillustrator
    {
        public XsiScrfillustrator()
        {
            XsiScrfillustrationNotification = new HashSet<XsiScrfillustrationNotification>();
            XsiScrfillustrationRegistration = new HashSet<XsiScrfillustrationRegistration>();
        }

        public long ItemId { get; set; }
        public string IsArchive { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string Year { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime? HardCopyDeadline { get; set; }
        public string Description { get; set; }
        public string FirstPrize { get; set; }
        public string SecondPrize { get; set; }
        public string ThirdPrize { get; set; }
        public string FirstHonorableMention { get; set; }
        public string SecondHonorableMention { get; set; }
        public string ThirdHonorableMention { get; set; }
        public string FourthHonorableMention { get; set; }
        public string FifthHonorableMention { get; set; }
        public string SixthHonorableMention { get; set; }
        public string Notes { get; set; }
        public string TitleAr { get; set; }
        public string DescriptionAr { get; set; }
        public string FirstPrizeAr { get; set; }
        public string SecondPrizeAr { get; set; }
        public string ThirdPrizeAr { get; set; }
        public string FirstHonorableMentionAr { get; set; }
        public string SecondHonorableMentionAr { get; set; }
        public string ThirdHonorableMentionAr { get; set; }
        public string FourthHonorableMentionAr { get; set; }
        public string FifthHonorableMentionAr { get; set; }
        public string SixthHonorableMentionAr { get; set; }
        public string NotesAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<XsiScrfillustrationNotification> XsiScrfillustrationNotification { get; set; }
        public virtual ICollection<XsiScrfillustrationRegistration> XsiScrfillustrationRegistration { get; set; }
    }
}

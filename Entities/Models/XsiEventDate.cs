﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiEventDate
    {
        public long ItemId { get; set; }
        public long? EventId { get; set; }
        public string EventusIden { get; set; }
        public string EventusIdar { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual XsiEvent Event { get; set; }
    }
}

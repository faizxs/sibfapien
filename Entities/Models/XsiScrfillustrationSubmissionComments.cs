﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfillustrationSubmissionComments
    {
        public long ItemId { get; set; }
        public long? IllustrationSubmissionId { get; set; }
        public long? LanguageId { get; set; }
        public string IsActive { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual XsiScrfillustrationSubmission IllustrationSubmission { get; set; }
    }
}

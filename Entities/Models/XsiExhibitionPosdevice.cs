﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionPosdevice
    {
        public long ItemId { get; set; }
        public long? LanguageUrl { get; set; }
        public string IsLocal { get; set; }
        public long? PosdeviceQauntity { get; set; }
        public long? ExhibitorId { get; set; }
        public string IdentificationNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FirstNameAr { get; set; }
        public string LastNameAr { get; set; }
        public long? RecipientNationality { get; set; }
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public long? PublisherOriginCountry { get; set; }
        public string PassportNumber { get; set; }
        public string EmiratedIdnumber { get; set; }
        public string MobileNumberLocal { get; set; }
        public string PublishingHouseEmail { get; set; }
        public string BankNameInsideCountry { get; set; }
        public string AccountHolderName { get; set; }
        public string Ibannumber { get; set; }
        public string AdressPublishingHouseBankAccount { get; set; }
        public string PassportCopy { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionMemberApplicationYearly Exhibitor { get; set; }
        public virtual XsiExhibitionCountry PublisherOriginCountryNavigation { get; set; }
        public virtual XsiExhibitionCountry RecipientNationalityNavigation { get; set; }
    }
}

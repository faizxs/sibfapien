﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfillustrationRegistrationLog
    {
        public long ItemId { get; set; }
        public long? RegistrationId { get; set; }
        public long? AdminUserId { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}

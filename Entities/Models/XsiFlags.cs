﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiFlags
    {
        public long ItemId { get; set; }
        public string FlagName { get; set; }
        public string FlagType { get; set; }
        public string FlagModuleIdentifier { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiCareer
    {
        public XsiCareer()
        {
            XsiCareerApplicants = new HashSet<XsiCareerApplicants>();
        }

        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string Title { get; set; }
        public string TitleAr { get; set; }
        public string JobId { get; set; }
        public DateTime? PostingDate { get; set; }
        public string Overview { get; set; }
        public string Description { get; set; }
        public string Responsibilities { get; set; }
        public string Experience { get; set; }
        public string OverviewAr { get; set; }
        public string DescriptionAr { get; set; }
        public string ResponsibilitiesAr { get; set; }
        public string ExperienceAr { get; set; }
        public long? Degree { get; set; }
        public long Department { get; set; }
        public long? SubDepartment { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiCareerDegrees DegreeNavigation { get; set; }
        public virtual XsiCareerDepartment DepartmentNavigation { get; set; }
        public virtual XsiCareerSubDepartment SubDepartmentNavigation { get; set; }
        public virtual ICollection<XsiCareerApplicants> XsiCareerApplicants { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiNewsPhotoGallery
    {
        public long ItemId { get; set; }
        public long? NewsId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }
        public string TitleAr { get; set; }
        public string DetailAr { get; set; }
        public string ImageName { get; set; }
        public long? SortIndex { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiNews News { get; set; }
    }
}

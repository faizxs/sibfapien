﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class GetExhibitorsAndCountriesSCRF_Result
    {
        public long ItemId { get; set; }
        public string PublisherNameEnglish { get; set; }
        public string PublisherNameArabic { get; set; }

        public string CountryName { get; set; }
        public string CountryNameAr { get; set; }
    }
}

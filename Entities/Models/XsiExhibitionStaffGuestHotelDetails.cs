﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionStaffGuestHotelDetails
    {
        public long ItemId { get; set; }
        public long? StaffGuestId { get; set; }
        public long? ExhibitionId { get; set; }
        public DateTime? HotelBookingStartDate { get; set; }
        public DateTime? HotelBookingEndDate { get; set; }
        public string RoomType { get; set; }
        public string SpecialRequest { get; set; }
        public string HotelStatus { get; set; }
        public string ReservationReferenceNumber { get; set; }
        public string HotelName { get; set; }
        public string HotelAddress { get; set; }
        public string HotelPhoneNumber { get; set; }
        public DateTime? HotelStartDate { get; set; }
        public DateTime? HotelEndDate { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public string RoomTypeAllotted { get; set; }
        public string Notes { get; set; }
        public string ReservationCopy { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

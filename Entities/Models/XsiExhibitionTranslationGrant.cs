﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionTranslationGrant
    {
        public XsiExhibitionTranslationGrant()
        {
            XsiExhibitionTranslationGrantMembers = new HashSet<XsiExhibitionTranslationGrantMembers>();
        }

        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string IsArchive { get; set; }
        public string IsActiveAr { get; set; }
        public string IsArchiveAr { get; set; }
        public long? ExhibitionId { get; set; }
        public string Title { get; set; }
        public string TitleAr { get; set; }
        public DateTime? Phase1StartDate { get; set; }
        public DateTime? Phase1EndDate { get; set; }
        public string Phase1Desc { get; set; }
        public DateTime? Phase2StartDate { get; set; }
        public DateTime? Phase2EndDate { get; set; }
        public string Phase2Desc { get; set; }
        public DateTime? Phase3StartDate { get; set; }
        public DateTime? Phase3EndDate { get; set; }
        public string Phase3Desc { get; set; }
        public string Phase4Desc { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibition Exhibition { get; set; }
        public virtual ICollection<XsiExhibitionTranslationGrantMembers> XsiExhibitionTranslationGrantMembers { get; set; }
    }
}

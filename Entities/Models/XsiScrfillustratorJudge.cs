﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfillustratorJudge
    {
        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? Dob { get; set; }
        public string Avatar { get; set; }
        public string Designation { get; set; }
        public string Overview { get; set; }
        public string FirstNameAr { get; set; }
        public string LastNameAr { get; set; }
        public string DesignationAr { get; set; }
        public string OverviewAr { get; set; }
        public long? SortOrder { get; set; }
        public long? LanguagePrefered { get; set; }
        public long? NationalityId { get; set; }
        public long? CountryId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}

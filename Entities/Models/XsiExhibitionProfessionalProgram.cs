﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionProfessionalProgram
    {
        public XsiExhibitionProfessionalProgram()
        {
            XsiExhibitionProfessionalProgramRegistration = new HashSet<XsiExhibitionProfessionalProgramRegistration>();
            XsiExhibitionProfessionalProgramSlots = new HashSet<XsiExhibitionProfessionalProgramSlots>();
        }

        public long ProfessionalProgramId { get; set; }
        public string Name { get; set; }
        public string NameAr { get; set; }
        public long? ExhibitionId { get; set; }
        public string IsActive { get; set; }
        public string IsArchive { get; set; }
        public string IsActiveAr { get; set; }
        public string IsArchiveAr { get; set; }
        public long? LanguageUrl { get; set; }
        public long? TimeSlot { get; set; }
        public DateTime? AppointmentStartDate { get; set; }
        public DateTime? AppointmentEndDate { get; set; }
        public DateTime? StartDateDayOne { get; set; }
        public DateTime? EndDateDayOne { get; set; }
        public DateTime? BreakTimeOneDayOneStart { get; set; }
        public DateTime? BreakTimeOneDayOneEnd { get; set; }
        public DateTime? BreakTimeTwoDayOneStart { get; set; }
        public DateTime? BreakTimeTwoDayOneEnd { get; set; }
        public DateTime? StartDateDayTwo { get; set; }
        public DateTime? EndDateDayTwo { get; set; }
        public DateTime? BreakTimeOneDayTwoStart { get; set; }
        public DateTime? BreakTimeOneDayTwoEnd { get; set; }
        public DateTime? BreakTimeTwoDayTwoStart { get; set; }
        public DateTime? BreakTimeTwoDayTwoEnd { get; set; }
        public DateTime? StartDateDayThree { get; set; }
        public DateTime? EndDateDayThree { get; set; }
        public DateTime? BreakTimeOneDayThreeStart { get; set; }
        public DateTime? BreakTimeOneDayThreeEnd { get; set; }
        public DateTime? BreakTimeTwoDayThreeStart { get; set; }
        public DateTime? BreakTimeTwoDayThreeEnd { get; set; }
        public DateTime? RegistrationStartDate { get; set; }
        public DateTime? RegistrationEndDate { get; set; }
        public string Location { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedById { get; set; }

        public virtual XsiExhibition Exhibition { get; set; }
        public virtual ICollection<XsiExhibitionProfessionalProgramRegistration> XsiExhibitionProfessionalProgramRegistration { get; set; }
        public virtual ICollection<XsiExhibitionProfessionalProgramSlots> XsiExhibitionProfessionalProgramSlots { get; set; }
    }
}

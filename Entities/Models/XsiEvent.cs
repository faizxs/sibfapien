﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiEvent
    {
        public XsiEvent()
        {
            XsiEventDate = new HashSet<XsiEventDate>();
        }

        public long ItemId { get; set; }
        public long? EventSubCategoryId { get; set; }
        public long? PhotoAlbumId { get; set; }
        public long? VideoAlbumId { get; set; }
        public long? ScrfphotoAlbumId { get; set; }
        public long? ScrfvideoAlbumId { get; set; }
        public string Type { get; set; }
        public long? WebsiteId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string IsFeatured { get; set; }
        public string Title { get; set; }
        public string Overview { get; set; }
        public string Details { get; set; }
        public string FileName { get; set; }
        public string EventThumbnailNew { get; set; }
        public string InnerTopImageOne { get; set; }
        public string InnerTopImageTwo { get; set; }
        public string Guest { get; set; }
        public string TitleAr { get; set; }
        public string OverviewAr { get; set; }
        public string DetailsAr { get; set; }
        public string FileNameAr { get; set; }
        public string EventThumbnailNewAr { get; set; }
        public string InnerTopImageOneAr { get; set; }
        public string InnerTopImageTwoAr { get; set; }
        public string GuestAr { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Location { get; set; }
        public string LocationAr { get; set; }
        public string BookingLink { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiEventSubCategory EventSubCategory { get; set; }
        public virtual ICollection<XsiEventDate> XsiEventDate { get; set; }
    }
}

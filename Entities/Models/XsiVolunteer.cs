﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiVolunteer
    {
        public XsiVolunteer()
        {
            XsiVolunteerPeriod = new HashSet<XsiVolunteerPeriod>();
        }

        public long ItemId { get; set; }
        public long? LanguageUrl { get; set; }
        public long? ExhibitionId { get; set; }
        public string ParticipationCertificate { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string FirstNameAr { get; set; }
        public string LastNameAr { get; set; }
        public string Gender { get; set; }
        public DateTime? Dob { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Organization { get; set; }
        public string Other { get; set; }
        public string IsUid { get; set; }
        public string UniversityNameOrUid { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string ClaimedNoofHrs { get; set; }
        public string PreviousExperience { get; set; }
        public string OtherNotes { get; set; }
        public string Passport { get; set; }
        public string EmiratesIddocument { get; set; }
        public string EmiratesIddocumentTwo { get; set; }
        public string Cv { get; set; }
        public string SecurityApprovalDocument { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Status { get; set; }
        public string IsActive { get; set; }
        public string Notes { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedById { get; set; }

        public virtual ICollection<XsiVolunteerPeriod> XsiVolunteerPeriod { get; set; }
    }
}

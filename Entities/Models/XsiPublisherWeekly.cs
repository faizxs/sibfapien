﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiPublisherWeekly
    {
        public long ItemId { get; set; }
        public long WebsiteId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public string IssueNumber { get; set; }
        public DateTime? PublisherDate { get; set; }
        public string PdffileName { get; set; }
        public string MagazineCover { get; set; }
        public string CategoryAr { get; set; }
        public string NameAr { get; set; }
        public string IssueNumberAr { get; set; }
        public DateTime? PublisherDateAr { get; set; }
        public string PdffileNameAr { get; set; }
        public string MagazineCoverAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

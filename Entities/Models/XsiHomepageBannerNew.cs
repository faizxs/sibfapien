﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiHomepageBannerNew
    {
        public long ItemId { get; set; }
        public string Cmstitle { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string HomeBanner { get; set; }
        public string Url { get; set; }
        public string IsActive { get; set; }
        public string IsFeatured { get; set; }
        public string Color { get; set; }
        public long? SortOrder { get; set; }
        public string CmstitleAr { get; set; }
        public string TitleAr { get; set; }
        public string SubTitleAr { get; set; }
        public string HomeBannerAr { get; set; }
        public string Urlar { get; set; }
        public string IsActiveAr { get; set; }
        public string ColorAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

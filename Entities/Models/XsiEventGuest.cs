﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiEventGuest
    {
        public long ItemId { get; set; }
        public long? EventId { get; set; }
        public long? GuestId { get; set; }
    }
}

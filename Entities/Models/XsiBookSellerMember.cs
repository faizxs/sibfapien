﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiBookSellerMember
    {
        public XsiBookSellerMember()
        {
            XsiBookSellerMemberStatusLog = new HashSet<XsiBookSellerMemberStatusLog>();
            XsiBooksellerMemberEvents = new HashSet<XsiBooksellerMemberEvents>();
            XsiExhibitionBookSellerProgramRegistration = new HashSet<XsiExhibitionBookSellerProgramRegistration>();
            XsiExhibitionBookSellerStaffGuest = new HashSet<XsiExhibitionBookSellerStaffGuest>();
        }

        public long MemberId { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string EmailNew { get; set; }
        public string Password { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string Mobile { get; set; }
        public string IsVerified { get; set; }
        public string IsEmailSent { get; set; }
        public string Status { get; set; }
        public string EditRequest { get; set; }
        public string IsSibfdepartment { get; set; }
        public long? SibfdepartmentId { get; set; }
        public string IsLibraryMember { get; set; }
        public string AdminEmailOtp { get; set; }
        public DateTime? AdminEmailOtpexpiresAt { get; set; }
        public DateTime? ForgotPasswordExpiresAt { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedById { get; set; }

        public virtual ICollection<XsiBookSellerMemberStatusLog> XsiBookSellerMemberStatusLog { get; set; }
        public virtual ICollection<XsiBooksellerMemberEvents> XsiBooksellerMemberEvents { get; set; }
        public virtual ICollection<XsiExhibitionBookSellerProgramRegistration> XsiExhibitionBookSellerProgramRegistration { get; set; }
        public virtual ICollection<XsiExhibitionBookSellerStaffGuest> XsiExhibitionBookSellerStaffGuest { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionOtherEventsStaffGuestParticipating
    {
        public long StaffGuestId { get; set; }
        public long ExhibitionId { get; set; }
        public long? CountryId { get; set; }
        public string IsActive { get; set; }
        public string IsUaeresident { get; set; }
        public string Uidno { get; set; }
        public string CountryUidno { get; set; }
        public string IsTravelDetails { get; set; }
        public string IsEmailSent { get; set; }
        public string Status { get; set; }
        public long? LanguageId { get; set; }
        public string NameEn { get; set; }
        public string LastName { get; set; }
        public string NameAr { get; set; }
        public string Profession { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string TravelDetails { get; set; }
        public string NeedVisa { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public long? ArrivalAirportId { get; set; }
        public long? ArrivalTerminalId { get; set; }
        public DateTime? DepartureDate { get; set; }
        public string Passport { get; set; }
        public string PlaceOfIssue { get; set; }
        public DateTime? DateOfIssue { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string PassportCopy { get; set; }
        public string Photo { get; set; }
        public string FlightTickets { get; set; }
        public string VisaCopy { get; set; }
        public string Invoice { get; set; }
        public string Notes { get; set; }
        public string VisaApplicationForm { get; set; }
        public string NoofVisaMonths { get; set; }
        public long? GuestType { get; set; }
        public long? GuestCategory { get; set; }
        public DateTime? ActivityDate { get; set; }
        public string PaidVisa { get; set; }
        public string RepresentativeAdminNote { get; set; }
        public string PassportCopyTwo { get; set; }
        public string VisaType { get; set; }
        public string HealthInsurance { get; set; }
        public string HealthInsuranceTwo { get; set; }
        public string AddedBy { get; set; }
        public string IsVisitedUae { get; set; }
        public string RejectedStatus { get; set; }
        public long? MainGuestId { get; set; }
        public string MainGuest { get; set; }
        public string IsCompanion { get; set; }
        public string CompanionDescription { get; set; }
        public string IsFromPcregistration { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionCountry Country { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiInvoiceStatusLog
    {
        public long ItemId { get; set; }
        public long? InvoiceId { get; set; }
        public long? ExhibitorId { get; set; }
        public long? AdminId { get; set; }
        public long? GalaxyTeamId { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}

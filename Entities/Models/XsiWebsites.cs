﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiWebsites
    {
        public XsiWebsites()
        {
            XsiExhibition = new HashSet<XsiExhibition>();
        }

        public long WebsiteId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<XsiExhibition> XsiExhibition { get; set; }
    }
}

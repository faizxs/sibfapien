﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiEventType
    {
        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public string Type { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
    }
}

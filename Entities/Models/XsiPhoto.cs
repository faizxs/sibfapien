﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiPhoto
    {
        public long ItemId { get; set; }
        public long PhotoAlbumId { get; set; }
        public string IsActive { get; set; }
        public string IsAlbumCover { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public long? SortIndex { get; set; }
        public string IsActiveAr { get; set; }
        public string IsAlbumCoverAr { get; set; }
        public string TitleAr { get; set; }
        public string DescriptionAr { get; set; }
        public string ThumbnailAr { get; set; }
        public long? SortIndexAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiPhotoAlbum PhotoAlbum { get; set; }
    }
}

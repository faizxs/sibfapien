﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiNews
    {
        public XsiNews()
        {
            XsiNewsPhotoGallery = new HashSet<XsiNewsPhotoGallery>();
        }

        public long ItemId { get; set; }
        public long CategoryId { get; set; }
        public long? WebsiteId { get; set; }
        public string Title { get; set; }
        public DateTime? Dated { get; set; }
        public string Detail { get; set; }
        public string Overview { get; set; }
        public string IsActive { get; set; }
        public string ImageName { get; set; }
        public string FileName { get; set; }
        public string TitleAr { get; set; }
        public DateTime? DatedAr { get; set; }
        public string DetailAr { get; set; }
        public string OverviewAr { get; set; }
        public string IsActiveAr { get; set; }
        public string ImageNameAr { get; set; }
        public string FileNameAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiNewsCategory Category { get; set; }
        public virtual ICollection<XsiNewsPhotoGallery> XsiNewsPhotoGallery { get; set; }
    }
}

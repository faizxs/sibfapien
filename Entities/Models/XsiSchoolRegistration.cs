﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiSchoolRegistration
    {
        public long ItemId { get; set; }
        public long? WebsiteId { get; set; }
        public long? ExhibitionId { get; set; }
        public long? EventId { get; set; }
        public string Name { get; set; }
        public long? Type { get; set; }
        public string PersonInCharge { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public long? Emirates { get; set; }
        public string NumberOfChildren { get; set; }
        public string ChildrenAgeRange { get; set; }
        public DateTime? DateOfVisit { get; set; }
        public string ActivityOfInterest { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibition Exhibition { get; set; }
    }
}

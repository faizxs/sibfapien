﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiAdminDashboardIcons
    {
        public long ItemId { get; set; }
        public long? UserId { get; set; }
        public long? RoleId { get; set; }
        public long? PageId { get; set; }
        public string PageName { get; set; }
        public string PageUrl { get; set; }
        public string IconName { get; set; }
        public string IsShow { get; set; }
        public string IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual XsiAdminRoles Role { get; set; }
        public virtual XsiAdminUsers User { get; set; }
    }
}

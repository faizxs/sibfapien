﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfpoetryAwardNominationNew
    {
        public XsiScrfpoetryAwardNominationNew()
        {
            XsiScrfpoetryAwardCandidateNominationNew = new HashSet<XsiScrfpoetryAwardCandidateNominationNew>();
        }

        public long ItemId { get; set; }
        public long? ExhibitionId { get; set; }
        public long? AwardId { get; set; }
        public long? SubAwardId { get; set; }
        public long? SubmissionType { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public int? NoOfCandidates { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibition Exhibition { get; set; }
        public virtual ICollection<XsiScrfpoetryAwardCandidateNominationNew> XsiScrfpoetryAwardCandidateNominationNew { get; set; }
    }
}

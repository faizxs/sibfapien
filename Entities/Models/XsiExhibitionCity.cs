﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionCity
    {
        public XsiExhibitionCity()
        {
            XsiExhibitionMember = new HashSet<XsiExhibitionMember>();
            XsiExhibitionProfessionalProgramRegistration = new HashSet<XsiExhibitionProfessionalProgramRegistration>();
            XsiScrfillustrationRegistration = new HashSet<XsiScrfillustrationRegistration>();
        }

        public long CityId { get; set; }
        public long CountryId { get; set; }
        public long? MemberId { get; set; }
        public string IsActive { get; set; }
        public string CityName { get; set; }
        public string CityNameAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionCountry Country { get; set; }
        public virtual ICollection<XsiExhibitionMember> XsiExhibitionMember { get; set; }
        public virtual ICollection<XsiExhibitionProfessionalProgramRegistration> XsiExhibitionProfessionalProgramRegistration { get; set; }
        public virtual ICollection<XsiScrfillustrationRegistration> XsiScrfillustrationRegistration { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBooth
    {
        public XsiExhibitionBooth()
        {
            XsiExhibitionBoothSubsection = new HashSet<XsiExhibitionBoothSubsection>();
            XsiExhibitionExhibitorDetails = new HashSet<XsiExhibitionExhibitorDetails>();
        }

        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public long? WebsiteType { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<XsiExhibitionBoothSubsection> XsiExhibitionBoothSubsection { get; set; }
        public virtual ICollection<XsiExhibitionExhibitorDetails> XsiExhibitionExhibitorDetails { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionRepresentativeParticipatingNoVisaStatusLogs
    {
        public long ItemId { get; set; }
        public long? ExhibitorId { get; set; }
        public long? RepresentativeId { get; set; }
        public long? AdminId { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionDate
    {
        public long ItemId { get; set; }
        public long? LanguageId { get; set; }
        public long? GroupId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public long? WebsiteId { get; set; }
        public string IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfcontactusEnquiry
    {
        public long ItemId { get; set; }
        public long? CategoryId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public long? CountryId { get; set; }
        public string Subject { get; set; }
        public string Detail { get; set; }
        public string Status { get; set; }
        public DateTime? DateAdded { get; set; }
        public DateTime? DateClosed { get; set; }
        public string IsViewed { get; set; }
        public string FlagType { get; set; }
    }
}

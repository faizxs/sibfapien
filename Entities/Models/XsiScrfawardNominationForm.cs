﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfawardNominationForm
    {
        public long ItemId { get; set; }
        public string Status { get; set; }
        public long? ExhibitionId { get; set; }
        public long? LanguageId { get; set; }
        public long? AwardId { get; set; }
        public long? SubAwardId { get; set; }
        public string OtherCity { get; set; }
        public long? CityId { get; set; }
        public long? CountryId { get; set; }
        public string Title { get; set; }
        public string AuthorName { get; set; }
        public string IllustratorName { get; set; }
        public string AgeGroupCategory { get; set; }
        public string AgeGroupCategory2 { get; set; }
        public string Publisher { get; set; }
        public string Isbn { get; set; }
        public string CopyrightHolder { get; set; }
        public string IllustratorOriginal { get; set; }
        public string AuthorOriginal { get; set; }
        public string IllustratorTactile { get; set; }
        public string AuthorTactile { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Pobox { get; set; }
        public string PublishingYear { get; set; }
        public string FileName { get; set; }
        public DateTime? Date { get; set; }
        public string Comments { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiPhotoSubCategory
    {
        public XsiPhotoSubCategory()
        {
            XsiPhotoAlbum = new HashSet<XsiPhotoAlbum>();
        }

        public long ItemId { get; set; }
        public long? CategoryId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiPhotoCategory Category { get; set; }
        public virtual ICollection<XsiPhotoAlbum> XsiPhotoAlbum { get; set; }
    }
}

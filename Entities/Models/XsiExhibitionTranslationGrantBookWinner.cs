﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionTranslationGrantBookWinner
    {
        public long ItemId { get; set; }
        public long? TranslationGrantRegistrationId { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public string BookTitleEn { get; set; }
        public string BookTitleAr { get; set; }
        public long? Category { get; set; }
        public string Year { get; set; }
        public long? BookLanguage { get; set; }
        public long? TranslatedTo { get; set; }
        public string BookThumbnail { get; set; }
        public long? Edition { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionTranslationGrantMembers TranslationGrantRegistration { get; set; }
    }
}

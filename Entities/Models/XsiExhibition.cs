﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibition
    {
        public XsiExhibition()
        {
            XsiAwardWinners = new HashSet<XsiAwardWinners>();
            XsiAwardsNominationForms = new HashSet<XsiAwardsNominationForms>();
            XsiExhibitionDays = new HashSet<XsiExhibitionDays>();
            XsiExhibitionMemberApplicationYearly = new HashSet<XsiExhibitionMemberApplicationYearly>();
            XsiExhibitionProfessionalProgram = new HashSet<XsiExhibitionProfessionalProgram>();
            XsiExhibitionStaffGuestParticipating = new HashSet<XsiExhibitionStaffGuestParticipating>();
            XsiExhibitionTranslationGrant = new HashSet<XsiExhibitionTranslationGrant>();
            XsiSchoolRegistration = new HashSet<XsiSchoolRegistration>();
            XsiScrfpoetryAwardNominationNew = new HashSet<XsiScrfpoetryAwardNominationNew>();
            XsiSharjahAwardForTranslation = new HashSet<XsiSharjahAwardForTranslation>();
        }

        public long ExhibitionId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string IsArchive { get; set; }
        public long? WebsiteId { get; set; }
        public string TotalArea { get; set; }
        public long? ExhibitionYear { get; set; }
        public long? ExhibitionNumber { get; set; }
        public string TopContent { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? ExhibitorStartDate { get; set; }
        public DateTime? ExhibitorEndDate { get; set; }
        public DateTime? RestaurantStartDate { get; set; }
        public DateTime? RestaurantEndDate { get; set; }
        public DateTime? AgencyStartDate { get; set; }
        public DateTime? AgencyEndDate { get; set; }
        public DateTime? AwardStartDate { get; set; }
        public DateTime? AwardEndDate { get; set; }
        public DateTime? TurjumanStartDate { get; set; }
        public DateTime? TurjumanEndDate { get; set; }
        public DateTime? AudioAwardStartDate { get; set; }
        public DateTime? AudioAwardEndDate { get; set; }
        public DateTime? RepresentativeStartDate { get; set; }
        public DateTime? RepresentativeEndDate { get; set; }
        public DateTime? StaffGuestStartDate { get; set; }
        public DateTime? StaffGuestEndDate { get; set; }
        public DateTime? DateOfCacellation { get; set; }
        public string CancellationFee { get; set; }
        public string RepresentativeCancellationFee { get; set; }
        public DateTime? DateOfDue { get; set; }
        public DateTime? StaffGuestBookingExpiryDate { get; set; }
        public string FileName { get; set; }
        public string Url { get; set; }
        public string Penalty { get; set; }
        public string ParticipationFee { get; set; }
        public string PriceSqM { get; set; }
        public string VisaProcessingPrice { get; set; }
        public string ArabicAgencyFees { get; set; }
        public string ForeignAgencyFees { get; set; }
        public string AdvertisementRates { get; set; }
        public string ImportantInformation { get; set; }
        public string ExpiryMonths { get; set; }
        public string TopContentAr { get; set; }
        public string CancellationFeeAr { get; set; }
        public string FileNameAr { get; set; }
        public string Urlar { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiWebsites Website { get; set; }
        public virtual ICollection<XsiAwardWinners> XsiAwardWinners { get; set; }
        public virtual ICollection<XsiAwardsNominationForms> XsiAwardsNominationForms { get; set; }
        public virtual ICollection<XsiExhibitionDays> XsiExhibitionDays { get; set; }
        public virtual ICollection<XsiExhibitionMemberApplicationYearly> XsiExhibitionMemberApplicationYearly { get; set; }
        public virtual ICollection<XsiExhibitionProfessionalProgram> XsiExhibitionProfessionalProgram { get; set; }
        public virtual ICollection<XsiExhibitionStaffGuestParticipating> XsiExhibitionStaffGuestParticipating { get; set; }
        public virtual ICollection<XsiExhibitionTranslationGrant> XsiExhibitionTranslationGrant { get; set; }
        public virtual ICollection<XsiSchoolRegistration> XsiSchoolRegistration { get; set; }
        public virtual ICollection<XsiScrfpoetryAwardNominationNew> XsiScrfpoetryAwardNominationNew { get; set; }
        public virtual ICollection<XsiSharjahAwardForTranslation> XsiSharjahAwardForTranslation { get; set; }
    }
}

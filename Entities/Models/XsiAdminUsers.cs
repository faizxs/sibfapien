﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiAdminUsers
    {
        public XsiAdminUsers()
        {
            XsiAdminDashboardIcons = new HashSet<XsiAdminDashboardIcons>();
            XsiAdminLogs = new HashSet<XsiAdminLogs>();
            XsiAdminPermissions = new HashSet<XsiAdminPermissions>();
        }

        public long ItemId { get; set; }
        public long RoleId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string IsActive { get; set; }
        public long? CountryId { get; set; }
        public string IsSuperAdmin { get; set; }
        public string IsTwoFactorEnabled { get; set; }
        public string AuthSecretKey { get; set; }
        public DateTime? CreatedOn { get; set; }

        public virtual XsiAdminRoles Role { get; set; }
        public virtual ICollection<XsiAdminDashboardIcons> XsiAdminDashboardIcons { get; set; }
        public virtual ICollection<XsiAdminLogs> XsiAdminLogs { get; set; }
        public virtual ICollection<XsiAdminPermissions> XsiAdminPermissions { get; set; }
    }
}

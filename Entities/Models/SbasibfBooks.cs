﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class SbasibfBooks
    {
        public long BookId { get; set; }
        public string BookName { get; set; }
        public string BookNameAr { get; set; }
        public string Isbn { get; set; }
        public string Thumbnail { get; set; }
        public string Price { get; set; }
        public long? PublisherId { get; set; }
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public long AuthorId { get; set; }
        public string AuthorName { get; set; }
        public string AuthorNameAr { get; set; }
        public long BookSubSubjectId { get; set; }
        public string BookSubSubject { get; set; }
        public string BookSubSubjectAr { get; set; }
        public long BookSubjectId { get; set; }
        public string BookSubject { get; set; }
        public string BookSubjectAr { get; set; }
        public string HallNumber { get; set; }
        public string AllocatedSpace { get; set; }
        public string StandCode { get; set; }
    }
}

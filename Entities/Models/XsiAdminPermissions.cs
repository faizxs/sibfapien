﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiAdminPermissions
    {
        public long ItemId { get; set; }
        public long? UserId { get; set; }
        public long? RoleId { get; set; }
        public long? PageId { get; set; }
        public string IsRead { get; set; }
        public string IsCreate { get; set; }
        public string IsWrite { get; set; }
        public string IsDelete { get; set; }

        public virtual XsiAdminPages Page { get; set; }
        public virtual XsiAdminRoles Role { get; set; }
        public virtual XsiAdminUsers User { get; set; }
    }
}

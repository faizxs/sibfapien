﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBoothSubsection
    {
        public XsiExhibitionBoothSubsection()
        {
            XsiExhibitionExhibitorDetails = new HashSet<XsiExhibitionExhibitorDetails>();
        }

        public long ItemId { get; set; }
        public long? CategoryId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public string Color { get; set; }
        public long? WebsiteType { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionBooth Category { get; set; }
        public virtual ICollection<XsiExhibitionExhibitorDetails> XsiExhibitionExhibitorDetails { get; set; }
    }
}

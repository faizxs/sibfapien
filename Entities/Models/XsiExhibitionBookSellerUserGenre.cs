﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBookSellerUserGenre
    {
        public long ItemId { get; set; }
        public long? BookSellerRegistrationId { get; set; }
        public long? GenreId { get; set; }

        public virtual XsiExhibitionBookSellerProgramRegistration BookSellerRegistration { get; set; }
    }
}

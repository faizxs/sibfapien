﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiEreceipt
    {
        public long ItemId { get; set; }
        public long? ExhibitorId { get; set; }
        public long? InvoiceId { get; set; }
        public long? OrderId { get; set; }
        public string EreceiptNumber { get; set; }
        public string IsActive { get; set; }
        public string Status { get; set; }
        public string IsPayLater { get; set; }
        public string AmountDueInAed { get; set; }
        public string PaymentReceivedInAed { get; set; }
        public string AmountDueInUsd { get; set; }
        public string PaymentReceivedInUsd { get; set; }
        public string FileName { get; set; }
        public string PaymentType { get; set; }
        public string FileBankTransfer1 { get; set; }
        public string FileBankTransfer2 { get; set; }
        public string FileBankTransfer3 { get; set; }
        public string BankName { get; set; }
        public string ReceiptOrChequeNumber { get; set; }
        public DateTime? TransferDate { get; set; }
        public string Comments { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionMemberApplicationYearly Exhibitor { get; set; }
        public virtual XsiInvoice Invoice { get; set; }
    }
}

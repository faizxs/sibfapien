﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiNewsCategory
    {
        public XsiNewsCategory()
        {
            XsiNews = new HashSet<XsiNews>();
        }

        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public string DescriptionAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }

        public virtual ICollection<XsiNews> XsiNews { get; set; }
    }
}

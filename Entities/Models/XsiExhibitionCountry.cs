﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionCountry
    {
        public XsiExhibitionCountry()
        {
            XsiCareerApplicantsNationalityNavigation = new HashSet<XsiCareerApplicants>();
            XsiCareerApplicantsResidenceNavigation = new HashSet<XsiCareerApplicants>();
            XsiContactusEnquiry = new HashSet<XsiContactusEnquiry>();
            XsiExhibitionCity = new HashSet<XsiExhibitionCity>();
            XsiExhibitionCountryAirport = new HashSet<XsiExhibitionCountryAirport>();
            XsiExhibitionMemberApplicationYearly = new HashSet<XsiExhibitionMemberApplicationYearly>();
            XsiExhibitionOtherEventsStaffGuestParticipating = new HashSet<XsiExhibitionOtherEventsStaffGuestParticipating>();
            XsiExhibitionPosdevicePublisherOriginCountryNavigation = new HashSet<XsiExhibitionPosdevice>();
            XsiExhibitionPosdeviceRecipientNationalityNavigation = new HashSet<XsiExhibitionPosdevice>();
            XsiExhibitionProfessionalProgramRegistration = new HashSet<XsiExhibitionProfessionalProgramRegistration>();
            XsiExhibitionRepresentativeParticipating = new HashSet<XsiExhibitionRepresentativeParticipating>();
            XsiExhibitionRepresentativeParticipatingNoVisa = new HashSet<XsiExhibitionRepresentativeParticipatingNoVisa>();
            XsiExhibitionShipmentShipmentCountryFromNavigation = new HashSet<XsiExhibitionShipment>();
            XsiExhibitionShipmentShipmentCountryToNavigation = new HashSet<XsiExhibitionShipment>();
            XsiExhibitionStaffGuestParticipating = new HashSet<XsiExhibitionStaffGuestParticipating>();
            XsiNewsletterSubscribers = new HashSet<XsiNewsletterSubscribers>();
            XsiScrfillustrationRegistrationCountry = new HashSet<XsiScrfillustrationRegistration>();
            XsiScrfillustrationRegistrationNationality = new HashSet<XsiScrfillustrationRegistration>();
            XsiSharjahAwardForTranslationArabPublishHouseCountry = new HashSet<XsiSharjahAwardForTranslation>();
            XsiSharjahAwardForTranslationCountry = new HashSet<XsiSharjahAwardForTranslation>();
        }

        public long CountryId { get; set; }
        public string IsActive { get; set; }
        public string CountryName { get; set; }
        public string CountryNameAr { get; set; }
        public string CountryCode { get; set; }
        public string CountryCodeIsotwo { get; set; }
        public string CurrencyCode { get; set; }
        public string IsVisaRequired { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<XsiCareerApplicants> XsiCareerApplicantsNationalityNavigation { get; set; }
        public virtual ICollection<XsiCareerApplicants> XsiCareerApplicantsResidenceNavigation { get; set; }
        public virtual ICollection<XsiContactusEnquiry> XsiContactusEnquiry { get; set; }
        public virtual ICollection<XsiExhibitionCity> XsiExhibitionCity { get; set; }
        public virtual ICollection<XsiExhibitionCountryAirport> XsiExhibitionCountryAirport { get; set; }
        public virtual ICollection<XsiExhibitionMemberApplicationYearly> XsiExhibitionMemberApplicationYearly { get; set; }
        public virtual ICollection<XsiExhibitionOtherEventsStaffGuestParticipating> XsiExhibitionOtherEventsStaffGuestParticipating { get; set; }
        public virtual ICollection<XsiExhibitionPosdevice> XsiExhibitionPosdevicePublisherOriginCountryNavigation { get; set; }
        public virtual ICollection<XsiExhibitionPosdevice> XsiExhibitionPosdeviceRecipientNationalityNavigation { get; set; }
        public virtual ICollection<XsiExhibitionProfessionalProgramRegistration> XsiExhibitionProfessionalProgramRegistration { get; set; }
        public virtual ICollection<XsiExhibitionRepresentativeParticipating> XsiExhibitionRepresentativeParticipating { get; set; }
        public virtual ICollection<XsiExhibitionRepresentativeParticipatingNoVisa> XsiExhibitionRepresentativeParticipatingNoVisa { get; set; }
        public virtual ICollection<XsiExhibitionShipment> XsiExhibitionShipmentShipmentCountryFromNavigation { get; set; }
        public virtual ICollection<XsiExhibitionShipment> XsiExhibitionShipmentShipmentCountryToNavigation { get; set; }
        public virtual ICollection<XsiExhibitionStaffGuestParticipating> XsiExhibitionStaffGuestParticipating { get; set; }
        public virtual ICollection<XsiNewsletterSubscribers> XsiNewsletterSubscribers { get; set; }
        public virtual ICollection<XsiScrfillustrationRegistration> XsiScrfillustrationRegistrationCountry { get; set; }
        public virtual ICollection<XsiScrfillustrationRegistration> XsiScrfillustrationRegistrationNationality { get; set; }
        public virtual ICollection<XsiSharjahAwardForTranslation> XsiSharjahAwardForTranslationArabPublishHouseCountry { get; set; }
        public virtual ICollection<XsiSharjahAwardForTranslation> XsiSharjahAwardForTranslationCountry { get; set; }
    }
}

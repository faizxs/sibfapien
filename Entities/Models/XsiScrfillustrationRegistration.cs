﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfillustrationRegistration
    {
        public XsiScrfillustrationRegistration()
        {
            XsiScrfillustrationNotificationStatus = new HashSet<XsiScrfillustrationNotificationStatus>();
            XsiScrfillustrationSubmission = new HashSet<XsiScrfillustrationSubmission>();
        }

        public long ItemId { get; set; }
        public long? LanguageId { get; set; }
        public long? IllustrationId { get; set; }
        public string FileNumber { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FirstNameAr { get; set; }
        public string LastNameAr { get; set; }
        public long? SequenceCounter { get; set; }
        public string SequenceNumber { get; set; }
        public string Passport { get; set; }
        public string Cv { get; set; }
        public string Thumbnail { get; set; }
        public DateTime? Dob { get; set; }
        public long? NationalityId { get; set; }
        public long? CountryId { get; set; }
        public long? CityId { get; set; }
        public string Others { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Bio { get; set; }
        public string BioAr { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PostalCode { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string NickName { get; set; }
        public string NickNameAr { get; set; }
        public string Status { get; set; }
        public string IsActive { get; set; }
        public string Notification { get; set; }
        public string Qrcode { get; set; }
        public long? Qrrank { get; set; }
        public string AdminEmailOtp { get; set; }
        public DateTime? AdminEmailOtpexpiresAt { get; set; }
        public DateTime? ForgotPasswordExpiresAt { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual XsiExhibitionCity City { get; set; }
        public virtual XsiExhibitionCountry Country { get; set; }
        public virtual XsiScrfillustrator Illustration { get; set; }
        public virtual XsiExhibitionCountry Nationality { get; set; }
        public virtual ICollection<XsiScrfillustrationNotificationStatus> XsiScrfillustrationNotificationStatus { get; set; }
        public virtual ICollection<XsiScrfillustrationSubmission> XsiScrfillustrationSubmission { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiPublisherNews
    {
        public XsiPublisherNews()
        {
            XsiPublisherNewsPhotoGallery = new HashSet<XsiPublisherNewsPhotoGallery>();
        }

        public long ItemId { get; set; }
        public long? PublisherNewsCategoryId { get; set; }
        public long? WebsiteId { get; set; }
        public string Title { get; set; }
        public DateTime? Dated { get; set; }
        public string Detail { get; set; }
        public string Overview { get; set; }
        public string IsActive { get; set; }
        public string ImageName { get; set; }
        public string FileName { get; set; }
        public string TitleAr { get; set; }
        public DateTime? DatedAr { get; set; }
        public string DetailAr { get; set; }
        public string OverviewAr { get; set; }
        public string IsActiveAr { get; set; }
        public string ImageNameAr { get; set; }
        public string FileNameAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiPublisherNewsCategory PublisherNewsCategory { get; set; }
        public virtual ICollection<XsiPublisherNewsPhotoGallery> XsiPublisherNewsPhotoGallery { get; set; }
    }
}

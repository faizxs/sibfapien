﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiSharjahAwardForTranslation
    {
        public long ItemId { get; set; }
        public string Status { get; set; }
        public long? ExhibitionId { get; set; }
        public long? AwardId { get; set; }
        public string TranslatedBookName { get; set; }
        public string PublishedYearOfFirstTranslation { get; set; }
        public string TranslatorName { get; set; }
        public string BookNameArabic { get; set; }
        public string PublishedYearOfFirstArabicTranslation { get; set; }
        public string AuthorName { get; set; }
        public string ForeignPublishName { get; set; }
        public long? CountryId { get; set; }
        public long? TranslationLanguageId { get; set; }
        public string CorrespondenceAddress { get; set; }
        public string Pobox { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string ArabPublishHouseName { get; set; }
        public long? ArabPublishHouseCountryId { get; set; }
        public string ArabPublishHouseNameAdress { get; set; }
        public string ArabPublishHousePobox { get; set; }
        public string ArabPublishHousePhone { get; set; }
        public string ArabPublishHouseFax { get; set; }
        public string ArabPublishHouseEmail { get; set; }
        public string ArabPublishHouseNominationBody { get; set; }
        public DateTime? Date { get; set; }
        public string PassportCopy { get; set; }
        public string Comments { get; set; }
        public string TranslatorNameEn { get; set; }
        public string TranslatorNameAr { get; set; }
        public string BookTitleEn { get; set; }
        public string BookTitleAr { get; set; }
        public string BookCover { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionCountry ArabPublishHouseCountry { get; set; }
        public virtual XsiExhibitionCountry Country { get; set; }
        public virtual XsiExhibition Exhibition { get; set; }
    }
}

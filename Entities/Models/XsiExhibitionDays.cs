﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionDays
    {
        public long ItemId { get; set; }
        public long? ExhibitionId { get; set; }
        public DateTime? ExhibitionDate { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibition Exhibition { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiVolunteerPeriod
    {
        public long ItemId { get; set; }
        public long? VolunteerId { get; set; }
        public long? Shift { get; set; }
        public string NoofDays { get; set; }

        public virtual XsiVolunteer Volunteer { get; set; }
    }
}

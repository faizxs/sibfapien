﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiEventCategory
    {
        public XsiEventCategory()
        {
            XsiEventSubCategory = new HashSet<XsiEventSubCategory>();
        }

        public long ItemId { get; set; }
        public long? WebsiteId { get; set; }
        public string IsActive { get; set; }
        public string EventtusEventCategoryId { get; set; }
        public string EmailId { get; set; }
        public string Title { get; set; }
        public string Thumbnail { get; set; }
        public string Details { get; set; }
        public string ScrfDetails { get; set; }
        public string Color { get; set; }
        public string ScrfThumbnail { get; set; }
        public string ScrfColor { get; set; }
        public string TitleAr { get; set; }
        public string ThumbnailAr { get; set; }
        public string DetailsAr { get; set; }
        public string ScrfDetailsAr { get; set; }
        public string ColorAr { get; set; }
        public string ScrfThumbnailAr { get; set; }
        public string ScrfColorAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<XsiEventSubCategory> XsiEventSubCategory { get; set; }
    }
}

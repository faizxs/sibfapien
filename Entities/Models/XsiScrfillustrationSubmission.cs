﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfillustrationSubmission
    {
        public XsiScrfillustrationSubmission()
        {
            XsiScrfillustrationSubmissionComments = new HashSet<XsiScrfillustrationSubmissionComments>();
        }

        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public long? IllustratorId { get; set; }
        public string UserPhotoId { get; set; }
        public long? WinnerPosition { get; set; }
        public string FileOriginial { get; set; }
        public string FilePreview { get; set; }
        public string FileFullView { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? CreateDate { get; set; }
        public string TechniqueOrMaterials { get; set; }
        public string SizeWidth { get; set; }
        public string SizeHeight { get; set; }
        public decimal? Price { get; set; }
        public string IsPublished { get; set; }
        public string BookTitleEn { get; set; }
        public string BookTitleAr { get; set; }
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public string Address { get; set; }
        public string PublishYear { get; set; }
        public string IsForSale { get; set; }
        public long? UsersRated { get; set; }
        public decimal? Rating { get; set; }
        public string Status { get; set; }
        public decimal? IllustrationPrice { get; set; }
        public string CreateDateYear { get; set; }
        public string BookBrief { get; set; }
        public string BookBriefAr { get; set; }
        public string IsReturn { get; set; }
        public string IsOriginalOrPrinted { get; set; }
        public decimal? InsuranceFee { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual XsiScrfillustrationRegistration Illustrator { get; set; }
        public virtual ICollection<XsiScrfillustrationSubmissionComments> XsiScrfillustrationSubmissionComments { get; set; }
    }
}

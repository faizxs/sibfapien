﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionStaffGuestFlightDetails
    {
        public long ItemId { get; set; }
        public long? StaffGuestId { get; set; }
        public long? ExhibitionId { get; set; }
        public string GuestCategory { get; set; }
        public string ContactDetails { get; set; }
        public string GuestPhoneNumber { get; set; }
        public string StaffInChargePhoneNumber { get; set; }
        public string ServiceType { get; set; }
        public string MarhabaReferenceNumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public long? PreferredTime { get; set; }
        public string PreferredAirline { get; set; }
        public string ClassSeat { get; set; }
        public int? FlightBookingType { get; set; }
        public string FlightStatus { get; set; }
        public string FlightDetails { get; set; }
        public string FlightNumber { get; set; }
        public string FlightCheckInTime { get; set; }
        public string FlightCheckOutTime { get; set; }
        public string ArrivalDateTime { get; set; }
        public string ReturnDateTime { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public string Terminal { get; set; }
        public string ClassSeatAllotted { get; set; }
        public string FlightTicket { get; set; }
        public string Passport { get; set; }
        public string PassportCopyTwo { get; set; }
        public string IsPassportModified { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

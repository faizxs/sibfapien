﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionShipment
    {
        public XsiExhibitionShipment()
        {
            XsiExhibitionShipmentAgency = new HashSet<XsiExhibitionShipmentAgency>();
        }

        public long ItemId { get; set; }
        public long? LanguageUrl { get; set; }
        public long? MemberId { get; set; }
        public long? ExhibitorId { get; set; }
        public long? AgencyId { get; set; }
        public long? ExhibitionId { get; set; }
        public string IsAgency { get; set; }
        public long? ShipmentCount { get; set; }
        public long? ShipmentType { get; set; }
        public string ShipmentCompanyName { get; set; }
        public string BillofLading { get; set; }
        public string ShipmentNumber { get; set; }
        public long? ShipmentCountryFrom { get; set; }
        public long? ShipmentCountryTo { get; set; }
        public string ReceivingCompanyName { get; set; }
        public long? ReceivedShipmentLocationType { get; set; }
        public string QrfileName { get; set; }
        public long? NoofBooks { get; set; }
        public long? CategoryId { get; set; }
        public string BooksFile { get; set; }
        public string IsLocalWarehouse { get; set; }
        public long? WshipmentCount { get; set; }
        public string WshipmentCompanyName { get; set; }
        public string WshipmentNumber { get; set; }
        public long? WnoofBooks { get; set; }
        public string WbooksFile { get; set; }
        public string IsActive { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionMemberApplicationYearly Exhibitor { get; set; }
        public virtual XsiExhibitionCountry ShipmentCountryFromNavigation { get; set; }
        public virtual XsiExhibitionCountry ShipmentCountryToNavigation { get; set; }
        public virtual ICollection<XsiExhibitionShipmentAgency> XsiExhibitionShipmentAgency { get; set; }
    }
}

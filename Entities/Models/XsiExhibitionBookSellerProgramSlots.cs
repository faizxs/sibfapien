﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBookSellerProgramSlots
    {
        public long ItemId { get; set; }
        public long? ProfessionalProgramId { get; set; }
        public string IsDelete { get; set; }
        public string IsBreak { get; set; }
        public long? DayId { get; set; }
        public string BreakTitle { get; set; }
        public string BreakTitleAr { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}

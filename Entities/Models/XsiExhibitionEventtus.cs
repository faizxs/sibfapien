﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionEventtus
    {
        public long ItemId { get; set; }
        public string EventtusId { get; set; }
        public long? WebsiteId { get; set; }
        public string IsActive { get; set; }
        public string Day { get; set; }
        public string Title { get; set; }
        public string DayAr { get; set; }
        public string TitleAr { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiAwards
    {
        public XsiAwards()
        {
            XsiAwardsNominationForms = new HashSet<XsiAwardsNominationForms>();
            XsiSubAwards = new HashSet<XsiSubAwards>();
        }

        public long ItemId { get; set; }
        public long WebsiteId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public string Introduction { get; set; }
        public string Category { get; set; }
        public string Objectives { get; set; }
        public string Conditions { get; set; }
        public string Requirements { get; set; }
        public string NameAr { get; set; }
        public string ThumbnailAr { get; set; }
        public string IntroductionAr { get; set; }
        public string CategoryAr { get; set; }
        public string ObjectivesAr { get; set; }
        public string ConditionsAr { get; set; }
        public string RequirementsAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<XsiAwardsNominationForms> XsiAwardsNominationForms { get; set; }
        public virtual ICollection<XsiSubAwards> XsiSubAwards { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBookSellerUserProgram
    {
        public long ItemId { get; set; }
        public long? BookSellerRegistrationId { get; set; }
        public long? PpprogramId { get; set; }

        public virtual XsiExhibitionBookSellerProgramRegistration BookSellerRegistration { get; set; }
    }
}

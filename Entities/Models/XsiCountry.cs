﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiCountry
    {
        public long ItemId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public long? ItemOrder { get; set; }
        public string IsActive { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class FinDueExcel
    {
        public long ItemId { get; set; }
        public long? CustomerId { get; set; }
        public string CustomerNameAr { get; set; }
        public string CustomerNameEn { get; set; }
        public string CustomerType { get; set; }
        public long? ExId { get; set; }
        public long? FileNumber { get; set; }
        public decimal? CustomerBalance { get; set; }
        public string IsAllow { get; set; }
    }
}

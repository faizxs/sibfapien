﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiAdvertisementPage
    {
        public long ItemId { get; set; }
        public long? AdvertisementId { get; set; }
        public long? PageId { get; set; }
    }
}

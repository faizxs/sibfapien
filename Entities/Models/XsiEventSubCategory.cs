﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiEventSubCategory
    {
        public XsiEventSubCategory()
        {
            XsiEvent = new HashSet<XsiEvent>();
        }

        public long ItemId { get; set; }
        public long? EventCategoryId { get; set; }
        public long? WebsiteId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiEventCategory EventCategory { get; set; }
        public virtual ICollection<XsiEvent> XsiEvent { get; set; }
    }
}

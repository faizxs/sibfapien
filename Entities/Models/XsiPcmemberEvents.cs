﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiPcmemberEvents
    {
        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public long? MemberId { get; set; }
        public long? EventId { get; set; }
        public long? TicketCount { get; set; }
        public string Status { get; set; }
        public string AdminNotes { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiPcevents Event { get; set; }
        public virtual XsiExhibitionMember Member { get; set; }
    }
}

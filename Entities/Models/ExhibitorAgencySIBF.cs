﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    public class ExhibitorAgencySIBF
    {
        public string Status { get; set; }
        public string PaymentStatus { get; set; }
        public long MemberExhibitionYearlyId { get; set; }
        public long? MemberId { get; set; }
        public long? MemberRoleId { get; set; }
        public long? ParentId { get; set; }
        public long? CountryId { get; set; }
        public long? CityId { get; set; }
        public string OtherCity { get; set; }
        public string IsActive { get; set; }
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonNameAr { get; set; }
        public string ContactPersonTitle { get; set; }
        public string ContactPersonTitleAr { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string TotalNumberOfTitles { get; set; }
        public string TotalNumberOfNewTitles { get; set; }
        public string FileNumber { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
        public string IsArabicPublisher { get; set; }
        public string YearStatus { get; set; }
        public string WifiVoucherCode { get; set; }
        public string ExhibitorYearlyStatus { get; set; }
        public long? Expr1 { get; set; }
        public string CityName { get; set; }
        public string CityNameAr { get; set; }
        public long? Expr2 { get; set; }
        public string CountryName { get; set; }
        public string CountryNameAr { get; set; }
        public long? BoothId { get; set; }
        public string BoothName { get; set; }
        public string BoothNameAr { get; set; }
        public long? BoothSubSectionId { get; set; }
        public string BoothSubSectionName { get; set; }
        public string BoothSubSectionNameAr { get; set; }
        public string AllocatedSpace { get; set; }
        public string HallNumber { get; set; }
        public string StandCode { get; set; }
    }
}
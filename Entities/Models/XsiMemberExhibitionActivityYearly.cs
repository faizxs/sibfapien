﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiMemberExhibitionActivityYearly
    {
        public long MemberExhibitionYearlyId { get; set; }
        public long ActivityId { get; set; }
    }
}

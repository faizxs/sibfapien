﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiAwardNominationRegistrations
    {
        public long ItemId { get; set; }
        public string Status { get; set; }
        public long? ExhibitionId { get; set; }
        public long? AwardId { get; set; }
        public long? SubAwardId { get; set; }
        public string NominationType { get; set; }
        public string Title { get; set; }
        public string BookCover { get; set; }
        public string PublishedYear { get; set; }
        public string Isbn { get; set; }
        public string AuthorName { get; set; }
        public string AuthorNumber { get; set; }
        public string AuthorEmail { get; set; }
        public long? AuthorNationalityId { get; set; }
        public string PassportCopy { get; set; }
        public string Publisher { get; set; }
        public string PublisherNumber { get; set; }
        public string PublisherEmail { get; set; }
        public long? PublisherCountryId { get; set; }
        public string Comments { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiSubAwardNew SubAward { get; set; }
    }
}

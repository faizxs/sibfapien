﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfillustrationNotification
    {
        public XsiScrfillustrationNotification()
        {
            XsiScrfillustrationNotificationStatus = new HashSet<XsiScrfillustrationNotificationStatus>();
        }

        public long ItemId { get; set; }
        public long? IllustrationId { get; set; }
        public string Description { get; set; }

        public virtual XsiScrfillustrator Illustration { get; set; }
        public virtual ICollection<XsiScrfillustrationNotificationStatus> XsiScrfillustrationNotificationStatus { get; set; }
    }
}

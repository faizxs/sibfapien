﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBookSubject
    {
        public XsiExhibitionBookSubject()
        {
            XsiExhibitionBookSubsubject = new HashSet<XsiExhibitionBookSubsubject>();
            XsiExhibitionBooks = new HashSet<XsiExhibitionBooks>();
        }

        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public string Color { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<XsiExhibitionBookSubsubject> XsiExhibitionBookSubsubject { get; set; }
        public virtual ICollection<XsiExhibitionBooks> XsiExhibitionBooks { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class FormDataDb
    {
        public int DataId { get; set; }
        public string TranslationLang { get; set; }
        public string RholderName { get; set; }
        public string RholderCompany { get; set; }
        public string RholderCountry { get; set; }
        public string RbuyerName { get; set; }
        public string RbuyerCompany { get; set; }
        public string RbuyerCountry { get; set; }
        public string BookOriginalLang { get; set; }
        public string BookProposedLang { get; set; }
        public string BookTitleOriginal { get; set; }
        public string BookTitleArabic { get; set; }
        public string BookTitleEnglish { get; set; }
        public bool OldTranslations { get; set; }
        public string OldTranslationLang { get; set; }
        public DateTime CreatedOn { get; set; }
        public string RbuyerEmail { get; set; }
        public string RholderEmail { get; set; }
        public string Author { get; set; }
        public string Genre { get; set; }
        public string Isbn { get; set; }
        public string RbuyerAddress { get; set; }
        public string RbuyerCountryCode { get; set; }
        public string RbuyerFirstName { get; set; }
        public string RbuyerJobTitle { get; set; }
        public string RbuyerLastName { get; set; }
        public string RbuyerPhoneExtension { get; set; }
        public string RbuyerPhoneNumber { get; set; }
        public string RbuyerSecondName { get; set; }
        public string RholderAddress { get; set; }
        public string RholderCountryCode { get; set; }
        public string RholderFirstName { get; set; }
        public string RholderJobTitle { get; set; }
        public string RholderLastName { get; set; }
        public string RholderPhoneExtension { get; set; }
        public string RholderPhoneNumber { get; set; }
        public string RholderSecondName { get; set; }
        public string NumOfPagesOriginal { get; set; }
        public string NumOfWordsOriginal { get; set; }
        public int GenreId { get; set; }
        public string OriginalLanguageId { get; set; }
        public string ProposedLanguageId { get; set; }
        public string TranslationType { get; set; }
        public string MemberId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitorBadges
    {
        public long ItemId { get; set; }
        public long? LanguageId { get; set; }
        public string IsActive { get; set; }
        public string Status { get; set; }
        public long? MemberExhibitionYearlyId { get; set; }
        public string Name { get; set; }
        public string NameAr { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string IsEmailSent { get; set; }
        public long? PrintCount { get; set; }
        public string FileName { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiDownloads
    {
        public long ItemId { get; set; }
        public long DownloadCategoryId { get; set; }
        public long? WebsiteId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public DateTime? Dated { get; set; }
        public string Thumbnail { get; set; }
        public string FileName { get; set; }
        public string Overview { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public DateTime? DatedAr { get; set; }
        public string ThumbnailAr { get; set; }
        public string FileNameAr { get; set; }
        public string OverviewAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiDownloadCategory DownloadCategory { get; set; }
    }
}

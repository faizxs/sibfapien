﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionOtherEventsEmailContent
    {
        public long ItemId { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string RollbackXml { get; set; }
        public string TitleAr { get; set; }
        public string EmailAr { get; set; }
        public string SubjectAr { get; set; }
        public string BodyAr { get; set; }
        public string RollbackXmlAr { get; set; }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Entities.Models
{
    public partial class sibfnewdbContext : DbContext
    {
        public sibfnewdbContext()
        {
        }

        public sibfnewdbContext(DbContextOptions<sibfnewdbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Dupauthor2020> Dupauthor2020 { get; set; }
        public virtual DbSet<Dupauthorunique2020> Dupauthorunique2020 { get; set; }
        public virtual DbSet<ElmahError> ElmahError { get; set; }
        public virtual DbSet<ExhibitionDashboard> ExhibitionDashboard { get; set; }
        public virtual DbSet<FinDueExcel> FinDueExcel { get; set; }
        public virtual DbSet<FormDataDb> FormDataDb { get; set; }
        public virtual DbSet<SbasibfBooks> SbasibfBooks { get; set; }
        public virtual DbSet<XsiActivitiesBanner> XsiActivitiesBanner { get; set; }
        public virtual DbSet<XsiActivitiesSuggestion> XsiActivitiesSuggestion { get; set; }
        public virtual DbSet<XsiAdminDashboardIcons> XsiAdminDashboardIcons { get; set; }
        public virtual DbSet<XsiAdminLogs> XsiAdminLogs { get; set; }
        public virtual DbSet<XsiAdminPages> XsiAdminPages { get; set; }
        public virtual DbSet<XsiAdminPermissions> XsiAdminPermissions { get; set; }
        public virtual DbSet<XsiAdminRoles> XsiAdminRoles { get; set; }
        public virtual DbSet<XsiAdminUsers> XsiAdminUsers { get; set; }
        public virtual DbSet<XsiAdvertisementBanner> XsiAdvertisementBanner { get; set; }
        public virtual DbSet<XsiAdvertisementPage> XsiAdvertisementPage { get; set; }
        public virtual DbSet<XsiAnnouncement> XsiAnnouncement { get; set; }
        public virtual DbSet<XsiArrivalTerminal> XsiArrivalTerminal { get; set; }
        public virtual DbSet<XsiAwardNew> XsiAwardNew { get; set; }
        public virtual DbSet<XsiAwardNominationRegistrations> XsiAwardNominationRegistrations { get; set; }
        public virtual DbSet<XsiAwardRegistration> XsiAwardRegistration { get; set; }
        public virtual DbSet<XsiAwardWinners> XsiAwardWinners { get; set; }
        public virtual DbSet<XsiAwards> XsiAwards { get; set; }
        public virtual DbSet<XsiAwardsNominationForms> XsiAwardsNominationForms { get; set; }
        public virtual DbSet<XsiBookSellerMember> XsiBookSellerMember { get; set; }
        public virtual DbSet<XsiBookSellerMemberLog> XsiBookSellerMemberLog { get; set; }
        public virtual DbSet<XsiBookSellerMemberStatusLog> XsiBookSellerMemberStatusLog { get; set; }
        public virtual DbSet<XsiBooksellerEvents> XsiBooksellerEvents { get; set; }
        public virtual DbSet<XsiBooksellerMemberEvents> XsiBooksellerMemberEvents { get; set; }
        public virtual DbSet<XsiCareer> XsiCareer { get; set; }
        public virtual DbSet<XsiCareerApplicants> XsiCareerApplicants { get; set; }
        public virtual DbSet<XsiCareerDegrees> XsiCareerDegrees { get; set; }
        public virtual DbSet<XsiCareerDepartment> XsiCareerDepartment { get; set; }
        public virtual DbSet<XsiCareerSpeciality> XsiCareerSpeciality { get; set; }
        public virtual DbSet<XsiCareerSubDepartment> XsiCareerSubDepartment { get; set; }
        public virtual DbSet<XsiCareerSubSpeciality> XsiCareerSubSpeciality { get; set; }
        public virtual DbSet<XsiCareerVisaStatus> XsiCareerVisaStatus { get; set; }
        public virtual DbSet<XsiCareerYob> XsiCareerYob { get; set; }
        public virtual DbSet<XsiCmspageMenu> XsiCmspageMenu { get; set; }
        public virtual DbSet<XsiContactusEnquiry> XsiContactusEnquiry { get; set; }
        public virtual DbSet<XsiCountry> XsiCountry { get; set; }
        public virtual DbSet<XsiDiscount> XsiDiscount { get; set; }
        public virtual DbSet<XsiDownloadCategory> XsiDownloadCategory { get; set; }
        public virtual DbSet<XsiDownloads> XsiDownloads { get; set; }
        public virtual DbSet<XsiEmailCategory> XsiEmailCategory { get; set; }
        public virtual DbSet<XsiEmailContent> XsiEmailContent { get; set; }
        public virtual DbSet<XsiEreceipt> XsiEreceipt { get; set; }
        public virtual DbSet<XsiEreceiptStatusLogs> XsiEreceiptStatusLogs { get; set; }
        public virtual DbSet<XsiEvent> XsiEvent { get; set; }
        public virtual DbSet<XsiEventCategory> XsiEventCategory { get; set; }
        public virtual DbSet<XsiEventDate> XsiEventDate { get; set; }
        public virtual DbSet<XsiEventGuest> XsiEventGuest { get; set; }
        public virtual DbSet<XsiEventSubCategory> XsiEventSubCategory { get; set; }
        public virtual DbSet<XsiEventType> XsiEventType { get; set; }
        public virtual DbSet<XsiEventWebsite> XsiEventWebsite { get; set; }
        public virtual DbSet<XsiExhibition> XsiExhibition { get; set; }
        public virtual DbSet<XsiExhibitionActivity> XsiExhibitionActivity { get; set; }
        public virtual DbSet<XsiExhibitionActivityYearly> XsiExhibitionActivityYearly { get; set; }
        public virtual DbSet<XsiExhibitionAgencyDetails> XsiExhibitionAgencyDetails { get; set; }
        public virtual DbSet<XsiExhibitionApplication> XsiExhibitionApplication { get; set; }
        public virtual DbSet<XsiExhibitionArea> XsiExhibitionArea { get; set; }
        public virtual DbSet<XsiExhibitionArrivalAirport> XsiExhibitionArrivalAirport { get; set; }
        public virtual DbSet<XsiExhibitionAuthor> XsiExhibitionAuthor { get; set; }
        public virtual DbSet<XsiExhibitionBookAuthor> XsiExhibitionBookAuthor { get; set; }
        public virtual DbSet<XsiExhibitionBookForUi> XsiExhibitionBookForUi { get; set; }
        public virtual DbSet<XsiExhibitionBookParticipating> XsiExhibitionBookParticipating { get; set; }
        public virtual DbSet<XsiExhibitionBookPublisher> XsiExhibitionBookPublisher { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerGenre> XsiExhibitionBookSellerGenre { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerProgram> XsiExhibitionBookSellerProgram { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerProgramRegistration> XsiExhibitionBookSellerProgramRegistration { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerProgramRegistrationStatusLogs> XsiExhibitionBookSellerProgramRegistrationStatusLogs { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerProgramSlots> XsiExhibitionBookSellerProgramSlots { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerProgramSource> XsiExhibitionBookSellerProgramSource { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerProgramUserBook> XsiExhibitionBookSellerProgramUserBook { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerSlotInvitation> XsiExhibitionBookSellerSlotInvitation { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerStaffGuest> XsiExhibitionBookSellerStaffGuest { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerStaffGuestBookingDetails> XsiExhibitionBookSellerStaffGuestBookingDetails { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerStaffGuestParticipating> XsiExhibitionBookSellerStaffGuestParticipating { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerStaffGuestParticipatingStatusLogs> XsiExhibitionBookSellerStaffGuestParticipatingStatusLogs { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerUserBooksLanguage> XsiExhibitionBookSellerUserBooksLanguage { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerUserGenre> XsiExhibitionBookSellerUserGenre { get; set; }
        public virtual DbSet<XsiExhibitionBookSellerUserProgram> XsiExhibitionBookSellerUserProgram { get; set; }
        public virtual DbSet<XsiExhibitionBookSubject> XsiExhibitionBookSubject { get; set; }
        public virtual DbSet<XsiExhibitionBookSubsubject> XsiExhibitionBookSubsubject { get; set; }
        public virtual DbSet<XsiExhibitionBookType> XsiExhibitionBookType { get; set; }
        public virtual DbSet<XsiExhibitionBooks> XsiExhibitionBooks { get; set; }
        public virtual DbSet<XsiExhibitionBookseller> XsiExhibitionBookseller { get; set; }
        public virtual DbSet<XsiExhibitionBooth> XsiExhibitionBooth { get; set; }
        public virtual DbSet<XsiExhibitionBoothSubsection> XsiExhibitionBoothSubsection { get; set; }
        public virtual DbSet<XsiExhibitionCategory> XsiExhibitionCategory { get; set; }
        public virtual DbSet<XsiExhibitionCateringSystemRegistration> XsiExhibitionCateringSystemRegistration { get; set; }
        public virtual DbSet<XsiExhibitionCity> XsiExhibitionCity { get; set; }
        public virtual DbSet<XsiExhibitionCountry> XsiExhibitionCountry { get; set; }
        public virtual DbSet<XsiExhibitionCountryAirport> XsiExhibitionCountryAirport { get; set; }
        public virtual DbSet<XsiExhibitionCurrency> XsiExhibitionCurrency { get; set; }
        public virtual DbSet<XsiExhibitionDate> XsiExhibitionDate { get; set; }
        public virtual DbSet<XsiExhibitionDays> XsiExhibitionDays { get; set; }
        public virtual DbSet<XsiExhibitionEventtus> XsiExhibitionEventtus { get; set; }
        public virtual DbSet<XsiExhibitionExhibitorDetails> XsiExhibitionExhibitorDetails { get; set; }
        public virtual DbSet<XsiExhibitionFinesCategory> XsiExhibitionFinesCategory { get; set; }
        public virtual DbSet<XsiExhibitionFurnitureItemOrderDetails> XsiExhibitionFurnitureItemOrderDetails { get; set; }
        public virtual DbSet<XsiExhibitionFurnitureItemOrders> XsiExhibitionFurnitureItemOrders { get; set; }
        public virtual DbSet<XsiExhibitionFurnitureItemStatusLogs> XsiExhibitionFurnitureItemStatusLogs { get; set; }
        public virtual DbSet<XsiExhibitionFurnitureItems> XsiExhibitionFurnitureItems { get; set; }
        public virtual DbSet<XsiExhibitionLanguage> XsiExhibitionLanguage { get; set; }
        public virtual DbSet<XsiExhibitionMember> XsiExhibitionMember { get; set; }
        public virtual DbSet<XsiExhibitionMemberApplicationYearly> XsiExhibitionMemberApplicationYearly { get; set; }
        public virtual DbSet<XsiExhibitionMemberApplicationYearlyLogs> XsiExhibitionMemberApplicationYearlyLogs { get; set; }
        public virtual DbSet<XsiExhibitionMemberApplicationYearlyPreApproved> XsiExhibitionMemberApplicationYearlyPreApproved { get; set; }
        public virtual DbSet<XsiExhibitionMemberLog> XsiExhibitionMemberLog { get; set; }
        public virtual DbSet<XsiExhibitionMemberStatusLog> XsiExhibitionMemberStatusLog { get; set; }
        public virtual DbSet<XsiExhibitionMemberandExhibitorFines> XsiExhibitionMemberandExhibitorFines { get; set; }
        public virtual DbSet<XsiExhibitionOtherEvents> XsiExhibitionOtherEvents { get; set; }
        public virtual DbSet<XsiExhibitionOtherEventsEmailContent> XsiExhibitionOtherEventsEmailContent { get; set; }
        public virtual DbSet<XsiExhibitionOtherEventsStaffGuest> XsiExhibitionOtherEventsStaffGuest { get; set; }
        public virtual DbSet<XsiExhibitionOtherEventsStaffGuestBookingDetails> XsiExhibitionOtherEventsStaffGuestBookingDetails { get; set; }
        public virtual DbSet<XsiExhibitionOtherEventsStaffGuestParticipating> XsiExhibitionOtherEventsStaffGuestParticipating { get; set; }
        public virtual DbSet<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs> XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs { get; set; }
        public virtual DbSet<XsiExhibitionPosdevice> XsiExhibitionPosdevice { get; set; }
        public virtual DbSet<XsiExhibitionPpbooksLanguage> XsiExhibitionPpbooksLanguage { get; set; }
        public virtual DbSet<XsiExhibitionPpgenre> XsiExhibitionPpgenre { get; set; }
        public virtual DbSet<XsiExhibitionPpprogram> XsiExhibitionPpprogram { get; set; }
        public virtual DbSet<XsiExhibitionPpslotInvitation> XsiExhibitionPpslotInvitation { get; set; }
        public virtual DbSet<XsiExhibitionProfessionalProgram> XsiExhibitionProfessionalProgram { get; set; }
        public virtual DbSet<XsiExhibitionProfessionalProgramBook> XsiExhibitionProfessionalProgramBook { get; set; }
        public virtual DbSet<XsiExhibitionProfessionalProgramRegistration> XsiExhibitionProfessionalProgramRegistration { get; set; }
        public virtual DbSet<XsiExhibitionProfessionalProgramRegistrationStatusLogs> XsiExhibitionProfessionalProgramRegistrationStatusLogs { get; set; }
        public virtual DbSet<XsiExhibitionProfessionalProgramSlots> XsiExhibitionProfessionalProgramSlots { get; set; }
        public virtual DbSet<XsiExhibitionProfessionalProgramSource> XsiExhibitionProfessionalProgramSource { get; set; }
        public virtual DbSet<XsiExhibitionRegion> XsiExhibitionRegion { get; set; }
        public virtual DbSet<XsiExhibitionRepresentative> XsiExhibitionRepresentative { get; set; }
        public virtual DbSet<XsiExhibitionRepresentativeParticipating> XsiExhibitionRepresentativeParticipating { get; set; }
        public virtual DbSet<XsiExhibitionRepresentativeParticipatingNoVisa> XsiExhibitionRepresentativeParticipatingNoVisa { get; set; }
        public virtual DbSet<XsiExhibitionRepresentativeParticipatingNoVisaStatusLogs> XsiExhibitionRepresentativeParticipatingNoVisaStatusLogs { get; set; }
        public virtual DbSet<XsiExhibitionRepresentativeParticipatingStatusLogs> XsiExhibitionRepresentativeParticipatingStatusLogs { get; set; }
        public virtual DbSet<XsiExhibitionShipment> XsiExhibitionShipment { get; set; }
        public virtual DbSet<XsiExhibitionShipmentAgency> XsiExhibitionShipmentAgency { get; set; }
        public virtual DbSet<XsiExhibitionStaffGuest> XsiExhibitionStaffGuest { get; set; }
        public virtual DbSet<XsiExhibitionStaffGuestBookingDetails> XsiExhibitionStaffGuestBookingDetails { get; set; }
        public virtual DbSet<XsiExhibitionStaffGuestFlightDetails> XsiExhibitionStaffGuestFlightDetails { get; set; }
        public virtual DbSet<XsiExhibitionStaffGuestHotelDetails> XsiExhibitionStaffGuestHotelDetails { get; set; }
        public virtual DbSet<XsiExhibitionStaffGuestParticipating> XsiExhibitionStaffGuestParticipating { get; set; }
        public virtual DbSet<XsiExhibitionStaffGuestParticipatingStatusLogs> XsiExhibitionStaffGuestParticipatingStatusLogs { get; set; }
        public virtual DbSet<XsiExhibitionStaffGuestTransportDetails> XsiExhibitionStaffGuestTransportDetails { get; set; }
        public virtual DbSet<XsiExhibitionTranslationGrant> XsiExhibitionTranslationGrant { get; set; }
        public virtual DbSet<XsiExhibitionTranslationGrantBookWinner> XsiExhibitionTranslationGrantBookWinner { get; set; }
        public virtual DbSet<XsiExhibitionTranslationGrantCategory> XsiExhibitionTranslationGrantCategory { get; set; }
        public virtual DbSet<XsiExhibitionTranslationGrantMemberStatusLog> XsiExhibitionTranslationGrantMemberStatusLog { get; set; }
        public virtual DbSet<XsiExhibitionTranslationGrantMembers> XsiExhibitionTranslationGrantMembers { get; set; }
        public virtual DbSet<XsiExhibitor> XsiExhibitor { get; set; }
        public virtual DbSet<XsiExhibitorBadges> XsiExhibitorBadges { get; set; }
        public virtual DbSet<XsiFlags> XsiFlags { get; set; }
        public virtual DbSet<XsiGenre> XsiGenre { get; set; }
        public virtual DbSet<XsiGuestOfHonor> XsiGuestOfHonor { get; set; }
        public virtual DbSet<XsiGuestWebsite> XsiGuestWebsite { get; set; }
        public virtual DbSet<XsiGuests> XsiGuests { get; set; }
        public virtual DbSet<XsiHamzatWasl> XsiHamzatWasl { get; set; }
        public virtual DbSet<XsiHomePageLink> XsiHomePageLink { get; set; }
        public virtual DbSet<XsiHomepageBanner> XsiHomepageBanner { get; set; }
        public virtual DbSet<XsiHomepageBannerNew> XsiHomepageBannerNew { get; set; }
        public virtual DbSet<XsiHomepageOrganiser> XsiHomepageOrganiser { get; set; }
        public virtual DbSet<XsiHomepageSectionFour> XsiHomepageSectionFour { get; set; }
        public virtual DbSet<XsiHomepageSectionTwo> XsiHomepageSectionTwo { get; set; }
        public virtual DbSet<XsiHomepageSectionTwoCategory> XsiHomepageSectionTwoCategory { get; set; }
        public virtual DbSet<XsiHomepageSubBanner> XsiHomepageSubBanner { get; set; }
        public virtual DbSet<XsiIconsForSponsor> XsiIconsForSponsor { get; set; }
        public virtual DbSet<XsiInvoice> XsiInvoice { get; set; }
        public virtual DbSet<XsiInvoiceStatusLog> XsiInvoiceStatusLog { get; set; }
        public virtual DbSet<XsiLanguages> XsiLanguages { get; set; }
        public virtual DbSet<XsiMemberExhibitionActivityYearly> XsiMemberExhibitionActivityYearly { get; set; }
        public virtual DbSet<XsiMemberReason> XsiMemberReason { get; set; }
        public virtual DbSet<XsiNews> XsiNews { get; set; }
        public virtual DbSet<XsiNewsCategory> XsiNewsCategory { get; set; }
        public virtual DbSet<XsiNewsPhotoGallery> XsiNewsPhotoGallery { get; set; }
        public virtual DbSet<XsiNewsletter> XsiNewsletter { get; set; }
        public virtual DbSet<XsiNewsletterBanner> XsiNewsletterBanner { get; set; }
        public virtual DbSet<XsiNewsletterSubscribers> XsiNewsletterSubscribers { get; set; }
        public virtual DbSet<XsiOurteamDepartment> XsiOurteamDepartment { get; set; }
        public virtual DbSet<XsiOurteamStaff> XsiOurteamStaff { get; set; }
        public virtual DbSet<XsiPageMenuNew> XsiPageMenuNew { get; set; }
        public virtual DbSet<XsiPagesContentNew> XsiPagesContentNew { get; set; }
        public virtual DbSet<XsiPcawardDetails> XsiPcawardDetails { get; set; }
        public virtual DbSet<XsiPcawardNominationForms> XsiPcawardNominationForms { get; set; }
        public virtual DbSet<XsiPcawards> XsiPcawards { get; set; }
        public virtual DbSet<XsiPcevents> XsiPcevents { get; set; }
        public virtual DbSet<XsiPcmemberEvents> XsiPcmemberEvents { get; set; }
        public virtual DbSet<XsiPhoto> XsiPhoto { get; set; }
        public virtual DbSet<XsiPhotoAlbum> XsiPhotoAlbum { get; set; }
        public virtual DbSet<XsiPhotoCategory> XsiPhotoCategory { get; set; }
        public virtual DbSet<XsiPhotoSubCategory> XsiPhotoSubCategory { get; set; }
        public virtual DbSet<XsiProfessionalProgramSource> XsiProfessionalProgramSource { get; set; }
        public virtual DbSet<XsiPublisherNews> XsiPublisherNews { get; set; }
        public virtual DbSet<XsiPublisherNewsCategory> XsiPublisherNewsCategory { get; set; }
        public virtual DbSet<XsiPublisherNewsPhotoGallery> XsiPublisherNewsPhotoGallery { get; set; }
        public virtual DbSet<XsiPublisherWeekly> XsiPublisherWeekly { get; set; }
        public virtual DbSet<XsiQuickLinks> XsiQuickLinks { get; set; }
        public virtual DbSet<XsiReason> XsiReason { get; set; }
        public virtual DbSet<XsiRepresentativeAgreement> XsiRepresentativeAgreement { get; set; }
        public virtual DbSet<XsiSchoolRegistration> XsiSchoolRegistration { get; set; }
        public virtual DbSet<XsiScrfactivitiesBanner> XsiScrfactivitiesBanner { get; set; }
        public virtual DbSet<XsiScrfadvertisementBanner> XsiScrfadvertisementBanner { get; set; }
        public virtual DbSet<XsiScrfadvertisementPage> XsiScrfadvertisementPage { get; set; }
        public virtual DbSet<XsiScrfannouncement> XsiScrfannouncement { get; set; }
        public virtual DbSet<XsiScrfawardNominationForm> XsiScrfawardNominationForm { get; set; }
        public virtual DbSet<XsiScrfcity> XsiScrfcity { get; set; }
        public virtual DbSet<XsiScrfcontactusEnquiry> XsiScrfcontactusEnquiry { get; set; }
        public virtual DbSet<XsiScrfcountry> XsiScrfcountry { get; set; }
        public virtual DbSet<XsiScrfemailContent> XsiScrfemailContent { get; set; }
        public virtual DbSet<XsiScrffestivalAdvertisementArea> XsiScrffestivalAdvertisementArea { get; set; }
        public virtual DbSet<XsiScrfhomepageBanner> XsiScrfhomepageBanner { get; set; }
        public virtual DbSet<XsiScrfhomepageSection> XsiScrfhomepageSection { get; set; }
        public virtual DbSet<XsiScrfhomepageSubBanner> XsiScrfhomepageSubBanner { get; set; }
        public virtual DbSet<XsiScrficonsForSponsor> XsiScrficonsForSponsor { get; set; }
        public virtual DbSet<XsiScrfillustrationNotification> XsiScrfillustrationNotification { get; set; }
        public virtual DbSet<XsiScrfillustrationNotificationStatus> XsiScrfillustrationNotificationStatus { get; set; }
        public virtual DbSet<XsiScrfillustrationRegistration> XsiScrfillustrationRegistration { get; set; }
        public virtual DbSet<XsiScrfillustrationRegistrationLog> XsiScrfillustrationRegistrationLog { get; set; }
        public virtual DbSet<XsiScrfillustrationRegistrationStatusLogs> XsiScrfillustrationRegistrationStatusLogs { get; set; }
        public virtual DbSet<XsiScrfillustrationSubmission> XsiScrfillustrationSubmission { get; set; }
        public virtual DbSet<XsiScrfillustrationSubmissionComments> XsiScrfillustrationSubmissionComments { get; set; }
        public virtual DbSet<XsiScrfillustrationSubmissionStatusLogs> XsiScrfillustrationSubmissionStatusLogs { get; set; }
        public virtual DbSet<XsiScrfillustrator> XsiScrfillustrator { get; set; }
        public virtual DbSet<XsiScrfillustratorJudge> XsiScrfillustratorJudge { get; set; }
        public virtual DbSet<XsiScrfpageMenu> XsiScrfpageMenu { get; set; }
        public virtual DbSet<XsiScrfpagesContent> XsiScrfpagesContent { get; set; }
        public virtual DbSet<XsiScrfphoto> XsiScrfphoto { get; set; }
        public virtual DbSet<XsiScrfphotoAlbum> XsiScrfphotoAlbum { get; set; }
        public virtual DbSet<XsiScrfphotoCategory> XsiScrfphotoCategory { get; set; }
        public virtual DbSet<XsiScrfphotoSubCategory> XsiScrfphotoSubCategory { get; set; }
        public virtual DbSet<XsiScrfpoetryAwardCandidateNominationNew> XsiScrfpoetryAwardCandidateNominationNew { get; set; }
        public virtual DbSet<XsiScrfpoetryAwardNominationNew> XsiScrfpoetryAwardNominationNew { get; set; }
        public virtual DbSet<XsiScrfpressRelease> XsiScrfpressRelease { get; set; }
        public virtual DbSet<XsiScrfquickLinks> XsiScrfquickLinks { get; set; }
        public virtual DbSet<XsiScrftechniqueMaterial> XsiScrftechniqueMaterial { get; set; }
        public virtual DbSet<XsiScrfvideo> XsiScrfvideo { get; set; }
        public virtual DbSet<XsiScrfvideoAlbum> XsiScrfvideoAlbum { get; set; }
        public virtual DbSet<XsiScrfvideoCategory> XsiScrfvideoCategory { get; set; }
        public virtual DbSet<XsiScrfwinners> XsiScrfwinners { get; set; }
        public virtual DbSet<XsiSgbdto> XsiSgbdto { get; set; }
        public virtual DbSet<XsiSharjahAwardForTranslation> XsiSharjahAwardForTranslation { get; set; }
        public virtual DbSet<XsiSibfcrawler> XsiSibfcrawler { get; set; }
        public virtual DbSet<XsiStaffGuestCategory> XsiStaffGuestCategory { get; set; }
        public virtual DbSet<XsiStaffGuestType> XsiStaffGuestType { get; set; }
        public virtual DbSet<XsiSubAwardNew> XsiSubAwardNew { get; set; }
        public virtual DbSet<XsiSubAwards> XsiSubAwards { get; set; }
        public virtual DbSet<XsiVideo> XsiVideo { get; set; }
        public virtual DbSet<XsiVideoAlbum> XsiVideoAlbum { get; set; }
        public virtual DbSet<XsiVideoCategory> XsiVideoCategory { get; set; }
        public virtual DbSet<XsiVipguest> XsiVipguest { get; set; }
        public virtual DbSet<XsiVipguestWebsite> XsiVipguestWebsite { get; set; }
        public virtual DbSet<XsiVisitor> XsiVisitor { get; set; }
        public virtual DbSet<XsiVolunteer> XsiVolunteer { get; set; }
        public virtual DbSet<XsiVolunteerExhibitionDays> XsiVolunteerExhibitionDays { get; set; }
        public virtual DbSet<XsiVolunteerPeriod> XsiVolunteerPeriod { get; set; }
        public virtual DbSet<XsiWebsites> XsiWebsites { get; set; }
        public DbQuery<ExhibitorAgencySIBF> vwExhibitorAgencySIBF { get; set; }

        // Unable to generate entity type for table 'dbo.PanWorldData2023'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.XsiExhibitionBookParticipatingArchive'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Books_2004'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.removebp2019'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PanWorldData'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.NewAuthor2020'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DUPAUTHOR2020All'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.findueexcelold'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.MapsData'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FzInvoiceDebitCredit'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TGMembers101'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FzLatestDebitCredits'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FinDueExcel1Jun2024'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ExhibitionMember856'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DataBookDarIbnNafees'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ExhibitionMemberReason856'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ExhibitionMemberPending'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ExhibitionMemberReasonPending'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ExhibitionMemberStatusNull'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FzNewInvoiceDebitCredit'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.sgotheremailcontent'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TempExhibitors'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.eventdatetemp'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FzFinalDebitsCredits'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tmpfzadminuserbk'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FzFinalDebitsCreditsNoDuplicates'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.XsiExhibitionAuthorNew'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Server=LAPTOP-8LB6SMU7;Database=sibfnewdb;Trusted_Connection=True;"); //Faiz
                                                                                                                    //optionsBuilder.UseSqlServer(@"server=10.1.52.98;database=sibfnewdblive;user id=sibf;password=sibf123#;"); // sibf etisalat server 
                                                                                                                    //optionsBuilder.UseSqlServer(@"server=10.1.52.98;database=sibfnewdbdemo;user id=sibfnewdbdemo;password=sibfnewdbdemo123#;"); // sibf server demodb
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Dupauthor2020>(entity =>
            {
                entity.HasKey(e => e.AuthorId);

                entity.ToTable("DUPAUTHOR2020");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Dupauthorunique2020>(entity =>
            {
                entity.HasKey(e => e.AuthorId);

                entity.ToTable("DUPAUTHORUnique2020");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ElmahError>(entity =>
            {
                entity.HasKey(e => e.ErrorId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("ELMAH_Error");

                entity.Property(e => e.ErrorId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AllXml)
                    .IsRequired()
                    .HasColumnType("ntext");

                entity.Property(e => e.Application)
                    .IsRequired()
                    .HasMaxLength(60);

                entity.Property(e => e.Host)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Sequence).ValueGeneratedOnAdd();

                entity.Property(e => e.Source)
                    .IsRequired()
                    .HasMaxLength(60);

                entity.Property(e => e.TimeUtc).HasColumnType("datetime");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ExhibitionDashboard>(entity =>
            {
                entity.HasKey(e => e.ExhibitionId);
            });

            modelBuilder.Entity<FinDueExcel>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CustomerBalance).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CustomerNameAr)
                    .HasColumnName("Customer Name Ar")
                    .HasMaxLength(255);

                entity.Property(e => e.CustomerNameEn)
                    .HasColumnName("Customer Name En")
                    .HasMaxLength(255);

                entity.Property(e => e.CustomerType)
                    .HasColumnName("Customer Type")
                    .HasMaxLength(255);

                entity.Property(e => e.ExId).HasColumnName("ExID");

                entity.Property(e => e.IsAllow)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FormDataDb>(entity =>
            {
                entity.HasKey(e => e.DataId);

                entity.ToTable("form_DataDb");

                entity.Property(e => e.DataId).HasColumnName("dataId");

                entity.Property(e => e.Author)
                    .IsRequired()
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.BookOriginalLang).IsRequired();

                entity.Property(e => e.BookProposedLang).IsRequired();

                entity.Property(e => e.BookTitleOriginal).IsRequired();

                entity.Property(e => e.CreatedOn).HasDefaultValueSql("('0001-01-01T00:00:00.0000000')");

                entity.Property(e => e.Isbn).HasColumnName("ISBN");

                entity.Property(e => e.MemberId).HasColumnName("memberId");

                entity.Property(e => e.NumOfPagesOriginal).HasColumnName("numOfPagesOriginal");

                entity.Property(e => e.NumOfWordsOriginal)
                    .IsRequired()
                    .HasColumnName("numOfWordsOriginal")
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.OriginalLanguageId)
                    .IsRequired()
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.ProposedLanguageId)
                    .IsRequired()
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.RbuyerAddress).HasColumnName("RBuyerAddress");

                entity.Property(e => e.RbuyerCompany)
                    .IsRequired()
                    .HasColumnName("RBuyerCompany");

                entity.Property(e => e.RbuyerCountry)
                    .IsRequired()
                    .HasColumnName("RBuyerCountry");

                entity.Property(e => e.RbuyerCountryCode)
                    .IsRequired()
                    .HasColumnName("RBuyerCountryCode")
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.RbuyerEmail)
                    .IsRequired()
                    .HasColumnName("RBuyerEmail")
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.RbuyerFirstName)
                    .IsRequired()
                    .HasColumnName("RBuyerFirstName")
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.RbuyerJobTitle).HasColumnName("RBuyerJobTitle");

                entity.Property(e => e.RbuyerLastName)
                    .IsRequired()
                    .HasColumnName("RBuyerLastName")
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.RbuyerName)
                    .IsRequired()
                    .HasColumnName("RBuyerName");

                entity.Property(e => e.RbuyerPhoneExtension)
                    .IsRequired()
                    .HasColumnName("RBuyerPhoneExtension")
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.RbuyerPhoneNumber)
                    .IsRequired()
                    .HasColumnName("RBuyerPhoneNumber")
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.RbuyerSecondName).HasColumnName("RBuyerSecondName");

                entity.Property(e => e.RholderAddress).HasColumnName("RHolderAddress");

                entity.Property(e => e.RholderCompany)
                    .IsRequired()
                    .HasColumnName("RHolderCompany");

                entity.Property(e => e.RholderCountry)
                    .IsRequired()
                    .HasColumnName("RHolderCountry");

                entity.Property(e => e.RholderCountryCode)
                    .IsRequired()
                    .HasColumnName("RHolderCountryCode")
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.RholderEmail)
                    .IsRequired()
                    .HasColumnName("RHolderEmail")
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.RholderFirstName)
                    .IsRequired()
                    .HasColumnName("RHolderFirstName")
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.RholderJobTitle).HasColumnName("RHolderJobTitle");

                entity.Property(e => e.RholderLastName)
                    .IsRequired()
                    .HasColumnName("RHolderLastName")
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.RholderName)
                    .IsRequired()
                    .HasColumnName("RHolderName");

                entity.Property(e => e.RholderPhoneExtension)
                    .IsRequired()
                    .HasColumnName("RHolderPhoneExtension")
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.RholderPhoneNumber)
                    .IsRequired()
                    .HasColumnName("RHolderPhoneNumber")
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.RholderSecondName).HasColumnName("RHolderSecondName");

                entity.Property(e => e.TranslationLang).IsRequired();

                entity.Property(e => e.TranslationType)
                    .IsRequired()
                    .HasDefaultValueSql("(N'')");
            });

            modelBuilder.Entity<SbasibfBooks>(entity =>
            {
                entity.HasKey(e => e.BookId);

                entity.ToTable("SBASibfBooks");

                entity.HasIndex(e => e.BookId)
                    .HasName("SBASIBFBookIdx")
                    .IsUnique();

                entity.Property(e => e.BookId).ValueGeneratedNever();

                entity.Property(e => e.Isbn).HasColumnName("ISBN");
            });

            modelBuilder.Entity<XsiActivitiesBanner>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiActivitiesSuggestion>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiAdminDashboardIcons>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsShow)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PageUrl).HasColumnName("PageURL");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.XsiAdminDashboardIcons)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_XsiAdminDashboardIcons_XsiAdminRoles");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.XsiAdminDashboardIcons)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_XsiAdminDashboardIcons_XsiAdminUsers");
            });

            modelBuilder.Entity<XsiAdminLogs>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XSI_AdminLogs");

                entity.Property(e => e.Ipaddress)
                    .HasColumnName("IPaddress")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.LoggedOn).HasColumnType("datetime");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.XsiAdminLogs)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_XSI_AdminLogs_XSI_AdminUsers");
            });

            modelBuilder.Entity<XsiAdminPages>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XSI_AdminPages");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiAdminPermissions>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XSI_AdminPermissions");

                entity.Property(e => e.IsCreate)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsDelete)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsRead)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsWrite)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Page)
                    .WithMany(p => p.XsiAdminPermissions)
                    .HasForeignKey(d => d.PageId)
                    .HasConstraintName("FK_XSI_AdminPermissions_XSI_AdminPages");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.XsiAdminPermissions)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_XSI_AdminPermissions_XSI_AdminRoles");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.XsiAdminPermissions)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_XSI_AdminPermissions_XSI_AdminUsers");
            });

            modelBuilder.Entity<XsiAdminRoles>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XSI_AdminRoles");
            });

            modelBuilder.Entity<XsiAdminUsers>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XSI_AdminUsers");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsSuperAdmin)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsTwoFactorEnabled)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.XsiAdminUsers)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XSI_AdminUsers_XSI_AdminRoles");
            });

            modelBuilder.Entity<XsiAdvertisementBanner>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiAdvertisementPage>(entity =>
            {
                entity.HasKey(e => e.ItemId);
            });

            modelBuilder.Entity<XsiAnnouncement>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dated).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiArrivalTerminal>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.XsiArrivalTerminal)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_XsiArrivalTerminal_XsiExhibitionArrivalAirport");
            });

            modelBuilder.Entity<XsiAwardNew>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XsiAwardNew_1");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiAwardNominationRegistrations>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Isbn).HasColumnName("ISBN");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.NominationType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.SubAward)
                    .WithMany(p => p.XsiAwardNominationRegistrations)
                    .HasForeignKey(d => d.SubAwardId)
                    .HasConstraintName("FK_XsiAwardNominationRegistrations_XsiSubAwardNew");
            });

            modelBuilder.Entity<XsiAwardRegistration>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(500);

                entity.Property(e => e.TitleAr).HasMaxLength(500);
            });

            modelBuilder.Entity<XsiAwardWinners>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.AwardsUrl).HasColumnName("AwardsURL");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Exhibition)
                    .WithMany(p => p.XsiAwardWinners)
                    .HasForeignKey(d => d.ExhibitionId)
                    .HasConstraintName("FK_XsiAwardWinners_XsiExhibition");

                entity.HasOne(d => d.NominationForm)
                    .WithMany(p => p.XsiAwardWinners)
                    .HasForeignKey(d => d.NominationFormId)
                    .HasConstraintName("FK_XsiAwardWinners_XsiAwardsNominationForms");
            });

            modelBuilder.Entity<XsiAwards>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiAwardsNominationForms>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Isbn).HasColumnName("ISBN");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.NominationType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Pobox).HasColumnName("POBox");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Award)
                    .WithMany(p => p.XsiAwardsNominationForms)
                    .HasForeignKey(d => d.AwardId)
                    .HasConstraintName("FK_XsiAwardsNominationForms_XsiAwards");

                entity.HasOne(d => d.Exhibition)
                    .WithMany(p => p.XsiAwardsNominationForms)
                    .HasForeignKey(d => d.ExhibitionId)
                    .HasConstraintName("FK_XsiAwardsNominationForms_XsiExhibition");

                entity.HasOne(d => d.SubAward)
                    .WithMany(p => p.XsiAwardsNominationForms)
                    .HasForeignKey(d => d.SubAwardId)
                    .HasConstraintName("FK_XsiAwardsNominationForms_XsiSubAwards");
            });

            modelBuilder.Entity<XsiBookSellerMember>(entity =>
            {
                entity.HasKey(e => e.MemberId);

                entity.Property(e => e.AdminEmailOtp).HasColumnName("AdminEmailOTP");

                entity.Property(e => e.AdminEmailOtpexpiresAt)
                    .HasColumnName("AdminEmailOTPExpiresAt")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EditRequest)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ForgotPasswordExpiresAt).HasColumnType("datetime");

                entity.Property(e => e.IsEmailSent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsLibraryMember)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsSibfdepartment)
                    .HasColumnName("IsSIBFDepartment")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsVerified)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.SibfdepartmentId).HasColumnName("SIBFDepartmentID");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiBookSellerMemberLog>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiBookSellerMemberStatusLog>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XsiBookSellerStatusLog");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.BookSellerMember)
                    .WithMany(p => p.XsiBookSellerMemberStatusLog)
                    .HasForeignKey(d => d.BookSellerMemberId)
                    .HasConstraintName("FK_XsiBookSellerMemberStatusLog_XsiBookSellerMember");
            });

            modelBuilder.Entity<XsiBooksellerEvents>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EventDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiBooksellerMemberEvents>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.XsiBooksellerMemberEvents)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("FK_XsiBooksellerMemberEvents_XsiBooksellerEvents");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.XsiBooksellerMemberEvents)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_XsiBooksellerMemberEvents_XsiBookSellerMember");
            });

            modelBuilder.Entity<XsiCareer>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PostingDate).HasColumnType("datetime");

                entity.HasOne(d => d.DegreeNavigation)
                    .WithMany(p => p.XsiCareer)
                    .HasForeignKey(d => d.Degree)
                    .HasConstraintName("FK_XsiCareer_XsiCareerDegrees");

                entity.HasOne(d => d.DepartmentNavigation)
                    .WithMany(p => p.XsiCareer)
                    .HasForeignKey(d => d.Department)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiCareer_XsiCareerDepartment");

                entity.HasOne(d => d.SubDepartmentNavigation)
                    .WithMany(p => p.XsiCareer)
                    .HasForeignKey(d => d.SubDepartment)
                    .HasConstraintName("FK_XsiCareer_XsiCareerSubDepartment");
            });

            modelBuilder.Entity<XsiCareerApplicants>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Cv).HasColumnName("CV");

                entity.Property(e => e.Gender)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.VisaStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Career)
                    .WithMany(p => p.XsiCareerApplicants)
                    .HasForeignKey(d => d.CareerId)
                    .HasConstraintName("FK_XsiCareerApplicants_XsiCareer");

                entity.HasOne(d => d.DegreeNavigation)
                    .WithMany(p => p.XsiCareerApplicants)
                    .HasForeignKey(d => d.Degree)
                    .HasConstraintName("FK_XsiCareerApplicants_XsiCareerDegrees");

                entity.HasOne(d => d.NationalityNavigation)
                    .WithMany(p => p.XsiCareerApplicantsNationalityNavigation)
                    .HasForeignKey(d => d.Nationality)
                    .HasConstraintName("FK_XsiCareerApplicants_XsiExhibitionCountry");

                entity.HasOne(d => d.ResidenceNavigation)
                    .WithMany(p => p.XsiCareerApplicantsResidenceNavigation)
                    .HasForeignKey(d => d.Residence)
                    .HasConstraintName("FK_XsiCareerApplicants_XsiExhibitionCountry1");
            });

            modelBuilder.Entity<XsiCareerDegrees>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiCareerDepartment>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiCareerSpeciality>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiCareerSubDepartment>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.XsiCareerSubDepartment)
                    .HasForeignKey(d => d.DepartmentId)
                    .HasConstraintName("FK_XsiCareerSubDepartment_XsiCareerDepartment");
            });

            modelBuilder.Entity<XsiCareerSubSpeciality>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Speciality)
                    .WithMany(p => p.XsiCareerSubSpeciality)
                    .HasForeignKey(d => d.SpecialityId)
                    .HasConstraintName("FK_XsiCareerSubSpeciality_XsiCareerSpeciality");
            });

            modelBuilder.Entity<XsiCareerVisaStatus>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiCareerYob>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiCareerYOB");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiCmspageMenu>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiCMSPageMenu");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsExternal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiContactusEnquiry>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateClosed).HasColumnType("datetime");

                entity.Property(e => e.FlagType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsViewed)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.XsiContactusEnquiry)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_XsiContactusEnquiry_XsiExhibitionCountry");
            });

            modelBuilder.Entity<XsiCountry>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiDiscount>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsManagerChecked)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Exhibitor)
                    .WithMany(p => p.XsiDiscount)
                    .HasForeignKey(d => d.ExhibitorId)
                    .HasConstraintName("FK_XsiDiscount_XsiExhibitionMemberApplicationYearly");
            });

            modelBuilder.Entity<XsiDownloadCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiDownloads>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dated).HasColumnType("datetime");

                entity.Property(e => e.DatedAr).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.DownloadCategory)
                    .WithMany(p => p.XsiDownloads)
                    .HasForeignKey(d => d.DownloadCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiDownloads_XsiDownloadCategory");
            });

            modelBuilder.Entity<XsiEmailCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiEmailContent>(entity =>
            {
                entity.HasKey(e => e.ItemId);
            });

            modelBuilder.Entity<XsiEreceipt>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiEReceipt");

                entity.Property(e => e.AmountDueInAed).HasColumnName("AmountDueInAED");

                entity.Property(e => e.AmountDueInUsd).HasColumnName("AmountDueInUSD");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EreceiptNumber).HasColumnName("EReceiptNumber");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsPayLater)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PaymentReceivedInAed).HasColumnName("PaymentReceivedInAED");

                entity.Property(e => e.PaymentReceivedInUsd).HasColumnName("PaymentReceivedInUSD");

                entity.Property(e => e.PaymentType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransferDate).HasColumnType("datetime");

                entity.HasOne(d => d.Exhibitor)
                    .WithMany(p => p.XsiEreceipt)
                    .HasForeignKey(d => d.ExhibitorId)
                    .HasConstraintName("FK_XsiEReceipt_XsiExhibitionMemberApplicationYearly");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.XsiEreceipt)
                    .HasForeignKey(d => d.InvoiceId)
                    .HasConstraintName("FK_XsiEReceipt_XsiInvoice");
            });

            modelBuilder.Entity<XsiEreceiptStatusLogs>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiEReceiptStatusLogs");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiEvent>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.HasIndex(e => e.ItemId)
                    .HasName("eventlistindexnew")
                    .IsUnique();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsFeatured)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ScrfphotoAlbumId).HasColumnName("SCRFPhotoAlbumId");

                entity.Property(e => e.ScrfvideoAlbumId).HasColumnName("SCRFVideoAlbumId");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Type)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.EventSubCategory)
                    .WithMany(p => p.XsiEvent)
                    .HasForeignKey(d => d.EventSubCategoryId)
                    .HasConstraintName("FK_XsiEvent_XsiEventSubCategory");
            });

            modelBuilder.Entity<XsiEventCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiEventDate>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.EventusIdar).HasColumnName("EventusIDAr");

                entity.Property(e => e.EventusIden).HasColumnName("EventusIDEn");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.XsiEventDate)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("FK_XsiEventDate_XsiEvent");
            });

            modelBuilder.Entity<XsiEventGuest>(entity =>
            {
                entity.HasKey(e => e.ItemId);
            });

            modelBuilder.Entity<XsiEventSubCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.EventCategory)
                    .WithMany(p => p.XsiEventSubCategory)
                    .HasForeignKey(d => d.EventCategoryId)
                    .HasConstraintName("FK_XsiEventSubCategory_XsiEventCategory");
            });

            modelBuilder.Entity<XsiEventType>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiEventWebsite>(entity =>
            {
                entity.HasKey(e => e.ItemId);
            });

            modelBuilder.Entity<XsiExhibition>(entity =>
            {
                entity.HasKey(e => e.ExhibitionId);

                entity.Property(e => e.AgencyEndDate).HasColumnType("datetime");

                entity.Property(e => e.AgencyStartDate).HasColumnType("datetime");

                entity.Property(e => e.AudioAwardEndDate).HasColumnType("datetime");

                entity.Property(e => e.AudioAwardStartDate).HasColumnType("datetime");

                entity.Property(e => e.AwardEndDate).HasColumnType("datetime");

                entity.Property(e => e.AwardStartDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateOfCacellation).HasColumnType("datetime");

                entity.Property(e => e.DateOfDue).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ExhibitorEndDate).HasColumnType("datetime");

                entity.Property(e => e.ExhibitorStartDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsArchive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.RepresentativeEndDate).HasColumnType("datetime");

                entity.Property(e => e.RepresentativeStartDate).HasColumnType("datetime");

                entity.Property(e => e.RestaurantEndDate).HasColumnType("datetime");

                entity.Property(e => e.RestaurantStartDate).HasColumnType("datetime");

                entity.Property(e => e.StaffGuestBookingExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.StaffGuestEndDate).HasColumnType("datetime");

                entity.Property(e => e.StaffGuestStartDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.TurjumanEndDate).HasColumnType("datetime");

                entity.Property(e => e.TurjumanStartDate).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");

                entity.HasOne(d => d.Website)
                    .WithMany(p => p.XsiExhibition)
                    .HasForeignKey(d => d.WebsiteId)
                    .HasConstraintName("FK_XsiExhibition_XsiWebsites");
            });

            modelBuilder.Entity<XsiExhibitionActivity>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionActivityYearly>(entity =>
            {
                entity.HasKey(e => new { e.ExhibitionId, e.ExhibitionActivityId });
            });

            modelBuilder.Entity<XsiExhibitionAgencyDetails>(entity =>
            {
                entity.HasKey(e => e.AgencyDetailsId);

                entity.HasIndex(e => e.AgencyDetailsId)
                    .HasName("AgencyDetailsIdx")
                    .IsUnique();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsIncludedInInvoice)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsRegisteredByExhibitor)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.MemberExhibitionYearly)
                    .WithMany(p => p.XsiExhibitionAgencyDetails)
                    .HasForeignKey(d => d.MemberExhibitionYearlyId)
                    .HasConstraintName("FK_XsiExhibitionAgencyDetails_XsiExhibitionMemberApplicationYearly");
            });

            modelBuilder.Entity<XsiExhibitionApplication>(entity =>
            {
                entity.HasKey(e => e.ApplicationId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveForScrf)
                    .HasColumnName("IsActiveForSCRF")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveForSibf)
                    .HasColumnName("IsActiveForSIBF")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionArea>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionArrivalAirport>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionAuthor>(entity =>
            {
                entity.HasKey(e => e.AuthorId);

                entity.HasIndex(e => e.AuthorId)
                    .HasName("AuthorIdx")
                    .IsUnique();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionBookAuthor>(entity =>
            {
                entity.HasKey(e => new { e.BookId, e.AuthorId });
            });

            modelBuilder.Entity<XsiExhibitionBookForUi>(entity =>
            {
                entity.HasKey(e => e.BookId);

                entity.ToTable("XsiExhibitionBookForUI");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EventusIdar).HasColumnName("EventusIDAr");

                entity.Property(e => e.EventusIden).HasColumnName("EventusIDEn");

                entity.Property(e => e.IsNew)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Isbn).HasColumnName("ISBN");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionBookParticipating>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.HasIndex(e => e.ItemId)
                    .HasName("BookParticipatingIdx")
                    .IsUnique();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Agency)
                    .WithMany(p => p.XsiExhibitionBookParticipatingAgency)
                    .HasForeignKey(d => d.AgencyId)
                    .HasConstraintName("FK_XsiExhibitionBookParticipating_XsiExhibitionMemberApplicationYearly1");

                entity.HasOne(d => d.Book)
                    .WithMany(p => p.XsiExhibitionBookParticipating)
                    .HasForeignKey(d => d.BookId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiExhibitionBookParticipating_XsiExhibitionBooks");

                entity.HasOne(d => d.Exhibitor)
                    .WithMany(p => p.XsiExhibitionBookParticipatingExhibitor)
                    .HasForeignKey(d => d.ExhibitorId)
                    .HasConstraintName("FK_XsiExhibitionBookParticipating_XsiExhibitionMemberApplicationYearly");
            });

            modelBuilder.Entity<XsiExhibitionBookPublisher>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionBookSellerGenre>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionBookSellerProgram>(entity =>
            {
                entity.HasKey(e => e.BookSellerProgramId);

                entity.Property(e => e.AppointmentEndDate).HasColumnType("datetime");

                entity.Property(e => e.AppointmentStartDate).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeOneDayOneEnd).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeOneDayOneStart).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeOneDayThreeEnd).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeOneDayThreeStart).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeOneDayTwoEnd).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeOneDayTwoStart).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeTwoDayOneEnd).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeTwoDayOneStart).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeTwoDayThreeEnd).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeTwoDayThreeStart).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeTwoDayTwoEnd).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeTwoDayTwoStart).HasColumnType("datetime");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDateDayOne).HasColumnType("datetime");

                entity.Property(e => e.EndDateDayThree).HasColumnType("datetime");

                entity.Property(e => e.EndDateDayTwo).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsArchive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsArchiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageUrl).HasColumnName("LanguageURL");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.RegistrationEndDate).HasColumnType("datetime");

                entity.Property(e => e.RegistrationStartDate).HasColumnType("datetime");

                entity.Property(e => e.StartDateDayOne).HasColumnType("datetime");

                entity.Property(e => e.StartDateDayThree).HasColumnType("datetime");

                entity.Property(e => e.StartDateDayTwo).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionBookSellerProgramRegistration>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.BooksUrl).HasColumnName("BooksURL");

                entity.Property(e => e.CatalogUrl).HasColumnName("CatalogURL");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAttended)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAttendedTg).HasColumnName("IsAttendedTG");

                entity.Property(e => e.IsCheckedIn)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEbooks)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEditRequest)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsHopitalityPackage)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsInterested)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsRequestDisplayBooth)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.NoofPendingTg).HasColumnName("NoofPendingTG");

                entity.Property(e => e.NoofPublishedTg).HasColumnName("NoofPublishedTG");

                entity.Property(e => e.ProfessionType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PublisherType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Qrcode).HasColumnName("QRCode");

                entity.Property(e => e.SocialLinkUrl).HasColumnName("SocialLinkURL");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TgreasonForNotAttend).HasColumnName("TGReasonForNotAttend");

                entity.Property(e => e.Title)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TradeType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.BookSellerProgram)
                    .WithMany(p => p.XsiExhibitionBookSellerProgramRegistration)
                    .HasForeignKey(d => d.BookSellerProgramId)
                    .HasConstraintName("FK_XsiExhibitionBookSellerProgramRegistration_XsiExhibitionBookSellerProgram");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.XsiExhibitionBookSellerProgramRegistration)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_XsiExhibitionBookSellerProgramRegistration_XsiBookSellerMember");
            });

            modelBuilder.Entity<XsiExhibitionBookSellerProgramRegistrationStatusLogs>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Registration)
                    .WithMany(p => p.XsiExhibitionBookSellerProgramRegistrationStatusLogs)
                    .HasForeignKey(d => d.RegistrationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiExhibitionBookSellerProgramRegistrationStatusLogs_XsiExhibitionBookSellerProgramRegistration");
            });

            modelBuilder.Entity<XsiExhibitionBookSellerProgramSlots>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.IsBreak)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsDelete)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StartTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionBookSellerProgramSource>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionBookSellerProgramUserBook>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XsiExhibitionBookSellerProgramBook");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsOwnRights)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsRightsAvailable)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.BookSellerRegistration)
                    .WithMany(p => p.XsiExhibitionBookSellerProgramUserBook)
                    .HasForeignKey(d => d.BookSellerRegistrationId)
                    .HasConstraintName("FK_XsiExhibitionBookSellerProgramUserBook_XsiExhibitionBookSellerProgramRegistration");
            });

            modelBuilder.Entity<XsiExhibitionBookSellerSlotInvitation>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.InviteUrl).HasColumnName("InviteURL");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsInviteUrlclicked)
                    .HasColumnName("IsInviteURLClicked")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsViewed)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionBookSellerStaffGuest>(entity =>
            {
                entity.HasKey(e => e.StaffGuestId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEmailSent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.XsiExhibitionBookSellerStaffGuest)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_XsiExhibitionBookSellerStaffGuest_XsiBookSellerMember");
            });

            modelBuilder.Entity<XsiExhibitionBookSellerStaffGuestBookingDetails>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ArrivalDate).HasColumnType("datetime");

                entity.Property(e => e.ClassSeat)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ClassSeatAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.FlightCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FlightStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.GuestCategory)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HotelBookingEndDate).HasColumnType("datetime");

                entity.Property(e => e.HotelBookingStartDate).HasColumnType("datetime");

                entity.Property(e => e.HotelCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.HotelEndDate).HasColumnType("datetime");

                entity.Property(e => e.HotelStartDate).HasColumnType("datetime");

                entity.Property(e => e.HotelStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsDirectOrTransitFlight)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsIndividualOrGroup)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsPassportModified)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ReturnClassSeat)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReturnClassSeatAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReturnDate).HasColumnType("datetime");

                entity.Property(e => e.ReturnFlightStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReturnTerminal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RoomType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RoomTypeAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Terminal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransportCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.TransportEndDate).HasColumnType("datetime");

                entity.Property(e => e.TransportStartDate).HasColumnType("datetime");

                entity.Property(e => e.TransportationStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransportationType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransportationTypeAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionBookSellerStaffGuestParticipating>(entity =>
            {
                entity.HasKey(e => new { e.StaffGuestId, e.ExhibitionId });

                entity.Property(e => e.ActivityDate).HasColumnType("datetime");

                entity.Property(e => e.ArrivalDate).HasColumnType("datetime");

                entity.Property(e => e.CountryUidno).HasColumnName("CountryUIDNo");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateOfIssue).HasColumnType("datetime");

                entity.Property(e => e.DepartureDate).HasColumnType("datetime");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsCompanion)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEmailSent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsFromPcregistration)
                    .HasColumnName("IsFromPCRegistration")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsTravelDetails)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsUaeresident)
                    .HasColumnName("IsUAEResident")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsVisitedUae)
                    .HasColumnName("IsVisitedUAE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.NeedVisa)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PaidVisa)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RejectedStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Uidno).HasColumnName("UIDNo");

                entity.Property(e => e.VisaType)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionBookSellerStaffGuestParticipatingStatusLogs>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionBookSellerUserBooksLanguage>(entity =>
            {
                entity.HasKey(e => e.ItemId);
            });

            modelBuilder.Entity<XsiExhibitionBookSellerUserGenre>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.HasOne(d => d.BookSellerRegistration)
                    .WithMany(p => p.XsiExhibitionBookSellerUserGenre)
                    .HasForeignKey(d => d.BookSellerRegistrationId)
                    .HasConstraintName("FK_XsiExhibitionBookSellerUserGenre_XsiExhibitionBookSellerProgramRegistration");
            });

            modelBuilder.Entity<XsiExhibitionBookSellerUserProgram>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.PpprogramId).HasColumnName("PPProgramId");

                entity.HasOne(d => d.BookSellerRegistration)
                    .WithMany(p => p.XsiExhibitionBookSellerUserProgram)
                    .HasForeignKey(d => d.BookSellerRegistrationId)
                    .HasConstraintName("FK_XsiExhibitionBookSellerUserProgram_XsiExhibitionBookSellerProgramRegistration");
            });

            modelBuilder.Entity<XsiExhibitionBookSubject>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.HasIndex(e => e.ItemId)
                    .HasName("SubjectIdx")
                    .IsUnique();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionBookSubsubject>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.HasIndex(e => e.ItemId)
                    .HasName("SubSubjectIdx")
                    .IsUnique();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.XsiExhibitionBookSubsubject)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_XsiExhibitionBookSubsubject_XsiExhibitionBookSubject");
            });

            modelBuilder.Entity<XsiExhibitionBookType>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionBooks>(entity =>
            {
                entity.HasKey(e => e.BookId);

                entity.HasIndex(e => e.BookId)
                    .HasName("BookIdx")
                    .IsUnique();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAvailableOnline)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsBestSeller)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEnable)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsNew)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Isbn).HasColumnName("ISBN");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.BookLanguage)
                    .WithMany(p => p.XsiExhibitionBooks)
                    .HasForeignKey(d => d.BookLanguageId)
                    .HasConstraintName("FK_XsiExhibitionBooks_XsiExhibitionLanguage");

                entity.HasOne(d => d.BookPublisher)
                    .WithMany(p => p.XsiExhibitionBooks)
                    .HasForeignKey(d => d.BookPublisherId)
                    .HasConstraintName("FK_XsiExhibitionBooks_XsiExhibitionBookPublisher");

                entity.HasOne(d => d.BookType)
                    .WithMany(p => p.XsiExhibitionBooks)
                    .HasForeignKey(d => d.BookTypeId)
                    .HasConstraintName("FK_XsiExhibitionBooks_XsiExhibitionBookType");

                entity.HasOne(d => d.ExhibitionSubject)
                    .WithMany(p => p.XsiExhibitionBooks)
                    .HasForeignKey(d => d.ExhibitionSubjectId)
                    .HasConstraintName("FK_XsiExhibitionBooks_XsiExhibitionBookSubject");

                entity.HasOne(d => d.ExhibitionSubsubject)
                    .WithMany(p => p.XsiExhibitionBooks)
                    .HasForeignKey(d => d.ExhibitionSubsubjectId)
                    .HasConstraintName("FK_XsiExhibitionBooks_XsiExhibitionBookSubsubject1");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.XsiExhibitionBooks)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_XsiExhibitionBooks_XsiExhibitionMember");
            });

            modelBuilder.Entity<XsiExhibitionBookseller>(entity =>
            {
                entity.HasKey(e => e.ExhibitionId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateOfDue).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsArchive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StaffGuestBookingExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.StaffGuestEndDate).HasColumnType("datetime");

                entity.Property(e => e.StaffGuestStartDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionBooth>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionBoothSubsection>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.XsiExhibitionBoothSubsection)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_XsiExhibitionBoothSubsection_XsiExhibitionBooth");
            });

            modelBuilder.Entity<XsiExhibitionCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionCateringSystemRegistration>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CateringType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.EndTimeMeridiain)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.EventType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.StartTimeMeridiain)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionCity>(entity =>
            {
                entity.HasKey(e => e.CityId);

                entity.HasIndex(e => e.CityId)
                    .HasName("ExhibitionCityIdx")
                    .IsUnique();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.XsiExhibitionCity)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiExhibitionCity_XsiExhibitionCountry");
            });

            modelBuilder.Entity<XsiExhibitionCountry>(entity =>
            {
                entity.HasKey(e => e.CountryId);

                entity.HasIndex(e => e.CountryId)
                    .HasName("ExhibitionCountryIdx")
                    .IsUnique();

                entity.Property(e => e.CountryCodeIsotwo).HasColumnName("CountryCodeISOTwo");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsVisaRequired)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionCountryAirport>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.XsiExhibitionCountryAirport)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiExhibitionCountryAirport_XsiExhibitionCountry");
            });

            modelBuilder.Entity<XsiExhibitionCurrency>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionDate>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionDays>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ExhibitionDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Exhibition)
                    .WithMany(p => p.XsiExhibitionDays)
                    .HasForeignKey(d => d.ExhibitionId)
                    .HasConstraintName("FK_XsiExhibitionDays_XsiExhibition");
            });

            modelBuilder.Entity<XsiExhibitionEventtus>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionExhibitorDetails>(entity =>
            {
                entity.HasKey(e => e.ExhibitorDetailsId);

                entity.HasIndex(e => e.ExhibitorDetailsId)
                    .HasName("ExhibitorDetailsIdx")
                    .IsUnique();

                entity.Property(e => e.AllocatedAreaType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Apu)
                    .HasColumnName("APU")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsInternalOrExternal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.RequiredAreaType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.BoothSection)
                    .WithMany(p => p.XsiExhibitionExhibitorDetails)
                    .HasForeignKey(d => d.BoothSectionId)
                    .HasConstraintName("FK_XsiExhibitionExhibitorDetails_XsiExhibitionBooth");

                entity.HasOne(d => d.BoothSubSection)
                    .WithMany(p => p.XsiExhibitionExhibitorDetails)
                    .HasForeignKey(d => d.BoothSubSectionId)
                    .HasConstraintName("FK_XsiExhibitionExhibitorDetails_XsiExhibitionBoothSubsection");

                entity.HasOne(d => d.MemberExhibitionYearly)
                    .WithMany(p => p.XsiExhibitionExhibitorDetails)
                    .HasForeignKey(d => d.MemberExhibitionYearlyId)
                    .HasConstraintName("FK_XsiExhibitionExhibitorDetails_XsiExhibitionMemberApplicationYearly");
            });

            modelBuilder.Entity<XsiExhibitionFinesCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionFurnitureItemOrderDetails>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedBy).HasColumnType("datetime");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Total).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.FurnitureItem)
                    .WithMany(p => p.XsiExhibitionFurnitureItemOrderDetails)
                    .HasForeignKey(d => d.FurnitureItemId)
                    .HasConstraintName("FK_XsiExhibitionFurnitureItemOrderDetails_XsiExhibitionFurnitureItems");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.XsiExhibitionFurnitureItemOrderDetails)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK_XsiExhibitionFurnitureItemOrderDetails_XsiExhibitionFurnitureItemOrders");
            });

            modelBuilder.Entity<XsiExhibitionFurnitureItemOrders>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Discount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.DiscountPercent).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PaidAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Penalty).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PenaltyPercent).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SubTotal).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Total).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TransferDate).HasColumnType("datetime");

                entity.Property(e => e.Vat).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.Exhibitor)
                    .WithMany(p => p.XsiExhibitionFurnitureItemOrders)
                    .HasForeignKey(d => d.ExhibitorId)
                    .HasConstraintName("FK_XsiExhibitionFurnitureItemOrders_XsiExhibitionMemberApplicationYearly");
            });

            modelBuilder.Entity<XsiExhibitionFurnitureItemStatusLogs>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XsiExhibitionFurntiureItemStatusLogs");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Exhibitor)
                    .WithMany(p => p.XsiExhibitionFurnitureItemStatusLogs)
                    .HasForeignKey(d => d.ExhibitorId)
                    .HasConstraintName("FK_XsiExhibitionFurnitureItemStatusLogs_XsiExhibitionMemberApplicationYearly");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.XsiExhibitionFurnitureItemStatusLogs)
                    .HasForeignKey(d => d.InvoiceId)
                    .HasConstraintName("FK_XsiExhibitionFurnitureItemStatusLogs_XsiExhibitionFurnitureItemOrders");
            });

            modelBuilder.Entity<XsiExhibitionFurnitureItems>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XsiExhibitionFurniture");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<XsiExhibitionLanguage>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionMember>(entity =>
            {
                entity.HasKey(e => e.MemberId);

                entity.HasIndex(e => e.MemberId)
                    .HasName("MemeberIdx")
                    .IsUnique();

                entity.Property(e => e.AdminEmailOtp).HasColumnName("AdminEmailOTP");

                entity.Property(e => e.AdminEmailOtpexpiresAt)
                    .HasColumnName("AdminEmailOTPExpiresAt")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EditRequest)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ForgotPasswordExpiresAt).HasColumnType("datetime");

                entity.Property(e => e.IsActivateForTg).HasColumnName("IsActivateForTG");

                entity.Property(e => e.IsBooksRequired)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEmailSent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsLibraryMember)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsNewPasswordChange)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsPcorTg).HasColumnName("IsPCOrTG");

                entity.Property(e => e.IsSibfdepartment)
                    .HasColumnName("IsSIBFDepartment")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsVerified)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageUrl).HasColumnName("LanguageURL");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Pobox).HasColumnName("POBox");

                entity.Property(e => e.SibfdepartmentId).HasColumnName("SIBFDepartmentID");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.XsiExhibitionMember)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK_XsiExhibitionMember_XsiExhibitionCity");
            });

            modelBuilder.Entity<XsiExhibitionMemberApplicationYearly>(entity =>
            {
                entity.HasKey(e => e.MemberExhibitionYearlyId);

                entity.HasIndex(e => e.MemberExhibitionYearlyId)
                    .HasName("ExhibitorIdx")
                    .IsUnique();

                entity.Property(e => e.ContractCms).HasColumnName("ContractCMS");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAllowApprove)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsArabicPublisher)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsBooksUpload)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEditRequest)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEditRequestEmailSent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsFirstTime)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsIncludedInInvoice)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsInternalOrExternal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsNotesRead)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsRegisteredByExhibitor)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsSampleReceived)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsStatusChanged)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageUrl).HasColumnName("LanguageURL");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.OldItemId).HasColumnName("OldItemID");

                entity.Property(e => e.RevenueTokenCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.SampleReceivedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TempId)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Trn).HasColumnName("TRN");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.XsiExhibitionMemberApplicationYearly)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_XsiExhibitionMemberApplicationYearly_XsiExhibitionCountry");

                entity.HasOne(d => d.ExhibitionCategory)
                    .WithMany(p => p.XsiExhibitionMemberApplicationYearly)
                    .HasForeignKey(d => d.ExhibitionCategoryId)
                    .HasConstraintName("FK_XsiExhibitionMemberApplicationYearly_XsiExhibitionCategory");

                entity.HasOne(d => d.Exhibition)
                    .WithMany(p => p.XsiExhibitionMemberApplicationYearly)
                    .HasForeignKey(d => d.ExhibitionId)
                    .HasConstraintName("FK_XsiExhibitionMemberApplicationYearly_XsiExhibition");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.XsiExhibitionMemberApplicationYearly)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_XsiExhibitionMemberApplicationYearly_XsiExhibitionMember");

                entity.HasOne(d => d.MemberRole)
                    .WithMany(p => p.XsiExhibitionMemberApplicationYearly)
                    .HasForeignKey(d => d.MemberRoleId)
                    .HasConstraintName("FK_XsiExhibitionMemberApplicationYearly_XsiExhibitionApplication");
            });

            modelBuilder.Entity<XsiExhibitionMemberApplicationYearlyLogs>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionMemberApplicationYearlyPreApproved>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionMemberLog>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionMemberStatusLog>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionMemberandExhibitorFines>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.FineCategory)
                    .WithMany(p => p.XsiExhibitionMemberandExhibitorFines)
                    .HasForeignKey(d => d.FineCategoryId)
                    .HasConstraintName("FK_XsiExhibitionMemberandExhibitorFines_XsiExhibitionFinesCategory");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.XsiExhibitionMemberandExhibitorFines)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_XsiExhibitionMemberandExhibitorFines_XsiExhibitionMember");
            });

            modelBuilder.Entity<XsiExhibitionOtherEvents>(entity =>
            {
                entity.HasKey(e => e.ExhibitionId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsArchive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StaffGuestBookingExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.StaffGuestEndDate).HasColumnType("datetime");

                entity.Property(e => e.StaffGuestStartDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiExhibitionOtherEventsEmailContent>(entity =>
            {
                entity.HasKey(e => e.ItemId);
            });

            modelBuilder.Entity<XsiExhibitionOtherEventsStaffGuest>(entity =>
            {
                entity.HasKey(e => e.StaffGuestId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEmailSent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.XsiExhibitionOtherEventsStaffGuest)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_XsiExhibitionOtherEventsStaffGuest_XsiExhibitionMember");
            });

            modelBuilder.Entity<XsiExhibitionOtherEventsStaffGuestBookingDetails>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ArrivalDate).HasColumnType("datetime");

                entity.Property(e => e.ClassSeat)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ClassSeatAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.FlightCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FlightStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.GuestCategory)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HotelBookingEndDate).HasColumnType("datetime");

                entity.Property(e => e.HotelBookingStartDate).HasColumnType("datetime");

                entity.Property(e => e.HotelCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.HotelEndDate).HasColumnType("datetime");

                entity.Property(e => e.HotelStartDate).HasColumnType("datetime");

                entity.Property(e => e.HotelStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsDirectOrTransitFlight)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsIndividualOrGroup)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsPassportModified)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ReturnClassSeat)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReturnClassSeatAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReturnDate).HasColumnType("datetime");

                entity.Property(e => e.ReturnFlightStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReturnTerminal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RoomType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RoomTypeAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Terminal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransportCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.TransportEndDate).HasColumnType("datetime");

                entity.Property(e => e.TransportStartDate).HasColumnType("datetime");

                entity.Property(e => e.TransportationStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransportationType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransportationTypeAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Exhibition)
                    .WithMany(p => p.XsiExhibitionOtherEventsStaffGuestBookingDetails)
                    .HasForeignKey(d => d.ExhibitionId)
                    .HasConstraintName("FK_XsiExhibitionOtherEventsStaffGuestBookingDetails_XsiExhibitionOtherEventsStaffGuest");
            });

            modelBuilder.Entity<XsiExhibitionOtherEventsStaffGuestParticipating>(entity =>
            {
                entity.HasKey(e => new { e.StaffGuestId, e.ExhibitionId });

                entity.Property(e => e.ActivityDate).HasColumnType("datetime");

                entity.Property(e => e.ArrivalDate).HasColumnType("datetime");

                entity.Property(e => e.CountryUidno).HasColumnName("CountryUIDNo");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateOfIssue).HasColumnType("datetime");

                entity.Property(e => e.DepartureDate).HasColumnType("datetime");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsCompanion)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEmailSent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsFromPcregistration)
                    .HasColumnName("IsFromPCRegistration")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsTravelDetails)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsUaeresident)
                    .HasColumnName("IsUAEResident")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsVisitedUae)
                    .HasColumnName("IsVisitedUAE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.NeedVisa)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PaidVisa)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RejectedStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Uidno).HasColumnName("UIDNo");

                entity.Property(e => e.VisaType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.XsiExhibitionOtherEventsStaffGuestParticipating)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_XsiExhibitionOtherEventsStaffGuestParticipating_XsiExhibitionCountry");
            });

            modelBuilder.Entity<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionPosdevice>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiExhibitionPOSDevice");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EmiratedIdnumber).HasColumnName("EmiratedIDNumber");

                entity.Property(e => e.Ibannumber).HasColumnName("IBANNumber");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsLocal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageUrl).HasColumnName("LanguageURL");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PosdeviceQauntity).HasColumnName("POSDeviceQauntity");

                entity.HasOne(d => d.Exhibitor)
                    .WithMany(p => p.XsiExhibitionPosdevice)
                    .HasForeignKey(d => d.ExhibitorId)
                    .HasConstraintName("FK_XsiExhibitionPOSDevice_XsiExhibitionMemberApplicationYearly");

                entity.HasOne(d => d.PublisherOriginCountryNavigation)
                    .WithMany(p => p.XsiExhibitionPosdevicePublisherOriginCountryNavigation)
                    .HasForeignKey(d => d.PublisherOriginCountry)
                    .HasConstraintName("FK_XsiExhibitionPOSDevice_XsiExhibitionCountry1");

                entity.HasOne(d => d.RecipientNationalityNavigation)
                    .WithMany(p => p.XsiExhibitionPosdeviceRecipientNationalityNavigation)
                    .HasForeignKey(d => d.RecipientNationality)
                    .HasConstraintName("FK_XsiExhibitionPOSDevice_XsiExhibitionCountry");
            });

            modelBuilder.Entity<XsiExhibitionPpbooksLanguage>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiExhibitionPPBooksLanguage");

                entity.Property(e => e.PpbookId).HasColumnName("PPBookId");
            });

            modelBuilder.Entity<XsiExhibitionPpgenre>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiExhibitionPPGenre");
            });

            modelBuilder.Entity<XsiExhibitionPpprogram>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiExhibitionPPProgram");

                entity.Property(e => e.PpprogramId).HasColumnName("PPProgramId");
            });

            modelBuilder.Entity<XsiExhibitionPpslotInvitation>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiExhibitionPPSlotInvitation");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.InviteUrl).HasColumnName("InviteURL");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsInviteUrlclicked)
                    .HasColumnName("IsInviteURLClicked")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsViewed)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionProfessionalProgram>(entity =>
            {
                entity.HasKey(e => e.ProfessionalProgramId);

                entity.Property(e => e.AppointmentEndDate).HasColumnType("datetime");

                entity.Property(e => e.AppointmentStartDate).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeOneDayOneEnd).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeOneDayOneStart).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeOneDayThreeEnd).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeOneDayThreeStart).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeOneDayTwoEnd).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeOneDayTwoStart).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeTwoDayOneEnd).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeTwoDayOneStart).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeTwoDayThreeEnd).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeTwoDayThreeStart).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeTwoDayTwoEnd).HasColumnType("datetime");

                entity.Property(e => e.BreakTimeTwoDayTwoStart).HasColumnType("datetime");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDateDayOne).HasColumnType("datetime");

                entity.Property(e => e.EndDateDayThree).HasColumnType("datetime");

                entity.Property(e => e.EndDateDayTwo).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsArchive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsArchiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageUrl).HasColumnName("LanguageURL");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.RegistrationEndDate).HasColumnType("datetime");

                entity.Property(e => e.RegistrationStartDate).HasColumnType("datetime");

                entity.Property(e => e.StartDateDayOne).HasColumnType("datetime");

                entity.Property(e => e.StartDateDayThree).HasColumnType("datetime");

                entity.Property(e => e.StartDateDayTwo).HasColumnType("datetime");

                entity.HasOne(d => d.Exhibition)
                    .WithMany(p => p.XsiExhibitionProfessionalProgram)
                    .HasForeignKey(d => d.ExhibitionId)
                    .HasConstraintName("FK_XsiExhibitionProfessionalProgram_XsiExhibition");
            });

            modelBuilder.Entity<XsiExhibitionProfessionalProgramBook>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsOwnRights)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsRightsAvailable)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionProfessionalProgramRegistration>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.BooksUrl).HasColumnName("BooksURL");

                entity.Property(e => e.CatalogUrl).HasColumnName("CatalogURL");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAttended)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAttendedTg).HasColumnName("IsAttendedTG");

                entity.Property(e => e.IsCheckedIn)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEbooks)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEditRequest)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsHopitalityPackage)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsInterested)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.NoofPendingTg).HasColumnName("NoofPendingTG");

                entity.Property(e => e.NoofPublishedTg).HasColumnName("NoofPublishedTG");

                entity.Property(e => e.Qrcode).HasColumnName("QRCode");

                entity.Property(e => e.SocialLinkUrl).HasColumnName("SocialLinkURL");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TgreasonForNotAttend).HasColumnName("TGReasonForNotAttend");

                entity.Property(e => e.Title)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TradeType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.XsiExhibitionProfessionalProgramRegistration)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK_XsiExhibitionProfessionalProgramRegistration_XsiExhibitionCity");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.XsiExhibitionProfessionalProgramRegistration)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_XsiExhibitionProfessionalProgramRegistration_XsiExhibitionCountry");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.XsiExhibitionProfessionalProgramRegistration)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_XsiExhibitionProfessionalProgramRegistration_XsiExhibitionMember");

                entity.HasOne(d => d.ProfessionalProgram)
                    .WithMany(p => p.XsiExhibitionProfessionalProgramRegistration)
                    .HasForeignKey(d => d.ProfessionalProgramId)
                    .HasConstraintName("FK_XsiExhibitionProfessionalProgramRegistration_XsiExhibitionProfessionalProgram");
            });

            modelBuilder.Entity<XsiExhibitionProfessionalProgramRegistrationStatusLogs>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionProfessionalProgramSlots>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.IsBreak)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsDelete)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.HasOne(d => d.ProfessionalProgram)
                    .WithMany(p => p.XsiExhibitionProfessionalProgramSlots)
                    .HasForeignKey(d => d.ProfessionalProgramId)
                    .HasConstraintName("FK_XsiExhibitionProfessionalProgramSlots_XsiExhibitionProfessionalProgram");
            });

            modelBuilder.Entity<XsiExhibitionProfessionalProgramSource>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionRegion>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionRepresentative>(entity =>
            {
                entity.HasKey(e => e.RepresentativeId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEmailSent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.XsiExhibitionRepresentative)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_XsiExhibitionRepresentative_XsiExhibitionMember");
            });

            modelBuilder.Entity<XsiExhibitionRepresentativeParticipating>(entity =>
            {
                entity.HasKey(e => new { e.RepresentativeId, e.MemberExhibitionYearlyId });

                entity.Property(e => e.ActivityDate).HasColumnType("datetime");

                entity.Property(e => e.ArrivalDate).HasColumnType("datetime");

                entity.Property(e => e.CountryUidno).HasColumnName("CountryUIDNo");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateOfIssue).HasColumnType("datetime");

                entity.Property(e => e.DepartureDate).HasColumnType("datetime");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAddHealthInsurance)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsCompanion)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEmailSent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsTravelDetails)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsUaeresident)
                    .HasColumnName("IsUAEResident")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsVisitedUae)
                    .HasColumnName("IsVisitedUAE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.NeedVisa)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PaidVisa)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RejectedStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Uidno).HasColumnName("UIDNo");

                entity.Property(e => e.VisaType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.ArrivalAirport)
                    .WithMany(p => p.XsiExhibitionRepresentativeParticipating)
                    .HasForeignKey(d => d.ArrivalAirportId)
                    .HasConstraintName("FK_XsiExhibitionRepresentativeParticipating_XsiExhibitionArrivalAirport");

                entity.HasOne(d => d.ArrivalTerminal)
                    .WithMany(p => p.XsiExhibitionRepresentativeParticipating)
                    .HasForeignKey(d => d.ArrivalTerminalId)
                    .HasConstraintName("FK_XsiExhibitionRepresentativeParticipating_XsiArrivalTerminal");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.XsiExhibitionRepresentativeParticipating)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_XsiExhibitionRepresentativeParticipating_XsiExhibitionCountry");

                entity.HasOne(d => d.MemberExhibitionYearly)
                    .WithMany(p => p.XsiExhibitionRepresentativeParticipating)
                    .HasForeignKey(d => d.MemberExhibitionYearlyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiExhibitionRepresentativeParticipating_XsiExhibitionMemberApplicationYearly");

                entity.HasOne(d => d.Representative)
                    .WithMany(p => p.XsiExhibitionRepresentativeParticipating)
                    .HasForeignKey(d => d.RepresentativeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiExhibitionRepresentativeParticipating_XsiExhibitionRepresentative");
            });

            modelBuilder.Entity<XsiExhibitionRepresentativeParticipatingNoVisa>(entity =>
            {
                entity.HasKey(e => new { e.RepresentativeId, e.MemberExhibitionYearlyId })
                    .HasName("PK_XsiExhibitionNoVisaRepresentative");

                entity.Property(e => e.ActivityDate).HasColumnType("datetime");

                entity.Property(e => e.ArrivalDate).HasColumnType("datetime");

                entity.Property(e => e.CountryUidno).HasColumnName("CountryUIDNo");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateOfIssue).HasColumnType("datetime");

                entity.Property(e => e.DepartureDate).HasColumnType("datetime");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAddHealthInsurance)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsCompanion)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEmailSent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsTravelDetails)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsUaeresident)
                    .HasColumnName("IsUAEResident")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsVisitedUae)
                    .HasColumnName("IsVisitedUAE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.NeedVisa)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PaidVisa)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RejectedStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Uidno).HasColumnName("UIDNo");

                entity.Property(e => e.VisaType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.ArrivalAirport)
                    .WithMany(p => p.XsiExhibitionRepresentativeParticipatingNoVisa)
                    .HasForeignKey(d => d.ArrivalAirportId)
                    .HasConstraintName("FK_XsiExhibitionRepresentativeParticipatingNoVisa_XsiExhibitionArrivalAirport");

                entity.HasOne(d => d.ArrivalTerminal)
                    .WithMany(p => p.XsiExhibitionRepresentativeParticipatingNoVisa)
                    .HasForeignKey(d => d.ArrivalTerminalId)
                    .HasConstraintName("FK_XsiExhibitionRepresentativeParticipatingNoVisa_XsiArrivalTerminal");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.XsiExhibitionRepresentativeParticipatingNoVisa)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_XsiExhibitionRepresentativeParticipatingNoVisa_XsiExhibitionCountry");

                entity.HasOne(d => d.MemberExhibitionYearly)
                    .WithMany(p => p.XsiExhibitionRepresentativeParticipatingNoVisa)
                    .HasForeignKey(d => d.MemberExhibitionYearlyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiExhibitionRepresentativeParticipatingNoVisa_XsiExhibitionMemberApplicationYearly");

                entity.HasOne(d => d.Representative)
                    .WithMany(p => p.XsiExhibitionRepresentativeParticipatingNoVisa)
                    .HasForeignKey(d => d.RepresentativeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiExhibitionRepresentativeParticipatingNoVisa_XsiExhibitionRepresentative");
            });

            modelBuilder.Entity<XsiExhibitionRepresentativeParticipatingNoVisaStatusLogs>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionRepresentativeParticipatingStatusLogs>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionShipment>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAgency)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsLocalWarehouse)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageUrl).HasColumnName("LanguageURL");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.QrfileName).HasColumnName("QRFileName");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.WbooksFile).HasColumnName("WBooksFile");

                entity.Property(e => e.WnoofBooks).HasColumnName("WNoofBooks");

                entity.Property(e => e.WshipmentCompanyName).HasColumnName("WShipmentCompanyName");

                entity.Property(e => e.WshipmentCount).HasColumnName("WShipmentCount");

                entity.Property(e => e.WshipmentNumber).HasColumnName("WShipmentNumber");

                entity.HasOne(d => d.Exhibitor)
                    .WithMany(p => p.XsiExhibitionShipment)
                    .HasForeignKey(d => d.ExhibitorId)
                    .HasConstraintName("FK_XsiExhibitionShipment_XsiExhibitionMemberApplicationYearly");

                entity.HasOne(d => d.ShipmentCountryFromNavigation)
                    .WithMany(p => p.XsiExhibitionShipmentShipmentCountryFromNavigation)
                    .HasForeignKey(d => d.ShipmentCountryFrom)
                    .HasConstraintName("FK_XsiExhibitionShipment_XsiExhibitionCountry1");

                entity.HasOne(d => d.ShipmentCountryToNavigation)
                    .WithMany(p => p.XsiExhibitionShipmentShipmentCountryToNavigation)
                    .HasForeignKey(d => d.ShipmentCountryTo)
                    .HasConstraintName("FK_XsiExhibitionShipment_XsiExhibitionCountry2");
            });

            modelBuilder.Entity<XsiExhibitionShipmentAgency>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.HasOne(d => d.ExhibitionShipment)
                    .WithMany(p => p.XsiExhibitionShipmentAgency)
                    .HasForeignKey(d => d.ExhibitionShipmentId)
                    .HasConstraintName("FK_XsiExhibitionShipmentAgency_XsiExhibitionShipment");
            });

            modelBuilder.Entity<XsiExhibitionStaffGuest>(entity =>
            {
                entity.HasKey(e => e.StaffGuestId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEmailSent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.XsiExhibitionStaffGuest)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_XsiExhibitionStaffGuest_XsiExhibitionMember");
            });

            modelBuilder.Entity<XsiExhibitionStaffGuestBookingDetails>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ArrivalDate).HasColumnType("datetime");

                entity.Property(e => e.ClassSeat)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ClassSeatAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.FlightCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FlightStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.GuestCategory)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HotelBookingEndDate).HasColumnType("datetime");

                entity.Property(e => e.HotelBookingStartDate).HasColumnType("datetime");

                entity.Property(e => e.HotelCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.HotelEndDate).HasColumnType("datetime");

                entity.Property(e => e.HotelStartDate).HasColumnType("datetime");

                entity.Property(e => e.HotelStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsDirectOrTransitFlight)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsIndividualOrGroup)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsPassportModified)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ReturnClassSeat)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReturnClassSeatAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReturnDate).HasColumnType("datetime");

                entity.Property(e => e.ReturnFlightStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ReturnTerminal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RoomType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RoomTypeAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Terminal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransportCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.TransportEndDate).HasColumnType("datetime");

                entity.Property(e => e.TransportStartDate).HasColumnType("datetime");

                entity.Property(e => e.TransportationStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransportationType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransportationTypeAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionStaffGuestFlightDetails>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XsiStaffGuestFlightDetails");

                entity.Property(e => e.ArrivalDate).HasColumnType("datetime");

                entity.Property(e => e.ClassSeat)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ClassSeatAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.FlightStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.GuestCategory)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsPassportModified)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ReturnDate).HasColumnType("datetime");

                entity.Property(e => e.ServiceType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Terminal)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionStaffGuestHotelDetails>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XsiStaffGuestHotelDetails");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.HotelBookingEndDate).HasColumnType("datetime");

                entity.Property(e => e.HotelBookingStartDate).HasColumnType("datetime");

                entity.Property(e => e.HotelEndDate).HasColumnType("datetime");

                entity.Property(e => e.HotelStartDate).HasColumnType("datetime");

                entity.Property(e => e.HotelStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.RoomType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RoomTypeAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionStaffGuestParticipating>(entity =>
            {
                entity.HasKey(e => new { e.StaffGuestId, e.ExhibitionId });

                entity.Property(e => e.ActivityDate).HasColumnType("datetime");

                entity.Property(e => e.ArrivalDate).HasColumnType("datetime");

                entity.Property(e => e.CountryUidno).HasColumnName("CountryUIDNo");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateOfIssue).HasColumnType("datetime");

                entity.Property(e => e.DepartureDate).HasColumnType("datetime");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsCompanion)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEmailSent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsFromPcregistration)
                    .HasColumnName("IsFromPCRegistration")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsTravelDetails)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsUaeresident)
                    .HasColumnName("IsUAEResident")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsVisitedUae)
                    .HasColumnName("IsVisitedUAE")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.NeedVisa)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PaidVisa)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.RejectedStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Uidno).HasColumnName("UIDNo");

                entity.Property(e => e.VisaType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.ArrivalAirport)
                    .WithMany(p => p.XsiExhibitionStaffGuestParticipating)
                    .HasForeignKey(d => d.ArrivalAirportId)
                    .HasConstraintName("FK_XsiExhibitionStaffGuestParticipating_XsiExhibitionArrivalAirport");

                entity.HasOne(d => d.ArrivalTerminal)
                    .WithMany(p => p.XsiExhibitionStaffGuestParticipating)
                    .HasForeignKey(d => d.ArrivalTerminalId)
                    .HasConstraintName("FK_XsiExhibitionStaffGuestParticipating_XsiArrivalTerminal");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.XsiExhibitionStaffGuestParticipating)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_XsiExhibitionStaffGuestParticipating_XsiExhibitionCountry");

                entity.HasOne(d => d.Exhibition)
                    .WithMany(p => p.XsiExhibitionStaffGuestParticipating)
                    .HasForeignKey(d => d.ExhibitionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiExhibitionStaffGuestParticipating_XsiExhibition");

                entity.HasOne(d => d.StaffGuest)
                    .WithMany(p => p.XsiExhibitionStaffGuestParticipating)
                    .HasForeignKey(d => d.StaffGuestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiExhibitionStaffGuestParticipating_XsiExhibitionStaffGuest");
            });

            modelBuilder.Entity<XsiExhibitionStaffGuestParticipatingStatusLogs>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionStaffGuestTransportDetails>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XsiStaffGuestTransportDetails");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.TransportEndDate).HasColumnType("datetime");

                entity.Property(e => e.TransportStartDate).HasColumnType("datetime");

                entity.Property(e => e.TransportationStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransportationType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransportationTypeAllotted)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionTranslationGrant>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsArchive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsArchiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Phase1EndDate).HasColumnType("datetime");

                entity.Property(e => e.Phase1StartDate).HasColumnType("datetime");

                entity.Property(e => e.Phase2EndDate).HasColumnType("datetime");

                entity.Property(e => e.Phase2StartDate).HasColumnType("datetime");

                entity.Property(e => e.Phase3EndDate).HasColumnType("datetime");

                entity.Property(e => e.Phase3StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Exhibition)
                    .WithMany(p => p.XsiExhibitionTranslationGrant)
                    .HasForeignKey(d => d.ExhibitionId)
                    .HasConstraintName("FK_XsiExhibitionTranslationGrant_XsiExhibition");
            });

            modelBuilder.Entity<XsiExhibitionTranslationGrantBookWinner>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.TranslationGrantRegistration)
                    .WithMany(p => p.XsiExhibitionTranslationGrantBookWinner)
                    .HasForeignKey(d => d.TranslationGrantRegistrationId)
                    .HasConstraintName("FK_XsiExhibitionTranslationGrantBookWinner_XsiExhibitionTranslationGrantMembers");
            });

            modelBuilder.Entity<XsiExhibitionTranslationGrantCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiExhibitionTranslationGrantMemberStatusLog>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiExhibitionTranslationGrantMembers>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateofShipment).HasColumnType("datetime");

                entity.Property(e => e.FinalPaymentDate).HasColumnType("datetime");

                entity.Property(e => e.FirstPaymentDate).HasColumnType("datetime");

                entity.Property(e => e.Ibanno).HasColumnName("IBANNo");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsHardCopy)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsIbanorBoth)
                    .HasColumnName("IsIBANorBoth")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Isbn).HasColumnName("ISBN");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.OriginalBookStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SecondPaymentDate).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TranslationType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.OriginalLanguage)
                    .WithMany(p => p.XsiExhibitionTranslationGrantMembersOriginalLanguage)
                    .HasForeignKey(d => d.OriginalLanguageId)
                    .HasConstraintName("FK_XsiExhibitionTranslationGrantMembers_XsiExhibitionLanguage");

                entity.HasOne(d => d.ProposedLanguage)
                    .WithMany(p => p.XsiExhibitionTranslationGrantMembersProposedLanguage)
                    .HasForeignKey(d => d.ProposedLanguageId)
                    .HasConstraintName("FK_XsiExhibitionTranslationGrantMembers_XsiExhibitionLanguage1");

                entity.HasOne(d => d.TranslationGrant)
                    .WithMany(p => p.XsiExhibitionTranslationGrantMembers)
                    .HasForeignKey(d => d.TranslationGrantId)
                    .HasConstraintName("FK_XsiExhibitionTranslationGrantMembers_XsiExhibitionTranslationGrant");
            });

            modelBuilder.Entity<XsiExhibitor>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiExhibitorBadges>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsEmailSent)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiFlags>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.FlagModuleIdentifier)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FlagType)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiGenre>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiGuestOfHonor>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsCentreBanner)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsCentreBannerAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiGuestWebsite>(entity =>
            {
                entity.HasKey(e => e.ItemId);
            });

            modelBuilder.Entity<XsiGuests>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.HasIndex(e => new { e.GuestName, e.GuestTitle, e.GuestNameAr, e.GuestTitleAr, e.ItemId })
                    .HasName("guestlistindexnew")
                    .IsUnique();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.GuestName).HasColumnType("nvarchar(max)");

                entity.Property(e => e.GuestNameAr).HasColumnType("nvarchar(max)");

                entity.Property(e => e.GuestTitle).HasColumnType("nvarchar(max)");

                entity.Property(e => e.GuestTitleAr).HasColumnType("nvarchar(max)");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiHamzatWasl>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dated).HasColumnType("datetime");

                entity.Property(e => e.DatedAr).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiHomePageLink>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsExternal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiHomepageBanner>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.Cmstitle).HasColumnName("CMSTitle");

                entity.Property(e => e.CmstitleAr).HasColumnName("CMSTitleAr");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsFeatured)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiHomepageBannerNew>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.Cmstitle).HasColumnName("CMSTitle");

                entity.Property(e => e.CmstitleAr).HasColumnName("CMSTitleAr");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsFeatured)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiHomepageOrganiser>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiHomepageSectionFour>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.SubSectionOneUrl).HasColumnName("SubSectionOneURL");

                entity.Property(e => e.SubSectionOneUrlar).HasColumnName("SubSectionOneURLAr");

                entity.Property(e => e.SubSectionThreeUrl).HasColumnName("SubSectionThreeURL");

                entity.Property(e => e.SubSectionThreeUrlar).HasColumnName("SubSectionThreeURLAr");

                entity.Property(e => e.SubSectionTwoUrl).HasColumnName("SubSectionTwoURL");

                entity.Property(e => e.SubSectionTwoUrlar).HasColumnName("SubSectionTwoURLAr");

                entity.Property(e => e.ViewAllUrl).HasColumnName("ViewAllURL");

                entity.Property(e => e.ViewAllUrlar).HasColumnName("ViewAllURLAr");
            });

            modelBuilder.Entity<XsiHomepageSectionTwo>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.XsiHomepageSectionTwo)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_XsiHomepageSectionTwo_XsiHomepageSectionTwoCategory");
            });

            modelBuilder.Entity<XsiHomepageSectionTwoCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiHomepageSubBanner>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.Cmstitle).HasColumnName("CMSTitle");

                entity.Property(e => e.CmstitleAr).HasColumnName("CMSTitleAr");

                entity.Property(e => e.CmstitleAr2).HasColumnName("CMSTitleAr2");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive2)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr2)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsFeatured)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsFeatured2)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Url2).HasColumnName("URL2");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");

                entity.Property(e => e.Urlar2).HasColumnName("URLAr2");
            });

            modelBuilder.Entity<XsiIconsForSponsor>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiInvoice>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ExemptionType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsChequeReceived)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsNewPending)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsPaymentInAed)
                    .HasColumnName("IsPaymentInAED")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsRegistrationInvoice)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsStatusChanged)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PaidAmountUsd).HasColumnName("PaidAmountUSD");

                entity.Property(e => e.Pgstatus).HasColumnName("PGStatus");

                entity.Property(e => e.PgtransactionId).HasColumnName("PGTransactionID");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SubTotalAed).HasColumnName("SubTotalAED");

                entity.Property(e => e.SubTotalUsd).HasColumnName("SubTotalUSD");

                entity.Property(e => e.TotalAed).HasColumnName("TotalAED");

                entity.Property(e => e.TotalUsd).HasColumnName("TotalUSD");

                entity.Property(e => e.TransferDate).HasColumnType("datetime");

                entity.Property(e => e.Vat).HasColumnName("VAT");

                entity.HasOne(d => d.Exhibitor)
                    .WithMany(p => p.XsiInvoice)
                    .HasForeignKey(d => d.ExhibitorId)
                    .HasConstraintName("FK_XsiInvoice_XsiExhibitionMemberApplicationYearly");
            });

            modelBuilder.Entity<XsiInvoiceStatusLog>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiLanguages>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsDefault)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiMemberExhibitionActivityYearly>(entity =>
            {
                entity.HasKey(e => new { e.MemberExhibitionYearlyId, e.ActivityId });
            });

            modelBuilder.Entity<XsiMemberReason>(entity =>
            {
                entity.HasKey(e => e.MemberReasonId);

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.XsiMemberReason)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_XsiMemberReason_XsiExhibitionMember");

                entity.HasOne(d => d.Reason)
                    .WithMany(p => p.XsiMemberReason)
                    .HasForeignKey(d => d.ReasonId)
                    .HasConstraintName("FK_XsiMemberReason_XsiReason");
            });

            modelBuilder.Entity<XsiNews>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dated).HasColumnType("datetime");

                entity.Property(e => e.DatedAr).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.XsiNews)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiNews_XsiNewsCategory");
            });

            modelBuilder.Entity<XsiNewsCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiNewsPhotoGallery>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.News)
                    .WithMany(p => p.XsiNewsPhotoGallery)
                    .HasForeignKey(d => d.NewsId)
                    .HasConstraintName("FK_XsiNewsPhotoGallery_XsiNews");
            });

            modelBuilder.Entity<XsiNewsletter>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dated).HasColumnType("datetime");

                entity.Property(e => e.DatedAr).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiNewsletterBanner>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiNewsletterSubscribers>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsSubscribe)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.XsiNewsletterSubscribers)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_XsiNewsletterSubscribers_XsiExhibitionCountry");
            });

            modelBuilder.Entity<XsiOurteamDepartment>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiOurteamStaff>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.XsiOurteamStaff)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_XsiOurteamStaff_XsiOurteamDepartment");
            });

            modelBuilder.Entity<XsiPageMenuNew>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsExternal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsTitleImagePrimary)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsTitleImagePrimaryAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.PageContent)
                    .WithMany(p => p.XsiPageMenuNew)
                    .HasForeignKey(d => d.PageContentId)
                    .HasConstraintName("FK_XsiPageMenuNew_XsiPagesContentNew");
            });

            modelBuilder.Entity<XsiPagesContentNew>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsDynamic)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiPcawardDetails>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiPCAwardDetails");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.RegistrationEndDate).HasColumnType("datetime");

                entity.Property(e => e.RegistrationStartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Award)
                    .WithMany(p => p.XsiPcawardDetails)
                    .HasForeignKey(d => d.AwardId)
                    .HasConstraintName("FK_XsiPCAwardDetails_XsiPCAwards");
            });

            modelBuilder.Entity<XsiPcawardNominationForms>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiPCAwardNominationForms");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.AwardDetail)
                    .WithMany(p => p.XsiPcawardNominationForms)
                    .HasForeignKey(d => d.AwardDetailId)
                    .HasConstraintName("FK_XsiPCAwardNominationForms_XsiPCAwardDetails");
            });

            modelBuilder.Entity<XsiPcawards>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiPCAwards");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiPcevents>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiPCEvents");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EventDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiPcmemberEvents>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiPCMemberEvents");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.XsiPcmemberEvents)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("FK_XsiPCMemberEvents_XsiPCEvents");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.XsiPcmemberEvents)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_XsiPCMemberEvents_XsiExhibitionMember");
            });

            modelBuilder.Entity<XsiPhoto>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAlbumCover)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAlbumCoverAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.PhotoAlbum)
                    .WithMany(p => p.XsiPhoto)
                    .HasForeignKey(d => d.PhotoAlbumId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_XsiPhoto_XsiPhotoAlbum");
            });

            modelBuilder.Entity<XsiPhotoAlbum>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dated).HasColumnType("datetime");

                entity.Property(e => e.DatedAr).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.SubCategory)
                    .WithMany(p => p.XsiPhotoAlbum)
                    .HasForeignKey(d => d.SubCategoryId)
                    .HasConstraintName("FK_XsiPhotoAlbum_XsiPhotoSubCategory");
            });

            modelBuilder.Entity<XsiPhotoCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiPhotoSubCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.XsiPhotoSubCategory)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_XsiPhotoSubCategory_XsiPhotoCategory");
            });

            modelBuilder.Entity<XsiProfessionalProgramSource>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiPublisherNews>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dated).HasColumnType("datetime");

                entity.Property(e => e.DatedAr).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.PublisherNewsCategory)
                    .WithMany(p => p.XsiPublisherNews)
                    .HasForeignKey(d => d.PublisherNewsCategoryId)
                    .HasConstraintName("FK_XsiPublisherNews_XsiPublisherNewsCategory");
            });

            modelBuilder.Entity<XsiPublisherNewsCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiPublisherNewsPhotoGallery>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.News)
                    .WithMany(p => p.XsiPublisherNewsPhotoGallery)
                    .HasForeignKey(d => d.NewsId)
                    .HasConstraintName("FK_XsiPublisherNewsPhotoGallery_XsiPublisherNews");
            });

            modelBuilder.Entity<XsiPublisherWeekly>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.Category)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CategoryAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PdffileName).HasColumnName("PDFFileName");

                entity.Property(e => e.PdffileNameAr).HasColumnName("PDFFileNameAr");

                entity.Property(e => e.PublisherDate).HasColumnType("datetime");

                entity.Property(e => e.PublisherDateAr).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiQuickLinks>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsExternal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiReason>(entity =>
            {
                entity.HasKey(e => e.ReasonId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiRepresentativeAgreement>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiSchoolRegistration>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateOfVisit).HasColumnType("datetime");

                entity.Property(e => e.ModifedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Exhibition)
                    .WithMany(p => p.XsiSchoolRegistration)
                    .HasForeignKey(d => d.ExhibitionId)
                    .HasConstraintName("FK_XsiSchoolRegistration_XsiExhibition");
            });

            modelBuilder.Entity<XsiScrfactivitiesBanner>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFActivitiesBanner");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiScrfadvertisementBanner>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFAdvertisementBanner");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiScrfadvertisementPage>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFAdvertisementPage");
            });

            modelBuilder.Entity<XsiScrfannouncement>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFAnnouncement");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dated).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiScrfawardNominationForm>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFAwardNominationForm");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Isbn).HasColumnName("ISBN");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Pobox).HasColumnName("POBox");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiScrfcity>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFCity");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.XsiScrfcity)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_XsiSCRFCity_XsiSCRFCountry");
            });

            modelBuilder.Entity<XsiScrfcontactusEnquiry>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFContactusEnquiry");

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateClosed).HasColumnType("datetime");

                entity.Property(e => e.FlagType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsViewed)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiScrfcountry>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFCountry");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiScrfemailContent>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFEmailContent");
            });

            modelBuilder.Entity<XsiScrffestivalAdvertisementArea>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XsiFestivalAdvertisementArea");

                entity.ToTable("XsiSCRFFestivalAdvertisementArea");

                entity.Property(e => e.Cmstitle).HasColumnName("CMSTitle");

                entity.Property(e => e.CmstitleAr).HasColumnName("CMSTitleAr");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsFeatured)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiScrfhomepageBanner>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XsiSCRFHomeBanner");

                entity.ToTable("XsiSCRFHomepageBanner");

                entity.Property(e => e.Cmstitle).HasColumnName("CMSTitle");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsFeatured)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiScrfhomepageSection>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFHomepageSection");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiScrfhomepageSubBanner>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFHomepageSubBanner");

                entity.Property(e => e.Cmstitle).HasColumnName("CMSTitle");

                entity.Property(e => e.CmstitleAr).HasColumnName("CMSTitleAr");

                entity.Property(e => e.CmstitleAr2).HasColumnName("CMSTitleAr2");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive2)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr2)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsFeatured)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsFeatured2)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Url2).HasColumnName("URL2");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");

                entity.Property(e => e.Urlar2).HasColumnName("URLAr2");
            });

            modelBuilder.Entity<XsiScrficonsForSponsor>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFIconsForSponsor");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiScrfillustrationNotification>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFIllustrationNotification");

                entity.HasOne(d => d.Illustration)
                    .WithMany(p => p.XsiScrfillustrationNotification)
                    .HasForeignKey(d => d.IllustrationId)
                    .HasConstraintName("FK_XsiSCRFIllustrationNotification_XsiSCRFIllustrator");
            });

            modelBuilder.Entity<XsiScrfillustrationNotificationStatus>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFIllustrationNotificationStatus");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.IllustrationNotification)
                    .WithMany(p => p.XsiScrfillustrationNotificationStatus)
                    .HasForeignKey(d => d.IllustrationNotificationId)
                    .HasConstraintName("FK_XsiSCRFIllustrationNotificationStatus_XsiSCRFIllustrationNotification");

                entity.HasOne(d => d.Illustrator)
                    .WithMany(p => p.XsiScrfillustrationNotificationStatus)
                    .HasForeignKey(d => d.IllustratorId)
                    .HasConstraintName("FK_XsiSCRFIllustrationNotificationStatus_XsiSCRFIllustrationRegistration");
            });

            modelBuilder.Entity<XsiScrfillustrationRegistration>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFIllustrationRegistration");

                entity.Property(e => e.AdminEmailOtpexpiresAt).HasColumnType("datetime");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Cv).HasColumnName("CV");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.ForgotPasswordExpiresAt).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Qrcode).HasColumnName("QRCode");

                entity.Property(e => e.Qrrank).HasColumnName("QRRank");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.XsiScrfillustrationRegistration)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK_XsiSCRFIllustrationRegistration_XsiExhibitionCity");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.XsiScrfillustrationRegistrationCountry)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_XsiSCRFIllustrationRegistration_XsiExhibitionCountry1");

                entity.HasOne(d => d.Illustration)
                    .WithMany(p => p.XsiScrfillustrationRegistration)
                    .HasForeignKey(d => d.IllustrationId)
                    .HasConstraintName("FK_XsiSCRFIllustrationRegistration_XsiSCRFIllustrator");

                entity.HasOne(d => d.Nationality)
                    .WithMany(p => p.XsiScrfillustrationRegistrationNationality)
                    .HasForeignKey(d => d.NationalityId)
                    .HasConstraintName("FK_XsiSCRFIllustrationRegistration_XsiExhibitionCountry");
            });

            modelBuilder.Entity<XsiScrfillustrationRegistrationLog>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFIllustrationRegistrationLog");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiScrfillustrationRegistrationStatusLogs>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFIllustrationRegistrationStatusLogs");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiScrfillustrationSubmission>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFIllustrationSubmission");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IllustrationPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.InsuranceFee).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsForSale)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsOriginalOrPrinted)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsPublished)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsReturn)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Rating).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Illustrator)
                    .WithMany(p => p.XsiScrfillustrationSubmission)
                    .HasForeignKey(d => d.IllustratorId)
                    .HasConstraintName("FK_XsiSCRFIllustrationSubmission_XsiSCRFIllustrationRegistration");
            });

            modelBuilder.Entity<XsiScrfillustrationSubmissionComments>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFIllustrationSubmissionComments");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.IllustrationSubmission)
                    .WithMany(p => p.XsiScrfillustrationSubmissionComments)
                    .HasForeignKey(d => d.IllustrationSubmissionId)
                    .HasConstraintName("FK_XsiSCRFIllustrationSubmissionComments_XsiSCRFIllustrationSubmission");
            });

            modelBuilder.Entity<XsiScrfillustrationSubmissionStatusLogs>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFIllustrationSubmissionStatusLogs");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiScrfillustrator>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFIllustrator");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Deadline).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.HardCopyDeadline).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsArchive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiScrfillustratorJudge>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFIllustratorJudge");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiScrfpageMenu>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFPageMenu");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsExternal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsTitleImagePrimary)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsTitleImagePrimaryAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiScrfpagesContent>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFPagesContent");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsDynamic)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsDynamicAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XsiScrfphoto>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFPhoto");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAlbumCover)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAlbumCoverAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Album)
                    .WithMany(p => p.XsiScrfphoto)
                    .HasForeignKey(d => d.AlbumId)
                    .HasConstraintName("FK_XsiSCRFPhoto_XsiSCRFPhotoAlbum");
            });

            modelBuilder.Entity<XsiScrfphotoAlbum>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFPhotoAlbum");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dated).HasColumnType("datetime");

                entity.Property(e => e.DatedAr).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.SubCategory)
                    .WithMany(p => p.XsiScrfphotoAlbum)
                    .HasForeignKey(d => d.SubCategoryId)
                    .HasConstraintName("FK_XsiSCRFPhotoAlbum_XsiSCRFPhotoSubCategory");
            });

            modelBuilder.Entity<XsiScrfphotoCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFPhotoCategory");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiScrfphotoSubCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFPhotoSubCategory");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.XsiScrfphotoSubCategory)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_XsiSCRFPhotoSubCategory_XsiSCRFPhotoCategory");
            });

            modelBuilder.Entity<XsiScrfpoetryAwardCandidateNominationNew>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFPoetryAwardCandidateNominationNew");

                entity.Property(e => e.CategoryType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.PoetryAwardNomination)
                    .WithMany(p => p.XsiScrfpoetryAwardCandidateNominationNew)
                    .HasForeignKey(d => d.PoetryAwardNominationId)
                    .HasConstraintName("FK_XsiSCRFPoetryAwardCandidateNominationNew_XsiSCRFPoetryAwardNominationNew");
            });

            modelBuilder.Entity<XsiScrfpoetryAwardNominationNew>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFPoetryAwardNominationNew");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Exhibition)
                    .WithMany(p => p.XsiScrfpoetryAwardNominationNew)
                    .HasForeignKey(d => d.ExhibitionId)
                    .HasConstraintName("FK_XsiSCRFPoetryAwardNominationNew_XsiExhibition");
            });

            modelBuilder.Entity<XsiScrfpressRelease>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFPressRelease");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dated).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiScrfquickLinks>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFQuickLinks");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsExternal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiScrftechniqueMaterial>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFTechniqueMaterial");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiScrfvideo>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFVideo");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dated).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAlbumCover)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAlbumCoverAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");

                entity.HasOne(d => d.Album)
                    .WithMany(p => p.XsiScrfvideo)
                    .HasForeignKey(d => d.AlbumId)
                    .HasConstraintName("FK_XsiSCRFVideo_XsiSCRFVideoAlbum");
            });

            modelBuilder.Entity<XsiScrfvideoAlbum>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK_XsiSCRFVideoAlbums");

                entity.ToTable("XsiSCRFVideoAlbum");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dated).HasColumnType("datetime");

                entity.Property(e => e.DatedAr).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.XsiScrfvideoAlbum)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_XsiSCRFVideoAlbum_XsiSCRFVideoCategory");
            });

            modelBuilder.Entity<XsiScrfvideoCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFVideoCategory");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiScrfwinners>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSCRFWinners");

                entity.Property(e => e.AwardsUrl).HasColumnName("AwardsURL");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiSgbdto>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSGBDto");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiSharjahAwardForTranslation>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ArabPublishHousePobox).HasColumnName("ArabPublishHousePOBox");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Pobox).HasColumnName("POBox");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.ArabPublishHouseCountry)
                    .WithMany(p => p.XsiSharjahAwardForTranslationArabPublishHouseCountry)
                    .HasForeignKey(d => d.ArabPublishHouseCountryId)
                    .HasConstraintName("FK_XsiSharjahAwardForTranslation_XsiExhibitionCountry1");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.XsiSharjahAwardForTranslationCountry)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_XsiSharjahAwardForTranslation_XsiExhibitionCountry");

                entity.HasOne(d => d.Exhibition)
                    .WithMany(p => p.XsiSharjahAwardForTranslation)
                    .HasForeignKey(d => d.ExhibitionId)
                    .HasConstraintName("FK_XsiSharjahAwardForTranslation_XsiExhibition");
            });

            modelBuilder.Entity<XsiSibfcrawler>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiSIBFCrawler");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiStaffGuestCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiStaffGuestType>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiSubAwardNew>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Award)
                    .WithMany(p => p.XsiSubAwardNew)
                    .HasForeignKey(d => d.AwardId)
                    .HasConstraintName("FK_XsiSubAwardNew_XsiAwardNew");
            });

            modelBuilder.Entity<XsiSubAwards>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Award)
                    .WithMany(p => p.XsiSubAwards)
                    .HasForeignKey(d => d.AwardId)
                    .HasConstraintName("FK_XsiSubAwards_XsiAwards");
            });

            modelBuilder.Entity<XsiVideo>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAlbumCover)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsAlbumCoverAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");

                entity.HasOne(d => d.VideoAlbum)
                    .WithMany(p => p.XsiVideo)
                    .HasForeignKey(d => d.VideoAlbumId)
                    .HasConstraintName("FK_XsiVideo_XsiVideoAlbum");
            });

            modelBuilder.Entity<XsiVideoAlbum>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dated).HasColumnType("datetime");

                entity.Property(e => e.DatedAr).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.VideoCategory)
                    .WithMany(p => p.XsiVideoAlbum)
                    .HasForeignKey(d => d.VideoCategoryId)
                    .HasConstraintName("FK_XsiVideoAlbum_XsiVideoCategory");
            });

            modelBuilder.Entity<XsiVideoCategory>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiVipguest>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiVIPGuest");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<XsiVipguestWebsite>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("XsiVIPGuestWebsite");

                entity.Property(e => e.VipguestId).HasColumnName("VIPGuestId");
            });

            modelBuilder.Entity<XsiVisitor>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveAr)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Url).HasColumnName("URL");

                entity.Property(e => e.Urlar).HasColumnName("URLAr");
            });

            modelBuilder.Entity<XsiVolunteer>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Cv).HasColumnName("CV");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmiratesIddocument).HasColumnName("EmiratesIDDocument");

                entity.Property(e => e.EmiratesIddocumentTwo).HasColumnName("EmiratesIDDocumentTwo");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.Gender)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.IsUid)
                    .HasColumnName("IsUID")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageUrl).HasColumnName("LanguageURL");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.UniversityNameOrUid).HasColumnName("UniversityNameOrUID");
            });

            modelBuilder.Entity<XsiVolunteerExhibitionDays>(entity =>
            {
                entity.HasKey(e => new { e.VolunteerPerioIdId, e.ExhibitionDaysId });
            });

            modelBuilder.Entity<XsiVolunteerPeriod>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.HasOne(d => d.Volunteer)
                    .WithMany(p => p.XsiVolunteerPeriod)
                    .HasForeignKey(d => d.VolunteerId)
                    .HasConstraintName("FK_XsiVolunteerPeriod_XsiVolunteer");
            });

            modelBuilder.Entity<XsiWebsites>(entity =>
            {
                entity.HasKey(e => e.WebsiteId);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Query<ExhibitorAgencySIBF>().ToView("View_ExhibitorAgencySIBF");
        }
    }
}

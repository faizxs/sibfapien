﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiPageMenuNew
    {
        public long ItemId { get; set; }
        public long? ParentId { get; set; }
        public string Title { get; set; }
        public string TitleImage { get; set; }
        public string IsTitleImagePrimary { get; set; }
        public string PageUrl { get; set; }
        public string TitleAr { get; set; }
        public string TitleImageAr { get; set; }
        public string IsTitleImagePrimaryAr { get; set; }
        public string PageUrlAr { get; set; }
        public long? PageContentId { get; set; }
        public long? SortOrder { get; set; }
        public string IsExternal { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }

        public virtual XsiPagesContentNew PageContent { get; set; }
    }
}

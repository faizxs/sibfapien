﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBookseller
    {
        public long ExhibitionId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string IsArchive { get; set; }
        public long? WebsiteId { get; set; }
        public long? ExhibitionYear { get; set; }
        public long? ExhibitionNumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? StaffGuestStartDate { get; set; }
        public DateTime? StaffGuestEndDate { get; set; }
        public DateTime? StaffGuestBookingExpiryDate { get; set; }
        public DateTime? DateOfDue { get; set; }
        public long? ExpiryMonths { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

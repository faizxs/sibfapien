﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiHomepageSectionFour
    {
        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string ViewAllUrl { get; set; }
        public string SubSectionOneTitle { get; set; }
        public string SubSectionOneOverview { get; set; }
        public string SubSectionOneImage { get; set; }
        public string SubSectionOneUrl { get; set; }
        public string SubSectionTwoTitle { get; set; }
        public string SubSectionTwoOverview { get; set; }
        public string SubSectionTwoImage { get; set; }
        public string SubSectionTwoUrl { get; set; }
        public string SubSectionThreeTitle { get; set; }
        public string SubSectionThreeOverview { get; set; }
        public string SubSectionThreeImage { get; set; }
        public string SubSectionThreeUrl { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public string ViewAllUrlar { get; set; }
        public string SubSectionOneTitleAr { get; set; }
        public string SubSectionOneOverviewAr { get; set; }
        public string SubSectionOneImageAr { get; set; }
        public string SubSectionOneUrlar { get; set; }
        public string SubSectionTwoTitleAr { get; set; }
        public string SubSectionTwoOverviewAr { get; set; }
        public string SubSectionTwoImageAr { get; set; }
        public string SubSectionTwoUrlar { get; set; }
        public string SubSectionThreeTitleAr { get; set; }
        public string SubSectionThreeOverviewAr { get; set; }
        public string SubSectionThreeImageAr { get; set; }
        public string SubSectionThreeUrlar { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

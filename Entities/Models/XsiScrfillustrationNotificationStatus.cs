﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiScrfillustrationNotificationStatus
    {
        public long ItemId { get; set; }
        public long? IllustratorId { get; set; }
        public long? IllustrationNotificationId { get; set; }
        public string Status { get; set; }

        public virtual XsiScrfillustrationNotification IllustrationNotification { get; set; }
        public virtual XsiScrfillustrationRegistration Illustrator { get; set; }
    }
}

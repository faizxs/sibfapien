﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionFurnitureItemOrderDetails
    {
        public long ItemId { get; set; }
        public long? OrderId { get; set; }
        public long? FurnitureItemId { get; set; }
        public long? Quantity { get; set; }
        public decimal? Price { get; set; }
        public decimal? Total { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionFurnitureItems FurnitureItem { get; set; }
        public virtual XsiExhibitionFurnitureItemOrders Order { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionMemberApplicationYearlyPreApproved
    {
        public long ItemId { get; set; }
        public long MemberExhibitionYearlyId { get; set; }
        public long? MemberId { get; set; }
        public long? MemberRoleId { get; set; }
        public string PublisherNameEn { get; set; }
        public string PublisherNameAr { get; set; }
        public string FileNumber { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBookSellerProgramRegistration
    {
        public XsiExhibitionBookSellerProgramRegistration()
        {
            XsiExhibitionBookSellerProgramRegistrationStatusLogs = new HashSet<XsiExhibitionBookSellerProgramRegistrationStatusLogs>();
            XsiExhibitionBookSellerProgramUserBook = new HashSet<XsiExhibitionBookSellerProgramUserBook>();
            XsiExhibitionBookSellerUserGenre = new HashSet<XsiExhibitionBookSellerUserGenre>();
            XsiExhibitionBookSellerUserProgram = new HashSet<XsiExhibitionBookSellerUserProgram>();
        }

        public long ItemId { get; set; }
        public long? MemberId { get; set; }
        public long? BookSellerProgramId { get; set; }
        public long? TableId { get; set; }
        public string IsActive { get; set; }
        public string Status { get; set; }
        public string ProfessionType { get; set; }
        public string PublisherType { get; set; }
        public long? LanguageId { get; set; }
        public long? CountryId { get; set; }
        public long? CityId { get; set; }
        public string OtherCity { get; set; }
        public long? StepId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FirstNameAr { get; set; }
        public string LastNameAr { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string CompanyName { get; set; }
        public string CompanyNameAr { get; set; }
        public string JobTitle { get; set; }
        public string JobTitleAr { get; set; }
        public string WorkProfile { get; set; }
        public string WorkProfileAr { get; set; }
        public string Website { get; set; }
        public long? GenreId { get; set; }
        public string OtherGenre { get; set; }
        public string TradeType { get; set; }
        public string IsEbooks { get; set; }
        public string ExamplesOfTranslatedBooks { get; set; }
        public string IsInterested { get; set; }
        public string InterestedInfo { get; set; }
        public string FileName { get; set; }
        public string FileName1 { get; set; }
        public string IsAttended { get; set; }
        public string HowManyTimes { get; set; }
        public string BooksUrl { get; set; }
        public string ZipCode { get; set; }
        public string ReasonToAttend { get; set; }
        public string SocialLinkUrl { get; set; }
        public string ReasonToAttendAgain { get; set; }
        public string IsAttendedTg { get; set; }
        public string NoofPendingTg { get; set; }
        public string NoofPublishedTg { get; set; }
        public string TgreasonForNotAttend { get; set; }
        public string ProgramOther { get; set; }
        public string UnSoldRightsBooks { get; set; }
        public string AcquireRightsToPublished { get; set; }
        public string IsAlreadyBoughtRights { get; set; }
        public string KeyPublisherTitlesOrBusinessRegion { get; set; }
        public string CatalogueMainLanguageOfOriginalTitle { get; set; }
        public string MainTerritories { get; set; }
        public string SuggestionToSupportApp { get; set; }
        public string RoundTableTopics { get; set; }
        public string SpeakOnRoundTable { get; set; }
        public string RecommendPublishers { get; set; }
        public string Qrcode { get; set; }
        public string Type { get; set; }
        public string IsEditRequest { get; set; }
        public string IsCheckedIn { get; set; }
        public string IsHopitalityPackage { get; set; }
        public string CatalogUrl { get; set; }
        public string LanguageToSellBooks { get; set; }
        public string DocumentRegProfile { get; set; }
        public string IsRequestDisplayBooth { get; set; }
        public long? CreatedById { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual XsiExhibitionBookSellerProgram BookSellerProgram { get; set; }
        public virtual XsiBookSellerMember Member { get; set; }
        public virtual ICollection<XsiExhibitionBookSellerProgramRegistrationStatusLogs> XsiExhibitionBookSellerProgramRegistrationStatusLogs { get; set; }
        public virtual ICollection<XsiExhibitionBookSellerProgramUserBook> XsiExhibitionBookSellerProgramUserBook { get; set; }
        public virtual ICollection<XsiExhibitionBookSellerUserGenre> XsiExhibitionBookSellerUserGenre { get; set; }
        public virtual ICollection<XsiExhibitionBookSellerUserProgram> XsiExhibitionBookSellerUserProgram { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiNewsletter
    {
        public long ItemId { get; set; }
        public string IssueNumber { get; set; }
        public string Url { get; set; }
        public DateTime? Dated { get; set; }
        public string FileName { get; set; }
        public string IsActive { get; set; }
        public string IssueNumberAr { get; set; }
        public string Urlar { get; set; }
        public DateTime? DatedAr { get; set; }
        public string FileNameAr { get; set; }
        public string IsActiveAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedById { get; set; }
    }
}

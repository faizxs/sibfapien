﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiSubAwards
    {
        public XsiSubAwards()
        {
            XsiAwardsNominationForms = new HashSet<XsiAwardsNominationForms>();
        }

        public long ItemId { get; set; }
        public long? AwardId { get; set; }
        public string IsActive { get; set; }
        public string Name { get; set; }
        public string Introduction { get; set; }
        public string Conditions { get; set; }
        public string Rrequirements { get; set; }
        public string IsActiveAr { get; set; }
        public string NameAr { get; set; }
        public string IntroductionAr { get; set; }
        public string ConditionsAr { get; set; }
        public string RrequirementsAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifieidBy { get; set; }

        public virtual XsiAwards Award { get; set; }
        public virtual ICollection<XsiAwardsNominationForms> XsiAwardsNominationForms { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiActivitiesSuggestion
    {
        public long ItemId { get; set; }
        public long? LanguageId { get; set; }
        public string IsActive { get; set; }
        public long? WebsiteId { get; set; }
        public long? ExhibitionId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Detail { get; set; }
        public long? CountryId { get; set; }
        public long? ActivityCategory { get; set; }
        public string ActivityTitle { get; set; }
        public string ActivityDescription { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

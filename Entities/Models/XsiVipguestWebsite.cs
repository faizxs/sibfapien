﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiVipguestWebsite
    {
        public long ItemId { get; set; }
        public long? WebsiteId { get; set; }
        public long? VipguestId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionStaffGuestTransportDetails
    {
        public long ItemId { get; set; }
        public long? StaffGuestId { get; set; }
        public long? ExhibitionId { get; set; }
        public string TransportationType { get; set; }
        public DateTime? TransportStartDate { get; set; }
        public DateTime? TransportEndDate { get; set; }
        public string TransportContactPerson { get; set; }
        public string TransportContactNumber { get; set; }
        public string TransportationStatus { get; set; }
        public string TransportationTypeAllotted { get; set; }
        public int? NoofPeople { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

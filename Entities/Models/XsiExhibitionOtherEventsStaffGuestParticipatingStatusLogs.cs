﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs
    {
        public long ItemId { get; set; }
        public long? ExhibitionId { get; set; }
        public long? StaffGuestId { get; set; }
        public long? AdminId { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiCareerApplicants
    {
        public long ItemId { get; set; }
        public long? LanguageId { get; set; }
        public string IsActive { get; set; }
        public string Status { get; set; }
        public long? CareerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string YearofBirth { get; set; }
        public string VisaStatus { get; set; }
        public string Gender { get; set; }
        public long? Nationality { get; set; }
        public long? Residence { get; set; }
        public long? Degree { get; set; }
        public long? Speciality { get; set; }
        public long? SubSpeciality { get; set; }
        public string Cv { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiCareer Career { get; set; }
        public virtual XsiCareerDegrees DegreeNavigation { get; set; }
        public virtual XsiExhibitionCountry NationalityNavigation { get; set; }
        public virtual XsiExhibitionCountry ResidenceNavigation { get; set; }
    }
}

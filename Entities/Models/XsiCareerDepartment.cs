﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiCareerDepartment
    {
        public XsiCareerDepartment()
        {
            XsiCareer = new HashSet<XsiCareer>();
            XsiCareerSubDepartment = new HashSet<XsiCareerSubDepartment>();
        }

        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string Title { get; set; }
        public string TitleAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<XsiCareer> XsiCareer { get; set; }
        public virtual ICollection<XsiCareerSubDepartment> XsiCareerSubDepartment { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionApplication
    {
        public XsiExhibitionApplication()
        {
            XsiExhibitionMemberApplicationYearly = new HashSet<XsiExhibitionMemberApplicationYearly>();
        }

        public long ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationNameAr { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public long? SortOrder { get; set; }
        public long? WebsiteId { get; set; }
        public string IsActiveForSibf { get; set; }
        public string IsActiveForScrf { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<XsiExhibitionMemberApplicationYearly> XsiExhibitionMemberApplicationYearly { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiVideoAlbum
    {
        public XsiVideoAlbum()
        {
            XsiVideo = new HashSet<XsiVideo>();
        }

        public long ItemId { get; set; }
        public long? VideoCategoryId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? Dated { get; set; }
        public long? SortIndex { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public string DescriptionAr { get; set; }
        public DateTime? DatedAr { get; set; }
        public long? SortIndexAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiVideoCategory VideoCategory { get; set; }
        public virtual ICollection<XsiVideo> XsiVideo { get; set; }
    }
}

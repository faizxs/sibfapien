﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionPpbooksLanguage
    {
        public long ItemId { get; set; }
        public long? PpbookId { get; set; }
        public long? LanguageId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiVipguest
    {
        public long ItemId { get; set; }
        public long? WebsiteId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string IsActive { get; set; }
        public string NameAr { get; set; }
        public string TitleAr { get; set; }
        public string ImageAr { get; set; }
        public string IsActiveAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}

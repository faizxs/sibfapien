﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiDiscount
    {
        public long ItemId { get; set; }
        public long? ExhibitorId { get; set; }
        public string IsActive { get; set; }
        public string IsManagerChecked { get; set; }
        public string Status { get; set; }
        public string ParticipationFee { get; set; }
        public string AllocatedSpace { get; set; }
        public string AllocatedSpacePrice { get; set; }
        public string Total { get; set; }
        public string DiscountAmount { get; set; }
        public string DiscountPercentage { get; set; }
        public string FinalTotal { get; set; }
        public string Comment { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedById { get; set; }

        public virtual XsiExhibitionMemberApplicationYearly Exhibitor { get; set; }
    }
}

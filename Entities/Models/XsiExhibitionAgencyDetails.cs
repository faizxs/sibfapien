﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionAgencyDetails
    {
        public long AgencyDetailsId { get; set; }
        public long? MemberExhibitionYearlyId { get; set; }
        public string IsIncludedInInvoice { get; set; }
        public string IsRegisteredByExhibitor { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionMemberApplicationYearly MemberExhibitionYearly { get; set; }
    }
}

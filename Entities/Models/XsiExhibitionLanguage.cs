﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionLanguage
    {
        public XsiExhibitionLanguage()
        {
            XsiExhibitionBooks = new HashSet<XsiExhibitionBooks>();
            XsiExhibitionTranslationGrantMembersOriginalLanguage = new HashSet<XsiExhibitionTranslationGrantMembers>();
            XsiExhibitionTranslationGrantMembersProposedLanguage = new HashSet<XsiExhibitionTranslationGrantMembers>();
        }

        public long ItemId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<XsiExhibitionBooks> XsiExhibitionBooks { get; set; }
        public virtual ICollection<XsiExhibitionTranslationGrantMembers> XsiExhibitionTranslationGrantMembersOriginalLanguage { get; set; }
        public virtual ICollection<XsiExhibitionTranslationGrantMembers> XsiExhibitionTranslationGrantMembersProposedLanguage { get; set; }
    }
}

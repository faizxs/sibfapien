﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiCareerSubDepartment
    {
        public XsiCareerSubDepartment()
        {
            XsiCareer = new HashSet<XsiCareer>();
        }

        public long ItemId { get; set; }
        public long? DepartmentId { get; set; }
        public string IsActive { get; set; }
        public string Title { get; set; }
        public string IsActiveAr { get; set; }
        public string TitleAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiCareerDepartment Department { get; set; }
        public virtual ICollection<XsiCareer> XsiCareer { get; set; }
    }
}

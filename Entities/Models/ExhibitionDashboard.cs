﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class ExhibitionDashboard
    {
        public long ExhibitionId { get; set; }
        public long? WebsiteId { get; set; }
        public long? ExhibitionYear { get; set; }
        public long? TotalRegistrationCount { get; set; }
        public long? TotalApprovedCount { get; set; }
        public long? ExhibitorApprovedCount { get; set; }
        public long? AgencyApprovedCount { get; set; }
        public long? ResaurantApprovedCount { get; set; }
        public long? ExhibitorOtherCount { get; set; }
        public long? AgencyOtherCount { get; set; }
        public long? RestaurantOtherCount { get; set; }
    }
}

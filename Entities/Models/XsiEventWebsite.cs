﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiEventWebsite
    {
        public long ItemId { get; set; }
        public long? EventId { get; set; }
        public long? WebsiteId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBookParticipating
    {
        public long ItemId { get; set; }
        public long BookId { get; set; }
        public long? OldExhibitorId { get; set; }
        public long? OldAgencyId { get; set; }
        public long? ExhibitorId { get; set; }
        public long? AgencyId { get; set; }
        public string Price { get; set; }
        public string PriceBeforeDiscount { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual XsiExhibitionMemberApplicationYearly Agency { get; set; }
        public virtual XsiExhibitionBooks Book { get; set; }
        public virtual XsiExhibitionMemberApplicationYearly Exhibitor { get; set; }
    }
}

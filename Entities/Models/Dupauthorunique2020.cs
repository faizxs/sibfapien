﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Dupauthorunique2020
    {
        public long AuthorId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public long? LanguageId { get; set; }
        public string Title { get; set; }
        public string TitleAr { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public string Status { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionBookSellerUserBooksLanguage
    {
        public long ItemId { get; set; }
        public long? BookSellerBookId { get; set; }
        public long? LanguageId { get; set; }
    }
}

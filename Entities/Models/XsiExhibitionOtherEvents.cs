﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class XsiExhibitionOtherEvents
    {
        public XsiExhibitionOtherEvents()
        {
            XsiExhibitionOtherEventsStaffGuestBookingDetails = new HashSet<XsiExhibitionOtherEventsStaffGuestBookingDetails>();
        }

        public long ExhibitionId { get; set; }
        public string IsActive { get; set; }
        public string IsActiveAr { get; set; }
        public string IsArchive { get; set; }
        public long? WebsiteId { get; set; }
        public string Title { get; set; }
        public string Overview { get; set; }
        public string Description { get; set; }
        public string TitleAr { get; set; }
        public string OverviewAr { get; set; }
        public string DescriptionAr { get; set; }
        public string TotalArea { get; set; }
        public long? ExhibitionYear { get; set; }
        public long? ExhibitionNumber { get; set; }
        public string TopContent { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? StaffGuestStartDate { get; set; }
        public DateTime? StaffGuestEndDate { get; set; }
        public DateTime? StaffGuestBookingExpiryDate { get; set; }
        public string FileName { get; set; }
        public string Url { get; set; }
        public string Penalty { get; set; }
        public string ParticipationFee { get; set; }
        public string PriceSqM { get; set; }
        public string VisaProcessingPrice { get; set; }
        public string AdvertisementRates { get; set; }
        public string ImportantInformation { get; set; }
        public string ExpiryMonths { get; set; }
        public string TopContentAr { get; set; }
        public string CancellationFeeAr { get; set; }
        public string FileNameAr { get; set; }
        public string Urlar { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public virtual ICollection<XsiExhibitionOtherEventsStaffGuestBookingDetails> XsiExhibitionOtherEventsStaffGuestBookingDetails { get; set; }
    }
}

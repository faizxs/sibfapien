﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PPSourceBL : IDisposable
    {
        #region Feilds
        IPPSourceRepository PPSourceRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PPSourceBL()
        {
            ConnectionString = null;
        }
        public PPSourceBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PPSourceRepository != null)
                this.PPSourceRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionProfessionalProgramSource GetPPSourceByItemId(long itemId)
        {
            using (PPSourceRepository = new PPSourceRepository(ConnectionString))
            {
                return PPSourceRepository.GetById(itemId);
            }
        }
         
        public List<XsiExhibitionProfessionalProgramSource> GetPPSource()
        {
            using (PPSourceRepository = new PPSourceRepository(ConnectionString))
            {
                return PPSourceRepository.Select();
            }
        }
        public List<XsiExhibitionProfessionalProgramSource> GetPPSource(EnumSortlistBy sortListBy)
        {
            using (PPSourceRepository = new PPSourceRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PPSourceRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PPSourceRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PPSourceRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PPSourceRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PPSourceRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PPSourceRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PPSourceRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionProfessionalProgramSource> GetPPSource(XsiExhibitionProfessionalProgramSource entity)
        {
            using (PPSourceRepository = new PPSourceRepository(ConnectionString))
            {
                return PPSourceRepository.Select(entity);
            }
        }
        public List<XsiExhibitionProfessionalProgramSource> GetPPSource(XsiExhibitionProfessionalProgramSource entity, EnumSortlistBy sortListBy)
        {
            using (PPSourceRepository = new PPSourceRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PPSourceRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PPSourceRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PPSourceRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PPSourceRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PPSourceRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PPSourceRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PPSourceRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public EnumResultType InsertPPSource(XsiExhibitionProfessionalProgramSource entity)
        {
            using (PPSourceRepository = new PPSourceRepository(ConnectionString))
            {
                entity = PPSourceRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePPSource(XsiExhibitionProfessionalProgramSource entity)
        {
            XsiExhibitionProfessionalProgramSource OriginalEntity = GetPPSourceByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PPSourceRepository = new PPSourceRepository(ConnectionString))
                {
                    PPSourceRepository.Update(entity);
                    if (PPSourceRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdatePPSourceStatus(long itemId)
        {
            XsiExhibitionProfessionalProgramSource entity = GetPPSourceByItemId(itemId);
            if (entity != null)
            {
                using (PPSourceRepository = new PPSourceRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    PPSourceRepository.Update(entity);
                    if (PPSourceRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePPSource(XsiExhibitionProfessionalProgramSource entity)
        {
            List<XsiExhibitionProfessionalProgramSource> entities = GetPPSource(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionProfessionalProgramSource e in entities)
                {
                    result = DeletePPSource(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeletePPSource(long itemId)
        {
            XsiExhibitionProfessionalProgramSource entity = GetPPSourceByItemId(itemId);
            if (entity != null)
            {
                using (PPSourceRepository = new PPSourceRepository(ConnectionString))
                {
                    PPSourceRepository.Delete(itemId);
                    if (PPSourceRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
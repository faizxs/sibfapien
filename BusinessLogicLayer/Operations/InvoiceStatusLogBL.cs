﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class InvoiceStatusLogBL : IDisposable
    {
        #region Feilds
        IInvoiceStatusLogRepository IInvoiceStatusLogRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public InvoiceStatusLogBL()
        {
            ConnectionString = null;
        }
        public InvoiceStatusLogBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IInvoiceStatusLogRepository != null)
                this.IInvoiceStatusLogRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IInvoiceStatusLogRepository = new InvoiceStatusLogRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiInvoiceStatusLog GetInvoiceStatusLogByItemId(long itemId)
        {
            using (IInvoiceStatusLogRepository = new InvoiceStatusLogRepository(ConnectionString))
            {
                return IInvoiceStatusLogRepository.GetById(itemId);
            }
        }
        
        public List<XsiInvoiceStatusLog> GetInvoiceStatusLog()
        {
            using (IInvoiceStatusLogRepository = new InvoiceStatusLogRepository(ConnectionString))
            {
                return IInvoiceStatusLogRepository.Select();
            }
        }
        public List<XsiInvoiceStatusLog> GetInvoiceStatusLog(EnumSortlistBy sortListBy)
        {
            using (IInvoiceStatusLogRepository = new InvoiceStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IInvoiceStatusLogRepository.Select().OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IInvoiceStatusLogRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IInvoiceStatusLogRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IInvoiceStatusLogRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IInvoiceStatusLogRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IInvoiceStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IInvoiceStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiInvoiceStatusLog> GetInvoiceStatusLog(XsiInvoiceStatusLog entity)
        {
            using (IInvoiceStatusLogRepository = new InvoiceStatusLogRepository(ConnectionString))
            {
                return IInvoiceStatusLogRepository.Select(entity);
            }
        }
        public List<XsiInvoiceStatusLog> GetInvoiceStatusLog(XsiInvoiceStatusLog entity, EnumSortlistBy sortListBy)
        {
            using (IInvoiceStatusLogRepository = new InvoiceStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IInvoiceStatusLogRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IInvoiceStatusLogRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IInvoiceStatusLogRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IInvoiceStatusLogRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IInvoiceStatusLogRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IInvoiceStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IInvoiceStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiInvoiceStatusLog GetInvoiceStatusLog(long groupId, long languageId)
        {
            using (IInvoiceStatusLogRepository = new InvoiceStatusLogRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertInvoiceStatusLog(XsiInvoiceStatusLog entity)
        {
            using (IInvoiceStatusLogRepository = new InvoiceStatusLogRepository(ConnectionString))
            {
                entity = IInvoiceStatusLogRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId =  -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateInvoiceStatusLog(XsiInvoiceStatusLog entity)
        {
            XsiInvoiceStatusLog OriginalEntity = GetInvoiceStatusLogByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IInvoiceStatusLogRepository = new InvoiceStatusLogRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line
                    
                    IInvoiceStatusLogRepository.Update(entity);
                    if (IInvoiceStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateInvoiceStatusLogStatus(long itemId)
        {
            XsiInvoiceStatusLog entity = GetInvoiceStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IInvoiceStatusLogRepository = new InvoiceStatusLogRepository(ConnectionString))
                {
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IInvoiceStatusLogRepository.Update(entity);
                    if (IInvoiceStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteInvoiceStatusLog(XsiInvoiceStatusLog entity)
        {
            List<XsiInvoiceStatusLog> entities = GetInvoiceStatusLog(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiInvoiceStatusLog e in entities)
                {
                    result = DeleteInvoiceStatusLog(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteInvoiceStatusLog(long itemId)
        {
            XsiInvoiceStatusLog entity = GetInvoiceStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IInvoiceStatusLogRepository = new InvoiceStatusLogRepository(ConnectionString))
                {
                    IInvoiceStatusLogRepository.Delete(itemId);
                    if (IInvoiceStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
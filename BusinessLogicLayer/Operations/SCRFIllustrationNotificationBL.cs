﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFIllustrationNotificationBL : IDisposable
    {
        #region Feilds
        ISCRFIllustrationNotificationRepository ISCRFIllustrationNotificationRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFIllustrationNotificationBL()
        {
            ConnectionString = null;
        }
        public SCRFIllustrationNotificationBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ISCRFIllustrationNotificationRepository != null)
                this.ISCRFIllustrationNotificationRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            return -1;
        }

        public XsiScrfillustrationNotification GetSCRFIllustrationNotificationByItemId(long itemId)
        {
            using (ISCRFIllustrationNotificationRepository = new SCRFIllustrationNotificationRepository(ConnectionString))
            {
                return ISCRFIllustrationNotificationRepository.GetById(itemId);
            }
        }

        public List<XsiScrfillustrationNotification> GetSCRFIllustrationNotification()
        {
            using (ISCRFIllustrationNotificationRepository = new SCRFIllustrationNotificationRepository(ConnectionString))
            {
                return ISCRFIllustrationNotificationRepository.Select();
            }
        }
        public List<XsiScrfillustrationNotification> GetSCRFIllustrationNotification(EnumSortlistBy sortListBy)
        {
            using (ISCRFIllustrationNotificationRepository = new SCRFIllustrationNotificationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return ISCRFIllustrationNotificationRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ISCRFIllustrationNotificationRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ISCRFIllustrationNotificationRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfillustrationNotification> GetSCRFIllustrationNotification(XsiScrfillustrationNotification entity)
        {
            using (ISCRFIllustrationNotificationRepository = new SCRFIllustrationNotificationRepository(ConnectionString))
            {
                return ISCRFIllustrationNotificationRepository.Select(entity);
            }
        }
        public List<XsiScrfillustrationNotification> GetSCRFIllustrationNotification(XsiScrfillustrationNotification entity, EnumSortlistBy sortListBy)
        {
            using (ISCRFIllustrationNotificationRepository = new SCRFIllustrationNotificationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return ISCRFIllustrationNotificationRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ISCRFIllustrationNotificationRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ISCRFIllustrationNotificationRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiScrfillustrationNotification GetSCRFIllustrationNotification(long groupId, long languageId)
        {
            using (ISCRFIllustrationNotificationRepository = new SCRFIllustrationNotificationRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertSCRFIllustrationNotification(XsiScrfillustrationNotification entity)
        {
            using (ISCRFIllustrationNotificationRepository = new SCRFIllustrationNotificationRepository(ConnectionString))
            {
                entity = ISCRFIllustrationNotificationRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFIllustrationNotification(XsiScrfillustrationNotification entity)
        {
            XsiScrfillustrationNotification OriginalEntity = GetSCRFIllustrationNotificationByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ISCRFIllustrationNotificationRepository = new SCRFIllustrationNotificationRepository(ConnectionString))
                {
                    ISCRFIllustrationNotificationRepository.Update(entity);
                    if (ISCRFIllustrationNotificationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFIllustrationNotification(XsiScrfillustrationNotification entity)
        {
            List<XsiScrfillustrationNotification> entities = GetSCRFIllustrationNotification(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfillustrationNotification e in entities)
                {
                    result = DeleteSCRFIllustrationNotification(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFIllustrationNotification(long itemId)
        {
            XsiScrfillustrationNotification entity = GetSCRFIllustrationNotificationByItemId(itemId);
            if (entity != null)
            {
                using (ISCRFIllustrationNotificationRepository = new SCRFIllustrationNotificationRepository(ConnectionString))
                {
                    ISCRFIllustrationNotificationRepository.Delete(itemId);
                    if (ISCRFIllustrationNotificationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
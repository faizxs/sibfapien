﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class CareerSubDepartmentBL : IDisposable
    {
        #region Feilds
        ICareerSubDepartmentRepository CareerSubDepartmentRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public CareerSubDepartmentBL()
        {
            ConnectionString = null;
        }
        public CareerSubDepartmentBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerSubDepartmentRepository != null)
                this.CareerSubDepartmentRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiCareerSubDepartment GetCareerSubDepartmentByItemId(long itemId)
        {
            using (CareerSubDepartmentRepository = new CareerSubDepartmentRepository(ConnectionString))
            {
                return CareerSubDepartmentRepository.GetById(itemId);
            }
        }
       

        public List<XsiCareerSubDepartment> GetCareerSubDepartment()
        {
            using (CareerSubDepartmentRepository = new CareerSubDepartmentRepository(ConnectionString))
            {
                return CareerSubDepartmentRepository.Select();
            }
        }
        public List<XsiCareerSubDepartment> GetCareerSubDepartment(EnumSortlistBy sortListBy)
        {
            using (CareerSubDepartmentRepository = new CareerSubDepartmentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerSubDepartmentRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerSubDepartmentRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerSubDepartmentRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerSubDepartmentRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerSubDepartmentRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerSubDepartmentRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerSubDepartmentRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareerSubDepartment> GetCareerSubDepartment(XsiCareerSubDepartment entity)
        {
            using (CareerSubDepartmentRepository = new CareerSubDepartmentRepository(ConnectionString))
            {
                return CareerSubDepartmentRepository.Select(entity);
            }
        }
        public List<XsiCareerSubDepartment> GetCareerSubDepartment(XsiCareerSubDepartment entity, EnumSortlistBy sortListBy)
        {
            using (CareerSubDepartmentRepository = new CareerSubDepartmentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerSubDepartmentRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerSubDepartmentRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerSubDepartmentRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerSubDepartmentRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerSubDepartmentRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerSubDepartmentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerSubDepartmentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
         
        public EnumResultType InsertCareerSubDepartment(XsiCareerSubDepartment entity)
        {
            using (CareerSubDepartmentRepository = new CareerSubDepartmentRepository(ConnectionString))
            {
                entity = CareerSubDepartmentRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateCareerSubDepartment(XsiCareerSubDepartment entity)
        {
            XsiCareerSubDepartment OriginalEntity = GetCareerSubDepartmentByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (CareerSubDepartmentRepository = new CareerSubDepartmentRepository(ConnectionString))
                {
                    CareerSubDepartmentRepository.Update(entity);
                    if (CareerSubDepartmentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateCareerSubDepartmentStatus(long itemId)
        {
            XsiCareerSubDepartment entity = GetCareerSubDepartmentByItemId(itemId);
            if (entity != null)
            {
                using (CareerSubDepartmentRepository = new CareerSubDepartmentRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    CareerSubDepartmentRepository.Update(entity);
                    if (CareerSubDepartmentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteCareerSubDepartment(XsiCareerSubDepartment entity)
        {
            List<XsiCareerSubDepartment> entities = GetCareerSubDepartment(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiCareerSubDepartment e in entities)
                {
                    result = DeleteCareerSubDepartment(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteCareerSubDepartment(long itemId)
        {
            XsiCareerSubDepartment entity = GetCareerSubDepartmentByItemId(itemId);
            if (entity != null)
            {
                using (CareerSubDepartmentRepository = new CareerSubDepartmentRepository(ConnectionString))
                {
                    CareerSubDepartmentRepository.Delete(itemId);
                    if (CareerSubDepartmentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
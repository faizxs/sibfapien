﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class BooksParticipatingBL : IDisposable
    {
        #region Feilds
        IBooksParticipatingRepository IBooksParticipatingRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public BooksParticipatingBL()
        {
            ConnectionString = null;
        }
        public BooksParticipatingBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IBooksParticipatingRepository != null)
                this.IBooksParticipatingRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionBookParticipating GetBooksParticipatingByItemId(long BookId)
        {
            using (IBooksParticipatingRepository = new BooksParticipatingRepository(ConnectionString))
            {
                return IBooksParticipatingRepository.GetById(BookId);
            }
        }


        public List<XsiExhibitionBookParticipating> GetBooksParticipating()
        {
            using (IBooksParticipatingRepository = new BooksParticipatingRepository(ConnectionString))
            {
                return IBooksParticipatingRepository.Select();
            }
        }
        public List<XsiExhibitionBookParticipating> GetBooksParticipating(EnumSortlistBy sortListBy)
        {
            using (IBooksParticipatingRepository = new BooksParticipatingRepository(ConnectionString))
            {
                switch (sortListBy)
                {

                    case EnumSortlistBy.ByDateAsc:
                        return IBooksParticipatingRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IBooksParticipatingRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IBooksParticipatingRepository.Select().OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IBooksParticipatingRepository.Select().OrderByDescending(p => p.BookId).ToList();

                    default:
                        return IBooksParticipatingRepository.Select().OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
        public List<XsiExhibitionBookParticipating> GetBooksParticipating(XsiExhibitionBookParticipating entity)
        {
            using (IBooksParticipatingRepository = new BooksParticipatingRepository(ConnectionString))
            {
                return IBooksParticipatingRepository.Select(entity);
            }
        }
        public List<XsiExhibitionBookParticipating> GetBooksParticipating(XsiExhibitionBookParticipating entity, EnumSortlistBy sortListBy)
        {
            using (IBooksParticipatingRepository = new BooksParticipatingRepository(ConnectionString))
            {
                switch (sortListBy)
                {

                    case EnumSortlistBy.ByDateAsc:
                        return IBooksParticipatingRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IBooksParticipatingRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IBooksParticipatingRepository.Select(entity).OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IBooksParticipatingRepository.Select(entity).OrderByDescending(p => p.BookId).ToList();

                    default:
                        return IBooksParticipatingRepository.Select(entity).OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
        public XsiExhibitionBookParticipating GetBooksParticipating(long groupId, long languageId)
        {
            return null;
        }

        public EnumResultType InsertBooksParticipating(XsiExhibitionBookParticipating entity)
        {
            using (IBooksParticipatingRepository = new BooksParticipatingRepository(ConnectionString))
            {
                entity = IBooksParticipatingRepository.Add(entity);
                if (entity.BookId > 0)
                {
                    XsiItemdId = entity.BookId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateBooksParticipating(XsiExhibitionBookParticipating entity)
        {
            XsiExhibitionBookParticipating OriginalEntity = GetBooksParticipating(new XsiExhibitionBookParticipating() { BookId = entity.BookId, ExhibitorId = entity.ExhibitorId }).FirstOrDefault();

            if (OriginalEntity != null)
            {
                using (IBooksParticipatingRepository = new BooksParticipatingRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line
                    //entity.RollbackXml = BusinessMethodFactory.GetRollbackXml(OriginalEntity);

                    IBooksParticipatingRepository.Update(entity);
                    if (IBooksParticipatingRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackBooksParticipating(long BookId)
        {
            //if no RollbackXml propery in BooksParticipating then delete below code and return EnumResultType.Invalid;
            return EnumResultType.Success;
        }
        public EnumResultType UpdateBooksParticipatingStatus(long BookId)
        {
            XsiExhibitionBookParticipating entity = GetBooksParticipatingByItemId(BookId);
            if (entity != null)
            {
                using (IBooksParticipatingRepository = new BooksParticipatingRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IBooksParticipatingRepository.Update(entity);
                    if (IBooksParticipatingRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteBooksParticipating(XsiExhibitionBookParticipating entity)
        {
            List<XsiExhibitionBookParticipating> entities = GetBooksParticipating(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionBookParticipating e in entities)
                {
                    result = DeleteBooksParticipating(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteBooksParticipating(long bookparticipatingItemd)
        {
            XsiExhibitionBookParticipating entity = GetBooksParticipatingByItemId(bookparticipatingItemd);
            if (entity != null)
            {
                using (IBooksParticipatingRepository = new BooksParticipatingRepository(ConnectionString))
                {
                    IBooksParticipatingRepository.Delete(bookparticipatingItemd);
                    if (IBooksParticipatingRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
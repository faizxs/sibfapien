﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class DownloadCategoryBL : IDisposable
    {
        #region Feilds
        IDownloadCategoryRepository DownloadCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public DownloadCategoryBL()
        {
            ConnectionString = null;
        }
        public DownloadCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.DownloadCategoryRepository != null)
                this.DownloadCategoryRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiDownloadCategory GetDownloadCategoryByItemId(long itemId)
        {
            using (DownloadCategoryRepository = new DownloadCategoryRepository(ConnectionString))
            {
                return DownloadCategoryRepository.GetById(itemId);
            }
        }
         
        public List<XsiDownloadCategory> GetDownloadCategory()
        {
            using (DownloadCategoryRepository = new DownloadCategoryRepository(ConnectionString))
            {
                return DownloadCategoryRepository.Select();
            }
        }
        public List<XsiDownloadCategory> GetDownloadCategory(EnumSortlistBy sortListBy)
        {
            using (DownloadCategoryRepository = new DownloadCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return DownloadCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return DownloadCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return DownloadCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return DownloadCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return DownloadCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return DownloadCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return DownloadCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiDownloadCategory> GetDownloadCategory(XsiDownloadCategory entity)
        {
            using (DownloadCategoryRepository = new DownloadCategoryRepository(ConnectionString))
            {
                return DownloadCategoryRepository.Select(entity);
            }
        }
        public List<XsiDownloadCategory> GetDownloadCategory(XsiDownloadCategory entity, EnumSortlistBy sortListBy)
        {
            using (DownloadCategoryRepository = new DownloadCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return DownloadCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return DownloadCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return DownloadCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return DownloadCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return DownloadCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return DownloadCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return DownloadCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiDownloadCategory> GetDownloadCategoryOr(XsiDownloadCategory entity, EnumSortlistBy sortListBy)
        {
            using (DownloadCategoryRepository = new DownloadCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return DownloadCategoryRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return DownloadCategoryRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return DownloadCategoryRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return DownloadCategoryRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return DownloadCategoryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return DownloadCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return DownloadCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
       
        public List<XsiDownloadCategory> GetOtherLanguageDownloadCategory(XsiDownloadCategory entity)
        {
            using (DownloadCategoryRepository = new DownloadCategoryRepository(ConnectionString))
            {
                return DownloadCategoryRepository.SelectOtherLanguageCategory(entity);
            }
        }
        public List<XsiDownloadCategory> GetCurrentLanguageDownloadCategory(XsiDownloadCategory entity)
        {
            using (DownloadCategoryRepository = new DownloadCategoryRepository(ConnectionString))
            {
                return DownloadCategoryRepository.SelectCurrentLanguageCategory(entity);
            }
        }

        public EnumResultType InsertDownloadCategory(XsiDownloadCategory entity)
        {
            using (DownloadCategoryRepository = new DownloadCategoryRepository(ConnectionString))
            {
                entity = DownloadCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateDownloadCategory(XsiDownloadCategory entity)
        {
            XsiDownloadCategory OriginalEntity = GetDownloadCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (DownloadCategoryRepository = new DownloadCategoryRepository(ConnectionString))
                {
                    DownloadCategoryRepository.Update(entity);
                    if (DownloadCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateDownloadCategoryStatus(long itemId)
        {
            XsiDownloadCategory entity = GetDownloadCategoryByItemId(itemId);
            if (entity != null)
            {
                using (DownloadCategoryRepository = new DownloadCategoryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    DownloadCategoryRepository.Update(entity);
                    if (DownloadCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteDownloadCategory(XsiDownloadCategory entity)
        {
            List<XsiDownloadCategory> entities = GetDownloadCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiDownloadCategory e in entities)
                {
                    result = DeleteDownloadCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteDownloadCategory(long itemId)
        {
            XsiDownloadCategory entity = GetDownloadCategoryByItemId(itemId);
            if (entity != null)
            {
                using (DownloadCategoryRepository = new DownloadCategoryRepository(ConnectionString))
                {
                    DownloadCategoryRepository.Delete(itemId);
                    if (DownloadCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
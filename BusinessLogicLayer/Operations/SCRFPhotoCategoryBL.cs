﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFPhotoCategoryBL : IDisposable
    {
        #region Feilds
        ISCRFPhotoCategoryRepository SCRFPhotoCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFPhotoCategoryBL()
        {
            ConnectionString = null;
        }
        public SCRFPhotoCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFPhotoCategoryRepository != null)
                this.SCRFPhotoCategoryRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiScrfphotoCategory GetSCRFPhotoCategoryByItemId(long itemId)
        {
            using (SCRFPhotoCategoryRepository = new SCRFPhotoCategoryRepository(ConnectionString))
            {
                return SCRFPhotoCategoryRepository.GetById(itemId);
            }
        }
        
        public List<XsiScrfphotoCategory> GetSCRFPhotoCategory()
        {
            using (SCRFPhotoCategoryRepository = new SCRFPhotoCategoryRepository(ConnectionString))
            {
                return SCRFPhotoCategoryRepository.Select();
            }
        }
        public List<XsiScrfphotoCategory> GetSCRFPhotoCategory(EnumSortlistBy sortListBy)
        {
            using (SCRFPhotoCategoryRepository = new SCRFPhotoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPhotoCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPhotoCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFPhotoCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFPhotoCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPhotoCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPhotoCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFPhotoCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfphotoCategory> GetSCRFPhotoCategory(XsiScrfphotoCategory entity)
        {
            using (SCRFPhotoCategoryRepository = new SCRFPhotoCategoryRepository(ConnectionString))
            {
                return SCRFPhotoCategoryRepository.Select(entity);
            }
        }
        public List<XsiScrfphotoCategory> GetSCRFPhotoCategory(XsiScrfphotoCategory entity, EnumSortlistBy sortListBy)
        {
            using (SCRFPhotoCategoryRepository = new SCRFPhotoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPhotoCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPhotoCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFPhotoCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFPhotoCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPhotoCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPhotoCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFPhotoCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfphotoCategory> GetSCRFPhotoCategoryOr(XsiScrfphotoCategory entity, EnumSortlistBy sortListBy)
        {
            using (SCRFPhotoCategoryRepository = new SCRFPhotoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPhotoCategoryRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPhotoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFPhotoCategoryRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFPhotoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPhotoCategoryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPhotoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFPhotoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
         
        public List<XsiScrfphotoCategory> GetOtherLanguageSCRFPhotoCategory(XsiScrfphotoCategory entity)
        {
            using (SCRFPhotoCategoryRepository = new SCRFPhotoCategoryRepository(ConnectionString))
            {
                return SCRFPhotoCategoryRepository.SelectOtherLanguageCategory(entity);
            }
        }
        public List<XsiScrfphotoCategory> GetCurrentLanguageSCRFPhotoCategory(XsiScrfphotoCategory entity)
        {
            using (SCRFPhotoCategoryRepository = new SCRFPhotoCategoryRepository(ConnectionString))
            {
                return SCRFPhotoCategoryRepository.SelectCurrentLanguageCategory(entity);
            }
        }

        public EnumResultType InsertSCRFPhotoCategory(XsiScrfphotoCategory entity)
        {
            using (SCRFPhotoCategoryRepository = new SCRFPhotoCategoryRepository(ConnectionString))
            {
                entity = SCRFPhotoCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFPhotoCategory(XsiScrfphotoCategory entity)
        {
            XsiScrfphotoCategory OriginalEntity = GetSCRFPhotoCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFPhotoCategoryRepository = new SCRFPhotoCategoryRepository(ConnectionString))
                {
                    SCRFPhotoCategoryRepository.Update(entity);
                    if (SCRFPhotoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFPhotoCategoryStatus(long itemId)
        {
            XsiScrfphotoCategory entity = GetSCRFPhotoCategoryByItemId(itemId);
            if (entity != null)
            {
                using (SCRFPhotoCategoryRepository = new SCRFPhotoCategoryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFPhotoCategoryRepository.Update(entity);
                    if (SCRFPhotoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFPhotoCategory(XsiScrfphotoCategory entity)
        {
            List<XsiScrfphotoCategory> entities = GetSCRFPhotoCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfphotoCategory e in entities)
                {
                    result = DeleteSCRFPhotoCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFPhotoCategory(long itemId)
        {
            XsiScrfphotoCategory entity = GetSCRFPhotoCategoryByItemId(itemId);
            if (entity != null)
            {
                using (SCRFPhotoCategoryRepository = new SCRFPhotoCategoryRepository(ConnectionString))
                {
                    SCRFPhotoCategoryRepository.Delete(itemId);
                    if (SCRFPhotoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
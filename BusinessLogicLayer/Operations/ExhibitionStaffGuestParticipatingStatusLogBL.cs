﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionStaffGuestParticipatingStatusLogBL : IDisposable
    {
        #region Feilds
        IExhibitionStaffGuestParticipatingStatusLogRepository IExhibitionStaffGuestParticipatingStatusLogRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestParticipatingStatusLogBL()
        {
            ConnectionString = null;
        }
        public ExhibitionStaffGuestParticipatingStatusLogBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionStaffGuestParticipatingStatusLogRepository != null)
                this.IExhibitionStaffGuestParticipatingStatusLogRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionStaffGuestParticipatingStatusLogRepository = new ExhibitionStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionStaffGuestParticipatingStatusLogs GetExhibitionStaffGuestParticipatingStatusLogByItemId(long itemId)
        {
            using (IExhibitionStaffGuestParticipatingStatusLogRepository = new ExhibitionStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                return IExhibitionStaffGuestParticipatingStatusLogRepository.GetById(itemId);
            }
        }
        
        public List<XsiExhibitionStaffGuestParticipatingStatusLogs> GetExhibitionStaffGuestParticipatingStatusLog()
        {
            using (IExhibitionStaffGuestParticipatingStatusLogRepository = new ExhibitionStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                return IExhibitionStaffGuestParticipatingStatusLogRepository.Select();
            }
        }
        public List<XsiExhibitionStaffGuestParticipatingStatusLogs> GetExhibitionStaffGuestParticipatingStatusLog(EnumSortlistBy sortListBy)
        {
            using (IExhibitionStaffGuestParticipatingStatusLogRepository = new ExhibitionStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitionStaffGuestParticipatingStatusLogRepository.Select().OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitionStaffGuestParticipatingStatusLogRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionStaffGuestParticipatingStatusLogRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionStaffGuestParticipatingStatusLogRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionStaffGuestParticipatingStatusLogRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionStaffGuestParticipatingStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionStaffGuestParticipatingStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionStaffGuestParticipatingStatusLogs> GetExhibitionStaffGuestParticipatingStatusLog(XsiExhibitionStaffGuestParticipatingStatusLogs entity)
        {
            using (IExhibitionStaffGuestParticipatingStatusLogRepository = new ExhibitionStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                return IExhibitionStaffGuestParticipatingStatusLogRepository.Select(entity);
            }
        }
        public List<XsiExhibitionStaffGuestParticipatingStatusLogs> GetExhibitionStaffGuestParticipatingStatusLog(XsiExhibitionStaffGuestParticipatingStatusLogs entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionStaffGuestParticipatingStatusLogRepository = new ExhibitionStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitionStaffGuestParticipatingStatusLogRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitionStaffGuestParticipatingStatusLogRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionStaffGuestParticipatingStatusLogRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionStaffGuestParticipatingStatusLogRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionStaffGuestParticipatingStatusLogRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionStaffGuestParticipatingStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionStaffGuestParticipatingStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionStaffGuestParticipatingStatusLogs GetExhibitionStaffGuestParticipatingStatusLog(long groupId, long languageId)
        {
            using (IExhibitionStaffGuestParticipatingStatusLogRepository = new ExhibitionStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionStaffGuestParticipatingStatusLog(XsiExhibitionStaffGuestParticipatingStatusLogs entity)
        {
            using (IExhibitionStaffGuestParticipatingStatusLogRepository = new ExhibitionStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                entity = IExhibitionStaffGuestParticipatingStatusLogRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId =  -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionStaffGuestParticipatingStatusLog(XsiExhibitionStaffGuestParticipatingStatusLogs entity)
        {
            XsiExhibitionStaffGuestParticipatingStatusLogs OriginalEntity = GetExhibitionStaffGuestParticipatingStatusLogByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IExhibitionStaffGuestParticipatingStatusLogRepository = new ExhibitionStaffGuestParticipatingStatusLogRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line
                    
                    IExhibitionStaffGuestParticipatingStatusLogRepository.Update(entity);
                    if (IExhibitionStaffGuestParticipatingStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionStaffGuestParticipatingStatusLogStatus(long itemId)
        {
            XsiExhibitionStaffGuestParticipatingStatusLogs entity = GetExhibitionStaffGuestParticipatingStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionStaffGuestParticipatingStatusLogRepository = new ExhibitionStaffGuestParticipatingStatusLogRepository(ConnectionString))
                {
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionStaffGuestParticipatingStatusLogRepository.Update(entity);
                    if (IExhibitionStaffGuestParticipatingStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionStaffGuestParticipatingStatusLog(XsiExhibitionStaffGuestParticipatingStatusLogs entity)
        {
            List<XsiExhibitionStaffGuestParticipatingStatusLogs> entities = GetExhibitionStaffGuestParticipatingStatusLog(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionStaffGuestParticipatingStatusLogs e in entities)
                {
                    result = DeleteExhibitionStaffGuestParticipatingStatusLog(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionStaffGuestParticipatingStatusLog(long itemId)
        {
            XsiExhibitionStaffGuestParticipatingStatusLogs entity = GetExhibitionStaffGuestParticipatingStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionStaffGuestParticipatingStatusLogRepository = new ExhibitionStaffGuestParticipatingStatusLogRepository(ConnectionString))
                {
                    IExhibitionStaffGuestParticipatingStatusLogRepository.Delete(itemId);
                    if (IExhibitionStaffGuestParticipatingStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
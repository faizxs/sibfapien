﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionBookTypeBL : IDisposable
    {
        #region Feilds
        IExhibitionBookTypeRepository ExhibitionBookTypeRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionBookTypeBL()
        {
            ConnectionString = null;
        }
        public ExhibitionBookTypeBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionBookTypeRepository != null)
                this.ExhibitionBookTypeRepository.Dispose();
        }
        #endregion
        #region Methods
      
        public XsiExhibitionBookType GetExhibitionBookTypeByItemId(long itemId)
        {
            using (ExhibitionBookTypeRepository = new ExhibitionBookTypeRepository(ConnectionString))
            {
                return ExhibitionBookTypeRepository.GetById(itemId);
            }
        }
      
        public List<XsiExhibitionBookType> GetExhibitionBookType()
        {
            using (ExhibitionBookTypeRepository = new ExhibitionBookTypeRepository(ConnectionString))
            {
                return ExhibitionBookTypeRepository.Select();
            }
        }
        public List<XsiExhibitionBookType> GetExhibitionBookType(EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookTypeRepository = new ExhibitionBookTypeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookTypeRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookTypeRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookTypeRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookTypeRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookTypeRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookTypeRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBookTypeRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionBookType> GetExhibitionBookType(XsiExhibitionBookType entity)
        {
            using (ExhibitionBookTypeRepository = new ExhibitionBookTypeRepository(ConnectionString))
            {
                return ExhibitionBookTypeRepository.Select(entity);
            }
        }
        public List<XsiExhibitionBookType> GetExhibitionBookType(XsiExhibitionBookType entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookTypeRepository = new ExhibitionBookTypeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookTypeRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookTypeRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookTypeRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookTypeRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookTypeRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookTypeRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBookTypeRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiExhibitionBookType> GetExhibitionBookTypeOr(XsiExhibitionBookType entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookTypeRepository = new ExhibitionBookTypeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookTypeRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookTypeRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookTypeRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookTypeRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookTypeRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookTypeRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBookTypeRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public EnumResultType InsertExhibitionBookType(XsiExhibitionBookType entity)
        {
            using (ExhibitionBookTypeRepository = new ExhibitionBookTypeRepository(ConnectionString))
            {
                entity = ExhibitionBookTypeRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionBookType(XsiExhibitionBookType entity)
        {
            XsiExhibitionBookType OriginalEntity = GetExhibitionBookTypeByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionBookTypeRepository = new ExhibitionBookTypeRepository(ConnectionString))
                {

                    ExhibitionBookTypeRepository.Update(entity);
                    if (ExhibitionBookTypeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        
        public EnumResultType UpdateExhibitionBookTypeStatus(long itemId)
        {
            XsiExhibitionBookType entity = GetExhibitionBookTypeByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionBookTypeRepository = new ExhibitionBookTypeRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionBookTypeRepository.Update(entity);
                    if (ExhibitionBookTypeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionBookType(XsiExhibitionBookType entity)
        {
            List<XsiExhibitionBookType> entities = GetExhibitionBookType(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionBookType e in entities)
                {
                    result = DeleteExhibitionBookType(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionBookType(long itemId)
        {
            XsiExhibitionBookType entity = GetExhibitionBookTypeByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionBookTypeRepository = new ExhibitionBookTypeRepository(ConnectionString))
                {
                    ExhibitionBookTypeRepository.Delete(itemId);
                    if (ExhibitionBookTypeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
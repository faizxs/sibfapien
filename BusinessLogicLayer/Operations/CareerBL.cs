﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class CareerBL : IDisposable
    {
        #region Feilds
        ICareerRepository CareerRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public CareerBL()
        {
            ConnectionString = null;
        }
        public CareerBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerRepository != null)
                this.CareerRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiCareer GetCareerByItemId(long itemId)
        {
            using (CareerRepository = new CareerRepository(ConnectionString))
            {
                return CareerRepository.GetById(itemId);
            }
        }
        
        public List<XsiCareer> GetCareer()
        {
            using (CareerRepository = new CareerRepository(ConnectionString))
            {
                return CareerRepository.Select();
            }
        }
        public List<XsiCareer> GetCareer(EnumSortlistBy sortListBy)
        {
            using (CareerRepository = new CareerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareer> GetCareer(XsiCareer entity)
        {
            using (CareerRepository = new CareerRepository(ConnectionString))
            {
                return CareerRepository.Select(entity);
            }
        }
        public List<XsiCareer> GetCareer(XsiCareer entity, EnumSortlistBy sortListBy)
        {
            using (CareerRepository = new CareerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareer> GetCareerOr(XsiCareer entity, EnumSortlistBy sortListBy)
        {
            using (CareerRepository = new CareerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public EnumResultType InsertCareer(XsiCareer entity)
        {
            using (CareerRepository = new CareerRepository(ConnectionString))
            {
                entity = CareerRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateCareer(XsiCareer entity)
        {
            XsiCareer OriginalEntity = GetCareerByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (CareerRepository = new CareerRepository(ConnectionString))
                {
                    CareerRepository.Update(entity);
                    if (CareerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateCareerStatus(long itemId)
        {
            XsiCareer entity = GetCareerByItemId(itemId);
            if (entity != null)
            {
                using (CareerRepository = new CareerRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    CareerRepository.Update(entity);
                    if (CareerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteCareer(XsiCareer entity)
        {
            List<XsiCareer> entities = GetCareer(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiCareer e in entities)
                {
                    result = DeleteCareer(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteCareer(long itemId)
        {
            XsiCareer entity = GetCareerByItemId(itemId);
            if (entity != null)
            {
                using (CareerRepository = new CareerRepository(ConnectionString))
                {
                    CareerRepository.Delete(itemId);
                    if (CareerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
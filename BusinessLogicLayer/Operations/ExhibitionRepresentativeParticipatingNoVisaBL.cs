﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionRepresentativeParticipatingNoVisaBL : IDisposable
    {
        #region Feilds
        IExhibitionRepresentativeParticipatingNoVisaRepository IExhibitionRepresentativeParticipatingNoVisaRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionRepresentativeParticipatingNoVisaBL()
        {
            ConnectionString = null;
        }
        public ExhibitionRepresentativeParticipatingNoVisaBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionRepresentativeParticipatingNoVisaRepository != null)
                this.IExhibitionRepresentativeParticipatingNoVisaRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionRepresentativeParticipatingNoVisaRepository = new ExhibitionRepresentativeParticipatingNoVisaRepository(ConnectionString))
            {
                return  -1;
            }
        }

        public XsiExhibitionRepresentativeParticipatingNoVisa GetExhibitionRepresentativeParticipatingNoVisaByItemId(long itemId)
        {
            using (IExhibitionRepresentativeParticipatingNoVisaRepository = new ExhibitionRepresentativeParticipatingNoVisaRepository(ConnectionString))
            {
                return IExhibitionRepresentativeParticipatingNoVisaRepository.GetById(itemId);
            }
        }

        public XsiExhibitionRepresentativeParticipatingNoVisa GetByRepresentativeId(long itemId)
        {
            using (IExhibitionRepresentativeParticipatingNoVisaRepository = new ExhibitionRepresentativeParticipatingNoVisaRepository(ConnectionString))
            {
                return IExhibitionRepresentativeParticipatingNoVisaRepository.GetByRepresentativeId(itemId);
            }
        }
        public List<XsiExhibitionRepresentativeParticipatingNoVisa> GetExhibitionRepresentativeParticipatingNoVisa()
        {
            using (IExhibitionRepresentativeParticipatingNoVisaRepository = new ExhibitionRepresentativeParticipatingNoVisaRepository(ConnectionString))
            {
                return IExhibitionRepresentativeParticipatingNoVisaRepository.Select();
            }
        }
        public List<XsiExhibitionRepresentativeParticipatingNoVisa> GetExhibitionRepresentativeParticipatingNoVisa(EnumSortlistBy sortListBy)
        {
            using (IExhibitionRepresentativeParticipatingNoVisaRepository = new ExhibitionRepresentativeParticipatingNoVisaRepository(ConnectionString))
            {
                switch (sortListBy)
                {

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionRepresentativeParticipatingNoVisaRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionRepresentativeParticipatingNoVisaRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionRepresentativeParticipatingNoVisaRepository.Select().OrderBy(p => p.RepresentativeId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionRepresentativeParticipatingNoVisaRepository.Select().OrderByDescending(p => p.RepresentativeId).ToList();

                    default:
                        return IExhibitionRepresentativeParticipatingNoVisaRepository.Select().OrderByDescending(p => p.RepresentativeId).ToList();
                }
            }
        }
        public List<XsiExhibitionRepresentativeParticipatingNoVisa> GetExhibitionRepresentativeParticipatingNoVisa(XsiExhibitionRepresentativeParticipatingNoVisa entity)
        {
            using (IExhibitionRepresentativeParticipatingNoVisaRepository = new ExhibitionRepresentativeParticipatingNoVisaRepository(ConnectionString))
            {
                return IExhibitionRepresentativeParticipatingNoVisaRepository.Select(entity);
            }
        }
        public List<XsiExhibitionRepresentativeParticipatingNoVisa> GetExhibitionRepresentativeParticipatingNoVisa(XsiExhibitionRepresentativeParticipatingNoVisa entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionRepresentativeParticipatingNoVisaRepository = new ExhibitionRepresentativeParticipatingNoVisaRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionRepresentativeParticipatingNoVisaRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionRepresentativeParticipatingNoVisaRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionRepresentativeParticipatingNoVisaRepository.Select(entity).OrderBy(p => p.RepresentativeId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionRepresentativeParticipatingNoVisaRepository.Select(entity).OrderByDescending(p => p.RepresentativeId).ToList();

                    default:
                        return IExhibitionRepresentativeParticipatingNoVisaRepository.Select(entity).OrderByDescending(p => p.RepresentativeId).ToList();
                }
            }
        }
        public XsiExhibitionRepresentativeParticipatingNoVisa GetExhibitionRepresentativeParticipatingNoVisa(long groupId, long languageId)
        {
            using (IExhibitionRepresentativeParticipatingNoVisaRepository = new ExhibitionRepresentativeParticipatingNoVisaRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionRepresentativeParticipatingNoVisa(XsiExhibitionRepresentativeParticipatingNoVisa entity)
        {
            using (IExhibitionRepresentativeParticipatingNoVisaRepository = new ExhibitionRepresentativeParticipatingNoVisaRepository(ConnectionString))
            {
                entity = IExhibitionRepresentativeParticipatingNoVisaRepository.Add(entity);
                if (entity.RepresentativeId > 0)
                {
                    XsiItemdId = entity.RepresentativeId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionRepresentativeParticipatingNoVisa(XsiExhibitionRepresentativeParticipatingNoVisa entity)
        {
            if (entity != null)
            {
                using (IExhibitionRepresentativeParticipatingNoVisaRepository = new ExhibitionRepresentativeParticipatingNoVisaRepository(ConnectionString))
                {

                    IExhibitionRepresentativeParticipatingNoVisaRepository.Update(entity);
                    if (IExhibitionRepresentativeParticipatingNoVisaRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackExhibitionRepresentativeParticipatingNoVisa(long itemId)
        {
            //if no RollbackXml propery in ExhibitionRepresentativeParticipatingNoVisa then delete below code and return EnumResultType.Invalid;

            return EnumResultType.Failed;

        }
        public EnumResultType UpdateExhibitionRepresentativeParticipatingNoVisaStatus(long itemId)
        {
            XsiExhibitionRepresentativeParticipatingNoVisa entity = GetExhibitionRepresentativeParticipatingNoVisaByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionRepresentativeParticipatingNoVisaRepository = new ExhibitionRepresentativeParticipatingNoVisaRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionRepresentativeParticipatingNoVisaRepository.Update(entity);
                    if (IExhibitionRepresentativeParticipatingNoVisaRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionRepresentativeParticipatingNoVisa(XsiExhibitionRepresentativeParticipatingNoVisa entity)
        {
            List<XsiExhibitionRepresentativeParticipatingNoVisa> entities = GetExhibitionRepresentativeParticipatingNoVisa(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionRepresentativeParticipatingNoVisa e in entities)
                {
                    result = DeleteExhibitionRepresentativeParticipatingNoVisa(e.RepresentativeId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionRepresentativeParticipatingNoVisa(long itemId)
        {
            XsiExhibitionRepresentativeParticipatingNoVisa entity = GetExhibitionRepresentativeParticipatingNoVisaByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionRepresentativeParticipatingNoVisaRepository = new ExhibitionRepresentativeParticipatingNoVisaRepository(ConnectionString))
                {
                    IExhibitionRepresentativeParticipatingNoVisaRepository.Delete(itemId);
                    if (IExhibitionRepresentativeParticipatingNoVisaRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
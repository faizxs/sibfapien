﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFContactusEnquiryBL : IDisposable
    {
        #region Feilds
        ISCRFContactusEnquiryRepository SCRFContactusEnquiryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFContactusEnquiryBL()
        {
            ConnectionString = null;
        }
        public SCRFContactusEnquiryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFContactusEnquiryRepository != null)
                this.SCRFContactusEnquiryRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfcontactusEnquiry GetSCRFContactusEnquiryByItemId(long itemId)
        {
            using (SCRFContactusEnquiryRepository = new SCRFContactusEnquiryRepository(ConnectionString))
            {
                return SCRFContactusEnquiryRepository.GetById(itemId);
            }
        }

        public List<XsiScrfcontactusEnquiry> GetSCRFContactusEnquiry()
        {
            using (SCRFContactusEnquiryRepository = new SCRFContactusEnquiryRepository(ConnectionString))
            {
                return SCRFContactusEnquiryRepository.Select();
            }
        }
        public List<XsiScrfcontactusEnquiry> GetSCRFContactusEnquiry(EnumSortlistBy sortListBy)
        {
            using (SCRFContactusEnquiryRepository = new SCRFContactusEnquiryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFContactusEnquiryRepository.Select().OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFContactusEnquiryRepository.Select().OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFContactusEnquiryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFContactusEnquiryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFContactusEnquiryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfcontactusEnquiry> GetSCRFContactusEnquiry(XsiScrfcontactusEnquiry entity)
        {
            using (SCRFContactusEnquiryRepository = new SCRFContactusEnquiryRepository(ConnectionString))
            {
                return SCRFContactusEnquiryRepository.Select(entity);
            }
        }
        public List<XsiScrfcontactusEnquiry> GetSCRFContactusEnquiry(XsiScrfcontactusEnquiry entity, EnumSortlistBy sortListBy)
        {
            using (SCRFContactusEnquiryRepository = new SCRFContactusEnquiryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFContactusEnquiryRepository.Select(entity).OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFContactusEnquiryRepository.Select(entity).OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFContactusEnquiryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFContactusEnquiryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFContactusEnquiryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfcontactusEnquiry> GetSCRFContactusEnquiryOr(XsiScrfcontactusEnquiry entity, EnumSortlistBy sortListBy)
        {
            using (SCRFContactusEnquiryRepository = new SCRFContactusEnquiryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFContactusEnquiryRepository.SelectOr(entity).OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFContactusEnquiryRepository.SelectOr(entity).OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFContactusEnquiryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFContactusEnquiryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFContactusEnquiryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFContactusEnquiry(XsiScrfcontactusEnquiry entity)
        {
            using (SCRFContactusEnquiryRepository = new SCRFContactusEnquiryRepository(ConnectionString))
            {
                entity = SCRFContactusEnquiryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFContactusEnquiry(XsiScrfcontactusEnquiry entity)
        {
            XsiScrfcontactusEnquiry OriginalEntity = GetSCRFContactusEnquiryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFContactusEnquiryRepository = new SCRFContactusEnquiryRepository(ConnectionString))
                {
                    SCRFContactusEnquiryRepository.Update(entity);
                    if (SCRFContactusEnquiryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFContactusEnquiryStatus(long itemId)
        {
            XsiScrfcontactusEnquiry entity = GetSCRFContactusEnquiryByItemId(itemId);
            if (entity != null)
            {
                using (SCRFContactusEnquiryRepository = new SCRFContactusEnquiryRepository(ConnectionString))
                {
                    if (EnumConversion.ToString(EnumStatus.Open) == entity.Status)
                    {
                        entity.Status = EnumConversion.ToString(EnumStatus.CLose);
                    }
                    else
                    {
                        entity.Status = EnumConversion.ToString(EnumStatus.CLose);
                        entity.DateClosed = DateTime.Now.Date;
                    }

                    SCRFContactusEnquiryRepository.Update(entity);
                    if (SCRFContactusEnquiryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFContactusEnquiryFlag(long itemId, string flag)
        {
            XsiScrfcontactusEnquiry entity = GetSCRFContactusEnquiryByItemId(itemId);
            if (entity != null)
            {
                using (SCRFContactusEnquiryRepository = new SCRFContactusEnquiryRepository(ConnectionString))
                {
                    entity.FlagType = flag;
                    SCRFContactusEnquiryRepository.Update(entity);
                    if (SCRFContactusEnquiryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFContactusEnquiry(XsiScrfcontactusEnquiry entity)
        {
            if (GetSCRFContactusEnquiry(entity).Count() > 0)
            {
                using (SCRFContactusEnquiryRepository = new SCRFContactusEnquiryRepository(ConnectionString))
                {
                    SCRFContactusEnquiryRepository.Delete(entity);
                    if (SCRFContactusEnquiryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFContactusEnquiry(long itemId)
        {
            if (GetSCRFContactusEnquiryByItemId(itemId) != null)
            {
                using (SCRFContactusEnquiryRepository = new SCRFContactusEnquiryRepository(ConnectionString))
                {
                    SCRFContactusEnquiryRepository.Delete(itemId);
                    if (SCRFContactusEnquiryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
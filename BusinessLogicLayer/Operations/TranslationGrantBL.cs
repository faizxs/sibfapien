﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class TranslationGrantBL : IDisposable
    {
        #region Feilds
        ITranslationGrantRepository TranslationGrantRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public TranslationGrantBL()
        {
            ConnectionString = null;
        }
        public TranslationGrantBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.TranslationGrantRepository != null)
                this.TranslationGrantRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionTranslationGrant GetTranslationGrantByItemId(long itemId)
        {
            using (TranslationGrantRepository = new TranslationGrantRepository(ConnectionString))
            {
                return TranslationGrantRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionTranslationGrant> GetTranslationGrant()
        {
            using (TranslationGrantRepository = new TranslationGrantRepository(ConnectionString))
            {
                return TranslationGrantRepository.Select();
            }
        }
        public List<XsiExhibitionTranslationGrant> GetTranslationGrant(EnumSortlistBy sortListBy)
        {
            using (TranslationGrantRepository = new TranslationGrantRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return TranslationGrantRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return TranslationGrantRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return TranslationGrantRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return TranslationGrantRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return TranslationGrantRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return TranslationGrantRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return TranslationGrantRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionTranslationGrant> GetTranslationGrant(XsiExhibitionTranslationGrant entity)
        {
            using (TranslationGrantRepository = new TranslationGrantRepository(ConnectionString))
            {
                return TranslationGrantRepository.Select(entity);
            }
        }
        public List<XsiExhibitionTranslationGrant> GetTranslationGrant(XsiExhibitionTranslationGrant entity, EnumSortlistBy sortListBy)
        {
            using (TranslationGrantRepository = new TranslationGrantRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return TranslationGrantRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return TranslationGrantRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return TranslationGrantRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return TranslationGrantRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return TranslationGrantRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return TranslationGrantRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return TranslationGrantRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertTranslationGrant(XsiExhibitionTranslationGrant entity)
        {
            using (TranslationGrantRepository = new TranslationGrantRepository(ConnectionString))
            {
                entity = TranslationGrantRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateTranslationGrant(XsiExhibitionTranslationGrant entity)
        {
            XsiExhibitionTranslationGrant OriginalEntity = GetTranslationGrantByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (TranslationGrantRepository = new TranslationGrantRepository(ConnectionString))
                {
                    TranslationGrantRepository.Update(entity);
                    if (TranslationGrantRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateTranslationGrantStatus(long itemId)
        {
            EnumResultType XsiResult = EnumResultType.NotFound;
            XsiExhibitionTranslationGrant entity = GetTranslationGrantByItemId(itemId);
            if (entity != null)
            {
                using (TranslationGrantRepository = new TranslationGrantRepository(ConnectionString))
                {
                    string str = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    XsiExhibitionTranslationGrant entityList = GetTranslationGrantByItemId(entity.ItemId);
                    XsiExhibitionTranslationGrant TranslationGrantEntity = GetTranslationGrantByItemId(entity.ItemId);
                    if (TranslationGrantEntity != null)
                    {
                        using (TranslationGrantRepository = new TranslationGrantRepository(ConnectionString))
                        {
                            TranslationGrantEntity.IsActive = str;
                            TranslationGrantRepository.Update(TranslationGrantEntity);
                            if (TranslationGrantRepository.SubmitChanges() > 0)
                                XsiResult = EnumResultType.Success;
                            else
                                XsiResult = EnumResultType.Failed;
                        }
                    }
                    else
                        return XsiResult;
                }
            }
            return XsiResult;
            /*
            XsiExhibitionTranslationGrant entity = GetTranslationGrantByItemId(itemId);
            if (entity != null)
            {
                using (TranslationGrantRepository = new TranslationGrantRepository(ConnectionString))
                {

                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    TranslationGrantRepository.Update(entity);
                    if (TranslationGrantRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;*/
        }
        public EnumResultType DeleteTranslationGrant(XsiExhibitionTranslationGrant entity)
        {
            List<XsiExhibitionTranslationGrant> entities = GetTranslationGrant(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionTranslationGrant e in entities)
                {
                    result = DeleteTranslationGrant(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteTranslationGrant(long itemId)
        {
            XsiExhibitionTranslationGrant entity = GetTranslationGrantByItemId(itemId);
            if (entity != null)
            {
                using (TranslationGrantRepository = new TranslationGrantRepository(ConnectionString))
                {
                    TranslationGrantRepository.Delete(itemId);
                    if (TranslationGrantRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
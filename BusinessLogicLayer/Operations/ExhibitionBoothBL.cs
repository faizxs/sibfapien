﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionBoothBL : IDisposable
    {
        #region Feilds
        IExhibitionBoothRepository ExhibitionBoothRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionBoothBL()
        {
            ConnectionString = null;
        }
        public ExhibitionBoothBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionBoothRepository != null)
                this.ExhibitionBoothRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionBooth GetExhibitionBoothByItemId(long itemId)
        {
            using (ExhibitionBoothRepository = new ExhibitionBoothRepository(ConnectionString))
            {
                return ExhibitionBoothRepository.GetById(itemId);
            }
        }
        
        public List<XsiExhibitionBooth> GetExhibitionBooth()
        {
            using (ExhibitionBoothRepository = new ExhibitionBoothRepository(ConnectionString))
            {
                return ExhibitionBoothRepository.Select();
            }
        }
        public List<XsiExhibitionBooth> GetExhibitionBooth(EnumSortlistBy sortListBy)
        {
            using (ExhibitionBoothRepository = new ExhibitionBoothRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBoothRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBoothRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBoothRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBoothRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBoothRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBoothRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBoothRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionBooth> GetExhibitionBooth(XsiExhibitionBooth entity)
        {
            using (ExhibitionBoothRepository = new ExhibitionBoothRepository(ConnectionString))
            {
                return ExhibitionBoothRepository.Select(entity);
            }
        }
        public List<XsiExhibitionBooth> GetExhibitionBooth(XsiExhibitionBooth entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBoothRepository = new ExhibitionBoothRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBoothRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBoothRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBoothRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBoothRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBoothRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBoothRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBoothRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiExhibitionBooth> GetExhibitionBoothOr(XsiExhibitionBooth entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBoothRepository = new ExhibitionBoothRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBoothRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBoothRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBoothRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBoothRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBoothRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBoothRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBoothRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public List<XsiExhibitionBooth> GetOtherLanguageExhibitionBooth(XsiExhibitionBooth entity)
        {
            using (ExhibitionBoothRepository = new ExhibitionBoothRepository(ConnectionString))
            {
                return ExhibitionBoothRepository.SelectOtherLanguageCategory(entity);
            }
        }
        public List<XsiExhibitionBooth> GetCurrentLanguageExhibitionBooth(XsiExhibitionBooth entity)
        {
            using (ExhibitionBoothRepository = new ExhibitionBoothRepository(ConnectionString))
            {
                return ExhibitionBoothRepository.SelectCurrentLanguageCategory(entity);
            }
        }
        public List<XsiExhibitionBooth> GetCompleteExhibitionBooth(XsiExhibitionBooth entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBoothRepository = new ExhibitionBoothRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBoothRepository.SelectComplete(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBoothRepository.SelectComplete(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBoothRepository.SelectComplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBoothRepository.SelectComplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBoothRepository.SelectComplete(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBoothRepository.SelectComplete(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBoothRepository.SelectComplete(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertExhibitionBooth(XsiExhibitionBooth entity)
        {
            using (ExhibitionBoothRepository = new ExhibitionBoothRepository(ConnectionString))
            {
                entity = ExhibitionBoothRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionBooth(XsiExhibitionBooth entity)
        {
            XsiExhibitionBooth OriginalEntity = GetExhibitionBoothByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionBoothRepository = new ExhibitionBoothRepository(ConnectionString))
                {
                    ExhibitionBoothRepository.Update(entity);
                    if (ExhibitionBoothRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionBoothStatus(long itemId)
        {
            XsiExhibitionBooth entity = GetExhibitionBoothByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionBoothRepository = new ExhibitionBoothRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionBoothRepository.Update(entity);
                    if (ExhibitionBoothRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionBooth(XsiExhibitionBooth entity)
        {
            List<XsiExhibitionBooth> entities = GetExhibitionBooth(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionBooth e in entities)
                {
                    result = DeleteExhibitionBooth(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionBooth(long itemId)
        {
            XsiExhibitionBooth entity = GetExhibitionBoothByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionBoothRepository = new ExhibitionBoothRepository(ConnectionString))
                {
                    ExhibitionBoothRepository.Delete(itemId);
                    if (ExhibitionBoothRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
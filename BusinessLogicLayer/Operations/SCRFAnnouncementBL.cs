﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFAnnouncementBL : IDisposable
    {
        #region Feilds
        ISCRFAnnouncementRepository SCRFAnnouncementRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFAnnouncementBL()
        {
            ConnectionString = null;
        }
        public SCRFAnnouncementBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFAnnouncementRepository != null)
                this.SCRFAnnouncementRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrfannouncement GetSCRFAnnouncementByItemId(long itemId)
        {
            using (SCRFAnnouncementRepository = new SCRFAnnouncementRepository(ConnectionString))
            {
                return SCRFAnnouncementRepository.GetById(itemId);
            }
        }

        public List<XsiScrfannouncement> GetSCRFAnnouncement()
        {
            using (SCRFAnnouncementRepository = new SCRFAnnouncementRepository(ConnectionString))
            {
                return SCRFAnnouncementRepository.Select();
            }
        }
        public List<XsiScrfannouncement> GetSCRFAnnouncement(EnumSortlistBy sortListBy)
        {
            using (SCRFAnnouncementRepository = new SCRFAnnouncementRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFAnnouncementRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFAnnouncementRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFAnnouncementRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFAnnouncementRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFAnnouncementRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFAnnouncementRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFAnnouncementRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfannouncement> GetSCRFAnnouncement(XsiScrfannouncement entity)
        {
            using (SCRFAnnouncementRepository = new SCRFAnnouncementRepository(ConnectionString))
            {
                return SCRFAnnouncementRepository.Select(entity);
            }
        }
        public List<XsiScrfannouncement> GetSCRFAnnouncement(XsiScrfannouncement entity, EnumSortlistBy sortListBy)
        {
            using (SCRFAnnouncementRepository = new SCRFAnnouncementRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFAnnouncementRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFAnnouncementRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFAnnouncementRepository.Select(entity).OrderBy(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFAnnouncementRepository.Select(entity).OrderByDescending(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFAnnouncementRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFAnnouncementRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFAnnouncementRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfannouncement> GetSCRFAnnouncementOr(XsiScrfannouncement entity, EnumSortlistBy sortListBy)
        {
            using (SCRFAnnouncementRepository = new SCRFAnnouncementRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFAnnouncementRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFAnnouncementRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFAnnouncementRepository.SelectOr(entity).OrderBy(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFAnnouncementRepository.SelectOr(entity).OrderByDescending(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFAnnouncementRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFAnnouncementRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFAnnouncementRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFAnnouncement(XsiScrfannouncement entity)
        {
            using (SCRFAnnouncementRepository = new SCRFAnnouncementRepository(ConnectionString))
            {
                entity = SCRFAnnouncementRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFAnnouncement(XsiScrfannouncement entity)
        {
            XsiScrfannouncement OriginalEntity = GetSCRFAnnouncementByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFAnnouncementRepository = new SCRFAnnouncementRepository(ConnectionString))
                {
                    SCRFAnnouncementRepository.Update(entity);
                    if (SCRFAnnouncementRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFAnnouncementStatus(long itemId)
        {
            XsiScrfannouncement entity = GetSCRFAnnouncementByItemId(itemId);
            if (entity != null)
            {
                using (SCRFAnnouncementRepository = new SCRFAnnouncementRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFAnnouncementRepository.Update(entity);
                    if (SCRFAnnouncementRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFAnnouncement(XsiScrfannouncement entity)
        {
            List<XsiScrfannouncement> entities = GetSCRFAnnouncement(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfannouncement e in entities)
                {
                    result = DeleteSCRFAnnouncement(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFAnnouncement(long itemId)
        {
            XsiScrfannouncement entity = GetSCRFAnnouncementByItemId(itemId);
            if (entity != null)
            {
                using (SCRFAnnouncementRepository = new SCRFAnnouncementRepository(ConnectionString))
                {
                    SCRFAnnouncementRepository.Delete(itemId);
                    if (SCRFAnnouncementRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ProfessionalProgramRegistrationBL : IDisposable
    {
        #region Feilds
        IProfessionalProgramRegistrationRepository IProfessionalProgramRegistrationRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ProfessionalProgramRegistrationBL()
        {
            ConnectionString = null;
        }
        public ProfessionalProgramRegistrationBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IProfessionalProgramRegistrationRepository != null)
                this.IProfessionalProgramRegistrationRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionProfessionalProgramRegistration GetProfessionalProgramRegistrationByItemId(long itemId)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                return IProfessionalProgramRegistrationRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionProfessionalProgramRegistration> GetProfessionalProgramRegistration()
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                return IProfessionalProgramRegistrationRepository.Select();
            }
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetProfessionalProgramRegistration(EnumSortlistBy sortListBy)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IProfessionalProgramRegistrationRepository.Select().OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IProfessionalProgramRegistrationRepository.Select().OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IProfessionalProgramRegistrationRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IProfessionalProgramRegistrationRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IProfessionalProgramRegistrationRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IProfessionalProgramRegistrationRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IProfessionalProgramRegistrationRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetProfessionalProgramRegistration(XsiExhibitionProfessionalProgramRegistration entity)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                return IProfessionalProgramRegistrationRepository.Select(entity);
            }
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetProfessionalProgramRegistration(XsiExhibitionProfessionalProgramRegistration entity, EnumSortlistBy sortListBy)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IProfessionalProgramRegistrationRepository.Select(entity).OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IProfessionalProgramRegistrationRepository.Select(entity).OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IProfessionalProgramRegistrationRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IProfessionalProgramRegistrationRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IProfessionalProgramRegistrationRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IProfessionalProgramRegistrationRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IProfessionalProgramRegistrationRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetProfessionalProgramRegistrationCMS(XsiExhibitionProfessionalProgramRegistration entity, EnumSortlistBy sortListBy)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IProfessionalProgramRegistrationRepository.SelectPPRegistrationCMS(entity).OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IProfessionalProgramRegistrationRepository.SelectPPRegistrationCMS(entity).OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IProfessionalProgramRegistrationRepository.SelectPPRegistrationCMS(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IProfessionalProgramRegistrationRepository.SelectPPRegistrationCMS(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IProfessionalProgramRegistrationRepository.SelectPPRegistrationCMS(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IProfessionalProgramRegistrationRepository.SelectPPRegistrationCMS(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IProfessionalProgramRegistrationRepository.SelectPPRegistrationCMS(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionProfessionalProgramRegistration GetProfessionalProgramRegistration(long groupId, long languageId)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                return null;
            }
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetSuggestedProfessionalProgramRegistration(XsiExhibitionProfessionalProgramRegistration entity)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                return IProfessionalProgramRegistrationRepository.SelectSuggestedProfessionals(entity);
            }
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetOtherProfessionals(List<long?> genreGroupIDList, List<long> suggestedIDList, XsiExhibitionProfessionalProgramRegistration entity)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                return IProfessionalProgramRegistrationRepository.SelectOtherProfessionals(genreGroupIDList, suggestedIDList, entity);
            }
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetOtherProfessionalsSearch(List<long?> genreGroupIDList, List<long> suggestedIDList, XsiExhibitionProfessionalProgramRegistration entity, string name)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                return IProfessionalProgramRegistrationRepository.SelectOtherProfessionalsSearch(genreGroupIDList, suggestedIDList, entity, name);
            }
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetOtherProfessionals(XsiExhibitionProfessionalProgramRegistration entity)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                return IProfessionalProgramRegistrationRepository.SelectOtherProfessionals(entity);
            }
        }
        public List<XsiExhibitionProfessionalProgramRegistration> GetOtherProfessionalsSearch(XsiExhibitionProfessionalProgramRegistration entity, string name)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                return IProfessionalProgramRegistrationRepository.SelectOtherProfessionalsSearch(entity, name);
            }
        }
        /*public List<GetApporvedMeetUps_Result> GetApprovedMeetUps(long professionalProgramGroupID)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                return IProfessionalProgramRegistrationRepository.SelectApprovedMeetups(professionalProgramGroupID);
            }
        }
        public List<GetApporvedMeetUpsByMember_Result> GetApprovedMeetUpsByMember(long professionalProgramGroupID, long profesionalProgramRegistrationID)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                return IProfessionalProgramRegistrationRepository.SelectApprovedMeetupsByMember(professionalProgramGroupID, profesionalProgramRegistrationID);
            }
        }*/


        public EnumResultType InsertProfessionalProgramRegistration(XsiExhibitionProfessionalProgramRegistration entity)
        {
            using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
            {
                XsiExhibitionProfessionalProgramRegistration where = new XsiExhibitionProfessionalProgramRegistration();
                where.MemberId = entity.MemberId;
                where.ProfessionalProgramId = entity.ProfessionalProgramId;
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);

                if (IProfessionalProgramRegistrationRepository.Select(where).Count() == 0)
                {
                    entity = IProfessionalProgramRegistrationRepository.Add(entity);
                    if (entity.ItemId > 0)
                    {
                        XsiItemdId = entity.ItemId;
                        XsiGroupId = -1;
                        return EnumResultType.Success;
                    }
                    else
                    {
                        XsiItemdId = -1;
                        XsiGroupId = -1;
                        return EnumResultType.Failed;
                    }
                }
                else
                    return EnumResultType.AlreadyExists;
            }
        }
        public EnumResultType UpdateProfessionalProgramRegistration(XsiExhibitionProfessionalProgramRegistration entity)
        {
            XsiExhibitionProfessionalProgramRegistration OriginalEntity = GetProfessionalProgramRegistrationByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line

                    IProfessionalProgramRegistrationRepository.Update(entity);
                    if (IProfessionalProgramRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateProfessionalProgramRegistrationStatus(long itemId)
        {
            XsiExhibitionProfessionalProgramRegistration entity = GetProfessionalProgramRegistrationByItemId(itemId);
            if (entity != null)
            {
                using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IProfessionalProgramRegistrationRepository.Update(entity);
                    if (IProfessionalProgramRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteProfessionalProgramRegistration(XsiExhibitionProfessionalProgramRegistration entity)
        {
            List<XsiExhibitionProfessionalProgramRegistration> entities = GetProfessionalProgramRegistration(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionProfessionalProgramRegistration e in entities)
                {
                    result = DeleteProfessionalProgramRegistration(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteProfessionalProgramRegistration(long itemId)
        {
            XsiExhibitionProfessionalProgramRegistration entity = GetProfessionalProgramRegistrationByItemId(itemId);
            if (entity != null)
            {
                using (IProfessionalProgramRegistrationRepository = new ProfessionalProgramRegistrationRepository(ConnectionString))
                {
                    IProfessionalProgramRegistrationRepository.Delete(itemId);
                    if (IProfessionalProgramRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
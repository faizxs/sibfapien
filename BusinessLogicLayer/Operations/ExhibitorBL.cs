﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitorBL : IDisposable
    {
        #region Feilds
        IExhibitorRepository ExhibitorRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitorBL()
        {
            ConnectionString = null;
        }
        public ExhibitorBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitorRepository != null)
                this.ExhibitorRepository.Dispose();
        }
        #endregion
        #region Methods
        //public long GetNextGroupId()
        //{
        //    //optional method. return -1 if not required and remove following
        //    using (ExhibitorRepository = new ExhibitorRepository(ConnectionString))
        //    {
        //        return (ExhibitorRepository.Select().Max(p => p.GroupId) ?? 0) + 1;
        //    }
        //}

        public XsiExhibitor GetExhibitorByItemId(long itemId)
        {
            using (ExhibitorRepository = new ExhibitorRepository(ConnectionString))
            {
                return ExhibitorRepository.GetById(itemId);
            }
        }
        //public List<XsiExhibitor> GetExhibitorByGroupId(long groupId)
        //{
        //    using (ExhibitorRepository = new ExhibitorRepository(ConnectionString))
        //    {
        //        return ExhibitorRepository.GetByGroupId(groupId);
        //    }
        //}

        public List<XsiExhibitor> GetExhibitor()
        {
            using (ExhibitorRepository = new ExhibitorRepository(ConnectionString))
            {
                return ExhibitorRepository.Select();
            }
        }
        public List<XsiExhibitor> GetExhibitor(EnumSortlistBy sortListBy)
        {
            using (ExhibitorRepository = new ExhibitorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitorRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitorRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitorRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitorRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitorRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitorRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitorRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitor> GetExhibitor(XsiExhibitor entity)
        {
            using (ExhibitorRepository = new ExhibitorRepository(ConnectionString))
            {
                return ExhibitorRepository.Select(entity);
            }
        }
        public List<XsiExhibitor> GetExhibitor(XsiExhibitor entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitorRepository = new ExhibitorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitorRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitorRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitorRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitorRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitorRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitorRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitorRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitor> GetExhibitorOr(XsiExhibitor entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitorRepository = new ExhibitorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitorRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitorRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitorRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitorRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitorRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitorRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitorRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        //public XsiExhibitor GetExhibitor(long groupId, long languageId)
        //{
        //    using (ExhibitorRepository = new ExhibitorRepository(ConnectionString))
        //    {
        //        return ExhibitorRepository.Select(new XsiExhibitor() { GroupId = groupId, LanguageId = languageId }).FirstOrDefault();
        //    }
        //}

        public EnumResultType InsertExhibitor(XsiExhibitor entity)
        {
            using (ExhibitorRepository = new ExhibitorRepository(ConnectionString))
            {
                entity = ExhibitorRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;                    
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitor(XsiExhibitor entity)
        {
            XsiExhibitor OriginalEntity = GetExhibitorByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitorRepository = new ExhibitorRepository(ConnectionString))
                {
                    ExhibitorRepository.Update(entity);
                    if (ExhibitorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateExhibitorStatus(long itemId)
        {
            XsiExhibitor entity = GetExhibitorByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitorRepository = new ExhibitorRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitorRepository.Update(entity);
                    if (ExhibitorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitor(XsiExhibitor entity)
        {
            List<XsiExhibitor> entities = GetExhibitor(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitor e in entities)
                {
                    result = DeleteExhibitor(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitor(long itemId)
        {
            XsiExhibitor entity = GetExhibitorByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitorRepository = new ExhibitorRepository(ConnectionString))
                {
                    ExhibitorRepository.Delete(itemId);
                    if (ExhibitorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
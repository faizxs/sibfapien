﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PhotoCategoryBL : IDisposable
    {
        #region Feilds
        IPhotoCategoryRepository PhotoCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PhotoCategoryBL()
        {
            ConnectionString = null;
        }
        public PhotoCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PhotoCategoryRepository != null)
                this.PhotoCategoryRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiPhotoCategory GetPhotoCategoryByItemId(long itemId)
        {
            using (PhotoCategoryRepository = new PhotoCategoryRepository(ConnectionString))
            {
                return PhotoCategoryRepository.GetById(itemId);
            }
        }

        public List<XsiPhotoCategory> GetPhotoCategory()
        {
            using (PhotoCategoryRepository = new PhotoCategoryRepository(ConnectionString))
            {
                return PhotoCategoryRepository.Select();
            }
        }
        public List<XsiPhotoCategory> GetPhotoCategory(EnumSortlistBy sortListBy)
        {
            using (PhotoCategoryRepository = new PhotoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PhotoCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PhotoCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PhotoCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PhotoCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PhotoCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PhotoCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PhotoCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPhotoCategory> GetPhotoCategory(XsiPhotoCategory entity)
        {
            using (PhotoCategoryRepository = new PhotoCategoryRepository(ConnectionString))
            {
                return PhotoCategoryRepository.Select(entity);
            }
        }
        public List<XsiPhotoCategory> GetPhotoCategory(XsiPhotoCategory entity, EnumSortlistBy sortListBy)
        {
            using (PhotoCategoryRepository = new PhotoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PhotoCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PhotoCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PhotoCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PhotoCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PhotoCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PhotoCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PhotoCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiPhotoCategory> GetPhotoCategoryOr(XsiPhotoCategory entity, EnumSortlistBy sortListBy)
        {
            using (PhotoCategoryRepository = new PhotoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PhotoCategoryRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PhotoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PhotoCategoryRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PhotoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PhotoCategoryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PhotoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PhotoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertPhotoCategory(XsiPhotoCategory entity)
        {
            using (PhotoCategoryRepository = new PhotoCategoryRepository(ConnectionString))
            {
                entity = PhotoCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePhotoCategory(XsiPhotoCategory entity)
        {
            XsiPhotoCategory OriginalEntity = GetPhotoCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PhotoCategoryRepository = new PhotoCategoryRepository(ConnectionString))
                {
                    PhotoCategoryRepository.Update(entity);
                    if (PhotoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdatePhotoCategoryStatus(long itemId, long langId)
        {
            XsiPhotoCategory entity = GetPhotoCategoryByItemId(itemId);
            if (entity != null)
            {
                using (PhotoCategoryRepository = new PhotoCategoryRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    PhotoCategoryRepository.Update(entity);
                    if (PhotoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePhotoCategory(XsiPhotoCategory entity)
        {
            List<XsiPhotoCategory> entities = GetPhotoCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiPhotoCategory e in entities)
                {
                    result = DeletePhotoCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeletePhotoCategory(long itemId)
        {
            XsiPhotoCategory entity = GetPhotoCategoryByItemId(itemId);
            if (entity != null)
            {
                using (PhotoCategoryRepository = new PhotoCategoryRepository(ConnectionString))
                {
                    PhotoCategoryRepository.Delete(itemId);
                    if (PhotoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
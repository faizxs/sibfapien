﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class TranslationGrantMemberBL : IDisposable
    {
        #region Feilds
        ITranslationGrantMemberRepository TranslationGrantMemberRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public TranslationGrantMemberBL()
        {
            ConnectionString = null;
        }
        public TranslationGrantMemberBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.TranslationGrantMemberRepository != null)
                this.TranslationGrantMemberRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionTranslationGrantMembers GetTranslationGrantMemberByItemId(long itemId)
        {
            using (TranslationGrantMemberRepository = new TranslationGrantMemberRepository(ConnectionString))
            {
                return TranslationGrantMemberRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionTranslationGrantMembers> GetTranslationGrantMember()
        {
            using (TranslationGrantMemberRepository = new TranslationGrantMemberRepository(ConnectionString))
            {
                return TranslationGrantMemberRepository.Select();
            }
        }
        public List<XsiExhibitionTranslationGrantMembers> GetTranslationGrantMember(EnumSortlistBy sortListBy)
        {
            using (TranslationGrantMemberRepository = new TranslationGrantMemberRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return TranslationGrantMemberRepository.Select().OrderBy(p => p.SellerName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return TranslationGrantMemberRepository.Select().OrderByDescending(p => p.SellerName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return TranslationGrantMemberRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return TranslationGrantMemberRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return TranslationGrantMemberRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return TranslationGrantMemberRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return TranslationGrantMemberRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionTranslationGrantMembers> GetTranslationGrantMember(XsiExhibitionTranslationGrantMembers entity)
        {
            using (TranslationGrantMemberRepository = new TranslationGrantMemberRepository(ConnectionString))
            {
                return TranslationGrantMemberRepository.Select(entity);
            }
        }
        public List<XsiExhibitionTranslationGrantMembers> GetTranslationGrantMember(XsiExhibitionTranslationGrantMembers entity, EnumSortlistBy sortListBy)
        {
            using (TranslationGrantMemberRepository = new TranslationGrantMemberRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return TranslationGrantMemberRepository.Select(entity).OrderBy(p => p.SellerName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return TranslationGrantMemberRepository.Select(entity).OrderByDescending(p => p.SellerName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return TranslationGrantMemberRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return TranslationGrantMemberRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return TranslationGrantMemberRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return TranslationGrantMemberRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return TranslationGrantMemberRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertTranslationGrantMember(XsiExhibitionTranslationGrantMembers entity)
        {
            using (TranslationGrantMemberRepository = new TranslationGrantMemberRepository(ConnectionString))
            {
                entity = TranslationGrantMemberRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId =  -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateTranslationGrantMember(XsiExhibitionTranslationGrantMembers entity)
        {
            XsiExhibitionTranslationGrantMembers OriginalEntity = GetTranslationGrantMemberByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (TranslationGrantMemberRepository = new TranslationGrantMemberRepository(ConnectionString))
                {
                    TranslationGrantMemberRepository.Update(entity);
                    if (TranslationGrantMemberRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateTranslationGrantMemberStatus(long itemId)
        {
            XsiExhibitionTranslationGrantMembers entity = GetTranslationGrantMemberByItemId(itemId);
            if (entity != null)
            {
                using (TranslationGrantMemberRepository = new TranslationGrantMemberRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    TranslationGrantMemberRepository.Update(entity);
                    if (TranslationGrantMemberRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteTranslationGrantMember(XsiExhibitionTranslationGrantMembers entity)
        {
            List<XsiExhibitionTranslationGrantMembers> entities = GetTranslationGrantMember(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionTranslationGrantMembers e in entities)
                {
                    result = DeleteTranslationGrantMember(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteTranslationGrantMember(long itemId)
        {
            XsiExhibitionTranslationGrantMembers entity = GetTranslationGrantMemberByItemId(itemId);
            if (entity != null)
            {
                using (TranslationGrantMemberRepository = new TranslationGrantMemberRepository(ConnectionString))
                {
                    TranslationGrantMemberRepository.Delete(itemId);
                    if (TranslationGrantMemberRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
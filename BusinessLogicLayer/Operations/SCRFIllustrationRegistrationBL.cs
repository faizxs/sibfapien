﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFIllustrationRegistrationBL : IDisposable
    {
        #region Feilds
        ISCRFIllustrationRegistrationRepository SCRFIllustrationRegistrationRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFIllustrationRegistrationBL()
        {
            ConnectionString = null;
        }
        public SCRFIllustrationRegistrationBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIllustrationRegistrationRepository != null)
                this.SCRFIllustrationRegistrationRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfillustrationRegistration GetSCRFIllustrationRegistrationByItemId(long itemId)
        {
            using (SCRFIllustrationRegistrationRepository = new SCRFIllustrationRegistrationRepository(ConnectionString))
            {
                return SCRFIllustrationRegistrationRepository.GetById(itemId);
            }
        }
        public XsiScrfillustrationRegistration GetSCRFIllustrationRegistrationByEmail(string email)
        {
            using (SCRFIllustrationRegistrationRepository = new SCRFIllustrationRegistrationRepository(ConnectionString))
            {
                return SCRFIllustrationRegistrationRepository.Select(new XsiScrfillustrationRegistration() { Email = email }).FirstOrDefault();
            }
        }

        public List<XsiScrfillustrationRegistration> GetSCRFIllustrationRegistration()
        {
            using (SCRFIllustrationRegistrationRepository = new SCRFIllustrationRegistrationRepository(ConnectionString))
            {
                return SCRFIllustrationRegistrationRepository.Select();
            }
        }
        public List<XsiScrfillustrationRegistration> GetSCRFIllustrationRegistration(EnumSortlistBy sortListBy)
        {
            using (SCRFIllustrationRegistrationRepository = new SCRFIllustrationRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIllustrationRegistrationRepository.Select().OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIllustrationRegistrationRepository.Select().OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFIllustrationRegistrationRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFIllustrationRegistrationRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIllustrationRegistrationRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIllustrationRegistrationRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIllustrationRegistrationRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfillustrationRegistration> GetSCRFIllustrationRegistration(XsiScrfillustrationRegistration entity)
        {
            using (SCRFIllustrationRegistrationRepository = new SCRFIllustrationRegistrationRepository(ConnectionString))
            {
                return SCRFIllustrationRegistrationRepository.Select(entity);
            }
        }
        public List<XsiScrfillustrationRegistration> GetSCRFIllustrationRegistration(XsiScrfillustrationRegistration entity, EnumSortlistBy sortListBy)
        {
            using (SCRFIllustrationRegistrationRepository = new SCRFIllustrationRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIllustrationRegistrationRepository.Select(entity).OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIllustrationRegistrationRepository.Select(entity).OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFIllustrationRegistrationRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFIllustrationRegistrationRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIllustrationRegistrationRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIllustrationRegistrationRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIllustrationRegistrationRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFIllustrationRegistration(XsiScrfillustrationRegistration entity)
        {
            using (SCRFIllustrationRegistrationRepository = new SCRFIllustrationRegistrationRepository(ConnectionString))
            {
                entity = SCRFIllustrationRegistrationRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFIllustrationRegistration(XsiScrfillustrationRegistration entity)
        {
            XsiScrfillustrationRegistration OriginalEntity = GetSCRFIllustrationRegistrationByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFIllustrationRegistrationRepository = new SCRFIllustrationRegistrationRepository(ConnectionString))
                {
                    SCRFIllustrationRegistrationRepository.Update(entity);
                    if (SCRFIllustrationRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFIllustrationRegistrationStatus(long itemId)
        {
            XsiScrfillustrationRegistration entity = GetSCRFIllustrationRegistrationByItemId(itemId);
            if (entity != null)
            {
                using (SCRFIllustrationRegistrationRepository = new SCRFIllustrationRegistrationRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFIllustrationRegistrationRepository.Update(entity);
                    if (SCRFIllustrationRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFIllustrationRegistration(XsiScrfillustrationRegistration entity)
        {
            List<XsiScrfillustrationRegistration> mem = GetSCRFIllustrationRegistration(entity).ToList();

            if (mem.Count > 0)
            {
                EnumResultType result= EnumResultType.NotFound;

                foreach (XsiScrfillustrationRegistration m in mem)
                {
                     result = DeleteSCRFIllustrationRegistration(m.ItemId);
                     if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                         return result;
                }

                return result;
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFIllustrationRegistration(long itemId)
        {
            if (GetSCRFIllustrationRegistrationByItemId(itemId) != null)
            {
                using (SCRFIllustrationRegistrationRepository = new SCRFIllustrationRegistrationRepository(ConnectionString))
                {
                    SCRFIllustrationRegistrationRepository.Delete(itemId);
                    if (SCRFIllustrationRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
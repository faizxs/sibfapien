﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class RestaurantRegistrationBL : IDisposable
    {
        #region Feilds
        IRestaurantRepository RestaurantRegistrationRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public RestaurantRegistrationBL()
        {
            ConnectionString = null;
        }
        public RestaurantRegistrationBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.RestaurantRegistrationRepository != null)
                this.RestaurantRegistrationRepository.Dispose();
        }
        #endregion
        #region Methods
        public List<GetExhibitorsAndCountriesSCRF_Result> GetExhibitorsForAutoComplete(string keyword, long langId, long exhibitionId)
        {
            using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
            {
                return RestaurantRegistrationRepository.GetExhibitorsForAutoComplete(keyword, langId, exhibitionId);
            }
        }
        public XsiExhibitionMemberApplicationYearly GetRestaurantRegistrationByItemId(long MemberExhibitionYearlyId)
        {
            using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
            {
                return RestaurantRegistrationRepository.GetById(MemberExhibitionYearlyId);
            }
        }
        public XsiExhibitionMemberApplicationYearly GetRestaurantRegistrationByItemIdAll(long MemberExhibitionYearlyId)
        {
            using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
            {
                return RestaurantRegistrationRepository.GetByIdAll(MemberExhibitionYearlyId);
            }
        }

        public List<XsiExhibitionMemberApplicationYearly> GetRestaurantRegistration()
        {
            using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
            {
                return RestaurantRegistrationRepository.Select();
            }
        }
        public List<XsiExhibitionMemberApplicationYearly> GetRestaurantRegistration(EnumSortlistBy sortListBy)
        {
            using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return RestaurantRegistrationRepository.Select().OrderBy(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return RestaurantRegistrationRepository.Select().OrderByDescending(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return RestaurantRegistrationRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return RestaurantRegistrationRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return RestaurantRegistrationRepository.Select().OrderBy(p => p.MemberExhibitionYearlyId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return RestaurantRegistrationRepository.Select().OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();

                    default:
                        return RestaurantRegistrationRepository.Select().OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();
                }
            }
        }
        public List<XsiExhibitionMemberApplicationYearly> GetRestaurantRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
            {
                return RestaurantRegistrationRepository.Select(entity);
            }
        }
        public List<XsiExhibitionMemberApplicationYearly> GetRestaurantRegistration(XsiExhibitionMemberApplicationYearly entity, EnumSortlistBy sortListBy)
        {
            using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return RestaurantRegistrationRepository.Select(entity).OrderBy(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return RestaurantRegistrationRepository.Select(entity).OrderByDescending(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return RestaurantRegistrationRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return RestaurantRegistrationRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return RestaurantRegistrationRepository.Select(entity).OrderBy(p => p.MemberExhibitionYearlyId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return RestaurantRegistrationRepository.Select(entity).OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();

                    default:
                        return RestaurantRegistrationRepository.Select(entity).OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();
                }
            }
        }
        public List<string> GetRestaurantRegistration(XsiExhibitionMemberApplicationYearly entity, List<long> exhibitionIDs)
        {
            using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
            {
                return RestaurantRegistrationRepository.Select(entity, exhibitionIDs);
            }
        }
        public List<string> GetRestaurantRegistrationAr(XsiExhibitionMemberApplicationYearly entity, List<long> exhibitionIDs)
        {
            using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
            {
                return RestaurantRegistrationRepository.SelectAr(entity, exhibitionIDs);
            }
        }
        //public List<GetExhibitors_Agencies_Result> GetExhibitorAndAgency(string whereExhibitor, string whereAgency, string languageId)
        //{
        //    RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString);
        //    return RestaurantRegistrationRepository.SelectExhibitorAgency(whereExhibitor, whereAgency, languageId);
        //}
        //public List<GetExhibitors_Agencies_Result> GetExhibitorAndAgencyAnd(string whereExhibitor, string whereAgency, GetExhibitors_Agencies_Result entity, List<long?> categoryIDs, string languageId)
        //{
        //    using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
        //    {
        //        return RestaurantRegistrationRepository.SelectExhibitorAgencyAnd(whereExhibitor, whereAgency, entity, categoryIDs, languageId);
        //    }
        //}
        //public List<GetExhibitors_Agencies_Result> GetExhibitorAndAgencyOr(string whereExhibitor, string whereAgency, GetExhibitors_Agencies_Result entity, List<long?> categoryIDs, string languageId)
        //{
        //    RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString);
        //    return RestaurantRegistrationRepository.SelectExhibitorAgencyOr(whereExhibitor, whereAgency, entity, categoryIDs, languageId);
        //}
        //public List<GetExhibitors_AgenciesToExport_Result> GetExhibitorAndAgencyToExport(string exhibitorIds, string agencyIds, string languageId)
        //{
        //    using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
        //    {
        //        return RestaurantRegistrationRepository.SelectExhibitorAgencyToExport(exhibitorIds, agencyIds, languageId);
        //    }
        //}
        //public List<GetExhibitorsAndCountries_Result> GetExhibitorsAndCountries()
        //{
        //    RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString);
        //    return RestaurantRegistrationRepository.SelectExhibitorsAndCountries();
        //}
        //public List<GetExhibitorsAndCountriesSCRF_Result> GetExhibitorsAndCountriesSCRF()
        //{
        //    using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
        //    {
        //        return RestaurantRegistrationRepository.SelectExhibitorsAndCountriesSCRF();
        //    }
        //}
        //public long? GetRequestedAreaSum()
        //{
        //    using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
        //    {
        //        return RestaurantRegistrationRepository.SelectRequestedAreaSum();
        //    }
        //}
        //public List<GetRestaurantRegistrationList_Result> GetExhibitorList(string languageId)
        //{
        //    using (RestaurantRegistrationRepository RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
        //    {
        //        return RestaurantRegistrationRepository.SelectExhibitorList(languageId);
        //    }
        //}

        public EnumResultType InsertRestaurantRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
            {
                XsiExhibitionMemberApplicationYearly where = new XsiExhibitionMemberApplicationYearly();
                where.MemberId = entity.MemberId;
                where.ExhibitionId = entity.ExhibitionId;
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);

                if (RestaurantRegistrationRepository.Select(where).Count() == 0)
                {
                    entity = RestaurantRegistrationRepository.Add(entity);
                    if (entity.MemberExhibitionYearlyId > 0)
                    {
                        XsiItemdId = entity.MemberExhibitionYearlyId;
                        XsiGroupId = -1;
                        return EnumResultType.Success;
                    }
                    else
                    {
                        XsiItemdId = -1;
                        XsiGroupId = -1;
                        return EnumResultType.Failed;
                    }
                }
                else
                    return EnumResultType.AlreadyExists;
            }
        }
        public EnumResultType UpdateRestaurantRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            XsiExhibitionMemberApplicationYearly OriginalEntity = GetRestaurantRegistrationByItemId(entity.MemberExhibitionYearlyId);

            if (OriginalEntity != null)
            {
                using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
                {
                    RestaurantRegistrationRepository.Update(entity);
                    if (RestaurantRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
      
        public EnumResultType UpdateRestaurantRegistrationStatus(long MemberExhibitionYearlyId)
        {
            XsiExhibitionMemberApplicationYearly entity = GetRestaurantRegistrationByItemId(MemberExhibitionYearlyId);
            if (entity != null)
            {
                using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    RestaurantRegistrationRepository.Update(entity);
                    if (RestaurantRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteRestaurantRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            List<XsiExhibitionMemberApplicationYearly> entities = GetRestaurantRegistration(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionMemberApplicationYearly e in entities)
                {
                    result = DeleteRestaurantRegistration(e.MemberExhibitionYearlyId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteRestaurantRegistration(long MemberExhibitionYearlyId)
        {
            XsiExhibitionMemberApplicationYearly entity = GetRestaurantRegistrationByItemId(MemberExhibitionYearlyId);
            if (entity != null)
            {
                using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
                {
                    RestaurantRegistrationRepository.Delete(MemberExhibitionYearlyId);
                    if (RestaurantRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public XsiExhibitionExhibitorDetails GetDetailsById(long MemberExhibitionYearlyId)
        {
            using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
            {
                return RestaurantRegistrationRepository.GetDetailsById(MemberExhibitionYearlyId);
            }
        }
        public EnumResultType InsertExhibitorDetailRegistration(XsiExhibitionExhibitorDetails entity)
        {
            using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
            {
                if (RestaurantRegistrationRepository.GetDetailsById(entity.MemberExhibitionYearlyId.Value) == null)
                {
                    entity = RestaurantRegistrationRepository.Add(entity);
                    if (entity.MemberExhibitionYearlyId > 0)
                    {
                        XsiItemdId = entity.ExhibitorDetailsId;
                        return EnumResultType.Success;
                    }
                    else
                    {
                        XsiItemdId = -1;
                        XsiGroupId = -1;
                        return EnumResultType.Failed;
                    }
                }
                else
                    return EnumResultType.AlreadyExists;
            }
        }
        public EnumResultType UpdateRestaurantRegistrationDetails(XsiExhibitionExhibitorDetails entity)
        {
            XsiExhibitionExhibitorDetails OriginalEntity = GetDetailsById(entity.MemberExhibitionYearlyId.Value);

            if (OriginalEntity != null)
            {
                using (RestaurantRegistrationRepository = new RestaurantRegistrationRepository(ConnectionString))
                {
                    RestaurantRegistrationRepository.Update(entity);
                    if (RestaurantRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class EventSubCategoryBL : IDisposable
    {
        #region Feilds
        IEventSubCategoryRepository EventSubCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public EventSubCategoryBL()
        {
            ConnectionString = null;
        }
        public EventSubCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventSubCategoryRepository != null)
                this.EventSubCategoryRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiEventSubCategory GetEventSubCategoryByItemId(long itemId)
        {
            using (EventSubCategoryRepository = new EventSubCategoryRepository(ConnectionString))
            {
                return EventSubCategoryRepository.GetById(itemId);
            }
        }
      
        public List<XsiEventSubCategory> GetEventSubCategory()
        {
            using (EventSubCategoryRepository = new EventSubCategoryRepository(ConnectionString))
            {
                return EventSubCategoryRepository.Select();
            }
        }
        public List<XsiEventSubCategory> GetEventSubCategory(EnumSortlistBy sortListBy)
        {
            using (EventSubCategoryRepository = new EventSubCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return EventSubCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return EventSubCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return EventSubCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventSubCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventSubCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventSubCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventSubCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEventSubCategory> GetEventSubCategory(XsiEventSubCategory entity)
        {
            using (EventSubCategoryRepository = new EventSubCategoryRepository(ConnectionString))
            {
                return EventSubCategoryRepository.Select(entity);
            }
        }
        public List<XsiEventSubCategory> GetEventSubCategory(XsiEventSubCategory entity, EnumSortlistBy sortListBy)
        {
            using (EventSubCategoryRepository = new EventSubCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return EventSubCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return EventSubCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return EventSubCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventSubCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventSubCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventSubCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventSubCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEventSubCategory> GetCompleteEventSubCategory(XsiEventSubCategory entity, EnumSortlistBy sortListBy)
        {
            using (EventSubCategoryRepository = new EventSubCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return EventSubCategoryRepository.SelectComeplete(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return EventSubCategoryRepository.SelectComeplete(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return EventSubCategoryRepository.SelectComeplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventSubCategoryRepository.SelectComeplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventSubCategoryRepository.SelectComeplete(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventSubCategoryRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventSubCategoryRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

         
        public EnumResultType InsertEventSubCategory(XsiEventSubCategory entity)
        {
            using (EventSubCategoryRepository = new EventSubCategoryRepository(ConnectionString))
            {
                entity = EventSubCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateEventSubCategory(XsiEventSubCategory entity)
        {
            XsiEventSubCategory OriginalEntity = GetEventSubCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (EventSubCategoryRepository = new EventSubCategoryRepository(ConnectionString))
                {
                    EventSubCategoryRepository.Update(entity);
                    if (EventSubCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateEventSubCategoryStatus(long itemId)
        {
            XsiEventSubCategory entity = GetEventSubCategoryByItemId(itemId);
            if (entity != null)
            {
                using (EventSubCategoryRepository = new EventSubCategoryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    EventSubCategoryRepository.Update(entity);
                    if (EventSubCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteEventSubCategory(XsiEventSubCategory entity)
        {
            List<XsiEventSubCategory> entities = GetEventSubCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiEventSubCategory e in entities)
                {
                    result = DeleteEventSubCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }
            return result;
        }
        public EnumResultType DeleteEventSubCategory(long itemId)
        {
            XsiEventSubCategory entity = GetEventSubCategoryByItemId(itemId);
            if (entity != null)
            {
                using (EventSubCategoryRepository = new EventSubCategoryRepository(ConnectionString))
                {
                    EventSubCategoryRepository.Delete(itemId);
                    if (EventSubCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
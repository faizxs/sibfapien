﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFVideoCategoryBL : IDisposable
    {
        #region Feilds
        ISCRFVideoCategoryRepository SCRFVideoCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFVideoCategoryBL()
        {
            ConnectionString = null;
        }
        public SCRFVideoCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFVideoCategoryRepository != null)
                this.SCRFVideoCategoryRepository.Dispose();
        }
        #endregion
        #region Methods
        

        public XsiScrfvideoCategory GetSCRFVideoCategoryByItemId(long itemId)
        {
            using (SCRFVideoCategoryRepository = new SCRFVideoCategoryRepository(ConnectionString))
            {
                return SCRFVideoCategoryRepository.GetById(itemId);
            }
        }
        
        public List<XsiScrfvideoCategory> GetSCRFVideoCategory()
        {
            using (SCRFVideoCategoryRepository = new SCRFVideoCategoryRepository(ConnectionString))
            {
                return SCRFVideoCategoryRepository.Select();
            }
        }
        public List<XsiScrfvideoCategory> GetSCRFVideoCategory(EnumSortlistBy sortListBy)
        {
            using (SCRFVideoCategoryRepository = new SCRFVideoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFVideoCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFVideoCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFVideoCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFVideoCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFVideoCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFVideoCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFVideoCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfvideoCategory> GetSCRFVideoCategory(XsiScrfvideoCategory entity)
        {
            using (SCRFVideoCategoryRepository = new SCRFVideoCategoryRepository(ConnectionString))
            {
                return SCRFVideoCategoryRepository.Select(entity);
            }
        }
        public List<XsiScrfvideoCategory> GetSCRFVideoCategory(XsiScrfvideoCategory entity, EnumSortlistBy sortListBy)
        {
            using (SCRFVideoCategoryRepository = new SCRFVideoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFVideoCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFVideoCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFVideoCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFVideoCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFVideoCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFVideoCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFVideoCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiScrfvideoCategory> GetSCRFVideoCategoryOr(XsiScrfvideoCategory entity, EnumSortlistBy sortListBy)
        {
            using (SCRFVideoCategoryRepository = new SCRFVideoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFVideoCategoryRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFVideoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFVideoCategoryRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFVideoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFVideoCategoryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFVideoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFVideoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
 
        public EnumResultType InsertSCRFVideoCategory(XsiScrfvideoCategory entity)
        {
            using (SCRFVideoCategoryRepository = new SCRFVideoCategoryRepository(ConnectionString))
            {
                entity = SCRFVideoCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFVideoCategory(XsiScrfvideoCategory entity)
        {
            XsiScrfvideoCategory OriginalEntity = GetSCRFVideoCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFVideoCategoryRepository = new SCRFVideoCategoryRepository(ConnectionString))
                {
                    SCRFVideoCategoryRepository.Update(entity);
                    if (SCRFVideoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFVideoCategoryStatus(long itemId)
        {
            XsiScrfvideoCategory entity = GetSCRFVideoCategoryByItemId(itemId);
            if (entity != null)
            {
                using (SCRFVideoCategoryRepository = new SCRFVideoCategoryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFVideoCategoryRepository.Update(entity);
                    if (SCRFVideoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFVideoCategory(XsiScrfvideoCategory entity)
        {
            List<XsiScrfvideoCategory> entities = GetSCRFVideoCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfvideoCategory e in entities)
                {
                    result = DeleteSCRFVideoCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFVideoCategory(long itemId)
        {
            XsiScrfvideoCategory entity = GetSCRFVideoCategoryByItemId(itemId);
            if (entity != null)
            {
                using (SCRFVideoCategoryRepository = new SCRFVideoCategoryRepository(ConnectionString))
                {
                    SCRFVideoCategoryRepository.Delete(itemId);
                    if (SCRFVideoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionActivityBL : IDisposable
    {
        #region Feilds
        IExhibitionActivityRepository ExhibitionActivityRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionActivityBL()
        {
            ConnectionString = null;
        }
        public ExhibitionActivityBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionActivityRepository != null)
                this.ExhibitionActivityRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionActivity GetExhibitionActivityByItemId(long itemId)
        {
            using (ExhibitionActivityRepository = new ExhibitionActivityRepository(ConnectionString))
            {
                return ExhibitionActivityRepository.GetById(itemId);
            }
        }
          
        public List<XsiExhibitionActivity> GetExhibitionActivityByItemIds(long[] ItemidArray)
        {
            using (ExhibitionActivityRepository = new ExhibitionActivityRepository(ConnectionString))
            {
                return ExhibitionActivityRepository.GetByItemIds(ItemidArray);
            }
        }

        public List<XsiExhibitionActivity> GetExhibitionActivity()
        {
            using (ExhibitionActivityRepository = new ExhibitionActivityRepository(ConnectionString))
            {
                return ExhibitionActivityRepository.Select();
            }
        }
        public List<XsiExhibitionActivity> GetExhibitionActivity(EnumSortlistBy sortListBy)
        {
            using (ExhibitionActivityRepository = new ExhibitionActivityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionActivityRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionActivityRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionActivityRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionActivityRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionActivityRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionActivityRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionActivityRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionActivity> GetExhibitionActivity(XsiExhibitionActivity entity)
        {
            using (ExhibitionActivityRepository = new ExhibitionActivityRepository(ConnectionString))
            {
                return ExhibitionActivityRepository.Select(entity);
            }
        }
        public List<XsiExhibitionActivity> GetExhibitionActivity(XsiExhibitionActivity entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionActivityRepository = new ExhibitionActivityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionActivityRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionActivityRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionActivityRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionActivityRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionActivityRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionActivityRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionActivityRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionActivity> GetExhibitionActivityOr(XsiExhibitionActivity entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionActivityRepository = new ExhibitionActivityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionActivityRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionActivityRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionActivityRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionActivityRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionActivityRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionActivityRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionActivityRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public EnumResultType InsertExhibitionActivity(XsiExhibitionActivity entity)
        {
            using (ExhibitionActivityRepository = new ExhibitionActivityRepository(ConnectionString))
            {
                entity = ExhibitionActivityRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionActivity(XsiExhibitionActivity entity)
        {
            XsiExhibitionActivity OriginalEntity = GetExhibitionActivityByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionActivityRepository = new ExhibitionActivityRepository(ConnectionString))
                {

                    ExhibitionActivityRepository.Update(entity);
                    if (ExhibitionActivityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
      
        public EnumResultType UpdateExhibitionActivityStatus(long itemId)
        {
            XsiExhibitionActivity entity = GetExhibitionActivityByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionActivityRepository = new ExhibitionActivityRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionActivityRepository.Update(entity);
                    if (ExhibitionActivityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionActivity(XsiExhibitionActivity entity)
        {
            List<XsiExhibitionActivity> entities = GetExhibitionActivity(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionActivity e in entities)
                {
                    result = DeleteExhibitionActivity(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionActivity(long itemId)
        {
            XsiExhibitionActivity entity = GetExhibitionActivityByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionActivityRepository = new ExhibitionActivityRepository(ConnectionString))
                {
                    ExhibitionActivityRepository.Delete(itemId);
                    if (ExhibitionActivityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
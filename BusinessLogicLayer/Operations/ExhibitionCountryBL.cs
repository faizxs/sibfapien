﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionCountryBL : IDisposable
    {
        #region Feilds
        IExhibitionCountryRepository ExhibitionCountryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionCountryBL()
        {
            ConnectionString = null;
        }
        public ExhibitionCountryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionCountryRepository != null)
                this.ExhibitionCountryRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionCountry GetExhibitionCountryByItemId(long itemId)
        {
            using (ExhibitionCountryRepository = new ExhibitionCountryRepository(ConnectionString))
            {
                return ExhibitionCountryRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionCountry> GetExhibitionCountry()
        {
            using (ExhibitionCountryRepository = new ExhibitionCountryRepository(ConnectionString))
            {
                return ExhibitionCountryRepository.Select();
            }
        }
        public List<XsiExhibitionCountry> GetExhibitionCountry(EnumSortlistBy sortListBy)
        {
            using (ExhibitionCountryRepository = new ExhibitionCountryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCountryRepository.Select().OrderBy(p => p.CountryName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCountryRepository.Select().OrderByDescending(p => p.CountryName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCountryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCountryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCountryRepository.Select().OrderBy(p => p.CountryId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCountryRepository.Select().OrderByDescending(p => p.CountryId).ToList();

                    default:
                        return ExhibitionCountryRepository.Select().OrderByDescending(p => p.CountryId).ToList();
                }
            }
        }
        public List<XsiExhibitionCountry> GetExhibitionCountry(XsiExhibitionCountry entity)
        {
            using (ExhibitionCountryRepository = new ExhibitionCountryRepository(ConnectionString))
            {
                return ExhibitionCountryRepository.Select(entity);
            }
        }
        public List<XsiExhibitionCountry> GetExhibitionCountry(XsiExhibitionCountry entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionCountryRepository = new ExhibitionCountryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCountryRepository.Select(entity).OrderBy(p => p.CountryName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCountryRepository.Select(entity).OrderByDescending(p => p.CountryName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCountryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCountryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCountryRepository.Select(entity).OrderBy(p => p.CountryId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCountryRepository.Select(entity).OrderByDescending(p => p.CountryId).ToList();

                    default:
                        return ExhibitionCountryRepository.Select(entity).OrderByDescending(p => p.CountryId).ToList();
                }
            }
        }

        public List<XsiExhibitionCountry> GetExhibitionCountryOr(XsiExhibitionCountry entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionCountryRepository = new ExhibitionCountryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCountryRepository.SelectOr(entity).OrderBy(p => p.CountryName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCountryRepository.SelectOr(entity).OrderByDescending(p => p.CountryName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCountryRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCountryRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCountryRepository.SelectOr(entity).OrderBy(p => p.CountryId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCountryRepository.SelectOr(entity).OrderByDescending(p => p.CountryId).ToList();

                    default:
                        return ExhibitionCountryRepository.SelectOr(entity).OrderByDescending(p => p.CountryId).ToList();
                }
            }
        }
        public List<XsiExhibitionCountry> GetOtherLanguageExhibitionCountry(XsiExhibitionCountry entity)
        {
            using (ExhibitionCountryRepository = new ExhibitionCountryRepository(ConnectionString))
            {
                return ExhibitionCountryRepository.SelectOtherLanguageCategory(entity);
            }
        }
        public List<XsiExhibitionCountry> GetCurrentLanguageExhibitionCountry(XsiExhibitionCountry entity)
        {
            using (ExhibitionCountryRepository = new ExhibitionCountryRepository(ConnectionString))
            {
                return ExhibitionCountryRepository.SelectCurrentLanguageCategory(entity);
            }
        }
        public List<XsiExhibitionCountry> GetCompleteExhibitionCountry(XsiExhibitionCountry entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionCountryRepository = new ExhibitionCountryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCountryRepository.SelectComplete(entity).OrderBy(p => p.CountryName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCountryRepository.SelectComplete(entity).OrderByDescending(p => p.CountryName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCountryRepository.SelectComplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCountryRepository.SelectComplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCountryRepository.SelectComplete(entity).OrderBy(p => p.CountryId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCountryRepository.SelectComplete(entity).OrderByDescending(p => p.CountryId).ToList();

                    default:
                        return ExhibitionCountryRepository.SelectComplete(entity).OrderByDescending(p => p.CountryId).ToList();
                }
            }
        }
        public EnumResultType InsertExhibitionCountry(XsiExhibitionCountry entity)
        {
            using (ExhibitionCountryRepository = new ExhibitionCountryRepository(ConnectionString))
            {
                entity = ExhibitionCountryRepository.Add(entity);
                if (entity.CountryId > 0)
                {
                    XsiItemId = entity.CountryId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionCountry(XsiExhibitionCountry entity)
        {
            XsiExhibitionCountry OriginalEntity = GetExhibitionCountryByItemId(entity.CountryId);

            if (OriginalEntity != null)
            {
                using (ExhibitionCountryRepository = new ExhibitionCountryRepository(ConnectionString))
                {
                    ExhibitionCountryRepository.Update(entity);
                    if (ExhibitionCountryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionCountryStatus(long itemId)
        {
            XsiExhibitionCountry entity = GetExhibitionCountryByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionCountryRepository = new ExhibitionCountryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionCountryRepository.Update(entity);
                    if (ExhibitionCountryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionCountry(XsiExhibitionCountry entity)
        {
            List<XsiExhibitionCountry> entities = GetExhibitionCountry(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionCountry e in entities)
                {
                    result = DeleteExhibitionCountry(e.CountryId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionCountry(long itemId)
        {
            XsiExhibitionCountry entity = GetExhibitionCountryByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionCountryRepository = new ExhibitionCountryRepository(ConnectionString))
                {
                    ExhibitionCountryRepository.Delete(itemId);
                    if (ExhibitionCountryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFPressReleaseBL : IDisposable
    {
        #region Feilds
        ISCRFPressReleaseRepository SCRFPressReleaseRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFPressReleaseBL()
        {
            ConnectionString = null;
        }
        public SCRFPressReleaseBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFPressReleaseRepository != null)
                this.SCRFPressReleaseRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrfpressRelease GetSCRFPressReleaseByItemId(long itemId)
        {
            using (SCRFPressReleaseRepository = new SCRFPressReleaseRepository(ConnectionString))
            {
                return SCRFPressReleaseRepository.GetById(itemId);
            }
        }

        public List<XsiScrfpressRelease> GetSCRFPressRelease()
        {
            using (SCRFPressReleaseRepository = new SCRFPressReleaseRepository(ConnectionString))
            {
                return SCRFPressReleaseRepository.Select();
            }
        }
        public List<XsiScrfpressRelease> GetSCRFPressRelease(EnumSortlistBy sortListBy)
        {
            using (SCRFPressReleaseRepository = new SCRFPressReleaseRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPressReleaseRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPressReleaseRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFPressReleaseRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFPressReleaseRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPressReleaseRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPressReleaseRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFPressReleaseRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfpressRelease> GetSCRFPressRelease(XsiScrfpressRelease entity)
        {
            using (SCRFPressReleaseRepository = new SCRFPressReleaseRepository(ConnectionString))
            {
                return SCRFPressReleaseRepository.Select(entity);
            }
        }
        public List<XsiScrfpressRelease> GetSCRFPressRelease(XsiScrfpressRelease entity, EnumSortlistBy sortListBy)
        {
            using (SCRFPressReleaseRepository = new SCRFPressReleaseRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPressReleaseRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPressReleaseRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFPressReleaseRepository.Select(entity).OrderBy(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFPressReleaseRepository.Select(entity).OrderByDescending(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPressReleaseRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPressReleaseRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFPressReleaseRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiScrfpressRelease> GetSCRFPressReleaseOr(XsiScrfpressRelease entity, EnumSortlistBy sortListBy)
        {
            using (SCRFPressReleaseRepository = new SCRFPressReleaseRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPressReleaseRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPressReleaseRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFPressReleaseRepository.SelectOr(entity).OrderBy(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFPressReleaseRepository.SelectOr(entity).OrderByDescending(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPressReleaseRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPressReleaseRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFPressReleaseRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFPressRelease(XsiScrfpressRelease entity)
        {
            using (SCRFPressReleaseRepository = new SCRFPressReleaseRepository(ConnectionString))
            {
                entity = SCRFPressReleaseRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFPressRelease(XsiScrfpressRelease entity)
        {
            XsiScrfpressRelease OriginalEntity = GetSCRFPressReleaseByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFPressReleaseRepository = new SCRFPressReleaseRepository(ConnectionString))
                {
                    SCRFPressReleaseRepository.Update(entity);
                    if (SCRFPressReleaseRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFPressReleaseStatus(long itemId)
        {
            XsiScrfpressRelease entity = GetSCRFPressReleaseByItemId(itemId);
            if (entity != null)
            {
                using (SCRFPressReleaseRepository = new SCRFPressReleaseRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFPressReleaseRepository.Update(entity);
                    if (SCRFPressReleaseRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFPressRelease(XsiScrfpressRelease entity)
        {
            List<XsiScrfpressRelease> entities = GetSCRFPressRelease(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfpressRelease e in entities)
                {
                    result = DeleteSCRFPressRelease(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFPressRelease(long itemId)
        {
            XsiScrfpressRelease entity = GetSCRFPressReleaseByItemId(itemId);
            if (entity != null)
            {
                using (SCRFPressReleaseRepository = new SCRFPressReleaseRepository(ConnectionString))
                {
                    SCRFPressReleaseRepository.Delete(itemId);
                    if (SCRFPressReleaseRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
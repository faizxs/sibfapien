﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionOtherEventsEmailContentBL : IDisposable
    {
        #region Feilds
        IExhibitionOtherEventsEmailContentRepository ExhibitionOtherEventsEmailContentRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionOtherEventsEmailContentBL()
        {
            ConnectionString = null;
        }
        public ExhibitionOtherEventsEmailContentBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionOtherEventsEmailContentRepository != null)
                this.ExhibitionOtherEventsEmailContentRepository.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiExhibitionOtherEventsEmailContent GetExhibitionOtherEventsEmailContentByItemId(long itemId)
        {
            using (ExhibitionOtherEventsEmailContentRepository = new ExhibitionOtherEventsEmailContentRepository(ConnectionString))
            {
                return ExhibitionOtherEventsEmailContentRepository.GetById(itemId);
            }
        }
       
        public List<XsiExhibitionOtherEventsEmailContent> GetExhibitionOtherEventsEmailContent()
        {
            using (ExhibitionOtherEventsEmailContentRepository = new ExhibitionOtherEventsEmailContentRepository(ConnectionString))
            {
                return ExhibitionOtherEventsEmailContentRepository.Select();
            }
        }
        public List<XsiExhibitionOtherEventsEmailContent> GetExhibitionOtherEventsEmailContent(EnumSortlistBy sortListBy)
        {
            using (ExhibitionOtherEventsEmailContentRepository = new ExhibitionOtherEventsEmailContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionOtherEventsEmailContentRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionOtherEventsEmailContentRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionOtherEventsEmailContentRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionOtherEventsEmailContentRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionOtherEventsEmailContentRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionOtherEventsEmailContent> GetExhibitionOtherEventsEmailContent(XsiExhibitionOtherEventsEmailContent entity)
        {
            using (ExhibitionOtherEventsEmailContentRepository = new ExhibitionOtherEventsEmailContentRepository(ConnectionString))
            {
                return ExhibitionOtherEventsEmailContentRepository.Select(entity);
            }
        }
        public List<XsiExhibitionOtherEventsEmailContent> GetExhibitionOtherEventsEmailContent(XsiExhibitionOtherEventsEmailContent entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionOtherEventsEmailContentRepository = new ExhibitionOtherEventsEmailContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionOtherEventsEmailContentRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionOtherEventsEmailContentRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionOtherEventsEmailContentRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionOtherEventsEmailContentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionOtherEventsEmailContentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionOtherEventsEmailContent> GetExhibitionOtherEventsEmailContentOr(XsiExhibitionOtherEventsEmailContent entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionOtherEventsEmailContentRepository = new ExhibitionOtherEventsEmailContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionOtherEventsEmailContentRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionOtherEventsEmailContentRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionOtherEventsEmailContentRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionOtherEventsEmailContentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionOtherEventsEmailContentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public EnumResultType InsertExhibitionOtherEventsEmailContent(XsiExhibitionOtherEventsEmailContent entity)
        {
            using (ExhibitionOtherEventsEmailContentRepository = new ExhibitionOtherEventsEmailContentRepository(ConnectionString))
            {
                entity = ExhibitionOtherEventsEmailContentRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionOtherEventsEmailContent(XsiExhibitionOtherEventsEmailContent entity)
        {
            XsiExhibitionOtherEventsEmailContent OriginalEntity = GetExhibitionOtherEventsEmailContentByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionOtherEventsEmailContentRepository = new ExhibitionOtherEventsEmailContentRepository(ConnectionString))
                {
                    entity.RollbackXml = BusinessMethodFactory.GetRollbackXml(OriginalEntity);

                    ExhibitionOtherEventsEmailContentRepository.Update(entity);
                    if (ExhibitionOtherEventsEmailContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackExhibitionOtherEventsEmailContent(long itemId)
        {
            XsiExhibitionOtherEventsEmailContent OriginalEntity = GetExhibitionOtherEventsEmailContentByItemId(itemId);

            if (OriginalEntity == null)
                return EnumResultType.NotFound;
            else if (string.IsNullOrEmpty(OriginalEntity.RollbackXml))
                return EnumResultType.Invalid;
            else
            {
                XsiExhibitionOtherEventsEmailContent NewEntity = (XsiExhibitionOtherEventsEmailContent)BusinessMethodFactory.GetXmlToObject(OriginalEntity.RollbackXml, OriginalEntity.GetType());

                OriginalEntity.RollbackXml = string.Empty;

                NewEntity.ItemId = itemId;
                NewEntity.RollbackXml = BusinessMethodFactory.GetRollbackXml(OriginalEntity);

                using (ExhibitionOtherEventsEmailContentRepository = new ExhibitionOtherEventsEmailContentRepository(ConnectionString))
                {
                    ExhibitionOtherEventsEmailContentRepository.Update(NewEntity);
                    if (ExhibitionOtherEventsEmailContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }

        }
        public EnumResultType DeleteExhibitionOtherEventsEmailContent(XsiExhibitionOtherEventsEmailContent entity)
        {
            List<XsiExhibitionOtherEventsEmailContent> entities = GetExhibitionOtherEventsEmailContent(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionOtherEventsEmailContent e in entities)
                {
                    result = DeleteExhibitionOtherEventsEmailContent(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionOtherEventsEmailContent(long itemId)
        {
            XsiExhibitionOtherEventsEmailContent entity = GetExhibitionOtherEventsEmailContentByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionOtherEventsEmailContentRepository = new ExhibitionOtherEventsEmailContentRepository(ConnectionString))
                {
                    ExhibitionOtherEventsEmailContentRepository.Delete(itemId);
                    if (ExhibitionOtherEventsEmailContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
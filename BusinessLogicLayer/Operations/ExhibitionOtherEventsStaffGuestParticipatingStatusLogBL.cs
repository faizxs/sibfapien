﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;


namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL : IDisposable
    {
        #region Feilds
        IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL()
        {
            ConnectionString = null;
        }
        public ExhibitionOtherEventsStaffGuestParticipatingStatusLogBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository != null)
                this.IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository = new ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs GetExhibitionOtherEventsStaffGuestParticipatingStatusLogByItemId(long itemId)
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository = new ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.GetById(itemId);
            }
        }
        
        public List<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs> GetExhibitionOtherEventsStaffGuestParticipatingStatusLog()
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository = new ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select();
            }
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs> GetExhibitionOtherEventsStaffGuestParticipatingStatusLog(EnumSortlistBy sortListBy)
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository = new ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select().OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs> GetExhibitionOtherEventsStaffGuestParticipatingStatusLog(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity)
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository = new ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select(entity);
            }
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs> GetExhibitionOtherEventsStaffGuestParticipatingStatusLog(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository = new ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs GetExhibitionOtherEventsStaffGuestParticipatingStatusLog(long groupId, long languageId)
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository = new ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionOtherEventsStaffGuestParticipatingStatusLog(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity)
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository = new ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository(ConnectionString))
            {
                entity = IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId =  -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuestParticipatingStatusLog(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity)
        {
            XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs OriginalEntity = GetExhibitionOtherEventsStaffGuestParticipatingStatusLogByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository = new ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line
                    
                    IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Update(entity);
                    if (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuestParticipatingStatusLogStatus(long itemId)
        {
            XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity = GetExhibitionOtherEventsStaffGuestParticipatingStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository = new ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository(ConnectionString))
                {
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Update(entity);
                    if (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuestParticipatingStatusLog(XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity)
        {
            List<XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs> entities = GetExhibitionOtherEventsStaffGuestParticipatingStatusLog(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs e in entities)
                {
                    result = DeleteExhibitionOtherEventsStaffGuestParticipatingStatusLog(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuestParticipatingStatusLog(long itemId)
        {
            XsiExhibitionOtherEventsStaffGuestParticipatingStatusLogs entity = GetExhibitionOtherEventsStaffGuestParticipatingStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository = new ExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository(ConnectionString))
                {
                    IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.Delete(itemId);
                    if (IExhibitionOtherEventsStaffGuestParticipatingStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
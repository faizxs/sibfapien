﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SIBFCrawlerBL : IDisposable
    {
        #region Feilds
        ISIBFCrawlerRepository SIBFCrawlerRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SIBFCrawlerBL()
        {
            ConnectionString = null;
        }
        public SIBFCrawlerBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SIBFCrawlerRepository != null)
                this.SIBFCrawlerRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiSibfcrawler GetSIBFCrawlerByItemId(long itemId)
        {
            using (SIBFCrawlerRepository = new SIBFCrawlerRepository(ConnectionString))
            {
                return SIBFCrawlerRepository.GetById(itemId);
            }
        }

        public List<XsiSibfcrawler> GetSIBFCrawler()
        {
            using (SIBFCrawlerRepository = new SIBFCrawlerRepository(ConnectionString))
            {
                return SIBFCrawlerRepository.Select();
            }
        }
        public List<XsiSibfcrawler> GetSIBFCrawler(EnumSortlistBy sortListBy)
        {
            using (SIBFCrawlerRepository = new SIBFCrawlerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SIBFCrawlerRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SIBFCrawlerRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SIBFCrawlerRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SIBFCrawlerRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SIBFCrawlerRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SIBFCrawlerRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SIBFCrawlerRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiSibfcrawler> GetSIBFCrawler(XsiSibfcrawler entity)
        {
            using (SIBFCrawlerRepository = new SIBFCrawlerRepository(ConnectionString))
            {
                return SIBFCrawlerRepository.Select(entity);
            }
        }
        public List<XsiSibfcrawler> GetSIBFCrawler(XsiSibfcrawler entity, EnumSortlistBy sortListBy)
        {
            using (SIBFCrawlerRepository = new SIBFCrawlerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SIBFCrawlerRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SIBFCrawlerRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SIBFCrawlerRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SIBFCrawlerRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SIBFCrawlerRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SIBFCrawlerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SIBFCrawlerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiSibfcrawler> GetSIBFCrawlerOr(XsiSibfcrawler entity, EnumSortlistBy sortListBy)
        {
            using (SIBFCrawlerRepository = new SIBFCrawlerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SIBFCrawlerRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SIBFCrawlerRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SIBFCrawlerRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SIBFCrawlerRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SIBFCrawlerRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SIBFCrawlerRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SIBFCrawlerRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSIBFCrawler(XsiSibfcrawler entity)
        {
            using (SIBFCrawlerRepository = new SIBFCrawlerRepository(ConnectionString))
            {
                entity = SIBFCrawlerRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSIBFCrawler(XsiSibfcrawler entity)
        {
            XsiSibfcrawler OriginalEntity = GetSIBFCrawlerByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SIBFCrawlerRepository = new SIBFCrawlerRepository(ConnectionString))
                {
                    SIBFCrawlerRepository.Update(entity);
                    if (SIBFCrawlerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSIBFCrawlerStatus(long itemId)
        {
            XsiSibfcrawler entity = GetSIBFCrawlerByItemId(itemId);
            if (entity != null)
            {
                using (SIBFCrawlerRepository = new SIBFCrawlerRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SIBFCrawlerRepository.Update(entity);
                    if (SIBFCrawlerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSIBFCrawler(XsiSibfcrawler entity)
        {
            List<XsiSibfcrawler> entities = GetSIBFCrawler(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiSibfcrawler e in entities)
                {
                    result = DeleteSIBFCrawler(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSIBFCrawler(long itemId)
        {
            XsiSibfcrawler entity = GetSIBFCrawlerByItemId(itemId);
            if (entity != null)
            {
                using (SIBFCrawlerRepository = new SIBFCrawlerRepository(ConnectionString))
                {
                    SIBFCrawlerRepository.Delete(itemId);
                    if (SIBFCrawlerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class GuestWebsiteBL : IDisposable
    {
        #region Feilds
        IGuestWebsiteRepository GuestWebsiteRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public GuestWebsiteBL()
        {
            ConnectionString = null;
        }
        public GuestWebsiteBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.GuestWebsiteRepository != null)
                this.GuestWebsiteRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (GuestWebsiteRepository = new GuestWebsiteRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiGuestWebsite GetGuestWebsiteByItemId(long itemId)
        {
            using (GuestWebsiteRepository = new GuestWebsiteRepository(ConnectionString))
            {
                return GuestWebsiteRepository.GetById(itemId);
            }
        }
        
        public List<XsiGuestWebsite> GetGuestWebsite()
        {
            using (GuestWebsiteRepository = new GuestWebsiteRepository(ConnectionString))
            {
                return GuestWebsiteRepository.Select();
            }
        }
        public List<XsiGuestWebsite> GetGuestWebsite(EnumSortlistBy sortListBy)
        {
            using (GuestWebsiteRepository = new GuestWebsiteRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return GuestWebsiteRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return GuestWebsiteRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return GuestWebsiteRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiGuestWebsite> GetGuestWebsite(XsiGuestWebsite entity)
        {
            using (GuestWebsiteRepository = new GuestWebsiteRepository(ConnectionString))
            {
                return GuestWebsiteRepository.Select(entity);
            }
        }
        public List<XsiGuestWebsite> GetGuestWebsite(XsiGuestWebsite entity, EnumSortlistBy sortListBy)
        {
            using (GuestWebsiteRepository = new GuestWebsiteRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return GuestWebsiteRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return GuestWebsiteRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return GuestWebsiteRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<long?> GetGuestId(long ID1, long ID2, long ID3)
        {
            using (GuestWebsiteRepository = new GuestWebsiteRepository(ConnectionString))
            {
                return GuestWebsiteRepository.Select(ID1, ID2, ID3);
            }
        }
        public XsiGuestWebsite GetGuestWebsite(long groupId, long languageId)
        {
            using (GuestWebsiteRepository = new GuestWebsiteRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertGuestWebsite(XsiGuestWebsite entity)
        {
            using (GuestWebsiteRepository = new GuestWebsiteRepository(ConnectionString))
            {
                entity = GuestWebsiteRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateGuestWebsite(XsiGuestWebsite entity)
        {
            XsiGuestWebsite OriginalEntity = GetGuestWebsiteByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (GuestWebsiteRepository = new GuestWebsiteRepository(ConnectionString))
                {
                    GuestWebsiteRepository.Update(entity);
                    if (GuestWebsiteRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateGuestWebsiteStatus(long itemId)
        {
            return EnumResultType.NotFound;
        }
        public EnumResultType DeleteGuestWebsite(XsiGuestWebsite entity)
        {
            List<XsiGuestWebsite> entities = GetGuestWebsite(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiGuestWebsite e in entities)
                {
                    result = DeleteGuestWebsite(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteGuestWebsite(long itemId)
        {
            XsiGuestWebsite entity = GetGuestWebsiteByItemId(itemId);
            if (entity != null)
            {
                using (GuestWebsiteRepository = new GuestWebsiteRepository(ConnectionString))
                {
                    GuestWebsiteRepository.Delete(itemId);
                    if (GuestWebsiteRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
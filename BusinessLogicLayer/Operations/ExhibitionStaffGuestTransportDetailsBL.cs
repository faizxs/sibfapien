﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionStaffGuestTransportDetailsBL : IDisposable
    {
        #region Fields
        IExhibitionStaffGuestTransportDetailsRepository IExhibitionStaffGuestTransportDetailsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestTransportDetailsBL()
        {
            ConnectionString = null;
        }
        public ExhibitionStaffGuestTransportDetailsBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionStaffGuestTransportDetailsRepository != null)
                this.IExhibitionStaffGuestTransportDetailsRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionStaffGuestTransportDetailsRepository = new ExhibitionStaffGuestTransportDetailRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionStaffGuestTransportDetails GetExhibitionStaffGuestTransportDetailsByItemId(long itemId)
        {
            using (IExhibitionStaffGuestTransportDetailsRepository = new ExhibitionStaffGuestTransportDetailRepository(ConnectionString))
            {
                return IExhibitionStaffGuestTransportDetailsRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionStaffGuestTransportDetails> GetExhibitionStaffGuestTransportDetails()
        {
            using (IExhibitionStaffGuestTransportDetailsRepository = new ExhibitionStaffGuestTransportDetailRepository(ConnectionString))
            {
                return IExhibitionStaffGuestTransportDetailsRepository.Select();
            }
        }
        public List<XsiExhibitionStaffGuestTransportDetails> GetExhibitionStaffGuestTransportDetails(EnumSortlistBy sortListBy)
        {
            using (IExhibitionStaffGuestTransportDetailsRepository = new ExhibitionStaffGuestTransportDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionStaffGuestTransportDetailsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionStaffGuestTransportDetailsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionStaffGuestTransportDetailsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionStaffGuestTransportDetailsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionStaffGuestTransportDetailsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionStaffGuestTransportDetails> GetExhibitionStaffGuestTransportDetails(XsiExhibitionStaffGuestTransportDetails entity)
        {
            using (IExhibitionStaffGuestTransportDetailsRepository = new ExhibitionStaffGuestTransportDetailRepository(ConnectionString))
            {
                return IExhibitionStaffGuestTransportDetailsRepository.Select(entity);
            }
        }
        public List<XsiExhibitionStaffGuestTransportDetails> GetExhibitionStaffGuestTransportDetails(XsiExhibitionStaffGuestTransportDetails entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionStaffGuestTransportDetailsRepository = new ExhibitionStaffGuestTransportDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionStaffGuestTransportDetailsRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionStaffGuestTransportDetailsRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionStaffGuestTransportDetailsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionStaffGuestTransportDetailsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionStaffGuestTransportDetailsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionStaffGuestTransportDetails GetExhibitionStaffGuestTransportDetails(long groupId, long languageId)
        {
            using (IExhibitionStaffGuestTransportDetailsRepository = new ExhibitionStaffGuestTransportDetailRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionStaffGuestTransportDetails(XsiExhibitionStaffGuestTransportDetails entity)
        {
            using (IExhibitionStaffGuestTransportDetailsRepository = new ExhibitionStaffGuestTransportDetailRepository(ConnectionString))
            {
                entity = IExhibitionStaffGuestTransportDetailsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionStaffGuestTransportDetails(XsiExhibitionStaffGuestTransportDetails entity)
        {
            var where = new XsiExhibitionStaffGuestTransportDetails();
            where.ItemId = entity.ItemId;
            XsiExhibitionStaffGuestTransportDetails OriginalEntity = GetExhibitionStaffGuestTransportDetails(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

            if (OriginalEntity != null)
            {
                using (IExhibitionStaffGuestTransportDetailsRepository = new ExhibitionStaffGuestTransportDetailRepository(ConnectionString))
                {

                    IExhibitionStaffGuestTransportDetailsRepository.Update(entity);
                    if (IExhibitionStaffGuestTransportDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackExhibitionStaffGuestTransportDetails(long itemId)
        {
            //if no RollbackXml propery in ExhibitionStaffGuestTransportDetails then delete below code and return EnumResultType.Invalid;

            return EnumResultType.Failed;

        }
        public EnumResultType UpdateExhibitionStaffGuestTransportDetailsStatus(long itemId)
        {
            XsiExhibitionStaffGuestTransportDetails entity = GetExhibitionStaffGuestTransportDetailsByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionStaffGuestTransportDetailsRepository = new ExhibitionStaffGuestTransportDetailRepository(ConnectionString))
                {
                   entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionStaffGuestTransportDetailsRepository.Update(entity);
                    if (IExhibitionStaffGuestTransportDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionStaffGuestTransportDetails(XsiExhibitionStaffGuestTransportDetails entity)
        {
            List<XsiExhibitionStaffGuestTransportDetails> entities = GetExhibitionStaffGuestTransportDetails(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionStaffGuestTransportDetails e in entities)
                {
                    result = DeleteExhibitionStaffGuestTransportDetails(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionStaffGuestTransportDetails(long itemId)
        {
            var where = new XsiExhibitionStaffGuestTransportDetails();
            where.ItemId = itemId;
            XsiExhibitionStaffGuestTransportDetails entity = GetExhibitionStaffGuestTransportDetails(where,EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            if (entity != null)
            {
                using (IExhibitionStaffGuestTransportDetailsRepository = new ExhibitionStaffGuestTransportDetailRepository(ConnectionString))
                {
                    IExhibitionStaffGuestTransportDetailsRepository.Delete(itemId);
                    if (IExhibitionStaffGuestTransportDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
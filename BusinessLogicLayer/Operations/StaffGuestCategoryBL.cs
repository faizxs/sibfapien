﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class StaffGuestCategoryBL : IDisposable
    {
        #region Feilds
        IStaffGuestCategoryRepository StaffGuestCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public StaffGuestCategoryBL()
        {
            ConnectionString = null;
        }
        public StaffGuestCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.StaffGuestCategoryRepository != null)
                this.StaffGuestCategoryRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiStaffGuestCategory GetStaffGuestCategoryByItemId(long itemId)
        {
            using (StaffGuestCategoryRepository = new StaffGuestCategoryRepository(ConnectionString))
            {
                return StaffGuestCategoryRepository.GetById(itemId);
            }
        }

        public List<XsiStaffGuestCategory> GetStaffGuestCategory()
        {
            using (StaffGuestCategoryRepository = new StaffGuestCategoryRepository(ConnectionString))
            {
                return StaffGuestCategoryRepository.Select();
            }
        }
        public List<XsiStaffGuestCategory> GetStaffGuestCategory(EnumSortlistBy sortListBy)
        {
            using (StaffGuestCategoryRepository = new StaffGuestCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return StaffGuestCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return StaffGuestCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return StaffGuestCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return StaffGuestCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return StaffGuestCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return StaffGuestCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return StaffGuestCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiStaffGuestCategory> GetStaffGuestCategory(XsiStaffGuestCategory entity)
        {
            using (StaffGuestCategoryRepository = new StaffGuestCategoryRepository(ConnectionString))
            {
                return StaffGuestCategoryRepository.Select(entity);
            }
        }
        public List<XsiStaffGuestCategory> GetStaffGuestCategory(XsiStaffGuestCategory entity, EnumSortlistBy sortListBy)
        {
            using (StaffGuestCategoryRepository = new StaffGuestCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return StaffGuestCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return StaffGuestCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return StaffGuestCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return StaffGuestCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return StaffGuestCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return StaffGuestCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return StaffGuestCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiStaffGuestCategory> GetStaffGuestCategoryOr(XsiStaffGuestCategory entity, EnumSortlistBy sortListBy)
        {
            using (StaffGuestCategoryRepository = new StaffGuestCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return StaffGuestCategoryRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return StaffGuestCategoryRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return StaffGuestCategoryRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return StaffGuestCategoryRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return StaffGuestCategoryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return StaffGuestCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return StaffGuestCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertStaffGuestCategory(XsiStaffGuestCategory entity)
        {
            using (StaffGuestCategoryRepository = new StaffGuestCategoryRepository(ConnectionString))
            {
                entity = StaffGuestCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateStaffGuestCategory(XsiStaffGuestCategory entity)
        {
            XsiStaffGuestCategory OriginalEntity = GetStaffGuestCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (StaffGuestCategoryRepository = new StaffGuestCategoryRepository(ConnectionString))
                {
                    StaffGuestCategoryRepository.Update(entity);
                    if (StaffGuestCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateStaffGuestCategoryStatus(long itemId, long langId)
        {
            XsiStaffGuestCategory entity = GetStaffGuestCategoryByItemId(itemId);
            if (entity != null)
            {
                using (StaffGuestCategoryRepository = new StaffGuestCategoryRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    StaffGuestCategoryRepository.Update(entity);
                    if (StaffGuestCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteStaffGuestCategory(XsiStaffGuestCategory entity)
        {
            List<XsiStaffGuestCategory> entities = GetStaffGuestCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiStaffGuestCategory e in entities)
                {
                    result = DeleteStaffGuestCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteStaffGuestCategory(long itemId)
        {
            XsiStaffGuestCategory entity = GetStaffGuestCategoryByItemId(itemId);
            if (entity != null)
            {
                using (StaffGuestCategoryRepository = new StaffGuestCategoryRepository(ConnectionString))
                {
                    StaffGuestCategoryRepository.Delete(itemId);
                    if (StaffGuestCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
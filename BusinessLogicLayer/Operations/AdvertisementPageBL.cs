﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class AdvertisementPageBL : IDisposable
    {
        #region Feilds
        IAdvertisementPageRepository AdvertisementPageRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public AdvertisementPageBL()
        {
            ConnectionString = null;
        }
        public AdvertisementPageBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AdvertisementPageRepository != null)
                this.AdvertisementPageRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (AdvertisementPageRepository = new AdvertisementPageRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiAdvertisementPage GetAdvertisementPageByItemId(long itemId)
        {
            using (AdvertisementPageRepository = new AdvertisementPageRepository(ConnectionString))
            {
                return AdvertisementPageRepository.GetById(itemId);
            }
        }

        public List<XsiAdvertisementPage> GetAdvertisementPage()
        {
            using (AdvertisementPageRepository = new AdvertisementPageRepository(ConnectionString))
            {
                return AdvertisementPageRepository.Select();
            }
        }
        public List<XsiAdvertisementPage> GetAdvertisementPage(EnumSortlistBy sortListBy)
        {
            using (AdvertisementPageRepository = new AdvertisementPageRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return AdvertisementPageRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AdvertisementPageRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return AdvertisementPageRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiAdvertisementPage> GetAdvertisementPage(XsiAdvertisementPage entity)
        {
            using (AdvertisementPageRepository = new AdvertisementPageRepository(ConnectionString))
            {
                return AdvertisementPageRepository.Select(entity);
            }
        }
        public List<XsiAdvertisementPage> GetAdvertisementPage(XsiAdvertisementPage entity, EnumSortlistBy sortListBy)
        {
            using (AdvertisementPageRepository = new AdvertisementPageRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return AdvertisementPageRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AdvertisementPageRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return AdvertisementPageRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiAdvertisementPage GetAdvertisementPage(long groupId, long languageId)
        {
            using (AdvertisementPageRepository = new AdvertisementPageRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertAdvertisementPage(XsiAdvertisementPage entity)
        {
            using (AdvertisementPageRepository = new AdvertisementPageRepository(ConnectionString))
            {
                entity = AdvertisementPageRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateAdvertisementPage(XsiAdvertisementPage entity)
        {
            XsiAdvertisementPage OriginalEntity = GetAdvertisementPageByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (AdvertisementPageRepository = new AdvertisementPageRepository(ConnectionString))
                {
                    AdvertisementPageRepository.Update(entity);
                    if (AdvertisementPageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackAdvertisementPage(long itemId)
        {
            //if no RollbackXml propery in AdvertisementPage then delete below code and return EnumResultType.Invalid;
            return EnumResultType.Invalid;
        }
        public EnumResultType UpdateAdvertisementPageStatus(long itemId)
        {
            XsiAdvertisementPage entity = GetAdvertisementPageByItemId(itemId);
            if (entity != null)
            {
                using (AdvertisementPageRepository = new AdvertisementPageRepository(ConnectionString))
                {
                    AdvertisementPageRepository.Update(entity);
                    if (AdvertisementPageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteAdvertisementPage(XsiAdvertisementPage entity)
        {
            List<XsiAdvertisementPage> entities = GetAdvertisementPage(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiAdvertisementPage e in entities)
                {
                    result = DeleteAdvertisementPage(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteAdvertisementPage(long itemId)
        {
            XsiAdvertisementPage entity = GetAdvertisementPageByItemId(itemId);
            if (entity != null)
            {
                using (AdvertisementPageRepository = new AdvertisementPageRepository(ConnectionString))
                {
                    AdvertisementPageRepository.Delete(itemId);
                    if (AdvertisementPageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
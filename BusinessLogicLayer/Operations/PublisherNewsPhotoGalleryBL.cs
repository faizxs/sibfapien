﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PublisherNewsPhotoGalleryBL : IDisposable
    {
        #region Feilds
        IPublisherNewsPhotoGalleryRepository PublisherNewsPhotoGalleryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PublisherNewsPhotoGalleryBL()
        {
            ConnectionString = null;
        }
        public PublisherNewsPhotoGalleryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PublisherNewsPhotoGalleryRepository != null)
                this.PublisherNewsPhotoGalleryRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (PublisherNewsPhotoGalleryRepository = new PublisherNewsPhotoGalleryRepository(ConnectionString))
            {
                return -1;
            }
        }
        public long GetNextSortIndex()
        {
            //optional method. return -1 if not required and remove following
            using (PublisherNewsPhotoGalleryRepository = new PublisherNewsPhotoGalleryRepository(ConnectionString))
            {
                return (PublisherNewsPhotoGalleryRepository.Select().Max(p => p.SortIndex) ?? 0) + 1;
            }
        }

        public XsiPublisherNewsPhotoGallery GetPublisherNewsPhotoGalleryByItemId(long itemId)
        {
            using (PublisherNewsPhotoGalleryRepository = new PublisherNewsPhotoGalleryRepository(ConnectionString))
            {
                return PublisherNewsPhotoGalleryRepository.GetById(itemId);
            }
        }
        
        public List<XsiPublisherNewsPhotoGallery> GetPublisherNewsPhotoGallery()
        {
            using (PublisherNewsPhotoGalleryRepository = new PublisherNewsPhotoGalleryRepository(ConnectionString))
            {
                return PublisherNewsPhotoGalleryRepository.Select();
            }
        }
        public List<XsiPublisherNewsPhotoGallery> GetPublisherNewsPhotoGallery(EnumSortlistBy sortListBy)
        {
            using (PublisherNewsPhotoGalleryRepository = new PublisherNewsPhotoGalleryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PublisherNewsPhotoGalleryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PublisherNewsPhotoGalleryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PublisherNewsPhotoGalleryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PublisherNewsPhotoGalleryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PublisherNewsPhotoGalleryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PublisherNewsPhotoGalleryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PublisherNewsPhotoGalleryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPublisherNewsPhotoGallery> GetPublisherNewsPhotoGallery(XsiPublisherNewsPhotoGallery entity)
        {
            using (PublisherNewsPhotoGalleryRepository = new PublisherNewsPhotoGalleryRepository(ConnectionString))
            {
                return PublisherNewsPhotoGalleryRepository.Select(entity);
            }
        }
        public List<XsiPublisherNewsPhotoGallery> GetPublisherNewsPhotoGallery(XsiPublisherNewsPhotoGallery entity, EnumSortlistBy sortListBy)
        {
            using (PublisherNewsPhotoGalleryRepository = new PublisherNewsPhotoGalleryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PublisherNewsPhotoGalleryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PublisherNewsPhotoGalleryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PublisherNewsPhotoGalleryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PublisherNewsPhotoGalleryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PublisherNewsPhotoGalleryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PublisherNewsPhotoGalleryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.BySortOrderAsc:
                        return PublisherNewsPhotoGalleryRepository.Select(entity).OrderBy(p => p.SortIndex).ToList();

                    case EnumSortlistBy.BySortOrderDesc:
                        return PublisherNewsPhotoGalleryRepository.Select(entity).OrderByDescending(p => p.SortIndex).ToList();

                    default:
                        return PublisherNewsPhotoGalleryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiPublisherNewsPhotoGallery GetPublisherNewsPhotoGallery(long groupId, long languageId)
        {
            using (PublisherNewsPhotoGalleryRepository = new PublisherNewsPhotoGalleryRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertPublisherNewsPhotoGallery(XsiPublisherNewsPhotoGallery entity)
        {
            using (PublisherNewsPhotoGalleryRepository = new PublisherNewsPhotoGalleryRepository(ConnectionString))
            {
                entity = PublisherNewsPhotoGalleryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePublisherNewsPhotoGallery(XsiPublisherNewsPhotoGallery entity)
        {
            XsiPublisherNewsPhotoGallery OriginalEntity = GetPublisherNewsPhotoGalleryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PublisherNewsPhotoGalleryRepository = new PublisherNewsPhotoGalleryRepository(ConnectionString))
                {
                    PublisherNewsPhotoGalleryRepository.Update(entity);
                    if (PublisherNewsPhotoGalleryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdatePublisherNewsPhotoGalleryStatus(long itemId)
        {
            XsiPublisherNewsPhotoGallery entity = GetPublisherNewsPhotoGalleryByItemId(itemId);
            if (entity != null)
            {
                using (PublisherNewsPhotoGalleryRepository = new PublisherNewsPhotoGalleryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    PublisherNewsPhotoGalleryRepository.Update(entity);
                    if (PublisherNewsPhotoGalleryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePublisherNewsPhotoGallery(XsiPublisherNewsPhotoGallery entity)
        {
            List<XsiPublisherNewsPhotoGallery> entities = GetPublisherNewsPhotoGallery(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiPublisherNewsPhotoGallery e in entities)
                {
                    result = DeletePublisherNewsPhotoGallery(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeletePublisherNewsPhotoGallery(long itemId)
        {
            XsiPublisherNewsPhotoGallery entity = GetPublisherNewsPhotoGalleryByItemId(itemId);
            if (entity != null)
            {
                using (PublisherNewsPhotoGalleryRepository = new PublisherNewsPhotoGalleryRepository(ConnectionString))
                {
                    PublisherNewsPhotoGalleryRepository.Delete(itemId);
                    if (PublisherNewsPhotoGalleryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
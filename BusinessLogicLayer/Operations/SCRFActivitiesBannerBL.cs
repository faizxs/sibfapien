﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFActivitiesBannerBL : IDisposable
    {
        #region Feilds
        ISCRFActivitiesBannerRepository SCRFActivitiesBannerRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFActivitiesBannerBL()
        {
            ConnectionString = null;
        }
        public SCRFActivitiesBannerBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFActivitiesBannerRepository != null)
                this.SCRFActivitiesBannerRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrfactivitiesBanner GetSCRFActivitiesBannerByItemId(long itemId)
        {
            using (SCRFActivitiesBannerRepository = new SCRFActivitiesBannerRepository(ConnectionString))
            {
                return SCRFActivitiesBannerRepository.GetById(itemId);
            }
        }

        public List<XsiScrfactivitiesBanner> GetSCRFActivitiesBanner()
        {
            using (SCRFActivitiesBannerRepository = new SCRFActivitiesBannerRepository(ConnectionString))
            {
                return SCRFActivitiesBannerRepository.Select();
            }
        }
        public List<XsiScrfactivitiesBanner> GetSCRFActivitiesBanner(EnumSortlistBy sortListBy)
        {
            using (SCRFActivitiesBannerRepository = new SCRFActivitiesBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFActivitiesBannerRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFActivitiesBannerRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFActivitiesBannerRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFActivitiesBannerRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFActivitiesBannerRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFActivitiesBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFActivitiesBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfactivitiesBanner> GetSCRFActivitiesBanner(XsiScrfactivitiesBanner entity)
        {
            using (SCRFActivitiesBannerRepository = new SCRFActivitiesBannerRepository(ConnectionString))
            {
                return SCRFActivitiesBannerRepository.Select(entity);
            }
        }
        public List<XsiScrfactivitiesBanner> GetSCRFActivitiesBanner(XsiScrfactivitiesBanner entity, EnumSortlistBy sortListBy)
        {
            using (SCRFActivitiesBannerRepository = new SCRFActivitiesBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFActivitiesBannerRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFActivitiesBannerRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFActivitiesBannerRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFActivitiesBannerRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFActivitiesBannerRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFActivitiesBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFActivitiesBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiScrfactivitiesBanner> GetSCRFActivitiesBannerOr(XsiScrfactivitiesBanner entity, EnumSortlistBy sortListBy)
        {
            using (SCRFActivitiesBannerRepository = new SCRFActivitiesBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFActivitiesBannerRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFActivitiesBannerRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFActivitiesBannerRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFActivitiesBannerRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFActivitiesBannerRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFActivitiesBannerRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFActivitiesBannerRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFActivitiesBanner(XsiScrfactivitiesBanner entity)
        {
            using (SCRFActivitiesBannerRepository = new SCRFActivitiesBannerRepository(ConnectionString))
            {
                entity = SCRFActivitiesBannerRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFActivitiesBanner(XsiScrfactivitiesBanner entity)
        {
            XsiScrfactivitiesBanner OriginalEntity = GetSCRFActivitiesBannerByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFActivitiesBannerRepository = new SCRFActivitiesBannerRepository(ConnectionString))
                {

                    SCRFActivitiesBannerRepository.Update(entity);
                    if (SCRFActivitiesBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFActivitiesBannerStatus(long itemId)
        {
            XsiScrfactivitiesBanner entity = GetSCRFActivitiesBannerByItemId(itemId);
            if (entity != null)
            {
                using (SCRFActivitiesBannerRepository = new SCRFActivitiesBannerRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFActivitiesBannerRepository.Update(entity);
                    if (SCRFActivitiesBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFActivitiesBanner(XsiScrfactivitiesBanner entity)
        {
            List<XsiScrfactivitiesBanner> entities = GetSCRFActivitiesBanner(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfactivitiesBanner e in entities)
                {
                    result = DeleteSCRFActivitiesBanner(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFActivitiesBanner(long itemId)
        {
            XsiScrfactivitiesBanner entity = GetSCRFActivitiesBannerByItemId(itemId);
            if (entity != null)
            {
                using (SCRFActivitiesBannerRepository = new SCRFActivitiesBannerRepository(ConnectionString))
                {
                    SCRFActivitiesBannerRepository.Delete(itemId);
                    if (SCRFActivitiesBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        //Custom Methods
        public long GetHomeBannerInfo(long languageid)
        {
            using (SCRFActivitiesBannerRepository = new SCRFActivitiesBannerRepository(ConnectionString))
            {
                return SCRFActivitiesBannerRepository.Select(new XsiScrfactivitiesBanner() { IsActive = EnumConversion.ToString(EnumBool.Yes) }).Count();
            }
        }
        #endregion
    }
}
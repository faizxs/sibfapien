﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class CareerYOBBL : IDisposable
    {
        #region Feilds
        ICareerYOBRepository CareerYOBRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public CareerYOBBL()
        {
            ConnectionString = null;
        }
        public CareerYOBBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerYOBRepository != null)
                this.CareerYOBRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiCareerYob GetCareerYOBByItemId(long itemId)
        {
            using (CareerYOBRepository = new CareerYOBRepository(ConnectionString))
            {
                return CareerYOBRepository.GetById(itemId);
            }
        }
          
        public List<XsiCareerYob> GetCareerYOB()
        {
            using (CareerYOBRepository = new CareerYOBRepository(ConnectionString))
            {
                return CareerYOBRepository.Select();
            }
        }
        public List<XsiCareerYob> GetCareerYOB(EnumSortlistBy sortListBy)
        {
            using (CareerYOBRepository = new CareerYOBRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerYOBRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerYOBRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerYOBRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerYOBRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerYOBRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerYOBRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerYOBRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareerYob> GetCareerYOB(XsiCareerYob entity)
        {
            using (CareerYOBRepository = new CareerYOBRepository(ConnectionString))
            {
                return CareerYOBRepository.Select(entity);
            }
        }
        public List<XsiCareerYob> GetCareerYOB(XsiCareerYob entity, EnumSortlistBy sortListBy)
        {
            using (CareerYOBRepository = new CareerYOBRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerYOBRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerYOBRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerYOBRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerYOBRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerYOBRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerYOBRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerYOBRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
         
        public EnumResultType InsertCareerYOB(XsiCareerYob entity)
        {
            using (CareerYOBRepository = new CareerYOBRepository(ConnectionString))
            {
                entity = CareerYOBRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateCareerYOB(XsiCareerYob entity)
        {
            XsiCareerYob OriginalEntity = GetCareerYOBByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (CareerYOBRepository = new CareerYOBRepository(ConnectionString))
                {
                    CareerYOBRepository.Update(entity);
                    if (CareerYOBRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateCareerYOBStatus(long itemId)
        {
            XsiCareerYob entity = GetCareerYOBByItemId(itemId);
            if (entity != null)
            {
                using (CareerYOBRepository = new CareerYOBRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    CareerYOBRepository.Update(entity);
                    if (CareerYOBRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteCareerYOB(XsiCareerYob entity)
        {
            List<XsiCareerYob> entities = GetCareerYOB(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiCareerYob e in entities)
                {
                    result = DeleteCareerYOB(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteCareerYOB(long itemId)
        {
            XsiCareerYob entity = GetCareerYOBByItemId(itemId);
            if (entity != null)
            {
                using (CareerYOBRepository = new CareerYOBRepository(ConnectionString))
                {
                    CareerYOBRepository.Delete(itemId);
                    if (CareerYOBRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
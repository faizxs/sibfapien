﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ActivitiesBannerBL : IDisposable
    {
        #region Feilds
        IActivitiesBannerRepository ActivitiesBannerRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ActivitiesBannerBL()
        {
            ConnectionString = null;
        }
        public ActivitiesBannerBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ActivitiesBannerRepository != null)
                this.ActivitiesBannerRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiActivitiesBanner GetActivitiesBannerByItemId(long itemId)
        {
            using (ActivitiesBannerRepository = new ActivitiesBannerRepository(ConnectionString))
            {
                return ActivitiesBannerRepository.GetById(itemId);
            }
        }
        public List<XsiActivitiesBanner> GetActivitiesBanner()
        {
            using (ActivitiesBannerRepository = new ActivitiesBannerRepository(ConnectionString))
            {
                return ActivitiesBannerRepository.Select();
            }
        }
        public List<XsiActivitiesBanner> GetActivitiesBanner(EnumSortlistBy sortListBy)
        {
            using (ActivitiesBannerRepository = new ActivitiesBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ActivitiesBannerRepository.Select().OrderBy(p => p.Title).ToList().ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ActivitiesBannerRepository.Select().OrderByDescending(p => p.Title).ToList().ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ActivitiesBannerRepository.Select().OrderBy(p => p.CreatedOn).ToList().ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ActivitiesBannerRepository.Select().OrderByDescending(p => p.CreatedOn).ToList().ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ActivitiesBannerRepository.Select().OrderBy(p => p.ItemId).ToList().ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ActivitiesBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList().ToList();

                    default:
                        return ActivitiesBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList().ToList();
                }
            }
        }
        public List<XsiActivitiesBanner> GetActivitiesBanner(XsiActivitiesBanner entity)
        {
            using (ActivitiesBannerRepository = new ActivitiesBannerRepository(ConnectionString))
            {
                return ActivitiesBannerRepository.Select(entity).ToList();
            }
        }
        public List<XsiActivitiesBanner> GetActivitiesBanner(XsiActivitiesBanner entity, EnumSortlistBy sortListBy)
        {
            using (ActivitiesBannerRepository = new ActivitiesBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ActivitiesBannerRepository.Select(entity).OrderBy(p => p.Title).ToList().ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ActivitiesBannerRepository.Select(entity).OrderByDescending(p => p.Title).ToList().ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ActivitiesBannerRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList().ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ActivitiesBannerRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList().ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ActivitiesBannerRepository.Select(entity).OrderBy(p => p.ItemId).ToList().ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ActivitiesBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList().ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return ActivitiesBannerRepository.Select(entity).OrderBy(p => p.SortOrder).ToList().ToList(); 

                    default:
                        return ActivitiesBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList().ToList();
                }
            }
        }
        public List<XsiActivitiesBanner> GetActivitiesBannerOr(XsiActivitiesBanner entity, EnumSortlistBy sortListBy)
        {
            using (ActivitiesBannerRepository = new ActivitiesBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ActivitiesBannerRepository.SelectOr(entity).OrderBy(p => p.Title).ToList().ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ActivitiesBannerRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList().ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ActivitiesBannerRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList().ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ActivitiesBannerRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList().ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ActivitiesBannerRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList().ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ActivitiesBannerRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList().ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return ActivitiesBannerRepository.SelectOr(entity).OrderBy(p => p.SortOrder).ToList().ToList();

                    default:
                        return ActivitiesBannerRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList().ToList();
                }
            }
        }

        public EnumResultType InsertActivitiesBanner(XsiActivitiesBanner entity)
        {
            using (ActivitiesBannerRepository = new ActivitiesBannerRepository(ConnectionString))
            {
                entity = ActivitiesBannerRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateActivitiesBanner(XsiActivitiesBanner entity)
        {
            XsiActivitiesBanner OriginalEntity = GetActivitiesBannerByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ActivitiesBannerRepository = new ActivitiesBannerRepository(ConnectionString))
                {

                    ActivitiesBannerRepository.Update(entity);
                    if (ActivitiesBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateActivitiesBannerStatus(long itemId)
        {
            XsiActivitiesBanner entity = GetActivitiesBannerByItemId(itemId);
            if (entity != null)
            {
                using (ActivitiesBannerRepository = new ActivitiesBannerRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ActivitiesBannerRepository.Update(entity);
                    if (ActivitiesBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteActivitiesBanner(XsiActivitiesBanner entity)
        {
            List<XsiActivitiesBanner> entities = GetActivitiesBanner(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiActivitiesBanner e in entities)
                {
                    result = DeleteActivitiesBanner(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteActivitiesBanner(long itemId)
        {
            XsiActivitiesBanner entity = GetActivitiesBannerByItemId(itemId);
            if (entity != null)
            {
                using (ActivitiesBannerRepository = new ActivitiesBannerRepository(ConnectionString))
                {
                    ActivitiesBannerRepository.Delete(itemId);
                    if (ActivitiesBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        //Custom Methods
        public long GetHomeBannerInfo(long languageid)
        {
            using (ActivitiesBannerRepository = new ActivitiesBannerRepository(ConnectionString))
            {
                return ActivitiesBannerRepository.Select(new XsiActivitiesBanner() { IsActive = EnumConversion.ToString(EnumBool.Yes) }).Count();
            }
        }
        #endregion
    }
}
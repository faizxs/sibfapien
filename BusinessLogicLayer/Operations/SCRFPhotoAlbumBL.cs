﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFPhotoAlbumBL : IDisposable
    {
        #region Feilds
        ISCRFPhotoAlbumRepository SCRFPhotoAlbumRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFPhotoAlbumBL()
        {
            ConnectionString = null;
        }
        public SCRFPhotoAlbumBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFPhotoAlbumRepository != null)
                this.SCRFPhotoAlbumRepository.Dispose();
        }
        #endregion
        #region Methods
       
        public long GetNextSortIndex()
        {
            //optional method. return -1 if not required and remove following
            using (SCRFPhotoAlbumRepository = new SCRFPhotoAlbumRepository(ConnectionString))
            {
                return (SCRFPhotoAlbumRepository.Select().Max(p => p.SortIndex) ?? 0) + 1;
            }
        }

        public XsiScrfphotoAlbum GetSCRFPhotoAlbumByItemId(long itemId)
        {
            using (SCRFPhotoAlbumRepository = new SCRFPhotoAlbumRepository(ConnectionString))
            {
                return SCRFPhotoAlbumRepository.GetById(itemId);
            }
        }
       
        public List<XsiScrfphotoAlbum> GetSCRFPhotoAlbum()
        {
            using (SCRFPhotoAlbumRepository = new SCRFPhotoAlbumRepository(ConnectionString))
            {
                return SCRFPhotoAlbumRepository.Select();
            }
        }
        public List<XsiScrfphotoAlbum> GetSCRFPhotoAlbum(EnumSortlistBy sortListBy)
        {
            using (SCRFPhotoAlbumRepository = new SCRFPhotoAlbumRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPhotoAlbumRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPhotoAlbumRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFPhotoAlbumRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFPhotoAlbumRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPhotoAlbumRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPhotoAlbumRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFPhotoAlbumRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfphotoAlbum> GetSCRFPhotoAlbum(XsiScrfphotoAlbum entity)
        {
            using (SCRFPhotoAlbumRepository = new SCRFPhotoAlbumRepository(ConnectionString))
            {
                return SCRFPhotoAlbumRepository.Select(entity);
            }
        }
        public List<XsiScrfphotoAlbum> GetSCRFPhotoAlbum(XsiScrfphotoAlbum entity, EnumSortlistBy sortListBy)
        {
            using (SCRFPhotoAlbumRepository = new SCRFPhotoAlbumRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPhotoAlbumRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPhotoAlbumRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFPhotoAlbumRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFPhotoAlbumRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPhotoAlbumRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPhotoAlbumRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.BySortOrderAsc:
                        return SCRFPhotoAlbumRepository.Select(entity).OrderBy(p => p.SortIndex).ToList();

                    case EnumSortlistBy.BySortOrderDesc:
                        return SCRFPhotoAlbumRepository.Select(entity).OrderByDescending(p => p.SortIndex).ToList();

                    default:
                        return SCRFPhotoAlbumRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
     
        public EnumResultType InsertSCRFPhotoAlbum(XsiScrfphotoAlbum entity)
        {
            using (SCRFPhotoAlbumRepository = new SCRFPhotoAlbumRepository(ConnectionString))
            {
                entity = SCRFPhotoAlbumRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFPhotoAlbum(XsiScrfphotoAlbum entity)
        {
            XsiScrfphotoAlbum OriginalEntity = GetSCRFPhotoAlbumByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFPhotoAlbumRepository = new SCRFPhotoAlbumRepository(ConnectionString))
                {
                    SCRFPhotoAlbumRepository.Update(entity);
                    if (SCRFPhotoAlbumRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFPhotoAlbumStatus(long itemId)
        {
            XsiScrfphotoAlbum entity = GetSCRFPhotoAlbumByItemId(itemId);
            if (entity != null)
            {
                using (SCRFPhotoAlbumRepository = new SCRFPhotoAlbumRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFPhotoAlbumRepository.Update(entity);
                    if (SCRFPhotoAlbumRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFPhotoAlbum(XsiScrfphotoAlbum entity)
        {
            List<XsiScrfphotoAlbum> entities = GetSCRFPhotoAlbum(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfphotoAlbum e in entities)
                {
                    result = DeleteSCRFPhotoAlbum(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFPhotoAlbum(long itemId)
        {
            XsiScrfphotoAlbum entity = GetSCRFPhotoAlbumByItemId(itemId);
            if (entity != null)
            {
                using (SCRFPhotoAlbumRepository = new SCRFPhotoAlbumRepository(ConnectionString))
                {
                    SCRFPhotoAlbumRepository.Delete(itemId);
                    if (SCRFPhotoAlbumRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
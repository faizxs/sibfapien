﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFIllustratorBL : IDisposable
    {
        #region Feilds
        ISCRFIllustratorRepository SCRFIllustratorRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFIllustratorBL()
        {
            ConnectionString = null;
        }
        public SCRFIllustratorBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIllustratorRepository != null)
                this.SCRFIllustratorRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrfillustrator GetSCRFIllustratorByItemId(long itemId)
        {
            using (SCRFIllustratorRepository = new SCRFIllustratorRepository(ConnectionString))
            {
                return SCRFIllustratorRepository.GetById(itemId);
            }
        }

        public List<XsiScrfillustrator> GetSCRFIllustrator()
        {
            using (SCRFIllustratorRepository = new SCRFIllustratorRepository(ConnectionString))
            {
                return SCRFIllustratorRepository.Select();
            }
        }
        public List<XsiScrfillustrator> GetSCRFIllustrator(EnumSortlistBy sortListBy)
        {
            using (SCRFIllustratorRepository = new SCRFIllustratorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIllustratorRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIllustratorRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFIllustratorRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFIllustratorRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIllustratorRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIllustratorRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIllustratorRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfillustrator> GetSCRFIllustrator(XsiScrfillustrator entity)
        {
            using (SCRFIllustratorRepository = new SCRFIllustratorRepository(ConnectionString))
            {
                return SCRFIllustratorRepository.Select(entity);
            }
        }
        public List<XsiScrfillustrator> GetSCRFIllustrator(XsiScrfillustrator entity, EnumSortlistBy sortListBy)
        {
            using (SCRFIllustratorRepository = new SCRFIllustratorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIllustratorRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIllustratorRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFIllustratorRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFIllustratorRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIllustratorRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIllustratorRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIllustratorRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFIllustrator(XsiScrfillustrator entity)
        {
            using (SCRFIllustratorRepository = new SCRFIllustratorRepository(ConnectionString))
            {
                entity = SCRFIllustratorRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFIllustrator(XsiScrfillustrator entity)
        {
            XsiScrfillustrator OriginalEntity = GetSCRFIllustratorByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFIllustratorRepository = new SCRFIllustratorRepository(ConnectionString))
                {
                    SCRFIllustratorRepository.Update(entity);
                    if (SCRFIllustratorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFIllustratorStatus(long itemId)
        {
            XsiScrfillustrator entity = GetSCRFIllustratorByItemId(itemId);
            if (entity != null)
            {
                using (SCRFIllustratorRepository = new SCRFIllustratorRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFIllustratorRepository.Update(entity);
                    if (SCRFIllustratorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFIllustrator(XsiScrfillustrator entity)
        {
            List<XsiScrfillustrator> entities = GetSCRFIllustrator(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfillustrator e in entities)
                {
                    result = DeleteSCRFIllustrator(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFIllustrator(long itemId)
        {
            XsiScrfillustrator entity = GetSCRFIllustratorByItemId(itemId);
            if (entity != null)
            {
                using (SCRFIllustratorRepository = new SCRFIllustratorRepository(ConnectionString))
                {
                    SCRFIllustratorRepository.Delete(itemId);
                    if (SCRFIllustratorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
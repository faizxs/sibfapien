﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionMemberStatusLogBL : IDisposable
    {
        #region Feilds
        IExhibitionMemberStatusLogRepository IExhibitionMemberStatusLogRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionMemberStatusLogBL()
        {
            ConnectionString = null;
        }
        public ExhibitionMemberStatusLogBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionMemberStatusLogRepository != null)
                this.IExhibitionMemberStatusLogRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionMemberStatusLogRepository = new ExhibitionMemberStatusLogRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionMemberStatusLog GetExhibitionMemberStatusLogByItemId(long itemId)
        {
            using (IExhibitionMemberStatusLogRepository = new ExhibitionMemberStatusLogRepository(ConnectionString))
            {
                return IExhibitionMemberStatusLogRepository.GetById(itemId);
            }
        }
        

        public List<XsiExhibitionMemberStatusLog> GetExhibitionMemberStatusLog()
        {
            using (IExhibitionMemberStatusLogRepository = new ExhibitionMemberStatusLogRepository(ConnectionString))
            {
                return IExhibitionMemberStatusLogRepository.Select();
            }
        }
        public List<XsiExhibitionMemberStatusLog> GetExhibitionMemberStatusLog(EnumSortlistBy sortListBy)
        {
            using (IExhibitionMemberStatusLogRepository = new ExhibitionMemberStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitionMemberStatusLogRepository.Select().OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitionMemberStatusLogRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionMemberStatusLogRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionMemberStatusLogRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionMemberStatusLogRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionMemberStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionMemberStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionMemberStatusLog> GetExhibitionMemberStatusLog(XsiExhibitionMemberStatusLog entity)
        {
            using (IExhibitionMemberStatusLogRepository = new ExhibitionMemberStatusLogRepository(ConnectionString))
            {
                return IExhibitionMemberStatusLogRepository.Select(entity);
            }
        }
        public List<XsiExhibitionMemberStatusLog> GetExhibitionMemberStatusLog(XsiExhibitionMemberStatusLog entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionMemberStatusLogRepository = new ExhibitionMemberStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitionMemberStatusLogRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitionMemberStatusLogRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionMemberStatusLogRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionMemberStatusLogRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionMemberStatusLogRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionMemberStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionMemberStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionMemberStatusLog GetExhibitionMemberStatusLog(long groupId, long languageId)
        {
            using (IExhibitionMemberStatusLogRepository = new ExhibitionMemberStatusLogRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionMemberStatusLog(XsiExhibitionMemberStatusLog entity)
        {
            using (IExhibitionMemberStatusLogRepository = new ExhibitionMemberStatusLogRepository(ConnectionString))
            {
                entity = IExhibitionMemberStatusLogRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionMemberStatusLog(XsiExhibitionMemberStatusLog entity)
        {
            XsiExhibitionMemberStatusLog OriginalEntity = GetExhibitionMemberStatusLogByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IExhibitionMemberStatusLogRepository = new ExhibitionMemberStatusLogRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line

                    IExhibitionMemberStatusLogRepository.Update(entity);
                    if (IExhibitionMemberStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionMemberStatusLogStatus(long itemId)
        {
            XsiExhibitionMemberStatusLog entity = GetExhibitionMemberStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionMemberStatusLogRepository = new ExhibitionMemberStatusLogRepository(ConnectionString))
                {
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionMemberStatusLogRepository.Update(entity);
                    if (IExhibitionMemberStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionMemberStatusLog(XsiExhibitionMemberStatusLog entity)
        {
            List<XsiExhibitionMemberStatusLog> entities = GetExhibitionMemberStatusLog(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionMemberStatusLog e in entities)
                {
                    result = DeleteExhibitionMemberStatusLog(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionMemberStatusLog(long itemId)
        {
            XsiExhibitionMemberStatusLog entity = GetExhibitionMemberStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionMemberStatusLogRepository = new ExhibitionMemberStatusLogRepository(ConnectionString))
                {
                    IExhibitionMemberStatusLogRepository.Delete(itemId);
                    if (IExhibitionMemberStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
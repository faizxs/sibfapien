﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class IconsForSponsorBL : IDisposable
    {
        #region Feilds
        IIconsForSponsorRepository IconsForSponsorRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public IconsForSponsorBL()
        {
            ConnectionString = null;
        }
        public IconsForSponsorBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IconsForSponsorRepository != null)
                this.IconsForSponsorRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiIconsForSponsor GetIconsForSponsorByItemId(long itemId)
        {
            using (IconsForSponsorRepository = new IconsForSponsorRepository(ConnectionString))
            {
                return IconsForSponsorRepository.GetById(itemId);
            }
        }
        public List<XsiIconsForSponsor> GetIconsForSponsor()
        {
            using (IconsForSponsorRepository = new IconsForSponsorRepository(ConnectionString))
            {
                return IconsForSponsorRepository.Select();
            }
        }
        public List<XsiIconsForSponsor> GetIconsForSponsor(EnumSortlistBy sortListBy)
        {
            using (IconsForSponsorRepository = new IconsForSponsorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IconsForSponsorRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IconsForSponsorRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IconsForSponsorRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IconsForSponsorRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IconsForSponsorRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IconsForSponsorRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IconsForSponsorRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiIconsForSponsor> GetIconsForSponsor(XsiIconsForSponsor entity)
        {
            using (IconsForSponsorRepository = new IconsForSponsorRepository(ConnectionString))
            {
                return IconsForSponsorRepository.Select(entity);
            }
        }
        public List<XsiIconsForSponsor> GetIconsForSponsor(XsiIconsForSponsor entity, EnumSortlistBy sortListBy)
        {
            using (IconsForSponsorRepository = new IconsForSponsorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IconsForSponsorRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IconsForSponsorRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IconsForSponsorRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IconsForSponsorRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IconsForSponsorRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IconsForSponsorRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IconsForSponsorRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiIconsForSponsor> GetIconsForSponsorOr(XsiIconsForSponsor entity, EnumSortlistBy sortListBy)
        {
            using (IconsForSponsorRepository = new IconsForSponsorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IconsForSponsorRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IconsForSponsorRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IconsForSponsorRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IconsForSponsorRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IconsForSponsorRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IconsForSponsorRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IconsForSponsorRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertIconsForSponsor(XsiIconsForSponsor entity)
        {
            using (IconsForSponsorRepository = new IconsForSponsorRepository(ConnectionString))
            {
                entity = IconsForSponsorRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateIconsForSponsor(XsiIconsForSponsor entity)
        {
            XsiIconsForSponsor OriginalEntity = GetIconsForSponsorByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IconsForSponsorRepository = new IconsForSponsorRepository(ConnectionString))
                {
                    IconsForSponsorRepository.Update(entity);
                    if (IconsForSponsorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateIconsForSponsorStatus(long itemId, long langId)
        {
            XsiIconsForSponsor entity = GetIconsForSponsorByItemId(itemId);
            if (entity != null)
            {
                using (IconsForSponsorRepository = new IconsForSponsorRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IconsForSponsorRepository.Update(entity);
                    if (IconsForSponsorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteIconsForSponsor(XsiIconsForSponsor entity)
        {
            List<XsiIconsForSponsor> entities = GetIconsForSponsor(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiIconsForSponsor e in entities)
                {
                    result = DeleteIconsForSponsor(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteIconsForSponsor(long itemId)
        {
            XsiIconsForSponsor entity = GetIconsForSponsorByItemId(itemId);
            if (entity != null)
            {
                using (IconsForSponsorRepository = new IconsForSponsorRepository(ConnectionString))
                {
                    IconsForSponsorRepository.Delete(itemId);
                    if (IconsForSponsorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
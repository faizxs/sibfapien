﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class TranslationGrantCategoryBL : IDisposable
    {
        #region Feilds
        ITranslationGrantCategoryRepository TranslationGrantCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public TranslationGrantCategoryBL()
        {
            ConnectionString = null;
        }
        public TranslationGrantCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.TranslationGrantCategoryRepository != null)
                this.TranslationGrantCategoryRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionTranslationGrantCategory GetTranslationGrantCategoryByItemId(long itemId)
        {
            using (TranslationGrantCategoryRepository = new TranslationGrantCategoryRepository(ConnectionString))
            {
                return TranslationGrantCategoryRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionTranslationGrantCategory> GetTranslationGrantCategory()
        {
            using (TranslationGrantCategoryRepository = new TranslationGrantCategoryRepository(ConnectionString))
            {
                return TranslationGrantCategoryRepository.Select();
            }
        }
        public List<XsiExhibitionTranslationGrantCategory> GetTranslationGrantCategory(EnumSortlistBy sortListBy)
        {
            using (TranslationGrantCategoryRepository = new TranslationGrantCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return TranslationGrantCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return TranslationGrantCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return TranslationGrantCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return TranslationGrantCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return TranslationGrantCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return TranslationGrantCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return TranslationGrantCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionTranslationGrantCategory> GetTranslationGrantCategory(XsiExhibitionTranslationGrantCategory entity)
        {
            using (TranslationGrantCategoryRepository = new TranslationGrantCategoryRepository(ConnectionString))
            {
                return TranslationGrantCategoryRepository.Select(entity);
            }
        }
        public List<XsiExhibitionTranslationGrantCategory> GetTranslationGrantCategory(XsiExhibitionTranslationGrantCategory entity, EnumSortlistBy sortListBy)
        {
            using (TranslationGrantCategoryRepository = new TranslationGrantCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return TranslationGrantCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return TranslationGrantCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return TranslationGrantCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return TranslationGrantCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return TranslationGrantCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return TranslationGrantCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return TranslationGrantCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionTranslationGrantCategory> GetTranslationGrantCategoryOr(XsiExhibitionTranslationGrantCategory entity, EnumSortlistBy sortListBy)
        {
            using (TranslationGrantCategoryRepository = new TranslationGrantCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return TranslationGrantCategoryRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return TranslationGrantCategoryRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return TranslationGrantCategoryRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return TranslationGrantCategoryRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return TranslationGrantCategoryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return TranslationGrantCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return TranslationGrantCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertTranslationGrantCategory(XsiExhibitionTranslationGrantCategory entity)
        {
            using (TranslationGrantCategoryRepository = new TranslationGrantCategoryRepository(ConnectionString))
            {
                entity = TranslationGrantCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateTranslationGrantCategory(XsiExhibitionTranslationGrantCategory entity)
        {
            XsiExhibitionTranslationGrantCategory OriginalEntity = GetTranslationGrantCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (TranslationGrantCategoryRepository = new TranslationGrantCategoryRepository(ConnectionString))
                {
                    TranslationGrantCategoryRepository.Update(entity);
                    if (TranslationGrantCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateTranslationGrantCategoryStatus(long itemId)
        {
            XsiExhibitionTranslationGrantCategory entity = GetTranslationGrantCategoryByItemId(itemId);
            if (entity != null)
            {
                using (TranslationGrantCategoryRepository = new TranslationGrantCategoryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    TranslationGrantCategoryRepository.Update(entity);
                    if (TranslationGrantCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteTranslationGrantCategory(XsiExhibitionTranslationGrantCategory entity)
        {
            List<XsiExhibitionTranslationGrantCategory> entities = GetTranslationGrantCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionTranslationGrantCategory e in entities)
                {
                    result = DeleteTranslationGrantCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteTranslationGrantCategory(long itemId)
        {
            XsiExhibitionTranslationGrantCategory entity = GetTranslationGrantCategoryByItemId(itemId);
            if (entity != null)
            {
                using (TranslationGrantCategoryRepository = new TranslationGrantCategoryRepository(ConnectionString))
                {
                    TranslationGrantCategoryRepository.Delete(itemId);
                    if (TranslationGrantCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
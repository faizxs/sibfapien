﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionRepresentativeBL : IDisposable
    {
        #region Feilds
        IExhibitionRepresentativeRepository ExhibitionRepresentativeRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionRepresentativeBL()
        {
            ConnectionString = null;
        }
        public ExhibitionRepresentativeBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionRepresentativeRepository != null)
                this.ExhibitionRepresentativeRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionRepresentative GetExhibitionRepresentativeByItemId(long itemId)
        {
            using (ExhibitionRepresentativeRepository = new ExhibitionRepresentativeRepository(ConnectionString))
            {
                return ExhibitionRepresentativeRepository.GetById(itemId);
            }
        }
         
        public List<XsiExhibitionRepresentative> GetExhibitionRepresentative()
        {
            using (ExhibitionRepresentativeRepository = new ExhibitionRepresentativeRepository(ConnectionString))
            {
                return ExhibitionRepresentativeRepository.Select();
            }
        }
        public List<XsiExhibitionRepresentative> GetExhibitionRepresentative(EnumSortlistBy sortListBy)
        {
            using (ExhibitionRepresentativeRepository = new ExhibitionRepresentativeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionRepresentativeRepository.Select().OrderBy(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionRepresentativeRepository.Select().OrderByDescending(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionRepresentativeRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionRepresentativeRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionRepresentativeRepository.Select().OrderBy(p => p.RepresentativeId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionRepresentativeRepository.Select().OrderByDescending(p => p.RepresentativeId).ToList();

                    default:
                        return ExhibitionRepresentativeRepository.Select().OrderByDescending(p => p.RepresentativeId).ToList();
                }
            }
        }
        public List<XsiExhibitionRepresentative> GetExhibitionRepresentative(XsiExhibitionRepresentative entity)
        {
            using (ExhibitionRepresentativeRepository = new ExhibitionRepresentativeRepository(ConnectionString))
            {
                return ExhibitionRepresentativeRepository.Select(entity);
            }
        }
        public List<XsiExhibitionRepresentative> GetExhibitionRepresentative(XsiExhibitionRepresentative entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionRepresentativeRepository = new ExhibitionRepresentativeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionRepresentativeRepository.Select(entity).OrderBy(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionRepresentativeRepository.Select(entity).OrderByDescending(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionRepresentativeRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionRepresentativeRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionRepresentativeRepository.Select(entity).OrderBy(p => p.RepresentativeId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionRepresentativeRepository.Select(entity).OrderByDescending(p => p.RepresentativeId).ToList();

                    default:
                        return ExhibitionRepresentativeRepository.Select(entity).OrderByDescending(p => p.RepresentativeId).ToList();
                }
            }
        }

        public EnumResultType InsertExhibitionRepresentative(XsiExhibitionRepresentative entity)
        {
            using (ExhibitionRepresentativeRepository = new ExhibitionRepresentativeRepository(ConnectionString))
            {
                entity = ExhibitionRepresentativeRepository.Add(entity);
                if (entity.RepresentativeId > 0)
                {
                    XsiItemdId = entity.RepresentativeId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionRepresentative(XsiExhibitionRepresentative entity)
        {
            //XsiExhibitionRepresentative OriginalEntity = GetExhibitionRepresentativeByItemId(entity.RepresentativeId);
            XsiExhibitionRepresentative OriginalEntity = GetExhibitionRepresentativeByItemId(entity.RepresentativeId);

            if (OriginalEntity != null)
            {
                using (ExhibitionRepresentativeRepository = new ExhibitionRepresentativeRepository(ConnectionString))
                {
                    ExhibitionRepresentativeRepository.Update(entity);
                    if (ExhibitionRepresentativeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionRepresentativeStatus(long itemId)
        {
            XsiExhibitionRepresentative entity = GetExhibitionRepresentativeByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionRepresentativeRepository = new ExhibitionRepresentativeRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionRepresentativeRepository.Update(entity);
                    if (ExhibitionRepresentativeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionRepresentative(XsiExhibitionRepresentative entity)
        {
            List<XsiExhibitionRepresentative> entities = GetExhibitionRepresentative(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionRepresentative e in entities)
                {
                    result = DeleteExhibitionRepresentative(e.RepresentativeId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionRepresentative(long itemId)
        {
            XsiExhibitionRepresentative entity = GetExhibitionRepresentativeByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionRepresentativeRepository = new ExhibitionRepresentativeRepository(ConnectionString))
                {
                    ExhibitionRepresentativeRepository.Delete(itemId);
                    if (ExhibitionRepresentativeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
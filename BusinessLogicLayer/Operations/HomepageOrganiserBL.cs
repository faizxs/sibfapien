﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class HomepageOrganiserBL : IDisposable
    {
        #region Feilds
        IHomepageOrganiserRepository HomepageOrganiserRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public HomepageOrganiserBL()
        {
            ConnectionString = null;
        }
        public HomepageOrganiserBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomepageOrganiserRepository != null)
                this.HomepageOrganiserRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiHomepageOrganiser GetHomepageOrganiserByItemId(long itemId)
        {
            using (HomepageOrganiserRepository = new HomepageOrganiserRepository(ConnectionString))
            {
                return HomepageOrganiserRepository.GetById(itemId);
            }
        }

        public List<XsiHomepageOrganiser> GetHomepageOrganiser()
        {
            using (HomepageOrganiserRepository = new HomepageOrganiserRepository(ConnectionString))
            {
                return HomepageOrganiserRepository.Select();
            }
        }
        public List<XsiHomepageOrganiser> GetHomepageOrganiser(EnumSortlistBy sortListBy)
        {
            using (HomepageOrganiserRepository = new HomepageOrganiserRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageOrganiserRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageOrganiserRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HomepageOrganiserRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HomepageOrganiserRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageOrganiserRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageOrganiserRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HomepageOrganiserRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiHomepageOrganiser> GetHomepageOrganiser(XsiHomepageOrganiser entity)
        {
            using (HomepageOrganiserRepository = new HomepageOrganiserRepository(ConnectionString))
            {
                return HomepageOrganiserRepository.Select(entity);
            }
        }
        public List<XsiHomepageOrganiser> GetHomepageOrganiser(XsiHomepageOrganiser entity, EnumSortlistBy sortListBy)
        {
            using (HomepageOrganiserRepository = new HomepageOrganiserRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageOrganiserRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageOrganiserRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HomepageOrganiserRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HomepageOrganiserRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageOrganiserRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageOrganiserRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    //case EnumSortlistBy.ByItemOrder:
                    //    return HomepageOrganiserRepository.Select(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return HomepageOrganiserRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiHomepageOrganiser GetHomepageOrganiser(long Id)
        {
            using (HomepageOrganiserRepository = new HomepageOrganiserRepository(ConnectionString))
            {
                return HomepageOrganiserRepository.Select(new XsiHomepageOrganiser() { ItemId = Id }).FirstOrDefault();
            }
        }

        public EnumResultType InsertHomepageOrganiser(XsiHomepageOrganiser entity)
        {
            using (HomepageOrganiserRepository = new HomepageOrganiserRepository(ConnectionString))
            {
                entity = HomepageOrganiserRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateHomepageOrganiser(XsiHomepageOrganiser entity)
        {
            XsiHomepageOrganiser OriginalEntity = GetHomepageOrganiserByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (HomepageOrganiserRepository = new HomepageOrganiserRepository(ConnectionString))
                {

                    HomepageOrganiserRepository.Update(entity);
                    if (HomepageOrganiserRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateHomepageOrganiserStatus(long itemId)
        {
            XsiHomepageOrganiser entity = GetHomepageOrganiserByItemId(itemId);
            if (entity != null)
            {
                using (HomepageOrganiserRepository = new HomepageOrganiserRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    HomepageOrganiserRepository.Update(entity);
                    if (HomepageOrganiserRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteHomepageOrganiser(XsiHomepageOrganiser entity)
        {
            List<XsiHomepageOrganiser> entities = GetHomepageOrganiser(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiHomepageOrganiser e in entities)
                {
                    result = DeleteHomepageOrganiser(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteHomepageOrganiser(long itemId)
        {
            XsiHomepageOrganiser entity = GetHomepageOrganiserByItemId(itemId);
            if (entity != null)
            {
                using (HomepageOrganiserRepository = new HomepageOrganiserRepository(ConnectionString))
                {
                    HomepageOrganiserRepository.Delete(itemId);
                    if (HomepageOrganiserRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateHomepageOrganiserSortOrder(long Id, long order)
        {
            EnumResultType result = EnumResultType.NotFound;

            List<XsiHomepageOrganiser> entities = GetHomepageOrganiser(new XsiHomepageOrganiser() { ItemId = Id }).ToList();
            if (entities != null)
            {
                using (HomepageOrganiserRepository = new HomepageOrganiserRepository(ConnectionString))
                {
                    foreach (XsiHomepageOrganiser entity in entities)
                    {
                        //entity.SortOrder = order;
                        HomepageOrganiserRepository.Update(entity);
                        if (HomepageOrganiserRepository.SubmitChanges() > 0)
                            result = EnumResultType.Success;
                        else
                            result = EnumResultType.Failed;
                    }
                }
            }
            return result;
        }
        //Custom Methods
        public long GetHomeBannerInfo()
        {
            using (HomepageOrganiserRepository = new HomepageOrganiserRepository(ConnectionString))
            {
                return HomepageOrganiserRepository.Select(new XsiHomepageOrganiser() { IsActive = EnumConversion.ToString(EnumBool.Yes) }).Count();
            }
        }
        #endregion
    }
}
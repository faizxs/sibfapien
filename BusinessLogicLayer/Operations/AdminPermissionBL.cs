﻿using System;
using System.Collections.Generic;
using System.Text;
using Xsi.Repositories;
using Entities.Models;
using System.Linq;

namespace Xsi.BusinessLogicLayer
{
    public class AdminUserPermissionBL : IDisposable
    {
        #region Feilds
        IAdminPermissionRepository XsiAdminPermissionRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public AdminUserPermissionBL()
        {
            ConnectionString = null;
        }
        public AdminUserPermissionBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiAdminPermissionRepository != null)
                this.XsiAdminPermissionRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdminPermissions GetPermissionByItemId(long itemId)
        {
            using (XsiAdminPermissionRepository = new AdminPermissionsRepository())
            {
                return XsiAdminPermissionRepository.GetById(itemId);
            }
        }

        public List<XsiAdminPermissions> GetPermission(EnumSortlistBy sortListBy)
        {
            using (XsiAdminPermissionRepository = new AdminPermissionsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiAdminPermissionRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiAdminPermissionRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiAdminPermissionRepository.Select().OrderBy(p => p.UserId).ToList();
                }
            }
        }
        public List<XsiAdminPermissions> GetPermission(XsiAdminPermissions entity)
        {
            using (XsiAdminPermissionRepository = new AdminPermissionsRepository(ConnectionString))
            {
                return XsiAdminPermissionRepository.Select(entity);
            }
        }
        public List<XsiAdminPermissions> GetPermissionAndUser(XsiAdminPermissions entity)
        {
            using (XsiAdminPermissionRepository = new AdminPermissionsRepository(ConnectionString))
            {
                return XsiAdminPermissionRepository.SelectAdminPermissionAndUser(entity);
            }
        }
        public List<XsiAdminPermissions> GetPermissionAndRole(XsiAdminPermissions entity)
        {
            using (XsiAdminPermissionRepository = new AdminPermissionsRepository(ConnectionString))
            {
                return XsiAdminPermissionRepository.SelectAdminPermissionAndRole(entity);
            }
        }
        public List<XsiAdminPermissions> GetPermissionAndPage(XsiAdminPermissions entity)
        {
            using (XsiAdminPermissionRepository = new AdminPermissionsRepository(ConnectionString))
            {
                return XsiAdminPermissionRepository.SelectAdminPermissionAndPage(entity);
            }
        }

        public EnumResultType InsertPermission(XsiAdminPermissions entity)
        {
            using (XsiAdminPermissionRepository = new AdminPermissionsRepository(ConnectionString))
            {
                entity = XsiAdminPermissionRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePermission(XsiAdminPermissions entity)
        {
            if (GetPermissionByItemId(entity.ItemId) != null)
            {
                using (XsiAdminPermissionRepository = new AdminPermissionsRepository(ConnectionString))
                {
                    XsiAdminPermissionRepository.Update(entity);
                    if (XsiAdminPermissionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePermission(XsiAdminPermissions entity)
        {
            if (GetPermission(entity).ToList().Count > 0)
            {
                using (XsiAdminPermissionRepository = new AdminPermissionsRepository(ConnectionString))
                {
                    XsiAdminPermissionRepository.Delete(entity);
                    if (XsiAdminPermissionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePermission(long itemId)
        {
            if (GetPermissionByItemId(itemId) != null)
            {
                using (XsiAdminPermissionRepository = new AdminPermissionsRepository(ConnectionString))
                {
                    XsiAdminPermissionRepository.Delete(itemId);
                    if (XsiAdminPermissionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
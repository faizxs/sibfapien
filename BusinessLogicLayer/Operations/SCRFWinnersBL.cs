﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFWinnersBL : IDisposable
    {
        #region Feilds
        ISCRFWinnersRepository SCRFWinnersRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFWinnersBL()
        {
            ConnectionString = null;
        }
        public SCRFWinnersBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFWinnersRepository != null)
                this.SCRFWinnersRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (SCRFWinnersRepository = new SCRFWinnersRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiScrfwinners GetSCRFWinnersByItemId(long itemId)
        {
            using (SCRFWinnersRepository = new SCRFWinnersRepository(ConnectionString))
            {
                return SCRFWinnersRepository.GetById(itemId);
            }
        }
        
        public List<XsiScrfwinners> GetSCRFWinners()
        {
            using (SCRFWinnersRepository = new SCRFWinnersRepository(ConnectionString))
            {
                return SCRFWinnersRepository.Select();
            }
        }
        public List<XsiScrfwinners> GetSCRFWinners(EnumSortlistBy sortListBy)
        {
            using (SCRFWinnersRepository = new SCRFWinnersRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFWinnersRepository.Select().OrderBy(p => p.FirstNameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFWinnersRepository.Select().OrderByDescending(p => p.FirstNameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFWinnersRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFWinnersRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFWinnersRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFWinnersRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFWinnersRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfwinners> GetSCRFWinners(XsiScrfwinners entity)
        {
            using (SCRFWinnersRepository = new SCRFWinnersRepository(ConnectionString))
            {
                return SCRFWinnersRepository.Select(entity);
            }
        }
        public List<XsiScrfwinners> GetSCRFWinners(XsiScrfwinners entity, EnumSortlistBy sortListBy)
        {
            using (SCRFWinnersRepository = new SCRFWinnersRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFWinnersRepository.Select(entity).OrderBy(p => p.FirstNameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFWinnersRepository.Select(entity).OrderByDescending(p => p.FirstNameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFWinnersRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFWinnersRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFWinnersRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFWinnersRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFWinnersRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiScrfwinners GetSCRFWinners(long groupId, long languageId)
        {
            using (SCRFWinnersRepository = new SCRFWinnersRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertSCRFWinners(XsiScrfwinners entity)
        {
            using (SCRFWinnersRepository = new SCRFWinnersRepository(ConnectionString))
            {
                entity = SCRFWinnersRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFWinners(XsiScrfwinners entity)
        {
            XsiScrfwinners OriginalEntity = GetSCRFWinnersByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFWinnersRepository = new SCRFWinnersRepository(ConnectionString))
                {
                    SCRFWinnersRepository.Update(entity);
                    if (SCRFWinnersRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFWinnersStatus(long itemId)
        {
            XsiScrfwinners entity = GetSCRFWinnersByItemId(itemId);
            if (entity != null)
            {
                using (SCRFWinnersRepository = new SCRFWinnersRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFWinnersRepository.Update(entity);
                    if (SCRFWinnersRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFWinners(XsiScrfwinners entity)
        {
            List<XsiScrfwinners> entities = GetSCRFWinners(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfwinners e in entities)
                {
                    result = DeleteSCRFWinners(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFWinners(long itemId)
        {
            XsiScrfwinners entity = GetSCRFWinnersByItemId(itemId);
            if (entity != null)
            {
                using (SCRFWinnersRepository = new SCRFWinnersRepository(ConnectionString))
                {
                    //delete news with news catefory id = itemid

                    SCRFWinnersRepository.Delete(itemId);
                    if (SCRFWinnersRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
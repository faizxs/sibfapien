﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionStaffGuestFlightDetailsBL : IDisposable
    {
        #region Fields
        IExhibitionStaffGuestFlightDetailsRepository IExhibitionStaffGuestFlightDetailsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestFlightDetailsBL()
        {
            ConnectionString = null;
        }
        public ExhibitionStaffGuestFlightDetailsBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionStaffGuestFlightDetailsRepository != null)
                this.IExhibitionStaffGuestFlightDetailsRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionStaffGuestFlightDetailsRepository = new ExhibitionStaffGuestFlightDetailRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionStaffGuestFlightDetails GetExhibitionStaffGuestFlightDetailsByItemId(long itemId)
        {
            using (IExhibitionStaffGuestFlightDetailsRepository = new ExhibitionStaffGuestFlightDetailRepository(ConnectionString))
            {
                return IExhibitionStaffGuestFlightDetailsRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionStaffGuestFlightDetails> GetExhibitionStaffGuestFlightDetails()
        {
            using (IExhibitionStaffGuestFlightDetailsRepository = new ExhibitionStaffGuestFlightDetailRepository(ConnectionString))
            {
                return IExhibitionStaffGuestFlightDetailsRepository.Select();
            }
        }
        public List<XsiExhibitionStaffGuestFlightDetails> GetExhibitionStaffGuestFlightDetails(EnumSortlistBy sortListBy)
        {
            using (IExhibitionStaffGuestFlightDetailsRepository = new ExhibitionStaffGuestFlightDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionStaffGuestFlightDetailsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionStaffGuestFlightDetailsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionStaffGuestFlightDetailsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionStaffGuestFlightDetailsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionStaffGuestFlightDetailsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionStaffGuestFlightDetails> GetExhibitionStaffGuestFlightDetails(XsiExhibitionStaffGuestFlightDetails entity)
        {
            using (IExhibitionStaffGuestFlightDetailsRepository = new ExhibitionStaffGuestFlightDetailRepository(ConnectionString))
            {
                return IExhibitionStaffGuestFlightDetailsRepository.Select(entity);
            }
        }
        public List<XsiExhibitionStaffGuestFlightDetails> GetExhibitionStaffGuestFlightDetails(XsiExhibitionStaffGuestFlightDetails entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionStaffGuestFlightDetailsRepository = new ExhibitionStaffGuestFlightDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionStaffGuestFlightDetailsRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionStaffGuestFlightDetailsRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionStaffGuestFlightDetailsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionStaffGuestFlightDetailsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionStaffGuestFlightDetailsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionStaffGuestFlightDetails GetExhibitionStaffGuestFlightDetails(long groupId, long languageId)
        {
            using (IExhibitionStaffGuestFlightDetailsRepository = new ExhibitionStaffGuestFlightDetailRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionStaffGuestFlightDetails(XsiExhibitionStaffGuestFlightDetails entity)
        {
            using (IExhibitionStaffGuestFlightDetailsRepository = new ExhibitionStaffGuestFlightDetailRepository(ConnectionString))
            {
                entity = IExhibitionStaffGuestFlightDetailsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionStaffGuestFlightDetails(XsiExhibitionStaffGuestFlightDetails entity)
        {
            var where = new XsiExhibitionStaffGuestFlightDetails();
            where.ItemId = entity.ItemId;
            XsiExhibitionStaffGuestFlightDetails OriginalEntity = GetExhibitionStaffGuestFlightDetails(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

            if (OriginalEntity != null)
            {
                using (IExhibitionStaffGuestFlightDetailsRepository = new ExhibitionStaffGuestFlightDetailRepository(ConnectionString))
                {

                    IExhibitionStaffGuestFlightDetailsRepository.Update(entity);
                    if (IExhibitionStaffGuestFlightDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackExhibitionStaffGuestFlightDetails(long itemId)
        {
            //if no RollbackXml propery in ExhibitionStaffGuestFlightDetails then delete below code and return EnumResultType.Invalid;

            return EnumResultType.Failed;

        }
        public EnumResultType UpdateExhibitionStaffGuestFlightDetailsStatus(long itemId)
        {
            XsiExhibitionStaffGuestFlightDetails entity = GetExhibitionStaffGuestFlightDetailsByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionStaffGuestFlightDetailsRepository = new ExhibitionStaffGuestFlightDetailRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionStaffGuestFlightDetailsRepository.Update(entity);
                    if (IExhibitionStaffGuestFlightDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionStaffGuestFlightDetails(XsiExhibitionStaffGuestFlightDetails entity)
        {
            List<XsiExhibitionStaffGuestFlightDetails> entities = GetExhibitionStaffGuestFlightDetails(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionStaffGuestFlightDetails e in entities)
                {
                    result = DeleteExhibitionStaffGuestFlightDetails(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionStaffGuestFlightDetails(long itemId)
        {
            var where = new XsiExhibitionStaffGuestFlightDetails();
            where.ItemId = itemId;
            XsiExhibitionStaffGuestFlightDetails entity = GetExhibitionStaffGuestFlightDetails(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            if (entity != null)
            {
                using (IExhibitionStaffGuestFlightDetailsRepository = new ExhibitionStaffGuestFlightDetailRepository(ConnectionString))
                {
                    IExhibitionStaffGuestFlightDetailsRepository.Delete(itemId);
                    if (IExhibitionStaffGuestFlightDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class GenreBL : IDisposable
    {
        #region Feilds
        IGenreRepository GenreRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public GenreBL()
        {
            ConnectionString = null;
        }
        public GenreBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.GenreRepository != null)
                this.GenreRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiGenre GetGenreByItemId(long itemId)
        {
            using (GenreRepository = new GenreRepository(ConnectionString))
            {
                return GenreRepository.GetById(itemId);
            }
        }
          
        public List<XsiGenre> GetGenre()
        {
            using (GenreRepository = new GenreRepository(ConnectionString))
            {
                return GenreRepository.Select();
            }
        }
        public List<XsiGenre> GetGenre(EnumSortlistBy sortListBy)
        {
            using (GenreRepository = new GenreRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return GenreRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return GenreRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return GenreRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return GenreRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return GenreRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return GenreRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return GenreRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiGenre> GetGenre(XsiGenre entity)
        {
            using (GenreRepository = new GenreRepository(ConnectionString))
            {
                return GenreRepository.Select(entity);
            }
        }
        public List<XsiGenre> GetGenre(XsiGenre entity, EnumSortlistBy sortListBy)
        {
            using (GenreRepository = new GenreRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return GenreRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return GenreRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return GenreRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return GenreRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return GenreRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return GenreRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return GenreRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiGenre> GetGenreOr(XsiGenre entity, EnumSortlistBy sortListBy)
        {
            using (GenreRepository = new GenreRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return GenreRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return GenreRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return GenreRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return GenreRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return GenreRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return GenreRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return GenreRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
          
        public EnumResultType InsertGenre(XsiGenre entity)
        {
            using (GenreRepository = new GenreRepository(ConnectionString))
            {
                entity = GenreRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateGenre(XsiGenre entity)
        {
            XsiGenre OriginalEntity = GetGenreByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (GenreRepository = new GenreRepository(ConnectionString))
                {
                    GenreRepository.Update(entity);
                    if (GenreRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateGenreStatus(long itemId)
        {
            XsiGenre entity = GetGenreByItemId(itemId);
            if (entity != null)
            {
                using (GenreRepository = new GenreRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    GenreRepository.Update(entity);
                    if (GenreRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteGenre(XsiGenre entity)
        {
            List<XsiGenre> entities = GetGenre(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiGenre e in entities)
                {
                    result = DeleteGenre(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteGenre(long itemId)
        {
            XsiGenre entity = GetGenreByItemId(itemId);
            if (entity != null)
            {
                using (GenreRepository = new GenreRepository(ConnectionString))
                {
                    GenreRepository.Delete(itemId);
                    if (GenreRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
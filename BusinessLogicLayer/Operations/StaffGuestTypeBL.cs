﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class StaffGuestTypeBL : IDisposable
    {
        #region Feilds
        IStaffGuestTypeRepository StaffGuestTypeRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public StaffGuestTypeBL()
        {
            ConnectionString = null;
        }
        public StaffGuestTypeBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.StaffGuestTypeRepository != null)
                this.StaffGuestTypeRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiStaffGuestType GetStaffGuestTypeByItemId(long itemId)
        {
            using (StaffGuestTypeRepository = new StaffGuestTypeRepository(ConnectionString))
            {
                return StaffGuestTypeRepository.GetById(itemId);
            }
        }
        public List<XsiStaffGuestType> GetStaffGuestType()
        {
            using (StaffGuestTypeRepository = new StaffGuestTypeRepository(ConnectionString))
            {
                return StaffGuestTypeRepository.Select();
            }
        }
        public List<XsiStaffGuestType> GetStaffGuestType(EnumSortlistBy sortListBy)
        {
            using (StaffGuestTypeRepository = new StaffGuestTypeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return StaffGuestTypeRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return StaffGuestTypeRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return StaffGuestTypeRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return StaffGuestTypeRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return StaffGuestTypeRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return StaffGuestTypeRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return StaffGuestTypeRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiStaffGuestType> GetStaffGuestType(XsiStaffGuestType entity)
        {
            using (StaffGuestTypeRepository = new StaffGuestTypeRepository(ConnectionString))
            {
                return StaffGuestTypeRepository.Select(entity);
            }
        }
        public List<XsiStaffGuestType> GetStaffGuestType(XsiStaffGuestType entity, EnumSortlistBy sortListBy)
        {
            using (StaffGuestTypeRepository = new StaffGuestTypeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return StaffGuestTypeRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return StaffGuestTypeRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return StaffGuestTypeRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return StaffGuestTypeRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return StaffGuestTypeRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return StaffGuestTypeRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return StaffGuestTypeRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiStaffGuestType> GetStaffGuestTypeOr(XsiStaffGuestType entity, EnumSortlistBy sortListBy)
        {
            using (StaffGuestTypeRepository = new StaffGuestTypeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return StaffGuestTypeRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return StaffGuestTypeRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return StaffGuestTypeRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return StaffGuestTypeRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return StaffGuestTypeRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return StaffGuestTypeRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return StaffGuestTypeRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertStaffGuestType(XsiStaffGuestType entity)
        {
            using (StaffGuestTypeRepository = new StaffGuestTypeRepository(ConnectionString))
            {
                entity = StaffGuestTypeRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateStaffGuestType(XsiStaffGuestType entity)
        {
            XsiStaffGuestType OriginalEntity = GetStaffGuestTypeByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (StaffGuestTypeRepository = new StaffGuestTypeRepository(ConnectionString))
                {
                    StaffGuestTypeRepository.Update(entity);
                    if (StaffGuestTypeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
   
        public EnumResultType UpdateStaffGuestTypeStatus(long itemId, long langId)
        {
            XsiStaffGuestType entity = GetStaffGuestTypeByItemId(itemId);
            if (entity != null)
            {
                using (StaffGuestTypeRepository = new StaffGuestTypeRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    StaffGuestTypeRepository.Update(entity);
                    if (StaffGuestTypeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteStaffGuestType(XsiStaffGuestType entity)
        {
            List<XsiStaffGuestType> entities = GetStaffGuestType(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiStaffGuestType e in entities)
                {
                    result = DeleteStaffGuestType(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteStaffGuestType(long itemId)
        {
            XsiStaffGuestType entity = GetStaffGuestTypeByItemId(itemId);
            if (entity != null)
            {
                using (StaffGuestTypeRepository = new StaffGuestTypeRepository(ConnectionString))
                {
                    StaffGuestTypeRepository.Delete(itemId);
                    if (StaffGuestTypeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
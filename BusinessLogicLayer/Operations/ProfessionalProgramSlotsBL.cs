﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ProfessionalProgramSlotsBL : IDisposable
    {
        #region Feilds
        IProfessionalProgramSlotsRepository ProfessionalProgramSlotsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ProfessionalProgramSlotsBL()
        {
            ConnectionString = null;
        }
        public ProfessionalProgramSlotsBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ProfessionalProgramSlotsRepository != null)
                this.ProfessionalProgramSlotsRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionProfessionalProgramSlots GetProfessionalProgramSlotsByItemId(long itemId)
        {
            using (ProfessionalProgramSlotsRepository = new ProfessionalProgramSlotsRepository(ConnectionString))
            {
                return ProfessionalProgramSlotsRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionProfessionalProgramSlots> GetProfessionalProgramSlots()
        {
            using (ProfessionalProgramSlotsRepository = new ProfessionalProgramSlotsRepository(ConnectionString))
            {
                return ProfessionalProgramSlotsRepository.Select();
            }
        }
        public List<XsiExhibitionProfessionalProgramSlots> GetProfessionalProgramSlots(EnumSortlistBy sortListBy)
        {
            using (ProfessionalProgramSlotsRepository = new ProfessionalProgramSlotsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return ProfessionalProgramSlotsRepository.Select().OrderBy(p => p.StartTime).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ProfessionalProgramSlotsRepository.Select().OrderByDescending(p => p.StartTime).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ProfessionalProgramSlotsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ProfessionalProgramSlotsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ProfessionalProgramSlotsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionProfessionalProgramSlots> GetProfessionalProgramSlots(XsiExhibitionProfessionalProgramSlots entity)
        {
            using (ProfessionalProgramSlotsRepository = new ProfessionalProgramSlotsRepository(ConnectionString))
            {
                return ProfessionalProgramSlotsRepository.Select(entity);
            }
        }
        public List<XsiExhibitionProfessionalProgramSlots> GetProfessionalProgramSlots(XsiExhibitionProfessionalProgramSlots entity, EnumSortlistBy sortListBy)
        {
            using (ProfessionalProgramSlotsRepository = new ProfessionalProgramSlotsRepository(ConnectionString))
            {
                switch (sortListBy)
                {

                    case EnumSortlistBy.ByDateAsc:
                        return ProfessionalProgramSlotsRepository.Select(entity).OrderBy(p => p.StartTime).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ProfessionalProgramSlotsRepository.Select(entity).OrderByDescending(p => p.StartTime).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ProfessionalProgramSlotsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ProfessionalProgramSlotsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ProfessionalProgramSlotsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertProfessionalProgramSlots(XsiExhibitionProfessionalProgramSlots entity)
        {
            using (ProfessionalProgramSlotsRepository = new ProfessionalProgramSlotsRepository(ConnectionString))
            {
                entity = ProfessionalProgramSlotsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateProfessionalProgramSlots(XsiExhibitionProfessionalProgramSlots entity)
        {
            XsiExhibitionProfessionalProgramSlots OriginalEntity = GetProfessionalProgramSlotsByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ProfessionalProgramSlotsRepository = new ProfessionalProgramSlotsRepository(ConnectionString))
                {
                    ProfessionalProgramSlotsRepository.Update(entity);
                    if (ProfessionalProgramSlotsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType DeleteProfessionalProgramSlots(XsiExhibitionProfessionalProgramSlots entity)
        {
            List<XsiExhibitionProfessionalProgramSlots> entities = GetProfessionalProgramSlots(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionProfessionalProgramSlots e in entities)
                {
                    result = DeleteProfessionalProgramSlots(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteProfessionalProgramSlots(long itemId)
        {
            XsiExhibitionProfessionalProgramSlots entity = GetProfessionalProgramSlotsByItemId(itemId);
            if (entity != null)
            {
                using (ProfessionalProgramSlotsRepository = new ProfessionalProgramSlotsRepository(ConnectionString))
                {
                    ProfessionalProgramSlotsRepository.Delete(itemId);
                    if (ProfessionalProgramSlotsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
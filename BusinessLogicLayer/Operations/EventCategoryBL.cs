﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class EventCategoryBL : IDisposable
    {
        #region Feilds
        IEventCategoryRepository EventCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public EventCategoryBL()
        {
            ConnectionString = null;
        }
        public EventCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventCategoryRepository != null)
                this.EventCategoryRepository.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiEventCategory GetEventCategoryByItemId(long itemId)
        {
            using (EventCategoryRepository = new EventCategoryRepository(ConnectionString))
            {
                return EventCategoryRepository.GetById(itemId);
            }
        }
         
        public List<XsiEventCategory> GetEventCategory()
        {
            using (EventCategoryRepository = new EventCategoryRepository(ConnectionString))
            {
                return EventCategoryRepository.Select();
            }
        }
        public List<XsiEventCategory> GetEventCategory(EnumSortlistBy sortListBy)
        {
            using (EventCategoryRepository = new EventCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return EventCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return EventCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return EventCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEventCategory> GetEventCategory(XsiEventCategory entity)
        {
            using (EventCategoryRepository = new EventCategoryRepository(ConnectionString))
            {
                return EventCategoryRepository.Select(entity);
            }
        }
        public List<XsiEventCategory> GetEventCategory(XsiEventCategory entity, EnumSortlistBy sortListBy)
        {
            using (EventCategoryRepository = new EventCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return EventCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return EventCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return EventCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEventCategory> GetEventCategoryOr(XsiEventCategory entity, EnumSortlistBy sortListBy)
        {
            using (EventCategoryRepository = new EventCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return EventCategoryRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return EventCategoryRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return EventCategoryRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventCategoryRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventCategoryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
       
        public List<XsiEventCategory> GetOtherLanguageEventCategory(XsiEventCategory entity)
        {
            using (EventCategoryRepository = new EventCategoryRepository(ConnectionString))
            {
                return EventCategoryRepository.SelectOtherLanguageCategory(entity);
            }
        }
        public List<XsiEventCategory> GetCurrentLanguageEventCategory(XsiEventCategory entity)
        {
            using (EventCategoryRepository = new EventCategoryRepository(ConnectionString))
            {
                return EventCategoryRepository.SelectCurrentLanguageCategory(entity);
            }
        }

        public EnumResultType InsertEventCategory(XsiEventCategory entity)
        {
            using (EventCategoryRepository = new EventCategoryRepository(ConnectionString))
            {
                entity = EventCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateEventCategory(XsiEventCategory entity)
        {
            XsiEventCategory OriginalEntity = GetEventCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (EventCategoryRepository = new EventCategoryRepository(ConnectionString))
                {
                    EventCategoryRepository.Update(entity);
                    if (EventCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateEventCategoryStatus(long itemId)
        {
            XsiEventCategory entity = GetEventCategoryByItemId(itemId);
            if (entity != null)
            {
                using (EventCategoryRepository = new EventCategoryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    EventCategoryRepository.Update(entity);
                    if (EventCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteEventCategory(XsiEventCategory entity)
        {
            List<XsiEventCategory> entities = GetEventCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiEventCategory e in entities)
                {
                    result = DeleteEventCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteEventCategory(long itemId)
        {
            XsiEventCategory entity = GetEventCategoryByItemId(itemId);
            if (entity != null)
            {
                using (EventCategoryRepository = new EventCategoryRepository(ConnectionString))
                {
                    EventCategoryRepository.Delete(itemId);
                    if (EventCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class VIPGuestBL : IDisposable
    {
        #region Feilds
        IVIPGuestRepository IVIPGuestRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public VIPGuestBL()
        {
            ConnectionString = null;
        }
        public VIPGuestBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IVIPGuestRepository != null)
                this.IVIPGuestRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiVipguest GetVIPGuestByItemId(long itemId)
        {
            using (IVIPGuestRepository = new VIPGuestRepository(ConnectionString))
            {
                return IVIPGuestRepository.GetById(itemId);
            }
        }

        public List<XsiVipguest> GetVIPGuest()
        {
            using (IVIPGuestRepository = new VIPGuestRepository(ConnectionString))
            {
                return IVIPGuestRepository.Select();
            }
        }
        public List<XsiVipguest> GetVIPGuest(EnumSortlistBy sortListBy)
        {
            using (IVIPGuestRepository = new VIPGuestRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IVIPGuestRepository.Select().OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IVIPGuestRepository.Select().OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IVIPGuestRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IVIPGuestRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IVIPGuestRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IVIPGuestRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IVIPGuestRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiVipguest> GetVIPGuest(XsiVipguest entity)
        {
            using (IVIPGuestRepository = new VIPGuestRepository(ConnectionString))
            {
                return IVIPGuestRepository.Select(entity);
            }

        }
        public List<XsiVipguest> GetVIPGuest(XsiVipguest entity, EnumSortlistBy sortListBy)
        {
            using (IVIPGuestRepository = new VIPGuestRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IVIPGuestRepository.Select(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IVIPGuestRepository.Select(entity).OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IVIPGuestRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IVIPGuestRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IVIPGuestRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IVIPGuestRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IVIPGuestRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiVipguest> GetVIPGuestOr(XsiVipguest entity, EnumSortlistBy sortListBy)
        {
            using (IVIPGuestRepository = new VIPGuestRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IVIPGuestRepository.SelectOr(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IVIPGuestRepository.SelectOr(entity).OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IVIPGuestRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IVIPGuestRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IVIPGuestRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IVIPGuestRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IVIPGuestRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertVIPGuest(XsiVipguest entity)
        {
            using (IVIPGuestRepository = new VIPGuestRepository(ConnectionString))
            {
                entity = IVIPGuestRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;

                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateVIPGuest(XsiVipguest entity)
        {
            XsiVipguest OriginalEntity = GetVIPGuestByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IVIPGuestRepository = new VIPGuestRepository(ConnectionString))
                {
                    IVIPGuestRepository.Update(entity);
                    if (IVIPGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateVIPGuestStatus(long itemId, long langId)
        {
            XsiVipguest entity = GetVIPGuestByItemId(itemId);
            if (entity != null)
            {
                using (IVIPGuestRepository = new VIPGuestRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IVIPGuestRepository.Update(entity);
                    if (IVIPGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteVIPGuest(XsiVipguest entity)
        {
            List<XsiVipguest> entities = GetVIPGuest(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiVipguest e in entities)
                {
                    result = DeleteVIPGuest(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteVIPGuest(long itemId)
        {
            XsiVipguest entity = GetVIPGuestByItemId(itemId);
            if (entity != null)
            {
                using (IVIPGuestRepository = new VIPGuestRepository(ConnectionString))
                {
                    IVIPGuestRepository.Delete(itemId);
                    if (IVIPGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
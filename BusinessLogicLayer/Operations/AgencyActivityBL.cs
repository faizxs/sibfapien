﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class AgencyActivityBL : IDisposable
    {
        #region Feilds
        IAgencyActivityRepository AgencyActivityRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public AgencyActivityBL()
        {
            ConnectionString = null;
        }
        public AgencyActivityBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AgencyActivityRepository != null)
                this.AgencyActivityRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiMemberExhibitionActivityYearly GetAgencyActivityByItemId(long itemId)
        {
            using (AgencyActivityRepository = new AgencyActivityRepository(ConnectionString))
            {
                return AgencyActivityRepository.GetById(itemId);
            }
        }

        public List<XsiMemberExhibitionActivityYearly> GetAgencyActivity()
        {
            using (AgencyActivityRepository = new AgencyActivityRepository(ConnectionString))
            {
                return AgencyActivityRepository.Select();
            }
        }
        public List<XsiMemberExhibitionActivityYearly> GetAgencyActivity(EnumSortlistBy sortListBy)
        {
            using (AgencyActivityRepository = new AgencyActivityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return AgencyActivityRepository.Select().OrderBy(p => p.MemberExhibitionYearlyId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AgencyActivityRepository.Select().OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();

                    default:
                        return AgencyActivityRepository.Select().OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();
                }
            }
        }
        public List<XsiMemberExhibitionActivityYearly> GetAgencyActivity(XsiMemberExhibitionActivityYearly entity)
        {
            using (AgencyActivityRepository = new AgencyActivityRepository(ConnectionString))
            {
                return AgencyActivityRepository.Select(entity);
            }
        }
        public List<XsiMemberExhibitionActivityYearly> GetAgencyActivity(XsiMemberExhibitionActivityYearly entity, EnumSortlistBy sortListBy)
        {
            using (AgencyActivityRepository = new AgencyActivityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return AgencyActivityRepository.Select(entity).OrderBy(p => p.MemberExhibitionYearlyId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AgencyActivityRepository.Select(entity).OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();

                    default:
                        return AgencyActivityRepository.Select(entity).OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();
                }
            }
        }

        public EnumResultType InsertAgencyActivity(XsiMemberExhibitionActivityYearly entity)
        {
            using (AgencyActivityRepository = new AgencyActivityRepository(ConnectionString))
            {
                entity = AgencyActivityRepository.Add(entity);
                if (entity.MemberExhibitionYearlyId > 0)
                {
                    XsiItemdId = entity.MemberExhibitionYearlyId;
                    XsiGroupId =-1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateAgencyActivity(XsiMemberExhibitionActivityYearly entity)
        {
            XsiMemberExhibitionActivityYearly OriginalEntity = GetAgencyActivityByItemId(entity.MemberExhibitionYearlyId);

            if (OriginalEntity != null)
            {
                using (AgencyActivityRepository = new AgencyActivityRepository(ConnectionString))
                {
                    AgencyActivityRepository.Update(entity);
                    if (AgencyActivityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteAgencyActivity(XsiMemberExhibitionActivityYearly entity)
        {
            List<XsiMemberExhibitionActivityYearly> entities = GetAgencyActivity(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiMemberExhibitionActivityYearly e in entities)
                {
                    result = DeleteAgencyActivity(e.MemberExhibitionYearlyId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteAgencyActivity(long itemId)
        {
            XsiMemberExhibitionActivityYearly entity = GetAgencyActivityByItemId(itemId);
            if (entity != null)
            {
                using (AgencyActivityRepository = new AgencyActivityRepository(ConnectionString))
                {
                    AgencyActivityRepository.Delete(itemId);
                    if (AgencyActivityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
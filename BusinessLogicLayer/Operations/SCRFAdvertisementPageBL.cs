﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFAdvertisementPageBL : IDisposable
    {
        #region Feilds
        ISCRFAdvertisementPageRepository SCRFAdvertisementPageRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFAdvertisementPageBL()
        {
            ConnectionString = null;
        }
        public SCRFAdvertisementPageBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFAdvertisementPageRepository != null)
                this.SCRFAdvertisementPageRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (SCRFAdvertisementPageRepository = new SCRFAdvertisementPageRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiScrfadvertisementPage GetSCRFAdvertisementPageByItemId(long itemId)
        {
            using (SCRFAdvertisementPageRepository = new SCRFAdvertisementPageRepository(ConnectionString))
            {
                return SCRFAdvertisementPageRepository.GetById(itemId);
            }
        }

        public List<XsiScrfadvertisementPage> GetSCRFAdvertisementPage()
        {
            using (SCRFAdvertisementPageRepository = new SCRFAdvertisementPageRepository(ConnectionString))
            {
                return SCRFAdvertisementPageRepository.Select();
            }
        }
        public List<XsiScrfadvertisementPage> GetSCRFAdvertisementPage(EnumSortlistBy sortListBy)
        {
            using (SCRFAdvertisementPageRepository = new SCRFAdvertisementPageRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFAdvertisementPageRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFAdvertisementPageRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFAdvertisementPageRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfadvertisementPage> GetSCRFAdvertisementPage(XsiScrfadvertisementPage entity)
        {
            using (SCRFAdvertisementPageRepository = new SCRFAdvertisementPageRepository(ConnectionString))
            {
                return SCRFAdvertisementPageRepository.Select(entity);
            }
        }
        public List<XsiScrfadvertisementPage> GetSCRFAdvertisementPage(XsiScrfadvertisementPage entity, EnumSortlistBy sortListBy)
        {
            using (SCRFAdvertisementPageRepository = new SCRFAdvertisementPageRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFAdvertisementPageRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFAdvertisementPageRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFAdvertisementPageRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiScrfadvertisementPage GetSCRFAdvertisementPage(long groupId, long languageId)
        {
            using (SCRFAdvertisementPageRepository = new SCRFAdvertisementPageRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertSCRFAdvertisementPage(XsiScrfadvertisementPage entity)
        {
            using (SCRFAdvertisementPageRepository = new SCRFAdvertisementPageRepository(ConnectionString))
            {
                entity = SCRFAdvertisementPageRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFAdvertisementPage(XsiScrfadvertisementPage entity)
        {
            XsiScrfadvertisementPage OriginalEntity = GetSCRFAdvertisementPageByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFAdvertisementPageRepository = new SCRFAdvertisementPageRepository(ConnectionString))
                {
                    SCRFAdvertisementPageRepository.Update(entity);
                    if (SCRFAdvertisementPageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackSCRFAdvertisementPage(long itemId)
        {
            //if no RollbackXml propery in SCRFAdvertisementPage then delete below code and return EnumResultType.Invalid;
            return EnumResultType.Invalid;
        }
        public EnumResultType UpdateSCRFAdvertisementPageStatus(long itemId)
        {
            XsiScrfadvertisementPage entity = GetSCRFAdvertisementPageByItemId(itemId);
            if (entity != null)
            {
                using (SCRFAdvertisementPageRepository = new SCRFAdvertisementPageRepository(ConnectionString))
                {
                    SCRFAdvertisementPageRepository.Update(entity);
                    if (SCRFAdvertisementPageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFAdvertisementPage(XsiScrfadvertisementPage entity)
        {
            List<XsiScrfadvertisementPage> entities = GetSCRFAdvertisementPage(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfadvertisementPage e in entities)
                {
                    result = DeleteSCRFAdvertisementPage(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFAdvertisementPage(long itemId)
        {
            XsiScrfadvertisementPage entity = GetSCRFAdvertisementPageByItemId(itemId);
            if (entity != null)
            {
                using (SCRFAdvertisementPageRepository = new SCRFAdvertisementPageRepository(ConnectionString))
                {
                    SCRFAdvertisementPageRepository.Delete(itemId);
                    if (SCRFAdvertisementPageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
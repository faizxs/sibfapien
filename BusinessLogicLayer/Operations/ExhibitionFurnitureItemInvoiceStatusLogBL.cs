﻿using System;
using System.Collections.Generic;
using System.Text;
using Xsi.Repositories;
using Entities.Models;
using System.Linq;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionFurnitureItemInvoiceStatusLogBL : IDisposable
    {
        #region Feilds
        IExhibitionFurnitureItemInvoiceStatusLogRepository IExhibitionFurnitureItemInvoiceStatusLogRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionFurnitureItemInvoiceStatusLogBL()
        {
            ConnectionString = null;
        }
        public ExhibitionFurnitureItemInvoiceStatusLogBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionFurnitureItemInvoiceStatusLogRepository != null)
                this.IExhibitionFurnitureItemInvoiceStatusLogRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionFurnitureItemInvoiceStatusLogRepository = new ExhibitionFurnitureItemInvoiceStatusLogRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionFurnitureItemStatusLogs GetExhibitionFurnitureItemInvoiceStatusLogByItemId(long itemId)
        {
            using (IExhibitionFurnitureItemInvoiceStatusLogRepository = new ExhibitionFurnitureItemInvoiceStatusLogRepository(ConnectionString))
            {
                return IExhibitionFurnitureItemInvoiceStatusLogRepository.GetById(itemId);
            }
        }
        
        public List<XsiExhibitionFurnitureItemStatusLogs> GetExhibitionFurnitureItemInvoiceStatusLog()
        {
            using (IExhibitionFurnitureItemInvoiceStatusLogRepository = new ExhibitionFurnitureItemInvoiceStatusLogRepository(ConnectionString))
            {
                return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select();
            }
        }
        public List<XsiExhibitionFurnitureItemStatusLogs> GetExhibitionFurnitureItemInvoiceStatusLog(EnumSortlistBy sortListBy)
        {
            using (IExhibitionFurnitureItemInvoiceStatusLogRepository = new ExhibitionFurnitureItemInvoiceStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select().OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionFurnitureItemStatusLogs> GetExhibitionFurnitureItemInvoiceStatusLog(XsiExhibitionFurnitureItemStatusLogs entity)
        {
            using (IExhibitionFurnitureItemInvoiceStatusLogRepository = new ExhibitionFurnitureItemInvoiceStatusLogRepository(ConnectionString))
            {
                return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select(entity);
            }
        }
        public List<XsiExhibitionFurnitureItemStatusLogs> GetExhibitionFurnitureItemInvoiceStatusLog(XsiExhibitionFurnitureItemStatusLogs entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionFurnitureItemInvoiceStatusLogRepository = new ExhibitionFurnitureItemInvoiceStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionFurnitureItemInvoiceStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionFurnitureItemStatusLogs GetExhibitionFurnitureItemInvoiceStatusLog(long groupId, long languageId)
        {
            using (IExhibitionFurnitureItemInvoiceStatusLogRepository = new ExhibitionFurnitureItemInvoiceStatusLogRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionFurnitureItemInvoiceStatusLog(XsiExhibitionFurnitureItemStatusLogs entity)
        {
            using (IExhibitionFurnitureItemInvoiceStatusLogRepository = new ExhibitionFurnitureItemInvoiceStatusLogRepository(ConnectionString))
            {
                entity = IExhibitionFurnitureItemInvoiceStatusLogRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId =  -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionFurnitureItemInvoiceStatusLog(XsiExhibitionFurnitureItemStatusLogs entity)
        {
            XsiExhibitionFurnitureItemStatusLogs OriginalEntity = GetExhibitionFurnitureItemInvoiceStatusLogByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IExhibitionFurnitureItemInvoiceStatusLogRepository = new ExhibitionFurnitureItemInvoiceStatusLogRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line
                    
                    IExhibitionFurnitureItemInvoiceStatusLogRepository.Update(entity);
                    if (IExhibitionFurnitureItemInvoiceStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionFurnitureItemInvoiceStatusLogStatus(long itemId)
        {
            XsiExhibitionFurnitureItemStatusLogs entity = GetExhibitionFurnitureItemInvoiceStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionFurnitureItemInvoiceStatusLogRepository = new ExhibitionFurnitureItemInvoiceStatusLogRepository(ConnectionString))
                {
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionFurnitureItemInvoiceStatusLogRepository.Update(entity);
                    if (IExhibitionFurnitureItemInvoiceStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionFurnitureItemInvoiceStatusLog(XsiExhibitionFurnitureItemStatusLogs entity)
        {
            List<XsiExhibitionFurnitureItemStatusLogs> entities = GetExhibitionFurnitureItemInvoiceStatusLog(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionFurnitureItemStatusLogs e in entities)
                {
                    result = DeleteExhibitionFurnitureItemInvoiceStatusLog(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionFurnitureItemInvoiceStatusLog(long itemId)
        {
            XsiExhibitionFurnitureItemStatusLogs entity = GetExhibitionFurnitureItemInvoiceStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionFurnitureItemInvoiceStatusLogRepository = new ExhibitionFurnitureItemInvoiceStatusLogRepository(ConnectionString))
                {
                    IExhibitionFurnitureItemInvoiceStatusLogRepository.Delete(itemId);
                    if (IExhibitionFurnitureItemInvoiceStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ContactusEnquiryBL : IDisposable
    {
        #region Feilds
        IContactusEnquiryRepository ContactusEnquiryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public ContactusEnquiryBL()
        {
            ConnectionString = null;
        }
        public ContactusEnquiryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ContactusEnquiryRepository != null)
                this.ContactusEnquiryRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiContactusEnquiry GetContactusEnquiryByItemId(long itemId)
        {
            using (ContactusEnquiryRepository = new ContactusEnquiryRepository(ConnectionString))
            {
                return ContactusEnquiryRepository.GetById(itemId);
            }
        }

        public List<XsiContactusEnquiry> GetContactusEnquiry()
        {
            using (ContactusEnquiryRepository = new ContactusEnquiryRepository(ConnectionString))
            {
                return ContactusEnquiryRepository.Select();
            }
        }
        public List<XsiContactusEnquiry> GetContactusEnquiry(EnumSortlistBy sortListBy)
        {
            using (ContactusEnquiryRepository = new ContactusEnquiryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ContactusEnquiryRepository.Select().OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ContactusEnquiryRepository.Select().OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ContactusEnquiryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ContactusEnquiryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ContactusEnquiryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiContactusEnquiry> GetContactusEnquiry(XsiContactusEnquiry entity)
        {
            using (ContactusEnquiryRepository = new ContactusEnquiryRepository(ConnectionString))
            {
                return ContactusEnquiryRepository.Select(entity);
            }
        }
        public List<XsiContactusEnquiry> GetContactusEnquiry(XsiContactusEnquiry entity, EnumSortlistBy sortListBy)
        {
            using (ContactusEnquiryRepository = new ContactusEnquiryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ContactusEnquiryRepository.Select(entity).OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ContactusEnquiryRepository.Select(entity).OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ContactusEnquiryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ContactusEnquiryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ContactusEnquiryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiContactusEnquiry> GetContactusEnquiryOr(XsiContactusEnquiry entity)
        {
            using (ContactusEnquiryRepository = new ContactusEnquiryRepository(ConnectionString))
            {
                return ContactusEnquiryRepository.SelectOr(entity);
            }
        }
        public List<XsiContactusEnquiry> GetContactusEnquiryOr(XsiContactusEnquiry entity, EnumSortlistBy sortListBy)
        {
            using (ContactusEnquiryRepository = new ContactusEnquiryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ContactusEnquiryRepository.SelectOr(entity).OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ContactusEnquiryRepository.SelectOr(entity).OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ContactusEnquiryRepository.SelectOr(entity).OrderBy(p => p.DateAdded).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ContactusEnquiryRepository.SelectOr(entity).OrderByDescending(p => p.DateAdded).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ContactusEnquiryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ContactusEnquiryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ContactusEnquiryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertContactusEnquiry(XsiContactusEnquiry entity)
        {
            using (ContactusEnquiryRepository = new ContactusEnquiryRepository(ConnectionString))
            {
                entity = ContactusEnquiryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateContactusEnquiry(XsiContactusEnquiry entity)
        {
            XsiContactusEnquiry OriginalEntity = GetContactusEnquiryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ContactusEnquiryRepository = new ContactusEnquiryRepository(ConnectionString))
                {
                    ContactusEnquiryRepository.Update(entity);
                    if (ContactusEnquiryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateContactusEnquiryStatus(long itemId)
        {
            XsiContactusEnquiry entity = GetContactusEnquiryByItemId(itemId);
            if (entity != null)
            {
                using (ContactusEnquiryRepository = new ContactusEnquiryRepository(ConnectionString))
                {
                    if (EnumConversion.ToString(EnumStatus.Open) == entity.Status)
                    {
                        entity.Status = EnumConversion.ToString(EnumStatus.CLose);
                    }
                    else
                    {
                        entity.Status = EnumConversion.ToString(EnumStatus.CLose);
                        entity.DateClosed = DateTime.Now.Date;
                    }

                    ContactusEnquiryRepository.Update(entity);
                    if (ContactusEnquiryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateContactusEnquiryFlag(long itemId, string flag)
        {
            XsiContactusEnquiry entity = GetContactusEnquiryByItemId(itemId);
            if (entity != null)
            {
                using (ContactusEnquiryRepository = new ContactusEnquiryRepository(ConnectionString))
                {
                    entity.FlagType = flag;
                    ContactusEnquiryRepository.Update(entity);
                    if (ContactusEnquiryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteContactusEnquiry(XsiContactusEnquiry entity)
        {
            if (GetContactusEnquiry(entity).Count() > 0)
            {
                using (ContactusEnquiryRepository = new ContactusEnquiryRepository(ConnectionString))
                {
                    ContactusEnquiryRepository.Delete(entity);
                    if (ContactusEnquiryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteContactusEnquiry(long itemId)
        {
            if (GetContactusEnquiryByItemId(itemId) != null)
            {
                using (ContactusEnquiryRepository = new ContactusEnquiryRepository(ConnectionString))
                {
                    ContactusEnquiryRepository.Delete(itemId);
                    if (ContactusEnquiryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class OurteamStaffBL : IDisposable
    {
        #region Feilds
        IOurteamStaffRepository OurteamStaffRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public OurteamStaffBL()
        {
            ConnectionString = null;
        }
        public OurteamStaffBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.OurteamStaffRepository != null)
                this.OurteamStaffRepository.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiOurteamStaff GetOurteamStaffByItemId(long itemId)
        {
            using (OurteamStaffRepository = new OurteamStaffRepository(ConnectionString))
            {
                return OurteamStaffRepository.GetById(itemId);
            }
        }
       
        public List<XsiOurteamStaff> GetOurteamStaff()
        {
            using (OurteamStaffRepository = new OurteamStaffRepository(ConnectionString))
            {
                return OurteamStaffRepository.Select();
            }
        }
        public List<XsiOurteamStaff> GetOurteamStaff(EnumSortlistBy sortListBy)
        {
            using (OurteamStaffRepository = new OurteamStaffRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return OurteamStaffRepository.Select().OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return OurteamStaffRepository.Select().OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return OurteamStaffRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return OurteamStaffRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return OurteamStaffRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return OurteamStaffRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return OurteamStaffRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiOurteamStaff> GetOurteamStaff(XsiOurteamStaff entity)
        {
            using (OurteamStaffRepository = new OurteamStaffRepository(ConnectionString))
            {
                return OurteamStaffRepository.Select(entity);
            }
        }
        public List<XsiOurteamStaff> GetOurteamStaff(XsiOurteamStaff entity, EnumSortlistBy sortListBy)
        {
            using (OurteamStaffRepository = new OurteamStaffRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return OurteamStaffRepository.Select(entity).OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return OurteamStaffRepository.Select(entity).OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return OurteamStaffRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return OurteamStaffRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return OurteamStaffRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return OurteamStaffRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return OurteamStaffRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiOurteamStaff> GetCompleteOurteamStaff(XsiOurteamStaff entity, EnumSortlistBy sortListBy)
        {
            using (OurteamStaffRepository = new OurteamStaffRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return OurteamStaffRepository.SelectComeplete(entity).OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return OurteamStaffRepository.SelectComeplete(entity).OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return OurteamStaffRepository.SelectComeplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return OurteamStaffRepository.SelectComeplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return OurteamStaffRepository.SelectComeplete(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return OurteamStaffRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return OurteamStaffRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        
        public EnumResultType InsertOurteamStaff(XsiOurteamStaff entity)
        {
            using (OurteamStaffRepository = new OurteamStaffRepository(ConnectionString))
            {
                entity = OurteamStaffRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateOurteamStaff(XsiOurteamStaff entity)
        {
            XsiOurteamStaff OriginalEntity = GetOurteamStaffByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (OurteamStaffRepository = new OurteamStaffRepository(ConnectionString))
                {
                    OurteamStaffRepository.Update(entity);
                    if (OurteamStaffRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateOurteamStaffStatus(long itemId)
        {
            XsiOurteamStaff entity = GetOurteamStaffByItemId(itemId);
            if (entity != null)
            {
                using (OurteamStaffRepository = new OurteamStaffRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    OurteamStaffRepository.Update(entity);
                    if (OurteamStaffRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteOurteamStaff(XsiOurteamStaff entity)
        {
            List<XsiOurteamStaff> entities = GetOurteamStaff(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiOurteamStaff e in entities)
                {
                    result = DeleteOurteamStaff(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteOurteamStaff(long itemId)
        {
            XsiOurteamStaff entity = GetOurteamStaffByItemId(itemId);
            if (entity != null)
            {
                using (OurteamStaffRepository = new OurteamStaffRepository(ConnectionString))
                {
                    OurteamStaffRepository.Delete(itemId);
                    if (OurteamStaffRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFIllustratorJudgeBL : IDisposable
    {
        #region Feilds
        ISCRFIllustratorJudgeRepository SCRFIllustratorJudgeRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFIllustratorJudgeBL()
        {
            ConnectionString = null;
        }
        public SCRFIllustratorJudgeBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIllustratorJudgeRepository != null)
                this.SCRFIllustratorJudgeRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrfillustratorJudge GetSCRFIllustratorJudgeByItemId(long itemId)
        {
            using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
            {
                return SCRFIllustratorJudgeRepository.GetById(itemId);
            }
        }
        public XsiScrfillustratorJudge GetSCRFIllustratorJudgeByEmail(string email)
        {
            using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
            {
                return SCRFIllustratorJudgeRepository.Select(new XsiScrfillustratorJudge() { Email = email }).FirstOrDefault();
            }
        }
        public List<XsiScrfillustratorJudge> GetSCRFIllustratorJudge()
        {
            using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
            {
                return SCRFIllustratorJudgeRepository.Select();
            }
        }
        public List<XsiScrfillustratorJudge> GetSCRFIllustratorJudge(EnumSortlistBy sortListBy)
        {
            using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIllustratorJudgeRepository.Select().OrderBy(p => p.FirstName.Trim()).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIllustratorJudgeRepository.Select().OrderByDescending(p => p.FirstName.Trim()).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIllustratorJudgeRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIllustratorJudgeRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIllustratorJudgeRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfillustratorJudge> GetSCRFIllustratorJudge(XsiScrfillustratorJudge entity)
        {
            using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
            {
                return SCRFIllustratorJudgeRepository.Select(entity);
            }
        }
        public List<XsiScrfillustratorJudge> GetSCRFIllustratorJudge(XsiScrfillustratorJudge entity, EnumSortlistBy sortListBy)
        {
            using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIllustratorJudgeRepository.Select(entity).OrderBy(p => p.FirstName.Trim()).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIllustratorJudgeRepository.Select(entity).OrderByDescending(p => p.FirstName.Trim()).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIllustratorJudgeRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIllustratorJudgeRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIllustratorJudgeRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiScrfillustratorJudge> GetSCRFIllustratorJudgeOr(XsiScrfillustratorJudge entity, EnumSortlistBy sortListBy)
        {
            using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIllustratorJudgeRepository.SelectOr(entity).OrderBy(p => p.FirstName.Trim()).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIllustratorJudgeRepository.SelectOr(entity).OrderByDescending(p => p.FirstName.Trim()).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIllustratorJudgeRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIllustratorJudgeRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIllustratorJudgeRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFIllustratorJudge(XsiScrfillustratorJudge entity)
        {
            using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
            {
                entity = SCRFIllustratorJudgeRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFIllustratorJudge(XsiScrfillustratorJudge entity)
        {
            XsiScrfillustratorJudge OriginalEntity = GetSCRFIllustratorJudgeByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
                {
                    SCRFIllustratorJudgeRepository.Update(entity);
                    if (SCRFIllustratorJudgeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFIllustratorJudgeStatus(long itemId)
        {
            XsiScrfillustratorJudge entity = GetSCRFIllustratorJudgeByItemId(itemId);
            if (entity != null)
            {
                using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFIllustratorJudgeRepository.Update(entity);
                    if (SCRFIllustratorJudgeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFIllustratorJudgeStatusByGroupId(long itemId)
        {
            EnumResultType XsiResult = EnumResultType.NotFound;
            /* XsiScrfillustratorJudge existingEntity = GetSCRFIllustratorJudgeByItemId(itemId);
             long groupId = existingEntity.GroupId.Value;
             List<XsiScrfillustratorJudge> entityList = GetSCRFIllustratorJudgeByGroupId(groupId);
             EnumResultType XsiResult = EnumResultType.NotFound;
             XsiScrfillustratorJudge entity;
             foreach (XsiScrfillustratorJudge SCRFIllustratorJudge in entityList)
             {
                 entity = GetSCRFIllustratorJudgeByItemId(SCRFIllustratorJudge.ItemId);
                 if (entity != null)
                 {
                     using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
                     {
                         entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == existingEntity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                         SCRFIllustratorJudgeRepository.Update(entity);
                         if (SCRFIllustratorJudgeRepository.SubmitChanges() > 0)
                         {
                             XsiResult = EnumResultType.Success;
                             //if (EnumConversion.ToString(EnumBool.Yes) == existingEntity.IsActive)
                             //{
                             //    SCRFIllustratorJudgeRepository SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository();
                             //    SCRFIllustratorJudgeRepository.Delete(SCRFIllustratorJudge.ItemId);
                             //}
                         }
                         else
                             XsiResult = EnumResultType.Failed;
                     }
                 }
                 else
                     return XsiResult;
             }*/
            return XsiResult;
        }
        public EnumResultType UpdatePublishorUnpublishAll(long itemId, bool ispublish)
        {
            XsiScrfillustratorJudge entity = GetSCRFIllustratorJudgeByItemId(itemId);
            if (entity != null)
            {
                using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
                {
                    if (ispublish)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActive = EnumConversion.ToString(EnumBool.No);
                    SCRFIllustratorJudgeRepository.Update(entity);
                    if (SCRFIllustratorJudgeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdatePublishorUnpublishAllByGroupId(long groupId, bool ispublish)
        {
            EnumResultType XsiResult = EnumResultType.NotFound;
            /*List<XsiScrfillustratorJudge> entityList = GetSCRFIllustratorJudgeByGroupId(groupId);
            EnumResultType XsiResult = EnumResultType.NotFound;
            XsiScrfillustratorJudge entity;
            foreach (XsiScrfillustratorJudge SCRFIllustratorJudge in entityList)
            {
                entity = GetSCRFIllustratorJudgeByItemId(SCRFIllustratorJudge.ItemId);
                if (entity != null)
                {
                    using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
                    {
                        if (ispublish)
                            entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
                        else
                            entity.IsActive = EnumConversion.ToString(EnumBool.No);
                        SCRFIllustratorJudgeRepository.Update(entity);
                        if (SCRFIllustratorJudgeRepository.SubmitChanges() > 0)
                            XsiResult = EnumResultType.Success;
                        else
                            XsiResult = EnumResultType.Failed;
                    }
                }
                else
                    return XsiResult;
            }*/
            return XsiResult;
        }
        public EnumResultType DeleteSCRFIllustratorJudge(XsiScrfillustratorJudge entity)
        {
            if (GetSCRFIllustratorJudge(entity).Count() > 0)
            {
                using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
                {
                    SCRFIllustratorJudgeRepository.Delete(entity);
                    if (SCRFIllustratorJudgeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFIllustratorJudge(long itemId)
        {
            XsiScrfillustratorJudge entity = GetSCRFIllustratorJudgeByItemId(itemId);
            if (entity != null)
            {
                using (SCRFIllustratorJudgeRepository = new SCRFIllustratorJudgeRepository(ConnectionString))
                {
                    SCRFIllustratorJudgeRepository.Delete(itemId);
                    if (SCRFIllustratorJudgeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
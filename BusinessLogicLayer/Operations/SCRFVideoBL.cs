﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFVideoBL : IDisposable
    {
        #region Feilds
        ISCRFVideoRepository SCRFVideoRepository;

        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFVideoBL()
        {
            ConnectionString = null;
        }
        public SCRFVideoBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFVideoRepository != null)
                this.SCRFVideoRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextSortIndex()
        {
            //optional method. return -1 if not required and remove following
            using (SCRFVideoRepository = new SCRFVideoRepository(ConnectionString))
            {
                return (SCRFVideoRepository.Select().Max(p => p.SortIndex) ?? 0) + 1;
            }
        }

        public XsiScrfvideo GetSCRFVideoByItemId(long itemId)
        {
            using (SCRFVideoRepository = new SCRFVideoRepository(ConnectionString))
            {
                return SCRFVideoRepository.GetById(itemId);
            }
        }

        public List<XsiScrfvideo> GetSCRFVideo()
        {
            using (SCRFVideoRepository = new SCRFVideoRepository(ConnectionString))
            {
                return SCRFVideoRepository.Select();
            }
        }
        public List<XsiScrfvideo> GetSCRFVideo(EnumSortlistBy sortListBy)
        {
            using (SCRFVideoRepository = new SCRFVideoRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFVideoRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFVideoRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFVideoRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFVideoRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFVideoRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFVideoRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFVideoRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfvideo> GetSCRFVideo(XsiScrfvideo entity)
        {
            using (SCRFVideoRepository = new SCRFVideoRepository(ConnectionString))
            {
                return SCRFVideoRepository.Select(entity);
            }
        }
        public List<XsiScrfvideo> GetSCRFVideo(XsiScrfvideo entity, EnumSortlistBy sortListBy)
        {
            using (SCRFVideoRepository = new SCRFVideoRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFVideoRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFVideoRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFVideoRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFVideoRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFVideoRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFVideoRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.BySortOrderAsc:
                        return SCRFVideoRepository.Select(entity).OrderBy(p => p.SortIndex).ToList();

                    case EnumSortlistBy.BySortOrderDesc:
                        return SCRFVideoRepository.Select(entity).OrderByDescending(p => p.SortIndex).ToList();

                    default:
                        return SCRFVideoRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFVideo(XsiScrfvideo entity)
        {
            using (SCRFVideoRepository = new SCRFVideoRepository(ConnectionString))
            {
                entity = SCRFVideoRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFVideo(XsiScrfvideo entity)
        {
            XsiScrfvideo OriginalEntity = GetSCRFVideoByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFVideoRepository = new SCRFVideoRepository(ConnectionString))
                {
                    SCRFVideoRepository.Update(entity);
                    if (SCRFVideoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFVideoStatus(long itemId)
        {
            XsiScrfvideo entity = GetSCRFVideoByItemId(itemId);
            if (entity != null)
            {
                using (SCRFVideoRepository = new SCRFVideoRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFVideoRepository.Update(entity);
                    if (SCRFVideoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFAlbumCover(long itemId)
        {
            using (SCRFVideoRepository = new SCRFVideoRepository(ConnectionString))
            {
                XsiScrfvideo entity = SCRFVideoRepository.GetById(itemId);
                XsiScrfvideo entity1 = new XsiScrfvideo();
                //entity1.CategoryId = entity.CategoryId;
                //entity1.LanguageId = entity.LanguageId;
                List<XsiScrfvideo> VideoList = SCRFVideoRepository.Select(entity1);
                if (VideoList != null)
                {
                    foreach (XsiScrfvideo entity2 in VideoList)
                    {
                        if (itemId != entity2.ItemId)
                        {
                            entity2.IsAlbumCover = EnumConversion.ToString(EnumBool.No);
                            SCRFVideoRepository.Update(entity2);
                        }
                    }
                    if (SCRFVideoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
                else
                    return EnumResultType.NotFound;
            }
        }
        public EnumResultType DeleteSCRFVideo(XsiScrfvideo entity)
        {
            List<XsiScrfvideo> entities = GetSCRFVideo(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfvideo e in entities)
                {
                    result = DeleteSCRFVideo(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFVideo(long itemId)
        {
            XsiScrfvideo entity = GetSCRFVideoByItemId(itemId);
            if (entity != null)
            {
                using (SCRFVideoRepository = new SCRFVideoRepository(ConnectionString))
                {
                    SCRFVideoRepository.Delete(itemId);
                    if (SCRFVideoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
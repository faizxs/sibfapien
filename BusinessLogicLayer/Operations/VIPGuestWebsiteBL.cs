﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class VIPGuestWebsiteBL : IDisposable
    {
        #region Feilds
        IVIPGuestWebsiteRepository VIPGuestWebsiteRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public VIPGuestWebsiteBL()
        {
            ConnectionString = null;
        }
        public VIPGuestWebsiteBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.VIPGuestWebsiteRepository != null)
                this.VIPGuestWebsiteRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (VIPGuestWebsiteRepository = new VIPGuestWebsiteRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiVipguestWebsite GetVIPGuestWebsiteByItemId(long itemId)
        {
            using (VIPGuestWebsiteRepository = new VIPGuestWebsiteRepository(ConnectionString))
            {
                return VIPGuestWebsiteRepository.GetById(itemId);
            }
        }

        public List<XsiVipguestWebsite> GetVIPGuestWebsite()
        {
            using (VIPGuestWebsiteRepository = new VIPGuestWebsiteRepository(ConnectionString))
            {
                return VIPGuestWebsiteRepository.Select();
            }
        }
        public List<XsiVipguestWebsite> GetVIPGuestWebsite(EnumSortlistBy sortListBy)
        {
            using (VIPGuestWebsiteRepository = new VIPGuestWebsiteRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return VIPGuestWebsiteRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return VIPGuestWebsiteRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return VIPGuestWebsiteRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiVipguestWebsite> GetVIPGuestWebsite(XsiVipguestWebsite entity)
        {
            using (VIPGuestWebsiteRepository = new VIPGuestWebsiteRepository(ConnectionString))
            {
                return VIPGuestWebsiteRepository.Select(entity);
            }
        }
        public List<XsiVipguestWebsite> GetVIPGuestWebsite(XsiVipguestWebsite entity, EnumSortlistBy sortListBy)
        {
            using (VIPGuestWebsiteRepository = new VIPGuestWebsiteRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return VIPGuestWebsiteRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return VIPGuestWebsiteRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return VIPGuestWebsiteRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<long?> GetVIPGuestId(long ID1, long ID2, long ID3)
        {
            using (VIPGuestWebsiteRepository = new VIPGuestWebsiteRepository(ConnectionString))
            {
                return VIPGuestWebsiteRepository.Select(ID1, ID2, ID3);
            }
        }
        public XsiVipguestWebsite GetVIPGuestWebsite(long groupId, long languageId)
        {
            using (VIPGuestWebsiteRepository = new VIPGuestWebsiteRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertVIPGuestWebsite(XsiVipguestWebsite entity)
        {
            using (VIPGuestWebsiteRepository = new VIPGuestWebsiteRepository(ConnectionString))
            {
                entity = VIPGuestWebsiteRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateVIPGuestWebsite(XsiVipguestWebsite entity)
        {
            XsiVipguestWebsite OriginalEntity = GetVIPGuestWebsiteByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (VIPGuestWebsiteRepository = new VIPGuestWebsiteRepository(ConnectionString))
                {
                    VIPGuestWebsiteRepository.Update(entity);
                    if (VIPGuestWebsiteRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateVIPGuestWebsiteStatus(long itemId)
        {
            return EnumResultType.NotFound;
        }
        public EnumResultType DeleteVIPGuestWebsite(XsiVipguestWebsite entity)
        {
            List<XsiVipguestWebsite> entities = GetVIPGuestWebsite(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiVipguestWebsite e in entities)
                {
                    result = DeleteVIPGuestWebsite(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteVIPGuestWebsite(long itemId)
        {
            XsiVipguestWebsite entity = GetVIPGuestWebsiteByItemId(itemId);
            if (entity != null)
            {
                using (VIPGuestWebsiteRepository = new VIPGuestWebsiteRepository(ConnectionString))
                {
                    VIPGuestWebsiteRepository.Delete(itemId);
                    if (VIPGuestWebsiteRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
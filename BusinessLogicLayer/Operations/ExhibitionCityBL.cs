﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionCityBL : IDisposable
    {
        #region Feilds
        IExhibitionCityRepository ExhibitionCityRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionCityBL()
        {
            ConnectionString = null;
        }
        public ExhibitionCityBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionCityRepository != null)
                this.ExhibitionCityRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionCity GetExhibitionCityByItemId(long itemId)
        {
            using (ExhibitionCityRepository = new ExhibitionCityRepository(ConnectionString))
            {
                return ExhibitionCityRepository.GetById(itemId);
            }
        }
        public List<XsiExhibitionCity> GetExhibitionCity()
        {
            using (ExhibitionCityRepository = new ExhibitionCityRepository(ConnectionString))
            {
                return ExhibitionCityRepository.Select();
            }
        }
        public List<XsiExhibitionCity> GetExhibitionCity(EnumSortlistBy sortListBy)
        {
            using (ExhibitionCityRepository = new ExhibitionCityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCityRepository.Select().OrderBy(p => p.CityName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCityRepository.Select().OrderByDescending(p => p.CityName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCityRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCityRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCityRepository.Select().OrderBy(p => p.CityId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCityRepository.Select().OrderByDescending(p => p.CityId).ToList();

                    default:
                        return ExhibitionCityRepository.Select().OrderByDescending(p => p.CityId).ToList();
                }
            }
        }
        public List<XsiExhibitionCity> GetExhibitionCity(XsiExhibitionCity entity)
        {
            using (ExhibitionCityRepository = new ExhibitionCityRepository(ConnectionString))
            {
                return ExhibitionCityRepository.Select(entity);
            }
        }
        public List<XsiExhibitionCity> GetExhibitionCity(XsiExhibitionCity entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionCityRepository = new ExhibitionCityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCityRepository.Select(entity).OrderBy(p => p.CityName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCityRepository.Select(entity).OrderByDescending(p => p.CityName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCityRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCityRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCityRepository.Select(entity).OrderBy(p => p.CityId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCityRepository.Select(entity).OrderByDescending(p => p.CityId).ToList();

                    default:
                        return ExhibitionCityRepository.Select(entity).OrderByDescending(p => p.CityId).ToList();
                }
            }
        }
        public List<XsiExhibitionCity> GetCompleteExhibitionCity(XsiExhibitionCity entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionCityRepository = new ExhibitionCityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCityRepository.SelectComeplete(entity).OrderBy(p => p.CityName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCityRepository.SelectComeplete(entity).OrderByDescending(p => p.CityName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCityRepository.SelectComeplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCityRepository.SelectComeplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCityRepository.SelectComeplete(entity).OrderBy(p => p.CityId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCityRepository.SelectComeplete(entity).OrderByDescending(p => p.CityId).ToList();

                    default:
                        return ExhibitionCityRepository.SelectComeplete(entity).OrderByDescending(p => p.CityId).ToList();
                }
            }
        }

        public EnumResultType InsertExhibitionCity(XsiExhibitionCity entity)
        {
            using (ExhibitionCityRepository = new ExhibitionCityRepository(ConnectionString))
            {
                entity = ExhibitionCityRepository.Add(entity);
                if (entity.CityId > 0)
                {
                    XsiItemId = entity.CityId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionCity(XsiExhibitionCity entity)
        {
            XsiExhibitionCity OriginalEntity = GetExhibitionCityByItemId(entity.CityId);

            if (OriginalEntity != null)
            {
                using (ExhibitionCityRepository = new ExhibitionCityRepository(ConnectionString))
                {
                    ExhibitionCityRepository.Update(entity);
                    if (ExhibitionCityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionCityStatus(long itemId)
        {
            XsiExhibitionCity entity = GetExhibitionCityByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionCityRepository = new ExhibitionCityRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionCityRepository.Update(entity);
                    if (ExhibitionCityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionCity(XsiExhibitionCity entity)
        {
            List<XsiExhibitionCity> entities = GetExhibitionCity(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionCity e in entities)
                {
                    result = DeleteExhibitionCity(e.CityId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionCity(long itemId)
        {
            XsiExhibitionCity entity = GetExhibitionCityByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionCityRepository = new ExhibitionCityRepository(ConnectionString))
                {
                    ExhibitionCityRepository.Delete(itemId);
                    if (ExhibitionCityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
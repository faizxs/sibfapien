﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class AgencyStatusLogBL : IDisposable
    {
        #region Feilds
        IAgencyStatusLogRepository IAgencyStatusLogRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public AgencyStatusLogBL()
        {
            ConnectionString = null;
        }
        public AgencyStatusLogBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IAgencyStatusLogRepository != null)
                this.IAgencyStatusLogRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionMemberApplicationYearlyLogs GetAgencyStatusLogByItemId(long itemId)
        {
            using (IAgencyStatusLogRepository = new AgencyStatusLogRepository(ConnectionString))
            {
                return IAgencyStatusLogRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionMemberApplicationYearlyLogs> GetAgencyStatusLog()
        {
            using (IAgencyStatusLogRepository = new AgencyStatusLogRepository(ConnectionString))
            {
                return IAgencyStatusLogRepository.Select();
            }
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetAgencyStatusLog(EnumSortlistBy sortListBy)
        {
            using (IAgencyStatusLogRepository = new AgencyStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IAgencyStatusLogRepository.Select().OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IAgencyStatusLogRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IAgencyStatusLogRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IAgencyStatusLogRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IAgencyStatusLogRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IAgencyStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IAgencyStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetAgencyStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            using (IAgencyStatusLogRepository = new AgencyStatusLogRepository(ConnectionString))
            {
                return IAgencyStatusLogRepository.Select(entity);
            }
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetAgencyStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity, EnumSortlistBy sortListBy)
        {
            using (IAgencyStatusLogRepository = new AgencyStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IAgencyStatusLogRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IAgencyStatusLogRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IAgencyStatusLogRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IAgencyStatusLogRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IAgencyStatusLogRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IAgencyStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IAgencyStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionMemberApplicationYearlyLogs GetAgencyStatusLog(long groupId, long languageId)
        {
            using (IAgencyStatusLogRepository = new AgencyStatusLogRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertAgencyStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            using (IAgencyStatusLogRepository = new AgencyStatusLogRepository(ConnectionString))
            {
                entity = IAgencyStatusLogRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateAgencyStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            XsiExhibitionMemberApplicationYearlyLogs OriginalEntity = GetAgencyStatusLogByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IAgencyStatusLogRepository = new AgencyStatusLogRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line

                    IAgencyStatusLogRepository.Update(entity);
                    if (IAgencyStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateAgencyStatusLogStatus(long itemId)
        {
            XsiExhibitionMemberApplicationYearlyLogs entity = GetAgencyStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IAgencyStatusLogRepository = new AgencyStatusLogRepository(ConnectionString))
                {
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IAgencyStatusLogRepository.Update(entity);
                    if (IAgencyStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteAgencyStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            List<XsiExhibitionMemberApplicationYearlyLogs> entities = GetAgencyStatusLog(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionMemberApplicationYearlyLogs e in entities)
                {
                    result = DeleteAgencyStatusLog(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteAgencyStatusLog(long itemId)
        {
            XsiExhibitionMemberApplicationYearlyLogs entity = GetAgencyStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IAgencyStatusLogRepository = new AgencyStatusLogRepository(ConnectionString))
                {
                    IAgencyStatusLogRepository.Delete(itemId);
                    if (IAgencyStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
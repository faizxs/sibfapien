﻿using System;
using System.Collections.Generic;
using System.Text;
using Xsi.Repositories;
using Entities.Models;
using System.Linq;

namespace Xsi.BusinessLogicLayer
{
    public class AdminUserBL : IDisposable
    {
        #region Feilds
        IAdminUserRepository XsiAdminUserRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public AdminUserBL()
        {
            ConnectionString = null;
        }
        public AdminUserBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiAdminUserRepository != null)
                this.XsiAdminUserRepository.Dispose();
        }
        #endregion
        #region Methods
        public bool IsValidAdmin(string userName, string password)
        {
            using (XsiAdminUserRepository = new AdminUsersRepository(ConnectionString))
            {
                XsiAdminUsers user = GetUserByUsername(userName);
                if (user != null)
                {
                    if (user.UserName == userName && user.Password == password && user.IsActive == EnumConversion.ToString(EnumBool.Yes))
                        return true;
                }

                return false;
            }
        }
        public bool IsSuperAdmin(long itemId)
        {
            using (XsiAdminUserRepository = new AdminUsersRepository(ConnectionString))
            {
                XsiAdminUsers user = GetAdminUserByItemId(itemId);
                if (user != null)
                {
                    if (user.IsSuperAdmin == EnumConversion.ToString(EnumBool.Yes))
                        return true;
                }

                return false;
            }
        }
        public bool IsAdmin(long itemId)
        {
            using (XsiAdminUserRepository = new AdminUsersRepository(ConnectionString))
            {
                XsiAdminUsers user = GetAdminUserByItemId(itemId);
                if (user != null)
                {
                    if (user.RoleId == 2)
                        return true;
                }

                return false;
            }
        }

        public XsiAdminUsers GetAdminUserByItemId(long itemId)
        {
            using (XsiAdminUserRepository = new AdminUsersRepository())
            {
                return XsiAdminUserRepository.GetById(itemId);
            }
        }
        public XsiAdminUsers GetUserByUsername(string userName)
        {
            using (XsiAdminUserRepository = new AdminUsersRepository())
            {
                return XsiAdminUserRepository.GetByUserName(userName);
            }
        }

        public List<XsiAdminUsers> GetAdminUser(EnumSortlistBy sortListBy)
        {
            using (XsiAdminUserRepository = new AdminUsersRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiAdminUserRepository.Select().OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiAdminUserRepository.Select().OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiAdminUserRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiAdminUserRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiAdminUserRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiAdminUsers> GetAdminUser(XsiAdminUsers users)
        {
            using (XsiAdminUserRepository = new AdminUsersRepository(ConnectionString))
            {
                return XsiAdminUserRepository.Select(users);
            }
        }

        public List<XsiAdminUsers> GetUsersAndLogs(XsiAdminUsers users)
        {
            using (XsiAdminUserRepository = new AdminUsersRepository(ConnectionString))
            {
                return XsiAdminUserRepository.SelectAdminUserAndLogs(users);
            }
        }
        public List<XsiAdminUsers> GetUsersAndRoles(XsiAdminUsers users)
        {
            using (XsiAdminUserRepository = new AdminUsersRepository(ConnectionString))
            {
                return XsiAdminUserRepository.SelectAdminUserAndRoles(users);
            }
        }
        public List<XsiAdminUsers> GetUsersAndPermissions(XsiAdminUsers users)
        {
            using (XsiAdminUserRepository = new AdminUsersRepository(ConnectionString))
            {
                return XsiAdminUserRepository.SelectAdminUserAndPermissions(users);
            }
        }

        public EnumResultType InsertAdminUser(XsiAdminUsers users)
        {
            using (XsiAdminUserRepository = new AdminUsersRepository(ConnectionString))
            {
                users = XsiAdminUserRepository.Add(users);
                if (users.ItemId > 0)
                {
                    XsiItemdId = users.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateAdminUser(XsiAdminUsers users)
        {
            XsiAdminUsers entity = GetAdminUserByItemId(users.ItemId);
            if (entity != null)
            {
                using (XsiAdminUserRepository = new AdminUsersRepository(ConnectionString))
                {
                    if (string.IsNullOrEmpty(users.Password))
                        users.Password = entity.Password;
                    users.IsActive = entity.IsActive;
                    users.IsSuperAdmin = entity.IsSuperAdmin;

                    XsiAdminUserRepository.Update(users);
                    if (XsiAdminUserRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateAdminUserStatus(long itemId)
        {
            XsiAdminUsers entity = GetAdminUserByItemId(itemId);
            if (entity != null)
            {
                using (XsiAdminUserRepository = new AdminUsersRepository(ConnectionString))
                {
                    entity.IsActive = entity.IsActive == EnumConversion.ToString(EnumBool.Yes) ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    XsiAdminUserRepository.Update(entity);
                    if (XsiAdminUserRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteAdminUser(XsiAdminUsers users)
        {
            if (GetAdminUser(users).Count() > 0)
            {
                using (XsiAdminUserRepository = new AdminUsersRepository(ConnectionString))
                {
                    using (IAdminLogRepository logs = new AdminUserLogsRepository(ConnectionString))
                    {
                        logs.Delete(new XsiAdminLogs() { UserId = users.ItemId });
                        logs.SubmitChanges();
                    }
                    using (IAdminPermissionRepository permissions = new AdminPermissionsRepository(ConnectionString))
                    {
                        permissions.Delete(new XsiAdminPermissions() { UserId = users.ItemId });
                        permissions.SubmitChanges();
                    }

                    XsiAdminUserRepository.Delete(users);
                    if (XsiAdminUserRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteAdminUser(long itemId)
        {
            if (GetAdminUserByItemId(itemId) != null)
            {
                using (XsiAdminUserRepository = new AdminUsersRepository(ConnectionString))
                {
                    using (IAdminLogRepository logs = new AdminUserLogsRepository(ConnectionString))
                    {
                        logs.Delete(new XsiAdminLogs() { UserId = itemId });
                        logs.SubmitChanges();
                    }
                    using (IAdminPermissionRepository permissions = new AdminPermissionsRepository(ConnectionString))
                    {
                        permissions.Delete(new XsiAdminPermissions() { UserId = itemId });
                        permissions.SubmitChanges();
                    }

                    XsiAdminUserRepository.Delete(itemId);
                    if (XsiAdminUserRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFPhotoSubCategoryBL : IDisposable
    {
        #region Feilds
        ISCRFPhotoSubCategoryRepository SCRFPhotoSubCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFPhotoSubCategoryBL()
        {
            ConnectionString = null;
        }
        public SCRFPhotoSubCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFPhotoSubCategoryRepository != null)
                this.SCRFPhotoSubCategoryRepository.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiScrfphotoSubCategory GetSCRFPhotoSubCategoryByItemId(long itemId)
        {
            using (SCRFPhotoSubCategoryRepository = new SCRFPhotoSubCategoryRepository(ConnectionString))
            {
                return SCRFPhotoSubCategoryRepository.GetById(itemId);
            }
        }
         
        public List<XsiScrfphotoSubCategory> GetSCRFPhotoSubCategory()
        {
            using (SCRFPhotoSubCategoryRepository = new SCRFPhotoSubCategoryRepository(ConnectionString))
            {
                return SCRFPhotoSubCategoryRepository.Select();
            }
        }
        public List<XsiScrfphotoSubCategory> GetSCRFPhotoSubCategory(EnumSortlistBy sortListBy)
        {
            using (SCRFPhotoSubCategoryRepository = new SCRFPhotoSubCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPhotoSubCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPhotoSubCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFPhotoSubCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFPhotoSubCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPhotoSubCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPhotoSubCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFPhotoSubCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfphotoSubCategory> GetSCRFPhotoSubCategory(XsiScrfphotoSubCategory entity)
        {
            using (SCRFPhotoSubCategoryRepository = new SCRFPhotoSubCategoryRepository(ConnectionString))
            {
                return SCRFPhotoSubCategoryRepository.Select(entity);
            }
        }
        public List<XsiScrfphotoSubCategory> GetSCRFPhotoSubCategory(XsiScrfphotoSubCategory entity, EnumSortlistBy sortListBy)
        {
            using (SCRFPhotoSubCategoryRepository = new SCRFPhotoSubCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPhotoSubCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPhotoSubCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFPhotoSubCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFPhotoSubCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPhotoSubCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPhotoSubCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFPhotoSubCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfphotoSubCategory> GetCompleteSCRFPhotoSubCategory(XsiScrfphotoSubCategory entity, EnumSortlistBy sortListBy)
        {
            using (SCRFPhotoSubCategoryRepository = new SCRFPhotoSubCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPhotoSubCategoryRepository.SelectComeplete(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPhotoSubCategoryRepository.SelectComeplete(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFPhotoSubCategoryRepository.SelectComeplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFPhotoSubCategoryRepository.SelectComeplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPhotoSubCategoryRepository.SelectComeplete(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPhotoSubCategoryRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFPhotoSubCategoryRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        
        public EnumResultType InsertSCRFPhotoSubCategory(XsiScrfphotoSubCategory entity)
        {
            using (SCRFPhotoSubCategoryRepository = new SCRFPhotoSubCategoryRepository(ConnectionString))
            {
                entity = SCRFPhotoSubCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFPhotoSubCategory(XsiScrfphotoSubCategory entity)
        {
            XsiScrfphotoSubCategory OriginalEntity = GetSCRFPhotoSubCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFPhotoSubCategoryRepository = new SCRFPhotoSubCategoryRepository(ConnectionString))
                {
                    SCRFPhotoSubCategoryRepository.Update(entity);
                    if (SCRFPhotoSubCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFPhotoSubCategoryStatus(long itemId)
        {
            XsiScrfphotoSubCategory entity = GetSCRFPhotoSubCategoryByItemId(itemId);
            if (entity != null)
            {
                using (SCRFPhotoSubCategoryRepository = new SCRFPhotoSubCategoryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFPhotoSubCategoryRepository.Update(entity);
                    if (SCRFPhotoSubCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFPhotoSubCategory(XsiScrfphotoSubCategory entity)
        {
            List<XsiScrfphotoSubCategory> entities = GetSCRFPhotoSubCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfphotoSubCategory e in entities)
                {
                    result = DeleteSCRFPhotoSubCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFPhotoSubCategory(long itemId)
        {
            XsiScrfphotoSubCategory entity = GetSCRFPhotoSubCategoryByItemId(itemId);
            if (entity != null)
            {
                using (SCRFPhotoSubCategoryRepository = new SCRFPhotoSubCategoryRepository(ConnectionString))
                {
                    SCRFPhotoSubCategoryRepository.Delete(itemId);
                    if (SCRFPhotoSubCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
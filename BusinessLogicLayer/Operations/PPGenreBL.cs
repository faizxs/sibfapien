﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PPGenreBL : IDisposable
    {
        #region Feilds
        IPPGenreRepository PPGenreRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PPGenreBL()
        {
            ConnectionString = null;
        }
        public PPGenreBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PPGenreRepository != null)
                this.PPGenreRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionPpgenre GetPPGenreByItemId(long itemId)
        {
            using (PPGenreRepository = new PPGenreRepository(ConnectionString))
            {
                return PPGenreRepository.GetById(itemId);
            }
        }
        public List<XsiExhibitionPpgenre> GetPPGenre()
        {
            using (PPGenreRepository = new PPGenreRepository(ConnectionString))
            {
                return PPGenreRepository.Select();
            }
        }
        public List<XsiExhibitionPpgenre> GetPPGenre(EnumSortlistBy sortListBy)
        {
            using (PPGenreRepository = new PPGenreRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return PPGenreRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PPGenreRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PPGenreRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionPpgenre> GetPPGenre(XsiExhibitionPpgenre entity)
        {
            using (PPGenreRepository = new PPGenreRepository(ConnectionString))
            {
                return PPGenreRepository.Select(entity);
            }
        }
        public List<XsiExhibitionPpgenre> GetPPGenre(XsiExhibitionPpgenre entity, EnumSortlistBy sortListBy)
        {
            using (PPGenreRepository = new PPGenreRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return PPGenreRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PPGenreRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PPGenreRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionPpgenre GetPPGenre(long groupId, long languageId)
        {
            return null;
        }

        public EnumResultType InsertPPGenre(XsiExhibitionPpgenre entity)
        {
            using (PPGenreRepository = new PPGenreRepository(ConnectionString))
            {
                entity = PPGenreRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePPGenre(XsiExhibitionPpgenre entity)
        {
            XsiExhibitionPpgenre OriginalEntity = GetPPGenreByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PPGenreRepository = new PPGenreRepository(ConnectionString))
                {
                    PPGenreRepository.Update(entity);
                    if (PPGenreRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePPGenre(XsiExhibitionPpgenre entity)
        {
            List<XsiExhibitionPpgenre> entities = GetPPGenre(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionPpgenre e in entities)
                {
                    result = DeletePPGenre(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeletePPGenre(long itemId)
        {
            XsiExhibitionPpgenre entity = GetPPGenreByItemId(itemId);
            if (entity != null)
            {
                using (PPGenreRepository = new PPGenreRepository(ConnectionString))
                {
                    PPGenreRepository.Delete(itemId);
                    if (PPGenreRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
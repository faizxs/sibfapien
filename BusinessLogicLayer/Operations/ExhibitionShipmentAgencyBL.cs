﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionShipmentAgencyBL : IDisposable
    {
        #region Feilds
        IExhibitionShipmentAgencyRepository ExhibitionShipmentAgencyRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionShipmentAgencyBL()
        {
            ConnectionString = null;
        }
        public ExhibitionShipmentAgencyBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionShipmentAgencyRepository != null)
                this.ExhibitionShipmentAgencyRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (ExhibitionShipmentAgencyRepository = new ExhibitionShipmentAgencyRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionShipmentAgency GetExhibitionShipmentAgencyByItemId(long itemId)
        {
            using (ExhibitionShipmentAgencyRepository = new ExhibitionShipmentAgencyRepository(ConnectionString))
            {
                return ExhibitionShipmentAgencyRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionShipmentAgency> GetExhibitionShipmentAgency()
        {
            using (ExhibitionShipmentAgencyRepository = new ExhibitionShipmentAgencyRepository(ConnectionString))
            {
                return ExhibitionShipmentAgencyRepository.Select();
            }
        }
        public List<XsiExhibitionShipmentAgency> GetExhibitionShipmentAgency(EnumSortlistBy sortListBy)
        {
            using (ExhibitionShipmentAgencyRepository = new ExhibitionShipmentAgencyRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionShipmentAgencyRepository.Select().OrderBy(p => p.ExhibitionShipmentId).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionShipmentAgencyRepository.Select().OrderByDescending(p => p.ExhibitionShipmentId).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionShipmentAgencyRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionShipmentAgencyRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionShipmentAgencyRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionShipmentAgency> GetExhibitionShipmentAgency(XsiExhibitionShipmentAgency entity)
        {
            using (ExhibitionShipmentAgencyRepository = new ExhibitionShipmentAgencyRepository(ConnectionString))
            {
                return ExhibitionShipmentAgencyRepository.Select(entity);
            }
        }
        public List<XsiExhibitionShipmentAgency> GetExhibitionShipmentAgency(XsiExhibitionShipmentAgency entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionShipmentAgencyRepository = new ExhibitionShipmentAgencyRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionShipmentAgencyRepository.Select(entity).OrderBy(p => p.ExhibitionShipmentId).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionShipmentAgencyRepository.Select(entity).OrderByDescending(p => p.ExhibitionShipmentId).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionShipmentAgencyRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionShipmentAgencyRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionShipmentAgencyRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionShipmentAgency GetExhibitionShipmentAgency(long groupId, long languageId)
        {
            using (ExhibitionShipmentAgencyRepository = new ExhibitionShipmentAgencyRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionShipmentAgency(XsiExhibitionShipmentAgency entity)
        {
            using (ExhibitionShipmentAgencyRepository = new ExhibitionShipmentAgencyRepository(ConnectionString))
            {
                entity = ExhibitionShipmentAgencyRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionShipmentAgency(XsiExhibitionShipmentAgency entity)
        {
            XsiExhibitionShipmentAgency OriginalEntity = GetExhibitionShipmentAgencyByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionShipmentAgencyRepository = new ExhibitionShipmentAgencyRepository(ConnectionString))
                {
                    ExhibitionShipmentAgencyRepository.Update(entity);
                    if (ExhibitionShipmentAgencyRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionShipmentAgencyStatus(long itemId)
        {

            return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionShipmentAgency(XsiExhibitionShipmentAgency entity)
        {
            List<XsiExhibitionShipmentAgency> entities = GetExhibitionShipmentAgency(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionShipmentAgency e in entities)
                {
                    result = DeleteExhibitionShipmentAgency(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionShipmentAgency(long itemId)
        {
            XsiExhibitionShipmentAgency entity = GetExhibitionShipmentAgencyByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionShipmentAgencyRepository = new ExhibitionShipmentAgencyRepository(ConnectionString))
                {
                    ExhibitionShipmentAgencyRepository.Delete(itemId);
                    if (ExhibitionShipmentAgencyRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        //public GetEvents_Result GetEventbyGuest(long guestId)
        //{
        //    using (ExhibitionShipmentAgencyRepository = new ExhibitionShipmentAgencyRepository(ConnectionString))
        //    {
        //        return ExhibitionShipmentAgencyRepository.GetEventbyGuest(guestId);
        //    }
        //}
        //public List<GetEvents_Result> GetEventbyGuestList(long guestId)
        //{
        //    using (ExhibitionShipmentAgencyRepository = new ExhibitionShipmentAgencyRepository(ConnectionString))
        //    {
        //        return ExhibitionShipmentAgencyRepository.GetEventbyGuestList(guestId);
        //    }
        //}

        //public List<GetEvents_Result> GetActivityList(string type, long guestGid)
        //{
        //    using (ExhibitionShipmentAgencyRepository = new ExhibitionShipmentAgencyRepository(ConnectionString))
        //    {
        //        return ExhibitionShipmentAgencyRepository.GetActivityList(type, guestGid);
        //    }
        //}
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class TranslationGrantMemberStatusLogBL : IDisposable
    {
        #region Feilds
        ITranslationGrantMemberStatusLogRepository ITranslationGrantMemberStatusLogRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public TranslationGrantMemberStatusLogBL()
        {
            ConnectionString = null;
        }
        public TranslationGrantMemberStatusLogBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ITranslationGrantMemberStatusLogRepository != null)
                this.ITranslationGrantMemberStatusLogRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (ITranslationGrantMemberStatusLogRepository = new TranslationGrantMemberStatusLogRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionTranslationGrantMemberStatusLog GetTranslationGrantMemberStatusLogByItemId(long itemId)
        {
            using (ITranslationGrantMemberStatusLogRepository = new TranslationGrantMemberStatusLogRepository(ConnectionString))
            {
                return ITranslationGrantMemberStatusLogRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionTranslationGrantMemberStatusLog> GetTranslationGrantMemberStatusLog()
        {
            using (ITranslationGrantMemberStatusLogRepository = new TranslationGrantMemberStatusLogRepository(ConnectionString))
            {
                return ITranslationGrantMemberStatusLogRepository.Select();
            }
        }
        public List<XsiExhibitionTranslationGrantMemberStatusLog> GetTranslationGrantMemberStatusLog(EnumSortlistBy sortListBy)
        {
            using (ITranslationGrantMemberStatusLogRepository = new TranslationGrantMemberStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return ITranslationGrantMemberStatusLogRepository.Select().OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return ITranslationGrantMemberStatusLogRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ITranslationGrantMemberStatusLogRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ITranslationGrantMemberStatusLogRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ITranslationGrantMemberStatusLogRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ITranslationGrantMemberStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ITranslationGrantMemberStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionTranslationGrantMemberStatusLog> GetTranslationGrantMemberStatusLog(XsiExhibitionTranslationGrantMemberStatusLog entity)
        {
            using (ITranslationGrantMemberStatusLogRepository = new TranslationGrantMemberStatusLogRepository(ConnectionString))
            {
                return ITranslationGrantMemberStatusLogRepository.Select(entity);
            }
        }
        public List<XsiExhibitionTranslationGrantMemberStatusLog> GetTranslationGrantMemberStatusLog(XsiExhibitionTranslationGrantMemberStatusLog entity, EnumSortlistBy sortListBy)
        {
            using (ITranslationGrantMemberStatusLogRepository = new TranslationGrantMemberStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return ITranslationGrantMemberStatusLogRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return ITranslationGrantMemberStatusLogRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ITranslationGrantMemberStatusLogRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ITranslationGrantMemberStatusLogRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ITranslationGrantMemberStatusLogRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ITranslationGrantMemberStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ITranslationGrantMemberStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionTranslationGrantMemberStatusLog GetTranslationGrantMemberStatusLog(long groupId, long languageId)
        {
            using (ITranslationGrantMemberStatusLogRepository = new TranslationGrantMemberStatusLogRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertTranslationGrantMemberStatusLog(XsiExhibitionTranslationGrantMemberStatusLog entity)
        {
            using (ITranslationGrantMemberStatusLogRepository = new TranslationGrantMemberStatusLogRepository(ConnectionString))
            {
                entity = ITranslationGrantMemberStatusLogRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId =  -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateTranslationGrantMemberStatusLog(XsiExhibitionTranslationGrantMemberStatusLog entity)
        {
            XsiExhibitionTranslationGrantMemberStatusLog OriginalEntity = GetTranslationGrantMemberStatusLogByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ITranslationGrantMemberStatusLogRepository = new TranslationGrantMemberStatusLogRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line
                    
                    ITranslationGrantMemberStatusLogRepository.Update(entity);
                    if (ITranslationGrantMemberStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateTranslationGrantMemberStatusLogStatus(long itemId)
        {
            XsiExhibitionTranslationGrantMemberStatusLog entity = GetTranslationGrantMemberStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (ITranslationGrantMemberStatusLogRepository = new TranslationGrantMemberStatusLogRepository(ConnectionString))
                {
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ITranslationGrantMemberStatusLogRepository.Update(entity);
                    if (ITranslationGrantMemberStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteTranslationGrantMemberStatusLog(XsiExhibitionTranslationGrantMemberStatusLog entity)
        {
            List<XsiExhibitionTranslationGrantMemberStatusLog> entities = GetTranslationGrantMemberStatusLog(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionTranslationGrantMemberStatusLog e in entities)
                {
                    result = DeleteTranslationGrantMemberStatusLog(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteTranslationGrantMemberStatusLog(long itemId)
        {
            XsiExhibitionTranslationGrantMemberStatusLog entity = GetTranslationGrantMemberStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (ITranslationGrantMemberStatusLogRepository = new TranslationGrantMemberStatusLogRepository(ConnectionString))
                {
                    ITranslationGrantMemberStatusLogRepository.Delete(itemId);
                    if (ITranslationGrantMemberStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
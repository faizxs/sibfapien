﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionOtherEventsBL : IDisposable
    {
        #region Feilds
        IExhibitionOtherEventsRepository ExhibitionOtherEventsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionOtherEventsBL()
        {
            ConnectionString = null;
        }
        public ExhibitionOtherEventsBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionOtherEventsRepository != null)
                this.ExhibitionOtherEventsRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionOtherEvents GetExhibitionOtherEventsByItemId(long itemId)
        {
            using (ExhibitionOtherEventsRepository = new ExhibitionOtherEventsRepository(ConnectionString))
            {
                return ExhibitionOtherEventsRepository.GetById(itemId);
            }
        }

        public dynamic GetExhibitionOtherEvents()
        {
            using (ExhibitionOtherEventsRepository = new ExhibitionOtherEventsRepository(ConnectionString))
            {
                return ExhibitionOtherEventsRepository.Select();
            }
        }
        public List<ExhibitionDashboard> GetExhibitionDashboard()
        {
            using (ExhibitionOtherEventsRepository = new ExhibitionOtherEventsRepository(ConnectionString))
            {
                return ExhibitionOtherEventsRepository.SelectDashBoard();
            }
        }
        public List<XsiExhibitionOtherEvents> GetExhibitionOtherEvents(EnumSortlistBy sortListBy)
        {
            using (ExhibitionOtherEventsRepository = new ExhibitionOtherEventsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionOtherEventsRepository.Select().OrderBy(p => p.ExhibitionNumber).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionOtherEventsRepository.Select().OrderByDescending(p => p.ExhibitionNumber).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionOtherEventsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionOtherEventsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionOtherEventsRepository.Select().OrderBy(p => p.ExhibitionId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionOtherEventsRepository.Select().OrderByDescending(p => p.ExhibitionId).ToList();

                    default:
                        return ExhibitionOtherEventsRepository.Select().OrderByDescending(p => p.ExhibitionId).ToList();
                }
            }
        }
        public List<XsiExhibitionOtherEvents> GetExhibitionOtherEvents(XsiExhibitionOtherEvents entity)
        {
            using (ExhibitionOtherEventsRepository = new ExhibitionOtherEventsRepository(ConnectionString))
            {
                return ExhibitionOtherEventsRepository.Select(entity);
            }
        }
        public List<XsiExhibitionOtherEvents> GetExhibitionOtherEvents(XsiExhibitionOtherEvents entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionOtherEventsRepository = new ExhibitionOtherEventsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionOtherEventsRepository.Select(entity).OrderBy(p => p.ExhibitionNumber).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionOtherEventsRepository.Select(entity).OrderByDescending(p => p.ExhibitionNumber).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionOtherEventsRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionOtherEventsRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionOtherEventsRepository.Select(entity).OrderBy(p => p.ExhibitionId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionOtherEventsRepository.Select(entity).OrderByDescending(p => p.ExhibitionId).ToList();

                    default:
                        return ExhibitionOtherEventsRepository.Select(entity).OrderByDescending(p => p.ExhibitionId).ToList();
                }
            }
        }

        public EnumResultType InsertExhibitionOtherEvents(XsiExhibitionOtherEvents entity)
        {
            using (ExhibitionOtherEventsRepository = new ExhibitionOtherEventsRepository(ConnectionString))
            {
                entity = ExhibitionOtherEventsRepository.Add(entity);
                if (entity.ExhibitionId > 0)
                {
                    XsiItemdId = entity.ExhibitionId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionOtherEvents(XsiExhibitionOtherEvents entity)
        {
            XsiExhibitionOtherEvents OriginalEntity = GetExhibitionOtherEventsByItemId(entity.ExhibitionId);

            if (OriginalEntity != null)
            {
                using (ExhibitionOtherEventsRepository = new ExhibitionOtherEventsRepository(ConnectionString))
                {
                    ExhibitionOtherEventsRepository.Update(entity);
                    if (ExhibitionOtherEventsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionOtherEventsStatus(long itemId, long langId)
        {
            XsiExhibitionOtherEvents entity = GetExhibitionOtherEventsByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionOtherEventsRepository = new ExhibitionOtherEventsRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionOtherEventsRepository.Update(entity);
                    if (ExhibitionOtherEventsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionOtherEventsArchiveStatus(long itemId)
        {
            XsiExhibitionOtherEvents entity = GetExhibitionOtherEventsByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionOtherEventsRepository = new ExhibitionOtherEventsRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    entity.IsArchive = EnumConversion.ToString(EnumBool.Yes) == entity.IsArchive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionOtherEventsRepository.Update(entity);
                    if (ExhibitionOtherEventsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionOtherEvents(XsiExhibitionOtherEvents entity)
        {
            List<XsiExhibitionOtherEvents> entities = GetExhibitionOtherEvents(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionOtherEvents e in entities)
                {
                    result = DeleteExhibitionOtherEvents(e.ExhibitionId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionOtherEvents(long itemId)
        {
            XsiExhibitionOtherEvents entity = GetExhibitionOtherEventsByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionOtherEventsRepository = new ExhibitionOtherEventsRepository(ConnectionString))
                {
                    ExhibitionOtherEventsRepository.Delete(itemId);
                    if (ExhibitionOtherEventsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
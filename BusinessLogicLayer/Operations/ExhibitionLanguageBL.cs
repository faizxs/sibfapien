﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionLanguageBL : IDisposable
    {
        #region Feilds
        IExhibitionLanguageRepository ExhibitionLanguageRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionLanguageBL()
        {
            ConnectionString = null;
        }
        public ExhibitionLanguageBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionLanguageRepository != null)
                this.ExhibitionLanguageRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionLanguage GetExhibitionLanguageByItemId(long itemId)
        {
            using (ExhibitionLanguageRepository = new ExhibitionLanguageRepository(ConnectionString))
            {
                return ExhibitionLanguageRepository.GetById(itemId);
            }
        }
         
        public List<XsiExhibitionLanguage> GetExhibitionLanguage()
        {
            using (ExhibitionLanguageRepository = new ExhibitionLanguageRepository(ConnectionString))
            {
                return ExhibitionLanguageRepository.Select();
            }
        }
        public List<XsiExhibitionLanguage> GetExhibitionLanguage(EnumSortlistBy sortListBy)
        {
            using (ExhibitionLanguageRepository = new ExhibitionLanguageRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionLanguageRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionLanguageRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionLanguageRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionLanguageRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionLanguageRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionLanguageRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionLanguageRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionLanguage> GetExhibitionLanguage(XsiExhibitionLanguage entity)
        {
            using (ExhibitionLanguageRepository = new ExhibitionLanguageRepository(ConnectionString))
            {
                return ExhibitionLanguageRepository.Select(entity);
            }
        }
        public List<XsiExhibitionLanguage> GetExhibitionLanguage(XsiExhibitionLanguage entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionLanguageRepository = new ExhibitionLanguageRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionLanguageRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionLanguageRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionLanguageRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionLanguageRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionLanguageRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionLanguageRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionLanguageRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiExhibitionLanguage> GetExhibitionLanguageOr(XsiExhibitionLanguage entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionLanguageRepository = new ExhibitionLanguageRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionLanguageRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionLanguageRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionLanguageRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionLanguageRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionLanguageRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionLanguageRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionLanguageRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
     

        public EnumResultType InsertExhibitionLanguage(XsiExhibitionLanguage entity)
        {
            using (ExhibitionLanguageRepository = new ExhibitionLanguageRepository(ConnectionString))
            {
                entity = ExhibitionLanguageRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionLanguage(XsiExhibitionLanguage entity)
        {
            XsiExhibitionLanguage OriginalEntity = GetExhibitionLanguageByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionLanguageRepository = new ExhibitionLanguageRepository(ConnectionString))
                {
                    ExhibitionLanguageRepository.Update(entity);
                    if (ExhibitionLanguageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
   
        public EnumResultType UpdateExhibitionLanguageStatus(long itemId)
        {
            XsiExhibitionLanguage entity = GetExhibitionLanguageByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionLanguageRepository = new ExhibitionLanguageRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionLanguageRepository.Update(entity);
                    if (ExhibitionLanguageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionLanguage(XsiExhibitionLanguage entity)
        {
            List<XsiExhibitionLanguage> entities = GetExhibitionLanguage(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionLanguage e in entities)
                {
                    result = DeleteExhibitionLanguage(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionLanguage(long itemId)
        {
            XsiExhibitionLanguage entity = GetExhibitionLanguageByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionLanguageRepository = new ExhibitionLanguageRepository(ConnectionString))
                {
                    ExhibitionLanguageRepository.Delete(itemId);
                    if (ExhibitionLanguageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
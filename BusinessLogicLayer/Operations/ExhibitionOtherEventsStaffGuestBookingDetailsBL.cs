﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionOtherEventsStaffGuestBookingDetailsBL : IDisposable
    {
        #region Fields
        IExhibitionOtherEventsStaffGuestBookingDetailsRepository IExhibitionOtherEventsStaffGuestBookingDetailsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionOtherEventsStaffGuestBookingDetailsBL()
        {
            ConnectionString = null;
        }
        public ExhibitionOtherEventsStaffGuestBookingDetailsBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionOtherEventsStaffGuestBookingDetailsRepository != null)
                this.IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionOtherEventsStaffGuestBookingDetailsRepository = new ExhibitionOtherEventsStaffGuestBookingDetailRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionOtherEventsStaffGuestBookingDetails GetExhibitionOtherEventsStaffGuestBookingDetailsByItemId(long itemId)
        {
            using (IExhibitionOtherEventsStaffGuestBookingDetailsRepository = new ExhibitionOtherEventsStaffGuestBookingDetailRepository(ConnectionString))
            {
                return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.GetById(itemId);
            }
        }

        public List<XsiSgbdto> GetExhibitionOtherEventsStaffGuestBookingListForMember(long memberid, long ExhibitionOtherEventsid, string bookingtype)
        {
            using (IExhibitionOtherEventsStaffGuestBookingDetailsRepository = new ExhibitionOtherEventsStaffGuestBookingDetailRepository(ConnectionString))
            {
                return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.SelectBookingListForMember(memberid, ExhibitionOtherEventsid, bookingtype);
            }
        }


        public List<XsiExhibitionOtherEventsStaffGuestBookingDetails> GetExhibitionOtherEventsStaffGuestBookingDetails()
        {
            using (IExhibitionOtherEventsStaffGuestBookingDetailsRepository = new ExhibitionOtherEventsStaffGuestBookingDetailRepository(ConnectionString))
            {
                return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Select();
            }
        }
        public List<XsiExhibitionOtherEventsStaffGuestBookingDetails> GetExhibitionOtherEventsStaffGuestBookingDetails(EnumSortlistBy sortListBy)
        {
            using (IExhibitionOtherEventsStaffGuestBookingDetailsRepository = new ExhibitionOtherEventsStaffGuestBookingDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionOtherEventsStaffGuestBookingDetails> GetExhibitionOtherEventsStaffGuestBookingDetails(XsiExhibitionOtherEventsStaffGuestBookingDetails entity)
        {
            using (IExhibitionOtherEventsStaffGuestBookingDetailsRepository = new ExhibitionOtherEventsStaffGuestBookingDetailRepository(ConnectionString))
            {
                return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Select(entity);
            }
        }

        public List<XsiExhibitionOtherEventsStaffGuestBookingDetails> GetExhibitionOtherEventsStaffGuestBookingDetails(XsiExhibitionOtherEventsStaffGuestBookingDetails entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionOtherEventsStaffGuestBookingDetailsRepository = new ExhibitionOtherEventsStaffGuestBookingDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionOtherEventsStaffGuestBookingDetails GetExhibitionOtherEventsStaffGuestBookingDetails(long groupId, long languageId)
        {
            using (IExhibitionOtherEventsStaffGuestBookingDetailsRepository = new ExhibitionOtherEventsStaffGuestBookingDetailRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionOtherEventsStaffGuestBookingDetails(XsiExhibitionOtherEventsStaffGuestBookingDetails entity)
        {
            using (IExhibitionOtherEventsStaffGuestBookingDetailsRepository = new ExhibitionOtherEventsStaffGuestBookingDetailRepository(ConnectionString))
            {
                entity = IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuestBookingDetails(XsiExhibitionOtherEventsStaffGuestBookingDetails entity)
        {
            var where = new XsiExhibitionOtherEventsStaffGuestBookingDetails();
            where.ItemId = entity.ItemId;
            XsiExhibitionOtherEventsStaffGuestBookingDetails OriginalEntity = GetExhibitionOtherEventsStaffGuestBookingDetails(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

            if (OriginalEntity != null)
            {
                using (IExhibitionOtherEventsStaffGuestBookingDetailsRepository = new ExhibitionOtherEventsStaffGuestBookingDetailRepository(ConnectionString))
                {

                    IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Update(entity);
                    if (IExhibitionOtherEventsStaffGuestBookingDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackExhibitionOtherEventsStaffGuestBookingDetails(long itemId)
        {
            //if no RollbackXml propery in ExhibitionOtherEventsStaffGuestBookingDetails then delete below code and return EnumResultType.Invalid;

            return EnumResultType.Failed;

        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuestBookingDetailsStatus(long itemId)
        {
            XsiExhibitionOtherEventsStaffGuestBookingDetails entity = GetExhibitionOtherEventsStaffGuestBookingDetailsByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionOtherEventsStaffGuestBookingDetailsRepository = new ExhibitionOtherEventsStaffGuestBookingDetailRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Update(entity);
                    if (IExhibitionOtherEventsStaffGuestBookingDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuestBookingDetails(XsiExhibitionOtherEventsStaffGuestBookingDetails entity)
        {
            List<XsiExhibitionOtherEventsStaffGuestBookingDetails> entities = GetExhibitionOtherEventsStaffGuestBookingDetails(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionOtherEventsStaffGuestBookingDetails e in entities)
                {
                    result = DeleteExhibitionOtherEventsStaffGuestBookingDetails(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuestBookingDetails(long itemId)
        {
            var where = new XsiExhibitionOtherEventsStaffGuestBookingDetails();
            where.ItemId = itemId;
            XsiExhibitionOtherEventsStaffGuestBookingDetails entity = GetExhibitionOtherEventsStaffGuestBookingDetails(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            if (entity != null)
            {
                using (IExhibitionOtherEventsStaffGuestBookingDetailsRepository = new ExhibitionOtherEventsStaffGuestBookingDetailRepository(ConnectionString))
                {
                    IExhibitionOtherEventsStaffGuestBookingDetailsRepository.Delete(itemId);
                    if (IExhibitionOtherEventsStaffGuestBookingDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
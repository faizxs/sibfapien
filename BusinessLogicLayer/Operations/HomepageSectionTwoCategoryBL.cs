﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class HomepageSectionTwoCategoryBL : IDisposable
    {
        #region Feilds
        IHomepageSectionTwoCategoryRepository HomepageSectionTwoCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public HomepageSectionTwoCategoryBL()
        {
            ConnectionString = null;
        }
        public HomepageSectionTwoCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomepageSectionTwoCategoryRepository != null)
                this.HomepageSectionTwoCategoryRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiHomepageSectionTwoCategory GetHomepageSectionTwoCategoryByItemId(long itemId)
        {
            using (HomepageSectionTwoCategoryRepository = new HomepageSectionTwoCategoryRepository(ConnectionString))
            {
                return HomepageSectionTwoCategoryRepository.GetById(itemId);
            }
        }

        public List<XsiHomepageSectionTwoCategory> GetHomepageSectionTwoCategory()
        {
            using (HomepageSectionTwoCategoryRepository = new HomepageSectionTwoCategoryRepository(ConnectionString))
            {
                return HomepageSectionTwoCategoryRepository.Select();
            }
        }
        public List<XsiHomepageSectionTwoCategory> GetHomepageSectionTwoCategory(EnumSortlistBy sortListBy)
        {
            using (HomepageSectionTwoCategoryRepository = new HomepageSectionTwoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageSectionTwoCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageSectionTwoCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HomepageSectionTwoCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HomepageSectionTwoCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageSectionTwoCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageSectionTwoCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HomepageSectionTwoCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiHomepageSectionTwoCategory> GetHomepageSectionTwoCategory(XsiHomepageSectionTwoCategory entity)
        {
            using (HomepageSectionTwoCategoryRepository = new HomepageSectionTwoCategoryRepository(ConnectionString))
            {
                return HomepageSectionTwoCategoryRepository.Select(entity);
            }
        }
        public List<XsiHomepageSectionTwoCategory> GetHomepageSectionTwoCategory(XsiHomepageSectionTwoCategory entity, EnumSortlistBy sortListBy)
        {
            using (HomepageSectionTwoCategoryRepository = new HomepageSectionTwoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageSectionTwoCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageSectionTwoCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HomepageSectionTwoCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HomepageSectionTwoCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageSectionTwoCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageSectionTwoCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HomepageSectionTwoCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiHomepageSectionTwoCategory GetHomepageSectionTwoCategory(long Id)
        {
            using (HomepageSectionTwoCategoryRepository = new HomepageSectionTwoCategoryRepository(ConnectionString))
            {
                return HomepageSectionTwoCategoryRepository.Select(new XsiHomepageSectionTwoCategory() { ItemId = Id }).FirstOrDefault();
            }
        }
        

        public EnumResultType InsertHomepageSectionTwoCategory(XsiHomepageSectionTwoCategory entity)
        {
            using (HomepageSectionTwoCategoryRepository = new HomepageSectionTwoCategoryRepository(ConnectionString))
            {
                entity = HomepageSectionTwoCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateHomepageSectionTwoCategory(XsiHomepageSectionTwoCategory entity)
        {
            XsiHomepageSectionTwoCategory OriginalEntity = GetHomepageSectionTwoCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (HomepageSectionTwoCategoryRepository = new HomepageSectionTwoCategoryRepository(ConnectionString))
                {
                    HomepageSectionTwoCategoryRepository.Update(entity);
                    if (HomepageSectionTwoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateHomepageSectionTwoCategoryStatus(long itemId)
        {
            XsiHomepageSectionTwoCategory entity = GetHomepageSectionTwoCategoryByItemId(itemId);
            if (entity != null)
            {
                using (HomepageSectionTwoCategoryRepository = new HomepageSectionTwoCategoryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    HomepageSectionTwoCategoryRepository.Update(entity);
                    if (HomepageSectionTwoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteHomepageSectionTwoCategory(XsiHomepageSectionTwoCategory entity)
        {
            List<XsiHomepageSectionTwoCategory> entities = GetHomepageSectionTwoCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiHomepageSectionTwoCategory e in entities)
                {
                    result = DeleteHomepageSectionTwoCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteHomepageSectionTwoCategory(long itemId)
        {
            XsiHomepageSectionTwoCategory entity = GetHomepageSectionTwoCategoryByItemId(itemId);
            if (entity != null)
            {
                using (HomepageSectionTwoCategoryRepository = new HomepageSectionTwoCategoryRepository(ConnectionString))
                {
                    HomepageSectionTwoCategoryRepository.Delete(itemId);
                    if (HomepageSectionTwoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class CareerSubSpecialityBL : IDisposable
    {
        #region Feilds
        ICareerSubSpecialityRepository CareerSubSpecialityRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public CareerSubSpecialityBL()
        {
            ConnectionString = null;
        }
        public CareerSubSpecialityBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerSubSpecialityRepository != null)
                this.CareerSubSpecialityRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiCareerSubSpeciality GetCareerSubSpecialityByItemId(long itemId)
        {
            using (CareerSubSpecialityRepository = new CareerSubSpecialityRepository(ConnectionString))
            {
                return CareerSubSpecialityRepository.GetById(itemId);
            }
        }
         
        public List<XsiCareerSubSpeciality> GetCareerSubSpeciality()
        {
            using (CareerSubSpecialityRepository = new CareerSubSpecialityRepository(ConnectionString))
            {
                return CareerSubSpecialityRepository.Select();
            }
        }
        public List<XsiCareerSubSpeciality> GetCareerSubSpeciality(EnumSortlistBy sortListBy)
        {
            using (CareerSubSpecialityRepository = new CareerSubSpecialityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerSubSpecialityRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerSubSpecialityRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerSubSpecialityRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerSubSpecialityRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerSubSpecialityRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerSubSpecialityRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerSubSpecialityRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareerSubSpeciality> GetCareerSubSpeciality(XsiCareerSubSpeciality entity)
        {
            using (CareerSubSpecialityRepository = new CareerSubSpecialityRepository(ConnectionString))
            {
                return CareerSubSpecialityRepository.Select(entity);
            }
        }
        public List<XsiCareerSubSpeciality> GetCareerSubSpeciality(XsiCareerSubSpeciality entity, EnumSortlistBy sortListBy)
        {
            using (CareerSubSpecialityRepository = new CareerSubSpecialityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerSubSpecialityRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerSubSpecialityRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerSubSpecialityRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerSubSpecialityRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerSubSpecialityRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerSubSpecialityRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerSubSpecialityRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
         
        public EnumResultType InsertCareerSubSpeciality(XsiCareerSubSpeciality entity)
        {
            using (CareerSubSpecialityRepository = new CareerSubSpecialityRepository(ConnectionString))
            {
                entity = CareerSubSpecialityRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateCareerSubSpeciality(XsiCareerSubSpeciality entity)
        {
            XsiCareerSubSpeciality OriginalEntity = GetCareerSubSpecialityByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (CareerSubSpecialityRepository = new CareerSubSpecialityRepository(ConnectionString))
                {
                    CareerSubSpecialityRepository.Update(entity);
                    if (CareerSubSpecialityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateCareerSubSpecialityStatus(long itemId)
        {
            XsiCareerSubSpeciality entity = GetCareerSubSpecialityByItemId(itemId);
            if (entity != null)
            {
                using (CareerSubSpecialityRepository = new CareerSubSpecialityRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    CareerSubSpecialityRepository.Update(entity);
                    if (CareerSubSpecialityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteCareerSubSpeciality(XsiCareerSubSpeciality entity)
        {
            List<XsiCareerSubSpeciality> entities = GetCareerSubSpeciality(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiCareerSubSpeciality e in entities)
                {
                    result = DeleteCareerSubSpeciality(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteCareerSubSpeciality(long itemId)
        {
            XsiCareerSubSpeciality entity = GetCareerSubSpecialityByItemId(itemId);
            if (entity != null)
            {
                using (CareerSubSpecialityRepository = new CareerSubSpecialityRepository(ConnectionString))
                {
                    CareerSubSpecialityRepository.Delete(itemId);
                    if (CareerSubSpecialityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
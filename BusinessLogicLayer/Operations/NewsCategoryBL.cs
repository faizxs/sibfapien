﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class NewsCategoryBL : IDisposable
    {
        #region Feilds
        INewsCategoryRepository NewsCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public NewsCategoryBL()
        {
            ConnectionString = null;
        }
        public NewsCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.NewsCategoryRepository != null)
                this.NewsCategoryRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiNewsCategory GetNewsCategoryByItemId(long itemId)
        {
            using (NewsCategoryRepository = new NewsCategoryRepository(ConnectionString))
            {
                return NewsCategoryRepository.GetById(itemId);
            }
        }
         
        public List<XsiNewsCategory> GetNewsCategory()
        {
            using (NewsCategoryRepository = new NewsCategoryRepository(ConnectionString))
            {
                return NewsCategoryRepository.Select();
            }
        }
        public List<XsiNewsCategory> GetNewsCategory(EnumSortlistBy sortListBy)
        {
            using (NewsCategoryRepository = new NewsCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    ////case EnumSortlistBy.ByDateAsc:
                    ////    return NewsCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    ////case EnumSortlistBy.ByDateDesc:
                    ////    return NewsCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiNewsCategory> GetNewsCategory(XsiNewsCategory entity)
        {
            using (NewsCategoryRepository = new NewsCategoryRepository(ConnectionString))
            {
                return NewsCategoryRepository.Select(entity);
            }
        }
        public List<XsiNewsCategory> GetNewsCategory(XsiNewsCategory entity, EnumSortlistBy sortListBy)
        {
            using (NewsCategoryRepository = new NewsCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    //case EnumSortlistBy.ByDateAsc:
                    //    return NewsCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    //case EnumSortlistBy.ByDateDesc:
                    //    return NewsCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiNewsCategory> GetNewsCategoryOr(XsiNewsCategory entity, EnumSortlistBy sortListBy)
        {
            using (NewsCategoryRepository = new NewsCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsCategoryRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsCategoryRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    //case EnumSortlistBy.ByDateAsc:
                    //    return NewsCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    //case EnumSortlistBy.ByDateDesc:
                    //    return NewsCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsCategoryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public List<XsiNewsCategory> GetOtherLanguageNewsCategory(XsiNewsCategory entity)
        {
            using (NewsCategoryRepository = new NewsCategoryRepository(ConnectionString))
            {
                return NewsCategoryRepository.SelectOtherLanguageCategory(entity);
            }
        }
        public List<XsiNewsCategory> GetCurrentLanguageNewsCategory(XsiNewsCategory entity)
        {
            using (NewsCategoryRepository = new NewsCategoryRepository(ConnectionString))
            {
                return NewsCategoryRepository.SelectCurrentLanguageCategory(entity);
            }
        }

        public EnumResultType InsertNewsCategory(XsiNewsCategory entity)
        {
            using (NewsCategoryRepository = new NewsCategoryRepository(ConnectionString))
            {
                entity = NewsCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;                   
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateNewsCategory(XsiNewsCategory entity)
        {
            XsiNewsCategory OriginalEntity = GetNewsCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (NewsCategoryRepository = new NewsCategoryRepository(ConnectionString))
                {
                    NewsCategoryRepository.Update(entity);
                    if (NewsCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateNewsCategoryStatus(long itemId)
        {
            XsiNewsCategory entity = GetNewsCategoryByItemId(itemId);
            if (entity != null)
            {
                using (NewsCategoryRepository = new NewsCategoryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    NewsCategoryRepository.Update(entity);
                    if (NewsCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteNewsCategory(XsiNewsCategory entity)
        {
            List<XsiNewsCategory> entities = GetNewsCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiNewsCategory e in entities)
                {
                    result = DeleteNewsCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteNewsCategory(long itemId)
        {
            XsiNewsCategory entity = GetNewsCategoryByItemId(itemId);
            if (entity != null)
            {
                using (NewsCategoryRepository = new NewsCategoryRepository(ConnectionString))
                {
                    //delete news with news catefory id = itemid

                    NewsCategoryRepository.Delete(itemId);
                    if (NewsCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
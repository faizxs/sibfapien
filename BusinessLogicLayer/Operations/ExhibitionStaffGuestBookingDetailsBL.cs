﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionStaffGuestBookingDetailsBL : IDisposable
    {
        #region Fields
        IExhibitionStaffGuestBookingDetailsRepository IExhibitionStaffGuestBookingDetailsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestBookingDetailsBL()
        {
            ConnectionString = null;
        }
        public ExhibitionStaffGuestBookingDetailsBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionStaffGuestBookingDetailsRepository != null)
                this.IExhibitionStaffGuestBookingDetailsRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionStaffGuestBookingDetailsRepository = new ExhibitionStaffGuestBookingDetailRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionStaffGuestBookingDetails GetExhibitionStaffGuestBookingDetailsByItemId(long itemId)
        {
            using (IExhibitionStaffGuestBookingDetailsRepository = new ExhibitionStaffGuestBookingDetailRepository(ConnectionString))
            {
                return IExhibitionStaffGuestBookingDetailsRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionStaffGuestBookingDetails> GetExhibitionStaffGuestBookingDetails()
        {
            using (IExhibitionStaffGuestBookingDetailsRepository = new ExhibitionStaffGuestBookingDetailRepository(ConnectionString))
            {
                return IExhibitionStaffGuestBookingDetailsRepository.Select();
            }
        }
        public List<XsiExhibitionStaffGuestBookingDetails> GetExhibitionStaffGuestBookingDetails(EnumSortlistBy sortListBy)
        {
            using (IExhibitionStaffGuestBookingDetailsRepository = new ExhibitionStaffGuestBookingDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionStaffGuestBookingDetailsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionStaffGuestBookingDetailsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionStaffGuestBookingDetailsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionStaffGuestBookingDetailsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionStaffGuestBookingDetailsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionStaffGuestBookingDetails> GetExhibitionStaffGuestBookingDetails(XsiExhibitionStaffGuestBookingDetails entity)
        {
            using (IExhibitionStaffGuestBookingDetailsRepository = new ExhibitionStaffGuestBookingDetailRepository(ConnectionString))
            {
                return IExhibitionStaffGuestBookingDetailsRepository.Select(entity);
            }
        }
       
        public List<XsiExhibitionStaffGuestBookingDetails> GetExhibitionStaffGuestBookingDetails(XsiExhibitionStaffGuestBookingDetails entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionStaffGuestBookingDetailsRepository = new ExhibitionStaffGuestBookingDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionStaffGuestBookingDetailsRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionStaffGuestBookingDetailsRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionStaffGuestBookingDetailsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionStaffGuestBookingDetailsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionStaffGuestBookingDetailsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionStaffGuestBookingDetails GetExhibitionStaffGuestBookingDetails(long groupId, long languageId)
        {
            using (IExhibitionStaffGuestBookingDetailsRepository = new ExhibitionStaffGuestBookingDetailRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionStaffGuestBookingDetails(XsiExhibitionStaffGuestBookingDetails entity)
        {
            using (IExhibitionStaffGuestBookingDetailsRepository = new ExhibitionStaffGuestBookingDetailRepository(ConnectionString))
            {
                entity = IExhibitionStaffGuestBookingDetailsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionStaffGuestBookingDetails(XsiExhibitionStaffGuestBookingDetails entity)
        {
            var where = new XsiExhibitionStaffGuestBookingDetails();
            where.ItemId = entity.ItemId;
            XsiExhibitionStaffGuestBookingDetails OriginalEntity = GetExhibitionStaffGuestBookingDetails(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

            if (OriginalEntity != null)
            {
                using (IExhibitionStaffGuestBookingDetailsRepository = new ExhibitionStaffGuestBookingDetailRepository(ConnectionString))
                {

                    IExhibitionStaffGuestBookingDetailsRepository.Update(entity);
                    if (IExhibitionStaffGuestBookingDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackExhibitionStaffGuestBookingDetails(long itemId)
        {
            //if no RollbackXml propery in ExhibitionStaffGuestBookingDetails then delete below code and return EnumResultType.Invalid;

            return EnumResultType.Failed;

        }
        public EnumResultType UpdateExhibitionStaffGuestBookingDetailsStatus(long itemId)
        {
            XsiExhibitionStaffGuestBookingDetails entity = GetExhibitionStaffGuestBookingDetailsByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionStaffGuestBookingDetailsRepository = new ExhibitionStaffGuestBookingDetailRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionStaffGuestBookingDetailsRepository.Update(entity);
                    if (IExhibitionStaffGuestBookingDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionStaffGuestBookingDetails(XsiExhibitionStaffGuestBookingDetails entity)
        {
            List<XsiExhibitionStaffGuestBookingDetails> entities = GetExhibitionStaffGuestBookingDetails(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionStaffGuestBookingDetails e in entities)
                {
                    result = DeleteExhibitionStaffGuestBookingDetails(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionStaffGuestBookingDetails(long itemId)
        {
            var where = new XsiExhibitionStaffGuestBookingDetails();
            where.ItemId = itemId;
            XsiExhibitionStaffGuestBookingDetails entity = GetExhibitionStaffGuestBookingDetails(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            if (entity != null)
            {
                using (IExhibitionStaffGuestBookingDetailsRepository = new ExhibitionStaffGuestBookingDetailRepository(ConnectionString))
                {
                    IExhibitionStaffGuestBookingDetailsRepository.Delete(itemId);
                    if (IExhibitionStaffGuestBookingDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFCountryBL : IDisposable
    {
        #region Feilds
        ISCRFCountryRepository SCRFCountryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFCountryBL()
        {
            ConnectionString = null;
        }
        public SCRFCountryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFCountryRepository != null)
                this.SCRFCountryRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrfcountry GetSCRFCountryByItemId(long itemId)
        {
            using (SCRFCountryRepository = new SCRFCountryRepository(ConnectionString))
            {
                return SCRFCountryRepository.GetById(itemId);
            }
        }

        public List<XsiScrfcountry> GetSCRFCountry()
        {
            using (SCRFCountryRepository = new SCRFCountryRepository(ConnectionString))
            {
                return SCRFCountryRepository.Select();
            }
        }
        public List<XsiScrfcountry> GetSCRFCountry(EnumSortlistBy sortListBy)
        {
            using (SCRFCountryRepository = new SCRFCountryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFCountryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFCountryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFCountryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFCountryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFCountryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFCountryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFCountryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfcountry> GetSCRFCountry(XsiScrfcountry entity)
        {
            using (SCRFCountryRepository = new SCRFCountryRepository(ConnectionString))
            {
                return SCRFCountryRepository.Select(entity);
            }
        }
        public List<XsiScrfcountry> GetSCRFCountry(XsiScrfcountry entity, EnumSortlistBy sortListBy)
        {
            using (SCRFCountryRepository = new SCRFCountryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFCountryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFCountryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFCountryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFCountryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFCountryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFCountryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFCountryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfcountry> GetSCRFCountryOr(XsiScrfcountry entity, EnumSortlistBy sortListBy)
        {
            using (SCRFCountryRepository = new SCRFCountryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFCountryRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFCountryRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFCountryRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFCountryRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFCountryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFCountryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFCountryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfcountry> GetOtherLanguageSCRFCountry(XsiScrfcountry entity)
        {
            using (SCRFCountryRepository = new SCRFCountryRepository(ConnectionString))
            {
                return SCRFCountryRepository.SelectOtherLanguageCategory(entity);
            }
        }
        public List<XsiScrfcountry> GetCurrentLanguageSCRFCountry(XsiScrfcountry entity)
        {
            using (SCRFCountryRepository = new SCRFCountryRepository(ConnectionString))
            {
                return SCRFCountryRepository.SelectCurrentLanguageCategory(entity);
            }
        }
        public List<XsiScrfcountry> GetCompleteSCRFCountry(XsiScrfcountry entity, EnumSortlistBy sortListBy)
        {
            using (SCRFCountryRepository = new SCRFCountryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFCountryRepository.SelectComplete(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFCountryRepository.SelectComplete(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFCountryRepository.SelectComplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFCountryRepository.SelectComplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFCountryRepository.SelectComplete(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFCountryRepository.SelectComplete(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFCountryRepository.SelectComplete(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFCountry(XsiScrfcountry entity)
        {
            using (SCRFCountryRepository = new SCRFCountryRepository(ConnectionString))
            {
                entity = SCRFCountryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFCountry(XsiScrfcountry entity)
        {
            XsiScrfcountry OriginalEntity = GetSCRFCountryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFCountryRepository = new SCRFCountryRepository(ConnectionString))
                {
                    SCRFCountryRepository.Update(entity);
                    if (SCRFCountryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFCountryStatus(long itemId)
        {
            XsiScrfcountry entity = GetSCRFCountryByItemId(itemId);
            if (entity != null)
            {
                using (SCRFCountryRepository = new SCRFCountryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFCountryRepository.Update(entity);
                    if (SCRFCountryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFCountry(XsiScrfcountry entity)
        {
            List<XsiScrfcountry> entities = GetSCRFCountry(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfcountry e in entities)
                {
                    result = DeleteSCRFCountry(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFCountry(long itemId)
        {
            XsiScrfcountry entity = GetSCRFCountryByItemId(itemId);
            if (entity != null)
            {
                using (SCRFCountryRepository = new SCRFCountryRepository(ConnectionString))
                {
                    SCRFCountryRepository.Delete(itemId);
                    if (SCRFCountryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class CareerDegreeBL : IDisposable
    {
        #region Feilds
        ICareerDegreesRepository CareerDegreesRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public CareerDegreeBL()
        {
            ConnectionString = null;
        }
        public CareerDegreeBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerDegreesRepository != null)
                this.CareerDegreesRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiCareerDegrees GetCareerDegreeByItemId(long itemId)
        {
            using (CareerDegreesRepository = new CareerDegreesRepository(ConnectionString))
            {
                return CareerDegreesRepository.GetById(itemId);
            }
        }
        
        public List<XsiCareerDegrees> GetCareerDegree()
        {
            using (CareerDegreesRepository = new CareerDegreesRepository(ConnectionString))
            {
                return CareerDegreesRepository.Select();
            }
        }
        public List<XsiCareerDegrees> GetCareerDegree(EnumSortlistBy sortListBy)
        {
            using (CareerDegreesRepository = new CareerDegreesRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerDegreesRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerDegreesRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerDegreesRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerDegreesRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerDegreesRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerDegreesRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerDegreesRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareerDegrees> GetCareerDegree(XsiCareerDegrees entity)
        {
            using (CareerDegreesRepository = new CareerDegreesRepository(ConnectionString))
            {
                return CareerDegreesRepository.Select(entity);
            }
        }
        public List<XsiCareerDegrees> GetCareerDegree(XsiCareerDegrees entity, EnumSortlistBy sortListBy)
        {
            using (CareerDegreesRepository = new CareerDegreesRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerDegreesRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerDegreesRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerDegreesRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerDegreesRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerDegreesRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerDegreesRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerDegreesRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareerDegrees> GetCareerDegreeOr(XsiCareerDegrees entity, EnumSortlistBy sortListBy)
        {
            using (CareerDegreesRepository = new CareerDegreesRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerDegreesRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerDegreesRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerDegreesRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerDegreesRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerDegreesRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerDegreesRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerDegreesRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public EnumResultType InsertCareerDegree(XsiCareerDegrees entity)
        {
            using (CareerDegreesRepository = new CareerDegreesRepository(ConnectionString))
            {
                entity = CareerDegreesRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateCareerDegree(XsiCareerDegrees entity)
        {
            XsiCareerDegrees OriginalEntity = GetCareerDegreeByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (CareerDegreesRepository = new CareerDegreesRepository(ConnectionString))
                {
                    CareerDegreesRepository.Update(entity);
                    if (CareerDegreesRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateCareerDegreeStatus(long itemId)
        {
            XsiCareerDegrees entity = GetCareerDegreeByItemId(itemId);
            if (entity != null)
            {
                using (CareerDegreesRepository = new CareerDegreesRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    CareerDegreesRepository.Update(entity);
                    if (CareerDegreesRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteCareerDegree(XsiCareerDegrees entity)
        {
            List<XsiCareerDegrees> entities = GetCareerDegree(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiCareerDegrees e in entities)
                {
                    result = DeleteCareerDegree(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteCareerDegree(long itemId)
        {
            XsiCareerDegrees entity = GetCareerDegreeByItemId(itemId);
            if (entity != null)
            {
                using (CareerDegreesRepository = new CareerDegreesRepository(ConnectionString))
                {
                    CareerDegreesRepository.Delete(itemId);
                    if (CareerDegreesRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
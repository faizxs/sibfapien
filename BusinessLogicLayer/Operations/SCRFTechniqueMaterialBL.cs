﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFTechniqueMaterialBL : IDisposable
    {
        #region Feilds
        ISCRFTechniqueMaterialRepository SCRFTechniqueMaterialRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFTechniqueMaterialBL()
        {
            ConnectionString = null;
        }
        public SCRFTechniqueMaterialBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFTechniqueMaterialRepository != null)
                this.SCRFTechniqueMaterialRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrftechniqueMaterial GetSCRFTechniqueMaterialByItemId(long itemId)
        {
            using (SCRFTechniqueMaterialRepository = new SCRFTechniqueMaterialRepository(ConnectionString))
            {
                return SCRFTechniqueMaterialRepository.GetById(itemId);
            }
        }

        public List<XsiScrftechniqueMaterial> GetSCRFTechniqueMaterial()
        {
            using (SCRFTechniqueMaterialRepository = new SCRFTechniqueMaterialRepository(ConnectionString))
            {
                return SCRFTechniqueMaterialRepository.Select();
            }
        }
        public List<XsiScrftechniqueMaterial> GetSCRFTechniqueMaterial(EnumSortlistBy sortListBy)
        {
            using (SCRFTechniqueMaterialRepository = new SCRFTechniqueMaterialRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFTechniqueMaterialRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFTechniqueMaterialRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFTechniqueMaterialRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFTechniqueMaterialRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFTechniqueMaterialRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFTechniqueMaterialRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFTechniqueMaterialRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrftechniqueMaterial> GetSCRFTechniqueMaterial(XsiScrftechniqueMaterial entity)
        {
            using (SCRFTechniqueMaterialRepository = new SCRFTechniqueMaterialRepository(ConnectionString))
            {
                return SCRFTechniqueMaterialRepository.Select(entity);
            }
        }
        public List<XsiScrftechniqueMaterial> GetSCRFTechniqueMaterial(XsiScrftechniqueMaterial entity, EnumSortlistBy sortListBy)
        {
            using (SCRFTechniqueMaterialRepository = new SCRFTechniqueMaterialRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFTechniqueMaterialRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFTechniqueMaterialRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFTechniqueMaterialRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFTechniqueMaterialRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFTechniqueMaterialRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFTechniqueMaterialRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFTechniqueMaterialRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertSCRFTechniqueMaterial(XsiScrftechniqueMaterial entity)
        {
            using (SCRFTechniqueMaterialRepository = new SCRFTechniqueMaterialRepository(ConnectionString))
            {
                entity = SCRFTechniqueMaterialRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFTechniqueMaterial(XsiScrftechniqueMaterial entity)
        {
            XsiScrftechniqueMaterial OriginalEntity = GetSCRFTechniqueMaterialByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFTechniqueMaterialRepository = new SCRFTechniqueMaterialRepository(ConnectionString))
                {
                    SCRFTechniqueMaterialRepository.Update(entity);
                    if (SCRFTechniqueMaterialRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFTechniqueMaterialStatus(long itemId)
        {
            XsiScrftechniqueMaterial entity = GetSCRFTechniqueMaterialByItemId(itemId);
            if (entity != null)
            {
                using (SCRFTechniqueMaterialRepository = new SCRFTechniqueMaterialRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFTechniqueMaterialRepository.Update(entity);
                    if (SCRFTechniqueMaterialRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFTechniqueMaterial(XsiScrftechniqueMaterial entity)
        {
            List<XsiScrftechniqueMaterial> entities = GetSCRFTechniqueMaterial(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrftechniqueMaterial e in entities)
                {
                    result = DeleteSCRFTechniqueMaterial(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFTechniqueMaterial(long itemId)
        {
            XsiScrftechniqueMaterial entity = GetSCRFTechniqueMaterialByItemId(itemId);
            if (entity != null)
            {
                using (SCRFTechniqueMaterialRepository = new SCRFTechniqueMaterialRepository(ConnectionString))
                {
                    SCRFTechniqueMaterialRepository.Delete(itemId);
                    if (SCRFTechniqueMaterialRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
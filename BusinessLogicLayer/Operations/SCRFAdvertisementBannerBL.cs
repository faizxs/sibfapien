﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFAdvertisementBannerBL : IDisposable
    {
        #region Feilds
        ISCRFAdvertisementBannerRepository SCRFAdvertisementBannerRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFAdvertisementBannerBL()
        {
            ConnectionString = null;
        }
        public SCRFAdvertisementBannerBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFAdvertisementBannerRepository != null)
                this.SCRFAdvertisementBannerRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrfadvertisementBanner GetSCRFAdvertisementBannerByItemId(long itemId)
        {
            using (SCRFAdvertisementBannerRepository = new SCRFAdvertisementBannerRepository(ConnectionString))
            {
                return SCRFAdvertisementBannerRepository.GetById(itemId);
            }
        }

        public List<XsiScrfadvertisementBanner> GetSCRFAdvertisementBanner()
        {
            using (SCRFAdvertisementBannerRepository = new SCRFAdvertisementBannerRepository(ConnectionString))
            {
                return SCRFAdvertisementBannerRepository.Select();
            }
        }
        public List<XsiScrfadvertisementBanner> GetSCRFAdvertisementBanner(EnumSortlistBy sortListBy)
        {
            using (SCRFAdvertisementBannerRepository = new SCRFAdvertisementBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFAdvertisementBannerRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFAdvertisementBannerRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFAdvertisementBannerRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFAdvertisementBannerRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFAdvertisementBannerRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFAdvertisementBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFAdvertisementBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfadvertisementBanner> GetSCRFAdvertisementBanner(XsiScrfadvertisementBanner entity)
        {
            using (SCRFAdvertisementBannerRepository = new SCRFAdvertisementBannerRepository(ConnectionString))
            {
                return SCRFAdvertisementBannerRepository.Select(entity);
            }
        }
        public List<XsiScrfadvertisementBanner> GetSCRFAdvertisementBanner(XsiScrfadvertisementBanner entity, EnumSortlistBy sortListBy)
        {
            using (SCRFAdvertisementBannerRepository = new SCRFAdvertisementBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFAdvertisementBannerRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFAdvertisementBannerRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFAdvertisementBannerRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFAdvertisementBannerRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFAdvertisementBannerRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFAdvertisementBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFAdvertisementBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfadvertisementBanner> GetSCRFAdvertisementBannerOr(XsiScrfadvertisementBanner entity, EnumSortlistBy sortListBy)
        {
            using (SCRFAdvertisementBannerRepository = new SCRFAdvertisementBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFAdvertisementBannerRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFAdvertisementBannerRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFAdvertisementBannerRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFAdvertisementBannerRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFAdvertisementBannerRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFAdvertisementBannerRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFAdvertisementBannerRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFAdvertisementBanner(XsiScrfadvertisementBanner entity)
        {
            using (SCRFAdvertisementBannerRepository = new SCRFAdvertisementBannerRepository(ConnectionString))
            {
                entity = SCRFAdvertisementBannerRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFAdvertisementBanner(XsiScrfadvertisementBanner entity)
        {
            XsiScrfadvertisementBanner OriginalEntity = GetSCRFAdvertisementBannerByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFAdvertisementBannerRepository = new SCRFAdvertisementBannerRepository(ConnectionString))
                {
                    SCRFAdvertisementBannerRepository.Update(entity);
                    if (SCRFAdvertisementBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackSCRFAdvertisementBanner(long itemId)
        {
            //if no RollbackXml propery in SCRFAdvertisementBanner then delete below code and return EnumResultType.Invalid;
                return EnumResultType.Invalid;
        }
        public EnumResultType UpdateSCRFAdvertisementBannerStatus(long itemId)
        {
            XsiScrfadvertisementBanner entity = GetSCRFAdvertisementBannerByItemId(itemId);
            if (entity != null)
            {
                using (SCRFAdvertisementBannerRepository = new SCRFAdvertisementBannerRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFAdvertisementBannerRepository.Update(entity);
                    if (SCRFAdvertisementBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFAdvertisementBanner(XsiScrfadvertisementBanner entity)
        {
            List<XsiScrfadvertisementBanner> entities = GetSCRFAdvertisementBanner(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfadvertisementBanner e in entities)
                {
                    result = DeleteSCRFAdvertisementBanner(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFAdvertisementBanner(long itemId)
        {
            XsiScrfadvertisementBanner entity = GetSCRFAdvertisementBannerByItemId(itemId);
            if (entity != null)
            {
                using (SCRFAdvertisementBannerRepository = new SCRFAdvertisementBannerRepository(ConnectionString))
                {
                    SCRFAdvertisementBannerRepository.Delete(itemId);
                    if (SCRFAdvertisementBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
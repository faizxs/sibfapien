﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitorActivityBL : IDisposable
    {
        #region Feilds
        IExhibitorActivityRepository ExhibitorActivityRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitorActivityBL()
        {
            ConnectionString = null;
        }
        public ExhibitorActivityBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitorActivityRepository != null)
                this.ExhibitorActivityRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (ExhibitorActivityRepository = new ExhibitorActivityRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiMemberExhibitionActivityYearly GetExhibitorActivityByItemId(long itemId)
        {
            using (ExhibitorActivityRepository = new ExhibitorActivityRepository(ConnectionString))
            {
                return ExhibitorActivityRepository.GetById(itemId);
            }
        }
         
        public List<XsiMemberExhibitionActivityYearly> GetExhibitorActivity()
        {
            using (ExhibitorActivityRepository = new ExhibitorActivityRepository(ConnectionString))
            {
                return ExhibitorActivityRepository.Select();
            }
        }
        public List<XsiMemberExhibitionActivityYearly> GetExhibitorActivity(EnumSortlistBy sortListBy)
        {
            using (ExhibitorActivityRepository = new ExhibitorActivityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitorActivityRepository.Select().OrderBy(p => p.MemberExhibitionYearlyId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitorActivityRepository.Select().OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();

                    default:
                        return ExhibitorActivityRepository.Select().OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();
                }
            }
        }
        public List<XsiMemberExhibitionActivityYearly> GetExhibitorActivity(XsiMemberExhibitionActivityYearly entity)
        {
            using (ExhibitorActivityRepository = new ExhibitorActivityRepository(ConnectionString))
            {
                return ExhibitorActivityRepository.Select(entity);
            }
        }
        public List<XsiMemberExhibitionActivityYearly> GetExhibitorActivity(XsiMemberExhibitionActivityYearly entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitorActivityRepository = new ExhibitorActivityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitorActivityRepository.Select(entity).OrderBy(p => p.MemberExhibitionYearlyId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitorActivityRepository.Select(entity).OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();

                    default:
                        return ExhibitorActivityRepository.Select(entity).OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();
                }
            }
        }

        public EnumResultType InsertExhibitorActivity(XsiMemberExhibitionActivityYearly entity)
        {
            using (ExhibitorActivityRepository = new ExhibitorActivityRepository(ConnectionString))
            {
                entity = ExhibitorActivityRepository.Add(entity);
                if (entity.MemberExhibitionYearlyId > 0)
                {
                    XsiItemdId = entity.MemberExhibitionYearlyId;
                    
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitorActivity(XsiMemberExhibitionActivityYearly entity)
        {
            XsiMemberExhibitionActivityYearly OriginalEntity = GetExhibitorActivityByItemId(entity.MemberExhibitionYearlyId);

            if (OriginalEntity != null)
            {
                using (ExhibitorActivityRepository = new ExhibitorActivityRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line

                    ExhibitorActivityRepository.Update(entity);
                    if (ExhibitorActivityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitorActivity(XsiMemberExhibitionActivityYearly entity)
        {
            List<XsiMemberExhibitionActivityYearly> entities = GetExhibitorActivity(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiMemberExhibitionActivityYearly e in entities)
                {
                    result = DeleteExhibitorActivity(e.MemberExhibitionYearlyId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitorActivity(long itemId)
        {
            XsiMemberExhibitionActivityYearly entity = GetExhibitorActivityByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitorActivityRepository = new ExhibitorActivityRepository(ConnectionString))
                {
                    ExhibitorActivityRepository.Delete(itemId);
                    if (ExhibitorActivityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
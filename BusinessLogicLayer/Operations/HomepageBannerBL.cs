﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class HomepageBannerBL : IDisposable
    {
        #region Feilds
        IHomepageBannerRepository HomepageBannerRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public HomepageBannerBL()
        {
            ConnectionString = null;
        }
        public HomepageBannerBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomepageBannerRepository != null)
                this.HomepageBannerRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiHomepageBanner GetHomepageBannerByItemId(long itemId)
        {
            using (HomepageBannerRepository = new HomepageBannerRepository(ConnectionString))
            {
                return HomepageBannerRepository.GetById(itemId);
            }
        }

        public List<XsiHomepageBanner> GetHomepageBanner()
        {
            using (HomepageBannerRepository = new HomepageBannerRepository(ConnectionString))
            {
                return HomepageBannerRepository.Select();
            }
        }
        public List<XsiHomepageBanner> GetHomepageBanner(EnumSortlistBy sortListBy)
        {
            using (HomepageBannerRepository = new HomepageBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageBannerRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageBannerRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HomepageBannerRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HomepageBannerRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageBannerRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HomepageBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiHomepageBanner> GetHomepageBanner(XsiHomepageBanner entity)
        {
            using (HomepageBannerRepository = new HomepageBannerRepository(ConnectionString))
            {
                return HomepageBannerRepository.Select(entity);
            }
        }
        public List<XsiHomepageBanner> GetHomepageBanner(XsiHomepageBanner entity, EnumSortlistBy sortListBy)
        {
            using (HomepageBannerRepository = new HomepageBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageBannerRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageBannerRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HomepageBannerRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HomepageBannerRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageBannerRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return HomepageBannerRepository.Select(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return HomepageBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertHomepageBanner(XsiHomepageBanner entity)
        {
            using (HomepageBannerRepository = new HomepageBannerRepository(ConnectionString))
            {
                entity = HomepageBannerRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateHomepageBanner(XsiHomepageBanner entity)
        {
            XsiHomepageBanner OriginalEntity = GetHomepageBannerByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (HomepageBannerRepository = new HomepageBannerRepository(ConnectionString))
                {

                    HomepageBannerRepository.Update(entity);
                    if (HomepageBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateHomepageBannerStatus(long itemId)
        {
            XsiHomepageBanner entity = GetHomepageBannerByItemId(itemId);
            if (entity != null)
            {
                using (HomepageBannerRepository = new HomepageBannerRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    HomepageBannerRepository.Update(entity);
                    if (HomepageBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteHomepageBanner(XsiHomepageBanner entity)
        {
            List<XsiHomepageBanner> entities = GetHomepageBanner(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiHomepageBanner e in entities)
                {
                    result = DeleteHomepageBanner(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteHomepageBanner(long itemId)
        {
            XsiHomepageBanner entity = GetHomepageBannerByItemId(itemId);
            if (entity != null)
            {
                using (HomepageBannerRepository = new HomepageBannerRepository(ConnectionString))
                {
                    HomepageBannerRepository.Delete(itemId);
                    if (HomepageBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        //Custom Methods
        public long GetHomeBannerInfo(long languageid)
        {
            using (HomepageBannerRepository = new HomepageBannerRepository(ConnectionString))
            {
                return HomepageBannerRepository.Select(new XsiHomepageBanner() {IsActive = EnumConversion.ToString(EnumBool.Yes) }).Count();
            }
        }
        #endregion
    }
}
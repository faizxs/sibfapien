﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionMemberLogBL : IDisposable
    {
        #region Feilds
        IExhibitionMemberLogRepository IExhibitionMemberLogRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionMemberLogBL()
        {
            ConnectionString = null;
        }
        public ExhibitionMemberLogBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionMemberLogRepository != null)
                this.IExhibitionMemberLogRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionMemberLogRepository = new ExhibitionMemberLogRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionMemberLog GetExhibitionMemberLogByItemId(long itemId)
        {
            using (IExhibitionMemberLogRepository = new ExhibitionMemberLogRepository(ConnectionString))
            {
                return IExhibitionMemberLogRepository.GetById(itemId);
            }
        } 

        public List<XsiExhibitionMemberLog> GetExhibitionMemberLog()
        {
            using (IExhibitionMemberLogRepository = new ExhibitionMemberLogRepository(ConnectionString))
            {
                return IExhibitionMemberLogRepository.Select();
            }
        }
        public List<XsiExhibitionMemberLog> GetExhibitionMemberLog(EnumSortlistBy sortListBy)
        {
            using (IExhibitionMemberLogRepository = new ExhibitionMemberLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionMemberLogRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionMemberLogRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionMemberLogRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionMemberLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionMemberLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionMemberLog> GetExhibitionMemberLog(XsiExhibitionMemberLog entity)
        {
            using (IExhibitionMemberLogRepository = new ExhibitionMemberLogRepository(ConnectionString))
            {
                return IExhibitionMemberLogRepository.Select(entity);
            }
        }
        public List<XsiExhibitionMemberLog> GetExhibitionMemberLog(XsiExhibitionMemberLog entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionMemberLogRepository = new ExhibitionMemberLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionMemberLogRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionMemberLogRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionMemberLogRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionMemberLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionMemberLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionMemberLog GetExhibitionMemberLog(long groupId, long languageId)
        {
            using (IExhibitionMemberLogRepository = new ExhibitionMemberLogRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionMemberLog(XsiExhibitionMemberLog entity)
        {
            using (IExhibitionMemberLogRepository = new ExhibitionMemberLogRepository(ConnectionString))
            {
                entity = IExhibitionMemberLogRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionMemberLog(XsiExhibitionMemberLog entity)
        {
            XsiExhibitionMemberLog OriginalEntity = GetExhibitionMemberLogByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IExhibitionMemberLogRepository = new ExhibitionMemberLogRepository(ConnectionString))
                {
                    IExhibitionMemberLogRepository.Update(entity);
                    if (IExhibitionMemberLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionMemberLogStatus(long itemId)
        {
            XsiExhibitionMemberLog entity = GetExhibitionMemberLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionMemberLogRepository = new ExhibitionMemberLogRepository(ConnectionString))
                {
                    IExhibitionMemberLogRepository.Update(entity);
                    if (IExhibitionMemberLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionMemberLog(XsiExhibitionMemberLog entity)
        {
            List<XsiExhibitionMemberLog> entities = GetExhibitionMemberLog(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionMemberLog e in entities)
                {
                    result = DeleteExhibitionMemberLog(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionMemberLog(long itemId)
        {
            XsiExhibitionMemberLog entity = GetExhibitionMemberLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionMemberLogRepository = new ExhibitionMemberLogRepository(ConnectionString))
                {
                    IExhibitionMemberLogRepository.Delete(itemId);
                    if (IExhibitionMemberLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SchoolRegistrationBL : IDisposable
    {
        #region Feilds
        ISchoolRegistrationRepository SchoolRegistrationRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SchoolRegistrationBL()
        {
            ConnectionString = null;
        }
        public SchoolRegistrationBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SchoolRegistrationRepository != null)
                this.SchoolRegistrationRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiSchoolRegistration GetSchoolRegistrationByItemId(long itemId)
        {
            using (SchoolRegistrationRepository = new SchoolRegistrationRepository(ConnectionString))
            {
                return SchoolRegistrationRepository.GetById(itemId);
            }
        }
        
        public List<XsiSchoolRegistration> GetSchoolRegistration()
        {
            using (SchoolRegistrationRepository = new SchoolRegistrationRepository(ConnectionString))
            {
                return SchoolRegistrationRepository.Select();
            }
        }
        public List<XsiSchoolRegistration> GetSchoolRegistration(EnumSortlistBy sortListBy)
        {
            using (SchoolRegistrationRepository = new SchoolRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SchoolRegistrationRepository.Select().OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SchoolRegistrationRepository.Select().OrderByDescending(p => p.Type).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SchoolRegistrationRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SchoolRegistrationRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SchoolRegistrationRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiSchoolRegistration> GetSchoolRegistration(XsiSchoolRegistration entity)
        {
            using (SchoolRegistrationRepository = new SchoolRegistrationRepository(ConnectionString))
            {
                return SchoolRegistrationRepository.Select(entity);
            }
        }
        public List<XsiSchoolRegistration> GetSchoolRegistration(XsiSchoolRegistration entity, EnumSortlistBy sortListBy)
        {
            using (SchoolRegistrationRepository = new SchoolRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SchoolRegistrationRepository.Select(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SchoolRegistrationRepository.Select(entity).OrderByDescending(p => p.Type).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SchoolRegistrationRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SchoolRegistrationRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SchoolRegistrationRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiSchoolRegistration> GetSchoolRegistrationOr(XsiSchoolRegistration entity, EnumSortlistBy sortListBy)
        {
            using (SchoolRegistrationRepository = new SchoolRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SchoolRegistrationRepository.SelectOr(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SchoolRegistrationRepository.SelectOr(entity).OrderByDescending(p => p.Type).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SchoolRegistrationRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SchoolRegistrationRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SchoolRegistrationRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertSchoolRegistration(XsiSchoolRegistration entity)
        {
            using (SchoolRegistrationRepository = new SchoolRegistrationRepository(ConnectionString))
            {
                entity = SchoolRegistrationRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSchoolRegistration(XsiSchoolRegistration entity)
        {
            XsiSchoolRegistration OriginalEntity = GetSchoolRegistrationByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SchoolRegistrationRepository = new SchoolRegistrationRepository(ConnectionString))
                {
                    SchoolRegistrationRepository.Update(entity);
                    if (SchoolRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSchoolRegistrationStatus(long itemId)
        {
            XsiSchoolRegistration entity = GetSchoolRegistrationByItemId(itemId);
            if (entity != null)
            {
                using (SchoolRegistrationRepository = new SchoolRegistrationRepository(ConnectionString))
                {
                    //if (EnumConversion.ToString(EnumStatus.Open) == entity.Status)
                    //{
                    //    entity.Status = EnumConversion.ToString(EnumStatus.CLose);
                    //}
                    //else
                    //{
                    //    entity.Status = EnumConversion.ToString(EnumStatus.CLose);
                    //    entity.DateClosed = DateTime.Now.Date;
                    //}

                    SchoolRegistrationRepository.Update(entity);
                    if (SchoolRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSchoolRegistrationFlag(long itemId, string flag)
        {
            XsiSchoolRegistration entity = GetSchoolRegistrationByItemId(itemId);
            if (entity != null)
            {
                using (SchoolRegistrationRepository = new SchoolRegistrationRepository(ConnectionString))
                {
                    //entity.FlagType = flag;
                    SchoolRegistrationRepository.Update(entity);
                    if (SchoolRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSchoolRegistration(XsiSchoolRegistration entity)
        {
            if (GetSchoolRegistration(entity).Count() > 0)
            {
                using (SchoolRegistrationRepository = new SchoolRegistrationRepository(ConnectionString))
                {
                    SchoolRegistrationRepository.Delete(entity);
                    if (SchoolRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSchoolRegistration(long itemId)
        {
            if (GetSchoolRegistrationByItemId(itemId) != null)
            {
                using (SchoolRegistrationRepository = new SchoolRegistrationRepository(ConnectionString))
                {
                    SchoolRegistrationRepository.Delete(itemId);
                    if (SchoolRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
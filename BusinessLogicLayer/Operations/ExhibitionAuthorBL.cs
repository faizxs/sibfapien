﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionAuthorBL : IDisposable
    {
        #region Feilds
        IExhibitionAuthorRepository ExhibitionAuthorRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionAuthorBL()
        {
            ConnectionString = null;
        }
        public ExhibitionAuthorBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionAuthorRepository != null)
                this.ExhibitionAuthorRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionAuthor GetExhibitionAuthorByItemId(long itemId)
        {
            using (ExhibitionAuthorRepository = new ExhibitionAuthorRepository(ConnectionString))
            {
                return ExhibitionAuthorRepository.GetById(itemId);
            }
        }
         
        public List<XsiExhibitionAuthor> GetExhibitionAuthor()
        {
            using (ExhibitionAuthorRepository = new ExhibitionAuthorRepository(ConnectionString))
            {
                return ExhibitionAuthorRepository.Select();
            }
        }
        public List<XsiExhibitionAuthor> GetExhibitionAuthor(EnumSortlistBy sortListBy)
        {
            using (ExhibitionAuthorRepository = new ExhibitionAuthorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionAuthorRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionAuthorRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionAuthorRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionAuthorRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionAuthorRepository.Select().OrderBy(p => p.AuthorId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionAuthorRepository.Select().OrderByDescending(p => p.AuthorId).ToList();

                    default:
                        return ExhibitionAuthorRepository.Select().OrderByDescending(p => p.AuthorId).ToList();
                }
            }
        }
        public List<XsiExhibitionAuthor> GetExhibitionAuthor(XsiExhibitionAuthor entity)
        {
            using (ExhibitionAuthorRepository = new ExhibitionAuthorRepository(ConnectionString))
            {
                return ExhibitionAuthorRepository.Select(entity);
            }
        }
        public Array GetExhibitionAuthorForUI(XsiExhibitionAuthor entity, long exhibitionGroupID)
        {
            using (ExhibitionAuthorRepository = new ExhibitionAuthorRepository(ConnectionString))
            {
                return ExhibitionAuthorRepository.SelectUI(entity, exhibitionGroupID);
            }
        }
        public Array GetExhibitionAuthorUIForCMS(XsiExhibitionAuthor entity)
        {
            using (ExhibitionAuthorRepository = new ExhibitionAuthorRepository(ConnectionString))
            {
                return ExhibitionAuthorRepository.SelectUIForCMS(entity);
            }
        }
        public List<XsiExhibitionAuthor> GetExhibitionAuthorForSCRFUI(XsiExhibitionAuthor entity, long exhibitionGroupID)
        {
            using (ExhibitionAuthorRepository = new ExhibitionAuthorRepository(ConnectionString))
            {
                return ExhibitionAuthorRepository.SelectSCRFUI(entity, exhibitionGroupID);
            }
        }
        public List<XsiExhibitionAuthor> GetExhibitionAuthor(XsiExhibitionAuthor entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionAuthorRepository = new ExhibitionAuthorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionAuthorRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionAuthorRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionAuthorRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionAuthorRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionAuthorRepository.Select(entity).OrderBy(p => p.AuthorId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionAuthorRepository.Select(entity).OrderByDescending(p => p.AuthorId).ToList();

                    default:
                        return ExhibitionAuthorRepository.Select(entity).OrderByDescending(p => p.AuthorId).ToList();
                }
            }
        }
        public EnumResultType InsertExhibitionAuthor(XsiExhibitionAuthor entity)
        {
            using (ExhibitionAuthorRepository = new ExhibitionAuthorRepository(ConnectionString))
            {
                entity = ExhibitionAuthorRepository.Add(entity);
                if (entity.AuthorId > 0)
                {
                    XsiItemdId = entity.AuthorId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionAuthor(XsiExhibitionAuthor entity)
        {
            XsiExhibitionAuthor OriginalEntity = GetExhibitionAuthorByItemId(entity.AuthorId);

            if (OriginalEntity != null)
            {
                using (ExhibitionAuthorRepository = new ExhibitionAuthorRepository(ConnectionString))
                {
                    ExhibitionAuthorRepository.Update(entity);
                    if (ExhibitionAuthorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionAuthorStatus(long itemId)
        {
            XsiExhibitionAuthor entity = GetExhibitionAuthorByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionAuthorRepository = new ExhibitionAuthorRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionAuthorRepository.Update(entity);
                    if (ExhibitionAuthorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionAuthor(XsiExhibitionAuthor entity)
        {
            List<XsiExhibitionAuthor> entities = GetExhibitionAuthor(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionAuthor e in entities)
                {
                    result = DeleteExhibitionAuthor(e.AuthorId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionAuthor(long itemId)
        {
            XsiExhibitionAuthor entity = GetExhibitionAuthorByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionAuthorRepository = new ExhibitionAuthorRepository(ConnectionString))
                {
                    ExhibitionAuthorRepository.Delete(itemId);
                    if (ExhibitionAuthorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
        //custom
        public long GetNameCount(long itemID, string title, string titlear)
        {
            using (ExhibitionAuthorRepository = new ExhibitionAuthorRepository(ConnectionString))
            {
                return ExhibitionAuthorRepository.GetNameCount(itemID, title, titlear);
            }

        }
    }
}
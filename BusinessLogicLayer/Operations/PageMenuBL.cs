﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PageMenuBL : IDisposable
    {
        #region Feilds
        IPageMenuRepository PageMenuRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PageMenuBL()
        {
            ConnectionString = null;
        }
        public PageMenuBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PageMenuRepository != null)
                this.PageMenuRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiPageMenuNew GetPageMenuByItemId(long itemId)
        {
            using (PageMenuRepository = new PageMenuRepository(ConnectionString))
            {
                return PageMenuRepository.GetById(itemId);
            }
        }

        public List<XsiPageMenuNew> GetPageMenu()
        {
            using (PageMenuRepository = new PageMenuRepository(ConnectionString))
            {
                return PageMenuRepository.Select();
            }
        }
        public List<XsiPageMenuNew> GetPageMenu(EnumSortlistBy sortListBy)
        {
            using (PageMenuRepository = new PageMenuRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PageMenuRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PageMenuRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PageMenuRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PageMenuRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return PageMenuRepository.Select().OrderBy(p => p.SortOrder).ToList();

                    default:
                        return PageMenuRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPageMenuNew> GetPageMenu(XsiPageMenuNew entity)
        {
            using (PageMenuRepository = new PageMenuRepository(ConnectionString))
            {
                return PageMenuRepository.Select(entity);
            }
        }

        public List<XsiPageMenuNew> GetPageMenuOr(XsiPageMenuNew entity, EnumSortlistBy sortListBy)
        {
            using (PageMenuRepository = new PageMenuRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PageMenuRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PageMenuRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PageMenuRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PageMenuRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return PageMenuRepository.SelectOr(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return PageMenuRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPageMenuNew> GetPageMenu(XsiPageMenuNew entity, EnumSortlistBy sortListBy)
        {
            using (PageMenuRepository = new PageMenuRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PageMenuRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PageMenuRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PageMenuRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PageMenuRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return PageMenuRepository.Select(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return PageMenuRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPageMenuNew> GetParents(long groupId, long languageId)
        {
            List<XsiPageMenuNew> ParentsMenuList = new List<XsiPageMenuNew>();
            using (PageMenuRepository = new PageMenuRepository(ConnectionString))
            {
                /*XsiPageMenuNew entity = new XsiPageMenuNew();
                long gid = groupId;
                do
                {
                    entity = GetPageMenu(gid, languageId);
                    if (entity != null)
                    {
                        ParentsMenuList.Add(entity);
                        gid = entity.ParentId ?? default(long);
                    }
                    else
                    {
                        gid = -1;
                    }
                }
                while (gid != default(long) && gid != -1);*/
                return ParentsMenuList.OrderBy(p => p.ParentId).ToList();
            }
        }
        public EnumResultType InsertPageMenu(XsiPageMenuNew entity)
        {
            using (PageMenuRepository = new PageMenuRepository(ConnectionString))
            {
                entity = PageMenuRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePageMenu(XsiPageMenuNew entity)
        {
            XsiPageMenuNew OriginalEntity = GetPageMenuByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PageMenuRepository = new PageMenuRepository(ConnectionString))
                {
                    PageMenuRepository.Update(entity);
                    if (PageMenuRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdatePageMenuStatus(long itemId)
        {
            XsiPageMenuNew entity = GetPageMenuByItemId(itemId);
            if (entity != null)
            {
                using (PageMenuRepository = new PageMenuRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    PageMenuRepository.Update(entity);
                    if (PageMenuRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdatePageMenuSortOrder(long groupId, long order)
        {
            EnumResultType result = EnumResultType.NotFound;

            /*List<XsiPageMenuNew> entities = GetPageMenuByGroupId(groupId).ToList();
            if (entities != null)
            {
                using (PageMenuRepository = new PageMenuRepository(ConnectionString))
                {
                    foreach (XsiPageMenuNew entity in entities)
                    {
                        entity.SortOrder = order;
                        PageMenuRepository.Update(entity);
                        if (PageMenuRepository.SubmitChanges() > 0)
                            result = EnumResultType.Success;
                        else
                            result = EnumResultType.Failed;
                    }
                }
            }*/

            return result;
        }
        public EnumResultType DeletePageMenu(XsiPageMenuNew entity)
        {
            if (GetPageMenu(entity).Count() > 0)
            {
                using (PageMenuRepository = new PageMenuRepository(ConnectionString))
                {
                    PageMenuRepository.Delete(entity);
                    if (PageMenuRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePageMenu(long itemId)
        {
            if (GetPageMenuByItemId(itemId) != null)
            {
                using (PageMenuRepository = new PageMenuRepository(ConnectionString))
                {
                    PageMenuRepository.Delete(itemId);
                    if (PageMenuRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
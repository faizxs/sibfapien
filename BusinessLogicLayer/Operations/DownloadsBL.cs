﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class DownloadsBL : IDisposable
    {
        #region Feilds
        IDownloadsRepository DownloadsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public DownloadsBL()
        {
            ConnectionString = null;
        }
        public DownloadsBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.DownloadsRepository != null)
                this.DownloadsRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiDownloads GetDownloadsByItemId(long itemId)
        {
            using (DownloadsRepository = new DownloadsRepository(ConnectionString))
            {
                return DownloadsRepository.GetById(itemId);
            }
        }
      
        public List<XsiDownloads> GetDownloads()
        {
            using (DownloadsRepository = new DownloadsRepository(ConnectionString))
            {
                return DownloadsRepository.Select();
            }
        }
        public List<XsiDownloads> GetDownloads(EnumSortlistBy sortListBy)
        {
            using (DownloadsRepository = new DownloadsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return DownloadsRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return DownloadsRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return DownloadsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return DownloadsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return DownloadsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return DownloadsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return DownloadsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiDownloads> GetDownloads(XsiDownloads entity)
        {
            using (DownloadsRepository = new DownloadsRepository(ConnectionString))
            {
                return DownloadsRepository.Select(entity);
            }
        }
        public List<XsiDownloads> GetDownloads(XsiDownloads entity, EnumSortlistBy sortListBy)
        {
            using (DownloadsRepository = new DownloadsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return DownloadsRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return DownloadsRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return DownloadsRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return DownloadsRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return DownloadsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return DownloadsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return DownloadsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiDownloads> GetCompleteDownloads(XsiDownloads entity, EnumSortlistBy sortListBy,long langId)
        {
            using (DownloadsRepository = new DownloadsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return DownloadsRepository.SelectComeplete(entity, langId).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return DownloadsRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return DownloadsRepository.SelectComeplete(entity, langId).OrderBy(p => p.Dated).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return DownloadsRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.Dated).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return DownloadsRepository.SelectComeplete(entity, langId).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return DownloadsRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return DownloadsRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public XsiDownloads GetDownloads(long itemId, long categoryId)
        {
            using (DownloadsRepository = new DownloadsRepository(ConnectionString))
            {
                return DownloadsRepository.Select(new XsiDownloads() { ItemId = itemId, DownloadCategoryId = categoryId }).FirstOrDefault();
            }
        }

        public EnumResultType InsertDownloads(XsiDownloads entity)
        {
            using (DownloadsRepository = new DownloadsRepository(ConnectionString))
            {
                entity = DownloadsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateDownloads(XsiDownloads entity)
        {
            XsiDownloads OriginalEntity = GetDownloadsByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (DownloadsRepository = new DownloadsRepository(ConnectionString))
                {
                    DownloadsRepository.Update(entity);
                    if (DownloadsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateDownloadsStatus(long itemId)
        {
            XsiDownloads entity = GetDownloadsByItemId(itemId);
            if (entity != null)
            {
                using (DownloadsRepository = new DownloadsRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    DownloadsRepository.Update(entity);
                    if (DownloadsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteDownloads(XsiDownloads entity)
        {
            List<XsiDownloads> entities = GetDownloads(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiDownloads e in entities)
                {
                    result = DeleteDownloads(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteDownloads(long itemId)
        {
            XsiDownloads entity = GetDownloadsByItemId(itemId);
            if (entity != null)
            {
                using (DownloadsRepository = new DownloadsRepository(ConnectionString))
                {
                    DownloadsRepository.Delete(itemId);
                    if (DownloadsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class CareerSpecialityBL : IDisposable
    {
        #region Feilds
        ICareerSpecialityRepository CareerSpecialityRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public CareerSpecialityBL()
        {
            ConnectionString = null;
        }
        public CareerSpecialityBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerSpecialityRepository != null)
                this.CareerSpecialityRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiCareerSpeciality GetCareerSpecialityByItemId(long itemId)
        {
            using (CareerSpecialityRepository = new CareerSpecialityRepository(ConnectionString))
            {
                return CareerSpecialityRepository.GetById(itemId);
            }
        }
        
        public List<XsiCareerSpeciality> GetCareerSpeciality()
        {
            using (CareerSpecialityRepository = new CareerSpecialityRepository(ConnectionString))
            {
                return CareerSpecialityRepository.Select();
            }
        }
        public List<XsiCareerSpeciality> GetCareerSpeciality(EnumSortlistBy sortListBy)
        {
            using (CareerSpecialityRepository = new CareerSpecialityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerSpecialityRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerSpecialityRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerSpecialityRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerSpecialityRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerSpecialityRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerSpecialityRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerSpecialityRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareerSpeciality> GetCareerSpeciality(XsiCareerSpeciality entity)
        {
            using (CareerSpecialityRepository = new CareerSpecialityRepository(ConnectionString))
            {
                return CareerSpecialityRepository.Select(entity);
            }
        }
        public List<XsiCareerSpeciality> GetCareerSpeciality(XsiCareerSpeciality entity, EnumSortlistBy sortListBy)
        {
            using (CareerSpecialityRepository = new CareerSpecialityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerSpecialityRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerSpecialityRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerSpecialityRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerSpecialityRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerSpecialityRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerSpecialityRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerSpecialityRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareerSpeciality> GetCareerSpecialityOr(XsiCareerSpeciality entity, EnumSortlistBy sortListBy)
        {
            using (CareerSpecialityRepository = new CareerSpecialityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerSpecialityRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerSpecialityRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerSpecialityRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerSpecialityRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerSpecialityRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerSpecialityRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerSpecialityRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public EnumResultType InsertCareerSpeciality(XsiCareerSpeciality entity)
        {
            using (CareerSpecialityRepository = new CareerSpecialityRepository(ConnectionString))
            {
                entity = CareerSpecialityRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateCareerSpeciality(XsiCareerSpeciality entity)
        {
            XsiCareerSpeciality OriginalEntity = GetCareerSpecialityByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (CareerSpecialityRepository = new CareerSpecialityRepository(ConnectionString))
                {
                    CareerSpecialityRepository.Update(entity);
                    if (CareerSpecialityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateCareerSpecialityStatus(long itemId)
        {
            XsiCareerSpeciality entity = GetCareerSpecialityByItemId(itemId);
            if (entity != null)
            {
                using (CareerSpecialityRepository = new CareerSpecialityRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    CareerSpecialityRepository.Update(entity);
                    if (CareerSpecialityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteCareerSpeciality(XsiCareerSpeciality entity)
        {
            List<XsiCareerSpeciality> entities = GetCareerSpeciality(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiCareerSpeciality e in entities)
                {
                    result = DeleteCareerSpeciality(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteCareerSpeciality(long itemId)
        {
            XsiCareerSpeciality entity = GetCareerSpecialityByItemId(itemId);
            if (entity != null)
            {
                using (CareerSpecialityRepository = new CareerSpecialityRepository(ConnectionString))
                {
                    CareerSpecialityRepository.Delete(itemId);
                    if (CareerSpecialityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class VideoCategoryBL : IDisposable
    {
        #region Feilds
        IVideoCategoryRepository VideoCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public VideoCategoryBL()
        {
            ConnectionString = null;
        }
        public VideoCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.VideoCategoryRepository != null)
                this.VideoCategoryRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiVideoCategory GetVideoCategoryByItemId(long itemId)
        {
            using (VideoCategoryRepository = new VideoCategoryRepository(ConnectionString))
            {
                return VideoCategoryRepository.GetById(itemId);
            }
        }
        public List<XsiVideoCategory> GetVideoCategory()
        {
            using (VideoCategoryRepository = new VideoCategoryRepository(ConnectionString))
            {
                return VideoCategoryRepository.Select();
            }
        }
        public List<XsiVideoCategory> GetVideoCategory(EnumSortlistBy sortListBy)
        {
            using (VideoCategoryRepository = new VideoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return VideoCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return VideoCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return VideoCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return VideoCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return VideoCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return VideoCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return VideoCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiVideoCategory> GetVideoCategory(XsiVideoCategory entity)
        {
            using (VideoCategoryRepository = new VideoCategoryRepository(ConnectionString))
            {
                return VideoCategoryRepository.Select(entity);
            }
        }
        public List<XsiVideoCategory> GetVideoCategory(XsiVideoCategory entity, EnumSortlistBy sortListBy)
        {
            using (VideoCategoryRepository = new VideoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return VideoCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return VideoCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return VideoCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return VideoCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return VideoCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return VideoCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return VideoCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiVideoCategory> GetVideoCategoryOr(XsiVideoCategory entity, EnumSortlistBy sortListBy)
        {
            using (VideoCategoryRepository = new VideoCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return VideoCategoryRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return VideoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return VideoCategoryRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return VideoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return VideoCategoryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return VideoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return VideoCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertVideoCategory(XsiVideoCategory entity)
        {
            using (VideoCategoryRepository = new VideoCategoryRepository(ConnectionString))
            {
                entity = VideoCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateVideoCategory(XsiVideoCategory entity)
        {
            XsiVideoCategory OriginalEntity = GetVideoCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (VideoCategoryRepository = new VideoCategoryRepository(ConnectionString))
                {
                    VideoCategoryRepository.Update(entity);
                    if (VideoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateVideoCategoryStatus(long itemId, long langId)
        {
            XsiVideoCategory entity = GetVideoCategoryByItemId(itemId);
            if (entity != null)
            {
                using (VideoCategoryRepository = new VideoCategoryRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    VideoCategoryRepository.Update(entity);
                    if (VideoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteVideoCategory(XsiVideoCategory entity)
        {
            List<XsiVideoCategory> entities = GetVideoCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiVideoCategory e in entities)
                {
                    result = DeleteVideoCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteVideoCategory(long itemId)
        {
            XsiVideoCategory entity = GetVideoCategoryByItemId(itemId);
            if (entity != null)
            {
                using (VideoCategoryRepository = new VideoCategoryRepository(ConnectionString))
                {
                    VideoCategoryRepository.Delete(itemId);
                    if (VideoCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
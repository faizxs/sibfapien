﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PPProgramBL : IDisposable
    {
        #region Feilds
        IPPProgramRepository PPProgramRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PPProgramBL()
        {
            ConnectionString = null;
        }
        public PPProgramBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PPProgramRepository != null)
                this.PPProgramRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (PPProgramRepository = new PPProgramRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionPpprogram GetPPProgramByItemId(long itemId)
        {
            using (PPProgramRepository = new PPProgramRepository(ConnectionString))
            {
                return PPProgramRepository.GetById(itemId);
            }
        }
        public List<XsiExhibitionPpprogram> GetPPProgram()
        {
            using (PPProgramRepository = new PPProgramRepository(ConnectionString))
            {
                return PPProgramRepository.Select();
            }
        }
        public List<XsiExhibitionPpprogram> GetPPProgram(EnumSortlistBy sortListBy)
        {
            using (PPProgramRepository = new PPProgramRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return PPProgramRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PPProgramRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PPProgramRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionPpprogram> GetPPProgram(XsiExhibitionPpprogram entity)
        {
            using (PPProgramRepository = new PPProgramRepository(ConnectionString))
            {
                return PPProgramRepository.Select(entity);
            }
        }
        public List<XsiExhibitionPpprogram> GetPPProgram(XsiExhibitionPpprogram entity, EnumSortlistBy sortListBy)
        {
            using (PPProgramRepository = new PPProgramRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return PPProgramRepository.Select(entity).OrderBy(p => p.ProfessionalProgramRegistrationId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PPProgramRepository.Select(entity).OrderByDescending(p => p.ProfessionalProgramRegistrationId).ToList();

                    default:
                        return PPProgramRepository.Select(entity).OrderByDescending(p => p.ProfessionalProgramRegistrationId).ToList();
                }
            }
        }
        public XsiExhibitionPpprogram GetPPProgram(long groupId, long languageId)
        {
            return null;
        }

        public EnumResultType InsertPPProgram(XsiExhibitionPpprogram entity)
        {
            using (PPProgramRepository = new PPProgramRepository(ConnectionString))
            {
                entity = PPProgramRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePPProgram(XsiExhibitionPpprogram entity)
        {
            XsiExhibitionPpprogram OriginalEntity = GetPPProgramByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PPProgramRepository = new PPProgramRepository(ConnectionString))
                {
                    PPProgramRepository.Update(entity);
                    if (PPProgramRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePPProgram(XsiExhibitionPpprogram entity)
        {
            List<XsiExhibitionPpprogram> entities = GetPPProgram(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionPpprogram e in entities)
                {
                    result = DeletePPProgram(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeletePPProgram(long itemId)
        {
            XsiExhibitionPpprogram entity = GetPPProgramByItemId(itemId);
            if (entity != null)
            {
                using (PPProgramRepository = new PPProgramRepository(ConnectionString))
                {
                    PPProgramRepository.Delete(itemId);
                    if (PPProgramRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
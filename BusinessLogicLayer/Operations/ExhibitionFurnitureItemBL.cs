﻿using System;
using System.Collections.Generic;
using System.Text;
using Xsi.Repositories;
using Entities.Models;
using System.Linq;


namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionFurnitureItemBL : IDisposable
    {
        #region Feilds
        IExhibitionFurnitureItemRepository ExhibitionFurnitureItemRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionFurnitureItemBL()
        {
            ConnectionString = null;
        }
        public ExhibitionFurnitureItemBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionFurnitureItemRepository != null)
                this.ExhibitionFurnitureItemRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionFurnitureItems GetExhibitionFurnitureItemByItemId(long itemId)
        {
            using (ExhibitionFurnitureItemRepository = new ExhibitionFurnitureItemRepository(ConnectionString))
            {
                return ExhibitionFurnitureItemRepository.GetById(itemId);
            }
        }
        
        public List<XsiExhibitionFurnitureItems> GetExhibitionFurnitureItem()
        {
            using (ExhibitionFurnitureItemRepository = new ExhibitionFurnitureItemRepository(ConnectionString))
            {
                return ExhibitionFurnitureItemRepository.Select();
            }
        }
        public List<XsiExhibitionFurnitureItems> GetExhibitionFurnitureItem(EnumSortlistBy sortListBy)
        {
            using (ExhibitionFurnitureItemRepository = new ExhibitionFurnitureItemRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionFurnitureItemRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionFurnitureItemRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionFurnitureItemRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionFurnitureItemRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionFurnitureItemRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionFurnitureItemRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionFurnitureItemRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionFurnitureItems> GetExhibitionFurnitureItem(XsiExhibitionFurnitureItems entity)
        {
            using (ExhibitionFurnitureItemRepository = new ExhibitionFurnitureItemRepository(ConnectionString))
            {
                return ExhibitionFurnitureItemRepository.Select(entity);
            }
        }
        public List<XsiExhibitionFurnitureItems> GetExhibitionFurnitureItem(XsiExhibitionFurnitureItems entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionFurnitureItemRepository = new ExhibitionFurnitureItemRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionFurnitureItemRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionFurnitureItemRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionFurnitureItemRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionFurnitureItemRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionFurnitureItemRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionFurnitureItemRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionFurnitureItemRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertExhibitionFurnitureItem(XsiExhibitionFurnitureItems entity)
        {
            using (ExhibitionFurnitureItemRepository = new ExhibitionFurnitureItemRepository(ConnectionString))
            {
                entity = ExhibitionFurnitureItemRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionFurnitureItem(XsiExhibitionFurnitureItems entity)
        {
            XsiExhibitionFurnitureItems OriginalEntity = GetExhibitionFurnitureItemByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionFurnitureItemRepository = new ExhibitionFurnitureItemRepository(ConnectionString))
                {
                    ExhibitionFurnitureItemRepository.Update(entity);
                    if (ExhibitionFurnitureItemRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateExhibitionFurnitureItemStatus(long itemId, long langId)
        {
            XsiExhibitionFurnitureItems entity = GetExhibitionFurnitureItemByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionFurnitureItemRepository = new ExhibitionFurnitureItemRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionFurnitureItemRepository.Update(entity);
                    if (ExhibitionFurnitureItemRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionFurnitureItem(XsiExhibitionFurnitureItems entity)
        {
            List<XsiExhibitionFurnitureItems> entities = GetExhibitionFurnitureItem(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionFurnitureItems e in entities)
                {
                    result = DeleteExhibitionFurnitureItem(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionFurnitureItem(long itemId)
        {
            XsiExhibitionFurnitureItems entity = GetExhibitionFurnitureItemByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionFurnitureItemRepository = new ExhibitionFurnitureItemRepository(ConnectionString))
                {
                    ExhibitionFurnitureItemRepository.Delete(itemId);
                    if (ExhibitionFurnitureItemRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
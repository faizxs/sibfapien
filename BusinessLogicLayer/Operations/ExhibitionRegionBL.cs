﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionRegionBL : IDisposable
    {
        #region Feilds
        IExhibitionRegionRepository ExhibitionRegionRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionRegionBL()
        {
            ConnectionString = null;
        }
        public ExhibitionRegionBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionRegionRepository != null)
                this.ExhibitionRegionRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionRegion GetExhibitionRegionByItemId(long itemId)
        {
            using (ExhibitionRegionRepository = new ExhibitionRegionRepository(ConnectionString))
            {
                return ExhibitionRegionRepository.GetById(itemId);
            }
        }
       
        public List<XsiExhibitionRegion> GetExhibitionRegion()
        {
            using (ExhibitionRegionRepository = new ExhibitionRegionRepository(ConnectionString))
            {
                return ExhibitionRegionRepository.Select();
            }
        }
        public List<XsiExhibitionRegion> GetExhibitionRegion(EnumSortlistBy sortListBy)
        {
            using (ExhibitionRegionRepository = new ExhibitionRegionRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionRegionRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionRegionRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionRegionRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionRegionRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionRegionRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionRegionRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionRegionRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionRegion> GetExhibitionRegion(XsiExhibitionRegion entity)
        {
            using (ExhibitionRegionRepository = new ExhibitionRegionRepository(ConnectionString))
            {
                return ExhibitionRegionRepository.Select(entity);
            }
        }
        public List<XsiExhibitionRegion> GetExhibitionRegion(XsiExhibitionRegion entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionRegionRepository = new ExhibitionRegionRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionRegionRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionRegionRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionRegionRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionRegionRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionRegionRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionRegionRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionRegionRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiExhibitionRegion> GetExhibitionRegionOr(XsiExhibitionRegion entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionRegionRepository = new ExhibitionRegionRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionRegionRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionRegionRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionRegionRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionRegionRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionRegionRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionRegionRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionRegionRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
       
        public EnumResultType InsertExhibitionRegion(XsiExhibitionRegion entity)
        {
            using (ExhibitionRegionRepository = new ExhibitionRegionRepository(ConnectionString))
            {
                entity = ExhibitionRegionRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionRegion(XsiExhibitionRegion entity)
        {
            XsiExhibitionRegion OriginalEntity = GetExhibitionRegionByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionRegionRepository = new ExhibitionRegionRepository(ConnectionString))
                {
                    ExhibitionRegionRepository.Update(entity);
                    if (ExhibitionRegionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionRegionStatus(long itemId)
        {
            XsiExhibitionRegion entity = GetExhibitionRegionByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionRegionRepository = new ExhibitionRegionRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionRegionRepository.Update(entity);
                    if (ExhibitionRegionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionRegion(XsiExhibitionRegion entity)
        {
            List<XsiExhibitionRegion> entities = GetExhibitionRegion(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionRegion e in entities)
                {
                    result = DeleteExhibitionRegion(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionRegion(long itemId)
        {
            XsiExhibitionRegion entity = GetExhibitionRegionByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionRegionRepository = new ExhibitionRegionRepository(ConnectionString))
                {
                    ExhibitionRegionRepository.Delete(itemId);
                    if (ExhibitionRegionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionStaffGuestBL : IDisposable
    {
        #region Feilds
        IExhibitionStaffGuestRepository ExhibitionStaffGuestRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestBL()
        {
            ConnectionString = null;
        }
        public ExhibitionStaffGuestBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionStaffGuestRepository != null)
                this.ExhibitionStaffGuestRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionStaffGuest GetExhibitionStaffGuestByItemId(long itemId)
        {
            using (ExhibitionStaffGuestRepository = new ExhibitionStaffGuestRepository(ConnectionString))
            {
                return ExhibitionStaffGuestRepository.GetById(itemId);
            }
        }
         
        public List<XsiExhibitionStaffGuest> GetExhibitionStaffGuest()
        {
            using (ExhibitionStaffGuestRepository = new ExhibitionStaffGuestRepository(ConnectionString))
            {
                return ExhibitionStaffGuestRepository.Select();
            }
        }
        public List<XsiExhibitionStaffGuest> GetExhibitionStaffGuest(EnumSortlistBy sortListBy)
        {
            using (ExhibitionStaffGuestRepository = new ExhibitionStaffGuestRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionStaffGuestRepository.Select().OrderBy(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionStaffGuestRepository.Select().OrderByDescending(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionStaffGuestRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionStaffGuestRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionStaffGuestRepository.Select().OrderBy(p => p.StaffGuestId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionStaffGuestRepository.Select().OrderByDescending(p => p.StaffGuestId).ToList();

                    default:
                        return ExhibitionStaffGuestRepository.Select().OrderByDescending(p => p.StaffGuestId).ToList();
                }
            }
        }
        public List<XsiExhibitionStaffGuest> GetExhibitionStaffGuest(XsiExhibitionStaffGuest entity)
        {
            using (ExhibitionStaffGuestRepository = new ExhibitionStaffGuestRepository(ConnectionString))
            {
                return ExhibitionStaffGuestRepository.Select(entity);
            }
        }
        public List<XsiExhibitionStaffGuest> GetExhibitionStaffGuest(XsiExhibitionStaffGuest entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionStaffGuestRepository = new ExhibitionStaffGuestRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionStaffGuestRepository.Select(entity).OrderBy(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionStaffGuestRepository.Select(entity).OrderByDescending(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionStaffGuestRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionStaffGuestRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionStaffGuestRepository.Select(entity).OrderBy(p => p.StaffGuestId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionStaffGuestRepository.Select(entity).OrderByDescending(p => p.StaffGuestId).ToList();

                    default:
                        return ExhibitionStaffGuestRepository.Select(entity).OrderByDescending(p => p.StaffGuestId).ToList();
                }
            }
        }

        public EnumResultType InsertExhibitionStaffGuest(XsiExhibitionStaffGuest entity)
        {
            using (ExhibitionStaffGuestRepository = new ExhibitionStaffGuestRepository(ConnectionString))
            {
                entity = ExhibitionStaffGuestRepository.Add(entity);
                if (entity.StaffGuestId > 0)
                {
                    XsiItemdId = entity.StaffGuestId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionStaffGuest(XsiExhibitionStaffGuest entity)
        {
            //XsiExhibitionStaffGuest OriginalEntity = GetExhibitionStaffGuestByItemId(entity.StaffGuestId);
            XsiExhibitionStaffGuest OriginalEntity = GetExhibitionStaffGuestByItemId(entity.StaffGuestId);

            if (OriginalEntity != null)
            {
                using (ExhibitionStaffGuestRepository = new ExhibitionStaffGuestRepository(ConnectionString))
                {
                    ExhibitionStaffGuestRepository.Update(entity);
                    if (ExhibitionStaffGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionStaffGuestStatus(long itemId)
        {
            XsiExhibitionStaffGuest entity = GetExhibitionStaffGuestByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionStaffGuestRepository = new ExhibitionStaffGuestRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionStaffGuestRepository.Update(entity);
                    if (ExhibitionStaffGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionStaffGuest(XsiExhibitionStaffGuest entity)
        {
            List<XsiExhibitionStaffGuest> entities = GetExhibitionStaffGuest(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionStaffGuest e in entities)
                {
                    result = DeleteExhibitionStaffGuest(e.StaffGuestId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionStaffGuest(long itemId)
        {
            XsiExhibitionStaffGuest entity = GetExhibitionStaffGuestByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionStaffGuestRepository = new ExhibitionStaffGuestRepository(ConnectionString))
                {
                    ExhibitionStaffGuestRepository.Delete(itemId);
                    if (ExhibitionStaffGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class HomepageSectionTwoBL : IDisposable
    {
        #region Feilds
        IHomepageSectionTwoRepository HomepageSectionTwoRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public HomepageSectionTwoBL()
        {
            ConnectionString = null;
        }
        public HomepageSectionTwoBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomepageSectionTwoRepository != null)
                this.HomepageSectionTwoRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiHomepageSectionTwo GetHomepageSectionTwoByItemId(long itemId)
        {
            using (HomepageSectionTwoRepository = new HomepageSectionTwoRepository(ConnectionString))
            {
                return HomepageSectionTwoRepository.GetById(itemId);
            }
        }
        public List<XsiHomepageSectionTwo> GetHomepageSectionTwo()
        {
            using (HomepageSectionTwoRepository = new HomepageSectionTwoRepository(ConnectionString))
            {
                return HomepageSectionTwoRepository.Select();
            }
        }
        public List<XsiHomepageSectionTwo> GetHomepageSectionTwo(EnumSortlistBy sortListBy)
        {
            using (HomepageSectionTwoRepository = new HomepageSectionTwoRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageSectionTwoRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageSectionTwoRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HomepageSectionTwoRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HomepageSectionTwoRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageSectionTwoRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageSectionTwoRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HomepageSectionTwoRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiHomepageSectionTwo> GetHomepageSectionTwo(XsiHomepageSectionTwo entity)
        {
            using (HomepageSectionTwoRepository = new HomepageSectionTwoRepository(ConnectionString))
            {
                return HomepageSectionTwoRepository.Select(entity);
            }
        }
        public List<XsiHomepageSectionTwo> GetHomepageSectionTwo(XsiHomepageSectionTwo entity, EnumSortlistBy sortListBy)
        {
            using (HomepageSectionTwoRepository = new HomepageSectionTwoRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageSectionTwoRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageSectionTwoRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HomepageSectionTwoRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HomepageSectionTwoRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageSectionTwoRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageSectionTwoRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HomepageSectionTwoRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiHomepageSectionTwo> GetCompleteHomepageSectionTwo(XsiHomepageSectionTwo entity, EnumSortlistBy sortListBy,long langId)
        {
            using (HomepageSectionTwoRepository = new HomepageSectionTwoRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageSectionTwoRepository.SelectComeplete(entity, langId).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageSectionTwoRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageSectionTwoRepository.SelectComeplete(entity, langId).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageSectionTwoRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HomepageSectionTwoRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public XsiHomepageSectionTwo GetHomepageSectionTwo(long Id, long categoryId)
        {
            using (HomepageSectionTwoRepository = new HomepageSectionTwoRepository(ConnectionString))
            {
                return HomepageSectionTwoRepository.Select(new XsiHomepageSectionTwo() { ItemId = Id, CategoryId = categoryId }).FirstOrDefault();
            }
        }

        public EnumResultType InsertHomepageSectionTwo(XsiHomepageSectionTwo entity)
        {
            using (HomepageSectionTwoRepository = new HomepageSectionTwoRepository(ConnectionString))
            {
                entity = HomepageSectionTwoRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateHomepageSectionTwo(XsiHomepageSectionTwo entity)
        {
            XsiHomepageSectionTwo OriginalEntity = GetHomepageSectionTwoByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (HomepageSectionTwoRepository = new HomepageSectionTwoRepository(ConnectionString))
                {
                    HomepageSectionTwoRepository.Update(entity);
                    if (HomepageSectionTwoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateHomepageSectionTwoStatus(long itemId)
        {
            XsiHomepageSectionTwo entity = GetHomepageSectionTwoByItemId(itemId);
            if (entity != null)
            {
                using (HomepageSectionTwoRepository = new HomepageSectionTwoRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    HomepageSectionTwoRepository.Update(entity);
                    if (HomepageSectionTwoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteHomepageSectionTwo(XsiHomepageSectionTwo entity)
        {
            List<XsiHomepageSectionTwo> entities = GetHomepageSectionTwo(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiHomepageSectionTwo e in entities)
                {
                    result = DeleteHomepageSectionTwo(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteHomepageSectionTwo(long itemId)
        {
            XsiHomepageSectionTwo entity = GetHomepageSectionTwoByItemId(itemId);
            if (entity != null)
            {
                using (HomepageSectionTwoRepository = new HomepageSectionTwoRepository(ConnectionString))
                {
                    HomepageSectionTwoRepository.Delete(itemId);
                    if (HomepageSectionTwoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
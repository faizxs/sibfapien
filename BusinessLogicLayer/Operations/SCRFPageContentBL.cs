﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFPageContentBL : IDisposable
    {
        #region Feilds
        ISCRFPagesContentRepository XsiSCRFPagesContentRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFPageContentBL()
        {
            ConnectionString = null;
        }
        public SCRFPageContentBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiSCRFPagesContentRepository != null)
                this.XsiSCRFPagesContentRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrfpagesContent GetSCRFPageContentByItemId(long itemId)
        {
            using (XsiSCRFPagesContentRepository = new SCRFPagesContentRepository(ConnectionString))
            {
                return XsiSCRFPagesContentRepository.GetById(itemId);
            }
        }

        public List<XsiScrfpagesContent> GetSCRFPageContent()
        {
            using (XsiSCRFPagesContentRepository = new SCRFPagesContentRepository(ConnectionString))
            {
                return XsiSCRFPagesContentRepository.Select();
            }
        }

        public List<XsiScrfpagesContent> GetSCRFPageContent(EnumSortlistBy sortListBy)
        {
            using (XsiSCRFPagesContentRepository = new SCRFPagesContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiSCRFPagesContentRepository.Select().OrderBy(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiSCRFPagesContentRepository.Select().OrderByDescending(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiSCRFPagesContentRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiSCRFPagesContentRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiSCRFPagesContentRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfpagesContent> GetSCRFPageContent(XsiScrfpagesContent entity)
        {
            using (XsiSCRFPagesContentRepository = new SCRFPagesContentRepository(ConnectionString))
            {
                return XsiSCRFPagesContentRepository.Select(entity);
            }
        }
        public List<XsiScrfpagesContent> GetSCRFPageContentOr(XsiScrfpagesContent entity, EnumSortlistBy sortListBy)
        {
            using (XsiSCRFPagesContentRepository = new SCRFPagesContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiSCRFPagesContentRepository.SelectOr(entity).OrderBy(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiSCRFPagesContentRepository.SelectOr(entity).OrderByDescending(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiSCRFPagesContentRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiSCRFPagesContentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiSCRFPagesContentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfpagesContent> GetSCRFPageContent(XsiScrfpagesContent entity, EnumSortlistBy sortListBy)
        {
            using (XsiSCRFPagesContentRepository = new SCRFPagesContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiSCRFPagesContentRepository.Select(entity).OrderBy(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiSCRFPagesContentRepository.Select(entity).OrderByDescending(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiSCRFPagesContentRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiSCRFPagesContentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiSCRFPagesContentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFPageContent(XsiScrfpagesContent entity)
        {
            using (XsiSCRFPagesContentRepository = new SCRFPagesContentRepository(ConnectionString))
            {
                entity = XsiSCRFPagesContentRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFPageContent(XsiScrfpagesContent entity)
        {
            XsiScrfpagesContent OriginalEntity = GetSCRFPageContentByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (XsiSCRFPagesContentRepository = new SCRFPagesContentRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line
                    entity.RollbackXml = BusinessMethodFactory.GetRollbackXml(OriginalEntity);

                    XsiSCRFPagesContentRepository.Update(entity);
                    if (XsiSCRFPagesContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackSCRFPageContent(long itemId)
        {
            //if no RollbackXml propery in entity then delete below code and return EnumResultType.Invalid;
            
            XsiScrfpagesContent OriginalEntity = GetSCRFPageContentByItemId(itemId);

            if (OriginalEntity == null)
                return EnumResultType.NotFound;
            else if (string.IsNullOrEmpty(OriginalEntity.RollbackXml))
                return EnumResultType.Invalid;
            else
            {

                XsiScrfpagesContent NewEntity = (XsiScrfpagesContent)BusinessMethodFactory.GetXmlToObject(OriginalEntity.RollbackXml, OriginalEntity.GetType());

                OriginalEntity.RollbackXml = string.Empty;

                NewEntity.ItemId = itemId;
                NewEntity.RollbackXml = BusinessMethodFactory.GetRollbackXml(OriginalEntity);

                using (XsiSCRFPagesContentRepository = new SCRFPagesContentRepository(ConnectionString))
                {
                    XsiSCRFPagesContentRepository.Update(NewEntity);
                    if (XsiSCRFPagesContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }

        }
        public EnumResultType UpdateSCRFPageContentStatus(long itemId)
        {
            XsiScrfpagesContent entity = GetSCRFPageContentByItemId(itemId);
            if (entity != null)
            {
                using (XsiSCRFPagesContentRepository = new SCRFPagesContentRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    XsiSCRFPagesContentRepository.Update(entity);
                    if (XsiSCRFPagesContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFPageContent(XsiScrfpagesContent entity)
        {
            if (GetSCRFPageContent(entity).Count() > 0)
            {
                using (XsiSCRFPagesContentRepository = new SCRFPagesContentRepository(ConnectionString))
                {
                    XsiSCRFPagesContentRepository.Delete(entity);
                    if (XsiSCRFPagesContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFPageContent(long itemId)
        {
            if (GetSCRFPageContentByItemId(itemId) != null)
            {
                using (XsiSCRFPagesContentRepository = new SCRFPagesContentRepository(ConnectionString))
                {
                    XsiSCRFPagesContentRepository.Delete(itemId);
                    if (XsiSCRFPagesContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
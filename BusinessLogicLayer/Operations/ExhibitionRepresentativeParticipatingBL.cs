﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionRepresentativeParticipatingBL : IDisposable
    {
        #region Feilds
        IExhibitionRepresentativeParticipatingRepository IExhibitionRepresentativeParticipatingRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionRepresentativeParticipatingBL()
        {
            ConnectionString = null;
        }
        public ExhibitionRepresentativeParticipatingBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionRepresentativeParticipatingRepository != null)
                this.IExhibitionRepresentativeParticipatingRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionRepresentativeParticipatingRepository = new ExhibitionRepresentativeParticipatingRepository(ConnectionString))
            {
                return  -1;
            }
        }

        public XsiExhibitionRepresentativeParticipating GetExhibitionRepresentativeParticipatingByItemId(long itemId)
        {
            using (IExhibitionRepresentativeParticipatingRepository = new ExhibitionRepresentativeParticipatingRepository(ConnectionString))
            {
                return IExhibitionRepresentativeParticipatingRepository.GetById(itemId);
            }
        }

        public XsiExhibitionRepresentativeParticipating GetByRepresentativeId(long itemId)
        {
            using (IExhibitionRepresentativeParticipatingRepository = new ExhibitionRepresentativeParticipatingRepository(ConnectionString))
            {
                return IExhibitionRepresentativeParticipatingRepository.GetByRepresentativeId(itemId);
            }
        }
        public List<XsiExhibitionRepresentativeParticipating> GetExhibitionRepresentativeParticipating()
        {
            using (IExhibitionRepresentativeParticipatingRepository = new ExhibitionRepresentativeParticipatingRepository(ConnectionString))
            {
                return IExhibitionRepresentativeParticipatingRepository.Select();
            }
        }
        public List<XsiExhibitionRepresentativeParticipating> GetExhibitionRepresentativeParticipating(EnumSortlistBy sortListBy)
        {
            using (IExhibitionRepresentativeParticipatingRepository = new ExhibitionRepresentativeParticipatingRepository(ConnectionString))
            {
                switch (sortListBy)
                {

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionRepresentativeParticipatingRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionRepresentativeParticipatingRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionRepresentativeParticipatingRepository.Select().OrderBy(p => p.RepresentativeId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionRepresentativeParticipatingRepository.Select().OrderByDescending(p => p.RepresentativeId).ToList();

                    default:
                        return IExhibitionRepresentativeParticipatingRepository.Select().OrderByDescending(p => p.RepresentativeId).ToList();
                }
            }
        }
        public List<XsiExhibitionRepresentativeParticipating> GetExhibitionRepresentativeParticipating(XsiExhibitionRepresentativeParticipating entity)
        {
            using (IExhibitionRepresentativeParticipatingRepository = new ExhibitionRepresentativeParticipatingRepository(ConnectionString))
            {
                return IExhibitionRepresentativeParticipatingRepository.Select(entity);
            }
        }
        public List<XsiExhibitionRepresentativeParticipating> GetExhibitionRepresentativeParticipating(XsiExhibitionRepresentativeParticipating entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionRepresentativeParticipatingRepository = new ExhibitionRepresentativeParticipatingRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionRepresentativeParticipatingRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionRepresentativeParticipatingRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionRepresentativeParticipatingRepository.Select(entity).OrderBy(p => p.RepresentativeId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionRepresentativeParticipatingRepository.Select(entity).OrderByDescending(p => p.RepresentativeId).ToList();

                    default:
                        return IExhibitionRepresentativeParticipatingRepository.Select(entity).OrderByDescending(p => p.RepresentativeId).ToList();
                }
            }
        }
        public XsiExhibitionRepresentativeParticipating GetExhibitionRepresentativeParticipating(long groupId, long languageId)
        {
            using (IExhibitionRepresentativeParticipatingRepository = new ExhibitionRepresentativeParticipatingRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionRepresentativeParticipating(XsiExhibitionRepresentativeParticipating entity)
        {
            using (IExhibitionRepresentativeParticipatingRepository = new ExhibitionRepresentativeParticipatingRepository(ConnectionString))
            {
                entity = IExhibitionRepresentativeParticipatingRepository.Add(entity);
                if (entity.RepresentativeId > 0)
                {
                    XsiItemdId = entity.RepresentativeId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionRepresentativeParticipating(XsiExhibitionRepresentativeParticipating entity)
        {
            if (entity != null)
            {
                using (IExhibitionRepresentativeParticipatingRepository = new ExhibitionRepresentativeParticipatingRepository(ConnectionString))
                {

                    IExhibitionRepresentativeParticipatingRepository.Update(entity);
                    if (IExhibitionRepresentativeParticipatingRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackExhibitionRepresentativeParticipating(long itemId)
        {
            //if no RollbackXml propery in ExhibitionRepresentativeParticipating then delete below code and return EnumResultType.Invalid;

            return EnumResultType.Failed;

        }
        public EnumResultType UpdateExhibitionRepresentativeParticipatingStatus(long itemId)
        {
            XsiExhibitionRepresentativeParticipating entity = GetExhibitionRepresentativeParticipatingByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionRepresentativeParticipatingRepository = new ExhibitionRepresentativeParticipatingRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionRepresentativeParticipatingRepository.Update(entity);
                    if (IExhibitionRepresentativeParticipatingRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionRepresentativeParticipating(XsiExhibitionRepresentativeParticipating entity)
        {
            List<XsiExhibitionRepresentativeParticipating> entities = GetExhibitionRepresentativeParticipating(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionRepresentativeParticipating e in entities)
                {
                    result = DeleteExhibitionRepresentativeParticipating(e.RepresentativeId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionRepresentativeParticipating(long itemId)
        {
            XsiExhibitionRepresentativeParticipating entity = GetExhibitionRepresentativeParticipatingByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionRepresentativeParticipatingRepository = new ExhibitionRepresentativeParticipatingRepository(ConnectionString))
                {
                    IExhibitionRepresentativeParticipatingRepository.Delete(itemId);
                    if (IExhibitionRepresentativeParticipatingRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionBoothSubsectionBL : IDisposable
    {
        #region Feilds
        IExhibitionBoothSubsectionRepository ExhibitionBoothSubsectionRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionBoothSubsectionBL()
        {
            ConnectionString = null;
        }
        public ExhibitionBoothSubsectionBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionBoothSubsectionRepository != null)
                this.ExhibitionBoothSubsectionRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionBoothSubsection GetExhibitionBoothSubsectionByItemId(long itemId)
        {
            using (ExhibitionBoothSubsectionRepository = new ExhibitionBoothSubsectionRepository(ConnectionString))
            {
                return ExhibitionBoothSubsectionRepository.GetById(itemId);
            }
        }
         
        public List<XsiExhibitionBoothSubsection> GetExhibitionBoothSubsection()
        {
            using (ExhibitionBoothSubsectionRepository = new ExhibitionBoothSubsectionRepository(ConnectionString))
            {
                return ExhibitionBoothSubsectionRepository.Select();
            }
        }
        public List<XsiExhibitionBoothSubsection> GetExhibitionBoothSubsection(EnumSortlistBy sortListBy)
        {
            using (ExhibitionBoothSubsectionRepository = new ExhibitionBoothSubsectionRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBoothSubsectionRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBoothSubsectionRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBoothSubsectionRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBoothSubsectionRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBoothSubsectionRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBoothSubsectionRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBoothSubsectionRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionBoothSubsection> GetExhibitionBoothSubsection(XsiExhibitionBoothSubsection entity)
        {
            using (ExhibitionBoothSubsectionRepository = new ExhibitionBoothSubsectionRepository(ConnectionString))
            {
                return ExhibitionBoothSubsectionRepository.Select(entity);
            }
        }
        public List<XsiExhibitionBoothSubsection> GetExhibitionBoothSubsection(XsiExhibitionBoothSubsection entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBoothSubsectionRepository = new ExhibitionBoothSubsectionRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBoothSubsectionRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBoothSubsectionRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBoothSubsectionRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBoothSubsectionRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBoothSubsectionRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBoothSubsectionRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBoothSubsectionRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionBoothSubsection> GetCompleteExhibitionBoothSubsection(XsiExhibitionBoothSubsection entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBoothSubsectionRepository = new ExhibitionBoothSubsectionRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBoothSubsectionRepository.SelectComeplete(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBoothSubsectionRepository.SelectComeplete(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBoothSubsectionRepository.SelectComeplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBoothSubsectionRepository.SelectComeplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBoothSubsectionRepository.SelectComeplete(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBoothSubsectionRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBoothSubsectionRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
         
        public EnumResultType InsertExhibitionBoothSubsection(XsiExhibitionBoothSubsection entity)
        {
            using (ExhibitionBoothSubsectionRepository = new ExhibitionBoothSubsectionRepository(ConnectionString))
            {
                entity = ExhibitionBoothSubsectionRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionBoothSubsection(XsiExhibitionBoothSubsection entity)
        {
            XsiExhibitionBoothSubsection OriginalEntity = GetExhibitionBoothSubsectionByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionBoothSubsectionRepository = new ExhibitionBoothSubsectionRepository(ConnectionString))
                {
                    ExhibitionBoothSubsectionRepository.Update(entity);
                    if (ExhibitionBoothSubsectionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionBoothSubsectionStatus(long itemId)
        {
            XsiExhibitionBoothSubsection entity = GetExhibitionBoothSubsectionByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionBoothSubsectionRepository = new ExhibitionBoothSubsectionRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionBoothSubsectionRepository.Update(entity);
                    if (ExhibitionBoothSubsectionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionBoothSubsection(XsiExhibitionBoothSubsection entity)
        {
            List<XsiExhibitionBoothSubsection> entities = GetExhibitionBoothSubsection(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionBoothSubsection e in entities)
                {
                    result = DeleteExhibitionBoothSubsection(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionBoothSubsection(long itemId)
        {
            XsiExhibitionBoothSubsection entity = GetExhibitionBoothSubsectionByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionBoothSubsectionRepository = new ExhibitionBoothSubsectionRepository(ConnectionString))
                {
                    ExhibitionBoothSubsectionRepository.Delete(itemId);
                    if (ExhibitionBoothSubsectionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionStaffGuestParticipatingBL : IDisposable
    {
        #region Feilds
        IExhibitionStaffGuestParticipatingRepository IExhibitionStaffGuestParticipatingRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestParticipatingBL()
        {
            ConnectionString = null;
        }
        public ExhibitionStaffGuestParticipatingBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionStaffGuestParticipatingRepository != null)
                this.IExhibitionStaffGuestParticipatingRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionStaffGuestParticipatingRepository = new ExhibitionStaffGuestParticipatingRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionStaffGuestParticipating GetExhibitionStaffGuestParticipatingByItemId(long itemId)
        {
            using (IExhibitionStaffGuestParticipatingRepository = new ExhibitionStaffGuestParticipatingRepository(ConnectionString))
            {
                return IExhibitionStaffGuestParticipatingRepository.GetById(itemId);
            }
        }

        public XsiExhibitionStaffGuestParticipating GetByStaffGuestId(long itemId)
        {
            using (IExhibitionStaffGuestParticipatingRepository = new ExhibitionStaffGuestParticipatingRepository(ConnectionString))
            {
                return IExhibitionStaffGuestParticipatingRepository.GetByStaffGuestId(itemId);
            }
        }
        public List<XsiExhibitionStaffGuestParticipating> GetExhibitionStaffGuestParticipating()
        {
            using (IExhibitionStaffGuestParticipatingRepository = new ExhibitionStaffGuestParticipatingRepository(ConnectionString))
            {
                return IExhibitionStaffGuestParticipatingRepository.Select();
            }
        }
        public List<XsiExhibitionStaffGuestParticipating> GetExhibitionStaffGuestParticipating(EnumSortlistBy sortListBy)
        {
            using (IExhibitionStaffGuestParticipatingRepository = new ExhibitionStaffGuestParticipatingRepository(ConnectionString))
            {
                switch (sortListBy)
                {

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionStaffGuestParticipatingRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionStaffGuestParticipatingRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionStaffGuestParticipatingRepository.Select().OrderBy(p => p.StaffGuestId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionStaffGuestParticipatingRepository.Select().OrderByDescending(p => p.StaffGuestId).ToList();

                    default:
                        return IExhibitionStaffGuestParticipatingRepository.Select().OrderByDescending(p => p.StaffGuestId).ToList();
                }
            }
        }
        public List<XsiExhibitionStaffGuestParticipating> GetExhibitionStaffGuestParticipating(XsiExhibitionStaffGuestParticipating entity)
        {
            using (IExhibitionStaffGuestParticipatingRepository = new ExhibitionStaffGuestParticipatingRepository(ConnectionString))
            {
                return IExhibitionStaffGuestParticipatingRepository.Select(entity);
            }
        }
        public List<XsiExhibitionStaffGuestParticipating> GetExhibitionStaffGuestParticipating(XsiExhibitionStaffGuestParticipating entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionStaffGuestParticipatingRepository = new ExhibitionStaffGuestParticipatingRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionStaffGuestParticipatingRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionStaffGuestParticipatingRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionStaffGuestParticipatingRepository.Select(entity).OrderBy(p => p.StaffGuestId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionStaffGuestParticipatingRepository.Select(entity).OrderByDescending(p => p.StaffGuestId).ToList();

                    default:
                        return IExhibitionStaffGuestParticipatingRepository.Select(entity).OrderByDescending(p => p.StaffGuestId).ToList();
                }
            }
        }
        public XsiExhibitionStaffGuestParticipating GetExhibitionStaffGuestParticipating(long groupId, long languageId)
        {
            using (IExhibitionStaffGuestParticipatingRepository = new ExhibitionStaffGuestParticipatingRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionStaffGuestParticipating(XsiExhibitionStaffGuestParticipating entity)
        {
            using (IExhibitionStaffGuestParticipatingRepository = new ExhibitionStaffGuestParticipatingRepository(ConnectionString))
            {
                entity = IExhibitionStaffGuestParticipatingRepository.Add(entity);
                if (entity.StaffGuestId > 0)
                {
                    XsiItemdId = entity.StaffGuestId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionStaffGuestParticipating(XsiExhibitionStaffGuestParticipating entity)
        {
            //XsiExhibitionStaffGuestParticipating OriginalEntity = GetExhibitionStaffGuestParticipatingByItemId(entity.StaffGuestId);

            if (entity != null)
            {
                using (IExhibitionStaffGuestParticipatingRepository = new ExhibitionStaffGuestParticipatingRepository(ConnectionString))
                {

                    IExhibitionStaffGuestParticipatingRepository.Update(entity);
                    if (IExhibitionStaffGuestParticipatingRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackExhibitionStaffGuestParticipating(long itemId)
        {
            //if no RollbackXml propery in ExhibitionStaffGuestParticipating then delete below code and return EnumResultType.Invalid;

            return EnumResultType.Failed;

        }
        public EnumResultType UpdateExhibitionStaffGuestParticipatingStatus(long itemId)
        {
            XsiExhibitionStaffGuestParticipating entity = GetExhibitionStaffGuestParticipatingByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionStaffGuestParticipatingRepository = new ExhibitionStaffGuestParticipatingRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionStaffGuestParticipatingRepository.Update(entity);
                    if (IExhibitionStaffGuestParticipatingRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionStaffGuestParticipating(XsiExhibitionStaffGuestParticipating entity)
        {
            List<XsiExhibitionStaffGuestParticipating> entities = GetExhibitionStaffGuestParticipating(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionStaffGuestParticipating e in entities)
                {
                    result = DeleteExhibitionStaffGuestParticipating(e.StaffGuestId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionStaffGuestParticipating(long itemId)
        {
            XsiExhibitionStaffGuestParticipating entity = GetExhibitionStaffGuestParticipatingByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionStaffGuestParticipatingRepository = new ExhibitionStaffGuestParticipatingRepository(ConnectionString))
                {
                    IExhibitionStaffGuestParticipatingRepository.Delete(itemId);
                    if (IExhibitionStaffGuestParticipatingRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
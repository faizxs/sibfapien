﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionCategoryBL : IDisposable
    {
        #region Feilds
        IExhibitionCategoryRepository ExhibitionCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionCategoryBL()
        {
            ConnectionString = null;
        }
        public ExhibitionCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionCategoryRepository != null)
                this.ExhibitionCategoryRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionCategory GetExhibitionCategoryByItemId(long itemId)
        {
            using (ExhibitionCategoryRepository = new ExhibitionCategoryRepository(ConnectionString))
            {
                return ExhibitionCategoryRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionCategory> GetExhibitionCategory()
        {
            using (ExhibitionCategoryRepository = new ExhibitionCategoryRepository(ConnectionString))
            {
                return ExhibitionCategoryRepository.Select();
            }
        }
        public List<XsiExhibitionCategory> GetExhibitionCategory(EnumSortlistBy sortListBy)
        {
            using (ExhibitionCategoryRepository = new ExhibitionCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionCategory> GetExhibitionCategory(XsiExhibitionCategory entity)
        {
            using (ExhibitionCategoryRepository = new ExhibitionCategoryRepository(ConnectionString))
            {
                return ExhibitionCategoryRepository.Select(entity);
            }
        }
        public List<XsiExhibitionCategory> GetExhibitionCategory(XsiExhibitionCategory entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionCategoryRepository = new ExhibitionCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }


        public List<XsiExhibitionCategory> GetExhibitionCategoryOr(XsiExhibitionCategory entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionCategoryRepository = new ExhibitionCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCategoryRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCategoryRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCategoryRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCategoryRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCategoryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertExhibitionCategory(XsiExhibitionCategory entity)
        {
            using (ExhibitionCategoryRepository = new ExhibitionCategoryRepository(ConnectionString))
            {
                entity = ExhibitionCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionCategory(XsiExhibitionCategory entity)
        {
            XsiExhibitionCategory OriginalEntity = GetExhibitionCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionCategoryRepository = new ExhibitionCategoryRepository(ConnectionString))
                {
                    ExhibitionCategoryRepository.Update(entity);
                    if (ExhibitionCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionCategoryStatus(long itemId, long langId)
        {
            XsiExhibitionCategory entity = GetExhibitionCategoryByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionCategoryRepository = new ExhibitionCategoryRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionCategoryRepository.Update(entity);
                    if (ExhibitionCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionCategory(XsiExhibitionCategory entity)
        {
            List<XsiExhibitionCategory> entities = GetExhibitionCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionCategory e in entities)
                {
                    result = DeleteExhibitionCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionCategory(long itemId)
        {
            XsiExhibitionCategory entity = GetExhibitionCategoryByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionCategoryRepository = new ExhibitionCategoryRepository(ConnectionString))
                {
                    ExhibitionCategoryRepository.Delete(itemId);
                    if (ExhibitionCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
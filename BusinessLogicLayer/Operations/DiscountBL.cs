﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class DiscountBL : IDisposable
    {
        #region Feilds
        IDiscountRepository IDiscountRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public DiscountBL()
        {
            ConnectionString = null;
        }
        public DiscountBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IDiscountRepository != null)
                this.IDiscountRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            return -1;
        }

        public XsiDiscount GetDiscountByItemId(long itemId)
        {
            using (IDiscountRepository = new DiscountRepository(ConnectionString))
            {
                return IDiscountRepository.GetById(itemId);
            }
        }
       
        public List<XsiDiscount> GetDiscount()
        {
            using (IDiscountRepository = new DiscountRepository(ConnectionString))
            {
                return IDiscountRepository.Select();
            }
        }
        public List<XsiDiscount> GetDiscount(EnumSortlistBy sortListBy)
        {
            using (IDiscountRepository = new DiscountRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IDiscountRepository.Select().OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IDiscountRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IDiscountRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IDiscountRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IDiscountRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IDiscountRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IDiscountRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiDiscount> GetDiscount(XsiDiscount entity)
        {
            using (IDiscountRepository = new DiscountRepository(ConnectionString))
            {
                return IDiscountRepository.Select(entity);
            }
        }
        public List<XsiDiscount> GetDiscount(XsiDiscount entity, EnumSortlistBy sortListBy)
        {
            using (IDiscountRepository = new DiscountRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IDiscountRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IDiscountRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IDiscountRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IDiscountRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IDiscountRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IDiscountRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IDiscountRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiDiscount GetDiscount(long groupId, long languageId)
        {
            return null;
        }

        public EnumResultType InsertDiscount(XsiDiscount entity)
        {
            using (IDiscountRepository = new DiscountRepository(ConnectionString))
            {
                entity = IDiscountRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateDiscount(XsiDiscount entity)
        {
            XsiDiscount OriginalEntity = GetDiscountByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IDiscountRepository = new DiscountRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line

                    IDiscountRepository.Update(entity);
                    if (IDiscountRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackDiscount(long itemId)
        {
            //if no RollbackXml propery in Discount then delete below code and return EnumResultType.Invalid;

            return EnumResultType.Failed;

        }
        public EnumResultType UpdateDiscountStatus(long itemId)
        {
            XsiDiscount entity = GetDiscountByItemId(itemId);
            if (entity != null)
            {
                using (IDiscountRepository = new DiscountRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IDiscountRepository.Update(entity);
                    if (IDiscountRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteDiscount(XsiDiscount entity)
        {
            List<XsiDiscount> entities = GetDiscount(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiDiscount e in entities)
                {
                    result = DeleteDiscount(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteDiscount(long itemId)
        {
            XsiDiscount entity = GetDiscountByItemId(itemId);
            if (entity != null)
            {
                using (IDiscountRepository = new DiscountRepository(ConnectionString))
                {
                    IDiscountRepository.Delete(itemId);
                    if (IDiscountRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
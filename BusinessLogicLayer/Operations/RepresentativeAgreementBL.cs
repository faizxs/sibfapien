﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class RepresentativeAgreementBL : IDisposable
    {
        #region Feilds
        IRepresentativeAgreementRepository RepresentativeAgreementRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public RepresentativeAgreementBL()
        {
            ConnectionString = null;
        }
        public RepresentativeAgreementBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.RepresentativeAgreementRepository != null)
                this.RepresentativeAgreementRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiRepresentativeAgreement GetRepresentativeAgreementByItemId(long itemId)
        {
            using (RepresentativeAgreementRepository = new RepresentativeAgreementRepository(ConnectionString))
            {
                return RepresentativeAgreementRepository.GetById(itemId);
            }
        }
      
        public List<XsiRepresentativeAgreement> GetRepresentativeAgreement()
        {
            using (RepresentativeAgreementRepository = new RepresentativeAgreementRepository(ConnectionString))
            {
                return RepresentativeAgreementRepository.Select();
            }
        }
        public List<XsiRepresentativeAgreement> GetRepresentativeAgreement(EnumSortlistBy sortListBy)
        {
            using (RepresentativeAgreementRepository = new RepresentativeAgreementRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return RepresentativeAgreementRepository.Select().OrderBy(p => p.NameEn).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return RepresentativeAgreementRepository.Select().OrderByDescending(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return RepresentativeAgreementRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return RepresentativeAgreementRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return RepresentativeAgreementRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return RepresentativeAgreementRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return RepresentativeAgreementRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiRepresentativeAgreement> GetRepresentativeAgreement(XsiRepresentativeAgreement entity)
        {
            using (RepresentativeAgreementRepository = new RepresentativeAgreementRepository(ConnectionString))
            {
                return RepresentativeAgreementRepository.Select(entity);
            }
        }
        public List<XsiRepresentativeAgreement> GetRepresentativeAgreement(XsiRepresentativeAgreement entity, EnumSortlistBy sortListBy)
        {
            using (RepresentativeAgreementRepository = new RepresentativeAgreementRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return RepresentativeAgreementRepository.Select(entity).OrderBy(p => p.NameEn).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return RepresentativeAgreementRepository.Select(entity).OrderByDescending(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return RepresentativeAgreementRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return RepresentativeAgreementRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return RepresentativeAgreementRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return RepresentativeAgreementRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return RepresentativeAgreementRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertRepresentativeAgreement(XsiRepresentativeAgreement entity)
        {
            using (RepresentativeAgreementRepository = new RepresentativeAgreementRepository(ConnectionString))
            {
                entity = RepresentativeAgreementRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateRepresentativeAgreement(XsiRepresentativeAgreement entity)
        {
            XsiRepresentativeAgreement OriginalEntity = GetRepresentativeAgreementByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (RepresentativeAgreementRepository = new RepresentativeAgreementRepository(ConnectionString))
                {
                    RepresentativeAgreementRepository.Update(entity);
                    if (RepresentativeAgreementRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateRepresentativeAgreementStatus(long itemId)
        {
            XsiRepresentativeAgreement entity = GetRepresentativeAgreementByItemId(itemId);
            if (entity != null)
            {
                using (RepresentativeAgreementRepository = new RepresentativeAgreementRepository(ConnectionString))
                {
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    RepresentativeAgreementRepository.Update(entity);
                    if (RepresentativeAgreementRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteRepresentativeAgreement(XsiRepresentativeAgreement entity)
        {
            List<XsiRepresentativeAgreement> entities = GetRepresentativeAgreement(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiRepresentativeAgreement e in entities)
                {
                    result = DeleteRepresentativeAgreement(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteRepresentativeAgreement(long itemId)
        {
            XsiRepresentativeAgreement entity = GetRepresentativeAgreementByItemId(itemId);
            if (entity != null)
            {
                using (RepresentativeAgreementRepository = new RepresentativeAgreementRepository(ConnectionString))
                {
                    RepresentativeAgreementRepository.Delete(itemId);
                    if (RepresentativeAgreementRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
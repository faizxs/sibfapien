﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFPhotoBL : IDisposable
    {
        #region Feilds
        ISCRFPhotoRepository SCRFPhotoRepository;

        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFPhotoBL()
        {
            ConnectionString = null;
        }
        public SCRFPhotoBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFPhotoRepository != null)
                this.SCRFPhotoRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public long GetNextSortIndex()
        {
            //optional method. return -1 if not required and remove following
            using (SCRFPhotoRepository = new SCRFPhotoRepository(ConnectionString))
            {
                return (SCRFPhotoRepository.Select().Max(p => p.SortIndex) ?? 0) + 1;
            }
        }

        public XsiScrfphoto GetSCRFPhotoByItemId(long itemId)
        {
            using (SCRFPhotoRepository = new SCRFPhotoRepository(ConnectionString))
            {
                return SCRFPhotoRepository.GetById(itemId);
            }
        }
         
        public List<XsiScrfphoto> GetSCRFPhoto()
        {
            using (SCRFPhotoRepository = new SCRFPhotoRepository(ConnectionString))
            {
                return SCRFPhotoRepository.Select();
            }
        }
        public List<XsiScrfphoto> GetSCRFPhoto(EnumSortlistBy sortListBy)
        {
            using (SCRFPhotoRepository = new SCRFPhotoRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPhotoRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPhotoRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFPhotoRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFPhotoRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPhotoRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPhotoRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFPhotoRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfphoto> GetSCRFPhoto(XsiScrfphoto entity)
        {
            using (SCRFPhotoRepository = new SCRFPhotoRepository(ConnectionString))
            {
                return SCRFPhotoRepository.Select(entity);
            }
        }
        public List<XsiScrfphoto> GetSCRFPhoto(XsiScrfphoto entity, EnumSortlistBy sortListBy)
        {
            using (SCRFPhotoRepository = new SCRFPhotoRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPhotoRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPhotoRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFPhotoRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFPhotoRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPhotoRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPhotoRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.BySortOrderAsc:
                        return SCRFPhotoRepository.Select(entity).OrderBy(p => p.SortIndex).ToList();

                    case EnumSortlistBy.BySortOrderDesc:
                        return SCRFPhotoRepository.Select(entity).OrderByDescending(p => p.SortIndex).ToList();

                    default:
                        return SCRFPhotoRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
         
        public EnumResultType InsertSCRFPhoto(XsiScrfphoto entity)
        {
            using (SCRFPhotoRepository = new SCRFPhotoRepository(ConnectionString))
            {
                entity = SCRFPhotoRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFPhoto(XsiScrfphoto entity)
        {
            XsiScrfphoto OriginalEntity = GetSCRFPhotoByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFPhotoRepository = new SCRFPhotoRepository(ConnectionString))
                {
                    SCRFPhotoRepository.Update(entity);
                    if (SCRFPhotoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFPhotoStatus(long itemId)
        {
            XsiScrfphoto entity = GetSCRFPhotoByItemId(itemId);
            if (entity != null)
            {
                using (SCRFPhotoRepository = new SCRFPhotoRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFPhotoRepository.Update(entity);
                    if (SCRFPhotoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFAlbumCover(long itemId)
        {
            using (SCRFPhotoRepository = new SCRFPhotoRepository(ConnectionString))
            {
                XsiScrfphoto entity = SCRFPhotoRepository.GetById(itemId);
                XsiScrfphoto entity1 = new XsiScrfphoto();
                //entity1.CategoryId = entity.CategoryId; 
                List<XsiScrfphoto> PhotoList = SCRFPhotoRepository.Select(entity1);
                if (PhotoList != null)
                {
                    foreach (XsiScrfphoto entity2 in PhotoList)
                    {
                        if (itemId != entity2.ItemId)
                        {
                            entity2.IsAlbumCover = EnumConversion.ToString(EnumBool.No);
                            SCRFPhotoRepository.Update(entity2);
                        }
                    }
                    if (SCRFPhotoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
                else
                    return EnumResultType.NotFound;
            }
        }
        public EnumResultType DeleteSCRFPhoto(XsiScrfphoto entity)
        {
            List<XsiScrfphoto> entities = GetSCRFPhoto(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfphoto e in entities)
                {
                    result = DeleteSCRFPhoto(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFPhoto(long itemId)
        {
            XsiScrfphoto entity = GetSCRFPhotoByItemId(itemId);
            if (entity != null)
            {
                using (SCRFPhotoRepository = new SCRFPhotoRepository(ConnectionString))
                {
                    SCRFPhotoRepository.Delete(itemId);
                    if (SCRFPhotoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
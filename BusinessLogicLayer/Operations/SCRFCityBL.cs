﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFCityBL : IDisposable
    {
        #region Feilds
        ISCRFCityRepository SCRFCityRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFCityBL()
        {
            ConnectionString = null;
        }
        public SCRFCityBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFCityRepository != null)
                this.SCRFCityRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfcity GetSCRFCityByItemId(long itemId)
        {
            using (SCRFCityRepository = new SCRFCityRepository(ConnectionString))
            {
                return SCRFCityRepository.GetById(itemId);
            }
        }

        public List<XsiScrfcity> GetSCRFCity()
        {
            using (SCRFCityRepository = new SCRFCityRepository(ConnectionString))
            {
                return SCRFCityRepository.Select();
            }
        }
        public List<XsiScrfcity> GetSCRFCity(EnumSortlistBy sortListBy)
        {
            using (SCRFCityRepository = new SCRFCityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFCityRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFCityRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFCityRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFCityRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFCityRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFCityRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFCityRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfcity> GetSCRFCity(XsiScrfcity entity)
        {
            using (SCRFCityRepository = new SCRFCityRepository(ConnectionString))
            {
                return SCRFCityRepository.Select(entity);
            }
        }
        public List<XsiScrfcity> GetSCRFCity(XsiScrfcity entity, EnumSortlistBy sortListBy)
        {
            using (SCRFCityRepository = new SCRFCityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFCityRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFCityRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFCityRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFCityRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFCityRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFCityRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFCityRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfcity> GetCompleteSCRFCity(XsiScrfcity entity, EnumSortlistBy sortListBy)
        {
            using (SCRFCityRepository = new SCRFCityRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFCityRepository.SelectComeplete(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFCityRepository.SelectComeplete(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFCityRepository.SelectComeplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFCityRepository.SelectComeplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFCityRepository.SelectComeplete(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFCityRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFCityRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }


        public EnumResultType InsertSCRFCity(XsiScrfcity entity)
        {
            using (SCRFCityRepository = new SCRFCityRepository(ConnectionString))
            {
                entity = SCRFCityRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFCity(XsiScrfcity entity)
        {
            XsiScrfcity OriginalEntity = GetSCRFCityByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFCityRepository = new SCRFCityRepository(ConnectionString))
                {
                    SCRFCityRepository.Update(entity);
                    if (SCRFCityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFCityStatus(long itemId)
        {
            XsiScrfcity entity = GetSCRFCityByItemId(itemId);
            if (entity != null)
            {
                using (SCRFCityRepository = new SCRFCityRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFCityRepository.Update(entity);
                    if (SCRFCityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFCity(XsiScrfcity entity)
        {
            List<XsiScrfcity> entities = GetSCRFCity(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfcity e in entities)
                {
                    result = DeleteSCRFCity(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFCity(long itemId)
        {
            XsiScrfcity entity = GetSCRFCityByItemId(itemId);
            if (entity != null)
            {
                using (SCRFCityRepository = new SCRFCityRepository(ConnectionString))
                {
                    SCRFCityRepository.Delete(itemId);
                    if (SCRFCityRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
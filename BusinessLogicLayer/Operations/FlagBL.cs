﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class FlagBL : IDisposable
    {
        #region Feilds
        IFlagRepository FlagRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public FlagBL()
        {
            ConnectionString = null;
        }
        public FlagBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.FlagRepository != null)
                this.FlagRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiFlags GetFlagByItemId(long itemId)
        {
            using (FlagRepository = new FlagRepository(ConnectionString))
            {
                return FlagRepository.GetById(itemId);
            }
        }

        public List<XsiFlags> GetFlag()
        {
            using (FlagRepository = new FlagRepository(ConnectionString))
            {
                return FlagRepository.Select();
            }
        }
        public List<XsiFlags> GetFlag(EnumSortlistBy sortListBy)
        {
            using (FlagRepository = new FlagRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return FlagRepository.Select().OrderBy(p => p.FlagName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return FlagRepository.Select().OrderByDescending(p => p.FlagName).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return FlagRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return FlagRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return FlagRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiFlags> GetFlag(XsiFlags entity)
        {
            using (FlagRepository = new FlagRepository(ConnectionString))
            {
                return FlagRepository.Select(entity);
            }
        }
        public List<XsiFlags> GetFlag(XsiFlags entity, EnumSortlistBy sortListBy)
        {
            using (FlagRepository = new FlagRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return FlagRepository.Select(entity).OrderBy(p => p.FlagName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return FlagRepository.Select(entity).OrderByDescending(p => p.FlagName).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return FlagRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return FlagRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return FlagRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertFlag(XsiFlags entity)
        {
            using (FlagRepository = new FlagRepository(ConnectionString))
            {
                entity = FlagRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateFlag(XsiFlags entity)
        {
            XsiFlags OriginalEntity = GetFlagByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (FlagRepository = new FlagRepository(ConnectionString))
                {
                    FlagRepository.Update(entity);
                    if (FlagRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteFlag(XsiFlags entity)
        {
            if (GetFlag(entity).Count() > 0)
            {
                using (FlagRepository = new FlagRepository(ConnectionString))
                {
                    FlagRepository.Delete(entity);
                    if (FlagRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteFlag(long itemId)
        {
            if (GetFlagByItemId(itemId) != null)
            {
                using (FlagRepository = new FlagRepository(ConnectionString))
                {
                    FlagRepository.Delete(itemId);
                    if (FlagRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
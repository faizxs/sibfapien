﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFHomepageBannerBL : IDisposable
    {
        #region Feilds
        ISCRFHomepageBannerRepository SCRFHomepageBannerRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFHomepageBannerBL()
        {
            ConnectionString = null;
        }
        public SCRFHomepageBannerBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFHomepageBannerRepository != null)
                this.SCRFHomepageBannerRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrfhomepageBanner GetSCRFHomepageBannerByItemId(long itemId)
        {
            using (SCRFHomepageBannerRepository = new SCRFHomepageBannerRepository(ConnectionString))
            {
                return SCRFHomepageBannerRepository.GetById(itemId);
            }
        }

        public List<XsiScrfhomepageBanner> GetSCRFHomepageBanner()
        {
            using (SCRFHomepageBannerRepository = new SCRFHomepageBannerRepository(ConnectionString))
            {
                return SCRFHomepageBannerRepository.Select();
            }
        }
        public List<XsiScrfhomepageBanner> GetSCRFHomepageBanner(EnumSortlistBy sortListBy)
        {
            using (SCRFHomepageBannerRepository = new SCRFHomepageBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFHomepageBannerRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFHomepageBannerRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFHomepageBannerRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFHomepageBannerRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFHomepageBannerRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFHomepageBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFHomepageBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfhomepageBanner> GetSCRFHomepageBanner(XsiScrfhomepageBanner entity)
        {
            using (SCRFHomepageBannerRepository = new SCRFHomepageBannerRepository(ConnectionString))
            {
                return SCRFHomepageBannerRepository.Select(entity);
            }
        }
        public List<XsiScrfhomepageBanner> GetSCRFHomepageBanner(XsiScrfhomepageBanner entity, EnumSortlistBy sortListBy)
        {
            using (SCRFHomepageBannerRepository = new SCRFHomepageBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFHomepageBannerRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFHomepageBannerRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFHomepageBannerRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFHomepageBannerRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFHomepageBannerRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFHomepageBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFHomepageBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFHomepageBanner(XsiScrfhomepageBanner entity)
        {
            using (SCRFHomepageBannerRepository = new SCRFHomepageBannerRepository(ConnectionString))
            {
                entity = SCRFHomepageBannerRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFHomepageBanner(XsiScrfhomepageBanner entity)
        {
            XsiScrfhomepageBanner OriginalEntity = GetSCRFHomepageBannerByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFHomepageBannerRepository = new SCRFHomepageBannerRepository(ConnectionString))
                {

                    SCRFHomepageBannerRepository.Update(entity);
                    if (SCRFHomepageBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFHomepageBannerStatus(long itemId)
        {
            XsiScrfhomepageBanner entity = GetSCRFHomepageBannerByItemId(itemId);
            if (entity != null)
            {
                using (SCRFHomepageBannerRepository = new SCRFHomepageBannerRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFHomepageBannerRepository.Update(entity);
                    if (SCRFHomepageBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFHomepageBanner(XsiScrfhomepageBanner entity)
        {
            List<XsiScrfhomepageBanner> entities = GetSCRFHomepageBanner(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfhomepageBanner e in entities)
                {
                    result = DeleteSCRFHomepageBanner(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFHomepageBanner(long itemId)
        {
            XsiScrfhomepageBanner entity = GetSCRFHomepageBannerByItemId(itemId);
            if (entity != null)
            {
                using (SCRFHomepageBannerRepository = new SCRFHomepageBannerRepository(ConnectionString))
                {
                    SCRFHomepageBannerRepository.Delete(itemId);
                    if (SCRFHomepageBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        //Custom Methods
        public long GetHomeBannerInfo(long languageid)
        {
            using (SCRFHomepageBannerRepository = new SCRFHomepageBannerRepository(ConnectionString))
            {
                return SCRFHomepageBannerRepository.Select(new XsiScrfhomepageBanner() { IsActive = EnumConversion.ToString(EnumBool.Yes) }).Count();
            }
        }
        #endregion
    }
}
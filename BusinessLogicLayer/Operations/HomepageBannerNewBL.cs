﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class HomepageBannerNewBL : IDisposable
    {
        #region Feilds
        IHomepageBannerNewRepository HomepageBannerNewRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public HomepageBannerNewBL()
        {
            ConnectionString = null;
        }
        public HomepageBannerNewBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomepageBannerNewRepository != null)
                this.HomepageBannerNewRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiHomepageBannerNew GetHomepageBannerNewByItemId(long itemId)
        {
            using (HomepageBannerNewRepository = new HomepageBannerNewRepository(ConnectionString))
            {
                return HomepageBannerNewRepository.GetById(itemId);
            }
        }
        public List<XsiHomepageBannerNew> GetHomepageBannerNew()
        {
            using (HomepageBannerNewRepository = new HomepageBannerNewRepository(ConnectionString))
            {
                return HomepageBannerNewRepository.Select();
            }
        }
        public List<XsiHomepageBannerNew> GetHomepageBannerNew(EnumSortlistBy sortListBy)
        {
            using (HomepageBannerNewRepository = new HomepageBannerNewRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageBannerNewRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageBannerNewRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HomepageBannerNewRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HomepageBannerNewRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageBannerNewRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageBannerNewRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HomepageBannerNewRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiHomepageBannerNew> GetHomepageBannerNew(XsiHomepageBannerNew entity)
        {
            using (HomepageBannerNewRepository = new HomepageBannerNewRepository(ConnectionString))
            {
                return HomepageBannerNewRepository.Select(entity);
            }
        }
        public List<XsiHomepageBannerNew> GetHomepageBannerNew(XsiHomepageBannerNew entity, EnumSortlistBy sortListBy)
        {
            using (HomepageBannerNewRepository = new HomepageBannerNewRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageBannerNewRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageBannerNewRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HomepageBannerNewRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HomepageBannerNewRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageBannerNewRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageBannerNewRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return HomepageBannerNewRepository.Select(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return HomepageBannerNewRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertHomepageBannerNew(XsiHomepageBannerNew entity)
        {
            using (HomepageBannerNewRepository = new HomepageBannerNewRepository(ConnectionString))
            {
                entity = HomepageBannerNewRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateHomepageBannerNew(XsiHomepageBannerNew entity)
        {
            XsiHomepageBannerNew OriginalEntity = GetHomepageBannerNewByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (HomepageBannerNewRepository = new HomepageBannerNewRepository(ConnectionString))
                {

                    HomepageBannerNewRepository.Update(entity);
                    if (HomepageBannerNewRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateHomepageBannerNewStatus(long itemId)
        {
            XsiHomepageBannerNew entity = GetHomepageBannerNewByItemId(itemId);
            if (entity != null)
            {
                using (HomepageBannerNewRepository = new HomepageBannerNewRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    HomepageBannerNewRepository.Update(entity);
                    if (HomepageBannerNewRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteHomepageBannerNew(XsiHomepageBannerNew entity)
        {
            List<XsiHomepageBannerNew> entities = GetHomepageBannerNew(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiHomepageBannerNew e in entities)
                {
                    result = DeleteHomepageBannerNew(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteHomepageBannerNew(long itemId)
        {
            XsiHomepageBannerNew entity = GetHomepageBannerNewByItemId(itemId);
            if (entity != null)
            {
                using (HomepageBannerNewRepository = new HomepageBannerNewRepository(ConnectionString))
                {
                    HomepageBannerNewRepository.Delete(itemId);
                    if (HomepageBannerNewRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateHomepageBannerNewSortOrder(long Id, long order)
        {
            EnumResultType result = EnumResultType.NotFound;

            List<XsiHomepageBannerNew> entities = GetHomepageBannerNew(new XsiHomepageBannerNew() { ItemId=Id }).ToList();
            if (entities != null)
            {
                using (HomepageBannerNewRepository = new HomepageBannerNewRepository(ConnectionString))
                {
                    foreach (XsiHomepageBannerNew entity in entities)
                    {
                        entity.SortOrder = order;
                        HomepageBannerNewRepository.Update(entity);
                        if (HomepageBannerNewRepository.SubmitChanges() > 0)
                            result = EnumResultType.Success;
                        else
                            result = EnumResultType.Failed;
                    }
                }
            }
            return result;
        }
        //Custom Methods
        public long GetHomeBannerInfo()
        {
            using (HomepageBannerNewRepository = new HomepageBannerNewRepository(ConnectionString))
            {
                return HomepageBannerNewRepository.Select(new XsiHomepageBannerNew() { IsActive = EnumConversion.ToString(EnumBool.Yes) }).Count();
            }
        }
        #endregion
    }
}
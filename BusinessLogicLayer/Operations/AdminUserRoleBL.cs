﻿using System;
using System.Collections.Generic;
using System.Text;
using Xsi.Repositories;
using Entities.Models;
using System.Linq;

namespace Xsi.BusinessLogicLayer
{
    public class AdminUserRoleBL : IDisposable
    {
        #region Feilds
        IAdminRoleRepository XsiAdminRoleRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public AdminUserRoleBL()
        {
            ConnectionString = null;
        }
        public AdminUserRoleBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiAdminRoleRepository != null)
                this.XsiAdminRoleRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdminRoles GetAdminUserRoleByItemId(long itemId)
        {
            using (XsiAdminRoleRepository = new AdminRolesRepository())
            {
                return XsiAdminRoleRepository.GetById(itemId);
            }
        }
        public List<XsiAdminRoles> GetNonSuperAdminUserRole(EnumSortlistBy sortListBy)
        {
            using (XsiAdminRoleRepository = new AdminRolesRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiAdminRoleRepository.Select().Where(p => p.ItemId > 1).OrderBy(p => p.Role).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiAdminRoleRepository.Select().Where(p => p.ItemId > 1).OrderByDescending(p => p.Role).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiAdminRoleRepository.Select().Where(p => p.ItemId > 1).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiAdminRoleRepository.Select().Where(p => p.ItemId > 1).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiAdminRoleRepository.Select().Where(p => p.ItemId > 1).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiAdminRoles> GetAdminUserRole(EnumSortlistBy sortListBy)
        {
            using (XsiAdminRoleRepository = new AdminRolesRepository())
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiAdminRoleRepository.Select().OrderBy(p => p.Role).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiAdminRoleRepository.Select().OrderByDescending(p => p.Role).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiAdminRoleRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiAdminRoleRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiAdminRoleRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiAdminRoles> GetAdminUserRole(XsiAdminRoles entity)
        {
            using (XsiAdminRoleRepository = new AdminRolesRepository())
            {
                return XsiAdminRoleRepository.Select(entity);
            }
        }
        public List<XsiAdminRoles> GetAdminUserRoleAndUser(XsiAdminRoles entity)
        {
            using (XsiAdminRoleRepository = new AdminRolesRepository())
            {
                return XsiAdminRoleRepository.SelectAdminRoleAndUsers(entity);
            }
        }
        public List<XsiAdminRoles> GetAdminUserRoleAndPermission(XsiAdminRoles entity)
        {
            using (XsiAdminRoleRepository = new AdminRolesRepository())
            {
                return XsiAdminRoleRepository.SelectAdminRoleAndPermissions(entity);
            }
        }

        public EnumResultType InsertAdminUserRole(XsiAdminRoles entity)
        {
            using (XsiAdminRoleRepository = new AdminRolesRepository(ConnectionString))
            {
                entity = XsiAdminRoleRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateAdminUserRole(XsiAdminRoles entity)
        {
            if (GetAdminUserRoleByItemId(entity.ItemId) != null)
            {
                using (XsiAdminRoleRepository = new AdminRolesRepository(ConnectionString))
                {
                    XsiAdminRoleRepository.Update(entity);
                    if (XsiAdminRoleRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteAdminUserRole(XsiAdminRoles entity)
        {
            if (GetAdminUserRoleAndUser(entity).Count() == 0 && GetAdminUserRoleAndPermission(entity).Count() == 0)
            {
                if (GetAdminUserRole(entity).ToList().Count > 0)
                {
                    using (XsiAdminRoleRepository = new AdminRolesRepository(ConnectionString))
                    {
                        XsiAdminRoleRepository.Delete(entity);
                        if (XsiAdminRoleRepository.SubmitChanges() > 0)
                            return EnumResultType.Success;
                        else
                            return EnumResultType.Failed;
                    }
                }
                else
                    return EnumResultType.NotFound;
            }
            else
                return EnumResultType.ValueInUse;
        }
        public EnumResultType DeleteAdminUserRole(long itemId)
        {
            if (GetAdminUserRoleAndPermission(new XsiAdminRoles() { ItemId = itemId }).Count() == 0 && GetAdminUserRoleAndUser(new XsiAdminRoles() { ItemId = itemId }).Count() == 0)
            {
                if (GetAdminUserRoleByItemId(itemId) != null)
                {
                    using (XsiAdminRoleRepository = new AdminRolesRepository(ConnectionString))
                    {
                        XsiAdminRoleRepository.Delete(itemId);
                        if (XsiAdminRoleRepository.SubmitChanges() > 0)
                            return EnumResultType.Success;
                        else
                            return EnumResultType.Failed;
                    }
                }
                else
                    return EnumResultType.NotFound;
            }
            else
                return EnumResultType.ValueInUse;
        }
        #endregion
    }
}
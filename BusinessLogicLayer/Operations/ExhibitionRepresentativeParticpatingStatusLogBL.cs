﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionRepresentativeParticpatingStatusLogBL : IDisposable
    {
        #region Fields
        IExhibitionRepresentativeParticipatingStatusLogRepository IExhibitionRepresentativeParticipatingStatusLogRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionRepresentativeParticpatingStatusLogBL()
        {
            ConnectionString = null;
        }
        public ExhibitionRepresentativeParticpatingStatusLogBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionRepresentativeParticipatingStatusLogRepository != null)
                this.IExhibitionRepresentativeParticipatingStatusLogRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionRepresentativeParticipatingStatusLogRepository = new ExhibitionRepresentativeParticipatingStatusLogRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionRepresentativeParticipatingStatusLogs GetExhibitionRepresentativeParticpatingStatusLogByItemId(long itemId)
        {
            using (IExhibitionRepresentativeParticipatingStatusLogRepository = new ExhibitionRepresentativeParticipatingStatusLogRepository(ConnectionString))
            {
                return IExhibitionRepresentativeParticipatingStatusLogRepository.GetById(itemId);
            }
        }
        
        public List<XsiExhibitionRepresentativeParticipatingStatusLogs> GetExhibitionRepresentativeParticpatingStatusLog()
        {
            using (IExhibitionRepresentativeParticipatingStatusLogRepository = new ExhibitionRepresentativeParticipatingStatusLogRepository(ConnectionString))
            {
                return IExhibitionRepresentativeParticipatingStatusLogRepository.Select();
            }
        }
        public List<XsiExhibitionRepresentativeParticipatingStatusLogs> GetExhibitionRepresentativeParticpatingStatusLog(EnumSortlistBy sortListBy)
        {
            using (IExhibitionRepresentativeParticipatingStatusLogRepository = new ExhibitionRepresentativeParticipatingStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitionRepresentativeParticipatingStatusLogRepository.Select().OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitionRepresentativeParticipatingStatusLogRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionRepresentativeParticipatingStatusLogRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionRepresentativeParticipatingStatusLogRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionRepresentativeParticipatingStatusLogRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionRepresentativeParticipatingStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionRepresentativeParticipatingStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionRepresentativeParticipatingStatusLogs> GetExhibitionRepresentativeParticpatingStatusLog(XsiExhibitionRepresentativeParticipatingStatusLogs entity)
        {
            using (IExhibitionRepresentativeParticipatingStatusLogRepository = new ExhibitionRepresentativeParticipatingStatusLogRepository(ConnectionString))
            {
                return IExhibitionRepresentativeParticipatingStatusLogRepository.Select(entity);
            }
        }
        public List<XsiExhibitionRepresentativeParticipatingStatusLogs> GetExhibitionRepresentativeParticpatingStatusLog(XsiExhibitionRepresentativeParticipatingStatusLogs entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionRepresentativeParticipatingStatusLogRepository = new ExhibitionRepresentativeParticipatingStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitionRepresentativeParticipatingStatusLogRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitionRepresentativeParticipatingStatusLogRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionRepresentativeParticipatingStatusLogRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionRepresentativeParticipatingStatusLogRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionRepresentativeParticipatingStatusLogRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionRepresentativeParticipatingStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionRepresentativeParticipatingStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionRepresentativeParticipatingStatusLogs GetExhibitionRepresentativeParticpatingStatusLog(long groupId, long languageId)
        {
            using (IExhibitionRepresentativeParticipatingStatusLogRepository = new ExhibitionRepresentativeParticipatingStatusLogRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionRepresentativeParticpatingStatusLog(XsiExhibitionRepresentativeParticipatingStatusLogs entity)
        {
            using (IExhibitionRepresentativeParticipatingStatusLogRepository = new ExhibitionRepresentativeParticipatingStatusLogRepository(ConnectionString))
            {
                entity = IExhibitionRepresentativeParticipatingStatusLogRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId =  -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionRepresentativeParticpatingStatusLog(XsiExhibitionRepresentativeParticipatingStatusLogs entity)
        {
            XsiExhibitionRepresentativeParticipatingStatusLogs OriginalEntity = GetExhibitionRepresentativeParticpatingStatusLogByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IExhibitionRepresentativeParticipatingStatusLogRepository = new ExhibitionRepresentativeParticipatingStatusLogRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line
                    
                    IExhibitionRepresentativeParticipatingStatusLogRepository.Update(entity);
                    if (IExhibitionRepresentativeParticipatingStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionRepresentativeParticpatingStatusLogStatus(long itemId)
        {
            XsiExhibitionRepresentativeParticipatingStatusLogs entity = GetExhibitionRepresentativeParticpatingStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionRepresentativeParticipatingStatusLogRepository = new ExhibitionRepresentativeParticipatingStatusLogRepository(ConnectionString))
                {
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionRepresentativeParticipatingStatusLogRepository.Update(entity);
                    if (IExhibitionRepresentativeParticipatingStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionRepresentativeParticpatingStatusLog(XsiExhibitionRepresentativeParticipatingStatusLogs entity)
        {
            List<XsiExhibitionRepresentativeParticipatingStatusLogs> entities = GetExhibitionRepresentativeParticpatingStatusLog(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionRepresentativeParticipatingStatusLogs e in entities)
                {
                    result = DeleteExhibitionRepresentativeParticpatingStatusLog(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionRepresentativeParticpatingStatusLog(long itemId)
        {
            XsiExhibitionRepresentativeParticipatingStatusLogs entity = GetExhibitionRepresentativeParticpatingStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionRepresentativeParticipatingStatusLogRepository = new ExhibitionRepresentativeParticipatingStatusLogRepository(ConnectionString))
                {
                    IExhibitionRepresentativeParticipatingStatusLogRepository.Delete(itemId);
                    if (IExhibitionRepresentativeParticipatingStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
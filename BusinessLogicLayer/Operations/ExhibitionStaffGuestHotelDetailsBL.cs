﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionStaffGuestHotelDetailsBL : IDisposable
    {
        #region Fields
        IExhibitionStaffGuestHotelDetailsRepository IExhibitionStaffGuestHotelDetailsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionStaffGuestHotelDetailsBL()
        {
            ConnectionString = null;
        }
        public ExhibitionStaffGuestHotelDetailsBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionStaffGuestHotelDetailsRepository != null)
                this.IExhibitionStaffGuestHotelDetailsRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionStaffGuestHotelDetailsRepository = new ExhibitionStaffGuestHotelDetailRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionStaffGuestHotelDetails GetExhibitionStaffGuestHotelDetailsByItemId(long itemId)
        {
            using (IExhibitionStaffGuestHotelDetailsRepository = new ExhibitionStaffGuestHotelDetailRepository(ConnectionString))
            {
                return IExhibitionStaffGuestHotelDetailsRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionStaffGuestHotelDetails> GetExhibitionStaffGuestHotelDetails()
        {
            using (IExhibitionStaffGuestHotelDetailsRepository = new ExhibitionStaffGuestHotelDetailRepository(ConnectionString))
            {
                return IExhibitionStaffGuestHotelDetailsRepository.Select();
            }
        }
        public List<XsiExhibitionStaffGuestHotelDetails> GetExhibitionStaffGuestHotelDetails(EnumSortlistBy sortListBy)
        {
            using (IExhibitionStaffGuestHotelDetailsRepository = new ExhibitionStaffGuestHotelDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionStaffGuestHotelDetailsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionStaffGuestHotelDetailsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionStaffGuestHotelDetailsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionStaffGuestHotelDetailsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionStaffGuestHotelDetailsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionStaffGuestHotelDetails> GetExhibitionStaffGuestHotelDetails(XsiExhibitionStaffGuestHotelDetails entity)
        {
            using (IExhibitionStaffGuestHotelDetailsRepository = new ExhibitionStaffGuestHotelDetailRepository(ConnectionString))
            {
                return IExhibitionStaffGuestHotelDetailsRepository.Select(entity);
            }
        }
        public List<XsiExhibitionStaffGuestHotelDetails> GetExhibitionStaffGuestHotelDetails(XsiExhibitionStaffGuestHotelDetails entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionStaffGuestHotelDetailsRepository = new ExhibitionStaffGuestHotelDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionStaffGuestHotelDetailsRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionStaffGuestHotelDetailsRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionStaffGuestHotelDetailsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionStaffGuestHotelDetailsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionStaffGuestHotelDetailsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionStaffGuestHotelDetails GetExhibitionStaffGuestHotelDetails(long groupId, long languageId)
        {
            using (IExhibitionStaffGuestHotelDetailsRepository = new ExhibitionStaffGuestHotelDetailRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionStaffGuestHotelDetails(XsiExhibitionStaffGuestHotelDetails entity)
        {
            using (IExhibitionStaffGuestHotelDetailsRepository = new ExhibitionStaffGuestHotelDetailRepository(ConnectionString))
            {
                entity = IExhibitionStaffGuestHotelDetailsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionStaffGuestHotelDetails(XsiExhibitionStaffGuestHotelDetails entity)
        {
            var where = new XsiExhibitionStaffGuestHotelDetails();
            where.ItemId = entity.ItemId;
            XsiExhibitionStaffGuestHotelDetails OriginalEntity = GetExhibitionStaffGuestHotelDetails(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

            if (OriginalEntity != null)
            {
                using (IExhibitionStaffGuestHotelDetailsRepository = new ExhibitionStaffGuestHotelDetailRepository(ConnectionString))
                {

                    IExhibitionStaffGuestHotelDetailsRepository.Update(entity);
                    if (IExhibitionStaffGuestHotelDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackExhibitionStaffGuestHotelDetails(long itemId)
        {
            //if no RollbackXml propery in ExhibitionStaffGuestHotelDetails then delete below code and return EnumResultType.Invalid;

            return EnumResultType.Failed;

        }
        public EnumResultType UpdateExhibitionStaffGuestHotelDetailsStatus(long itemId)
        {
            XsiExhibitionStaffGuestHotelDetails entity = GetExhibitionStaffGuestHotelDetailsByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionStaffGuestHotelDetailsRepository = new ExhibitionStaffGuestHotelDetailRepository(ConnectionString))
                {
                   entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionStaffGuestHotelDetailsRepository.Update(entity);
                    if (IExhibitionStaffGuestHotelDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionStaffGuestHotelDetails(XsiExhibitionStaffGuestHotelDetails entity)
        {
            List<XsiExhibitionStaffGuestHotelDetails> entities = GetExhibitionStaffGuestHotelDetails(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionStaffGuestHotelDetails e in entities)
                {
                    result = DeleteExhibitionStaffGuestHotelDetails(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionStaffGuestHotelDetails(long itemId)
        {
            var where = new XsiExhibitionStaffGuestHotelDetails();
            where.ItemId = itemId;
            XsiExhibitionStaffGuestHotelDetails entity = GetExhibitionStaffGuestHotelDetails(where,EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            if (entity != null)
            {
                using (IExhibitionStaffGuestHotelDetailsRepository = new ExhibitionStaffGuestHotelDetailRepository(ConnectionString))
                {
                    IExhibitionStaffGuestHotelDetailsRepository.Delete(itemId);
                    if (IExhibitionStaffGuestHotelDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
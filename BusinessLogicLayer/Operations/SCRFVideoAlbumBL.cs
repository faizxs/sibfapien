﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFVideoAlbumBL : IDisposable
    {
        #region Feilds
        ISCRFVideoAlbumRepository SCRFVideoAlbumRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFVideoAlbumBL()
        {
            ConnectionString = null;
        }
        public SCRFVideoAlbumBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFVideoAlbumRepository != null)
                this.SCRFVideoAlbumRepository.Dispose();
        }
        #endregion
        #region Methods
       
        public long GetNextSortIndex()
        {
            //optional method. return -1 if not required and remove following
            using (SCRFVideoAlbumRepository = new SCRFVideoAlbumRepository(ConnectionString))
            {
                return (SCRFVideoAlbumRepository.Select().Max(p => p.SortIndex) ?? 0) + 1;
            }
        }

        public XsiScrfvideoAlbum GetSCRFVideoAlbumByItemId(long itemId)
        {
            using (SCRFVideoAlbumRepository = new SCRFVideoAlbumRepository(ConnectionString))
            {
                return SCRFVideoAlbumRepository.GetById(itemId);
            }
        }
         
        public List<XsiScrfvideoAlbum> GetSCRFVideoAlbum()
        {
            using (SCRFVideoAlbumRepository = new SCRFVideoAlbumRepository(ConnectionString))
            {
                return SCRFVideoAlbumRepository.Select();
            }
        }
        public List<XsiScrfvideoAlbum> GetSCRFVideoAlbum(EnumSortlistBy sortListBy)
        {
            using (SCRFVideoAlbumRepository = new SCRFVideoAlbumRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFVideoAlbumRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFVideoAlbumRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFVideoAlbumRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFVideoAlbumRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFVideoAlbumRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFVideoAlbumRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFVideoAlbumRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfvideoAlbum> GetSCRFVideoAlbum(XsiScrfvideoAlbum entity)
        {
            using (SCRFVideoAlbumRepository = new SCRFVideoAlbumRepository(ConnectionString))
            {
                return SCRFVideoAlbumRepository.Select(entity);
            }
        }
        public List<XsiScrfvideoAlbum> GetSCRFVideoAlbum(XsiScrfvideoAlbum entity, EnumSortlistBy sortListBy)
        {
            using (SCRFVideoAlbumRepository = new SCRFVideoAlbumRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFVideoAlbumRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFVideoAlbumRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFVideoAlbumRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFVideoAlbumRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFVideoAlbumRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFVideoAlbumRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.BySortOrderAsc:
                        return SCRFVideoAlbumRepository.Select(entity).OrderBy(p => p.SortIndex).ToList();

                    case EnumSortlistBy.BySortOrderDesc:
                        return SCRFVideoAlbumRepository.Select(entity).OrderByDescending(p => p.SortIndex).ToList();

                    default:
                        return SCRFVideoAlbumRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public EnumResultType InsertSCRFVideoAlbum(XsiScrfvideoAlbum entity)
        {
            using (SCRFVideoAlbumRepository = new SCRFVideoAlbumRepository(ConnectionString))
            {
                entity = SCRFVideoAlbumRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFVideoAlbum(XsiScrfvideoAlbum entity)
        {
            XsiScrfvideoAlbum OriginalEntity = GetSCRFVideoAlbumByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFVideoAlbumRepository = new SCRFVideoAlbumRepository(ConnectionString))
                {
                    SCRFVideoAlbumRepository.Update(entity);
                    if (SCRFVideoAlbumRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFVideoAlbumStatus(long itemId)
        {
            XsiScrfvideoAlbum entity = GetSCRFVideoAlbumByItemId(itemId);
            if (entity != null)
            {
                using (SCRFVideoAlbumRepository = new SCRFVideoAlbumRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFVideoAlbumRepository.Update(entity);
                    if (SCRFVideoAlbumRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFVideoAlbum(XsiScrfvideoAlbum entity)
        {
            List<XsiScrfvideoAlbum> entities = GetSCRFVideoAlbum(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfvideoAlbum e in entities)
                {
                    result = DeleteSCRFVideoAlbum(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFVideoAlbum(long itemId)
        {
            XsiScrfvideoAlbum entity = GetSCRFVideoAlbumByItemId(itemId);
            if (entity != null)
            {
                using (SCRFVideoAlbumRepository = new SCRFVideoAlbumRepository(ConnectionString))
                {
                    SCRFVideoAlbumRepository.Delete(itemId);
                    if (SCRFVideoAlbumRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
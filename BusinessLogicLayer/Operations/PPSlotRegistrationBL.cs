﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PPSlotRegistrationBL : IDisposable
    {
        #region Feilds
        IPPSlotRegistrationRepository IPPSlotRegistrationRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PPSlotRegistrationBL()
        {
            ConnectionString = null;
        }
        public PPSlotRegistrationBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IPPSlotRegistrationRepository != null)
                this.IPPSlotRegistrationRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionPpslotInvitation GetPPSlotRegistrationByItemId(long itemId)
        {
            using (IPPSlotRegistrationRepository = new PPSlotRegistrationRepository(ConnectionString))
            {
                return IPPSlotRegistrationRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionPpslotInvitation> GetPPSlotRegistration()
        {
            using (IPPSlotRegistrationRepository = new PPSlotRegistrationRepository(ConnectionString))
            {
                return IPPSlotRegistrationRepository.Select();
            }
        }
        public List<XsiExhibitionPpslotInvitation> GetPPSlotRegistration(EnumSortlistBy sortListBy)
        {
            using (IPPSlotRegistrationRepository = new PPSlotRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IPPSlotRegistrationRepository.Select().OrderBy(p => p.Status).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IPPSlotRegistrationRepository.Select().OrderByDescending(p => p.Status).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IPPSlotRegistrationRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IPPSlotRegistrationRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IPPSlotRegistrationRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IPPSlotRegistrationRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IPPSlotRegistrationRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionPpslotInvitation> GetPPSlotRegistration(XsiExhibitionPpslotInvitation entity)
        {
            using (IPPSlotRegistrationRepository = new PPSlotRegistrationRepository(ConnectionString))
            {
                return IPPSlotRegistrationRepository.Select(entity);
            }
        }
        public List<XsiExhibitionPpslotInvitation> GetPPSlotRegistration(XsiExhibitionPpslotInvitation entity, EnumSortlistBy sortListBy)
        {
            using (IPPSlotRegistrationRepository = new PPSlotRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IPPSlotRegistrationRepository.Select(entity).OrderBy(p => p.Status).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IPPSlotRegistrationRepository.Select(entity).OrderByDescending(p => p.Status).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IPPSlotRegistrationRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IPPSlotRegistrationRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IPPSlotRegistrationRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IPPSlotRegistrationRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IPPSlotRegistrationRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionPpslotInvitation GetPPSlotRegistration(long groupId, long languageId)
        {
            using (IPPSlotRegistrationRepository = new PPSlotRegistrationRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertPPSlotRegistration(XsiExhibitionPpslotInvitation entity)
        {
            using (IPPSlotRegistrationRepository = new PPSlotRegistrationRepository(ConnectionString))
            {
                entity = IPPSlotRegistrationRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePPSlotRegistration(XsiExhibitionPpslotInvitation entity)
        {
            XsiExhibitionPpslotInvitation OriginalEntity = GetPPSlotRegistrationByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IPPSlotRegistrationRepository = new PPSlotRegistrationRepository(ConnectionString))
                {
                    IPPSlotRegistrationRepository.Update(entity);
                    if (IPPSlotRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackPPSlotRegistration(long itemId)
        {
            return EnumResultType.Failed;

        }
        /*public EnumResultType UpdatePPSlotRegistrationStatus(long itemId)
        {
            XsiExhibitionPpslotInvitation entity = GetPPSlotRegistrationByItemId(itemId);
            if (entity != null)
            {
                using (IPPSlotRegistrationRepository = new PPSlotRegistrationRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IPPSlotRegistrationRepository.Update(entity);
                    if (IPPSlotRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }*/
        public EnumResultType DeletePPSlotRegistration(XsiExhibitionPpslotInvitation entity)
        {
            List<XsiExhibitionPpslotInvitation> entities = GetPPSlotRegistration(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionPpslotInvitation e in entities)
                {
                    result = DeletePPSlotRegistration(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeletePPSlotRegistration(long itemId)
        {
            XsiExhibitionPpslotInvitation entity = GetPPSlotRegistrationByItemId(itemId);
            if (entity != null)
            {
                using (IPPSlotRegistrationRepository = new PPSlotRegistrationRepository(ConnectionString))
                {
                    IPPSlotRegistrationRepository.Delete(itemId);
                    if (IPPSlotRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ActivitiesSuggestionBL : IDisposable
    {
        #region Feilds
        IActivitiesSuggestionRepository ActivitiesSuggestionRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ActivitiesSuggestionBL()
        {
            ConnectionString = null;
        }
        public ActivitiesSuggestionBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ActivitiesSuggestionRepository != null)
                this.ActivitiesSuggestionRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiActivitiesSuggestion GetActivitiesSuggestionByItemId(long itemId)
        {
            using (ActivitiesSuggestionRepository = new ActivitiesSuggestionRepository(ConnectionString))
            {
                return ActivitiesSuggestionRepository.GetById(itemId);
            }
        }

        public List<XsiActivitiesSuggestion> GetActivitiesSuggestion()
        {
            using (ActivitiesSuggestionRepository = new ActivitiesSuggestionRepository(ConnectionString))
            {
                return ActivitiesSuggestionRepository.Select();
            }
        }
        public List<XsiActivitiesSuggestion> GetActivitiesSuggestion(EnumSortlistBy sortListBy)
        {
            using (ActivitiesSuggestionRepository = new ActivitiesSuggestionRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ActivitiesSuggestionRepository.Select().OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ActivitiesSuggestionRepository.Select().OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ActivitiesSuggestionRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ActivitiesSuggestionRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ActivitiesSuggestionRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ActivitiesSuggestionRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ActivitiesSuggestionRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiActivitiesSuggestion> GetActivitiesSuggestion(XsiActivitiesSuggestion entity)
        {
            using (ActivitiesSuggestionRepository = new ActivitiesSuggestionRepository(ConnectionString))
            {
                return ActivitiesSuggestionRepository.Select(entity).ToList();
            }
        }
        public List<XsiActivitiesSuggestion> GetActivitiesSuggestion(XsiActivitiesSuggestion entity, EnumSortlistBy sortListBy)
        {
            using (ActivitiesSuggestionRepository = new ActivitiesSuggestionRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ActivitiesSuggestionRepository.Select(entity).ToList().OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ActivitiesSuggestionRepository.Select(entity).ToList().OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ActivitiesSuggestionRepository.Select(entity).ToList().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ActivitiesSuggestionRepository.Select(entity).ToList().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ActivitiesSuggestionRepository.Select(entity).ToList().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ActivitiesSuggestionRepository.Select(entity).ToList().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ActivitiesSuggestionRepository.Select(entity).ToList().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiActivitiesSuggestion> GetActivitiesSuggestionOr(XsiActivitiesSuggestion entity, EnumSortlistBy sortListBy)
        {
            using (ActivitiesSuggestionRepository = new ActivitiesSuggestionRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ActivitiesSuggestionRepository.SelectOr(entity).ToList().OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ActivitiesSuggestionRepository.SelectOr(entity).ToList().OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ActivitiesSuggestionRepository.SelectOr(entity).ToList().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ActivitiesSuggestionRepository.SelectOr(entity).ToList().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ActivitiesSuggestionRepository.SelectOr(entity).ToList().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ActivitiesSuggestionRepository.SelectOr(entity).ToList().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ActivitiesSuggestionRepository.SelectOr(entity).ToList().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertActivitiesSuggestion(XsiActivitiesSuggestion entity)
        {
            using (ActivitiesSuggestionRepository = new ActivitiesSuggestionRepository(ConnectionString))
            {
                entity = ActivitiesSuggestionRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId =  -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateActivitiesSuggestion(XsiActivitiesSuggestion entity)
        {
            XsiActivitiesSuggestion OriginalEntity = GetActivitiesSuggestionByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ActivitiesSuggestionRepository = new ActivitiesSuggestionRepository(ConnectionString))
                {
                    ActivitiesSuggestionRepository.Update(entity);
                    if (ActivitiesSuggestionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateActivitiesSuggestionStatus(long itemId)
        {
            XsiActivitiesSuggestion entity = GetActivitiesSuggestionByItemId(itemId);
            if (entity != null)
            {
                using (ActivitiesSuggestionRepository = new ActivitiesSuggestionRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ActivitiesSuggestionRepository.Update(entity);
                    if (ActivitiesSuggestionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteActivitiesSuggestion(XsiActivitiesSuggestion entity)
        {
            List<XsiActivitiesSuggestion> entities = GetActivitiesSuggestion(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiActivitiesSuggestion e in entities)
                {
                    result = DeleteActivitiesSuggestion(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteActivitiesSuggestion(long itemId)
        {
            XsiActivitiesSuggestion entity = GetActivitiesSuggestionByItemId(itemId);
            if (entity != null)
            {
                using (ActivitiesSuggestionRepository = new ActivitiesSuggestionRepository(ConnectionString))
                {
                    ActivitiesSuggestionRepository.Delete(itemId);
                    if (ActivitiesSuggestionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
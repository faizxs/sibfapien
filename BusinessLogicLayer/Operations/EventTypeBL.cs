﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class EventTypeBL : IDisposable
    {
        #region Feilds
        IEventTypeRepository EventTypeRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public EventTypeBL()
        {
            ConnectionString = null;
        }
        public EventTypeBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventTypeRepository != null)
                this.EventTypeRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (EventTypeRepository = new EventTypeRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiEventType GetEventTypeByItemId(long itemId)
        {
            using (EventTypeRepository = new EventTypeRepository(ConnectionString))
            {
                return EventTypeRepository.GetById(itemId);
            }
        }
         
        public List<XsiEventType> GetEventType()
        {
            using (EventTypeRepository = new EventTypeRepository(ConnectionString))
            {
                return EventTypeRepository.Select();
            }
        }
        public List<XsiEventType> GetEventType(EnumSortlistBy sortListBy)
        {
            using (EventTypeRepository = new EventTypeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return EventTypeRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return EventTypeRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return EventTypeRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventTypeRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventTypeRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventTypeRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventTypeRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEventType> GetEventType(XsiEventType entity)
        {
            using (EventTypeRepository = new EventTypeRepository(ConnectionString))
            {
                return EventTypeRepository.Select(entity);
            }
        }
        public List<XsiEventType> GetEventType(XsiEventType entity, EnumSortlistBy sortListBy)
        {
            using (EventTypeRepository = new EventTypeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return EventTypeRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return EventTypeRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return EventTypeRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventTypeRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventTypeRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventTypeRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventTypeRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiEventType GetEventType(long groupId, long languageId)
        {
            using (EventTypeRepository = new EventTypeRepository(ConnectionString))
            {
                return null;
            }
        }
        public EnumResultType InsertEventType(XsiEventType entity)
        {
            using (EventTypeRepository = new EventTypeRepository(ConnectionString))
            {
                entity = EventTypeRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateEventType(XsiEventType entity)
        {
            XsiEventType OriginalEntity = GetEventTypeByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (EventTypeRepository = new EventTypeRepository(ConnectionString))
                {
                    EventTypeRepository.Update(entity);
                    if (EventTypeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateEventTypeStatus(long itemId)
        {
            XsiEventType entity = GetEventTypeByItemId(itemId);
            if (entity != null)
            {
                using (EventTypeRepository = new EventTypeRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    EventTypeRepository.Update(entity);
                    if (EventTypeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteEventType(XsiEventType entity)
        {
            List<XsiEventType> entities = GetEventType(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiEventType e in entities)
                {
                    result = DeleteEventType(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteEventType(long itemId)
        {
            XsiEventType entity = GetEventTypeByItemId(itemId);
            if (entity != null)
            {
                using (EventTypeRepository = new EventTypeRepository(ConnectionString))
                {
                    EventTypeRepository.Delete(itemId);
                    if (EventTypeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
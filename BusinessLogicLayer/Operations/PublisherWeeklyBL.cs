﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PublisherWeeklyBL : IDisposable
    {
        #region Feilds
        IPublisherWeeklyRepository PublisherWeeklyRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PublisherWeeklyBL()
        {
            ConnectionString = null;
        }
        public PublisherWeeklyBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PublisherWeeklyRepository != null)
                this.PublisherWeeklyRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiPublisherWeekly GetPublisherWeeklyByItemId(long itemId)
        {
            using (PublisherWeeklyRepository = new PublisherWeeklyRepository(ConnectionString))
            {
                return PublisherWeeklyRepository.GetById(itemId);
            }
        }
        
        public List<XsiPublisherWeekly> GetPublisherWeekly()
        {
            using (PublisherWeeklyRepository = new PublisherWeeklyRepository(ConnectionString))
            {
                return PublisherWeeklyRepository.Select();
            }
        }
        public List<XsiPublisherWeekly> GetPublisherWeekly(EnumSortlistBy sortListBy)
        {
            using (PublisherWeeklyRepository = new PublisherWeeklyRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PublisherWeeklyRepository.Select().OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PublisherWeeklyRepository.Select().OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PublisherWeeklyRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PublisherWeeklyRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PublisherWeeklyRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PublisherWeeklyRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PublisherWeeklyRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPublisherWeekly> GetPublisherWeekly(XsiPublisherWeekly entity)
        {
            using (PublisherWeeklyRepository = new PublisherWeeklyRepository(ConnectionString))
            {
                return PublisherWeeklyRepository.Select(entity);
            }
        }
        public List<XsiPublisherWeekly> GetPublisherWeekly(XsiPublisherWeekly entity, EnumSortlistBy sortListBy)
        {
            using (PublisherWeeklyRepository = new PublisherWeeklyRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PublisherWeeklyRepository.Select(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PublisherWeeklyRepository.Select(entity).OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PublisherWeeklyRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PublisherWeeklyRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PublisherWeeklyRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PublisherWeeklyRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PublisherWeeklyRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPublisherWeekly> GetPublisherWeeklyOr(XsiPublisherWeekly entity, EnumSortlistBy sortListBy)
        {
            using (PublisherWeeklyRepository = new PublisherWeeklyRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PublisherWeeklyRepository.SelectOr(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PublisherWeeklyRepository.SelectOr(entity).OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PublisherWeeklyRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PublisherWeeklyRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PublisherWeeklyRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PublisherWeeklyRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PublisherWeeklyRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public EnumResultType InsertPublisherWeekly(XsiPublisherWeekly entity)
        {
            using (PublisherWeeklyRepository = new PublisherWeeklyRepository(ConnectionString))
            {
                entity = PublisherWeeklyRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePublisherWeekly(XsiPublisherWeekly entity)
        {
            XsiPublisherWeekly OriginalEntity = GetPublisherWeeklyByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PublisherWeeklyRepository = new PublisherWeeklyRepository(ConnectionString))
                {
                    PublisherWeeklyRepository.Update(entity);
                    if (PublisherWeeklyRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdatePublisherWeeklytatus(long itemId)
        {
            XsiPublisherWeekly entity = GetPublisherWeeklyByItemId(itemId);
            if (entity != null)
            {
                using (PublisherWeeklyRepository = new PublisherWeeklyRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    PublisherWeeklyRepository.Update(entity);
                    if (PublisherWeeklyRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePublisherWeekly(XsiPublisherWeekly entity)
        {
            List<XsiPublisherWeekly> entities = GetPublisherWeekly(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiPublisherWeekly e in entities)
                {
                    result = DeletePublisherWeekly(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeletePublisherWeekly(long itemId)
        {
            XsiPublisherWeekly entity = GetPublisherWeeklyByItemId(itemId);
            if (entity != null)
            {
                using (PublisherWeeklyRepository = new PublisherWeeklyRepository(ConnectionString))
                {
                    PublisherWeeklyRepository.Delete(itemId);
                    if (PublisherWeeklyRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
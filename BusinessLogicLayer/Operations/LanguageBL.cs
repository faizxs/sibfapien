﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class LanguageBL : IDisposable
    {
        #region Feilds
        ILanguageRepository XsiLanguageRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public LanguageBL()
        {
            ConnectionString = null;
        }
        public LanguageBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiLanguageRepository != null)
                this.XsiLanguageRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiLanguages GetLanguageByItemId(long itemId)
        {
            using (XsiLanguageRepository = new LanguageRepository())
            {
                return XsiLanguageRepository.GetById(itemId);
            }
        }
        public XsiLanguages GetDefaultLanguage()
        {
            return GetLanguage(new XsiLanguages() { IsDefault = EnumConversion.ToString(EnumBool.Yes) }).FirstOrDefault();
        }
        public bool IsLanguageDirectionLeft(long itemId)
        {
            XsiLanguages lang = GetLanguageByItemId(itemId);
            if (lang != null)
                return lang.Direction.ToLower().Equals("ltr");

            return true;
        }

        public List<XsiLanguages> GetLanguage(EnumSortlistBy sortListBy)
        {
            using (XsiLanguageRepository = new LanguageRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiLanguageRepository.Select().OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiLanguageRepository.Select().OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiLanguageRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiLanguageRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiLanguageRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiLanguages> GetLanguage(XsiLanguages entity)
        {
            using (XsiLanguageRepository = new LanguageRepository(ConnectionString))
            {
                return XsiLanguageRepository.Select(entity);
            }
        }
        public List<XsiLanguages> GetLanguage(XsiLanguages entity, EnumSortlistBy sortListBy)
        {
            using (XsiLanguageRepository = new LanguageRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiLanguageRepository.Select(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiLanguageRepository.Select(entity).OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiLanguageRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiLanguageRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiLanguageRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiLanguages> GetActiveLanguages()
        {
            string isActive = EnumConversion.ToString(EnumBool.Yes);
            XsiLanguageRepository = new LanguageRepository(ConnectionString);
            return XsiLanguageRepository.Select().Where(p => p.IsActive == isActive).ToList();
        }

        public EnumResultType InsertLanguage(XsiLanguages entity)
        {
            using (XsiLanguageRepository = new LanguageRepository(ConnectionString))
            {
                entity = XsiLanguageRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateLanguage(XsiLanguages entity)
        {
            if (GetLanguageByItemId(entity.ItemId) != null)
            {
                using (XsiLanguageRepository = new LanguageRepository(ConnectionString))
                {
                    XsiLanguageRepository.Update(entity);
                    if (XsiLanguageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateLanguageStatus(long itemId)
        {
            XsiLanguages entity = GetLanguageByItemId(itemId);
            if (entity != null)
            {
                using (XsiLanguageRepository = new LanguageRepository(ConnectionString))
                {
                    entity.IsActive = entity.IsActive == EnumConversion.ToString(EnumBool.Yes) ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    XsiLanguageRepository.Update(entity);
                    if (XsiLanguageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateDefaultLanguage(long itemId)
        {
            XsiLanguages entity = GetLanguageByItemId(itemId);
            if (entity != null)
            {
                using (XsiLanguageRepository = new LanguageRepository(ConnectionString))
                {
                    entity.IsDefault = entity.IsDefault == EnumConversion.ToString(EnumBool.Yes) ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    XsiLanguageRepository.Update(entity);
                    if (XsiLanguageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteLanguage(XsiLanguages entity)
        {
            if (GetLanguage(entity).Count() > 0)
            {
                using (XsiLanguageRepository = new LanguageRepository(ConnectionString))
                {
                    XsiLanguageRepository.Delete(entity);
                    if (XsiLanguageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteLanguage(long itemId)
        {
            if (GetLanguageByItemId(itemId) != null)
            {
                using (XsiLanguageRepository = new LanguageRepository(ConnectionString))
                {
                    XsiLanguageRepository.Delete(itemId);
                    if (XsiLanguageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
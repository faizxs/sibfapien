﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFIconsForSponsorBL : IDisposable
    {
        #region Feilds
        ISCRFIconsForSponsorRepository SCRFIconsForSponsorRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFIconsForSponsorBL()
        {
            ConnectionString = null;
        }
        public SCRFIconsForSponsorBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIconsForSponsorRepository != null)
                this.SCRFIconsForSponsorRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrficonsForSponsor GetSCRFIconsForSponsorByItemId(long itemId)
        {
            using (SCRFIconsForSponsorRepository = new SCRFIconsForSponsorRepository(ConnectionString))
            {
                return SCRFIconsForSponsorRepository.GetById(itemId);
            }
        }

        public List<XsiScrficonsForSponsor> GetSCRFIconsForSponsor()
        {
            using (SCRFIconsForSponsorRepository = new SCRFIconsForSponsorRepository(ConnectionString))
            {
                return SCRFIconsForSponsorRepository.Select();
            }
        }
        public List<XsiScrficonsForSponsor> GetSCRFIconsForSponsor(EnumSortlistBy sortListBy)
        {
            using (SCRFIconsForSponsorRepository = new SCRFIconsForSponsorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIconsForSponsorRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIconsForSponsorRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFIconsForSponsorRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFIconsForSponsorRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIconsForSponsorRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIconsForSponsorRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIconsForSponsorRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrficonsForSponsor> GetSCRFIconsForSponsor(XsiScrficonsForSponsor entity)
        {
            using (SCRFIconsForSponsorRepository = new SCRFIconsForSponsorRepository(ConnectionString))
            {
                return SCRFIconsForSponsorRepository.Select(entity);
            }
        }
        public List<XsiScrficonsForSponsor> GetSCRFIconsForSponsor(XsiScrficonsForSponsor entity, EnumSortlistBy sortListBy)
        {
            using (SCRFIconsForSponsorRepository = new SCRFIconsForSponsorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIconsForSponsorRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIconsForSponsorRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFIconsForSponsorRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFIconsForSponsorRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIconsForSponsorRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIconsForSponsorRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIconsForSponsorRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrficonsForSponsor> GetSCRFIconsForSponsorOr(XsiScrficonsForSponsor entity, EnumSortlistBy sortListBy)
        {
            using (SCRFIconsForSponsorRepository = new SCRFIconsForSponsorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIconsForSponsorRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIconsForSponsorRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFIconsForSponsorRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFIconsForSponsorRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIconsForSponsorRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIconsForSponsorRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIconsForSponsorRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFIconsForSponsor(XsiScrficonsForSponsor entity)
        {
            using (SCRFIconsForSponsorRepository = new SCRFIconsForSponsorRepository(ConnectionString))
            {
                entity = SCRFIconsForSponsorRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFIconsForSponsor(XsiScrficonsForSponsor entity)
        {
            XsiScrficonsForSponsor OriginalEntity = GetSCRFIconsForSponsorByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFIconsForSponsorRepository = new SCRFIconsForSponsorRepository(ConnectionString))
                {
                    SCRFIconsForSponsorRepository.Update(entity);
                    if (SCRFIconsForSponsorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFIconsForSponsorStatus(long itemId)
        {
            XsiScrficonsForSponsor entity = GetSCRFIconsForSponsorByItemId(itemId);
            if (entity != null)
            {
                using (SCRFIconsForSponsorRepository = new SCRFIconsForSponsorRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFIconsForSponsorRepository.Update(entity);
                    if (SCRFIconsForSponsorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFIconsForSponsor(XsiScrficonsForSponsor entity)
        {
            List<XsiScrficonsForSponsor> entities = GetSCRFIconsForSponsor(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrficonsForSponsor e in entities)
                {
                    result = DeleteSCRFIconsForSponsor(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFIconsForSponsor(long itemId)
        {
            XsiScrficonsForSponsor entity = GetSCRFIconsForSponsorByItemId(itemId);
            if (entity != null)
            {
                using (SCRFIconsForSponsorRepository = new SCRFIconsForSponsorRepository(ConnectionString))
                {
                    SCRFIconsForSponsorRepository.Delete(itemId);
                    if (SCRFIconsForSponsorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
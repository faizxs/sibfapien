﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class WebsiteBL : IDisposable
    {
        #region Feilds
        IWebsiteRepository WebsiteRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public WebsiteBL()
        {
            ConnectionString = null;
        }
        public WebsiteBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.WebsiteRepository != null)
                this.WebsiteRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiWebsites GetWebsiteByItemId(long itemId)
        {
            using (WebsiteRepository = new WebsiteRepository(ConnectionString))
            {
                return WebsiteRepository.GetById(itemId);
            }
        }
         
        public List<XsiWebsites> GetWebsite()
        {
            using (WebsiteRepository = new WebsiteRepository(ConnectionString))
            {
                return WebsiteRepository.Select();
            }
        }
        public List<XsiWebsites> GetWebsite(EnumSortlistBy sortListBy)
        {
            using (WebsiteRepository = new WebsiteRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return WebsiteRepository.Select().OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return WebsiteRepository.Select().OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return WebsiteRepository.Select().OrderBy(p => p.WebsiteId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return WebsiteRepository.Select().OrderByDescending(p => p.WebsiteId).ToList();

                    default:
                        return WebsiteRepository.Select().OrderByDescending(p => p.WebsiteId).ToList();
                }
            }
        }
        public List<XsiWebsites> GetWebsite(XsiWebsites entity)
        {
            using (WebsiteRepository = new WebsiteRepository(ConnectionString))
            {
                return WebsiteRepository.Select(entity);
            }
        }
        public List<XsiWebsites> GetWebsite(XsiWebsites entity, EnumSortlistBy sortListBy)
        {
            using (WebsiteRepository = new WebsiteRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return WebsiteRepository.Select(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return WebsiteRepository.Select(entity).OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return WebsiteRepository.Select(entity).OrderBy(p => p.WebsiteId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return WebsiteRepository.Select(entity).OrderByDescending(p => p.WebsiteId).ToList();

                    default:
                        return WebsiteRepository.Select(entity).OrderByDescending(p => p.WebsiteId).ToList();
                }
            }
        }

        public EnumResultType InsertWebsite(XsiWebsites entity)
        {
            using (WebsiteRepository = new WebsiteRepository(ConnectionString))
            {
                entity = WebsiteRepository.Add(entity);
                if (entity.WebsiteId > 0)
                {
                    XsiItemdId = entity.WebsiteId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateWebsite(XsiWebsites entity)
        {
            XsiWebsites OriginalEntity = GetWebsiteByItemId(entity.WebsiteId);

            if (OriginalEntity != null)
            {
                using (WebsiteRepository = new WebsiteRepository(ConnectionString))
                {

                    WebsiteRepository.Update(entity);
                    if (WebsiteRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteWebsite(XsiWebsites entity)
        {
            List<XsiWebsites> entities = GetWebsite(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiWebsites e in entities)
                {
                    result = DeleteWebsite(e.WebsiteId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteWebsite(long itemId)
        {
            XsiWebsites entity = GetWebsiteByItemId(itemId);
            if (entity != null)
            {
                using (WebsiteRepository = new WebsiteRepository(ConnectionString))
                {
                    WebsiteRepository.Delete(itemId);
                    if (WebsiteRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
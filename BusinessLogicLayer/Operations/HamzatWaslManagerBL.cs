﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class HamzatWaslBL : IDisposable
    {
        #region Feilds
        IHamzatWaslRepository HamzatWaslRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public HamzatWaslBL()
        {
            ConnectionString = null;
        }
        public HamzatWaslBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HamzatWaslRepository != null)
                this.HamzatWaslRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiHamzatWasl GetHamzatWaslByItemId(long itemId)
        {
            using (HamzatWaslRepository = new HamzatWaslRepository(ConnectionString))
            {
                return HamzatWaslRepository.GetById(itemId);
            }
        }

        public List<XsiHamzatWasl> GetHamzatWasl()
        {
            using (HamzatWaslRepository = new HamzatWaslRepository(ConnectionString))
            {
                return HamzatWaslRepository.Select();
            }
        }
        public List<XsiHamzatWasl> GetHamzatWasl(EnumSortlistBy sortListBy)
        {
            using (HamzatWaslRepository = new HamzatWaslRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HamzatWaslRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HamzatWaslRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HamzatWaslRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HamzatWaslRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HamzatWaslRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HamzatWaslRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HamzatWaslRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiHamzatWasl> GetHamzatWasl(XsiHamzatWasl entity)
        {
            using (HamzatWaslRepository = new HamzatWaslRepository(ConnectionString))
            {
                return HamzatWaslRepository.Select(entity);
            }
        }
        public List<XsiHamzatWasl> GetHamzatWasl(XsiHamzatWasl entity, EnumSortlistBy sortListBy)
        {
            using (HamzatWaslRepository = new HamzatWaslRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HamzatWaslRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HamzatWaslRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HamzatWaslRepository.Select(entity).OrderBy(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HamzatWaslRepository.Select(entity).OrderByDescending(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HamzatWaslRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HamzatWaslRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HamzatWaslRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiHamzatWasl> GetHamzatWaslOr(XsiHamzatWasl entity, EnumSortlistBy sortListBy)
        {
            using (HamzatWaslRepository = new HamzatWaslRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HamzatWaslRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HamzatWaslRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HamzatWaslRepository.SelectOr(entity).OrderBy(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HamzatWaslRepository.SelectOr(entity).OrderByDescending(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HamzatWaslRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HamzatWaslRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HamzatWaslRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertHamzatWasl(XsiHamzatWasl entity)
        {
            using (HamzatWaslRepository = new HamzatWaslRepository(ConnectionString))
            {
                entity = HamzatWaslRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateHamzatWasl(XsiHamzatWasl entity)
        {
            XsiHamzatWasl OriginalEntity = GetHamzatWaslByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (HamzatWaslRepository = new HamzatWaslRepository(ConnectionString))
                {
                    HamzatWaslRepository.Update(entity);
                    if (HamzatWaslRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateHamzatWaslStatus(long itemId)
        {
            XsiHamzatWasl entity = GetHamzatWaslByItemId(itemId);
            if (entity != null)
            {
                using (HamzatWaslRepository = new HamzatWaslRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    HamzatWaslRepository.Update(entity);
                    if (HamzatWaslRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteHamzatWasl(XsiHamzatWasl entity)
        {
            List<XsiHamzatWasl> entities = GetHamzatWasl(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiHamzatWasl e in entities)
                {
                    result = DeleteHamzatWasl(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteHamzatWasl(long itemId)
        {
            XsiHamzatWasl entity = GetHamzatWaslByItemId(itemId);
            if (entity != null)
            {
                using (HamzatWaslRepository = new HamzatWaslRepository(ConnectionString))
                {
                    HamzatWaslRepository.Delete(itemId);
                    if (HamzatWaslRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;
using System.Web;

namespace Xsi.BusinessLogicLayer
{
    public class BookBL : IDisposable
    {
        #region Feilds
        IBookRepository BookRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public BookBL()
        {
            ConnectionString = null;
        }
        public BookBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.BookRepository != null)
                this.BookRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public long GetNextClickCount(long itemId)
        {
            //optional method. return -1 if not required and remove following
            using (BookRepository = new BookRepository(ConnectionString))
            {
                return (BookRepository.GetById(itemId).ClickCount ?? 0) + 1;
            }
        }

        public XsiExhibitionBooks GetBookByItemId(long itemId)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                return BookRepository.GetById(itemId);
            }
        }
        
        public List<XsiExhibitionBooks> GetBook()
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                return BookRepository.Select();
            }
        }
        public List<XsiExhibitionBooks> GetBook(EnumSortlistBy sortListBy)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return BookRepository.Select().OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return BookRepository.Select().OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return BookRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return BookRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return BookRepository.Select().OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return BookRepository.Select().OrderByDescending(p => p.BookId).ToList();

                    default:
                        return BookRepository.Select().OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
        public List<XsiExhibitionBooks> GetBook(XsiExhibitionBooks entity)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                return BookRepository.Select(entity);
            }
        }

        public List<XsiExhibitionBooks> GetXsiExhibitionBooksByExhibitorId(long ehibitorId)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                return BookRepository.GetXsiExhibitionBooksByExhibitorId(ehibitorId);
            }
        }

        public List<XsiExhibitionBookForUi> GetBookForUI(XsiExhibitionBooks entity, EnumSortlistBy sortListBy)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return BookRepository.SelectBooksForUI(entity).OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return BookRepository.SelectBooksForUI(entity).OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return BookRepository.SelectBooksForUI(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return BookRepository.SelectBooksForUI(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return BookRepository.SelectBooksForUI(entity).OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return BookRepository.SelectBooksForUI(entity).OrderByDescending(p => p.BookId).ToList();

                    default:
                        return BookRepository.SelectBooksForUI(entity).OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
        public List<XsiExhibitionBookForUi> GetBookUIAnd(XsiExhibitionBooks entity, EnumSortlistBy sortListBy, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return BookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return BookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return BookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return BookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return BookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return BookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.BookId).ToList();

                    default:
                        return BookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
        public List<XsiExhibitionBookForUi> GetBookUIOr(XsiExhibitionBooks entity, EnumSortlistBy sortListBy, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return BookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return BookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return BookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return BookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return BookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return BookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.BookId).ToList();

                    default:
                        return BookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
        public List<XsiExhibitionBookForUi> GetBookUITopSearchable(XsiExhibitionBooks entity, long websiteId)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                return BookRepository.SelectBooksUITopSearchable(entity, websiteId);
            }
        }
        public Array GetBooksTitle(XsiExhibitionBooks entity, long websiteId, EnumSortlistBy sortListBy)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return BookRepository.SelectBooksTitle(entity, websiteId).OrderBy(p => p.TitleEn).ToList().ToArray();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return BookRepository.SelectBooksTitle(entity, websiteId).OrderByDescending(p => p.TitleEn).ToList().ToArray();

                    case EnumSortlistBy.ByItemIdAsc:
                        return BookRepository.SelectBooksTitle(entity, websiteId).OrderBy(p => p.BookId).ToList().ToArray();

                    case EnumSortlistBy.ByItemIdDesc:
                        return BookRepository.SelectBooksTitle(entity, websiteId).OrderByDescending(p => p.BookId).ToList().ToArray();

                    default:
                        return BookRepository.SelectBooksTitle(entity, websiteId).OrderByDescending(p => p.BookId).ToList().ToArray();
                }
            }
        }
        public List<XsiExhibitionBookForUi> GetBooksSCRFTitle(XsiExhibitionBooks entity, long websiteId, EnumSortlistBy sortListBy)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return BookRepository.SelectBooksTitle(entity, websiteId).OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return BookRepository.SelectBooksTitle(entity, websiteId).OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return BookRepository.SelectBooksTitle(entity, websiteId).OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return BookRepository.SelectBooksTitle(entity, websiteId).OrderByDescending(p => p.BookId).ToList();

                    default:
                        return BookRepository.SelectBooksTitle(entity, websiteId).OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
        public List<XsiExhibitionBooks> GetBook(XsiExhibitionBooks entity, EnumSortlistBy sortListBy)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return BookRepository.Select(entity).OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return BookRepository.Select(entity).OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return BookRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return BookRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return BookRepository.Select(entity).OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return BookRepository.Select(entity).OrderByDescending(p => p.BookId).ToList();

                    default:
                        return BookRepository.Select(entity).OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
         
        public List<GetBooksToExport_Result> GetBooksToExport(string bookIds, string languageId)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                return BookRepository.SelectBooksToExport(bookIds, languageId);
            }
        }
        public void UpdatePublisherId(long? oldPublisherID, long? newPublisherID)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                BookRepository.UpdatePublisherId(oldPublisherID, newPublisherID);
            }
        }
        public void UpdateAuthorId(long? oldAuthorID, long? newAuthorID)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                BookRepository.UpdateAuthorId(oldAuthorID, newAuthorID);
            }
        }

        public EnumResultType InsertBook(XsiExhibitionBooks entity)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                entity = BookRepository.Add(entity);
                if (entity.BookId > 0)
                {
                    XsiItemdId = entity.BookId;
                    
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateBook(XsiExhibitionBooks entity)
        {
            XsiExhibitionBooks OriginalEntity = GetBookByItemId(entity.BookId);

            if (OriginalEntity != null)
            {
                using (BookRepository = new BookRepository(ConnectionString))
                {
                    BookRepository.Update(entity);
                    if (BookRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateBookStatus(long itemId)
        {
            XsiExhibitionBooks entity = GetBookByItemId(itemId);
            if (entity != null)
            {
                using (BookRepository = new BookRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    BookRepository.Update(entity);
                    if (BookRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteBook(XsiExhibitionBooks entity)
        {
            List<XsiExhibitionBooks> entities = GetBook(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionBooks e in entities)
                {
                    result = DeleteBook(e.BookId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteBook(long itemId)
        {
            XsiExhibitionBooks entity = GetBookByItemId(itemId);
            if (entity != null)
            {
                using (BookRepository = new BookRepository(ConnectionString))
                {
                    BookRepository.Delete(itemId);
                    if (BookRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public List<XsiExhibitionBookForUi> GetBookUIAnd1(XsiExhibitionBooks entity, EnumSortlistBy sortListBy, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return BookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return BookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return BookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return BookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return BookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return BookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.BookId).ToList();

                    default:
                        return BookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.BookId).ToList();
                }
            }
        }

        public void UpdateAuthorInBooks(long oldAuthorId, long newAuthorId, long AdminUserId)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                BookRepository.UpdateAuthorInBooks(oldAuthorId, newAuthorId, AdminUserId);
            }
        }

        public void UpdatePublisherInBooks(long oldPublisherId, long newPublisherId, long AdminUserId)
        {
            using (BookRepository = new BookRepository(ConnectionString))
            {
                BookRepository.UpdatePublisherInBooks(oldPublisherId, newPublisherId, AdminUserId);
            }
        }
        #endregion
    }
}
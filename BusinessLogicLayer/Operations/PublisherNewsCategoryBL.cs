﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PublisherNewsCategoryBL : IDisposable
    {
        #region Feilds
        IPublisherNewsCategoryRepository PublisherNewsCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PublisherNewsCategoryBL()
        {
            ConnectionString = null;
        }
        public PublisherNewsCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PublisherNewsCategoryRepository != null)
                this.PublisherNewsCategoryRepository.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiPublisherNewsCategory GetPublisherNewsCategoryByItemId(long itemId)
        {
            using (PublisherNewsCategoryRepository = new PublisherNewsCategoryRepository(ConnectionString))
            {
                return PublisherNewsCategoryRepository.GetById(itemId);
            }
        }
     
        public List<XsiPublisherNewsCategory> GetPublisherNewsCategory()
        {
            using (PublisherNewsCategoryRepository = new PublisherNewsCategoryRepository(ConnectionString))
            {
                return PublisherNewsCategoryRepository.Select();
            }
        }
        public List<XsiPublisherNewsCategory> GetPublisherNewsCategory(EnumSortlistBy sortListBy)
        {
            using (PublisherNewsCategoryRepository = new PublisherNewsCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PublisherNewsCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PublisherNewsCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    ////case EnumSortlistBy.ByDateAsc:
                    ////    return PublisherNewsCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    ////case EnumSortlistBy.ByDateDesc:
                    ////    return PublisherNewsCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PublisherNewsCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PublisherNewsCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PublisherNewsCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPublisherNewsCategory> GetPublisherNewsCategory(XsiPublisherNewsCategory entity)
        {
            using (PublisherNewsCategoryRepository = new PublisherNewsCategoryRepository(ConnectionString))
            {
                return PublisherNewsCategoryRepository.Select(entity);
            }
        }
        public List<XsiPublisherNewsCategory> GetPublisherNewsCategory(XsiPublisherNewsCategory entity, EnumSortlistBy sortListBy)
        {
            using (PublisherNewsCategoryRepository = new PublisherNewsCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PublisherNewsCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PublisherNewsCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    //case EnumSortlistBy.ByDateAsc:
                    //    return PublisherNewsCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    //case EnumSortlistBy.ByDateDesc:
                    //    return PublisherNewsCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PublisherNewsCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PublisherNewsCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PublisherNewsCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiPublisherNewsCategory> GetPublisherNewsCategoryOr(XsiPublisherNewsCategory entity, EnumSortlistBy sortListBy)
        {
            using (PublisherNewsCategoryRepository = new PublisherNewsCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PublisherNewsCategoryRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PublisherNewsCategoryRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    //case EnumSortlistBy.ByDateAsc:
                    //    return PublisherNewsCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    //case EnumSortlistBy.ByDateDesc:
                    //    return PublisherNewsCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PublisherNewsCategoryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PublisherNewsCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PublisherNewsCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        //public XsiPublisherNewsCategory GetPublisherNewsCategory(long itemId, long languageId)
        //{
        //    using (PublisherNewsCategoryRepository = new PublisherNewsCategoryRepository(ConnectionString))
        //    {
        //        return PublisherNewsCategoryRepository.Select(new XsiPublisherNewsCategory() { ItemId = itemId, LanguageId = languageId }).FirstOrDefault();
        //    }
        //}
        public List<XsiPublisherNewsCategory> GetOtherLanguagePublisherNewsCategory(XsiPublisherNewsCategory entity)
        {
            using (PublisherNewsCategoryRepository = new PublisherNewsCategoryRepository(ConnectionString))
            {
                return PublisherNewsCategoryRepository.SelectOtherLanguageCategory(entity);
            }
        }
        public List<XsiPublisherNewsCategory> GetCurrentLanguagePublisherNewsCategory(XsiPublisherNewsCategory entity)
        {
            using (PublisherNewsCategoryRepository = new PublisherNewsCategoryRepository(ConnectionString))
            {
                return PublisherNewsCategoryRepository.SelectCurrentLanguageCategory(entity);
            }
        }

        public EnumResultType InsertPublisherNewsCategory(XsiPublisherNewsCategory entity)
        {
            using (PublisherNewsCategoryRepository = new PublisherNewsCategoryRepository(ConnectionString))
            {
                entity = PublisherNewsCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePublisherNewsCategory(XsiPublisherNewsCategory entity)
        {
            XsiPublisherNewsCategory OriginalEntity = GetPublisherNewsCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PublisherNewsCategoryRepository = new PublisherNewsCategoryRepository(ConnectionString))
                {
                    PublisherNewsCategoryRepository.Update(entity);
                    if (PublisherNewsCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdatePublisherNewsCategoryStatus(long itemId)
        {
            XsiPublisherNewsCategory entity = GetPublisherNewsCategoryByItemId(itemId);
            if (entity != null)
            {
                using (PublisherNewsCategoryRepository = new PublisherNewsCategoryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    PublisherNewsCategoryRepository.Update(entity);
                    if (PublisherNewsCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePublisherNewsCategory(XsiPublisherNewsCategory entity)
        {
            List<XsiPublisherNewsCategory> entities = GetPublisherNewsCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiPublisherNewsCategory e in entities)
                {
                    result = DeletePublisherNewsCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeletePublisherNewsCategory(long itemId)
        {
            XsiPublisherNewsCategory entity = GetPublisherNewsCategoryByItemId(itemId);
            if (entity != null)
            {
                using (PublisherNewsCategoryRepository = new PublisherNewsCategoryRepository(ConnectionString))
                {
                    //delete PublisherNews with PublisherNews catefory id = itemid

                    PublisherNewsCategoryRepository.Delete(itemId);
                    if (PublisherNewsCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
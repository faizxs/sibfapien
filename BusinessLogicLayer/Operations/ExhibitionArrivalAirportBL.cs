﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionArrivalAirportBL : IDisposable
    {
        #region Feilds
        IExhibitionArrivalAirportRepository ExhibitionArrivalAirportRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionArrivalAirportBL()
        {
            ConnectionString = null;
        }
        public ExhibitionArrivalAirportBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionArrivalAirportRepository != null)
                this.ExhibitionArrivalAirportRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionArrivalAirport GetExhibitionArrivalAirportByItemId(long itemId)
        {
            using (ExhibitionArrivalAirportRepository = new ExhibitionArrivalAirportRepository(ConnectionString))
            {
                return ExhibitionArrivalAirportRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionArrivalAirport> GetExhibitionArrivalAirport()
        {
            using (ExhibitionArrivalAirportRepository = new ExhibitionArrivalAirportRepository(ConnectionString))
            {
                return ExhibitionArrivalAirportRepository.Select();
            }
        }
        public List<XsiExhibitionArrivalAirport> GetExhibitionArrivalAirport(EnumSortlistBy sortListBy)
        {
            using (ExhibitionArrivalAirportRepository = new ExhibitionArrivalAirportRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionArrivalAirportRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionArrivalAirportRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionArrivalAirportRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionArrivalAirportRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionArrivalAirportRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionArrivalAirportRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionArrivalAirportRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionArrivalAirport> GetExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity)
        {
            using (ExhibitionArrivalAirportRepository = new ExhibitionArrivalAirportRepository(ConnectionString))
            {
                return ExhibitionArrivalAirportRepository.Select(entity);
            }
        }
        public List<XsiExhibitionArrivalAirport> GetExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionArrivalAirportRepository = new ExhibitionArrivalAirportRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionArrivalAirportRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionArrivalAirportRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionArrivalAirportRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionArrivalAirportRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionArrivalAirportRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionArrivalAirportRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionArrivalAirportRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionArrivalAirport> GetExhibitionArrivalAirportOr(XsiExhibitionArrivalAirport entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionArrivalAirportRepository = new ExhibitionArrivalAirportRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionArrivalAirportRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionArrivalAirportRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionArrivalAirportRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionArrivalAirportRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionArrivalAirportRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionArrivalAirportRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionArrivalAirportRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionArrivalAirport> GetOtherLanguageExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity)
        {
            using (ExhibitionArrivalAirportRepository = new ExhibitionArrivalAirportRepository(ConnectionString))
            {
                return ExhibitionArrivalAirportRepository.SelectOtherLanguageCategory(entity);
            }
        }
        public List<XsiExhibitionArrivalAirport> GetCurrentLanguageExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity)
        {
            using (ExhibitionArrivalAirportRepository = new ExhibitionArrivalAirportRepository(ConnectionString))
            {
                return ExhibitionArrivalAirportRepository.SelectCurrentLanguageCategory(entity);
            }
        }
        public List<XsiExhibitionArrivalAirport> GetCompleteExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionArrivalAirportRepository = new ExhibitionArrivalAirportRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionArrivalAirportRepository.SelectComplete(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionArrivalAirportRepository.SelectComplete(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionArrivalAirportRepository.SelectComplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionArrivalAirportRepository.SelectComplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionArrivalAirportRepository.SelectComplete(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionArrivalAirportRepository.SelectComplete(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionArrivalAirportRepository.SelectComplete(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity)
        {
            using (ExhibitionArrivalAirportRepository = new ExhibitionArrivalAirportRepository(ConnectionString))
            {
                entity = ExhibitionArrivalAirportRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity)
        {
            XsiExhibitionArrivalAirport OriginalEntity = GetExhibitionArrivalAirportByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionArrivalAirportRepository = new ExhibitionArrivalAirportRepository(ConnectionString))
                {
                    ExhibitionArrivalAirportRepository.Update(entity);
                    if (ExhibitionArrivalAirportRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionArrivalAirportStatus(long itemId, long langId)
        {
            XsiExhibitionArrivalAirport entity = GetExhibitionArrivalAirportByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionArrivalAirportRepository = new ExhibitionArrivalAirportRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionArrivalAirportRepository.Update(entity);
                    if (ExhibitionArrivalAirportRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionArrivalAirport(XsiExhibitionArrivalAirport entity)
        {
            List<XsiExhibitionArrivalAirport> entities = GetExhibitionArrivalAirport(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionArrivalAirport e in entities)
                {
                    result = DeleteExhibitionArrivalAirport(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionArrivalAirport(long itemId)
        {
            XsiExhibitionArrivalAirport entity = GetExhibitionArrivalAirportByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionArrivalAirportRepository = new ExhibitionArrivalAirportRepository(ConnectionString))
                {
                    ExhibitionArrivalAirportRepository.Delete(itemId);
                    if (ExhibitionArrivalAirportRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
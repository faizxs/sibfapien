﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionOtherEventsStaffGuestParticipatingBL : IDisposable
    {
        #region Feilds
        IExhibitionOtherEventsStaffGuestParticipatingRepository IExhibitionOtherEventsStaffGuestParticipatingRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionOtherEventsStaffGuestParticipatingBL()
        {
            ConnectionString = null;
        }
        public ExhibitionOtherEventsStaffGuestParticipatingBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionOtherEventsStaffGuestParticipatingRepository != null)
                this.IExhibitionOtherEventsStaffGuestParticipatingRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionOtherEventsStaffGuestParticipatingRepository = new ExhibitionOtherEventsStaffGuestParticipatingRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionOtherEventsStaffGuestParticipating GetExhibitionOtherEventsStaffGuestParticipatingByItemId(long itemId)
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingRepository = new ExhibitionOtherEventsStaffGuestParticipatingRepository(ConnectionString))
            {
                return IExhibitionOtherEventsStaffGuestParticipatingRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionOtherEventsStaffGuestParticipating> GetExhibitionOtherEventsStaffGuestParticipating()
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingRepository = new ExhibitionOtherEventsStaffGuestParticipatingRepository(ConnectionString))
            {
                return IExhibitionOtherEventsStaffGuestParticipatingRepository.Select();
            }
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipating> GetExhibitionOtherEventsStaffGuestParticipating(EnumSortlistBy sortListBy)
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingRepository = new ExhibitionOtherEventsStaffGuestParticipatingRepository(ConnectionString))
            {
                switch (sortListBy)
                {

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionOtherEventsStaffGuestParticipatingRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionOtherEventsStaffGuestParticipatingRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionOtherEventsStaffGuestParticipatingRepository.Select().OrderBy(p => p.StaffGuestId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionOtherEventsStaffGuestParticipatingRepository.Select().OrderByDescending(p => p.StaffGuestId).ToList();

                    default:
                        return IExhibitionOtherEventsStaffGuestParticipatingRepository.Select().OrderByDescending(p => p.StaffGuestId).ToList();
                }
            }
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipating> GetExhibitionOtherEventsStaffGuestParticipating(XsiExhibitionOtherEventsStaffGuestParticipating entity)
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingRepository = new ExhibitionOtherEventsStaffGuestParticipatingRepository(ConnectionString))
            {
                return IExhibitionOtherEventsStaffGuestParticipatingRepository.Select(entity);
            }
        }
        public List<XsiExhibitionOtherEventsStaffGuestParticipating> GetExhibitionOtherEventsStaffGuestParticipating(XsiExhibitionOtherEventsStaffGuestParticipating entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingRepository = new ExhibitionOtherEventsStaffGuestParticipatingRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionOtherEventsStaffGuestParticipatingRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionOtherEventsStaffGuestParticipatingRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionOtherEventsStaffGuestParticipatingRepository.Select(entity).OrderBy(p => p.StaffGuestId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionOtherEventsStaffGuestParticipatingRepository.Select(entity).OrderByDescending(p => p.StaffGuestId).ToList();

                    default:
                        return IExhibitionOtherEventsStaffGuestParticipatingRepository.Select(entity).OrderByDescending(p => p.StaffGuestId).ToList();
                }
            }
        }
        public XsiExhibitionOtherEventsStaffGuestParticipating GetExhibitionOtherEventsStaffGuestParticipating(long groupId, long languageId)
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingRepository = new ExhibitionOtherEventsStaffGuestParticipatingRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitionOtherEventsStaffGuestParticipating(XsiExhibitionOtherEventsStaffGuestParticipating entity)
        {
            using (IExhibitionOtherEventsStaffGuestParticipatingRepository = new ExhibitionOtherEventsStaffGuestParticipatingRepository(ConnectionString))
            {
                entity = IExhibitionOtherEventsStaffGuestParticipatingRepository.Add(entity);
                if (entity.StaffGuestId > 0)
                {
                    XsiItemdId = entity.StaffGuestId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuestParticipating(XsiExhibitionOtherEventsStaffGuestParticipating entity)
        {
            //XsiExhibitionOtherEventsStaffGuestParticipating OriginalEntity = GetExhibitionOtherEventsStaffGuestParticipatingByItemId(entity.StaffGuestId);

            var where = new XsiExhibitionOtherEventsStaffGuestParticipating();
            where.StaffGuestId = entity.StaffGuestId;
            XsiExhibitionOtherEventsStaffGuestParticipating OriginalEntity = GetExhibitionOtherEventsStaffGuestParticipating(where, EnumSortlistBy.ByItemIdDesc).FirstOrDefault();

            if (OriginalEntity != null)
            {
                using (IExhibitionOtherEventsStaffGuestParticipatingRepository = new ExhibitionOtherEventsStaffGuestParticipatingRepository(ConnectionString))
                {

                    IExhibitionOtherEventsStaffGuestParticipatingRepository.Update(entity);
                    if (IExhibitionOtherEventsStaffGuestParticipatingRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackExhibitionOtherEventsStaffGuestParticipating(long itemId)
        {
            //if no RollbackXml propery in ExhibitionOtherEventsStaffGuestParticipating then delete below code and return EnumResultType.Invalid;

            return EnumResultType.Failed;

        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuestParticipatingStatus(long itemId)
        {
            XsiExhibitionOtherEventsStaffGuestParticipating entity = GetExhibitionOtherEventsStaffGuestParticipatingByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionOtherEventsStaffGuestParticipatingRepository = new ExhibitionOtherEventsStaffGuestParticipatingRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionOtherEventsStaffGuestParticipatingRepository.Update(entity);
                    if (IExhibitionOtherEventsStaffGuestParticipatingRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuestParticipating(XsiExhibitionOtherEventsStaffGuestParticipating entity)
        {
            List<XsiExhibitionOtherEventsStaffGuestParticipating> entities = GetExhibitionOtherEventsStaffGuestParticipating(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionOtherEventsStaffGuestParticipating e in entities)
                {
                    result = DeleteExhibitionOtherEventsStaffGuestParticipating(e.StaffGuestId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuestParticipating(long itemId)
        {
            var where = new XsiExhibitionOtherEventsStaffGuestParticipating();
            where.StaffGuestId = itemId;
            XsiExhibitionOtherEventsStaffGuestParticipating entity = GetExhibitionOtherEventsStaffGuestParticipating(where,EnumSortlistBy.ByItemIdDesc).FirstOrDefault();
            if (entity != null)
            {
                using (IExhibitionOtherEventsStaffGuestParticipatingRepository = new ExhibitionOtherEventsStaffGuestParticipatingRepository(ConnectionString))
                {
                    IExhibitionOtherEventsStaffGuestParticipatingRepository.Delete(itemId);
                    if (IExhibitionOtherEventsStaffGuestParticipatingRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
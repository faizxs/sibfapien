﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ProfessionalProgramBookBL : IDisposable
    {
        #region Feilds
        IProfessionalProgramBookRepository IProfessionalProgramBookRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ProfessionalProgramBookBL()
        {
            ConnectionString = null;
        }
        public ProfessionalProgramBookBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IProfessionalProgramBookRepository != null)
                this.IProfessionalProgramBookRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IProfessionalProgramBookRepository = new ProfessionalProgramBookRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionProfessionalProgramBook GetProfessionalProgramBookByItemId(long itemId)
        {
            using (IProfessionalProgramBookRepository = new ProfessionalProgramBookRepository(ConnectionString))
            {
                return IProfessionalProgramBookRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionProfessionalProgramBook> GetProfessionalProgramBook()
        {
            using (IProfessionalProgramBookRepository = new ProfessionalProgramBookRepository(ConnectionString))
            {
                return IProfessionalProgramBookRepository.Select();
            }
        }
        public List<XsiExhibitionProfessionalProgramBook> GetProfessionalProgramBook(EnumSortlistBy sortListBy)
        {
            using (IProfessionalProgramBookRepository = new ProfessionalProgramBookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IProfessionalProgramBookRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IProfessionalProgramBookRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IProfessionalProgramBookRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IProfessionalProgramBookRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IProfessionalProgramBookRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IProfessionalProgramBookRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IProfessionalProgramBookRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionProfessionalProgramBook> GetProfessionalProgramBook(XsiExhibitionProfessionalProgramBook entity)
        {
            using (IProfessionalProgramBookRepository = new ProfessionalProgramBookRepository(ConnectionString))
            {
                return IProfessionalProgramBookRepository.Select(entity);
            }
        }
        public List<XsiExhibitionProfessionalProgramBook> GetProfessionalProgramBook(XsiExhibitionProfessionalProgramBook entity, EnumSortlistBy sortListBy)
        {
            using (IProfessionalProgramBookRepository = new ProfessionalProgramBookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IProfessionalProgramBookRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IProfessionalProgramBookRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IProfessionalProgramBookRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IProfessionalProgramBookRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IProfessionalProgramBookRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IProfessionalProgramBookRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IProfessionalProgramBookRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionProfessionalProgramBook GetProfessionalProgramBook(long groupId, long languageId)
        {
            using (IProfessionalProgramBookRepository = new ProfessionalProgramBookRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertProfessionalProgramBook(XsiExhibitionProfessionalProgramBook entity)
        {
            using (IProfessionalProgramBookRepository = new ProfessionalProgramBookRepository(ConnectionString))
            {
                entity = IProfessionalProgramBookRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateProfessionalProgramBook(XsiExhibitionProfessionalProgramBook entity)
        {
            XsiExhibitionProfessionalProgramBook OriginalEntity = GetProfessionalProgramBookByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IProfessionalProgramBookRepository = new ProfessionalProgramBookRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line

                    IProfessionalProgramBookRepository.Update(entity);
                    if (IProfessionalProgramBookRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateProfessionalProgramBookStatus(long itemId)
        {
            XsiExhibitionProfessionalProgramBook entity = GetProfessionalProgramBookByItemId(itemId);
            if (entity != null)
            {
                using (IProfessionalProgramBookRepository = new ProfessionalProgramBookRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IProfessionalProgramBookRepository.Update(entity);
                    if (IProfessionalProgramBookRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteProfessionalProgramBook(XsiExhibitionProfessionalProgramBook entity)
        {
            List<XsiExhibitionProfessionalProgramBook> entities = GetProfessionalProgramBook(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionProfessionalProgramBook e in entities)
                {
                    result = DeleteProfessionalProgramBook(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteProfessionalProgramBook(long itemId)
        {
            XsiExhibitionProfessionalProgramBook entity = GetProfessionalProgramBookByItemId(itemId);
            if (entity != null)
            {
                using (IProfessionalProgramBookRepository = new ProfessionalProgramBookRepository(ConnectionString))
                {
                    IProfessionalProgramBookRepository.Delete(itemId);
                    if (IProfessionalProgramBookRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class AgencyRegistrationBL : IDisposable
    {
        #region Feilds
        IAgencyRegistrationRepository AgencyRegistrationRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public AgencyRegistrationBL()
        {
            ConnectionString = null;
        }
        public AgencyRegistrationBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AgencyRegistrationRepository != null)
                this.AgencyRegistrationRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionMemberApplicationYearly GetAgencyRegistrationByItemId(long itemId)
        {
            using (AgencyRegistrationRepository = new AgencyRegistrationRepository(ConnectionString))
            {
                return AgencyRegistrationRepository.GetById(itemId);
            }
        }
       
        public List<XsiExhibitionMemberApplicationYearly> GetAgencyRegistration()
        {
            using (AgencyRegistrationRepository = new AgencyRegistrationRepository(ConnectionString))
            {
                return AgencyRegistrationRepository.Select();
            }
        }
        public List<XsiExhibitionMemberApplicationYearly> GetAgencyRegistration(EnumSortlistBy sortListBy)
        {
            using (AgencyRegistrationRepository = new AgencyRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return AgencyRegistrationRepository.Select().OrderBy(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return AgencyRegistrationRepository.Select().OrderByDescending(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return AgencyRegistrationRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return AgencyRegistrationRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return AgencyRegistrationRepository.Select().OrderBy(p => p.MemberExhibitionYearlyId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AgencyRegistrationRepository.Select().OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();

                    default:
                        return AgencyRegistrationRepository.Select().OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();
                }
            }
        }
        public List<string> GetAgencyRegistrationAgencyName(XsiExhibitionMemberApplicationYearly entity)
        {
            using (AgencyRegistrationRepository = new AgencyRegistrationRepository(ConnectionString))
            {
                return AgencyRegistrationRepository.SelectAgencyName(entity);
            }
        }
        public List<string> GetAgencyRegistrationAgencyNameAr(XsiExhibitionMemberApplicationYearly entity)
        {
            using (AgencyRegistrationRepository = new AgencyRegistrationRepository(ConnectionString))
            {
                return AgencyRegistrationRepository.SelectAgencyNameAr(entity);
            }
        }
        public List<XsiExhibitionMemberApplicationYearly> GetAgencyRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            using (AgencyRegistrationRepository = new AgencyRegistrationRepository(ConnectionString))
            {
                return AgencyRegistrationRepository.Select(entity);
            }
        }
        public List<XsiExhibitionMemberApplicationYearly> GetAgencyRegistration(XsiExhibitionMemberApplicationYearly entity, EnumSortlistBy sortListBy)
        {
            using (AgencyRegistrationRepository = new AgencyRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return AgencyRegistrationRepository.Select(entity).OrderBy(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return AgencyRegistrationRepository.Select(entity).OrderByDescending(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return AgencyRegistrationRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return AgencyRegistrationRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return AgencyRegistrationRepository.Select(entity).OrderBy(p => p.MemberExhibitionYearlyId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AgencyRegistrationRepository.Select(entity).OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();

                    default:
                        return AgencyRegistrationRepository.Select(entity).OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();
                }
            }
        }
        public List<XsiExhibitionMemberApplicationYearly> GetAgencyRegistrationExact(XsiExhibitionMemberApplicationYearly entity)
        {
            using (AgencyRegistrationRepository = new AgencyRegistrationRepository(ConnectionString))
            {
                return AgencyRegistrationRepository.SelectExact(entity);
            }
        }

        public EnumResultType InsertAgencyRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            using (AgencyRegistrationRepository = new AgencyRegistrationRepository(ConnectionString))
            {
                XsiExhibitionMemberApplicationYearly where = new XsiExhibitionMemberApplicationYearly();
                where.MemberId = entity.MemberId;
                where.ExhibitionId = entity.ExhibitionId;
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);
                //where.IsRegisteredByExhibitor = EnumConversion.ToString(EnumBool.No);

                if (AgencyRegistrationRepository.Select(where).Count() == 0)
                {
                    entity = AgencyRegistrationRepository.Add(entity);
                    if (entity.MemberExhibitionYearlyId > 0)
                    {
                        XsiItemdId = entity.MemberExhibitionYearlyId;
                        
                        return EnumResultType.Success;
                    }
                    else
                    {
                        XsiItemdId = -1; 
                        return EnumResultType.Failed;
                    }
                }
                else
                    return EnumResultType.AlreadyExists;
            }
        }
        public EnumResultType UpdateAgencyRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            XsiExhibitionMemberApplicationYearly OriginalEntity = GetAgencyRegistrationByItemId(entity.MemberExhibitionYearlyId);

            if (OriginalEntity != null)
            {
                using (AgencyRegistrationRepository = new AgencyRegistrationRepository(ConnectionString))
                {
                    AgencyRegistrationRepository.Update(entity);
                    if (AgencyRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateAgencyRegistrationStatus(long itemId)
        {
            XsiExhibitionMemberApplicationYearly entity = GetAgencyRegistrationByItemId(itemId);
            if (entity != null)
            {
                using (AgencyRegistrationRepository = new AgencyRegistrationRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    AgencyRegistrationRepository.Update(entity);
                    if (AgencyRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteAgencyRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            List<XsiExhibitionMemberApplicationYearly> entities = GetAgencyRegistration(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionMemberApplicationYearly e in entities)
                {
                    result = DeleteAgencyRegistration(e.MemberExhibitionYearlyId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteAgencyRegistration(long itemId)
        {
            XsiExhibitionMemberApplicationYearly entity = GetAgencyRegistrationByItemId(itemId);
            if (entity != null)
            {
                using (AgencyRegistrationRepository = new AgencyRegistrationRepository(ConnectionString))
                {
                    AgencyRegistrationRepository.Delete(itemId);
                    if (AgencyRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
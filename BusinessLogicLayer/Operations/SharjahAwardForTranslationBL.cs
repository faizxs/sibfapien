﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SharjahAwardForTranslationBL : IDisposable
    {
        #region Feilds
        ISharjahAwardForTranslationRepository SharjahAwardForTranslationsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SharjahAwardForTranslationBL()
        {
            ConnectionString = null;
        }
        public SharjahAwardForTranslationBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SharjahAwardForTranslationsRepository != null)
                this.SharjahAwardForTranslationsRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiSharjahAwardForTranslation GetSharjahAwardForTranslationByItemId(long itemId)
        {
            using (SharjahAwardForTranslationsRepository = new SharjahAwardForTranslationRepository(ConnectionString))
            {
                return SharjahAwardForTranslationsRepository.GetById(itemId);
            }
        }
        
        public List<XsiSharjahAwardForTranslation> GetSharjahAwardForTranslation()
        {
            using (SharjahAwardForTranslationsRepository = new SharjahAwardForTranslationRepository(ConnectionString))
            {
                return SharjahAwardForTranslationsRepository.Select();
            }
        }
        public List<XsiSharjahAwardForTranslation> GetSharjahAwardForTranslation(EnumSortlistBy sortListBy)
        {
            using (SharjahAwardForTranslationsRepository = new SharjahAwardForTranslationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SharjahAwardForTranslationsRepository.Select().OrderBy(p => p.TranslatedBookName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SharjahAwardForTranslationsRepository.Select().OrderByDescending(p => p.TranslatedBookName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SharjahAwardForTranslationsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SharjahAwardForTranslationsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SharjahAwardForTranslationsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SharjahAwardForTranslationsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SharjahAwardForTranslationsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiSharjahAwardForTranslation> GetSharjahAwardForTranslation(XsiSharjahAwardForTranslation entity)
        {
            using (SharjahAwardForTranslationsRepository = new SharjahAwardForTranslationRepository(ConnectionString))
            {
                return SharjahAwardForTranslationsRepository.Select(entity);
            }
        }
        public List<XsiSharjahAwardForTranslation> GetSharjahAwardForTranslation(XsiSharjahAwardForTranslation entity, EnumSortlistBy sortListBy)
        {
            using (SharjahAwardForTranslationsRepository = new SharjahAwardForTranslationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SharjahAwardForTranslationsRepository.Select(entity).OrderBy(p => p.TranslatedBookName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SharjahAwardForTranslationsRepository.Select(entity).OrderByDescending(p => p.TranslatedBookName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SharjahAwardForTranslationsRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SharjahAwardForTranslationsRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SharjahAwardForTranslationsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SharjahAwardForTranslationsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SharjahAwardForTranslationsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSharjahAwardForTranslation(XsiSharjahAwardForTranslation entity)
        {
            using (SharjahAwardForTranslationsRepository = new SharjahAwardForTranslationRepository(ConnectionString))
            {
                entity = SharjahAwardForTranslationsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSharjahAwardForTranslation(XsiSharjahAwardForTranslation entity)
        {
            XsiSharjahAwardForTranslation OriginalEntity = GetSharjahAwardForTranslationByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SharjahAwardForTranslationsRepository = new SharjahAwardForTranslationRepository(ConnectionString))
                {

                    SharjahAwardForTranslationsRepository.Update(entity);
                    if (SharjahAwardForTranslationsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSharjahAwardForTranslationStatus(long itemId)
        {
            XsiSharjahAwardForTranslation entity = GetSharjahAwardForTranslationByItemId(itemId);
            if (entity != null)
            {
                using (SharjahAwardForTranslationsRepository = new SharjahAwardForTranslationRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SharjahAwardForTranslationsRepository.Update(entity);
                    if (SharjahAwardForTranslationsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSharjahAwardForTranslation(XsiSharjahAwardForTranslation entity)
        {
            List<XsiSharjahAwardForTranslation> entities = GetSharjahAwardForTranslation(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiSharjahAwardForTranslation e in entities)
                {
                    result = DeleteSharjahAwardForTranslation(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSharjahAwardForTranslation(long itemId)
        {
            XsiSharjahAwardForTranslation entity = GetSharjahAwardForTranslationByItemId(itemId);
            if (entity != null)
            {
                using (SharjahAwardForTranslationsRepository = new SharjahAwardForTranslationRepository(ConnectionString))
                {
                    SharjahAwardForTranslationsRepository.Delete(itemId);
                    if (SharjahAwardForTranslationsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
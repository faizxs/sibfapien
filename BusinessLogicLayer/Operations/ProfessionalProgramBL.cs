﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ProfessionalProgramBL : IDisposable
    {
        #region Feilds
        IProfessionalProgramRepository ProfessionalProgramRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ProfessionalProgramBL()
        {
            ConnectionString = null;
        }
        public ProfessionalProgramBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ProfessionalProgramRepository != null)
                this.ProfessionalProgramRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionProfessionalProgram GetProfessionalProgramByItemId(long itemId)
        {
            using (ProfessionalProgramRepository = new ProfessionalProgramRepository(ConnectionString))
            {
                return ProfessionalProgramRepository.GetById(itemId);
            }
        }
         
        public List<XsiExhibitionProfessionalProgram> GetProfessionalProgram()
        {
            using (ProfessionalProgramRepository = new ProfessionalProgramRepository(ConnectionString))
            {
                return ProfessionalProgramRepository.Select();
            }
        }
        public List<XsiExhibitionProfessionalProgram> GetProfessionalProgram(EnumSortlistBy sortListBy)
        {
            using (ProfessionalProgramRepository = new ProfessionalProgramRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ProfessionalProgramRepository.Select().OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ProfessionalProgramRepository.Select().OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ProfessionalProgramRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ProfessionalProgramRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ProfessionalProgramRepository.Select().OrderBy(p => p.ProfessionalProgramId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ProfessionalProgramRepository.Select().OrderByDescending(p => p.ProfessionalProgramId).ToList();

                    default:
                        return ProfessionalProgramRepository.Select().OrderByDescending(p => p.ProfessionalProgramId).ToList();
                }
            }
        }
        public List<XsiExhibitionProfessionalProgram> GetProfessionalProgram(XsiExhibitionProfessionalProgram entity)
        {
            ProfessionalProgramRepository = new ProfessionalProgramRepository(ConnectionString);
            return ProfessionalProgramRepository.Select(entity);
        }
        public List<XsiExhibitionProfessionalProgram> GetProfessionalProgram(XsiExhibitionProfessionalProgram entity, EnumSortlistBy sortListBy)
        {
            using (ProfessionalProgramRepository = new ProfessionalProgramRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ProfessionalProgramRepository.Select(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ProfessionalProgramRepository.Select(entity).OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ProfessionalProgramRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ProfessionalProgramRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ProfessionalProgramRepository.Select(entity).OrderBy(p => p.ProfessionalProgramId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ProfessionalProgramRepository.Select(entity).OrderByDescending(p => p.ProfessionalProgramId).ToList();

                    default:
                        return ProfessionalProgramRepository.Select(entity).OrderByDescending(p => p.ProfessionalProgramId).ToList();
                }
            }
        }
       
        public EnumResultType InsertProfessionalProgram(XsiExhibitionProfessionalProgram entity)
        {
            using (ProfessionalProgramRepository = new ProfessionalProgramRepository(ConnectionString))
            {
                entity = ProfessionalProgramRepository.Add(entity);
                if (entity.ProfessionalProgramId > 0)
                {
                    XsiItemdId = entity.ProfessionalProgramId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateProfessionalProgram(XsiExhibitionProfessionalProgram entity)
        {
            XsiExhibitionProfessionalProgram OriginalEntity = GetProfessionalProgramByItemId(entity.ProfessionalProgramId);

            if (OriginalEntity != null)
            {
                using (ProfessionalProgramRepository = new ProfessionalProgramRepository(ConnectionString))
                {
                    ProfessionalProgramRepository.Update(entity);
                    if (ProfessionalProgramRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateProfessionalProgramStatus(long itemId)
        {
            EnumResultType XsiResult = EnumResultType.NotFound;
            XsiExhibitionProfessionalProgram entity = GetProfessionalProgramByItemId(itemId);
            if (entity != null)
            {
                using (ProfessionalProgramRepository = new ProfessionalProgramRepository(ConnectionString))
                {
                    string str = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    //List<XsiExhibitionProfessionalProgram> entityList = GetProfessionalProgramByItemId(entity.ProfessionalProgramId);
                    XsiExhibitionProfessionalProgram ProfessionalProgramEntity;
                    //foreach (XsiExhibitionProfessionalProgram professionalprogram in entityList)
                    //{
                        ProfessionalProgramEntity = GetProfessionalProgramByItemId(entity.ProfessionalProgramId);
                        if (ProfessionalProgramEntity != null)
                        {
                            using (ProfessionalProgramRepository = new ProfessionalProgramRepository(ConnectionString))
                            {
                                ProfessionalProgramEntity.IsActive = str;
                                ProfessionalProgramRepository.Update(ProfessionalProgramEntity);
                                if (ProfessionalProgramRepository.SubmitChanges() > 0)
                                    XsiResult = EnumResultType.Success;
                                else
                                    XsiResult = EnumResultType.Failed;
                            }
                        }
                        else
                            return XsiResult;
                    //}
                }
            }
            return XsiResult;
        }
        public EnumResultType DeleteProfessionalProgram(XsiExhibitionProfessionalProgram entity)
        {
            List<XsiExhibitionProfessionalProgram> entities = GetProfessionalProgram(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionProfessionalProgram e in entities)
                {
                    result = DeleteProfessionalProgram(e.ProfessionalProgramId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteProfessionalProgram(long itemId)
        {
            XsiExhibitionProfessionalProgram entity = GetProfessionalProgramByItemId(itemId);
            if (entity != null)
            {
                using (ProfessionalProgramRepository = new ProfessionalProgramRepository(ConnectionString))
                {
                    ProfessionalProgramRepository.Delete(itemId);
                    if (ProfessionalProgramRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
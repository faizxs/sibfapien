﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class NewsBL : IDisposable
    {
        #region Feilds
        INewsRepository NewsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public NewsBL()
        {
            ConnectionString = null;
        }
        public NewsBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.NewsRepository != null)
                this.NewsRepository.Dispose();
        }
        #endregion
        #region Methods
          
        public XsiNews GetNewsByItemId(long itemId)
        {
            using (NewsRepository = new NewsRepository(ConnectionString))
            {
                return NewsRepository.GetById(itemId);
            }
        }
        
        public List<XsiNews> GetNews()
        {
            using (NewsRepository = new NewsRepository(ConnectionString))
            {
                return NewsRepository.Select();
            }
        }
        public List<XsiNews> GetNews(EnumSortlistBy sortListBy)
        {
            using (NewsRepository = new NewsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return NewsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return NewsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiNews> GetNews(XsiNews entity)
        {
            using (NewsRepository = new NewsRepository(ConnectionString))
            {
                return NewsRepository.Select(entity);
            }
        }
        public List<XsiNews> GetNews(XsiNews entity, EnumSortlistBy sortListBy)
        {
            using (NewsRepository = new NewsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return NewsRepository.Select(entity).OrderBy(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return NewsRepository.Select(entity).OrderByDescending(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiNews> GetCompleteNews(XsiNews entity, EnumSortlistBy sortListBy,long langId)
        {
            using (NewsRepository = new NewsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsRepository.SelectComeplete(entity, langId).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return NewsRepository.SelectComeplete(entity, langId).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return NewsRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsRepository.SelectComeplete(entity, langId).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public XsiNews GetNews(long itemId, long categoryId)
        {
            using (NewsRepository = new NewsRepository(ConnectionString))
            {
                return NewsRepository.Select(new XsiNews() {ItemId= itemId, CategoryId = categoryId }).FirstOrDefault();
            }
        }

        public EnumResultType InsertNews(XsiNews entity)
        {
            using (NewsRepository = new NewsRepository(ConnectionString))
            {
                entity = NewsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateNews(XsiNews entity)
        {
            XsiNews OriginalEntity = GetNewsByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (NewsRepository = new NewsRepository(ConnectionString))
                {
                    NewsRepository.Update(entity);
                    if (NewsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateNewsStatus(long itemId)
        {
            XsiNews entity = GetNewsByItemId(itemId);
            if (entity != null)
            {
                using (NewsRepository = new NewsRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    NewsRepository.Update(entity);
                    if (NewsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteNews(XsiNews entity)
        {
            List<XsiNews> entities = GetNews(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiNews e in entities)
                {
                    result = DeleteNews(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteNews(long itemId)
        {
            XsiNews entity = GetNewsByItemId(itemId);
            if (entity != null)
            {
                using (NewsRepository = new NewsRepository(ConnectionString))
                {
                    NewsRepository.Delete(itemId);
                    if (NewsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
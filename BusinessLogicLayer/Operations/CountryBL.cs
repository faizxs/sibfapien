﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class CountryBL : IDisposable
    {
        #region Feilds
        ICountryRepository XsiCountryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public CountryBL()
        {
            ConnectionString = null;
        }
        public CountryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiCountryRepository != null)
                this.XsiCountryRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiCountry GetCountryByItemId(long itemId)
        {
            using (XsiCountryRepository = new CountryRepository(ConnectionString))
            {
                return XsiCountryRepository.GetById(itemId);
            }
        }

        public List<XsiCountry> GetCountry()
        {
            using (XsiCountryRepository = new CountryRepository(ConnectionString))
            {
                return XsiCountryRepository.Select();
            }
        }
        public List<XsiCountry> GetCountry(EnumSortlistBy sortListBy)
        {
            using (XsiCountryRepository = new CountryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiCountryRepository.Select().OrderBy(p => p.CountryName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiCountryRepository.Select().OrderByDescending(p => p.CountryName).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiCountryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiCountryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiCountryRepository.Select().OrderBy(p => p.ItemOrder).ToList();
                }
            }
        }
        public List<XsiCountry> GetCountry(XsiCountry entity)
        {
            using (XsiCountryRepository = new CountryRepository(ConnectionString))
            {
                return XsiCountryRepository.Select(entity);
            }
        }
        public List<XsiCountry> GetCountry(XsiCountry entity, EnumSortlistBy sortListBy)
        {
            using (XsiCountryRepository = new CountryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiCountryRepository.Select(entity).OrderBy(p => p.CountryName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiCountryRepository.Select(entity).OrderByDescending(p => p.CountryName).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiCountryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiCountryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiCountryRepository.Select(entity).OrderBy(p => p.ItemOrder).ToList();
                }
            }
        }

        public EnumResultType InsertCountry(XsiCountry entity)
        {
            using (XsiCountryRepository = new CountryRepository(ConnectionString))
            {
                entity = XsiCountryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateCountry(XsiCountry entity)
        {
            XsiCountry OriginalEntity = GetCountryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (XsiCountryRepository = new CountryRepository(ConnectionString))
                {
                    XsiCountryRepository.Update(entity);
                    if (XsiCountryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateCountryStatus(long itemId)
        {
            XsiCountry entity = GetCountryByItemId(itemId);
            if (entity != null)
            {
                using (XsiCountryRepository = new CountryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    XsiCountryRepository.Update(entity);
                    if (XsiCountryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteCountry(XsiCountry entity)
        {
            if (GetCountry(entity).Count() > 0)
            {
                using (XsiCountryRepository = new CountryRepository(ConnectionString))
                {
                    XsiCountryRepository.Delete(entity);
                    if (XsiCountryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteCountry(long itemId)
        {
            if (GetCountryByItemId(itemId) != null)
            {
                using (XsiCountryRepository = new CountryRepository(ConnectionString))
                {
                    XsiCountryRepository.Delete(itemId);
                    if (XsiCountryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class HomePageLinkBL : IDisposable
    {
        #region Feilds
        IHomePageLinkRepository HomePageLinkRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public HomePageLinkBL()
        {
            ConnectionString = null;
        }
        public HomePageLinkBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomePageLinkRepository != null)
                this.HomePageLinkRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiHomePageLink GetHomePageLinkByItemId(long itemId)
        {
            using (HomePageLinkRepository = new HomePageLinkRepository(ConnectionString))
            {
                return HomePageLinkRepository.GetById(itemId);
            }
        }

        public List<XsiHomePageLink> GetHomePageLink()
        {
            using (HomePageLinkRepository = new HomePageLinkRepository(ConnectionString))
            {
                return HomePageLinkRepository.Select();
            }
        }
        public List<XsiHomePageLink> GetHomePageLink(EnumSortlistBy sortListBy)
        {
            using (HomePageLinkRepository = new HomePageLinkRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomePageLinkRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomePageLinkRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomePageLinkRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomePageLinkRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return HomePageLinkRepository.Select().OrderBy(p => p.SortOrder).ToList();

                    default:
                        return HomePageLinkRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiHomePageLink> GetHomePageLink(XsiHomePageLink entity)
        {
            using (HomePageLinkRepository = new HomePageLinkRepository(ConnectionString))
            {
                return HomePageLinkRepository.Select(entity);
            }
        }
        public List<XsiHomePageLink> GetHomePageLink(XsiHomePageLink entity, EnumSortlistBy sortListBy)
        {
            using (HomePageLinkRepository = new HomePageLinkRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomePageLinkRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomePageLinkRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomePageLinkRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomePageLinkRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return HomePageLinkRepository.Select(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return HomePageLinkRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiHomePageLink> GetHomePageLinkOr(XsiHomePageLink entity, EnumSortlistBy sortListBy)
        {
            using (HomePageLinkRepository = new HomePageLinkRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomePageLinkRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomePageLinkRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomePageLinkRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomePageLinkRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return HomePageLinkRepository.SelectOr(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return HomePageLinkRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertHomePageLink(XsiHomePageLink entity)
        {
            using (HomePageLinkRepository = new HomePageLinkRepository(ConnectionString))
            {
                entity = HomePageLinkRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateHomePageLink(XsiHomePageLink entity)
        {
            XsiHomePageLink OriginalEntity = GetHomePageLinkByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (HomePageLinkRepository = new HomePageLinkRepository(ConnectionString))
                {
                    HomePageLinkRepository.Update(entity);
                    if (HomePageLinkRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateHomePageLinkStatus(long itemId)
        {
            XsiHomePageLink entity = GetHomePageLinkByItemId(itemId);
            if (entity != null)
            {
                using (HomePageLinkRepository = new HomePageLinkRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    HomePageLinkRepository.Update(entity);
                    if (HomePageLinkRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateHomePageLinkSortOrder(long groupId, long order)
        {
            EnumResultType result = EnumResultType.NotFound;

            XsiHomePageLink entity = GetHomePageLinkByItemId(groupId);
            if (entity != null)
            {
                using (HomePageLinkRepository = new HomePageLinkRepository(ConnectionString))
                {

                    entity.SortOrder = order;
                    HomePageLinkRepository.Update(entity);
                    if (HomePageLinkRepository.SubmitChanges() > 0)
                        result = EnumResultType.Success;
                    else
                        result = EnumResultType.Failed;
                }
            }

            return result;
        }
        public EnumResultType DeleteHomePageLink(XsiHomePageLink entity)
        {
            if (GetHomePageLink(entity).Count() > 0)
            {
                using (HomePageLinkRepository = new HomePageLinkRepository(ConnectionString))
                {
                    HomePageLinkRepository.Delete(entity);
                    if (HomePageLinkRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteHomePageLink(long itemId)
        {
            if (GetHomePageLinkByItemId(itemId) != null)
            {
                using (HomePageLinkRepository = new HomePageLinkRepository(ConnectionString))
                {
                    HomePageLinkRepository.Delete(itemId);
                    if (HomePageLinkRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
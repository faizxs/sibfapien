﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFEmailContentBL : IDisposable
    {
        #region Feilds
        ISCRFEmailContentRepository SCRFEmailContentRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFEmailContentBL()
        {
            ConnectionString = null;
        }
        public SCRFEmailContentBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFEmailContentRepository != null)
                this.SCRFEmailContentRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiScrfemailContent GetSCRFEmailContentByItemId(long itemId)
        {
            using (SCRFEmailContentRepository = new SCRFEmailContentRepository(ConnectionString))
            {
                return SCRFEmailContentRepository.GetById(itemId);
            }
        }
         
        public List<XsiScrfemailContent> GetSCRFEmailContent()
        {
            using (SCRFEmailContentRepository = new SCRFEmailContentRepository(ConnectionString))
            {
                return SCRFEmailContentRepository.Select();
            }
        }
        public List<XsiScrfemailContent> GetSCRFEmailContent(EnumSortlistBy sortListBy)
        {
            using (SCRFEmailContentRepository = new SCRFEmailContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFEmailContentRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFEmailContentRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFEmailContentRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFEmailContentRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFEmailContentRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfemailContent> GetSCRFEmailContent(XsiScrfemailContent entity)
        {
            using (SCRFEmailContentRepository = new SCRFEmailContentRepository(ConnectionString))
            {
                return SCRFEmailContentRepository.Select(entity);
            }
        }
        public List<XsiScrfemailContent> GetSCRFEmailContent(XsiScrfemailContent entity, EnumSortlistBy sortListBy)
        {
            using (SCRFEmailContentRepository = new SCRFEmailContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFEmailContentRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFEmailContentRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFEmailContentRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFEmailContentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFEmailContentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfemailContent> GetSCRFEmailContentOr(XsiScrfemailContent entity, EnumSortlistBy sortListBy)
        {
            using (SCRFEmailContentRepository = new SCRFEmailContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFEmailContentRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFEmailContentRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFEmailContentRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFEmailContentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFEmailContentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
         
        public EnumResultType InsertSCRFEmailContent(XsiScrfemailContent entity)
        {
            using (SCRFEmailContentRepository = new SCRFEmailContentRepository(ConnectionString))
            {
                entity = SCRFEmailContentRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFEmailContent(XsiScrfemailContent entity)
        {
            XsiScrfemailContent OriginalEntity = GetSCRFEmailContentByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFEmailContentRepository = new SCRFEmailContentRepository(ConnectionString))
                {
                    entity.RollbackXml = BusinessMethodFactory.GetRollbackXml(OriginalEntity);

                    SCRFEmailContentRepository.Update(entity);
                    if (SCRFEmailContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackSCRFEmailContent(long itemId)
        {
            XsiScrfemailContent OriginalEntity = GetSCRFEmailContentByItemId(itemId);

            if (OriginalEntity == null)
                return EnumResultType.NotFound;
            else if (string.IsNullOrEmpty(OriginalEntity.RollbackXml))
                return EnumResultType.Invalid;
            else
            {
                XsiScrfemailContent NewEntity = (XsiScrfemailContent)BusinessMethodFactory.GetXmlToObject(OriginalEntity.RollbackXml, OriginalEntity.GetType());
                OriginalEntity.RollbackXml = string.Empty;
                NewEntity.ItemId = itemId;
                NewEntity.RollbackXml = BusinessMethodFactory.GetRollbackXml(OriginalEntity);

                using (SCRFEmailContentRepository = new SCRFEmailContentRepository(ConnectionString))
                {
                    SCRFEmailContentRepository.Update(NewEntity);
                    if (SCRFEmailContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }

        }
        public EnumResultType DeleteSCRFEmailContent(XsiScrfemailContent entity)
        {
            List<XsiScrfemailContent> entities = GetSCRFEmailContent(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfemailContent e in entities)
                {
                    result = DeleteSCRFEmailContent(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFEmailContent(long itemId)
        {
            XsiScrfemailContent entity = GetSCRFEmailContentByItemId(itemId);
            if (entity != null)
            {
                using (SCRFEmailContentRepository = new SCRFEmailContentRepository(ConnectionString))
                {
                    SCRFEmailContentRepository.Delete(itemId);
                    if (SCRFEmailContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
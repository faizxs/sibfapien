﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionMemberBL : IDisposable
    {
        #region Feilds
        IExhibitionMemberRepository ExhibitionMemberRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionMemberBL()
        {
            ConnectionString = null;
        }
        public ExhibitionMemberBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionMemberRepository != null)
                this.ExhibitionMemberRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionMember GetExhibitionMemberByItemId(long itemId)
        {
            using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
            {
                return ExhibitionMemberRepository.GetById(itemId);
            }
        }
        
        public XsiExhibitionMember GetMemberByEmail(string email)
        {
            using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
            {
                return ExhibitionMemberRepository.Select(new XsiExhibitionMember() { Email = email }).FirstOrDefault();
            }
        }
        public XsiExhibitionMember GetMemberByUsername(string username)
        {
            using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
            {
                return ExhibitionMemberRepository.Select(new XsiExhibitionMember() { UserName = username }).FirstOrDefault();
            }
        }
        public List<XsiExhibitionMember> GetExhibitionMember()
        {
            using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
            {
                return ExhibitionMemberRepository.Select();
            }
        }
        public List<XsiExhibitionMember> GetExhibitionMember(EnumSortlistBy sortListBy)
        {
            using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionMemberRepository.Select().OrderBy(p => p.Firstname).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionMemberRepository.Select().OrderByDescending(p => p.Firstname).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionMemberRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionMemberRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionMemberRepository.Select().OrderBy(p => p.MemberId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionMemberRepository.Select().OrderByDescending(p => p.MemberId).ToList();

                    default:
                        return ExhibitionMemberRepository.Select().OrderByDescending(p => p.MemberId).ToList();
                }
            }
        }
        public List<XsiExhibitionMember> GetExhibitionMember(XsiExhibitionMember entity)
        {
            using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
            {
                return ExhibitionMemberRepository.Select(entity);
            }
        }
        public List<XsiExhibitionMember> GetExhibitionMember(XsiExhibitionMember entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionMemberRepository.Select(entity).OrderBy(p => p.Firstname).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionMemberRepository.Select(entity).OrderByDescending(p => p.Firstname).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionMemberRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionMemberRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionMemberRepository.Select(entity).OrderBy(p => p.MemberId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionMemberRepository.Select(entity).OrderByDescending(p => p.MemberId).ToList();

                    default:
                        return ExhibitionMemberRepository.Select(entity).OrderByDescending(p => p.MemberId).ToList();
                }
            }
        }
        public List<XsiExhibitionMember> GetExhibitionMemberOr(XsiExhibitionMember entity)
        {
            using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
            {
                return ExhibitionMemberRepository.SelectOr(entity);
            }
        }
        public List<XsiExhibitionMember> GetExhibitionMemberOr(XsiExhibitionMember entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionMemberRepository.SelectOr(entity).OrderBy(p => p.Firstname).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionMemberRepository.SelectOr(entity).OrderByDescending(p => p.Firstname).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionMemberRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionMemberRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionMemberRepository.SelectOr(entity).OrderBy(p => p.MemberId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionMemberRepository.SelectOr(entity).OrderByDescending(p => p.MemberId).ToList();

                    default:
                        return ExhibitionMemberRepository.SelectOr(entity).OrderByDescending(p => p.MemberId).ToList();
                }
            }
        }

        public EnumResultType InsertExhibitionMember(XsiExhibitionMember entity)
        {
            using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
            {
                entity = ExhibitionMemberRepository.Add(entity);
                if (entity.MemberId > 0)
                {
                    XsiItemdId = entity.MemberId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionMember(XsiExhibitionMember entity)
        {
            XsiExhibitionMember OriginalEntity = GetExhibitionMemberByItemId(entity.MemberId);

            if (OriginalEntity != null)
            {
                using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
                {

                    ExhibitionMemberRepository.Update(entity);
                    if (ExhibitionMemberRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionMemberStatus(long itemId)
        {
            XsiExhibitionMember entity = GetExhibitionMemberByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
                {
                    entity.IsVerified = EnumConversion.ToString(EnumBool.Yes) == entity.IsVerified ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    entity.IsEmailSent = EnumConversion.ToString(EnumBool.Yes) == entity.IsEmailSent ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionMemberRepository.Update(entity);
                    if (ExhibitionMemberRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionMember(XsiExhibitionMember entity)
        {
            List<XsiExhibitionMember> entities = GetExhibitionMember(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionMember e in entities)
                {
                    result = DeleteExhibitionMember(e.MemberId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionMember(long itemId)
        {
            XsiExhibitionMember entity = GetExhibitionMemberByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
                {
                    ExhibitionMemberRepository.Delete(itemId);
                    if (ExhibitionMemberRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
        #region custom method
        public EnumResultType DeleteAnonymousExhibitionMemberRegistration(long itemId)
        {
            XsiExhibitionMember entity = GetExhibitionMemberByItemId(itemId);
            if (entity != null)
            {
                if (entity.Status == EnumConversion.ToString(EnumExhibitionMemberStatus.EmailNotVerified))
                {
                    using (ExhibitionMemberRepository = new ExhibitionMemberRepository(ConnectionString))
                    {
                        ExhibitionMemberRepository.Delete(itemId);
                        if (ExhibitionMemberRepository.SubmitChanges() > 0)
                            return EnumResultType.Success;
                        else
                            return EnumResultType.Failed;
                    }
                }
                else
                    return EnumResultType.ValueInUse;
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
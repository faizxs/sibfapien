﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class WinnersBL : IDisposable
    {
        #region Feilds
        IWinnersRepository WinnersRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public WinnersBL()
        {
            ConnectionString = null;
        }
        public WinnersBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.WinnersRepository != null)
                this.WinnersRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiAwardWinners GetWinnersByItemId(long itemId)
        {
            using (WinnersRepository = new WinnersRepository(ConnectionString))
            {
                return WinnersRepository.GetById(itemId);
            }
        }
         
        public List<XsiAwardWinners> GetWinners()
        {
            using (WinnersRepository = new WinnersRepository(ConnectionString))
            {
                return WinnersRepository.Select();
            }
        }
        public List<XsiAwardWinners> GetWinners(EnumSortlistBy sortListBy)
        {
            using (WinnersRepository = new WinnersRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return WinnersRepository.Select().OrderBy(p => p.FirstNameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return WinnersRepository.Select().OrderByDescending(p => p.FirstNameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return WinnersRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return WinnersRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return WinnersRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return WinnersRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return WinnersRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiAwardWinners> GetWinners(XsiAwardWinners entity)
        {
            using (WinnersRepository = new WinnersRepository(ConnectionString))
            {
                return WinnersRepository.Select(entity);
            }
        }
        public List<XsiAwardWinners> GetWinners(XsiAwardWinners entity, EnumSortlistBy sortListBy)
        {
            using (WinnersRepository = new WinnersRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return WinnersRepository.Select(entity).OrderBy(p => p.FirstNameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return WinnersRepository.Select(entity).OrderByDescending(p => p.FirstNameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return WinnersRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return WinnersRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return WinnersRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return WinnersRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return WinnersRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiAwardWinners GetWinners(long groupId, long languageId)
        {
            using (WinnersRepository = new WinnersRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertWinners(XsiAwardWinners entity)
        {
            using (WinnersRepository = new WinnersRepository(ConnectionString))
            {
                entity = WinnersRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1; 
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateWinners(XsiAwardWinners entity)
        {
            XsiAwardWinners OriginalEntity = GetWinnersByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (WinnersRepository = new WinnersRepository(ConnectionString))
                {
                    WinnersRepository.Update(entity);
                    if (WinnersRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateWinnersStatus(long itemId)
        {
            XsiAwardWinners entity = GetWinnersByItemId(itemId);
            if (entity != null)
            {
                using (WinnersRepository = new WinnersRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    WinnersRepository.Update(entity);
                    if (WinnersRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteWinners(XsiAwardWinners entity)
        {
            List<XsiAwardWinners> entities = GetWinners(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiAwardWinners e in entities)
                {
                    result = DeleteWinners(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteWinners(long itemId)
        {
            XsiAwardWinners entity = GetWinnersByItemId(itemId);
            if (entity != null)
            {
                using (WinnersRepository = new WinnersRepository(ConnectionString))
                {
                    //delete news with news catefory id = itemid

                    WinnersRepository.Delete(itemId);
                    if (WinnersRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
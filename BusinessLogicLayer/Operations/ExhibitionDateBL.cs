﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionDateBL : IDisposable
    {
        #region Feilds
        IExhibitionDateRepository ExhibitionDateRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionDateBL()
        {
            ConnectionString = null;
        }
        public ExhibitionDateBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionDateRepository != null)
                this.ExhibitionDateRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (ExhibitionDateRepository = new ExhibitionDateRepository(ConnectionString))
            {
                return (ExhibitionDateRepository.Select().Max(p => p.GroupId) ?? 0) + 1;
            }
        }

        public XsiExhibitionDate GetExhibitionDateByItemId(long itemId)
        {
            using (ExhibitionDateRepository = new ExhibitionDateRepository(ConnectionString))
            {
                return ExhibitionDateRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionDate> GetExhibitionDate()
        {
            using (ExhibitionDateRepository = new ExhibitionDateRepository(ConnectionString))
            {
                return ExhibitionDateRepository.Select();
            }
        }
        public List<XsiExhibitionDate> GetExhibitionDate(EnumSortlistBy sortListBy)
        {
            using (ExhibitionDateRepository = new ExhibitionDateRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionDateRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionDateRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionDateRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionDateRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionDateRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionDate> GetExhibitionDate(XsiExhibitionDate entity)
        {
            using (ExhibitionDateRepository = new ExhibitionDateRepository(ConnectionString))
            {
                return ExhibitionDateRepository.Select(entity);
            }
        }
        public List<XsiExhibitionDate> GetExhibitionDate(XsiExhibitionDate entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionDateRepository = new ExhibitionDateRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionDateRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionDateRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionDateRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionDateRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionDateRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionDate GetExhibitionDate(long groupId, long languageId)
        {
            using (ExhibitionDateRepository = new ExhibitionDateRepository(ConnectionString))
            {
                return ExhibitionDateRepository.Select(new XsiExhibitionDate() { GroupId = groupId, LanguageId = languageId }).FirstOrDefault();
            }
        }
        public EnumResultType InsertExhibitionDate(XsiExhibitionDate entity)
        {
            using (ExhibitionDateRepository = new ExhibitionDateRepository(ConnectionString))
            {
                entity = ExhibitionDateRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = entity.GroupId ?? -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionDate(XsiExhibitionDate entity)
        {
            XsiExhibitionDate OriginalEntity = GetExhibitionDateByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionDateRepository = new ExhibitionDateRepository(ConnectionString))
                {
                    ExhibitionDateRepository.Update(entity);
                    if (ExhibitionDateRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionDateStatus(long itemId)
        {
            XsiExhibitionDate entity = GetExhibitionDateByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionDateRepository = new ExhibitionDateRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionDateRepository.Update(entity);
                    if (ExhibitionDateRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionDateSortOrder(long groupId, long order)
        {
            EnumResultType result = EnumResultType.NotFound;

            /*List<XsiExhibitionDate> entities = GetExhibitionDateByGroupId(groupId).ToList();
            if (entities != null)
            {
                using (ExhibitionDateRepository = new ExhibitionDateRepository(ConnectionString))
                {
                    foreach (XsiExhibitionDate entity in entities)
                    {
                        ExhibitionDateRepository.Update(entity);
                        if (ExhibitionDateRepository.SubmitChanges() > 0)
                            result = EnumResultType.Success;
                        else
                            result = EnumResultType.Failed;
                    }
                }
            }*/

            return result;
        }
        public EnumResultType DeleteExhibitionDate(XsiExhibitionDate entity)
        {
            if (GetExhibitionDate(entity).Count() > 0)
            {
                using (ExhibitionDateRepository = new ExhibitionDateRepository(ConnectionString))
                {
                    ExhibitionDateRepository.Delete(entity);
                    if (ExhibitionDateRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionDate(long itemId)
        {
            if (GetExhibitionDateByItemId(itemId) != null)
            {
                using (ExhibitionDateRepository = new ExhibitionDateRepository(ConnectionString))
                {
                    ExhibitionDateRepository.Delete(itemId);
                    if (ExhibitionDateRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
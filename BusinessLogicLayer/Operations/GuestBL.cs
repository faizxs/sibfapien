﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class GuestBL : IDisposable
    {
        #region Feilds
        IGuestRepository IGuestRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public GuestBL()
        {
            ConnectionString = null;
        }
        public GuestBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IGuestRepository != null)
                this.IGuestRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiGuests GetGuestByItemId(long itemId)
        {
            using (IGuestRepository = new GuestRepository(ConnectionString))
            {
                return IGuestRepository.GetById(itemId);
            }
        }
         
        public List<XsiGuests> GetGuest()
        {
            using (IGuestRepository = new GuestRepository(ConnectionString))
            {
                return IGuestRepository.Select();
            }
        }
        public List<XsiGuests> GetGuest(EnumSortlistBy sortListBy)
        {
            using (IGuestRepository = new GuestRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IGuestRepository.Select().OrderBy(p => p.GuestName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IGuestRepository.Select().OrderByDescending(p => p.GuestName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IGuestRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IGuestRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IGuestRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IGuestRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IGuestRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiGuests> GetGuest(XsiGuests entity)
        {
            using (IGuestRepository = new GuestRepository(ConnectionString))
            {
                return IGuestRepository.Select(entity);
            }

        }
        public List<XsiGuests> GetGuest(XsiGuests entity, EnumSortlistBy sortListBy)
        {
            using (IGuestRepository = new GuestRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IGuestRepository.Select(entity).OrderBy(p => p.GuestName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IGuestRepository.Select(entity).OrderByDescending(p => p.GuestName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IGuestRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IGuestRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IGuestRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IGuestRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IGuestRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiGuests> GetGuestOr(XsiGuests entity, EnumSortlistBy sortListBy)
        {
            using (IGuestRepository = new GuestRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IGuestRepository.SelectOr(entity).OrderBy(p => p.GuestName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IGuestRepository.SelectOr(entity).OrderByDescending(p => p.GuestName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IGuestRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IGuestRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IGuestRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IGuestRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IGuestRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public EnumResultType InsertGuest(XsiGuests entity)
        {
            using (IGuestRepository = new GuestRepository(ConnectionString))
            {
                entity = IGuestRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateGuest(XsiGuests entity)
        {
            XsiGuests OriginalEntity = GetGuestByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IGuestRepository = new GuestRepository(ConnectionString))
                {
                    IGuestRepository.Update(entity);
                    if (IGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateGuestStatus(long itemId)
        {
            XsiGuests entity = GetGuestByItemId(itemId);
            if (entity != null)
            {
                using (IGuestRepository = new GuestRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IGuestRepository.Update(entity);
                    if (IGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteGuest(XsiGuests entity)
        {
            List<XsiGuests> entities = GetGuest(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiGuests e in entities)
                {
                    result = DeleteGuest(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteGuest(long itemId)
        {
            XsiGuests entity = GetGuestByItemId(itemId);
            if (entity != null)
            {
                using (IGuestRepository = new GuestRepository(ConnectionString))
                {
                    IGuestRepository.Delete(itemId);
                    if (IGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
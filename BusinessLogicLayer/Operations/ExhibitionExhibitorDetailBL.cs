﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionExhibitorDetailBL : IDisposable
    {
        #region Feilds
        IExhibitionExhibitorDetailRepository ExhibitionExhibitorDetailRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionExhibitorDetailBL()
        {
            ConnectionString = null;
        }
        public ExhibitionExhibitorDetailBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionExhibitorDetailRepository != null)
                this.ExhibitionExhibitorDetailRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionExhibitorDetails GetExhibitorRegistrationDetailsByItemId(long ExhibitorDetailsId)
        {
            using (ExhibitionExhibitorDetailRepository = new ExhibitionExhibitorDetailRepository(ConnectionString))
            {
                return ExhibitionExhibitorDetailRepository.GetById(ExhibitorDetailsId);
            }
        }
             
        public List<XsiExhibitionExhibitorDetails> GetExhibitorRegistrationDetails()
        {
            using (ExhibitionExhibitorDetailRepository = new ExhibitionExhibitorDetailRepository(ConnectionString))
            {
                return ExhibitionExhibitorDetailRepository.Select();
            }
        }
        public List<XsiExhibitionExhibitorDetails> GetExhibitorRegistrationDetails(EnumSortlistBy sortListBy)
        {
            using (ExhibitionExhibitorDetailRepository = new ExhibitionExhibitorDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionExhibitorDetailRepository.Select().OrderBy(p => p.HallNumber).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionExhibitorDetailRepository.Select().OrderByDescending(p => p.HallNumber).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionExhibitorDetailRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionExhibitorDetailRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionExhibitorDetailRepository.Select().OrderBy(p => p.MemberExhibitionYearlyId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionExhibitorDetailRepository.Select().OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();

                    default:
                        return ExhibitionExhibitorDetailRepository.Select().OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();
                }
            }
        }
        public List<XsiExhibitionExhibitorDetails> GetExhibitorRegistrationDetails(XsiExhibitionExhibitorDetails entity)
        {
            using (ExhibitionExhibitorDetailRepository = new ExhibitionExhibitorDetailRepository(ConnectionString))
            {
                return ExhibitionExhibitorDetailRepository.Select(entity);
            }
        }
        public List<XsiExhibitionExhibitorDetails> GetExhibitorRegistrationDetails(XsiExhibitionExhibitorDetails entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionExhibitorDetailRepository = new ExhibitionExhibitorDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionExhibitorDetailRepository.Select(entity).OrderBy(p => p.HallNumber).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionExhibitorDetailRepository.Select(entity).OrderByDescending(p => p.HallNumber).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionExhibitorDetailRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionExhibitorDetailRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionExhibitorDetailRepository.Select(entity).OrderBy(p => p.MemberExhibitionYearlyId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionExhibitorDetailRepository.Select(entity).OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();

                    default:
                        return ExhibitionExhibitorDetailRepository.Select(entity).OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();
                }
            }
        }
             

        public EnumResultType InsertExhibitorRegistration(XsiExhibitionExhibitorDetails entity)
        {
            using (ExhibitionExhibitorDetailRepository = new ExhibitionExhibitorDetailRepository(ConnectionString))
            {
                XsiExhibitionExhibitorDetails where = new XsiExhibitionExhibitorDetails();
                where.ExhibitorDetailsId = entity.ExhibitorDetailsId;

                if (ExhibitionExhibitorDetailRepository.Select(where).Count() == 0)
                {
                    entity = ExhibitionExhibitorDetailRepository.Add(entity);
                    if (entity.MemberExhibitionYearlyId > 0)
                    {
                        XsiItemdId = entity.ExhibitorDetailsId;
                        
                        return EnumResultType.Success;
                    }
                    else
                    {
                        XsiItemdId = -1; 
                        return EnumResultType.Failed;
                    }
                }
                else
                    return EnumResultType.AlreadyExists;
            }
        }
        public EnumResultType UpdateExhibitorRegistration(XsiExhibitionExhibitorDetails entity)
        {
            XsiExhibitionExhibitorDetails OriginalEntity = GetExhibitorRegistrationDetailsByItemId(entity.ExhibitorDetailsId);

            if (OriginalEntity != null)
            {
                using (ExhibitionExhibitorDetailRepository = new ExhibitionExhibitorDetailRepository(ConnectionString))
                {
                    ExhibitionExhibitorDetailRepository.Update(entity);
                    if (ExhibitionExhibitorDetailRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
     
        public EnumResultType DeleteExhibitorRegistration(XsiExhibitionExhibitorDetails entity)
        {
            List<XsiExhibitionExhibitorDetails> entities = GetExhibitorRegistrationDetails(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionExhibitorDetails e in entities)
                {
                    result = DeleteExhibitorDetailsRegistration(e.ExhibitorDetailsId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitorDetailsRegistration(long ExhibitorDetailsId)
        {
            XsiExhibitionExhibitorDetails entity = GetExhibitorRegistrationDetailsByItemId(ExhibitorDetailsId);
            if (entity != null)
            {
                using (ExhibitionExhibitorDetailRepository = new ExhibitionExhibitorDetailRepository(ConnectionString))
                {
                    ExhibitionExhibitorDetailRepository.Delete(ExhibitorDetailsId);
                    if (ExhibitionExhibitorDetailRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
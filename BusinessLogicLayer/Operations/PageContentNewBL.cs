﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PageContentNewBL : IDisposable
    {
        #region Feilds
        IPagesContentNewRepository XsiPagesContentNewRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PageContentNewBL()
        {
            ConnectionString = null;
        }
        public PageContentNewBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiPagesContentNewRepository != null)
                this.XsiPagesContentNewRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiPagesContentNew GetPageContentNewByItemId(long itemId)
        {
            using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
            {
                return XsiPagesContentNewRepository.GetById(itemId);
            }
        }
        //public List<XsiPagesContentNew> GetPageContentNewByGroupId(long groupId)
        //{
        //    using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
        //    {
        //        return XsiPagesContentNewRepository.GetByGroupId(groupId);
        //    }
        //}

        public List<XsiPagesContentNew> GetPageContentNew()
        {
            using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
            {
                return XsiPagesContentNewRepository.Select();
            }
        }
        public List<XsiPagesContentNew> GetPageContentNew(EnumSortlistBy sortListBy)
        {
            using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiPagesContentNewRepository.Select().OrderBy(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiPagesContentNewRepository.Select().OrderByDescending(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiPagesContentNewRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiPagesContentNewRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiPagesContentNewRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPagesContentNew> GetPageContentNew(XsiPagesContentNew entity)
        {
            using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
            {
                return XsiPagesContentNewRepository.Select(entity);
            }
        }
        public List<XsiPagesContentNew> GetPageContentNew(XsiPagesContentNew entity, EnumSortlistBy sortListBy)
        {
            using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiPagesContentNewRepository.Select(entity).OrderBy(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiPagesContentNewRepository.Select(entity).OrderByDescending(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiPagesContentNewRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiPagesContentNewRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiPagesContentNewRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        //public XsiPagesContentNew GetPageContentNew(long itemId, long languageId)
        //{
        //    using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
        //    {
        //        return XsiPagesContentNewRepository.Select(new XsiPagesContentNew() { ItemId = itemId, LanguageId = languageId }).FirstOrDefault();
        //    }
        //}
        public List<XsiPagesContentNew> GetPageContentNewOr(XsiPagesContentNew entity, EnumSortlistBy sortListBy)
        {
            using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiPagesContentNewRepository.SelectOr(entity).OrderBy(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiPagesContentNewRepository.SelectOr(entity).OrderByDescending(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiPagesContentNewRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiPagesContentNewRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiPagesContentNewRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        //public List<sp_FetchSIBFSearchResults_Result> GetSearchContent(string searchString, long languageId)
        //{
        //    using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
        //    {
        //        return XsiPagesContentNewRepository.SelectSearch(searchString, languageId);
        //    }
        //}
        public EnumResultType InsertPageContentNew(XsiPagesContentNew entity)
        {
            using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
            {
                entity = XsiPagesContentNewRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePageContentNew(XsiPagesContentNew entity)
        {
            XsiPagesContentNew OriginalEntity = GetPageContentNewByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line
                    entity.RollbackXml = BusinessMethodFactory.GetRollbackXml(OriginalEntity);

                    XsiPagesContentNewRepository.Update(entity);
                    if (XsiPagesContentNewRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackPageContentNew(long itemId)
        {
            //if no RollbackXml propery in entity then delete below code and return EnumResultType.Invalid;

            XsiPagesContentNew OriginalEntity = GetPageContentNewByItemId(itemId);

            if (OriginalEntity == null)
                return EnumResultType.NotFound;
            else if (string.IsNullOrEmpty(OriginalEntity.RollbackXml))
                return EnumResultType.Invalid;
            else
            {

                XsiPagesContentNew NewEntity = (XsiPagesContentNew)BusinessMethodFactory.GetXmlToObject(OriginalEntity.RollbackXml, OriginalEntity.GetType());

                OriginalEntity.RollbackXml = string.Empty;

                NewEntity.ItemId = itemId;
                NewEntity.RollbackXml = BusinessMethodFactory.GetRollbackXml(OriginalEntity);

                using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
                {
                    XsiPagesContentNewRepository.Update(NewEntity);
                    if (XsiPagesContentNewRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }

        }
        public EnumResultType UpdatePageContentNewStatus(long itemId)
        {
            XsiPagesContentNew entity = GetPageContentNewByItemId(itemId);
            if (entity != null)
            {
                using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    XsiPagesContentNewRepository.Update(entity);
                    if (XsiPagesContentNewRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePageContentNew(XsiPagesContentNew entity)
        {
            if (GetPageContentNew(entity).Count() > 0)
            {
                using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
                {
                    XsiPagesContentNewRepository.Delete(entity);
                    if (XsiPagesContentNewRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePageContentNew(long itemId)
        {
            if (GetPageContentNewByItemId(itemId) != null)
            {
                using (XsiPagesContentNewRepository = new PagesContentNewRepository(ConnectionString))
                {
                    XsiPagesContentNewRepository.Delete(itemId);
                    if (XsiPagesContentNewRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
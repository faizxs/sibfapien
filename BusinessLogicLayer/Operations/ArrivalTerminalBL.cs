﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ArrivalTerminalBL : IDisposable
    {
        #region Feilds
        IArrivalTerminalRepository ArrivalTerminalRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ArrivalTerminalBL()
        {
            ConnectionString = null;
        }
        public ArrivalTerminalBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ArrivalTerminalRepository != null)
                this.ArrivalTerminalRepository.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiArrivalTerminal GetArrivalTerminalByItemId(long itemId)
        {
            using (ArrivalTerminalRepository = new ArrivalTerminalRepository(ConnectionString))
            {
                return ArrivalTerminalRepository.GetById(itemId);
            }
        }
        
        public List<XsiArrivalTerminal> GetArrivalTerminal()
        {
            using (ArrivalTerminalRepository = new ArrivalTerminalRepository(ConnectionString))
            {
                return ArrivalTerminalRepository.Select();
            }
        }
        public List<XsiArrivalTerminal> GetArrivalTerminal(EnumSortlistBy sortListBy)
        {
            using (ArrivalTerminalRepository = new ArrivalTerminalRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ArrivalTerminalRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ArrivalTerminalRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ArrivalTerminalRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ArrivalTerminalRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ArrivalTerminalRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ArrivalTerminalRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ArrivalTerminalRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiArrivalTerminal> GetArrivalTerminal(XsiArrivalTerminal entity)
        {
            using (ArrivalTerminalRepository = new ArrivalTerminalRepository(ConnectionString))
            {
                return ArrivalTerminalRepository.Select(entity);
            }
        }
        public List<XsiArrivalTerminal> GetArrivalTerminal(XsiArrivalTerminal entity, EnumSortlistBy sortListBy)
        {
            using (ArrivalTerminalRepository = new ArrivalTerminalRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ArrivalTerminalRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ArrivalTerminalRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ArrivalTerminalRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ArrivalTerminalRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ArrivalTerminalRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ArrivalTerminalRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ArrivalTerminalRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiArrivalTerminal> GetCompleteArrivalTerminal(XsiArrivalTerminal entity, EnumSortlistBy sortListBy)
        {
            using (ArrivalTerminalRepository = new ArrivalTerminalRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ArrivalTerminalRepository.SelectComeplete(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ArrivalTerminalRepository.SelectComeplete(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ArrivalTerminalRepository.SelectComeplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ArrivalTerminalRepository.SelectComeplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ArrivalTerminalRepository.SelectComeplete(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ArrivalTerminalRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ArrivalTerminalRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiArrivalTerminal GetArrivalTerminal(long itemId, long categoryId)
        {
            using (ArrivalTerminalRepository = new ArrivalTerminalRepository(ConnectionString))
            {
                {
                    return ArrivalTerminalRepository.Select(new XsiArrivalTerminal() { ItemId = itemId, CategoryId = categoryId }).FirstOrDefault();
                }
            }
        }

        public EnumResultType InsertArrivalTerminal(XsiArrivalTerminal entity)
        {
            using (ArrivalTerminalRepository = new ArrivalTerminalRepository(ConnectionString))
            {
                entity = ArrivalTerminalRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateArrivalTerminal(XsiArrivalTerminal entity)
        {
            XsiArrivalTerminal OriginalEntity = GetArrivalTerminalByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ArrivalTerminalRepository = new ArrivalTerminalRepository(ConnectionString))
                {
                    ArrivalTerminalRepository.Update(entity);
                    if (ArrivalTerminalRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateArrivalTerminalStatus(long itemId)
        {
            XsiArrivalTerminal entity = GetArrivalTerminalByItemId(itemId);
            if (entity != null)
            {
                using (ArrivalTerminalRepository = new ArrivalTerminalRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ArrivalTerminalRepository.Update(entity);
                    if (ArrivalTerminalRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteArrivalTerminal(XsiArrivalTerminal entity)
        {
            List<XsiArrivalTerminal> entities = GetArrivalTerminal(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiArrivalTerminal e in entities)
                {
                    result = DeleteArrivalTerminal(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteArrivalTerminal(long itemId)
        {
            XsiArrivalTerminal entity = GetArrivalTerminalByItemId(itemId);
            if (entity != null)
            {
                using (ArrivalTerminalRepository = new ArrivalTerminalRepository(ConnectionString))
                {
                    ArrivalTerminalRepository.Delete(itemId);
                    if (ArrivalTerminalRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
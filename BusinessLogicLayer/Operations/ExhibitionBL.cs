﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionBL : IDisposable
    {
        #region Feilds
        IExhibitionRepository ExhibitionRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionBL()
        {
            ConnectionString = null;
        }
        public ExhibitionBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionRepository != null)
                this.ExhibitionRepository.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiExhibition GetExhibitionByItemId(long itemId)
        {
            using (ExhibitionRepository = new ExhibitionRepository(ConnectionString))
            {
                return ExhibitionRepository.GetById(itemId);
            }
        }
          
        public List<XsiExhibition> GetExhibition()
        {
            using (ExhibitionRepository = new ExhibitionRepository(ConnectionString))
            {
                return ExhibitionRepository.Select();
            }
        }
        public List<XsiExhibition> GetExhibition(EnumSortlistBy sortListBy)
        {
            using (ExhibitionRepository = new ExhibitionRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionRepository.Select().OrderBy(p => p.ExhibitionNumber).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionRepository.Select().OrderByDescending(p => p.ExhibitionNumber).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionRepository.Select().OrderBy(p => p.ExhibitionId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionRepository.Select().OrderByDescending(p => p.ExhibitionId).ToList();

                    default:
                        return ExhibitionRepository.Select().OrderByDescending(p => p.ExhibitionId).ToList();
                }
            }
        }
        public List<XsiExhibition> GetExhibition(XsiExhibition entity)
        {
            using (ExhibitionRepository = new ExhibitionRepository(ConnectionString))
            {
                return ExhibitionRepository.Select(entity);
            }
        }
        public List<XsiExhibition> GetExhibition(XsiExhibition entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionRepository = new ExhibitionRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionRepository.Select(entity).OrderBy(p => p.ExhibitionNumber).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionRepository.Select(entity).OrderByDescending(p => p.ExhibitionNumber).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionRepository.Select(entity).OrderBy(p => p.ExhibitionId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionRepository.Select(entity).OrderByDescending(p => p.ExhibitionId).ToList();

                    default:
                        return ExhibitionRepository.Select(entity).OrderByDescending(p => p.ExhibitionId).ToList();
                }
            }
        }
       
        public EnumResultType InsertExhibition(XsiExhibition entity)
        {
            using (ExhibitionRepository = new ExhibitionRepository(ConnectionString))
            {
                entity = ExhibitionRepository.Add(entity);
                if (entity.ExhibitionId > 0)
                {
                    XsiItemdId = entity.ExhibitionId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibition(XsiExhibition entity)
        {
            XsiExhibition OriginalEntity = GetExhibitionByItemId(entity.ExhibitionId);

            if (OriginalEntity != null)
            {
                using (ExhibitionRepository = new ExhibitionRepository(ConnectionString))
                {
                    ExhibitionRepository.Update(entity);
                    if (ExhibitionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionStatus(long itemId)
        {
            XsiExhibition entity = GetExhibitionByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionRepository = new ExhibitionRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionRepository.Update(entity);
                    if (ExhibitionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionArchiveStatus(long itemId)
        {
            XsiExhibition entity = GetExhibitionByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionRepository = new ExhibitionRepository(ConnectionString))
                {
                    entity.IsArchive = EnumConversion.ToString(EnumBool.Yes) == entity.IsArchive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionRepository.Update(entity);
                    if (ExhibitionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibition(XsiExhibition entity)
        {
            List<XsiExhibition> entities = GetExhibition(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibition e in entities)
                {
                    result = DeleteExhibition(e.ExhibitionId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibition(long itemId)
        {
            XsiExhibition entity = GetExhibitionByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionRepository = new ExhibitionRepository(ConnectionString))
                {
                    ExhibitionRepository.Delete(itemId);
                    if (ExhibitionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
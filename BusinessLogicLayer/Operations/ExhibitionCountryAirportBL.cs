﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionCountryAirportBL : IDisposable
    {
        #region Feilds
        IExhibitionCountryAirportRepository ExhibitionCountryAirportRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionCountryAirportBL()
        {
            ConnectionString = null;
        }
        public ExhibitionCountryAirportBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionCountryAirportRepository != null)
                this.ExhibitionCountryAirportRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionCountryAirport GetExhibitionCountryAirportByItemId(long itemId)
        {
            using (ExhibitionCountryAirportRepository = new ExhibitionCountryAirportRepository(ConnectionString))
            {
                return ExhibitionCountryAirportRepository.GetById(itemId);
            }
        }
        public List<XsiExhibitionCountryAirport> GetExhibitionCountryAirport()
        {
            using (ExhibitionCountryAirportRepository = new ExhibitionCountryAirportRepository(ConnectionString))
            {
                return ExhibitionCountryAirportRepository.Select();
            }
        }
        public List<XsiExhibitionCountryAirport> GetExhibitionCountryAirport(EnumSortlistBy sortListBy)
        {
            using (ExhibitionCountryAirportRepository = new ExhibitionCountryAirportRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCountryAirportRepository.Select().OrderBy(p => p.AirportName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCountryAirportRepository.Select().OrderByDescending(p => p.AirportName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCountryAirportRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCountryAirportRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCountryAirportRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCountryAirportRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionCountryAirportRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionCountryAirport> GetExhibitionCountryAirport(XsiExhibitionCountryAirport entity)
        {
            using (ExhibitionCountryAirportRepository = new ExhibitionCountryAirportRepository(ConnectionString))
            {
                return ExhibitionCountryAirportRepository.Select(entity);
            }
        }
        public List<XsiExhibitionCountryAirport> GetExhibitionCountryAirport(XsiExhibitionCountryAirport entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionCountryAirportRepository = new ExhibitionCountryAirportRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCountryAirportRepository.Select(entity).OrderBy(p => p.AirportName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCountryAirportRepository.Select(entity).OrderByDescending(p => p.AirportName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCountryAirportRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCountryAirportRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCountryAirportRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCountryAirportRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionCountryAirportRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionCountryAirport> GetCompleteExhibitionCountryAirport(XsiExhibitionCountryAirport entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionCountryAirportRepository = new ExhibitionCountryAirportRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCountryAirportRepository.SelectComplete(entity).OrderBy(p => p.AirportName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCountryAirportRepository.SelectComplete(entity).OrderByDescending(p => p.AirportName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCountryAirportRepository.SelectComplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCountryAirportRepository.SelectComplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCountryAirportRepository.SelectComplete(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCountryAirportRepository.SelectComplete(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionCountryAirportRepository.SelectComplete(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertExhibitionCountryAirport(XsiExhibitionCountryAirport entity)
        {
            using (ExhibitionCountryAirportRepository = new ExhibitionCountryAirportRepository(ConnectionString))
            {
                entity = ExhibitionCountryAirportRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionCountryAirport(XsiExhibitionCountryAirport entity)
        {
            XsiExhibitionCountryAirport OriginalEntity = GetExhibitionCountryAirportByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionCountryAirportRepository = new ExhibitionCountryAirportRepository(ConnectionString))
                {
                    ExhibitionCountryAirportRepository.Update(entity);
                    if (ExhibitionCountryAirportRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionCountryAirportStatus(long itemId)
        {
            XsiExhibitionCountryAirport entity = GetExhibitionCountryAirportByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionCountryAirportRepository = new ExhibitionCountryAirportRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionCountryAirportRepository.Update(entity);
                    if (ExhibitionCountryAirportRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionCountryAirport(XsiExhibitionCountryAirport entity)
        {
            List<XsiExhibitionCountryAirport> entities = GetExhibitionCountryAirport(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionCountryAirport e in entities)
                {
                    result = DeleteExhibitionCountryAirport(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionCountryAirport(long itemId)
        {
            XsiExhibitionCountryAirport entity = GetExhibitionCountryAirportByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionCountryAirportRepository = new ExhibitionCountryAirportRepository(ConnectionString))
                {
                    ExhibitionCountryAirportRepository.Delete(itemId);
                    if (ExhibitionCountryAirportRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
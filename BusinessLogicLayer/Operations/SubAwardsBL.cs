﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SubAwardsBL : IDisposable
    {
        #region Feilds
        ISubAwardsRepository SubAwardsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SubAwardsBL()
        {
            ConnectionString = null;
        }
        public SubAwardsBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SubAwardsRepository != null)
                this.SubAwardsRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiSubAwards GetSubAwardsByItemId(long itemId)
        {
            using (SubAwardsRepository = new SubAwardsRepository(ConnectionString))
            {
                return SubAwardsRepository.GetById(itemId);
            }
        }
        
        public List<XsiSubAwards> GetSubAwards()
        {
            using (SubAwardsRepository = new SubAwardsRepository(ConnectionString))
            {
                return SubAwardsRepository.Select();
            }
        }
        public List<XsiSubAwards> GetSubAwards(EnumSortlistBy sortListBy)
        {
            using (SubAwardsRepository = new SubAwardsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SubAwardsRepository.Select().OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SubAwardsRepository.Select().OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SubAwardsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SubAwardsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SubAwardsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SubAwardsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SubAwardsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiSubAwards> GetSubAwards(XsiSubAwards entity)
        {
            using (SubAwardsRepository = new SubAwardsRepository(ConnectionString))
            {
                return SubAwardsRepository.Select(entity);
            }
        }
        public List<XsiSubAwards> GetSubAwards(XsiSubAwards entity, EnumSortlistBy sortListBy)
        {
            using (SubAwardsRepository = new SubAwardsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SubAwardsRepository.Select(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SubAwardsRepository.Select(entity).OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SubAwardsRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SubAwardsRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SubAwardsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SubAwardsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SubAwardsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiSubAwards> GetCompleteSubAwards(XsiSubAwards entity, EnumSortlistBy sortListBy)
        {
            using (SubAwardsRepository = new SubAwardsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SubAwardsRepository.SelectComeplete(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SubAwardsRepository.SelectComeplete(entity).OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SubAwardsRepository.SelectComeplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SubAwardsRepository.SelectComeplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SubAwardsRepository.SelectComeplete(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SubAwardsRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SubAwardsRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
 
        public EnumResultType InsertSubAwards(XsiSubAwards entity)
        {
            using (SubAwardsRepository = new SubAwardsRepository(ConnectionString))
            {
                entity = SubAwardsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSubAwards(XsiSubAwards entity)
        {
            XsiSubAwards OriginalEntity = GetSubAwardsByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SubAwardsRepository = new SubAwardsRepository(ConnectionString))
                {
                    SubAwardsRepository.Update(entity);
                    if (SubAwardsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSubAwardsStatus(long itemId)
        {
            XsiSubAwards entity = GetSubAwardsByItemId(itemId);
            if (entity != null)
            {
                using (SubAwardsRepository = new SubAwardsRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SubAwardsRepository.Update(entity);
                    if (SubAwardsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSubAwards(XsiSubAwards entity)
        {
            List<XsiSubAwards> entities = GetSubAwards(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiSubAwards e in entities)
                {
                    result = DeleteSubAwards(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSubAwards(long itemId)
        {
            XsiSubAwards entity = GetSubAwardsByItemId(itemId);
            if (entity != null)
            {
                using (SubAwardsRepository = new SubAwardsRepository(ConnectionString))
                {
                    SubAwardsRepository.Delete(itemId);
                    if (SubAwardsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
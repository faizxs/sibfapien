﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class NewsletterBannerBL : IDisposable
    {
        #region Feilds
        INewsletterBannerRepository NewsletterBannerRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public NewsletterBannerBL()
        {
            ConnectionString = null;
        }
        public NewsletterBannerBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.NewsletterBannerRepository != null)
                this.NewsletterBannerRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiNewsletterBanner GetNewsletterBannerByItemId(long itemId)
        {
            using (NewsletterBannerRepository = new NewsletterBannerRepository(ConnectionString))
            {
                return NewsletterBannerRepository.GetById(itemId);
            }
        }

        public List<XsiNewsletterBanner> GetNewsletterBanner()
        {
            using (NewsletterBannerRepository = new NewsletterBannerRepository(ConnectionString))
            {
                return NewsletterBannerRepository.Select();
            }
        }
        public List<XsiNewsletterBanner> GetNewsletterBanner(EnumSortlistBy sortListBy)
        {
            using (NewsletterBannerRepository = new NewsletterBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsletterBannerRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsletterBannerRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return NewsletterBannerRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return NewsletterBannerRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsletterBannerRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsletterBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsletterBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiNewsletterBanner> GetNewsletterBanner(XsiNewsletterBanner entity)
        {
            using (NewsletterBannerRepository = new NewsletterBannerRepository(ConnectionString))
            {
                return NewsletterBannerRepository.Select(entity);
            }
        }
        public List<XsiNewsletterBanner> GetNewsletterBanner(XsiNewsletterBanner entity, EnumSortlistBy sortListBy)
        {
            using (NewsletterBannerRepository = new NewsletterBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsletterBannerRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsletterBannerRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return NewsletterBannerRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return NewsletterBannerRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsletterBannerRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsletterBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsletterBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertNewsletterBanner(XsiNewsletterBanner entity)
        {
            using (NewsletterBannerRepository = new NewsletterBannerRepository(ConnectionString))
            {
                entity = NewsletterBannerRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateNewsletterBanner(XsiNewsletterBanner entity)
        {
            XsiNewsletterBanner OriginalEntity = GetNewsletterBannerByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (NewsletterBannerRepository = new NewsletterBannerRepository(ConnectionString))
                {
                    NewsletterBannerRepository.Update(entity);
                    if (NewsletterBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateNewsletterBannerStatus(long itemId)
        {
            XsiNewsletterBanner entity = GetNewsletterBannerByItemId(itemId);
            if (entity != null)
            {
                using (NewsletterBannerRepository = new NewsletterBannerRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    NewsletterBannerRepository.Update(entity);
                    if (NewsletterBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteNewsletterBanner(XsiNewsletterBanner entity)
        {
            List<XsiNewsletterBanner> entities = GetNewsletterBanner(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiNewsletterBanner e in entities)
                {
                    result = DeleteNewsletterBanner(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteNewsletterBanner(long itemId)
        {
            XsiNewsletterBanner entity = GetNewsletterBannerByItemId(itemId);
            if (entity != null)
            {
                using (NewsletterBannerRepository = new NewsletterBannerRepository(ConnectionString))
                {
                    NewsletterBannerRepository.Delete(itemId);
                    if (NewsletterBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class AwardsBL : IDisposable
    {
        #region Feilds
        IAwardsRepository AwardRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public AwardsBL()
        {
            ConnectionString = null;
        }
        public AwardsBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AwardRepository != null)
                this.AwardRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiAwards GetAwardsByItemId(long itemId)
        {
            using (AwardRepository = new AwardRepository(ConnectionString))
            {
                return AwardRepository.GetById(itemId);
            }
        }
        
        public List<XsiAwards> GetAwards()
        {
            using (AwardRepository = new AwardRepository(ConnectionString))
            {
                return AwardRepository.Select();
            }
        }
        public List<XsiAwards> GetAwards(EnumSortlistBy sortListBy)
        {
            using (AwardRepository = new AwardRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return AwardRepository.Select().OrderBy(p => p.Name).ToList().ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return AwardRepository.Select().OrderByDescending(p => p.Name).ToList().ToList();

                    ////case EnumSortlistBy.ByDateAsc:
                    ////    return AwardRepository.Select().OrderBy(p => p.CreatedOn).ToList().ToList();

                    ////case EnumSortlistBy.ByDateDesc:
                    ////    return AwardRepository.Select().OrderByDescending(p => p.CreatedOn).ToList().ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return AwardRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AwardRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return AwardRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiAwards> GetAwards(XsiAwards entity)
        {
            using (AwardRepository = new AwardRepository(ConnectionString))
            {
                return AwardRepository.Select(entity);
            }
        }
        public List<XsiAwards> GetAwards(XsiAwards entity, EnumSortlistBy sortListBy)
        {
            using (AwardRepository = new AwardRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return AwardRepository.Select(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return AwardRepository.Select(entity).OrderByDescending(p => p.Name).ToList();

                    //case EnumSortlistBy.ByDateAsc:
                    //    return AwardRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    //case EnumSortlistBy.ByDateDesc:
                    //    return AwardRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return AwardRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AwardRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return AwardRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiAwards> GetAwardsOr(XsiAwards entity, EnumSortlistBy sortListBy)
        {
            using (AwardRepository = new AwardRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return AwardRepository.SelectOr(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return AwardRepository.SelectOr(entity).OrderByDescending(p => p.Name).ToList();

                    //case EnumSortlistBy.ByDateAsc:
                    //    return AwardRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    //case EnumSortlistBy.ByDateDesc:
                    //    return AwardRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return AwardRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AwardRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return AwardRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public List<XsiAwards> GetOtherLanguageAwards(XsiAwards entity)
        {
            using (AwardRepository = new AwardRepository(ConnectionString))
            {
                return AwardRepository.SelectOtherLanguageCategory(entity);
            }
        }
        public List<XsiAwards> GetCurrentLanguageAwards(XsiAwards entity)
        {
            using (AwardRepository = new AwardRepository(ConnectionString))
            {
                return AwardRepository.SelectCurrentLanguageCategory(entity);
            }
        }

        public EnumResultType InsertAwards(XsiAwards entity)
        {
            using (AwardRepository = new AwardRepository(ConnectionString))
            {
                entity = AwardRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateAwards(XsiAwards entity)
        {
            XsiAwards OriginalEntity = GetAwardsByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (AwardRepository = new AwardRepository(ConnectionString))
                {
                    AwardRepository.Update(entity);
                    if (AwardRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateAwardsStatus(long itemId)
        {
            XsiAwards entity = GetAwardsByItemId(itemId);
            if (entity != null)
            {
                using (AwardRepository = new AwardRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    AwardRepository.Update(entity);
                    if (AwardRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteAwards(XsiAwards entity)
        {
            List<XsiAwards> entities = GetAwards(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiAwards e in entities)
                {
                    result = DeleteAwards(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteAwards(long itemId)
        {
            XsiAwards entity = GetAwardsByItemId(itemId);
            if (entity != null)
            {
                using (AwardRepository = new AwardRepository(ConnectionString))
                {
                    //delete news with news catefory id = itemid

                    AwardRepository.Delete(itemId);
                    if (AwardRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
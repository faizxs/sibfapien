﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PhotoAlbumBL : IDisposable
    {
        #region Feilds
        IPhotoAlbumRepository PhotoAlbumRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PhotoAlbumBL()
        {
            ConnectionString = null;
        }
        public PhotoAlbumBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PhotoAlbumRepository != null)
                this.PhotoAlbumRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextSortIndex()
        {
            //optional method. return -1 if not required and remove following
            using (PhotoAlbumRepository = new PhotoAlbumRepository(ConnectionString))
            {
                return (PhotoAlbumRepository.Select().Max(p => p.SortIndex) ?? 0) + 1;
            }
        }

        public XsiPhotoAlbum GetPhotoAlbumByItemId(long itemId)
        {
            using (PhotoAlbumRepository = new PhotoAlbumRepository(ConnectionString))
            {
                return PhotoAlbumRepository.GetById(itemId);
            }
        }

        public List<XsiPhotoAlbum> GetPhotoAlbum()
        {
            using (PhotoAlbumRepository = new PhotoAlbumRepository(ConnectionString))
            {
                return PhotoAlbumRepository.Select();
            }
        }
        public List<XsiPhotoAlbum> GetPhotoAlbum(EnumSortlistBy sortListBy)
        {
            using (PhotoAlbumRepository = new PhotoAlbumRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PhotoAlbumRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PhotoAlbumRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PhotoAlbumRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PhotoAlbumRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PhotoAlbumRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PhotoAlbumRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PhotoAlbumRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPhotoAlbum> GetPhotoAlbum(XsiPhotoAlbum entity)
        {
            using (PhotoAlbumRepository = new PhotoAlbumRepository(ConnectionString))
            {
                return PhotoAlbumRepository.Select(entity);
            }
        }
        public List<XsiPhotoAlbum> GetPhotoAlbum(XsiPhotoAlbum entity, EnumSortlistBy sortListBy)
        {
            using (PhotoAlbumRepository = new PhotoAlbumRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PhotoAlbumRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PhotoAlbumRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PhotoAlbumRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PhotoAlbumRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PhotoAlbumRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PhotoAlbumRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.BySortOrderAsc:
                        return PhotoAlbumRepository.Select(entity).OrderBy(p => p.SortIndex).ToList();

                    case EnumSortlistBy.BySortOrderDesc:
                        return PhotoAlbumRepository.Select(entity).OrderByDescending(p => p.SortIndex).ToList();

                    default:
                        return PhotoAlbumRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertPhotoAlbum(XsiPhotoAlbum entity)
        {
            using (PhotoAlbumRepository = new PhotoAlbumRepository(ConnectionString))
            {
                entity = PhotoAlbumRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePhotoAlbum(XsiPhotoAlbum entity)
        {
            XsiPhotoAlbum OriginalEntity = GetPhotoAlbumByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PhotoAlbumRepository = new PhotoAlbumRepository(ConnectionString))
                {
                    PhotoAlbumRepository.Update(entity);
                    if (PhotoAlbumRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdatePhotoAlbumStatus(long itemId, long langId)
        {
            XsiPhotoAlbum entity = GetPhotoAlbumByItemId(itemId);
            if (entity != null)
            {
                using (PhotoAlbumRepository = new PhotoAlbumRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    PhotoAlbumRepository.Update(entity);
                    if (PhotoAlbumRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePhotoAlbum(XsiPhotoAlbum entity)
        {
            List<XsiPhotoAlbum> entities = GetPhotoAlbum(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiPhotoAlbum e in entities)
                {
                    result = DeletePhotoAlbum(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeletePhotoAlbum(long itemId)
        {
            XsiPhotoAlbum entity = GetPhotoAlbumByItemId(itemId);
            if (entity != null)
            {
                using (PhotoAlbumRepository = new PhotoAlbumRepository(ConnectionString))
                {
                    PhotoAlbumRepository.Delete(itemId);
                    if (PhotoAlbumRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
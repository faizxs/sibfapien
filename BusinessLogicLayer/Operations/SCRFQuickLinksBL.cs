﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFQuickLinksBL : IDisposable
    {
        #region Feilds
        ISCRFQuickLinksRepository SCRFQuickLinksRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFQuickLinksBL()
        {
            ConnectionString = null;
        }
        public SCRFQuickLinksBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFQuickLinksRepository != null)
                this.SCRFQuickLinksRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrfquickLinks GetSCRFQuickLinksByItemId(long itemId)
        {
            using (SCRFQuickLinksRepository = new SCRFQuickLinksRepository(ConnectionString))
            {
                return SCRFQuickLinksRepository.GetById(itemId);
            }
        }

        public List<XsiScrfquickLinks> GetSCRFQuickLinks()
        {
            using (SCRFQuickLinksRepository = new SCRFQuickLinksRepository(ConnectionString))
            {
                return SCRFQuickLinksRepository.Select();
            }
        }
        public List<XsiScrfquickLinks> GetSCRFQuickLinks(EnumSortlistBy sortListBy)
        {
            using (SCRFQuickLinksRepository = new SCRFQuickLinksRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFQuickLinksRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFQuickLinksRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFQuickLinksRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFQuickLinksRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return SCRFQuickLinksRepository.Select().OrderBy(p => p.SortOrder).ToList();

                    default:
                        return SCRFQuickLinksRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfquickLinks> GetSCRFQuickLinks(XsiScrfquickLinks entity)
        {
            using (SCRFQuickLinksRepository = new SCRFQuickLinksRepository(ConnectionString))
            {
                return SCRFQuickLinksRepository.Select(entity);
            }
        }
        public List<XsiScrfquickLinks> GetSCRFQuickLinks(XsiScrfquickLinks entity, EnumSortlistBy sortListBy)
        {
            using (SCRFQuickLinksRepository = new SCRFQuickLinksRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFQuickLinksRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFQuickLinksRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFQuickLinksRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFQuickLinksRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return SCRFQuickLinksRepository.Select(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return SCRFQuickLinksRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfquickLinks> GetSCRFQuickLinksOr(XsiScrfquickLinks entity, EnumSortlistBy sortListBy)
        {
            using (SCRFQuickLinksRepository = new SCRFQuickLinksRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFQuickLinksRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFQuickLinksRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFQuickLinksRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFQuickLinksRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return SCRFQuickLinksRepository.SelectOr(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return SCRFQuickLinksRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertSCRFQuickLinks(XsiScrfquickLinks entity)
        {
            using (SCRFQuickLinksRepository = new SCRFQuickLinksRepository(ConnectionString))
            {
                entity = SCRFQuickLinksRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFQuickLinks(XsiScrfquickLinks entity)
        {
            XsiScrfquickLinks OriginalEntity = GetSCRFQuickLinksByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFQuickLinksRepository = new SCRFQuickLinksRepository(ConnectionString))
                {
                    SCRFQuickLinksRepository.Update(entity);
                    if (SCRFQuickLinksRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFQuickLinksStatus(long itemId)
        {
            XsiScrfquickLinks entity = GetSCRFQuickLinksByItemId(itemId);
            if (entity != null)
            {
                using (SCRFQuickLinksRepository = new SCRFQuickLinksRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFQuickLinksRepository.Update(entity);
                    if (SCRFQuickLinksRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFQuickLinksSortOrder(long groupId, long order)
        {
            EnumResultType result = EnumResultType.NotFound;

            /*List<XsiScrfquickLinks> entities = GetSCRFQuickLinksByGroupId(groupId).ToList();
            if (entities != null)
            {
                using (SCRFQuickLinksRepository = new SCRFQuickLinksRepository(ConnectionString))
                {
                    foreach (XsiScrfquickLinks entity in entities)
                    {
                        entity.SortOrder = order;
                        SCRFQuickLinksRepository.Update(entity);
                        if (SCRFQuickLinksRepository.SubmitChanges() > 0)
                            result = EnumResultType.Success;
                        else
                            result = EnumResultType.Failed;
                    }
                }
            }*/

            return result;
        }
        public EnumResultType DeleteSCRFQuickLinks(XsiScrfquickLinks entity)
        {
            if (GetSCRFQuickLinks(entity).Count() > 0)
            {
                using (SCRFQuickLinksRepository = new SCRFQuickLinksRepository(ConnectionString))
                {
                    SCRFQuickLinksRepository.Delete(entity);
                    if (SCRFQuickLinksRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFQuickLinks(long itemId)
        {
            if (GetSCRFQuickLinksByItemId(itemId) != null)
            {
                using (SCRFQuickLinksRepository = new SCRFQuickLinksRepository(ConnectionString))
                {
                    SCRFQuickLinksRepository.Delete(itemId);
                    if (SCRFQuickLinksRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
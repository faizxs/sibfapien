﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class GuestOfHonorBL : IDisposable
    {
        #region Feilds
        IGuestOfHonorRepository GuestOfHonorRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public GuestOfHonorBL()
        {
            ConnectionString = null;
        }
        public GuestOfHonorBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.GuestOfHonorRepository != null)
                this.GuestOfHonorRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiGuestOfHonor GetGuestOfHonorByItemId(long itemId)
        {
            using (GuestOfHonorRepository = new GuestOfHonorRepository(ConnectionString))
            {
                return GuestOfHonorRepository.GetById(itemId);
            }
        }

        public List<XsiGuestOfHonor> GetGuestOfHonor()
        {
            using (GuestOfHonorRepository = new GuestOfHonorRepository(ConnectionString))
            {
                return GuestOfHonorRepository.Select();
            }
        }
        public List<XsiGuestOfHonor> GetGuestOfHonor(EnumSortlistBy sortListBy)
        {
            using (GuestOfHonorRepository = new GuestOfHonorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return GuestOfHonorRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return GuestOfHonorRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return GuestOfHonorRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return GuestOfHonorRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return GuestOfHonorRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return GuestOfHonorRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return GuestOfHonorRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiGuestOfHonor> GetGuestOfHonor(XsiGuestOfHonor entity)
        {
            using (GuestOfHonorRepository = new GuestOfHonorRepository(ConnectionString))
            {
                return GuestOfHonorRepository.Select(entity);
            }
        }
        public List<XsiGuestOfHonor> GetGuestOfHonor(XsiGuestOfHonor entity, EnumSortlistBy sortListBy)
        {
            using (GuestOfHonorRepository = new GuestOfHonorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return GuestOfHonorRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return GuestOfHonorRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return GuestOfHonorRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return GuestOfHonorRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return GuestOfHonorRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return GuestOfHonorRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return GuestOfHonorRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiGuestOfHonor> GetGuestOfHonorOr(XsiGuestOfHonor entity, EnumSortlistBy sortListBy)
        {
            using (GuestOfHonorRepository = new GuestOfHonorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return GuestOfHonorRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return GuestOfHonorRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return GuestOfHonorRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return GuestOfHonorRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return GuestOfHonorRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return GuestOfHonorRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return GuestOfHonorRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertGuestOfHonor(XsiGuestOfHonor entity)
        {
            using (GuestOfHonorRepository = new GuestOfHonorRepository(ConnectionString))
            {
                entity = GuestOfHonorRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateGuestOfHonor(XsiGuestOfHonor entity)
        {
            XsiGuestOfHonor OriginalEntity = GetGuestOfHonorByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (GuestOfHonorRepository = new GuestOfHonorRepository(ConnectionString))
                {
                    GuestOfHonorRepository.Update(entity);
                    if (GuestOfHonorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateGuestOfHonorStatus(long itemId)
        {
            XsiGuestOfHonor entity = GetGuestOfHonorByItemId(itemId);
            if (entity != null)
            {
                using (GuestOfHonorRepository = new GuestOfHonorRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    GuestOfHonorRepository.Update(entity);
                    if (GuestOfHonorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteGuestOfHonor(XsiGuestOfHonor entity)
        {
            List<XsiGuestOfHonor> entities = GetGuestOfHonor(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiGuestOfHonor e in entities)
                {
                    result = DeleteGuestOfHonor(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteGuestOfHonor(long itemId)
        {
            XsiGuestOfHonor entity = GetGuestOfHonorByItemId(itemId);
            if (entity != null)
            {
                using (GuestOfHonorRepository = new GuestOfHonorRepository(ConnectionString))
                {
                    GuestOfHonorRepository.Delete(itemId);
                    if (GuestOfHonorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
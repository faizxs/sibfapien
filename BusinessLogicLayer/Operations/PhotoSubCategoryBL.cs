﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PhotoSubCategoryBL : IDisposable
    {
        #region Feilds
        IPhotoSubCategoryRepository PhotoSubCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PhotoSubCategoryBL()
        {
            ConnectionString = null;
        }
        public PhotoSubCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PhotoSubCategoryRepository != null)
                this.PhotoSubCategoryRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiPhotoSubCategory GetPhotoSubCategoryByItemId(long itemId)
        {
            using (PhotoSubCategoryRepository = new PhotoSubCategoryRepository(ConnectionString))
            {
                return PhotoSubCategoryRepository.GetById(itemId);
            }
        }

        public List<XsiPhotoSubCategory> GetPhotoSubCategory()
        {
            using (PhotoSubCategoryRepository = new PhotoSubCategoryRepository(ConnectionString))
            {
                return PhotoSubCategoryRepository.Select();
            }
        }
        public List<XsiPhotoSubCategory> GetPhotoSubCategory(EnumSortlistBy sortListBy)
        {
            using (PhotoSubCategoryRepository = new PhotoSubCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PhotoSubCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PhotoSubCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PhotoSubCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PhotoSubCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PhotoSubCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PhotoSubCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PhotoSubCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPhotoSubCategory> GetPhotoSubCategory(XsiPhotoSubCategory entity)
        {
            using (PhotoSubCategoryRepository = new PhotoSubCategoryRepository(ConnectionString))
            {
                return PhotoSubCategoryRepository.Select(entity);
            }
        }
        public List<XsiPhotoSubCategory> GetPhotoSubCategory(XsiPhotoSubCategory entity, EnumSortlistBy sortListBy)
        {
            using (PhotoSubCategoryRepository = new PhotoSubCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PhotoSubCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PhotoSubCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PhotoSubCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PhotoSubCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PhotoSubCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PhotoSubCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PhotoSubCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPhotoSubCategory> GetCompletePhotoSubCategory(XsiPhotoSubCategory entity, EnumSortlistBy sortListBy)
        {
            using (PhotoSubCategoryRepository = new PhotoSubCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PhotoSubCategoryRepository.SelectComeplete(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PhotoSubCategoryRepository.SelectComeplete(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PhotoSubCategoryRepository.SelectComeplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PhotoSubCategoryRepository.SelectComeplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PhotoSubCategoryRepository.SelectComeplete(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PhotoSubCategoryRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PhotoSubCategoryRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public XsiPhotoSubCategory GetPhotoSubCategory(long groupId, long categoryId)
        {
            using (PhotoSubCategoryRepository = new PhotoSubCategoryRepository(ConnectionString))
            {
                return PhotoSubCategoryRepository.Select(new XsiPhotoSubCategory() {ItemId=groupId, CategoryId = categoryId }).FirstOrDefault();
            }
        }

        public EnumResultType InsertPhotoSubCategory(XsiPhotoSubCategory entity)
        {
            using (PhotoSubCategoryRepository = new PhotoSubCategoryRepository(ConnectionString))
            {
                entity = PhotoSubCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePhotoSubCategory(XsiPhotoSubCategory entity)
        {
            XsiPhotoSubCategory OriginalEntity = GetPhotoSubCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PhotoSubCategoryRepository = new PhotoSubCategoryRepository(ConnectionString))
                {
                    PhotoSubCategoryRepository.Update(entity);
                    if (PhotoSubCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdatePhotoSubCategoryStatus(long itemId, long langId)
        {
            XsiPhotoSubCategory entity = GetPhotoSubCategoryByItemId(itemId);
            if (entity != null)
            {
                using (PhotoSubCategoryRepository = new PhotoSubCategoryRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    PhotoSubCategoryRepository.Update(entity);
                    if (PhotoSubCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePhotoSubCategory(XsiPhotoSubCategory entity)
        {
            List<XsiPhotoSubCategory> entities = GetPhotoSubCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiPhotoSubCategory e in entities)
                {
                    result = DeletePhotoSubCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeletePhotoSubCategory(long itemId)
        {
            XsiPhotoSubCategory entity = GetPhotoSubCategoryByItemId(itemId);
            if (entity != null)
            {
                using (PhotoSubCategoryRepository = new PhotoSubCategoryRepository(ConnectionString))
                {
                    PhotoSubCategoryRepository.Delete(itemId);
                    if (PhotoSubCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
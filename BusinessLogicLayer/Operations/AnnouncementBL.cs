﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class AnnouncementBL : IDisposable
    {
        #region Feilds
        IAnnouncementRepository AnnouncementRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public AnnouncementBL()
        {
            ConnectionString = null;
        }
        public AnnouncementBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AnnouncementRepository != null)
                this.AnnouncementRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiAnnouncement GetAnnouncementByItemId(long itemId)
        {
            using (AnnouncementRepository = new AnnouncementRepository(ConnectionString))
            {
                return AnnouncementRepository.GetById(itemId);
            }
        }
        public List<XsiAnnouncement> GetAnnouncement()
        {
            using (AnnouncementRepository = new AnnouncementRepository(ConnectionString))
            {
                return AnnouncementRepository.Select();
            }
        }
        public List<XsiAnnouncement> GetAnnouncement(EnumSortlistBy sortListBy)
        {
            using (AnnouncementRepository = new AnnouncementRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return AnnouncementRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return AnnouncementRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return AnnouncementRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return AnnouncementRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return AnnouncementRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AnnouncementRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return AnnouncementRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiAnnouncement> GetAnnouncement(XsiAnnouncement entity)
        {
            using (AnnouncementRepository = new AnnouncementRepository(ConnectionString))
            {
                return AnnouncementRepository.Select(entity);
            }
        }
        public List<XsiAnnouncement> GetAnnouncement(XsiAnnouncement entity, EnumSortlistBy sortListBy)
        {
            using (AnnouncementRepository = new AnnouncementRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return AnnouncementRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return AnnouncementRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return AnnouncementRepository.Select(entity).OrderBy(p => p.Dated).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return AnnouncementRepository.Select(entity).OrderByDescending(p => p.Dated).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return AnnouncementRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AnnouncementRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return AnnouncementRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiAnnouncement> GetAnnouncementOr(XsiAnnouncement entity, EnumSortlistBy sortListBy)
        {
            using (AnnouncementRepository = new AnnouncementRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return AnnouncementRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return AnnouncementRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return AnnouncementRepository.SelectOr(entity).OrderBy(p => p.Dated).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return AnnouncementRepository.SelectOr(entity).OrderByDescending(p => p.Dated).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return AnnouncementRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AnnouncementRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return AnnouncementRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertAnnouncement(XsiAnnouncement entity)
        {
            using (AnnouncementRepository = new AnnouncementRepository(ConnectionString))
            {
                entity = AnnouncementRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateAnnouncement(XsiAnnouncement entity)
        {
            XsiAnnouncement OriginalEntity = GetAnnouncementByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (AnnouncementRepository = new AnnouncementRepository(ConnectionString))
                {
                    AnnouncementRepository.Update(entity);
                    if (AnnouncementRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateAnnouncementStatus(long itemId)
        {
            XsiAnnouncement entity = GetAnnouncementByItemId(itemId);
            if (entity != null)
            {
                using (AnnouncementRepository = new AnnouncementRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    AnnouncementRepository.Update(entity);
                    if (AnnouncementRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteAnnouncement(XsiAnnouncement entity)
        {
            List<XsiAnnouncement> entities = GetAnnouncement(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiAnnouncement e in entities)
                {
                    result = DeleteAnnouncement(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteAnnouncement(long itemId)
        {
            XsiAnnouncement entity = GetAnnouncementByItemId(itemId);
            if (entity != null)
            {
                using (AnnouncementRepository = new AnnouncementRepository(ConnectionString))
                {
                    AnnouncementRepository.Delete(itemId);
                    if (AnnouncementRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
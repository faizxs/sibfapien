﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitorStatusLogBL : IDisposable
    {
        #region Feilds
        IExhibitorStatusLogRepository IExhibitorStatusLogRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitorStatusLogBL()
        {
            ConnectionString = null;
        }
        public ExhibitorStatusLogBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitorStatusLogRepository != null)
                this.IExhibitorStatusLogRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitorStatusLogRepository = new ExhibitorStatusLogRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionMemberApplicationYearlyLogs GetExhibitorStatusLogByItemId(long itemId)
        {
            using (IExhibitorStatusLogRepository = new ExhibitorStatusLogRepository(ConnectionString))
            {
                return IExhibitorStatusLogRepository.GetById(itemId);
            }
        }
        
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetExhibitorStatusLog()
        {
            using (IExhibitorStatusLogRepository = new ExhibitorStatusLogRepository(ConnectionString))
            {
                return IExhibitorStatusLogRepository.Select();
            }
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetExhibitorStatusLog(EnumSortlistBy sortListBy)
        {
            using (IExhibitorStatusLogRepository = new ExhibitorStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitorStatusLogRepository.Select().OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitorStatusLogRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitorStatusLogRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitorStatusLogRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitorStatusLogRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitorStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitorStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetExhibitorStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            using (IExhibitorStatusLogRepository = new ExhibitorStatusLogRepository(ConnectionString))
            {
                return IExhibitorStatusLogRepository.Select(entity);
            }
        }
        public List<XsiExhibitionMemberApplicationYearlyLogs> GetExhibitorStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitorStatusLogRepository = new ExhibitorStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitorStatusLogRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitorStatusLogRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitorStatusLogRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitorStatusLogRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitorStatusLogRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitorStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitorStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionMemberApplicationYearlyLogs GetExhibitorStatusLog(long groupId, long languageId)
        {
            using (IExhibitorStatusLogRepository = new ExhibitorStatusLogRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertExhibitorStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            using (IExhibitorStatusLogRepository = new ExhibitorStatusLogRepository(ConnectionString))
            {
                entity = IExhibitorStatusLogRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId =  -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitorStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            XsiExhibitionMemberApplicationYearlyLogs OriginalEntity = GetExhibitorStatusLogByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IExhibitorStatusLogRepository = new ExhibitorStatusLogRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line
                    
                    IExhibitorStatusLogRepository.Update(entity);
                    if (IExhibitorStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitorStatusLogStatus(long itemId)
        {
            XsiExhibitionMemberApplicationYearlyLogs entity = GetExhibitorStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitorStatusLogRepository = new ExhibitorStatusLogRepository(ConnectionString))
                {
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitorStatusLogRepository.Update(entity);
                    if (IExhibitorStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitorStatusLog(XsiExhibitionMemberApplicationYearlyLogs entity)
        {
            List<XsiExhibitionMemberApplicationYearlyLogs> entities = GetExhibitorStatusLog(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionMemberApplicationYearlyLogs e in entities)
                {
                    result = DeleteExhibitorStatusLog(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitorStatusLog(long itemId)
        {
            XsiExhibitionMemberApplicationYearlyLogs entity = GetExhibitorStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitorStatusLogRepository = new ExhibitorStatusLogRepository(ConnectionString))
                {
                    IExhibitorStatusLogRepository.Delete(itemId);
                    if (IExhibitorStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
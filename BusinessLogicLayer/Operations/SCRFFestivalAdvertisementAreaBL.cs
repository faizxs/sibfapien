﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFFestivalAdvertisementAreaBL : IDisposable
    {
        #region Feilds
        ISCRFFestivalAdvertisementAreaRepository SCRFFestivalAdvertisementAreaRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFFestivalAdvertisementAreaBL()
        {
            ConnectionString = null;
        }
        public SCRFFestivalAdvertisementAreaBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFFestivalAdvertisementAreaRepository != null)
                this.SCRFFestivalAdvertisementAreaRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrffestivalAdvertisementArea GetSCRFFestivalAdvertisementAreaByItemId(long itemId)
        {
            using (SCRFFestivalAdvertisementAreaRepository = new SCRFFestivalAdvertisementAreaRepository(ConnectionString))
            {
                return SCRFFestivalAdvertisementAreaRepository.GetById(itemId);
            }
        }

        public List<XsiScrffestivalAdvertisementArea> GetSCRFFestivalAdvertisementArea()
        {
            using (SCRFFestivalAdvertisementAreaRepository = new SCRFFestivalAdvertisementAreaRepository(ConnectionString))
            {
                return SCRFFestivalAdvertisementAreaRepository.Select();
            }
        }
        public List<XsiScrffestivalAdvertisementArea> GetSCRFFestivalAdvertisementArea(EnumSortlistBy sortListBy)
        {
            using (SCRFFestivalAdvertisementAreaRepository = new SCRFFestivalAdvertisementAreaRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFFestivalAdvertisementAreaRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFFestivalAdvertisementAreaRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFFestivalAdvertisementAreaRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFFestivalAdvertisementAreaRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFFestivalAdvertisementAreaRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFFestivalAdvertisementAreaRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFFestivalAdvertisementAreaRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrffestivalAdvertisementArea> GetSCRFFestivalAdvertisementArea(XsiScrffestivalAdvertisementArea entity)
        {
            using (SCRFFestivalAdvertisementAreaRepository = new SCRFFestivalAdvertisementAreaRepository(ConnectionString))
            {
                return SCRFFestivalAdvertisementAreaRepository.Select(entity);
            }
        }
        public List<XsiScrffestivalAdvertisementArea> GetSCRFFestivalAdvertisementArea(XsiScrffestivalAdvertisementArea entity, EnumSortlistBy sortListBy)
        {
            using (SCRFFestivalAdvertisementAreaRepository = new SCRFFestivalAdvertisementAreaRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFFestivalAdvertisementAreaRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFFestivalAdvertisementAreaRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFFestivalAdvertisementAreaRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFFestivalAdvertisementAreaRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFFestivalAdvertisementAreaRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFFestivalAdvertisementAreaRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFFestivalAdvertisementAreaRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrffestivalAdvertisementArea> GetSCRFFestivalAdvertisementAreaOr(XsiScrffestivalAdvertisementArea entity, EnumSortlistBy sortListBy)
        {
            using (SCRFFestivalAdvertisementAreaRepository = new SCRFFestivalAdvertisementAreaRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFFestivalAdvertisementAreaRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFFestivalAdvertisementAreaRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFFestivalAdvertisementAreaRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFFestivalAdvertisementAreaRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFFestivalAdvertisementAreaRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFFestivalAdvertisementAreaRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFFestivalAdvertisementAreaRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFFestivalAdvertisementArea(XsiScrffestivalAdvertisementArea entity)
        {
            using (SCRFFestivalAdvertisementAreaRepository = new SCRFFestivalAdvertisementAreaRepository(ConnectionString))
            {
                entity = SCRFFestivalAdvertisementAreaRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFFestivalAdvertisementArea(XsiScrffestivalAdvertisementArea entity)
        {
            XsiScrffestivalAdvertisementArea OriginalEntity = GetSCRFFestivalAdvertisementAreaByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFFestivalAdvertisementAreaRepository = new SCRFFestivalAdvertisementAreaRepository(ConnectionString))
                {

                    SCRFFestivalAdvertisementAreaRepository.Update(entity);
                    if (SCRFFestivalAdvertisementAreaRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFFestivalAdvertisementAreaStatus(long itemId)
        {
            XsiScrffestivalAdvertisementArea entity = GetSCRFFestivalAdvertisementAreaByItemId(itemId);
            if (entity != null)
            {
                using (SCRFFestivalAdvertisementAreaRepository = new SCRFFestivalAdvertisementAreaRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFFestivalAdvertisementAreaRepository.Update(entity);
                    if (SCRFFestivalAdvertisementAreaRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFFestivalAdvertisementArea(XsiScrffestivalAdvertisementArea entity)
        {
            List<XsiScrffestivalAdvertisementArea> entities = GetSCRFFestivalAdvertisementArea(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrffestivalAdvertisementArea e in entities)
                {
                    result = DeleteSCRFFestivalAdvertisementArea(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFFestivalAdvertisementArea(long itemId)
        {
            XsiScrffestivalAdvertisementArea entity = GetSCRFFestivalAdvertisementAreaByItemId(itemId);
            if (entity != null)
            {
                using (SCRFFestivalAdvertisementAreaRepository = new SCRFFestivalAdvertisementAreaRepository(ConnectionString))
                {
                    SCRFFestivalAdvertisementAreaRepository.Delete(itemId);
                    if (SCRFFestivalAdvertisementAreaRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        //Custom Methods
        public long GetHomeBannerInfo(long languageid)
        {
            using (SCRFFestivalAdvertisementAreaRepository = new SCRFFestivalAdvertisementAreaRepository(ConnectionString))
            {
                return SCRFFestivalAdvertisementAreaRepository.Select(new XsiScrffestivalAdvertisementArea() { IsActive = EnumConversion.ToString(EnumBool.Yes) }).Count();
            }
        }
        #endregion
    }
}
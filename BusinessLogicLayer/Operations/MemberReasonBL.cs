﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class MemberReasonBL : IDisposable
    {
        #region Feilds
        IMemberReasonRepository MemberReasonRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public MemberReasonBL()
        {
            ConnectionString = null;
        }
        public MemberReasonBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.MemberReasonRepository != null)
                this.MemberReasonRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiMemberReason GetMemberReasonByItemId(long itemId)
        {
            using (MemberReasonRepository = new MemberReasonRepository(ConnectionString))
            {
                return MemberReasonRepository.GetById(itemId);
            }
        }

        public List<XsiMemberReason> GetMemberReason()
        {
            using (MemberReasonRepository = new MemberReasonRepository(ConnectionString))
            {
                return MemberReasonRepository.Select();
            }
        }
        public List<XsiMemberReason> GetMemberReason(EnumSortlistBy sortListBy)
        {
            using (MemberReasonRepository = new MemberReasonRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return MemberReasonRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return MemberReasonRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return MemberReasonRepository.Select().OrderBy(p => p.ReasonId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return MemberReasonRepository.Select().OrderByDescending(p => p.ReasonId).ToList();

                    default:
                        return MemberReasonRepository.Select().OrderByDescending(p => p.ReasonId).ToList();
                }
            }
        }
        public List<XsiMemberReason> GetMemberReason(XsiMemberReason entity)
        {
            using (MemberReasonRepository = new MemberReasonRepository(ConnectionString))
            {
                return MemberReasonRepository.Select(entity);
            }
        }
        public List<XsiMemberReason> GetMemberReason(XsiMemberReason entity, EnumSortlistBy sortListBy)
        {
            using (MemberReasonRepository = new MemberReasonRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return MemberReasonRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return MemberReasonRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return MemberReasonRepository.Select(entity).OrderBy(p => p.ReasonId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return MemberReasonRepository.Select(entity).OrderByDescending(p => p.ReasonId).ToList();

                    default:
                        return MemberReasonRepository.Select(entity).OrderByDescending(p => p.ReasonId).ToList();
                }
            }
        }
        public EnumResultType InsertMemberReason(XsiMemberReason entity)
        {
            using (MemberReasonRepository = new MemberReasonRepository(ConnectionString))
            {
                entity = MemberReasonRepository.Add(entity);
                if (entity.ReasonId > 0)
                {
                    XsiItemdId = entity.MemberReasonId;
                    //XsiGroupId = entity.GroupId ?? -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateMemberReason(XsiMemberReason entity)
        {
            XsiMemberReason OriginalEntity = GetMemberReasonByItemId(entity.MemberReasonId);

            if (OriginalEntity != null)
            {
                using (MemberReasonRepository = new MemberReasonRepository(ConnectionString))
                {

                    MemberReasonRepository.Update(entity);
                    if (MemberReasonRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteMemberReason(XsiMemberReason entity)
        {
            List<XsiMemberReason> entities = GetMemberReason(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiMemberReason e in entities)
                {
                    result = DeleteMemberReason(e.MemberReasonId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteMemberReason(long itemId)
        {
            XsiMemberReason entity = GetMemberReasonByItemId(itemId);
            if (entity != null)
            {
                using (MemberReasonRepository = new MemberReasonRepository(ConnectionString))
                {
                    MemberReasonRepository.Delete(itemId);
                    if (MemberReasonRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class NewsPhotoGalleryBL : IDisposable
    {
        #region Feilds
        INewsPhotoGalleryRepository NewsPhotoGalleryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public NewsPhotoGalleryBL()
        {
            ConnectionString = null;
        }
        public NewsPhotoGalleryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.NewsPhotoGalleryRepository != null)
                this.NewsPhotoGalleryRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (NewsPhotoGalleryRepository = new NewsPhotoGalleryRepository(ConnectionString))
            {
                return -1;
            }
        }
        public long GetNextSortIndex()
        {
            //optional method. return -1 if not required and remove following
            using (NewsPhotoGalleryRepository = new NewsPhotoGalleryRepository(ConnectionString))
            {
                return (NewsPhotoGalleryRepository.Select().Max(p => p.SortIndex) ?? 0) + 1;
            }
        }

        public XsiNewsPhotoGallery GetNewsPhotoGalleryByItemId(long itemId)
        {
            using (NewsPhotoGalleryRepository = new NewsPhotoGalleryRepository(ConnectionString))
            {
                return NewsPhotoGalleryRepository.GetById(itemId);
            }
        }
       
        public List<XsiNewsPhotoGallery> GetNewsPhotoGallery()
        {
            using (NewsPhotoGalleryRepository = new NewsPhotoGalleryRepository(ConnectionString))
            {
                return NewsPhotoGalleryRepository.Select();
            }
        }
        public List<XsiNewsPhotoGallery> GetNewsPhotoGallery(EnumSortlistBy sortListBy)
        {
            using (NewsPhotoGalleryRepository = new NewsPhotoGalleryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsPhotoGalleryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsPhotoGalleryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return NewsPhotoGalleryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return NewsPhotoGalleryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsPhotoGalleryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsPhotoGalleryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsPhotoGalleryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiNewsPhotoGallery> GetNewsPhotoGallery(XsiNewsPhotoGallery entity)
        {
            using (NewsPhotoGalleryRepository = new NewsPhotoGalleryRepository(ConnectionString))
            {
                return NewsPhotoGalleryRepository.Select(entity);
            }
        }
        public List<XsiNewsPhotoGallery> GetNewsPhotoGallery(XsiNewsPhotoGallery entity, EnumSortlistBy sortListBy)
        {
            using (NewsPhotoGalleryRepository = new NewsPhotoGalleryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsPhotoGalleryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsPhotoGalleryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return NewsPhotoGalleryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return NewsPhotoGalleryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsPhotoGalleryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsPhotoGalleryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.BySortOrderAsc:
                        return NewsPhotoGalleryRepository.Select(entity).OrderBy(p => p.SortIndex).ToList();

                    case EnumSortlistBy.BySortOrderDesc:
                        return NewsPhotoGalleryRepository.Select(entity).OrderByDescending(p => p.SortIndex).ToList();

                    default:
                        return NewsPhotoGalleryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiNewsPhotoGallery GetNewsPhotoGallery(long groupId, long languageId)
        {
            using (NewsPhotoGalleryRepository = new NewsPhotoGalleryRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertNewsPhotoGallery(XsiNewsPhotoGallery entity)
        {
            using (NewsPhotoGalleryRepository = new NewsPhotoGalleryRepository(ConnectionString))
            {
                entity = NewsPhotoGalleryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateNewsPhotoGallery(XsiNewsPhotoGallery entity)
        {
            XsiNewsPhotoGallery OriginalEntity = GetNewsPhotoGalleryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (NewsPhotoGalleryRepository = new NewsPhotoGalleryRepository(ConnectionString))
                {
                    NewsPhotoGalleryRepository.Update(entity);
                    if (NewsPhotoGalleryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateNewsPhotoGalleryStatus(long itemId)
        {
            XsiNewsPhotoGallery entity = GetNewsPhotoGalleryByItemId(itemId);
            if (entity != null)
            {
                using (NewsPhotoGalleryRepository = new NewsPhotoGalleryRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    NewsPhotoGalleryRepository.Update(entity);
                    if (NewsPhotoGalleryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteNewsPhotoGallery(XsiNewsPhotoGallery entity)
        {
            List<XsiNewsPhotoGallery> entities = GetNewsPhotoGallery(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiNewsPhotoGallery e in entities)
                {
                    result = DeleteNewsPhotoGallery(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteNewsPhotoGallery(long itemId)
        {
            XsiNewsPhotoGallery entity = GetNewsPhotoGalleryByItemId(itemId);
            if (entity != null)
            {
                using (NewsPhotoGalleryRepository = new NewsPhotoGalleryRepository(ConnectionString))
                {
                    NewsPhotoGalleryRepository.Delete(itemId);
                    if (NewsPhotoGalleryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
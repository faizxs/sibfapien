﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class EventGuestBL : IDisposable
    {
        #region Feilds
        IEventGuestRepository EventGuestRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public EventGuestBL()
        {
            ConnectionString = null;
        }
        public EventGuestBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventGuestRepository != null)
                this.EventGuestRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (EventGuestRepository = new EventGuestRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiEventGuest GetEventGuestByItemId(long itemId)
        {
            using (EventGuestRepository = new EventGuestRepository(ConnectionString))
            {
                return EventGuestRepository.GetById(itemId);
            }
        }

        public List<XsiEventGuest> GetEventGuest()
        {
            using (EventGuestRepository = new EventGuestRepository(ConnectionString))
            {
                return EventGuestRepository.Select();
            }
        }
        public List<XsiEventGuest> GetEventGuest(EnumSortlistBy sortListBy)
        {
            using (EventGuestRepository = new EventGuestRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return EventGuestRepository.Select().OrderBy(p => p.GuestId).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventGuestRepository.Select().OrderByDescending(p => p.GuestId).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventGuestRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventGuestRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventGuestRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEventGuest> GetEventGuest(XsiEventGuest entity)
        {
            using (EventGuestRepository = new EventGuestRepository(ConnectionString))
            {
                return EventGuestRepository.Select(entity);
            }
        }
        public List<XsiEventGuest> GetEventGuest(XsiEventGuest entity, EnumSortlistBy sortListBy)
        {
            using (EventGuestRepository = new EventGuestRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return EventGuestRepository.Select(entity).OrderBy(p => p.GuestId).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventGuestRepository.Select(entity).OrderByDescending(p => p.GuestId).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventGuestRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventGuestRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventGuestRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiEventGuest GetEventGuest(long groupId, long languageId)
        {
            using (EventGuestRepository = new EventGuestRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertEventGuest(XsiEventGuest entity)
        {
            using (EventGuestRepository = new EventGuestRepository(ConnectionString))
            {
                entity = EventGuestRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateEventGuest(XsiEventGuest entity)
        {
            XsiEventGuest OriginalEntity = GetEventGuestByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (EventGuestRepository = new EventGuestRepository(ConnectionString))
                {
                    EventGuestRepository.Update(entity);
                    if (EventGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateEventGuestStatus(long itemId)
        {

            return EnumResultType.NotFound;
        }
        public EnumResultType DeleteEventGuest(XsiEventGuest entity)
        {
            List<XsiEventGuest> entities = GetEventGuest(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiEventGuest e in entities)
                {
                    result = DeleteEventGuest(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteEventGuest(long itemId)
        {
            XsiEventGuest entity = GetEventGuestByItemId(itemId);
            if (entity != null)
            {
                using (EventGuestRepository = new EventGuestRepository(ConnectionString))
                {
                    EventGuestRepository.Delete(itemId);
                    if (EventGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        //public GetEvents_Result GetEventbyGuest(long guestId)
        //{
        //    using (EventGuestRepository = new EventGuestRepository(ConnectionString))
        //    {
        //        return EventGuestRepository.GetEventbyGuest(guestId);
        //    }
        //}
        //public List<GetEvents_Result> GetEventbyGuestList(long guestId)
        //{
        //    using (EventGuestRepository = new EventGuestRepository(ConnectionString))
        //    {
        //        return EventGuestRepository.GetEventbyGuestList(guestId);
        //    }
        //}

        //public List<GetEvents_Result> GetActivityList(string type, long guestGid)
        //{
        //    using (EventGuestRepository = new EventGuestRepository(ConnectionString))
        //    {
        //        return EventGuestRepository.GetActivityList(type, guestGid);
        //    }
        //}
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class QuickLinksBL : IDisposable
    {
        #region Feilds
        IQuickLinksRepository QuickLinksRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public QuickLinksBL()
        {
            ConnectionString = null;
        }
        public QuickLinksBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.QuickLinksRepository != null)
                this.QuickLinksRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiQuickLinks GetQuickLinksByItemId(long itemId)
        {
            using (QuickLinksRepository = new QuickLinksRepository(ConnectionString))
            {
                return QuickLinksRepository.GetById(itemId);
            }
        }

        public List<XsiQuickLinks> GetQuickLinks()
        {
            using (QuickLinksRepository = new QuickLinksRepository(ConnectionString))
            {
                return QuickLinksRepository.Select();
            }
        }
        public List<XsiQuickLinks> GetQuickLinks(EnumSortlistBy sortListBy)
        {
            using (QuickLinksRepository = new QuickLinksRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return QuickLinksRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return QuickLinksRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return QuickLinksRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return QuickLinksRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return QuickLinksRepository.Select().OrderBy(p => p.SortOrder).ToList();

                    default:
                        return QuickLinksRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiQuickLinks> GetQuickLinks(XsiQuickLinks entity)
        {
            using (QuickLinksRepository = new QuickLinksRepository(ConnectionString))
            {
                return QuickLinksRepository.Select(entity);
            }
        }
        public List<XsiQuickLinks> GetQuickLinks(XsiQuickLinks entity, EnumSortlistBy sortListBy)
        {
            using (QuickLinksRepository = new QuickLinksRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return QuickLinksRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return QuickLinksRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return QuickLinksRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return QuickLinksRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return QuickLinksRepository.Select(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return QuickLinksRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiQuickLinks> GetQuickLinksOr(XsiQuickLinks entity, EnumSortlistBy sortListBy)
        {
            using (QuickLinksRepository = new QuickLinksRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return QuickLinksRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return QuickLinksRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return QuickLinksRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return QuickLinksRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return QuickLinksRepository.SelectOr(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return QuickLinksRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertQuickLinks(XsiQuickLinks entity)
        {
            using (QuickLinksRepository = new QuickLinksRepository(ConnectionString))
            {
                entity = QuickLinksRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateQuickLinks(XsiQuickLinks entity)
        {
            XsiQuickLinks OriginalEntity = GetQuickLinksByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (QuickLinksRepository = new QuickLinksRepository(ConnectionString))
                {
                    QuickLinksRepository.Update(entity);
                    if (QuickLinksRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateQuickLinksStatus(long itemId)
        {
            XsiQuickLinks entity = GetQuickLinksByItemId(itemId);
            if (entity != null)
            {
                using (QuickLinksRepository = new QuickLinksRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    QuickLinksRepository.Update(entity);
                    if (QuickLinksRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateQuickLinksSortOrder(long groupId, long order)
        {
            EnumResultType result = EnumResultType.NotFound;

            /*List<XsiQuickLinks> entities = GetQuickLinksByGroupId(groupId).ToList();
            if (entities != null)
            {
                using (QuickLinksRepository = new QuickLinksRepository(ConnectionString))
                {
                    foreach (XsiQuickLinks entity in entities)
                    {
                        entity.SortOrder = order;
                        QuickLinksRepository.Update(entity);
                        if (QuickLinksRepository.SubmitChanges() > 0)
                            result = EnumResultType.Success;
                        else
                            result = EnumResultType.Failed;
                    }
                }
            }
            */
            return result;
        }
        public EnumResultType DeleteQuickLinks(XsiQuickLinks entity)
        {
            if (GetQuickLinks(entity).Count() > 0)
            {
                using (QuickLinksRepository = new QuickLinksRepository(ConnectionString))
                {
                    QuickLinksRepository.Delete(entity);
                    if (QuickLinksRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteQuickLinks(long itemId)
        {
            if (GetQuickLinksByItemId(itemId) != null)
            {
                using (QuickLinksRepository = new QuickLinksRepository(ConnectionString))
                {
                    QuickLinksRepository.Delete(itemId);
                    if (QuickLinksRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using Xsi.Repositories;
using Entities.Models;
using System.Linq;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionFurnitureItemOrdersBL : IDisposable
    {
        #region Fields
        IExhibitionFurnitureItemOrdersRepository ExhibitionFurnitureItemOrdersRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionFurnitureItemOrdersBL()
        {
            ConnectionString = null;
        }
        public ExhibitionFurnitureItemOrdersBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionFurnitureItemOrdersRepository != null)
                this.ExhibitionFurnitureItemOrdersRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionFurnitureItemOrders GetExhibitionFurnitureItemOrdersByItemId(long itemId)
        {
            using (ExhibitionFurnitureItemOrdersRepository = new ExhibitionFurnitureItemOrderRepository(ConnectionString))
            {
                return ExhibitionFurnitureItemOrdersRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionFurnitureItemOrders> GetExhibitionFurnitureItemOrders()
        {
            using (ExhibitionFurnitureItemOrdersRepository = new ExhibitionFurnitureItemOrderRepository(ConnectionString))
            {
                return ExhibitionFurnitureItemOrdersRepository.Select();
            }
        }
        public List<XsiExhibitionFurnitureItemOrders> GetExhibitionFurnitureItemOrders(EnumSortlistBy sortListBy)
        {
            using (ExhibitionFurnitureItemOrdersRepository = new ExhibitionFurnitureItemOrderRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionFurnitureItemOrdersRepository.Select().OrderBy(p => p.InvoiceNumber).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionFurnitureItemOrdersRepository.Select().OrderByDescending(p => p.InvoiceNumber).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionFurnitureItemOrdersRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionFurnitureItemOrdersRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionFurnitureItemOrdersRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionFurnitureItemOrdersRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionFurnitureItemOrdersRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionFurnitureItemOrders> GetExhibitionFurnitureItemOrders(XsiExhibitionFurnitureItemOrders entity)
        {
            using (ExhibitionFurnitureItemOrdersRepository = new ExhibitionFurnitureItemOrderRepository(ConnectionString))
            {
                return ExhibitionFurnitureItemOrdersRepository.Select(entity);
            }
        }
        public List<XsiExhibitionFurnitureItemOrders> GetExhibitionFurnitureItemOrders(XsiExhibitionFurnitureItemOrders entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionFurnitureItemOrdersRepository = new ExhibitionFurnitureItemOrderRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionFurnitureItemOrdersRepository.Select(entity).OrderBy(p => p.InvoiceNumber).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionFurnitureItemOrdersRepository.Select(entity).OrderByDescending(p => p.InvoiceNumber).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionFurnitureItemOrdersRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionFurnitureItemOrdersRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionFurnitureItemOrdersRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionFurnitureItemOrdersRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionFurnitureItemOrdersRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertExhibitionFurnitureItemOrders(XsiExhibitionFurnitureItemOrders entity)
        {
            using (ExhibitionFurnitureItemOrdersRepository = new ExhibitionFurnitureItemOrderRepository(ConnectionString))
            {
                entity = ExhibitionFurnitureItemOrdersRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionFurnitureItemOrders(XsiExhibitionFurnitureItemOrders entity)
        {
            XsiExhibitionFurnitureItemOrders OriginalEntity = GetExhibitionFurnitureItemOrdersByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionFurnitureItemOrdersRepository = new ExhibitionFurnitureItemOrderRepository(ConnectionString))
                {
                    ExhibitionFurnitureItemOrdersRepository.Update(entity);
                    if (ExhibitionFurnitureItemOrdersRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateExhibitionFurnitureItemOrdersStatus(long itemId, long langId)
        {
            XsiExhibitionFurnitureItemOrders entity = GetExhibitionFurnitureItemOrdersByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionFurnitureItemOrdersRepository = new ExhibitionFurnitureItemOrderRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionFurnitureItemOrdersRepository.Update(entity);
                    if (ExhibitionFurnitureItemOrdersRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionFurnitureItemOrders(XsiExhibitionFurnitureItemOrders entity)
        {
            List<XsiExhibitionFurnitureItemOrders> entities = GetExhibitionFurnitureItemOrders(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionFurnitureItemOrders e in entities)
                {
                    result = DeleteExhibitionFurnitureItemOrders(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionFurnitureItemOrders(long itemId)
        {
            XsiExhibitionFurnitureItemOrders entity = GetExhibitionFurnitureItemOrdersByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionFurnitureItemOrdersRepository = new ExhibitionFurnitureItemOrderRepository(ConnectionString))
                {
                    ExhibitionFurnitureItemOrdersRepository.Delete(itemId);
                    if (ExhibitionFurnitureItemOrdersRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFPageMenuBL : IDisposable
    {
        #region Feilds
        ISCRFPageMenuRepository SCRFPageMenuRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFPageMenuBL()
        {
            ConnectionString = null;
        }
        public SCRFPageMenuBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFPageMenuRepository != null)
                this.SCRFPageMenuRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiScrfpageMenu GetSCRFPageMenuByItemId(long itemId)
        {
            using (SCRFPageMenuRepository = new SCRFPageMenuRepository(ConnectionString))
            {
                return SCRFPageMenuRepository.GetById(itemId);
            }
        }

        public List<XsiScrfpageMenu> GetSCRFPageMenu()
        {
            using (SCRFPageMenuRepository = new SCRFPageMenuRepository(ConnectionString))
            {
                return SCRFPageMenuRepository.Select();
            }
        }
        public List<XsiScrfpageMenu> GetSCRFPageMenu(EnumSortlistBy sortListBy)
        {
            using (SCRFPageMenuRepository = new SCRFPageMenuRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPageMenuRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPageMenuRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPageMenuRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPageMenuRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return SCRFPageMenuRepository.Select().OrderBy(p => p.SortOrder).ToList();

                    default:
                        return SCRFPageMenuRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfpageMenu> GetSCRFPageMenu(XsiScrfpageMenu entity)
        {
            using (SCRFPageMenuRepository = new SCRFPageMenuRepository(ConnectionString))
            {
                return SCRFPageMenuRepository.Select(entity);
            }
        }
        public List<XsiScrfpageMenu> GetSCRFPageMenu(XsiScrfpageMenu entity, EnumSortlistBy sortListBy)
        {
            using (SCRFPageMenuRepository = new SCRFPageMenuRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPageMenuRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPageMenuRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPageMenuRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPageMenuRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return SCRFPageMenuRepository.Select(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return SCRFPageMenuRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiScrfpageMenu> GetSCRFPageMenuOr(XsiScrfpageMenu entity, EnumSortlistBy sortListBy)
        {
            using (SCRFPageMenuRepository = new SCRFPageMenuRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFPageMenuRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFPageMenuRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFPageMenuRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFPageMenuRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return SCRFPageMenuRepository.SelectOr(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return SCRFPageMenuRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFPageMenu(XsiScrfpageMenu entity)
        {
            using (SCRFPageMenuRepository = new SCRFPageMenuRepository(ConnectionString))
            {
                entity = SCRFPageMenuRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFPageMenu(XsiScrfpageMenu entity)
        {
            XsiScrfpageMenu OriginalEntity = GetSCRFPageMenuByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFPageMenuRepository = new SCRFPageMenuRepository(ConnectionString))
                {
                    SCRFPageMenuRepository.Update(entity);
                    if (SCRFPageMenuRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFPageMenuStatus(long itemId)
        {
            XsiScrfpageMenu entity = GetSCRFPageMenuByItemId(itemId);
            if (entity != null)
            {
                using (SCRFPageMenuRepository = new SCRFPageMenuRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFPageMenuRepository.Update(entity);
                    if (SCRFPageMenuRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFPageMenuSortOrder(long groupId, long order)
        {
            EnumResultType result = EnumResultType.NotFound;

           /* List<XsiScrfpageMenu> entities = GetSCRFPageMenuByGroupId(groupId).ToList();
            if (entities != null)
            {
                using (SCRFPageMenuRepository = new SCRFPageMenuRepository(ConnectionString))
                {
                    foreach (XsiScrfpageMenu entity in entities)
                    {
                        entity.SortOrder = order;
                        SCRFPageMenuRepository.Update(entity);
                        if (SCRFPageMenuRepository.SubmitChanges() > 0)
                            result = EnumResultType.Success;
                        else
                            result = EnumResultType.Failed;
                    }
                }
            }*/

            return result;
        }
        public EnumResultType DeleteSCRFPageMenu(XsiScrfpageMenu entity)
        {
            if (GetSCRFPageMenu(entity).Count() > 0)
            {
                using (SCRFPageMenuRepository = new SCRFPageMenuRepository(ConnectionString))
                {
                    SCRFPageMenuRepository.Delete(entity);
                    if (SCRFPageMenuRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFPageMenu(long itemId)
        {
            if (GetSCRFPageMenuByItemId(itemId) != null)
            {
                using (SCRFPageMenuRepository = new SCRFPageMenuRepository(ConnectionString))
                {
                    SCRFPageMenuRepository.Delete(itemId);
                    if (SCRFPageMenuRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
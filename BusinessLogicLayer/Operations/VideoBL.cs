﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class VideoBL : IDisposable
    {
        #region Feilds
        IVideoRepository VideoRepository;

        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public VideoBL()
        {
            ConnectionString = null;
        }
        public VideoBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.VideoRepository != null)
                this.VideoRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextSortIndex()
        {
            //optional method. return -1 if not required and remove following
            using (VideoRepository = new VideoRepository(ConnectionString))
            {
                return (VideoRepository.Select().Max(p => p.SortIndex) ?? 0) + 1;
            }
        }

        public XsiVideo GetVideoByItemId(long itemId)
        {
            using (VideoRepository = new VideoRepository(ConnectionString))
            {
                return VideoRepository.GetById(itemId);
            }
        }

        public List<XsiVideo> GetVideo()
        {
            using (VideoRepository = new VideoRepository(ConnectionString))
            {
                return VideoRepository.Select();
            }
        }
        public List<XsiVideo> GetVideo(EnumSortlistBy sortListBy)
        {
            using (VideoRepository = new VideoRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return VideoRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return VideoRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return VideoRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return VideoRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return VideoRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return VideoRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return VideoRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiVideo> GetVideo(XsiVideo entity)
        {
            using (VideoRepository = new VideoRepository(ConnectionString))
            {
                return VideoRepository.Select(entity);
            }
        }
        public List<XsiVideo> GetVideo(XsiVideo entity, EnumSortlistBy sortListBy)
        {
            using (VideoRepository = new VideoRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return VideoRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return VideoRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return VideoRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return VideoRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return VideoRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return VideoRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.BySortOrderAsc:
                        return VideoRepository.Select(entity).OrderBy(p => p.SortIndex).ToList();

                    case EnumSortlistBy.BySortOrderDesc:
                        return VideoRepository.Select(entity).OrderByDescending(p => p.SortIndex).ToList();

                    default:
                        return VideoRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertVideo(XsiVideo entity)
        {
            using (VideoRepository = new VideoRepository(ConnectionString))
            {
                entity = VideoRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateVideo(XsiVideo entity)
        {
            XsiVideo OriginalEntity = GetVideoByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (VideoRepository = new VideoRepository(ConnectionString))
                {
                    VideoRepository.Update(entity);
                    if (VideoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateVideoStatus(long itemId, long langId)
        {
            XsiVideo entity = GetVideoByItemId(itemId);
            if (entity != null)
            {
                using (VideoRepository = new VideoRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    VideoRepository.Update(entity);
                    if (VideoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateAlbumCover(long itemId)
        {
            using (VideoRepository = new VideoRepository(ConnectionString))
            {
                XsiVideo entity = VideoRepository.GetById(itemId);
                XsiVideo entity1 = new XsiVideo();
                entity1.VideoAlbumId = entity.VideoAlbumId;
                List<XsiVideo> VideoList = VideoRepository.Select(entity1);
                if (VideoList != null)
                {
                    foreach (XsiVideo entity2 in VideoList)
                    {
                        if (itemId != entity2.ItemId)
                        {
                            entity2.IsAlbumCover = EnumConversion.ToString(EnumBool.No);
                            VideoRepository.Update(entity2);
                        }
                    }
                    if (VideoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
                else
                    return EnumResultType.NotFound;
            }
        }
        public EnumResultType DeleteVideo(XsiVideo entity)
        {
            List<XsiVideo> entities = GetVideo(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiVideo e in entities)
                {
                    result = DeleteVideo(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteVideo(long itemId)
        {
            XsiVideo entity = GetVideoByItemId(itemId);
            if (entity != null)
            {
                using (VideoRepository = new VideoRepository(ConnectionString))
                {
                    VideoRepository.Delete(itemId);
                    if (VideoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
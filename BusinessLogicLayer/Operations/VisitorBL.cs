﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class VisitorBL : IDisposable
    {
        #region Feilds
        IVisitorRepository VisitorRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public VisitorBL()
        {
            ConnectionString = null;
        }
        public VisitorBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.VisitorRepository != null)
                this.VisitorRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiVisitor GetVisitorByItemId(long itemId)
        {
            using (VisitorRepository = new VisitorRepository(ConnectionString))
            {
                return VisitorRepository.GetById(itemId);
            }
        }
        

        public List<XsiVisitor> GetVisitor()
        {
            using (VisitorRepository = new VisitorRepository(ConnectionString))
            {
                return VisitorRepository.Select();
            }
        }
        public List<XsiVisitor> GetVisitor(EnumSortlistBy sortListBy)
        {
            using (VisitorRepository = new VisitorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return VisitorRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return VisitorRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return VisitorRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return VisitorRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return VisitorRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return VisitorRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return VisitorRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiVisitor> GetVisitor(XsiVisitor entity)
        {
            using (VisitorRepository = new VisitorRepository(ConnectionString))
            {
                return VisitorRepository.Select(entity);
            }
        }
        public List<XsiVisitor> GetVisitor(XsiVisitor entity, EnumSortlistBy sortListBy)
        {
            using (VisitorRepository = new VisitorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return VisitorRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return VisitorRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return VisitorRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return VisitorRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return VisitorRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return VisitorRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return VisitorRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiVisitor> GetVisitorOr(XsiVisitor entity, EnumSortlistBy sortListBy)
        {
            using (VisitorRepository = new VisitorRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return VisitorRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return VisitorRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return VisitorRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return VisitorRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return VisitorRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return VisitorRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return VisitorRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        //public XsiVisitor GetVisitor(long groupId, long languageId)
        //{
        //    using (VisitorRepository = new VisitorRepository(ConnectionString))
        //    {
        //        return VisitorRepository.Select(new XsiVisitor() { GroupId = groupId, LanguageId = languageId }).FirstOrDefault();
        //    }
        //}

        public EnumResultType InsertVisitor(XsiVisitor entity)
        {
            using (VisitorRepository = new VisitorRepository(ConnectionString))
            {
                entity = VisitorRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateVisitor(XsiVisitor entity)
        {
            XsiVisitor OriginalEntity = GetVisitorByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (VisitorRepository = new VisitorRepository(ConnectionString))
                {
                    VisitorRepository.Update(entity);
                    if (VisitorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateVisitorStatus(long itemId)
        {
            XsiVisitor entity = GetVisitorByItemId(itemId);
            if (entity != null)
            {
                using (VisitorRepository = new VisitorRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    VisitorRepository.Update(entity);
                    if (VisitorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteVisitor(XsiVisitor entity)
        {
            List<XsiVisitor> entities = GetVisitor(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiVisitor e in entities)
                {
                    result = DeleteVisitor(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteVisitor(long itemId)
        {
            XsiVisitor entity = GetVisitorByItemId(itemId);
            if (entity != null)
            {
                using (VisitorRepository = new VisitorRepository(ConnectionString))
                {
                    VisitorRepository.Delete(itemId);
                    if (VisitorRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
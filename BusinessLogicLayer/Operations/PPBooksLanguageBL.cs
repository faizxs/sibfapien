﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PPBooksLanguageBL : IDisposable
    {
        #region Feilds
        IPPBooksLanguageRepository PPBooksLanguageRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PPBooksLanguageBL()
        {
            ConnectionString = null;
        }
        public PPBooksLanguageBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PPBooksLanguageRepository != null)
                this.PPBooksLanguageRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (PPBooksLanguageRepository = new PPBooksLanguageRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionPpbooksLanguage GetPPBooksLanguageByItemId(long itemId)
        {
            using (PPBooksLanguageRepository = new PPBooksLanguageRepository(ConnectionString))
            {
                return PPBooksLanguageRepository.GetById(itemId);
            }
        }
        public List<XsiExhibitionPpbooksLanguage> GetPPBooksLanguage()
        {
            using (PPBooksLanguageRepository = new PPBooksLanguageRepository(ConnectionString))
            {
                return PPBooksLanguageRepository.Select();
            }
        }
        public List<XsiExhibitionPpbooksLanguage> GetPPBooksLanguage(EnumSortlistBy sortListBy)
        {
            using (PPBooksLanguageRepository = new PPBooksLanguageRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return PPBooksLanguageRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PPBooksLanguageRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PPBooksLanguageRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionPpbooksLanguage> GetPPBooksLanguage(XsiExhibitionPpbooksLanguage entity)
        {
            using (PPBooksLanguageRepository = new PPBooksLanguageRepository(ConnectionString))
            {
                return PPBooksLanguageRepository.Select(entity);
            }
        }
        public List<XsiExhibitionPpbooksLanguage> GetPPBooksLanguage(XsiExhibitionPpbooksLanguage entity, EnumSortlistBy sortListBy)
        {
            using (PPBooksLanguageRepository = new PPBooksLanguageRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return PPBooksLanguageRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PPBooksLanguageRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PPBooksLanguageRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionPpbooksLanguage GetPPBooksLanguage(long groupId, long languageId)
        {
            return null;
        }

        public EnumResultType InsertPPBooksLanguage(XsiExhibitionPpbooksLanguage entity)
        {
            using (PPBooksLanguageRepository = new PPBooksLanguageRepository(ConnectionString))
            {
                entity = PPBooksLanguageRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePPBooksLanguage(XsiExhibitionPpbooksLanguage entity)
        {
            XsiExhibitionPpbooksLanguage OriginalEntity = GetPPBooksLanguageByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PPBooksLanguageRepository = new PPBooksLanguageRepository(ConnectionString))
                {
                    PPBooksLanguageRepository.Update(entity);
                    if (PPBooksLanguageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePPBooksLanguage(XsiExhibitionPpbooksLanguage entity)
        {
            List<XsiExhibitionPpbooksLanguage> entities = GetPPBooksLanguage(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionPpbooksLanguage e in entities)
                {
                    result = DeletePPBooksLanguage(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeletePPBooksLanguage(long itemId)
        {
            XsiExhibitionPpbooksLanguage entity = GetPPBooksLanguageByItemId(itemId);
            if (entity != null)
            {
                using (PPBooksLanguageRepository = new PPBooksLanguageRepository(ConnectionString))
                {
                    PPBooksLanguageRepository.Delete(itemId);
                    if (PPBooksLanguageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionMemberApplicationYearlyPreApprovedBL : IDisposable
    {
        #region Feilds
        IExhibitionMemberApplicationYearlyPreApprovedRepository ExhibitionMemberApplicationYearlyPreApprovedsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionMemberApplicationYearlyPreApprovedBL()
        {
            ConnectionString = null;
        }
        public ExhibitionMemberApplicationYearlyPreApprovedBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionMemberApplicationYearlyPreApprovedsRepository != null)
                this.ExhibitionMemberApplicationYearlyPreApprovedsRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionMemberApplicationYearlyPreApproved GetExhibitionMemberApplicationYearlyPreApprovedByItemId(long itemId)
        {
            using (ExhibitionMemberApplicationYearlyPreApprovedsRepository = new ExhibitionMemberApplicationYearlyPreApprovedRepository(ConnectionString))
            {
                return ExhibitionMemberApplicationYearlyPreApprovedsRepository.GetById(itemId);
            }
        }
        
        public List<XsiExhibitionMemberApplicationYearlyPreApproved> GetExhibitionMemberApplicationYearlyPreApproved()
        {
            using (ExhibitionMemberApplicationYearlyPreApprovedsRepository = new ExhibitionMemberApplicationYearlyPreApprovedRepository(ConnectionString))
            {
                return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select();
            }
        }
        public List<XsiExhibitionMemberApplicationYearlyPreApproved> GetExhibitionMemberApplicationYearlyPreApproved(EnumSortlistBy sortListBy)
        {
            using (ExhibitionMemberApplicationYearlyPreApprovedsRepository = new ExhibitionMemberApplicationYearlyPreApprovedRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select().OrderBy(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select().OrderByDescending(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionMemberApplicationYearlyPreApproved> GetExhibitionMemberApplicationYearlyPreApproved(XsiExhibitionMemberApplicationYearlyPreApproved entity)
        {
            using (ExhibitionMemberApplicationYearlyPreApprovedsRepository = new ExhibitionMemberApplicationYearlyPreApprovedRepository(ConnectionString))
            {
                return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select(entity);
            }
        }
        public List<XsiExhibitionMemberApplicationYearlyPreApproved> GetExhibitionMemberApplicationYearlyPreApproved(XsiExhibitionMemberApplicationYearlyPreApproved entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionMemberApplicationYearlyPreApprovedsRepository = new ExhibitionMemberApplicationYearlyPreApprovedRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select(entity).OrderBy(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select(entity).OrderByDescending(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionMemberApplicationYearlyPreApprovedsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertExhibitionMemberApplicationYearlyPreApproved(XsiExhibitionMemberApplicationYearlyPreApproved entity)
        {
            using (ExhibitionMemberApplicationYearlyPreApprovedsRepository = new ExhibitionMemberApplicationYearlyPreApprovedRepository(ConnectionString))
            {
                entity = ExhibitionMemberApplicationYearlyPreApprovedsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionMemberApplicationYearlyPreApproved(XsiExhibitionMemberApplicationYearlyPreApproved entity)
        {
            XsiExhibitionMemberApplicationYearlyPreApproved OriginalEntity = GetExhibitionMemberApplicationYearlyPreApprovedByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionMemberApplicationYearlyPreApprovedsRepository = new ExhibitionMemberApplicationYearlyPreApprovedRepository(ConnectionString))
                {
                    ExhibitionMemberApplicationYearlyPreApprovedsRepository.Update(entity);
                    if (ExhibitionMemberApplicationYearlyPreApprovedsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateExhibitionMemberApplicationYearlyPreApprovedStatus(long itemId, long langId)
        {
            XsiExhibitionMemberApplicationYearlyPreApproved entity = GetExhibitionMemberApplicationYearlyPreApprovedByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionMemberApplicationYearlyPreApprovedsRepository = new ExhibitionMemberApplicationYearlyPreApprovedRepository(ConnectionString))
                {
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionMemberApplicationYearlyPreApprovedsRepository.Update(entity);
                    if (ExhibitionMemberApplicationYearlyPreApprovedsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionMemberApplicationYearlyPreApproved(XsiExhibitionMemberApplicationYearlyPreApproved entity)
        {
            List<XsiExhibitionMemberApplicationYearlyPreApproved> entities = GetExhibitionMemberApplicationYearlyPreApproved(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionMemberApplicationYearlyPreApproved e in entities)
                {
                    result = DeleteExhibitionMemberApplicationYearlyPreApproved(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionMemberApplicationYearlyPreApproved(long itemId)
        {
            XsiExhibitionMemberApplicationYearlyPreApproved entity = GetExhibitionMemberApplicationYearlyPreApprovedByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionMemberApplicationYearlyPreApprovedsRepository = new ExhibitionMemberApplicationYearlyPreApprovedRepository(ConnectionString))
                {
                    ExhibitionMemberApplicationYearlyPreApprovedsRepository.Delete(itemId);
                    if (ExhibitionMemberApplicationYearlyPreApprovedsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
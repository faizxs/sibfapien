﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFAwardsNominationFormBL : IDisposable
    {
        #region Feilds
        ISCRFAwardNominationFormRepository ISCRFAwardNominationFormRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFAwardsNominationFormBL()
        {
            ConnectionString = null;
        }
        public SCRFAwardsNominationFormBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ISCRFAwardNominationFormRepository != null)
                this.ISCRFAwardNominationFormRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (ISCRFAwardNominationFormRepository = new SCRFAwardsNominationFormRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiScrfawardNominationForm GetSCRFAwardsNominationFormByItemId(long itemId)
        {
            using (ISCRFAwardNominationFormRepository = new SCRFAwardsNominationFormRepository(ConnectionString))
            {
                return ISCRFAwardNominationFormRepository.GetById(itemId);
            }
        }
        
        public List<XsiScrfawardNominationForm> GetSCRFAwardsNominationForm()
        {
            using (ISCRFAwardNominationFormRepository = new SCRFAwardsNominationFormRepository(ConnectionString))
            {
                return ISCRFAwardNominationFormRepository.Select();
            }
        }
        public List<XsiScrfawardNominationForm> GetSCRFAwardsNominationForm(EnumSortlistBy sortListBy)
        {
            using (ISCRFAwardNominationFormRepository = new SCRFAwardsNominationFormRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ISCRFAwardNominationFormRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ISCRFAwardNominationFormRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ISCRFAwardNominationFormRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ISCRFAwardNominationFormRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ISCRFAwardNominationFormRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ISCRFAwardNominationFormRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ISCRFAwardNominationFormRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfawardNominationForm> GetSCRFAwardsNominationForm(XsiScrfawardNominationForm entity)
        {
            using (ISCRFAwardNominationFormRepository = new SCRFAwardsNominationFormRepository(ConnectionString))
            {
                return ISCRFAwardNominationFormRepository.Select(entity);
            }
        }
        public List<XsiScrfawardNominationForm> GetSCRFAwardsNominationForm(XsiScrfawardNominationForm entity, EnumSortlistBy sortListBy)
        {
            using (ISCRFAwardNominationFormRepository = new SCRFAwardsNominationFormRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ISCRFAwardNominationFormRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ISCRFAwardNominationFormRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ISCRFAwardNominationFormRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ISCRFAwardNominationFormRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ISCRFAwardNominationFormRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ISCRFAwardNominationFormRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ISCRFAwardNominationFormRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiScrfawardNominationForm GetSCRFAwardsNominationForm(long groupId, long languageId)
        {
            using (ISCRFAwardNominationFormRepository = new SCRFAwardsNominationFormRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertSCRFAwardsNominationForm(XsiScrfawardNominationForm entity)
        {
            using (ISCRFAwardNominationFormRepository = new SCRFAwardsNominationFormRepository(ConnectionString))
            {
                entity = ISCRFAwardNominationFormRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    //  XsiGroupId = entity.GroupId ?? -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFAwardsNominationForm(XsiScrfawardNominationForm entity)
        {
            XsiScrfawardNominationForm OriginalEntity = GetSCRFAwardsNominationFormByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ISCRFAwardNominationFormRepository = new SCRFAwardsNominationFormRepository(ConnectionString))
                {
                    ISCRFAwardNominationFormRepository.Update(entity);
                    if (ISCRFAwardNominationFormRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFAwardsNominationFormStatus(long itemId)
        {
            XsiScrfawardNominationForm entity = GetSCRFAwardsNominationFormByItemId(itemId);
            if (entity != null)
            {
                using (ISCRFAwardNominationFormRepository = new SCRFAwardsNominationFormRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ISCRFAwardNominationFormRepository.Update(entity);
                    if (ISCRFAwardNominationFormRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFAwardsNominationForm(XsiScrfawardNominationForm entity)
        {
            List<XsiScrfawardNominationForm> entities = GetSCRFAwardsNominationForm(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfawardNominationForm e in entities)
                {
                    result = DeleteSCRFAwardsNominationForm(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFAwardsNominationForm(long itemId)
        {
            XsiScrfawardNominationForm entity = GetSCRFAwardsNominationFormByItemId(itemId);
            if (entity != null)
            {
                using (ISCRFAwardNominationFormRepository = new SCRFAwardsNominationFormRepository(ConnectionString))
                {
                    ISCRFAwardNominationFormRepository.Delete(itemId);
                    if (ISCRFAwardNominationFormRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
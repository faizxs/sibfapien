﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class OurteamDepartmentBL : IDisposable
    {
        #region Feilds
        IOurteamDepartmentRepository OurteamDepartmentRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public OurteamDepartmentBL()
        {
            ConnectionString = null;
        }
        public OurteamDepartmentBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.OurteamDepartmentRepository != null)
                this.OurteamDepartmentRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiOurteamDepartment GetOurteamDepartmentByItemId(long itemId)
        {
            using (OurteamDepartmentRepository = new OurteamDepartmentRepository(ConnectionString))
            {
                return OurteamDepartmentRepository.GetById(itemId);
            }
        }
        public List<XsiOurteamDepartment> GetOurteamDepartment()
        {
            using (OurteamDepartmentRepository = new OurteamDepartmentRepository(ConnectionString))
            {
                return OurteamDepartmentRepository.Select();
            }
        }
        public List<XsiOurteamDepartment> GetOurteamDepartment(EnumSortlistBy sortListBy)
        {
            using (OurteamDepartmentRepository = new OurteamDepartmentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return OurteamDepartmentRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return OurteamDepartmentRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return OurteamDepartmentRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return OurteamDepartmentRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return OurteamDepartmentRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return OurteamDepartmentRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return OurteamDepartmentRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiOurteamDepartment> GetOurteamDepartment(XsiOurteamDepartment entity)
        {
            using (OurteamDepartmentRepository = new OurteamDepartmentRepository(ConnectionString))
            {
                return OurteamDepartmentRepository.Select(entity);
            }
        }
        public List<XsiOurteamDepartment> GetOurteamDepartment(XsiOurteamDepartment entity, EnumSortlistBy sortListBy)
        {
            using (OurteamDepartmentRepository = new OurteamDepartmentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return OurteamDepartmentRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return OurteamDepartmentRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return OurteamDepartmentRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return OurteamDepartmentRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return OurteamDepartmentRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return OurteamDepartmentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return OurteamDepartmentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiOurteamDepartment> GetOurteamDepartmentOr(XsiOurteamDepartment entity, EnumSortlistBy sortListBy)
        {
            using (OurteamDepartmentRepository = new OurteamDepartmentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return OurteamDepartmentRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return OurteamDepartmentRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return OurteamDepartmentRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return OurteamDepartmentRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return OurteamDepartmentRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return OurteamDepartmentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return OurteamDepartmentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiOurteamDepartment> GetOtherLanguageOurteamDepartment(XsiOurteamDepartment entity)
        {
            using (OurteamDepartmentRepository = new OurteamDepartmentRepository(ConnectionString))
            {
                return OurteamDepartmentRepository.SelectOtherLanguageCategory(entity);
            }
        }
        public List<XsiOurteamDepartment> GetCurrentLanguageOurteamDepartment(XsiOurteamDepartment entity)
        {
            using (OurteamDepartmentRepository = new OurteamDepartmentRepository(ConnectionString))
            {
                return OurteamDepartmentRepository.SelectCurrentLanguageCategory(entity);
            }
        }

        public EnumResultType InsertOurteamDepartment(XsiOurteamDepartment entity)
        {
            using (OurteamDepartmentRepository = new OurteamDepartmentRepository(ConnectionString))
            {
                entity = OurteamDepartmentRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateOurteamDepartment(XsiOurteamDepartment entity)
        {
            XsiOurteamDepartment OriginalEntity = GetOurteamDepartmentByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (OurteamDepartmentRepository = new OurteamDepartmentRepository(ConnectionString))
                {
                    OurteamDepartmentRepository.Update(entity);
                    if (OurteamDepartmentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateOurteamDepartmentStatus(long itemId, long langId)
        {
            XsiOurteamDepartment entity = GetOurteamDepartmentByItemId(itemId);
            if (entity != null)
            {
                using (OurteamDepartmentRepository = new OurteamDepartmentRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    OurteamDepartmentRepository.Update(entity);
                    if (OurteamDepartmentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteOurteamDepartment(XsiOurteamDepartment entity)
        {
            List<XsiOurteamDepartment> entities = GetOurteamDepartment(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiOurteamDepartment e in entities)
                {
                    result = DeleteOurteamDepartment(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteOurteamDepartment(long itemId)
        {
            XsiOurteamDepartment entity = GetOurteamDepartmentByItemId(itemId);
            if (entity != null)
            {
                using (OurteamDepartmentRepository = new OurteamDepartmentRepository(ConnectionString))
                {
                    OurteamDepartmentRepository.Delete(itemId);
                    if (OurteamDepartmentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
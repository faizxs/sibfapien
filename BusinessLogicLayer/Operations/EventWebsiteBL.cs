﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class EventWebsiteBL : IDisposable
    {
        #region Feilds
        IEventWebsiteRepository EventWebsiteRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public EventWebsiteBL()
        {
            ConnectionString = null;
        }
        public EventWebsiteBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventWebsiteRepository != null)
                this.EventWebsiteRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (EventWebsiteRepository = new EventWebsiteRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiEventWebsite GetEventWebsiteByItemId(long itemId)
        {
            using (EventWebsiteRepository = new EventWebsiteRepository(ConnectionString))
            {
                return EventWebsiteRepository.GetById(itemId);
            }
        }
         
        public List<XsiEventWebsite> GetEventWebsite()
        {
            using (EventWebsiteRepository = new EventWebsiteRepository(ConnectionString))
            {
                return EventWebsiteRepository.Select();
            }
        }
        public List<XsiEventWebsite> GetEventWebsite(EnumSortlistBy sortListBy)
        {
            using (EventWebsiteRepository = new EventWebsiteRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return EventWebsiteRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventWebsiteRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventWebsiteRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEventWebsite> GetEventWebsite(XsiEventWebsite entity)
        {
            using (EventWebsiteRepository = new EventWebsiteRepository(ConnectionString))
            {
                return EventWebsiteRepository.Select(entity);
            }
        }
        public List<XsiEventWebsite> GetEventWebsite(XsiEventWebsite entity, EnumSortlistBy sortListBy)
        {
            using (EventWebsiteRepository = new EventWebsiteRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return EventWebsiteRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventWebsiteRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventWebsiteRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiEventWebsite GetEventWebsite(long groupId, long languageId)
        {
            using (EventWebsiteRepository = new EventWebsiteRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertEventWebsite(XsiEventWebsite entity)
        {
            using (EventWebsiteRepository = new EventWebsiteRepository(ConnectionString))
            {
                entity = EventWebsiteRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateEventWebsite(XsiEventWebsite entity)
        {
            XsiEventWebsite OriginalEntity = GetEventWebsiteByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (EventWebsiteRepository = new EventWebsiteRepository(ConnectionString))
                {
                    EventWebsiteRepository.Update(entity);
                    if (EventWebsiteRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateEventWebsiteStatus(long itemId)
        {
            return EnumResultType.NotFound;
        }
        public EnumResultType DeleteEventWebsite(XsiEventWebsite entity)
        {
            List<XsiEventWebsite> entities = GetEventWebsite(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiEventWebsite e in entities)
                {
                    result = DeleteEventWebsite(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteEventWebsite(long itemId)
        {
            XsiEventWebsite entity = GetEventWebsiteByItemId(itemId);
            if (entity != null)
            {
                using (EventWebsiteRepository = new EventWebsiteRepository(ConnectionString))
                {
                    EventWebsiteRepository.Delete(itemId);
                    if (EventWebsiteRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFIllustrationSubmissionBL : IDisposable
    {
        #region Feilds
        ISCRFIllustrationSubmissionRepository SCRFIllustrationSubmissionRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFIllustrationSubmissionBL()
        {
            ConnectionString = null;
        }
        public SCRFIllustrationSubmissionBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIllustrationSubmissionRepository != null)
                this.SCRFIllustrationSubmissionRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiScrfillustrationSubmission GetSCRFIllustrationSubmissionByItemId(long itemId)
        {
            using (SCRFIllustrationSubmissionRepository = new SCRFIllustrationSubmissionRepository(ConnectionString))
            {
                return SCRFIllustrationSubmissionRepository.GetById(itemId);
            }
        }

        public List<XsiScrfillustrationSubmission> GetSCRFIllustrationSubmission()
        {
            using (SCRFIllustrationSubmissionRepository = new SCRFIllustrationSubmissionRepository(ConnectionString))
            {
                return SCRFIllustrationSubmissionRepository.Select();
            }
        }
        public List<XsiScrfillustrationSubmission> GetSCRFIllustrationSubmission(EnumSortlistBy sortListBy)
        {
            using (SCRFIllustrationSubmissionRepository = new SCRFIllustrationSubmissionRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIllustrationSubmissionRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIllustrationSubmissionRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFIllustrationSubmissionRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFIllustrationSubmissionRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIllustrationSubmissionRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIllustrationSubmissionRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIllustrationSubmissionRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfillustrationSubmission> GetSCRFIllustrationSubmission(XsiScrfillustrationSubmission entity)
        {
            using (SCRFIllustrationSubmissionRepository = new SCRFIllustrationSubmissionRepository(ConnectionString))
            {
                return SCRFIllustrationSubmissionRepository.Select(entity);
            }
        }
        public List<XsiScrfillustrationSubmission> GetSCRFIllustrationSubmission(XsiScrfillustrationSubmission entity, EnumSortlistBy sortListBy)
        {
            using (SCRFIllustrationSubmissionRepository = new SCRFIllustrationSubmissionRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIllustrationSubmissionRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIllustrationSubmissionRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFIllustrationSubmissionRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFIllustrationSubmissionRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIllustrationSubmissionRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIllustrationSubmissionRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIllustrationSubmissionRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFIllustrationSubmission(XsiScrfillustrationSubmission entity)
        {
            using (SCRFIllustrationSubmissionRepository = new SCRFIllustrationSubmissionRepository(ConnectionString))
            {
                entity = SCRFIllustrationSubmissionRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFIllustrationSubmission(XsiScrfillustrationSubmission entity)
        {
            XsiScrfillustrationSubmission OriginalEntity = GetSCRFIllustrationSubmissionByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFIllustrationSubmissionRepository = new SCRFIllustrationSubmissionRepository(ConnectionString))
                {

                    SCRFIllustrationSubmissionRepository.Update(entity);
                    if (SCRFIllustrationSubmissionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFIllustrationSubmissionStatus(long itemId)
        {
            XsiScrfillustrationSubmission entity = GetSCRFIllustrationSubmissionByItemId(itemId);
            if (entity != null)
            {
                using (SCRFIllustrationSubmissionRepository = new SCRFIllustrationSubmissionRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFIllustrationSubmissionRepository.Update(entity);
                    if (SCRFIllustrationSubmissionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFIllustrationSubmission(XsiScrfillustrationSubmission entity)
        {
            List<XsiScrfillustrationSubmission> entities = GetSCRFIllustrationSubmission(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfillustrationSubmission e in entities)
                {
                    result = DeleteSCRFIllustrationSubmission(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFIllustrationSubmission(long itemId)
        {
            XsiScrfillustrationSubmission entity = GetSCRFIllustrationSubmissionByItemId(itemId);
            if (entity != null)
            {
                using (SCRFIllustrationSubmissionRepository = new SCRFIllustrationSubmissionRepository(ConnectionString))
                {
                    SCRFIllustrationSubmissionRepository.Delete(itemId);
                    if (SCRFIllustrationSubmissionRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        //Custom Methods
        public long GetHomeBannerInfo(long languageid)
        {
            using (SCRFIllustrationSubmissionRepository = new SCRFIllustrationSubmissionRepository(ConnectionString))
            {
                return SCRFIllustrationSubmissionRepository.Select(new XsiScrfillustrationSubmission() { IsActive = EnumConversion.ToString(EnumBool.Yes) }).Count();
            }
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitorBadgeBL : IDisposable
    {
        #region Feilds
        IExhibitorBadgeRepository ExhibitorBadgeRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitorBadgeBL()
        {
            ConnectionString = null;
        }
        public ExhibitorBadgeBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitorBadgeRepository != null)
                this.ExhibitorBadgeRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitorBadges GetExhibitorBadgesByItemId(long itemId)
        {
            using (ExhibitorBadgeRepository = new ExhibitorBadgeRepository(ConnectionString))
            {
                return ExhibitorBadgeRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitorBadges> GetExhibitorBadges()
        {
            using (ExhibitorBadgeRepository = new ExhibitorBadgeRepository(ConnectionString))
            {
                return ExhibitorBadgeRepository.Select();
            }
        }
        public List<XsiExhibitorBadges> GetExhibitorBadges(EnumSortlistBy sortListBy)
        {
            using (ExhibitorBadgeRepository = new ExhibitorBadgeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitorBadgeRepository.Select().OrderBy(p => p.Name).ToList();

                   

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitorBadgeRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitorBadgeRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitorBadgeRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitorBadges> GetExhibitorBadges(XsiExhibitorBadges entity)
        {
            using (ExhibitorBadgeRepository = new ExhibitorBadgeRepository(ConnectionString))
            {
                return ExhibitorBadgeRepository.Select(entity);
            }
        }
        public List<XsiExhibitorBadges> GetExhibitorBadges(XsiExhibitorBadges entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitorBadgeRepository = new ExhibitorBadgeRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitorBadgeRepository.Select(entity).OrderBy(p => p.Name).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return ExhibitorBadgeRepository.Select(entity).OrderByDescending(p => p.Type).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitorBadgeRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitorBadgeRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitorBadgeRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        //public List<XsiExhibitorBadges> GetExhibitorBadgesOr(XsiExhibitorBadges entity, EnumSortlistBy sortListBy)
        //{
        //    using (ExhibitorBadgeRepository = new ExhibitorBadgeRepository(ConnectionString))
        //    {
        //        switch (sortListBy)
        //        {
        //            case EnumSortlistBy.ByAlphabetAsc:
        //                return ExhibitorBadgeRepository.SelectOr(entity).OrderBy(p => p.Name).ToList();

        //            case EnumSortlistBy.ByItemIdAsc:
        //                return ExhibitorBadgeRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

        //            case EnumSortlistBy.ByItemIdDesc:
        //                return ExhibitorBadgeRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

        //            default:
        //                return ExhibitorBadgeRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
        //        }
        //    }
        //}
        public EnumResultType InsertExhibitorBadges(XsiExhibitorBadges entity)
        {
            using (ExhibitorBadgeRepository = new ExhibitorBadgeRepository(ConnectionString))
            {
                entity = ExhibitorBadgeRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitorBadges(XsiExhibitorBadges entity)
        {
            XsiExhibitorBadges OriginalEntity = GetExhibitorBadgesByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitorBadgeRepository = new ExhibitorBadgeRepository(ConnectionString))
                {
                    ExhibitorBadgeRepository.Update(entity);
                    if (ExhibitorBadgeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSchoolRegistrationStatus(long itemId)
        {
            XsiExhibitorBadges entity = GetExhibitorBadgesByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitorBadgeRepository = new ExhibitorBadgeRepository(ConnectionString))
                {
                    //if (EnumConversion.ToString(EnumStatus.Open) == entity.Status)
                    //{
                    //    entity.Status = EnumConversion.ToString(EnumStatus.CLose);
                    //}
                    //else
                    //{
                    //    entity.Status = EnumConversion.ToString(EnumStatus.CLose);
                    //    entity.DateClosed = DateTime.Now.Date;
                    //}

                    ExhibitorBadgeRepository.Update(entity);
                    if (ExhibitorBadgeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSchoolRegistrationFlag(long itemId, string flag)
        {
            XsiExhibitorBadges entity = GetExhibitorBadgesByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitorBadgeRepository = new ExhibitorBadgeRepository(ConnectionString))
                {
                    //entity.FlagType = flag;
                    ExhibitorBadgeRepository.Update(entity);
                    if (ExhibitorBadgeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitorBadges(XsiExhibitorBadges entity)
        {
            if (GetExhibitorBadges(entity).Count() > 0)
            {
                using (ExhibitorBadgeRepository = new ExhibitorBadgeRepository(ConnectionString))
                {
                    ExhibitorBadgeRepository.Delete(entity);
                    if (ExhibitorBadgeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitorBadges(long itemId)
        {
            if (GetExhibitorBadgesByItemId(itemId) != null)
            {
                using (ExhibitorBadgeRepository = new ExhibitorBadgeRepository(ConnectionString))
                {
                    ExhibitorBadgeRepository.Delete(itemId);
                    if (ExhibitorBadgeRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}

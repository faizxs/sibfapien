﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class CareerVisaStatusBL : IDisposable
    {
        #region Feilds
        ICareerVisaStatusRepository CareerVisaStatusRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public CareerVisaStatusBL()
        {
            ConnectionString = null;
        }
        public CareerVisaStatusBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerVisaStatusRepository != null)
                this.CareerVisaStatusRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiCareerVisaStatus GetCareerVisaStatusByItemId(long itemId)
        {
            using (CareerVisaStatusRepository = new CareerVisaStatusRepository(ConnectionString))
            {
                return CareerVisaStatusRepository.GetById(itemId);
            }
        }
        
        public List<XsiCareerVisaStatus> GetCareerVisaStatus()
        {
            using (CareerVisaStatusRepository = new CareerVisaStatusRepository(ConnectionString))
            {
                return CareerVisaStatusRepository.Select();
            }
        }
        public List<XsiCareerVisaStatus> GetCareerVisaStatus(EnumSortlistBy sortListBy)
        {
            using (CareerVisaStatusRepository = new CareerVisaStatusRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerVisaStatusRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerVisaStatusRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerVisaStatusRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerVisaStatusRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerVisaStatusRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerVisaStatusRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerVisaStatusRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareerVisaStatus> GetCareerVisaStatus(XsiCareerVisaStatus entity)
        {
            using (CareerVisaStatusRepository = new CareerVisaStatusRepository(ConnectionString))
            {
                return CareerVisaStatusRepository.Select(entity);
            }
        }
        public List<XsiCareerVisaStatus> GetCareerVisaStatus(XsiCareerVisaStatus entity, EnumSortlistBy sortListBy)
        {
            using (CareerVisaStatusRepository = new CareerVisaStatusRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerVisaStatusRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerVisaStatusRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerVisaStatusRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerVisaStatusRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerVisaStatusRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerVisaStatusRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerVisaStatusRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareerVisaStatus> GetCareerVisaStatusOr(XsiCareerVisaStatus entity, EnumSortlistBy sortListBy)
        {
            using (CareerVisaStatusRepository = new CareerVisaStatusRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerVisaStatusRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerVisaStatusRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerVisaStatusRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerVisaStatusRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerVisaStatusRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerVisaStatusRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerVisaStatusRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public EnumResultType InsertCareerVisaStatus(XsiCareerVisaStatus entity)
        {
            using (CareerVisaStatusRepository = new CareerVisaStatusRepository(ConnectionString))
            {
                entity = CareerVisaStatusRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateCareerVisaStatus(XsiCareerVisaStatus entity)
        {
            XsiCareerVisaStatus OriginalEntity = GetCareerVisaStatusByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (CareerVisaStatusRepository = new CareerVisaStatusRepository(ConnectionString))
                {
                    CareerVisaStatusRepository.Update(entity);
                    if (CareerVisaStatusRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateCareerVisaStatusStatus(long itemId)
        {
            XsiCareerVisaStatus entity = GetCareerVisaStatusByItemId(itemId);
            if (entity != null)
            {
                using (CareerVisaStatusRepository = new CareerVisaStatusRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    CareerVisaStatusRepository.Update(entity);
                    if (CareerVisaStatusRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteCareerVisaStatus(XsiCareerVisaStatus entity)
        {
            List<XsiCareerVisaStatus> entities = GetCareerVisaStatus(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiCareerVisaStatus e in entities)
                {
                    result = DeleteCareerVisaStatus(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteCareerVisaStatus(long itemId)
        {
            XsiCareerVisaStatus entity = GetCareerVisaStatusByItemId(itemId);
            if (entity != null)
            {
                using (CareerVisaStatusRepository = new CareerVisaStatusRepository(ConnectionString))
                {
                    CareerVisaStatusRepository.Delete(itemId);
                    if (CareerVisaStatusRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
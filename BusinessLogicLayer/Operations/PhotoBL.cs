﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PhotoBL : IDisposable
    {
        #region Feilds
        IPhotoRepository PhotoRepository;

        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PhotoBL()
        {
            ConnectionString = null;
        }
        public PhotoBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PhotoRepository != null)
                this.PhotoRepository.Dispose();
        }
        #endregion
        #region Methods

        public long GetNextSortIndex()
        {
            //optional method. return -1 if not required and remove following
            using (PhotoRepository = new PhotoRepository(ConnectionString))
            {
                return (PhotoRepository.Select().Max(p => p.SortIndex) ?? 0) + 1;
            }
        }

        public XsiPhoto GetPhotoByItemId(long itemId)
        {
            using (PhotoRepository = new PhotoRepository(ConnectionString))
            {
                return PhotoRepository.GetById(itemId);
            }
        }

        public List<XsiPhoto> GetPhoto()
        {
            using (PhotoRepository = new PhotoRepository(ConnectionString))
            {
                return PhotoRepository.Select();
            }
        }
        public List<XsiPhoto> GetPhoto(EnumSortlistBy sortListBy)
        {
            using (PhotoRepository = new PhotoRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PhotoRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PhotoRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PhotoRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PhotoRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PhotoRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PhotoRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PhotoRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPhoto> GetPhoto(XsiPhoto entity)
        {
            using (PhotoRepository = new PhotoRepository(ConnectionString))
            {
                return PhotoRepository.Select(entity);
            }
        }
        public List<XsiPhoto> GetPhoto(XsiPhoto entity, EnumSortlistBy sortListBy)
        {
            using (PhotoRepository = new PhotoRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PhotoRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PhotoRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PhotoRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PhotoRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PhotoRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PhotoRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.BySortOrderAsc:
                        return PhotoRepository.Select(entity).OrderBy(p => p.SortIndex).ToList();

                    case EnumSortlistBy.BySortOrderDesc:
                        return PhotoRepository.Select(entity).OrderByDescending(p => p.SortIndex).ToList();

                    default:
                        return PhotoRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertPhoto(XsiPhoto entity)
        {
            using (PhotoRepository = new PhotoRepository(ConnectionString))
            {
                entity = PhotoRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePhoto(XsiPhoto entity)
        {
            XsiPhoto OriginalEntity = GetPhotoByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PhotoRepository = new PhotoRepository(ConnectionString))
                {
                    PhotoRepository.Update(entity);
                    if (PhotoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdatePhotoStatus(long itemId, long langId)
        {
            XsiPhoto entity = GetPhotoByItemId(itemId);
            if (entity != null)
            {
                using (PhotoRepository = new PhotoRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    PhotoRepository.Update(entity);
                    if (PhotoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateAlbumCover(long itemId)
        {
            using (PhotoRepository = new PhotoRepository(ConnectionString))
            {
                XsiPhoto entity = PhotoRepository.GetById(itemId);
                XsiPhoto entity1 = new XsiPhoto();
                entity1.PhotoAlbumId = entity.PhotoAlbumId;
                List<XsiPhoto> PhotoList = PhotoRepository.Select(entity1);
                if (PhotoList != null)
                {
                    foreach (XsiPhoto entity2 in PhotoList)
                    {
                        if (itemId != entity2.ItemId)
                        {
                            entity2.IsAlbumCover = EnumConversion.ToString(EnumBool.No);
                            PhotoRepository.Update(entity2);
                        }
                    }
                    if (PhotoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
                else
                    return EnumResultType.NotFound;
            }
        }
        public EnumResultType DeletePhoto(XsiPhoto entity)
        {
            List<XsiPhoto> entities = GetPhoto(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiPhoto e in entities)
                {
                    result = DeletePhoto(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeletePhoto(long itemId)
        {
            XsiPhoto entity = GetPhotoByItemId(itemId);
            if (entity != null)
            {
                using (PhotoRepository = new PhotoRepository(ConnectionString))
                {
                    PhotoRepository.Delete(itemId);
                    if (PhotoRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
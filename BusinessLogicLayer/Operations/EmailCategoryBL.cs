﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class EmailCategoryBL : IDisposable
    {
        #region Feilds
        IEmailCategoryRepository IEmailCategoryRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public EmailCategoryBL()
        {
            ConnectionString = null;
        }
        public EmailCategoryBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IEmailCategoryRepository != null)
                this.IEmailCategoryRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiEmailCategory GetEmailCategoryByItemId(long itemId)
        {
            using (IEmailCategoryRepository = new EmailCategoryRepository(ConnectionString))
            {
                return IEmailCategoryRepository.GetById(itemId);
            }
        }

        public List<XsiEmailCategory> GetEmailCategory()
        {
            using (IEmailCategoryRepository = new EmailCategoryRepository(ConnectionString))
            {
                return IEmailCategoryRepository.Select();
            }
        }
        public List<XsiEmailCategory> GetEmailCategory(EnumSortlistBy sortListBy)
        {
            using (IEmailCategoryRepository = new EmailCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IEmailCategoryRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IEmailCategoryRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IEmailCategoryRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IEmailCategoryRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IEmailCategoryRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IEmailCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IEmailCategoryRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEmailCategory> GetEmailCategory(XsiEmailCategory entity)
        {
            using (IEmailCategoryRepository = new EmailCategoryRepository(ConnectionString))
            {
                return IEmailCategoryRepository.Select(entity);
            }
        }
        public List<XsiEmailCategory> GetEmailCategory(XsiEmailCategory entity, EnumSortlistBy sortListBy)
        {
            using (IEmailCategoryRepository = new EmailCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IEmailCategoryRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IEmailCategoryRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IEmailCategoryRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IEmailCategoryRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IEmailCategoryRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IEmailCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IEmailCategoryRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEmailCategory> GetEmailCategoryOr(XsiEmailCategory entity, EnumSortlistBy sortListBy)
        {
            using (IEmailCategoryRepository = new EmailCategoryRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IEmailCategoryRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IEmailCategoryRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IEmailCategoryRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IEmailCategoryRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IEmailCategoryRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IEmailCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IEmailCategoryRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertEmailCategory(XsiEmailCategory entity)
        {
            using (IEmailCategoryRepository = new EmailCategoryRepository(ConnectionString))
            {
                entity = IEmailCategoryRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateEmailCategory(XsiEmailCategory entity)
        {
            XsiEmailCategory OriginalEntity = GetEmailCategoryByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IEmailCategoryRepository = new EmailCategoryRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line
                    IEmailCategoryRepository.Update(entity);
                    if (IEmailCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateEmailCategoryStatus(long itemId, long langId)
        {
            XsiEmailCategory entity = GetEmailCategoryByItemId(itemId);
            if (entity != null)
            {
                using (IEmailCategoryRepository = new EmailCategoryRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IEmailCategoryRepository.Update(entity);
                    if (IEmailCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteEmailCategory(XsiEmailCategory entity)
        {
            List<XsiEmailCategory> entities = GetEmailCategory(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiEmailCategory e in entities)
                {
                    result = DeleteEmailCategory(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteEmailCategory(long itemId)
        {
            XsiEmailCategory entity = GetEmailCategoryByItemId(itemId);
            if (entity != null)
            {
                using (IEmailCategoryRepository = new EmailCategoryRepository(ConnectionString))
                {
                    IEmailCategoryRepository.Delete(itemId);
                    if (IEmailCategoryRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionPublisherBL : IDisposable
    {
        #region Feilds
        IExhibitionPublisherRepository ExhibitionPublisherRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionPublisherBL()
        {
            ConnectionString = null;
        }
        public ExhibitionPublisherBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionPublisherRepository != null)
                this.ExhibitionPublisherRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionBookPublisher GetExhibitionPublisherByItemId(long itemId)
        {
            using (ExhibitionPublisherRepository = new ExhibitionPublisherRepository(ConnectionString))
            {
                return ExhibitionPublisherRepository.GetById(itemId);
            }
        }
         
        public List<XsiExhibitionBookPublisher> GetExhibitionPublisher()
        {
            using (ExhibitionPublisherRepository = new ExhibitionPublisherRepository(ConnectionString))
            {
                return ExhibitionPublisherRepository.Select();
            }
        }
        public List<XsiExhibitionBookPublisher> GetExhibitionPublisher(EnumSortlistBy sortListBy)
        {
            using (ExhibitionPublisherRepository = new ExhibitionPublisherRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionPublisherRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionPublisherRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionPublisherRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionPublisherRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionPublisherRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionPublisherRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionPublisherRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionBookPublisher> GetExhibitionPublisher(XsiExhibitionBookPublisher entity)
        {
            using (ExhibitionPublisherRepository = new ExhibitionPublisherRepository(ConnectionString))
            {
                return ExhibitionPublisherRepository.Select(entity);
            }
        }
        public Array GetExhibitionPublisherForUI(XsiExhibitionBookPublisher entity, long exhibitionGroupID)
        {
            using (ExhibitionPublisherRepository = new ExhibitionPublisherRepository(ConnectionString))
            {
                return ExhibitionPublisherRepository.SelectUI(entity, exhibitionGroupID);
            }
        }
        public List<XsiExhibitionBookPublisher> GetExhibitionPublisherForSCRFUI(XsiExhibitionBookPublisher entity, long exhibitionGroupID)
        {
            using (ExhibitionPublisherRepository = new ExhibitionPublisherRepository(ConnectionString))
            {
                return ExhibitionPublisherRepository.SelectSCRFUI(entity, exhibitionGroupID);
            }
        }
        public List<XsiExhibitionBookPublisher> GetExhibitionPublisher(XsiExhibitionBookPublisher entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionPublisherRepository = new ExhibitionPublisherRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionPublisherRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionPublisherRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionPublisherRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionPublisherRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionPublisherRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionPublisherRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionPublisherRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertExhibitionPublisher(XsiExhibitionBookPublisher entity)
        {
            using (ExhibitionPublisherRepository = new ExhibitionPublisherRepository(ConnectionString))
            {
                entity = ExhibitionPublisherRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionPublisher(XsiExhibitionBookPublisher entity)
        {
            XsiExhibitionBookPublisher OriginalEntity = GetExhibitionPublisherByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionPublisherRepository = new ExhibitionPublisherRepository(ConnectionString))
                {
                    ExhibitionPublisherRepository.Update(entity);
                    if (ExhibitionPublisherRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionPublisherStatus(long itemId)
        {
            XsiExhibitionBookPublisher entity = GetExhibitionPublisherByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionPublisherRepository = new ExhibitionPublisherRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionPublisherRepository.Update(entity);
                    if (ExhibitionPublisherRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionPublisher(XsiExhibitionBookPublisher entity)
        {
            List<XsiExhibitionBookPublisher> entities = GetExhibitionPublisher(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionBookPublisher e in entities)
                {
                    result = DeleteExhibitionPublisher(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionPublisher(long itemId)
        {
            XsiExhibitionBookPublisher entity = GetExhibitionPublisherByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionPublisherRepository = new ExhibitionPublisherRepository(ConnectionString))
                {
                    ExhibitionPublisherRepository.Delete(itemId);
                    if (ExhibitionPublisherRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
        //custom
        public long GetNameCount(long itemID, string title, string titlear)
        {
            using (ExhibitionPublisherRepository = new ExhibitionPublisherRepository(ConnectionString))
            {
                ExhibitionPublisherRepository = new ExhibitionPublisherRepository(ConnectionString);
                return ExhibitionPublisherRepository.GetNameCount(itemID, title, titlear);
            }

        }
    }
}
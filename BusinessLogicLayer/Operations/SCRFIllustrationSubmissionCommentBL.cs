﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFIllustrationSubmissionCommentBL : IDisposable
    {
        #region Feilds
        ISCRFIllustrationSubmissionCommentsRepository SCRFIllustrationSubmissionCommentRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFIllustrationSubmissionCommentBL()
        {
            ConnectionString = null;
        }
        public SCRFIllustrationSubmissionCommentBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.SCRFIllustrationSubmissionCommentRepository != null)
                this.SCRFIllustrationSubmissionCommentRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (SCRFIllustrationSubmissionCommentRepository = new SCRFIllustrationSubmissionCommentRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiScrfillustrationSubmissionComments GetSCRFIllustrationSubmissionCommentByItemId(long itemId)
        {
            using (SCRFIllustrationSubmissionCommentRepository = new SCRFIllustrationSubmissionCommentRepository(ConnectionString))
            {
                return SCRFIllustrationSubmissionCommentRepository.GetById(itemId);
            }
        }
        public List<XsiScrfillustrationSubmissionComments> GetSCRFIllustrationSubmissionComment()
        {
            using (SCRFIllustrationSubmissionCommentRepository = new SCRFIllustrationSubmissionCommentRepository(ConnectionString))
            {
                return SCRFIllustrationSubmissionCommentRepository.Select();
            }
        }
        public List<XsiScrfillustrationSubmissionComments> GetSCRFIllustrationSubmissionComment(EnumSortlistBy sortListBy)
        {
            using (SCRFIllustrationSubmissionCommentRepository = new SCRFIllustrationSubmissionCommentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIllustrationSubmissionCommentRepository.Select().OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIllustrationSubmissionCommentRepository.Select().OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFIllustrationSubmissionCommentRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFIllustrationSubmissionCommentRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIllustrationSubmissionCommentRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIllustrationSubmissionCommentRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIllustrationSubmissionCommentRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfillustrationSubmissionComments> GetSCRFIllustrationSubmissionComment(XsiScrfillustrationSubmissionComments entity)
        {
            using (SCRFIllustrationSubmissionCommentRepository = new SCRFIllustrationSubmissionCommentRepository(ConnectionString))
            {
                return SCRFIllustrationSubmissionCommentRepository.Select(entity);
            }
        }
        public List<XsiScrfillustrationSubmissionComments> GetSCRFIllustrationSubmissionComment(XsiScrfillustrationSubmissionComments entity, EnumSortlistBy sortListBy)
        {
            using (SCRFIllustrationSubmissionCommentRepository = new SCRFIllustrationSubmissionCommentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return SCRFIllustrationSubmissionCommentRepository.Select(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return SCRFIllustrationSubmissionCommentRepository.Select(entity).OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return SCRFIllustrationSubmissionCommentRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return SCRFIllustrationSubmissionCommentRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return SCRFIllustrationSubmissionCommentRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return SCRFIllustrationSubmissionCommentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return SCRFIllustrationSubmissionCommentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertSCRFIllustrationSubmissionComment(XsiScrfillustrationSubmissionComments entity)
        {
            using (SCRFIllustrationSubmissionCommentRepository = new SCRFIllustrationSubmissionCommentRepository(ConnectionString))
            {
                entity = SCRFIllustrationSubmissionCommentRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFIllustrationSubmissionComment(XsiScrfillustrationSubmissionComments entity)
        {
            XsiScrfillustrationSubmissionComments OriginalEntity = GetSCRFIllustrationSubmissionCommentByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (SCRFIllustrationSubmissionCommentRepository = new SCRFIllustrationSubmissionCommentRepository(ConnectionString))
                {

                    SCRFIllustrationSubmissionCommentRepository.Update(entity);
                    if (SCRFIllustrationSubmissionCommentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFIllustrationSubmissionCommentStatus(long itemId)
        {
            XsiScrfillustrationSubmissionComments entity = GetSCRFIllustrationSubmissionCommentByItemId(itemId);
            if (entity != null)
            {
                using (SCRFIllustrationSubmissionCommentRepository = new SCRFIllustrationSubmissionCommentRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    SCRFIllustrationSubmissionCommentRepository.Update(entity);
                    if (SCRFIllustrationSubmissionCommentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFIllustrationSubmissionComment(XsiScrfillustrationSubmissionComments entity)
        {
            List<XsiScrfillustrationSubmissionComments> entities = GetSCRFIllustrationSubmissionComment(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfillustrationSubmissionComments e in entities)
                {
                    result = DeleteSCRFIllustrationSubmissionComment(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFIllustrationSubmissionComment(long itemId)
        {
            XsiScrfillustrationSubmissionComments entity = GetSCRFIllustrationSubmissionCommentByItemId(itemId);
            if (entity != null)
            {
                using (SCRFIllustrationSubmissionCommentRepository = new SCRFIllustrationSubmissionCommentRepository(ConnectionString))
                {
                    SCRFIllustrationSubmissionCommentRepository.Delete(itemId);
                    if (SCRFIllustrationSubmissionCommentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }


        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PageContentBL : IDisposable
    {
        #region Feilds
        IPagesContentRepository XsiPagesContentRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PageContentBL()
        {
            ConnectionString = null;
        }
        public PageContentBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiPagesContentRepository != null)
                this.XsiPagesContentRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiPagesContentNew GetPageContentByItemId(long itemId)
        {
            using (XsiPagesContentRepository = new PagesContentRepository(ConnectionString))
            {
                return XsiPagesContentRepository.GetById(itemId);
            }
        }

        public List<XsiPagesContentNew> GetPageContent()
        {
            using (XsiPagesContentRepository = new PagesContentRepository(ConnectionString))
            {
                return XsiPagesContentRepository.Select();
            }
        }
        public List<XsiPagesContentNew> GetPageContent(EnumSortlistBy sortListBy)
        {
            using (XsiPagesContentRepository = new PagesContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiPagesContentRepository.Select().OrderBy(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiPagesContentRepository.Select().OrderByDescending(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiPagesContentRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiPagesContentRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiPagesContentRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPagesContentNew> GetPageContent(XsiPagesContentNew entity)
        {
            using (XsiPagesContentRepository = new PagesContentRepository(ConnectionString))
            {
                return XsiPagesContentRepository.Select(entity);
            }
        }
        public List<XsiPagesContentNew> GetPageContent(XsiPagesContentNew entity, EnumSortlistBy sortListBy)
        {
            using (XsiPagesContentRepository = new PagesContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiPagesContentRepository.Select(entity).OrderBy(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiPagesContentRepository.Select(entity).OrderByDescending(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiPagesContentRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiPagesContentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiPagesContentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPagesContentNew> GetPageContentOr(XsiPagesContentNew entity, EnumSortlistBy sortListBy)
        {
            using (XsiPagesContentRepository = new PagesContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiPagesContentRepository.SelectOr(entity).OrderBy(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiPagesContentRepository.SelectOr(entity).OrderByDescending(p => p.PageTitle).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiPagesContentRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiPagesContentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiPagesContentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        //public List<sp_FetchSIBFSearchResults_Result> GetSearchContent(string searchString, long languageId)
        //{
        //    using (XsiPagesContentRepository = new PagesContentRepository(ConnectionString))
        //    {
        //        return XsiPagesContentRepository.SelectSearch(searchString, languageId);
        //    }
        //}
        public EnumResultType InsertPageContent(XsiPagesContentNew entity)
        {
            using (XsiPagesContentRepository = new PagesContentRepository(ConnectionString))
            {
                entity = XsiPagesContentRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePageContent(XsiPagesContentNew entity)
        {
            XsiPagesContentNew OriginalEntity = GetPageContentByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (XsiPagesContentRepository = new PagesContentRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line
                    entity.RollbackXml = BusinessMethodFactory.GetRollbackXml(OriginalEntity);

                    XsiPagesContentRepository.Update(entity);
                    if (XsiPagesContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackPageContent(long itemId)
        {
            //if no RollbackXml propery in entity then delete below code and return EnumResultType.Invalid;

            XsiPagesContentNew OriginalEntity = GetPageContentByItemId(itemId);

            if (OriginalEntity == null)
                return EnumResultType.NotFound;
            else if (string.IsNullOrEmpty(OriginalEntity.RollbackXml))
                return EnumResultType.Invalid;
            else
            {

                XsiPagesContentNew NewEntity = (XsiPagesContentNew)BusinessMethodFactory.GetXmlToObject(OriginalEntity.RollbackXml, OriginalEntity.GetType());

                OriginalEntity.RollbackXml = string.Empty;

                NewEntity.ItemId = itemId;
                NewEntity.RollbackXml = BusinessMethodFactory.GetRollbackXml(OriginalEntity);

                using (XsiPagesContentRepository = new PagesContentRepository(ConnectionString))
                {
                    XsiPagesContentRepository.Update(NewEntity);
                    if (XsiPagesContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }

        }
        public EnumResultType UpdatePageContentStatus(long itemId)
        {
            XsiPagesContentNew entity = GetPageContentByItemId(itemId);
            if (entity != null)
            {
                using (XsiPagesContentRepository = new PagesContentRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    XsiPagesContentRepository.Update(entity);
                    if (XsiPagesContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePageContent(XsiPagesContentNew entity)
        {
            if (GetPageContent(entity).Count() > 0)
            {
                using (XsiPagesContentRepository = new PagesContentRepository(ConnectionString))
                {
                    XsiPagesContentRepository.Delete(entity);
                    if (XsiPagesContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePageContent(long itemId)
        {
            if (GetPageContentByItemId(itemId) != null)
            {
                using (XsiPagesContentRepository = new PagesContentRepository(ConnectionString))
                {
                    XsiPagesContentRepository.Delete(itemId);
                    if (XsiPagesContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
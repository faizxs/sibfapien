﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionBookSubjectBL : IDisposable
    {
        #region Feilds
        IExhibitionBookSubjectRepository ExhibitionBookSubjectRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionBookSubjectBL()
        {
            ConnectionString = null;
        }
        public ExhibitionBookSubjectBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionBookSubjectRepository != null)
                this.ExhibitionBookSubjectRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiExhibitionBookSubject GetExhibitionBookSubjectByItemId(long itemId)
        {
            using (ExhibitionBookSubjectRepository = new ExhibitionBookSubjectRepository(ConnectionString))
            {
                return ExhibitionBookSubjectRepository.GetById(itemId);
            }
        }
        
        public List<XsiExhibitionBookSubject> GetExhibitionBookSubject()
        {
            using (ExhibitionBookSubjectRepository = new ExhibitionBookSubjectRepository(ConnectionString))
            {
                return ExhibitionBookSubjectRepository.Select();
            }
        }
        public List<XsiExhibitionBookSubject> GetExhibitionBookSubject(EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookSubjectRepository = new ExhibitionBookSubjectRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookSubjectRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookSubjectRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookSubjectRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookSubjectRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookSubjectRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookSubjectRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBookSubjectRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionBookSubject> GetExhibitionBookSubject(XsiExhibitionBookSubject entity)
        {
            using (ExhibitionBookSubjectRepository = new ExhibitionBookSubjectRepository(ConnectionString))
            {
                return ExhibitionBookSubjectRepository.Select(entity);
            }
        }
        public List<XsiExhibitionBookSubject> GetExhibitionBookSubject(XsiExhibitionBookSubject entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookSubjectRepository = new ExhibitionBookSubjectRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookSubjectRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookSubjectRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookSubjectRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookSubjectRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookSubjectRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookSubjectRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBookSubjectRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public List<XsiExhibitionBookSubject> GetExhibitionBookSubjectOr(XsiExhibitionBookSubject entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookSubjectRepository = new ExhibitionBookSubjectRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookSubjectRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookSubjectRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookSubjectRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookSubjectRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookSubjectRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookSubjectRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBookSubjectRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public List<XsiExhibitionBookSubject> GetOtherLanguageExhibitionBookSubject(XsiExhibitionBookSubject entity)
        {
            using (ExhibitionBookSubjectRepository = new ExhibitionBookSubjectRepository(ConnectionString))
            {
                return ExhibitionBookSubjectRepository.SelectOtherLanguageCategory(entity);
            }
        }
        public List<XsiExhibitionBookSubject> GetCurrentLanguageExhibitionBookSubject(XsiExhibitionBookSubject entity)
        {
            using (ExhibitionBookSubjectRepository = new ExhibitionBookSubjectRepository(ConnectionString))
            {
                return ExhibitionBookSubjectRepository.SelectCurrentLanguageCategory(entity);
            }
        }

        public EnumResultType InsertExhibitionBookSubject(XsiExhibitionBookSubject entity)
        {
            using (ExhibitionBookSubjectRepository = new ExhibitionBookSubjectRepository(ConnectionString))
            {
                entity = ExhibitionBookSubjectRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionBookSubject(XsiExhibitionBookSubject entity)
        {
            XsiExhibitionBookSubject OriginalEntity = GetExhibitionBookSubjectByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionBookSubjectRepository = new ExhibitionBookSubjectRepository(ConnectionString))
                {
                    ExhibitionBookSubjectRepository.Update(entity);
                    if (ExhibitionBookSubjectRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionBookSubjectStatus(long itemId)
        {
            XsiExhibitionBookSubject entity = GetExhibitionBookSubjectByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionBookSubjectRepository = new ExhibitionBookSubjectRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionBookSubjectRepository.Update(entity);
                    if (ExhibitionBookSubjectRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionBookSubject(XsiExhibitionBookSubject entity)
        {
            List<XsiExhibitionBookSubject> entities = GetExhibitionBookSubject(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionBookSubject e in entities)
                {
                    result = DeleteExhibitionBookSubject(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionBookSubject(long itemId)
        {
            XsiExhibitionBookSubject entity = GetExhibitionBookSubjectByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionBookSubjectRepository = new ExhibitionBookSubjectRepository(ConnectionString))
                {
                    ExhibitionBookSubjectRepository.Delete(itemId);
                    if (ExhibitionBookSubjectRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
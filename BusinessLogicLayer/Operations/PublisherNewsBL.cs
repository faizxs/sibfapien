﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PublisherNewsBL : IDisposable
    {
        #region Feilds
        IPublisherNewsRepository PublisherNewsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PublisherNewsBL()
        {
            ConnectionString = null;
        }
        public PublisherNewsBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PublisherNewsRepository != null)
                this.PublisherNewsRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiPublisherNews GetPublisherNewsByItemId(long itemId)
        {
            using (PublisherNewsRepository = new PublisherNewsRepository(ConnectionString))
            {
                return PublisherNewsRepository.GetById(itemId);
            }
        }
         

        public List<XsiPublisherNews> GetPublisherNews()
        {
            using (PublisherNewsRepository = new PublisherNewsRepository(ConnectionString))
            {
                return PublisherNewsRepository.Select();
            }
        }
        public List<XsiPublisherNews> GetPublisherNews(EnumSortlistBy sortListBy)
        {
            using (PublisherNewsRepository = new PublisherNewsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PublisherNewsRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PublisherNewsRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PublisherNewsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PublisherNewsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PublisherNewsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PublisherNewsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PublisherNewsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPublisherNews> GetPublisherNews(XsiPublisherNews entity)
        {
            using (PublisherNewsRepository = new PublisherNewsRepository(ConnectionString))
            {
                return PublisherNewsRepository.Select(entity);
            }
        }
        public List<XsiPublisherNews> GetPublisherNews(XsiPublisherNews entity, EnumSortlistBy sortListBy)
        {
            using (PublisherNewsRepository = new PublisherNewsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PublisherNewsRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PublisherNewsRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PublisherNewsRepository.Select(entity).OrderBy(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PublisherNewsRepository.Select(entity).OrderByDescending(p => p.Dated).ToList().ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PublisherNewsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PublisherNewsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PublisherNewsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPublisherNews> GetCompletePublisherNews(XsiPublisherNews entity, EnumSortlistBy sortListBy,long langId)
        {
            using (PublisherNewsRepository = new PublisherNewsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PublisherNewsRepository.SelectComeplete(entity, langId).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PublisherNewsRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return PublisherNewsRepository.SelectComeplete(entity, langId).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return PublisherNewsRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PublisherNewsRepository.SelectComeplete(entity, langId).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PublisherNewsRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return PublisherNewsRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public XsiPublisherNews GetPublisherNews(long itemId, long categoryId)
        {
            using (PublisherNewsRepository = new PublisherNewsRepository(ConnectionString))
            {
                return PublisherNewsRepository.Select(new XsiPublisherNews() { ItemId = itemId, PublisherNewsCategoryId = categoryId }).FirstOrDefault();
            }
        }

        public EnumResultType InsertPublisherNews(XsiPublisherNews entity)
        {
            using (PublisherNewsRepository = new PublisherNewsRepository(ConnectionString))
            {
                entity = PublisherNewsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePublisherNews(XsiPublisherNews entity)
        {
            XsiPublisherNews OriginalEntity = GetPublisherNewsByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PublisherNewsRepository = new PublisherNewsRepository(ConnectionString))
                {
                    PublisherNewsRepository.Update(entity);
                    if (PublisherNewsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdatePublisherNewsStatus(long itemId)
        {
            XsiPublisherNews entity = GetPublisherNewsByItemId(itemId);
            if (entity != null)
            {
                using (PublisherNewsRepository = new PublisherNewsRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    PublisherNewsRepository.Update(entity);
                    if (PublisherNewsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePublisherNews(XsiPublisherNews entity)
        {
            List<XsiPublisherNews> entities = GetPublisherNews(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiPublisherNews e in entities)
                {
                    result = DeletePublisherNews(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeletePublisherNews(long itemId)
        {
            XsiPublisherNews entity = GetPublisherNewsByItemId(itemId);
            if (entity != null)
            {
                using (PublisherNewsRepository = new PublisherNewsRepository(ConnectionString))
                {
                    PublisherNewsRepository.Delete(itemId);
                    if (PublisherNewsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
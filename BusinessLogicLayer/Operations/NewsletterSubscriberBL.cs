﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class NewsletterSubscriberBL : IDisposable
    {
        #region Feilds
        INewsletterSubscriberRepository NewsletterSubscriberRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public NewsletterSubscriberBL()
        {
            ConnectionString = null;
        }
        public NewsletterSubscriberBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.NewsletterSubscriberRepository != null)
                this.NewsletterSubscriberRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiNewsletterSubscribers GetNewsletterSubscriberByItemId(long itemId)
        {
            using (NewsletterSubscriberRepository = new NewsletterSubscriberRepository(ConnectionString))
            {
                return NewsletterSubscriberRepository.GetById(itemId);
            }
        }
         
        public List<XsiNewsletterSubscribers> GetNewsletterSubscriber()
        {
            using (NewsletterSubscriberRepository = new NewsletterSubscriberRepository(ConnectionString))
            {
                return NewsletterSubscriberRepository.Select();
            }
        }
        public List<XsiNewsletterSubscribers> GetNewsletterSubscriber(EnumSortlistBy sortListBy)
        {
            using (NewsletterSubscriberRepository = new NewsletterSubscriberRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsletterSubscriberRepository.Select().OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsletterSubscriberRepository.Select().OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return NewsletterSubscriberRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return NewsletterSubscriberRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsletterSubscriberRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsletterSubscriberRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsletterSubscriberRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiNewsletterSubscribers> GetNewsletterSubscriber(XsiNewsletterSubscribers entity)
        {
            using (NewsletterSubscriberRepository = new NewsletterSubscriberRepository(ConnectionString))
            {
                return NewsletterSubscriberRepository.Select(entity);
            }
        }
        public List<XsiNewsletterSubscribers> GetNewsletterSubscriber(XsiNewsletterSubscribers entity, EnumSortlistBy sortListBy)
        {
            using (NewsletterSubscriberRepository = new NewsletterSubscriberRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsletterSubscriberRepository.Select(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsletterSubscriberRepository.Select(entity).OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return NewsletterSubscriberRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return NewsletterSubscriberRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsletterSubscriberRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsletterSubscriberRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsletterSubscriberRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiNewsletterSubscribers> GetNewsletterSubscriberOr(XsiNewsletterSubscribers entity, EnumSortlistBy sortListBy)
        {
            using (NewsletterSubscriberRepository = new NewsletterSubscriberRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsletterSubscriberRepository.SelectOr(entity).OrderBy(p => p.Name).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsletterSubscriberRepository.SelectOr(entity).OrderByDescending(p => p.Name).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return NewsletterSubscriberRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return NewsletterSubscriberRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsletterSubscriberRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsletterSubscriberRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsletterSubscriberRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertNewsletterSubscriber(XsiNewsletterSubscribers entity)
        {
            using (NewsletterSubscriberRepository = new NewsletterSubscriberRepository(ConnectionString))
            {
                entity = NewsletterSubscriberRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateNewsletterSubscriber(XsiNewsletterSubscribers entity)
        {
            XsiNewsletterSubscribers OriginalEntity = GetNewsletterSubscriberByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (NewsletterSubscriberRepository = new NewsletterSubscriberRepository(ConnectionString))
                {
                    NewsletterSubscriberRepository.Update(entity);
                    if (NewsletterSubscriberRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateNewsletterSubscriberStatus(long itemId)
        {
            XsiNewsletterSubscribers entity = GetNewsletterSubscriberByItemId(itemId);
            if (entity != null)
            {
                using (NewsletterSubscriberRepository = new NewsletterSubscriberRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    NewsletterSubscriberRepository.Update(entity);
                    if (NewsletterSubscriberRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteNewsletterSubscriber(XsiNewsletterSubscribers entity)
        {
            List<XsiNewsletterSubscribers> entities = GetNewsletterSubscriber(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiNewsletterSubscribers e in entities)
                {
                    result = DeleteNewsletterSubscriber(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteNewsletterSubscriber(long itemId)
        {
            XsiNewsletterSubscribers entity = GetNewsletterSubscriberByItemId(itemId);
            if (entity != null)
            {
                using (NewsletterSubscriberRepository = new NewsletterSubscriberRepository(ConnectionString))
                {
                    NewsletterSubscriberRepository.Delete(itemId);
                    if (NewsletterSubscriberRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
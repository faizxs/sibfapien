﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionOtherEventsStaffGuestBL : IDisposable
    {
        #region Feilds
        IExhibitionOtherEventsStaffGuestRepository ExhibitionOtherEventsStaffGuestRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionOtherEventsStaffGuestBL()
        {
            ConnectionString = null;
        }
        public ExhibitionOtherEventsStaffGuestBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionOtherEventsStaffGuestRepository != null)
                this.ExhibitionOtherEventsStaffGuestRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionOtherEventsStaffGuest GetExhibitionOtherEventsStaffGuestByItemId(long itemId)
        {
            using (ExhibitionOtherEventsStaffGuestRepository = new ExhibitionOtherEventsStaffGuestRepository(ConnectionString))
            {
                return ExhibitionOtherEventsStaffGuestRepository.GetById(itemId);
            }
        }
         
        public List<XsiExhibitionOtherEventsStaffGuest> GetExhibitionOtherEventsStaffGuest()
        {
            using (ExhibitionOtherEventsStaffGuestRepository = new ExhibitionOtherEventsStaffGuestRepository(ConnectionString))
            {
                return ExhibitionOtherEventsStaffGuestRepository.Select();
            }
        }
        public List<XsiExhibitionOtherEventsStaffGuest> GetExhibitionOtherEventsStaffGuest(EnumSortlistBy sortListBy)
        {
            using (ExhibitionOtherEventsStaffGuestRepository = new ExhibitionOtherEventsStaffGuestRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionOtherEventsStaffGuestRepository.Select().OrderBy(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionOtherEventsStaffGuestRepository.Select().OrderByDescending(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionOtherEventsStaffGuestRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionOtherEventsStaffGuestRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionOtherEventsStaffGuestRepository.Select().OrderBy(p => p.StaffGuestId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionOtherEventsStaffGuestRepository.Select().OrderByDescending(p => p.StaffGuestId).ToList();

                    default:
                        return ExhibitionOtherEventsStaffGuestRepository.Select().OrderByDescending(p => p.StaffGuestId).ToList();
                }
            }
        }
        public List<XsiExhibitionOtherEventsStaffGuest> GetExhibitionOtherEventsStaffGuest(XsiExhibitionOtherEventsStaffGuest entity)
        {
            using (ExhibitionOtherEventsStaffGuestRepository = new ExhibitionOtherEventsStaffGuestRepository(ConnectionString))
            {
                return ExhibitionOtherEventsStaffGuestRepository.Select(entity);
            }
        }
        public List<XsiExhibitionOtherEventsStaffGuest> GetExhibitionOtherEventsStaffGuest(XsiExhibitionOtherEventsStaffGuest entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionOtherEventsStaffGuestRepository = new ExhibitionOtherEventsStaffGuestRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionOtherEventsStaffGuestRepository.Select(entity).OrderBy(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionOtherEventsStaffGuestRepository.Select(entity).OrderByDescending(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionOtherEventsStaffGuestRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionOtherEventsStaffGuestRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionOtherEventsStaffGuestRepository.Select(entity).OrderBy(p => p.StaffGuestId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionOtherEventsStaffGuestRepository.Select(entity).OrderByDescending(p => p.StaffGuestId).ToList();

                    default:
                        return ExhibitionOtherEventsStaffGuestRepository.Select(entity).OrderByDescending(p => p.StaffGuestId).ToList();
                }
            }
        }

        public EnumResultType InsertExhibitionOtherEventsStaffGuest(XsiExhibitionOtherEventsStaffGuest entity)
        {
            using (ExhibitionOtherEventsStaffGuestRepository = new ExhibitionOtherEventsStaffGuestRepository(ConnectionString))
            {
                entity = ExhibitionOtherEventsStaffGuestRepository.Add(entity);
                if (entity.StaffGuestId > 0)
                {
                    XsiItemdId = entity.StaffGuestId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuest(XsiExhibitionOtherEventsStaffGuest entity)
        {
            //XsiExhibitionOtherEventsStaffGuest OriginalEntity = GetExhibitionOtherEventsStaffGuestByItemId(entity.StaffGuestId);
            XsiExhibitionOtherEventsStaffGuest OriginalEntity = GetExhibitionOtherEventsStaffGuestByItemId(entity.StaffGuestId);

            if (OriginalEntity != null)
            {
                using (ExhibitionOtherEventsStaffGuestRepository = new ExhibitionOtherEventsStaffGuestRepository(ConnectionString))
                {
                    ExhibitionOtherEventsStaffGuestRepository.Update(entity);
                    if (ExhibitionOtherEventsStaffGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionOtherEventsStaffGuestStatus(long itemId)
        {
            XsiExhibitionOtherEventsStaffGuest entity = GetExhibitionOtherEventsStaffGuestByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionOtherEventsStaffGuestRepository = new ExhibitionOtherEventsStaffGuestRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionOtherEventsStaffGuestRepository.Update(entity);
                    if (ExhibitionOtherEventsStaffGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuest(XsiExhibitionOtherEventsStaffGuest entity)
        {
            List<XsiExhibitionOtherEventsStaffGuest> entities = GetExhibitionOtherEventsStaffGuest(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionOtherEventsStaffGuest e in entities)
                {
                    result = DeleteExhibitionOtherEventsStaffGuest(e.StaffGuestId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionOtherEventsStaffGuest(long itemId)
        {
            XsiExhibitionOtherEventsStaffGuest entity = GetExhibitionOtherEventsStaffGuestByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionOtherEventsStaffGuestRepository = new ExhibitionOtherEventsStaffGuestRepository(ConnectionString))
                {
                    ExhibitionOtherEventsStaffGuestRepository.Delete(itemId);
                    if (ExhibitionOtherEventsStaffGuestRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
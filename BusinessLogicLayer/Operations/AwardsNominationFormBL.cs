﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class AwardsNominationFormBL : IDisposable
    {
        #region Feilds
        IAwardsNominationFormRepository IAwardsNominationFormRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public AwardsNominationFormBL()
        {
            ConnectionString = null;
        }
        public AwardsNominationFormBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IAwardsNominationFormRepository != null)
                this.IAwardsNominationFormRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IAwardsNominationFormRepository = new AwardsNominationFormRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiAwardsNominationForms GetAwardsNominationFormByItemId(long itemId)
        {
            using (IAwardsNominationFormRepository = new AwardsNominationFormRepository(ConnectionString))
            {
                return IAwardsNominationFormRepository.GetById(itemId);
            }
        }
         
        public List<XsiAwardsNominationForms> GetAwardsNominationForm()
        {
            using (IAwardsNominationFormRepository = new AwardsNominationFormRepository(ConnectionString))
            {
                return IAwardsNominationFormRepository.Select();
            }
        }
        public List<XsiAwardsNominationForms> GetAwardsNominationForm(EnumSortlistBy sortListBy)
        {
            using (IAwardsNominationFormRepository = new AwardsNominationFormRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IAwardsNominationFormRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IAwardsNominationFormRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IAwardsNominationFormRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IAwardsNominationFormRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IAwardsNominationFormRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IAwardsNominationFormRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IAwardsNominationFormRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiAwardsNominationForms> GetAwardsNominationForm(XsiAwardsNominationForms entity)
        {
            using (IAwardsNominationFormRepository = new AwardsNominationFormRepository(ConnectionString))
            {
                return IAwardsNominationFormRepository.Select(entity);
            }
        }
        public List<XsiAwardsNominationForms> GetAwardsNominationForm(XsiAwardsNominationForms entity, EnumSortlistBy sortListBy)
        {
            using (IAwardsNominationFormRepository = new AwardsNominationFormRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return IAwardsNominationFormRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return IAwardsNominationFormRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IAwardsNominationFormRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IAwardsNominationFormRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IAwardsNominationFormRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IAwardsNominationFormRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IAwardsNominationFormRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiAwardsNominationForms GetAwardsNominationForm(long groupId, long languageId)
        {
            using (IAwardsNominationFormRepository = new AwardsNominationFormRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertAwardsNominationForm(XsiAwardsNominationForms entity)
        {
            using (IAwardsNominationFormRepository = new AwardsNominationFormRepository(ConnectionString))
            {
                entity = IAwardsNominationFormRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    //  XsiGroupId = entity.GroupId ?? -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateAwardsNominationForm(XsiAwardsNominationForms entity)
        {
            XsiAwardsNominationForms OriginalEntity = GetAwardsNominationFormByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IAwardsNominationFormRepository = new AwardsNominationFormRepository(ConnectionString))
                {
                    IAwardsNominationFormRepository.Update(entity);
                    if (IAwardsNominationFormRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateAwardsNominationFormStatus(long itemId)
        {
            XsiAwardsNominationForms entity = GetAwardsNominationFormByItemId(itemId);
            if (entity != null)
            {
                using (IAwardsNominationFormRepository = new AwardsNominationFormRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IAwardsNominationFormRepository.Update(entity);
                    if (IAwardsNominationFormRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteAwardsNominationForm(XsiAwardsNominationForms entity)
        {
            List<XsiAwardsNominationForms> entities = GetAwardsNominationForm(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiAwardsNominationForms e in entities)
                {
                    result = DeleteAwardsNominationForm(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteAwardsNominationForm(long itemId)
        {
            XsiAwardsNominationForms entity = GetAwardsNominationFormByItemId(itemId);
            if (entity != null)
            {
                using (IAwardsNominationFormRepository = new AwardsNominationFormRepository(ConnectionString))
                {
                    IAwardsNominationFormRepository.Delete(itemId);
                    if (IAwardsNominationFormRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
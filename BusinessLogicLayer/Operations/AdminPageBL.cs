﻿using System;
using System.Collections.Generic;
using System.Text;
using Xsi.Repositories;
using Entities.Models;
using System.Linq;

namespace Xsi.BusinessLogicLayer
{
    public class AdminPageBL : IDisposable
    {
        #region Feilds
        IAdminPageRepository XsiAdminPageRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public AdminPageBL()
        {
            ConnectionString = null;
        }
        public AdminPageBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiAdminPageRepository != null)
                this.XsiAdminPageRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdminPages GetAdminPageByItemId(long itemId)
        {
            using (XsiAdminPageRepository = new AdminPagesRepository())
            {
                return XsiAdminPageRepository.GetById(itemId);
            }
        }

        public List<XsiAdminPages> GetAdminPage(EnumSortlistBy sortListBy)
        {
            using (XsiAdminPageRepository = new AdminPagesRepository())
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiAdminPageRepository.Select().OrderBy(p => p.PageName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiAdminPageRepository.Select().OrderByDescending(p => p.PageName).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiAdminPageRepository.Select().OrderBy(p => p.ItemId).ToList().ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiAdminPageRepository.Select().OrderByDescending(p => p.ItemId).ToList().ToList();

                    default:
                        return XsiAdminPageRepository.Select().OrderByDescending(p => p.ItemId).ToList().ToList();
                }
            }
        }
        public List<XsiAdminPages> GetAdminPage(XsiAdminPages entity)
        {
            using (XsiAdminPageRepository = new AdminPagesRepository())
            {
                return XsiAdminPageRepository.Select(entity);
            }
        }
        public List<XsiAdminPages> GetAdminPageAndPermission(XsiAdminPages entity)
        {
            using (XsiAdminPageRepository = new AdminPagesRepository())
            {
                return XsiAdminPageRepository.SelectAdminPageAndPermissions(entity);
            }
        }

        public EnumResultType InsertAdminPage(XsiAdminPages entity)
        {
            using (XsiAdminPageRepository = new AdminPagesRepository(ConnectionString))
            {
                entity = XsiAdminPageRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateAdminPage(XsiAdminPages entity)
        {
            if (GetAdminPageByItemId(entity.ItemId) != null)
            {
                using (XsiAdminPageRepository = new AdminPagesRepository(ConnectionString))
                {
                    XsiAdminPageRepository.Update(entity);
                    if (XsiAdminPageRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteAdminPage(XsiAdminPages entity)
        {
            EnumResultType result = EnumResultType.NotFound;

            List<XsiAdminPages> AdminPageList = GetAdminPage(entity).ToList();

            if (AdminPageList.Count > 0)
            {
                foreach (XsiAdminPages admPage in AdminPageList)
                {
                    result = DeleteAdminPage(admPage.ItemId);
                }
            }

            return result;
        }
        public EnumResultType DeleteAdminPage(long itemId)
        {
            using (AdminUserPermissionBL AdminUserPermissionBL = new AdminUserPermissionBL(ConnectionString))
            {
                AdminUserPermissionBL.DeletePermission(new XsiAdminPermissions() { PageId = itemId });

                if (AdminUserPermissionBL.GetPermission(new XsiAdminPermissions() { PageId= itemId }).Count() == 0)
                {
                    if (GetAdminPageByItemId(itemId) != null)
                    {
                        using (XsiAdminPageRepository = new AdminPagesRepository(ConnectionString))
                        {
                            XsiAdminPageRepository.Delete(itemId);
                            if (XsiAdminPageRepository.SubmitChanges() > 0)
                                return EnumResultType.Success;
                            else
                                return EnumResultType.Failed;
                        }
                    }
                    else
                        return EnumResultType.NotFound;
                }
                else
                    return EnumResultType.ValueInUse;
            }
        }
        #endregion
    }
}
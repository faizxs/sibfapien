﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class EventDateBL : IDisposable
    {
        #region Feilds
        IEventDateRepository EventDateRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public EventDateBL()
        {
            ConnectionString = null;
        }
        public EventDateBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventDateRepository != null)
                this.EventDateRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (EventDateRepository = new EventDateRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiEventDate GetEventDateByItemId(long itemId)
        {
            using (EventDateRepository = new EventDateRepository(ConnectionString))
            {
                return EventDateRepository.GetById(itemId);
            }
        }
         
        public List<XsiEventDate> GetEventDate()
        {
            using (EventDateRepository = new EventDateRepository(ConnectionString))
            {
                return EventDateRepository.Select();
            }
        }
        public List<XsiEventDate> GetEventDate(EnumSortlistBy sortListBy)
        {
            using (EventDateRepository = new EventDateRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return EventDateRepository.Select().OrderBy(p => p.StartDate).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventDateRepository.Select().OrderByDescending(p => p.StartDate).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventDateRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventDateRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventDateRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEventDate> GetEventDate(XsiEventDate entity)
        {
            using (EventDateRepository = new EventDateRepository(ConnectionString))
            {
                return EventDateRepository.Select(entity);
            }
        }
        public List<XsiEventDate> GetEventDate(XsiEventDate entity, EnumSortlistBy sortListBy)
        {
            using (EventDateRepository = new EventDateRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByDateAsc:
                        return EventDateRepository.Select(entity).OrderBy(p => p.StartDate).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventDateRepository.Select(entity).OrderByDescending(p => p.StartDate).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventDateRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventDateRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventDateRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiEventDate GetEventDate(long groupId, long languageId)
        {
            using (EventDateRepository = new EventDateRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertEventDate(XsiEventDate entity)
        {
            using (EventDateRepository = new EventDateRepository(ConnectionString))
            {
                entity = EventDateRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId =-1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateEventDate(XsiEventDate entity)
        {
            XsiEventDate OriginalEntity = GetEventDateByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (EventDateRepository = new EventDateRepository(ConnectionString))
                {
                    EventDateRepository.Update(entity);
                    if (EventDateRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateEventDateStatus(long itemId)
        {
           
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteEventDate(XsiEventDate entity)
        {
            List<XsiEventDate> entities = GetEventDate(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiEventDate e in entities)
                {
                    result = DeleteEventDate(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteEventDate(long itemId)
        {
            XsiEventDate entity = GetEventDateByItemId(itemId);
            if (entity != null)
            {
                using (EventDateRepository = new EventDateRepository(ConnectionString))
                {
                    EventDateRepository.Delete(itemId);
                    if (EventDateRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class EventBL : IDisposable
    {
        #region Feilds
        IEventRepository EventRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public EventBL()
        {
            ConnectionString = null;
        }
        public EventBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EventRepository != null)
                this.EventRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiEvent GetEventByItemId(long itemId)
        {
            using (EventRepository = new EventRepository(ConnectionString))
            {
                return EventRepository.GetById(itemId);
            }
        }
         
        public List<XsiEvent> GetEvent()
        {
            using (EventRepository = new EventRepository(ConnectionString))
            {
                return EventRepository.Select();
            }
        }
        public List<XsiEvent> GetEvent(EnumSortlistBy sortListBy)
        {
            using (EventRepository = new EventRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return EventRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return EventRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return EventRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEvent> GetEvent(XsiEvent entity)
        {
            using (EventRepository = new EventRepository(ConnectionString))
            {
                return EventRepository.Select(entity);
            }
        }
        public List<XsiEvent> GetEvent(XsiEvent entity, EnumSortlistBy sortListBy)
        {
            using (EventRepository = new EventRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return EventRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return EventRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return EventRepository.Select(entity).OrderBy(p => p.StartDate).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EventRepository.Select(entity).OrderByDescending(p => p.StartDate).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EventRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EventRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EventRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
         
        //public List<GetEvents_Result> GetEvent(long dayVal, long monthVal, long yearVal, string groupidsList, long languageId)
        //{
        //    using (EventRepository = new EventRepository(ConnectionString))
        //    {
        //        return EventRepository.SelectEventsResult(dayVal, monthVal, yearVal, groupidsList, languageId);
        //    }
        //}

        public EnumResultType InsertEvent(XsiEvent entity)
        {
            using (EventRepository = new EventRepository(ConnectionString))
            {
                entity = EventRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                     
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateEvent(XsiEvent entity)
        {
            XsiEvent OriginalEntity = GetEventByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (EventRepository = new EventRepository(ConnectionString))
                {
                    EventRepository.Update(entity);
                    if (EventRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateEventStatus(long itemId)
        {
            XsiEvent entity = GetEventByItemId(itemId);
            if (entity != null)
            {
                using (EventRepository = new EventRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    EventRepository.Update(entity);
                    if (EventRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteEvent(XsiEvent entity)
        {
            List<XsiEvent> entities = GetEvent(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiEvent e in entities)
                {
                    result = DeleteEvent(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteEvent(long itemId)
        {
            XsiEvent entity = GetEventByItemId(itemId);
            if (entity != null)
            {
                using (EventRepository = new EventRepository(ConnectionString))
                {
                    EventRepository.Delete(itemId);
                    if (EventRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        //public EnumResultType UpdatePublishorUnpublishAllByGroupId(long itemId, bool ispublish)
        //{
        //    List<XsiEvent> entityList = GetEventByItemId(itemId);
        //    EnumResultType XsiResult = EnumResultType.NotFound;
        //    XsiEvent entity;
        //    foreach (XsiEvent rows in entityList)
        //    {
        //        entity = GetEventByItemId(rows.ItemId);
        //        if (entity != null)
        //        {
        //            using (EventRepository = new EventRepository(ConnectionString))
        //            {
        //                if (ispublish)
        //                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes);
        //                else
        //                    entity.IsActive = EnumConversion.ToString(EnumBool.No);
        //                EventRepository.Update(entity);
        //                if (EventRepository.SubmitChanges() > 0)
        //                    XsiResult = EnumResultType.Success;
        //                else
        //                    XsiResult = EnumResultType.Failed;
        //            }
        //        }
        //        else
        //            return XsiResult;
        //    }
        //    return XsiResult;
        //}
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionBookSubsubjectBL : IDisposable
    {
        #region Feilds
        IExhibitionBookSubsubjectRepository ExhibitionBookSubsubjectRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionBookSubsubjectBL()
        {
            ConnectionString = null;
        }
        public ExhibitionBookSubsubjectBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionBookSubsubjectRepository != null)
                this.ExhibitionBookSubsubjectRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionBookSubsubject GetExhibitionBookSubsubjectByItemId(long itemId)
        {
            using (ExhibitionBookSubsubjectRepository = new ExhibitionBookSubsubjectRepository(ConnectionString))
            {
                return ExhibitionBookSubsubjectRepository.GetById(itemId);
            }
        }
         
        public List<XsiExhibitionBookSubsubject> GetExhibitionBookSubsubject()
        {
            using (ExhibitionBookSubsubjectRepository = new ExhibitionBookSubsubjectRepository(ConnectionString))
            {
                return ExhibitionBookSubsubjectRepository.Select();
            }
        }
        public List<XsiExhibitionBookSubsubject> GetExhibitionBookSubsubject(EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookSubsubjectRepository = new ExhibitionBookSubsubjectRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookSubsubjectRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookSubsubjectRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookSubsubjectRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookSubsubjectRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookSubsubjectRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookSubsubjectRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBookSubsubjectRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionBookSubsubject> GetExhibitionBookSubsubject(XsiExhibitionBookSubsubject entity)
        {
            using (ExhibitionBookSubsubjectRepository = new ExhibitionBookSubsubjectRepository(ConnectionString))
            {
                return ExhibitionBookSubsubjectRepository.Select(entity);
            }
        }
        public List<XsiExhibitionBookSubsubject> GetExhibitionBookSubsubject(XsiExhibitionBookSubsubject entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookSubsubjectRepository = new ExhibitionBookSubsubjectRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookSubsubjectRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookSubsubjectRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookSubsubjectRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookSubsubjectRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookSubsubjectRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookSubsubjectRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBookSubsubjectRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionBookSubsubject> GetCompleteExhibitionBookSubsubject(XsiExhibitionBookSubsubject entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookSubsubjectRepository = new ExhibitionBookSubsubjectRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookSubsubjectRepository.SelectComeplete(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookSubsubjectRepository.SelectComeplete(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookSubsubjectRepository.SelectComeplete(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookSubsubjectRepository.SelectComeplete(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookSubsubjectRepository.SelectComeplete(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookSubsubjectRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionBookSubsubjectRepository.SelectComeplete(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
         
        public EnumResultType InsertExhibitionBookSubsubject(XsiExhibitionBookSubsubject entity)
        {
            using (ExhibitionBookSubsubjectRepository = new ExhibitionBookSubsubjectRepository(ConnectionString))
            {
                entity = ExhibitionBookSubsubjectRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionBookSubsubject(XsiExhibitionBookSubsubject entity)
        {
            XsiExhibitionBookSubsubject OriginalEntity = GetExhibitionBookSubsubjectByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionBookSubsubjectRepository = new ExhibitionBookSubsubjectRepository(ConnectionString))
                {
                    ExhibitionBookSubsubjectRepository.Update(entity);
                    if (ExhibitionBookSubsubjectRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionBookSubsubjectStatus(long itemId)
        {
            XsiExhibitionBookSubsubject entity = GetExhibitionBookSubsubjectByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionBookSubsubjectRepository = new ExhibitionBookSubsubjectRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionBookSubsubjectRepository.Update(entity);
                    if (ExhibitionBookSubsubjectRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionBookSubsubject(XsiExhibitionBookSubsubject entity)
        {
            List<XsiExhibitionBookSubsubject> entities = GetExhibitionBookSubsubject(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionBookSubsubject e in entities)
                {
                    result = DeleteExhibitionBookSubsubject(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionBookSubsubject(long itemId)
        {
            XsiExhibitionBookSubsubject entity = GetExhibitionBookSubsubjectByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionBookSubsubjectRepository = new ExhibitionBookSubsubjectRepository(ConnectionString))
                {
                    ExhibitionBookSubsubjectRepository.Delete(itemId);
                    if (ExhibitionBookSubsubjectRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
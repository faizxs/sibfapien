﻿using System;
using System.Collections.Generic;
using System.Text;
using Xsi.Repositories;
using Entities.Models;
using System.Linq;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionFurnitureItemOrderDetailsBL : IDisposable
    {
        #region Fields
        IExhibitionFurnitureItemOrderDetailsRepository ExhibitionFurnitureItemOrderDetailsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionFurnitureItemOrderDetailsBL()
        {
            ConnectionString = null;
        }
        public ExhibitionFurnitureItemOrderDetailsBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionFurnitureItemOrderDetailsRepository != null)
                this.ExhibitionFurnitureItemOrderDetailsRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionFurnitureItemOrderDetails GetExhibitionFurnitureItemOrderDetailsByItemId(long itemId)
        {
            using (ExhibitionFurnitureItemOrderDetailsRepository = new ExhibitionFurnitureItemOrderDetailRepository(ConnectionString))
            {
                return ExhibitionFurnitureItemOrderDetailsRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionFurnitureItemOrderDetails> GetExhibitionFurnitureItemOrderDetails()
        {
            using (ExhibitionFurnitureItemOrderDetailsRepository = new ExhibitionFurnitureItemOrderDetailRepository(ConnectionString))
            {
                return ExhibitionFurnitureItemOrderDetailsRepository.Select();
            }
        }
        public List<XsiExhibitionFurnitureItemOrderDetails> GetExhibitionFurnitureItemOrderDetails(EnumSortlistBy sortListBy)
        {
            using (ExhibitionFurnitureItemOrderDetailsRepository = new ExhibitionFurnitureItemOrderDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select().OrderBy(p => p.OrderId).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select().OrderByDescending(p => p.OrderId).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionFurnitureItemOrderDetails> GetExhibitionFurnitureItemOrderDetails(XsiExhibitionFurnitureItemOrderDetails entity)
        {
            using (ExhibitionFurnitureItemOrderDetailsRepository = new ExhibitionFurnitureItemOrderDetailRepository(ConnectionString))
            {
                return ExhibitionFurnitureItemOrderDetailsRepository.Select(entity);
            }
        }
        public List<XsiExhibitionFurnitureItemOrderDetails> GetExhibitionFurnitureItemOrderDetails(XsiExhibitionFurnitureItemOrderDetails entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionFurnitureItemOrderDetailsRepository = new ExhibitionFurnitureItemOrderDetailRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select(entity).OrderBy(p => p.OrderId).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select(entity).OrderByDescending(p => p.OrderId).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionFurnitureItemOrderDetailsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertExhibitionFurnitureItemOrderDetails(XsiExhibitionFurnitureItemOrderDetails entity)
        {
            using (ExhibitionFurnitureItemOrderDetailsRepository = new ExhibitionFurnitureItemOrderDetailRepository(ConnectionString))
            {
                entity = ExhibitionFurnitureItemOrderDetailsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionFurnitureItemOrderDetails(XsiExhibitionFurnitureItemOrderDetails entity)
        {
            XsiExhibitionFurnitureItemOrderDetails OriginalEntity = GetExhibitionFurnitureItemOrderDetailsByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionFurnitureItemOrderDetailsRepository = new ExhibitionFurnitureItemOrderDetailRepository(ConnectionString))
                {
                    ExhibitionFurnitureItemOrderDetailsRepository.Update(entity);
                    if (ExhibitionFurnitureItemOrderDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateExhibitionFurnitureItemOrderDetailsStatus(long itemId, long langId)
        {
            XsiExhibitionFurnitureItemOrderDetails entity = GetExhibitionFurnitureItemOrderDetailsByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionFurnitureItemOrderDetailsRepository = new ExhibitionFurnitureItemOrderDetailRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionFurnitureItemOrderDetailsRepository.Update(entity);
                    if (ExhibitionFurnitureItemOrderDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionFurnitureItemOrderDetails(XsiExhibitionFurnitureItemOrderDetails entity)
        {
            List<XsiExhibitionFurnitureItemOrderDetails> entities = GetExhibitionFurnitureItemOrderDetails(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionFurnitureItemOrderDetails e in entities)
                {
                    result = DeleteExhibitionFurnitureItemOrderDetails(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionFurnitureItemOrderDetails(long itemId)
        {
            XsiExhibitionFurnitureItemOrderDetails entity = GetExhibitionFurnitureItemOrderDetailsByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionFurnitureItemOrderDetailsRepository = new ExhibitionFurnitureItemOrderDetailRepository(ConnectionString))
                {
                    ExhibitionFurnitureItemOrderDetailsRepository.Delete(itemId);
                    if (ExhibitionFurnitureItemOrderDetailsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
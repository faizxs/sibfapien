﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionShipmentBL : IDisposable
    {
        #region Feilds
        IExhibitionShipmentRepository ExhibitionShipmentRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionShipmentBL()
        {
            ConnectionString = null;
        }
        public ExhibitionShipmentBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionShipmentRepository != null)
                this.ExhibitionShipmentRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionShipment GetExhibitionShipmentByItemId(long itemId)
        {
            using (ExhibitionShipmentRepository = new ExhibitionShipmentRepository(ConnectionString))
            {
                return ExhibitionShipmentRepository.GetById(itemId);
            }
        }
        
        public List<XsiExhibitionShipment> GetExhibitionShipment()
        {
            using (ExhibitionShipmentRepository = new ExhibitionShipmentRepository(ConnectionString))
            {
                return ExhibitionShipmentRepository.Select();
            }
        }
        public List<XsiExhibitionShipment> GetExhibitionShipment(EnumSortlistBy sortListBy)
        {
            using (ExhibitionShipmentRepository = new ExhibitionShipmentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionShipmentRepository.Select().OrderBy(p => p.ShipmentCompanyName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionShipmentRepository.Select().OrderByDescending(p => p.ShipmentCompanyName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionShipmentRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionShipmentRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionShipmentRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionShipmentRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionShipmentRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionShipment> GetExhibitionShipment(XsiExhibitionShipment entity)
        {
            using (ExhibitionShipmentRepository = new ExhibitionShipmentRepository(ConnectionString))
            {
                return ExhibitionShipmentRepository.Select(entity);
            }
        }
        public List<XsiExhibitionShipment> GetExhibitionShipment(XsiExhibitionShipment entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionShipmentRepository = new ExhibitionShipmentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionShipmentRepository.Select(entity).OrderBy(p => p.ShipmentCompanyName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionShipmentRepository.Select(entity).OrderByDescending(p => p.ShipmentCompanyName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionShipmentRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionShipmentRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionShipmentRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionShipmentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionShipmentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public EnumResultType InsertExhibitionShipment(XsiExhibitionShipment entity)
        {
            using (ExhibitionShipmentRepository = new ExhibitionShipmentRepository(ConnectionString))
            {
                entity = ExhibitionShipmentRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionShipment(XsiExhibitionShipment entity)
        {
            XsiExhibitionShipment OriginalEntity = GetExhibitionShipmentByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionShipmentRepository = new ExhibitionShipmentRepository(ConnectionString))
                {
                    ExhibitionShipmentRepository.Update(entity);
                    if (ExhibitionShipmentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateExhibitionShipmentStatus(long itemId)
        {
            XsiExhibitionShipment entity = GetExhibitionShipmentByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionShipmentRepository = new ExhibitionShipmentRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionShipmentRepository.Update(entity);
                    if (ExhibitionShipmentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionShipment(XsiExhibitionShipment entity)
        {
            List<XsiExhibitionShipment> entities = GetExhibitionShipment(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionShipment e in entities)
                {
                    result = DeleteExhibitionShipment(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionShipment(long itemId)
        {
            XsiExhibitionShipment entity = GetExhibitionShipmentByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionShipmentRepository = new ExhibitionShipmentRepository(ConnectionString))
                {
                    ExhibitionShipmentRepository.Delete(itemId);
                    if (ExhibitionShipmentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
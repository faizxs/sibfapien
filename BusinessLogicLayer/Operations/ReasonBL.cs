﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ReasonBL : IDisposable
    {
        #region Feilds
        IReasonRepository ReasonRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ReasonBL()
        {
            ConnectionString = null;
        }
        public ReasonBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ReasonRepository != null)
                this.ReasonRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiReason GetReasonByItemId(long itemId)
        {
            using (ReasonRepository = new ReasonRepository(ConnectionString))
            {
                return ReasonRepository.GetById(itemId);
            }
        }
       
        public List<XsiReason> GetReason()
        {
            using (ReasonRepository = new ReasonRepository(ConnectionString))
            {
                return ReasonRepository.Select();
            }
        }
        public List<XsiReason> GetReason(EnumSortlistBy sortListBy)
        {
            using (ReasonRepository = new ReasonRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ReasonRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ReasonRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ReasonRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ReasonRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ReasonRepository.Select().OrderBy(p => p.ReasonId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ReasonRepository.Select().OrderByDescending(p => p.ReasonId).ToList();

                    default:
                        return ReasonRepository.Select().OrderByDescending(p => p.ReasonId).ToList();
                }
            }
        }
        public List<XsiReason> GetReason(XsiReason entity)
        {
            using (ReasonRepository = new ReasonRepository(ConnectionString))
            {
                return ReasonRepository.Select(entity);
            }
        }
        public List<XsiReason> GetReason(XsiReason entity, EnumSortlistBy sortListBy)
        {
            using (ReasonRepository = new ReasonRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ReasonRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ReasonRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ReasonRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ReasonRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ReasonRepository.Select(entity).OrderBy(p => p.ReasonId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ReasonRepository.Select(entity).OrderByDescending(p => p.ReasonId).ToList();

                    default:
                        return ReasonRepository.Select(entity).OrderByDescending(p => p.ReasonId).ToList();
                }
            }
        }
        public List<XsiReason> GetReasonOr(XsiReason entity, EnumSortlistBy sortListBy)
        {
            using (ReasonRepository = new ReasonRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ReasonRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ReasonRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ReasonRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ReasonRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ReasonRepository.SelectOr(entity).OrderBy(p => p.ReasonId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ReasonRepository.SelectOr(entity).OrderByDescending(p => p.ReasonId).ToList();

                    default:
                        return ReasonRepository.SelectOr(entity).OrderByDescending(p => p.ReasonId).ToList();
                }
            }
        }
      
        public EnumResultType InsertReason(XsiReason entity)
        {
            using (ReasonRepository = new ReasonRepository(ConnectionString))
            {
                entity = ReasonRepository.Add(entity);
                if (entity.ReasonId > 0)
                {
                    XsiItemdId = entity.ReasonId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateReason(XsiReason entity)
        {
            XsiReason OriginalEntity = GetReasonByItemId(entity.ReasonId);

            if (OriginalEntity != null)
            {
                using (ReasonRepository = new ReasonRepository(ConnectionString))
                {

                    ReasonRepository.Update(entity);
                    if (ReasonRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
   
        public EnumResultType UpdateReasonStatus(long itemId)
        {
            XsiReason entity = GetReasonByItemId(itemId);
            if (entity != null)
            {
                using (ReasonRepository = new ReasonRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ReasonRepository.Update(entity);
                    if (ReasonRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteReason(XsiReason entity)
        {
            List<XsiReason> entities = GetReason(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiReason e in entities)
                {
                    result = DeleteReason(e.ReasonId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteReason(long itemId)
        {
            XsiReason entity = GetReasonByItemId(itemId);
            if (entity != null)
            {
                using (ReasonRepository = new ReasonRepository(ConnectionString))
                {
                    ReasonRepository.Delete(itemId);
                    if (ReasonRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class AdvertisementBannerBL : IDisposable
    {
        #region Feilds
        IAdvertisementBannerRepository AdvertisementBannerRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public AdvertisementBannerBL()
        {
            ConnectionString = null;
        }
        public AdvertisementBannerBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.AdvertisementBannerRepository != null)
                this.AdvertisementBannerRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiAdvertisementBanner GetAdvertisementBannerByItemId(long itemId)
        {
            using (AdvertisementBannerRepository = new AdvertisementBannerRepository(ConnectionString))
            {
                return AdvertisementBannerRepository.GetById(itemId);
            }
        }

        public List<XsiAdvertisementBanner> GetAdvertisementBanner()
        {
            using (AdvertisementBannerRepository = new AdvertisementBannerRepository(ConnectionString))
            {
                return AdvertisementBannerRepository.Select();
            }
        }
        public List<XsiAdvertisementBanner> GetAdvertisementBanner(EnumSortlistBy sortListBy)
        {
            using (AdvertisementBannerRepository = new AdvertisementBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return AdvertisementBannerRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return AdvertisementBannerRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return AdvertisementBannerRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return AdvertisementBannerRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return AdvertisementBannerRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AdvertisementBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return AdvertisementBannerRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiAdvertisementBanner> GetAdvertisementBanner(XsiAdvertisementBanner entity)
        {
            using (AdvertisementBannerRepository = new AdvertisementBannerRepository(ConnectionString))
            {
                return AdvertisementBannerRepository.Select(entity);
            }
        }
        public List<XsiAdvertisementBanner> GetAdvertisementBanner(XsiAdvertisementBanner entity, EnumSortlistBy sortListBy)
        {
            using (AdvertisementBannerRepository = new AdvertisementBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return AdvertisementBannerRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return AdvertisementBannerRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return AdvertisementBannerRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return AdvertisementBannerRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return AdvertisementBannerRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AdvertisementBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return AdvertisementBannerRepository.Select(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return AdvertisementBannerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiAdvertisementBanner> GetAdvertisementBannerOr(XsiAdvertisementBanner entity, EnumSortlistBy sortListBy)
        {
            using (AdvertisementBannerRepository = new AdvertisementBannerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return AdvertisementBannerRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return AdvertisementBannerRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return AdvertisementBannerRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return AdvertisementBannerRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return AdvertisementBannerRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return AdvertisementBannerRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return AdvertisementBannerRepository.SelectOr(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return AdvertisementBannerRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertAdvertisementBanner(XsiAdvertisementBanner entity)
        {
            using (AdvertisementBannerRepository = new AdvertisementBannerRepository(ConnectionString))
            {
                entity = AdvertisementBannerRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateAdvertisementBanner(XsiAdvertisementBanner entity)
        {
            XsiAdvertisementBanner OriginalEntity = GetAdvertisementBannerByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (AdvertisementBannerRepository = new AdvertisementBannerRepository(ConnectionString))
                {
                    AdvertisementBannerRepository.Update(entity);
                    if (AdvertisementBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackAdvertisementBanner(long itemId)
        {
            //if no RollbackXml propery in AdvertisementBanner then delete below code and return EnumResultType.Invalid;
                return EnumResultType.Invalid;
        }
        public EnumResultType UpdateAdvertisementBannerStatus(long itemId)
        {
            XsiAdvertisementBanner entity = GetAdvertisementBannerByItemId(itemId);
            if (entity != null)
            {
                using (AdvertisementBannerRepository = new AdvertisementBannerRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    AdvertisementBannerRepository.Update(entity);
                    if (AdvertisementBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteAdvertisementBanner(XsiAdvertisementBanner entity)
        {
            List<XsiAdvertisementBanner> entities = GetAdvertisementBanner(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiAdvertisementBanner e in entities)
                {
                    result = DeleteAdvertisementBanner(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteAdvertisementBanner(long itemId)
        {
            XsiAdvertisementBanner entity = GetAdvertisementBannerByItemId(itemId);
            if (entity != null)
            {
                using (AdvertisementBannerRepository = new AdvertisementBannerRepository(ConnectionString))
                {
                    AdvertisementBannerRepository.Delete(itemId);
                    if (AdvertisementBannerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateAdvertisementBannerSortOrder(long groupId, long order)
        {
            EnumResultType result = EnumResultType.NotFound;

            /*List<XsiAdvertisementBanner> entities = GetAdvertisementBannerByGroupId(groupId).ToList();
            if (entities != null)
            {
                using (AdvertisementBannerRepository = new AdvertisementBannerRepository(ConnectionString))
                {
                    foreach (XsiAdvertisementBanner entity in entities)
                    {
                        entity.SortOrder = order;
                        AdvertisementBannerRepository.Update(entity);
                        if (AdvertisementBannerRepository.SubmitChanges() > 0)
                            result = EnumResultType.Success;
                        else
                            result = EnumResultType.Failed;
                    }
                }
            }
            */
            return result;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitorRegistrationBL : IDisposable
    {
        #region Feilds
        IExhibitorRegistrationRepository ExhibitorRegistrationRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitorRegistrationBL()
        {
            ConnectionString = null;
        }
        public ExhibitorRegistrationBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitorRegistrationRepository != null)
                this.ExhibitorRegistrationRepository.Dispose();
        }
        #endregion
        #region Methods
        public List<GetExhibitorsAndCountriesSCRF_Result> GetExhibitorsForAutoComplete(string keyword, long langId, long exhibitionId)
        {
            using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
            {
                return ExhibitorRegistrationRepository.GetExhibitorsForAutoComplete(keyword, langId, exhibitionId);
            }
        }
        public XsiExhibitionMemberApplicationYearly GetExhibitorRegistrationByItemId(long MemberExhibitionYearlyId)
        {
            using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
            {
                return ExhibitorRegistrationRepository.GetById(MemberExhibitionYearlyId);
            }
        }
        public XsiExhibitionMemberApplicationYearly GetExhibitorRegistrationByItemIdAll(long MemberExhibitionYearlyId)
        {
            using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
            {
                return ExhibitorRegistrationRepository.GetByIdAll(MemberExhibitionYearlyId);
            }
        }

        public List<XsiExhibitionMemberApplicationYearly> GetExhibitorRegistration()
        {
            using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
            {
                return ExhibitorRegistrationRepository.Select();
            }
        }
        public List<XsiExhibitionMemberApplicationYearly> GetExhibitorRegistration(EnumSortlistBy sortListBy)
        {
            using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitorRegistrationRepository.Select().OrderBy(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitorRegistrationRepository.Select().OrderByDescending(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitorRegistrationRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitorRegistrationRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitorRegistrationRepository.Select().OrderBy(p => p.MemberExhibitionYearlyId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitorRegistrationRepository.Select().OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();

                    default:
                        return ExhibitorRegistrationRepository.Select().OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();
                }
            }
        }
        public List<XsiExhibitionMemberApplicationYearly> GetExhibitorRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
            {
                return ExhibitorRegistrationRepository.Select(entity);
            }
        }
        public List<XsiExhibitionMemberApplicationYearly> GetExhibitorRegistration(XsiExhibitionMemberApplicationYearly entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitorRegistrationRepository.Select(entity).OrderBy(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitorRegistrationRepository.Select(entity).OrderByDescending(p => p.PublisherNameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitorRegistrationRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitorRegistrationRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitorRegistrationRepository.Select(entity).OrderBy(p => p.MemberExhibitionYearlyId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitorRegistrationRepository.Select(entity).OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();

                    default:
                        return ExhibitorRegistrationRepository.Select(entity).OrderByDescending(p => p.MemberExhibitionYearlyId).ToList();
                }
            }
        }
        public List<string> GetExhibitorRegistration(XsiExhibitionMemberApplicationYearly entity, List<long> exhibitionIDs)
        {
            using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
            {
                return ExhibitorRegistrationRepository.Select(entity, exhibitionIDs);
            }
        }
        public List<string> GetExhibitorRegistrationAr(XsiExhibitionMemberApplicationYearly entity, List<long> exhibitionIDs)
        {
            using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
            {
                return ExhibitorRegistrationRepository.SelectAr(entity, exhibitionIDs);
            }
        }
        //public List<GetExhibitors_Agencies_Result> GetExhibitorAndAgency(string whereExhibitor, string whereAgency, string languageId)
        //{
        //    ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString);
        //    return ExhibitorRegistrationRepository.SelectExhibitorAgency(whereExhibitor, whereAgency, languageId);
        //}
        //public List<GetExhibitors_Agencies_Result> GetExhibitorAndAgencyAnd(string whereExhibitor, string whereAgency, GetExhibitors_Agencies_Result entity, List<long?> categoryIDs, string languageId)
        //{
        //    using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
        //    {
        //        return ExhibitorRegistrationRepository.SelectExhibitorAgencyAnd(whereExhibitor, whereAgency, entity, categoryIDs, languageId);
        //    }
        //}
        //public List<GetExhibitors_Agencies_Result> GetExhibitorAndAgencyOr(string whereExhibitor, string whereAgency, GetExhibitors_Agencies_Result entity, List<long?> categoryIDs, string languageId)
        //{
        //    ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString);
        //    return ExhibitorRegistrationRepository.SelectExhibitorAgencyOr(whereExhibitor, whereAgency, entity, categoryIDs, languageId);
        //}
        //public List<GetExhibitors_AgenciesToExport_Result> GetExhibitorAndAgencyToExport(string exhibitorIds, string agencyIds, string languageId)
        //{
        //    using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
        //    {
        //        return ExhibitorRegistrationRepository.SelectExhibitorAgencyToExport(exhibitorIds, agencyIds, languageId);
        //    }
        //}
        //public List<GetExhibitorsAndCountries_Result> GetExhibitorsAndCountries()
        //{
        //    ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString);
        //    return ExhibitorRegistrationRepository.SelectExhibitorsAndCountries();
        //}
        //public List<GetExhibitorsAndCountriesSCRF_Result> GetExhibitorsAndCountriesSCRF()
        //{
        //    using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
        //    {
        //        return ExhibitorRegistrationRepository.SelectExhibitorsAndCountriesSCRF();
        //    }
        //}
        //public long? GetRequestedAreaSum()
        //{
        //    using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
        //    {
        //        return ExhibitorRegistrationRepository.SelectRequestedAreaSum();
        //    }
        //}
        //public List<GetExhibitorRegistrationList_Result> GetExhibitorList(string languageId)
        //{
        //    using (ExhibitorRegistrationRepository ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
        //    {
        //        return ExhibitorRegistrationRepository.SelectExhibitorList(languageId);
        //    }
        //}

        public EnumResultType InsertExhibitorRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
            {
                XsiExhibitionMemberApplicationYearly where = new XsiExhibitionMemberApplicationYearly();
                where.MemberId = entity.MemberId;
                where.ExhibitionId = entity.ExhibitionId;
                where.IsActive = EnumConversion.ToString(EnumBool.Yes);

                if (ExhibitorRegistrationRepository.Select(where).Count() == 0)
                {
                    entity = ExhibitorRegistrationRepository.Add(entity);
                    if (entity.MemberExhibitionYearlyId > 0)
                    {
                        XsiItemdId = entity.MemberExhibitionYearlyId;
                        XsiGroupId = -1;
                        return EnumResultType.Success;
                    }
                    else
                    {
                        XsiItemdId = -1;
                        XsiGroupId = -1;
                        return EnumResultType.Failed;
                    }
                }
                else
                    return EnumResultType.AlreadyExists;
            }
        }
        public EnumResultType UpdateExhibitorRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            XsiExhibitionMemberApplicationYearly OriginalEntity = GetExhibitorRegistrationByItemId(entity.MemberExhibitionYearlyId);

            if (OriginalEntity != null)
            {
                using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
                {
                    ExhibitorRegistrationRepository.Update(entity);
                    if (ExhibitorRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
      
        public EnumResultType UpdateExhibitorRegistrationStatus(long MemberExhibitionYearlyId)
        {
            XsiExhibitionMemberApplicationYearly entity = GetExhibitorRegistrationByItemId(MemberExhibitionYearlyId);
            if (entity != null)
            {
                using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitorRegistrationRepository.Update(entity);
                    if (ExhibitorRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitorRegistration(XsiExhibitionMemberApplicationYearly entity)
        {
            List<XsiExhibitionMemberApplicationYearly> entities = GetExhibitorRegistration(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionMemberApplicationYearly e in entities)
                {
                    result = DeleteExhibitorRegistration(e.MemberExhibitionYearlyId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitorRegistration(long MemberExhibitionYearlyId)
        {
            XsiExhibitionMemberApplicationYearly entity = GetExhibitorRegistrationByItemId(MemberExhibitionYearlyId);
            if (entity != null)
            {
                using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
                {
                    ExhibitorRegistrationRepository.Delete(MemberExhibitionYearlyId);
                    if (ExhibitorRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public XsiExhibitionExhibitorDetails GetDetailsById(long MemberExhibitionYearlyId)
        {
            using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
            {
                return ExhibitorRegistrationRepository.GetDetailsById(MemberExhibitionYearlyId);
            }
        }
        public EnumResultType InsertExhibitorDetailRegistration(XsiExhibitionExhibitorDetails entity)
        {
            using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
            {
                if (ExhibitorRegistrationRepository.GetDetailsById(entity.MemberExhibitionYearlyId.Value) == null)
                {
                    entity = ExhibitorRegistrationRepository.Add(entity);
                    if (entity.MemberExhibitionYearlyId > 0)
                    {
                        XsiItemdId = entity.ExhibitorDetailsId;
                        return EnumResultType.Success;
                    }
                    else
                    {
                        XsiItemdId = -1;
                        XsiGroupId = -1;
                        return EnumResultType.Failed;
                    }
                }
                else
                    return EnumResultType.AlreadyExists;
            }
        }
        public EnumResultType UpdateExhibitorRegistrationDetails(XsiExhibitionExhibitorDetails entity)
        {
            XsiExhibitionExhibitorDetails OriginalEntity = GetDetailsById(entity.MemberExhibitionYearlyId.Value);

            if (OriginalEntity != null)
            {
                using (ExhibitorRegistrationRepository = new ExhibitorRegistrationRepository(ConnectionString))
                {
                    ExhibitorRegistrationRepository.Update(entity);
                    if (ExhibitorRegistrationRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionCurrencyBL : IDisposable
    {
        #region Feilds
        IExhibitionCurrencyRepository ExhibitionCurrencyRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionCurrencyBL()
        {
            ConnectionString = null;
        }
        public ExhibitionCurrencyBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionCurrencyRepository != null)
                this.ExhibitionCurrencyRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiExhibitionCurrency GetExhibitionCurrencyByItemId(long itemId)
        {
            using (ExhibitionCurrencyRepository = new ExhibitionCurrencyRepository(ConnectionString))
            {
                return ExhibitionCurrencyRepository.GetById(itemId);
            }
        }
     
        public List<XsiExhibitionCurrency> GetExhibitionCurrency()
        {
            using (ExhibitionCurrencyRepository = new ExhibitionCurrencyRepository(ConnectionString))
            {
                return ExhibitionCurrencyRepository.Select();
            }
        }
        public List<XsiExhibitionCurrency> GetExhibitionCurrency(EnumSortlistBy sortListBy)
        {
            using (ExhibitionCurrencyRepository = new ExhibitionCurrencyRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCurrencyRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCurrencyRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCurrencyRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCurrencyRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCurrencyRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCurrencyRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionCurrencyRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionCurrency> GetExhibitionCurrency(XsiExhibitionCurrency entity)
        {
            using (ExhibitionCurrencyRepository = new ExhibitionCurrencyRepository(ConnectionString))
            {
                return ExhibitionCurrencyRepository.Select(entity);
            }
        }
        public List<XsiExhibitionCurrency> GetExhibitionCurrency(XsiExhibitionCurrency entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionCurrencyRepository = new ExhibitionCurrencyRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCurrencyRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCurrencyRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCurrencyRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCurrencyRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCurrencyRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCurrencyRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionCurrencyRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionCurrency> GetExhibitionCurrencyOr(XsiExhibitionCurrency entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionCurrencyRepository = new ExhibitionCurrencyRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionCurrencyRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionCurrencyRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionCurrencyRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionCurrencyRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionCurrencyRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionCurrencyRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionCurrencyRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
     
        public EnumResultType InsertExhibitionCurrency(XsiExhibitionCurrency entity)
        {
            using (ExhibitionCurrencyRepository = new ExhibitionCurrencyRepository(ConnectionString))
            {
                entity = ExhibitionCurrencyRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;                    
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionCurrency(XsiExhibitionCurrency entity)
        {
            XsiExhibitionCurrency OriginalEntity = GetExhibitionCurrencyByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionCurrencyRepository = new ExhibitionCurrencyRepository(ConnectionString))
                {

                    ExhibitionCurrencyRepository.Update(entity);
                    if (ExhibitionCurrencyRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionCurrencyStatus(long itemId)
        {
            XsiExhibitionCurrency entity = GetExhibitionCurrencyByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionCurrencyRepository = new ExhibitionCurrencyRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionCurrencyRepository.Update(entity);
                    if (ExhibitionCurrencyRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionCurrency(XsiExhibitionCurrency entity)
        {
            List<XsiExhibitionCurrency> entities = GetExhibitionCurrency(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionCurrency e in entities)
                {
                    result = DeleteExhibitionCurrency(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionCurrency(long itemId)
        {
            XsiExhibitionCurrency entity = GetExhibitionCurrencyByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionCurrencyRepository = new ExhibitionCurrencyRepository(ConnectionString))
                {
                    ExhibitionCurrencyRepository.Delete(itemId);
                    if (ExhibitionCurrencyRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
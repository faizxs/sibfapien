﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class HomepageSectionFourBL : IDisposable
    {
        #region Feilds
        IHomepageSectionFourRepository HomepageSectionFourRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public HomepageSectionFourBL()
        {
            ConnectionString = null;
        }
        public HomepageSectionFourBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.HomepageSectionFourRepository != null)
                this.HomepageSectionFourRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiHomepageSectionFour GetHomepageSectionFourByItemId(long itemId)
        {
            using (HomepageSectionFourRepository = new HomepageSectionFourRepository(ConnectionString))
            {
                return HomepageSectionFourRepository.GetById(itemId);
            }
        }
        public List<XsiHomepageSectionFour> GetHomepageSectionFour()
        {
            using (HomepageSectionFourRepository = new HomepageSectionFourRepository(ConnectionString))
            {
                return HomepageSectionFourRepository.Select();
            }
        }
        public List<XsiHomepageSectionFour> GetHomepageSectionFour(EnumSortlistBy sortListBy)
        {
            using (HomepageSectionFourRepository = new HomepageSectionFourRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageSectionFourRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageSectionFourRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HomepageSectionFourRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HomepageSectionFourRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageSectionFourRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageSectionFourRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HomepageSectionFourRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiHomepageSectionFour> GetHomepageSectionFour(XsiHomepageSectionFour entity)
        {
            using (HomepageSectionFourRepository = new HomepageSectionFourRepository(ConnectionString))
            {
                return HomepageSectionFourRepository.Select(entity);
            }
        }
        public List<XsiHomepageSectionFour> GetHomepageSectionFour(XsiHomepageSectionFour entity, EnumSortlistBy sortListBy)
        {
            using (HomepageSectionFourRepository = new HomepageSectionFourRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageSectionFourRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageSectionFourRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return HomepageSectionFourRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return HomepageSectionFourRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageSectionFourRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageSectionFourRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HomepageSectionFourRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiHomepageSectionFour> GetCompleteHomepageSectionFour(XsiHomepageSectionFour entity, EnumSortlistBy sortListBy,long langId)
        {
            using (HomepageSectionFourRepository = new HomepageSectionFourRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return HomepageSectionFourRepository.SelectComeplete(entity, langId).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return HomepageSectionFourRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return HomepageSectionFourRepository.SelectComeplete(entity, langId).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return HomepageSectionFourRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return HomepageSectionFourRepository.SelectComeplete(entity, langId).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public XsiHomepageSectionFour GetHomepageSectionFour(long Id)
        {
            using (HomepageSectionFourRepository = new HomepageSectionFourRepository(ConnectionString))
            {
                return HomepageSectionFourRepository.Select(new XsiHomepageSectionFour() { ItemId = Id }).FirstOrDefault();
            }
        }

        public EnumResultType InsertHomepageSectionFour(XsiHomepageSectionFour entity)
        {
            using (HomepageSectionFourRepository = new HomepageSectionFourRepository(ConnectionString))
            {
                entity = HomepageSectionFourRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateHomepageSectionFour(XsiHomepageSectionFour entity)
        {
            XsiHomepageSectionFour OriginalEntity = GetHomepageSectionFourByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (HomepageSectionFourRepository = new HomepageSectionFourRepository(ConnectionString))
                {
                    HomepageSectionFourRepository.Update(entity);
                    if (HomepageSectionFourRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateHomepageSectionFourStatus(long itemId)
        {
            XsiHomepageSectionFour entity = GetHomepageSectionFourByItemId(itemId);
            if (entity != null)
            {
                using (HomepageSectionFourRepository = new HomepageSectionFourRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    HomepageSectionFourRepository.Update(entity);
                    if (HomepageSectionFourRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteHomepageSectionFour(XsiHomepageSectionFour entity)
        {
            List<XsiHomepageSectionFour> entities = GetHomepageSectionFour(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiHomepageSectionFour e in entities)
                {
                    result = DeleteHomepageSectionFour(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteHomepageSectionFour(long itemId)
        {
            XsiHomepageSectionFour entity = GetHomepageSectionFourByItemId(itemId);
            if (entity != null)
            {
                using (HomepageSectionFourRepository = new HomepageSectionFourRepository(ConnectionString))
                {
                    HomepageSectionFourRepository.Delete(itemId);
                    if (HomepageSectionFourRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
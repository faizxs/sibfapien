﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class PageMenuNewBL : IDisposable
    {
        #region Feilds
        IPageMenuNewRepository PageMenuNewRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public PageMenuNewBL()
        {
            ConnectionString = null;
        }
        public PageMenuNewBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.PageMenuNewRepository != null)
                this.PageMenuNewRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiPageMenuNew GetPageMenuNewByItemId(long itemId)
        {
            using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
            {
                return PageMenuNewRepository.GetById(itemId);
            }
        }

        public List<XsiPageMenuNew> GetPageMenuNew()
        {
            using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
            {
                return PageMenuNewRepository.Select();
            }
        }
        public List<XsiPageMenuNew> GetPageMenuNew(EnumSortlistBy sortListBy)
        {
            using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PageMenuNewRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PageMenuNewRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PageMenuNewRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PageMenuNewRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return PageMenuNewRepository.Select().OrderBy(p => p.SortOrder).ToList();

                    default:
                        return PageMenuNewRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPageMenuNew> GetPageMenuNew(XsiPageMenuNew entity)
        {
            using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
            {
                return PageMenuNewRepository.Select(entity);
            }
        }

        public List<XsiPageMenuNew> GetPageMenuNewOr(XsiPageMenuNew entity, EnumSortlistBy sortListBy)
        {
            using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PageMenuNewRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PageMenuNewRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PageMenuNewRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PageMenuNewRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return PageMenuNewRepository.SelectOr(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return PageMenuNewRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiPageMenuNew> GetPageMenuNew(XsiPageMenuNew entity, EnumSortlistBy sortListBy)
        {
            using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return PageMenuNewRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return PageMenuNewRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return PageMenuNewRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return PageMenuNewRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemOrder:
                        return PageMenuNewRepository.Select(entity).OrderBy(p => p.SortOrder).ToList();

                    default:
                        return PageMenuNewRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        //public List<XsiPageMenuNew> GetParents(long groupId, long languageId)
        public List<XsiPageMenuNew> GetParents(long itemId)
        {
            List<XsiPageMenuNew> ParentsMenuList = new List<XsiPageMenuNew>();
            using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
            {
                XsiPageMenuNew entity = new XsiPageMenuNew();
                //long gid = groupId;
                do
                {
                    entity = GetPageMenuNewByItemId(itemId);
                    if (entity != null)
                    {
                        ParentsMenuList.Add(entity);
                        itemId = entity.ParentId ?? default(long);
                    }
                    else
                    {
                        itemId = -1;
                    }
                }
                while (itemId != default(long) && itemId != -1);
                return ParentsMenuList.OrderBy(p => p.ParentId).ToList();
            }
        }
        public EnumResultType InsertPageMenuNew(XsiPageMenuNew entity)
        {
            using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
            {
                entity = PageMenuNewRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdatePageMenuNew(XsiPageMenuNew entity)
        {
            XsiPageMenuNew OriginalEntity = GetPageMenuNewByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
                {
                    PageMenuNewRepository.Update(entity);
                    if (PageMenuNewRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdatePageMenuNewStatus(long itemId)
        {
            XsiPageMenuNew entity = GetPageMenuNewByItemId(itemId);
            if (entity != null)
            {
                using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    PageMenuNewRepository.Update(entity);
                    if (PageMenuNewRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdatePageMenuNewSortOrder(long itemId, long order)
        {
            EnumResultType result = EnumResultType.NotFound;
            XsiPageMenuNew entity = GetPageMenuNewByItemId(itemId);
            if (entity != null)
            {
                using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
                {
                    entity.SortOrder = order;
                    PageMenuNewRepository.Update(entity);
                    if (PageMenuNewRepository.SubmitChanges() > 0)
                        result = EnumResultType.Success;
                    else
                        result = EnumResultType.Failed;
                }
            }
            //List<XsiPageMenuNew> entities = GetPageMenuNewByItemId(itemId).ToList();
            //if (entities != null)
            //{
            //    using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
            //    {
            //        foreach (XsiPageMenuNew entity in entities)
            //        {
            //            entity.SortOrder = order;
            //            PageMenuNewRepository.Update(entity);
            //            if (PageMenuNewRepository.SubmitChanges() > 0)
            //                result = EnumResultType.Success;
            //            else
            //                result = EnumResultType.Failed;
            //        }
            //    }
            //}

            return result;
        }
        public EnumResultType DeletePageMenuNew(XsiPageMenuNew entity)
        {
            if (GetPageMenuNew(entity).Count() > 0)
            {
                using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
                {
                    PageMenuNewRepository.Delete(entity);
                    if (PageMenuNewRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeletePageMenuNew(long itemId)
        {
            if (GetPageMenuNewByItemId(itemId) != null)
            {
                using (PageMenuNewRepository = new PageMenuNewRepository(ConnectionString))
                {
                    PageMenuNewRepository.Delete(itemId);
                    if (PageMenuNewRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
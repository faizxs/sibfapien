﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class EmailContentBL : IDisposable
    {
        #region Feilds
        IEmailContentRepository EmailContentRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public EmailContentBL()
        {
            ConnectionString = null;
        }
        public EmailContentBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EmailContentRepository != null)
                this.EmailContentRepository.Dispose();
        }
        #endregion
        #region Methods
       
        public XsiEmailContent GetEmailContentByItemId(long itemId)
        {
            using (EmailContentRepository = new EmailContentRepository(ConnectionString))
            {
                return EmailContentRepository.GetById(itemId);
            }
        }
       
        public List<XsiEmailContent> GetEmailContent()
        {
            using (EmailContentRepository = new EmailContentRepository(ConnectionString))
            {
                return EmailContentRepository.Select();
            }
        }
        public List<XsiEmailContent> GetEmailContent(EnumSortlistBy sortListBy)
        {
            using (EmailContentRepository = new EmailContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return EmailContentRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return EmailContentRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EmailContentRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EmailContentRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EmailContentRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEmailContent> GetEmailContent(XsiEmailContent entity)
        {
            using (EmailContentRepository = new EmailContentRepository(ConnectionString))
            {
                return EmailContentRepository.Select(entity);
            }
        }
        public List<XsiEmailContent> GetEmailContent(XsiEmailContent entity, EnumSortlistBy sortListBy)
        {
            using (EmailContentRepository = new EmailContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return EmailContentRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return EmailContentRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EmailContentRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EmailContentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EmailContentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEmailContent> GetEmailContentOr(XsiEmailContent entity, EnumSortlistBy sortListBy)
        {
            using (EmailContentRepository = new EmailContentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return EmailContentRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return EmailContentRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EmailContentRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EmailContentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EmailContentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public EnumResultType InsertEmailContent(XsiEmailContent entity)
        {
            using (EmailContentRepository = new EmailContentRepository(ConnectionString))
            {
                entity = EmailContentRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateEmailContent(XsiEmailContent entity)
        {
            XsiEmailContent OriginalEntity = GetEmailContentByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (EmailContentRepository = new EmailContentRepository(ConnectionString))
                {
                    entity.RollbackXml = BusinessMethodFactory.GetRollbackXml(OriginalEntity);

                    EmailContentRepository.Update(entity);
                    if (EmailContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType RollbackEmailContent(long itemId)
        {
            XsiEmailContent OriginalEntity = GetEmailContentByItemId(itemId);

            if (OriginalEntity == null)
                return EnumResultType.NotFound;
            else if (string.IsNullOrEmpty(OriginalEntity.RollbackXml))
                return EnumResultType.Invalid;
            else
            {
                XsiEmailContent NewEntity = (XsiEmailContent)BusinessMethodFactory.GetXmlToObject(OriginalEntity.RollbackXml, OriginalEntity.GetType());

                OriginalEntity.RollbackXml = string.Empty;

                NewEntity.ItemId = itemId;
                NewEntity.RollbackXml = BusinessMethodFactory.GetRollbackXml(OriginalEntity);

                using (EmailContentRepository = new EmailContentRepository(ConnectionString))
                {
                    EmailContentRepository.Update(NewEntity);
                    if (EmailContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }

        }
        public EnumResultType DeleteEmailContent(XsiEmailContent entity)
        {
            List<XsiEmailContent> entities = GetEmailContent(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiEmailContent e in entities)
                {
                    result = DeleteEmailContent(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteEmailContent(long itemId)
        {
            XsiEmailContent entity = GetEmailContentByItemId(itemId);
            if (entity != null)
            {
                using (EmailContentRepository = new EmailContentRepository(ConnectionString))
                {
                    EmailContentRepository.Delete(itemId);
                    if (EmailContentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
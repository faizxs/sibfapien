﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class SCRFIllustrationNotificationStatusBL : IDisposable
    {
        #region Feilds
        ISCRFIllustrationNotificationStatusRepository ISCRFIllustrationNotificationStatusRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public SCRFIllustrationNotificationStatusBL()
        {
            ConnectionString = null;
        }
        public SCRFIllustrationNotificationStatusBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ISCRFIllustrationNotificationStatusRepository != null)
                this.ISCRFIllustrationNotificationStatusRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            return -1;
        }

        public XsiScrfillustrationNotificationStatus GetSCRFIllustrationNotificationStatusByItemId(long itemId)
        {
            using (ISCRFIllustrationNotificationStatusRepository = new SCRFIllustrationNotificationStatusRepository(ConnectionString))
            {
                return ISCRFIllustrationNotificationStatusRepository.GetById(itemId);
            }
        }

        public List<XsiScrfillustrationNotificationStatus> GetSCRFIllustrationNotificationStatus()
        {
            using (ISCRFIllustrationNotificationStatusRepository = new SCRFIllustrationNotificationStatusRepository(ConnectionString))
            {
                return ISCRFIllustrationNotificationStatusRepository.Select();
            }
        }
        public List<XsiScrfillustrationNotificationStatus> GetSCRFIllustrationNotificationStatus(EnumSortlistBy sortListBy)
        {
            using (ISCRFIllustrationNotificationStatusRepository = new SCRFIllustrationNotificationStatusRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return ISCRFIllustrationNotificationStatusRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ISCRFIllustrationNotificationStatusRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ISCRFIllustrationNotificationStatusRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiScrfillustrationNotificationStatus> GetSCRFIllustrationNotificationStatus(XsiScrfillustrationNotificationStatus entity)
        {
            using (ISCRFIllustrationNotificationStatusRepository = new SCRFIllustrationNotificationStatusRepository(ConnectionString))
            {
                return ISCRFIllustrationNotificationStatusRepository.Select(entity);
            }
        }
        public List<XsiScrfillustrationNotificationStatus> GetSCRFIllustrationNotificationStatus(XsiScrfillustrationNotificationStatus entity, EnumSortlistBy sortListBy)
        {
            using (ISCRFIllustrationNotificationStatusRepository = new SCRFIllustrationNotificationStatusRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByItemIdAsc:
                        return ISCRFIllustrationNotificationStatusRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ISCRFIllustrationNotificationStatusRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ISCRFIllustrationNotificationStatusRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiScrfillustrationNotificationStatus GetSCRFIllustrationNotificationStatus(long groupId, long languageId)
        {
            using (ISCRFIllustrationNotificationStatusRepository = new SCRFIllustrationNotificationStatusRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertSCRFIllustrationNotificationStatus(XsiScrfillustrationNotificationStatus entity)
        {
            using (ISCRFIllustrationNotificationStatusRepository = new SCRFIllustrationNotificationStatusRepository(ConnectionString))
            {
                entity = ISCRFIllustrationNotificationStatusRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateSCRFIllustrationNotificationStatus(XsiScrfillustrationNotificationStatus entity)
        {
            XsiScrfillustrationNotificationStatus OriginalEntity = GetSCRFIllustrationNotificationStatusByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ISCRFIllustrationNotificationStatusRepository = new SCRFIllustrationNotificationStatusRepository(ConnectionString))
                {
                    ISCRFIllustrationNotificationStatusRepository.Update(entity);
                    if (ISCRFIllustrationNotificationStatusRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateSCRFIllustrationNotificationStatus(long itemId)
        {
            XsiScrfillustrationNotificationStatus entity = GetSCRFIllustrationNotificationStatusByItemId(itemId);
            if (entity != null)
            {
                using (ISCRFIllustrationNotificationStatusRepository = new SCRFIllustrationNotificationStatusRepository(ConnectionString))
                {
                    entity.Status = EnumConversion.ToString(EnumBool.Yes) == entity.Status ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ISCRFIllustrationNotificationStatusRepository.Update(entity);
                    if (ISCRFIllustrationNotificationStatusRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteSCRFIllustrationNotificationStatus(XsiScrfillustrationNotificationStatus entity)
        {
            List<XsiScrfillustrationNotificationStatus> entities = GetSCRFIllustrationNotificationStatus(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiScrfillustrationNotificationStatus e in entities)
                {
                    result = DeleteSCRFIllustrationNotificationStatus(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteSCRFIllustrationNotificationStatus(long itemId)
        {
            XsiScrfillustrationNotificationStatus entity = GetSCRFIllustrationNotificationStatusByItemId(itemId);
            if (entity != null)
            {
                using (ISCRFIllustrationNotificationStatusRepository = new SCRFIllustrationNotificationStatusRepository(ConnectionString))
                {
                    ISCRFIllustrationNotificationStatusRepository.Delete(itemId);
                    if (ISCRFIllustrationNotificationStatusRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
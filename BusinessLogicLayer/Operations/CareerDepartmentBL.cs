﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class CareerDepartmentBL : IDisposable
    {
        #region Feilds
        ICareerDepartmentRepository CareerDepartmentRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public CareerDepartmentBL()
        {
            ConnectionString = null;
        }
        public CareerDepartmentBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerDepartmentRepository != null)
                this.CareerDepartmentRepository.Dispose();
        }
        #endregion
        #region Methods
         
        public XsiCareerDepartment GetCareerDepartmentByItemId(long itemId)
        {
            using (CareerDepartmentRepository = new CareerDepartmentRepository(ConnectionString))
            {
                return CareerDepartmentRepository.GetById(itemId);
            }
        }
         
        public List<XsiCareerDepartment> GetCareerDepartment()
        {
            using (CareerDepartmentRepository = new CareerDepartmentRepository(ConnectionString))
            {
                return CareerDepartmentRepository.Select();
            }
        }
        public List<XsiCareerDepartment> GetCareerDepartment(EnumSortlistBy sortListBy)
        {
            using (CareerDepartmentRepository = new CareerDepartmentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerDepartmentRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerDepartmentRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerDepartmentRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerDepartmentRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerDepartmentRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerDepartmentRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerDepartmentRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareerDepartment> GetCareerDepartment(XsiCareerDepartment entity)
        {
            using (CareerDepartmentRepository = new CareerDepartmentRepository(ConnectionString))
            {
                return CareerDepartmentRepository.Select(entity);
            }
        }
        public List<XsiCareerDepartment> GetCareerDepartment(XsiCareerDepartment entity, EnumSortlistBy sortListBy)
        {
            using (CareerDepartmentRepository = new CareerDepartmentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerDepartmentRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerDepartmentRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerDepartmentRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerDepartmentRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerDepartmentRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerDepartmentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerDepartmentRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareerDepartment> GetCareerDepartmentOr(XsiCareerDepartment entity, EnumSortlistBy sortListBy)
        {
            using (CareerDepartmentRepository = new CareerDepartmentRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerDepartmentRepository.SelectOr(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerDepartmentRepository.SelectOr(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerDepartmentRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerDepartmentRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerDepartmentRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerDepartmentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerDepartmentRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        
        public EnumResultType InsertCareerDepartment(XsiCareerDepartment entity)
        {
            using (CareerDepartmentRepository = new CareerDepartmentRepository(ConnectionString))
            {
                entity = CareerDepartmentRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateCareerDepartment(XsiCareerDepartment entity)
        {
            XsiCareerDepartment OriginalEntity = GetCareerDepartmentByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (CareerDepartmentRepository = new CareerDepartmentRepository(ConnectionString))
                {
                    CareerDepartmentRepository.Update(entity);
                    if (CareerDepartmentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateCareerDepartmentStatus(long itemId)
        {
            XsiCareerDepartment entity = GetCareerDepartmentByItemId(itemId);
            if (entity != null)
            {
                using (CareerDepartmentRepository = new CareerDepartmentRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    CareerDepartmentRepository.Update(entity);
                    if (CareerDepartmentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteCareerDepartment(XsiCareerDepartment entity)
        {
            List<XsiCareerDepartment> entities = GetCareerDepartment(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiCareerDepartment e in entities)
                {
                    result = DeleteCareerDepartment(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteCareerDepartment(long itemId)
        {
            XsiCareerDepartment entity = GetCareerDepartmentByItemId(itemId);
            if (entity != null)
            {
                using (CareerDepartmentRepository = new CareerDepartmentRepository(ConnectionString))
                {
                    CareerDepartmentRepository.Delete(itemId);
                    if (CareerDepartmentRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
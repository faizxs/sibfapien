﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class EReceiptBL : IDisposable
    {
        #region Feilds
        IEReceiptRepository EReceiptRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public EReceiptBL()
        {
            ConnectionString = null;
        }
        public EReceiptBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.EReceiptRepository != null)
                this.EReceiptRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (EReceiptRepository = new EReceiptRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiEreceipt GetEReceiptByItemId(long itemId)
        {
            using (EReceiptRepository = new EReceiptRepository(ConnectionString))
            {
                return EReceiptRepository.GetById(itemId);
            }
        }

        public List<XsiEreceipt> GetEReceipt()
        {
            using (EReceiptRepository = new EReceiptRepository(ConnectionString))
            {
                return EReceiptRepository.Select();
            }
        }
        public List<XsiEreceipt> GetEReceipt(EnumSortlistBy sortListBy)
        {
            using (EReceiptRepository = new EReceiptRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return EReceiptRepository.Select().OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return EReceiptRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return EReceiptRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EReceiptRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EReceiptRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EReceiptRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EReceiptRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiEreceipt> GetEReceipt(XsiEreceipt entity)
        {
            using (EReceiptRepository = new EReceiptRepository(ConnectionString))
            {
                return EReceiptRepository.Select(entity);
            }
        }
        public List<XsiEreceipt> GetEReceipt(XsiEreceipt entity, EnumSortlistBy sortListBy)
        {
            using (EReceiptRepository = new EReceiptRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return EReceiptRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return EReceiptRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return EReceiptRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return EReceiptRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return EReceiptRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return EReceiptRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return EReceiptRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiEreceipt GetEReceipt(long groupId, long languageId)
        {
            using (EReceiptRepository = new EReceiptRepository(ConnectionString))
            {
                return null;//EReceiptRepository.Select(new XsiEreceipt() { GroupId = groupId, LanguageId = languageId }).FirstOrDefault();
            }
        }

        public EnumResultType InsertEReceipt(XsiEreceipt entity)
        {
            using (EReceiptRepository = new EReceiptRepository(ConnectionString))
            {
                entity = EReceiptRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                  //  XsiGroupId = entity.GroupId ?? -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateEReceipt(XsiEreceipt entity)
        {
            XsiEreceipt OriginalEntity = GetEReceiptByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (EReceiptRepository = new EReceiptRepository(ConnectionString))
                {
                    EReceiptRepository.Update(entity);
                    if (EReceiptRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public EnumResultType UpdateEReceiptStatus(long itemId)
        {
            XsiEreceipt entity = GetEReceiptByItemId(itemId);
            if (entity != null)
            {
                using (EReceiptRepository = new EReceiptRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    EReceiptRepository.Update(entity);
                    if (EReceiptRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteEReceipt(XsiEreceipt entity)
        {
            List<XsiEreceipt> entities = GetEReceipt(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiEreceipt e in entities)
                {
                    result = DeleteEReceipt(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteEReceipt(long itemId)
        {
            XsiEreceipt entity = GetEReceiptByItemId(itemId);
            if (entity != null)
            {
                using (EReceiptRepository = new EReceiptRepository(ConnectionString))
                {
                    EReceiptRepository.Delete(itemId);
                    if (EReceiptRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
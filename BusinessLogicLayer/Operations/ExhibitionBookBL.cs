﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;
using System.Web;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionBookBL : IDisposable
    {
        #region Feilds
        IExhibitionBookRepository ExhibitionBookRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionBookBL()
        {
            ConnectionString = null;
        }
        public ExhibitionBookBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionBookRepository != null)
                this.ExhibitionBookRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public long GetNextClickCount(long itemId)
        {
            //optional method. return -1 if not required and remove following
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                return (ExhibitionBookRepository.GetById(itemId).ClickCount ?? 0) + 1;
            }
        }

        public XsiExhibitionBooks GetBookByItemId(long itemId)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                return ExhibitionBookRepository.GetById(itemId);
            }
        }
        
        public List<XsiExhibitionBooks> GetBook()
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                return ExhibitionBookRepository.Select();
            }
        }
        public List<XsiExhibitionBooks> GetBook(EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookRepository.Select().OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookRepository.Select().OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookRepository.Select().OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookRepository.Select().OrderByDescending(p => p.BookId).ToList();

                    default:
                        return ExhibitionBookRepository.Select().OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
        public List<XsiExhibitionBooks> GetBook(XsiExhibitionBooks entity)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                return ExhibitionBookRepository.Select(entity);
            }
        }
        public List<XsiExhibitionBookForUi> GetBookForUI(XsiExhibitionBooks entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookRepository.SelectBooksForUI(entity).OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookRepository.SelectBooksForUI(entity).OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookRepository.SelectBooksForUI(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookRepository.SelectBooksForUI(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookRepository.SelectBooksForUI(entity).OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookRepository.SelectBooksForUI(entity).OrderByDescending(p => p.BookId).ToList();

                    default:
                        return ExhibitionBookRepository.SelectBooksForUI(entity).OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
        public List<XsiExhibitionBookForUi> GetBookUIAnd(XsiExhibitionBooks entity, EnumSortlistBy sortListBy, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.BookId).ToList();

                    default:
                        return ExhibitionBookRepository.SelectBooksUIAnd(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
        public List<XsiExhibitionBookForUi> GetBookUIOr(XsiExhibitionBooks entity, EnumSortlistBy sortListBy, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.BookId).ToList();

                    default:
                        return ExhibitionBookRepository.SelectBooksUIOr(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
        public List<XsiExhibitionBookForUi> GetBookUITopSearchable(XsiExhibitionBooks entity, long websiteId)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                return ExhibitionBookRepository.SelectBooksUITopSearchable(entity, websiteId);
            }
        }
        public Array GetBooksTitle(XsiExhibitionBooks entity, long websiteId, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookRepository.SelectBooksTitle(entity, websiteId).OrderBy(p => p.TitleEn).ToList().ToArray();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookRepository.SelectBooksTitle(entity, websiteId).OrderByDescending(p => p.TitleEn).ToList().ToArray();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookRepository.SelectBooksTitle(entity, websiteId).OrderBy(p => p.BookId).ToList().ToArray();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookRepository.SelectBooksTitle(entity, websiteId).OrderByDescending(p => p.BookId).ToList().ToArray();

                    default:
                        return ExhibitionBookRepository.SelectBooksTitle(entity, websiteId).OrderByDescending(p => p.BookId).ToList().ToArray();
                }
            }
        }
        public List<XsiExhibitionBookForUi> GetBooksSCRFTitle(XsiExhibitionBooks entity, long websiteId, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookRepository.SelectBooksTitle(entity, websiteId).OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookRepository.SelectBooksTitle(entity, websiteId).OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookRepository.SelectBooksTitle(entity, websiteId).OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookRepository.SelectBooksTitle(entity, websiteId).OrderByDescending(p => p.BookId).ToList();

                    default:
                        return ExhibitionBookRepository.SelectBooksTitle(entity, websiteId).OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
        public List<XsiExhibitionBooks> GetBook(XsiExhibitionBooks entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookRepository.Select(entity).OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookRepository.Select(entity).OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookRepository.Select(entity).OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookRepository.Select(entity).OrderByDescending(p => p.BookId).ToList();

                    default:
                        return ExhibitionBookRepository.Select(entity).OrderByDescending(p => p.BookId).ToList();
                }
            }
        }
        
        //public List<GetBooksToExport_Result> GetBooksToExport(string bookIds, string languageId)
        //{
        //    using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
        //    {
        //        return ExhibitionBookRepository.SelectBooksToExport(bookIds, languageId);
        //    }
        //}
        //public void UpdatePublisherId(long? oldPublisherID, long? newPublisherID)
        //{
        //    using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
        //    {
        //        ExhibitionBookRepository.UpdatePublisherId(oldPublisherID, newPublisherID);
        //    }
        //}
        //public void UpdateAuthorId(long? oldAuthorID, long? newAuthorID)
        //{
        //    using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
        //    {
        //        ExhibitionBookRepository.UpdateAuthorId(oldAuthorID, newAuthorID);
        //    }
        //}

        public EnumResultType InsertBook(XsiExhibitionBooks entity)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                entity = ExhibitionBookRepository.Add(entity);
                if (entity.BookId > 0)
                {
                    XsiItemdId = entity.BookId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateBook(XsiExhibitionBooks entity)
        {
            XsiExhibitionBooks OriginalEntity = GetBookByItemId(entity.BookId);

            if (OriginalEntity != null)
            {
                using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
                {
                    ExhibitionBookRepository.Update(entity);
                    if (ExhibitionBookRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateBookStatus(long itemId)
        {
            XsiExhibitionBooks entity = GetBookByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionBookRepository.Update(entity);
                    if (ExhibitionBookRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteBook(XsiExhibitionBooks entity)
        {
            List<XsiExhibitionBooks> entities = GetBook(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionBooks e in entities)
                {
                    result = DeleteBook(e.BookId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteBook(long itemId)
        {
            XsiExhibitionBooks entity = GetBookByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
                {
                    ExhibitionBookRepository.Delete(itemId);
                    if (ExhibitionBookRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }

        public List<XsiExhibitionBookForUi> GetBookUIAnd1(XsiExhibitionBooks entity, EnumSortlistBy sortListBy, string subjectIDs, string subsubjectIDs, string bookTypeIDs, double priceFrom, double priceTo, long websiteId)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionBookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionBookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.TitleEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionBookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionBookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionBookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderBy(p => p.BookId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionBookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.BookId).ToList();

                    default:
                        return ExhibitionBookRepository.SelectBooksUIAnd1(entity, subjectIDs, subsubjectIDs, bookTypeIDs, priceFrom, priceTo, websiteId).OrderByDescending(p => p.BookId).ToList();
                }
            }
        }

        //public void UpdateAuthorInBooks(long oldAuthorId, long newAuthorId, long AdminUserId)
        //{
        //    using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
        //    {
        //        ExhibitionBookRepository.UpdateAuthorInBooks(oldAuthorId, newAuthorId, AdminUserId);
        //    }
        //}

        //public void UpdatePublisherInBooks(long oldPublisherId, long newPublisherId, long AdminUserId)
        //{
        //    using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
        //    {
        //        ExhibitionBookRepository.UpdatePublisherInBooks(oldPublisherId, newPublisherId, AdminUserId);
        //    }
        //}
        public void InsertBook(XsiExhibitionBookAuthor entity)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                ExhibitionBookRepository.AddAuthor(entity);                
            }
        }
        public void DeleteBookAuthor(long bookId)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                ExhibitionBookRepository.DeleteBookAuthor(bookId);
            }
        }
        public void AddBooksParticipating(XsiExhibitionBookParticipating entity)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                ExhibitionBookRepository.AddBookParticipating(entity);
            }
        }
        public void UpdateBookParticipating(XsiExhibitionBookParticipating entity)
        {
            using (ExhibitionBookRepository = new ExhibitionBookRepository(ConnectionString))
            {
                ExhibitionBookRepository.UpdateBookParticipating(entity);
            }
        }
        #endregion
    }
}
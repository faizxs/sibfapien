﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class CareerApplicantBL : IDisposable
    {
        #region Feilds
        ICareerApplicantsRepository CareerApplicantsRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public CareerApplicantBL()
        {
            ConnectionString = null;
        }
        public CareerApplicantBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.CareerApplicantsRepository != null)
                this.CareerApplicantsRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiCareerApplicants GetCareerApplicantByItemId(long itemId)
        {
            using (CareerApplicantsRepository = new CareerApplicantsRepository(ConnectionString))
            {
                return CareerApplicantsRepository.GetById(itemId);
            }
        }
        
        public List<XsiCareerApplicants> GetCareerApplicant()
        {
            using (CareerApplicantsRepository = new CareerApplicantsRepository(ConnectionString))
            {
                return CareerApplicantsRepository.Select();
            }
        }
        public List<XsiCareerApplicants> GetCareerApplicant(EnumSortlistBy sortListBy)
        {
            using (CareerApplicantsRepository = new CareerApplicantsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerApplicantsRepository.Select().OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerApplicantsRepository.Select().OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerApplicantsRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerApplicantsRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerApplicantsRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerApplicantsRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerApplicantsRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareerApplicants> GetCareerApplicant(XsiCareerApplicants entity)
        {
            using (CareerApplicantsRepository = new CareerApplicantsRepository(ConnectionString))
            {
                return CareerApplicantsRepository.Select(entity);
            }
        }
        public List<XsiCareerApplicants> GetCareerApplicant(XsiCareerApplicants entity, EnumSortlistBy sortListBy)
        {
            using (CareerApplicantsRepository = new CareerApplicantsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerApplicantsRepository.Select(entity).OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerApplicantsRepository.Select(entity).OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerApplicantsRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerApplicantsRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerApplicantsRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerApplicantsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerApplicantsRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiCareerApplicants> GetCareerApplicantOr(XsiCareerApplicants entity, EnumSortlistBy sortListBy)
        {
            using (CareerApplicantsRepository = new CareerApplicantsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return CareerApplicantsRepository.SelectOr(entity).OrderBy(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return CareerApplicantsRepository.SelectOr(entity).OrderByDescending(p => p.FirstName).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return CareerApplicantsRepository.SelectOr(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return CareerApplicantsRepository.SelectOr(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return CareerApplicantsRepository.SelectOr(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return CareerApplicantsRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return CareerApplicantsRepository.SelectOr(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertCareerApplicant(XsiCareerApplicants entity)
        {
            using (CareerApplicantsRepository = new CareerApplicantsRepository(ConnectionString))
            {
                entity = CareerApplicantsRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateCareerApplicant(XsiCareerApplicants entity)
        {
            XsiCareerApplicants OriginalEntity = GetCareerApplicantByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (CareerApplicantsRepository = new CareerApplicantsRepository(ConnectionString))
                {

                    CareerApplicantsRepository.Update(entity);
                    if (CareerApplicantsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateCareerApplicantStatus(long itemId)
        {
            XsiCareerApplicants entity = GetCareerApplicantByItemId(itemId);
            if (entity != null)
            {
                using (CareerApplicantsRepository = new CareerApplicantsRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    CareerApplicantsRepository.Update(entity);
                    if (CareerApplicantsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteCareerApplicant(XsiCareerApplicants entity)
        {
            List<XsiCareerApplicants> entities = GetCareerApplicant(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiCareerApplicants e in entities)
                {
                    result = DeleteCareerApplicant(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteCareerApplicant(long itemId)
        {
            XsiCareerApplicants entity = GetCareerApplicantByItemId(itemId);
            if (entity != null)
            {
                using (CareerApplicantsRepository = new CareerApplicantsRepository(ConnectionString))
                {
                    CareerApplicantsRepository.Delete(itemId);
                    if (CareerApplicantsRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
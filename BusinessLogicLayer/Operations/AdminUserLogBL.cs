﻿using System;
using System.Collections.Generic;
using System.Text;
using Xsi.Repositories;
using Entities.Models;
using System.Linq;

namespace Xsi.BusinessLogicLayer
{
    public class AdminUserLogBL : IDisposable
    {
        #region Feilds
        IAdminLogRepository XsiAdminLogRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public AdminUserLogBL()
        {
            ConnectionString = null;
        }
        public AdminUserLogBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.XsiAdminLogRepository != null)
                this.XsiAdminLogRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiAdminLogs GetLogByItemId(long itemId)
        {
            using (XsiAdminLogRepository = new AdminUserLogsRepository())
            {
                return XsiAdminLogRepository.GetById(itemId);
            }
        }
        public XsiAdminLogs GetLogLastLogin(long userId)
        {
            using (XsiAdminLogRepository = new AdminUserLogsRepository(ConnectionString))
            {
                return XsiAdminLogRepository.GetLogLastLogin(userId);
            }
        }

        public List<XsiAdminLogs> GetLog(EnumSortlistBy sortListBy)
        {
            using (XsiAdminLogRepository = new AdminUserLogsRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return XsiAdminLogRepository.Select().OrderBy(p => p.Username).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return XsiAdminLogRepository.Select().OrderByDescending(p => p.Username).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return XsiAdminLogRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return XsiAdminLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return XsiAdminLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiAdminLogs> GetLog(XsiAdminLogs entity)
        {
            using (XsiAdminLogRepository = new AdminUserLogsRepository(ConnectionString))
            {
                return XsiAdminLogRepository.Select(entity);
            }
        }
        public EnumResultType InsertLog(XsiAdminLogs entity)
        {
            using (XsiAdminLogRepository = new AdminUserLogsRepository(ConnectionString))
            {
                entity = XsiAdminLogRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId = -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateLog(XsiAdminLogs entity)
        {
            if (GetLogByItemId(entity.ItemId) != null)
            {
                using (XsiAdminLogRepository = new AdminUserLogsRepository(ConnectionString))
                {
                    XsiAdminLogRepository.Update(entity);
                    if (XsiAdminLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteLog(XsiAdminLogs entity)
        {
            if (GetLog(entity).ToList().Count > 0)
            {
                using (XsiAdminLogRepository = new AdminUserLogsRepository(ConnectionString))
                {
                    XsiAdminLogRepository.Delete(entity);
                    if (XsiAdminLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteLog(long itemId)
        {
            if (GetLogByItemId(itemId) != null)
            {
                using (XsiAdminLogRepository = new AdminUserLogsRepository(ConnectionString))
                {
                    XsiAdminLogRepository.Delete(itemId);
                    if (XsiAdminLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public void DeleteSixMonthsOldLog()
        {
            using (XsiAdminLogRepository = new AdminUserLogsRepository(ConnectionString))
            {
                DateTime sixMonthsAgo= DateTime.Now.AddMonths(-6).Date;
                foreach (XsiAdminLogs log in GetLog(EnumSortlistBy.ByItemIdDesc).Where(p => p.LoggedOn <= sixMonthsAgo))
                {
                    DeleteLog(log.ItemId);
                }
            }
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class NewsletterBL : IDisposable
    {
        #region Feilds
        INewsletterRepository NewsletterRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public NewsletterBL()
        {
            ConnectionString = null;
        }
        public NewsletterBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.NewsletterRepository != null)
                this.NewsletterRepository.Dispose();
        }
        #endregion
        #region Methods
        
        public XsiNewsletter GetNewsletterByItemId(long itemId)
        {
            using (NewsletterRepository = new NewsletterRepository(ConnectionString))
            {
                return NewsletterRepository.GetById(itemId);
            }
        }
         
        public List<XsiNewsletter> GetNewsletter()
        {
            using (NewsletterRepository = new NewsletterRepository(ConnectionString))
            {
                return NewsletterRepository.Select();
            }
        }
        public List<XsiNewsletter> GetNewsletter(EnumSortlistBy sortListBy)
        {
            using (NewsletterRepository = new NewsletterRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsletterRepository.Select().OrderBy(p => p.IssueNumber).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsletterRepository.Select().OrderByDescending(p => p.IssueNumber).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return NewsletterRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return NewsletterRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsletterRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsletterRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsletterRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiNewsletter> GetNewsletter(XsiNewsletter entity)
        {
            using (NewsletterRepository = new NewsletterRepository(ConnectionString))
            {
                return NewsletterRepository.Select(entity);
            }
        }
        public List<XsiNewsletter> GetNewsletter(XsiNewsletter entity, EnumSortlistBy sortListBy)
        {
            using (NewsletterRepository = new NewsletterRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return NewsletterRepository.Select(entity).OrderBy(p => p.IssueNumber).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return NewsletterRepository.Select(entity).OrderByDescending(p => p.IssueNumber).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return NewsletterRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return NewsletterRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return NewsletterRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return NewsletterRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return NewsletterRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
         
        public EnumResultType InsertNewsletter(XsiNewsletter entity)
        {
            using (NewsletterRepository = new NewsletterRepository(ConnectionString))
            {
                entity = NewsletterRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId; 
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateNewsletter(XsiNewsletter entity)
        {
            XsiNewsletter OriginalEntity = GetNewsletterByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (NewsletterRepository = new NewsletterRepository(ConnectionString))
                {
                    NewsletterRepository.Update(entity);
                    if (NewsletterRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateNewsletterStatus(long itemId)
        {
            XsiNewsletter entity = GetNewsletterByItemId(itemId);
            if (entity != null)
            {
                using (NewsletterRepository = new NewsletterRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    NewsletterRepository.Update(entity);
                    if (NewsletterRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteNewsletter(XsiNewsletter entity)
        {
            List<XsiNewsletter> entities = GetNewsletter(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiNewsletter e in entities)
                {
                    result = DeleteNewsletter(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteNewsletter(long itemId)
        {
            XsiNewsletter entity = GetNewsletterByItemId(itemId);
            if (entity != null)
            {
                using (NewsletterRepository = new NewsletterRepository(ConnectionString))
                {
                    NewsletterRepository.Delete(itemId);
                    if (NewsletterRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
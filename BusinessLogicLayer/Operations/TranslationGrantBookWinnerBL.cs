﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class TranslationGrantBookWinnerBL : IDisposable
    {
        #region Feilds
        ITranslationGrantBookWinnerRepository TranslationGrantBookWinnerRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public TranslationGrantBookWinnerBL()
        {
            ConnectionString = null;
        }
        public TranslationGrantBookWinnerBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.TranslationGrantBookWinnerRepository != null)
                this.TranslationGrantBookWinnerRepository.Dispose();
        }
        #endregion
        #region Methods

        public XsiExhibitionTranslationGrantBookWinner GetTranslationGrantBookWinnerByItemId(long itemId)
        {
            using (TranslationGrantBookWinnerRepository = new TranslationGrantBookWinnerRepository(ConnectionString))
            {
                return TranslationGrantBookWinnerRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionTranslationGrantBookWinner> GetTranslationGrantBookWinner()
        {
            using (TranslationGrantBookWinnerRepository = new TranslationGrantBookWinnerRepository(ConnectionString))
            {
                return TranslationGrantBookWinnerRepository.Select();
            }
        }
        public List<XsiExhibitionTranslationGrantBookWinner> GetTranslationGrantBookWinner(EnumSortlistBy sortListBy)
        {
            using (TranslationGrantBookWinnerRepository = new TranslationGrantBookWinnerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return TranslationGrantBookWinnerRepository.Select().OrderBy(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return TranslationGrantBookWinnerRepository.Select().OrderByDescending(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return TranslationGrantBookWinnerRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return TranslationGrantBookWinnerRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return TranslationGrantBookWinnerRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return TranslationGrantBookWinnerRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return TranslationGrantBookWinnerRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionTranslationGrantBookWinner> GetTranslationGrantBookWinner(XsiExhibitionTranslationGrantBookWinner entity)
        {
            using (TranslationGrantBookWinnerRepository = new TranslationGrantBookWinnerRepository(ConnectionString))
            {
                return TranslationGrantBookWinnerRepository.Select(entity);
            }
        }
        public List<XsiExhibitionTranslationGrantBookWinner> GetTranslationGrantBookWinner(XsiExhibitionTranslationGrantBookWinner entity, EnumSortlistBy sortListBy)
        {
            using (TranslationGrantBookWinnerRepository = new TranslationGrantBookWinnerRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return TranslationGrantBookWinnerRepository.Select(entity).OrderBy(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return TranslationGrantBookWinnerRepository.Select(entity).OrderByDescending(p => p.NameEn).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return TranslationGrantBookWinnerRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return TranslationGrantBookWinnerRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return TranslationGrantBookWinnerRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return TranslationGrantBookWinnerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return TranslationGrantBookWinnerRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertTranslationGrantBookWinner(XsiExhibitionTranslationGrantBookWinner entity)
        {
            using (TranslationGrantBookWinnerRepository = new TranslationGrantBookWinnerRepository(ConnectionString))
            {
                entity = TranslationGrantBookWinnerRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId =  -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateTranslationGrantBookWinner(XsiExhibitionTranslationGrantBookWinner entity)
        {
            XsiExhibitionTranslationGrantBookWinner OriginalEntity = GetTranslationGrantBookWinnerByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (TranslationGrantBookWinnerRepository = new TranslationGrantBookWinnerRepository(ConnectionString))
                {
                    TranslationGrantBookWinnerRepository.Update(entity);
                    if (TranslationGrantBookWinnerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateTranslationGrantBookWinnerStatus(long itemId)
        {
            XsiExhibitionTranslationGrantBookWinner entity = GetTranslationGrantBookWinnerByItemId(itemId);
            if (entity != null)
            {
                using (TranslationGrantBookWinnerRepository = new TranslationGrantBookWinnerRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    TranslationGrantBookWinnerRepository.Update(entity);
                    if (TranslationGrantBookWinnerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteTranslationGrantBookWinner(XsiExhibitionTranslationGrantBookWinner entity)
        {
            List<XsiExhibitionTranslationGrantBookWinner> entities = GetTranslationGrantBookWinner(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionTranslationGrantBookWinner e in entities)
                {
                    result = DeleteTranslationGrantBookWinner(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteTranslationGrantBookWinner(long itemId)
        {
            XsiExhibitionTranslationGrantBookWinner entity = GetTranslationGrantBookWinnerByItemId(itemId);
            if (entity != null)
            {
                using (TranslationGrantBookWinnerRepository = new TranslationGrantBookWinnerRepository(ConnectionString))
                {
                    TranslationGrantBookWinnerRepository.Delete(itemId);
                    if (TranslationGrantBookWinnerRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ExhibitionAreaBL : IDisposable
    {
        #region Feilds
        IExhibitionAreaRepository ExhibitionAreaRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemId { get; private set; }
        #endregion
        #region Constructor
        public ExhibitionAreaBL()
        {
            ConnectionString = null;
        }
        public ExhibitionAreaBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.ExhibitionAreaRepository != null)
                this.ExhibitionAreaRepository.Dispose();
        }
        #endregion
        #region Methods
        public XsiExhibitionArea GetExhibitionAreaByItemId(long itemId)
        {
            using (ExhibitionAreaRepository = new ExhibitionAreaRepository(ConnectionString))
            {
                return ExhibitionAreaRepository.GetById(itemId);
            }
        }

        public List<XsiExhibitionArea> GetExhibitionArea()
        {
            using (ExhibitionAreaRepository = new ExhibitionAreaRepository(ConnectionString))
            {
                return ExhibitionAreaRepository.Select();
            }
        }
        public List<XsiExhibitionArea> GetExhibitionArea(EnumSortlistBy sortListBy)
        {
            using (ExhibitionAreaRepository = new ExhibitionAreaRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionAreaRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionAreaRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionAreaRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionAreaRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionAreaRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionAreaRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionAreaRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionArea> GetExhibitionArea(XsiExhibitionArea entity)
        {
            using (ExhibitionAreaRepository = new ExhibitionAreaRepository(ConnectionString))
            {
                return ExhibitionAreaRepository.Select(entity);
            }
        }
        public List<XsiExhibitionArea> GetExhibitionArea(XsiExhibitionArea entity, EnumSortlistBy sortListBy)
        {
            using (ExhibitionAreaRepository = new ExhibitionAreaRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return ExhibitionAreaRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return ExhibitionAreaRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return ExhibitionAreaRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return ExhibitionAreaRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return ExhibitionAreaRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return ExhibitionAreaRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return ExhibitionAreaRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertExhibitionArea(XsiExhibitionArea entity)
        {
            using (ExhibitionAreaRepository = new ExhibitionAreaRepository(ConnectionString))
            {
                entity = ExhibitionAreaRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateExhibitionArea(XsiExhibitionArea entity)
        {
            XsiExhibitionArea OriginalEntity = GetExhibitionAreaByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (ExhibitionAreaRepository = new ExhibitionAreaRepository(ConnectionString))
                {
                    ExhibitionAreaRepository.Update(entity);
                    if (ExhibitionAreaRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateExhibitionAreaStatus(long itemId)
        {
            XsiExhibitionArea entity = GetExhibitionAreaByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionAreaRepository = new ExhibitionAreaRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    ExhibitionAreaRepository.Update(entity);
                    if (ExhibitionAreaRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteExhibitionArea(XsiExhibitionArea entity)
        {
            List<XsiExhibitionArea> entities = GetExhibitionArea(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionArea e in entities)
                {
                    result = DeleteExhibitionArea(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteExhibitionArea(long itemId)
        {
            XsiExhibitionArea entity = GetExhibitionAreaByItemId(itemId);
            if (entity != null)
            {
                using (ExhibitionAreaRepository = new ExhibitionAreaRepository(ConnectionString))
                {
                    ExhibitionAreaRepository.Delete(itemId);
                    if (ExhibitionAreaRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
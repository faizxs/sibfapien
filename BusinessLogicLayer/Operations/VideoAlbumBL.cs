﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class VideoAlbumBL : IDisposable
    {
        #region Feilds
        IVideoAlbumRepository VideoAlbumRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public VideoAlbumBL()
        {
            ConnectionString = null;
        }
        public VideoAlbumBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.VideoAlbumRepository != null)
                this.VideoAlbumRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextSortIndex()
        {
            //optional method. return -1 if not required and remove following
            using (VideoAlbumRepository = new VideoAlbumRepository(ConnectionString))
            {
                return (VideoAlbumRepository.Select().Max(p => p.SortIndex) ?? 0) + 1;
            }
        }

        public XsiVideoAlbum GetVideoAlbumByItemId(long itemId)
        {
            using (VideoAlbumRepository = new VideoAlbumRepository(ConnectionString))
            {
                return VideoAlbumRepository.GetById(itemId);
            }
        }

        public List<XsiVideoAlbum> GetVideoAlbum()
        {
            using (VideoAlbumRepository = new VideoAlbumRepository(ConnectionString))
            {
                return VideoAlbumRepository.Select();
            }
        }
        public List<XsiVideoAlbum> GetVideoAlbum(EnumSortlistBy sortListBy)
        {
            using (VideoAlbumRepository = new VideoAlbumRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return VideoAlbumRepository.Select().OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return VideoAlbumRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return VideoAlbumRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return VideoAlbumRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return VideoAlbumRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return VideoAlbumRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return VideoAlbumRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiVideoAlbum> GetVideoAlbum(XsiVideoAlbum entity)
        {
            using (VideoAlbumRepository = new VideoAlbumRepository(ConnectionString))
            {
                return VideoAlbumRepository.Select(entity);
            }
        }
        public List<XsiVideoAlbum> GetVideoAlbum(XsiVideoAlbum entity, EnumSortlistBy sortListBy)
        {
            using (VideoAlbumRepository = new VideoAlbumRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return VideoAlbumRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return VideoAlbumRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return VideoAlbumRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return VideoAlbumRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return VideoAlbumRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return VideoAlbumRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    case EnumSortlistBy.BySortOrderAsc:
                        return VideoAlbumRepository.Select(entity).OrderBy(p => p.SortIndex).ToList();

                    case EnumSortlistBy.BySortOrderDesc:
                        return VideoAlbumRepository.Select(entity).OrderByDescending(p => p.SortIndex).ToList();

                    default:
                        return VideoAlbumRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }

        public EnumResultType InsertVideoAlbum(XsiVideoAlbum entity)
        {
            using (VideoAlbumRepository = new VideoAlbumRepository(ConnectionString))
            {
                entity = VideoAlbumRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateVideoAlbum(XsiVideoAlbum entity)
        {
            XsiVideoAlbum OriginalEntity = GetVideoAlbumByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (VideoAlbumRepository = new VideoAlbumRepository(ConnectionString))
                {
                    VideoAlbumRepository.Update(entity);
                    if (VideoAlbumRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateVideoAlbumStatus(long itemId, long langId)
        {
            XsiVideoAlbum entity = GetVideoAlbumByItemId(itemId);
            if (entity != null)
            {
                using (VideoAlbumRepository = new VideoAlbumRepository(ConnectionString))
                {
                    if (langId == 1)
                        entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    else
                        entity.IsActiveAr = EnumConversion.ToString(EnumBool.Yes) == entity.IsActiveAr ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    VideoAlbumRepository.Update(entity);
                    if (VideoAlbumRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteVideoAlbum(XsiVideoAlbum entity)
        {
            List<XsiVideoAlbum> entities = GetVideoAlbum(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiVideoAlbum e in entities)
                {
                    result = DeleteVideoAlbum(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteVideoAlbum(long itemId)
        {
            XsiVideoAlbum entity = GetVideoAlbumByItemId(itemId);
            if (entity != null)
            {
                using (VideoAlbumRepository = new VideoAlbumRepository(ConnectionString))
                {
                    VideoAlbumRepository.Delete(itemId);
                    if (VideoAlbumRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
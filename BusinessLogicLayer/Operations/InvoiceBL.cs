﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class InvoiceBL : IDisposable
    {
        #region Feilds
        IInvoiceRepository InvoiceRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public InvoiceBL()
        {
            ConnectionString = null;
        }
        public InvoiceBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.InvoiceRepository != null)
                this.InvoiceRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (InvoiceRepository = new InvoiceRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiInvoice GetInvoiceByItemId(long itemId)
        {
            using (InvoiceRepository = new InvoiceRepository(ConnectionString))
            {
                return InvoiceRepository.GetById(itemId);
            }
        }
     
        public List<XsiInvoice> GetInvoice()
        {
            InvoiceRepository = new InvoiceRepository(ConnectionString);
            return InvoiceRepository.Select();
        }
        public List<XsiInvoice> GetInvoice(EnumSortlistBy sortListBy)
        {
            using (InvoiceRepository = new InvoiceRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return InvoiceRepository.Select().OrderBy(p => p.InvoiceNumber).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return InvoiceRepository.Select().OrderByDescending(p => p.InvoiceNumber).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return InvoiceRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return InvoiceRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return InvoiceRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return InvoiceRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return InvoiceRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiInvoice> GetInvoice(XsiInvoice entity)
        {
            InvoiceRepository = new InvoiceRepository(ConnectionString);
            return InvoiceRepository.Select(entity);
        }
        public List<XsiInvoice> GetInvoice(XsiInvoice entity, EnumSortlistBy sortListBy)
        {
            using (InvoiceRepository = new InvoiceRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    case EnumSortlistBy.ByAlphabetAsc:
                        return InvoiceRepository.Select(entity).OrderBy(p => p.InvoiceNumber).ToList();

                    case EnumSortlistBy.ByAlphabetDesc:
                        return InvoiceRepository.Select(entity).OrderByDescending(p => p.InvoiceNumber).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return InvoiceRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return InvoiceRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return InvoiceRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return InvoiceRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return InvoiceRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiInvoice GetInvoice(long groupId, long languageId)
        {
            using (InvoiceRepository = new InvoiceRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertInvoice(XsiInvoice entity)
        {
            using (InvoiceRepository = new InvoiceRepository(ConnectionString))
            {
                entity = InvoiceRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId =  -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateInvoice(XsiInvoice entity)
        {
            XsiInvoice OriginalEntity = GetInvoiceByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (InvoiceRepository = new InvoiceRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line

                    InvoiceRepository.Update(entity);
                    if (InvoiceRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateInvoiceStatus(long itemId)
        {
            XsiInvoice entity = GetInvoiceByItemId(itemId);
            if (entity != null)
            {
                using (InvoiceRepository = new InvoiceRepository(ConnectionString))
                {
                    entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    InvoiceRepository.Update(entity);
                    if (InvoiceRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteInvoice(XsiInvoice entity)
        {
            List<XsiInvoice> entities = GetInvoice(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiInvoice e in entities)
                {
                    result = DeleteInvoice(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteInvoice(long itemId)
        {
            XsiInvoice entity = GetInvoiceByItemId(itemId);
            if (entity != null)
            {
                using (InvoiceRepository = new InvoiceRepository(ConnectionString))
                {
                    InvoiceRepository.Delete(itemId);
                    if (InvoiceRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
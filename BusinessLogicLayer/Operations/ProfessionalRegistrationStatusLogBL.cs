﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xsi.Repositories;
using Entities.Models;

namespace Xsi.BusinessLogicLayer
{
    public class ProfessionalRegistrationStatusLogBL : IDisposable
    {
        #region Feilds
        IExhibitionProfessionalProgramRegistrationStatusLogRepository IExhibitionProfessionalProgramRegistrationStatusLogRepository;
        #endregion
        #region Properties
        public string ConnectionString { get; private set; }
        public long XsiItemdId { get; private set; }
        public long XsiGroupId { get; private set; }
        #endregion
        #region Constructor
        public ProfessionalRegistrationStatusLogBL()
        {
            ConnectionString = null;
        }
        public ProfessionalRegistrationStatusLogBL(string connectionString)
        {
            ConnectionString = connectionString;
        }
        #endregion
        #region Destructor
        public void Dispose()
        {
            if (this.IExhibitionProfessionalProgramRegistrationStatusLogRepository != null)
                this.IExhibitionProfessionalProgramRegistrationStatusLogRepository.Dispose();
        }
        #endregion
        #region Methods
        public long GetNextGroupId()
        {
            //optional method. return -1 if not required and remove following
            using (IExhibitionProfessionalProgramRegistrationStatusLogRepository = new ExhibitionProfessionalProgramRegistrationStatusLogRepository(ConnectionString))
            {
                return -1;
            }
        }

        public XsiExhibitionProfessionalProgramRegistrationStatusLogs GetProfessionalRegistrationStatusLogByItemId(long itemId)
        {
            using (IExhibitionProfessionalProgramRegistrationStatusLogRepository = new ExhibitionProfessionalProgramRegistrationStatusLogRepository(ConnectionString))
            {
                return IExhibitionProfessionalProgramRegistrationStatusLogRepository.GetById(itemId);
            }
        }
        
        public List<XsiExhibitionProfessionalProgramRegistrationStatusLogs> GetProfessionalRegistrationStatusLog()
        {
            using (IExhibitionProfessionalProgramRegistrationStatusLogRepository = new ExhibitionProfessionalProgramRegistrationStatusLogRepository(ConnectionString))
            {
                return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select();
            }
        }
        public List<XsiExhibitionProfessionalProgramRegistrationStatusLogs> GetProfessionalRegistrationStatusLog(EnumSortlistBy sortListBy)
        {
            using (IExhibitionProfessionalProgramRegistrationStatusLogRepository = new ExhibitionProfessionalProgramRegistrationStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select().OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select().OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select().OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select().OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select().OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select().OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public List<XsiExhibitionProfessionalProgramRegistrationStatusLogs> GetProfessionalRegistrationStatusLog(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity)
        {
            using (IExhibitionProfessionalProgramRegistrationStatusLogRepository = new ExhibitionProfessionalProgramRegistrationStatusLogRepository(ConnectionString))
            {
                return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select(entity);
            }
        }
        public List<XsiExhibitionProfessionalProgramRegistrationStatusLogs> GetProfessionalRegistrationStatusLog(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity, EnumSortlistBy sortListBy)
        {
            using (IExhibitionProfessionalProgramRegistrationStatusLogRepository = new ExhibitionProfessionalProgramRegistrationStatusLogRepository(ConnectionString))
            {
                switch (sortListBy)
                {
                    //case EnumSortlistBy.ByAlphabetAsc:
                    //    return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select(entity).OrderBy(p => p.Title).ToList();

                    //case EnumSortlistBy.ByAlphabetDesc:
                    //    return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select(entity).OrderByDescending(p => p.Title).ToList();

                    case EnumSortlistBy.ByDateAsc:
                        return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select(entity).OrderBy(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByDateDesc:
                        return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select(entity).OrderByDescending(p => p.CreatedOn).ToList();

                    case EnumSortlistBy.ByItemIdAsc:
                        return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select(entity).OrderBy(p => p.ItemId).ToList();

                    case EnumSortlistBy.ByItemIdDesc:
                        return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();

                    default:
                        return IExhibitionProfessionalProgramRegistrationStatusLogRepository.Select(entity).OrderByDescending(p => p.ItemId).ToList();
                }
            }
        }
        public XsiExhibitionProfessionalProgramRegistrationStatusLogs GetProfessionalRegistrationStatusLog(long groupId, long languageId)
        {
            using (IExhibitionProfessionalProgramRegistrationStatusLogRepository = new ExhibitionProfessionalProgramRegistrationStatusLogRepository(ConnectionString))
            {
                return null;
            }
        }

        public EnumResultType InsertProfessionalRegistrationStatusLog(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity)
        {
            using (IExhibitionProfessionalProgramRegistrationStatusLogRepository = new ExhibitionProfessionalProgramRegistrationStatusLogRepository(ConnectionString))
            {
                entity = IExhibitionProfessionalProgramRegistrationStatusLogRepository.Add(entity);
                if (entity.ItemId > 0)
                {
                    XsiItemdId = entity.ItemId;
                    XsiGroupId =  -1;
                    return EnumResultType.Success;
                }
                else
                {
                    XsiItemdId = -1;
                    XsiGroupId = -1;
                    return EnumResultType.Failed;
                }
            }
        }
        public EnumResultType UpdateProfessionalRegistrationStatusLog(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity)
        {
            XsiExhibitionProfessionalProgramRegistrationStatusLogs OriginalEntity = GetProfessionalRegistrationStatusLogByItemId(entity.ItemId);

            if (OriginalEntity != null)
            {
                using (IExhibitionProfessionalProgramRegistrationStatusLogRepository = new ExhibitionProfessionalProgramRegistrationStatusLogRepository(ConnectionString))
                {
                    //if no RollbackXml propery in entity then delete this line
                    
                    IExhibitionProfessionalProgramRegistrationStatusLogRepository.Update(entity);
                    if (IExhibitionProfessionalProgramRegistrationStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType UpdateProfessionalRegistrationStatusLogStatus(long itemId)
        {
            XsiExhibitionProfessionalProgramRegistrationStatusLogs entity = GetProfessionalRegistrationStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionProfessionalProgramRegistrationStatusLogRepository = new ExhibitionProfessionalProgramRegistrationStatusLogRepository(ConnectionString))
                {
                    //entity.IsActive = EnumConversion.ToString(EnumBool.Yes) == entity.IsActive ? EnumConversion.ToString(EnumBool.No) : EnumConversion.ToString(EnumBool.Yes);
                    IExhibitionProfessionalProgramRegistrationStatusLogRepository.Update(entity);
                    if (IExhibitionProfessionalProgramRegistrationStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        public EnumResultType DeleteProfessionalRegistrationStatusLog(XsiExhibitionProfessionalProgramRegistrationStatusLogs entity)
        {
            List<XsiExhibitionProfessionalProgramRegistrationStatusLogs> entities = GetProfessionalRegistrationStatusLog(entity).ToList();
            EnumResultType result = EnumResultType.NotFound;

            if (entities.Count > 0)
            {
                foreach (XsiExhibitionProfessionalProgramRegistrationStatusLogs e in entities)
                {
                    result = DeleteProfessionalRegistrationStatusLog(e.ItemId);
                    if (result == EnumResultType.ValueInUse || result == EnumResultType.Failed)
                        return result;
                }
            }

            return result;
        }
        public EnumResultType DeleteProfessionalRegistrationStatusLog(long itemId)
        {
            XsiExhibitionProfessionalProgramRegistrationStatusLogs entity = GetProfessionalRegistrationStatusLogByItemId(itemId);
            if (entity != null)
            {
                using (IExhibitionProfessionalProgramRegistrationStatusLogRepository = new ExhibitionProfessionalProgramRegistrationStatusLogRepository(ConnectionString))
                {
                    IExhibitionProfessionalProgramRegistrationStatusLogRepository.Delete(itemId);
                    if (IExhibitionProfessionalProgramRegistrationStatusLogRepository.SubmitChanges() > 0)
                        return EnumResultType.Success;
                    else
                        return EnumResultType.Failed;
                }
            }
            else
                return EnumResultType.NotFound;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xsi.BusinessLogicLayer
{
    public class BusinessMethodFactory
    {
        #region Methods
        public static string GetRollbackXml<T>(T entity)
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(entity.GetType());
            StringBuilder entityXml = new StringBuilder();
            System.Xml.XmlWriter xmlWriter = System.Xml.XmlWriter.Create(entityXml);
            serializer.Serialize(xmlWriter, entity);

            return entityXml.ToString();
        }
        public static object GetXmlToObject(string xml, Type objType)
        {
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            xmlDoc.LoadXml(xml);

            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(objType);

            return serializer.Deserialize(new System.Xml.XmlNodeReader(xmlDoc.DocumentElement));
        }

        
        #endregion
    }
}
